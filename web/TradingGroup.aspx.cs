﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class TradingGroup : PageWithNoControlState
    {
        private bool _groupIdObtained;
        protected int _groupId;
        protected bool _rename;
        //renaming name
        protected string _groupName;
        protected string _validationErrorMsg;
        protected bool _hasActiveSystems;
        protected bool _hasMissingTypes;


        protected int? _systemTypeIdToAddPagePlan;
        protected int? _pageIdToModify;
        protected string _beginSymbol;
        protected string _endSymbol;
        protected int? _stripsNum;
        protected bool _addSystemType;
        private List<int> _availableTypeIdsForEdditedGroup;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated || !Utils.IsKgtUser(Session))
            {
                if (GetGroup() && CheckIsGroupKnown())
                {
                    if (!PerformQueryAction())
                    {
                        FillGroupsStatisticsRepeater();
                    }
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        private bool GetGroup()
        {
            if (!_groupIdObtained)
            {
                if (!int.TryParse(Request.QueryString["GroupId"], out _groupId))
                {
                    this.AC.ReportError("Malformed or unspecified group id in URL");
                    this.AC.AllowAutoRefresh = false;
                    _groupIdObtained = false;
                }

                _groupIdObtained = true;
            }
            return _groupIdObtained;
        }

        private bool CheckIsGroupKnown()
        {
            if (string.IsNullOrEmpty(GlobalState.GetGroupName(_groupId)))
            {
                this.AC.ReportError("Unknown group id - group was likely already deleted");
                this.AC.AllowAutoRefresh = false;
                return false;
            }

            return true;
        }

        protected string GroupName
        {
            get
            {
                string groupName = GlobalState.GetGroupName(_groupId);

                if (string.IsNullOrEmpty(groupName))
                    groupName = "<Group Not specified>";

                return groupName;
            }
        }


        private void RedirectToSelf(string query)
        {
            query = AdjustUrlQuery(query);
            if (Request.UrlReferrer != null)
            {
                Response.Redirect(new Uri(Request.UrlReferrer.AbsoluteUri).GetLeftPart(UriPartial.Path) + query, false);
            }
            else
            {
                Response.Redirect("~/TradingGroup.aspx" + query, false);
            }
        }

        protected string AdjustUrlQuery(string originalUrlQuery)
        {
            GetGroup();

            return string.Format("?GroupId={0}{1}{2}", _groupId,
                string.IsNullOrEmpty(originalUrlQuery) ? string.Empty : "&", originalUrlQuery);
        }

        protected string UrlQueryStartString
        {
            get
            {
                GetGroup();

                return "?GroupId=" + _groupId + "&";
            }
        }

        private bool PerformQueryAction()
        {
            //GroupId needs to be allways present and is not counted
            if (Request.QueryString.Keys.Count > 1)
            {
                if (Request.QueryString["IsOrderTransmissionDisabled"] != null)
                {
                    bool disableTransmission;
                    int? systemTypeId = null;

                    if (TryObtainFlipStateInfo("IsOrderTransmissionDisabled", out disableTransmission,
                        out systemTypeId))
                    {
                        if (FlipTransmissionState(disableTransmission, _groupId, systemTypeId))
                        {
                            RefreshBackendStats();
                            RedirectToSelf(null);
                            return true;
                        }
                        else
                        {
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }
                    }
                }
                else if (Request.QueryString["IsGoFlatOnlyOn"] != null)
                {
                    bool goFlatOnlyOn;
                    int? systemTypeId = null;

                    if (TryObtainFlipStateInfo("IsGoFlatOnlyOn", out goFlatOnlyOn, out systemTypeId))
                    {
                        if (FlipGoFlatOnlyState(goFlatOnlyOn, _groupId, systemTypeId))
                        {
                            RefreshBackendStats();
                            RedirectToSelf(null);
                            return true;
                        }
                        else
                        {
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }
                    }
                }
                else if (Request.QueryString["Disable"] != null)
                {
                    bool disable;
                    int groupId;
                    int? systemTypeId = null;

                    if (TryObtainFlipStateInfo("Disable", out disable, out systemTypeId))
                    {
                        if (FlipEnablementState(!disable, _groupId, systemTypeId))
                        {
                            RefreshBackendStats();
                            RedirectToSelf(null);
                            return true;
                        }
                        else
                        {
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }
                    }
                }
                else if (Request.QueryString["ResetAll"] != null)
                {
                    bool reset;
                    int groupId;
                    int? systemTypeId = null;

                    if (TryObtainFlipStateInfo("ResetAll", out reset, out systemTypeId))
                    {
                        if (ResetMulti(_groupId, systemTypeId))
                        {
                            RefreshBackendStats();
                            RedirectToSelf(null);
                            return true;
                        }
                        else
                        {
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }
                    }
                }
                else if (Request.QueryString["PagesPlanModifyOne"] != null)
                {
                    int pageIdToModifyLocal;
                    if (int.TryParse(Request.QueryString["PagesPlanModifyOne"], out pageIdToModifyLocal))
                    {
                        _validationErrorMsg = Request.QueryString["ValidationErrorMsg"];
                        _beginSymbol = Request.QueryString["BeginSymbol"];
                        _endSymbol = Request.QueryString["EndSymbol"];
                        int stripsNumLocal;
                        if (int.TryParse(Request.QueryString["StripsNum"], out stripsNumLocal))
                        {
                            _stripsNum = stripsNumLocal;
                        }
                        _pageIdToModify = pageIdToModifyLocal;
                        this.AC.AllowAutoRefresh = false;
                        return false;
                    }
                }
                else if (Request.QueryString["PagesPlanModifyOneDone"] != null)
                {
                    string beginSymbol = Request.QueryString["BeginSymbol"];
                    string endSymbol = Request.QueryString["EndSymbol"];
                    string counterparty = Request.QueryString["Ctp"];
                    int stripsNum;
                    int pageId;
                    int systemTypeId;
                    if (
                        int.TryParse(Request.QueryString["StripsNum"], out stripsNum)
                        && int.TryParse(Request.QueryString["PagesPlanModifyOneDone"], out pageId)
                        && int.TryParse(Request.QueryString["SystemTypeId"], out systemTypeId))
                    {
                        string errorMessage;
                        if (
                            !IsSymbolRangeValid(beginSymbol, endSymbol, _groupId, systemTypeId, counterparty, pageId,
                                                out errorMessage))
                        {
                            this.RedirectToSelf(
                                string.Format(
                                    "PagesPlanModifyOne={0}&BeginSymbol={1}&EndSymbol={2}&StripsNum={3}&ValidationErrorMsg={4}",
                                    pageId, beginSymbol, endSymbol, stripsNum, errorMessage));
                            return true;
                        }

                        if (PagesPlanChangeSystem(pageId, beginSymbol, endSymbol, stripsNum))
                        {
                            RefreshBackendStats();
                            RedirectToSelf(null);
                            return true;
                        }
                        else
                        {
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }
                    }
                }
                else if (Request.QueryString["PagesPlanAddOne"] != null)
                {
                    _validationErrorMsg = Request.QueryString["ValidationErrorMsg"];
                    _beginSymbol = Request.QueryString["BeginSymbol"];
                    _endSymbol = Request.QueryString["EndSymbol"];
                    int stripsNumLocal;
                    if (int.TryParse(Request.QueryString["StripsNum"], out stripsNumLocal))
                    {
                        _stripsNum = stripsNumLocal;
                    }

                    int systemTypeIdToAddLocal;
                    if (int.TryParse(Request.QueryString["PagesPlanAddOne"], out systemTypeIdToAddLocal))
                    {
                        _systemTypeIdToAddPagePlan = systemTypeIdToAddLocal;
                        this.AC.AllowAutoRefresh = false;
                        return false;
                    }
                }
                else if (Request.QueryString["PagesPlanAddOneDone"] != null)
                {
                    string beginSymbol = Request.QueryString["BeginSymbol"];
                    string endSymbol = Request.QueryString["EndSymbol"];
                    string counterparty = Request.QueryString["Ctp"];
                    int stripsNum;
                    int systemTypeId;
                    if (
                        int.TryParse(Request.QueryString["StripsNum"], out stripsNum)
                        && int.TryParse(Request.QueryString["SystemTypeId"], out systemTypeId))
                    {
                        string errorMessage;
                        if (
                            !IsSymbolRangeValid(beginSymbol, endSymbol, _groupId, systemTypeId, counterparty, null,
                                                out errorMessage))
                        {
                            this.RedirectToSelf(
                                string.Format(
                                    "PagesPlanAddOne={0}&BeginSymbol={1}&EndSymbol={2}&StripsNum={3}&ValidationErrorMsg={4}",
                                    systemTypeId, beginSymbol, endSymbol, stripsNum, errorMessage));
                            return true;
                        }

                        if (PagesPlanInsertSystem(_groupId, systemTypeId, beginSymbol, endSymbol, stripsNum))
                        {
                            RefreshBackendStats();
                            RedirectToSelf(null);
                            return true;
                        }
                        else
                        {
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }
                    }
                }
                else if (Request.QueryString["PagesPlanRemoveOne"] != null)
                {
                    int pageIdToDelete;
                    if (int.TryParse(Request.QueryString["PagesPlanRemoveOne"], out pageIdToDelete))
                    {
                        string errorMessage;
                        if (!CanDeletePagesPlan(pageIdToDelete, out errorMessage))
                        {
                            this.AC.ReportError(errorMessage);
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }

                        if (PagesPlanDeleteSystem(pageIdToDelete))
                        {
                            RefreshBackendStats();
                            RedirectToSelf(null);
                            return true;
                        }
                        else
                        {
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }
                    }
                }
                else if (Request.QueryString["DeleteSystemType"] != null)
                {
                    int tradingTypeId;
                    if (int.TryParse(Request.QueryString["DeleteSystemType"], out tradingTypeId))
                    {
                        if (DeleteSystemType(tradingTypeId, _groupId))
                        {
                            RefreshBackendStats();
                            RedirectToSelf(null);
                            return true;
                        }
                        else
                        {
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }
                    }
                }
                else if (Request.QueryString["DeleteGroup"] != null)
                {
                    if (DeleteGroup(_groupId))
                    {
                        RefreshBackendStats();
                        GlobalState.RefreshGroupNames();
                        RedirectToSelf(null);
                        return true;
                    }
                    else
                    {
                        this.AC.AllowAutoRefresh = false;
                        return false;
                    }
                }
                else if (Request.QueryString["RenameGroup"] != null)
                {
                    _validationErrorMsg = Request.QueryString["ValidationErrorMsg"];
                    _groupName = Request.QueryString["GroupName"];
                    _rename = true;
                    this.AC.AllowAutoRefresh = false;
                    return false;
                }
                else if (Request.QueryString["GroupRenameDone"] != null)
                {
                    string newName = Request.QueryString["GroupRenameDone"];

                    
                        string errorMessage = null;

                        if (string.IsNullOrWhiteSpace(newName))
                        {
                            errorMessage = "Group name cannot be empty!";
                        }

                        if (GlobalState.AllGroupNames.Contains(newName, StringComparer.InvariantCultureIgnoreCase))
                        {
                            errorMessage = "Entered group name is already in use!";
                        }

                        if (errorMessage != null)
                        {
                            this.RedirectToSelf(
                                string.Format(
                                    "RenameGroup=true&GroupName={0}&ValidationErrorMsg={1}",
                                    Server.HtmlEncode(newName), errorMessage));
                            return true;
                        }

                        if (RenameGroup(_groupId, newName))
                        {
                            RefreshBackendStats();
                            GlobalState.RefreshGroupNames();
                            RedirectToSelf(null);
                            return true;
                        }
                        else
                        {
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }
                }
                else if (Request.QueryString["AddSystemType"] != null)
                {
                    _validationErrorMsg = Request.QueryString["ValidationErrorMsg"];

                    _addSystemType = true;
                    this.AC.AllowAutoRefresh = false;
                    return false;
                }
                else if (Request.QueryString["AddSystemTypeDone"] != null)
                {
                    int typeId;

                    if (int.TryParse(Request.QueryString["AddSystemTypeDone"], out typeId))
                    {
                        if (AddTypeToGroup(_groupId, typeId))
                        {
                            RefreshBackendStats();
                            RedirectToSelf(null);
                            return true;
                        }
                        else
                        {
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }
                    }
                }


                this.AC.ReportError("Unknown or malformed query action. Please remove the query from URL or retry.");
                this.AC.AllowAutoRefresh = false;
            }

            return false;
        }

        private bool TryObtainFlipStateInfo(string stateFlagName, out bool requestedState, out int? systemTypeId)
        {
            requestedState = false;
            systemTypeId = null;

            if (Request.QueryString[stateFlagName] != null)
            {
                if (
                    bool.TryParse(Request.QueryString[stateFlagName], out requestedState)
                    &&
                    (Request.QueryString["TradingSystemTypeId"] == null
                        || Utils.TryParseIntIntToNullable(Request.QueryString["TradingSystemTypeId"], out systemTypeId))
                    )
                {
                    return true;
                }
            }

            return false;
        }

        private bool FlipTransmissionState(bool disableTransmission, int groupId, int? systemTypeId)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[TradingSystemsGroupUpdateTransmissionControl_SP]", connection);
                        command.Parameters.AddWithValue("@OrderTransmissionDisabled", disableTransmission);
                        command.Parameters.AddWithValue("@SystemsGroupId", groupId);
                        if (systemTypeId.HasValue)
                            command.Parameters.AddWithValue("@SystemTypeId", systemTypeId);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (SqlException exc)
            {
                this.AC.ReportError(exc.Message.Replace(Environment.NewLine, "<BR />"));
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool FlipGoFlatOnlyState(bool isGoFlatOnlyOn, int groupId, int? systemTypeId)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[TradingSystemsGroupUpdateGoFlatOnly_SP]", connection);
                        command.Parameters.AddWithValue("@GoFlatOnlyOn", isGoFlatOnlyOn);
                        command.Parameters.AddWithValue("@SystemsGroupId", groupId);
                        if (systemTypeId.HasValue)
                            command.Parameters.AddWithValue("@SystemTypeId", systemTypeId);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (SqlException exc)
            {
                this.AC.ReportError(exc.Message.Replace(Environment.NewLine, "<BR />"));
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool FlipEnablementState(bool enable, int groupId, int? systemTypeId)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[TradingSystemsGroupDisableEnableMulti_SP]", connection);
                        command.Parameters.AddWithValue("@Enable", enable);
                        command.Parameters.AddWithValue("@SystemsGroupId", groupId);
                        if (systemTypeId.HasValue)
                            command.Parameters.AddWithValue("@SystemTypeId", systemTypeId);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (SqlException exc)
            {
                this.AC.ReportError(exc.Message.Replace(Environment.NewLine, "<BR />"));
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }


        private bool ResetMulti(int groupId, int? systemTypeId)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[TradingSystemsGroupResetMulti_SP]", connection);
                        command.Parameters.AddWithValue("@SystemsGroupId", groupId);
                        if (systemTypeId.HasValue)
                            command.Parameters.AddWithValue("@SystemTypeId", systemTypeId);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (SqlException exc)
            {
                this.AC.ReportError(exc.Message.Replace(Environment.NewLine, "<BR />"));
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }


        private bool PagesPlanChangeSystem(int pageId, string beginSymbol, string endSymbol, int stripsNum)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[UpdateTradingSystemMonitoringPages_SP]", connection);
                        command.Parameters.AddWithValue("@TradingSystemMonitoringPageId", pageId);
                        command.Parameters.AddWithValue("@BeginSymbol", beginSymbol);
                        command.Parameters.AddWithValue("@EndSymbol", endSymbol);
                        command.Parameters.AddWithValue("@StripsCount", stripsNum);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool PagesPlanInsertSystem(int systemGroupId, int systemTypId, string beginSymbol, string endSymbol, int stripsNum)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[InsertTradingSystemMonitoringPages_SP]", connection);
                        command.Parameters.AddWithValue("@BeginSymbol", beginSymbol);
                        command.Parameters.AddWithValue("@EndSymbol", endSymbol);
                        command.Parameters.AddWithValue("@StripsCount", stripsNum);
                        command.Parameters.AddWithValue("@SystemGroupId", systemGroupId);
                        command.Parameters.AddWithValue("@SystemTypeId", systemTypId);
                        SqlParameter tradingSystemPageIdParam = new SqlParameter("@TradingSystemMonitoringPageId", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        command.Parameters.Add(tradingSystemPageIdParam);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool CanDeletePagesPlan(int pagePlanId, out string errorMessage)
        {
            errorMessage = null;
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[GetActiveSystemsCountForPagePlan_SP]", connection);
                        command.Parameters.AddWithValue("@PageId", pagePlanId);
                        command.CommandType = CommandType.StoredProcedure;

                        int activeSystems = (int)command.ExecuteScalar();
                        if (activeSystems > 0)
                        {
                            errorMessage =
                                string.Format(
                                    "Cannot delete page plan id:{0} as it still has {1} active system(s). Please disable those prior deleting the page plan",
                                    pagePlanId, activeSystems);
                            return false;
                        }

                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            errorMessage = "Error in backend during validating ability to delete page plan " + pagePlanId;
            return false;
        }

        private bool PagesPlanDeleteSystem(int pageId)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[DeleteTradingSystemMonitoringPages_SP]", connection);
                        command.Parameters.AddWithValue("@TradingSystemMonitoringPageId", pageId);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool DeleteSystemType(int systemTypeId, int groupId)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[TradingSystemGroupDeleteSubtype_SP]", connection);
                        command.Parameters.AddWithValue("@TradingSystemGroupId", groupId);
                        command.Parameters.AddWithValue("@TradingSystemTypeId", systemTypeId);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool DeleteGroup(int groupId)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[TradingSystemGroupDelete_SP]", connection);
                        command.Parameters.AddWithValue("@TradingSystemGroupId", groupId);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool RenameGroup(int groupId, string newName)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[TradingSystemGroupRename_SP]", connection);
                        command.Parameters.AddWithValue("@TradingSystemGroupId", groupId);
                        command.Parameters.AddWithValue("@NewName", newName);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool AddTypeToGroup(int groupId, int typeId)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[TradingSystemGroupAddType_SP]", connection);
                        command.Parameters.AddWithValue("@GroupId", groupId);
                        command.Parameters.AddWithValue("@TypeId", typeId);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool RefreshBackendStats()
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool IsSymbolRangeValid(string beginSymbol, string endSymbol, int systemGroupId, int systemTypeId, string venueCounterpartyString,
                                        int? pageIdToExcludeFromTest, out string errorMessage)
        {
            errorMessage = null;

            VenueCounterparty venueCounterparty;
            if (!VenueCounterparty.TryParse(venueCounterpartyString, true, out venueCounterparty))
            {
                errorMessage = string.Format("Unrecognized Venue Counterparty [{0}]", venueCounterpartyString);
                return false;
            }

            List<string> supportedSymbols = GlobalState.GetSymbolListSupportedByCounterparty(venueCounterparty);

            if (!supportedSymbols.Contains(beginSymbol))
            {
                errorMessage = string.Format("Symbol [{0}] is not recognized valid symbol for counterparty [{1}].",
                                             beginSymbol, venueCounterparty);
                return false;
            }

            if (!supportedSymbols.Contains(endSymbol))
            {
                errorMessage = string.Format("Symbol [{0}] is not recognized valid symbol for counterparty [{1}].",
                                             endSymbol, venueCounterparty);
                return false;
            }

            if (string.Compare(beginSymbol, endSymbol, StringComparison.InvariantCultureIgnoreCase) > 0)
            {
                errorMessage = "Begin symbol must be below or equal to end symbol.";
                return false;
            }

            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[GetNumberOfConflicitngSymbolRangesForSystemType_SP]", connection);
                        command.Parameters.AddWithValue("@TradingSystemTypeId", systemTypeId);
                        command.Parameters.AddWithValue("@TradingSystemsGroupId", systemGroupId);
                        if (pageIdToExcludeFromTest.HasValue)
                        {
                            command.Parameters.AddWithValue("@PageIdToExclude", pageIdToExcludeFromTest.Value);
                        }
                        command.Parameters.AddWithValue("@BeginSymbol", beginSymbol);
                        command.Parameters.AddWithValue("@EndSymbol", endSymbol);
                        command.CommandType = CommandType.StoredProcedure;
                        int conflicts = (int)command.ExecuteScalar();
                        if (conflicts > 0)
                        {
                            errorMessage =
                                string.Format(
                                    "Selected symbol range conflicts with {0} other existing monitoring plan(s). Select non-overlapping range",
                                    conflicts);
                            return false;
                        }
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            errorMessage = "Error during validating symbols range validity in backend";
            return false;
        }

        private void FillGroupsStatisticsRepeater()
        {
            try
            {
                DataTable dt = this.GetGroupsStatisticsDataTable();
                AdjustChildTableWithPagesPlanData(dt, out _hasActiveSystems, out _hasMissingTypes);
                StatsSummary statsSummary = this.RecalculateTable(dt);
                GroupStatsRepeater.DataSource = dt;
                GroupStatsRepeater.DataBind();
                this.FillGlobalstatsRepeaterFooter(GroupStatsRepeater, statsSummary);
            }
            catch (Exception exc)
            {
                this.AC.ReportException(exc);
            }
        }

        private StatsSummary RecalculateTable(DataTable dt)
        {
            StatsSummary statsSummary = new StatsSummary();
            decimal overalVolumeM = GlobalState.TotalDailyVolume / 1000000;

            dt.Columns.Add("VolumePct", typeof(decimal));
            foreach (DataRow dataRow in dt.Rows)
            {
                if (dataRow["VolumeUsdM"] != DBNull.Value && overalVolumeM != 0m)
                {
                    dataRow["VolumePct"] = (decimal)dataRow["VolumeUsdM"] / overalVolumeM * 100m;
                }
                statsSummary.AddDatRowToStats(dataRow);
            }

            statsSummary.RecalculateStats();
            return statsSummary;
        }

        private static List<string> doNotNullifyColumnNames = new List<string>()
        {
            "TradingSystemTypeName",
            "TradingSystemTypeId",
            "TradingSystemGroupId",
            "Counterparty",
            "PageId",
            "BeginSymbol",
            "EndSymbol",
            "StripsCount"
        };

        private void AdjustChildTableWithPagesPlanData(DataTable dt, out bool hasActiveSystems, out bool hasMissingTypes)
        {
            hasActiveSystems = false;
            hasMissingTypes = true;

            if (dt == null || dt.Rows.Count == 0 || dt.Rows[0]["TradingSystemTypeName"] == DBNull.Value)
                return;

            foreach (DataColumn c in dt.Columns)
            {
                c.AllowDBNull = true; // Allow Nulls in all columns
                c.AutoIncrement = false;
                c.ReadOnly = false;
            }

            dt.Columns.Add("SystemSubrows", typeof(int));
            dt.Columns.Add("AvailableSymbols", typeof(string));
            dt.Columns.Add("IsInsertRow", typeof(bool));

            string currentSystem = "none";
            List<int> separatorRowsIndexes = new List<int>();
            DataRow lastGroupStartingRow = null;

            List<int> availableTypeIds = new List<int>(GlobalState.AllTypeIds);

            int subRowsCnt = 0;
            int currentSubtableRowIdx = 0;
            List<string> availableSymbolsList = new List<string>();
            List<string> availableSymbolsToInsetingRow = null;
            for (int currentRowIdx = 0; currentRowIdx < dt.Rows.Count; currentRowIdx++, currentSubtableRowIdx++)
            {
                DataRow row = dt.Rows[currentRowIdx];
                subRowsCnt++;
                hasActiveSystems |= (row["EnabledSystems"] is int && (int)row["EnabledSystems"] > 0);
                if (row["TradingSystemTypeId"] is int)
                    availableTypeIds.Remove((int)row["TradingSystemTypeId"]);


                bool isNewSystem = (string)row["TradingSystemTypeName"] != currentSystem;

                if (isNewSystem)
                {
                    separatorRowsIndexes.Add(currentRowIdx);
                    currentSystem = (string) row["TradingSystemTypeName"];
                    if (lastGroupStartingRow != null)
                    {
                        if ((int) lastGroupStartingRow["TradingSystemTypeId"] == _systemTypeIdToAddPagePlan)
                        {
                            availableSymbolsToInsetingRow = availableSymbolsList;
                            subRowsCnt++;
                        }
                        lastGroupStartingRow["SystemSubrows"] = subRowsCnt;
                        lastGroupStartingRow["AvailableSymbols"] = string.Join(", ", availableSymbolsList);
                    }
                    lastGroupStartingRow = row;
                    subRowsCnt = 0;
                    availableSymbolsList =
                        new List<string>(
                            GlobalState.GetSymbolListSupportedByCounterparty(
                                (VenueCounterparty)
                                    Enum.Parse(typeof (VenueCounterparty), row["Counterparty"] as string)));
                }
                //repeating row for additional pages plans - nullify
                else
                {
                    foreach (DataColumn dataColumn in dt.Columns)
                    {
                        string columnName = dataColumn.ColumnName;

                        if (!doNotNullifyColumnNames.Contains(columnName))
                        {
                            row[columnName] = DBNull.Value;
                        }
                    }
                }

                if (row["BeginSymbol"] != DBNull.Value && row["EndSymbol"] != DBNull.Value)
                {
                    string beginSymbol = row["BeginSymbol"] as string;
                    string endSymbol = row["EndSymbol"] as string;
                    availableSymbolsList =
                        availableSymbolsList.Where(
                            s =>
                            string.Compare(s, beginSymbol, StringComparison.InvariantCultureIgnoreCase) < 0 ||
                            string.Compare(s, endSymbol, StringComparison.InvariantCultureIgnoreCase) > 0).ToList();
                }

            }
            if (lastGroupStartingRow != null)
            {
                if ((int)lastGroupStartingRow["TradingSystemTypeId"] == _systemTypeIdToAddPagePlan)
                {
                    availableSymbolsToInsetingRow = availableSymbolsList;
                    subRowsCnt++;
                }
                lastGroupStartingRow["SystemSubrows"] = subRowsCnt + 1;
                lastGroupStartingRow["AvailableSymbols"] = string.Join(", ", availableSymbolsList);
            }

            separatorRowsIndexes.Reverse();
            foreach (int separatorRowIndex in separatorRowsIndexes)
            {
                dt.Rows.InsertAt(dt.NewRow(), separatorRowIndex);
            }

            if (_systemTypeIdToAddPagePlan.HasValue)
            {
                List<string> supportedSymbolsList = null;
                int currentRowIdx = 0;
                for (; currentRowIdx < dt.Rows.Count; currentRowIdx++, currentSubtableRowIdx++)
                {
                    DataRow row = dt.Rows[currentRowIdx];
                    if (row["TradingSystemTypeId"] is int && (int)row["TradingSystemTypeId"] == _systemTypeIdToAddPagePlan)
                    {
                        supportedSymbolsList = new List<string>(
                            GlobalState.GetSymbolListSupportedByCounterparty(
                                (VenueCounterparty)
                                Enum.Parse(typeof(VenueCounterparty), row["Counterparty"] as string)));
                    }
                    else if (supportedSymbolsList != null)
                    {
                        break;
                    }
                }

                dt.Rows.InsertAt(dt.NewRow(), currentRowIdx);
                dt.Rows[currentRowIdx]["IsInsertRow"] = true;
                dt.Rows[currentRowIdx]["StripsCount"] = 2;
                dt.Rows[currentRowIdx]["TradingSystemTypeId"] = dt.Rows[currentRowIdx - 1]["TradingSystemTypeId"];
                dt.Rows[currentRowIdx]["Counterparty"] = dt.Rows[currentRowIdx - 1]["Counterparty"];
                dt.Rows[currentRowIdx]["TradingSystemTypeName"] = dt.Rows[currentRowIdx - 1]["TradingSystemTypeName"];
                dt.Rows[currentRowIdx]["TradingSystemGroupId"] = dt.Rows[currentRowIdx - 1]["TradingSystemGroupId"];
                dt.Rows[currentRowIdx]["BeginSymbol"] = availableSymbolsToInsetingRow[0];

                //get the last symbol in continuous available range
                for (int supportedSymbolIdx = 0; supportedSymbolIdx < supportedSymbolsList.Count; supportedSymbolIdx++)
                {
                    if (supportedSymbolsList[supportedSymbolIdx] == availableSymbolsToInsetingRow[0])
                    {
                        int availableSymbolsIdx = 0;
                        for (; availableSymbolsIdx < availableSymbolsToInsetingRow.Count; availableSymbolsIdx++, supportedSymbolIdx++)
                        {
                            if (supportedSymbolsList[supportedSymbolIdx] != availableSymbolsToInsetingRow[availableSymbolsIdx])
                                break;
                        }
                        dt.Rows[currentRowIdx]["EndSymbol"] =
                                    availableSymbolsToInsetingRow[availableSymbolsIdx - 1];
                        break;
                    }
                }
            }

            hasMissingTypes = availableTypeIds.Any();
            _availableTypeIdsForEdditedGroup = availableTypeIds;
        }

        private DataTable GetGroupsStatisticsDataTable()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                //bool isTradingEnabled = false;
                using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("[dbo].[GetTradingSystemsGroupOveralDailyStatsAndInfo_SP]", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@GroupId", _groupId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        DataTable dt = new DataTable();
                        dt.Load(reader);
                        return dt;
                    }
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }

            return null;
        }

        private void FillGlobalstatsRepeaterFooter(Repeater statsRepeater, StatsSummary statsSummary)
        {
            (statsRepeater.FindControlInFooter("FooterCell_ConfiguredSystems") as HtmlTableCell).InnerText =
                    statsSummary.ConfiguredSystems.ToString();
            (statsRepeater.FindControlInFooter("FooterCell_ActiveSystems") as HtmlTableCell).InnerText =
                    statsSummary.ActiveSystems.ToString();
            (statsRepeater.FindControlInFooter("FooterCell_HardBlockedSystems") as HtmlTableCell).InnerText =
                    statsSummary.BlockedSystems.ToString();
            (statsRepeater.FindControlInFooter("FooterCell_Volume") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.SystemsVolumeM, 1, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_VolumePct") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.VolumePct, 1, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_Deals") as HtmlTableCell).InnerText =
                    statsSummary.Deals.ToString();
            (statsRepeater.FindControlInFooter("FooterCell_CtpRejections") as HtmlTableCell).InnerText =
                    statsSummary.CtpRejections.ToString();
            (statsRepeater.FindControlInFooter("FooterCell_KGTRejections") as HtmlTableCell).InnerText =
                    statsSummary.KGTRejections.ToString();
            (statsRepeater.FindControlInFooter("FooterCell_PnlGrossPerM") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.PnlGrossPerM, 2, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_CommPerM") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.CommPerM, 2, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_PnlNetPerM") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.PnLNetPerM, 2, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_PnlGross") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.PnlGross, 0, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_Comm") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.Comm, 0, Utils.NullsHandling.DbNullToEmpty);

            var pnlCell = (statsRepeater.FindControlInFooter("FooterCell_PnlNet") as HtmlTableCell);
            pnlCell.InnerText = Utils.FormatDecimalNumber(statsSummary.PnlNet, 0, Utils.NullsHandling.DbNullToEmpty);
            pnlCell.Attributes.Add("class", Utils.GetNumberCellStyle(statsSummary.PnlNet));
        }

        protected void SymbolDropDown_OnPreRender(object sender, EventArgs e)
        {
            DropDownList dropDown = sender as DropDownList;

            dropDown.Items.Clear();

            if (!string.IsNullOrEmpty(dropDown.Attributes["Ctp"]))
            {
                foreach (var value in GlobalState.GetSymbolListSupportedByCounterparty((VenueCounterparty)Enum.Parse(typeof(VenueCounterparty), dropDown.Attributes["Ctp"])))
                {
                    dropDown.Items.Add(value.ToString());
                }
            }

            //BEWARE! this can cause exception - databinding inside databinding
            //dropDown.DataSource = Enum.GetValues(typeof(DayOfWeek));
            //dropDown.DataBind();

            if (!string.IsNullOrEmpty(dropDown.ToolTip))
            {
                dropDown.SelectedValue = dropDown.ToolTip;
            }
        }

        protected void TradingTypeDropDown_OnPreRender(object sender, EventArgs e)
        {
            DropDownList dropDown = sender as DropDownList;

            dropDown.Items.Clear();
            int groupId;

            if (int.TryParse(dropDown.Attributes["GrpId"], out groupId))
            {
                foreach (var typeId in _availableTypeIdsForEdditedGroup ?? GlobalState.AllTypeIds)
                {
                    dropDown.Items.Add(new ListItem(GlobalState.GetTradingTypeName(typeId), typeId.ToString()));
                }
            }
        }

        protected void StripsDropDown_OnPreRender(object sender, EventArgs e)
        {
            DropDownList dropDown = sender as DropDownList;

            dropDown.Items.Clear();

            for (int i = 1; i < 10; i++)
            {
                dropDown.Items.Add(i.ToString());
            }

            //BEWARE! this can cause exception - databinding inside databinding
            //dropDown.DataSource = Enum.GetValues(typeof(DayOfWeek));
            //dropDown.DataBind();

            if (!string.IsNullOrEmpty(dropDown.ToolTip))
            {
                dropDown.SelectedValue = dropDown.ToolTip;
            }
        }

        private class StatsSummary
        {
            public void AddDatRowToStats(DataRow dr)
            {
                if (dr["ConfiguredSystems"] != DBNull.Value)
                    this.ConfiguredSystems += (int)dr["ConfiguredSystems"];

                if (dr["EnabledSystems"] != DBNull.Value)
                    this.ActiveSystems += (int)dr["EnabledSystems"];

                if (dr["HardBlockedSystems"] != DBNull.Value)
                    this.BlockedSystems += (int)dr["HardBlockedSystems"];

                if (dr["VolumeUsdM"] != DBNull.Value)
                    this.SystemsVolumeM += (decimal)dr["VolumeUsdM"];

                if (dr["VolumePct"] != DBNull.Value)
                    this.VolumePct += (decimal)dr["VolumePct"];

                if (dr["DealsNum"] != DBNull.Value)
                    this.Deals += (int)dr["DealsNum"];

                if (dr["CtpRejectionsNum"] != DBNull.Value)
                    this.CtpRejections += (int)dr["CtpRejectionsNum"];

                if (dr["KGTRejectionsNum"] != DBNull.Value)
                    this.KGTRejections += (int)dr["KGTRejectionsNum"];

                if (dr["PnlGrossUsd"] != DBNull.Value)
                    this.PnlGross += (decimal)dr["PnlGrossUsd"];

                if (dr["CommissionsUsd"] != DBNull.Value)
                    this.Comm += (decimal)dr["CommissionsUsd"];

                if (dr["PnlNetUsd"] != DBNull.Value)
                    this.PnlNet += (decimal)dr["PnlNetUsd"];
            }

            public void RecalculateStats()
            {
                if (this.SystemsVolumeM != 0m)
                {
                    this.PnlGrossPerM = this.PnlGross / this.SystemsVolumeM;
                    this.CommPerM = this.Comm / this.SystemsVolumeM;
                    this.PnLNetPerM = this.PnlNet / this.SystemsVolumeM;
                }
            }

            public int ConfiguredSystems { get; set; }
            public int ActiveSystems { get; set; }
            public int BlockedSystems { get; set; }
            public decimal SystemsVolumeM { get; set; }
            public decimal VolumePct { get; set; }
            public int Deals { get; set; }
            public int CtpRejections { get; set; }
            public int KGTRejections { get; set; }
            public decimal PnlGrossPerM { get; set; }
            public decimal CommPerM { get; set; }
            public decimal PnLNetPerM { get; set; }
            public decimal PnlGross { get; set; }
            public decimal Comm { get; set; }
            public decimal PnlNet { get; set; }
        }

    }
}