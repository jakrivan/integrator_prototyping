﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" MasterPageFile="~/Site.Master" CodeBehind="TimerTest2.aspx.cs" Inherits="KGTKillSwitchWeb.TimerTest2" %>

<%@ Register Src="~/AutorefreshContainer.ascx" TagPrefix="uc1" TagName="AutorefreshContainer" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title></title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    <%--<script type="text/javascript" src="Scripts/KGTScripts.js"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

        
        <uc1:autorefreshcontainer runat="server" id="AutorefreshContainer"  AutorefreshMillisecondsInterval="1000" AllowAutoRefresh="True">
            
            <%--<AsyncPostBackTrigger ControlID="LinkButton1"></AsyncPostBackTrigger>--%>
            <%--<AsyncPostBackTrigger ControlID="LinkButton2"></AsyncPostBackTrigger>--%>
            
            <ContentTemplate>
                
                
                <asp:Label runat="server" ID="LblTime"></asp:Label>
        
                <a href="TimerTest2.aspx?blah=2">Update</a>
            <asp:LinkButton ID="LinkButton1" runat="server" Text="UpdateX" PostBackUrl="TimerTest2.aspx?blah=2"></asp:LinkButton>
            
            <asp:Panel runat="server" ID="EditPanel" Visible="False">
                <asp:TextBox runat="server" ID="txtEdit"></asp:TextBox>
                <asp:LinkButton ID="LinkButton2" runat="server" Text="Done" OnCommand="OnCommand" CommandName="Done"></asp:LinkButton>
            </asp:Panel>
            
            <asp:Label runat="server" ID="lblResult"></asp:Label>
                
                
                <asp:ImageButton ID="TradingOffImageButton2" Visible="True" runat="server" ImageUrl="Styles/pause_big.png" style="display: block; margin-left: auto; margin-right: auto;" 
                    PostBackUrl="TimerTest2.aspx?Action=turnon" OnClientClick="if(!confirm('Are you sure you want to enable trading?')) return false; DisableAutoRefresh();"/>
                
                
                
                <asp:ImageButton ID="ImageButton1" Visible="True" runat="server" ImageUrl="Styles/pause_big.png" style="display: block; margin-left: auto; margin-right: auto;" 
                    PostBackUrl="TimerTest2.aspx?Action=turnoff" OnClientClick="DisableAutoRefresh();" />
                
                <asp:Label runat="server" ID="lblAction"></asp:Label>


            </ContentTemplate>
        </uc1:autorefreshcontainer>

</asp:Content>
