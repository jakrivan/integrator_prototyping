﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Schedule.aspx.cs" Inherits="KGTKillSwitchWeb.Schedule" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="KGTKillSwitchWeb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Trading Schedule</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></ajaxToolkit:ToolkitScriptManager>

    <asp:Panel ID="ErrorPanel" runat="server" Visible="False">
                        <h2  style="text-align: center; color: red">There was an error during obtainig/updating settings.</h2>
                    </asp:Panel>
    
    <asp:Panel ID="ValueChangedPanel" runat="server" Visible="False">
                        <h2  style="text-align: center; color: red">Some of the settings for edited row changed in the meantime. CHANGES WERE NOT PERFORMED, you can try again.</h2>
                    </asp:Panel>

    <asp:Panel ID="Panel1" runat="server" style="text-align: right">
        <asp:Label ID="LblUpdatedOn" runat="server" style="text-align:right;" Text="Rendered At <not updated>"></asp:Label>
        &nbsp;<asp:LinkButton ID="LinkBtnUpdate" runat="server">Update Now</asp:LinkButton>
        <br />
        <asp:Label ID="LblSettingsUpdatedOn" runat="server" style="text-align:right;" Text="Settings updated <not known>"></asp:Label>
    </asp:Panel>
    
    <asp:HiddenField ID="HiddenLastUpdatedUtc" runat="server" />

    <h2>Systems Trading Control Schedule:</h2>
    <br/>
    Conversions of weekly and daily schedules to other timezones are valid for this week only, they might change after DST switch dates. Those are the comming DST changes for this year:
    <ul>
        <li>New Zealand Time:&nbsp;&nbsp;&nbsp;<asp:Label ID="nztLiteral" runat="server"/></li>
        <li>London Time:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="btLiteral" runat="server"/></li>
        <li>Central Europe Time:&nbsp;<asp:Label ID="cetLiteral" runat="server"/></li>
        <li>Eastern Time:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="etLiteral" runat="server"/></li>
    </ul>
    <br/>
    
    <%----%>
    
    
    
    <asp:Panel ID="DailySchedulesHeaderCollapsePanel" runat="server" CssClass="collapsePanelTitle" ToolTip="Click to toggle expansion">
    <asp:Image ID="DailySchedulesHeaderCollapseImage" runat="server"/>&nbsp;&nbsp;
    <asp:Label ID="DailySchedulesHeader" runat="server" Text="Daily schedules:" CssClass="myH3"></asp:Label>
    </asp:Panel>
    <br/>
    <ajaxToolkit:CollapsiblePanelExtender 
        ID="CollapsiblePanelExtender3" 
        runat="server"
        TargetControlID="DailyScheduleGridViewPanel"
        CollapseControlID="DailySchedulesHeaderCollapsePanel"
        ExpandControlID="DailySchedulesHeaderCollapsePanel"
        ImageControlID="DailySchedulesHeaderCollapseImage"
        ExpandedImage="~/Styles/collapse.jpg"
        CollapsedImage="~/Styles/expand.jpg"
        ExpandedText="Click to expand"
        CollapsedText="Click to collapse"
        ExpandDirection="Vertical">
    </ajaxToolkit:CollapsiblePanelExtender> 
    <asp:Panel ID="DailyScheduleGridViewPanel" runat="server"  Width="1000">
        <%--AlternatingRowStyle-CssClass="gridAltRow"--%>
    <asp:GridView 
        ID="DailyScheduleGridView" runat="server"
        GridLines="None" autogeneratecolumns="False" Width="1000"
        CssClass="mGrid" RowStyle-CssClass="gridRow" 
        onrowcancelingedit="GridView_RowCancelingEdit"
        onrowediting="GridView_RowEditing"
        onrowupdating="GridView_RowUpdating"
        OnRowCommand="GridView_RowCommand" OnDataBound="GridView_DataBound" ShowFooter="False" ShowHeaderWhenEmpty="True">

        <Columns>
            <asp:TemplateField>
                <ItemStyle Width="60"></ItemStyle> 
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenScheduleId" runat="server" Value='<%# ((DataRowView)Container.DataItem)["ScheduleEntryId"] %>'/>
                    <asp:LinkButton ID="DisableBtn" runat="server" 
                        Visible='<%# (bool) ((DataRowView)Container.DataItem)["IsEnabled"] %>' CommandName="Disable" Text="Disable" CommandArgument='<%# Container.DataItemIndex %>'/>
                    
                    <asp:LinkButton ID="EnableBtn" runat="server"
                        Visible='<%# !(bool) ((DataRowView)Container.DataItem)["IsEnabled"] %>' CommandName="Enable" Text="Enable" CommandArgument='<%# Container.DataItemIndex %>'/>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenScheduleId" runat="server" Value='<%# ((DataRowView)Container.DataItem)["ScheduleEntryId"] %>'/>
                    <asp:CheckBox runat="server" ID="EnabledChckBx" Checked='<%# (bool) ((DataRowView)Container.DataItem)["IsEnabled"] %>' ToolTip="Check to create entry as enabled"/>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:CheckBox runat="server" ID="EnabledChckBx" Checked="True" ToolTip="Check to create entry as enabled"/>
                </FooterTemplate>
            </asp:TemplateField>
            
            
            
            <asp:TemplateField HeaderText="Time">    
                <ItemTemplate>
                    <asp:Literal ID="TimeLbl" runat="server" Text='<%# Eval("ScheduleTimeZoneSpecific", "{0:HH:mm:ss}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TimeTxt" runat="server" Text='<%# Eval("ScheduleTimeZoneSpecific", "{0:HH:mm:ss}") %>' />
                    <asp:CustomValidator ID="TimeValidator" ValidationGroup="DailyUpdateValidation" runat="server" ControlToValidate="TimeTxt" Display="Dynamic" ErrorMessage="Time Invalid" ValidateEmptyText="True" OnServerValidate="TimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TimeTxt" runat="server" />
                    <asp:CustomValidator ID="InsertDateValidator" ValidationGroup="DailyInsertValidation" runat="server" ControlToValidate="TimeTxt" Display="Dynamic" ErrorMessage="Time Invalid" ValidateEmptyText="True" OnServerValidate="TimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="TimeZone">    
                <ItemTemplate>
                    <asp:Literal ID="TimeZoneLbl" runat="server" Text='<%# Utils.ReplaceSpacesWithNonBreakableSpaces(TimeZoneHelpers.GetTimeZoneDescription(((DataRowView)Container.DataItem)["TimeZoneCode"].ToString())) %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList runat="server" ID="TimeZoneDropDown" OnPreRender="TimeZoneDropDown_OnPreRender" ToolTip='<%# ((DataRowView)Container.DataItem)["TimeZoneCode"] %>'/>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList runat="server" ID="TimeZoneDropDown" OnPreRender="TimeZoneDropDown_OnPreRender"/>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="New&nbsp;York&nbsp;(ET)">    
                <ItemTemplate>
                    <asp:Literal ID="TimeEtLbl" runat="server" Text='<%# Eval("ScheduleTimeEt", "{0:HH:mm:ss}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                </EditItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="UTC">    
                <ItemTemplate>
                    <asp:Literal ID="TimeUtcLbl" runat="server" Text='<%# Eval("ScheduleTimeUtc", "{0:HH:mm:ss}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                </EditItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="London&nbsp;(BT)">    
                <ItemTemplate>
                    <asp:Literal ID="TimeBtLbl" runat="server" Text='<%# Eval("ScheduleTimeBt", "{0:HH:mm:ss}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                </EditItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Paris&nbsp;(CET)">    
                <ItemTemplate>
                    <asp:Literal ID="TimeCetLbl" runat="server" Text='<%# Eval("ScheduleTimeCet", "{0:HH:mm:ss}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                </EditItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Action">    
                <ItemTemplate>
                    <asp:Literal ID="ActionLbl" runat="server" Text='<%# ((DataRowView)Container.DataItem)["ActionName"] %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList runat="server" ID="ActionDropDown" OnPreRender="ActionDropDown_OnPreRender" ToolTip='<%# ((DataRowView)Container.DataItem)["ActionName"] %>'/>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList runat="server" ID="ActionDropDown" OnPreRender="ActionDropDown_OnPreRender" />
                </FooterTemplate>
            </asp:TemplateField>
            
            
            <asp:TemplateField HeaderText="Description">    
                <ItemTemplate>
                    <asp:Literal ID="DescLbl" runat="server" Text='<%# ((DataRowView)Container.DataItem)["ScheduleDescription"] %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="DescTxt" runat="server" Text='<%# ((DataRowView)Container.DataItem)["ScheduleDescription"] %>' />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="DescTxt" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            
            <asp:TemplateField>
                <ItemStyle Width="60"></ItemStyle> 
                <ItemTemplate>
                    <%--'Edit' is asp.net recognized command and is handeled by framework in a special way --%>
                    <asp:LinkButton ID="EditBtn" runat="server" 
                         CommandName="Edit" Text="Change" ValidationGroup="DailyUpdateValidation" CommandArgument='<%# Container.DataItemIndex %>'/>
                </ItemTemplate>
                <EditItemTemplate>
                    <%--'Update' is asp.net recognized command and is handeled by framework in a special way --%>
                    <%--'Cancel' is asp.net recognized command and is handeled by framework in a special way --%>
                    <asp:LinkButton ID="UpdateBtn" runat="server" 
                         CommandName="Update" Text="Update" ValidationGroup="DailyUpdateValidation" CommandArgument='<%# Container.DataItemIndex %>'
                        />&nbsp;<asp:LinkButton ID="CancelBtn" runat="server" 
                         CommandName="Cancel" Text="Cancel" ValidationGroup="NoValidation" CommandArgument='<%# Container.DataItemIndex %>'/>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:LinkButton ID="InsertBtn" runat="server" 
                         CommandName="InsertMe" Text="Insert" ValidationGroup="DailyInsertValidation"/>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                <ItemStyle Width="60"></ItemStyle> 
                <ItemTemplate>
                    <%--Be careful! 'Delete' is asp.net recognized command and is handeled by framework in a special way --%>
                    <asp:LinkButton ID="DeleteBtn" runat="server" OnClientClick="return confirm('Do you really want delete this schedule?');" 
                         CommandName="DeleteMe" Text="Delete" ValidationGroup="NoValidation" CommandArgument='<%# Container.DataItemIndex %>'/>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:LinkButton ID="CancelInsertBtn" runat="server" OnClientClick="return confirm('Do you really want cancel insertion and remove values you entered?');" 
                         CommandName="CancelInsertMe" Text="Cancel" ValidationGroup="NoValidation"/>
                </FooterTemplate>
            </asp:TemplateField>
            
        </Columns>

    </asp:GridView>
        <asp:Panel ID="Panel4" runat="server" style="text-align:right;">
            <asp:LinkButton ID="DailyInsertDataBtn" runat="server" style="text-align:right;" OnClick="DailyInsertDataBtn_Click">Add new</asp:LinkButton>    
        </asp:Panel> 
        <br />
	</asp:Panel>

    <br/>
    <br/>

    <asp:Panel ID="SpecSchedulesHeaderCollapsePanel" runat="server" CssClass="collapsePanelTitle" ToolTip="Click to toggle expansion">
    <asp:Image ID="SpecSchedulesHeaderCollapseImage" runat="server"/>&nbsp;&nbsp;
    <asp:Label ID="SpecSchedulesHeader" runat="server" Text="Specific dates schedules:" CssClass="myH3"></asp:Label>
    </asp:Panel>
    <br/>
    <ajaxToolkit:CollapsiblePanelExtender 
        ID="CollapsiblePanelExtender1" 
        runat="server"
        TargetControlID="SpecificDatesScheduleGridViewPanel"
        CollapseControlID="SpecSchedulesHeaderCollapsePanel"
        ExpandControlID="SpecSchedulesHeaderCollapsePanel"
        ImageControlID="SpecSchedulesHeaderCollapseImage"
        ExpandedImage="~/Styles/collapse.jpg"
        CollapsedImage="~/Styles/expand.jpg"
        ExpandedText="Click to expand"
        CollapsedText="Click to collapse"
        ExpandDirection="Vertical">
    </ajaxToolkit:CollapsiblePanelExtender> 
    <asp:Panel ID="SpecificDatesScheduleGridViewPanel" runat="server" Width="1000">
        <%--AlternatingRowStyle-CssClass="gridAltRow"--%>
    <asp:GridView 
        ID="SpecificDatesScheduleGridView" runat="server"
        GridLines="None" autogeneratecolumns="False" Width="1000"
        CssClass="mGrid" RowStyle-CssClass="gridRow"
        onrowcancelingedit="GridView_RowCancelingEdit"
        onrowediting="GridView_RowEditing"
        onrowupdating="GridView_RowUpdating"
        OnRowCommand="GridView_RowCommand" OnDataBound="GridView_DataBound" ShowFooter="False" ShowHeaderWhenEmpty="True">
        
        <EmptyDataTemplate>
            No Records Found
        </EmptyDataTemplate>

        <Columns>
            <asp:TemplateField>
                <ItemStyle Width="60"></ItemStyle> 
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenScheduleId" runat="server" Value='<%# ((DataRowView)Container.DataItem)["ScheduleEntryId"] %>'/>
                    <asp:LinkButton ID="DisableBtn" runat="server" 
                        Visible='<%# (bool)((DataRowView)Container.DataItem)["IsEnabled"] %>' CommandName="Disable" Text="Disable" CommandArgument='<%# Container.DataItemIndex %>'/>
                    
                    <asp:LinkButton ID="EnableBtn" runat="server" OnClientClick="return confirm('Do you really want enable this schedule?');" 
                        Visible='<%# !(bool) ((DataRowView)Container.DataItem)["IsEnabled"] %>' CommandName="Enable" Text="Enable" CommandArgument='<%# Container.DataItemIndex %>'/>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenScheduleId" runat="server" Value='<%# ((DataRowView)Container.DataItem)["ScheduleEntryId"] %>'/>
                    <asp:CheckBox runat="server" ID="EnabledChckBx" Checked='<%# (bool)((DataRowView)Container.DataItem)["IsEnabled"] %>' ToolTip="Check to create entry as enabled"/>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:CheckBox runat="server" ID="EnabledChckBx" Checked="True" ToolTip="Check to create entry as enabled"/>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Date">    
                <ItemTemplate>
                    <asp:Literal ID="DateLbl" runat="server" Text='<%# Eval("ScheduleTimeZoneSpecific", "{0:yyyy-MM-dd}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="DateTxt" runat="server" Text='<%# Eval("ScheduleTimeZoneSpecific", "{0:yyyy-MM-dd}") %>' Width="70" />
                    <asp:CustomValidator ID="DateValidator" ValidationGroup="ExactDateUpdateValidation" runat="server" ControlToValidate="DateTxt" Display="Dynamic" ErrorMessage="Date Invalid" ValidateEmptyText="True" OnServerValidate="DateValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    <asp:ImageButton ID="CalendarImgBtn" runat="server" ImageUrl="Styles/Calendar_scheduleHS.png" />
                    <ajaxToolkit:CalendarExtender runat="server"
                                                  TargetControlID="DateTxt"
                                                  Format="yyyy-MM-dd" CssClass="MyCalendar"
                                                  PopupButtonID="CalendarImgBtn" />

                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="DateTxt" runat="server" Width="70" />
                    <asp:CustomValidator ID="DateValidator" ValidationGroup="ExactDateInsertValidation" runat="server" ControlToValidate="DateTxt" Display="Dynamic" ErrorMessage="Date Invalid" ValidateEmptyText="True" OnServerValidate="DateValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    <asp:ImageButton ID="CalendarImgBtn" runat="server" ImageUrl="Styles/Calendar_scheduleHS.png" />
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                  TargetControlID="DateTxt"
                                                  Format="yyyy-MM-dd" CssClass="MyCalendar"
                                                  PopupButtonID="CalendarImgBtn" />
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Time">    
                <ItemTemplate>
                    <asp:Literal ID="TimeLbl" runat="server" Text='<%# Eval("ScheduleTimeZoneSpecific", "{0:HH:mm:ss}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TimeTxt" runat="server" Text='<%# Eval("ScheduleTimeZoneSpecific", "{0:HH:mm:ss}") %>' />
                    <asp:CustomValidator ID="TimeValidator" ValidationGroup="ExactDateUpdateValidation" runat="server" ControlToValidate="TimeTxt" Display="Dynamic" ErrorMessage="Time Invalid" ValidateEmptyText="True" OnServerValidate="TimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TimeTxt" runat="server" />
                    <asp:CustomValidator ID="InsertDateValidator" ValidationGroup="ExactDateInsertValidation" runat="server" ControlToValidate="TimeTxt" Display="Dynamic" ErrorMessage="Time Invalid" ValidateEmptyText="True" OnServerValidate="TimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="TimeZone">    
                <ItemTemplate>
                    <asp:Literal ID="TimeZoneLbl" runat="server" Text='<%# ((DataRowView)Container.DataItem)["TimeZoneCode"] %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList runat="server" ID="TimeZoneDropDown" OnPreRender="TimeZoneDropDown_OnPreRender" ToolTip='<%# Utils.ReplaceSpacesWithNonBreakableSpaces(TimeZoneHelpers.GetTimeZoneDescription(((DataRowView)Container.DataItem)["TimeZoneCode"].ToString())) %>'/>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList runat="server" ID="TimeZoneDropDown" OnPreRender="TimeZoneDropDown_OnPreRender"/>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="New&nbsp;York&nbsp;(ET)">    
                <ItemTemplate>
                    <asp:Literal ID="TimeEtLbl" runat="server" Text='<%# Eval("ScheduleTimeEt", "{0:yyyy-MM-dd HH:mm:ss}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                </EditItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="UTC">    
                <ItemTemplate>
                    <asp:Literal ID="TimeUtcLbl" runat="server" Text='<%# Eval("ScheduleTimeUtc", "{0:yyyy-MM-dd HH:mm:ss}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                </EditItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Paris&nbsp;(CET)">    
                <ItemTemplate>
                    <asp:Literal ID="TimeCetLbl" runat="server" Text='<%# Eval("ScheduleTimeCet", "{0:yyyy-MM-dd HH:mm:ss}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                </EditItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Action">    
                <ItemTemplate>
                    <asp:Literal ID="ActionLbl" runat="server" Text='<%# ((DataRowView)Container.DataItem)["ActionName"] %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList runat="server" ID="ActionDropDown" OnPreRender="ActionDropDown_OnPreRender" ToolTip='<%# ((DataRowView)Container.DataItem)["ActionName"] %>'/>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList runat="server" ID="ActionDropDown" OnPreRender="ActionDropDown_OnPreRender" />
                </FooterTemplate>
            </asp:TemplateField>
            

            
            <asp:TemplateField HeaderText="Description">    
                <ItemTemplate>
                    <asp:Literal ID="DescLbl" runat="server" Text='<%# ((DataRowView)Container.DataItem)["ScheduleDescription"] %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="DescTxt" runat="server" Text='<%# ((DataRowView)Container.DataItem)["ScheduleDescription"] %>' />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="DescTxt" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            
            <%--<asp:CommandField ShowEditButton="True" EditText='Change' ValidationGroup="ExactDateUpdateValidation" >
                <ItemStyle Width="60"></ItemStyle>
            </asp:CommandField>--%>
            
            <asp:TemplateField>
                <ItemStyle Width="60"></ItemStyle> 
                <ItemTemplate>
                    <%--'Edit' is asp.net recognized command and is handeled by framework in a special way --%>
                    <asp:LinkButton ID="EditBtn" runat="server" 
                         CommandName="Edit" Text="Change" ValidationGroup="ExactDateUpdateValidation" CommandArgument='<%# Container.DataItemIndex %>'/>
                </ItemTemplate>
                <EditItemTemplate>
                    <%--'Update' is asp.net recognized command and is handeled by framework in a special way --%>
                    <%--'Cancel' is asp.net recognized command and is handeled by framework in a special way --%>
                    <asp:LinkButton ID="UpdateBtn" runat="server" 
                         CommandName="Update" Text="Update" ValidationGroup="ExactDateUpdateValidation" CommandArgument='<%# Container.DataItemIndex %>'
                        />&nbsp;<asp:LinkButton ID="CancelBtn" runat="server" 
                         CommandName="Cancel" Text="Cancel" ValidationGroup="NoValidation" CommandArgument='<%# Container.DataItemIndex %>'/>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:LinkButton ID="InsertBtn" runat="server" OnClientClick="return confirm('Do you really want insert this schedule?');" 
                         CommandName="InsertMe" Text="Insert" ValidationGroup="ExactDateInsertValidation"/>
                </FooterTemplate>
            </asp:TemplateField>

            
            <asp:TemplateField>
                <ItemStyle Width="60"></ItemStyle> 
                <ItemTemplate>
                    <%--Be careful! 'Delete' is asp.net recognized command and is handeled by framework in a special way --%>
                    <asp:LinkButton ID="DeleteBtn" runat="server" OnClientClick="return confirm('Do you really want delete this schedule?');" 
                         CommandName="DeleteMe" Text="Delete" ValidationGroup="NoValidation" CommandArgument='<%# Container.DataItemIndex %>'/>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:LinkButton ID="CancelInsertBtn" runat="server" OnClientClick="return confirm('Do you really want cancel insertion and remove values you entered?');" 
                         CommandName="CancelInsertMe" Text="Cancel" ValidationGroup="NoValidation"/>
                </FooterTemplate>
            </asp:TemplateField>
            
        </Columns>

    </asp:GridView>  
        <asp:Panel ID="Panel2" runat="server" style="text-align:right;">
            <asp:LinkButton ID="ExactDateInsertDataBtn" runat="server" style="text-align:right;" OnClick="ExactDateInsertDataBtn_Click">Add new</asp:LinkButton>    
        </asp:Panel> 
        <br /> 
    </asp:Panel>
    

    <br/>
    <br/>
    
    
    
    <asp:Panel ID="WeeklySchedulesHeaderCollapsePanel" runat="server" CssClass="collapsePanelTitle" ToolTip="Click to toggle expansion">
    <asp:Image ID="WeeklySchedulesHeaderCollapseImage" runat="server"/>&nbsp;&nbsp;
    <asp:Label ID="WeeklySchedulesHeader" runat="server" Text="Weekly schedules:" CssClass="myH3"></asp:Label>
    </asp:Panel>
    <br/>
    <ajaxToolkit:CollapsiblePanelExtender 
        ID="CollapsiblePanelExtender2" 
        runat="server"
        TargetControlID="WeeklyScheduleGridViewPanel"
        CollapseControlID="WeeklySchedulesHeaderCollapsePanel"
        ExpandControlID="WeeklySchedulesHeaderCollapsePanel"
        ImageControlID="WeeklySchedulesHeaderCollapseImage"
        ExpandedImage="~/Styles/collapse.jpg"
        CollapsedImage="~/Styles/expand.jpg"
        ExpandedText="Click to expand"
        CollapsedText="Click to collapse"
        ExpandDirection="Vertical">
    </ajaxToolkit:CollapsiblePanelExtender> 
    <asp:Panel ID="WeeklyScheduleGridViewPanel" runat="server"  Width="1000">
        <%--AlternatingRowStyle-CssClass="gridAltRow"--%>
    <asp:GridView 
        ID="WeeklyScheduleGridView" runat="server"
        GridLines="None" autogeneratecolumns="False" Width="1000"
        CssClass="mGrid" RowStyle-CssClass="gridRow"
        onrowcancelingedit="GridView_RowCancelingEdit"
        onrowediting="GridView_RowEditing"
        onrowupdating="GridView_RowUpdating"
        OnRowCommand="GridView_RowCommand" OnDataBound="GridView_DataBound" ShowFooter="False" ShowHeaderWhenEmpty="True">

        <Columns>
            <asp:TemplateField>
                <ItemStyle Width="60"></ItemStyle> 
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenScheduleId" runat="server" Value='<%# ((DataRowView)Container.DataItem)["ScheduleEntryId"] %>'/>
                    <asp:LinkButton ID="DisableBtn" runat="server" 
                        Visible='<%# (bool)((DataRowView)Container.DataItem)["IsEnabled"] %>' CommandName="Disable" Text="Disable" CommandArgument='<%# Container.DataItemIndex %>'/>
                    
                    <asp:LinkButton ID="EnableBtn" runat="server" OnClientClick="return confirm('Do you really want enable this schedule?');" 
                        Visible='<%# !(bool) ((DataRowView)Container.DataItem)["IsEnabled"] %>' CommandName="Enable" Text="Enable" CommandArgument='<%# Container.DataItemIndex %>'/>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenScheduleId" runat="server" Value='<%# ((DataRowView)Container.DataItem)["ScheduleEntryId"] %>'/>
                    <asp:CheckBox runat="server" ID="EnabledChckBx" Checked='<%# (bool)((DataRowView)Container.DataItem)["IsEnabled"] %>' ToolTip="Check to create entry as enabled"/>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:CheckBox runat="server" ID="EnabledChckBx" Checked="True" ToolTip="Check to create entry as enabled"/>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Day">    
                <ItemTemplate>
                    <asp:Literal ID="DayLbl" runat="server" Text='<%# string.Format(CultureInfo.InvariantCulture, "{0:dddd}", ((DataRowView)Container.DataItem)["ScheduleTimeZoneSpecific"]) %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList ID="DayDropDown" runat="server" OnPreRender="DayDropDown_OnPreRender" ToolTip='<%# string.Format(CultureInfo.InvariantCulture, "{0:dddd}", ((DataRowView)Container.DataItem)["ScheduleTimeZoneSpecific"]) %>' >
                    </asp:DropDownList>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList runat="server" ID="DayDropDown" OnPreRender="DayDropDown_OnPreRender" />
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Time">    
                <ItemTemplate>
                    <asp:Literal ID="TimeLbl" runat="server" Text='<%# Eval("ScheduleTimeZoneSpecific", "{0:HH:mm:ss}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TimeTxt" runat="server" Text='<%# Eval("ScheduleTimeZoneSpecific", "{0:HH:mm:ss}") %>' />
                    <asp:CustomValidator ID="TimeValidator" ValidationGroup="WeeklyUpdateValidation" runat="server" ControlToValidate="TimeTxt" Display="Dynamic" ErrorMessage="Time Invalid" ValidateEmptyText="True" OnServerValidate="TimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TimeTxt" runat="server" />
                    <asp:CustomValidator ID="InsertDateValidator" ValidationGroup="WeeklyInsertValidation" runat="server" ControlToValidate="TimeTxt" Display="Dynamic" ErrorMessage="Time Invalid" ValidateEmptyText="True" OnServerValidate="TimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="TimeZone">    
                <ItemTemplate>
                    <asp:Literal ID="TimeZoneLbl" runat="server" Text='<%# Utils.ReplaceSpacesWithNonBreakableSpaces(TimeZoneHelpers.GetTimeZoneDescription(((DataRowView)Container.DataItem)["TimeZoneCode"].ToString())) %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList runat="server" ID="TimeZoneDropDown" OnPreRender="TimeZoneDropDown_OnPreRender" ToolTip='<%# ((DataRowView)Container.DataItem)["TimeZoneCode"] %>'/>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList runat="server" ID="TimeZoneDropDown" OnPreRender="TimeZoneDropDown_OnPreRender"/>
                </FooterTemplate>
            </asp:TemplateField>

            
            <asp:TemplateField HeaderText="New&nbsp;York&nbsp;(ET)">    
                <ItemTemplate>
                    <asp:Literal ID="TimeEtLbl" runat="server" Text='<%# Eval("ScheduleTimeEt", "{0:HH:mm:ss}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                </EditItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="UTC">    
                <ItemTemplate>
                    <asp:Literal ID="TimeUtcLbl" runat="server" Text='<%# Eval("ScheduleTimeUtc", "{0:HH:mm:ss}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                </EditItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Paris&nbsp;(CET)">    
                <ItemTemplate>
                    <asp:Literal ID="TimeCetLbl" runat="server" Text='<%# Eval("ScheduleTimeCet", "{0:HH:mm:ss}") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                </EditItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Action">    
                <ItemTemplate>
                    <asp:Literal ID="ActionLbl" runat="server" Text='<%# ((DataRowView)Container.DataItem)["ActionName"] %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList runat="server" ID="ActionDropDown" OnPreRender="ActionDropDown_OnPreRender" ToolTip='<%# ((DataRowView)Container.DataItem)["ActionName"] %>'/>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList runat="server" ID="ActionDropDown" OnPreRender="ActionDropDown_OnPreRender" />
                </FooterTemplate>
            </asp:TemplateField>
            

            
            <asp:TemplateField HeaderText="Description">    
                <ItemTemplate>
                    <asp:Literal ID="DescLbl" runat="server" Text='<%# ((DataRowView)Container.DataItem)["ScheduleDescription"] %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="DescTxt" runat="server" Text='<%# ((DataRowView)Container.DataItem)["ScheduleDescription"] %>' />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="DescTxt" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            
            <asp:TemplateField>
                <ItemStyle Width="60"></ItemStyle> 
                <ItemTemplate>
                    <%--'Edit' is asp.net recognized command and is handeled by framework in a special way --%>
                    <asp:LinkButton ID="EditBtn" runat="server" 
                         CommandName="Edit" Text="Change" ValidationGroup="WeeklyUpdateValidation" CommandArgument='<%# Container.DataItemIndex %>'/>
                </ItemTemplate>
                <EditItemTemplate>
                    <%--'Update' is asp.net recognized command and is handeled by framework in a special way --%>
                    <%--'Cancel' is asp.net recognized command and is handeled by framework in a special way --%>
                    <asp:LinkButton ID="UpdateBtn" runat="server" 
                         CommandName="Update" Text="Update" ValidationGroup="WeeklyUpdateValidation" CommandArgument='<%# Container.DataItemIndex %>'
                        />&nbsp;<asp:LinkButton ID="CancelBtn" runat="server" 
                         CommandName="Cancel" Text="Cancel" ValidationGroup="NoValidation" CommandArgument='<%# Container.DataItemIndex %>'/>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:LinkButton ID="InsertBtn" runat="server" OnClientClick="return confirm('Do you really want insert this schedule?');" 
                         CommandName="InsertMe" Text="Insert" ValidationGroup="WeeklyInsertValidation"/>
                </FooterTemplate>
            </asp:TemplateField>

            
            <asp:TemplateField>
                <ItemStyle Width="60"></ItemStyle> 
                <ItemTemplate>
                    <%--Be careful! 'Delete' is asp.net recognized command and is handeled by framework in a special way --%>
                    <asp:LinkButton ID="DeleteBtn" runat="server" OnClientClick="return confirm('Do you really want delete this schedule?');" 
                         CommandName="DeleteMe" Text="Delete" ValidationGroup="NoValidation" CommandArgument='<%# Container.DataItemIndex %>'/>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:LinkButton ID="CancelInsertBtn" runat="server" OnClientClick="return confirm('Do you really want cancel insertion and remove values you entered?');" 
                         CommandName="CancelInsertMe" Text="Cancel" ValidationGroup="NoValidation"/>
                </FooterTemplate>
            </asp:TemplateField>
            
        </Columns>

    </asp:GridView>
        <asp:Panel ID="Panel3" runat="server" style="text-align:right;">
            <asp:LinkButton ID="WeeklyInsertDataBtn" runat="server" style="text-align:right;" OnClick="WeeklyInsertDataBtn_Click">Add new</asp:LinkButton>    
        </asp:Panel> 
        <br />
    </asp:Panel>
    
    
    

</asp:Content>
