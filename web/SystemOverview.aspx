﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SystemOverview.aspx.cs" Inherits="KGTKillSwitchWeb.SystemOverview" %>
<%@ Import Namespace="System.Data.Common" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>System State Overview</title>
    
    <%--<script type="text/javascript">
        $(".deleteLink").click(function () {
            return confirm('Are you sure you wish to delete this record?');
        });
</script>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <%--OnTick="Timer1_Tick"--%>
            <asp:Timer ID="Timer1" runat="server" Interval="3000">
            </asp:Timer>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
        </Triggers>

        <ContentTemplate>
            
            
    <asp:Panel ID="Panel1" runat="server" style="text-align: right">
        <asp:Label ID="LblUpdatedOn" runat="server" style="text-align:right;" Text="Updated At <not updated>"></asp:Label>
        &nbsp;
        <span runat="server" ID="progressDiv">
            <%--<progress value="0" max="10" runat="server" id="progressBar"/>--%> 
        </span>  
    </asp:Panel>
    

    <asp:Panel ID="ErrorPanel" runat="server" Visible="False">
                        <h2  style="text-align: center; color: red">There was an error during obtainig positions.</h2>
                    </asp:Panel>
    
    <h2>Sessions claim status:</h2>
    <br/>
    <asp:GridView 
        ID="SessionsGridView" runat="server"
        GridLines="None" autogeneratecolumns="False"
        CssClass="mGrid" RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow" Width="200"
        onrowcancelingedit="SessionsGridView_RowCancelingEdit"
        onrowediting="SessionsGridView_RowEditing"
        onrowupdating="SessionsGridView_RowUpdating">
        <Columns>
                        
            <asp:TemplateField HeaderText="Counterparty">    
                <ItemTemplate>
                    <asp:Literal ID="LiteralCtp" runat="server" Text='<%# Bind("CounterpartyCode") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Literal ID="LiteralCtp" runat="server" Text='<%# Bind("CounterpartyCode") %>' />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Instance">
                <ItemTemplate>
                    <asp:Literal ID="LiteralInstance" runat="server" Text='<%# Bind("InstanceName") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownInstance" runat="server">
                    </asp:DropDownList>
                    <asp:HiddenField runat="server" ID="HiddenInstance" Value='<%# Bind("InstanceName") %>'/>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" EditText='Change' />

      </Columns>
    </asp:GridView>
    
    <br/>
    <br/>
    <br/>
    <h2>Instance processes status</h2>
    <br/>
            
            
    <asp:Panel ID="SelectionErrorPanel" runat="server" Visible="False">
                        <h2  style="text-align: center; color: red">Invalid selection...</h2>
                    </asp:Panel>        

    <%--AllowSorting="True"--%>
    <asp:GridView 
        ID="ProcessesGridView" runat="server"
        GridLines="None" autogeneratecolumns="False"
        CssClass="mGrid" RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow" OnRowCommand="ProcessesGridView_RowCommand"
        OnRowDataBound="ProcessesGridView_RowDataBound">
        <Columns>
            
            <asp:TemplateField HeaderText="Instnace" SortExpression="InstanceName">
                 <ItemTemplate>
                     <%--Be carefull!!! Damned HiddenField is screwing whole updatepanel if other controls ids are not unique--%>
                     <asp:HiddenField ID="MyHiddenIdx" runat="server" Value='<%# Bind("ProcessId") %>'/>
                     <asp:Label ID="LabelInstnace" runat="server" Text='<%# Bind("InstanceName") %>' ToolTip ='<%# Bind("InstanceDescription") %>'></asp:Label>
                 </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Process" SortExpression="ProcessName">
                 <ItemTemplate>
                     <asp:Label ID="LabelProcName" runat="server" Text='<%# Bind("ProcessName") %>' ToolTip ='<%# Bind("ProcessDescription") %>'></asp:Label>
                 </ItemTemplate>
            </asp:TemplateField>

            <asp:BoundField DataField="State" HeaderText="State" SortExpression="State"/>
            <asp:BoundField DataField="Endpoint" HeaderText="Endpoint" SortExpression="Endpoint"/>
            <asp:TemplateField HeaderText="Last HB (UTC)" SortExpression="LastHBTimeUtc">
                 <ItemTemplate>
                     <asp:Label ID="LabelLastHb" runat="server" Text='<%# Eval("LastHBTimeUtc", "{0:yyyy-MM-dd HH:mm:ss}") %>' ForeColor='<%# ((DbDataRecord)Container.DataItem)["IsOld"].Equals(1) ? Color.Red : Color.Empty %>'></asp:Label>
                 </ItemTemplate>
            </asp:TemplateField>
            
            <asp:ButtonField ButtonType="Link" CommandName="DeleteMe" Text="Delete" />
      </Columns>
    </asp:GridView>
            
            
       </ContentTemplate>
    </asp:UpdatePanel>
    
    

</asp:Content>
