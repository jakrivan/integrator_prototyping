﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class PositionsOverview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (!IsPostBack)
                {
                    DateTime today = DateTime.UtcNow;
                    Calendar1.TodaysDate = today;
                    Calendar1.SelectedDate = Calendar1.TodaysDate;
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        //protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        //{

        //}

        protected void Page_PreRender(object sender, EventArgs e)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetPolarizedNopPerPair_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@StartTime", Calendar1.SelectedDate);
                        command.Parameters.AddWithValue("@EndTime", Calendar1.SelectedDate.AddDays(1));


                        SqlDataReader reader = command.ExecuteReader();

                        (this.NopLoginView.FindControl("NopGridView") as GridView).DataSource = reader;
                        (this.NopLoginView.FindControl("NopGridView") as GridView).DataBind();
                        reader.Close();
                    }
                }
                catch (Exception exc)
                {
                    this.NopLoginView.FindControl("ErrorPanel").Visible = true;
                }
            }
            else
            {
                this.NopLoginView.FindControl("ErrorPanel").Visible = true;
            }
        }
    }
}