﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class MyInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                this.FillLoginActionGrid();
                this.FillSwitchActionGrid();
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        private void FillLoginActionGrid()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetRecentKillSwitchLoginActivity_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@roleName", SqlDbType.NVarChar).Value =
                            Utils.GetKillSwitchRoleName(Session);

                        if (!Utils.GetIsAdmin(Session))
                        {
                            command.Parameters.AddWithValue("@username", HttpContext.Current.User.Identity.Name);
                        }

                        SqlDataReader reader = command.ExecuteReader();

                        (this.MyInfoLoginView.FindControl("GridViewLoginActions") as GridView).DataSource = reader;
                        (this.MyInfoLoginView.FindControl("GridViewLoginActions") as GridView).DataBind();
                        reader.Close();
                    }
                }
                catch (Exception e)
                {
                    this.MyInfoLoginView.FindControl("ErrorPanel").Visible = true;
                }
            }
            else
            {
                this.MyInfoLoginView.FindControl("ErrorPanel").Visible = true;
            }
        }

        private void FillSwitchActionGrid()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetRecentExternalOrdersTransmissionControlState_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@SwitchName", SqlDbType.NVarChar).Value =
                            Utils.GetKillSwitchBitName(Session);

                        if (!Utils.GetIsAdmin(Session))
                        {
                            command.Parameters.AddWithValue("@ChangedBy", HttpContext.Current.User.Identity.Name);
                        }

                        SqlDataReader reader = command.ExecuteReader();

                        (this.MyInfoLoginView.FindControl("SwitchChangesGridView") as GridView).DataSource = reader;
                        (this.MyInfoLoginView.FindControl("SwitchChangesGridView") as GridView).DataBind();
                        reader.Close();
                    }
                }
                catch (Exception)
                {
                    this.MyInfoLoginView.FindControl("ErrorPanel").Visible = true;
                }
            }
            else
            {
                this.MyInfoLoginView.FindControl("ErrorPanel").Visible = true;
            }
        }
    }
}