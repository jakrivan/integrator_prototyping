﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class SystemDetails : PageWithNoControlState //System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string AdjustUrlQuery(string originalUrlQuery)
        {
            return this.VenueSettingsWithPnl.AdjustUrlQuery(originalUrlQuery);
        }
    }
}