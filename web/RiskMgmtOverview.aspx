<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" EnableViewState="false" ViewStateMode="Disabled" AutoEventWireup="true" CodeBehind="RiskMgmtOverview.aspx.cs" Inherits="KGTKillSwitchWeb.KGTRiskMgtmtOverview" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Common" %>
<%@ Import Namespace="KGTKillSwitchWeb" %>
<%@ Register Src="~/AutorefreshContainer.ascx" TagPrefix="uc1" TagName="AutorefreshContainer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>KGT Risk Management Quick Overview</title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 100%;
            border-style: none;
            border-width: 1px;
            border-spacing: 0px
        }
        .auto-style2 td {
            border: solid 1px #c1c1c1;
        }
    </style>
    
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.2)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.2)" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <uc1:autorefreshcontainer runat="server" id="AC"  AutorefreshMillisecondsInterval="1000" AllowAutoRefresh="True">
        <ContentTemplate>
            
            
    <asp:Panel runat="server" ID="KgtUserWarningPanel" Visible="False">
        <h2  style="text-align: center; color: red">You are viewing limited external version of Risk Management Overview. <a href="~/RiskMgmtOverviewInternal.aspx" ID="A2" runat="server">Click here to transfer to internal view</a></h2>
    </asp:Panel>        

    <asp:Panel ID="ErrorPanel" runat="server" Visible="False">
         <h2  style="text-align: center; color: red">There was an error during obtainig positions.</h2>
        <p style="color: red"><asp:Literal ID="exceptionLiteral" runat="server" Text="?"></asp:Literal></p>
    </asp:Panel>

    <br />
    
    <asp:Panel ID="TradingOnPanel" runat="server" Visible="False">
        <asp:ImageButton ID="TradingOnImageButton" runat="server" ImageUrl="Styles/play_big.png" style="display: block; margin-left: auto; margin-right: auto;" 
            PostBackUrl="RiskMgmtOverview.aspx?Action=turnoff" OnClientClick="DisableAutoRefresh();"/>
        <h2  style="text-align: center" onclick="DisableAutoRefresh();">Orders Transmission is <span style="color: green">ENABLED</span>. <a href="~/RiskMgmtOverview.aspx?Action=turnoff" ID="A1" runat="server">Click here to disable</a></h2>
    </asp:Panel>

    <asp:Panel ID="TradingOffPanel" runat="server" Visible="False">
        <asp:Image ID="Image3" runat="server"  ImageUrl="Styles/pause_big.png" style="display: block; margin-left: auto; margin-right: auto;" />
        <h2  style="text-align: center">Orders Transmission is <span style="color: red">DISABLED</span>.</h2>
    </asp:Panel>
    
    <asp:Panel ID="TradingOffPanelAdmin" runat="server" Visible="False">
        <asp:ImageButton ID="TradingOffImageButton" runat="server" ImageUrl="Styles/pause_big.png" style="display: block; margin-left: auto; margin-right: auto;" 
            PostBackUrl="RiskMgmtOverview.aspx?Action=turnon" OnClientClick="if(!confirm('Are you sure you want to enable trading?')) return false; DisableAutoRefresh();"/>
        <h2  style="text-align: center" onclick="if(!confirm('Are you sure you want to enable trading?')) return false; DisableAutoRefresh();">Orders Transmission is <span style="color: red">DISABLED</span>. <a href="~/RiskMgmtOverview.aspx?Action=turnon" ID="LogoutLink" runat="server">Click here to enable</a></h2>
    </asp:Panel>
            
    <asp:Panel ID="StateChangedErrorPanel" runat="server" Visible="False">
        <h2  style="text-align: center; color: red">Your request to change the state was denied, because the state was changed by someone else in the meantime.<br/> You can now see the last known state. Perform full refresh to re-enable auto-refresh.</h2>
    </asp:Panel>
    
    <br />
    <br />
    <h2  style="text-align: center">KGT net open position in USD (abs): <span style="border-style:solid; padding: 5px;"><asp:Literal ID="LtrlNopTotal" runat="server" Text="?"></asp:Literal></span></h2>
    
    <br /> 
    <br /> 
    

    <table  cellpadding="5" class="auto-style1">
        <tr>
            <td><h2  style="text-align: center">Cross Positions</h2></td>
            <td><h2  style="text-align: center">Positions by currency</h2></td>
            <td><h2  style="text-align: center">Positions by counterparty</h2></td>
        </tr>
        <tr>
            <td style="vertical-align:top">
                <asp:GridView ID="CrossPointsGridView" runat="server"
                        GridLines="None"  
                        autogeneratecolumns="False"
                    CssClass="mGrid"   RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow"
                    ShowHeaderWhenEmpty="True">
                    <Columns>  
                        <asp:BoundField DataField="Symbol" HeaderText="CCY Pair" />  
                        <asp:TemplateField>
                            <HeaderTemplate>Pos in CCY1</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Panel ID="Panelccy1" HorizontalAlign="Right" runat="server" BackColor='<%# Utils.GetColorForNumber(((DbDataRecord)Container.DataItem)["NopBasePol"]) %>'>
                                    <asp:Label ID="lblCCY1" runat="server" Text='<%# FormatNumber(((DbDataRecord)Container.DataItem)["NopBasePol"]) %>' ></asp:Label>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="NopTermPol" HeaderText="Pos in CCY2" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"  />
                    </Columns>
                </asp:GridView>
            </td>
            <td style="vertical-align:top">
                <asp:GridView ID="CCYPositionsNops" runat="server"
                        GridLines="None"  
                        autogeneratecolumns="False"
                    CssClass="mGrid"   
                    RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow"
                    ShowFooter="True" ShowHeaderWhenEmpty="True">
                    <Columns>  
                        <asp:BoundField DataField="CCY" HeaderText="CCY" />  
                        <asp:BoundField DataField="NopPol" HeaderText="Position in CCY" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" />  
                        <asp:BoundField DataField="NopPolInUsd" HeaderText="Position in USD" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" />  
                    </Columns>
                    <FooterStyle Font-Bold="True"></FooterStyle>
                </asp:GridView>
            </td>
            <td style="vertical-align:top">
                <asp:GridView ID="CounterpartyNops" runat="server"
                        GridLines="None"  
                        autogeneratecolumns="False"
                    CssClass="mGrid"   
                    RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow" ShowHeaderWhenEmpty="True">
                    <Columns>  
                        <asp:BoundField DataField="Counterparty" HeaderText="Counterparty" />  
                        <asp:BoundField DataField="NopAbs" HeaderText="Abs. position in USD" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" />  
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    
        </ContentTemplate>
    </uc1:AutorefreshContainer>
    
    
</asp:Content>