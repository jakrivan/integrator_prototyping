﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class Systems : PageWithNoControlState
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated || !Utils.IsKgtUser(Session))
            {
                if (!PerformQueryAction())
                {
                    FillGlobalStatisticsRepeater();
                    FillGroupsStatisticsRepeater();
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        private void RedirectToSelf(string query)
        {
            query = string.IsNullOrWhiteSpace(query) ? string.Empty : "?" + query;
            if (Request.UrlReferrer != null)
            {
                Response.Redirect(new Uri(Request.UrlReferrer.AbsoluteUri).GetLeftPart(UriPartial.Path) + query, false);
            }
            else
            {
                Response.Redirect("~/Systems.aspx" + query, false);
            }
        }

        protected int? _systemTypeIdToAdd;
        protected int? _systemGroupIdToAdd;
        protected int? _pageIdToModify;
        protected int? _groupIdToModify;
        protected string _groupName;
        protected string _validationErrorMsg;

        protected string _beginSymbol;
        protected string _endSymbol;
        protected int? _stripsNum;
        protected bool _addGroup;
        protected int? _groupIdToAddSystemType;
        private List<int> _availableTypeIdsForEdditedGroup;

        private bool PerformQueryAction()
        {
            if (Request.QueryString.HasKeys())
            {
                if (Request.QueryString["DeleteGroup"] != null)
                {
                    int groupId;
                    if (int.TryParse(Request.QueryString["DeleteGroup"], out groupId))
                    {
                        if (CanDeleteGroup(groupId) && DeleteGroup(groupId))
                        {
                            RefreshBackendStats();
                            GlobalState.RefreshGroupNames();
                            RedirectToSelf(null);
                            return true;
                        }
                        else
                        {
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }
                    }
                }
                else if (Request.QueryString["AddGroup"] != null)
                {
                    _validationErrorMsg = Request.QueryString["ValidationErrorMsg"];
                    _addGroup = true;
                    this.AC.AllowAutoRefresh = false;
                    return false;
                }
                else if (Request.QueryString["RenameGroup"] != null)
                {
                    _validationErrorMsg = Request.QueryString["ValidationErrorMsg"];
                    _groupName = Request.QueryString["GroupName"];
                    if (Utils.TryParseIntIntToNullable(Request.QueryString["RenameGroup"], out _groupIdToModify))
                    {
                        this.AC.AllowAutoRefresh = false;
                        return false;
                    }
                }
                else if (Request.QueryString["GroupRenameDone"] != null)
                {
                    string newName = Request.QueryString["GroupRenameDone"];
                    int groupId;

                    if (int.TryParse(Request.QueryString["SystemGroupId"], out groupId))
                    {
                        string errorMessage = null;

                        if (string.IsNullOrWhiteSpace(newName))
                        {
                            errorMessage = "Group name cannot be empty!";
                        }

                        if (GlobalState.AllGroupNames.Contains(newName, StringComparer.InvariantCultureIgnoreCase))
                        {
                            errorMessage = "Entered group name is already in use!";
                        }

                        if (errorMessage != null)
                        {
                            this.RedirectToSelf(
                                string.Format(
                                    "RenameGroup={0}&GroupName={1}&ValidationErrorMsg={2}",
                                    groupId, Server.HtmlEncode(newName), errorMessage));
                            return true;
                        }

                        if (RenameGroup(groupId, newName))
                        {
                            RefreshBackendStats();
                            GlobalState.RefreshGroupNames();
                            RedirectToSelf(null);
                            return true;
                        }
                        else
                        {
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }

                    }
                }
                else if (Request.QueryString["GroupAddDone"] != null)
                {
                    string newName = Request.QueryString["GroupAddDone"];

                    string errorMessage = null;

                    if (string.IsNullOrWhiteSpace(newName))
                    {
                        errorMessage = "Group name cannot be empty!";
                    }

                    if (GlobalState.AllGroupNames.Contains(newName, StringComparer.InvariantCultureIgnoreCase))
                    {
                        errorMessage = "Entered group name is already in use!";
                    }

                    if (errorMessage != null)
                    {
                        this.RedirectToSelf(
                            string.Format(
                                "AddGroup={0}&ValidationErrorMsg={1}",
                                Server.HtmlEncode(newName), errorMessage));
                        return true;
                    }

                    if (AddGroup(newName))
                    {
                        RefreshBackendStats();
                        GlobalState.RefreshGroupNames();
                        RedirectToSelf(null);
                        return true;
                    }
                    else
                    {
                        this.AC.AllowAutoRefresh = false;
                        return false;
                    }
                }

                this.AC.ReportError("Unknown or malformed query action. Please remove the query from URL or retry.");
                this.AC.AllowAutoRefresh = false;
            }

            return false;
        }

        private bool CanDeleteGroup(int groupId)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[TradingSystemGroupHasActiveSystems_SP]", connection);
                        command.Parameters.AddWithValue("@GroupId", groupId);
                        command.CommandType = CommandType.StoredProcedure;
                        var val = command.ExecuteScalar();
                        int activeSystemsCount = (int)command.ExecuteScalar();
                        if (activeSystemsCount > 0)
                        {
                            this.AC.ReportError(string.Format("Cannot delete group [{0}] as it still has {1} active systems - disable those first.",
                                GlobalState.GetGroupName(groupId), activeSystemsCount));
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool DeleteGroup(int groupId)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[TradingSystemGroupDelete_SP]", connection);
                        command.Parameters.AddWithValue("@TradingSystemGroupId", groupId);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool RenameGroup(int groupId, string newName)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[TradingSystemGroupRename_SP]", connection);
                        command.Parameters.AddWithValue("@TradingSystemGroupId", groupId);
                        command.Parameters.AddWithValue("@NewName", newName);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool AddGroup(string newName)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[TradingSystemGroupAdd_SP]", connection);
                        command.Parameters.AddWithValue("@NewName", newName);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool RefreshBackendStats()
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private void FillGlobalStatisticsRepeater()
        {
            try
            {
                DataTable dt = this.GetGlobalStatisticsDataTable();
                StatsSummary statsSummary = this.RecalculateTable(dt);
                GlobalStatsRepeater.DataSource = dt;
                GlobalStatsRepeater.DataBind();

                this.FillGlobalstatsRepeaterFooter(GlobalStatsRepeater, statsSummary);
            }
            catch (Exception exc)
            {
                this.AC.ReportException(exc);
            }
        }

        private void FillGroupsStatisticsRepeater()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("TradingSystemGroup", typeof(string));
                dt.Columns.Add("TradingSystemGroupId", typeof(int));

                foreach (int groupId in GlobalState.AllGroupIds)
                {
                    dt.Rows.Add(GlobalState.GetGroupName(groupId), groupId);
                }

                GroupsRepeater.DataSource = dt;
                GroupsRepeater.DataBind();
            }
            catch (Exception exc)
            {
                this.AC.ReportException(exc);
            }
        }

        private StatsSummary RecalculateTable(DataTable dt)
        {
            StatsSummary statsSummary = new StatsSummary();
            decimal overalVolumeM = GlobalState.TotalDailyVolume / 1000000;

            dt.Columns.Add("VolumePct", typeof(decimal));
            foreach (DataRow dataRow in dt.Rows)
            {
                if (dataRow["VolumeUsdM"] != DBNull.Value && overalVolumeM != 0m)
                {
                    dataRow["VolumePct"] = (decimal)dataRow["VolumeUsdM"] / overalVolumeM * 100m;
                }
                statsSummary.AddDatRowToStats(dataRow);
            }

            statsSummary.RecalculateStats();
            return statsSummary;
        }

        private DataTable GetGlobalStatisticsDataTable()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                //bool isTradingEnabled = false;
                using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("[dbo].[GetTradingSystemsOveralDailyStats_SP]", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        DataTable dt = new DataTable();
                        dt.Load(reader);
                        return dt;
                    }
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }

            return null;
        }

        private void FillGlobalstatsRepeaterFooter(Repeater statsRepeater, StatsSummary statsSummary)
        {
            (statsRepeater.FindControlInFooter("FooterCell_ConfiguredSystems") as HtmlTableCell).InnerText =
                    statsSummary.ConfiguredSystems.ToString();
            (statsRepeater.FindControlInFooter("FooterCell_ActiveSystems") as HtmlTableCell).InnerText =
                    statsSummary.ActiveSystems.ToString();
            (statsRepeater.FindControlInFooter("FooterCell_HardBlockedSystems") as HtmlTableCell).InnerText =
                    statsSummary.BlockedSystems.ToString();
            (statsRepeater.FindControlInFooter("FooterCell_Volume") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.SystemsVolumeM, 1, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_VolumePct") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.VolumePct, 1, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_Deals") as HtmlTableCell).InnerText =
                    statsSummary.Deals.ToString();
            (statsRepeater.FindControlInFooter("FooterCell_CtpRejections") as HtmlTableCell).InnerText =
                    statsSummary.CtpRejections.ToString();
            (statsRepeater.FindControlInFooter("FooterCell_KGTRejections") as HtmlTableCell).InnerText =
                    statsSummary.KGTRejections.ToString();
            (statsRepeater.FindControlInFooter("FooterCell_PnlGrossPerM") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.PnlGrossPerM, 2, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_CommPbPerM") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.CommPbPerM, 2, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_CommCptPerM") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.CommCptPerM, 2, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_CommPerM") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.CommPerM, 2, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_PnlNetPerM") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.PnLNetPerM, 2, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_PnlGross") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.PnlGross, 0, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_CommPb") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.CommPb, 0, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_CommCpt") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.CommCpt, 0, Utils.NullsHandling.DbNullToEmpty);
            (statsRepeater.FindControlInFooter("FooterCell_Comm") as HtmlTableCell).InnerText =
                Utils.FormatDecimalNumber(statsSummary.Comm, 0, Utils.NullsHandling.DbNullToEmpty);

            var pnlCell = (statsRepeater.FindControlInFooter("FooterCell_PnlNet") as HtmlTableCell);
            pnlCell.InnerText = Utils.FormatDecimalNumber(statsSummary.PnlNet, 0, Utils.NullsHandling.DbNullToEmpty);
            pnlCell.Attributes.Add("class", Utils.GetNumberCellStyle(statsSummary.PnlNet));
        }

        private class StatsSummary
        {
            public void AddDatRowToStats(DataRow dr)
            {
                if (dr["ConfiguredSystems"] != DBNull.Value)
                    this.ConfiguredSystems += (int)dr["ConfiguredSystems"];

                if (dr["EnabledSystems"] != DBNull.Value)
                    this.ActiveSystems += (int)dr["EnabledSystems"];

                if (dr["HardBlockedSystems"] != DBNull.Value)
                    this.BlockedSystems += (int)dr["HardBlockedSystems"];

                if (dr["VolumeUsdM"] != DBNull.Value)
                    this.SystemsVolumeM += (decimal)dr["VolumeUsdM"];

                if (dr["VolumePct"] != DBNull.Value)
                    this.VolumePct += (decimal)dr["VolumePct"];

                if (dr["DealsNum"] != DBNull.Value)
                    this.Deals += (int)dr["DealsNum"];

                if (dr["CtpRejectionsNum"] != DBNull.Value)
                    this.CtpRejections += (int)dr["CtpRejectionsNum"];

                if (dr["KGTRejectionsNum"] != DBNull.Value)
                    this.KGTRejections += (int)dr["KGTRejectionsNum"];

                if (dr["PnlGrossUsd"] != DBNull.Value)
                    this.PnlGross += (decimal)dr["PnlGrossUsd"];

                if (dr["CommissionsPbUsd"] != DBNull.Value)
                    this.CommPb += (decimal)dr["CommissionsPbUsd"];

                if (dr["CommissionsCptUsd"] != DBNull.Value)
                    this.CommCpt += (decimal)dr["CommissionsCptUsd"];

                if (dr["CommissionsUsd"] != DBNull.Value)
                    this.Comm += (decimal)dr["CommissionsUsd"];

                if (dr["PnlNetUsd"] != DBNull.Value)
                    this.PnlNet += (decimal)dr["PnlNetUsd"];
            }

            public void RecalculateStats()
            {
                if (this.SystemsVolumeM != 0m)
                {
                    this.PnlGrossPerM = this.PnlGross / this.SystemsVolumeM;
                    this.CommPbPerM = this.CommPb / this.SystemsVolumeM;
                    this.CommCptPerM = this.CommCpt / this.SystemsVolumeM;
                    this.CommPerM = this.Comm / this.SystemsVolumeM;
                    this.PnLNetPerM = this.PnlNet / this.SystemsVolumeM;
                }
            }

            public int ConfiguredSystems { get; set; }
            public int ActiveSystems { get; set; }
            public int BlockedSystems { get; set; }
            public decimal SystemsVolumeM { get; set; }
            public decimal VolumePct { get; set; }
            public int Deals { get; set; }
            public int CtpRejections { get; set; }
            public int KGTRejections { get; set; }
            public decimal PnlGrossPerM { get; set; }
            public decimal CommPbPerM { get; set; }
            public decimal CommCptPerM { get; set; }
            public decimal CommPerM { get; set; }
            public decimal PnLNetPerM { get; set; }
            public decimal PnlGross { get; set; }
            public decimal CommPb { get; set; }
            public decimal CommCpt { get; set; }
            public decimal Comm { get; set; }
            public decimal PnlNet { get; set; }
        }
    }
}