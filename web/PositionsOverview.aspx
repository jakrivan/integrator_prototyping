﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PositionsOverview.aspx.cs" Inherits="KGTKillSwitchWeb.PositionsOverview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Net Open Positions Overview</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <p>Select the desired date in order to display or refresh NOP list for that day (in UTC time zone). Data will not autorefresh by itself!</p>

    <asp:Calendar ID="Calendar1" runat="server">
        <SelectedDayStyle BackColor="#0066FF" />
    </asp:Calendar>
    
    <asp:LoginView ID="NopLoginView" runat="server" EnableViewState="false">
        <AnonymousTemplate>
            Not Authorised!
        </AnonymousTemplate>
        <LoggedInTemplate>
    
    <h2>Positions for all pairs:</h2>
                    <br/>
            
                    <asp:Panel ID="ErrorPanel" runat="server" Visible="False">
                        <h2  style="text-align: center; color: red">There was an error during obtainig positions.</h2>
                    </asp:Panel>

                    <asp:GridView ID="NopGridView" runat="server"
                        GridLines="None"  
                        autogeneratecolumns="False"
                    CssClass="mGrid"   
                    RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow">
                    <Columns>  
                        <asp:BoundField DataField="Symbol" HeaderText="Symbol" />  
                        <asp:BoundField DataField="PolarizedNop" HeaderText="Polarized Nop" />  

                    </Columns>

                    </asp:GridView>
            
            <br/>
    

    <a href="/Positions.aspx">Positions Overview by Settlemet Dates</a>

            </LoggedInTemplate>
    </asp:LoginView>

</asp:Content>
