﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            string address = Request.UserHostAddress;

            address = string.IsNullOrEmpty(address) ? "UNKNOWN" : address;
            if (Session["HostName"] != null)
            {
                address += " (" + Session["HostName"] + ")";
            }

            LoginOperationResult loginOperationResult = Utils.TryLoginUser(this.LoginUser.UserName,
                                                                           this.LoginUser.Password, address);

            if (loginOperationResult.Succeeded)
            {
                FormsAuthentication.SetAuthCookie(this.LoginUser.UserName, this.LoginUser.RememberMeSet);
                Session["LoginOperationResult"] = loginOperationResult;
                if (Utils.IsKgtUser(Session))
                {
                    Response.Redirect("~/RiskMgmtOverviewInternal.aspx"); 
                }
                else
                {
                    Response.Redirect("~/RiskMgmtOverview.aspx");  
                }           
            }
            else
            {
                this.LoginFailedLabel.Text = loginOperationResult.ErrorMessage;
                this.LoginFailedLabel.Visible = true;
            }
        }

    }
}
