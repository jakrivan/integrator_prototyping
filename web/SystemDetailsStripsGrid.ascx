﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SystemDetailsStripsGrid.ascx.cs" Inherits="KGTKillSwitchWeb.SystemDetailsStripsGrid" %>
<%@ Register TagPrefix="uc1" TagName="autorefreshcontainer" Src="~/AutorefreshContainer.ascx" %>
<%@ Register Src="~/SystemDetailsStrip.ascx" TagPrefix="uc2" TagName="SystemDetailsStrip" %>


<%--<uc1:autorefreshcontainer runat="server" id="AC"  AutorefreshMillisecondsInterval="1000" AllowAutoRefresh="True" OnRefreshUrlAdjustRequested="AdjustUrlQuery">
<ContentTemplate> --%>
    
    
    <h1 style="font-size:2.6em; margin-top: 5px">
                    <asp:Literal runat="server" ID="GroupNameLiteral"></asp:Literal>:&nbsp;<asp:Literal runat="server" ID="VenueTypeLiteral"></asp:Literal>-<asp:Literal runat="server" ID="TradingTypeLiteral"></asp:Literal>
                </h1>

    <table class="EnclosingTable">
       <tr>
           <td>
               <uc2:SystemDetailsStrip ID="PnlGrid01" runat="server" />
           </td>
           <td>
               <uc2:SystemDetailsStrip ID="PnlGrid02"  runat="server" />
           </td>
           <td>
               <uc2:SystemDetailsStrip ID="PnlGrid03"  runat="server" />
           </td>
           <td>
               <uc2:SystemDetailsStrip ID="PnlGrid04"  runat="server" />
           </td>
           <td>
               <uc2:SystemDetailsStrip ID="PnlGrid05"  runat="server" />
           </td>
       </tr>
       <tr>
           <td colspan="5">
               <hr style="background-color: #c1c1c1; height: 1px"/>  
           </td>
       </tr>
       <tr>
           <td colspan="5">
                <a ID="AllUnconfiguredLink" runat="server" class="SymbolBox">Add all unconfigured symbols</a>
               <asp:Repeater ID="SymbolsRepeater" runat="server">
                   <ItemTemplate>
                       <a class="SymbolBox" href='<%# string.Format("InsertEditSystem.aspx{0}&Action=Insert&SymbolsNum=1&Symbols={1}", this.GetQueryStringWithNoAction(), Container.DataItem) %>'><%# Container.DataItem %></a>
                   </ItemTemplate>
               </asp:Repeater>
           </td>
       </tr>
   </table>
    

    
<%--</ContentTemplate>
</uc1:autorefreshcontainer> --%>