﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="SystemDetails.aspx.cs" Inherits="KGTKillSwitchWeb.SystemDetails" %>
<%@ Import Namespace="KGTKillSwitchWeb" %>
<%--<%@ Register Src="~/VenueSettingsWithPnl.ascx" TagPrefix="uc1" TagName="VenueSettingsWithPnl" %>--%>
<%@ Register Src="~/SystemDetailsStripsGrid.ascx" TagPrefix="uc1" TagName="VenueSettingsWithPnl" %>
<%@ Register TagPrefix="uc1" TagName="autorefreshcontainer" Src="~/AutorefreshContainer.ascx" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><%= Utils.GetUatPrefix(Session) + Utils.GetGroupName(Request) + " : " + Request.QueryString["TradingSystemType"] + " " + Request.QueryString["VenueCounterparty"]%></title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server"  autocomplete="off">
    <div>
        
        <uc1:autorefreshcontainer runat="server" id="AC"  AutorefreshMillisecondsInterval="1000" AllowAutoRefresh="True" OnRefreshUrlAdjustRequested="AdjustUrlQuery">
        <ContentTemplate> 

        <uc1:VenueSettingsWithPnl runat="server" ID="VenueSettingsWithPnl"/>
            
        </ContentTemplate>
        </uc1:autorefreshcontainer>
    </div>
    </form>
</body>
</html>
