using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class KGTRiskMgtmtOverview: PageWithNoControlState //: System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            //scriptManager.RegisterPostBackControl(TradingOnImageButton);
            //scriptManager.RegisterPostBackControl(TradingOffImageButton);

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                KgtUserWarningPanel.Visible = Utils.IsKgtUser(Session);

                DateTime today = DateTime.UtcNow.Date; //DateTime.UtcNow.AddDays(0).Date;
                decimal? totalNop;

                UpdateRiskMgmtState();
                totalNop = GetTotalNop(today);
                UpdateTotalNop(totalNop);
                UpdateCrossPoints(today);
                UpdateCcyNops(today, totalNop);
                UpdateCounterpartyNops(today);
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        protected string FormatNumber(object dbObject)
        {
            if (dbObject == DBNull.Value)
                return 0m.ToString("n2");
            else if (dbObject is decimal)
                return ((decimal)dbObject).ToString("n2");
            else
                return "?";
        }

        private decimal? GetTotalNop(DateTime day)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetNopAbsTotal_Daily_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Day", day.Date);

                        object nopObject = command.ExecuteScalar();

                        if (nopObject == DBNull.Value)
                            return 0m;
                        else if (nopObject is decimal)
                            return ((decimal) nopObject);
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }

            return null;
        }

        private const decimal MILLION = 1000000m;

        private void UpdateTotalNop(decimal? totalNop)
        {
            if (totalNop.HasValue)
            {
                LtrlNopTotal.Text = string.Format("{0:N2} M", (totalNop.Value / MILLION));
            }
            else
            {
                LtrlNopTotal.Text = "?";
            }
        }

        private void UpdateCounterpartyNops(DateTime day)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetStatisticsPerCounterparty_DailyCached_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        //command.Parameters.AddWithValue("@Day", day.Date);


                        SqlDataReader reader = command.ExecuteReader();

                        CounterpartyNops.DataSource = reader;
                        CounterpartyNops.DataBind();
                        reader.Close();
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }
        }

        private void UpdateCcyNops(DateTime day, decimal? totalNop)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetNopsPolSidedPerCurrency_Daily_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Day", day.Date); ;


                        SqlDataReader reader = command.ExecuteReader();

                        CCYPositionsNops.DataSource = reader;
                        CCYPositionsNops.DataBind();
                        reader.Close();

                        if (CCYPositionsNops.FooterRow != null)
                        {
                            CCYPositionsNops.FooterRow.Cells[0].Text = "\u03A3 (abs)";

                            CCYPositionsNops.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                            if (totalNop.HasValue)
                            {
                                CCYPositionsNops.FooterRow.Cells[2].Text = totalNop.Value.ToString("n2");
                            }
                            else
                            {
                                CCYPositionsNops.FooterRow.Cells[2].Text = "?";
                            }
                        }
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }
        }

        private void UpdateCrossPoints(DateTime day)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetStatisticsPerPair_Daily_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Day", day.Date);


                        SqlDataReader reader = command.ExecuteReader();

                        CrossPointsGridView.DataSource = reader;
                        CrossPointsGridView.DataBind();
                        reader.Close();
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            //LblUpdatedOn.Text = "Last Updated: " + DateTime.Now.ToLongTimeString();
        }

        private bool? GetIsSwitchEnabled(string switchName)
        {
             ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command =
                            new SqlCommand("SELECT [dbo].[GetExternalOrdersTransmissionSwitchState](@SwitchName)",
                                           connection);
                        command.Parameters.Add("@SwitchName", SqlDbType.NVarChar).Value = switchName;
                        object isTradingOffResult = command.ExecuteScalar();
                        if (isTradingOffResult != null)
                        {
                            return (bool) isTradingOffResult;
                        }
                    }
                }
                catch (Exception exc)
                { }
            }

            return null;
        }

        private void UpdateRiskMgmtState()
        {
            bool isTradingEnabled;

            bool? isTradingSwitchEnabled = this.GetIsSwitchEnabled(Utils.GetKillSwitchBitName(Session));

            if (!(isTradingSwitchEnabled.HasValue))
            {
                this.ErrorPanel.Visible = true;
                return;
            }

            isTradingEnabled = isTradingSwitchEnabled.Value;

            this.TradingOnPanel.Visible = isTradingSwitchEnabled.Value;

            this.TradingOffPanel.Visible =
                !isTradingSwitchEnabled.Value && (!Utils.GetIsAdmin(Session));

            this.TradingOffPanelAdmin.Visible =
                !isTradingSwitchEnabled.Value && (Utils.GetIsAdmin(Session));

            if (Request.QueryString["Action"] != null)
            {
                //allow it after redirect
                this.AC.AllowAutoRefresh = false;
                if (Request.QueryString["Action"].Equals("turnoff", StringComparison.OrdinalIgnoreCase))
                {
                    if (!isTradingEnabled)
                    {
                        this.StateChangedErrorPanel.Visible = true;
                    }
                    else
                    {
                        if (!DoSwitchAction(false, Utils.GetKillSwitchBitName(Session)))
                        {
                            this.ErrorPanel.Visible = true;
                            return;
                        }
                        isTradingEnabled = false;

                        if (Request.UrlReferrer != null)
                        {
                            Response.Redirect(new Uri(Request.UrlReferrer.AbsoluteUri).GetLeftPart(UriPartial.Path), false);
                        }
                        else
                        {
                            Response.Redirect("~/RiskMgmtOverview.aspx", false);
                        }
                    }
                }
                else if (
                    Request.QueryString["Action"].Equals("turnon", StringComparison.OrdinalIgnoreCase) &&
                    Utils.GetIsAdmin(Session))
                {
                    if (isTradingEnabled)
                    {
                        this.StateChangedErrorPanel.Visible = true;
                    }
                    else
                    {
                        if (!DoSwitchAction(true, Utils.GetKillSwitchBitName(Session)))
                        {
                            this.ErrorPanel.Visible = true;
                            return;
                        }
                        isTradingEnabled = true;
                        if (Request.UrlReferrer != null)
                        {
                            Response.Redirect(new Uri(Request.UrlReferrer.AbsoluteUri).GetLeftPart(UriPartial.Path), false);
                        }
                        else
                        {
                            Response.Redirect("~/RiskMgmtOverview.aspx", false);
                        }
                    }
                }
            }
        }

        private bool DoSwitchAction(bool enable, string switchName)
        {
            string spName = enable
                                ? "[dbo].[EnableExternalOrdersTransmission_SP]"
                                : "[dbo].[DisableExternalOrdersTransmission_SP]";

            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        string address = Request.UserHostAddress;
                        address = string.IsNullOrEmpty(address) ? "UNKNOWN" : address;
                        if (Session["HostName"] != null)
                        {
                            address += " (" + Session["HostName"] + ")";
                        }

                        SqlCommand command =
                                            new SqlCommand(spName, connection);
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@SwitchName", SqlDbType.NVarChar).Value = switchName;
                        command.Parameters.AddWithValue("@ChangedBy", HttpContext.Current.User.Identity.Name);
                        command.Parameters.AddWithValue("@ChangedFrom", address);

                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                catch (SqlException exc)
                {
                    this.AC.ReportError(exc.Message.Replace(Environment.NewLine, "<BR />"));
                }
                catch (Exception exc)
                { }
            }

            return false;
        }

        protected void IntegratorTradingOffImageButton_Click(object sender, ImageClickEventArgs e)
        {
            if (!DoSwitchAction(true, Utils.IntegratorSwitchName))
            {
                ErrorPanel.Visible = true;
            }
        }
    }
}