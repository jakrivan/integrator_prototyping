﻿<%@ Page Title="My Information" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Log.aspx.cs" Inherits="KGTKillSwitchWeb.MyInfo" %>
<%@ Import Namespace="System.Activities.Expressions" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
    <title>Information visible to the current user</title>
    
    <script type="text/javascript">

        function AutoRefresh() {
            setTimeout("location.reload(true);", 1000 * 30);
        }


        if (window.addEventListener) window.addEventListener("load", AutoRefresh, true);
        else window.onload = AutoRefresh;


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <asp:LoginView ID="MyInfoLoginView" runat="server" EnableViewState="false">
        <AnonymousTemplate>
            Not Authorised!
        </AnonymousTemplate>
        <LoggedInTemplate>
            <h2>Last 10 login actions visible to your account:</h2>
            <br/>

            <asp:GridView ID="GridViewLoginActions" runat="server"
                GridLines="None"  
                autogeneratecolumns="False"
            CssClass="mGrid"   
            RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow">
            <Columns>  
                <asp:BoundField DataField="UserName" HeaderText="User Name" />  
                <asp:BoundField DataField="ActionTime" HeaderText="Login Attempt Time (UTC)" />  
                <asp:BoundField DataField="Source" HeaderText="Connected From" />  
                <asp:TemplateField>
                    <HeaderTemplate>Success</HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="Image1" ImageUrl="~/Styles/CheckSmall_1.png" Visible='<%# (bool)((DbDataRecord) Container.DataItem)["Success"] %>' runat="server" />

                                    <asp:Image ID="Image2" ImageUrl="~/Styles/CrossSmall_1.png" Visible='<%# ((DbDataRecord)Container.DataItem)["Success"].Equals(false) %>' runat="server" />


                                </ItemTemplate>
                            </asp:TemplateField>

            </Columns>
            </asp:GridView>
    
            
            <br />
            <br />

            <h2>Last 10 state changes visible to your account:</h2>
                    <br/>

                    <asp:GridView ID="SwitchChangesGridView" runat="server"
                        GridLines="None"  
                        autogeneratecolumns="False"
                    CssClass="mGrid"   
                    RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow">
                    <Columns>  
                        <asp:BoundField DataField="ChangedBy" HeaderText="User Name" />  
                        <asp:BoundField DataField="Changed" HeaderText="Action Time (UTC)" />  
                        <asp:BoundField DataField="ChangedFrom" HeaderText="Connected From" />  
                        <asp:TemplateField>
                            <HeaderTemplate>Enabled/Dsabled</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="Image1" ImageUrl="~/Styles/pause_small.png" Visible='<%# ((DbDataRecord)Container.DataItem)["TransmissionEnabled"].Equals(false) %>' runat="server" />
                                            <asp:Label ID="Label1" Visible='<%# ((DbDataRecord)Container.DataItem)["TransmissionEnabled"].Equals(false) %>'  runat="server" Text="&nbsp;&nbsp;&nbsp;(DISABLED)" ></asp:Label>

                                            <asp:Image ID="Image2" ImageUrl="~/Styles/play_small.png" Visible='<%# (bool)((DbDataRecord)Container.DataItem)["TransmissionEnabled"] %>' runat="server" />
                                            <asp:Label ID="Label2" Visible='<%# (bool)((DbDataRecord)Container.DataItem)["TransmissionEnabled"] %>'  runat="server" Text="&nbsp;&nbsp;&nbsp;(ENABLED)" ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                    </Columns>

                    </asp:GridView>

            <br/>
            
            <asp:Panel ID="ErrorPanel" runat="server" Visible="False">
                <h2  style="text-align: center; color: red">There was an error during obtainig activity log.</h2>
            </asp:Panel>
        </LoggedInTemplate>
    </asp:LoginView>
    
    
    
    

</asp:Content>
