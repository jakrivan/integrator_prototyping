﻿
//handle multiple include of this file - only one handler needs to be registered!
var called;
var lastCalled = new Date();

Sys.Application.add_init(AppInit);

function AppInit(sender) {

    if (!called) {
        called = true;
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        //RemovePreviousPageTag();
        SetIfAutorefreshRequested();
        SetWatchdogIfAutorefreshRequested();
    }
}

function RemovePreviousPageTag() {
    var el = document.getElementById("__PREVIOUSPAGE");
    if (el != undefined && el != null) {
        el.parentNode.removeChild(el);
    }
}

function SetIfAutorefreshRequested() {
    RemovePreviousPageTag();
    
    var intervalField = $get("refreshIntervalField");
    if (intervalField != undefined) {
        var milliseconds = parseInt(intervalField.value);
        setTimeout(RequestAsyncUpdate, milliseconds);
    }
}

function SetWatchdogIfAutorefreshRequested() {
    var intervalField = $get("refreshIntervalField");
    if (intervalField != undefined) {
        setInterval(WatchdogFunc, 15*1000);
    }
}

var errorDisplayed = false;

function WatchdogFunc() {
    var isInErrorState = new Date() - lastCalled > 15 * 1000;

    if (isInErrorState && !errorDisplayed) {
        $get("AutoRefreshErrorPanel").style.display = "block";
        $get("TimeText").innerText = StampToString(new Date());
        errorDisplayed = true;
    }
    else if (!isInErrorState && errorDisplayed && $get("refreshIntervalField") != undefined) {
        $get("AutoRefreshErrorPanel").style.display = "none";
        errorDisplayed = false;
    }
}


/*DO NOT IMPLEMENT here
  this needs to be implemented in individual aspx file - so that client id of update panel can be injected in*/
/*function RequestAsyncUpdate() {}*/

function ToDoubleDigitString(i) {
    if (i < 10)
        return "0" + i;
    else
        return i;
}

var progressChars = ['|', '/', '-', '\\'];

function StampToString(timeStamp) {
    var timeStampString = ToDoubleDigitString(timeStamp.getUTCHours()) + ":" + ToDoubleDigitString(timeStamp.getUTCMinutes()) + ":" + ToDoubleDigitString(timeStamp.getUTCSeconds()) + " UTC";
    return timeStampString;
}

function EndRequestHandler(sender, args) {
    
    var timeStamp = new Date();
    var timeStampString = StampToString(timeStamp);

    if (args.get_error() != undefined && allowRequest) {
        var errorMessage = "ERROR(" + timeStampString + "): " + args.get_error().message;
        args.set_errorHandled(true);
        $get("updateErrorSpan").innerText = errorMessage;
        $get("updateErrorDiv").style.display = "block";
    }
    else {
        var currentTitle = window.document.title;
        var titleEnd = currentTitle.indexOf("[");
        if (titleEnd > 0) {
            currentTitle = currentTitle.slice(0, titleEnd);
        }

        window.document.title = currentTitle + " [Updated: " + ToDoubleDigitString(timeStamp.getUTCHours()) + ":" + ToDoubleDigitString(timeStamp.getUTCMinutes()) + ":" + ToDoubleDigitString(timeStamp.getUTCSeconds()) + " UTC] "
            + progressChars[timeStamp.getSeconds() % 4] + progressChars[timeStamp.getSeconds() % 4] + progressChars[timeStamp.getSeconds() % 4];

        lastCalled = timeStamp;
    }

    SetIfAutorefreshRequested();
}

function HideErrorDetails() {
    $get("updateErrorDiv").style.display = "none";
}


//
// Helper functions
//

function popitup(url) {
    var params = [
        //'height=' + screen.height,
        //'width=' + screen.width,
        'height=' + 450,
        'width=' + 1440,
        'resizable=1'
        //,'fullscreen=yes' // only works in IE, but here for completeness
    ].join(',');

    newwindow = window.open(url, '_blank', params);
    if (window.focus) {
        newwindow.focus();
    }
    newwindow.moveTo(0, 0);
    return false;
}

function htmlEscape(str) {
    return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}

function hasHtmlChars(str) {
    return String(str).indexOf("&") > -1 ||
        String(str).indexOf("\"") > -1 ||
        String(str).indexOf("\'") > -1 ||
        String(str).indexOf("<") > -1 ||
        String(str).indexOf(">") > -1;
}