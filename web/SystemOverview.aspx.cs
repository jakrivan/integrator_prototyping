﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class SystemOverview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (!Utils.IsKgtUser(Session))
                {
                    Response.Redirect("~/NotAuthorized.aspx");
                    return;
                }

                //Do not update the grid once in edit mode! that would make user experience very bad 
                // (selecting new value but then under your hand page refreshes and selected value is lost)
                if (SessionsGridView.EditIndex == -1)
                {
                    PopulateSessionsOverview();
                    PopulateProcessesOverview();

                    LblUpdatedOn.Text = "All data shown for today (UTC). Last updated: " +
                                        DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss UTC");
                    progressDiv.InnerHtml = string.Format("<progress value=\"{0}\" max=\"9\" />",
                                                          DateTime.UtcNow.Second%10);
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        private void PopulateSessionsOverview()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetSessionsClaimStatus_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;

                        SqlDataReader reader = command.ExecuteReader();

                        SessionsGridView.DataSource = reader;
                        SessionsGridView.DataBind();
                        reader.Close();
                    }
                }
                catch (Exception exc)
                {
                    ErrorPanel.Visible = true;
                }
            }
            else
            {
                ErrorPanel.Visible = true;
            }
        }

        private void PopulateProcessesOverview()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetIntegratorProcessDetailsList_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;

                        SqlDataReader reader = command.ExecuteReader();

                        ProcessesGridView.DataSource = reader;
                        ProcessesGridView.DataBind();
                        reader.Close();
                    }
                }
                catch (Exception exc)
                {
                    ErrorPanel.Visible = true;
                }
            }
            else
            {
                ErrorPanel.Visible = true;
            }
        }

        private void SetErr(string err)
        {
            LblUpdatedOn.Text = "AAAAAAAAAAA";
        }

        //Warning this cannot contain xml/html tags otherwise asp.net requests validation will throw
        // We can avoid this by disabling security validations - but that's rather overkill
        private const string NULL_INSTANCE_PLACEHOLDER = "*NONE*";

        private IEnumerable<string> GetInstancesList()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];
            List<string> instanceNames = new List<string>() { NULL_INSTANCE_PLACEHOLDER };

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetInstanceNames_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;

                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            instanceNames.Add(reader.GetString(reader.GetOrdinal("Name")));
                        }
                    }
                }
                catch (Exception exc)
                {
                    ErrorPanel.Visible = true;
                }
            }
            else
            {
                ErrorPanel.Visible = true;
            }

            return instanceNames;
        }

        private void ForceClaimSession(string counterpartyCode, string instanceName)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[ForceClaimSessionsForInstance_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@InstanceName", string.IsNullOrEmpty(instanceName) ? DBNull.Value : (object) instanceName);
                        command.Parameters.AddWithValue("@Counterparty", counterpartyCode);

                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception exc)
                {
                    ErrorPanel.Visible = true;
                }
            }
            else
            {
                ErrorPanel.Visible = true;
            }
        }

        private void DeleteProcess(int processId)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[DeleteIntegratorProcess_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ProcessId", processId);

                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception exc)
                {
                    ErrorPanel.Visible = true;
                }
            }
            else
            {
                ErrorPanel.Visible = true;
            }
        }

        protected void SessionsGridView_RowEditing(object sender, GridViewEditEventArgs e)
        {
            SessionsGridView.EditIndex = e.NewEditIndex;
            PopulateSessionsOverview();


            DropDownList dropDownInstance = (DropDownList)SessionsGridView.Rows[e.NewEditIndex].Cells[1].FindControl("DropDownInstance");
            dropDownInstance.DataSource = GetInstancesList();
            dropDownInstance.DataBind();
            dropDownInstance.SelectedValue = ((HiddenField)SessionsGridView.Rows[e.NewEditIndex].Cells[1].FindControl("HiddenInstance")).Value;
        }

        protected void SessionsGridView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            SessionsGridView.EditIndex = -1;
            PopulateSessionsOverview();
        }

        protected void SessionsGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            string ctpCode = ((Literal) SessionsGridView.Rows[e.RowIndex].Cells[0].FindControl("LiteralCtp")).Text;
            string instanceName =
                ((DropDownList) SessionsGridView.Rows[e.RowIndex].Cells[1].FindControl("DropDownInstance"))
                    .SelectedValue;

            this.ForceClaimSession(ctpCode, instanceName.Equals(NULL_INSTANCE_PLACEHOLDER) ? null : instanceName);

            SessionsGridView.EditIndex = -1;
            PopulateSessionsOverview();
        }

        protected void ProcessesGridView_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteMe")
            {
                int row;
                if (!int.TryParse(e.CommandArgument as string, out row) || row < 0)
                {
                    this.SelectionErrorPanel.Visible = true;
                    return;
                }

                var procId = (ProcessesGridView.Rows[row].Cells[0].FindControl("MyHiddenIdx") as HiddenField).Value;
                DeleteProcess(int.Parse(procId));
                PopulateProcessesOverview();
            }
        }

        protected void ProcessesGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //bool old = DataBinder.Eval(e.Row.DataItem, "IsOld").Equals(1);
                bool old = ((DbDataRecord)e.Row.DataItem)["IsOld"].Equals(1);
                string state = ((DbDataRecord)e.Row.DataItem)["State"].ToString();

                LinkButton lb1 = (LinkButton)e.Row.Cells[5].Controls[0];

                if (!old)
                {
                    lb1.Visible = false;
                }
                else
                {
                    lb1.Attributes.Add("onclick", "return confirm('Do you really want to delete this record?');");
                }

            }
        }
    }
}