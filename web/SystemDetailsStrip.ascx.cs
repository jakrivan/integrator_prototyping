﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public static class HighlightDurationConsts
    {
        public static readonly TimeSpan UPDATED_SYSTEM_HIGHLIGHT_DURATION = TimeSpan.FromSeconds(5);
        public static readonly TimeSpan TRADING_ACTION_SYSTEM_HIGHLIGHT_DURATION = TimeSpan.FromSeconds(5);
    }

    public partial class SystemDetailsStrip : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        { }

        protected TradingSystemType TradingSystemType { get { return (TradingSystemType)Enum.Parse(typeof(TradingSystemType), Request.QueryString["TradingSystemType"]); } }
        protected VenueCounterparty VenueCounterparty { get { return (VenueCounterparty)Enum.Parse(typeof(VenueCounterparty), Request.QueryString["VenueCounterparty"]); } }

        public void BindToDataTable(DataTable dt, string refreshUrlWithNoAction)
        {
        }

        public string GetQueryStringWithNoAction()
        {
            return
                string.Format(
                    "?TradingSystemType={0}&VenueCounterparty={1}&GroupId={2}&PageId={3}&StartSymbol={4}&EndSymbol={5}&NumOfStrips={6}",
                    this.TradingSystemType, this.VenueCounterparty, Request.QueryString["GroupId"], Request.QueryString["PageId"],
                    Utils.RemoveSlashFromSymbol(this.GetStartSybol()), Utils.RemoveSlashFromSymbol(this.GetEndSybol()),
                    GetNumberOfStrips());
        }

        public int GetNumberOfStrips()
        {
            int numOfStrips;

            if (Request.QueryString["NumOfStrips"] == null ||
                !int.TryParse(Request.QueryString["NumOfStrips"], out numOfStrips) ||
                numOfStrips < 1)
            {
                numOfStrips = 2;
            }

            return numOfStrips;
        }

        public string GetStartSybol()
        {
            string startSymbol;

            if (Request.QueryString["StartSymbol"] == null ||
                Request.QueryString["StartSymbol"].Length != 6)
            {
                startSymbol = "AUDCAD";
            }
            else
            {
                startSymbol = Request.QueryString["StartSymbol"];
            }

            return Utils.AddSlashToSymbol(startSymbol);
        }

        public string GetEndSybol()
        {
            string endSymbol;

            if (Request.QueryString["EndSymbol"] == null ||
                Request.QueryString["EndSymbol"].Length != 6)
            {
                endSymbol = "ZARJPY";
            }
            else
            {
                endSymbol = Request.QueryString["EndSymbol"];
            }

            return Utils.AddSlashToSymbol(endSymbol);
        }
    }
}