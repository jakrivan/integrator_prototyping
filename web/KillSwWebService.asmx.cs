﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace KGTKillSwitchWeb
{
    /// <summary>
    /// Summary description for KillSwWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class KillSwWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public string PrintMessage()
        {
            return "Hello World..This is from service";
        }

        [WebMethod]
        public bool IsLoggedOn()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        [WebMethod(EnableSession = true)]
        //[ScriptMethod(UseHttpGet = true)]
        public string IsTradingEnabled()
        {
            string returnString = "error";

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {

                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    try
                    {
                        bool isTradingEnabled = false;
                        using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                        {
                            connection.Open();

                            SqlCommand command;
                            command = new SqlCommand(
                                "SELECT [dbo].[GetExternalOrdersTransmissionSwitchState](@SwitchName)", connection);
                            command.Parameters.Add("@SwitchName", SqlDbType.NVarChar).Value =
                                Utils.GetKillSwitchBitName(Session);
                            object isTradingOffResult = command.ExecuteScalar();
                            if (isTradingOffResult != null)
                            {
                                isTradingEnabled = (bool) isTradingOffResult;
                                returnString = isTradingEnabled.ToString();
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        returnString = exc.ToString();
                    }
                }
            }

            return returnString;
        }
    }
}
