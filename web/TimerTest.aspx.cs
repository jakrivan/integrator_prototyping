﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class TimerTest : PageWithNoControlState //System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.EditPanel.Visible)
            {
                this.LblTime.Text = System.DateTime.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.ffffff");
                PseudoBind();
                //this.Timer1.Enabled = true;
            }
            else
            {
                //this.Timer1.Enabled = false;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            //this.Timer1.Enabled = !this.EditPanel.Visible;
            RefreshEnabledPlaceholder.Visible = !this.EditPanel.Visible;

            base.OnPreRender(e);
        }

        private void PseudoBind()
        {
            this.txtEdit.Text = this.lblResult.Text;
        }

        protected void OnCommand(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                this.EditPanel.Visible = true;
                //this.Timer1.Enabled = false;

            }
            else if (e.CommandName == "Done")
            {
                this.EditPanel.Visible = false;
                this.lblResult.Text = this.txtEdit.Text;
                //this.Timer1.Enabled = true;
            }
        }


        protected void ErrorProcessClick_Handler(object sender, EventArgs e)
        {
            // This handler demonstrates an error condition. In this example
            // the server error gets intercepted on the client and an alert is shown.
            Exception exc = new ArgumentException();
            exc.Data["GUID"] = Guid.NewGuid().ToString();
            throw exc;
        }
    }
}