﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Mini.aspx.cs" Inherits="KGTKillSwitchWeb.Mini" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <script type="text/javascript" src="Scripts/jquery-1.4.1.min.js"> 
    </script>
    
    <script language="javascript" type="text/javascript">
        var errorNum = 0;
        var logErrors = '<%= HttpContext.Current.User.Identity.Name.Equals("Jan", 
StringComparison.OrdinalIgnoreCase) ||  HttpContext.Current.User.Identity.Name.Equals("Honza", StringComparison.OrdinalIgnoreCase) || HttpContext.Current.User.Identity.Name.Equals("Michal.Kreslik@KGT", StringComparison.OrdinalIgnoreCase)%>';

        function CallIsTradingEnabledFromJquery() {
            $.ajax({
                type: "POST",
                url: "KillSwWebService.asmx/IsTradingEnabled",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                error: OnError
            });

        }

        function OnSuccess(data, status) {

            var isSwitchOnResult;

            if (!(typeof data === 'undefined' || data == null) && !(typeof data.d === 'undefined' || data.d == null)) {
                isSwitchOnResult = data.d.toLowerCase();
            }
            else {
                isSwitchOnResult = '*Nothing Specified By Server*';
            }

            if (isSwitchOnResult == "true") {
                markTimestamp();
                
                document.getElementById('TradingOnDiv').style.display = 'block';
                document.getElementById('TradingOffDiv').style.display = 'none';
                document.getElementById('TradingUnknownDiv').style.display = 'none';
            }
            else if (isSwitchOnResult == "false") {
                markTimestamp();
                
                document.getElementById('TradingOnDiv').style.display = 'none';
                document.getElementById('TradingOffDiv').style.display = 'block';
                document.getElementById('TradingUnknownDiv').style.display = 'none';
            } else {
                if (logErrors.toLowerCase() == "true" && errorNum < 100) {
                    document.getElementById("errors").innerHTML = document.getElementById("errors").innerHTML + "<BR/>Server returned: " + isSwitchOnResult;
                    errorNum++;
                }
                
                OnTradingStatusUnknown();
            }
        }
        
        function OnError(request, status, error) {
            if (logErrors.toLowerCase() == "true" && errorNum < 100) {

                var errorText = "";
                if (!(typeof error === 'undefined' || error == null)) {
                    errorText += error.toString();
                }
                
                if (!(typeof request === 'undefined' || request == null || typeof request.responseText === 'undefined')) {
                    errorText += request.responseText;
                }

                document.getElementById("errors").innerHTML = document.getElementById("errors").innerHTML + "<BR/>" + errorText;
                errorNum++;
            }
            
            OnTradingStatusUnknown();
        }
        
        function OnTradingStatusUnknown() {
            
            document.getElementById('TradingOnDiv').style.display = 'none';
            document.getElementById('TradingOffDiv').style.display = 'none';
            document.getElementById('TradingUnknownDiv').style.display = 'block';

            CallIsLoggedOnFromJquery();
        }

        
        
        function CallIsLoggedOnFromJquery() {
            $.ajax({
                type: "POST",
                url: "KillSwWebService.asmx/IsLoggedOn",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess2,
                error: OnError2
            });
        }

        function OnSuccess2(data, status) {
            if (!(typeof data === 'undefined' || data == null) && !(typeof data.d === 'undefined' || data.d == null) && data.d == false) {
                window.location.href = 'Account/Login.aspx';
            }
            
            markTimestamp();
        }

        function OnError2(request, status, error) {

            markTimestamp();
        }



        
        function markTimestamp() {
            var d = new Date();
            document.getElementById("timestamp").innerText = d.toUTCString();
            
            setTimeout("CallIsTradingEnabledFromJquery();", 1000 * 1);
        }
        

        //function AutoRefresh() {
        //    CallIsTradingEnabledFromJquery();
            
        //    setTimeout("AutoRefresh();", 1000 * 1);
        //}
        
        function OnPageLoad() {
            window.resizeTo(270, 290);
            CallIsTradingEnabledFromJquery();
        }

        if (window.addEventListener) 
            window.addEventListener("load", OnPageLoad, true);
        else window.onload = OnPageLoad;

    </script>

    <title></title>
    <style type="text/css">
        .auto-style1 {
            text-align: right;
        }
        .auto-style2 {
            text-align: center;
        }
        #timestamp {
            text-align: right;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        <div style="text-align: right"><span id="timestamp">N/A</span></div>
        <br/>
        
        <div id="TradingOnDiv" style="display: none"><a href="RiskMgmtOverview.aspx?Action=turnoff"><img src="Styles/play_big.png" title="Trading is ON. Click here to turn it OFF" style="display: block; margin-left: auto; margin-right: auto; border-style: none;"/> </a></div>
        <div id="TradingOffDiv" style="display: none"><a href="RiskMgmtOverview.aspx?Action=turnon" onclick="return confirm('Are you sure you want to enable trading?')"><img src="Styles/pause_big.png" title="Trading is OFF. Click here to turn it ON" style="display: block; margin-left: auto; margin-right: auto; border-style: none;"/> </a></div>
        <div id="TradingUnknownDiv" style="display: block"><img src="Styles/unknown_big.png" title="Trading status is UNKNOWN, please go to Full Web UI page for more details" style="display: block; margin-left: auto; margin-right: auto; border-style: none;"/></div>
        
        <a href="RiskMgmtOverviewInternal.aspx">Full Web UI</a>
        <br/>
        <span id="errors" style="color: red;"></span>

    </div>
    </form>
</body>
</html>
