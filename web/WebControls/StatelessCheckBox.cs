﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebControls
{

    //public class StatelessCheckBox : CheckBox
    //{
    //    private bool _checked;

    //    public override bool Checked
    //    {
    //        get { return _checked; }
    //        set { _checked = value; }
    //    }

    //    protected override void OnInit(EventArgs e)
    //    {
    //        base.OnInit(e);
    //        //You must tell the page that you use ControlState.
    //        //Page.RegisterRequiresControlState(this);
    //    }

    //    protected override object SaveControlState()
    //    {
    //        //You save the base's control state, and add your property.
    //        //object obj = base.SaveControlState();

    //        //return new Pair(obj, _checked);
    //        return null;
    //    }

    //    protected override object SaveViewState()
    //    {
    //        return null;
    //    }

    //    protected override void LoadControlState(object state)
    //    {
    //        if (state != null)
    //        {
    //            //Take the property back.
    //            Pair p = state as Pair;
    //            if (p != null)
    //            {
    //                base.LoadControlState(p.First);
    //                _checked = (bool) p.Second;
    //            }
    //            else
    //            {
    //                base.LoadControlState(state);
    //            }
    //        }
    //    }
    //}



    public class StatelessCheckBox : WebControl
    {
        public bool Checked { get; set; }
        //public bool Enabled { get; set; }

        //IMPORTNAT:
        // the trick is in not using the 'name' attribute!!! - once this is used - asp.net will use it on client
        // side to collect the state for POAT request
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write("<input id=\"{0}\" type=\"checkbox\" {1} {2}>", this.ClientID, this.Checked ? "checked=\"checked\"" : string.Empty,
                this.Enabled ? string.Empty : "disabled=\"disabled\"");
        }
    }
}
