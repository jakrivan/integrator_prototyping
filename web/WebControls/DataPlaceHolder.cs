﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebControls
{
    public class DataPlaceHolder : PlaceHolder
    {
        protected override void DataBindChildren()
        {
            if (Visible)
            {
                base.DataBindChildren();
            }
        }
    }
}
