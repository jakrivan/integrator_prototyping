﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Common.CommandTrees;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public class PageWithNoControlState : System.Web.UI.Page
    {

        protected override void SavePageStateToPersistenceMedium(object state)
        {
            // Do nothing here
        }

        //protected override NameValueCollection DeterminePostBackMode()
        //{
        //    NameValueCollection coll = base.DeterminePostBackMode();

        //    if (coll != null)
        //    {
        //        // The collection provided by the base class can be readonly.
        //        coll = new NameValueCollection(coll);
        //        coll.Remove("__PREVIOUSPAGE");r
        //    }

        //    return coll;
        //}

    }

    public static class RepeaterExtensionMethods
    {
        public static Control FindControlInHeader(this Repeater repeater, string controlName)
        {
            return repeater.Controls[0].Controls[0].FindControl(controlName);
        }

        public static Control FindControlInFooter(this Repeater repeater, string controlName)
        {
            return repeater.Controls[repeater.Controls.Count - 1].Controls[0].FindControl(controlName);
        }
    }


    public enum RoleId
    {
        PrimeBroker = 1,
        Kgt = 2
    }

    public enum TradingSystemType
    {
        Cross,
        MM,
        Quoting,
        Stream,
        Glider,
        SmartCross
    }

    public enum VenueCounterparty
    {
        L01,
        LM2,
        LM3,
        LX1,
        LGC,
        FA1,
        FL1,
        FC1,
        FC2,
        HTF,
        HTA,
        HT3,
        H4T,
        H4M,
        FS1,
        FS2
    }

    public enum CreditZone
    {
        NoCheckingZone,
        SimpleCheckingZone,
        LockedZone,
        OverTheMaxZone
    }

    public class LoginOperationResult
    {
        private static Dictionary<string, RoleId> _roleMappings = new Dictionary<string, RoleId>()
            {
                {"Primebroker", RoleId.PrimeBroker},
                {"KGT", RoleId.Kgt}
            };

        public static bool TryGetRoleId(string roleString, out RoleId roleId)
        {
            return _roleMappings.TryGetValue(roleString, out roleId);
        }

        public static LoginOperationResult CreateFailedResult(string errorMessage)
        {
            return new LoginOperationResult(false, errorMessage, false, 0);
        }

        public static LoginOperationResult CreateSuccededResult(bool isAdmin, RoleId roleId)
        {
            return new LoginOperationResult(true, null, isAdmin, roleId);
        }

        private LoginOperationResult(bool succeeded, string errorMessage, bool isAdmin, RoleId roleId)
        {
            this.Succeeded = succeeded;
            this.ErrorMessage = errorMessage;
            this.IsAdmin = isAdmin;
            this.RoleId = roleId;
        }

        public bool Succeeded { get; private set; }
        public string ErrorMessage { get; private set; }
        public bool IsAdmin { get; private set; }
        public RoleId RoleId { get; private set; }
    }

    public static class Utils
    {
        public static string VenueTypeString(VenueCounterparty venueCounterparty)
        {
            switch (venueCounterparty)
            {
                case VenueCounterparty.L01:
                case VenueCounterparty.LM2:
                case VenueCounterparty.LM3:
                case VenueCounterparty.LX1:
                case VenueCounterparty.LGC:
                    return "LMAX";
                case VenueCounterparty.FA1:
                case VenueCounterparty.FL1:
                    return "FXall";
                case VenueCounterparty.FC1:
                case VenueCounterparty.FC2:
                    return "FXCM";
                case VenueCounterparty.FS1:
                case VenueCounterparty.FS2:
                    return "FXCMMM";
                case VenueCounterparty.HTF:
                case VenueCounterparty.HTA:
                case VenueCounterparty.HT3:
                case VenueCounterparty.H4T:
                case VenueCounterparty.H4M:
                    return "Hotspot";
                default:
                    throw new ArgumentOutOfRangeException("venueCounterparty");
            }
        }

        public static string BackendGlidingCounterpartyToUi(string counterpartyCode)
        {
            return counterpartyCode.Equals("L01") ? "L0*" : counterpartyCode;
        }

        public static string UiGlidingCounterpartyToBackend(string counterpartyCode)
        {
            return counterpartyCode.Equals("L0*") ? "L01" : counterpartyCode;
        }

        public static decimal Normalize(this decimal value)
        {
            return value / 1.000000000000000000000000000000000m;
        }

        public enum NullsHandling
        {
            DbNullToEmpty,
            DbNullToZero,
            DbNullToNA
        }

        public static string GetCssClassForNumber(object dbObject)
        {
            if (dbObject == DBNull.Value)
                return string.Empty;
            else if (dbObject is decimal)
                return ((decimal)dbObject) > 0
                           ? "RecentDealChange"
                           : ((decimal)dbObject) < 0 ? "attentionCell" : string.Empty;
            else
                return string.Empty;
        }

        public static string CoallesceDbStrings(object dbObject1, object dbObject2)
        {
            if (dbObject1 == null || dbObject1 == DBNull.Value)
                return dbObject2.ToString();

            if (string.IsNullOrEmpty(dbObject1.ToString()))
                return dbObject2.ToString();

            return dbObject1.ToString();
        }

        public static string FormatDecimalNumber(object dbObject, int decimals, NullsHandling nullsHandling)
        {
            if (dbObject == DBNull.Value)
                switch (nullsHandling)
                {
                    case NullsHandling.DbNullToEmpty:
                        return string.Empty;
                    case NullsHandling.DbNullToZero:
                        return 0m.ToString("n" + decimals);
                    case NullsHandling.DbNullToNA:
                        return "N/A";
                    default:
                        return "UnsupportedArg";
                }
            else if (dbObject is decimal)
                return ((decimal) dbObject).ToString("n" + decimals);
            else
                return "N/A";
        }

        public static bool NullOrZero(object dbObject)
        {
            if (dbObject == DBNull.Value)
                return true;

            if (dbObject is decimal)
                return (decimal) dbObject == 0m;
            if (dbObject is int)
                return (int) dbObject == 0;
            if (dbObject is long)
                return (long)dbObject == 0;

            return false;
        }

        public static System.Drawing.Color GetColorForNumber(object dbObject)
        {
            if (dbObject == DBNull.Value)
                return System.Drawing.Color.Empty;
            else if (dbObject is decimal)
                return ((decimal)dbObject) > 0
                           ? System.Drawing.Color.LightGreen
                           : ((decimal)dbObject) < 0 ? System.Drawing.Color.LightPink : System.Drawing.Color.Empty;
            else
                return System.Drawing.Color.Empty;
        }

        public static bool NotNullOrFalse(object dbObject)
        {
            return dbObject != DBNull.Value && (bool) dbObject;
        }

        public static string FormatNormalizedDecimalNumber(object dbObject, NullsHandling nullsHandling)
        {
            if (dbObject == DBNull.Value)
                switch (nullsHandling)
                {
                    case NullsHandling.DbNullToEmpty:
                        return string.Empty;
                    case NullsHandling.DbNullToZero:
                        return "0";
                    case NullsHandling.DbNullToNA:
                        return "N/A";
                    default:
                        return "UnsupportedArg";
                }
            else if (dbObject is decimal)
                return ((decimal)dbObject).Normalize().ToString();
            else
                return "N/A";
        }

        public static string ReplaceSpacesWithNonBreakableSpaces(string input)
        {
            return input.Replace(" ", "&nbsp;");
        }

        public static string FormatLargeNumber(object dbObject, NullsHandling nullsHandling)
        {
            if (dbObject == DBNull.Value)
                switch (nullsHandling)
                {
                    case NullsHandling.DbNullToEmpty:
                        return string.Empty;
                    case NullsHandling.DbNullToZero:
                        return "0";
                    case NullsHandling.DbNullToNA:
                        return "N/A";
                    default:
                        return "UnsupportedArg";
                }
            else if (dbObject is decimal)
            {
                decimal d = (decimal) dbObject;
                if (d == 0)
                    return "0";
                if (d % 1000000000 == 0)
                    return string.Format("{0:n0}G", d / 1000000000);
                if (d%1000000 == 0)
                    return string.Format("{0:n0}M", d/1000000);
                if(d%1000 == 0)
                    return string.Format("{0:n0}k", d / 1000);
                return d.Normalize().ToString();
            }
            else if (dbObject is int || dbObject is long)
            {
                long i;
                if (dbObject is int)
                    i = (long) (int) dbObject;
                else
                    i = (long)dbObject;
                if (i == 0)
                    return "0";
                if (i % 1000000000 == 0)
                    return string.Format("{0}G", i / 1000000000);
                if (i % 1000000 == 0)
                    return string.Format("{0}M", i / 1000000);
                if (i % 1000 == 0)
                    return string.Format("{0}k", i / 1000);
                return i.ToString();
            }
            else
                return "N/A";
        }

        public static string GetNumberCellStyle(object dbValue)
        {
            if (dbValue == DBNull.Value)
                return string.Empty;

            if (dbValue is decimal)
            {
                decimal val = (decimal)dbValue;

                if (val > 0)
                    return "PositiveNum";

                if (val < 0)
                    return "NegativeNum";
            }

            if (dbValue is int)
            {
                int val = (int)dbValue;

                if (val > 0)
                    return "PositiveNum";

                if (val < 0)
                    return "NegativeNum";
            }

            return string.Empty;
        }

        public enum DealAction
        {
            KgtDeal,
            KgtRejection,
            CptRejection
        }

        private static string[] _dealActionTimesNamers = new[]
        {"LastDealUtc", "LastKgtRejectionUtc", "LastCptRejectionUtc"};

        public static string GetDealActionCellStyle(object dataItem, DealAction dealAction)
        {
            if (dataItem == null)
                return string.Empty;

            object dbValue;

            if (dataItem is DataRowView)
                dbValue = ((DataRowView) dataItem)[_dealActionTimesNamers[(int) dealAction]];
            else if (dataItem is DbDataRecord)
                dbValue = ((DbDataRecord)dataItem)[_dealActionTimesNamers[(int)dealAction]];
            else
                dbValue = DataBinder.Eval(dataItem, _dealActionTimesNamers[(int)dealAction]);
            

            if (dbValue == DBNull.Value || !(dbValue is DateTime))
                return string.Empty;

            if (DateTime.UtcNow - (DateTime) dbValue < HighlightDurationConsts.TRADING_ACTION_SYSTEM_HIGHLIGHT_DURATION)
            {
                if (dealAction == DealAction.KgtDeal)
                    return "RecentDealChange";
                else
                    return "RecentRejectionChange";
            }
            else
            {
                return string.Empty;
            }
        }

        public static bool TryParseIntIntToNullable(string s, out int? result)
        {
            int temp;
            if (int.TryParse(s, out temp))
            {
                result = temp;
                return true;
            }
            else
            {
                result = null;
                return false;
            }
        }

        public static string AddSlashToSymbol(string symbol)
        {
            if (symbol.Length == 6)
            {
                symbol = symbol.Substring(0, 3) + '/' + symbol.Substring(3, 3);
            }

            return symbol;
        }

        public static string RemoveSlashFromSymbol(string symbol)
        {
            if (symbol.Length == 7)
            {
                symbol = symbol.Substring(0, 3) + symbol.Substring(4, 3);
            }

            return symbol;
        }


        public static string[] _devUsersLogins = new string[] {"Honza", "Jan", "test", "Michal.Kreslik@KGT"};

        public static LoginOperationResult TryLoginUser(string username, string password, string address)
        {
            bool logedOn = false;
            bool isAdmin = false;
            RoleId roleId = 0;
            string errorMessage = null;

            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[TryLoginRiskMgmtUser_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@username", SqlDbType.NVarChar).Value = username;
                        command.Parameters.Add("@password", SqlDbType.NVarChar).Value = password;
                        command.Parameters.Add("@identity", SqlDbType.NVarChar).Value = address;

                        string roleString = string.Empty;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            logedOn = reader.GetBoolean(reader.GetOrdinal("Loged"));
                            isAdmin = reader.GetBoolean(reader.GetOrdinal("IsAdmin"));
                            if (reader.IsDBNull(reader.GetOrdinal("RoleName")))
                            {
                                roleString = string.Empty;
                            }
                            else
                            {
                                roleString = reader.GetString(reader.GetOrdinal("RoleName"));
                            }

                            break;
                        }

                        if (!logedOn)
                        {
                            errorMessage = "Logon failed! (Unknown username or password)";
                        }
                        else
                        {
                            if (!LoginOperationResult.TryGetRoleId(roleString, out roleId))
                            {
                                logedOn = false;
                                errorMessage = string.Format("Internal error (unknown RoleId: {0})",
                                                                           roleString);
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    errorMessage = "Logon failed! (Backend is currently unavailable)";
                }
            }
            else
            {
                errorMessage = "Logon failed! (Website configuration error)";
            }


            if (logedOn)
            {
                return LoginOperationResult.CreateSuccededResult(isAdmin, roleId);
            }
            else
            {
                return LoginOperationResult.CreateFailedResult(errorMessage);
            }
        }

        public static string GetGroupName(HttpRequest request)
        {
            string groupName = null;
            int? tradingGroupId;
            if (Utils.TryParseIntIntToNullable(request.QueryString["GroupId"], out tradingGroupId))
            {
                groupName = GlobalState.GetGroupName(tradingGroupId.Value);
            }

            return string.IsNullOrEmpty(groupName) ? "<Unknwon Group>" : groupName;
        }

        public static string GetUatPrefix(HttpSessionState session)
        {
            bool? isUatSession = Utils.IsUatSession(session);

            return isUatSession.HasValue ? (isUatSession.Value ? "UAT " : string.Empty) : "(Unknown session type)";
        }

        public static LoginOperationResult TryGetInfoForUser(string username)
        {
            bool logedOn = false;
            bool isAdmin = false;
            RoleId roleId = 0;
            string errorMessage = null;

            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetDetailsForRiskMgmtUser_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@username", SqlDbType.NVarChar).Value = username;

                        string roleString = string.Empty;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            logedOn = reader.GetBoolean(reader.GetOrdinal("Loged"));
                            isAdmin = reader.GetBoolean(reader.GetOrdinal("IsAdmin"));
                            roleString = reader.GetString(reader.GetOrdinal("RoleName"));
                            break;
                        }

                        if (!logedOn)
                        {
                            errorMessage = "Logon failed! (Unknown username or password)";
                        }
                        else
                        {
                            if (!LoginOperationResult.TryGetRoleId(roleString, out roleId))
                            {
                                logedOn = false;
                                errorMessage = string.Format("Internal error (unknown RoleId: {0})",
                                                                           roleString);
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    errorMessage = "Logon failed! (Backend is currently unavailable)";
                }
            }
            else
            {
                errorMessage = "Logon failed! (Website configuration error)";
            }


            if (logedOn)
            {
                return LoginOperationResult.CreateSuccededResult(isAdmin, roleId);
            }
            else
            {
                return LoginOperationResult.CreateFailedResult(errorMessage);
            }
        }

        public const string PrimebrokerSwitchName = "PrimebrokerKillSwitch";
        public const string KgtSwitchName = "KGTManualKillSwitch";
        public const string IntegratorSwitchName = "KGTIntegratorKillSwitch";

        private static Dictionary<RoleId, string> _killSwitchBitNameMappings = new Dictionary<RoleId, string>()
            {
                {RoleId.PrimeBroker, PrimebrokerSwitchName},
                {RoleId.Kgt, KgtSwitchName}
            };

        public static string GetKillSwitchBitName(HttpSessionState Session)
        {
            LoginOperationResult loginOperationResult = null;

            if (Session["LoginOperationResult"] == null || Session["LoginOperationResult"].GetType() != typeof(LoginOperationResult))
            {
                loginOperationResult = TryGetInfoForUser(HttpContext.Current.User.Identity.Name);
                Session["LoginOperationResult"] = loginOperationResult;
            }
            else
            {
                loginOperationResult = Session["LoginOperationResult"] as LoginOperationResult;
            }

            string killSwitchBitName = string.Empty;
            _killSwitchBitNameMappings.TryGetValue(loginOperationResult.RoleId, out killSwitchBitName);

            return killSwitchBitName;
        }

        private static Dictionary<RoleId, string> _killSwitchRoleNameMappings = new Dictionary<RoleId, string>()
            {
                {RoleId.PrimeBroker, "Primebroker"},
                {RoleId.Kgt, "KGT"}
            };

        public static string GetKillSwitchRoleName(HttpSessionState Session)
        {
            LoginOperationResult loginOperationResult = null;

            if (Session["LoginOperationResult"] == null || Session["LoginOperationResult"].GetType() != typeof(LoginOperationResult))
            {
                loginOperationResult = TryGetInfoForUser(HttpContext.Current.User.Identity.Name);
                Session["LoginOperationResult"] = loginOperationResult;
            }
            else
            {
                loginOperationResult = Session["LoginOperationResult"] as LoginOperationResult;
            }

            string killSwitchSiteName = string.Empty;
            _killSwitchRoleNameMappings.TryGetValue(loginOperationResult.RoleId, out killSwitchSiteName);

            return killSwitchSiteName;
        }

        public static bool GetIsDevUser()
        {
            return _devUsersLogins.Contains(HttpContext.Current.User.Identity.Name, StringComparer.OrdinalIgnoreCase);
        }

        public static bool IsKgtUser(HttpSessionState Session)
        {
            LoginOperationResult loginOperationResult = null;

            if (Session["LoginOperationResult"] == null || Session["LoginOperationResult"].GetType() != typeof(LoginOperationResult))
            {
                loginOperationResult = TryGetInfoForUser(HttpContext.Current.User.Identity.Name);
                Session["LoginOperationResult"] = loginOperationResult;
            }
            else
            {
                loginOperationResult = Session["LoginOperationResult"] as LoginOperationResult;
            }

            return loginOperationResult.RoleId == RoleId.Kgt;
        }

        public static bool GetIsAdmin(HttpSessionState Session)
        {
            if (Session["IsAdmin"] != null)
            {
                return Session["IsAdmin"].Equals(true);
            }

            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return false;
            }

            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();


                        SqlCommand command = new SqlCommand("SELECT [dbo].[GetIsKillSwitchUserAdmin](@UserName)", connection);
                        command.Parameters.Add("@UserName", SqlDbType.NVarChar).Value =
                        HttpContext.Current.User.Identity.Name;
                        object isAdminResult = command.ExecuteScalar();            
                        if (isAdminResult != null)
                        {
                            Session["IsAdmin"] = (bool) isAdminResult;
                            return (bool) isAdminResult;
                        }
                    }
                }
                catch (Exception)
                {

                }
            }

            return false;
        }

        public static bool? IsUatSession(HttpSessionState Session)
        {
            if (Session == null)
                return null;

            if (Session["IsUat"] == null)
            {
                try
                {
                    Session["IsUat"] = new SqlConnectionStringBuilder(
                        ConfigurationManager.ConnectionStrings["KillSwitchBackend"].ConnectionString).InitialCatalog
                                                                                                     .Contains("Testing");
                }
                catch (Exception)
                {
                    return null;
                }
            }

            return Session["IsUat"].Equals(true);
        }
    }
}