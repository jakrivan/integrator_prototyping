﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace KGTKillSwitchWeb
{
    public class StreamingSettingsDal
    {
        private TimeSpan[] _maxLLTimesOnDestination = new TimeSpan[Enum.GetValues(typeof(VenueCounterparty)).Length];
        private TimeSpan[] _expectedRTHalfTime = new TimeSpan[Enum.GetValues(typeof(VenueCounterparty)).Length];
        
        public static readonly StreamingSettingsDal Instance = new StreamingSettingsDal();
        
        private StreamingSettingsDal()
        {
            this.ReadSettings();
        }

        private void ReadSettings()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];
            using (SqlConnection sqlConnection = new SqlConnection(connectionString.ConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand command = new SqlCommand("[dbo].[GetStreamingSettingsPerCounterparty_SP]", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            VenueCounterparty ctp;
                            if(Enum.TryParse(reader.GetString(reader.GetOrdinal("Counterparty")), out ctp))
                            {
                                _maxLLTimesOnDestination[(int)ctp] =
                                TimeSpan.FromMilliseconds(
                                    reader.GetInt16(reader.GetOrdinal("MaxLLTimeOnDestination_milliseconds")));
                            }
                        }
                    }
                }
            }

            using (SqlConnection sqlConnection = new SqlConnection(connectionString.ConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand command = new SqlCommand("[dbo].[GetDelaySettings_SP]", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            VenueCounterparty ctp;
                            if (Enum.TryParse(reader.GetString(reader.GetOrdinal("Counterparty")), out ctp))
                            {
                                _expectedRTHalfTime[(int) ctp] =
                                    TimeSpan.FromTicks(reader.GetInt64(reader.GetOrdinal("ExpectedDelayTicks")));
                            }
                        }
                    }
                }
            }
        }

        public TimeSpan GetMaxLLTimeOnDestination(VenueCounterparty counterparty)
        {
            return this._maxLLTimesOnDestination[(int)counterparty];
        }

        public TimeSpan GetExpectedRTHalfTime(VenueCounterparty counterparty)
        {
            return this._expectedRTHalfTime[(int)counterparty];
        }
    }
}