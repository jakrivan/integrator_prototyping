﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public static class DateTimeExtensions
    {
        public static DateTime Next(this DateTime date, DayOfWeek dayOfWeek)
        {
            return date.AddDays((dayOfWeek < date.DayOfWeek ? 7 : 0) + dayOfWeek - date.DayOfWeek);
        }
    }

    public static class TimeZoneHelpers
    {
        public static readonly TimeZoneInfo EasternTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
        public static readonly TimeZoneInfo CentralEuropeTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time");
        public static readonly TimeZoneInfo NzTimeZone = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
        public static readonly TimeZoneInfo UtcTimeZone = TimeZoneInfo.FindSystemTimeZoneById("UTC");
        public static readonly TimeZoneInfo LondonTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");

        public enum TimeZoneCode
        {
            ET = 0,
            UTC = 1,
            CET = 2,
            NZT = 3,
            BT = 4
        }

        public enum SystemsTradingControlScheduleAction
        {
            ActivateGoFlatOnly,
            ActivateKillSwitch,
            RestartIntegrator,
            NotifyClients
        }

        public static string GetTimeZoneDescription(TimeZoneCode timeZoneCode)
        {
            return _timeZoneNames[(int) timeZoneCode];
        }

        public static string GetTimeZoneDescription(string timeZoneCode)
        {
            return _timeZoneNames[(int) Enum.Parse(typeof(TimeZoneCode), timeZoneCode)];
        }

        private static readonly TimeZoneInfo[] _timeZoneInfos = new TimeZoneInfo[] { EasternTimeZone, UtcTimeZone, CentralEuropeTimeZone, NzTimeZone, LondonTimeZone };
        private static readonly string[] _timeZoneNames = new string[] { "New York (ET)", "UTC", "Paris (CET)", "New Zealand (NZT)", "London (BT)" };
        

        public static TimeZoneInfo GetTimeZoneInfo(string timezoneCode)
        {
            timezoneCode = timezoneCode.TrimEnd();


            var timeZoneCodes = Enum.GetNames(typeof (TimeZoneCode));
            for (int i = 0; i < timeZoneCodes.Length; i++)
            {
                if (timeZoneCodes[i].Equals(timezoneCode))
                    return _timeZoneInfos[i];
            }

            throw new ArgumentOutOfRangeException("timezoneCode", timezoneCode, "Unrecognized timezone code");
        }

        public static IEnumerable<TimeZoneCode> GetSortedTimeZoneCodes()
        {
            return Enum.GetValues(typeof (TimeZoneHelpers.TimeZoneCode)).Cast<TimeZoneCode>().OrderBy(code => _timeZoneInfos[(int) code].BaseUtcOffset);
        }

        public static DateTime ConvertToUtc(DateTime time, TimeZoneInfo timeZone)
        {
            return TimeZoneInfo.ConvertTimeToUtc(time, timeZone);
        }

        public static DateTime ConvertToCet(DateTime time, TimeZoneInfo timeZone)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(TimeZoneInfo.ConvertTimeToUtc(time, timeZone), CentralEuropeTimeZone);
        }

        public static DateTime ConvertToEt(DateTime time, TimeZoneInfo timeZone)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(TimeZoneInfo.ConvertTimeToUtc(time, timeZone), EasternTimeZone);
        }

        public static DateTime ConvertToBt(DateTime time, TimeZoneInfo timeZone)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(TimeZoneInfo.ConvertTimeToUtc(time, timeZone), LondonTimeZone);
        }

        public static DateTime? GetNextAdjustmentDate(TimeZoneInfo timeZoneInfo)
        {
            var adjustments = timeZoneInfo.GetAdjustmentRules();

            if (adjustments.Length == 0)
            {
                return null;
            }

            int year = DateTime.UtcNow.Year;
            TimeZoneInfo.AdjustmentRule adjustment = null;
            foreach (TimeZoneInfo.AdjustmentRule adjustment1 in adjustments)
            {
                // Determine if this adjustment rule covers year desired 
                if (adjustment1.DateStart.Year <= year && adjustment1.DateEnd.Year >= year)
                    adjustment = adjustment1;
            }
            if (adjustment == null)
                return null;

            //TimeZoneInfo.TransitionTime startTransition, endTransition;

            DateTime dstStart = GetCurrentYearAdjustmentDate(adjustment.DaylightTransitionStart);
            DateTime dstEnd = GetCurrentYearAdjustmentDate(adjustment.DaylightTransitionEnd);


            if (dstStart >= DateTime.UtcNow.Date)
                return dstStart;
            if (dstEnd >= DateTime.UtcNow.Date)
                return dstEnd;
            return null;
        }

        private static DateTime GetCurrentYearAdjustmentDate(TimeZoneInfo.TransitionTime transitionTime)
        {
            int year = DateTime.UtcNow.Year;

            if (transitionTime.IsFixedDateRule)
                return new DateTime(year, transitionTime.Month, transitionTime.Day);
            else
            {
                // For non-fixed date rules, get local calendar
                System.Globalization.Calendar cal = CultureInfo.CurrentCulture.Calendar;
                // Get first day of week for transition 
                // For example, the 3rd week starts no earlier than the 15th of the month 
                int startOfWeek = transitionTime.Week * 7 - 6;
                // What day of the week does the month start on? 
                int firstDayOfWeek = (int)cal.GetDayOfWeek(new DateTime(year, transitionTime.Month, 1));
                // Determine how much start date has to be adjusted 
                int transitionDay;
                int changeDayOfWeek = (int)transitionTime.DayOfWeek;

                if (firstDayOfWeek <= changeDayOfWeek)
                    transitionDay = startOfWeek + (changeDayOfWeek - firstDayOfWeek);
                else
                    transitionDay = startOfWeek + (7 - firstDayOfWeek + changeDayOfWeek);

                // Adjust for months with no fifth week 
                if (transitionDay > cal.GetDaysInMonth(year, transitionTime.Month))
                    transitionDay -= 7;

                return new DateTime(year, transitionTime.Month, transitionDay);
            }
        }
    }

    public partial class Schedule : System.Web.UI.Page
    {
        private enum ScheduleType
        {
            Daily,
            Weekly,
            ExactDate
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (!Utils.IsKgtUser(Session))
                {
                    Response.Redirect("~/NotAuthorized.aspx");
                    return;
                }

                PopulateDSTInfo();

                //Be carefull on updating the grid after the edit is in progress - this would
                // yield the validators unusable
                if (!IsGridEditOrInsert())
                {
                    this.PopulateSettings();
                    ValueChangedPanel.Visible = false;
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        private void PopulateDSTInfo()
        {
            DateTime? dt;
            dt = TimeZoneHelpers.GetNextAdjustmentDate(TimeZoneHelpers.CentralEuropeTimeZone);
            this.cetLiteral.Text = dt.HasValue ? dt.Value.ToString("dd. MMMM") : "None this year";
            if (dt.HasValue && dt.Value <= DateTime.UtcNow.AddDays(14))
            {
                this.cetLiteral.Font.Bold = true;
                this.cetLiteral.ForeColor = Color.Red;
            }
            dt = TimeZoneHelpers.GetNextAdjustmentDate(TimeZoneHelpers.EasternTimeZone);
            this.etLiteral.Text = dt.HasValue ? dt.Value.ToString("dd. MMMM") : "None this year";
            if (dt.HasValue && dt.Value <= DateTime.UtcNow.AddDays(14))
            {
                this.etLiteral.Font.Bold = true;
                this.etLiteral.ForeColor = Color.Red;
            }
            dt = TimeZoneHelpers.GetNextAdjustmentDate(TimeZoneHelpers.NzTimeZone);
            this.nztLiteral.Text = dt.HasValue ? dt.Value.ToString("dd. MMMM") : "None this year";
            if (dt.HasValue && dt.Value <= DateTime.UtcNow.AddDays(14))
            {
                this.nztLiteral.Font.Bold = true;
                this.nztLiteral.ForeColor = Color.Red;
            }
            dt = TimeZoneHelpers.GetNextAdjustmentDate(TimeZoneHelpers.LondonTimeZone);
            this.btLiteral.Text = dt.HasValue ? dt.Value.ToString("dd. MMMM") : "None this year";
            if (dt.HasValue && dt.Value <= DateTime.UtcNow.AddDays(14))
            {
                this.btLiteral.Font.Bold = true;
                this.btLiteral.ForeColor = Color.Red;
            }
        }

        private bool IsGridEditOrInsert()
        {
            bool isEdit = SpecificDatesScheduleGridView.EditIndex != -1 || WeeklyScheduleGridView.EditIndex != -1 ||
            DailyScheduleGridView.EditIndex != -1;

            bool isInsert = 
                (SpecificDatesScheduleGridView.FooterRow != null && SpecificDatesScheduleGridView.FooterRow.Visible)
                || (WeeklyScheduleGridView.FooterRow != null && WeeklyScheduleGridView.FooterRow.Visible)
                || (DailyScheduleGridView.FooterRow != null && DailyScheduleGridView.FooterRow.Visible);

            return isEdit || isInsert;
        }

        private void UpdateTimeLable()
        {
            LblUpdatedOn.Text =
                string.Format(
                    "This page was rendered on {0:yyyy-MM-dd HH:mm:ss} UTC (No autoupdating!).", DateTime.UtcNow);
            LblSettingsUpdatedOn.Text = string.Format(
                    "Settings were last updated on {0} UTC", HiddenLastUpdatedUtc.Value);
        }

        private void DisableEnableOne(int scheduleEntryId, bool enable)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[SystemsTradingControlScheduleDisableEnableOne_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ScheduleEntryId", scheduleEntryId);
                        command.Parameters.AddWithValue("@Enable", enable);

                        int rows = command.ExecuteNonQuery();
                    }
                }
                catch (Exception exc)
                {
                    ErrorPanel.Visible = true;
                }
            }
            else
            {
                ErrorPanel.Visible = true;
            }
        }

        private void DeleteSettingEntry(int scheduleEntryId)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[SystemsTradingControlScheduleDeleteOne_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ScheduleEntryId", scheduleEntryId);

                        int rows = command.ExecuteNonQuery();
                    }
                }
                catch (Exception exc)
                {
                    ErrorPanel.Visible = true;
                }
            }
            else
            {
                ErrorPanel.Visible = true;
            }
        }

        private bool UpdateOne(ScheduleInfoBag scheduleInfoBag)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[SystemsTradingControlScheduleUpdateSingle_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ScheduleEntryId", scheduleInfoBag.ScheduleId);
                        command.Parameters.AddWithValue("@Enable", scheduleInfoBag.Enabled);
                        command.Parameters.AddWithValue("@IsDaily",
                            scheduleInfoBag.ScheduleType == ScheduleType.Daily ? true : false);
                        command.Parameters.AddWithValue("@IsWeekly",
                            scheduleInfoBag.ScheduleType == ScheduleType.Weekly ? true : false);
                        command.Parameters.Add("@ScheduleTimeZoneSpecific", SqlDbType.DateTime2).Value =
                        scheduleInfoBag.ScheduleTimeZoneSpecific;
                        command.Parameters.AddWithValue("@TimeZoneCode", scheduleInfoBag.TimeZoneCode.ToString());
                        command.Parameters.AddWithValue("@ActionName", scheduleInfoBag.Action.ToString());
                        command.Parameters.AddWithValue("@ScheduleDescription", scheduleInfoBag.Description);

                        return command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception exc)
                {
                    ErrorPanel.Visible = true;
                }
            }
            else
            {
                ErrorPanel.Visible = true;
            }

            return false;
        }

        private int InsertOne(ScheduleInfoBag scheduleInfoBag)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[SystemsTradingControlScheduleInsertSingle_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Enable", scheduleInfoBag.Enabled);
                        command.Parameters.AddWithValue("@IsDaily",
                            scheduleInfoBag.ScheduleType == ScheduleType.Daily ? true : false);
                        command.Parameters.AddWithValue("@IsWeekly",
                            scheduleInfoBag.ScheduleType == ScheduleType.Weekly ? true : false);
                        command.Parameters.Add("@ScheduleTimeZoneSpecific", SqlDbType.DateTime2).Value =
                        scheduleInfoBag.ScheduleTimeZoneSpecific;
                        command.Parameters.AddWithValue("@TimeZoneCode", scheduleInfoBag.TimeZoneCode.ToString());
                        command.Parameters.AddWithValue("@ActionName", scheduleInfoBag.Action.ToString());
                        command.Parameters.AddWithValue("@ScheduleDescription", scheduleInfoBag.Description);

                        SqlParameter scheduleIdParam = new SqlParameter("@ScheduleEntryId", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        command.Parameters.Add(scheduleIdParam);

                        command.ExecuteNonQuery();

                        if (scheduleIdParam.Value != null && scheduleIdParam.Value != DBNull.Value)
                        {
                            return (int)scheduleIdParam.Value;
                        }
                    }
                }
                catch (Exception exc)
                {
                    ErrorPanel.Visible = true;
                }
            }
            else
            {
                ErrorPanel.Visible = true;
            }

            return -1;
        }

        private void RecalculateTable(ref DataTable dt)
        {
            dt.Columns.Add("ScheduleTimeCet", typeof(DateTime));
            dt.Columns.Add("ScheduleTimeEt", typeof(DateTime));
            dt.Columns.Add("ScheduleTimeUtc", typeof(DateTime));
            dt.Columns.Add("ScheduleTimeBt", typeof(DateTime));

            foreach (DataRow row in dt.Rows)
            {
                DateTime scheduleTimeZoneSpecific = (DateTime) row["ScheduleTimeZoneSpecific"];

                //daily and weekly needs to be placed to current week. currently 0001-01-0x
                if (((bool) row["IsDaily"]) || ((bool) row["IsWeekly"]))
                {
                    DateTime utcNow = DateTime.UtcNow;
                    //set it to today
                    DateTime scheduleTimeZoneSpecificInCurrentWeek = new DateTime(utcNow.Year, utcNow.Month, utcNow.Day);
                    if ((bool) row["IsWeekly"])
                    {
                        //if it's weekly - move it to a day in current week
                        scheduleTimeZoneSpecificInCurrentWeek =
                            scheduleTimeZoneSpecificInCurrentWeek.AddDays(scheduleTimeZoneSpecific.DayOfWeek -
                                                                          utcNow.DayOfWeek);
                    }

                    //and then add the actual time
                    scheduleTimeZoneSpecificInCurrentWeek =
                        scheduleTimeZoneSpecificInCurrentWeek.Add(scheduleTimeZoneSpecific.TimeOfDay);

                    row["ScheduleTimeZoneSpecific"] = scheduleTimeZoneSpecificInCurrentWeek;
                    scheduleTimeZoneSpecific = scheduleTimeZoneSpecificInCurrentWeek;
                }

                row["ScheduleTimeCet"] = TimeZoneHelpers.ConvertToCet(scheduleTimeZoneSpecific,
                    TimeZoneHelpers.GetTimeZoneInfo(row["TimeZoneCode"] as string));
                row["ScheduleTimeEt"] = TimeZoneHelpers.ConvertToEt(scheduleTimeZoneSpecific,
                    TimeZoneHelpers.GetTimeZoneInfo(row["TimeZoneCode"] as string));
                row["ScheduleTimeBt"] = TimeZoneHelpers.ConvertToBt(scheduleTimeZoneSpecific,
                    TimeZoneHelpers.GetTimeZoneInfo(row["TimeZoneCode"] as string));
                row["ScheduleTimeUtc"] = TimeZoneHelpers.ConvertToUtc(scheduleTimeZoneSpecific,
                    TimeZoneHelpers.GetTimeZoneInfo(row["TimeZoneCode"] as string));

                if ((bool) row["IsDaily"])
                {
                    row["ScheduleTimeUtc"] = DateTime.MinValue + ((DateTime) row["ScheduleTimeUtc"]).TimeOfDay;
                }

                if ((bool)row["IsWeekly"])
                {
                    DateTime utcTime = (DateTime) row["ScheduleTimeUtc"];
                    utcTime = DateTime.MinValue.AddDays((int) utcTime.DayOfWeek) + utcTime.TimeOfDay;
                    row["ScheduleTimeUtc"] = utcTime;
                }
            }

            //http://stackoverflow.com/questions/9107916/sorting-rows-in-a-data-table
            DataView dv = dt.DefaultView;
            dv.Sort = "ScheduleTimeUtc";
            dt = dv.ToTable();
        }

        private void PopulateSettingsGrid(GridView gridView, ScheduleType scheduleType)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetUpdatedSystemsTradingControlSchedule_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@LastUpdateTimeUtc", SqlDbType.DateTime2).Value =
                        DateTime.MinValue;
                        command.Parameters.AddWithValue("@IsDailyRecords",
                            scheduleType == ScheduleType.Daily ? true : false);
                        command.Parameters.AddWithValue("@IsWeeklyRecords",
                            scheduleType == ScheduleType.Weekly ? true : false);
                        command.Parameters.AddWithValue("@IsImmediateRecords", false);
                        SqlParameter lastUpdateTimeOutParam = new SqlParameter("@LastUpdateTimeServerSideUtc",
                                                                               SqlDbType.DateTime2)
                        {
                            Direction = ParameterDirection.Output
                        };
                        command.Parameters.Add(lastUpdateTimeOutParam);


                        SqlDataReader reader = command.ExecuteReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader);
                        reader.Close();

                        bool isEmpty = false;
                        if (dt.Rows.Count == 0)
                        {
                            isEmpty = true;
                            //foreach (DataColumn c in dt.Columns)
                            //    c.AllowDBNull = true; // Allow Nulls in all columns
                            dt.Rows.Add(-1, false, false, false, false, DateTime.MinValue, "UTC", "ActivateGoFlatOnly", string.Empty, DateTime.MinValue);
                        }

                        RecalculateTable(ref dt);

                        gridView.DataSource = dt;
                        gridView.DataBind();

                        if (isEmpty)
                        {
                            int columnsCount = gridView.Columns.Count;
                            gridView.Rows[0].Cells.Clear();// clear all the cells in the row
                            gridView.Rows[0].Cells.Add(new TableCell()); //add a new blank cell
                            gridView.Rows[0].Cells[0].ColumnSpan = columnsCount; //set the column span to the new added cell
                            gridView.Rows[0].Cells[0].Text = "No Records found";
                        }

                        if (lastUpdateTimeOutParam.Value != null && lastUpdateTimeOutParam.Value != DBNull.Value)
                        {
                            HiddenLastUpdatedUtc.Value = ((DateTime)lastUpdateTimeOutParam.Value).ToString("yyyy-MM-dd HH:mm:ss.ff");
                        }
                    }

                    UpdateTimeLable();
                }
                catch (Exception exc)
                {
                    ErrorPanel.Visible = true;
                }
            }
            else
            {
                ErrorPanel.Visible = true;
            }
        }

        private void PopulateSettings()
        {
            this.PopulateSettingsGrid(SpecificDatesScheduleGridView, ScheduleType.ExactDate);
            this.PopulateSettingsGrid(WeeklyScheduleGridView, ScheduleType.Weekly);
            this.PopulateSettingsGrid(DailyScheduleGridView, ScheduleType.Daily);
        }

        private class ScheduleInfoBag
        {
            public ScheduleInfoBag(ScheduleType scheduleType, bool enabled, DateTime scheduleTimeZoneSpecific, TimeZoneHelpers.TimeZoneCode timeZoneCode, TimeZoneHelpers.SystemsTradingControlScheduleAction action, string description, int scheduleId)
            {
                this.ScheduleType = scheduleType;
                this.Enabled = enabled;
                this.ScheduleTimeZoneSpecific = scheduleTimeZoneSpecific;
                this.TimeZoneCode = timeZoneCode;
                this.Action = action;
                this.Description = description;
                this.ScheduleId = scheduleId;
            }

            public ScheduleInfoBag(ScheduleType scheduleType, bool enabled, DateTime scheduleTimeZoneSpecific, TimeZoneHelpers.TimeZoneCode timeZoneCode, TimeZoneHelpers.SystemsTradingControlScheduleAction action, string description)
            {
                this.ScheduleType = scheduleType;
                this.Enabled = enabled;
                this.ScheduleTimeZoneSpecific = scheduleTimeZoneSpecific;
                this.TimeZoneCode = timeZoneCode;
                this.Action = action;
                this.Description = description;
            }

            public ScheduleType ScheduleType { get; private set; }
            public bool Enabled { get; private set; }
            public DateTime ScheduleTimeZoneSpecific { get; private set; }
            public TimeZoneHelpers.TimeZoneCode TimeZoneCode { get; private set; }
            public TimeZoneHelpers.SystemsTradingControlScheduleAction Action { get; private set; }
            public string Description { get; private set; }
            public int ScheduleId { get; private set; }
        }

        private ScheduleInfoBag ExtractScheduleInfoBag(GridViewRow gridRow, GridView gridView)
        {
            DateTime scheduleTimeZoneSpecific = DateTime.MinValue;
            ScheduleType scheduleType = ScheduleType.ExactDate;
            int descriptionCollIdx = 8;
            int timezoneColIdx = 3;

            if (gridView == this.SpecificDatesScheduleGridView)
            {
                scheduleTimeZoneSpecific =
                    DateTime.ParseExact((gridRow.Cells[1].FindControl("DateTxt") as TextBox).Text,
                                        "yyyy-MM-dd", CultureInfo.InvariantCulture,
                                        DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal)
                    +
                    TimeSpan.ParseExact((gridRow.Cells[2].FindControl("TimeTxt") as TextBox).Text,
                                        @"hh\:mm\:ss", CultureInfo.InvariantCulture);

                scheduleType = ScheduleType.ExactDate;
            }
            else if (gridView == this.WeeklyScheduleGridView)
            {
                scheduleType = ScheduleType.Weekly;

                scheduleTimeZoneSpecific =
                    DateTime.MinValue.Next(
                        (DayOfWeek)
                        Enum.Parse(typeof (DayOfWeek),
                                   (gridRow.Cells[2].FindControl("DayDropDown") as DropDownList).Text))
                    +
                    TimeSpan.ParseExact((gridRow.Cells[2].FindControl("TimeTxt") as TextBox).Text,
                                        @"hh\:mm\:ss", CultureInfo.InvariantCulture);
            }
            else if (gridView == this.DailyScheduleGridView)
            {
                scheduleType = ScheduleType.Daily;
                descriptionCollIdx = 7;
                timezoneColIdx = 2;

                scheduleTimeZoneSpecific =
                    DateTime.MinValue
                    +
                    TimeSpan.ParseExact((gridRow.Cells[2].FindControl("TimeTxt") as TextBox).Text,
                                        @"hh\:mm\:ss", CultureInfo.InvariantCulture);
            }
            else
            {
                throw new Exception("Unexpected grid type");
            }

            bool enabled = (gridRow.Cells[0].FindControl("EnabledChckBx") as CheckBox).Checked;
            TimeZoneHelpers.SystemsTradingControlScheduleAction action =
                (TimeZoneHelpers.SystemsTradingControlScheduleAction)
                    Enum.Parse(typeof (TimeZoneHelpers.SystemsTradingControlScheduleAction),
                        (gridRow.Cells[descriptionCollIdx - 1].FindControl("ActionDropDown") as DropDownList)
                            .SelectedValue);
            TimeZoneHelpers.TimeZoneCode timeZone =
                (TimeZoneHelpers.TimeZoneCode)
                    Enum.Parse(typeof(TimeZoneHelpers.TimeZoneCode),
                        (gridRow.Cells[timezoneColIdx].FindControl("TimeZoneDropDown") as DropDownList)
                            .SelectedValue);

            string description = (gridRow.Cells[descriptionCollIdx].FindControl("DescTxt") as TextBox).Text;
            int scheduleId = -1;
            if (gridRow.Cells[0].FindControl("HiddenScheduleId") is HiddenField)
            {
                scheduleId = int.Parse((gridRow.Cells[0].FindControl("HiddenScheduleId") as HiddenField).Value);
            }

            return new ScheduleInfoBag(scheduleType, enabled, scheduleTimeZoneSpecific, timeZone, action, description, scheduleId);
        }

        //
        //Below are grids common functions
        //

        protected void GridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridView gridView = sender as GridView;
            GridViewRow gridRow = gridView.Rows[e.RowIndex];

            var scheduleInfo = this.ExtractScheduleInfoBag(gridRow, gridView);
            if (this.UpdateOne(scheduleInfo))
            {
                gridView.EditIndex = -1;
                PopulateSettings();
                //BEWARE! cannot use gridRow as it is already replaced by new row
                // Also row order can change - nned to highlight correct schedule
                foreach (GridViewRow gvr in (sender as GridView).Rows)
                {
                    var ctrl = gvr.Cells[0].FindControl("HiddenScheduleId");

                    if (ctrl is HiddenField && int.Parse((ctrl as HiddenField).Value) == scheduleInfo.ScheduleId)
                    {
                        //gvr.Style.Value += ";background:LightGreen";
                        gvr.CssClass = "changedRow";
                    }
                }
            }
            else
            {
                ValueChangedPanel.Visible = true;
                PopulateSettings();
            }
        }

        protected void DateValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime result;
            if (
                !DateTime.TryParseExact(args.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture,
                                        DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out result))
            {
                (source as CustomValidator).ErrorMessage = "Invalid date - should be in \'yyyy-MM-dd\' format";
                args.IsValid = false;
                return;
            }

            if (result < DateTime.UtcNow.Date)
            {
                (source as CustomValidator).ErrorMessage = "Invalid UTC date - should be in future";
                args.IsValid = false;
            }
        }

        protected void TimeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //BEWARE! - TimeSpan.ParseExact takes different format strings than DateTime.ParseExact - they are not interchangeable!
            TimeSpan result;
            if (
                !TimeSpan.TryParseExact(args.Value, @"hh\:mm\:ss", CultureInfo.InvariantCulture, out result))
            {
                (source as CustomValidator).ErrorMessage = "Invalid time - should be in \'hh:mm:ss\' format";
                args.IsValid = false;
                return;
            }
        }

        protected void GridView_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView gridView = sender as GridView;
            gridView.EditIndex = e.NewEditIndex;
            PopulateSettings();
        }

        protected void GridView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            (sender as GridView).EditIndex = -1;
            PopulateSettings();
            ValueChangedPanel.Visible = false;
        }

        protected void GridView_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            int? scheduleId = null;
            int rowIdx = -1;
            if (int.TryParse(e.CommandArgument as string, out rowIdx) && rowIdx >= 0)
            {
                scheduleId =
                    int.Parse(
                        ((sender as GridView).Rows[rowIdx].Cells[0].FindControl("HiddenScheduleId") as HiddenField)
                            .Value);
            }

             
            if (e.CommandName == "Enable" && scheduleId.HasValue)
            {
                this.DisableEnableOne(scheduleId.Value, true);
                PopulateSettings();
                (sender as GridView).Rows[rowIdx].CssClass = "changedRow";
            }
            else if (e.CommandName == "Disable")
            {
                this.DisableEnableOne(scheduleId.Value, false);
                PopulateSettings();
                (sender as GridView).Rows[rowIdx].CssClass = "changedRow";
            }
            else if (e.CommandName == "DeleteMe" && scheduleId.HasValue)
            {
                this.DeleteSettingEntry(scheduleId.Value);
                PopulateSettings();
            }
            else if (e.CommandName == "InsertMe" && Page.IsValid)
            {
                int newScheduleId = this.InsertOne(this.ExtractScheduleInfoBag((sender as GridView).FooterRow, sender as GridView));
                (sender as GridView).ShowFooter = false;
                PopulateSettings();
                if (newScheduleId >= 0)
                {
                    foreach (GridViewRow gvr in (sender as GridView).Rows)
                    {
                        var ctrl = gvr.Cells[0].FindControl("HiddenScheduleId");

                        if (ctrl is HiddenField && int.Parse((ctrl as HiddenField).Value) == newScheduleId)
                        {
                            gvr.CssClass = "changedRow";
                        }
                    }
                }
            }
            else if (e.CommandName == "CancelInsertMe")
            {
                (sender as GridView).ShowFooter = false;
                PopulateSettings();
            }
        }

        protected void GridView_DataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow gvr in (sender as GridView).Rows)
            {
                var ctrl = gvr.Cells[0].FindControl("DisableBtn");

                if (ctrl != null && ctrl.Visible)
                {
                    //gvr.Style.Value += ";background:LightCoral";
                    gvr.CssClass = "selectedRow";
                }
            }
        }

        protected void DayDropDown_OnPreRender(object sender, EventArgs e)
        {
            DropDownList dropDown = sender as DropDownList;

            dropDown.Items.Clear();

            foreach (var value in Enum.GetValues(typeof(DayOfWeek)))
            {
                dropDown.Items.Add(value.ToString());
            }

            //BEWARE! this can cause exception - databinding inside databinding
            //dropDown.DataSource = Enum.GetValues(typeof(DayOfWeek));
            //dropDown.DataBind();

            if (!string.IsNullOrEmpty(dropDown.ToolTip))
            {
                dropDown.SelectedValue = dropDown.ToolTip;
            }
        }

        protected void TimeZoneDropDown_OnPreRender(object sender, EventArgs e)
        {
            DropDownList dropDown = sender as DropDownList;

            dropDown.Items.Clear();

            foreach (KGTKillSwitchWeb.TimeZoneHelpers.TimeZoneCode value in TimeZoneHelpers.GetSortedTimeZoneCodes())
            {
                dropDown.Items.Add(new ListItem(TimeZoneHelpers.GetTimeZoneDescription(value), value.ToString()));
            }

            //BEWARE! this can cause exception - databinding inside databinding
            //dropDown.DataSource = Enum.GetValues(typeof(DayOfWeek));
            //dropDown.DataBind();

            if (!string.IsNullOrEmpty(dropDown.ToolTip))
            {
                dropDown.SelectedValue = dropDown.ToolTip.Trim();
            }
        }

        protected void ActionDropDown_OnPreRender(object sender, EventArgs e)
        {
            DropDownList dropDown = sender as DropDownList;

            dropDown.Items.Clear();

            foreach (var value in Enum.GetNames(typeof(TimeZoneHelpers.SystemsTradingControlScheduleAction)))
            {
                dropDown.Items.Add(value);
            }

            //BEWARE! this can cause exception - databinding inside databinding
            //dropDown.DataSource = Enum.GetValues(typeof(DayOfWeek));
            //dropDown.DataBind();

            if (!string.IsNullOrEmpty(dropDown.ToolTip))
            {
                dropDown.SelectedValue = dropDown.ToolTip;
            }
        }

        protected void ExactDateInsertDataBtn_Click(object sender, EventArgs e)
        {
            SpecificDatesScheduleGridView.ShowFooter = true;
            PopulateSettings();
        }

        protected void WeeklyInsertDataBtn_Click(object sender, EventArgs e)
        {
            WeeklyScheduleGridView.ShowFooter = true;
            PopulateSettings();
        }

        protected void DailyInsertDataBtn_Click(object sender, EventArgs e)
        {
            DailyScheduleGridView.ShowFooter = true;
            PopulateSettings();
        }

        
    }
}