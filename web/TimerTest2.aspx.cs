﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class TimerTest2 : PageWithNoControlState //System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["Action"] != null)
            {
                string action = Request.QueryString["action"];
                GlobalState.Action = action;

                if (Request.UrlReferrer != null)
                {
                    Response.Redirect(Request.UrlReferrer.AbsoluteUri, false);
                }
                else
                {
                    Response.Redirect("~/TimerTest2.aspx", false);
                }
            }

            lblAction.Text = GlobalState.Action;

            var postback = IsPostBack;

            if (Request.QueryString["blah"] != null)
            {
                if (Request.QueryString["blah"] == "2")
                    this.EditPanel.Visible = true;
            }

            if (!this.EditPanel.Visible)
            {
                this.LblTime.Text = System.DateTime.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.ffffff");
                PseudoBind();
                //this.Timer1.Enabled = true;
            }
            else
            {
                //this.Timer1.Enabled = false;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            //this.Timer1.Enabled = !this.EditPanel.Visible;
            AutorefreshContainer.AllowAutoRefresh = !this.EditPanel.Visible;

            //if (Request.QueryString.HasKeys())
            //{
            //    AutorefreshContainer.AllowAutoRefresh = false;
            //}

            base.OnPreRender(e);
        }

        private void PseudoBind()
        {
            this.txtEdit.Text = this.lblResult.Text;
        }

        protected void OnCommand(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                this.EditPanel.Visible = true;
                //this.Timer1.Enabled = false;

            }
            else if (e.CommandName == "Done")
            {
                this.EditPanel.Visible = false;
                this.lblResult.Text = this.txtEdit.Text;
                Response.Redirect("TimerTest2.aspx");
                //this.Timer1.Enabled = true;
            }
        }
    }
}