﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class SystemDetailsStripsGrid : System.Web.UI.UserControl
    {
        public TradingSystemType TradingSystemType { get { return (TradingSystemType)Enum.Parse(typeof(TradingSystemType), Request.QueryString["TradingSystemType"]); } }
        public VenueCounterparty VenueCounterparty { get { return (VenueCounterparty)Enum.Parse(typeof(VenueCounterparty), Request.QueryString["VenueCounterparty"]); } }

        private KGTKillSwitchWeb.AutorefreshContainer _ac;

        private KGTKillSwitchWeb.AutorefreshContainer AC
        {
            get
            {

                if (_ac == null && this.Parent.Parent.Parent != null)
                {
                    //HACK: so that we don't have to pass this throug. It's complicated to pass it to be here oninit
                    _ac = this.Parent.Parent.Parent.Parent.Parent as AutorefreshContainer;
                }
                return _ac;
            }
        }

        public void DisableAutoRefresh()
        {
            if(this.AC != null)
                this.AC.AllowAutoRefresh = false;
        }

        protected int? _tradingGroupId;

        //private List<VenueCrossPnlAndStatsGrid> _stripsGrids = new List<VenueCrossPnlAndStatsGrid>();
        //We cannot easily create the strips dynamicaly - as those would need to store view state and postback data between loads
        // otherwise page load without PopulateData(); would delete all data
        private const int _MAX_STRIPS_CNT = 5;
        private List<SystemDetailsStrip> StoredStripsGrids
        {
            get
            {
                return new List<SystemDetailsStrip>()
                    {
                        this.PnlGrid01,
                        this.PnlGrid02,
                        this.PnlGrid03,
                        this.PnlGrid04,
                        this.PnlGrid05
                    };
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //TODO - parse URL

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (!Utils.IsKgtUser(Session))
                {
                    Response.Redirect("~/NotAuthorized.aspx");
                    return;
                }
                if (!this.PerformQueryAction())
                {
                    SetStripsSettings();
                    PopulateData();
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        public string AdjustUrlQuery(string originalUrlQuery)
        {
            return string.Format("{0}{1}{2}", GetQueryStringWithNoAction(),
                string.IsNullOrEmpty(originalUrlQuery) ? string.Empty : "&", originalUrlQuery);
        }

        public string GetQueryStringWithNoAction()
        {
            return this.PnlGrid01.GetQueryStringWithNoAction();
        }

        private void RedirectToSelfWithNoQueryAction()
        {
            string query = GetQueryStringWithNoAction();
            if (Request.UrlReferrer != null)
            {
                Response.Redirect(new Uri(Request.UrlReferrer.AbsoluteUri).GetLeftPart(UriPartial.Path) + query, false);
            }
            else
            {
                Response.Redirect("~/SystemDetails.aspx" + query, false);
            }
        }

        private bool TryGetTradingSystemId(string s, out int id)
        {
            if (!int.TryParse(s, out id))
            {
                this.ReportError(string.Format("Invalid system id: [{0}]", s));
                return false;
            }

            return true;
        }

        private bool PerformQueryAction()
        {
            if (Request.QueryString.Keys.Count > 1)
            {
                if (Request.QueryString["EnableSystemById"] != null)
                {
                    int tradingSystemId;
                    if (TryGetTradingSystemId(Request.QueryString["EnableSystemById"], out tradingSystemId) && this.DisableEnableOne(tradingSystemId, true))
                    {
                        RedirectToSelfWithNoQueryAction();
                        return true;
                    }
                    else
                    {
                        this.AC.AllowAutoRefresh = false;
                        return false;
                    }
                }
                else if (Request.QueryString["DisableSystemById"] != null)
                {
                    int tradingSystemId;
                    if (TryGetTradingSystemId(Request.QueryString["DisableSystemById"], out tradingSystemId) && this.DisableEnableOne(tradingSystemId, false))
                    {
                        RedirectToSelfWithNoQueryAction();
                        return true;
                    }
                    else
                    {
                        this.AC.AllowAutoRefresh = false;
                        return false;
                    }
                }
                else if (Request.QueryString["DeleteSystemById"] != null)
                {
                    int tradingSystemId;
                    if (TryGetTradingSystemId(Request.QueryString["DeleteSystemById"], out tradingSystemId) && this.DeleteOne(tradingSystemId))
                    {
                        RedirectToSelfWithNoQueryAction();
                        return true;
                    }
                    else
                    {
                        this.AC.AllowAutoRefresh = false;
                        return false;
                    }
                }
                else if (Request.QueryString["ResetSystemById"] != null)
                {
                    int tradingSystemId;
                    if (TryGetTradingSystemId(Request.QueryString["ResetSystemById"], out tradingSystemId) && this.ResetOne(tradingSystemId))
                    {
                        RedirectToSelfWithNoQueryAction();
                        return true;
                    }
                    else
                    {
                        this.AC.AllowAutoRefresh = false;
                        return false;
                    }
                }
            }

            return false;
        }

        private bool DisableEnableOne(int tradingSystemId, bool enable)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        const string commandText = "[dbo].[VenueSystemDisableEnableOne_SP]";
                        SqlCommand command = new SqlCommand(commandText, connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@TradingSystemId", tradingSystemId);
                        command.Parameters.AddWithValue("@Enable", enable);

                        return command.ExecuteNonQuery() > 0;
                    }
                }
                catch (SqlException exc)
                {
                    this.ReportError(exc.Message.Replace(Environment.NewLine, "<BR />"));
                }
                catch (Exception exc)
                {
                    ReportException(exc);
                }
            }
            else
            {
                ReportError("Connection string missing");
            }

            return false;
        }

        private bool DeleteOne(int tradingSystemId)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        string commandText = "[dbo].[VenueSystemDeleteOne_SP]";

                        SqlCommand command = new SqlCommand(commandText, connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@TradingSystemId", tradingSystemId);

                        return command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception exc)
                {
                    ReportException(exc);
                }
            }
            else
            {
                ReportError("Connection string missing");
            }

            return false;
        }

        private bool ResetOne(int tradingSystemId)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        const string commandText = "[dbo].[VenueSystemReset_SP]";
                        SqlCommand command = new SqlCommand(commandText, connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@TradingSystemId", tradingSystemId);

                        return command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception exc)
                {
                    ReportException(exc);
                }
            }
            else
            {
                ReportError("Connection string missing");
            }

            return false;
        }

        private void SetStripsSettings()
        {
            string groupName = null;
            if (Utils.TryParseIntIntToNullable(Request.QueryString["GroupId"], out _tradingGroupId))
            {
                groupName = GlobalState.GetGroupName(_tradingGroupId.Value);
            }
            groupName = groupName ?? Server.HtmlEncode("<All groups - unfiltered>");
            groupName = Utils.GetUatPrefix(Session) + groupName;
            groupName = groupName.Replace(" ", "&nbsp;");
            string venueCtp = this.TradingSystemType.ToString().Replace(" ", "&nbsp;");

            GroupNameLiteral.Text = groupName;
            VenueTypeLiteral.Text = this.VenueCounterparty.ToString();
            TradingTypeLiteral.Text = venueCtp;
        }

        private void PopulateDataInternal()
        {
            int maxNumOfStrips = Math.Min(this.PnlGrid01.GetNumberOfStrips(), _MAX_STRIPS_CNT);
            string startSymbol = this.PnlGrid01.GetStartSybol();
            string endSymbol = this.PnlGrid01.GetEndSybol();

            DataTable dt = GetStatsDataTable();
            List<string> containedSymbols;
            List<DataTable> stripDataTables = AdjustAndSplitData(dt, maxNumOfStrips, out containedSymbols);

            for (int i = stripDataTables.Count; i < _MAX_STRIPS_CNT; i++)
            {
                stripDataTables.Add(null);
            }

            for (int dataIdx = 0; dataIdx < stripDataTables.Count; dataIdx++)
            {
                StoredStripsGrids[dataIdx].BindToDataTable(stripDataTables[dataIdx], this.GetQueryStringWithNoAction());
            }

            var availableSymbolsList =
                GlobalState.GetSymbolListSupportedByCounterparty(this.VenueCounterparty)
                           .Where(
                               s =>
                               string.Compare(s, startSymbol, StringComparison.InvariantCultureIgnoreCase) >= 0 &&
                               string.Compare(s, endSymbol, StringComparison.InvariantCultureIgnoreCase) <= 0 &&
                               !containedSymbols.Contains(s))
                           .OrderBy(s => s)
                           .Select(Utils.RemoveSlashFromSymbol).ToList();
            this.SymbolsRepeater.DataSource = availableSymbolsList;
            this.SymbolsRepeater.DataBind();
            this.SymbolsRepeater.Visible = availableSymbolsList.Any();
            this.AllUnconfiguredLink.Visible = availableSymbolsList.Any();
            this.AllUnconfiguredLink.HRef =
                string.Format(
                    "~/InsertEditSystem.aspx{0}&Action=Insert&SymbolsNum={1}&Symbols={2}",
                    GetQueryStringWithNoAction(), availableSymbolsList.Count, string.Join(",", availableSymbolsList));
        }

        private void PopulateData()
        {
            try
            {
                this.PopulateDataInternal();
            }
            catch (Exception e)
            {
                ReportException(e);
            }
        }

        DataTable GetStatsDataTable()
        {
            string counterpartyCode = this.VenueCounterparty.ToString();
            string beginWithSymbol = this.PnlGrid01.GetStartSybol();
            string endWithSymbol = this.PnlGrid01.GetEndSybol();

            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        string commandText = string.Format("[dbo].[GetVenue{0}SystemSettingsAndStats_SP]", this.TradingSystemType);

                        SqlCommand command = new SqlCommand(commandText, connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@CounterpartyCode", counterpartyCode);
                        if (_tradingGroupId.HasValue)
                            command.Parameters.AddWithValue("@TradingGroupId", _tradingGroupId.Value);
                        command.Parameters.AddWithValue("@BeginWithSymbol", beginWithSymbol);
                        command.Parameters.AddWithValue("@EndWithSymbol", endWithSymbol);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            DataTable dt = new DataTable();
                            dt.Load(reader);
                            return dt;
                        }
                    }
                }
                catch (Exception exc)
                {
                    ReportException(exc);
                }
            }
            else
            {
                this.ReportError("Connection string is missing");
            }

            return null;
        }

        private List<DataTable> AdjustAndSplitData(DataTable dt, int maxNumOfTables,
                                                   out List<string> listOfContainedSymbols)
        {
            //we need upper boundary of numberof rows in one strip
            int minRowsInSingleTable = (int)Math.Ceiling(dt.Rows.Count / (double)maxNumOfTables);
            List<DataTable> stripDataTables = new List<DataTable>();
            DataTable copyTable = null;

            foreach (DataColumn c in dt.Columns)
                c.AllowDBNull = true; // Allow Nulls in all columns

            listOfContainedSymbols = new List<string>();
            dt.Columns.Add("SymbolSubrows", typeof(int));

            string currentSymbol = "none";
            //List<int> separatorRowsIndexes = new List<int>();
            DataRow lastGroupStartingRow = null;

            int subRowsCnt = 0;
            int currentSubtableRowIdx = 0;
            for (int currentRowIdx = 0; currentRowIdx < dt.Rows.Count; currentRowIdx++, currentSubtableRowIdx++)
            {
                DataRow row = dt.Rows[currentRowIdx];
                subRowsCnt++;

                bool isNewSymbol = (string)row["Symbol"] != currentSymbol;

                if (isNewSymbol)
                {
                    listOfContainedSymbols.Add((string)row["Symbol"]);
                    currentSymbol = (string)row["Symbol"];

                    if (currentSubtableRowIdx == 0 || currentSubtableRowIdx > minRowsInSingleTable)
                    {
                        copyTable = new DataTable();
                        // Clone the structure of the table.
                        copyTable = dt.Clone();
                        // Add the new DataTable to the list.
                        stripDataTables.Add(copyTable);

                        currentSubtableRowIdx = 1;
                    }
                    else
                    {
                        copyTable.Rows.Add(copyTable.NewRow());
                    }

                }
                else
                {
                    row["SymbolSubrows"] = DBNull.Value;
                }

                copyTable.ImportRow(row);

                if (isNewSymbol)
                {
                    if (lastGroupStartingRow != null)
                    {
                        lastGroupStartingRow["SymbolSubrows"] = subRowsCnt;
                    }
                    lastGroupStartingRow = copyTable.Rows[copyTable.Rows.Count - 1];
                    subRowsCnt = 0;
                }

            }
            if (lastGroupStartingRow != null)
            {
                lastGroupStartingRow["SymbolSubrows"] = subRowsCnt + 1;
            }

            return stripDataTables;
        }


        private void ReportException(Exception e)
        {
            this.AC.ReportException(e);
            this.AC.AllowAutoRefresh = false;
        }

        private void ReportError(string error)
        {
            this.AC.ReportError(error);
            this.AC.AllowAutoRefresh = false;
        }

        private string addingAllText = "All unconfigured symbols";
        

    }
}