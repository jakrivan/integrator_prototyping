//autorefresh

//function AutoRefreshWithDefaultInterval()
//{
//    AutoRefresh(5);
//}

//function AutoRefresh(t)
//{
//    setTimeout("location.reload(true);", t);
//}

//
//
// Function to work with modal div dialogs
//

function closeDialog(dialog)
{
    document.getElementById("freezeDiv").className = "";
    dialog.className = "DialogHide";
}

function openDialog(dialog) 
{

    document.getElementById("freezeDiv").className = "freeze";
    dialog.className = "DialogShow";
}



var offsetX = 0;
var offsetY = 0;
var objToMove;

function shiftTo()
{
    objToMove.style.pixelLeft = event.clientX - 100 + offsetX;
    objToMove.style.pixelTop = event.clientY - 18 + offsetY;

}

function SetMovable(obj)
{
    
    offsetX = document.documentElement.scrollLeft;
    offsetY = document.documentElement.scrollTop;  
    obj.onmousemove = shiftTo;
    objToMove = obj;
}

function SetStatic(obj)
{
    obj.onmousemove = null;
}