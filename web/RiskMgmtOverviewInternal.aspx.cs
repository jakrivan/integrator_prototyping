﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class KGTRiskMgtmtOverviewInternal : PageWithNoControlState // System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (!Utils.IsKgtUser(Session))
                {
                    Response.Redirect("~/RiskMgmtOverview.aspx");
                    return;
                }

                DateTime today = DateTime.UtcNow.Date; //DateTime.UtcNow.AddDays(0).Date;
                decimal? totalNop;

                UpdateRiskMgmtState();
                totalNop = GetTotalNop(today);
                UpdateTotalNop(totalNop);
                UpdateCrossPoints(today);
                UpdateCcyNops(today, totalNop);
                UpdateCounterpartyNops(today);
                UpdateIgnoredOrders(today);
                UpdateUnexpectedExecutions();
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        protected string FormatNumber(object dbObject)
        {
            if (dbObject == DBNull.Value)
                return 0m.ToString("n2");
            else if (dbObject is decimal)
                return ((decimal)dbObject).ToString("n2");
            else
                return "?";
        }

        protected string FormatNumberMilions(object dbObject)
        {
            if (dbObject == DBNull.Value)
                return string.Empty;
            else if (dbObject is decimal)
                return (((decimal) dbObject)/MILLION).ToString("n2");
            else
                return "?";
        }

        protected string FormatNumberMilionsOneDecimal(object dbObject)
        {
            if (dbObject == DBNull.Value)
                return string.Empty;
            else if (dbObject is decimal)
                return (((decimal)dbObject) / MILLION).ToString("n1");
            else
                return "?";
        }

        protected string FormatNumberPercents(object dbObject)
        {
            if (dbObject == DBNull.Value)
                return string.Empty;
            else if (dbObject is decimal)
                return (((decimal) dbObject)*100m).ToString("n1");
            else
                return "?";
        }

        private decimal? GetTotalNop(DateTime day)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetNopAbsTotal_Daily_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Day", day.Date);

                        object nopObject = command.ExecuteScalar();

                        if (nopObject == DBNull.Value)
                            return 0m;
                        else if (nopObject is decimal)
                            return ((decimal) nopObject);
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }

            return null;
        }

        private const decimal MILLION = 1000000m;

        private void UpdateTotalNop(decimal? totalNop)
        {
            if (totalNop.HasValue)
            {
                LtrlNopTotal.Text = string.Format("{0:N2} M", (totalNop.Value / MILLION));
            }
            else
            {
                LtrlNopTotal.Text = "?";
            }
        }

        private void UpdateCounterpartyNops(DateTime day)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetStatisticsPerCounterparty_DailyCached_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        //command.Parameters.AddWithValue("@Day", day.Date);


                        SqlDataReader reader = command.ExecuteReader();

                        CounterpartyNopsGridView.DataSource = reader;
                        CounterpartyNopsGridView.DataBind();
                        //(this.NopLoginView.FindControl("NopGridView") as GridView).DataSource = reader;
                        //(this.NopLoginView.FindControl("NopGridView") as GridView).DataBind();
                        reader.Close();
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }
        }

        private void UpdateCcyNops(DateTime day, decimal? totalNop)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetNopsPolSidedPerCurrency_Daily_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Day", day.Date); ;


                        SqlDataReader reader = command.ExecuteReader();

                        CCYPositionsNops.DataSource = reader;
                        CCYPositionsNops.DataBind();
                        //(this.NopLoginView.FindControl("NopGridView") as GridView).DataSource = reader;
                        //(this.NopLoginView.FindControl("NopGridView") as GridView).DataBind();
                        reader.Close();

                        if (CCYPositionsNops.FooterRow != null)
                        {
                            CCYPositionsNops.FooterRow.Cells[0].Text = "\u03A3 (abs)";

                            CCYPositionsNops.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                            if (totalNop.HasValue)
                            {
                                CCYPositionsNops.FooterRow.Cells[2].Text = totalNop.Value.ToString("n2");
                            }
                            else
                            {
                                CCYPositionsNops.FooterRow.Cells[2].Text = "?";
                            }
                        }
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }
        }

        private void UpdateCrossPoints(DateTime day)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetStatisticsPerPair_Daily_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Day", day.Date);


                        SqlDataReader reader = command.ExecuteReader();

                        CrossPointsGridView.DataSource = reader;
                        CrossPointsGridView.DataBind();
                        //(this.NopLoginView.FindControl("NopGridView") as GridView).DataSource = reader;
                        //(this.NopLoginView.FindControl("NopGridView") as GridView).DataBind();
                        reader.Close();
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }
        }

        private void UpdateIgnoredOrders(DateTime day)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetIgnoredOrders_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@StartTime", day.Date);
                        command.Parameters.AddWithValue("@EndTime", day.Date.AddDays(1));


                        SqlDataReader reader = command.ExecuteReader();

                        IgnoredOrdersGridView.DataSource = reader;
                        IgnoredOrdersGridView.DataBind();
                        reader.Close();

                        IgnoredOrdersHeader.Visible = IgnoredOrdersGridView.Rows.Count > 0;
                        NoIgnoredOrdersHeader.Visible = IgnoredOrdersGridView.Rows.Count == 0;
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }
        }

        private void UpdateUnexpectedExecutions()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetTodayUnexpectedExecutions_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;

                        SqlDataReader reader = command.ExecuteReader();

                        UnexpectedDealsGridView.DataSource = reader;
                        UnexpectedDealsGridView.DataBind();
                        reader.Close();

                        UnexpectedDealsHeader.Visible = UnexpectedDealsGridView.Rows.Count > 0;
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            //LblUpdatedOn.Text = "Last Updated: " + DateTime.Now.ToLongTimeString();
        }

        private bool? GetIsSwitchEnabled(string switchName)
        {
             ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command =
                            new SqlCommand("SELECT [dbo].[GetExternalOrdersTransmissionSwitchState](@SwitchName)",
                                           connection);
                        command.Parameters.Add("@SwitchName", SqlDbType.NVarChar).Value = switchName;
                        object isTradingOffResult = command.ExecuteScalar();
                        if (isTradingOffResult != null)
                        {
                            return (bool) isTradingOffResult;
                        }
                    }
                }
                catch (Exception exc)
                { }
            }

            return null;
        }

        private bool? ReadGoFlatState()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetSystemsTradingControl_SP]", connection);

                        object boolObject = command.ExecuteScalar();

                        if (boolObject == DBNull.Value || !(boolObject is bool))
                            return null;
                        else
                            return (bool)boolObject;
                    }
                }
                catch (Exception exc)
                { }
            }

            return null;
        }


        private bool SetGoFlatState(bool goFlat)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[FlipSystemsTradingControl_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@GoFlatOnly", goFlat);

                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                catch (SqlException exc)
                {
                    this.AC.ReportError(exc.Message.Replace(Environment.NewLine, "<BR />"));
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }

            return false;
        }

        private void UpdateTimeAndReasonForIntegratorSwitch()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetRecentExternalOrdersTransmissionControlState_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@SwitchName", SqlDbType.NVarChar).Value = Utils.IntegratorSwitchName;

                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            bool enabled = reader.GetBoolean(reader.GetOrdinal("TransmissionEnabled"));
                            if (!enabled)
                            {
                                SwitchDisableTimeLiteral.Text =
                                    reader.GetDateTime(reader.GetOrdinal("Changed"))
                                          .ToString("yyyy-MM-dd HH:mm:ss.fff UTC");

                                if (reader.IsDBNull(reader.GetOrdinal("Reason")))
                                {
                                    SwitchDisableReasonLiteral.Text = Server.HtmlEncode("<Not Specified>");
                                }
                                else
                                {
                                    SwitchDisableReasonLiteral.Text = Server.HtmlEncode(reader.GetString(reader.GetOrdinal("Reason")));
                                }
                                break;
                            }
                        }

                        reader.Close();
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }
        }

        private void UpdateRiskMgmtState()
        {
            bool isTradingEnabled;

            bool? kgtSwitchState = this.GetIsSwitchEnabled(Utils.KgtSwitchName);
            bool? pbSwitchState = this.GetIsSwitchEnabled(Utils.PrimebrokerSwitchName);
            bool? integratorSwitchState = this.GetIsSwitchEnabled(Utils.IntegratorSwitchName);
            bool? goFlatOnlyOn = this.ReadGoFlatState();

            if (!(kgtSwitchState.HasValue && pbSwitchState.HasValue && integratorSwitchState.HasValue && goFlatOnlyOn.HasValue))
            {
                this.ErrorPanel.Visible = true;
                return;
            }

            isTradingEnabled = kgtSwitchState.Value;


            TradingOnImageButton2.Visible = kgtSwitchState.Value;
            TradingOffImageButton2.Visible = !kgtSwitchState.Value;

            PrimebrokerTradingOnImage.Visible = pbSwitchState.Value;
            PrimebrokerTradingOffImage.Visible = !pbSwitchState.Value;

            IntegratorTradingOnImage.Visible = integratorSwitchState.Value;
            IntegratorTradingOffImageButton.Visible = !integratorSwitchState.Value;
            cellErrorDetails.Visible = !integratorSwitchState.Value;

            GoFlatOnlyOffImageButton.Visible = !goFlatOnlyOn.Value;
            GoFlatOnlyOffLiteral.Visible = !goFlatOnlyOn.Value;
            GoFlatOnlyOnImageButton.Visible = goFlatOnlyOn.Value;
            GoFlatOnlyOnLiteral.Visible = goFlatOnlyOn.Value;

            if (!integratorSwitchState.Value)
            {
                UpdateTimeAndReasonForIntegratorSwitch();
            }

            if (Request.QueryString["Action"] != null)
            {
                //allow it after redirect
                this.AC.AllowAutoRefresh = false;
                if (Request.QueryString["Action"].Equals("turnoff", StringComparison.OrdinalIgnoreCase))
                {
                    if (!isTradingEnabled)
                    {
                        this.StateChangedErrorPanel.Visible = true;
                        this.AC.AllowAutoRefresh = false;
                    }
                    else
                    {
                        if (!DoSwitchAction(false, Utils.GetKillSwitchBitName(Session)))
                        {
                            this.ErrorPanel.Visible = true;
                            this.AC.AllowAutoRefresh = false;
                            return;
                        }
                        isTradingEnabled = false;

                        if (Request.UrlReferrer != null)
                        {
                            Response.Redirect(new Uri(Request.UrlReferrer.AbsoluteUri).GetLeftPart(UriPartial.Path), false);
                        }
                        else
                        {
                            Response.Redirect("~/RiskMgmtOverviewInternal.aspx", false);
                        }
                    }
                }
                else if (
                    Request.QueryString["Action"].Equals("turnon", StringComparison.OrdinalIgnoreCase) &&
                    Utils.GetIsAdmin(Session))
                {
                    if (isTradingEnabled)
                    {
                        this.StateChangedErrorPanel.Visible = true;
                        this.AC.AllowAutoRefresh = false;
                    }
                    else
                    {
                        if (!DoSwitchAction(true, Utils.GetKillSwitchBitName(Session)))
                        {
                            this.ErrorPanel.Visible = true;
                            this.AC.AllowAutoRefresh = false;
                            return;
                        }
                        isTradingEnabled = true;
                        if (Request.UrlReferrer != null)
                        {
                            Response.Redirect(new Uri(Request.UrlReferrer.AbsoluteUri).GetLeftPart(UriPartial.Path), false);
                        }
                        else
                        {
                            Response.Redirect("~/RiskMgmtOverviewInternal.aspx", false);
                        }
                    }
                }
                else if (
                    Request.QueryString["Action"].Equals("goflaton", StringComparison.OrdinalIgnoreCase) &&
                    Utils.GetIsAdmin(Session))
                {
                    if (goFlatOnlyOn.Value)
                    {
                        this.StateChangedErrorPanel.Visible = true;
                        this.AC.AllowAutoRefresh = false;
                    }
                    else
                    {
                        if (!SetGoFlatState(true))
                        {
                            this.ErrorPanel.Visible = true;
                            this.AC.AllowAutoRefresh = false;
                            return;
                        }
                        if (Request.UrlReferrer != null)
                        {
                            Response.Redirect(new Uri(Request.UrlReferrer.AbsoluteUri).GetLeftPart(UriPartial.Path), false);
                        }
                        else
                        {
                            Response.Redirect("~/RiskMgmtOverviewInternal.aspx", false);
                        }
                    }
                }
                else if (
                    Request.QueryString["Action"].Equals("goflatoff", StringComparison.OrdinalIgnoreCase) &&
                    Utils.GetIsAdmin(Session))
                {
                    if (!goFlatOnlyOn.Value)
                    {
                        this.StateChangedErrorPanel.Visible = true;
                        this.AC.AllowAutoRefresh = false;
                    }
                    else
                    {
                        if (!SetGoFlatState(false))
                        {
                            this.ErrorPanel.Visible = true;
                            this.AC.AllowAutoRefresh = false;
                            return;
                        }
                        if (Request.UrlReferrer != null)
                        {
                            Response.Redirect(new Uri(Request.UrlReferrer.AbsoluteUri).GetLeftPart(UriPartial.Path), false);
                        }
                        else
                        {
                            Response.Redirect("~/RiskMgmtOverviewInternal.aspx", false);
                        }
                    }
                }
                else if (
                    Request.QueryString["Action"].Equals("internalturnon", StringComparison.OrdinalIgnoreCase) &&
                    Utils.GetIsAdmin(Session))
                {
                    if (!DoSwitchAction(true, Utils.IntegratorSwitchName))
                    {
                        this.AC.AllowAutoRefresh = false;
                        ErrorPanel.Visible = true;
                    }
                    if (Request.UrlReferrer != null)
                    {
                        Response.Redirect(Request.UrlReferrer.AbsoluteUri, false);
                    }
                    else
                    {
                        Response.Redirect("~/RiskMgmtOverviewInternal.aspx", false);
                    }
                }
            }
        }

        private bool DoSwitchAction(bool enable, string switchName)
        {
            string spName = enable
                                ? "[dbo].[EnableExternalOrdersTransmission_SP]"
                                : "[dbo].[DisableExternalOrdersTransmission_SP]";

            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        string address = Request.UserHostAddress;
                        address = string.IsNullOrEmpty(address) ? "UNKNOWN" : address;
                        if (Session["HostName"] != null)
                        {
                            address += " (" + Session["HostName"] + ")";
                        }

                        SqlCommand command =
                            new SqlCommand(spName, connection);
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@SwitchName", SqlDbType.NVarChar).Value = switchName;
                        command.Parameters.AddWithValue("@ChangedBy", HttpContext.Current.User.Identity.Name);
                        command.Parameters.AddWithValue("@ChangedFrom", address);

                        command.ExecuteNonQuery();
                        return true;
                    }
                }
                catch (SqlException exc)
                {
                    this.AC.ReportError(exc.Message.Replace(Environment.NewLine, "<BR />"));
                }
                catch (Exception exc)
                { }
            }

            return false;
        }

        protected void CrossPointsGridView_DataBound(object sender, EventArgs e)
        {
            if (!Utils.IsKgtUser(Session))
            {
                CrossPointsGridView.Columns[2].Visible = false;
                CrossPointsGridView.Columns[3].Visible = false;
                CrossPointsGridView.ShowFooter = false;
            }
            else
            {
                if (CrossPointsGridView.FooterRow != null)
                {
                    CrossPointsGridView.FooterRow.Cells[0].Text = "\u03A3";

                    decimal totalPnl = 0m;
                    decimal totalVol = 0m;
                    int totalDeals = 0;

                    for (int rowIdx = 0; rowIdx < CrossPointsGridView.Rows.Count; rowIdx++)
                    {
                        decimal val;
                        if (Decimal.TryParse(CrossPointsGridView.Rows[rowIdx].Cells[4].Text, out val))
                        {
                            //CrossPointsGridView.Rows[rowIdx].Cells[4].BackColor = GetColorForNumber(val);
                            totalPnl += val;
                        }

                        Label volumeLabel =
                            CrossPointsGridView.Rows[rowIdx].Cells[6].FindControl("VolumeUsdLabel") as Label;
                        if (volumeLabel != null && Decimal.TryParse(volumeLabel.ToolTip, out val))
                        {
                            totalVol += val;
                        }

                        int intVal;
                        if (int.TryParse(CrossPointsGridView.Rows[rowIdx].Cells[5].Text, out intVal))
                        {
                            totalDeals += intVal;
                        }
                    }

                    CrossPointsGridView.FooterRow.Cells[4].Text = FormatNumber(totalPnl);
                    CrossPointsGridView.FooterRow.Cells[5].Text = totalDeals.ToString();
                    CrossPointsGridView.FooterRow.Cells[7].Text = (totalVol/MILLION).ToString("n1");
                }
            }
        }

        protected void CounterpartyNops_DataBound(object sender, EventArgs e)
        {
            int column_lbl = 0;
            int column_position = 1;
            int column_deals = 2;
            int column_ctpRejections = 3;
            int column_ctpRejRate = 4;
            int column_kgtRejections = 5;
            int column_kgtRejRate = 6;

            int column_avgDeal = 7;
            int column_volume = 8;
            //int column_volumeshare = 7;
            int column_comm_pb = 10;
            int column_comm_cpt = 11;
            int column_comm_sum = 12;


            if (CounterpartyNopsGridView.FooterRow != null)
            {
                CounterpartyNopsGridView.FooterRow.Cells[column_lbl].Text = "\u03A3";

                int totalCtpRejections = 0;
                int totalKgtRejections = 0;
                int totalDeals = 0;
                decimal totalVolume = 0m;
                decimal totalPosition = 0m;
                decimal commPb = 0;
                decimal commCpt = 0;
                for (int rowIdx = 0; rowIdx < CounterpartyNopsGridView.Rows.Count; rowIdx++)
                {
                    int val;
                    if (int.TryParse(CounterpartyNopsGridView.Rows[rowIdx].Cells[column_deals].Text, out val))
                    {
                        totalDeals += val;
                    }


                    //ItemStyle-CssClass='<%# Utils.GetDealActionCellStyle(Container.DataItem, Utils.DealAction.KgtDeal) %>'
                    //CounterpartyNopsGridView.Rows[rowIdx].Cells[column_deals].CssClass = "RecentDealChange";
                    //CounterpartyNopsGridView.Rows[rowIdx].Cells[column_deals].CssClass = Utils.GetDealActionCellStyle(CounterpartyNopsGridView.Rows[rowIdx].DataItem, Utils.DealAction.KgtDeal);


                    if (int.TryParse(CounterpartyNopsGridView.Rows[rowIdx].Cells[column_ctpRejections].Text, out val))
                    {
                        totalCtpRejections += val;
                    }

                    if (int.TryParse(CounterpartyNopsGridView.Rows[rowIdx].Cells[column_kgtRejections].Text, out val))
                    {
                        totalKgtRejections += val;
                    }

                    decimal decVal;
                    Label volumeLabel =
                        CounterpartyNopsGridView.Rows[rowIdx].Cells[column_volume].FindControl("VolumeUsdLabel") as Label;
                    if (volumeLabel != null && Decimal.TryParse(volumeLabel.ToolTip, out decVal))
                    {
                        totalVolume += decVal;
                    }

                    if (Decimal.TryParse(CounterpartyNopsGridView.Rows[rowIdx].Cells[column_position].Text, out decVal))
                    {
                        totalPosition += decVal;
                    }

                    if (Decimal.TryParse(CounterpartyNopsGridView.Rows[rowIdx].Cells[column_comm_pb].Text, out decVal))
                    {
                        commPb += decVal;
                    }

                    if (Decimal.TryParse(CounterpartyNopsGridView.Rows[rowIdx].Cells[column_comm_cpt].Text, out decVal))
                    {
                        commCpt += decVal;
                    }
                }

                GlobalState.TotalDailyVolume = totalVolume;

                if (totalCtpRejections > 0)
                {
                    CounterpartyNopsGridView.FooterRow.Cells[column_ctpRejections].Text = totalCtpRejections.ToString();
                    CounterpartyNopsGridView.FooterRow.Cells[column_ctpRejRate].Text =
                        (totalCtpRejections*100m/(totalCtpRejections + totalDeals)).ToString("n1");
                }

                if (totalKgtRejections > 0)
                {
                    CounterpartyNopsGridView.FooterRow.Cells[column_kgtRejections].Text = totalKgtRejections.ToString();
                    CounterpartyNopsGridView.FooterRow.Cells[column_kgtRejRate].Text =
                        (totalKgtRejections * 100m / (totalKgtRejections + totalDeals)).ToString("n1");
                }

                if (totalDeals > 0)
                {
                    CounterpartyNopsGridView.FooterRow.Cells[column_position].Text = totalPosition.ToString("n2");
                    CounterpartyNopsGridView.FooterRow.Cells[column_deals].Text = totalDeals.ToString();

                    CounterpartyNopsGridView.FooterRow.Cells[column_volume].Text = (totalVolume / MILLION).ToString("n1");


                    CounterpartyNopsGridView.FooterRow.Cells[column_avgDeal].Text = ((totalVolume / totalDeals) / MILLION).ToString("n2");

                    CounterpartyNopsGridView.FooterRow.Cells[column_comm_pb].Text = commPb.ToString("n2");
                    CounterpartyNopsGridView.FooterRow.Cells[column_comm_cpt].Text = commCpt.ToString("n2");
                    CounterpartyNopsGridView.FooterRow.Cells[column_comm_sum].Text = (commPb + commCpt).ToString("n2");
                }
            }
        }

        protected void CounterpartyNopsGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            int column_deals = 2;
            int column_ctpRejections = 3;
            int column_kgtRejections = 5;
            
            e.Row.Cells[column_deals].CssClass = Utils.GetDealActionCellStyle(e.Row.DataItem, Utils.DealAction.KgtDeal);
            e.Row.Cells[column_ctpRejections].CssClass = Utils.GetDealActionCellStyle(e.Row.DataItem, Utils.DealAction.CptRejection);
            e.Row.Cells[column_kgtRejections].CssClass = Utils.GetDealActionCellStyle(e.Row.DataItem, Utils.DealAction.KgtRejection);
        }

        protected void CrossPointsGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            int column_deals = 5;

            e.Row.Cells[column_deals].CssClass = Utils.GetDealActionCellStyle(e.Row.DataItem, Utils.DealAction.KgtDeal);
        }
    }
}