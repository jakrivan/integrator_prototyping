﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TimerTest.aspx.cs" Inherits="KGTKillSwitchWeb.TimerTest" %>

<%@ Register Src="~/VenueSettingsWithPnl.ascx" TagPrefix="uc1" TagName="VenueSettingsWithPnl" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Venue cross systems settings with PnL info TEST</title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    <%--<script type="text/javascript">
        
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);*/

        function callUpdate() {
            alert("Will postback");
            __doPostBack('<%=UpdatePanel1.ClientID%>', '');
        }
        

        function EndRequestHandler(sender, args) {
            if (args.get_error() != undefined) {
                $get("resultSpan").innerText = "..ERROR!..: " + errorMessage;
            }
            {
                $get("resultSpan").innerText = "..OK!..";
            }
        }
    </script>--%>
    
    <%--<script type="text/javascript">
        function RequestAsyncUpdate() {
            __doPostBack('<%=UpdatePanel1.ClientID%>', '');
        }
    </script>--%>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        
        <script type="text/javascript">
            function RequestAsyncUpdate() {
                __doPostBack('<%=UpdatePanel1.ClientID%>', '');
        }
        </script>

        <asp:Panel ID="Panel1" runat="server" style="text-align: right; padding: 0px 10px">
            <asp:Label ID="Label1" runat="server" style="text-align:right;" Text="All data shown for today (UTC). Last updated:"></asp:Label>&nbsp;<span id="timeStampSpan"></span>
            &nbsp;
            <span>
                <progress id="progressBar" value="0" max="9" />
            </span>
            <div id="updateErrorDiv" style="display: none">
                <span style="color: red; font-weight: bold" id="updateErrorSpan"></span>
            </div>
            <br/>
            <asp:LinkButton ID="LinkBtnUpdate" runat="server">Perform Full Refresh Now</asp:LinkButton>
        </asp:Panel>
        

    
        <%--<uc1:VenueSettingsWithPnl runat="server" ID="VenueSettingsWithPnl" />--%>

        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Path="Scripts/KGTScripts.js"/>
            </Scripts>
        </asp:ScriptManager>
        
            <%--<asp:Timer ID="Timer1" runat="server" Interval="1000" Enabled="False">
            </asp:Timer>--%>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />--%>
        </Triggers>
        <ContentTemplate>
            
            
            
            <asp:PlaceHolder runat="server" ID="RefreshEnabledPlaceholder">
                <input type="hidden" id="refreshIntervalField" value="1000"/>
            </asp:PlaceHolder>
            
            
            

            Last request result: <span id="resultSpan"></span>
            <br/>
            <br/>

            <input type="hidden" id="blah1" value="balh2"/>
<%--            <asp:HiddenField runat="server" ID="Blah1OnServer" Value="blah2fromserver"/>--%>
            
            <%--<div style="background: black; width: 100px; height: 10px" id="somediv" onclick="__doPostBack('<%=UpdatePanel1.ClientID%>', '');">
                </div>--%>
            
            <div style="background: black; width: 100px; height: 10px" id="somediv" onclick="callUpdate();">
                Request For Success Update
            </div>
            <br />

            <asp:Button runat="server" Text="Request for failed update" OnClick="ErrorProcessClick_Handler"/>

            <asp:Label runat="server" ID="LblTime"></asp:Label>
        
            <asp:LinkButton runat="server" Text="Update" OnCommand="OnCommand" CommandName="Edit"></asp:LinkButton>
            
            <asp:Panel runat="server" ID="EditPanel" Visible="False">
                <asp:TextBox runat="server" ID="txtEdit"></asp:TextBox>
                <asp:LinkButton ID="LinkButton1" runat="server" Text="Done" OnCommand="OnCommand" CommandName="Done"></asp:LinkButton>
            </asp:Panel>
            
            <asp:Label runat="server" ID="lblResult"></asp:Label>
            
            </ContentTemplate>
    </asp:UpdatePanel>
            

    </div>
    </form>
</body>
</html>
