﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace KGTKillSwitchWeb
{
    public static class GlobalState
    {
        public static string Action { get; set; }

        private static decimal _totalDailyVolume;
        private static DateTime _totalDailyVolumeLastUpdatedUtc;
        public static decimal TotalDailyVolume
        {
            get
            {
                if (DateTime.UtcNow - _totalDailyVolumeLastUpdatedUtc > TimeSpan.FromSeconds(2))
                {
                    ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetTotalDailyVolume_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;

                        object volumeObject = command.ExecuteScalar();

                        if (volumeObject == DBNull.Value)
                            _totalDailyVolume = 0m;
                        else
                            _totalDailyVolume = ((decimal)volumeObject);

                        _totalDailyVolumeLastUpdatedUtc = DateTime.UtcNow;
                    }
                }

                return _totalDailyVolume;
            }

            set
            {
                _totalDailyVolume = value;
                _totalDailyVolumeLastUpdatedUtc = DateTime.UtcNow;
            }
        }

        private static string AddSlashToSymbol(string symbol)
        {
            if (symbol.Length == 6)
            {
                symbol = symbol.Substring(0, 3) + '/' + symbol.Substring(3, 3);
            }

            return symbol;
        }

        private static List<string> _lm2SupportedSymbols = new List<string>()
        {
            "AUD/CAD",
            "AUD/CHF",
            "AUD/JPY",
            "AUD/NZD",
            "AUD/USD",
            "CAD/CHF",
            "CAD/JPY",
            "CHF/JPY",
            "EUR/AUD",
            "EUR/CAD",
            "EUR/DKK",
            "EUR/GBP",
            "EUR/HKD",
            "EUR/CHF",
            "EUR/JPY",
            "EUR/MXN",
            "EUR/NOK",
            "EUR/NZD",
            "EUR/SEK",
            "EUR/SGD",
            "EUR/USD",
            "EUR/ZAR",
            "GBP/AUD",
            "GBP/CAD",
            "GBP/DKK",
            "GBP/HKD",
            "GBP/CHF",
            "GBP/JPY",
            "GBP/MXN",
            "GBP/NOK",
            "GBP/NZD",
            "GBP/SEK",
            "GBP/SGD",
            "GBP/USD",
            "GBP/ZAR",
            "NZD/CAD",
            "NZD/CHF",
            "NZD/JPY",
            "NZD/SGD",
            "NZD/USD",
            "USD/CAD",
            "USD/DKK",
            "USD/HKD",
            "USD/CHF",
            "USD/JPY",
            "USD/MXN",
            "USD/NOK",
            "USD/SEK",
            "USD/SGD",
            "USD/ZAR"
        };

        static GlobalState()
        {
            _symbolsSupportedByCtp[(int) VenueCounterparty.LM2] = _lm2SupportedSymbols;
        }

        private static List<string>[] _symbolsSupportedByCtp = new List<string>[Enum.GetValues(typeof(VenueCounterparty)).Length];

        public static List<string> GetSymbolListSupportedByCounterparty(VenueCounterparty venueCounterparty)
        {
            if (_symbolsSupportedByCtp[(int) venueCounterparty] == null)
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["DCBackend"];
                using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                {
                    connection.Open();
                    string counterpartyTypeCode = Utils.VenueTypeString(venueCounterparty);
                    SqlCommand command = new SqlCommand("[dbo].[GetSupportedSymbolsForCounterparty_SP]", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@TradingTargetCode", counterpartyTypeCode);

                    List<string> supportedSymbols = new List<string>();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        string dummy;
                        while (reader.Read())
                        {
                            string symbol = AddSlashToSymbol(reader.GetString(reader.GetOrdinal("Symbol")));
                            //TODO: uncoment here if we want only symbols also with supported currencies
                            if(!HasSymbolUnsupportedCurrency(symbol, out dummy))
                                supportedSymbols.Add(symbol);
                        }
                    }

                    _symbolsSupportedByCtp[(int)venueCounterparty] = supportedSymbols;
                }
            }

            return _symbolsSupportedByCtp[(int) venueCounterparty];
        }

        public static bool HasSymbolUnsupportedCurrency(string symbol, out string unsupportedCurrency)
        {
            unsupportedCurrency = symbol.Substring(0, 3);

            if (!GetSupportedCurrenciesList().Contains(unsupportedCurrency))
                return true;

            unsupportedCurrency = symbol.Substring(symbol.Length - 3, 3);

            return !GetSupportedCurrenciesList().Contains(unsupportedCurrency);
        }

        private static List<string> _supportedCurrencies = null; 
        public static List<string> GetSupportedCurrenciesList()
        {
            if (_supportedCurrencies == null)
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["DCBackend"];
                using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("[dbo].[GetSupportedCurrencies_SP]", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    List<string> supportedCurrencies = new List<string>();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            supportedCurrencies.Add(reader.GetString(reader.GetOrdinal("CurrencyAlphaCode")));
                        }
                    }

                    _supportedCurrencies = supportedCurrencies;
                }
            }

            return _supportedCurrencies;
        }

        private static GroupNamesCache _groupNamesCache = new GroupNamesCache();
        private static TradingTypeNamesCache _typeNamesCache = new TradingTypeNamesCache();
        private static CreditLineCptsCache _creditLineCptsCache = new CreditLineCptsCache();

        public static string GetGroupName(int groupId)
        {
            return _groupNamesCache.GetGroupName(groupId);
        }

        public static IEnumerable<string> AllGroupNames
        {
            get { return _groupNamesCache.AllGroupNames; }
        }

        public static IEnumerable<int> AllGroupIds
        {
            get { return _groupNamesCache.AllGroupIds; }
        }

        public static void RefreshGroupNames()
        {
            _groupNamesCache.Refresh();
        }

        public static string GetTradingTypeName(int tradingTypeId)
        {
            return _typeNamesCache.GetTradingTypeName(tradingTypeId);
        }

        public static IEnumerable<int> AllTypeIds
        {
            get { return _typeNamesCache.AllTypeIds; }
        }

        public static List<string> GetCounteprartiesForCreditLine(string creditLineName)
        {
            return _creditLineCptsCache.GetLinkedCounterparties(creditLineName);
        }
    }


    internal abstract class AutoRefreshStateBase
    {
        private TimeSpan _maximumCacheAge;
        private DateTime _nextRefreshDueTimeUtc = DateTime.MinValue;
        private object _locker = new object();

        protected AutoRefreshStateBase(TimeSpan maximumCacheAge)
        {
            this._maximumCacheAge = maximumCacheAge;
        }

        protected abstract void RefreshState();

        public void Refresh()
        {
            //This is to prevent IndexOutOfRangeException during Array.Clear
            // This way we potentially block for lengthy time (during sql operation), but it's very rare
            // it doesn't pay of to over-optimize here
            lock (_locker)
            {
                this.RefreshState();
            }
        }

        protected void RefreshStateIfNeeded()
        {
            DateTime now = DateTime.UtcNow;
            if (now > _nextRefreshDueTimeUtc)
            {
                Refresh();
                _nextRefreshDueTimeUtc = now + _maximumCacheAge;
            }
        }
    }

    internal class GroupNamesCache : AutoRefreshStateBase
    {
        public GroupNamesCache()
            :base(TimeSpan.FromSeconds(5))
        { }

        public string GetGroupName(int groupId)
        {
            this.RefreshStateIfNeeded();

            string groupName;
            _groupNameMappings.TryGetValue(groupId, out groupName);
            return groupName;
        }

        public IEnumerable<string> AllGroupNames
        {
            get
            {
                this.RefreshStateIfNeeded();
                return _groupNameMappings.Values;
            }
        }

        public IEnumerable<int> AllGroupIds
        {
            get
            {
                this.RefreshStateIfNeeded();
                return _groupNameMappings.Keys;
            }
        }

        private Dictionary<int, string> _groupNameMappings = new Dictionary<int, string>(); 

        protected override void RefreshState()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("[dbo].[GetTradingSystemGroupNames_SP]", connection);
                command.CommandType = CommandType.StoredProcedure;

                using (var reader = command.ExecuteReader())
                {
                    _groupNameMappings.Clear();
                    while (reader.Read())
                    {
                        _groupNameMappings[reader.GetInt32(reader.GetOrdinal("TradingSystemGroupId"))]
                            = reader.GetString(reader.GetOrdinal("TradingSystemGroupName"));
                    }
                }
            }
        }
    }

    internal class TradingTypeNamesCache : AutoRefreshStateBase
    {
        public TradingTypeNamesCache()
            : base(TimeSpan.FromSeconds(5))
        { }

        public string GetTradingTypeName(int tradingTypeId)
        {
            this.RefreshStateIfNeeded();

            string groupName;
            _typeNameMappings.TryGetValue(tradingTypeId, out groupName);
            return groupName;
        }

        public IEnumerable<int> AllTypeIds
        {
            get
            {
                this.RefreshStateIfNeeded();
                return _typeNameMappings.Keys;
            }
        }

        private Dictionary<int, string> _typeNameMappings = new Dictionary<int, string>();

        protected override void RefreshState()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("[dbo].[GetTradingSystemTypeNames_SP]", connection);
                command.CommandType = CommandType.StoredProcedure;

                using (var reader = command.ExecuteReader())
                {
                    _typeNameMappings.Clear();
                    while (reader.Read())
                    {
                        _typeNameMappings[reader.GetByte(reader.GetOrdinal("TradingSystemTypeId"))]
                            = reader.GetString(reader.GetOrdinal("TradingSystemTypeName"));
                    }
                }
            }
        }
    }

    internal class CreditLineCptsCache : AutoRefreshStateBase
    {
        public CreditLineCptsCache()
            : base(TimeSpan.FromSeconds(5))
        { }

        public List<string> GetLinkedCounterparties(string creditLineName)
        {
            this.RefreshStateIfNeeded();

            List<string> counteparties;
            if (!_creditLinesMap.TryGetValue(creditLineName, out counteparties))
            {
                this.Refresh();
                _creditLinesMap.TryGetValue(creditLineName, out counteparties);
            }

            return counteparties;
        }

        private Dictionary<string, List<string>> _creditLinesMap = new Dictionary<string, List<string>>();

        protected override void RefreshState()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("[dbo].[GetCreditLineCptMap_SP]", connection);
                command.CommandType = CommandType.StoredProcedure;

                using (var reader = command.ExecuteReader())
                {
                    Dictionary<string, List<string>> creditLinesMapLocal = new Dictionary<string, List<string>>();

                    while (reader.Read())
                    {
                        string creditLineName = reader.GetString(reader.GetOrdinal("CreditLineName"));
                        string counterparty = reader.GetString(reader.GetOrdinal("Counterparty"));

                        if(creditLinesMapLocal.ContainsKey(creditLineName))
                            creditLinesMapLocal[creditLineName].Add(counterparty);
                        else
                            creditLinesMapLocal[creditLineName] = new List<string>(){counterparty};
                    }

                    _creditLinesMap = creditLinesMapLocal;
                }
            }
        }
    }
}