﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="KGTKillSwitchWeb.Test" %>

<%@ Register Src="~/RotatedText.ascx" TagPrefix="uc1" TagName="RotatedText" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    
    <style type="text/css">
        .rotatediv {
            -ms-transform: rotate(90deg); /* IE 9 */
            -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
            transform: rotate(90deg);
        }
        .blah {
            background-color: red;
        }

        .rotated-text {
            display: inline-block;
            overflow: hidden;
            width: 1.0em;
            line-height: 0.9;
        }

        .rotated-text__inner {
            display: inline-block;
            white-space: nowrap;
            transform: translate(0,100%) rotate(-90deg);
            transform-origin: 0 0;
        }

            .rotated-text__inner:after {
                content: "";
                float: left;
                margin-top: 100%;
            }


            .rotated-text2 {
            display: inline-block;
            overflow: hidden;
            width: 1.0em;
            line-height: 0.9;
        }

        .rotated-text__inner2 {
            display: inline-block;
            white-space: nowrap;
            transform: translate(0,100%) rotate(-90deg);
            transform-origin: 0 0;
        }

            .rotated-text__inner2:after {
                content: "";
                float: left;
                margin-top: 100%;
            }

    </style>

</head>
<body>
    <form id="form1" runat="server">
        
        <div id="AutoRefreshErrorPanel" style="display: block">
            <div runat="server" class="fadeMe" id="fadingPanel" />
            <div class="centeredErrorPanel">
                <h2  style="text-align: center; color: red">There was an error during obtainig page content. Hit Refresh link to continue.</h2>
                <p style="color: red">Page was not updated from <span id="TimeText">[unknown]</span>. This may be due to connection issues.</p>
                <div class="linkInCenteredErrorPanel"><a href="?">Refresh</a></div>
            </div>
        </div>
        
        <table class="SingleTable" width="100%">
            <thead style="height: 200px">
            <tr style="height: 150px">
                <td>
                   dffddfdf 
                </td>
                <td>
                   45454 
                </td>
                <td>
                   wwwd 
                </td>
            </tr>
            </thead>
            <tbody style="height: 300px">
                <tr style="height: 150px">
                <td>
                   dffddfdf 
                </td>
                <td>
                   45454 
                </td>
                <td>
                   wwwd 
                </td>
            </tr>
                <tr style="height: 150px">
                <td>
                   dffddfdf 
                </td>
                <td>
                   45454 
                </td>
                <td>
                   wwwd 
                </td>
            </tr>
                <tr style="height: 150px">
                <td>
                   dffddfdf 
                </td>
                <td>
                   45454 
                </td>
                <td>
                   wwwd 
                </td>
                    <tr style="height: 150px">
                <td>
                   dffddfdf 
                </td>
                <td>
                   45454 
                </td>
                <td>
                   wwwd 
                </td>
                        <tr style="height: 150px">
                <td>
                   dffddfdf 
                </td>
                <td>
                   45454 
                </td>
                <td>
                   wwwd 
                </td>
                            <tr style="height: 150px">
                <td>
                   dffddfdf 
                </td>
                <td>
                   45454 
                </td>
                <td>
                   wwwd 
                </td>
            </tr>

            </tbody>
        </table>
    
        <%--<div id="ErrorDiv" style="display: block">
            <div runat="server" class="fadeMe" ID="AutoRefreshErrorPanel" Visible="True" style="display: block" />
            <div class="centeredErrorPanel">dsdskmdksm</div>
        </div>--%>
        
        
        
        <%--<asp:Panel runat="server" CssClass="fadeMe" ID="FadingPanel" Visible="True" >
        <div class="centeredErrorPanel">dsdskmdksm</div>
    </asp:Panel>--%>
        

    
    </form>
</body>
</html>
