﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class UnsettledNop : PageWithNoControlState
    {
        protected class LimitSetting
        {
            private readonly List<LimitSettingEntry> _limitSettings = new List<LimitSettingEntry>()
            {
                new LimitSettingEntry("LimitTotal"),
                new LimitSettingEntry("LockLimitTotal"),
                new LimitSettingEntry("LimitPerDay"),
                new LimitSettingEntry("LockLimitPerDay")
            };

            private readonly List<LimitSettingEntry> _pbLimitSettings;

            public LimitSetting()
            {
                this._pbLimitSettings = new List<LimitSettingEntry>()
                {
                    _limitSettings[0],
                    _limitSettings[1]
                };
            }

            private class LimitSettingEntry
            {
                public LimitSettingEntry(string name)
                {
                    this.Name = name;
                }

                public string Name { get; private set; }
                public string StringValue { get; set; }
                public decimal ParsedValue { get; set; }
                public string ErrorMessage { get; set; }
            }

            public bool TryExtractAndValidateLimits(LimitType limitType)
            {
                bool succeed = true;

                List<LimitSettingEntry> limitSettingsLocal = limitType == LimitType.CreditLine
                    ? _limitSettings
                    : _pbLimitSettings;

                foreach (LimitSettingEntry limitSetting in limitSettingsLocal)
                {
                    decimal parsedValue = 0;
                    if (string.IsNullOrWhiteSpace(limitSetting.StringValue))
                    {
                        limitSetting.ErrorMessage = "New limit cannot be empty! (Must be 0 or number above 2M)";
                        succeed = false;
                    }
                    else if (!decimal.TryParse(limitSetting.StringValue, out parsedValue))
                    {
                        limitSetting.ErrorMessage = "The passed number is not a valid decimal number (might be too high)!";
                        succeed = false;
                    }
                    limitSetting.ParsedValue = parsedValue;

                    if (parsedValue < 0)
                    {
                        limitSetting.ErrorMessage = "Limit must be nonnegative number";
                        succeed = false;
                    }
                }

                if (this.GetValue("LimitTotal") < this.GetValue("LockLimitTotal"))
                {
                    _limitSettings.First(set => set.Name == "LockLimitTotal").ErrorMessage =
                        "Lock limit must be less or equal to max limit";
                    succeed = false;
                }

                if (limitType == LimitType.CreditLine && this.GetValue("LimitPerDay") < this.GetValue("LockLimitPerDay"))
                {
                    _limitSettings.First(set => set.Name == "LockLimitPerDay").ErrorMessage =
                        "Lock limit must be less or equal to max limit";
                    succeed = false;
                }

                return succeed;
            }

            public void FillLimitsListWithValues(NameValueCollection queryString)
            {
                foreach (LimitSettingEntry limitSetting in _limitSettings)
                {
                    limitSetting.StringValue = queryString[limitSetting.Name];
                    limitSetting.ErrorMessage = queryString[limitSetting.Name + "ValidationErrorMsg"];
                }
            }

            public string BuildQueryString()
            {
                string queryString = string.Empty;

                foreach (LimitSettingEntry limitSetting in _limitSettings)
                {
                    queryString += string.Format("&{0}={1}", limitSetting.Name, limitSetting.StringValue);
                    if (!string.IsNullOrEmpty(limitSetting.ErrorMessage))
                        queryString += string.Format("&{0}ValidationErrorMsg={1}", limitSetting.Name, limitSetting.ErrorMessage);
                }

                return queryString;
            }

            public string GetValueString(string limitName)
            {
                return _limitSettings.First(set => set.Name == limitName).StringValue;
            }

            public string GetErrorString(string limitName)
            {
                return _limitSettings.First(set => set.Name == limitName).ErrorMessage;
            }

            public decimal GetValue(string limitName)
            {
                return _limitSettings.First(set => set.Name == limitName).ParsedValue;
            }
        }


        protected LimitSetting _limitSetting = new LimitSetting();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (!PerformQueryAction())
                {
                    this.UpdateUnsettledNops(false);
                    this.UpdatePerPbNops(false);
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        private string RootUrl
        {
            get
            {
                if (Request.UrlReferrer != null)
                {
                    return new Uri(Request.UrlReferrer.AbsoluteUri).GetLeftPart(UriPartial.Path);
                }
                else
                {
                    return "~/NOP.aspx";
                }
            }
        }

        


        private bool PerformQueryAction()
        {
            //creditLineId needs to be allways present and is not counted
            if (Request.QueryString.Keys.Count > 0)
            {
                LimitType limitType = (LimitType) Enum.Parse(typeof (LimitType), Request.QueryString["Type"]);

                if (Request.QueryString["Edit"] != null)
                {
                    this._limitSetting.FillLimitsListWithValues(Request.QueryString);

                    this.AC.AllowAutoRefresh = false;
                    return false;
                }
                else if (Request.QueryString["Update"] != null)
                {
                    string creditLineId = Request.QueryString["Update"];

                    this._limitSetting.FillLimitsListWithValues(Request.QueryString);

                    if (this._limitSetting.TryExtractAndValidateLimits(limitType))
                    {
                        if (UpdateLimit(limitType, creditLineId, _limitSetting))
                        {
                            //this force refreshes both
                            this.UpdateUnsettledNops(true);
                            this.Response.Redirect(this.RootUrl);
                            return true;
                        }
                        else
                        {
                            this.AC.AllowAutoRefresh = false;
                            return false;
                        }
                    }

                    this.Response.Redirect(string.Format("{0}?Type={1}&Edit={2}{3}",
                            this.RootUrl, Request.QueryString["Type"], creditLineId, this._limitSetting.BuildQueryString()));
                    return true;
                }


                this.AC.AllowAutoRefresh = false;

            }

            return false;
        }

        private bool UpdateLimit(LimitType limitType, string rowId, LimitSetting limitSetting)
        {
            if (limitType == LimitType.CreditLine)
                return this.UpdateCreditLimit(rowId, limitSetting);
            else
                return this.UpdatePBLimit(rowId, limitSetting);
        }

        private bool UpdatePBLimit(string pbId, LimitSetting limitSetting)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[PrimeBrokerCreditLine_UpdatePBLimit_SP]", connection);
                        if (!string.IsNullOrEmpty(pbId))
                        {
                            command.Parameters.AddWithValue("@PrimeBrokerId", pbId);
                        }
                        command.Parameters.AddWithValue("@NewUnsettledLimitUsdTotal", limitSetting.GetValue("LimitTotal"));
                        command.Parameters.AddWithValue("@NewUnsettledLimitUsdTotalToLockDestiantion", limitSetting.GetValue("LockLimitTotal"));
                        command.CommandType = CommandType.StoredProcedure;
                        return command.ExecuteNonQuery() > 0;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        private bool UpdateCreditLimit(string creditLineId, LimitSetting limitSetting)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[PrimeBrokerCreditLine_UpdateLimit_SP]", connection);
                        command.Parameters.AddWithValue("@CreditLineId", creditLineId);
                        command.Parameters.AddWithValue("@NewUnsettledLimitUsdTotal", limitSetting.GetValue("LimitTotal"));
                        command.Parameters.AddWithValue("@NewUnsettledLimitUsdTotalToLockDestiantion", limitSetting.GetValue("LockLimitTotal"));
                        command.Parameters.AddWithValue("@NewUnsettledLimitUsdPerValueDate", limitSetting.GetValue("LimitPerDay"));
                        command.Parameters.AddWithValue("@NewUnsettledLimitUsdPerValueDateToLockDestiantion", limitSetting.GetValue("LockLimitPerDay"));
                        command.CommandType = CommandType.StoredProcedure;
                        return command.ExecuteNonQuery() > 0;
                    }
                }
                else
                {
                    this.AC.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.AC.ReportException(e);
            }

            return false;
        }

        protected enum LimitType
        {
            CreditLine,
            PrimeBroker
        }

        protected bool IsEditedRow(string creditLineId, LimitType limitType)
        {
            return Request.QueryString["Edit"] != null && Request.QueryString["Edit"].Equals(creditLineId) &&
                   limitType.ToString() == Request.QueryString["Type"];
        }

        private void UpdateUnsettledNops(bool force)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetSettlementStatisticsPerCreditLine_Cached_SP]", connection);
                        command.Parameters.AddWithValue("@Force", force);
                        command.CommandType = CommandType.StoredProcedure;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            UnsettledNopsGridView.DataSource = reader;
                            UnsettledNopsGridView.DataBind();
                        }
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }
        }

        private void UpdatePerPbNops(bool force)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetNopStatsPerPb_Cached_SP]", connection);
                        command.Parameters.AddWithValue("@Force", force);
                        command.CommandType = CommandType.StoredProcedure;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            PbNopGridView.DataSource = reader;
                            PbNopGridView.DataBind();
                        }
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }
        }

        private void FlipChildLabelColorIfNeeded(Control parentControl)
        {
            if (parentControl == null || parentControl.Controls == null)
                return;

            foreach (Control control in parentControl.Controls)
            {
                if (control is Label)
                {
                    Label lbl = control as Label;
                    if (lbl.ForeColor == Color.Red)
                        lbl.ForeColor = Color.White;
                }

                FlipChildLabelColorIfNeeded(control);
            }
        }

        private decimal GetNullableDecimal(GridViewRow row, string columnName)
        {
            return ((DbDataRecord) row.DataItem)[columnName] == DBNull.Value
                ? 0m
                : (decimal) ((DbDataRecord) row.DataItem)[columnName];

        }

        protected void UnsettledNopsGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                decimal unsetledNopTotal = GetNullableDecimal(e.Row, "UnsettledNopUsdTotal");

                if (unsetledNopTotal >=
                    (decimal)((DbDataRecord)e.Row.DataItem)["MaxUnsettledNopUsdTotal"])
                {
                    TableCell cell = 
                        e.Row.Cells[
                            UnsettletNopGridColumnsInfo.DealsCountTotalColumnIdx +
                            UnsettletNopGridColumnsInfo.MaxOpenSettlementDates + 1];

                    cell.CssClass = "highAttentionCell";
                    cell.ToolTip = "Backend detected exceeded Max Allowed Total NOP";
                    FlipChildLabelColorIfNeeded(cell);
                }

                else if (unsetledNopTotal >=
                    (decimal)((DbDataRecord)e.Row.DataItem)["MaxUnsettledNopUsdTotalToLockDestination"])
                {
                    TableCell cell =
                        e.Row.Cells[
                            UnsettletNopGridColumnsInfo.DealsCountTotalColumnIdx +
                            UnsettletNopGridColumnsInfo.MaxOpenSettlementDates + 2];
                    cell.CssClass = "attentionCell";
                    cell.ToolTip = "Backend detected exceeded Lockdown Limit Total NOP";
                }


                decimal unsetledNopPerDay = GetNullableDecimal(e.Row, "HighestUnsettledNopInSingleT");

                if (unsetledNopPerDay >=
                    (decimal)((DbDataRecord)e.Row.DataItem)["MaxUnsettledNopUsdPerValueDate"])
                {
                    TableCell cell =
                        e.Row.Cells[
                            UnsettletNopGridColumnsInfo.DealsCountTotalColumnIdx +
                            UnsettletNopGridColumnsInfo.MaxOpenSettlementDates + 3];

                    cell.CssClass = "highAttentionCell";
                    cell.ToolTip = "Backend detected exceeded Max Allowed NOP per Single Settlement Date";
                    FlipChildLabelColorIfNeeded(cell);
                }

                else if (unsetledNopPerDay >=
                    (decimal)((DbDataRecord)e.Row.DataItem)["MaxUnsettledNopUsdPerValueDateToLockDestination"])
                {
                    TableCell cell =
                        e.Row.Cells[
                            UnsettletNopGridColumnsInfo.DealsCountTotalColumnIdx +
                            UnsettletNopGridColumnsInfo.MaxOpenSettlementDates + 4];
                    cell.CssClass = "attentionCell";
                    cell.ToolTip = "Backend detected exceeded Lockdown NOP Limit per Single Settlement Date";
                }

                {
                    CreditZone zone;
                    if (((DbDataRecord) e.Row.DataItem)["Zone_Total"] != DBNull.Value)
                    {
                        zone = (CreditZone) Enum.Parse(typeof (CreditZone), (((DbDataRecord) e.Row.DataItem)["Zone_Total"]).ToString());
                    }
                    else
                    {
                        zone = (decimal) ((DbDataRecord) e.Row.DataItem)["MaxUnsettledNopUsdTotal"] > 0m
                            ? CreditZone.NoCheckingZone
                            : CreditZone.OverTheMaxZone;
                    }

                    TableCell totalNopCell = e.Row.Cells[UnsettletNopGridColumnsInfo.NopTotalColumnIdx];
                    totalNopCell.ToolTip = zone.ToString();
                    totalNopCell.CssClass = _cellClassNamesForZones[(int) zone];
                }
                

                for (int settlmentIdx = 1; settlmentIdx <= _unsettletNopGridColumnsInfo.CurrentlyOpenSettlementDates; settlmentIdx++)
                {
                    CreditZone zone;
                    if (((DbDataRecord)e.Row.DataItem)["Zone_T" + settlmentIdx] != DBNull.Value)
                    {
                        zone = (CreditZone)Enum.Parse(typeof(CreditZone), (((DbDataRecord)e.Row.DataItem)["Zone_T" + settlmentIdx]).ToString());
                    }
                    else
                    {
                        zone = (decimal)((DbDataRecord)e.Row.DataItem)["MaxUnsettledNopUsdPerValueDate"] > 0m
                            ? CreditZone.NoCheckingZone
                            : CreditZone.OverTheMaxZone;
                    }

                    TableCell perDateNopCell = e.Row.Cells[UnsettletNopGridColumnsInfo.NopTotalColumnIdx + settlmentIdx];
                    perDateNopCell.ToolTip = zone.ToString();
                    perDateNopCell.CssClass = _cellClassNamesForZones[(int)zone];
                }
            }
        }

        private static readonly string[] _cellClassNamesForZones = new string[]
        {
            "RecentDealChange",
            "neutrallCell",
            "attentionCell",
            "highAttentionCell"
        };

        private class UnsettletNopGridColumnsInfo
        {
            public const int DealsCountTotalColumnIdx = 3;
            public const int MaxOpenSettlementDates = 8;
            public const int LimitColumnsNum = 4;
            public const int NopTotalColumnIdx = DealsCountTotalColumnIdx + MaxOpenSettlementDates + LimitColumnsNum + 1;

            public bool HeaderAdjusted;
            public int CurrentlyOpenSettlementDates;
        }

        private UnsettletNopGridColumnsInfo _unsettletNopGridColumnsInfo = new UnsettletNopGridColumnsInfo();

        protected void UnsettledNopsGridView_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (_unsettletNopGridColumnsInfo.HeaderAdjusted)
                    return;
                _unsettletNopGridColumnsInfo.HeaderAdjusted = true;

                GridView grid = (GridView)sender;

                //The deals columns
                for (int settlmentIdx = 1; settlmentIdx <= UnsettletNopGridColumnsInfo.MaxOpenSettlementDates; settlmentIdx++)
                {
                    if (((DbDataRecord)e.Row.DataItem)[string.Format("T{0}", settlmentIdx)] == DBNull.Value)
                    {
                        grid.Columns[UnsettletNopGridColumnsInfo.DealsCountTotalColumnIdx + settlmentIdx].Visible = false;
                        grid.Columns[UnsettletNopGridColumnsInfo.NopTotalColumnIdx + settlmentIdx].Visible = false;
                    }
                    else
                    {
                        _unsettletNopGridColumnsInfo.CurrentlyOpenSettlementDates++;
                        grid.HeaderRow.Cells[UnsettletNopGridColumnsInfo.DealsCountTotalColumnIdx + settlmentIdx].Text =
                            ((DateTime)((DbDataRecord)e.Row.DataItem)[string.Format("T{0}", settlmentIdx)]).ToString(
                                "MMM-dd");
                        grid.HeaderRow.Cells[UnsettletNopGridColumnsInfo.NopTotalColumnIdx + settlmentIdx].Text =
                            ((DateTime)((DbDataRecord)e.Row.DataItem)[string.Format("T{0}", settlmentIdx)]).ToString(
                                "MMM-dd");
                    }
                }

                //Double Header
                GridViewRow headerGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableHeaderCell headerCell = new TableHeaderCell();
                headerCell.ColumnSpan = 3;
                headerCell.CssClass = "noShow";
                headerGridRow.Cells.Add(headerCell);

                headerCell = new TableHeaderCell();
                headerCell.Text = "Unsettled Deals Count";
                headerCell.CssClass = "secondHeader";
                //+1 for total
                headerCell.ColumnSpan = _unsettletNopGridColumnsInfo.CurrentlyOpenSettlementDates + 1;
                headerGridRow.Cells.Add(headerCell);

                headerCell = new TableHeaderCell();
                headerCell.Text = "Unsettled NOP (abs USD)";
                headerCell.CssClass = "secondHeader";
                //+1 for total, +4 for limits
                headerCell.ColumnSpan = _unsettletNopGridColumnsInfo.CurrentlyOpenSettlementDates + 1 + UnsettletNopGridColumnsInfo.LimitColumnsNum;
                headerGridRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerGridRow);
            }
        }

        private static class PBNopGridColumnsInfo
        {
            public const int PbNameIdx = 0;
            public const int MaxNopLimitIdx = 2;
            public const int LockDownNopLimitIdx = 3;
            public const int NopColumnIdx = 4;
            public const int ZoneColumnIdx = 5;
        }


        protected void PbNopGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                decimal unsetledNopTotal = GetNullableDecimal(e.Row, "NopAbsUsdCurrentTradeDate");

                if (unsetledNopTotal >=
                    (decimal)((DbDataRecord)e.Row.DataItem)["MaxUnsettledNopUsdTotal"])
                {
                    TableCell cell = e.Row.Cells[PBNopGridColumnsInfo.MaxNopLimitIdx];

                    cell.CssClass = "highAttentionCell";
                    cell.ToolTip = "Backend detected exceeded Max Allowed Total NOP";
                    FlipChildLabelColorIfNeeded(cell);
                }

                else if (unsetledNopTotal >=
                    (decimal)((DbDataRecord)e.Row.DataItem)["MaxUnsettledNopUsdTotalToLockDestination"])
                {
                    TableCell cell = e.Row.Cells[PBNopGridColumnsInfo.LockDownNopLimitIdx];

                    cell.CssClass = "attentionCell";
                    cell.ToolTip = "Backend detected exceeded Lockdown Limit Total NOP";
                }

                e.Row.Cells[PBNopGridColumnsInfo.NopColumnIdx].CssClass =
                    Utils.GetCssClassForNumber(((DbDataRecord) e.Row.DataItem)["NopAbsUsdCurrentTradeDate"]);

                if (((DbDataRecord) e.Row.DataItem)["PrimeBrokerName"] == DBNull.Value)
                {
                    e.Row.Cells[PBNopGridColumnsInfo.PbNameIdx].Text = "ALL";
                    e.Row.Font.Bold = true;
                    e.Row.Font.Italic = true;
                }


                {
                    CreditZone zone;
                    string zoneString;
                    if (((DbDataRecord)e.Row.DataItem)["CheckingZone"] != DBNull.Value)
                    {
                        zone = (CreditZone)Enum.Parse(typeof(CreditZone), (((DbDataRecord)e.Row.DataItem)["CheckingZone"]).ToString());
                        zoneString = zone.ToString();
                    }
                    else
                    {
                        zone = (decimal)((DbDataRecord)e.Row.DataItem)["MaxUnsettledNopUsdTotal"] > 0m
                            ? CreditZone.NoCheckingZone
                            : CreditZone.OverTheMaxZone;
                        zoneString =string.Format("{0} (web amend)", zone);
                        e.Row.Cells[PBNopGridColumnsInfo.ZoneColumnIdx].Text = zoneString;
                    }

                    TableCell zoneCell = e.Row.Cells[PBNopGridColumnsInfo.ZoneColumnIdx];
                    zoneCell.ToolTip = zoneString;
                    zoneCell.CssClass = _cellClassNamesForZones[(int)zone];
                }

            }
        }
    }
}