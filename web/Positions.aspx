﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Positions.aspx.cs" Inherits="KGTKillSwitchWeb.UnsettledPositions" %>
<%@ Import Namespace="KGTKillSwitchWeb" %>
<%@ Import Namespace="System.Data.Common" %>
<%@ Register TagPrefix="uc1" TagName="autorefreshcontainer" Src="~/AutorefreshContainer.ascx" %>
<%@ Register TagPrefix="uc1" Namespace="WebControls" Assembly="WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <h2>Open position by symbol and value date (Central Bank settlement time)</h2>

    <uc1:autorefreshcontainer runat="server" id="AC"  AutorefreshMillisecondsInterval="2000" AllowAutoRefresh="True">
    <ContentTemplate> 
        <asp:GridView ID="OpenPositionsGridView" runat="server"
            GridLines="None"  
            autogeneratecolumns="False"
        CssClass="mGrid"   
        RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow"
             OnRowCreated="UnsettledNopsGridView_OnRowCreated">
        <Columns>  
            <asp:BoundField DataField="SymbolName" HeaderText="Symbol"/>  
            
            <asp:BoundField DataField="UnsettledNopNominalBase_T1" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" ItemStyle-Font-Bold="True" /> 
            <asp:BoundField DataField="UnsettledNopNominalBase_T2" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" ItemStyle-Font-Bold="True" /> 
            <asp:BoundField DataField="UnsettledNopNominalBase_T3" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" ItemStyle-Font-Bold="True" /> 
            <asp:BoundField DataField="UnsettledNopNominalBase_T4" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" ItemStyle-Font-Bold="True" /> 
            <asp:BoundField DataField="UnsettledNopNominalBase_T5" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" ItemStyle-Font-Bold="True" /> 
            <asp:BoundField DataField="UnsettledNopNominalBase_T6" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" ItemStyle-Font-Bold="True" /> 
            <asp:BoundField DataField="UnsettledNopNominalBase_T7" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" ItemStyle-Font-Bold="True" /> 
            <asp:BoundField DataField="UnsettledNopNominalBase_T8" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" ItemStyle-Font-Bold="True" /> 
            
            <asp:BoundField DataField="UnsettledNopNominalTerm_T1" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopNominalTerm_T2" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopNominalTerm_T3" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopNominalTerm_T4" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopNominalTerm_T5" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopNominalTerm_T6" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopNominalTerm_T7" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopNominalTerm_T8" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
 
            <asp:BoundField DataField="DealsCount_T1" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T2" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T3" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T4" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T5" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T6" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T7" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T8" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 

        </Columns>
    </asp:GridView>    
     </ContentTemplate>
     </uc1:autorefreshcontainer>
    

    <br/>
    

    <a href="/PositionsOverview.aspx">Positions Overview by Trade Dates</a>
</asp:Content>
