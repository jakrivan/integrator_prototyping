﻿<%@ Page Title="KGT Kill Switch Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="KGTKillSwitchWeb.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
    .style1
    {
        text-align: center;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="style1">
    <strong>KGT Risk Management</strong></h1>
<p>
    If you are experiencing problems with this webpage, please use the following contacts:
</p>
<p>
    email: <a href="mailto:support@kgtinv.com">support@kgtinv.com</a>
    <br/>telephone (trading room Monaco): +377-9325-1158 
    <br/>cell phone (Michal Kreslik): +336-7863-4891 
    <br/>cell phone (Marek Fogiel): +336-4391-6707
</p>
<p>
    <em><strong>All kill switch activity is monitored and logged.
</strong></em>
</p>
    <br/>
<p>
    &copy; 2013 KGT Investments, LLC, all rights reserved.
</p>
<p class="style1">
    
    &nbsp;</p>
</asp:Content>
