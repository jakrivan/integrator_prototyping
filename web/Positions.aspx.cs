﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class UnsettledPositions :  PageWithNoControlState
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                this.UpdateUnsettledNops();
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        private void UpdateUnsettledNops()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    //bool isTradingEnabled = false;
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("[dbo].[GetSettlementStatisticsPerSymbol_Cached_SP]", connection);
                        command.CommandType = CommandType.StoredProcedure;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            OpenPositionsGridView.DataSource = reader;
                            OpenPositionsGridView.DataBind();
                        }
                    }
                }
                catch (Exception exc)
                {
                    this.AC.ReportException(exc);
                }
            }
            else
            {
                this.AC.ReportError("Connection string is missing");
            }
        }


        //private class UnsettletNopGridColumnsInfo
        //{
        //    public const int DealsCountTotalColumnIdx = 3;
        //    public const int MaxOpenSettlementDates = 8;
        //    public const int LimitColumnsNum = 4;
        //    public const int NopTotalColumnIdx = DealsCountTotalColumnIdx + MaxOpenSettlementDates + LimitColumnsNum + 1;

        //    public bool HeaderAdjusted;
        //    public int CurrentlyOpenSettlementDates;
        //}

        //private UnsettletNopGridColumnsInfo _unsettletNopGridColumnsInfo = new UnsettletNopGridColumnsInfo();


        private static class UnsettledNopsGridColumnsInfo
        {
            public const int MaxOpenSettlementDates = 8;
            public const int FirstDealColumnIdx = 17;
            public const int FirstNopBaseColumnIdx = 1;
            public const int FirstNopTermColumnIdx = 9;
        }

        private bool _headerAdjusted = false;
        protected void UnsettledNopsGridView_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int settlmentIdx = 1;
                    settlmentIdx <= UnsettledNopsGridColumnsInfo.MaxOpenSettlementDates;
                    settlmentIdx++)
                {
                    e.Row.Cells[UnsettledNopsGridColumnsInfo.FirstNopBaseColumnIdx + settlmentIdx - 1].CssClass =
                        Utils.GetCssClassForNumber(
                            ((DbDataRecord) e.Row.DataItem)[string.Format("UnsettledNopNominalBase_T{0}", settlmentIdx)]);

                    e.Row.Cells[UnsettledNopsGridColumnsInfo.FirstNopTermColumnIdx + settlmentIdx - 1].CssClass =
                        Utils.GetCssClassForNumber(
                            ((DbDataRecord)e.Row.DataItem)[string.Format("UnsettledNopNominalTerm_T{0}", settlmentIdx)]);
                }

                if (_headerAdjusted)
                    return;
                _headerAdjusted = true;

                GridView grid = (GridView)sender;
                int currentlyOpenSettlementDates = 0;

                //The deals columns
                for (int settlmentIdx = 1; settlmentIdx <= UnsettledNopsGridColumnsInfo.MaxOpenSettlementDates; settlmentIdx++)
                {
                    if (((DbDataRecord)e.Row.DataItem)[string.Format("T{0}", settlmentIdx)] == DBNull.Value)
                    {
                        grid.Columns[UnsettledNopsGridColumnsInfo.FirstDealColumnIdx + settlmentIdx - 1].Visible = false;
                        grid.Columns[UnsettledNopsGridColumnsInfo.FirstNopBaseColumnIdx + settlmentIdx - 1].Visible = false;
                        grid.Columns[UnsettledNopsGridColumnsInfo.FirstNopTermColumnIdx + settlmentIdx - 1].Visible = false;
                    }
                    else
                    {
                        currentlyOpenSettlementDates++;
                        grid.HeaderRow.Cells[UnsettledNopsGridColumnsInfo.FirstDealColumnIdx + settlmentIdx - 1].Text =
                            ((DateTime)((DbDataRecord)e.Row.DataItem)[string.Format("T{0}", settlmentIdx)]).ToString(
                                "MMM-dd");
                        grid.HeaderRow.Cells[UnsettledNopsGridColumnsInfo.FirstNopBaseColumnIdx + settlmentIdx - 1].Text =
                            ((DateTime)((DbDataRecord)e.Row.DataItem)[string.Format("T{0}", settlmentIdx)]).ToString(
                                "MMM-dd");
                        grid.HeaderRow.Cells[UnsettledNopsGridColumnsInfo.FirstNopTermColumnIdx + settlmentIdx - 1].Text =
                            ((DateTime)((DbDataRecord)e.Row.DataItem)[string.Format("T{0}", settlmentIdx)]).ToString(
                                "MMM-dd");
                    }
                }

                //Double Header
                GridViewRow headerGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableHeaderCell headerCell = new TableHeaderCell();
                headerCell.ColumnSpan = 1;
                headerCell.CssClass = "noShow";
                headerGridRow.Cells.Add(headerCell);

                headerCell = new TableHeaderCell();
                headerCell.Text = "Unsettled NOP (base)";
                headerCell.CssClass = "secondHeader";
                headerCell.ColumnSpan = currentlyOpenSettlementDates;
                headerGridRow.Cells.Add(headerCell);

                headerCell = new TableHeaderCell();
                headerCell.Text = "Unsettled NOP (term)";
                headerCell.CssClass = "secondHeader";
                headerCell.ColumnSpan = currentlyOpenSettlementDates;
                headerGridRow.Cells.Add(headerCell);

                headerCell = new TableHeaderCell();
                headerCell.Text = "Unsettled Deals Count";
                headerCell.CssClass = "secondHeader";
                headerCell.ColumnSpan = currentlyOpenSettlementDates;
                headerGridRow.Cells.Add(headerCell);

                grid.Controls[0].Controls.AddAt(0, headerGridRow);
            }
        }
    }
}