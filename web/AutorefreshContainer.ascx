﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutorefreshContainer.ascx.cs" Inherits="KGTKillSwitchWeb.AutorefreshContainer" %>


<script type="text/javascript" src="Scripts/jquery-1.4.1.min.js"></script>

<script type="text/javascript">

    var allowRequest = true;
    
    //when clicking links, disable refresh in the meantime
    $('a').click(function () { DisableAutoRefresh(); });

    function DisableAutoRefresh() {
        allowRequest = false;
    }

    function RequestAsyncUpdate() {
        if (allowRequest) {
            __doPostBack('<%=UP.ClientID%>', '');
        }
    }
</script>


<asp:ScriptManager ID="SM" runat="server" EnablePartialRendering="true" ScriptMode="release">
    <Scripts>
        <asp:ScriptReference Path="Scripts/KGTScripts.js"/>
    </Scripts>
</asp:ScriptManager>




<asp:UpdatePanel ID="UP" runat="server">
<Triggers>
    <%--<asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />--%>
</Triggers>
<ContentTemplate>
    
    <div runat="server" id="AutoRefreshErrorPanel" ClientIDMode="Static" style="display: none">
        <div runat="server" class="fadeMe" />
        <div class="centeredErrorPanel">
            <h2  style="text-align: center; color: red">There was an error during obtainig page content. Hit Refresh link to continue.</h2>
            <p runat="server" id="errorLiteral" ClientIDMode="Static" style="color: red">Page was not successfully updated from <span id="TimeText">[unknown]</span>. This may be due to connection issues.</p>
            <div class="linkInCenteredErrorPanel"><a href='<%= this.RefreshUrl %>' onclick="DisableAutoRefresh();">Refresh</a></div>
        </div>
    </div>

    <asp:PlaceHolder runat="server" ID="RefreshEnabledPlaceholder">
        <input type="hidden" id="refreshIntervalField" value='<%=this.AutorefreshMillisecondsInterval%>'/>
    </asp:PlaceHolder>

    <asp:PlaceHolder runat="server" ID="ContentTemplatePlaceHolder">
        
    </asp:PlaceHolder>        
            
</ContentTemplate>
</asp:UpdatePanel>

<asp:Panel ID="Panel1" runat="server" style="text-align: right; padding: 0px 10px">
    <a href='<%= this.RefreshUrl %>' onclick="DisableAutoRefresh();">Refresh</a>
    <div id="updateErrorDiv" style="display: none">
        <span style="color: red; font-weight: bold" id="updateErrorSpan"></span>&nbsp;<asp:LinkButton runat="server" OnClientClick="$get('updateErrorDiv').style.display = 'none';" Text="Hide"></asp:LinkButton>
    </div>
</asp:Panel>