﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NotAuthorized.aspx.cs" Inherits="KGTKillSwitchWeb.NotAuthorized" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Not Authorized</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1><asp:Label ID="Label1" runat="server" ForeColor="Red" Text="You are not authorized to view requested page"></asp:Label></h1>
</asp:Content>
