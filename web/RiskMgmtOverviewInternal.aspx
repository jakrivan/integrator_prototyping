﻿<%@ Page Title="" Language="C#" EnableViewState="false" ViewStateMode="Disabled" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RiskMgmtOverviewInternal.aspx.cs" Inherits="KGTKillSwitchWeb.KGTRiskMgtmtOverviewInternal" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Common" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="KGTKillSwitchWeb" %>
<%@ Register Src="~/AutorefreshContainer.ascx" TagPrefix="uc1" TagName="AutorefreshContainer" %>
<%@ Register TagPrefix="uc1" Namespace="KGTKillSwitchWeb" Assembly="KGTKillSwitchWeb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>KGT Risk Management Quick Overview</title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 100%;
            border-style: none;
            border-width: 1px;
            border-spacing: 0px
        }
        .auto-style2 td {
            border: solid 1px #c1c1c1;
        }
    </style>
    
    <%--<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.2)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.2)" />--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <uc1:autorefreshcontainer runat="server" id="AC"  AutorefreshMillisecondsInterval="1000" AllowAutoRefresh="True">
    <ContentTemplate>
    

    <%--<asp:Panel runat="server" style="text-align: right">
        <asp:Label ID="LblUpdatedOn" runat="server" style="text-align:right;" Text="Updated At <not updated>"></asp:Label>
        &nbsp;
        <span runat="server" ID="progressDiv">
            //comment<progress value="0" max="10" runat="server" id="progressBar"/> 
        </span>  
    </asp:Panel>--%>
    
    
    <asp:Panel ID="ErrorPanel" runat="server" Visible="False">
         <h2  style="text-align: center; color: red">There was an error during obtainig positions.</h2>
        <p style="color: red"><asp:Literal ID="exceptionLiteral" runat="server" Text="?"></asp:Literal></p>
    </asp:Panel>

    <br />
            
    <asp:Panel ID="StateChangedErrorPanel" runat="server" Visible="False">
        <h2  style="text-align: center; color: red">Your request to change the state was denied, because the state was changed by someone else in the meantime.<br/> You can now see the last known state. Perform full refresh to re-enable auto-refresh.</h2>
    </asp:Panel>


    <table class="auto-style2" cellspacing="0">
        <tr>
            <td style="border-bottom-style: none">
                <asp:ImageButton ID="TradingOnImageButton2" Visible="False" runat="server" ImageUrl="Styles/play_big.png" style="display: block; margin-left: auto; margin-right: auto;" 
                    PostBackUrl="RiskMgmtOverviewInternal.aspx?Action=turnoff" OnClientClick="DisableAutoRefresh();" />
                  
                <%--Javascript must not return otherwise proper query string would not be populated--%>
                <asp:ImageButton ID="TradingOffImageButton2" Visible="False" runat="server" ImageUrl="Styles/pause_big.png" style="display: block; margin-left: auto; margin-right: auto;" 
                    PostBackUrl="RiskMgmtOverviewInternal.aspx?Action=turnon" OnClientClick="if(!confirm('Are you sure you want to enable trading?')) return false;  DisableAutoRefresh();"/>
            </td>
            <td style="text-align: center; border-bottom-style: none">
                <asp:ImageButton ID="GoFlatOnlyOnImageButton"  Visible="False" runat="server" ImageUrl="~/Styles/pause_smaller.png" style="display: block; margin-left: auto; margin-right: auto;" 
                    PostBackUrl="RiskMgmtOverviewInternal.aspx?Action=goflatoff" OnClientClick="if(!confirm('Are you sure you want to enable trading?')) return false;  DisableAutoRefresh();" />
                <asp:ImageButton ID="GoFlatOnlyOffImageButton" Visible="False" runat="server" ImageUrl="Styles/play_smaller.png" style="display: block; margin-left: auto; margin-right: auto;" 
                    PostBackUrl="RiskMgmtOverviewInternal.aspx?Action=goflaton" OnClientClick="DisableAutoRefresh();" />
            </td>

            <td style="text-align: center; border-bottom-style: none">
                <asp:Image ID="PrimebrokerTradingOnImage" ImageUrl="~/Styles/play_smaller.png" Visible="False" runat="server" />
                <asp:Image ID="PrimebrokerTradingOffImage" ImageUrl="~/Styles/pause_smaller.png" Visible="False" runat="server" />
            </td>
            <td style="text-align: center; border-bottom-style: none">
                    
                <asp:Image ID="IntegratorTradingOnImage" ImageUrl="~/Styles/play_smaller.png" Visible="False" runat="server" />
                    
                <asp:ImageButton ID="IntegratorTradingOffImageButton" Visible="False" runat="server" ImageUrl="Styles/pause_smaller.png" style="display: block; margin-left: auto; margin-right: auto;" 
                    PostBackUrl="RiskMgmtOverviewInternal.aspx?Action=internalturnon" OnClientClick="if(!confirm('Are you sure you want to enable trading?')) return false;  DisableAutoRefresh();" />

            </td>
            <td style="padding: 8px; border-left-style: dotted" id="cellErrorDetails" Visible="False" runat="server">
                Integrator Order Transmission disabled on <asp:Literal runat="server" ID="SwitchDisableTimeLiteral"></asp:Literal>:<br/>
                '<asp:Literal runat="server" ID="SwitchDisableReasonLiteral"></asp:Literal>'
            </td>
        </tr>
        <tr>
            <td style="border-top-style: none">
                <h2  style="text-align: center; margin: 0">Manual Kill&nbsp;Switch</h2> 
            </td>
            <td style="border-top-style: none">
                <h2  style="text-align: center; margin: 0">
                    <asp:Literal ID="GoFlatOnlyOnLiteral" runat="server" Visible="False">Go Flat Only</asp:Literal>
                    <asp:Literal ID="GoFlatOnlyOffLiteral" runat="server" Visible="False">Trading Allowed</asp:Literal>
                </h2>
            </td>
            <td style="border-top-style: none">
                <h2  style="text-align: center; margin: 0">Prime Broker Kill&nbsp;Switch</h2>
            </td>
            <td style="border-top-style: none">
                <h2  style="text-align: center; margin: 0">Integrator Kill&nbsp;Switch</h2>
            </td>
            <td style="border-bottom-style: none; border-right-style: none; display: none; margin: 0"></td>
        </tr>
    </table>

    
    <br />
    <br />
    <h2  style="text-align: center">KGT net open position in USD (abs): <span style="border-style:solid; padding: 5px;"><asp:Literal ID="LtrlNopTotal" runat="server" Text="?"></asp:Literal></span></h2>

    <br /> 
            
    <table  cellpadding="5" class="auto-style1">
        <tr>
            <td style="vertical-align:top">
                <h2  style="text-align: center">By currency</h2>
                
                <asp:GridView ID="CCYPositionsNops" runat="server"
                        GridLines="None"  
                        autogeneratecolumns="False"
                    CssClass="mGrid"   
                    RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow"
                    ShowFooter="True" ShowHeaderWhenEmpty="True">
                    <Columns>  
                        <asp:BoundField DataField="CCY" HeaderText="CCY" />  
                        <asp:BoundField DataField="NopPol" HeaderText="Pos (CCY)" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" />  
                        <asp:BoundField DataField="NopPolInUsd" HeaderText="Pos (USD)" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" />  
                    </Columns>
                    <FooterStyle Font-Bold="True"></FooterStyle>
                </asp:GridView>
            </td>
            <td style="vertical-align:top" rowspan="2">
                <h2  style="text-align: center">By counterparty</h2>
                
                <asp:GridView ID="CounterpartyNopsGridView" runat="server"
                        GridLines="None"  
                        autogeneratecolumns="False"
                    CssClass="mGrid"   
                    RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow" OnDataBound="CounterpartyNops_DataBound"
                    OnRowDataBound="CounterpartyNopsGridView_OnRowDataBound" ShowFooter="True" ShowHeaderWhenEmpty="True">
                    <Columns>  
                        <asp:BoundField DataField="Counterparty" HeaderText="C-party" FooterStyle-HorizontalAlign="Center"/>  
                        <asp:BoundField DataField="NopAbs" HeaderText="Pos (abs USD)" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" />  
                        
                        <%--ItemStyle-CssClass="RecentDealChange"--%>
                        <asp:BoundField DataField="DealsNum" HeaderText="Deals" ItemStyle-HorizontalAlign="Right" /> 
                        
                        <%--<asp:TemplateField ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass='<%# Utils.GetDealActionCellStyle(Container.DataItem, Utils.DealAction.KgtDeal) %>'>
                            <HeaderTemplate>Deals</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Literal runat="server" Text='<%# ((DbDataRecord)Container.DataItem)["DealsNum"] %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        

                        <asp:BoundField DataField="CtpRejectionsNum" HeaderText="Cpt Rej" ItemStyle-HorizontalAlign="Right" />  
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right">
                            <HeaderTemplate>Cpt Rej rate %</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Literal ID="CtpRejRateLieral" runat="server" Text='<%# FormatNumberPercents(((DbDataRecord)Container.DataItem)["CtpRejectionsRate"]) %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="KGTRejectionsNum" HeaderText="KGT Rej" ItemStyle-HorizontalAlign="Right" />  
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right">
                            <HeaderTemplate>KGT Rej rate %</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Literal ID="KGTRejRateLieral" runat="server" Text='<%# FormatNumberPercents(((DbDataRecord)Container.DataItem)["KGTRejectionsRate"]) %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right">
                            <HeaderTemplate>Avg deal (M USD)</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Literal ID="AvgDealLiteral" runat="server" Text='<%# FormatNumberMilions(((DbDataRecord)Container.DataItem)["AvgDealSizeUsd"]) %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right">
                            <HeaderTemplate>Vol (M USD)</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="VolumeUsdLabel" runat="server" Text='<%# FormatNumberMilionsOneDecimal(((DbDataRecord)Container.DataItem)["VolumeBaseAbsInUsd"]) %>' ToolTip='<%# ((DbDataRecord)Container.DataItem)["VolumeBaseAbsInUsd"] %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right">
                            <HeaderTemplate>Vol share %</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Literal ID="VolumeShareLieral" runat="server" Text='<%# FormatNumberPercents(((DbDataRecord)Container.DataItem)["VolumeShare"]) %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField> 
                        
                        
                        <asp:BoundField DataField="CommissionsPbUsd" HeaderText="Comm USD PB" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" /> 
                        <asp:BoundField DataField="CommissionsCptUsd" HeaderText="Comm USD Cpt" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" /> 
                        <asp:BoundField DataField="CommissionsSumUsd" HeaderText="Comm USD SUM" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" />  
                       

                    </Columns>
                    <FooterStyle Font-Bold="True" HorizontalAlign="Right"></FooterStyle>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top">
                <h2  style="text-align: center">By symbol</h2>

                <asp:GridView ID="CrossPointsGridView" runat="server"
                        GridLines="None"  
                        autogeneratecolumns="False"
                    CssClass="mGrid"   
                    RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow" OnDataBound="CrossPointsGridView_DataBound"
                    OnRowDataBound="CrossPointsGridView_OnRowDataBound"
                    ShowFooter="True" ShowHeaderWhenEmpty="True">
                    <Columns>  
                        <asp:BoundField DataField="Symbol" HeaderText="CCY Pair" FooterStyle-HorizontalAlign="Center"/>  

                        <asp:TemplateField>
                            <HeaderTemplate>Pos (CCY1)</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Panel ID="Panelccy1" HorizontalAlign="Right" runat="server" BackColor='<%# Utils.GetColorForNumber(((DbDataRecord)Container.DataItem)["NopBasePol"]) %>'>
                                    <asp:Label ID="lblCCY1" runat="server" Text='<%# FormatNumber(((DbDataRecord)Container.DataItem)["NopBasePol"]) %>' ></asp:Label>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="NopTermPol" HeaderText="Pos (CCY2)" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"  />
                        
                        <asp:BoundField DataField="PnlTermPol" HeaderText="Pnl (CCY2)" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"  />
                        
                        <asp:BoundField DataField="PnlTermPolInUsd" HeaderText="Pnl (USD)" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"  FooterStyle-HorizontalAlign="Right" />
                        
                        <asp:BoundField DataField="DealsNum" HeaderText="Deals" ItemStyle-HorizontalAlign="Right"  FooterStyle-HorizontalAlign="Right" />

                        <asp:TemplateField ItemStyle-HorizontalAlign="Right">
                            <HeaderTemplate>Vol (M CCY1)</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Literal runat="server" ID="VolumeCcyLiteral" Text='<%# FormatNumberMilions(((DbDataRecord)Container.DataItem)["VolumeInCcy1"]) %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right">
                            <HeaderTemplate>Vol (M USD)</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="VolumeUsdLabel" runat="server" Text='<%# FormatNumberMilionsOneDecimal(((DbDataRecord)Container.DataItem)["VolumeInUsd"]) %>' ToolTip='<%# ((DbDataRecord)Container.DataItem)["VolumeInUsd"] %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> 

                    </Columns>
                    <FooterStyle Font-Bold="True" HorizontalAlign="Right"></FooterStyle>
                </asp:GridView>
            </td>
        </tr>
    </table>
            
    <br />
    <br />
            
    <h2 style="text-align: center" runat="server" id="UnexpectedDealsHeader" Visible="False">Unexpected Executions reported by Counterparties (not included in KGT stats!):</h2>

    <asp:GridView ID="UnexpectedDealsGridView" runat="server"
            GridLines="None"  
            autogeneratecolumns="False"
        CssClass="mGrid"   
        RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow">
        <Columns>  
            <asp:BoundField DataField="IntegratorReceivedExecutionUtc" HeaderText="Deal Received [UTC]" DataFormatString="{0:yyyy-MM-dd HH:mm:ss.fff}" />  
            <asp:BoundField DataField="CounterpartySentExecutionUtc" HeaderText="Deal Sent [UTC]" DataFormatString="{0:yyyy-MM-dd HH:mm:ss.fff}" />  
            <asp:BoundField DataField="CounterpartyTransactionTimeUtc" HeaderText="Deal Executed [UTC]" DataFormatString="{0:yyyy-MM-dd HH:mm:ss.fff}" />  
            <asp:BoundField DataField="CounterpartySettlementTimeLocal" HeaderText="Value Date" DataFormatString="{0:yyyy-MM-dd}" />  
            <asp:BoundField DataField="CounterpartyCode" HeaderText="Counterparty" /> 
            <asp:BoundField DataField="Symbol" HeaderText="Symbol" /> 
            <asp:BoundField DataField="Direction" HeaderText="Direction" /> 
            <asp:BoundField DataField="FilledAmountBaseAbs" HeaderText="Base Size Abs" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" />  
            <asp:BoundField DataField="FilledPrice" HeaderText="Price" DataFormatString="{0:G29}" ItemStyle-HorizontalAlign="Right" />  
            <asp:BoundField DataField="CounterpartyTransactionId" HeaderText="Transaction label" ItemStyle-Font-Bold="True" /> 
            <asp:TemplateField ItemStyle-HorizontalAlign="Right">
                            <HeaderTemplate>Order Identifier</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="OrderIdLbl" runat="server" Font-Bold="True" BackColor='<%# (bool)((DbDataRecord)Container.DataItem)["WasPreviouslyBroken"] ? Color.PaleVioletRed : Color.Empty %>' Text='<%# ((DbDataRecord)Container.DataItem)["ExternalClientOrderId"] %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
            
        </Columns>
    </asp:GridView>             

        
    <h2 style="text-align: center" runat="server" id="IgnoredOrdersHeader" Visible="False">KGT Orders Ignored by Counterparties:</h2>
    <h2 style="text-align: center" runat="server" id="NoIgnoredOrdersHeader" Visible="False">There are currently no ignored orders by counterparties today (UTC).</h2>
            
    <asp:GridView ID="IgnoredOrdersGridView" runat="server"
            GridLines="None"  
            autogeneratecolumns="False"
        CssClass="mGrid"   
        RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow">
        <Columns>  
            <asp:BoundField DataField="IntegratorSentExternalOrderUtc" HeaderText="Order Sent [UTC]" DataFormatString="{0:yyyy-MM-dd HH:mm:ss.fff}" />  
            <asp:BoundField DataField="Direction" HeaderText="Direction" /> 
            <asp:BoundField DataField="RequestedAmountBaseAbs" HeaderText="Base Size Abs" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" />  
            <asp:BoundField DataField="Symbol" HeaderText="Symbol" /> 
            <asp:BoundField DataField="CounterpartyCode" HeaderText="Counterparty" /> 
            <asp:BoundField DataField="RequestedPrice" HeaderText="Price" DataFormatString="{0:G29}" ItemStyle-HorizontalAlign="Right" /> 
            <asp:BoundField DataField="ExternalOrderLabelForCounterparty" HeaderText="Order Identifier" ItemStyle-Font-Bold="True" /> 
        </Columns>
    </asp:GridView>  
    
        </ContentTemplate>
        </uc1:autorefreshcontainer>
    
    
</asp:Content>
