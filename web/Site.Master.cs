﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string address = Request.UserHostAddress;

            if (Session["HostName"] == null)
            {
                System.Threading.Tasks.Task.Factory.StartNew(
                    () =>
                        {
                            try
                            {
                                Session["HostName"] = System.Net.Dns.GetHostEntry(Request.UserHostAddress).HostName;
                            }
                            catch (Exception)
                            {
                                Session["HostName"] = null;
                            }
                        });
            }

            bool? isUatSession = Utils.IsUatSession(Session);
            if (isUatSession.HasValue)
            {
                if (isUatSession.Value)
                {
                    lblUat.Visible = true;
                }
            }
            else
            {
                lblUat.Visible = true;
                lblUat.Text = "(Unknown session type)";
            }


            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                //<asp:MenuItem NavigateUrl="~/RiskMgmtOverview.aspx" Text="Go To Switch"/>
                if (Utils.IsKgtUser(Session))
                {
                    NavigationMenu.Items.AddAt(0, new MenuItem("Risk Mgmt", null, null, "~/RiskMgmtOverviewInternal.aspx"));
                }
                else
                {
                    NavigationMenu.Items.AddAt(0, new MenuItem("Risk Management", null, null, "~/RiskMgmtOverview.aspx"));
                }
                NavigationMenu.Items.AddAt(1, new MenuItem("NOP", null, null, "~/NOP.aspx"));
                NavigationMenu.Items.AddAt(2, new MenuItem("Pos", null, null, "~/Positions.aspx"));

                if (Utils.GetIsAdmin(Session) && Utils.IsKgtUser(Session))
                {
                    NavigationMenu.Items.AddAt(3, new MenuItem("Systems", null, null, "~/Systems.aspx"));
                    NavigationMenu.Items.AddAt(4, new MenuItem("Schedule", null, null, "~/Schedule.aspx"));
                    NavigationMenu.Items.AddAt(5, new MenuItem("Overview", null, null, "~/SystemOverview.aspx"));
                }

                NavigationMenu.Items.AddAt(6, new MenuItem("Log", null, null, "~/Log.aspx"));
            }
        }
    }
}
