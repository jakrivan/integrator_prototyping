﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NOP.aspx.cs" Inherits="KGTKillSwitchWeb.UnsettledNop" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Common" %>
<%@ Import Namespace="KGTKillSwitchWeb" %>
<%@ Register TagPrefix="uc1" TagName="autorefreshcontainer" Src="~/AutorefreshContainer.ascx" %>
<%@ Register TagPrefix="uc1" Namespace="WebControls" Assembly="WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Unsettled NOPs</title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript">
            function AlterUpdateLink(hrefToAlter) {
                //if (!confirm("Are you sure you want to update current limit?"))
                //    return false;

                var newLimitTotal = document.getElementById("NewLimitTotalTxt").value;
                var newLockLimitTotal = document.getElementById("NewLockLimitTotalTxt").value;

                var newLimitPerDay = 0;
                var newLockLimitPerDay = 0;

                if (document.getElementById("NewLimitPerDayTxt") != undefined) {
                    newLimitPerDay = document.getElementById("NewLimitPerDayTxt").value;
                    newLockLimitPerDay = document.getElementById("NewLockLimitPerDayTxt").value;
                }

                if (hasHtmlChars(newLimitTotal) || hasHtmlChars(newLockLimitTotal) || hasHtmlChars(newLimitPerDay) || hasHtmlChars(newLockLimitPerDay)) {
                    alert("Entered value contains some of the disallowed chars: &, <, >, \', \"");
                    return false;
                }

                hrefToAlter.href +=
                    "LimitTotal=" + newLimitTotal + "&LockLimitTotal=" + newLockLimitTotal +
                    "&LimitPerDay=" + newLimitPerDay + "&LockLimitPerDay=" + newLockLimitPerDay;

                return true;
            }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <uc1:autorefreshcontainer runat="server" id="AC"  AutorefreshMillisecondsInterval="2000" AllowAutoRefresh="True">
    <ContentTemplate> 

        
        <h2>Currently unsettled NOPs by credit lines (counterparty-specific settlement time):</h2>
            
    <asp:GridView ID="UnsettledNopsGridView" runat="server"
            GridLines="None"  
            autogeneratecolumns="False"
        CssClass="mGrid"   
        RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow"
        OnRowDataBound="UnsettledNopsGridView_OnRowDataBound" OnRowCreated="UnsettledNopsGridView_OnRowCreated"
        RowStyle-HorizontalAlign="Right"
        >
        <Columns>  
            <asp:BoundField DataField="PrimeBrokerName" HeaderText="PB" ItemStyle-HorizontalAlign="Center"/>  
            <asp:BoundField DataField="CreditLineName" HeaderText="PB Credit Line" ItemStyle-HorizontalAlign="Center"/> 
            <asp:TemplateField>
                <HeaderTemplate>
                    Linked To
                </HeaderTemplate>
                <ItemStyle CssClass="maxWidthGrid"></ItemStyle>
                <ItemTemplate>
                    <%# string.Join(", ", GlobalState.GetCounteprartiesForCreditLine(((DbDataRecord)Container.DataItem)["CreditLineName"] as string)) %>  
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="DealsCountTotal" HeaderText="Total" ItemStyle-HorizontalAlign="Right" />  
            <asp:BoundField DataField="DealsCount_T1" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T2" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T3" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T4" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T5" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T6" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T7" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="DealsCount_T8" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            
            <asp:TemplateField>
                <HeaderTemplate>
                    Allowed Max Total
                </HeaderTemplate>
                <ItemTemplate>
                    <uc1:DataPlaceHolder ID="DataPlaceHolder1" runat="server" Visible='<%# this.IsEditedRow(((DbDataRecord)Container.DataItem)["CreditLineId"].ToString(), LimitType.CreditLine) %>'>
                        <input type="number" id="NewLimitTotalTxt" value='<%# string.IsNullOrEmpty(_limitSetting.GetValueString("LimitTotal")) ? string.Format("{0:f0}",((DbDataRecord)Container.DataItem)["MaxUnsettledNopUsdTotal"]) : _limitSetting.GetValueString("LimitTotal") %>'>             
                        <asp:PlaceHolder runat="server" Visible='<%# !string.IsNullOrEmpty(_limitSetting.GetErrorString("LimitTotal")) %>'>
                                <asp:Label runat="server" ForeColor="Red"><%# _limitSetting.GetErrorString("LimitTotal") %></asp:Label>
                        </asp:PlaceHolder>
                    </uc1:DataPlaceHolder>  
                    <uc1:DataPlaceHolder ID="DataPlaceHolder2" runat="server" Visible='<%# !this.IsEditedRow(((DbDataRecord)Container.DataItem)["CreditLineId"].ToString(), LimitType.CreditLine) %>'>
                        <%# string.Format("{0:n2}", ((DbDataRecord)Container.DataItem)["MaxUnsettledNopUsdTotal"]) %>               
                    </uc1:DataPlaceHolder>      
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                <HeaderTemplate>
                    Lockdown Limit Total
                </HeaderTemplate>
                <ItemTemplate>
                    <uc1:DataPlaceHolder ID="DataPlaceHolder1" runat="server" Visible='<%# this.IsEditedRow(((DbDataRecord)Container.DataItem)["CreditLineId"].ToString(), LimitType.CreditLine) %>'>
                        <input type="number" id="NewLockLimitTotalTxt" value='<%# string.IsNullOrEmpty(_limitSetting.GetValueString("LockLimitTotal")) ? string.Format("{0:f0}",((DbDataRecord)Container.DataItem)["MaxUnsettledNopUsdTotalToLockDestination"]) : _limitSetting.GetValueString("LockLimitTotal") %>'>             
                        <asp:PlaceHolder runat="server" Visible='<%# !string.IsNullOrEmpty(_limitSetting.GetErrorString("LockLimitTotal")) %>'>
                                <asp:Label runat="server" ForeColor="Red"><%# _limitSetting.GetErrorString("LockLimitTotal") %></asp:Label>
                        </asp:PlaceHolder>
                    </uc1:DataPlaceHolder>  
                    <uc1:DataPlaceHolder ID="DataPlaceHolder2" runat="server" Visible='<%# !this.IsEditedRow(((DbDataRecord)Container.DataItem)["CreditLineId"].ToString(), LimitType.CreditLine) %>'>
                        <%# string.Format("{0:n2}", ((DbDataRecord)Container.DataItem)["MaxUnsettledNopUsdTotalToLockDestination"]) %>               
                    </uc1:DataPlaceHolder>      
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                <HeaderTemplate>
                    Allowed Max Per Value Date
                </HeaderTemplate>
                <ItemTemplate>
                    <uc1:DataPlaceHolder ID="DataPlaceHolder1" runat="server" Visible='<%# this.IsEditedRow(((DbDataRecord)Container.DataItem)["CreditLineId"].ToString(), LimitType.CreditLine) %>'>
                        <input type="number" id="NewLimitPerDayTxt" value='<%# string.IsNullOrEmpty(_limitSetting.GetValueString("LimitPerDay")) ? string.Format("{0:f0}",((DbDataRecord)Container.DataItem)["MaxUnsettledNopUsdPerValueDate"]) : _limitSetting.GetValueString("LimitPerDay") %>'>             
                        <asp:PlaceHolder runat="server" Visible='<%# !string.IsNullOrEmpty(_limitSetting.GetErrorString("LimitPerDay")) %>'>
                                <asp:Label runat="server" ForeColor="Red"><%# _limitSetting.GetErrorString("LimitPerDay") %></asp:Label>
                        </asp:PlaceHolder>
                    </uc1:DataPlaceHolder>  
                    <uc1:DataPlaceHolder ID="DataPlaceHolder2" runat="server" Visible='<%# !this.IsEditedRow(((DbDataRecord)Container.DataItem)["CreditLineId"].ToString(), LimitType.CreditLine) %>'>
                        <%# string.Format("{0:n2}", ((DbDataRecord)Container.DataItem)["MaxUnsettledNopUsdPerValueDate"]) %>               
                    </uc1:DataPlaceHolder>      
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                <HeaderTemplate>
                    Lockdown Limit Per Value Date
                </HeaderTemplate>
                <ItemTemplate>
                    <uc1:DataPlaceHolder ID="DataPlaceHolder1" runat="server" Visible='<%# this.IsEditedRow(((DbDataRecord)Container.DataItem)["CreditLineId"].ToString(), LimitType.CreditLine) %>'>
                        <input type="number" id="NewLockLimitPerDayTxt" value='<%# string.IsNullOrEmpty(_limitSetting.GetValueString("LockLimitPerDay")) ? string.Format("{0:f0}",((DbDataRecord)Container.DataItem)["MaxUnsettledNopUsdPerValueDateToLockDestination"]) : _limitSetting.GetValueString("LockLimitPerDay") %>'>             
                        <asp:PlaceHolder runat="server" Visible='<%# !string.IsNullOrEmpty(_limitSetting.GetErrorString("LockLimitPerDay")) %>'>
                                <asp:Label runat="server" ForeColor="Red"><%# _limitSetting.GetErrorString("LockLimitPerDay") %></asp:Label>
                        </asp:PlaceHolder>
                    </uc1:DataPlaceHolder>  
                    <uc1:DataPlaceHolder ID="DataPlaceHolder2" runat="server" Visible='<%# !this.IsEditedRow(((DbDataRecord)Container.DataItem)["CreditLineId"].ToString(), LimitType.CreditLine) %>'>
                        <%# string.Format("{0:n2}", ((DbDataRecord)Container.DataItem)["MaxUnsettledNopUsdPerValueDateToLockDestination"]) %>               
                    </uc1:DataPlaceHolder>      
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:BoundField DataField="UnsettledNopUsdTotal" HeaderText="Total" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" />    
            <asp:BoundField DataField="UnsettledNopUsd_T1" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopUsd_T2" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopUsd_T3" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopUsd_T4" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopUsd_T5" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopUsd_T6" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopUsd_T7" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 
            <asp:BoundField DataField="UnsettledNopUsd_T8" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="False" /> 

            <asp:TemplateField>
                <ItemTemplate>
                    <uc1:DataPlaceHolder ID="DataPlaceHolder1" runat="server" Visible='<%# this.IsEditedRow(((DbDataRecord)Container.DataItem)["CreditLineId"].ToString(), LimitType.CreditLine) %>'>
                        <a href='<%# string.Format("?Type=CreditLine&Update={0}&", ((DbDataRecord)Container.DataItem)["CreditLineId"])%>'  onclick="return AlterUpdateLink(this);">Update</a>&nbsp;<a href='?'>Cancel</a>             
                    </uc1:DataPlaceHolder>  
                    <uc1:DataPlaceHolder ID="DataPlaceHolder2" runat="server" Visible='<%# !this.IsEditedRow(((DbDataRecord)Container.DataItem)["CreditLineId"].ToString(), LimitType.CreditLine) %>'>
                        <a href='<%# string.Format("?Type=CreditLine&Edit={0}", ((DbDataRecord)Container.DataItem)["CreditLineId"])%>'>Edit&nbsp;Limits</a>               
                    </uc1:DataPlaceHolder>      
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
        
    <br/>

     <h2>NOPs (abs) per PB and total for current UTC trading day:</h2>
        
     <asp:GridView ID="PbNopGridView" runat="server"
            GridLines="None"  
            autogeneratecolumns="False"
        CssClass="mGrid"   
        RowStyle-CssClass="gridRow" AlternatingRowStyle-CssClass="gridAltRow"
         OnRowDataBound="PbNopGridView_OnRowDataBound"
         RowStyle-HorizontalAlign="Right">
        <Columns>  
            <asp:BoundField DataField="PrimeBrokerName" HeaderText="PB" ItemStyle-HorizontalAlign="Center"/>  
            <asp:BoundField DataField="DealsCountCurrentTradeDate" HeaderText="Deals" /> 
            
            

            <%--<asp:BoundField DataField="MaxUnsettledNopUsdTotal" HeaderText="Allowed Max Total" />--%>
            <asp:TemplateField>
                <HeaderTemplate>
                    Allowed Max Total
                </HeaderTemplate>
                <ItemTemplate>
                    <uc1:DataPlaceHolder ID="DataPlaceHolder1" runat="server" Visible='<%# this.IsEditedRow(((DbDataRecord)Container.DataItem)["PrimeBrokerId"].ToString(), LimitType.PrimeBroker) %>'>
                        <input type="number" id="NewLimitTotalTxt" value='<%# string.IsNullOrEmpty(_limitSetting.GetValueString("LimitTotal")) ? string.Format("{0:f0}",((DbDataRecord)Container.DataItem)["MaxUnsettledNopUsdTotal"]) : _limitSetting.GetValueString("LimitTotal") %>'>             
                        <asp:PlaceHolder runat="server" Visible='<%# !string.IsNullOrEmpty(_limitSetting.GetErrorString("LimitTotal")) %>'>
                                <asp:Label runat="server" ForeColor="Red"><%# _limitSetting.GetErrorString("LimitTotal") %></asp:Label>
                        </asp:PlaceHolder>
                    </uc1:DataPlaceHolder>  
                    <uc1:DataPlaceHolder ID="DataPlaceHolder2" runat="server" Visible='<%# !this.IsEditedRow(((DbDataRecord)Container.DataItem)["PrimeBrokerId"].ToString(), LimitType.PrimeBroker) %>'>
                        <%# string.Format("{0:n2}", ((DbDataRecord)Container.DataItem)["MaxUnsettledNopUsdTotal"]) %>               
                    </uc1:DataPlaceHolder>      
                </ItemTemplate>
            </asp:TemplateField>

            <%--<asp:BoundField DataField="MaxUnsettledNopUsdTotalToLockDestination" HeaderText="Lockdown Limit Total" />--%>
            <asp:TemplateField>
                <HeaderTemplate>
                    Lockdown Limit Total
                </HeaderTemplate>
                <ItemTemplate>
                    <uc1:DataPlaceHolder ID="DataPlaceHolder1" runat="server" Visible='<%# this.IsEditedRow(((DbDataRecord)Container.DataItem)["PrimeBrokerId"].ToString(), LimitType.PrimeBroker) %>'>
                        <input type="number" id="NewLockLimitTotalTxt" value='<%# string.IsNullOrEmpty(_limitSetting.GetValueString("LockLimitTotal")) ? string.Format("{0:f0}",((DbDataRecord)Container.DataItem)["MaxUnsettledNopUsdTotalToLockDestination"]) : _limitSetting.GetValueString("LockLimitTotal") %>'>             
                        <asp:PlaceHolder runat="server" Visible='<%# !string.IsNullOrEmpty(_limitSetting.GetErrorString("LockLimitTotal")) %>'>
                                <asp:Label runat="server" ForeColor="Red"><%# _limitSetting.GetErrorString("LockLimitTotal") %></asp:Label>
                        </asp:PlaceHolder>
                    </uc1:DataPlaceHolder>  
                    <uc1:DataPlaceHolder ID="DataPlaceHolder2" runat="server" Visible='<%# !this.IsEditedRow(((DbDataRecord)Container.DataItem)["PrimeBrokerId"].ToString(), LimitType.PrimeBroker) %>'>
                        <%# string.Format("{0:n2}", ((DbDataRecord)Container.DataItem)["MaxUnsettledNopUsdTotalToLockDestination"]) %>               
                    </uc1:DataPlaceHolder>      
                </ItemTemplate>
            </asp:TemplateField>
            

            <asp:BoundField DataField="NopAbsUsdCurrentTradeDate" HeaderText="NOP (abs USD)" ItemStyle-Font-Bold="True" />
            <asp:BoundField DataField="CheckingZone" HeaderText="Risk Check Zone" ItemStyle-HorizontalAlign="Center"/>
            
            <asp:TemplateField>
                <ItemTemplate>
                    <uc1:DataPlaceHolder ID="DataPlaceHolder1" runat="server" Visible='<%# this.IsEditedRow(((DbDataRecord)Container.DataItem)["PrimeBrokerId"].ToString(), LimitType.PrimeBroker) %>'>
                        <a href='<%# string.Format("?Type=PrimeBroker&Update={0}&", ((DbDataRecord)Container.DataItem)["PrimeBrokerId"])%>'  onclick="return AlterUpdateLink(this);">Update</a>&nbsp;<a href='?'>Cancel</a>             
                    </uc1:DataPlaceHolder>  
                    <uc1:DataPlaceHolder ID="DataPlaceHolder2" runat="server" Visible='<%# !this.IsEditedRow(((DbDataRecord)Container.DataItem)["PrimeBrokerId"].ToString(), LimitType.PrimeBroker) %>'>
                        <a href='<%# string.Format("?Type=PrimeBroker&Edit={0}", ((DbDataRecord)Container.DataItem)["PrimeBrokerId"])%>'>Edit&nbsp;Limits</a>               
                    </uc1:DataPlaceHolder>      
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
     </asp:GridView>

            
    </ContentTemplate>
    </uc1:autorefreshcontainer>
    <br/>
    * Cells coloring and tooltips shows the current status of unsettled NOP. The limit cells are adjusted based on data from backend, the individual NOP cells are adjusted based on direct info from integrator.

    

</asp:Content>
