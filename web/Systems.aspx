﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" EnableViewState="false" ViewStateMode="Disabled"  AutoEventWireup="true" CodeBehind="Systems.aspx.cs" Inherits="KGTKillSwitchWeb.Systems" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="KGTKillSwitchWeb" %>

<%@ Register Src="~/AutorefreshContainer.ascx" TagPrefix="uc1" TagName="AutorefreshContainer" %>
<%@ Register TagPrefix="uc1" Namespace="WebControls" Assembly="WebControls" %>
<%@ Register Src="~/RotatedText.ascx" TagPrefix="uc1" TagName="RotatedText" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Integrated systems overview</title>
    <script type="text/javascript">
        function AlterGroupRenamingLink(hrefToAlter, changeType, systemGroupId) {
            if (!confirm("Are you sure you want to " + changeType + " current Group?"))
                return false;

            var groupNameTxt = document.getElementById("GroupNameTxt");
            var groupName = groupNameTxt.value;

            if (hasHtmlChars(groupName)) {
                alert("Entered group name contains some of the disallowed chars: &, <, >, \', \"");
                return false;
            }

            hrefToAlter.href = "?Group" + changeType + "Done=" + groupName + "&SystemGroupId=" + systemGroupId;

            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <uc1:autorefreshcontainer runat="server" id="AC"  AutorefreshMillisecondsInterval="1000" AllowAutoRefresh="True">
    <ContentTemplate>


    <h1>Systems Home Page</h1>
        <br/>
        <br/>
        <p class="SystemsHompegeHeader">Global online statistics for today (UTC)</p>
        <br/>
    
    <asp:repeater ID="GlobalStatsRepeater" runat="server">

        <HeaderTemplate>
            <table class="SingleTable SingleTableMoreSpace">
            <thead>
            <tr>
                <th>
                    System
                </th>
                <th class="DividerCell"></th>

                <th> <uc1:RotatedText runat="server" Value="Formed Systems" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Active Systems" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Blocked Systems" /> </th>
                <th class="DividerCell"></th>
                
                <th> <uc1:RotatedText runat="server" Value="Volume USD M" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Volume %" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Deals" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Cpt Rejections" /> </th>
                <th> <uc1:RotatedText runat="server" Value="KGT Rejections" /> </th>
                <th class="DividerCell"></th>
                
                <th> <uc1:RotatedText runat="server" Value="Pnl Gross USD" /> <uc1:RotatedText runat="server" Value="Per M USD" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Comm USD PB" /> <uc1:RotatedText runat="server" Value="Per M USD" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Comm USD Cpt" /> <uc1:RotatedText runat="server" Value="Per M USD" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Comm USD Sum" /> <uc1:RotatedText runat="server" Value="Per M USD" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Pnl Net USD" /> <uc1:RotatedText runat="server" Value="Per M USD" /> </th>
                <th class="DividerCell"></th>
                
                <th> <uc1:RotatedText runat="server" Value="Pnl Gross USD" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Comm USD PB" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Comm USD Cpt" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Comm USD Sum" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Pnl Net USD" /> </th>
                
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="DividerCell"/>
                </tr>
        </HeaderTemplate>

        <ItemTemplate>
                <tr title='<%# ((DataRowView)Container.DataItem)["SystemName"] %>'>
                    <td class="HeaderColumn"><%# ((DataRowView)Container.DataItem)["SystemName"] %></td>
                    <td class="DividerCell"></td>
                    <td><%# ((DataRowView)Container.DataItem)["ConfiguredSystems"] %></td>
                    <td class='<%# (int)((DataRowView)Container.DataItem)["EnabledSystems"] > 0 ? "ActiveSystem" : string.Empty %>'><%# ((DataRowView)Container.DataItem)["EnabledSystems"] %></td>
                    <td class='<%# (int)((DataRowView)Container.DataItem)["HardBlockedSystems"] > 0 ? "ActiveSystem" : string.Empty %>'><%# ((DataRowView)Container.DataItem)["HardBlockedSystems"] %></td>
                    <td class="DividerCell"></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["VolumeUsdM"], 1, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["VolumePct"], 1, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td class='<%# Utils.GetDealActionCellStyle(Container.DataItem, Utils.DealAction.KgtDeal) %>'>
                        <%# ((DataRowView)Container.DataItem)["DealsNum"] %>
                    </td>
                    <td class='<%# Utils.GetDealActionCellStyle(Container.DataItem, Utils.DealAction.CptRejection) %>'>
                        <%# ((DataRowView)Container.DataItem)["CtpRejectionsNum"] %>
                    </td>
                    <td class='<%# Utils.GetDealActionCellStyle(Container.DataItem, Utils.DealAction.KgtRejection) %>'>
                        <%# ((DataRowView)Container.DataItem)["KGTRejectionsNum"] %>
                    </td>
                    <td class="DividerCell"></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["PnlGrossUsdPerMUsd"], 2, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["CommissionsPbUsdPerMUsd"], 2, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["CommissionsCptUsdPerMUsd"], 2, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["CommissionsUsdPerMUsd"], 2, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["PnlNetUsdPerMUsd"], 2, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td class="DividerCell"></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["PnlGrossUsd"], 0, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["CommissionsPbUsd"], 0, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["CommissionsCptUsd"], 0, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["CommissionsUsd"], 0, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td class='<%# Utils.GetNumberCellStyle(((DataRowView)Container.DataItem)["PnlNetUsd"])  %>'>
                        <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["PnlNetUsd"], 0, Utils.NullsHandling.DbNullToEmpty) %>
                    </td>
                </tr>      
        </ItemTemplate>
        
        <FooterTemplate>
            </tbody>
            <tfoot>
                <tr><td class="DividerCell"/></tr>
                <tr class="FooterRow">
                    <td style="text-align: center">&Sigma;</td>
                    <td class="DividerCell"/>
                    <td runat="server" ID="FooterCell_ConfiguredSystems"></td>
                    <td runat="server" ID="FooterCell_ActiveSystems"></td>
                    <td runat="server" ID="FooterCell_HardBlockedSystems"></td>
                    <td class="DividerCell"/>
                    <td runat="server" ID="FooterCell_Volume"></td>
                    <td runat="server" ID="FooterCell_VolumePct"></td>
                    <td runat="server" ID="FooterCell_Deals"></td>
                    <td runat="server" ID="FooterCell_CtpRejections"></td>
                    <td runat="server" ID="FooterCell_KGTRejections"></td>
                    <td class="DividerCell"/>
                    <td runat="server" ID="FooterCell_PnlGrossPerM"></td>
                    <td runat="server" ID="FooterCell_CommPbPerM"></td>
                    <td runat="server" ID="FooterCell_CommCptPerM"></td>
                    <td runat="server" ID="FooterCell_CommPerM"></td>
                    <td runat="server" ID="FooterCell_PnlNetPerM"></td>
                    <td class="DividerCell"/>
                    <td runat="server" ID="FooterCell_PnlGross"></td>
                    <td runat="server" ID="FooterCell_CommPb"></td>
                    <td runat="server" ID="FooterCell_CommCpt"></td>
                    <td runat="server" ID="FooterCell_Comm"></td>
                    <td runat="server" ID="FooterCell_PnlNet"></td>
                </tr>
            </tfoot>
            </table> 
        </FooterTemplate>

    </asp:repeater>    
     
    
    
    <br/>
    <br/>
    <br/>
        
    <asp:Repeater ID="GroupsRepeater" runat="server">
        <HeaderTemplate>
            
        </HeaderTemplate>
        <ItemTemplate>
            
        <table class="TradingGroupsTable">
        <tbody>
        <tr>
            <td>
                <uc1:DataPlaceHolder runat="server" Visible='<%# !((DataRowView)Container.DataItem)["TradingSystemGroupId"].Equals(_groupIdToModify) %>' >
                    <a href='<%# "TradingGroup.aspx?GroupId=" + ((DataRowView)Container.DataItem)["TradingSystemGroupId"] %>' style="text-decoration: none"
                        target="_blank" onclick="return popitup(this.href);" onkeypress="return popitup(this.href);">
                    <h1 style="color: white"><b><%# ((DataRowView)Container.DataItem)["TradingSystemGroup"] %></b> - Trading Systems Group</h1>
                    </a>
                </uc1:DataPlaceHolder>
                <uc1:DataPlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["TradingSystemGroupId"].Equals(_groupIdToModify) %>' >
                    <h1 style="color: white">
                        <input type="text" id="GroupNameTxt" maxlength="32" value='<%# string.IsNullOrEmpty(_groupName) ? ((DataRowView)Container.DataItem)["TradingSystemGroup"] : _groupName %>'/>
                        <asp:PlaceHolder runat="server" Visible="<%# !string.IsNullOrEmpty(this._validationErrorMsg) %>">
                                <asp:Label runat="server" ForeColor="Red"><%# this._validationErrorMsg %></asp:Label>
                        </asp:PlaceHolder>
                         - Trading Systems Group
                    </h1>    
                </uc1:DataPlaceHolder>
            </td>
            <td class="ActionLinks">
                
                <uc1:DataPlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["TradingSystemGroupId"].Equals(_groupIdToModify) %>' >
                    <a href="#" onclick="return AlterGroupRenamingLink(this, 'Rename', '<%# ((DataRowView)Container.DataItem)["TradingSystemGroupId"] %>');" >Confirm Renaming</a>&nbsp;&nbsp;<a href="?">Cancel Renaming</a>
                </uc1:DataPlaceHolder>
                <uc1:DataPlaceHolder runat="server" Visible='<%# !((DataRowView)Container.DataItem)["TradingSystemGroupId"].Equals(_groupIdToModify) %>' >
                    <a href='<%# "?RenameGroup=" + ((DataRowView)Container.DataItem)["TradingSystemGroupId"] %>'>
                            Rename Group
                    </a>
                </uc1:DataPlaceHolder>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href='<%# "?DeleteGroup=" + ((DataRowView)Container.DataItem)["TradingSystemGroupId"]%>' onclick="return confirm('Do you really wish to delete selected group with all the systems')">
                            Remove Group
                </a>
                
                
                
                

            </td>
        </tr>
        </tbody>
        </table>
        <br/>             


        </ItemTemplate>
        <FooterTemplate>
            
            <uc1:DataPlaceHolder runat="server" Visible='<%# !_addGroup %>' >
                <a href="?AddGroup=true" style="color: black" onclick="DisableAutoRefresh();">Add new Trading Systems Group</a>
            </uc1:DataPlaceHolder>
            
            <uc1:DataPlaceHolder runat="server" Visible='<%# _addGroup %>' >
                <table class="TradingGroupsTable">
                    <tbody>
                        <tr>
                            <td>
                                <h1 style="color: white">
                                    New Trading Systems Group Name:
                                    <input type="text" id="GroupNameTxt" maxlength="32" value='<%# _groupName %>'/>
                                    <asp:PlaceHolder runat="server" Visible="<%# !string.IsNullOrEmpty(this._validationErrorMsg) %>">
                                            <asp:Label runat="server" ForeColor="Red"><%# this._validationErrorMsg %></asp:Label>
                                    </asp:PlaceHolder>
                                </h1>
                            </td>
                            <td class="ActionLinks">
                                <a href="#" onclick="return AlterGroupRenamingLink(this, 'Add', null);" >Confirm</a>&nbsp;&nbsp;<a href="?">Cancel</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </uc1:DataPlaceHolder>
            
            

        </FooterTemplate>
    </asp:Repeater>    
        
        
        
    </ContentTemplate>
    </uc1:autorefreshcontainer>

</asp:Content>
