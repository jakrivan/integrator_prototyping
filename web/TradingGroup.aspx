﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TradingGroup.aspx.cs" Inherits="KGTKillSwitchWeb.TradingGroup" %>
<%@ Import Namespace="System.Data" %>
<%@ Register Src="~/AutorefreshContainer.ascx" TagPrefix="uc1" TagName="AutorefreshContainer" %>
<%@ Import Namespace="KGTKillSwitchWeb" %>
<%@ Register TagPrefix="uc1" Namespace="WebControls" Assembly="WebControls" %>
<%@ Register Src="~/RotatedText.ascx" TagPrefix="uc1" TagName="RotatedText" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <title><%= Utils.GetUatPrefix(Session) + this.GroupName %> - Trading System Group</title>
        <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    <%--do not include here as it is included from autoupdate compoent - double including prevents functionality--%>
    <%--<script type="text/javascript" src="Scripts/KGTScripts.js"></script>--%>
        <script type="text/javascript">
            function AlterChangeLink(hrefToAlter, urlQueryStarter, changeType, pageId, systemTypeId, counterparty) {
                if (!confirm("Are you sure you want to " + changeType + " current record?"))
                    return false;

                var beginSymDropDown = document.getElementById("StartSymbolDropDown");
                var beginSym = beginSymDropDown.options[beginSymDropDown.selectedIndex].value;

                var endSymDropDown = document.getElementById("EndSymbolDropDown");
                var endSym = endSymDropDown.options[endSymDropDown.selectedIndex].value;

                var stripsDropDown = document.getElementById("StripsDropDown");
                var stripsNum = stripsDropDown.options[stripsDropDown.selectedIndex].value;

                hrefToAlter.href = urlQueryStarter + "PagesPlan" + changeType + "OneDone=" + pageId + "&SystemTypeId=" + systemTypeId + "&Ctp=" + counterparty + "&BeginSymbol=" + beginSym + "&EndSymbol=" + endSym + "&StripsNum=" + stripsNum;

                return true;
            }

            function AlterAddTypeLink(hrefToAlter, urlQueryStarter) {
                if (!confirm("Are you sure you want to add selected system type?"))
                    return false;

                var tradingTypeDropDown = document.getElementById("TradingTypeDropDown");
                var tradingType = tradingTypeDropDown.options[tradingTypeDropDown.selectedIndex].value;

                hrefToAlter.href = urlQueryStarter + "AddSystemTypeDone=" + tradingType;

                return true;
            }

            function AlterGroupRenamingLink(hrefToAlter, urlQueryStarter, changeType) {
                if (!confirm("Are you sure you want to " + changeType + " current Group?"))
                    return false;

                var groupNameTxt = document.getElementById("GroupNameTxt");
                var groupName = groupNameTxt.value;

                if (hasHtmlChars(groupName)) {
                    alert("Entered group name contains some of the disallowed chars: &, <, >, \', \"");
                    return false;
                }

                hrefToAlter.href = urlQueryStarter + "Group" + changeType + "Done=" + groupName;

                return true;
            }
        </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <uc1:autorefreshcontainer runat="server" id="AC"  AutorefreshMillisecondsInterval="1000" AllowAutoRefresh="True" OnRefreshUrlAdjustRequested="AdjustUrlQuery">
    <ContentTemplate>    

        
     <div style="padding: 10px">  

            <asp:Repeater ID="GroupStatsRepeater"  runat="server">
                <HeaderTemplate>
                    
                    <table class="TradingGroupsTable">
        <tbody>
        <tr>
            <td>
                <uc1:DataPlaceHolder runat="server" Visible='<%# !_rename %>' >
                    <h1 style="color: white"><asp:PlaceHolder runat="server" Visible='<%# Utils.IsUatSession(Session).HasValue && Utils.IsUatSession(Session).Value %>'><span style="color: red; font-weight: bold">UAT </span></asp:PlaceHolder><b><%# this.GroupName %></b> - Trading Systems Group</h1>
                </uc1:DataPlaceHolder>
                <uc1:DataPlaceHolder runat="server" Visible='<%# _rename %>' >
                    <h1 style="color: white">
                        <input type="text" id="GroupNameTxt" maxlength="32" value='<%# string.IsNullOrEmpty(_groupName) ? this.GroupName : _groupName %>'/>
                        <asp:PlaceHolder runat="server" Visible="<%# !string.IsNullOrEmpty(this._validationErrorMsg) %>">
                                <asp:Label runat="server" ForeColor="Red"><%# this._validationErrorMsg %></asp:Label>
                        </asp:PlaceHolder>
                         - Trading Systems Group
                    </h1>    
                </uc1:DataPlaceHolder>
            </td>
            <td class="ActionLinks">
                
                <uc1:DataPlaceHolder runat="server" Visible='<%# _rename %>' >
                    <a href="#" onclick="return AlterGroupRenamingLink(this, '<%# UrlQueryStartString %>', 'Rename');" >Confirm Renaming</a>&nbsp;&nbsp;<a href='<%# AdjustUrlQuery(null) %>'>Cancel Renaming</a>
                </uc1:DataPlaceHolder>
                <uc1:DataPlaceHolder runat="server" Visible='<%# !_rename %>' >
                    <a href='<%# AdjustUrlQuery("RenameGroup=true") %>'>
                            Rename Group
                    </a>
                </uc1:DataPlaceHolder>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href='<%# _hasActiveSystems ? 
                            "javascript:alert(\"You cannot delete trading group with active systems\")" : 
                            AdjustUrlQuery("DeleteGroup=true") %>' onclick="return confirm('Do you really wish to delete selected group with all the systems')">
                            Remove Group
                </a>
                
                
                
                

            </td>
        </tr>
        <tr>
            <td colspan="2">
                                
        <%-- inner table start  --%>  
                    
    <table class="SingleTable SingleTableMoreSpace">
    <thead>
    <tr class="AboveHeaderRow">
        <th colspan="20">
            Online statistics for today (UTC)
        </th>
        <th colspan="10">
            Risk management
        </th>
        <th colspan="9">
            Monitoring pages plan
        </th>
    </tr>
        <%--divider of the header - to prevent connecting line in header--%>
    <tr></tr>
    <tr>
        <th rowspan="2">System</th>
        <th rowspan="2" class="DividerCell"></th>

        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Formed Systems" /> </th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Active Systems" /> </th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Blocked Systems" /> </th>
        <th rowspan="2" class="DividerCell"></th>
        
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Volume USD M" /> </th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Volume %" /> </th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Deals" /> </th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Cpt Rejections" /> </th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="KGT Rejections" /> </th>
        <th rowspan="2" class="DividerCell"></th>
                
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Pnl Gross USD" /> <uc1:RotatedText runat="server" Value="Per M USD" /> </th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Comm USD" /> <uc1:RotatedText runat="server" Value="Per M USD" /> </th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Pnl Net USD" /> <uc1:RotatedText runat="server" Value="Per M USD" /> </th>
        <th rowspan="2" class="DividerCell"></th>
                
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Pnl Gross USD" /> </th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Comm USD" /> </th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Pnl Net USD" /> </th>
                
        <th rowspan="2" class="WideDividerCell"></th>
                
        <th rowspan="2">System</th>
        <th colspan="2"> <uc1:RotatedText runat="server" Value="Kill Switch" /> </th>
        <th colspan="2"> <uc1:RotatedText runat="server" Value="Go Flat Only" /> </th>
        <th colspan="2">On/Off</th>
        <th colspan="2" title="Reset or Unblock all applicable systems (all except running ones)"> <uc1:RotatedText runat="server" Value="Reset All" /> </th>
        <th rowspan="2" class="WideDividerCell"></th>
                
        <th rowspan="2">System</th>
        <th rowspan="2" class="DividerCell"></th>

        <th rowspan="2">Symbols Range</th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Strips" /> </th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Remove" /> </th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Strips" /> </th>
        <th rowspan="2" class="DividerCell"></th>
        <th rowspan="2"> <uc1:RotatedText runat="server" Value="Spare Symbols" /> </th>
        <th rowspan="2" class="WideDividerCell"></th>

        <th rowspan="2">
            <img src="Styles/Icon-Remove-16-pixels.png" alt="Remove Trading System Type from Group" title="Remove Trading System Type from Group"/>
        </th>

    </tr>
        <tr>
            <th style="padding: 2px 5px;">
                <a href='<%# AdjustUrlQuery("IsOrderTransmissionDisabled=false") %>' 
                    onclick="if(!confirm('Are you sure you want to allow trading for the whole group?')) return false; DisableAutoRefresh();">    
                    <img src="Styles/Icon-On-16-pixels.png" />
                </a>
            </th>
            <th style="padding: 2px 5px;">
                <a href='<%# AdjustUrlQuery("IsOrderTransmissionDisabled=true") %>' 
                    onclick="DisableAutoRefresh();">    
                    <img src="Styles/Icon-Off-16-pixels.png" />
                </a>          
            </th>
            <th style="padding: 2px 5px;">
                <a href='<%# AdjustUrlQuery("IsGoFlatOnlyOn=false") %>' 
                    onclick="if(!confirm('Are you sure you want to allow trading for the whole group?')) return false; DisableAutoRefresh();">    
                    <img src="Styles/Icon-On-16-pixels.png" />
                </a>
            </th>
            <th style="padding: 2px 5px;">
                <a href='<%# AdjustUrlQuery("IsGoFlatOnlyOn=true") %>' 
                    onclick="DisableAutoRefresh();">    
                    <img src="Styles/Icon-Off-16-pixels.png" />
                </a>          
            </th>
            <th>
                <a href='<%# AdjustUrlQuery("Disable=false") %>' 
                    onclick="if(!confirm('Are you sure you want to allow trading for the whole group?')) return false; DisableAutoRefresh();">    
                    <img src="Styles/Icon-On-16-pixels.png" />
                </a>
            </th>
            <th>
                <a href='<%# AdjustUrlQuery("Disable=true") %>' 
                    onclick="DisableAutoRefresh();">    
                    <img src="Styles/Icon-Off-16-pixels.png" />
                </a>          
            </th>
            <th title="Reset or Unblock all applicable systems (all except running ones)">
                <a href='<%# AdjustUrlQuery("ResetAll=true") %>' 
                    onclick="DisableAutoRefresh();">    
                    <img src="Styles/Icon-Reset-16-pixels.png" />
                </a>          
            </th>
        </tr>

    </thead>           
    <tbody>
                    

                </HeaderTemplate>    
                
                
                <ItemTemplate>
                <asp:PlaceHolder  runat="server" Visible='<%# ((DataRowView)Container.DataItem)["TradingSystemTypeId"] == DBNull.Value %>'> 
                <tr>
                    <td class="DividerCell"/>
                </tr>
                </asp:PlaceHolder>  
                    
                    
                <uc1:DataPlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["TradingSystemTypeId"] != DBNull.Value %>'>

                <tr title='<%# ((DataRowView)Container.DataItem)["TradingSystemTypeName"] %>' class='<%# ((DataRowView)Container.DataItem)["ConfiguredSystems"] == DBNull.Value ? "noHighlight" : string.Empty %>'>
                    
                    <uc1:DataPlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["ConfiguredSystems"] != DBNull.Value %>'>

                    <td class="HeaderColumn"><%# ((DataRowView)Container.DataItem)["TradingSystemTypeName"].ToString().Replace(" ", "&nbsp;") %></td>
                    <td class="DividerCell"></td>
                    <td><%# ((DataRowView)Container.DataItem)["ConfiguredSystems"] %></td>
                    <td class='<%# (int)((DataRowView)Container.DataItem)["EnabledSystems"] > 0 ? "ActiveSystem" : string.Empty %>'><%# ((DataRowView)Container.DataItem)["EnabledSystems"] %></td>
                    <td class='<%# (int)((DataRowView)Container.DataItem)["HardBlockedSystems"] > 0 ? "ActiveSystem" : string.Empty %>'><%# ((DataRowView)Container.DataItem)["HardBlockedSystems"] %></td>
                    <td class="DividerCell"></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["VolumeUsdM"], 1, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["VolumePct"], 1, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td class='<%# Utils.GetDealActionCellStyle(Container.DataItem, Utils.DealAction.KgtDeal) %>'>
                        <%# ((DataRowView)Container.DataItem)["DealsNum"] %>
                    </td>
                    <td class='<%# Utils.GetDealActionCellStyle(Container.DataItem, Utils.DealAction.CptRejection) %>'>
                        <%# ((DataRowView)Container.DataItem)["CtpRejectionsNum"] %>
                    </td>
                    <td class='<%# Utils.GetDealActionCellStyle(Container.DataItem, Utils.DealAction.KgtRejection) %>'>
                        <%# ((DataRowView)Container.DataItem)["KGTRejectionsNum"] %>
                    </td>
                    <td class="DividerCell"></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["PnlGrossUsdPerMUsd"], 2, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["CommissionsUsdPerMUsd"], 2, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["PnlNetUsdPerMUsd"], 2, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td class="DividerCell"></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["PnlGrossUsd"], 0, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["CommissionsUsd"], 0, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td class='<%# Utils.GetNumberCellStyle(((DataRowView)Container.DataItem)["PnlNetUsd"])  %>'>
                        <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["PnlNetUsd"], 0, Utils.NullsHandling.DbNullToEmpty) %>
                    </td>
                    
                    <td class="WideDividerCell"></td>
                 
                    <td class="HeaderColumn"><%# ((DataRowView)Container.DataItem)["TradingSystemTypeName"].ToString().Replace(" ", "&nbsp;") %></td>
                    <td colspan="2" class="TableImageButtonCentered" >
                        <asp:PlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["IsOrderTransmissionDisabled"].Equals(true) %>'>
                            <a href='<%# AdjustUrlQuery("IsOrderTransmissionDisabled=false&TradingSystemTypeId=" + ((DataRowView)Container.DataItem)["TradingSystemTypeId"]) %>' onclick="if(!confirm('Are you sure you want to allow trading for selected system type?')) return false; DisableAutoRefresh();">    
                            <img src="Styles/Icon-Off-16-pixels.png" />
                            </a>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["IsOrderTransmissionDisabled"].Equals(false) %>'>
                            <a href='<%# AdjustUrlQuery("IsOrderTransmissionDisabled=true&TradingSystemTypeId=" + ((DataRowView)Container.DataItem)["TradingSystemTypeId"]) %>' onclick="DisableAutoRefresh();">
                            <img src="Styles/Icon-On-16-pixels.png" />
                            </a>
                        </asp:PlaceHolder>
                    </td>
                    <td colspan="2" class="TableImageButtonCentered">
                        <asp:PlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["IsGoFlatOnlyOn"].Equals(true) %>'>
                            <a href='<%# AdjustUrlQuery("IsGoFlatOnlyOn=false&TradingSystemTypeId=" + ((DataRowView)Container.DataItem)["TradingSystemTypeId"]) %>' onclick="if(!confirm('Are you sure you want to allow trading for selected system type?')) return false; DisableAutoRefresh();">    
                            <img src="Styles/Icon-Off-16-pixels.png" />
                            </a>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["IsGoFlatOnlyOn"].Equals(false) %>'>
                            <a href='<%# AdjustUrlQuery("IsGoFlatOnlyOn=true&TradingSystemTypeId=" + ((DataRowView)Container.DataItem)["TradingSystemTypeId"]) %>' onclick="DisableAutoRefresh();">
                            <img src="Styles/Icon-On-16-pixels.png" />
                            </a>
                        </asp:PlaceHolder>
                    </td>
                    <td colspan="2" class="TableImageButtonCentered">
                        <a href='<%# AdjustUrlQuery("Disable=false&TradingSystemTypeId=" + ((DataRowView)Container.DataItem)["TradingSystemTypeId"]) %>' onclick="if(!confirm('Are you sure you want to allow trading for selected system type?')) return false; DisableAutoRefresh();">    
                        <img src="Styles/Icon-On-16-pixels.png" style="padding-right: 5px"/>
                        </a>
                        <a href='<%# AdjustUrlQuery("Disable=true&TradingSystemTypeId=" + ((DataRowView)Container.DataItem)["TradingSystemTypeId"]) %>' onclick="DisableAutoRefresh();">
                        <img src="Styles/Icon-Off-16-pixels.png" style="padding-left: 5px"/>
                        </a>
                    </td>
                    <td colspan="2" title="Reset or Unblock all applicable systems (all except running ones)">
                        <a href='<%# AdjustUrlQuery("ResetAll=true&TradingSystemTypeId=" + ((DataRowView)Container.DataItem)["TradingSystemTypeId"]) %>' onclick="DisableAutoRefresh();">
                        <img src="Styles/Icon-Reset-16-pixels.png" style="padding-left: 5px"/>
                        </a>
                    </td>
                 
                    <td class="WideDividerCell"></td>
                        
                    </uc1:DataPlaceHolder>
                    <uc1:DataPlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["ConfiguredSystems"] == DBNull.Value %>'>

                    <td class="DividerCell" colspan="1"></td>
                    <td colspan="29" style="border: none"></td>
                        
                    </uc1:DataPlaceHolder>
                    
                    
                    <asp:PlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["SystemSubrows"] != DBNull.Value %>'> 
                        <td rowspan='<%# ((DataRowView)Container.DataItem)["SystemSubrows"] %>' class="HeaderColumn">
                            <%--Enable add clicking only if there is something to add--%>
                            <asp:PlaceHolder runat="server" Visible='<%# !string.IsNullOrWhiteSpace(((DataRowView)Container.DataItem)["AvailableSymbols"] as string) %>'>
                                <a href='<%# AdjustUrlQuery("PagesPlanAddOne=" + ((DataRowView)Container.DataItem)["TradingSystemTypeId"]) %>' onclick="DisableAutoRefresh();"><%# ((DataRowView)Container.DataItem)["TradingSystemTypeName"].ToString().Replace(" ", "&nbsp;") %> </a>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible='<%# string.IsNullOrWhiteSpace(((DataRowView)Container.DataItem)["AvailableSymbols"] as string) %>'>
                                <%# ((DataRowView)Container.DataItem)["TradingSystemTypeName"].ToString().Replace(" ", "&nbsp;") %>
                            </asp:PlaceHolder>
                        </td>
                    </asp:PlaceHolder>
                    <td class="DividerCell"></td>
                    <asp:PlaceHolder runat="server" Visible='<%# !((DataRowView)Container.DataItem)["PageId"].Equals(this._pageIdToModify) && !((DataRowView)Container.DataItem)["IsInsertRow"].Equals(true) %>'>
                    <td><uc1:DataPlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["BeginSymbol"] != DBNull.Value %>'>
                            <a href='<%# string.Format(
                            "SystemDetails.aspx?TradingSystemType={0}&VenueCounterparty={1}&GroupId={2}&PageId={3}&StartSymbol={4}&EndSymbol={5}&NumOfStrips={6}",  
                            ((string) ((DataRowView)Container.DataItem)["TradingSystemTypeName"]).Split()[1], 
                            ((DataRowView)Container.DataItem)["Counterparty"],
                            ((DataRowView)Container.DataItem)["TradingSystemGroupId"],
                            ((DataRowView)Container.DataItem)["PageId"],
                            ((string) ((DataRowView)Container.DataItem)["BeginSymbol"]).Replace("/", string.Empty), 
                            ((string) ((DataRowView)Container.DataItem)["EndSymbol"]).Replace("/", string.Empty), 
                            ((DataRowView)Container.DataItem)["StripsCount"]
                            ) %>' target="_blank"
                                onclick="return popitup(this.href);" 
                                onkeypress="return popitup(this.href);"><%# ((DataRowView)Container.DataItem)["BeginSymbol"] %>&nbsp;-&nbsp;<%# ((DataRowView)Container.DataItem)["EndSymbol"] %></a>
                        </uc1:DataPlaceHolder></td>
                    <td><%# ((DataRowView)Container.DataItem)["StripsCount"] %></td>
                    <td><asp:PlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["PageId"] != DBNull.Value %>'>
                        <a href='<%# AdjustUrlQuery("PagesPlanRemoveOne=" + ((DataRowView)Container.DataItem)["PageId"]) %>' onclick="if(!confirm('Are you sure you want to remove selected page?')) return false; DisableAutoRefresh();">    
                        <img src="Styles/Icon-Remove-Faded-16-pixels.png" alt="Remove"/>
                        </a>
                        </asp:PlaceHolder></td>
                    <td><asp:PlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["PageId"] != DBNull.Value %>'>
                            <a href='<%# AdjustUrlQuery("PagesPlanModifyOne=" + ((DataRowView)Container.DataItem)["PageId"]) %>' onclick="DisableAutoRefresh();">  
                            <img src="Styles/Icon-Modify-16-pixels.png" alt="Modify"/>
                            </a>
                        </asp:PlaceHolder>
                    </td>
                    </asp:PlaceHolder>
                    <uc1:DataPlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["PageId"].Equals(this._pageIdToModify) || ((DataRowView)Container.DataItem)["IsInsertRow"].Equals(true) %>'>
                        <td>
                            <div style="white-space: nowrap;">
                            <asp:DropDownList ID="StartSymbolDropDown" ClientIDMode="Static" runat="server" OnPreRender="SymbolDropDown_OnPreRender" ToolTip='<%# string.IsNullOrEmpty(_beginSymbol) ? ((DataRowView)Container.DataItem)["BeginSymbol"] : _beginSymbol %>' Ctp='<%# ((DataRowView)Container.DataItem)["Counterparty"] %>'>
                            </asp:DropDownList>
                            -
                            <asp:DropDownList runat="server" ID="EndSymbolDropDown" ClientIDMode="Static" OnPreRender="SymbolDropDown_OnPreRender" ToolTip='<%# string.IsNullOrEmpty(_endSymbol) ? ((DataRowView)Container.DataItem)["EndSymbol"] : _endSymbol %>' Ctp='<%# ((DataRowView)Container.DataItem)["Counterparty"] %>'/>
                            </div>
                            <asp:PlaceHolder runat="server" Visible="<%# !string.IsNullOrEmpty(this._validationErrorMsg) %>">
                                <br/>
                                <asp:Label runat="server" ForeColor="Red"><%# this._validationErrorMsg %></asp:Label>
                            </asp:PlaceHolder>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="StripsDropDown" ClientIDMode="Static" OnPreRender="StripsDropDown_OnPreRender" ToolTip='<%# _stripsNum.HasValue ? _stripsNum.Value : ((DataRowView)Container.DataItem)["StripsCount"] %>'/>
                        </td>
                        <td colspan="2">
                            <a href="#" onclick="return AlterChangeLink(this,'<%# this.UrlQueryStartString %>', '<%# ((DataRowView)Container.DataItem)["PageId"].Equals(this._pageIdToModify) ? "Modify" : "Add" %>' , '<%# ((DataRowView)Container.DataItem)["PageId"] %>', '<%# ((DataRowView)Container.DataItem)["TradingSystemTypeId"] %>', '<%# ((DataRowView)Container.DataItem)["Counterparty"] %>');"><%# ((DataRowView)Container.DataItem)["PageId"].Equals(this._pageIdToModify) ? "Change" : "Add" %></a>&nbsp;<a href='<%# AdjustUrlQuery(null) %>'>Cancel</a>
                        </td>
                    </uc1:DataPlaceHolder>
                    <td class="DividerCell"></td>
                    <asp:PlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["SystemSubrows"] != DBNull.Value %>'> 
                        <td rowspan='<%# ((DataRowView)Container.DataItem)["SystemSubrows"] %>' style="text-align: left">
                            <%# ((DataRowView)Container.DataItem)["AvailableSymbols"] %>
                        </td>
                    </asp:PlaceHolder>
                    
                    
                    <td class="WideDividerCell"></td>
                    
                    <uc1:DataPlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["ConfiguredSystems"] != DBNull.Value %>'>
                        <td>
                            <a href='<%# (int) ((DataRowView)Container.DataItem)["EnabledSystems"] > 0 ? 
                            "javascript:alert(\"You cannot delete system type with active systems\")" : 
                            AdjustUrlQuery("DeleteSystemType=" + ((DataRowView)Container.DataItem)["TradingSystemTypeId"])%>'
                                onclick="if(!confirm('Are you sure you want to remove selected system type with all the systems?')) return false; DisableAutoRefresh();">
                            <img src="Styles/Icon-Remove-Faded-16-pixels.png" alt="Remove"/>
                            </a>
                        </td>
                    </uc1:DataPlaceHolder>
                    <uc1:DataPlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["ConfiguredSystems"] == DBNull.Value %>'>
                        <td class="DividerCell" colspan="1"></td>  
                    </uc1:DataPlaceHolder>

                </tr> 
                     
                </uc1:DataPlaceHolder> 
                </ItemTemplate>
                
                
                <FooterTemplate>
                    
                    
    </tbody>
    <tfoot>
        <tr><td class="DividerCell"/></tr>
        <tr class="FooterRow">
            <td style="text-align: center">&Sigma;</td>
            <td class="DividerCell"/>
            <td runat="server" ID="FooterCell_ConfiguredSystems"></td>
            <td runat="server" ID="FooterCell_ActiveSystems"></td>
            <td runat="server" ID="FooterCell_HardBlockedSystems"></td>
            <td class="DividerCell"/>
            <td runat="server" ID="FooterCell_Volume"></td>
            <td runat="server" ID="FooterCell_VolumePct"></td>
            <td runat="server" ID="FooterCell_Deals"></td>
            <td runat="server" ID="FooterCell_CtpRejections"></td>
            <td runat="server" ID="FooterCell_KGTRejections"></td>
            <td class="DividerCell"/>
            <td runat="server" ID="FooterCell_PnlGrossPerM"></td>
            <td runat="server" ID="FooterCell_CommPerM"></td>
            <td runat="server" ID="FooterCell_PnlNetPerM"></td>
            <td class="DividerCell"/>
            <td runat="server" ID="FooterCell_PnlGross"></td>
            <td runat="server" ID="FooterCell_Comm"></td>
            <td runat="server" ID="FooterCell_PnlNet"></td>
            <asp:PlaceHolder runat="server" Visible='<%# _hasMissingTypes %>'>
                <td colspan="100" style="text-align: right; border: none; font-weight: normal;">
                    <asp:PlaceHolder runat="server" Visible='<%# !_addSystemType %>'>
                        <a href='<%# AdjustUrlQuery("AddSystemType=true") %>' onclick="DisableAutoRefresh();">Add trading system type to group</a>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" Visible='<%# _addSystemType %>'>
                        <asp:DropDownList ID="TradingTypeDropDown" ClientIDMode="Static" runat="server" OnPreRender="TradingTypeDropDown_OnPreRender" GrpId='<%# _groupId %>' />&nbsp;&nbsp;&nbsp;         
                        <a href="#" onclick="return AlterAddTypeLink(this, '<%# UrlQueryStartString %>');">Confirm</a>&nbsp;
                        <a href='<%# AdjustUrlQuery(null) %>'>Cancel</a>
                    </asp:PlaceHolder>

                </td>
            </asp:PlaceHolder>
        </tr>
    </tfoot>
    </table> 
                    
                    <%--  inner table end--%>
                                
            </td>
        </tr>
            <tr><td colspan="2" style="height: 4px"></td></tr>
        </tbody>
        </table>

                </FooterTemplate>
            </asp:Repeater>
            
    </div>  
    
    </ContentTemplate>
    </uc1:autorefreshcontainer>
    </div>
    </form>
</body>
</html>
