﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public class MyTemplateContainer : Control, INamingContainer { }

    public partial class AutorefreshContainer : System.Web.UI.UserControl
    {
        public AutorefreshContainer()
        {
            this.AutorefreshMillisecondsInterval = 1000;
        }

        [TemplateContainer(typeof(MyTemplateContainer))]
        [TemplateInstance(TemplateInstance.Single)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public ITemplate ContentTemplate { get; set; }

        public bool AllowAutoRefresh
        {
            set { RefreshEnabledPlaceholder.Visible = value; }
        }

        public int AutorefreshMillisecondsInterval { get; set; }

        public event Func<string, string> RefreshUrlAdjustRequested; 

        protected string RefreshUrl
        {
            get
            {
                if (RefreshUrlAdjustRequested == null)
                    return "?";
                else
                    return RefreshUrlAdjustRequested(null);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        { }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.ContentTemplatePlaceHolder.Controls.Clear();

            if (this.ContentTemplate != null)
            {
                var container = new MyTemplateContainer();

                this.ContentTemplate.InstantiateIn(container);
                this.ContentTemplatePlaceHolder.Controls.Add(container);
            }
            else
            {
                this.ContentTemplatePlaceHolder.Controls.Add(new LiteralControl("No template defined"));
            }
        }

        public void ReportException(Exception e)
        {
            string errorDetails;

            if (Utils.GetIsDevUser())
            {
                errorDetails = e.ToString();
            }
            else
            {
                errorDetails = string.Format("Please report the error to KGT (Error code: {0})", e.GetType().ToString());
            }

            this.ReportError(errorDetails);
        }

        public void ReportError(string error)
        {
            this.AutoRefreshErrorPanel.Style.Add(HtmlTextWriterStyle.Display, "block");
            this.errorLiteral.InnerText = error;
            this.AllowAutoRefresh = false;
        }
    }
}