﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;

namespace Kreslik.Integrator.BusinessLayer.Tests
{
    [TestFixture]
    public class Playground
    {
        [SetUp]
        public void SetUp()
        {
            //logic performed before each test
        }

        [TearDown]
        public void TearDown()
        {
            //logic performed after each test
        }

        [Test]
        public void FirstTest()
        {
            //ARRANGE
            var listMock = new Mock<IList<int>>();
            listMock.Setup(l => l.Add(It.Is<int>(i => i%2 == 0))).Throws(new ArgumentOutOfRangeException());
            listMock.Setup(l => l.Count).Returns(5);

            //ACT
            int count = listMock.Object.Count;

            //ASSERT
            Assert.AreEqual(5, count, "Tested list should have exactly 5 itms");
            listMock.Verify(l => l.Count, Times.Exactly(1));
            listMock.Verify(l => l.Add(It.IsAny<int>()), Times.Never(), "It is forbidden to add itmes to FooBar list");
        }
    }
}
