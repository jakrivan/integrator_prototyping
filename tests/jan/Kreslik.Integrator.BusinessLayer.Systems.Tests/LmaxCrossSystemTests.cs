﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.BusinessLayer.Systems;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.DAL;
using Moq;
using NUnit.Framework;

namespace Kreslik.Integrator.BusinessLayer.Systmes.Tests
{
    [TestFixture]
    public class LmaxCrossSystemTests
    {
        private static Counterparty _dummyInitializationCtp = Counterparty.JPM;

        private class TestLmaxCrossSystemSettings : IVenueCrossSystemSettings
        {
            public decimal MinimumSizeToTrade { get; set; }
            public decimal MaximumSizeToTrade { get; set; }
            public decimal MinimumDecimalDiscountToTrade { get; private set; }
            public decimal MinimumDiscountBasisPointsToTrade { get; set; }
            public TimeSpan MinimumTimeBetweenSingleCounterpartySignals { get; set; }
            public decimal MinimumFillSize { get; set; }
            public TimeSpan MaximumWaitTimeOnImprovementTick { get; set; }
            public int TradingSystemId { get; set; }

            public int TradingSystemGroupId { get; set; }

            public bool UpdateInternalState(Kreslik.Integrator.DAL.IIntegratedSystemSettingsBag integratedSystemSettingsBag)
            {
                throw new NotImplementedException();
            }

            public void UpdateBpConversions(decimal midPriceRate)
            {
                throw new NotImplementedException();
            }

            public void Initialize(DAL.IIntegratedSystemSettingsBag integratedSystemSettingsBag)
            {
                throw new NotImplementedException();
            }
        }

        private class LmaxCrossSystemSettingsProvider : IIntegratedSystemSettingsProvider<IVenueCrossSystemSettings>
        {
            public IVenueCrossSystemSettings CurrentSettings { get; set; }
            public event Action<IVenueCrossSystemSettings> SettingsChanged;
            public bool Enabled { get; set; }
            public event Action<bool> EnableChanged;
            public bool IsOutgoingPriceSideEnabled(PriceSide? side)
            {
                return true;
            }
            public event Action<bool, bool> SideEnablementChanged;
            public bool GoFlatOnly { get; set; }
            public event Action<bool> GoFlatOnlyChanged;
            public bool OrdersTransmissionDisabled { get; set; }
            public event Action<bool> OrdersTransmissionDisabledChanged;
            public event Action ResetRequested;
            public Counterparty TargetVenueCounterparty { get; set; }
            public Symbol TargetSymbol { get; set; }
            public event Action<IIntegratedSystemSettingsProvider<IVenueCrossSystemSettings>> SystemDeleted;
            public event Action<DateTime> RegularCheck;
        }

        [Test]
        public void LmaxCrossSystem_constructionTest()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var lmaxBookMock = new Mock<IBookTop<PriceObjectInternal>>();
            var orderManagementMock = new Mock<IOrderManagement>();
            var riskManagementMock = new Mock<IRiskManager>();
            var lmaxCrossSettingsMock = new Mock<IVenueCrossSystemSettings>();
            var lmaxCrossSettingsProviderMock = new Mock<IIntegratedSystemSettingsProvider<IVenueCrossSystemSettings>>();
            var orderFlowSession = new Mock<IOrderFlowSession>();
            var symbolsInfoMock = new Mock<ISymbolsInfo>();
            var groupsCacheMock = new Mock<ITradingGroupsCache>();
            var unicastFwdHub = new Mock<IUnicastInfoForwardingHub>();

            ILogger testLogger = new TestLogger(Assert.Fail);

            lmaxCrossSettingsProviderMock.Setup(prov => prov.CurrentSettings).Returns(lmaxCrossSettingsMock.Object);

            var lmaxSystem = new LmaxCrossSystem(priceBookMock.Object, priceBookMock.Object, lmaxBookMock.Object, lmaxBookMock.Object, Symbol.EUR_AUD,
                                                 orderManagementMock.Object, riskManagementMock.Object, groupsCacheMock.Object, orderFlowSession.Object, symbolsInfoMock.Object,
                                                 unicastFwdHub.Object, testLogger,
                                                 lmaxCrossSettingsProviderMock.Object, LmaxCounterparty.LM2);
        }

        [Test]
        public void LmaxCrossSystem_NoOrdersIssuedWhenConditionsNotMet()
        {
            var askPriceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var bidPriceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var lmaxAskBookMock = new Mock<IBookTop<PriceObjectInternal>>();
            var lmaxBidBookMock = new Mock<IBookTop<PriceObjectInternal>>();
            var orderManagementMock = new Mock<IOrderManagement>();
            var riskManagementMock = new Mock<IRiskManager>();
            var orderFlowSession = new Mock<IOrderFlowSession>();
            var symbolsInfoMock = new Mock<ISymbolsInfo>();
            var groupsCacheMock = new Mock<ITradingGroupsCache>();
            var unicastFwdHub = new Mock<IUnicastInfoForwardingHub>();
            var lmaxCrossSettings = new TestLmaxCrossSystemSettings()
                {
                    MaximumSizeToTrade = 200,
                    MinimumDiscountBasisPointsToTrade = 10000,
                    MaximumWaitTimeOnImprovementTick = TimeSpan.FromMilliseconds(100),
                    MinimumSizeToTrade = 10,
                    MinimumTimeBetweenSingleCounterpartySignals = TimeSpan.FromMilliseconds(10),
                    TradingSystemId = 12345
                };
            var lmaxCrossSettingsProvider = new LmaxCrossSystemSettingsProvider()
                {
                    CurrentSettings = lmaxCrossSettings,
                    Enabled = true
                };

            ILogger testLogger = new TestLogger(Assert.Fail);

            var lmaxSystem = new LmaxCrossSystem(askPriceBookMock.Object, bidPriceBookMock.Object, lmaxAskBookMock.Object, lmaxBidBookMock.Object, Symbol.EUR_AUD,
                                                 orderManagementMock.Object, riskManagementMock.Object, groupsCacheMock.Object, orderFlowSession.Object, symbolsInfoMock.Object, 
                                                 unicastFwdHub.Object, testLogger,
                                                 lmaxCrossSettingsProvider, LmaxCounterparty.LM2);
            //lmaxSystem.Enable();

            PriceObjectInternal askTop =
                PriceObjectInternal.GetAllocatedTestingPriceObject(10m, 1000, PriceSide.Ask, Symbol.EUR_AUD, Counterparty.L01, "",
                    MarketDataRecordType.BankQuoteData, DateTime.MinValue, DateTime.MinValue);
            askPriceBookMock.Setup(book => book.MaxNodeInternal).Returns(askTop);

            bidPriceBookMock.Setup(book => book.MaxNodeInternal)
                            .Returns(PriceObjectInternal.GetAllocatedTestingPriceObject(20m, 2000, PriceSide.Bid, Symbol.EUR_AUD, Counterparty.NOM, "",
                    MarketDataRecordType.BankQuoteData, DateTime.MinValue, DateTime.MinValue));

            askPriceBookMock.Raise(book => book.ChangeableBookTopImproved += null, askTop);

            orderManagementMock.Verify(
                ordMgmt => ordMgmt.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()),
                Times.Never());
        }
    }
}
