﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.FIXMessaging;
using Moq;
using NUnit.Framework;

namespace Kreslik.Integrator.BusinessLayer.Systems.Tests
{
    internal class GliderSystemUnderTest : StreamingMMGliderSystemBase
    {
        public GliderSystemUnderTest(IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxAskPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxBidPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolAskPricebook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolBidPricebook,
            IMedianProvider<decimal> askMedianProvider, IMedianProvider<decimal> bidMedianProvider,
            Symbol symbol,
            PriceSide observedBookSide, Counterparty destinationVenueCounterparty,
            IStreamingChannel destinationStreamingChannel, ISymbolsInfo symbolsInfo,
            IMedianProvider<decimal> medianPriceProvider, IOrderManagement orderManagement, IRiskManager riskManager,
            ITradingGroupsCache tradingGroupsCache, ILogger logger,
            IIntegratedSystemSettingsProvider<IVenueGliderStreamMMSystemSettings> settingsProvider)
            : base(
                lmaxAskPriceBook, lmaxBidPriceBook, bankpoolAskPricebook, bankpoolBidPricebook,
                askMedianProvider, bidMedianProvider, symbol, observedBookSide,
                destinationVenueCounterparty, destinationStreamingChannel, symbolsInfo, medianPriceProvider,
                orderManagement, riskManager, tradingGroupsCache, new Mock<IUnicastInfoForwardingHub>().Object, logger, settingsProvider)
        {
        }

        protected override VenueClientOrderRequestInfo CreateVenueGliderReplaceOrderRequest(string replacedOrderId,
            VenueClientOrderRequestInfo replacingOrderRequest)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxReplaceOrder(replacedOrderId,
                replacingOrderRequest as LmaxClientOrderRequestInfo);
        }

        protected override GliderLmaxClientOrder CreateVenueGlidingOrder(string orderIdentity, string clientIdentity,
            VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification, GlidingInfoBag glidingInfoBag)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateGliderLmaxClientOrder(orderIdentity, clientIdentity,
                venueOrderRequest as LmaxClientOrderRequestInfo, integratedTradingSystemIdentification, glidingInfoBag);
        }

        public void PerformFinalDecisioningUnderTest(GlidingInfoBag gib)
        {
            this.PerformFinalDecisioning(gib);
        }

        public void ProcessGlidingEventUnderTest(GlidingInfoBag glidingInfoBag, bool invokedFromTimer)
        {
            this._enabled = true;
            this._enabledInSettings = true;
            this.ExitSingleAccessRegion();
            this.ProcessGlidingEvent(glidingInfoBag, invokedFromTimer);
        }
        protected override void ProcessEvent()
        { }

        protected override bool IsAllowedToBeEnabled { get { return true; } }

        public void UpdateSettings(IVenueGliderStreamMMSystemSettings sett)
        {
            this.SettingsUpdating(sett);
        }

        public class SchedulingInfoBag
        {
            public SchedulingInfoBag(GlidingInfoBag glidingInfoBag, DateTime nextDueTime,
            bool invokedFromTimer)
            {
                this.GlidingInfoBag = glidingInfoBag;
                this.NextDueTime = nextDueTime;
                this.InvokedFromTimer = invokedFromTimer;
            }

            public GlidingInfoBag GlidingInfoBag { get; private set; }
            public DateTime NextDueTime { get; private set; }
            public bool InvokedFromTimer { get; private set; }
        }

        public SchedulingInfoBag LastSchedulingInfoBag { get; private set; }

        protected override void ScheduleNextGlidingEvaluation(GlidingInfoBag glidingInfoBag, DateTime nextDueTime,
            bool invokedFromTimer)
        {
            this.LastSchedulingInfoBag = new SchedulingInfoBag(glidingInfoBag, nextDueTime, invokedFromTimer);
        }

        private DateTime _utcNow;
        protected override DateTime UtcNow { get { return _utcNow; } }
        public void SetUtcNow(DateTime utcNow)
        {
            this._utcNow = utcNow;
        }

        protected override VenueClientOrderRequestInfo CreateVenueGliderOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol,
            decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxOrder(
                sizeBaseAbs, price, dealDirection, symbol, TimeInForce.Day, LmaxCounterparty.L01);
        }
    }

    [TestFixture]
    public class StreamingMMGliderSystemTests
    {
        GliderSystemUnderTest CreateSystem()
        {
            var streamChannelProxy = new Mock<IStreamingChannel>();
            streamChannelProxy.Setup(prx => prx.GetStreamingChannelProxy(It.IsAny<IStreamingPricesProducer>())).Returns((IStreamingChannelProxy) null);
            var riskMgmtMock = new Mock<IRiskManager>();
            var symTrustWatcherMock = new Mock<ISymbolTrustworthyWatcher>();
            riskMgmtMock.Setup(r => r.GetSymbolTrustworthyWatcher(It.IsAny<Symbol>()))
                .Returns(symTrustWatcherMock.Object);
            var settingsProviderMock = new Mock<IIntegratedSystemSettingsProvider<IVenueGliderStreamMMSystemSettings>>();
            var settingsMock = new Mock<IVenueGliderStreamMMSystemSettings>();
            settingsMock.Setup(s => s.TradingSystemId).Returns(555);
            settingsMock.Setup(s => s.TradingSystemGroupId).Returns(44);
            settingsProviderMock.Setup(sp => sp.CurrentSettings).Returns(settingsMock.Object);
            var tradingGroupsCacheMock = new Mock<ITradingGroupsCache>();
            tradingGroupsCacheMock.Setup(c => c.GeTradingSystemGroup(It.IsAny<int>())).Returns(new TradingSystemGroup(44, "hh"));
            var orderManagementMock = new Mock<IOrderManagement>();
            orderManagementMock.Setup(
                om => om.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()))
                .Returns(IntegratorRequestResult.GetSuccessResult);

            var lmaxAskBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            lmaxAskBookMock.Setup(b => b.MaxNodeInternal)
                .Returns(PriceObjectInternal.GetAllocatedTestingPriceObject(4, 10, PriceSide.Ask, Symbol.EUR_USD,
                    Counterparty.L01,
                    null, MarketDataRecordType.BankQuoteData, DateTime.UtcNow, DateTime.UtcNow));

            return new GliderSystemUnderTest(lmaxAskBookMock.Object, null, null, null, null, null, Symbol.EUR_USD, PriceSide.Ask, Counterparty.FS2,
                streamChannelProxy.Object, null, null, orderManagementMock.Object, riskMgmtMock.Object, tradingGroupsCacheMock.Object,
                new TestLogger(Assert.Fail), settingsProviderMock.Object);
        }

        [Test]
        public void GliderSystem_NoFillAndPriceGood_WillAcceptIfPriceAboveMinGain()
        {
            //Ask observing system - so it sell and want to buy cheaper (for lower price)

            var streamChannelMock = new Mock<IStreamingChannel>();
            streamChannelMock.Setup(prx => prx.GetStreamingChannelProxy(It.IsAny<IStreamingPricesProducer>())).Returns((IStreamingChannelProxy)null);
            streamChannelMock.Setup(sch => sch.AcceptDeal(It.IsAny<RejectableMMDeal>())).Returns(true);
            var riskMgmtMock = new Mock<IRiskManager>();
            var symTrustWatcherMock = new Mock<ISymbolTrustworthyWatcher>();
            riskMgmtMock.Setup(r => r.GetSymbolTrustworthyWatcher(It.IsAny<Symbol>()))
                .Returns(symTrustWatcherMock.Object);
            var settingsProviderMock = new Mock<IIntegratedSystemSettingsProvider<IVenueGliderStreamMMSystemSettings>>();
            var settingsMock = new Mock<IVenueGliderStreamMMSystemSettings>();
            settingsMock.Setup(s => s.TradingSystemId).Returns(555);
            settingsMock.Setup(s => s.TradingSystemGroupId).Returns(44);
            settingsProviderMock.Setup(sp => sp.CurrentSettings).Returns(settingsMock.Object);
            var tradingGroupsCacheMock = new Mock<ITradingGroupsCache>();
            tradingGroupsCacheMock.Setup(c => c.GeTradingSystemGroup(It.IsAny<int>())).Returns(new TradingSystemGroup(44, "hh"));
            var orderManagementMock = new Mock<IOrderManagement>();
            orderManagementMock.Setup(
                om => om.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()))
                .Returns(IntegratorRequestResult.GetSuccessResult);

            //Current price is 4
            var lmaxAskBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            lmaxAskBookMock.Setup(b => b.MaxNodeInternal)
                .Returns(PriceObjectInternal.GetAllocatedTestingPriceObject(4m, 10, PriceSide.Ask, Symbol.EUR_USD,
                    Counterparty.L01,
                    null, MarketDataRecordType.BankQuoteData, DateTime.UtcNow, DateTime.UtcNow));

            var lmaxBidBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            lmaxBidBookMock.Setup(b => b.MaxNodeInternal)
                .Returns(PriceObjectInternal.GetAllocatedTestingPriceObject(1.5m, 10, PriceSide.Bid, Symbol.EUR_USD,
                    Counterparty.L01,
                    null, MarketDataRecordType.BankQuoteData, DateTime.UtcNow, DateTime.UtcNow));

            GliderSystemUnderTest system = new GliderSystemUnderTest(lmaxAskBookMock.Object, lmaxBidBookMock.Object, null, null, null, null, Symbol.EUR_USD, PriceSide.Ask, Counterparty.FS1,
                streamChannelMock.Object, null, null, orderManagementMock.Object, riskMgmtMock.Object, tradingGroupsCacheMock.Object,
                new TestLogger(Assert.Fail), settingsProviderMock.Object);


            var rejSourceMock = new Mock<ICounterpartyRejectionSource>();
            RejectableMMDeal deal = new RejectableMMDeal(Symbol.EUR_USD, Counterparty.FS1, DealDirection.Sell, 7, 1000, 100, "", "", "", DateTime.UtcNow, rejSourceMock.Object);
            //We want to buy at worst for 6
            GlidingInfoBag gib = new GlidingInfoBag(deal, 1000, 6, DateTime.UtcNow);


            system.PerformFinalDecisioningUnderTest(gib);

            streamChannelMock.Verify(ch => ch.AcceptDeal(deal), Times.Once());
            Assert.IsTrue(!deal.IsRiskRemovingOrder);
            streamChannelMock.Verify(ch => ch.RejectDeal(deal, RejectableMMDeal.DealRejectionReason.DealMissed), Times.Never());
            orderManagementMock.Verify(om => om.RegisterOrder(It.Is<IClientOrder>(
                ord => ord.IsRiskRemovingOrder && ord.OrderRequestInfo.SizeBaseAbsInitial == 1000 && ord.OrderRequestInfo.IntegratorDealDirection == DealDirection.Buy), It.IsAny<ITakerUnicastInfoForwarder>()), Times.Once());

            //Now try for worse price - 6.001
            lmaxAskBookMock.Setup(b => b.MaxNodeInternal)
                .Returns(PriceObjectInternal.GetAllocatedTestingPriceObject(6.001m, 10, PriceSide.Ask, Symbol.EUR_USD,
                    Counterparty.L01,
                    null, MarketDataRecordType.BankQuoteData, DateTime.UtcNow, DateTime.UtcNow));
            gib = new GlidingInfoBag(deal, 1000, 6, DateTime.UtcNow);
            system.PerformFinalDecisioningUnderTest(gib);
            streamChannelMock.Verify(ch => ch.RejectDeal(deal, RejectableMMDeal.DealRejectionReason.DealMissed), Times.Once());
            orderManagementMock.Verify(om => om.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()), Times.Once());
        }


        [Test]
        public void GliderSystem_FilledAndPriceBad_WillAcceptHigherGain()
        {
            //Bids observing system - so it Buys and want to sell more expensive (for higher price)

            var streamChannelMock = new Mock<IStreamingChannel>();
            streamChannelMock.Setup(prx => prx.GetStreamingChannelProxy(It.IsAny<IStreamingPricesProducer>())).Returns((IStreamingChannelProxy)null);
            streamChannelMock.Setup(sch => sch.AcceptDeal(It.IsAny<RejectableMMDeal>())).Returns(true);
            var riskMgmtMock = new Mock<IRiskManager>();
            var symTrustWatcherMock = new Mock<ISymbolTrustworthyWatcher>();
            riskMgmtMock.Setup(r => r.GetSymbolTrustworthyWatcher(It.IsAny<Symbol>()))
                .Returns(symTrustWatcherMock.Object);
            var settingsProviderMock = new Mock<IIntegratedSystemSettingsProvider<IVenueGliderStreamMMSystemSettings>>();
            var settingsMock = new Mock<IVenueGliderStreamMMSystemSettings>();
            settingsMock.Setup(s => s.TradingSystemId).Returns(555);
            settingsMock.Setup(s => s.TradingSystemGroupId).Returns(44);
            settingsProviderMock.Setup(sp => sp.CurrentSettings).Returns(settingsMock.Object);
            var tradingGroupsCacheMock = new Mock<ITradingGroupsCache>();
            tradingGroupsCacheMock.Setup(c => c.GeTradingSystemGroup(It.IsAny<int>())).Returns(new TradingSystemGroup(44, "hh"));
            var orderManagementMock = new Mock<IOrderManagement>();
            orderManagementMock.Setup(
                om => om.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()))
                .Returns(IntegratorRequestResult.GetSuccessResult);

            //Current price is 4
            var lmaxAskBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            lmaxAskBookMock.Setup(b => b.MaxNodeInternal)
                .Returns(PriceObjectInternal.GetAllocatedTestingPriceObject(4m, 10, PriceSide.Bid, Symbol.EUR_USD,
                    Counterparty.L01,
                    null, MarketDataRecordType.BankQuoteData, DateTime.UtcNow, DateTime.UtcNow));
            lmaxAskBookMock.Setup(b => b.GetWeightedPriceForExactSize(It.IsAny<decimal>(), int.MaxValue))
                .Returns(4m);

            var lmaxBidBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            lmaxBidBookMock.Setup(b => b.MaxNodeInternal)
                .Returns(PriceObjectInternal.GetAllocatedTestingPriceObject(1.5m, 10, PriceSide.Bid, Symbol.EUR_USD,
                    Counterparty.L01,
                    null, MarketDataRecordType.BankQuoteData, DateTime.UtcNow, DateTime.UtcNow));
            lmaxBidBookMock.Setup(b => b.GetWeightedPriceForExactSize(It.IsAny<decimal>(), int.MaxValue))
                .Returns(1.5m);
            settingsMock.Setup(s => s.PreferLLDestinationCheckToLmax).Returns(true);


            GliderSystemUnderTest system = new GliderSystemUnderTest(lmaxAskBookMock.Object, lmaxBidBookMock.Object, null, null, null, null, Symbol.EUR_USD, PriceSide.Bid, Counterparty.FS1,
                streamChannelMock.Object, null, null, orderManagementMock.Object, riskMgmtMock.Object, tradingGroupsCacheMock.Object,
                new TestLogger(Assert.Fail), settingsProviderMock.Object);


            var rejSourceMock = new Mock<ICounterpartyRejectionSource>();
            RejectableMMDeal deal = new RejectableMMDeal(Symbol.EUR_USD, Counterparty.FS1, DealDirection.Buy, 5, 1000, 10, "", "", "", DateTime.UtcNow, rejSourceMock.Object);
            //We want to sell at worst for 6
            GlidingInfoBag gib = new GlidingInfoBag(deal, 1000, 6, DateTime.UtcNow);


            

            //We did not sold anything; and we wanted to sell at least for 6 - but price is 1.5 - Reject
            system.PerformFinalDecisioningUnderTest(gib);

            streamChannelMock.Verify(ch => ch.RejectDeal(deal, RejectableMMDeal.DealRejectionReason.DealMissed), Times.Once());
            orderManagementMock.Verify(om => om.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()), Times.Never());


            gib = new GlidingInfoBag(deal, 1000, 6, DateTime.UtcNow);
            gib.TotalInTimeFilledSizeBasePol.InterlockedAdd(-400);
            gib.TotalInTimeFilledSizeTermPol.InterlockedAdd(400 * 8);

            //We sold 400 for 8;
            //  so now it pays of to buy it for 4 as 
            //  as opposed to sell other 600 for 1.5 and buy 1000 for 5
            system.PerformFinalDecisioningUnderTest(gib);

            streamChannelMock.Verify(ch => ch.RejectDeal(deal, RejectableMMDeal.DealRejectionReason.DealMissed), Times.Exactly(2));
            streamChannelMock.Verify(ch => ch.AcceptDeal(deal), Times.Never());
            orderManagementMock.Verify(om => om.RegisterOrder(It.Is<IClientOrder>(
                ord => ord.IsRiskRemovingOrder && ord.OrderRequestInfo.SizeBaseAbsInitial == 400 && ord.OrderRequestInfo.IntegratorDealDirection == DealDirection.Buy), It.IsAny<ITakerUnicastInfoForwarder>()), Times.Once());



            gib = new GlidingInfoBag(deal, 1000, 6, DateTime.UtcNow);
            gib.TotalInTimeFilledSizeBasePol.InterlockedAdd(-400);
            gib.TotalInTimeFilledSizeTermPol.InterlockedAdd(400 * 8);

            lmaxBidBookMock.Setup(b => b.MaxNodeInternal)
                .Returns(PriceObjectInternal.GetAllocatedTestingPriceObject(6m, 10, PriceSide.Bid, Symbol.EUR_USD,
                    Counterparty.L01,
                    null, MarketDataRecordType.BankQuoteData, DateTime.UtcNow, DateTime.UtcNow));

            //We sold 400 for 8;
            //  so now it is worse to buy it for 4 then 
            //  to sell other 600 for 6 and buy 1000 for 5
            system.PerformFinalDecisioningUnderTest(gib);

            streamChannelMock.Verify(ch => ch.AcceptDeal(deal), Times.Once());
            Assert.IsTrue(deal.IsRiskRemovingOrder);
            streamChannelMock.Verify(ch => ch.RejectDeal(deal, RejectableMMDeal.DealRejectionReason.DealMissed), Times.Exactly(2));
            orderManagementMock.Verify(om => om.RegisterOrder(It.Is<IClientOrder>(
                ord => ord.IsRiskRemovingOrder && ord.OrderRequestInfo.SizeBaseAbsInitial == 600 && ord.OrderRequestInfo.IntegratorDealDirection == DealDirection.Sell), It.IsAny<ITakerUnicastInfoForwarder>()), Times.Once());
        }


        


        [Test]
        public void GliderSystem__ProcessGlidingEvent_WillRescheduleOnNoPendingOrdes()
        {
            //Ask observing system - so it sell and want to buy cheaper (for lower price)

            var streamChannelMock = new Mock<IStreamingChannel>();
            streamChannelMock.Setup(prx => prx.GetStreamingChannelProxy(It.IsAny<IStreamingPricesProducer>()))
                .Returns(new Mock<IStreamingChannelProxy>().Object);
            streamChannelMock.Setup(sch => sch.IsReadyAndOpen).Returns(true);
            var riskMgmtMock = new Mock<IRiskManager>();
            var symTrustWatcherMock = new Mock<ISymbolTrustworthyWatcher>();
            symTrustWatcherMock.Setup(w => w.IsTrustworthy).Returns(true);
            riskMgmtMock.Setup(r => r.GetSymbolTrustworthyWatcher(It.IsAny<Symbol>()))
                .Returns(symTrustWatcherMock.Object);
            var settingsProviderMock = new Mock<IIntegratedSystemSettingsProvider<IVenueGliderStreamMMSystemSettings>>();
            var settingsMock = new Mock<IVenueGliderStreamMMSystemSettings>();
            settingsMock.Setup(s => s.TradingSystemId).Returns(555);
            settingsMock.Setup(s => s.TradingSystemGroupId).Returns(44);
            settingsMock.Setup(s => s.GlidePriceLife).Returns(TimeSpan.FromSeconds(1));
            settingsProviderMock.Setup(sp => sp.CurrentSettings).Returns(settingsMock.Object);
            var tradingGroupsCacheMock = new Mock<ITradingGroupsCache>();
            tradingGroupsCacheMock.Setup(c => c.GeTradingSystemGroup(It.IsAny<int>()))
                .Returns(new TradingSystemGroup(44, "hh"));
            var orderManagementMock = new Mock<IOrderManagement>();
            orderManagementMock.Setup(
                om => om.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()))
                .Returns(IntegratorRequestResult.GetSuccessResult);
            var symbolsInfoMock = new Mock<ISymbolsInfo>();
            decimal dummy;

            //Current price is 4 - 0.9
            var lmaxAskBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            lmaxAskBookMock.Setup(b => b.MaxNodeInternal)
                .Returns(PriceObjectInternal.GetAllocatedTestingPriceObject(14m, 10, PriceSide.Bid, Symbol.EUR_USD,
                    Counterparty.L01,
                    null, MarketDataRecordType.BankQuoteData, DateTime.UtcNow, DateTime.UtcNow));
            var lmaxBidBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            lmaxBidBookMock.Setup(b => b.MaxNodeInternal)
                .Returns(PriceObjectInternal.GetAllocatedTestingPriceObject(0.9m, 10, PriceSide.Bid, Symbol.EUR_USD,
                    Counterparty.L01,
                    null, MarketDataRecordType.BankQuoteData, DateTime.UtcNow, DateTime.UtcNow));

            GliderSystemUnderTest system = new GliderSystemUnderTest(lmaxAskBookMock.Object, lmaxBidBookMock.Object,
                lmaxAskBookMock.Object, lmaxBidBookMock.Object, null, null, Symbol.EUR_USD,
                PriceSide.Ask, Counterparty.FS1,
                streamChannelMock.Object, new SymbolsInfoMock(), null, orderManagementMock.Object, riskMgmtMock.Object,
                tradingGroupsCacheMock.Object,
                new TestLogger(Assert.Fail), settingsProviderMock.Object);

            DateTime now = DateTime.UtcNow;
            system.UpdateSettings(settingsMock.Object);
            system.SetUtcNow(now);

            var rejSourceMock = new Mock<ICounterpartyRejectionSource>();
            RejectableMMDeal deal = new RejectableMMDeal(Symbol.EUR_USD, Counterparty.FS1, DealDirection.Sell, 7, 1000, 1000,
                "", "", "", DateTime.UtcNow, rejSourceMock.Object);
            //We want to buy at worst for 6
            DateTime cutoffTime = now + TimeSpan.FromSeconds(4.5);
            GlidingInfoBag gib = new GlidingInfoBag(deal, 1000, 4, cutoffTime);

            //ToB - 0.9, one below is 1.0; worst is 4
            // We have 4.5 seconds with 1 second step - so 4 steps => 3 decreases - each by 1


            system.ProcessGlidingEventUnderTest(gib, false);

            Assert.AreEqual(now + TimeSpan.FromSeconds(1), system.LastSchedulingInfoBag.NextDueTime);
            orderManagementMock.Verify(om => om.RegisterOrder(It.Is<IClientOrder>(
                ord =>
                    !ord.IsRiskRemovingOrder && ord.OrderRequestInfo.SizeBaseAbsInitial == 1000 &&
                    ord.OrderRequestInfo.IntegratorDealDirection == DealDirection.Buy & ord.OrderRequestInfo.RequestedPrice == 1),
                It.IsAny<ITakerUnicastInfoForwarder>()), Times.Once());

            system.SetUtcNow(now + TimeSpan.FromSeconds(2.2));

            //since orderNext is active it will not create new and it will reschedule for cuttoff
            system.ProcessGlidingEventUnderTest(gib, true);
            Assert.AreEqual(cutoffTime, system.LastSchedulingInfoBag.NextDueTime);
            orderManagementMock.Verify(
                om => om.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()), Times.Once());


            gib.OrderNext = null;
            system.SetUtcNow(now + TimeSpan.FromSeconds(1));
            //next step
            system.ProcessGlidingEventUnderTest(gib, true);
            Assert.AreEqual(now + TimeSpan.FromSeconds(2), system.LastSchedulingInfoBag.NextDueTime);
            orderManagementMock.Verify(om => om.RegisterOrder(It.Is<IClientOrder>(
                ord =>
                    !ord.IsRiskRemovingOrder && ord.OrderRequestInfo.SizeBaseAbsInitial == 1000 &&
                    ord.OrderRequestInfo.IntegratorDealDirection == DealDirection.Buy & ord.OrderRequestInfo.RequestedPrice == 2),
                It.IsAny<ITakerUnicastInfoForwarder>()), Times.Once());
            orderManagementMock.Verify(
                om => om.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()), Times.Exactly(2));

            gib.OrderNext = null;
            system.SetUtcNow(now + TimeSpan.FromSeconds(2));
            //next step
            system.ProcessGlidingEventUnderTest(gib, true);
            Assert.AreEqual(now + TimeSpan.FromSeconds(3), system.LastSchedulingInfoBag.NextDueTime);
            orderManagementMock.Verify(om => om.RegisterOrder(It.Is<IClientOrder>(
                ord =>
                    !ord.IsRiskRemovingOrder && ord.OrderRequestInfo.SizeBaseAbsInitial == 1000 &&
                    ord.OrderRequestInfo.IntegratorDealDirection == DealDirection.Buy & ord.OrderRequestInfo.RequestedPrice == 3),
                It.IsAny<ITakerUnicastInfoForwarder>()), Times.Once());
            orderManagementMock.Verify(
                om => om.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()), Times.Exactly(3));

            gib.OrderNext = null;
            system.SetUtcNow(now + TimeSpan.FromSeconds(3));
            //next step
            system.ProcessGlidingEventUnderTest(gib, true);
            Assert.AreEqual(cutoffTime, system.LastSchedulingInfoBag.NextDueTime);
            orderManagementMock.Verify(om => om.RegisterOrder(It.Is<IClientOrder>(
                ord =>
                    !ord.IsRiskRemovingOrder && ord.OrderRequestInfo.SizeBaseAbsInitial == 1000 &&
                    ord.OrderRequestInfo.IntegratorDealDirection == DealDirection.Buy & ord.OrderRequestInfo.RequestedPrice == 4),
                It.IsAny<ITakerUnicastInfoForwarder>()), Times.Once());
            orderManagementMock.Verify(
                om => om.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()), Times.Exactly(4));

            //if there is less then 1 step - the last step will be repeated
            gib.OrderNext = null;
            system.SetUtcNow(now + TimeSpan.FromSeconds(4));
            //next step
            system.ProcessGlidingEventUnderTest(gib, true);
            Assert.AreEqual(cutoffTime, system.LastSchedulingInfoBag.NextDueTime);
            orderManagementMock.Verify(om => om.RegisterOrder(It.Is<IClientOrder>(
                ord =>
                    !ord.IsRiskRemovingOrder && ord.OrderRequestInfo.SizeBaseAbsInitial == 1000 &&
                    ord.OrderRequestInfo.IntegratorDealDirection == DealDirection.Buy & ord.OrderRequestInfo.RequestedPrice == 4),
                It.IsAny<ITakerUnicastInfoForwarder>()), Times.Exactly(2));
            orderManagementMock.Verify(
                om => om.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()), Times.Exactly(5));
        }


        [Test]
        public void GliderSystem__ProcessGlidingEvent_WillRescheduleAndReplaceOrder()
        {
            //Bids observing system - so it buys and want to sell more expensive (for higher price)

            var streamChannelMock = new Mock<IStreamingChannel>();
            streamChannelMock.Setup(prx => prx.GetStreamingChannelProxy(It.IsAny<IStreamingPricesProducer>()))
                .Returns(new Mock<IStreamingChannelProxy>().Object);
            streamChannelMock.Setup(sch => sch.IsReadyAndOpen).Returns(true);
            var riskMgmtMock = new Mock<IRiskManager>();
            var symTrustWatcherMock = new Mock<ISymbolTrustworthyWatcher>();
            symTrustWatcherMock.Setup(w => w.IsTrustworthy).Returns(true);
            riskMgmtMock.Setup(r => r.GetSymbolTrustworthyWatcher(It.IsAny<Symbol>()))
                .Returns(symTrustWatcherMock.Object);
            var settingsProviderMock = new Mock<IIntegratedSystemSettingsProvider<IVenueGliderStreamMMSystemSettings>>();
            var settingsMock = new Mock<IVenueGliderStreamMMSystemSettings>();
            settingsMock.Setup(s => s.TradingSystemId).Returns(555);
            settingsMock.Setup(s => s.TradingSystemGroupId).Returns(44);
            settingsMock.Setup(s => s.GlidePriceLife).Returns(TimeSpan.FromSeconds(1));
            settingsProviderMock.Setup(sp => sp.CurrentSettings).Returns(settingsMock.Object);
            var tradingGroupsCacheMock = new Mock<ITradingGroupsCache>();
            tradingGroupsCacheMock.Setup(c => c.GeTradingSystemGroup(It.IsAny<int>()))
                .Returns(new TradingSystemGroup(44, "hh"));
            var orderManagementMock = new Mock<IOrderManagement>();
            orderManagementMock.Setup(
                om => om.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()))
                .Returns(IntegratorRequestResult.GetSuccessResult);
            var symbolsInfoMock = new Mock<ISymbolsInfo>();
            decimal dummy;

            //Current price is 4 - 0.9
            var lmaxAskBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            lmaxAskBookMock.Setup(b => b.MaxNodeInternal)
                .Returns(PriceObjectInternal.GetAllocatedTestingPriceObject(4m, 10, PriceSide.Bid, Symbol.EUR_USD,
                    Counterparty.L01,
                    null, MarketDataRecordType.BankQuoteData, DateTime.UtcNow, DateTime.UtcNow));
            var lmaxBidBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            lmaxBidBookMock.Setup(b => b.MaxNodeInternal)
                .Returns(PriceObjectInternal.GetAllocatedTestingPriceObject(0.9m, 10, PriceSide.Bid, Symbol.EUR_USD,
                    Counterparty.L01,
                    null, MarketDataRecordType.BankQuoteData, DateTime.UtcNow, DateTime.UtcNow));

            GliderSystemUnderTest system = new GliderSystemUnderTest(lmaxAskBookMock.Object, lmaxBidBookMock.Object,
                lmaxAskBookMock.Object, lmaxBidBookMock.Object, null, null, Symbol.EUR_USD,
                PriceSide.Bid, Counterparty.FS1,
                streamChannelMock.Object, new SymbolsInfoMock(), null, orderManagementMock.Object, riskMgmtMock.Object,
                tradingGroupsCacheMock.Object,
                new TestLogger(Assert.Fail), settingsProviderMock.Object);

            DateTime now = DateTime.UtcNow;
            system.UpdateSettings(settingsMock.Object);
            system.SetUtcNow(now);

            var rejSourceMock = new Mock<ICounterpartyRejectionSource>();
            RejectableMMDeal deal = new RejectableMMDeal(Symbol.EUR_USD, Counterparty.FS1, DealDirection.Buy, 0.5m, 1000, 10,
                "", "", "", DateTime.UtcNow, rejSourceMock.Object);
            //We want to sell at worst for 0.9
            DateTime cutoffTime = now + TimeSpan.FromSeconds(4.5);
            GlidingInfoBag gib = new GlidingInfoBag(deal, 1000, 0.9m, cutoffTime);

            //ToB - 4, one below is 3.9; worst is 0.9
            // We have 4.5 seconds with 1 second step - so 4 steps => 3 decreases - each by 1


            system.ProcessGlidingEventUnderTest(gib, false);

            Assert.AreEqual(now + TimeSpan.FromSeconds(1), system.LastSchedulingInfoBag.NextDueTime);
            orderManagementMock.Verify(om => om.RegisterOrder(It.Is<IClientOrder>(
                ord =>
                    !ord.IsRiskRemovingOrder && ord.OrderRequestInfo.SizeBaseAbsInitial == 1000 &&
                    ord.OrderRequestInfo.IntegratorDealDirection == DealDirection.Sell & ord.OrderRequestInfo.RequestedPrice == 3.9m),
                It.IsAny<ITakerUnicastInfoForwarder>()), Times.Once());

            system.SetUtcNow(now + TimeSpan.FromSeconds(2.2));
            //this will lead to reprocess of event
            system.OnIntegratorClientOrderUpdate(
                VenueClientOrderUpdateInfo.CreateConfirmedClientOrderUpdateInfo(gib.OrderNext, Counterparty.L01, 1000),
                Symbol.EUR_USD);

            //only 2 steps remaining - so will jump by 1.5
            //system.ProcessGlidingEventUnderTest(gib, true);
            Assert.AreEqual(now + TimeSpan.FromSeconds(3.2), system.LastSchedulingInfoBag.NextDueTime);
            orderManagementMock.Verify(om => om.RegisterOrder(It.Is<IClientOrder>(
                ord =>
                    !ord.IsRiskRemovingOrder && ord.OrderRequestInfo.SizeBaseAbsInitial == 1000 &&
                    ord.OrderRequestInfo.IntegratorDealDirection == DealDirection.Sell & ord.OrderRequestInfo.RequestedPrice == 2.4m),
                It.IsAny<ITakerUnicastInfoForwarder>()), Times.Once());
            orderManagementMock.Verify(
                om => om.RegisterOrder(It.IsAny<IClientOrder>(), It.IsAny<ITakerUnicastInfoForwarder>()), Times.Exactly(2));
        }

    }


    internal class SymbolsInfoMock : ISymbolsInfo
    {

        public decimal GetCommisionPricePerUnit(Counterparty counterparty)
        {
            throw new NotImplementedException();
        }

        public decimal GetCommisionPricePerMillion(Counterparty counterparty)
        {
            throw new NotImplementedException();
        }

        public bool IsSupported(Symbol symbol, TradingTargetType tradingTargetType)
        {
            throw new NotImplementedException();
        }

        public bool IsSupported(Currency currency)
        {
            throw new NotImplementedException();
        }

        public List<string> GetAscendingSortedStringListOfSupportedSymbols(TradingTargetType tradingTargetType)
        {
            throw new NotImplementedException();
        }

        public decimal? GetMinimumPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType)
        {
            return 0.1m;
        }

        public bool IsPriceWithinPriceIncrementLimit(decimal price, Symbol symbol, TradingTargetType tradingTargetType)
        {
            throw new NotImplementedException();
        }

        public RoundingResult TryRoundToSupportedPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType, RoundingKind roundingKind, decimal price)
        {
            throw new NotImplementedException();
        }

        public RoundingResult TryRoundToSupportedPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType, decimal price)
        {
            throw new NotImplementedException();
        }

        public decimal? GetOrderMinimumSize(Symbol symbol, TradingTargetType tradingTargetType)
        {
            throw new NotImplementedException();
        }

        public decimal? GetOrderMinimumSizeIncrement(Symbol symbol, TradingTargetType tradingTargetType)
        {
            throw new NotImplementedException();
        }

        public bool IsOrderSizeWithinSizeAndIncrementLimit(decimal orderSize, Symbol symbol, TradingTargetType tradingTargetType)
        {
            throw new NotImplementedException();
        }

        public bool IsOrderMinSizeZeroOrWithinSizeAndIncrementLimit(decimal orderMinSize, Symbol symbol, TradingTargetType tradingTargetType)
        {
            throw new NotImplementedException();
        }

        public bool IsOrderSizeWithinIncrementLimit(decimal orderSize, Symbol symbol, TradingTargetType tradingTargetType)
        {
            throw new NotImplementedException();
        }

        public RoundingResult TryRoundToSupportedSizeIncrement(Symbol symbol, TradingTargetType tradingTargetType, RoundingKind roundingKind, decimal size)
        {
            throw new NotImplementedException();
        }

        public bool TryRoundToSupportedPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType, RoundingKind roundingKind, decimal price, out decimal result)
        {
            result = SymbolsInfoUtils.RoundToIncrement(roundingKind, 0.1m, price);
            return true;
        }

        public RoundingResult TryRoundToSupportedSizeIncrement(Symbol symbol, TradingTargetType tradingTargetType, decimal size)
        {
            throw new NotImplementedException();
        }
    }
}
