﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using NUnit.Framework;
using Moq;

namespace Kreslik.Integrator.FIXMessaging.Tests
{
    // For help on mocking see https://code.google.com/p/moq/wiki/QuickStart


    //[TestFixture]
    //public class MarketDataSessionTests
    //{
    //    [Test]
    //    public void MarketDataSession_SubscribeWhenNotRunning_LogsErrorAndReturnNull()
    //    {
    //        //Setup

    //        var fixCahnnelMock = new Mock<IFIXChannel>();
    //        var marketDataMessagingAdapterMock = new Mock<IMarketDataMessagingAdapter>();
    //        var loggerMock = new Mock<ILogger>();
    //        fixCahnnelMock.Setup(ch => ch.State).Returns(SessionState.Idle);
    //        MarketDataSessionSettings mdsettings = new MarketDataSessionSettings();
    //        var prieDelayEvaluatorMock = new Mock<IPriceDelayEvaluator>();

    //        //Act

    //        MarketDataSession mks = new MarketDataSession(fixCahnnelMock.Object, marketDataMessagingAdapterMock.Object,
    //                                                      loggerMock.Object, Counterparty.HSB, mdsettings, prieDelayEvaluatorMock.Object);

    //        mks.Subscribe(new SubscriptionRequestInfo(Symbol.USD_ZAR, 10));

    //        //Verify

    //        loggerMock.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s => s.Contains("Cannot subscribe")), It.IsAny<object[]>()), Times.Exactly(1));
    //    }


    //    [Test]
    //    public void MarketDataSession_SubscribtionChangeWhenNoActiveSubscriptions_LogsError()
    //    {
    //        //Setup

    //        var fixCahnnelMock = new Mock<IFIXChannel>();
    //        var marketDataMessagingAdapterMock = new Mock<IMarketDataMessagingAdapter>();
    //        var loggerMock = new Mock<ILogger>();
    //        MarketDataSessionSettings mdsettings = new MarketDataSessionSettings();
    //        var prieDelayEvaluatorMock = new Mock<IPriceDelayEvaluator>();

    //        //Act

    //        MarketDataSession mks = new MarketDataSession(fixCahnnelMock.Object, marketDataMessagingAdapterMock.Object,
    //                                                      loggerMock.Object, Counterparty.HSB, mdsettings, prieDelayEvaluatorMock.Object);

    //        marketDataMessagingAdapterMock.Raise(adapter => adapter.OnSubscriptionChanged += null, new SubscriptionChangeInfo(string.Empty, SubscriptionStatus.NotSubscribed));

    //        //Verify

    //        loggerMock.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s => s.Contains("Receiving subscription change for nonexisting subscription")), It.IsAny<object[]>()), Times.Exactly(1));
    //    }
    //}
}
