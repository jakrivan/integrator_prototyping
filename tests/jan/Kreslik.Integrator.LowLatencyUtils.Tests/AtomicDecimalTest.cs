﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Kreslik.Integrator.LowLatencyUtils.Tests
{
    [TestFixture]
    public class AtomicDecimalTest
    {
        private void ShouldThow(Action action)
        {
            try
            {
                action();
                Assert.Fail("Action was expected to fail");
            }
            catch (Exception)
            { }
        }

        [Test]
        public void Construction_OutsideRanges()
        {
            ShouldThow(() => new AtomicDecimal(-1));
            ShouldThow(() => new AtomicDecimal(decimal.MaxValue));
            ShouldThow(() => new AtomicDecimal(AtomicDecimal.MaxValueDecimal + 1));
            ShouldThow(() => new AtomicDecimal(1.1230000000m));
        }

        private void ShouldConvertBackAndForth(decimal d)
        {
            AtomicDecimal ad = new AtomicDecimal(d);
            Assert.AreEqual(d, ad.ToDecimal());
        }

        private void ShouldConvertBackAndForth(int i)
        {
            AtomicDecimal ad = new AtomicDecimal(i);
            Assert.AreEqual(i, ad.ToDecimal());
        }

        [Test]
        public void Construction_BasicNumbers()
        {
            ShouldConvertBackAndForth(0m);
            ShouldConvertBackAndForth(0);
            ShouldConvertBackAndForth(0.1m);
            ShouldConvertBackAndForth(1.1m);
            ShouldConvertBackAndForth(1m);
            ShouldConvertBackAndForth(1);
            ShouldConvertBackAndForth(1.000m);
            ShouldConvertBackAndForth(1.100m);
            ShouldConvertBackAndForth(145.123456m);
        }

        [Test]
        public void Construction_LargeNumbers()
        {
            ShouldConvertBackAndForth(AtomicDecimal.MaxValueDecimal);
            ShouldConvertBackAndForth(AtomicDecimal.MaxValueDecimal - 0.000001m);
            ShouldConvertBackAndForth(10000000000m);
            ShouldConvertBackAndForth(9999999999m);
            Assert.AreEqual(AtomicDecimal.MaxValue, new AtomicDecimal(decimal.MaxValue));
            Assert.AreEqual(AtomicDecimal.MaxValueDecimal, new AtomicDecimal(decimal.MaxValue).ToDecimal());
        }

        [Test]
        public void Construction_LargeScale()
        {
            ShouldConvertBackAndForth(1.000000000m);
            ShouldConvertBackAndForth(1.000000001m);
            ShouldThow(() => new AtomicDecimal(1.0000000000m));
            ShouldThow(() => new AtomicDecimal(1.0000000001m));
            ShouldConvertBackAndForth(AtomicDecimal.MaxValueDecimal - 0.111111111m);
            ShouldConvertBackAndForth(AtomicDecimal.MaxValueDecimal - 0.000000001m);
            ShouldThow(() => new AtomicDecimal(AtomicDecimal.MaxValueDecimal - 0.0000000001m));
            ShouldConvertBackAndForth(12345.123456789m);
            ShouldConvertBackAndForth(12345.123456000m);
        }

        [Test]
        public void Construction_SpecialCases()
        {
            ShouldConvertBackAndForth(6.480680000m);


            decimal dec = 6.480680000m;
            int[] bits = decimal.GetBits(dec);
            decimal dec2 = new decimal(bits[0], -15656562, 0, false, 9);
            ShouldConvertBackAndForth(dec2);

            ShouldConvertBackAndForth(new decimal(int.MaxValue, int.MaxValue, 0, false, 9));
            ShouldConvertBackAndForth(new decimal(int.MinValue, int.MinValue, 0, false, 9));
            ShouldConvertBackAndForth(new decimal(int.MaxValue, int.MinValue, 0, false, 9));
            ShouldConvertBackAndForth(new decimal(int.MinValue, int.MaxValue, 0, false, 9));
        }

        private void ShouldConvertBackAndForth_ScaleCutting(decimal d)
        {
            AtomicDecimal ad = AtomicDecimal.ConvertToAtomicDecimalCuttingExcessScale(d);
            Assert.AreEqual(d, ad.ToDecimal());
        }

        private void ShouldConvertRounding_ScaleCutting(decimal d, decimal expectedResult)
        {
            AtomicDecimal ad = AtomicDecimal.ConvertToAtomicDecimalCuttingExcessScale(d);
            Assert.AreEqual(expectedResult, ad.ToDecimal());
        }

        [Test]
        public void ConvertToAtomicDecimalCuttingExcessScale_Tests()
        {
            ShouldConvertBackAndForth_ScaleCutting(1.000000000m);
            ShouldConvertBackAndForth_ScaleCutting(1.000000001m);
            ShouldConvertBackAndForth_ScaleCutting(AtomicDecimal.MaxValueDecimal - 0.111111111m);
            ShouldConvertBackAndForth_ScaleCutting(AtomicDecimal.MaxValueDecimal - 0.000000001m);
            ShouldConvertBackAndForth_ScaleCutting(12345.123456789m);
            ShouldConvertBackAndForth_ScaleCutting(12345.123456000m);

            ShouldConvertRounding_ScaleCutting(1.0000000000m, 1);
            ShouldConvertRounding_ScaleCutting(1.0000000001m, 1);
            ShouldConvertRounding_ScaleCutting(1.0000000005m, 1.000000001m);
            ShouldConvertRounding_ScaleCutting(12345.1234567899m, 12345.12345679m);
            ShouldConvertRounding_ScaleCutting(12345.1234567894m, 12345.123456789m);

            //AtomicDecimal ad1 = AtomicDecimal.ConvertToAtomicDecimalCuttingExcessScale(1.0000000000m);
            //AtomicDecimal ad2 = AtomicDecimal.ConvertToAtomicDecimalCuttingExcessScale(1.0000000001m);
        }

        [Test]
        public void ConvertToAtomicDecimalZeroTest_Tests()
        {
            AtomicDecimal zero = 0;
            Assert.IsTrue(zero == AtomicDecimal.Zero);
            Assert.IsFalse(zero != AtomicDecimal.Zero);
            Assert.AreEqual(zero, AtomicDecimal.Zero);
        }

        [Test]
        public void DivideByPowerOf2_Tests()
        {
            AtomicDecimal ad1 = new AtomicDecimal(64);
            AtomicDecimal ad2 = ad1.DivideByPowerOf2(6);
            Assert.AreEqual(64m, ad1.ToDecimal());
            Assert.AreEqual(1m, ad2.ToDecimal());

            AtomicDecimal ad3 = new AtomicDecimal(64.64m);
            AtomicDecimal ad4 = ad3.DivideByPowerOf2(6);
            Assert.AreEqual(64.64m, ad3.ToDecimal());
            Assert.AreEqual(1.01m, ad4.ToDecimal());

            AtomicDecimal ad5 = new AtomicDecimal(64.64646464m);
            AtomicDecimal ad6 = ad5.DivideByPowerOf2(6);
            Assert.AreEqual(64.64646464m, ad5.ToDecimal());
            Assert.AreEqual(1.01010101m, ad6.ToDecimal());
        }
    }
}
