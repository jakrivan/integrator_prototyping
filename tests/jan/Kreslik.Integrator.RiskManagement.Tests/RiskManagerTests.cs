﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.RiskManagement;
using Moq;
using NUnit.Framework;

namespace Kreslik.Integrator.BusinessLayer.Tests
{

    internal interface IOrderRequestInfoBuilder
    {
        BankPoolClientOrderRequestInfo CreateLimit(decimal totalAmountBaseAbs, decimal requestedPrice,
                                                   DealDirection side, Symbol symbol);

        BankPoolClientOrderRequestInfo CreateMarket(decimal totalAmountBaseAbs, DealDirection side, Symbol symbol);

        BankPoolClientOrderRequestInfo CreateQuoted(decimal totalAmountBaseAbs, decimal requestedPrice,
                                                    DealDirection side,
                                                    PriceObject price);
        //BankPoolClientOrder CreateCompositeClientOrder(List<BankPoolClientOrderRequestInfo> orderRequestInfos,
        //                                       CompositeOrderStrategy compositeOrderStrategy);
        //BankPoolClientOrder CreateCompositeClientOrder(List<BankPoolClientOrderRequestInfo> orderRequestInfos,
        //                                       CompositeOrderStrategy compositeOrderStrategy, TimeSpan maximumAcceptedPriceAge);
        BankPoolClientOrder CreateClientOrder(BankPoolClientOrderRequestInfo orderRequestInfo);
    }

    internal class OrderRequestInfoBuilder : ClientGatewayBase, IOrderRequestInfoBuilder
    {
        private OrderRequestInfoBuilder()
            : base(null, null)
        { }

        public static IOrderRequestInfoBuilder Instance = new OrderRequestInfoBuilder();

        public BankPoolClientOrderRequestInfo CreateLimit(decimal totalAmountBaseAbs, decimal requestedPrice,
                                                   DealDirection side, Symbol symbol)
        {
            return this.Targets.BankPool.CreateLimitOrderRequest(totalAmountBaseAbs, requestedPrice, side, symbol, false);
        }

        public BankPoolClientOrderRequestInfo CreateMarket(decimal totalAmountBaseAbs, DealDirection side, Symbol symbol)
        {
            return this.Targets.BankPool.CreateMarketOrderRequest(totalAmountBaseAbs, side, symbol, false);
        }

        public BankPoolClientOrderRequestInfo CreateQuoted(decimal totalAmountBaseAbs, decimal requestedPrice,
                                                           DealDirection side,
                                                           PriceObject price)
        {
            return this.Targets.BankPool.CreateQuotedOrderRequest(totalAmountBaseAbs, requestedPrice, side, price, false);
        }

        public BankPoolClientOrder CreateClientOrder(BankPoolClientOrderRequestInfo orderRequestInfo)
        {
            return this.Targets.BankPool.CreateClientOrder(orderRequestInfo);
        }

        protected override void LocalResourcesCleanupRoutine()
        {
            throw new NotImplementedException();
        }

        protected override string ClientIdentityInternal
        {
            get { return "TestClient_001"; }
            set
            {
                throw new NotImplementedException();
            }
        }

        protected override IntegratorRequestResult SubmitRequestInternal(IntegratorRequestInfo clientOrderBase)
        {
            throw new NotImplementedException();
        }

        protected override bool IsConnectionChannelClosed
        {
            get { throw new NotImplementedException(); }
        }
    }

    internal class NoIdentityQuote
    {
        public PriceObjectInternal AskPrice { get; private set; }
        public PriceObjectInternal BidPrice { get; private set; }

        public PriceObjectInternal GetPrice(PriceSide side)
        {
            if (side == PriceSide.Ask)
                return this.AskPrice;
            else
                return this.BidPrice;
        }

        public NoIdentityQuote(decimal price, decimal size, PriceSide side, Symbol symbol, Counterparty counterparty,
            string subscriptionidentity, DateTime counterpartySendTime, MarketDataRecordType marketDataRecordType)
        {
            var priceObject = PriceObjectInternal.GetAllocatedTestingPriceObject(price, size, side, symbol, counterparty,
                subscriptionidentity, marketDataRecordType, DateTime.UtcNow, counterpartySendTime);

            if (side == PriceSide.Ask)
                this.AskPrice = priceObject;
            else
                this.BidPrice = priceObject;
        }

        public static PriceObjectInternal DummyPrice = PriceObjectInternal.GetAllocatedTestingPriceObject(0, 0,
            PriceSide.Ask, Common.Symbol.NULL, Counterparty.SOC, null, MarketDataRecordType.BankQuoteData, DateTime.MinValue, DateTime.MinValue);
    }

    [TestFixture]
    public class RiskManagerTests
    {
        private class RiskManagerUnderTests : RiskManager
        {
            public static ISymbolsInfo GetSymbolsInfoMock()
            {
                var symInfoMock = new Mock<ISymbolsInfo>();
                symInfoMock.Setup(si => si.IsSupported(It.IsAny<Currency>())).Returns(true);
                return symInfoMock.Object;
            }

            public static ISettlementDatesKeeper GetSettlementsInfoMock()
            {
                var settlementsInfoMock = new Mock<ISettlementDatesKeeper>();
                settlementsInfoMock.Setup(si => si.GetSettlementDate(It.IsAny<Symbol>())).Returns(DateTime.UtcNow.Date);
                return settlementsInfoMock.Object;
            }

            public RiskManagerUnderTests(ILogger logger, RiskManagerSettingsBag riskManagerSettingsBag)
                : base(logger, riskManagerSettingsBag, new Mock<IPriceStreamMonitor>().Object, new Mock<IPersistedNopProvider>().Object, GetSymbolsInfoMock(), GetSettlementsInfoMock())
            { }

            public RiskManagerUnderTests(ILogger logger, RiskManagerSettingsBag riskManagerSettingsBag, IPriceStreamMonitor priceStreamMonitor)
                : base(logger, riskManagerSettingsBag, priceStreamMonitor, new Mock<IPersistedNopProvider>().Object, GetSymbolsInfoMock(), GetSettlementsInfoMock())
            { }
        }

        [Test]
        public void RiskManager_CumulativeSizesTest_AddExternalOrderWithoutInternalOrderNotAllowed()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var ordersTransmissionControlMock = new Mock<IExternalOrdersTransmissionControl>();
            var settingsBag = new RiskManagerSettingsBag()
                {
                    PersistentOrdersTransmissionControl = ordersTransmissionControlMock.Object,
                    CumulativeSizesCheckAllowed = true
                };

            IRiskManager riskManager = new RiskManagerUnderTests(testLogger, settingsBag);

            OrderRequestInfo orderInfo = OrderRequestInfo.CreateQuoted(100m, 10m, DealDirection.Sell,
                                                                 new NoIdentityQuote(11m, 150m, PriceSide.Bid,
                                                                                     Symbol.AUD_CAD, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice, false);
            var externalOrderMock = new Mock<IIntegratorOrderExternal>();
            externalOrderMock.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo);

            decimal nopEstimate;
            bool allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsFalse(allowed);

            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void RiskManager_CumulativeSizesTest_AddExternalOrderWillAllowOnlyMatchingInternalOrderUpToSize()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var ordersTransmissionControlMock = new Mock<IExternalOrdersTransmissionControl>();
            var settingsBag = new RiskManagerSettingsBag()
            {
                PersistentOrdersTransmissionControl = ordersTransmissionControlMock.Object,
                CumulativeSizesCheckAllowed = true
            };

            IRiskManager riskManager = new RiskManagerUnderTests(testLogger, settingsBag);

            riskManager.AddInternalOrderAndCheckIfAllowed(
                OrderRequestInfoBuilder.Instance.CreateClientOrder(OrderRequestInfoBuilder.Instance.CreateLimit(200m, 20m, DealDirection.Sell, Symbol.AUD_CAD)));


            OrderRequestInfo orderInfo = OrderRequestInfo.CreateQuoted(110m, 10m, DealDirection.Sell,
                                                                 new NoIdentityQuote(11m, 150m, PriceSide.Bid,
                                                                                     Symbol.AUD_CAD, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice, false);
            var externalOrderMock = new Mock<IIntegratorOrderExternal>();
            externalOrderMock.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo);

            decimal nopEstimate;
            bool allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);
            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Never());


            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsFalse(allowed);
            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void RiskManager_CumulativeSizesTest_AddExternalOrderWillAllowOnlyMatchingInternalOrderUpToSize_RejectsAreSubstracted()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var ordersTransmissionControlMock = new Mock<IExternalOrdersTransmissionControl>();
            var settingsBag = new RiskManagerSettingsBag()
            {
                PersistentOrdersTransmissionControl = ordersTransmissionControlMock.Object,
                CumulativeSizesCheckAllowed = true
            };

            IRiskManager riskManager = new RiskManagerUnderTests(testLogger, settingsBag);

            riskManager.AddInternalOrderAndCheckIfAllowed(
                OrderRequestInfoBuilder.Instance.CreateClientOrder(OrderRequestInfoBuilder.Instance.CreateLimit(200m, 20m, DealDirection.Sell, Symbol.AUD_CAD)));


            OrderRequestInfo orderInfo = OrderRequestInfo.CreateQuoted(110m, 10m, DealDirection.Sell,
                                                                 new NoIdentityQuote(11m, 150m, PriceSide.Bid,
                                                                                     Symbol.AUD_CAD, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice, false);
            var externalOrderMock = new Mock<IIntegratorOrderExternal>();
            externalOrderMock.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo);

            decimal nopEstimate;
            bool allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);
            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Never());

            riskManager.UpdateStateOfExternalOrder(externalOrderMock.Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected));

            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);
            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Never());
        }

        [Test]
        public void RiskManager_CumulativeSizesTest_AddExternalOrderWillDisallowMismatchedExternalOrders()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var ordersTransmissionControlMock = new Mock<IExternalOrdersTransmissionControl>();
            var settingsBag = new RiskManagerSettingsBag()
            {
                PersistentOrdersTransmissionControl = ordersTransmissionControlMock.Object,
                CumulativeSizesCheckAllowed = true
            };

            IRiskManager riskManager = new RiskManagerUnderTests(testLogger, settingsBag);

            riskManager.AddInternalOrderAndCheckIfAllowed(
                OrderRequestInfoBuilder.Instance.CreateClientOrder(OrderRequestInfoBuilder.Instance.CreateLimit(2000m, 20m, DealDirection.Sell, Symbol.AUD_CAD)));


            OrderRequestInfo orderInfo = OrderRequestInfo.CreateQuoted(110m, 10m, DealDirection.Buy,
                                                                 new NoIdentityQuote(11m, 150m, PriceSide.Bid,
                                                                                     Symbol.AUD_CAD, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice, false);
            var externalOrderMock = new Mock<IIntegratorOrderExternal>();
            externalOrderMock.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo);

            decimal nopEstimate;
            bool allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsFalse(allowed);
            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Once());


            orderInfo = OrderRequestInfo.CreateQuoted(110m, 10m, DealDirection.Sell,
                                                                 new NoIdentityQuote(11m, 150m, PriceSide.Bid,
                                                                                     Symbol.AUD_CHF, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice, false);
            externalOrderMock.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo);


            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsFalse(allowed);
            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Exactly(2));
        }


        [Test]
        public void RiskManager_CumulativeSizesTest_AddExternalOrderWillAllowOnlyMatchingInternalOrderUpToSize_RejectsAreSubstracted2()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var ordersTransmissionControlMock = new Mock<IExternalOrdersTransmissionControl>();
            var settingsBag = new RiskManagerSettingsBag()
            {
                PersistentOrdersTransmissionControl = ordersTransmissionControlMock.Object,
                CumulativeSizesCheckAllowed = true
            };

            IRiskManager riskManager = new RiskManagerUnderTests(testLogger, settingsBag);

            riskManager.AddInternalOrderAndCheckIfAllowed(
                OrderRequestInfoBuilder.Instance.CreateClientOrder(OrderRequestInfoBuilder.Instance.CreateLimit(200m, 20m, DealDirection.Buy, Symbol.AUD_CAD)));


            OrderRequestInfo orderInfo = OrderRequestInfo.CreateQuoted(200m, 10m, DealDirection.Buy,
                                                                 new NoIdentityQuote(9m, 250m, PriceSide.Ask,
                                                                                     Symbol.AUD_CAD, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice, false);
            var externalOrderMock = new Mock<IIntegratorOrderExternal>();
            externalOrderMock.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo);

            decimal nopEstimate;
            bool allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);
            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Never());

            ClientOrderBase clOrd = OrderRequestInfoBuilder.Instance.CreateClientOrder(OrderRequestInfoBuilder.Instance.CreateLimit(200m, 20m, DealDirection.Buy,
                                                                                   Symbol.AUD_CAD));

            riskManager.AddInternalOrderAndCheckIfAllowed(clOrd);

            riskManager.UpdateStateOfExternalOrder(externalOrderMock.Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected));

            riskManager.CancelInternalOrder(clOrd, clOrd.SizeBaseAbsActive);

            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);
            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Never());
        }

        private decimal[] GetUsdConversionRates()
        {
            decimal[] usdConversionRates = new decimal[Currency.ValuesCount];

            //initialize everything to high, so we can spot usage of improper indexes
            for (int currencyIdx = 0; currencyIdx < Currency.ValuesCount; currencyIdx++)
            {
                usdConversionRates[currencyIdx] = 1000000000m;
            }

            usdConversionRates[(int)Currency.USD] = 1;
            usdConversionRates[(int)Currency.EUR] = 1.2m;
            usdConversionRates[(int)Currency.GBP] = 1.56591m;
            usdConversionRates[(int)Currency.JPY] = 1m / 94.372m;

            return usdConversionRates;
        }

        [Test]
        public void RiskManager_NOPChecksTest_AddExternalOrderWillBeAllowedIfUnderNop_UnconfirmedOrdersAreNotSubstractedFromNop()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var ordersTransmissionControlMock = new Mock<IExternalOrdersTransmissionControl>();
            var settingsBag = new RiskManagerSettingsBag()
            {
                PersistentOrdersTransmissionControl = ordersTransmissionControlMock.Object,
                NopRiskCheckAllowed = true
            };

            var priceStreamMonitorMock = new Mock<IPriceStreamMonitor>();
            priceStreamMonitorMock.Setup(pm => pm.UsdConversionMultipliers).Returns(GetUsdConversionRates());

            IRiskManager riskManager = new RiskManagerUnderTests(testLogger, settingsBag, priceStreamMonitorMock.Object);

            OrderRequestInfo orderInfo = OrderRequestInfo.CreateQuoted(100m, 10m, DealDirection.Buy,
                                                                 new NoIdentityQuote(1.2m, 150m, PriceSide.Ask,
                                                                                     Symbol.EUR_USD, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice, false);
            var externalOrderMock = new Mock<IIntegratorOrderExternal>();
            externalOrderMock.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo);

            decimal nopEstimate;
            bool allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);
            Assert.AreEqual(100m * 1.2m, nopEstimate);
            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Never());


            OrderRequestInfo orderInfo2 = OrderRequestInfo.CreateQuoted(900m, 1.1m, DealDirection.Sell,
                                                                 new NoIdentityQuote(1.2m, 1500m, PriceSide.Bid,
                                                                                     Symbol.EUR_USD, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice, false);

            var externalOrderMock2 = new Mock<IIntegratorOrderExternal>();
            externalOrderMock2.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo2);

            //As First order was not confirmed, we still need to count with case that it would be rejected
            // therefore this will not come through
            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock2.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsFalse(allowed);
            Assert.AreEqual(900m * 1.2m, nopEstimate);
            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Once());
        }


        [Test]
        public void RiskManager_NOPChecksTest_AddExternalOrderWillBeAllowedIfUnderNop_ConfirmedOrdersAreSubstractedFromNop()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var ordersTransmissionControlMock = new Mock<IExternalOrdersTransmissionControl>();
            var settingsBag = new RiskManagerSettingsBag()
            {
                PersistentOrdersTransmissionControl = ordersTransmissionControlMock.Object,
                NopRiskCheckAllowed = true
            };

            var priceStreamMonitorMock = new Mock<IPriceStreamMonitor>();
            priceStreamMonitorMock.Setup(pm => pm.UsdConversionMultipliers).Returns(GetUsdConversionRates());

            IRiskManager riskManager = new RiskManagerUnderTests(testLogger, settingsBag, priceStreamMonitorMock.Object);

            OrderRequestInfo orderInfo = OrderRequestInfo.CreateQuoted(100m, 10m, DealDirection.Buy,
                                                                 new NoIdentityQuote(1.2m, 150m, PriceSide.Ask,
                                                                                     Symbol.EUR_USD, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice, false);
            var externalOrderMock = new Mock<IIntegratorOrderExternal>();
            externalOrderMock.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo);

            decimal nopEstimate;
            bool allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);
            Assert.AreEqual(100m * 1.2m, nopEstimate);
            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Never());

            externalOrderMock.Setup(ord => ord.FilledAmount).Returns(orderInfo.SizeBaseAbsInitial);
            riskManager.UpdateStateOfExternalOrder(externalOrderMock.Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled));


            OrderRequestInfo orderInfo2 = OrderRequestInfo.CreateQuoted(900m, 1.1m, DealDirection.Sell,
                                                                 new NoIdentityQuote(1.2m, 1500m, PriceSide.Bid,
                                                                                     Symbol.EUR_USD, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice, false);

            var externalOrderMock2 = new Mock<IIntegratorOrderExternal>();
            externalOrderMock2.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo2);

            //As First order was confirmed, we substracted it from bounds of NOP
            // therefore this will come through
            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock2.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);
            Assert.AreEqual(800m * 1.2m, nopEstimate);
            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Never());
        }

        [Test]
        public void RiskManager_NOPChecksTest_NopCalculationExample()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var ordersTransmissionControlMock = new Mock<IExternalOrdersTransmissionControl>();
            var settingsBag = new RiskManagerSettingsBag()
            {
                PersistentOrdersTransmissionControl = ordersTransmissionControlMock.Object,
                NopRiskCheckAllowed = true
            };

            var priceStreamMonitorMock = new Mock<IPriceStreamMonitor>();
            priceStreamMonitorMock.Setup(pm => pm.UsdConversionMultipliers).Returns(GetUsdConversionRates());

            IRiskManager riskManager = new RiskManagerUnderTests(testLogger, settingsBag, priceStreamMonitorMock.Object);


            OrderRequestInfo orderInfo = OrderRequestInfo.CreateQuoted(1000000m, 10m, DealDirection.Buy,
                                                                 new NoIdentityQuote(1.33401m, 1500000m, PriceSide.Ask,
                                                                                     Symbol.EUR_USD, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice, false);
            var externalOrderMock = new Mock<IIntegratorOrderExternal>();
            externalOrderMock.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo);

            decimal nopEstimate;
            bool allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);



            OrderRequestInfo orderInfo2 = OrderRequestInfo.CreateQuoted(1250000m, 0.1m, DealDirection.Sell,
                                                                 new NoIdentityQuote(0.8518m, 1500000m, PriceSide.Bid,
                                                                                     Symbol.EUR_GBP, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice, false);
            var externalOrderMock2 = new Mock<IIntegratorOrderExternal>();
            externalOrderMock2.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo2);

            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock2.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);



            OrderRequestInfo orderInfo3 = OrderRequestInfo.CreateQuoted(3000000m, 100m, DealDirection.Buy,
                                                                 new NoIdentityQuote(94.378m, 3500000m, PriceSide.Ask,
                                                                                     Symbol.USD_JPY, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice, false);
            var externalOrderMock3 = new Mock<IIntegratorOrderExternal>();
            externalOrderMock3.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo3);

            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock3.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);




            OrderRequestInfo orderInfo4 = OrderRequestInfo.CreateQuoted(1000000m, 10m, DealDirection.Sell,
                                                                 new NoIdentityQuote(147.758m, 3500000m, PriceSide.Bid,
                                                                                     Symbol.GBP_JPY, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice, false);
            var externalOrderMock4 = new Mock<IIntegratorOrderExternal>();
            externalOrderMock4.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo4);

            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock4.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);



            OrderRequestInfo orderInfo5 = OrderRequestInfo.CreateQuoted(1000000m, 10m, DealDirection.Buy,
                                                                 new NoIdentityQuote(1.56597m, 3500000m, PriceSide.Ask,
                                                                                     Symbol.GBP_USD, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice, false);
            var externalOrderMock5 = new Mock<IIntegratorOrderExternal>();
            externalOrderMock5.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo5);

            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock5.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);



            externalOrderMock.Setup(ord => ord.FilledAmount).Returns(orderInfo.SizeBaseAbsInitial);
            externalOrderMock2.Setup(ord => ord.FilledAmount).Returns(orderInfo2.SizeBaseAbsInitial);
            externalOrderMock3.Setup(ord => ord.FilledAmount).Returns(orderInfo3.SizeBaseAbsInitial);
            externalOrderMock4.Setup(ord => ord.FilledAmount).Returns(orderInfo4.SizeBaseAbsInitial);
            externalOrderMock5.Setup(ord => ord.FilledAmount).Returns(orderInfo5.SizeBaseAbsInitial);

            riskManager.UpdateStateOfExternalOrder(externalOrderMock.Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled));
            riskManager.UpdateStateOfExternalOrder(externalOrderMock2.Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled));
            riskManager.UpdateStateOfExternalOrder(externalOrderMock3.Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled));
            riskManager.UpdateStateOfExternalOrder(externalOrderMock4.Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled));
            riskManager.UpdateStateOfExternalOrder(externalOrderMock5.Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled));





            OrderRequestInfo orderInfo6 = OrderRequestInfo.CreateQuoted(1m, 10m, DealDirection.Buy,
                                                                 new NoIdentityQuote(1.56597m, 3500000m, PriceSide.Ask,
                                                                                     Symbol.GBP_USD, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice, false);
            var externalOrderMock6 = new Mock<IIntegratorOrderExternal>();
            externalOrderMock6.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo6);

            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock6.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(nopEstimate > 1700000 && nopEstimate < 1800000);



            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Never());
        }


        [Test]
        public void RiskManager_PriceDeviationTest_NotEnoughBackendPrices_OrderWillNotBeAllowed()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var ordersTransmissionControlMock = new Mock<IExternalOrdersTransmissionControl>();
            var settingsBag = new RiskManagerSettingsBag()
            {
                PersistentOrdersTransmissionControl = ordersTransmissionControlMock.Object,
                NopRiskCheckAllowed = false,
                PriceDeviationCheckAllowed = true
            };

            var priceStreamMonitorMock = new Mock<IPriceStreamMonitor>();
            priceStreamMonitorMock.Setup(pm => pm.UsdConversionMultipliers).Returns(GetUsdConversionRates());

            IRiskManager riskManager = new RiskManagerUnderTests(testLogger, settingsBag, priceStreamMonitorMock.Object);

            //Despite we request price 1.2m, Integrator will automatically create improvement (as it's quoted order)
            // and will request price 1.589 which meets price deviation criteria
            OrderRequestInfo orderInfo = OrderRequestInfo.CreateQuoted(100m, 1.2m, DealDirection.Sell,
                                                                 new NoIdentityQuote(1.589m, 150m, PriceSide.Bid,
                                                                                     Symbol.EUR_USD, Counterparty.BNP,
                                                                                     "---", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice, false);
            var externalOrderMock = new Mock<IIntegratorOrderExternal>();
            externalOrderMock.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo);

            decimal nopEstimate;
            bool allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsFalse(allowed);

            ordersTransmissionControlMock.Verify(
                ctrl =>
                ctrl.DisableTransmission(
                    It.Is<string>(
                        reason =>
                        reason.Contains("coming in but online price deviation check is still not fully initialized"))),
                Times.Once());


            OrderRequestInfo orderInfoLimit = OrderRequestInfo.CreateLimit(100m, 1.2m, DealDirection.Sell,
                                                                           Symbol.EUR_USD, TimeInForce.ImmediateOrKill, false);

            var externalLimitOrderMock = new Mock<IIntegratorOrderExternal>();
            externalLimitOrderMock.Setup(ord => ord.OrderRequestInfo).Returns(orderInfoLimit);
            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalLimitOrderMock.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsFalse(allowed);

            ordersTransmissionControlMock.Verify(
                ctrl =>
                ctrl.DisableTransmission(
                    It.Is<string>(
                        reason =>
                        reason.Contains("coming in but online price deviation check is still not fully initialized"))),
                Times.Exactly(2));
        }

        [Test]
        public void RiskManager_PriceDeviationTest_HasEnoughBackendPrices_OnlineWillBeUsed()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var ordersTransmissionControlMock = new Mock<IExternalOrdersTransmissionControl>();
            var settingsBag = new RiskManagerSettingsBag()
            {
                PersistentOrdersTransmissionControl = ordersTransmissionControlMock.Object,
                NopRiskCheckAllowed = false,
                PriceDeviationCheckAllowed = true,
                OnlineDeviationCheckRate = Enumerable.Repeat(0m, Symbol.ValuesCount).ToArray(),
                OnlineDeviationCheckMaxPriceAge = TimeSpan.FromDays(1)
            };
            //settingsBag.MinPrices[(int)Symbol.EUR_USD] = 1.3m;
            settingsBag.OnlineDeviationCheckRate[(int)Symbol.EUR_USD] = 0.05m; //5%

            var priceHistoryProvider = new Mock<IPriceHistoryProvider>();
            priceHistoryProvider.Setup(provider => provider.GetLastPricesForSymbol(It.IsAny<Symbol>(), It.IsAny<int>()))
                                .Returns(new List<Tuple<decimal, PriceSide, DateTime>>()
                                    {
                                        new Tuple<decimal, PriceSide, DateTime>(10, PriceSide.Ask, DateTime.UtcNow),
                                        new Tuple<decimal, PriceSide, DateTime>(1, PriceSide.Bid, DateTime.UtcNow),
                                        new Tuple<decimal, PriceSide, DateTime>(2, PriceSide.Bid, DateTime.UtcNow),
                                        new Tuple<decimal, PriceSide, DateTime>(3, PriceSide.Ask, DateTime.UtcNow)
                                    });

            PriceStreamsMonitor priceStreamsMonitor = new PriceStreamsMonitor(testLogger, priceHistoryProvider.Object, 4);

            IRiskManager riskManager = new RiskManagerUnderTests(testLogger, settingsBag, priceStreamsMonitor);
            //give the parallel initialization chance to run
            Thread.Sleep(100);

            //There is a weird bug in Moq preventing us from verifying this asynchronous task
            //System.Threading.Thread.Sleep(1000);
            //priceHistoryProvider.Verify(
            //    provider => provider.GetLastPricesForSymbol(It.IsAny<Symbol>(), It.Is<int>(i => i == 4)),
            //    Times.Exactly(Symbol.ValuesCount));

            //2.38 is within 5% of median - 2.5
            OrderRequestInfo orderInfo1 = OrderRequestInfo.CreateLimit(100m, 2.38m, DealDirection.Sell,
                                                                           Symbol.EUR_USD, TimeInForce.ImmediateOrKill, false);
            var externalOrderMock1 = new Mock<IIntegratorOrderExternal>();
            externalOrderMock1.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo1);

            decimal nopEstimate;
            bool allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock1.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);

            //Now there is no price improvement and this price doesn't change offline price check
            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Never());

            //2.37 is NOT within 5% of median - 2.5
            OrderRequestInfo orderInfo2 = OrderRequestInfo.CreateLimit(100m, 2.37m, DealDirection.Sell,
                                                                           Symbol.EUR_USD, TimeInForce.ImmediateOrKill, false);

            var externalOrderMock2 = new Mock<IIntegratorOrderExternal>();
            externalOrderMock2.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo2);
            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock2.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsFalse(allowed);

            ordersTransmissionControlMock.Verify(
                ctrl =>
                ctrl.DisableTransmission(
                    It.Is<string>(
                        reason =>
                        reason.Contains("didn't pass the price deviation check (against current median price"))),
                Times.Once());
        }

        bool b = false;

        [Test]
        public void RiskManager_PriceDeviationTest_NotEnoughBackendPrices_OnlineWillBeUsedOnceEnaughPricesArrive()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var ordersTransmissionControlMock = new Mock<IExternalOrdersTransmissionControl>();
            var settingsBag = new RiskManagerSettingsBag()
            {
                PersistentOrdersTransmissionControl = ordersTransmissionControlMock.Object,
                NopRiskCheckAllowed = false,
                PriceDeviationCheckAllowed = true,
                //UsdConversionMultipliers = GetUsdConversionRates(),
                OnlineDeviationCheckRate = Enumerable.Repeat(0m, Symbol.ValuesCount).ToArray(),
                OnlineDeviationCheckMaxPriceAge = TimeSpan.FromDays(1)
            };
            settingsBag.OnlineDeviationCheckRate[(int)Symbol.EUR_USD] = 0.05m; //5%

            var priceHistoryProvider = new Mock<IPriceHistoryProvider>();
            priceHistoryProvider.Setup(provider => provider.GetLastPricesForSymbol(It.IsAny<Symbol>(), It.IsAny<int>()))
                                .Returns(new List<Tuple<decimal, PriceSide, DateTime>>()
                                    {
                                        new Tuple<decimal, PriceSide, DateTime>(10, PriceSide.Ask, DateTime.UtcNow),
                                        new Tuple<decimal, PriceSide, DateTime>(1, PriceSide.Bid, DateTime.UtcNow),
                                        new Tuple<decimal, PriceSide, DateTime>(2, PriceSide.Bid, DateTime.UtcNow),
                                        new Tuple<decimal, PriceSide, DateTime>(3, PriceSide.Ask, DateTime.UtcNow)
                                    });

            PriceStreamsMonitor priceStreamsMonitor = new PriceStreamsMonitor(testLogger, priceHistoryProvider.Object, 4);

            IRiskManager riskManager = new RiskManagerUnderTests(testLogger, settingsBag, priceStreamsMonitor);

            //2.38 is NOT within offline price 3.3
            OrderRequestInfo orderInfo1 = OrderRequestInfo.CreateLimit(100m, 2.38m, DealDirection.Sell,
                                                                           Symbol.EUR_USD, TimeInForce.ImmediateOrKill, false);
            var externalOrderMock1 = new Mock<IIntegratorOrderExternal>();
            externalOrderMock1.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo1);

            decimal nopEstimate;
            bool allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock1.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsFalse(allowed);

            ordersTransmissionControlMock.Verify(
                ctrl =>
                ctrl.DisableTransmission(
                    It.Is<string>(
                        reason =>
                        reason.Contains("coming in but online price deviation check is still not fully initialized"))),
                Times.Once());


            priceStreamsMonitor.OnNewPrice(new NoIdentityQuote(3m, 1000m, PriceSide.Ask, Symbol.EUR_USD,
                                                                         Counterparty.BOA, null, DateTime.MinValue, MarketDataRecordType.BankQuoteData).AskPrice, ref b);

            //2.38 is within 5% of median - 2.5
            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock1.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);

            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Once());


            //Now we will replace oldest price - 10m
            priceStreamsMonitor.OnNewPrice(new NoIdentityQuote(13.256m, 1000m, PriceSide.Ask, Symbol.EUR_USD,
                                                                         Counterparty.BOA, null, DateTime.MinValue, MarketDataRecordType.BankQuoteData).AskPrice, ref b);

            //2.38 is within 5% of median - 2.5
            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock1.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);

            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Once());



            //Now we will replace oldest price - 1m
            priceStreamsMonitor.OnNewPrice(new NoIdentityQuote(7m, 1000m, PriceSide.Ask, Symbol.EUR_USD,
                                                                         Counterparty.BOA, null, DateTime.MinValue, MarketDataRecordType.BankQuoteData).AskPrice, ref b);

            //2.38 is within 5% of median - 5
            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock1.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsFalse(allowed);

            ordersTransmissionControlMock.Verify(
                ctrl =>
                ctrl.DisableTransmission(
                    It.Is<string>(
                        reason =>
                        reason.Contains("didn't pass the price deviation check (against current median price"))),
                Times.Once());
        }


        [Test]
        public void RiskManager_PriceDeviationTest_TooOldPrices_OrderWillNotBeAllowed()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var ordersTransmissionControlMock = new Mock<IExternalOrdersTransmissionControl>();
            var settingsBag = new RiskManagerSettingsBag()
            {
                PersistentOrdersTransmissionControl = ordersTransmissionControlMock.Object,
                NopRiskCheckAllowed = false,
                PriceDeviationCheckAllowed = true,
                OnlineDeviationCheckRate = Enumerable.Repeat(0m, Symbol.ValuesCount).ToArray(),
                OnlineDeviationCheckMaxPriceAge = TimeSpan.FromDays(1)
            };
            settingsBag.OnlineDeviationCheckRate[(int)Symbol.EUR_USD] = 0.05m; //5%

            var priceHistoryProvider = new Mock<IPriceHistoryProvider>();
            priceHistoryProvider.Setup(provider => provider.GetLastPricesForSymbol(It.IsAny<Symbol>(), It.IsAny<int>()))
                                .Returns(new List<Tuple<decimal, PriceSide, DateTime>>()
                                    {
                                        new Tuple<decimal, PriceSide, DateTime>(10, PriceSide.Ask, DateTime.UtcNow),
                                        new Tuple<decimal, PriceSide, DateTime>(1, PriceSide.Bid, DateTime.UtcNow),
                                        new Tuple<decimal, PriceSide, DateTime>(2, PriceSide.Bid, DateTime.UtcNow),
                                        new Tuple<decimal, PriceSide, DateTime>(3, PriceSide.Ask, DateTime.UtcNow)
                                    });

            PriceStreamsMonitor priceStreamsMonitor = new PriceStreamsMonitor(testLogger, priceHistoryProvider.Object, 4);

            IRiskManager riskManager = new RiskManagerUnderTests(testLogger, settingsBag, priceStreamsMonitor);

            OrderRequestInfo orderInfo1 = OrderRequestInfo.CreateLimit(100m, 2.38m, DealDirection.Sell,
                                                                           Symbol.EUR_USD, TimeInForce.ImmediateOrKill, false);
            var externalOrderMock1 = new Mock<IIntegratorOrderExternal>();
            externalOrderMock1.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo1);

            decimal nopEstimate;
            bool allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock1.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);

            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Never());


            priceStreamsMonitor.OnNewPrice(new NoIdentityQuote(1m, 1000m, PriceSide.Ask, Symbol.EUR_USD,
                                                                         Counterparty.BOA, null, DateTime.MinValue, MarketDataRecordType.BankQuoteData).AskPrice, ref b);
            priceStreamsMonitor.OnNewPrice(new NoIdentityQuote(1m, 1000m, PriceSide.Ask, Symbol.EUR_USD,
                                                                         Counterparty.BOA, null, DateTime.MinValue, MarketDataRecordType.BankQuoteData).AskPrice, ref b);
            priceStreamsMonitor.OnNewPrice(new NoIdentityQuote(1m, 1000m, PriceSide.Ask, Symbol.EUR_USD,
                                                                         Counterparty.BOA, null, DateTime.MinValue, MarketDataRecordType.BankQuoteData).AskPrice, ref b);
            priceStreamsMonitor.OnNewPrice(new NoIdentityQuote(1m, 1000m, PriceSide.Ask, Symbol.EUR_USD,
                                                                         Counterparty.BOA, null, DateTime.MinValue, MarketDataRecordType.BankQuoteData).AskPrice, ref b);

            //2.38 is well above median, but median is too old
            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock1.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsFalse(allowed);

            ordersTransmissionControlMock.Verify(
                ctrl =>
                ctrl.DisableTransmission(
                    It.Is<string>(
                        reason =>
                        reason.Contains("didn't pass the price deviation check as the oldest price was from"))),
                Times.Once());

            priceStreamsMonitor.OnNewPrice(new NoIdentityQuote(1m, 1000m, PriceSide.Ask, Symbol.EUR_USD,
                                                                         Counterparty.BOA, null, DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice, ref b);
            priceStreamsMonitor.OnNewPrice(new NoIdentityQuote(1m, 1000m, PriceSide.Ask, Symbol.EUR_USD,
                                                                         Counterparty.BOA, null, DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice, ref b);
            priceStreamsMonitor.OnNewPrice(new NoIdentityQuote(1m, 1000m, PriceSide.Ask, Symbol.EUR_USD,
                                                                         Counterparty.BOA, null, DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice, ref b);
            priceStreamsMonitor.OnNewPrice(new NoIdentityQuote(1m, 1000m, PriceSide.Ask, Symbol.EUR_USD,
                                                                         Counterparty.BOA, null, DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice, ref b);

            //2.38 is well above median, and median is fresh
            allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock1.Object, out nopEstimate) == RiskManagementCheckResult.Accepted;
            Assert.IsTrue(allowed);

            ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Once());
        }





        //[Test]
        //public void RiskManager_NOPChecksTest_AddExternalOrderWillBeAllowedIfUnderNop_PerformsEvenForBiggerNumberOfOpenOrders()
        //{
        //    ILogger testLogger = new TestLogger(Assert.Fail);
        //    var ordersTransmissionControlMock = new Mock<IExternalOrdersTransmissionControl>();
        //    var settingsBag = new RiskManagerSettingsBag()
        //    {
        //        PersistentOrdersTransmissionControl = ordersTransmissionControlMock.Object,
        //        NopRiskCheckAllowed = true
        //        UsdConversionMultipliers = GetUsdConversionRates()
        //    };

        //    IRiskManager riskManager = new RiskManagerUnderTests(testLogger, settingsBag);

        //    for (int orderId = 0; orderId < 12; orderId++)
        //    {
        //        testLogger.Log(LogLevel.Info, "Adding {0} buy order", orderId);

        //        OrderRequestInfo orderInfo = OrderRequestInfo.Create(100m, 10m, DealDirection.Buy,
        //                                                         new NoIdentityQuote(1.2m, 150m, PriceSide.Ask,
        //                                                                             Symbol.EUR_USD, Counterparty.BNP,
        //                                                                             "---"));
        //        var externalOrderMock = new Mock<IIntegratorOrderExternal>();
        //        externalOrderMock.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo);

        //        bool allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock.Object);
        //        Assert.IsTrue(allowed);
        //        ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Never());


        //        testLogger.Log(LogLevel.Info, "Adding {0} sell order", orderId);

        //        OrderRequestInfo orderInfo2 = OrderRequestInfo.Create(100m, 1.2m, DealDirection.Sell,
        //                                                         new NoIdentityQuote(1.2m, 1500m, PriceSide.Bid,
        //                                                                             Symbol.EUR_USD, Counterparty.BNP,
        //                                                                             "---"));

        //        var externalOrderMock2 = new Mock<IIntegratorOrderExternal>();
        //        externalOrderMock2.Setup(ord => ord.OrderRequestInfo).Returns(orderInfo2);

        //        allowed = riskManager.AddExternalOrderAndCheckIfAllowed(externalOrderMock2.Object);
        //        Assert.IsTrue(allowed);
        //        ordersTransmissionControlMock.Verify(ctrl => ctrl.DisableTransmission(It.IsAny<string>()), Times.Never());
        //    }

        //    string s = (testLogger as TestLogger).Content;
        //}
    }
}
