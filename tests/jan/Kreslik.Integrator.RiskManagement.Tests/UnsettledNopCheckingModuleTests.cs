﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.LowLatencyUtils;
using Moq;
using NUnit.Framework;

namespace Kreslik.Integrator.RiskManagement.Tests
{
    [TestFixture]
    public class UnsettledNopCheckingModuleTests
    {
        [Test]
        public void UnsettledNopCheckingModule_CalculateNominalNopDiffTest_NoDiffAtAll()
        {
            AtomicSize oldPos, newPos, shortDiff, longDiff;

            oldPos = AtomicSize.ZERO;
            newPos = AtomicSize.ZERO;
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.ZERO, longDiff);
            Assert.AreEqual(AtomicSize.ZERO, shortDiff);

            oldPos = AtomicSize.FromDecimal(15.5m);
            newPos = AtomicSize.FromDecimal(15.5m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.ZERO, longDiff);
            Assert.AreEqual(AtomicSize.ZERO, shortDiff);

            oldPos = AtomicSize.FromDecimal(-15.5m);
            newPos = AtomicSize.FromDecimal(-15.5m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.ZERO, longDiff);
            Assert.AreEqual(AtomicSize.ZERO, shortDiff);
        }

        [Test]
        public void UnsettledNopCheckingModule_CalculateNominalNopDiffTest_JustOneSideDiff()
        {
            AtomicSize oldPos, newPos, shortDiff, longDiff;

            //LONGS
            oldPos = AtomicSize.ZERO;
            newPos = AtomicSize.FromDecimal(15.5m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(newPos, longDiff);
            Assert.AreEqual(AtomicSize.ZERO, shortDiff);

            oldPos = AtomicSize.FromDecimal(15.5m);
            newPos = AtomicSize.ZERO; 
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(-oldPos, longDiff);
            Assert.AreEqual(AtomicSize.ZERO, shortDiff);

            oldPos = AtomicSize.FromDecimal(15.5m);
            newPos = AtomicSize.FromDecimal(20m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(4.5m), longDiff);
            Assert.AreEqual(AtomicSize.ZERO, shortDiff);


            oldPos = AtomicSize.FromDecimal(20m);
            newPos = AtomicSize.FromDecimal(15.5m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(-4.5m), longDiff);
            Assert.AreEqual(AtomicSize.ZERO, shortDiff);


            //SHORTS
            oldPos = AtomicSize.ZERO;
            newPos = AtomicSize.FromDecimal(-15.5m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.ZERO, longDiff);
            Assert.AreEqual(-newPos, shortDiff);

            oldPos = AtomicSize.FromDecimal(-15.5m);
            newPos = AtomicSize.ZERO;
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.ZERO, longDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(-15.5m), shortDiff);

            oldPos = AtomicSize.FromDecimal(-15.5m);
            newPos = AtomicSize.FromDecimal(-20m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.ZERO, longDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(4.5m), shortDiff);


            oldPos = AtomicSize.FromDecimal(-20m);
            newPos = AtomicSize.FromDecimal(-15.5m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.ZERO, longDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(-4.5m), shortDiff);
        }

        [Test]
        public void UnsettledNopCheckingModule_CalculateNominalNopDiffTest_CrossingZeroDiff()
        {
            AtomicSize oldPos, newPos, shortDiff, longDiff;

            oldPos = AtomicSize.FromDecimal(-15.5m);
            newPos = AtomicSize.FromDecimal(15.5m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(15.5m), longDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(-15.5m), shortDiff);

            oldPos = AtomicSize.FromDecimal(15.5m);
            newPos = AtomicSize.FromDecimal(-15.5m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(-15.5m), longDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(15.5m), shortDiff);

            oldPos = AtomicSize.FromDecimal(20m);
            newPos = AtomicSize.FromDecimal(-5m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(-20m), longDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(5m), shortDiff);

            oldPos = AtomicSize.FromDecimal(-20m);
            newPos = AtomicSize.FromDecimal(5m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(5m), longDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(-20m), shortDiff);

            oldPos = AtomicSize.FromDecimal(-5m);
            newPos = AtomicSize.FromDecimal(20m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(20m), longDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(-5m), shortDiff);

            oldPos = AtomicSize.FromDecimal(5m);
            newPos = AtomicSize.FromDecimal(-20m);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(oldPos, newPos, out longDiff, out shortDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(-5m), longDiff);
            Assert.AreEqual(AtomicSize.FromDecimal(20m), shortDiff);
        }

        private UnsettledNopCheckingModule.UnsettledNopStatsPerCreditLine PrepareNopStats()
        {
            UnsettledNopCheckingModule.UnsettledNopStatsPerCreditLine nopStats = new UnsettledNopCheckingModule.UnsettledNopStatsPerCreditLine(3);
            decimal[] usdConversionMultipliers = Enumerable.Repeat(-1m, Currency.ValuesCount).ToArray();
            usdConversionMultipliers[(int)Currency.EUR] = 1.2m;
            usdConversionMultipliers[(int)Currency.JPY] = 0.01m;
            usdConversionMultipliers[(int)Currency.USD] = 1m;

            IntegratorDealDone deal = new IntegratorDealDone(Counterparty.NULL, Symbol.EUR_JPY, DateTime.MinValue,
                DateTime.MinValue, DateTime.MinValue, 1000m, -120000m);
            int creditLineIdx = 1;

            AtomicSize nop = UnsettledNopCheckingModule.AddDataPointAndGetNop(nopStats, creditLineIdx, deal, usdConversionMultipliers);

            Assert.AreEqual(AtomicSize.FromDecimal(1200m), nop);
            Assert.AreEqual(AtomicSize.FromDecimal(1200m), nopStats.LongUsdNops[creditLineIdx]);
            Assert.AreEqual(AtomicSize.FromDecimal(1200m), nopStats.ShortUsdNops[creditLineIdx]);

            deal = new IntegratorDealDone(Counterparty.NULL, Symbol.EUR_USD, DateTime.MinValue,
                DateTime.MinValue, DateTime.MinValue, -900m, 1000m);

            nop = UnsettledNopCheckingModule.AddDataPointAndGetNop(nopStats, creditLineIdx, deal, usdConversionMultipliers);

            Assert.AreEqual(AtomicSize.FromDecimal(1200m), nop);
            Assert.AreEqual(AtomicSize.FromDecimal(1120m), nopStats.LongUsdNops[creditLineIdx]);
            Assert.AreEqual(AtomicSize.FromDecimal(1200m), nopStats.ShortUsdNops[creditLineIdx]);


            deal = new IntegratorDealDone(Counterparty.NULL, Symbol.EUR_USD, DateTime.MinValue,
                DateTime.MinValue, DateTime.MinValue, -900m, 1000m);
            int secondCreditLineIdx = 0;

            nop = UnsettledNopCheckingModule.AddDataPointAndGetNop(nopStats, secondCreditLineIdx, deal, usdConversionMultipliers);

            Assert.AreEqual(AtomicSize.FromDecimal(1080m), nop);
            Assert.AreEqual(AtomicSize.FromDecimal(1000m), nopStats.LongUsdNops[secondCreditLineIdx]);
            Assert.AreEqual(AtomicSize.FromDecimal(1080m), nopStats.ShortUsdNops[secondCreditLineIdx]);
            Assert.AreEqual(AtomicSize.FromDecimal(1120m), nopStats.LongUsdNops[creditLineIdx]);
            Assert.AreEqual(AtomicSize.FromDecimal(1200m), nopStats.ShortUsdNops[creditLineIdx]);

            return nopStats;
        }

        [Test]
        public void UnsettledNopCheckingModule_AddDataPointAndGetNop()
        {
            this.PrepareNopStats();

        }

        [Test]
        public void UnsettledNopCheckingModule_GetNopIfOrderAdded()
        {
            UnsettledNopCheckingModule.UnsettledNopStatsPerCreditLine nopStats = this.PrepareNopStats();
            int creditLineIdx = 1;
            decimal[] usdConversionMultipliers = Enumerable.Repeat(-1m, Currency.ValuesCount).ToArray();
            usdConversionMultipliers[(int) Currency.EUR] = 1.2m; 
            usdConversionMultipliers[(int) Currency.JPY] = 0.01m;
            usdConversionMultipliers[(int) Currency.USD] = 1m;

            var integratorOrderInfoMock = new Mock<IIntegratorOrderInfo>();
            integratorOrderInfoMock.Setup(o => o.Symbol).Returns(Symbol.EUR_USD);
            integratorOrderInfoMock.Setup(o => o.IntegratorDealDirection).Returns(DealDirection.Buy);
            integratorOrderInfoMock.Setup(o => o.SizeBaseAbsInitial).Returns(1900);
            integratorOrderInfoMock.Setup(o => o.RequestedPrice).Returns(1.05m); //-1995 USD


            AtomicSize nop = UnsettledNopCheckingModule.GetNopIfOrderAdded(nopStats, creditLineIdx,
                integratorOrderInfoMock.Object, usdConversionMultipliers, null);

            Assert.AreEqual(AtomicSize.FromDecimal(2400m), nop);
            Assert.AreEqual(AtomicSize.FromDecimal(1120m), nopStats.LongUsdNops[creditLineIdx]);
            Assert.AreEqual(AtomicSize.FromDecimal(1200m), nopStats.ShortUsdNops[creditLineIdx]);

        }

        private class UnsettledNopCheckingModuleUnderTest : UnsettledNopCheckingModule
        {
            public UnsettledNopCheckingModuleUnderTest(IPersistedNopProvider persistedNopProvider,
                IPriceStreamMonitor priceStreamMonitor, ISettlementDatesKeeper settlementDatesKeeper, ILogger logger)
                : base(persistedNopProvider, priceStreamMonitor, settlementDatesKeeper, logger)
            {
            }

            public void Initialize_Test(IEnumerable<IntegratorDealDone> unsettletNopInfoPoints,
            IEnumerable<NopSettingsPointPerCreditLine> unsettletNopSettingsPoints)
            {
                this.Initialize(unsettletNopInfoPoints, unsettletNopSettingsPoints);
            }
        }

        [Test]
        public void UnsettledNopCheckingModule_InitializationTest()
        {
            NopSettingsPointPerCreditLine settA = new NopSettingsPointPerCreditLine(5, "c5", 5, 5, 5, 5, new List<Counterparty>{Counterparty.BNP, Counterparty.BRX});
            NopSettingsPointPerCreditLine settB = new NopSettingsPointPerCreditLine(3, "c3", 3, 3, 3, 3, new List<Counterparty>{Counterparty.L01, Counterparty.H4M});
            IEnumerable<NopSettingsPointPerCreditLine> settings = new List<NopSettingsPointPerCreditLine>() {settA, settB};

            var persistedNopProviderMock = new Mock<IPersistedNopProvider>();
            persistedNopProviderMock.Setup(p => p.GetCreditLineNopSettings()).Returns(settings);
            persistedNopProviderMock.Setup(p => p.GetNopPolPerCreditLineSettlementAndCurrency()).Returns(new List<IntegratorDealDone>());

            var priceStreamMonitorMock = new Mock<IPriceStreamMonitor>();
            priceStreamMonitorMock.Setup(pm => pm.UsdConversionMultipliers).Returns(Enumerable.Repeat(1m,200).ToArray());

            var settelemntsDatesKeeperMock = new Mock<ISettlementDatesKeeper>();
            settelemntsDatesKeeperMock.Setup(k => k.GetSettlementDate(It.IsAny<Symbol>())).Returns(DateTime.Now);

            //Case 1: fatal if attempt to reconfigure also mappings
            bool fatalLogged = false;
            UnsettledNopCheckingModuleUnderTest uut2 =
                new UnsettledNopCheckingModuleUnderTest(persistedNopProviderMock.Object, priceStreamMonitorMock.Object,
                    settelemntsDatesKeeperMock.Object, new TestLogger(s => { fatalLogged = true; }) { FailTestOnUnexpectedLogs = true });


            NopSettingsPointPerCreditLine settD = new NopSettingsPointPerCreditLine(5, "s5", 50, 50, 50, 50, new List<Counterparty> { Counterparty.BNP, Counterparty.BRX, Counterparty.NOM });
            persistedNopProviderMock.Raise(p => p.CreditLineNopSettingsChanged += null, new List<NopSettingsPointPerCreditLine>() { settD, settB });

            Assert.IsTrue(fatalLogged);


            //Case 2: ok if reconfigure without altering mappings
            UnsettledNopCheckingModuleUnderTest uut =
                new UnsettledNopCheckingModuleUnderTest(persistedNopProviderMock.Object, priceStreamMonitorMock.Object,
                    settelemntsDatesKeeperMock.Object, new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true });

            persistedNopProviderMock.Raise(p => p.CreditLineNopSettingsChanged += null, settings);

            NopSettingsPointPerCreditLine settC = new NopSettingsPointPerCreditLine(5, "s5", 50, 50, 50, 50, new List<Counterparty> { Counterparty.BNP, Counterparty.BRX });
            persistedNopProviderMock.Raise(p => p.CreditLineNopSettingsChanged += null, new List<NopSettingsPointPerCreditLine>() {settC, settB});


            var integratorOrderInfoMock = new Mock<IIntegratorOrderInfo>();
            //unconfigured
            integratorOrderInfoMock.Setup(o => o.Counterparty).Returns(Counterparty.FC1);
            integratorOrderInfoMock.Setup(o => o.Symbol).Returns(Symbol.EUR_USD);
            integratorOrderInfoMock.Setup(o => o.IntegratorDealDirection).Returns(DealDirection.Buy);
            integratorOrderInfoMock.Setup(o => o.SizeBaseAbsInitial).Returns(1000000000);
            integratorOrderInfoMock.Setup(o => o.RequestedPrice).Returns(1.05m); //-1995 USD

            Assert.True(uut2.CheckIsOutgoingIncommingOrderInfoAllowed(integratorOrderInfoMock.Object));

            //configured
            integratorOrderInfoMock.Setup(o => o.Counterparty).Returns(Counterparty.BNP);

            Assert.IsFalse(uut2.CheckIsOutgoingIncommingOrderInfoAllowed(integratorOrderInfoMock.Object));
        }

        public void UnsettledNopCheckingModule_RolloverTest()
        {
            
        }

    }
}
