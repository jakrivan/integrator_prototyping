﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.LowLatencyUtils;
using NUnit.Framework;

namespace Kreslik.Integrator.RiskManagement.Tests
{
    [TestFixture]
    public class MedianTrackerTests
    {
        [Test]
        public void Test1()
        {
            MedianTracker<decimal> medianTracker = 
                new MedianTracker<decimal>(5, Comparer<decimal>.Default, (arg1, arg2) => (arg1 + arg2)/2m);

            medianTracker.InsertItem(10, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(10, medianTracker.Median);

            medianTracker.InsertItem(6, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(8, medianTracker.Median);

            medianTracker.InsertItem(9, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(9, medianTracker.Median);

            medianTracker.InsertItem(6, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(7.5, medianTracker.Median);

            medianTracker.InsertItem(100, DateTime.MinValue);
            Assert.IsTrue(medianTracker.IsFull);
            Assert.AreEqual(9, medianTracker.Median);

            medianTracker.InsertItem(100, DateTime.MinValue);
            Assert.IsTrue(medianTracker.IsFull);
            Assert.AreEqual(9, medianTracker.Median);

            medianTracker.InsertItem(100, DateTime.MinValue);
            Assert.IsTrue(medianTracker.IsFull);
            Assert.AreEqual(100, medianTracker.Median);
        }

        [Test]
        public void Test2()
        {
            MedianTrackerUnsynchronized<decimal> medianTracker =
                new MedianTrackerUnsynchronized<decimal>(8, Comparer<decimal>.Default);

            medianTracker.InsertItem(10, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(10, medianTracker.Median);

            medianTracker.InsertItem(6, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(10, medianTracker.Median);

            medianTracker.InsertItem(9, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(9, medianTracker.Median);

            medianTracker.InsertItem(6, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(9, medianTracker.Median);

            medianTracker.InsertItem(100, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(9, medianTracker.Median);

            medianTracker.InsertItem(100, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(10, medianTracker.Median);

            medianTracker.InsertItem(100, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(10, medianTracker.Median);

            medianTracker.InsertItem(1, DateTime.MinValue);
            Assert.IsTrue(medianTracker.IsFull);
            Assert.AreEqual(10, medianTracker.Median);

            medianTracker.InsertItem(1, DateTime.MinValue);
            Assert.IsTrue(medianTracker.IsFull);
            Assert.AreEqual(9, medianTracker.Median);

            medianTracker.InsertItem(11, DateTime.MinValue);
            Assert.IsTrue(medianTracker.IsFull);
            Assert.AreEqual(11, medianTracker.Median);

            medianTracker.InsertItem(101, DateTime.MinValue);
            Assert.IsTrue(medianTracker.IsFull);
            Assert.AreEqual(100, medianTracker.Median);
        }

        [Test]
        public void Test3()
        {
            PriceMedianTrackerUnsynchronized medianTracker = new PriceMedianTrackerUnsynchronized(8, AtomicDecimal.Compare);

            medianTracker.InsertItem(10, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(10, medianTracker.Median.ToDecimal());

            medianTracker.InsertItem(6, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(10, medianTracker.Median.ToDecimal());

            medianTracker.InsertItem(9, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(9, medianTracker.Median.ToDecimal());

            medianTracker.InsertItem(6, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(9, medianTracker.Median.ToDecimal());

            medianTracker.InsertItem(100, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(9, medianTracker.Median.ToDecimal());

            medianTracker.InsertItem(100, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(10, medianTracker.Median.ToDecimal());

            medianTracker.InsertItem(100, DateTime.MinValue);
            Assert.IsFalse(medianTracker.IsFull);
            Assert.AreEqual(10, medianTracker.Median.ToDecimal());

            medianTracker.InsertItem(1, DateTime.MinValue);
            Assert.IsTrue(medianTracker.IsFull);
            Assert.AreEqual(10, medianTracker.Median.ToDecimal());

            medianTracker.InsertItem(1, DateTime.MinValue);
            Assert.IsTrue(medianTracker.IsFull);
            Assert.AreEqual(9, medianTracker.Median.ToDecimal());

            medianTracker.InsertItem(11, DateTime.MinValue);
            Assert.IsTrue(medianTracker.IsFull);
            Assert.AreEqual(11, medianTracker.Median.ToDecimal());

            medianTracker.InsertItem(101, DateTime.MinValue);
            Assert.IsTrue(medianTracker.IsFull);
            Assert.AreEqual(100, medianTracker.Median.ToDecimal());
        }
    }
}
