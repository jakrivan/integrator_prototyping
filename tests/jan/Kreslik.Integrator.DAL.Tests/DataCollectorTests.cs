﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL.DataCollection;
using Moq;
using NUnit.Framework;

namespace Kreslik.Integrator.DAL.Tests
{
    [TestFixture]
    public class DataCollectorTests
    {
        private class TestFxDbTypesMapper: IFxDbTypesMapper
        {
            public int DataProviderId { get; set; }

            public int SourceFileId { get; set; }

            public byte GetFxDdSymbolId(Symbol symbol)
            {
                return (byte) (int) symbol;
            }

            public byte GetFxDdLiquidityProviderStreamId(Counterparty counterparty)
            {
                return (byte) (int) counterparty;
            }

            public long ConvertTimeToTickTime(DateTime time, bool convertInputUtcToEt)
            {
                return time.Ticks;
            }

            public bool GetFxSideCode(PriceSide priceSide)
            {
                return priceSide == PriceSide.Ask;
            }

            public bool? GetFxSideCode(PriceSide? priceSide)
            {
                return priceSide.HasValue ? GetFxSideCode(priceSide.Value) : (bool?)null;
            }

            public bool? GetFxTradeSideCode(TradeSide? tradeSide)
            {
                return tradeSide.HasValue ? tradeSide.Value == TradeSide.BuyPaid : (bool?)null;
            }  
        }

        private class TestIntegratorDbTypesMapper: IIntegratorDbTypesMapper
        {

            public byte GetCounterpartyCode(Counterparty counterparty)
            {
                throw new NotImplementedException();
            }

            public byte GetSymbolCode(Symbol symbol)
            {
                throw new NotImplementedException();
            }

            public byte GetOrderTypeCode(OrderType orderType)
            {
                throw new NotImplementedException();
            }

            public byte GetOrderTimeInForceCode(TimeInForce timeInForce)
            {
                throw new NotImplementedException();
            }

            public byte GetDealDirectionCode(DealDirection dealDirection)
            {
                throw new NotImplementedException();
            }

            public byte GetPegTypeCode(PegType pegType)
            {
                throw new NotImplementedException();
            }
        }

        public static void AssertAreAllPublicPropertiesSame<T>(T objectA, T objectB, params string[] ignore) where T: class
        {
            if (objectA != null && objectB != null)
            {
                Type type = typeof(T);
                List<string> ignoreList = new List<string>(ignore);
                foreach (
                    System.Reflection.PropertyInfo pi in
                        type.GetProperties(System.Reflection.BindingFlags.Public |
                                           System.Reflection.BindingFlags.Instance)
                            .Where(p => p.CanRead && !ignore.Contains(p.Name)))
                {
                    object objectAValue = type.GetProperty(pi.Name).GetValue(objectA, null);
                    object objectBValue = type.GetProperty(pi.Name).GetValue(objectB, null);

                    if (objectAValue != objectBValue && (objectAValue == null || !objectAValue.Equals(objectBValue)))
                    {
                        Assert.Fail("Property {0} expected to be same, but it's not ({1} != {2})", pi.Name, objectAValue, objectBValue);
                    }
                }
            }
            else
            {
                Assert.AreEqual(objectA, objectB, "One of object is null, another is not");
            }
        }

        [Test]
        public void DataCollector_constructionAndStoppingTest()
        {
            var settingsMock = new Mock<IDataCollectorSettings>();
            settingsMock.Setup(s => s.DataCollectionFolder).Returns("TempDCFolder");
            settingsMock.Setup(s => s.MaxBcpBatchSize).Returns(10);

            settingsMock.Setup(s => s.MaxIntervalBetweenUploads).Returns(TimeSpan.FromSeconds(5));

            IFxDbTypesMapper fxTypesMapper = new TestFxDbTypesMapper();
            IDbTypesMappers dbTypesMappers = new DbTypesMappers(fxTypesMapper, new TestIntegratorDbTypesMapper());
            ILogger testLogger = LogFactory.Instance.GetLogger("MDDCTest");

            MarketDataCollector mc = new MarketDataCollector(dbTypesMappers, testLogger, settingsMock.Object, 4, 6);

            

            mc.AddPrice(PriceObjectInternal.GetAllocatedTestingPriceObject(5.5m, 100000, PriceSide.Ask, Symbol.EUR_AUD, Counterparty.BNP, null, MarketDataRecordType.BankQuoteData, DateTime.MinValue, DateTime.MinValue));
            //WritablePriceObject.CreateNewHotspotOrderPriceObject(289.98m, 6893, 123.5m, 10m, PriceSide.Ask, Symbol.DKK_SEK, Counterparty.HTF, MarketDataRecordType.VenueNewOrder, null, "ctpIdentity", DateTime.Today, DateTime.MinValue));
            mc.AddHotspotPrice(
                PriceObjectPool.OverridePrefetchedHotspotPriceObject(
                    PriceObjectInternal.GetAllocatedTestingPriceObject(5.5m, 100000, PriceSide.Ask, Symbol.EUR_AUD,
                        Counterparty.BNP, null, MarketDataRecordType.BankQuoteData, DateTime.MinValue, DateTime.MinValue),
                    289.98m, 6893, 123.5m, 10m, PriceSide.Ask, Symbol.DKK_SEK, Counterparty.HTF,
                    MarketDataRecordType.VenueNewOrder, null, "ctpIdentity", DateTime.Today, DateTime.MinValue));

            mc.StopDataCollection();
            mc.DataCollectionDoneEvent.WaitOne();

            CountdownEvent testDoneEvent = new CountdownEvent(2);

            int calls = 0;
            FileBytesReceiver<MarketDataSerializationEntry> fbr = new FileBytesReceiver<MarketDataSerializationEntry>(testLogger, @"TempDCFolder\MD");
            fbr.NewMessageAvailable += entry =>
                {
                    if (calls++ == 0)
                    {
                        Assert.AreEqual((byte) MarketDataRecordType.BankQuoteData ,entry.RecordTypeId);
                        Assert.AreEqual(5.5m, entry.Price);
                        Assert.AreEqual(100000, entry.Size);
                        Assert.AreEqual(fxTypesMapper.GetFxSideCode(PriceSide.Ask), entry.SideId);
                        Assert.AreEqual(fxTypesMapper.GetFxDdSymbolId(Symbol.EUR_AUD), entry.FXPairId);
                        Assert.AreEqual(fxTypesMapper.GetFxDdLiquidityProviderStreamId(Counterparty.BNP), entry.CounterpartyId);
                        Assert.AreEqual(string.Empty, entry.CounterpartyPriceIdentity);
                        Assert.AreEqual(null, entry.TradeSideId);
                    }
                    else
                    {
                        Assert.AreEqual((byte)MarketDataRecordType.VenueNewOrder, entry.RecordTypeId);
                        Assert.AreEqual(289.98m, entry.Price);
                        Assert.AreEqual(6893, entry.Size);
                        Assert.AreEqual(123.5m, entry.MinimumSize);
                        Assert.AreEqual(10m, entry.Granularity);
                        Assert.AreEqual(fxTypesMapper.GetFxSideCode(PriceSide.Ask), entry.SideId);
                        Assert.AreEqual(fxTypesMapper.GetFxDdSymbolId(Symbol.DKK_SEK), entry.FXPairId);
                        Assert.AreEqual(fxTypesMapper.GetFxDdLiquidityProviderStreamId(Counterparty.HTF), entry.CounterpartyId);
                        Assert.AreEqual("ctpIdentity", entry.CounterpartyPriceIdentity);
                        Assert.AreEqual(DateTime.Today, entry.IntegratorReceivedTimeUtc);
                        Assert.AreEqual(DateTime.MinValue, entry.CounterpartySentTimeUtc);
                        Assert.AreEqual(null, entry.TradeSideId);
                    }
                    testDoneEvent.Signal();
                };
            fbr.GapInContinuousMessageStream += () =>
                {
                    Assert.IsTrue(testDoneEvent.IsSet, "Detected gap before tests finished");
                };
            fbr.StartReceivingMessages();

            Assert.IsTrue(testDoneEvent.Wait(TimeSpan.FromSeconds(1)), "Test finished by timeout");

            fbr.StopReceivingMessages();  
        }


        [Test]
        public void DataCollector_OutgoingPricesTest()
        {
            var settingsMock = new Mock<IDataCollectorSettings>();
            settingsMock.Setup(s => s.DataCollectionFolder).Returns("TempOutPricesFolder");
            settingsMock.Setup(s => s.MaxBcpBatchSize).Returns(10);

            settingsMock.Setup(s => s.MaxIntervalBetweenUploads).Returns(TimeSpan.FromSeconds(5));

            IFxDbTypesMapper fxTypesMapper = new TestFxDbTypesMapper();
            IDbTypesMappers dbTypesMappers = new DbTypesMappers(fxTypesMapper, new TestIntegratorDbTypesMapper());
            ILogger testLogger = LogFactory.Instance.GetLogger("OutPricesDCTest");

            OutgoingPriceDataCollector opdc = new OutgoingPriceDataCollector(dbTypesMappers, testLogger, settingsMock.Object, 4, 6);

            var price = new StreamingPrice();
            price.OverrideContent(Symbol.AUD_JPY, Counterparty.FS1, PriceSide.Bid, 1234.567m, 0,
                1234.56789m, new StreamingPricesProducerIndex(5, 9));
            price.IntegratorSentTimeUtc = DateTime.MaxValue;
            price.IntegratorPriceIdentity = "ABC123";
            opdc.AddOutgoingPrice(price);
            var cancel = new StreamingPriceCancel(Symbol.AUD_DKK, Counterparty.CZB, null,
                new StreamingPricesProducerIndex(1, 4));
            cancel.SetSentPriceCancelInfo(DateTime.MaxValue);
            opdc.AddOutgoingPriceCancel(cancel);


            opdc.StopDataCollection();
            opdc.DataCollectionDoneEvent.WaitOne();

            CountdownEvent testDoneEvent = new CountdownEvent(3);

            int calls = 0;
            FileBytesReceiver<OutgoingPriceSerializationEntry> fbr = new FileBytesReceiver<OutgoingPriceSerializationEntry>(testLogger, @"TempOutPricesFolder\OutgoingPrice");
            fbr.NewMessageAvailable += entry =>
            {
                calls++;
                if (calls == 1)
                {
                    Assert.AreEqual(fxTypesMapper.GetFxDdSymbolId(Symbol.AUD_JPY), entry.FXPairId);
                    Assert.AreEqual(fxTypesMapper.GetFxDdLiquidityProviderStreamId(Counterparty.FS1), entry.CounterpartyId);
                    Assert.AreEqual(fxTypesMapper.GetFxSideCode(PriceSide.Bid), entry.SideId);
                    Assert.AreEqual(1234.56789m, entry.Price);
                    Assert.AreEqual(1234.567m, entry.Size);
                    Assert.AreEqual(9, entry.TradingSystemId);
                    Assert.AreEqual(DateTime.MaxValue, entry.IntegratorSentTimeUtc);
                    Assert.AreEqual("ABC123", entry.IntegratorPriceIdentity);
                }
                else if(calls == 2)
                {
                    Assert.AreEqual(fxTypesMapper.GetFxDdSymbolId(Symbol.AUD_DKK), entry.FXPairId);
                    Assert.AreEqual(fxTypesMapper.GetFxDdLiquidityProviderStreamId(Counterparty.CZB), entry.CounterpartyId);
                    Assert.AreEqual(fxTypesMapper.GetFxSideCode(PriceSide.Ask), entry.SideId);
                    Assert.AreEqual(null, entry.Price);
                    Assert.AreEqual(null, entry.Size);
                    Assert.AreEqual(4, entry.TradingSystemId);
                    Assert.AreEqual(DateTime.MaxValue, entry.IntegratorSentTimeUtc);
                    Assert.AreEqual(null, entry.IntegratorPriceIdentity);
                }
                else
                {
                    Assert.AreEqual(fxTypesMapper.GetFxDdSymbolId(Symbol.AUD_DKK), entry.FXPairId);
                    Assert.AreEqual(fxTypesMapper.GetFxDdLiquidityProviderStreamId(Counterparty.CZB), entry.CounterpartyId);
                    Assert.AreEqual(fxTypesMapper.GetFxSideCode(PriceSide.Bid), entry.SideId);
                    Assert.AreEqual(null, entry.Price);
                    Assert.AreEqual(null, entry.Size);
                    Assert.AreEqual(4, entry.TradingSystemId);
                    Assert.AreEqual(DateTime.MaxValue, entry.IntegratorSentTimeUtc);
                    Assert.AreEqual(null, entry.IntegratorPriceIdentity);
                }
                testDoneEvent.Signal();
            };
            fbr.GapInContinuousMessageStream += () =>
            {
                Assert.IsTrue(testDoneEvent.IsSet, "Detected gap before tests finished");
            };
            fbr.StartReceivingMessages();

            Assert.IsTrue(testDoneEvent.Wait(TimeSpan.FromSeconds(1)), "Test finished by timeout");

            fbr.StopReceivingMessages();
        }
    }
}

//        [Test]
//        public void DataCollector_constructionAndStoppingTest()
//        {
//            var dataCollectorHelperMock = new Mock<IDataCollectorHelper<int>>();
//            var dataCollectorSettingsMock = new Mock<IDataCollectorSettings>();
//            var loggerMock = new Mock<ILogger>();

//            dataCollectorSettingsMock.Setup(setting => setting.MaxBcpBatchSize).Returns(1);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxInMemoryColectionSize).Returns(2);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxUploadTimeAfterWhichSerializationStarts)
//                                     .Returns(TimeSpan.FromSeconds(1));

//            DataCollector<int> dc = new DataCollector<int>(dataCollectorHelperMock.Object, dataCollectorSettingsMock.Object, loggerMock.Object);
//            dc.StopDataCollection();
//            Assert.IsTrue(dc.DataCollectorCompletelyShuttedDownWaitHandle.WaitOne(5), "Datacollector was not completely shutted down");
//        }

//        [Test]
//        public void DataCollector_ShutdownTestBeforeAnyUploads_WillUploadEverythingToDbAndNoFlushingToDisk()
//        {
//            var dataCollectorHelperMock = new Mock<IDataCollectorHelper<int>>();
//            var dataCollectorSettingsMock = new Mock<IDataCollectorSettings>();
//            var loggerMock = new Mock<ILogger>();

//            dataCollectorSettingsMock.Setup(setting => setting.MaxBcpBatchSize).Returns(4);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxInMemoryColectionSize).Returns(5);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxIntervalBetweenUploads)
//                                     .Returns(TimeSpan.FromSeconds(1));
//            dataCollectorSettingsMock.Setup(settings => settings.MaxProcessingTimeAfterShutdownSignalled)
//                                     .Returns(TimeSpan.FromSeconds(1));
//            dataCollectorSettingsMock.Setup(settings => settings.WaitIntervalBeforeShutdownStartsSerialization)
//                                     .Returns(TimeSpan.FromMilliseconds(5));
//            dataCollectorSettingsMock.Setup(settings => settings.MaxUploadTimeAfterWhichSerializationStarts)
//                                     .Returns(TimeSpan.FromSeconds(1));

//            var itemUploadInfoMock = new Mock<IItemUploadInfo>();

//            dataCollectorHelperMock.Setup(helper => helper.CreateItemUploadInfo(It.IsAny<int>()))
//                               .Returns(itemUploadInfoMock.Object);

//            var itemsTableMock = new Mock<IItemsDataTable>();
//            itemsTableMock.Setup(tbl => tbl.FileFullPath).Returns("Tbl");

//            dataCollectorHelperMock.Setup(helper => helper.CreateItemsDataTable())
//                               .Returns(() => itemsTableMock.Object);

//            dataCollectorHelperMock.Setup(helper => helper.SerializeTabel(It.IsAny<IItemsDataTable>())).Returns(true);

//            dataCollectorHelperMock.Setup(helper => helper.DeserializeTable(It.IsAny<string>()))
//                .Returns(() => itemsTableMock.Object);

//            DataCollector<int> dc = new DataCollector<int>(dataCollectorHelperMock.Object, dataCollectorSettingsMock.Object, loggerMock.Object);

//            dc.AddItem(5);
//            dc.AddItem(6);
//            dc.AddItem(7);

//            dc.StopDataCollection();
//            Assert.IsTrue(dc.DataCollectorCompletelyShuttedDownWaitHandle.WaitOne(20), "Datacollector was not completely shutted down");

//            dataCollectorHelperMock.Verify(helper => helper.CreateItemsDataTable(), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.SerializeTabel(It.IsAny<IItemsDataTable>()), Times.Never());
//            dataCollectorHelperMock.Verify(helper => helper.DeserializeTable(It.IsAny<string>()), Times.Never());
//            dataCollectorHelperMock.Verify(helper => helper.DeleteFile(It.IsAny<string>()), Times.Never());
//            dataCollectorHelperMock.Verify(helper => helper.BcpTableToSqlServer(itemsTableMock.Object), Times.Once());
//        }

//        [Test]
//        public void DataCollector_ShutdownTestBeforeAnyUploadsAndLastUploadTakesLong_WillFlushEverythingTodiskDuringDbUpload()
//        {
//            var dataCollectorHelperMock = new Mock<IDataCollectorHelper<int>>();
//            var dataCollectorSettingsMock = new Mock<IDataCollectorSettings>();
//            var loggerMock = new Mock<ILogger>();

//            dataCollectorSettingsMock.Setup(setting => setting.MaxBcpBatchSize).Returns(4);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxInMemoryColectionSize).Returns(5);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxIntervalBetweenUploads)
//                                     .Returns(TimeSpan.FromSeconds(1));
//            dataCollectorSettingsMock.Setup(settings => settings.MaxProcessingTimeAfterShutdownSignalled)
//                                     .Returns(TimeSpan.FromSeconds(1));
//            dataCollectorSettingsMock.Setup(settings => settings.WaitIntervalBeforeShutdownStartsSerialization)
//                                     .Returns(TimeSpan.FromMilliseconds(5));
//            dataCollectorSettingsMock.Setup(settings => settings.MaxUploadTimeAfterWhichSerializationStarts)
//                                     .Returns(TimeSpan.FromSeconds(1));

//            var itemUploadInfoMock = new Mock<IItemUploadInfo>();

//            dataCollectorHelperMock.Setup(helper => helper.CreateItemUploadInfo(It.IsAny<int>()))
//                               .Returns(itemUploadInfoMock.Object);

//            var itemsTableMock = new Mock<IItemsDataTable>();
//            itemsTableMock.Setup(tbl => tbl.FileFullPath).Returns("Tbl");

//            dataCollectorHelperMock.Setup(helper => helper.CreateItemsDataTable())
//                               .Returns(() => itemsTableMock.Object);

//            dataCollectorHelperMock.Setup(helper => helper.SerializeTabel(It.IsAny<IItemsDataTable>())).Returns(true);

//            dataCollectorHelperMock.Setup(helper => helper.DeserializeTable(It.IsAny<string>()))
//                .Returns(() => itemsTableMock.Object);

//            //Upload to server will now take some time (so that during shutdown there is an upload in process)
//            dataCollectorHelperMock.Setup(helper => helper.BcpTableToSqlServer(It.IsAny<IItemsDataTable>()))
//                               .Callback(() => Thread.Sleep(10));

//            DataCollector<int> dc = new DataCollector<int>(dataCollectorHelperMock.Object, dataCollectorSettingsMock.Object, loggerMock.Object);

//            dc.AddItem(5);
//            dc.AddItem(6);
//            dc.AddItem(7);

//            dc.StopDataCollection();
//            Assert.IsTrue(dc.DataCollectorCompletelyShuttedDownWaitHandle.WaitOne(20), "Datacollector was not completely shutted down");

//            dataCollectorHelperMock.Verify(helper => helper.CreateItemsDataTable(), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.SerializeTabel(itemsTableMock.Object), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.DeserializeTable(It.IsAny<string>()), Times.Never());
//            dataCollectorHelperMock.Verify(helper => helper.DeleteFile("Tbl"), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.BcpTableToSqlServer(itemsTableMock.Object), Times.Once());
//        }

//        [Test]
//        public void DataCollector_AddingItems_WillFlushRegularlyToDb()
//        {
//            var dataCollectorHelperMock = new Mock<IDataCollectorHelper<int>>();
//            var dataCollectorSettingsMock = new Mock<IDataCollectorSettings>();
//            var loggerMock = new Mock<ILogger>();

//            dataCollectorSettingsMock.Setup(setting => setting.MaxBcpBatchSize).Returns(2);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxInMemoryColectionSize).Returns(5);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxIntervalBetweenUploads)
//                                     .Returns(TimeSpan.FromSeconds(1));
//            dataCollectorSettingsMock.Setup(settings => settings.MaxProcessingTimeAfterShutdownSignalled)
//                                     .Returns(TimeSpan.FromSeconds(1));
//            dataCollectorSettingsMock.Setup(settings => settings.MaxUploadTimeAfterWhichSerializationStarts)
//                                     .Returns(TimeSpan.FromSeconds(1));

//            var itemUploadInfoMock = new Mock<IItemUploadInfo>();

//            dataCollectorHelperMock.Setup(helper => helper.CreateItemUploadInfo(It.IsAny<int>()))
//                               .Returns(itemUploadInfoMock.Object);

//            var itemsTableMocks = new Mock<IItemsDataTable>[3];
//            for (int i = 0; i < 3; i++)
//            {
//                var itemsTableMock = new Mock<IItemsDataTable>();
//                itemsTableMock.Setup(tbl => tbl.FileFullPath).Returns(i.ToString());
//                itemsTableMocks[i] = itemsTableMock;
//            }

//            int createdTablesCnt = -1;
//            dataCollectorHelperMock.Setup(helper => helper.CreateItemsDataTable())
//                               .Returns(() => itemsTableMocks[Interlocked.Increment(ref createdTablesCnt)].Object);


//            DataCollector<int> dc = new DataCollector<int>(dataCollectorHelperMock.Object, dataCollectorSettingsMock.Object, loggerMock.Object);

//            dc.AddItem(5);
//            dc.AddItem(6);
//            dc.AddItem(7);
//            //give a chance to upload thread to run, otherwise serialization would run
//            Thread.Sleep(10);
//            dc.AddItem(8);
//            dc.AddItem(9);
//            dc.AddItem(12);
//            Thread.Sleep(10);

//            dc.StopDataCollection();
//            Assert.IsTrue(dc.DataCollectorCompletelyShuttedDownWaitHandle.WaitOne(/*10*/ 10000), "Datacollector was not completely shutted down");

//            dataCollectorHelperMock.Verify(helper => helper.SerializeTabel(It.IsAny<IItemsDataTable>()), Times.Never());
//            dataCollectorHelperMock.Verify(helper => helper.DeserializeTable(It.IsAny<string>()), Times.Never());
//            dataCollectorHelperMock.Verify(helper => helper.DeleteFile(It.IsAny<string>()), Times.Never());
//        }

//        [Test]
//        public void DataCollector_AddingItemsAndThenShutdown_WillFlushRegularlyToDb_LastTableTemporarilySerialized()
//        {
//            var dataCollectorHelperMock = new Mock<IDataCollectorHelper<int>>();
//            var dataCollectorSettingsMock = new Mock<IDataCollectorSettings>();
//            var loggerMock = new Mock<ILogger>();

//            dataCollectorSettingsMock.Setup(setting => setting.MaxBcpBatchSize).Returns(2);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxInMemoryColectionSize).Returns(5);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxIntervalBetweenUploads)
//                                     .Returns(TimeSpan.FromSeconds(1));
//            dataCollectorSettingsMock.Setup(settings => settings.MaxProcessingTimeAfterShutdownSignalled)
//                                     .Returns(TimeSpan.FromSeconds(1));
//            dataCollectorSettingsMock.Setup(settings => settings.MaxUploadTimeAfterWhichSerializationStarts)
//                                     .Returns(TimeSpan.FromSeconds(1));

//            var itemUploadInfoMock = new Mock<IItemUploadInfo>();

//            dataCollectorHelperMock.Setup(helper => helper.CreateItemUploadInfo(It.IsAny<int>()))
//                               .Returns(itemUploadInfoMock.Object);

//            var itemsTableMocks = new Mock<IItemsDataTable>[3];
//            for (int i = 0; i < 3; i++)
//            {
//                var itemsTableMock = new Mock<IItemsDataTable>();
//                itemsTableMock.Setup(tbl => tbl.FileFullPath).Returns(i.ToString());
//                itemsTableMocks[i] = itemsTableMock;
//            }

//            int createdTablesCnt = -1;
//            dataCollectorHelperMock.Setup(helper => helper.CreateItemsDataTable())
//                               .Returns(() => itemsTableMocks[Interlocked.Increment(ref createdTablesCnt)].Object);

//            //Upload to server will now take some time (so that during shutdown there is an upload in process)
//            dataCollectorHelperMock.Setup(helper => helper.BcpTableToSqlServer(It.IsAny<IItemsDataTable>()))
//                               .Callback(() => Thread.Sleep(5));

//            dataCollectorHelperMock.Setup(helper => helper.DeserializeTable(It.IsAny<string>()))
//                .Returns(() => itemsTableMocks[0].Object);

//            DataCollector<int> dc = new DataCollector<int>(dataCollectorHelperMock.Object, dataCollectorSettingsMock.Object, loggerMock.Object);

//            dc.AddItem(5);
//            dc.AddItem(6);
//            dc.AddItem(7);
//            //give a chance to upload thread to run, otherwise serialization would run
//            Thread.Sleep(10);
//            dc.AddItem(8);
//            dc.AddItem(9);
//            dc.AddItem(12);
//            //give chance to upload thread to fully upload 2nd batch, but be in-progress of uploading 3rd when shutdown came
//            Thread.Sleep(8);

//            dc.StopDataCollection();
//            Assert.IsTrue(dc.DataCollectorCompletelyShuttedDownWaitHandle.WaitOne(10), "Datacollector was not completely shutted down");

//            dataCollectorHelperMock.Verify(helper => helper.SerializeTabel(itemsTableMocks[2].Object), Times.Once());
//            //It won't deserialize anything because it already uploaded the table, it was serialized 'just in case'
//            dataCollectorHelperMock.Verify(helper => helper.DeserializeTable(It.IsAny<string>()), Times.Never());
//            dataCollectorHelperMock.Verify(helper => helper.DeleteFile(itemsTableMocks[2].Object.FileFullPath), Times.Once());
//        }

//        [Test]
//        public void DataCollector_AddingItemsAndThenShutdown_WillFlushRegularlyToDb_LastItemUploadInvokedByShutdown()
//        {
//            //
//            // This is hard scenario to unit test as it is undeterministics
//            // Once ther is less then batch items waiting during the shutdown came, one of those scenarios will happen:
//            //   1) BCP thread will be woken and it will grab the last batch and upload it completely
//            //   2) BCP thread will be woken and it will grab the last batch, during it's procession a Shutdown routine will
//            //         wake up and serialize the currently uploding batch

//            var dataCollectorHelperMock = new Mock<IDataCollectorHelper<int>>();
//            var dataCollectorSettingsMock = new Mock<IDataCollectorSettings>();
//            var loggerMock = new Mock<ILogger>();

//            dataCollectorSettingsMock.Setup(setting => setting.MaxBcpBatchSize).Returns(2);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxInMemoryColectionSize).Returns(5);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxIntervalBetweenUploads)
//                                     .Returns(TimeSpan.FromSeconds(1));
//            dataCollectorSettingsMock.Setup(settings => settings.MaxProcessingTimeAfterShutdownSignalled)
//                                     .Returns(TimeSpan.FromSeconds(1));
//            dataCollectorSettingsMock.Setup(settings => settings.WaitIntervalBeforeShutdownStartsSerialization)
//                                     .Returns(TimeSpan.FromMilliseconds(5));
//            dataCollectorSettingsMock.Setup(settings => settings.MaxUploadTimeAfterWhichSerializationStarts)
//                                     .Returns(TimeSpan.FromSeconds(1));

//            var itemUploadInfoMock = new Mock<IItemUploadInfo>();

//            dataCollectorHelperMock.Setup(helper => helper.CreateItemUploadInfo(It.IsAny<int>()))
//                               .Returns(itemUploadInfoMock.Object);

//            var itemsTableMocks = new Mock<IItemsDataTable>[3];
//            for (int i = 0; i < 3; i++)
//            {
//                var itemsTableMock = new Mock<IItemsDataTable>();
//                itemsTableMock.Setup(tbl => tbl.FileFullPath).Returns(i.ToString());
//                itemsTableMocks[i] = itemsTableMock;
//            }

//            int createdTablesCnt = -1;
//            dataCollectorHelperMock.Setup(helper => helper.CreateItemsDataTable())
//                               .Returns(() => itemsTableMocks[Interlocked.Increment(ref createdTablesCnt)].Object);

//            dataCollectorHelperMock.Setup(helper => helper.DeserializeTable(It.IsAny<string>()))
//                .Returns(() => itemsTableMocks[0].Object);

//            DataCollector<int> dc = new DataCollector<int>(dataCollectorHelperMock.Object, dataCollectorSettingsMock.Object, loggerMock.Object);

//            dc.AddItem(5);
//            dc.AddItem(6);
//            dc.AddItem(7);
//            //give a chance to upload thread to run, otherwise serialization would run
//            Thread.Sleep(10);
//            dc.AddItem(8);
//            dc.AddItem(9);
//            //give chance to upload thread to fully upload 2nd batch, but be in-progress of uploading 3rd when shutdown came
//            Thread.Sleep(10);

//            dc.StopDataCollection();
//            Assert.IsTrue(dc.DataCollectorCompletelyShuttedDownWaitHandle.WaitOne(20), "Datacollector was not completely shutted down");

//            dataCollectorHelperMock.Verify(helper => helper.SerializeTabel(It.IsAny<IItemsDataTable>()), Times.Never());
//            dataCollectorHelperMock.Verify(helper => helper.DeserializeTable(It.IsAny<string>()), Times.Never());
//            dataCollectorHelperMock.Verify(helper => helper.DeleteFile(It.IsAny<string>()), Times.Never());
//            dataCollectorHelperMock.Verify(helper => helper.BcpTableToSqlServer(It.IsAny<IItemsDataTable>()), Times.Exactly(3));
//        }

//        [Test]
//        public void DataCollector_BCPThreadDoesnotCatchUp_SerializationWillKickIn()
//        {
//            var dataCollectorHelperMock = new Mock<IDataCollectorHelper<int>>();
//            var dataCollectorSettingsMock = new Mock<IDataCollectorSettings>();
//            var loggerMock = new Mock<ILogger>();

//            dataCollectorSettingsMock.Setup(setting => setting.MaxBcpBatchSize).Returns(2);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxInMemoryColectionSize).Returns(4);
//            dataCollectorSettingsMock.Setup(settings => settings.MaxIntervalBetweenUploads)
//                                     .Returns(TimeSpan.FromSeconds(1));
//            dataCollectorSettingsMock.Setup(settings => settings.MaxProcessingTimeAfterShutdownSignalled)
//                                     .Returns(TimeSpan.FromSeconds(1));
//            dataCollectorSettingsMock.Setup(settings => settings.WaitIntervalBeforeShutdownStartsSerialization)
//                                     .Returns(TimeSpan.FromMilliseconds(5));
//            dataCollectorSettingsMock.Setup(settings => settings.MaxUploadTimeAfterWhichSerializationStarts)
//                                     .Returns(TimeSpan.FromSeconds(1));

//            var itemUploadInfoMock = new Mock<IItemUploadInfo>();

//            dataCollectorHelperMock.Setup(helper => helper.CreateItemUploadInfo(It.IsAny<int>()))
//                               .Returns(itemUploadInfoMock.Object);

//            var itemsTableMocks = new Mock<IItemsDataTable>[5];
//            for (int i = 0; i < 5; i++)
//            {
//                var itemsTableMock = new Mock<IItemsDataTable>();
//                itemsTableMock.Setup(tbl => tbl.FileFullPath).Returns(i.ToString());
//                itemsTableMock.Setup(tbl => tbl.RowsCount).Returns(2);
//                itemsTableMocks[i] = itemsTableMock;
//            }

//            int createdTablesCnt = -1;
//            dataCollectorHelperMock.Setup(helper => helper.CreateItemsDataTable())
//                               .Returns(() => itemsTableMocks[Interlocked.Increment(ref createdTablesCnt)].Object);

//            dataCollectorHelperMock.Setup(helper => helper.SerializeTabel(It.IsAny<IItemsDataTable>())).Returns(true);

//            int deserializedTablesCnt = 0;
//            dataCollectorHelperMock.Setup(helper => helper.DeserializeTable(It.IsAny<string>()))
//                .Returns(() => itemsTableMocks[Interlocked.Increment(ref deserializedTablesCnt)].Object);

//            //Upload to server will now take some time (so that during shutdown there is an upload in process)
//            dataCollectorHelperMock.Setup(helper => helper.BcpTableToSqlServer(It.IsAny<IItemsDataTable>()))
//                               .Callback(() => Thread.Sleep(100));

//            DataCollector<int> dc = new DataCollector<int>(dataCollectorHelperMock.Object, dataCollectorSettingsMock.Object, loggerMock.Object);

//            dc.AddItem(5);
//            dc.AddItem(6);
//            //give bcp thread chance to claim first batch
//            Thread.Sleep(2);

//            //verifi that first batch was claimed
//            dataCollectorHelperMock.Verify(helper => helper.BcpTableToSqlServer(itemsTableMocks[0].Object), Times.Once());

//            dc.AddItem(7);
//            dc.AddItem(8);
//            dc.AddItem(9);
//            dc.AddItem(15);
//            //wait so that serialization doesn't 'eat' following items
//            Thread.Sleep(5);

//            //verify that currently bcp-ing table is serialized
//            dataCollectorHelperMock.Verify(helper => helper.SerializeTabel(itemsTableMocks[0].Object), Times.Once());
//            //verify that second two batches were claimed and nothing was uploaded yet
//            dataCollectorHelperMock.Verify(helper => helper.SerializeTabel(itemsTableMocks[1].Object), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.SerializeTabel(itemsTableMocks[2].Object), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.DeserializeTable(It.IsAny<string>()), Times.Never());
//            dataCollectorHelperMock.Verify(helper => helper.DeleteFile(It.IsAny<string>()), Times.Never());
//            dataCollectorHelperMock.Verify(helper => helper.BcpTableToSqlServer(It.IsAny<IItemsDataTable>()), Times.Once());

//            dc.AddItem(16);
//            dc.AddItem(17);
//            dc.AddItem(18);
//            //give bcp thread a chance to proces rest of the items
//            Thread.Sleep(500);

//            //Verify that nothing else was serialized and that bcp has catched up and uploaded everything remaining
//            dataCollectorHelperMock.Verify(helper => helper.SerializeTabel(It.IsAny<IItemsDataTable>()), Times.Exactly(3));
//            dataCollectorHelperMock.Verify(helper => helper.DeserializeTable(itemsTableMocks[1].Object.FileFullPath), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.DeserializeTable(itemsTableMocks[2].Object.FileFullPath), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.DeserializeTable(It.IsAny<string>()), Times.Exactly(2));

//            dataCollectorHelperMock.Verify(helper => helper.DeleteFile(itemsTableMocks[0].Object.FileFullPath), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.BcpTableToSqlServer(itemsTableMocks[1].Object), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.DeleteFile(itemsTableMocks[1].Object.FileFullPath), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.BcpTableToSqlServer(itemsTableMocks[2].Object), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.DeleteFile(itemsTableMocks[2].Object.FileFullPath), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.BcpTableToSqlServer(itemsTableMocks[3].Object), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.BcpTableToSqlServer(It.IsAny<IItemsDataTable>()), Times.Exactly(4));
//            dataCollectorHelperMock.Verify(helper => helper.DeleteFile(It.IsAny<string>()), Times.Exactly(3));


//            dc.AddItem(19);
//            Thread.Sleep(150);

//            //Verify that bcp is now up to speed and it uploaded last table
//            dataCollectorHelperMock.Verify(helper => helper.BcpTableToSqlServer(itemsTableMocks[4].Object), Times.Once());
//            dataCollectorHelperMock.Verify(helper => helper.SerializeTabel(It.IsAny<IItemsDataTable>()), Times.Exactly(3));
//            dataCollectorHelperMock.Verify(helper => helper.DeserializeTable(It.IsAny<string>()), Times.Exactly(2));
//            dataCollectorHelperMock.Verify(helper => helper.DeleteFile(It.IsAny<string>()), Times.Exactly(3));
            


//            dc.StopDataCollection();
//            Assert.IsTrue(dc.DataCollectorCompletelyShuttedDownWaitHandle.WaitOne(5), "Datacollector was not completely shutted down");

//            //verify that nothing happened during and after shutdown
//            dataCollectorHelperMock.Verify(helper => helper.SerializeTabel(It.IsAny<IItemsDataTable>()), Times.Exactly(3));
//            dataCollectorHelperMock.Verify(helper => helper.DeserializeTable(It.IsAny<string>()), Times.Exactly(2));
//            dataCollectorHelperMock.Verify(helper => helper.BcpTableToSqlServer(It.IsAny<IItemsDataTable>()), Times.Exactly(5));
//        }
//    }
//}
