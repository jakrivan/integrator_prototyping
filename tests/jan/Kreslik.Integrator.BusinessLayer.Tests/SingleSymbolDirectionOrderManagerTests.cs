﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.BusinessLayer.Fakes;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.FIXMessaging;
using Moq.Language;
using NUnit.Framework;
using Moq;
using NUnit.Framework.Constraints;

namespace Kreslik.Integrator.BusinessLayer.Tests
{
    internal interface IOrderRequestInfoBuilder
    {
        BankPoolClientOrderRequestInfo CreateLimit(decimal totalAmountBaseAbs, decimal requestedPrice,
                                                   DealDirection side, Symbol symbol);

        BankPoolClientOrderRequestInfo CreateMarket(decimal totalAmountBaseAbs, DealDirection side, Symbol symbol);

        BankPoolClientOrderRequestInfo CreateMarketOnImprovement(decimal totalAmountBaseAbs, TimeSpan waitTiemout, DealDirection side, Symbol symbol);

        BankPoolClientOrderRequestInfo CreateQuoted(decimal totalAmountBaseAbs, decimal requestedPrice,
                                                    DealDirection side,
                                                    PriceObject price);
        BankPoolClientOrder CreateClientOrder(BankPoolClientOrderRequestInfo orderRequestInfo);
    }

    internal class OrderRequestInfoBuilder : ClientGatewayBase, IOrderRequestInfoBuilder
    {
        private OrderRequestInfoBuilder()
            : base(null, null)
        { }

        protected override bool CheckOrdersGranularity
        {
            get
            {
                return false;
            }
        }

        public static IOrderRequestInfoBuilder Instance = new OrderRequestInfoBuilder();

        public BankPoolClientOrderRequestInfo CreateLimit(decimal totalAmountBaseAbs, decimal requestedPrice,
                                                   DealDirection side, Symbol symbol)
        {
            return this.Targets.BankPool.CreateLimitOrderRequest(totalAmountBaseAbs, requestedPrice, side, symbol, false);
        }

        public BankPoolClientOrderRequestInfo CreateMarket(decimal totalAmountBaseAbs, DealDirection side, Symbol symbol)
        {
            return this.Targets.BankPool.CreateMarketOrderRequest(totalAmountBaseAbs, side, symbol, false);
        }

        public BankPoolClientOrderRequestInfo CreateMarketOnImprovement(decimal totalAmountBaseAbs, TimeSpan waitTimeout, DealDirection side, Symbol symbol)
        {
            return this.Targets.BankPool.CreateMarketOnImprovementOrderRequest(totalAmountBaseAbs, waitTimeout, side, symbol, false);
        }

        public BankPoolClientOrderRequestInfo CreateQuoted(decimal totalAmountBaseAbs, decimal requestedPrice,
                                                           DealDirection side,
                                                           PriceObject price)
        {
            return this.Targets.BankPool.CreateQuotedOrderRequest(totalAmountBaseAbs, requestedPrice, side, price, false);
        }

        public BankPoolClientOrder CreateClientOrder(BankPoolClientOrderRequestInfo orderRequestInfo)
        {
            return this.Targets.BankPool.CreateClientOrder(orderRequestInfo);
        }

        protected override void LocalResourcesCleanupRoutine()
        {
            throw new NotImplementedException();
        }

        protected override string ClientIdentityInternal
        {
            get { return "TestClient_001"; }
            set
            {
                throw new NotImplementedException();
            }
        }

        protected override IntegratorRequestResult SubmitRequestInternal(IntegratorRequestInfo clientOrderBase)
        {
            throw new NotImplementedException();
        }

        protected override bool IsConnectionChannelClosed
        {
            get { throw new NotImplementedException(); }
        }
    }

    internal class NoIdentityQuote
    {
        public PriceObjectInternal AskPrice { get; private set; }
        public PriceObjectInternal BidPrice { get; private set; }

        public PriceObjectInternal GetPrice(PriceSide side)
        {
            if (side == PriceSide.Ask)
                return this.AskPrice;
            else
                return this.BidPrice;
        }

        public NoIdentityQuote(decimal price, decimal size, PriceSide side, Symbol symbol, Counterparty counterparty,
            string subscriptionidentity, DateTime counterpartySendTime, MarketDataRecordType marketDataRecordType)
        {
            var priceObject = PriceObjectInternal.GetAllocatedTestingPriceObject(price, size, side, symbol, counterparty,
                subscriptionidentity, marketDataRecordType, DateTime.UtcNow, counterpartySendTime);

            if (side == PriceSide.Ask)
                this.AskPrice = priceObject;
            else
                this.BidPrice = priceObject;
        }

        public static PriceObjectInternal DummyPrice = PriceObjectInternal.GetAllocatedTestingPriceObject(0, 0,
            PriceSide.Ask, Common.Symbol.NULL, Counterparty.SOC, null, MarketDataRecordType.BankQuoteData, DateTime.MinValue, DateTime.MinValue);
    }

    [TestFixture]
    public class SingleSymbolDirectionOrderManagerTests
    {


        private static IRiskManager _nullRiskManager = new NullRiskManager();
        private static string _dummyErrorParameter = "foo";
        private static Counterparty _dummyCtp = Counterparty.L01;


        [Test]
        public void SingleSymbolDirectionOrderManager_constructionTest()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<List<IOrderFlowSession>>();

            ILogger testLogger = new TestLogger(Assert.Fail);

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.ZAR_JPY,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);
        }

        [Test]
        public void SingleSymbolDirectionOrderManager_constructionTest_WillThrowOnMultipleIdenticalInstances()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<List<IOrderFlowSession>>();
            ILogger testLogger = new TestLogger(Assert.Fail);

            var ssdm1 = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                              orderFlowSessionsListMock.Object, Symbol.USD_ZAR,
                                                              DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            var ssdm2 = new SingleSymbolDirectionOrderManager(OrderEvaluator.SellOrderEvaluator, priceBookMock.Object,
                                                              orderFlowSessionsListMock.Object, Symbol.USD_ZAR,
                                                              DealDirection.Sell, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            var ssdm3 = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                              orderFlowSessionsListMock.Object, Symbol.EUR_SEK,
                                                              DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            var ssdm4 = new SingleSymbolDirectionOrderManager(OrderEvaluator.SellOrderEvaluator, priceBookMock.Object,
                                                              orderFlowSessionsListMock.Object, Symbol.EUR_SEK,
                                                              DealDirection.Sell, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);
            bool thrown = false;
            try
            {
                var ssdm5 = new SingleSymbolDirectionOrderManager(OrderEvaluator.SellOrderEvaluator, priceBookMock.Object,
                                                              orderFlowSessionsListMock.Object, Symbol.USD_ZAR,
                                                              DealDirection.Sell, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message.Contains("thread safe"));
                thrown = true;
            }

            Assert.IsTrue(thrown);
        }

        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterOrder_WillRefuseInvalidOrders()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns(integratorOrderExternalMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);

            ILogger testLogger = new TestLogger(Assert.Fail);

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.USD_TRY,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(200, 3, DealDirection.Buy, Symbol.EUR_SEK);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);
            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateLimit(200, 3, DealDirection.Sell, Symbol.USD_TRY);
            IBankPoolClientOrder clientOrder2 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info2);
            ssdm.RegisterOrder(clientOrder2, dispatchGatewayMock.Object);

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.RejectedByIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_SEK),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.RejectedByIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_TRY),
                Times.Exactly(1));
        }

        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterMultipleLimitOrders_OnlyBestOnesWillBeFilledOnMatchingPrice()
        {
            //
            // SCENARIO: 3 orders comes in, we want to test that all that can be satisfied (even partially) will be satisfied
            //    also that they will be satisfied in proper order (age of orders)
            // 

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            //Be careful! this is just for the case where one external order is reurned
            var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                    {
                                        //var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                        integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                                   .Returns(ori);
                                        integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                                   .Returns(Counterparty.L19);
                                        integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                                   .Returns("extOrd1");
                                        integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                                   .Returns(200);
                                        return integratorOrderExternalMock.Object;
                                    });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.USD_MXN,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            //Populate the best price by something insuficient
            NoIdentityQuote aQuoteBad = new NoIdentityQuote(9.9m, 400, PriceSide.Ask, Symbol.USD_MXN, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuoteBad.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuoteBad.AskPrice);

            //Let's submit 3 orders, middle, least demanding and then the most demanding. Order of submission
            // is important for the actual matching order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 3, DealDirection.Buy, Symbol.USD_MXN);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateLimit(200, 4, DealDirection.Buy, Symbol.USD_MXN);
            IBankPoolClientOrder clientOrder2 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info2);

            BankPoolClientOrderRequestInfo info3 = OrderRequestInfoBuilder.Instance.CreateLimit(50, 2, DealDirection.Buy, Symbol.USD_MXN);
            IBankPoolClientOrder clientOrder3 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info3);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder2, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder3, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_MXN),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_MXN),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder3.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_MXN),
                Times.Exactly(1));

            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(2.5m, 200, PriceSide.Ask, Symbol.USD_MXN, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 200)),
                Times.Exactly(1));

            // Verify that the matching happened in proper sizes and order

            integratorOrderExternalMock.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              integratorOrderExternalMock.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      integratorOrderExternalMock.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      integratorOrderExternalMock.Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder2.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(2));
        }


        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterMultipleLimitOrders_OthersWillBeFilledAfterBestOneIsCancelled()
        {
            //
            // SCENARIO: 3 orders comes in, we want to test that all that can be satisfied (even partially) will be satisfied
            //    also that they will be satisfied in proper order (age of orders)
            // 

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            //Be careful! this is just for the case where one external order is reurned
            var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    //var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(200);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.NZD_SEK,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);
            ssdm.KeepClosedOrders = true;

            //Populate the best price by something insuficient
            NoIdentityQuote aQuoteBad = new NoIdentityQuote(9.9m, 400, PriceSide.Ask, Symbol.NZD_SEK, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuoteBad.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuoteBad.AskPrice);

            //Let's submit 3 orders, middle, least demanding and then the most demanding. Order of submission
            // is important for the actual matching order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 3, DealDirection.Buy, Symbol.NZD_SEK);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateLimit(200, 4, DealDirection.Buy, Symbol.NZD_SEK);
            IBankPoolClientOrder clientOrder2 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info2);

            BankPoolClientOrderRequestInfo info3 = OrderRequestInfoBuilder.Instance.CreateLimit(50, 2, DealDirection.Buy, Symbol.NZD_SEK);
            IBankPoolClientOrder clientOrder3 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info3);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder2, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder3, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.NZD_SEK),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.NZD_SEK),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder3.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.NZD_SEK),
                Times.Exactly(1));

            //Now cancel the best order

            ssdm.CancelOrder(new CancelOrderRequestInfo(clientOrder2.ClientOrderIdentity, clientOrder2.ClientIdentity, TradingTargetType.BankPool), dispatchGatewayMock.Object);

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.RemovedFromIntegrator &&
                        info.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestAccepted &&
                        info.SizeBaseAbsCancelled == 200), Symbol.NZD_SEK),
                Times.Exactly(1));

            Assert.AreEqual(0, ssdm.GetOrder(clientOrder2.ClientOrderIdentity).SizeBaseAbsActive);
            Assert.AreEqual(0, ssdm.GetOrder(clientOrder2.ClientOrderIdentity).SizeBaseAbsFilled);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(2, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);


            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(3.5m, 200, PriceSide.Ask, Symbol.NZD_SEK, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.AskPrice);

            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Never());
        }

        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterMultipleLimitOrders_LiquidityIsNotExceededOnMatchingPrice()
        {
            //
            // SCENARIO: 3 orders comes in, we want to test that all that can be satisfied (even partially) will be satisfied
            //    but only up to available liquidity of incoming price
            // 

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            //Be careful! this is just for the case where one external order is reurned
            var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    //var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(250);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.USD_ILS,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            //Populate the best price by something insuficient
            NoIdentityQuote aQuoteBad = new NoIdentityQuote(9.9m, 400, PriceSide.Ask, Symbol.USD_ILS, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuoteBad.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuoteBad.AskPrice);

            //Let's submit 3 orders, middle, least demanding and then the most demanding. Order of submission
            // is important for the actual matching order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 3, DealDirection.Buy, Symbol.USD_ILS);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateLimit(200, 4, DealDirection.Buy, Symbol.USD_ILS);
            IBankPoolClientOrder clientOrder2 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info2);

            BankPoolClientOrderRequestInfo info3 = OrderRequestInfoBuilder.Instance.CreateLimit(50, 2, DealDirection.Buy, Symbol.USD_ILS);
            IBankPoolClientOrder clientOrder3 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info3);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder2, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder3, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_ILS),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_ILS),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder3.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_ILS),
                Times.Exactly(1));

            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(1.5m, 250, PriceSide.Ask, Symbol.USD_ILS, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 250)),
                Times.Exactly(1));

            // Verify that the matching happened in proper sizes and order

            integratorOrderExternalMock.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              integratorOrderExternalMock.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      integratorOrderExternalMock.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      integratorOrderExternalMock.Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 1.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder2.ClientOrderIdentity && deal.FilledAmountBaseAbs == 150 && deal.Price == 1.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(2));
        }


        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterMultipleLimitOrders_EachIncomingPriceFillsOneBestOrder()
        {
            //
            // SCENARIO: 3 orders comes in, all are saisfied allways from the least demanding one
            //   (this test proper setting of top order)

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.USD_CZK,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            //Let's submit 3 orders, middle, least demanding and then the most demanding. Order of submission
            // is important for the actual matching order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 3, DealDirection.Buy, Symbol.USD_CZK);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateLimit(200, 4, DealDirection.Buy, Symbol.USD_CZK);
            IBankPoolClientOrder clientOrder2 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info2);

            BankPoolClientOrderRequestInfo info3 = OrderRequestInfoBuilder.Instance.CreateLimit(50, 2, DealDirection.Buy, Symbol.USD_CZK);
            IBankPoolClientOrder clientOrder3 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info3);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder2, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder3, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_CZK),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_CZK),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder3.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_CZK),
                Times.Exactly(1));

            // Send first price and verify that the matching happened

            var integratorOrderExternalMock1 = new Mock<IIntegratorOrderExternal>();
            integratorOrderExternalMock1.Setup(ordeextern => ordeextern.Counterparty)
                                       .Returns(Counterparty.L19);
            integratorOrderExternalMock1.Setup(ordeextern => ordeextern.Identity)
                                       .Returns("extOrd1");
            integratorOrderExternalMock1.Setup(ordeextern => ordeextern.FilledAmount)
                                       .Returns(200);
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    integratorOrderExternalMock1.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    return integratorOrderExternalMock1.Object;
                                });

            NoIdentityQuote aQuote = new NoIdentityQuote(3.5m, 400, PriceSide.Ask, Symbol.USD_CZK, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 200)),
                Times.Exactly(1));


            // Send second price and verify that the matching happened

            var integratorOrderExternalMock2 = new Mock<IIntegratorOrderExternal>();
            integratorOrderExternalMock2.Setup(ordeextern => ordeextern.Counterparty)
                                       .Returns(Counterparty.L19);
            integratorOrderExternalMock2.Setup(ordeextern => ordeextern.Identity)
                                       .Returns("extOrd2");
            integratorOrderExternalMock2.Setup(ordeextern => ordeextern.FilledAmount)
                                       .Returns(100);
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    integratorOrderExternalMock2.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    return integratorOrderExternalMock2.Object;
                                });

            NoIdentityQuote aQuote2 = new NoIdentityQuote(3.0m, 400, PriceSide.Ask, Symbol.USD_CZK, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote2.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote2.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote2) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            // Send third price and verify that the matching happened

            var integratorOrderExternalMock3 = new Mock<IIntegratorOrderExternal>();
            integratorOrderExternalMock3.Setup(ordeextern => ordeextern.Counterparty)
                                       .Returns(Counterparty.L19);
            integratorOrderExternalMock3.Setup(ordeextern => ordeextern.Identity)
                                       .Returns("extOrd2");
            integratorOrderExternalMock3.Setup(ordeextern => ordeextern.FilledAmount)
                                       .Returns(50);
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    integratorOrderExternalMock3.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    return integratorOrderExternalMock3.Object;
                                });

            NoIdentityQuote aQuote3 = new NoIdentityQuote(1.9m, 400, PriceSide.Ask, Symbol.USD_CZK, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote3.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote3.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote3) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));


            // Verify that the matching happened in proper sizes and order

            integratorOrderExternalMock1.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              integratorOrderExternalMock1.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      integratorOrderExternalMock1.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      integratorOrderExternalMock1.Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder2.ClientOrderIdentity && deal.FilledAmountBaseAbs == 200 && deal.Price == 3.5m)), Times.Exactly(1));

            integratorOrderExternalMock2.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              integratorOrderExternalMock2.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      integratorOrderExternalMock2.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      integratorOrderExternalMock2.Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 3.0m)), Times.Exactly(1));

            integratorOrderExternalMock3.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              integratorOrderExternalMock3.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      integratorOrderExternalMock3.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      integratorOrderExternalMock3.Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder3.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 && deal.Price == 1.9m)), Times.Exactly(1));


            //nothing else with no orders will hapen after this

            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            NoIdentityQuote aQuote4 = new NoIdentityQuote(1.0m, 400, PriceSide.Ask, Symbol.USD_CZK, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote4.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote4.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Exactly(3));


            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(3));
        }

        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterLimitOrder_WillBeSatisfiedByExistingBestPrice()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            //Be careful! this is just for the case where one external order is reurned
            var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    //var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.NOK_SEK,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            // Send price
            NoIdentityQuote aQuote = new NoIdentityQuote(2.5m, 400, PriceSide.Ask, Symbol.NOK_SEK, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.AskPrice);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(aQuote.AskPrice);

            //Let's submit order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 3, DealDirection.Buy, Symbol.NOK_SEK);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the order was successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.NOK_SEK),
                Times.Exactly(1));


            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            // Verify that the matching happened in proper siz and order

            integratorOrderExternalMock.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              integratorOrderExternalMock.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      integratorOrderExternalMock.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      integratorOrderExternalMock.Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.5m)), Times.Exactly(1));


            //nothing else with no orders will hapen after this
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(1));
        }


        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterLimitOrder_WillBeSatisfiedByExistingBestPriceAndOtherPricesFromQuoteBook()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.AUD_SGD,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);


            List<PriceObjectInternal> priceBook = new List<PriceObjectInternal>
                {
                    new NoIdentityQuote(2.5m, 100, PriceSide.Ask, Symbol.AUD_SGD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.6m, 50, PriceSide.Ask, Symbol.AUD_SGD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.7m, 100, PriceSide.Ask, Symbol.AUD_SGD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.8m, 100, PriceSide.Ask, Symbol.AUD_SGD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal
                };
            //price book copy in order to be able to assert
            List<PriceObjectInternal> priceBookCopy = new List<PriceObjectInternal>(priceBook);

            priceBookMock.Setup(pb => pb.RemoveIdentical(It.IsAny<PriceObjectInternal>()))
                         .Returns((PriceObjectInternal price) => priceBook.Remove(price));

            //Beware! we need to set the mock with method (priceBook.ToArray) not the value (priceBook.ToArray())
            // so that we don't get the old snapshot
            priceBookMock.Setup(pb => pb.GetSortedClone()).Returns(priceBook.ToArray);

            // Send price
            priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, priceBook[0]);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(priceBook[0]);



            //Let's submit order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(300, 3, DealDirection.Buy, Symbol.AUD_SGD);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the order was successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_SGD),
                Times.Exactly(1));


            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[0]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[1]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[2]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[3]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));


            // Verify that the matching happened in proper size and order

            foreach (Mock<IIntegratorOrderExternal> externalOrdersMock in externalOrdersMocks)
            {
                externalOrdersMock.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMock.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMock.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMock.Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));
            }

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 && deal.Price == 2.6m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.7m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 && deal.Price == 2.8m)), Times.Exactly(1));

            //we used top 3 prices
            Assert.AreEqual(1, priceBook.Count);

            //nothing else with no orders will hapen after this
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(4));
        }


        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterLimitOrder_WillBePartiallySatisfiedByExistingBestPriceAndRegisteredForLaterFilling()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.GBP_AUD,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);


            List<PriceObjectInternal> priceBook = new List<PriceObjectInternal>
                {
                    new NoIdentityQuote(2.5m, 100, PriceSide.Ask, Symbol.GBP_AUD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.6m, 50, PriceSide.Ask, Symbol.GBP_AUD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.7m, 100, PriceSide.Ask, Symbol.GBP_AUD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.8m, 100, PriceSide.Ask, Symbol.GBP_AUD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal
                };
            //price book copy in order to be able to assert
            List<PriceObjectInternal> priceBookCopy = new List<PriceObjectInternal>(priceBook);

            priceBookMock.Setup(pb => pb.RemoveIdentical(It.IsAny<PriceObjectInternal>()))
                         .Returns((PriceObjectInternal price) => priceBook.Remove(price));

            //Beware! we need to set the mock with method (priceBook.ToArray) not the value (priceBook.ToArray())
            // so that we don't get the old snapshot
            priceBookMock.Setup(pb => pb.GetSortedClone()).Returns(priceBook.ToArray);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, priceBook[0], DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, priceBook[0]);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(priceBook[0]);



            //Let's submit order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(300, 2.6m, DealDirection.Buy, Symbol.GBP_AUD);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the order was successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.GBP_AUD),
                Times.Exactly(1));


            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[0]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[1]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));

            // Verify that the matching happened in proper size and order

            foreach (Mock<IIntegratorOrderExternal> externalOrdersMock in externalOrdersMocks)
            {
                externalOrdersMock.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMock.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMock.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMock.Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));
            }

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 && deal.Price == 2.6m)), Times.Exactly(1));

            //we used top 2 prices
            Assert.AreEqual(2, priceBook.Count);

            //One limit order is still registered
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(2));

            // Send price that will satisfy the order
            NoIdentityQuote anotherQuote = new NoIdentityQuote(2.45m, 400, PriceSide.Ask, Symbol.GBP_AUD, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, anotherQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, anotherQuote.AskPrice);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(anotherQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, anotherQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 150)),
                Times.Exactly(1));

            //Last limit order is now pending to be fully filled
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 150 && deal.Price == 2.45m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(3));

            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
        }


        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterMarketOrder_WillBePartiallySatisfiedByExistingBestPriceAndRestOfPriceBookAndRegisteredForLaterFilling()
        {
            //
            // SCENARIO: market order comes in - it immediately consumes the Price book top and then rest of the pricebook
            //    if it was not fully satisfied yet. Lastly it will be registered and wait for any incoming quotes.

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.EUR_PLN,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);


            List<PriceObjectInternal> priceBook = new List<PriceObjectInternal>
                {
                    new NoIdentityQuote(2.5m, 100, PriceSide.Ask, Symbol.EUR_PLN, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.6m, 50, PriceSide.Ask, Symbol.EUR_PLN, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(20.7m, 100, PriceSide.Ask, Symbol.EUR_PLN, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal
                };
            //price book copy in order to be able to assert
            List<PriceObjectInternal> priceBookCopy = new List<PriceObjectInternal>(priceBook);

            priceBookMock.Setup(pb => pb.RemoveIdentical(It.IsAny<PriceObjectInternal>()))
                         .Returns((PriceObjectInternal price) => priceBook.Remove(price));

            //Beware! we need to set the mock with method (priceBook.ToArray) not the value (priceBook.ToArray())
            // so that we don't get the old snapshot
            priceBookMock.Setup(pb => pb.GetSortedClone()).Returns(priceBook.ToArray);

            // Send price
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, priceBook[0], DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, priceBook[0]);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(priceBook[0]);



            //Let's submit order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarket(400, DealDirection.Buy, Symbol.EUR_PLN);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the order was successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_PLN),
                Times.Exactly(1));


            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[0]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[1]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[2]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            // Verify that the matching happened in proper size and order

            foreach (Mock<IIntegratorOrderExternal> externalOrdersMock in externalOrdersMocks)
            {
                externalOrdersMock.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMock.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMock.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMock.Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));
            }

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.5m && deal.MarketOnImprovementResult == null && deal.FlowSide == FlowSide.LiquidityTaker)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 && deal.Price == 2.6m && deal.MarketOnImprovementResult == null && deal.FlowSide == FlowSide.LiquidityTaker)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 20.7m && deal.MarketOnImprovementResult == null && deal.FlowSide == FlowSide.LiquidityTaker)), Times.Exactly(1));

            //we used whole price book
            Assert.AreEqual(0, priceBook.Count);

            //One market order is still registered
            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(3));

            // Send new price
            NoIdentityQuote anotherQuote = new NoIdentityQuote(2.45m, 100, PriceSide.Ask, Symbol.EUR_PLN, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, anotherQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, anotherQuote.AskPrice);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(anotherQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, anotherQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            //One market order is still registered
            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);


            // Send another price - this will finally fill the market order
            NoIdentityQuote lastQuote = new NoIdentityQuote(20.45m, 100, PriceSide.Ask, Symbol.EUR_PLN, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, lastQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, lastQuote.AskPrice);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(lastQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, lastQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));


            //Last market order is now pending to be fully filled
            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            externalOrdersMocks[externalOrdersMocks.Count - 2].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[externalOrdersMocks.Count - 2].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks[externalOrdersMocks.Count - 2].Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks[externalOrdersMocks.Count - 2].Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 && deal.Price == 20.45m && deal.MarketOnImprovementResult == null && deal.FlowSide == FlowSide.LiquidityMaker)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.45m && deal.MarketOnImprovementResult == null && deal.FlowSide == FlowSide.LiquidityMaker)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(5));

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
        }


        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterMultipleMarketOrders_TheywillBefilledInChronologicOrder()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(() => ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.GBP_CAD,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            //Let's submit 3 market orders

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarket(100, DealDirection.Buy, Symbol.GBP_CAD);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateMarket(200, DealDirection.Buy, Symbol.GBP_CAD);
            IBankPoolClientOrder clientOrder2 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info2);

            BankPoolClientOrderRequestInfo info3 = OrderRequestInfoBuilder.Instance.CreateMarket(50, DealDirection.Buy, Symbol.GBP_CAD);
            IBankPoolClientOrder clientOrder3 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info3);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder2, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder3, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.GBP_CAD),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.GBP_CAD),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder3.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.GBP_CAD),
                Times.Exactly(1));

            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(2.5m, 200, PriceSide.Ask, Symbol.GBP_CAD, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 200)),
                Times.Exactly(1));

            // Verify that the matching happened in proper sizes and order

            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder2.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(2));

            Assert.AreEqual(2, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            NoIdentityQuote anotherQuote = new NoIdentityQuote(20.5m, 200, PriceSide.Ask, Symbol.GBP_CAD, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, anotherQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, anotherQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, anotherQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 150)),
                Times.Exactly(1));

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(2, ssdm.InactivePendingOrdersCount);

            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder2.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 20.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder3.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 && deal.Price == 20.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(4));

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
        }

        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterMultipleMarketAndLimitOrders_LimitGetPreference()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(() => ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.GBP_NZD,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            //Let's submit 3 market orders

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarket(100, DealDirection.Buy, Symbol.GBP_NZD);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 2.4m, DealDirection.Buy, Symbol.GBP_NZD);
            IBankPoolClientOrder clientOrder2 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info2);

            BankPoolClientOrderRequestInfo info3 = OrderRequestInfoBuilder.Instance.CreateMarket(100, DealDirection.Buy, Symbol.GBP_NZD);
            IBankPoolClientOrder clientOrder3 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info3);

            BankPoolClientOrderRequestInfo info4 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 2.5m, DealDirection.Buy, Symbol.GBP_NZD);
            IBankPoolClientOrder clientOrder4 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info4);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder2, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder3, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder4, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.GBP_NZD),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.GBP_NZD),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder3.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.GBP_NZD),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder4.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.GBP_NZD),
                Times.Exactly(1));

            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(2.5m, 100, PriceSide.Ask, Symbol.GBP_NZD, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            Assert.AreEqual(2, ssdm.ActiveMarketOrders);
            Assert.AreEqual(2, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.AskPrice);

            Assert.AreEqual(2, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            // Verify that the matching happened in proper sizes and order

            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder4.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(1));

            Assert.AreEqual(2, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            NoIdentityQuote anotherQuote = new NoIdentityQuote(20.5m, 100, PriceSide.Ask, Symbol.GBP_NZD, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, anotherQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, anotherQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, anotherQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 20.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(2));

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);


            NoIdentityQuote lastQuote = new NoIdentityQuote(2.4m, 190, PriceSide.Ask, Symbol.GBP_NZD, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, lastQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, lastQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, lastQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 190)),
                Times.Exactly(1));

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder2.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.4m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder3.ClientOrderIdentity && deal.FilledAmountBaseAbs == 90 && deal.Price == 2.4m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(4));

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
        }

        //test successfull full cancellation

        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterMultipleMarketAndLimitOrders_AllCanBeSuccessfullyCancelled()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.AUD_JPY,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);
            ssdm.KeepClosedOrders = true;

            //Let's submit 3 market orders

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarket(100, DealDirection.Buy, Symbol.AUD_JPY);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateLimit(200, 2.4m, DealDirection.Buy, Symbol.AUD_JPY);
            IBankPoolClientOrder clientOrder2 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info2);

            BankPoolClientOrderRequestInfo info3 = OrderRequestInfoBuilder.Instance.CreateMarket(300, DealDirection.Buy, Symbol.AUD_JPY);
            IBankPoolClientOrder clientOrder3 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info3);

            BankPoolClientOrderRequestInfo info4 = OrderRequestInfoBuilder.Instance.CreateLimit(400, 2.5m, DealDirection.Buy, Symbol.AUD_JPY);
            IBankPoolClientOrder clientOrder4 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info4);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder2, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder3, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder4, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_JPY),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_JPY),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder3.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_JPY),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder4.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_JPY),
                Times.Exactly(1));

            Assert.AreEqual(2, ssdm.ActiveMarketOrders);
            Assert.AreEqual(2, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            ssdm.CancelOrder(new CancelOrderRequestInfo(clientOrder1.ClientOrderIdentity, clientOrder1.ClientIdentity, TradingTargetType.BankPool), dispatchGatewayMock.Object);

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.RemovedFromIntegrator &&
                        info.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestAccepted &&
                        info.SizeBaseAbsCancelled == 100), Symbol.AUD_JPY),
                Times.Exactly(1));

            Assert.AreEqual(0, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsActive);
            Assert.AreEqual(0, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsFilled);

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(2, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);


            ssdm.CancelOrder(new CancelOrderRequestInfo(clientOrder2.ClientOrderIdentity, clientOrder2.ClientIdentity, TradingTargetType.BankPool), dispatchGatewayMock.Object);

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.RemovedFromIntegrator &&
                        info.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestAccepted &&
                        info.SizeBaseAbsCancelled == 200), Symbol.AUD_JPY),
                Times.Exactly(1));

            Assert.AreEqual(0, ssdm.GetOrder(clientOrder2.ClientOrderIdentity).SizeBaseAbsActive);
            Assert.AreEqual(0, ssdm.GetOrder(clientOrder2.ClientOrderIdentity).SizeBaseAbsFilled);

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);


            ssdm.CancelOrder(new CancelOrderRequestInfo(clientOrder3.ClientOrderIdentity, clientOrder3.ClientIdentity, TradingTargetType.BankPool), dispatchGatewayMock.Object);

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder3.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.RemovedFromIntegrator &&
                        info.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestAccepted &&
                        info.SizeBaseAbsCancelled == 300), Symbol.AUD_JPY),
                Times.Exactly(1));

            Assert.AreEqual(0, ssdm.GetOrder(clientOrder3.ClientOrderIdentity).SizeBaseAbsActive);
            Assert.AreEqual(0, ssdm.GetOrder(clientOrder3.ClientOrderIdentity).SizeBaseAbsFilled);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);


            ssdm.CancelOrder(new CancelOrderRequestInfo(clientOrder4.ClientOrderIdentity, clientOrder4.ClientIdentity, TradingTargetType.BankPool), dispatchGatewayMock.Object);

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder4.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.RemovedFromIntegrator &&
                        info.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestAccepted &&
                        info.SizeBaseAbsCancelled == 400), Symbol.AUD_JPY),
                Times.Exactly(1));

            Assert.AreEqual(0, ssdm.GetOrder(clientOrder4.ClientOrderIdentity).SizeBaseAbsActive);
            Assert.AreEqual(0, ssdm.GetOrder(clientOrder4.ClientOrderIdentity).SizeBaseAbsFilled);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
        }


        [Test]
        public void SingleSymbolDirectionOrderManager_CancelNonexistingOrder_ErrorWillBeLoggedAndFailedStatusReturned()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = false };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.USD_HUF,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);
            ssdm.KeepClosedOrders = true;

            //Let's submit 3 market orders

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarket(100, DealDirection.Buy, Symbol.USD_HUF);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.CancelOrder(new CancelOrderRequestInfo(clientOrder1.ClientOrderIdentity, clientOrder1.ClientIdentity, TradingTargetType.BankPool), dispatchGatewayMock.Object);

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestFailed &&
                        info.OrderStatus == ClientOrderStatus.NotActiveInIntegrator), Common.Symbol.NULL),
                Times.Once());
        }

        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterLimitOrder_SuccessfulCancellationAfterPartialFill()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            //Be careful! this is just for the case where one external order is reurned
            var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    //var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.EUR_MXN,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);
            ssdm.KeepClosedOrders = true;

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 3, DealDirection.Buy, Symbol.EUR_MXN);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_MXN),
                Times.Exactly(1));

            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(2.5m, 50, PriceSide.Ask, Symbol.EUR_MXN, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);


            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.AskPrice);
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.AskPrice, DateTime.UtcNow);


            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));

            // Verify that the matching happened in proper sizes and order

            integratorOrderExternalMock.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              integratorOrderExternalMock.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      integratorOrderExternalMock.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      integratorOrderExternalMock.Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 && deal.Price == 2.5m)), Times.Exactly(1));

            Assert.AreEqual(50, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsActive);
            Assert.AreEqual(50, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsFilled);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            ssdm.CancelOrder(new CancelOrderRequestInfo(clientOrder1.ClientOrderIdentity, clientOrder1.ClientIdentity, TradingTargetType.BankPool), dispatchGatewayMock.Object);

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.RemovedFromIntegrator &&
                        info.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestAccepted &&
                        info.SizeBaseAbsCancelled == 50), Symbol.EUR_MXN),
                Times.Exactly(1));

            Assert.AreEqual(0, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsActive);
            Assert.AreEqual(50, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsFilled);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
        }


        private bool IsOrderCreatedFromQuote(OrderRequestInfo orderRequestInfo, NoIdentityQuote quote)
        {
            return IsOrderCreatedFromPrice(orderRequestInfo,
                quote.GetPrice(orderRequestInfo.Side.ToInitiatingPriceDirection()));
        }

        private bool IsOrderCreatedFromPrice(OrderRequestInfo orderRequestInfo, PriceObject price)
        {
            return orderRequestInfo.IntegratorPriceIdentity == price.IntegratorIdentity;
        }

        [Test]
        public void SingleSymbolDirectionOrderManager_CancelLimitOrderWhenFillIsInProgress_PendingCancelProceedWithFillAndCancelRestAfterwards()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            //Be careful! this is just for the case where one external order is reurned
            var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    //var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.EUR_ZAR,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);
            ssdm.KeepClosedOrders = true;

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 3, DealDirection.Buy, Symbol.EUR_ZAR);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_ZAR),
                Times.Exactly(1));

            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(2.5m, 50, PriceSide.Ask, Symbol.EUR_ZAR, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));


            ssdm.CancelOrder(new CancelOrderRequestInfo(clientOrder1.ClientOrderIdentity, clientOrder1.ClientIdentity, TradingTargetType.BankPool), dispatchGatewayMock.Object);

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator &&
                        info.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestInProgress &&
                        info.SizeBaseAbsCancelled == 50), Symbol.EUR_ZAR),
                Times.Exactly(1));

            Assert.AreEqual(0, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsActive);
            Assert.AreEqual(0, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsFilled);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);


            integratorOrderExternalMock.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              integratorOrderExternalMock.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      integratorOrderExternalMock.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      integratorOrderExternalMock.Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 && deal.Price == 2.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.NotActiveInIntegrator &&
                        info.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestAccepted &&
                        info.SizeBaseAbsCancelled == 0), Symbol.EUR_ZAR),
                Times.Exactly(1));

            Assert.AreEqual(0, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsActive);
            Assert.AreEqual(50, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsFilled);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
        }



        [Test]
        public void SingleSymbolDirectionOrderManager_CancelLimitOrderWhenFillIsInProgress_PendingCancelProceedWithRejectAndCancelRestAfterwards()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            //Be careful! this is just for the case where one external order is reurned
            var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    //var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.ZAR_MXN,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);
            ssdm.KeepClosedOrders = true;

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 3, DealDirection.Buy, Symbol.ZAR_MXN);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.ZAR_MXN),
                Times.Exactly(1));

            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(2.5m, 50, PriceSide.Ask, Symbol.ZAR_MXN, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));


            ssdm.CancelOrder(new CancelOrderRequestInfo(clientOrder1.ClientOrderIdentity, clientOrder1.ClientIdentity, TradingTargetType.BankPool), dispatchGatewayMock.Object);

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator &&
                        info.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestInProgress &&
                        info.SizeBaseAbsCancelled == 50), Symbol.ZAR_MXN),
                Times.Exactly(1));

            Assert.AreEqual(0, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsActive);
            Assert.AreEqual(0, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsFilled);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);


            integratorOrderExternalMock.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              integratorOrderExternalMock.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected, null,
                                                  new RejectionInfo(string.Empty,
                                                      integratorOrderExternalMock.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      RejectionType.OrderReject)));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.IsAny<IntegratorDealInternal>()), Times.Never());

            dispatchGatewayMock.Verify(gateway => gateway.OnCounterpartyRejectedOrder(It.IsAny<CounterpartyRejectionInfo>()), Times.Once());

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.RemovedFromIntegrator &&
                        info.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestAccepted &&
                        info.SizeBaseAbsCancelled == 50), Symbol.ZAR_MXN),
                Times.Exactly(1));

            Assert.AreEqual(0, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsActive);
            Assert.AreEqual(0, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsFilled);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
        }


        [Test]
        public void SingleSymbolDirectionOrderManager_MultipleMarketAndLimitOrdersRejectComes_OrdersWillBeAgainImmediatelySubjectToFilling()
        {
            //  - 2 limit and 2 market orders are regstered
            //  - older limit and market gets fully covered another market partially by incoming quote
            //  - another price comes in - this still doesn't satisfy the another limit
            //  - external order gets rejected - this will immediately lead to older limit and market get rematched (they will also use other prices in quote book)

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(
                                                                   () =>
                                                                   integratorOrderExternalMock.Object.OrderStatus ==
                                                                   IntegratorOrderExternalStatus.Filled
                                                                       ? ori.SizeBaseAbsInitial
                                                                       : 0);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = false, AllowedFailureLogPortion = "was rejected" };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.SellOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.AUD_CAD,
                                                             DealDirection.Sell, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            //Let's submit orders

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarket(100, DealDirection.Sell, Symbol.AUD_CAD);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 2.5m, DealDirection.Sell, Symbol.AUD_CAD);
            IBankPoolClientOrder clientOrder2 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info2);

            BankPoolClientOrderRequestInfo info3 = OrderRequestInfoBuilder.Instance.CreateMarket(100, DealDirection.Sell, Symbol.AUD_CAD);
            IBankPoolClientOrder clientOrder3 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info3);

            BankPoolClientOrderRequestInfo info4 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 2.4m, DealDirection.Sell, Symbol.AUD_CAD);
            IBankPoolClientOrder clientOrder4 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info4);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder2, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder3, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder4, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_CAD),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_CAD),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder3.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_CAD),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder4.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_CAD),
                Times.Exactly(1));

            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(2.45m, 240, PriceSide.Bid, Symbol.AUD_CAD, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            Assert.AreEqual(2, ssdm.ActiveMarketOrders);
            Assert.AreEqual(2, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.BidPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.BidPrice);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(aQuote.BidPrice);

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(2, ssdm.InactivePendingOrdersCount);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 240)),
                Times.Exactly(1));


            //Setup the price book and send one price update

            List<PriceObjectInternal> priceBook = new List<PriceObjectInternal>
                {
                    new NoIdentityQuote(2.45m, 100, PriceSide.Bid, Symbol.AUD_CAD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.46m, 100, PriceSide.Bid, Symbol.AUD_CAD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice as PriceObjectInternal,
                    new NoIdentityQuote(20.7m, 100, PriceSide.Bid, Symbol.AUD_CAD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice as PriceObjectInternal
                };
            //price book copy in order to be able to assert
            List<PriceObjectInternal> priceBookCopy = new List<PriceObjectInternal>(priceBook);

            priceBookMock.Setup(pb => pb.RemoveIdentical(It.IsAny<PriceObjectInternal>()))
                         .Returns((PriceObjectInternal price) => priceBook.Remove(price));

            priceBookMock.Setup(pb => pb.GetSortedClone()).Returns(priceBook.ToArray);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, priceBook[0], DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, priceBook[0]);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(priceBook[0]);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(3, ssdm.InactivePendingOrdersCount);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[0]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 60)),
                Times.Exactly(1));


            // Now reject the first external order and verify that immediate rematching happens
            externalOrdersMocks[externalOrdersMocks.Count - 2].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[externalOrdersMocks.Count - 2].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected, null,
                                                  new RejectionInfo("Rejected", externalOrdersMocks[externalOrdersMocks.Count - 2].Object.OrderRequestInfo.SizeBaseAbsInitial, RejectionType.OrderReject)));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Never());

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(3, ssdm.InactivePendingOrdersCount);


            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[0]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 40)),
                Times.Exactly(1));


            //TODO:
            // this actually shows the design bug - if one external order is rejected and it was created from multiple internal orders
            //  each internal order will have it's own external order (or orders if multiple prices were used), if if some gouping would be possible
            // This is of a low priority now

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[1]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 60)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[1]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 40)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[2]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 60)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[2]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 40)),
                Times.Exactly(1));


            externalOrdersMocks[externalOrdersMocks.Count - 5].Setup(exto => exto.OrderStatus).Returns(IntegratorOrderExternalStatus.Filled);
            externalOrdersMocks[externalOrdersMocks.Count - 5].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[externalOrdersMocks.Count - 5].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks[externalOrdersMocks.Count - 5].Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks[externalOrdersMocks.Count - 5].Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder4.ClientOrderIdentity && deal.FilledAmountBaseAbs == 40 && deal.Price == 2.45m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(1));

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(3, ssdm.InactivePendingOrdersCount);

            externalOrdersMocks[externalOrdersMocks.Count - 4].Setup(exto => exto.OrderStatus).Returns(IntegratorOrderExternalStatus.Filled);
            externalOrdersMocks[externalOrdersMocks.Count - 4].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[externalOrdersMocks.Count - 4].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks[externalOrdersMocks.Count - 4].Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks[externalOrdersMocks.Count - 4].Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            externalOrdersMocks[externalOrdersMocks.Count - 3].Setup(exto => exto.OrderStatus).Returns(IntegratorOrderExternalStatus.Filled);
            externalOrdersMocks[externalOrdersMocks.Count - 3].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[externalOrdersMocks.Count - 3].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks[externalOrdersMocks.Count - 3].Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks[externalOrdersMocks.Count - 3].Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder4.ClientOrderIdentity && deal.FilledAmountBaseAbs == 60 && deal.Price == 2.46m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 40 && deal.Price == 2.46m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(3));

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(2, ssdm.InactivePendingOrdersCount);

            externalOrdersMocks[externalOrdersMocks.Count - 2].Setup(exto => exto.OrderStatus).Returns(IntegratorOrderExternalStatus.Filled);
            externalOrdersMocks[externalOrdersMocks.Count - 2].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[externalOrdersMocks.Count - 2].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks[externalOrdersMocks.Count - 2].Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks[externalOrdersMocks.Count - 2].Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            externalOrdersMocks[externalOrdersMocks.Count - 1].Setup(exto => exto.OrderStatus).Returns(IntegratorOrderExternalStatus.Filled);
            externalOrdersMocks[externalOrdersMocks.Count - 1].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[externalOrdersMocks.Count - 1].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks[externalOrdersMocks.Count - 1].Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks[externalOrdersMocks.Count - 1].Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 60 && deal.Price == 20.7m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder3.ClientOrderIdentity && deal.FilledAmountBaseAbs == 40 && deal.Price == 20.7m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(5));

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);
        }



        [Test]
        public void SingleSymbolDirectionOrderManager_MultipleMarketAndLimitOrdersRejectComes_OrdersWillBeAgainSubjectToFillingWithNextPrice()
        {
            //  - 2 limit and 1 market orders are regstered
            //  - older limit and older market gets covered by incoming quote
            //  - external order gets rejected
            //  - new incoming price will afgain satisfy same older market and limit

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(
                                                                   () =>
                                                                   integratorOrderExternalMock.Object.OrderStatus ==
                                                                   IntegratorOrderExternalStatus.Filled
                                                                       ? ori.SizeBaseAbsInitial
                                                                       : 0);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true, AllowedFailureLogPortion = "was rejected" };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.SellOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.AUD_CHF,
                                                             DealDirection.Sell, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            //Let's submit orders

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarket(100, DealDirection.Sell, Symbol.AUD_CHF);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 2.4m, DealDirection.Sell, Symbol.AUD_CHF);
            IBankPoolClientOrder clientOrder2 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info2);

            BankPoolClientOrderRequestInfo info3 = OrderRequestInfoBuilder.Instance.CreateMarket(100, DealDirection.Sell, Symbol.AUD_CHF);
            IBankPoolClientOrder clientOrder3 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info3);

            BankPoolClientOrderRequestInfo info4 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 2.5m, DealDirection.Sell, Symbol.AUD_CHF);
            IBankPoolClientOrder clientOrder4 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info4);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder2, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder3, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder4, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_CHF),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_CHF),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder3.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_CHF),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder4.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_CHF),
                Times.Exactly(1));

            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(2.45m, 240, PriceSide.Bid, Symbol.AUD_CHF, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            Assert.AreEqual(2, ssdm.ActiveMarketOrders);
            Assert.AreEqual(2, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.BidPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.BidPrice);

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(2, ssdm.InactivePendingOrdersCount);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 240)),
                Times.Exactly(1));


            // Now reject the first external order and verify that they are put back to lists
            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected, null,
                                                  new RejectionInfo("Rejected", externalOrdersMocks.Last().Object.OrderRequestInfo.SizeBaseAbsInitial, RejectionType.OrderReject)));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Never());

            Assert.AreEqual(2, ssdm.ActiveMarketOrders);
            Assert.AreEqual(2, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);


            NoIdentityQuote anotherQuote = new NoIdentityQuote(2.56m, 230, PriceSide.Bid, Symbol.AUD_CHF, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, anotherQuote.BidPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, anotherQuote.BidPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, anotherQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 230)),
                Times.Exactly(1));

            //No other external orders were send
            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Exactly(2));


            externalOrdersMocks.Last().Setup(exto => exto.OrderStatus).Returns(IntegratorOrderExternalStatus.Filled);
            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder2.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.56m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder4.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.56m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 30 && deal.Price == 2.56m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(3));

            Assert.AreEqual(2, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
        }


        [Test]
        public void SingleSymbolDirectionOrderManager_MultipleMarketOrdersBrokenExecReportComes_OrdersPortionsWillNotBeAgainFilled()
        {
            //  - 2 market orders are regstered
            //  - fully and partially covered by incoming quote
            //  - external order gets rejected
            //  - new incoming price will only satisfy the used-to-be partially filled order

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(
                                                                   () =>
                                                                   integratorOrderExternalMock.Object.OrderStatus ==
                                                                   IntegratorOrderExternalStatus.Filled
                                                                       ? ori.SizeBaseAbsInitial
                                                                       : 0);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true, AllowedFailureLogPortion = "is in Broken state" };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.SellOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.AUD_NZD,
                                                             DealDirection.Sell, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            //Let's submit orders

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarket(100, DealDirection.Sell, Symbol.AUD_NZD);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateMarket(100, DealDirection.Sell, Symbol.AUD_NZD);
            IBankPoolClientOrder clientOrder2 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info2);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder2, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_NZD),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_NZD),
                Times.Exactly(1));

            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(2.45m, 140, PriceSide.Bid, Symbol.AUD_NZD, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            Assert.AreEqual(2, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.BidPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.BidPrice);

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 140)),
                Times.Exactly(1));


            // Now set the first external order to broken state and verify that they are not put back to lists

            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.InBrokenState));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Never());

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);


            NoIdentityQuote anotherQuote = new NoIdentityQuote(2.56m, 400, PriceSide.Bid, Symbol.AUD_NZD, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, anotherQuote.BidPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, anotherQuote.BidPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, anotherQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 60)),
                Times.Exactly(1));

            //No other external orders were send
            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Exactly(2));


            externalOrdersMocks.Last().Setup(exto => exto.OrderStatus).Returns(IntegratorOrderExternalStatus.Filled);
            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder2.ClientOrderIdentity && deal.FilledAmountBaseAbs == 60 && deal.Price == 2.56m)), Times.Exactly(1));


            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(1));

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
        }

        //test multiple execution reports for one order (this should be gracefully handeled)
        [Test]
        public void SingleSymbolDirectionOrderManager_MarketAndLimitOrdersExtraRejectComes_OrdersWillNotExceedTheirAmounts()
        {
            //  - 2 market orders are regstered
            //  - fully and partially covered by incoming quote
            //  - external order gets rejected
            //  - new incoming price will only satisfy the used-to-be partially filled order

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(
                                                                   () =>
                                                                   integratorOrderExternalMock.Object.OrderStatus ==
                                                                   IntegratorOrderExternalStatus.Filled
                                                                       ? ori.SizeBaseAbsInitial
                                                                       : 0);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true, AllowedFailureLogPortion = "was rejected" };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.SellOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.AUD_USD,
                                                             DealDirection.Sell, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);

            //Let's submit orders

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarket(100, DealDirection.Sell, Symbol.AUD_USD);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 2.38m, DealDirection.Sell, Symbol.AUD_USD);
            IBankPoolClientOrder clientOrder2 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info2);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);
            ssdm.RegisterOrder(clientOrder2, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_USD),
                Times.Exactly(1));

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder2.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.AUD_USD),
                Times.Exactly(1));

            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(2.45m, 140, PriceSide.Bid, Symbol.AUD_USD, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.BidPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.BidPrice);

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 140)),
                Times.Exactly(1));


            // Now reject the first external order and verify that they are put back to lists
            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected, null,
                                                  new RejectionInfo("Rejected", externalOrdersMocks.Last().Object.OrderRequestInfo.SizeBaseAbsInitial, RejectionType.OrderReject)));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Never());

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);


            // Send the reject again and nothing happens

            (testLogger as TestLogger).FailTestOnUnexpectedLogs = false;

            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected, null, new RejectionInfo("Rejected", null, RejectionType.OrderReject)));

            (testLogger as TestLogger).FailTestOnUnexpectedLogs = true;

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Never());

            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);


            NoIdentityQuote anotherQuote = new NoIdentityQuote(2.56m, 400, PriceSide.Bid, Symbol.AUD_USD, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, anotherQuote.BidPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, anotherQuote.BidPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, anotherQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 200)),
                Times.Exactly(1));

            //No other external orders were send
            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Exactly(2));


            externalOrdersMocks.Last().Setup(exto => exto.OrderStatus).Returns(IntegratorOrderExternalStatus.Filled);
            externalOrdersMocks.Last().Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks.Last().Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks.Last().Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.56m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder2.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.56m)), Times.Exactly(1));


            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(2));

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
        }

        [Test]
        public void SingleSymbolDirectionOrderManager_ConsecutioveRejectionsStopStrategy_WillMatchBasedOnStrategy()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true, AllowedFailureLogPortion = "was rejected" };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.EUR_RUB,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);
            ssdm.KeepClosedOrders = true;

            BusinessLayerSettings settings = new BusinessLayerSettings();
            settings.ConsecutiveRejectionsStopStrategyBehavior = new BusinessLayerSettings.ConsecutiveRejectionsStopStrategySettings();
            settings.ConsecutiveRejectionsStopStrategyBehavior.StrategyEnabled = true;
            settings.ConsecutiveRejectionsStopStrategyBehavior.NumerOfRejectionsToTriggerStrategy = 1;
            settings.ConsecutiveRejectionsStopStrategyBehavior.DefaultCounterpartyRank = 10;
            settings.ConsecutiveRejectionsStopStrategyBehavior.CounterpartyRanks =
                new List<BusinessLayerSettings.ConsecutiveRejectionsStopStrategySettings.CounterpartyRank>()
                    {
                        new BusinessLayerSettings.ConsecutiveRejectionsStopStrategySettings.CounterpartyRank()
                            {
                                CounterpartyCode = "MGS",
                                Rank = 4
                            }
                    };
            settings.ConsecutiveRejectionsStopStrategyBehavior.CounterpartyRankWeightMultiplier = 20;
            settings.ConsecutiveRejectionsStopStrategyBehavior.PriceAgeRankWeightMultiplier = 1;
            settings.ConsecutiveRejectionsStopStrategyBehavior.PriceBpRankWeightMultiplier = 20;
            settings.PreventMatchingToSameCounterpartyAfterOrderRejectBehavior = new BusinessLayerSettings.PreventMatchingToSameCounterpartyAfterOrderRejectSettings();
            ssdm.UpdateSettings(settings);

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 3, DealDirection.Buy, Symbol.EUR_RUB);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_RUB),
                Times.Exactly(1));

            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(2.5m, 50, PriceSide.Ask, Symbol.EUR_RUB, Counterparty.NOM,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                         extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),



                Times.Exactly(1));


            NoIdentityQuote bQuote = new NoIdentityQuote(2.4m, 60, PriceSide.Ask, Symbol.EUR_RUB, Counterparty.HSB,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(bQuote.AskPrice);
            priceBookMock.Setup(pb => pb.GetSortedClone()).Returns(new[] { bQuote.AskPrice });


            externalOrdersMocks[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[0].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected,
                                                  null, new RejectionInfo(null, externalOrdersMocks[0].Object.OrderRequestInfo.SizeBaseAbsInitial, RejectionType.OrderReject)));


            priceBookMock.Verify(pb => pb.MaxNodeInternal, Times.Exactly(2));
            priceBookMock.Verify(pb => pb.GetSortedClone(), Times.Once());

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, bQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 60)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Exactly(2));


            //dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
            //    deal =>
            //    deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 && deal.Price == 2.5m)), Times.Exactly(1));

            Assert.AreEqual(40, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsActive);
            Assert.AreEqual(0, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsFilled);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
        }


        [Test]
        public void SingleSymbolDirectionOrderManager_ConsecutioveRejectionsStopStrategy_WillOrderMatchesBasedOnStrategy()
        {
            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(NoIdentityQuote.DummyPrice);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true, AllowedFailureLogPortion = "was rejected" };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.USD_RUB,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);
            ssdm.KeepClosedOrders = true;

            BusinessLayerSettings settings = new BusinessLayerSettings();
            settings.ConsecutiveRejectionsStopStrategyBehavior = new BusinessLayerSettings.ConsecutiveRejectionsStopStrategySettings();
            settings.ConsecutiveRejectionsStopStrategyBehavior.StrategyEnabled = true;
            settings.ConsecutiveRejectionsStopStrategyBehavior.NumerOfRejectionsToTriggerStrategy = 1;
            settings.ConsecutiveRejectionsStopStrategyBehavior.DefaultCounterpartyRank = 10;
            settings.ConsecutiveRejectionsStopStrategyBehavior.CounterpartyRanks =
                new List<BusinessLayerSettings.ConsecutiveRejectionsStopStrategySettings.CounterpartyRank>()
                    {
                        new BusinessLayerSettings.ConsecutiveRejectionsStopStrategySettings.CounterpartyRank()
                            {
                                CounterpartyCode = "MGS",
                                Rank = 4
                            }
                    };
            settings.ConsecutiveRejectionsStopStrategyBehavior.CounterpartyRankWeightMultiplier = 20;
            settings.ConsecutiveRejectionsStopStrategyBehavior.PriceAgeRankWeightMultiplier = 1;
            settings.ConsecutiveRejectionsStopStrategyBehavior.PriceBpRankWeightMultiplier = 0.0020m;
            settings.PreventMatchingToSameCounterpartyAfterOrderRejectBehavior = new BusinessLayerSettings.PreventMatchingToSameCounterpartyAfterOrderRejectSettings();
            ssdm.UpdateSettings(settings);

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarket(100, DealDirection.Buy, Symbol.USD_RUB);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the orders vere successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_RUB),
                Times.Exactly(1));

            // Send price and verify that the matching happened

            NoIdentityQuote aQuote = new NoIdentityQuote(2.5m, 50, PriceSide.Ask, Symbol.USD_RUB, Counterparty.NOM,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, aQuote.AskPrice, DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, aQuote.AskPrice);

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, aQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));

            //Matching happens based on total latency (from counterparty send)
            DateTime receiveTime = DateTime.MinValue;

            PriceObjectInternal bPrice = PriceObjectInternal.GetAllocatedTestingPriceObject(2.4m, 60, PriceSide.Ask, Symbol.USD_RUB, Counterparty.HSB,
                                                         "Subscription1", MarketDataRecordType.BankQuoteData, receiveTime, HighResolutionDateTime.UtcNow.AddMilliseconds(-150));
            PriceObjectInternal cPrice = PriceObjectInternal.GetAllocatedTestingPriceObject(3.4m, 60, PriceSide.Ask, Symbol.USD_RUB, Counterparty.HSB,
                                                         "Subscription1", MarketDataRecordType.BankQuoteData, receiveTime, HighResolutionDateTime.UtcNow.AddMilliseconds(-10));
            PriceObjectInternal dPrice = PriceObjectInternal.GetAllocatedTestingPriceObject(3.4m, 60, PriceSide.Ask, Symbol.USD_RUB, Counterparty.MGS,
                                                         "Subscription1", MarketDataRecordType.BankQuoteData, receiveTime, HighResolutionDateTime.UtcNow.AddMilliseconds(-10));
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(bPrice);
            priceBookMock.Setup(pb => pb.GetSortedClone()).Returns(new[] { bPrice, cPrice, dPrice });

            externalOrdersMocks[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[0].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected,
                                                  null, new RejectionInfo(null, externalOrdersMocks[0].Object.OrderRequestInfo.SizeBaseAbsInitial, RejectionType.OrderReject)));


            priceBookMock.Verify(pb => pb.MaxNodeInternal, Times.Exactly(2));
            priceBookMock.Verify(pb => pb.GetSortedClone(), Times.Once());

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, dPrice) && extord.OrderRequestInfo.SizeBaseAbsInitial == 60)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, cPrice) && extord.OrderRequestInfo.SizeBaseAbsInitial == 40)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Exactly(3));

            //dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
            //    deal =>
            //    deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 && deal.Price == 2.5m)), Times.Exactly(1));

            Assert.AreEqual(0, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsActive);
            Assert.AreEqual(0, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsFilled);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);
        }

        [Test]
        public void SingleSymbolDirectionOrderManager_MultiplePartiallFillsOnOnePriceUntilFullyFilled_WillPerformInternalMatching()
        {
            //
            // SCENARIO: 
            //          - 1 LMT order is in, and is partially satisfied by price A
            //          - 3 Partialfills came - order is correctly updated and finally fully matched
            //          

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    //integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                    //                           .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.USD_SGD,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);


            List<PriceObjectInternal> priceBook = new List<PriceObjectInternal>
                {
                    new NoIdentityQuote(2.5m, 100, PriceSide.Ask, Symbol.USD_SGD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.6m, 50, PriceSide.Ask, Symbol.USD_SGD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.7m, 100, PriceSide.Ask, Symbol.USD_SGD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal
                };
            //price book copy in order to be able to assert
            List<PriceObjectInternal> priceBookCopy = new List<PriceObjectInternal>(priceBook);

            priceBookMock.Setup(pb => pb.RemoveIdentical(It.IsAny<PriceObjectInternal>()))
                         .Returns((PriceObjectInternal price) => priceBook.Remove(price));

            //Beware! we need to set the mock with method (priceBook.ToArray) not the value (priceBook.ToArray())
            // so that we don't get the old snapshot
            priceBookMock.Setup(pb => pb.GetSortedClone()).Returns(priceBook.ToArray);

            // Send price
            priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, priceBook[0]);
            //this needs to be lambda!!
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(() => priceBook.Count > 0 ? priceBook[0] : null);



            //Let's submit order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 2.65m, DealDirection.Buy, Symbol.USD_SGD);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the order was successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_SGD),
                Times.Exactly(1));


            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[0]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            //verify that just a single order was generated
            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Exactly(1));

            //Two prices still remains in book
            Assert.AreEqual(2, priceBook.Count);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            // 1st Partialfill cames for A
            externalOrdersMocks[0].Setup(o => o.HasRemainingAmount).Returns(true);
            externalOrdersMocks[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[0].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.PartiallyFilled,
                                                  new ExecutionInfo(
                                                      20,
                                                      2.5m,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            //order still remainsto be matched
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 20 && deal.Price == 2.5m)), Times.Exactly(1));


            // 2nd Partialfill cames for A
            externalOrdersMocks[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[0].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.PartiallyFilled,
                                                  new ExecutionInfo(
                                                      70,
                                                      2.4m,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            //order still remainsto be matched
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 70 && deal.Price == 2.4m)), Times.Exactly(1));



            // last Partialfill cames for A
            externalOrdersMocks[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[0].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.PartiallyFilled,
                                                  new ExecutionInfo(
                                                      10,
                                                      2.5m,
                                                      null, null, null, DealDirection.Buy),
                                                      null));


            //order is done
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 10 && deal.Price == 2.5m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorClientOrderUpdate(It.Is<BankPoolClientOrderUpdateInfo>(
                update =>
                update.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && update.OrderStatus == ClientOrderStatus.NotActiveInIntegrator), Symbol.USD_SGD), Times.Exactly(1));


            //Only 3 total deals came in
            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(3));

            //Total number of 1 external order was sent
            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Exactly(1));

            //And 2 total updates (opened, not active)
            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorClientOrderUpdate(It.IsAny<BankPoolClientOrderUpdateInfo>(), Symbol.USD_SGD), Times.Exactly(2));
        }

        [Test]
        public void SingleSymbolDirectionOrderManager_MultiplePartiallFillsAndRejectsOnMultiplePrices_WillPerformInternalMatching()
        {
            //
            // SCENARIO: 
            //          - 1 MKT order is in, and is partially satisfied by prices A, B and C
            //          - 1st Partialfill cames for C, full reject for A, 1st patiall fill for B
            //          - 2nd partiall fill for B, reject for C

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    //integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                    //                           .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.USD_INR,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);


            List<PriceObjectInternal> priceBook = new List<PriceObjectInternal>
                {
                    new NoIdentityQuote(2.5m, 100, PriceSide.Ask, Symbol.USD_INR, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.6m, 50, PriceSide.Ask, Symbol.USD_INR, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.7m, 100, PriceSide.Ask, Symbol.USD_INR, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal
                };
            //price book copy in order to be able to assert
            List<PriceObjectInternal> priceBookCopy = new List<PriceObjectInternal>(priceBook);

            priceBookMock.Setup(pb => pb.RemoveIdentical(It.IsAny<PriceObjectInternal>()))
                         .Returns((PriceObjectInternal price) => priceBook.Remove(price));

            //Beware! we need to set the mock with method (priceBook.ToArray) not the value (priceBook.ToArray())
            // so that we don't get the old snapshot
            priceBookMock.Setup(pb => pb.GetSortedClone()).Returns(priceBook.ToArray);

            // Send price
            priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, priceBook[0]);
            //this needs to be lambda!!
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(() => priceBook.Count > 0 ? priceBook[0] : NoIdentityQuote.DummyPrice);



            //Let's submit order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarket(200, DealDirection.Buy, Symbol.USD_INR);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the order was successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_INR),
                Times.Exactly(1));


            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[0]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[1]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[2]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));

            //One price still remains in book
            Assert.AreEqual(1, priceBook.Count);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            // 1st Partialfill cames for C
            externalOrdersMocks[2].Setup(o => o.HasRemainingAmount).Returns(true);
            externalOrdersMocks[2].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[2].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.PartiallyFilled,
                                                  new ExecutionInfo(
                                                      20,
                                                      externalOrdersMocks[2].Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            //order still remainsto be matched
            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 20 && deal.Price == 2.7m)), Times.Exactly(1));


            // full reject for A
            externalOrdersMocks[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[0].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected,
                                                  null, new RejectionInfo(null, 100, RejectionType.OrderReject)));

            //order now has remaing amount to be filled
            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            //rejection happened
            dispatchGatewayMock.Verify(gateway => gateway.OnCounterpartyRejectedOrder(It.Is<CounterpartyRejectionInfo>(
                reject =>
                reject.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && reject.RejectedAmountBaseAbs == 100)), Times.Exactly(1));

            //And rematching happened
            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[2]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(2));

            //Emptying the pricebook
            Assert.AreEqual(0, priceBook.Count);


            //1st patiall fill for B
            externalOrdersMocks[1].Setup(o => o.HasRemainingAmount).Returns(true);
            externalOrdersMocks[1].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[1].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.PartiallyFilled,
                                                  new ExecutionInfo(
                                                      30,
                                                      externalOrdersMocks[1].Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            //order still remainsto be matched
            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
            Assert.AreEqual(50, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsActive);

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 30 && deal.Price == 2.6m)), Times.Exactly(1));


            //2nd partiall fill for B
            externalOrdersMocks[1].Setup(o => o.HasRemainingAmount).Returns(false);
            externalOrdersMocks[1].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[1].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.PartiallyFilled,
                                                  new ExecutionInfo(
                                                      20,
                                                      externalOrdersMocks[1].Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            //order still remainsto be matched
            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
            Assert.AreEqual(50, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsActive);

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 20 && deal.Price == 2.6m)), Times.Exactly(1));

            //Reject for C
            externalOrdersMocks[2].Setup(o => o.HasRemainingAmount).Returns(false);
            externalOrdersMocks[2].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[2].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected,
                                                  null, new RejectionInfo(null, 30, RejectionType.OrderReject)));

            //order still remains to be matched
            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
            Assert.AreEqual(80, ssdm.GetOrder(clientOrder1.ClientOrderIdentity).SizeBaseAbsActive);

            dispatchGatewayMock.Verify(gateway => gateway.OnCounterpartyRejectedOrder(It.Is<CounterpartyRejectionInfo>(
                reject =>
                reject.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && reject.RejectedAmountBaseAbs == 30)), Times.Exactly(1));


            //any other deal happened (we need to be carefull about 0 size deals)
            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.IsAny<IntegratorDealInternal>()), Times.Exactly(3));

            //Total number of 4 external orders were sent
            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Exactly(4));

            //And 1 total update (opened)
            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorClientOrderUpdate(It.IsAny<BankPoolClientOrderUpdateInfo>(), Symbol.USD_INR), Times.Exactly(1));
        }

        [Test]
        public void SingleSymbolDirectionOrderManager_MultiplePartiallFillsAndOnePartialRejectOnOnePriceUntilFullyFilled_WillPerformInternalMatching()
        {
            //
            // SCENARIO: 
            //          - 1 LMT order is in, and is partially satisfied by price A
            //          - 1st Partiall fill, 1 partiall reject - leaving remeining quantity, rematching happens
            //          - 2nd Partialfill came - order is correctly updated
            //          - Full fill cames for rematched order - client order is correctly closed
            //

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    //integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                    //                           .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.SellOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.GBP_HKD,
                                                             DealDirection.Sell, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);


            List<PriceObjectInternal> priceBook = new List<PriceObjectInternal>
                {
                    new NoIdentityQuote(2.7m, 100, PriceSide.Bid, Symbol.GBP_HKD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.6m, 50, PriceSide.Bid, Symbol.GBP_HKD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.5m, 100, PriceSide.Bid, Symbol.GBP_HKD, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice as PriceObjectInternal
                };
            //price book copy in order to be able to assert
            List<PriceObjectInternal> priceBookCopy = new List<PriceObjectInternal>(priceBook);

            priceBookMock.Setup(pb => pb.RemoveIdentical(It.IsAny<PriceObjectInternal>()))
                         .Returns((PriceObjectInternal price) => priceBook.Remove(price));

            //Beware! we need to set the mock with method (priceBook.ToArray) not the value (priceBook.ToArray())
            // so that we don't get the old snapshot
            priceBookMock.Setup(pb => pb.GetSortedClone()).Returns(priceBook.ToArray);

            // Send price
            priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, priceBook[0]);
            //this needs to be lambda!!
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(() => priceBook.Count > 0 ? priceBook[0] : null);



            //Let's submit order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(100, 2.55m, DealDirection.Sell, Symbol.GBP_HKD);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the order was successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.GBP_HKD),
                Times.Exactly(1));


            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[0]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            //verify that just a single order was generated
            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Exactly(1));

            //Two prices still remains in book
            Assert.AreEqual(2, priceBook.Count);

            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            // 1st Partialfill cames for A
            externalOrdersMocks[0].Setup(o => o.HasRemainingAmount).Returns(true);
            externalOrdersMocks[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[0].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.PartiallyFilled,
                                                  new ExecutionInfo(
                                                      20,
                                                      2.7m,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            //order still remains to be matched
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 20 && deal.Price == 2.7m)), Times.Exactly(1));


            // 1 partiall reject for A
            externalOrdersMocks[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[0].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected,
                                                  null, new RejectionInfo("Partially rejected", 50, RejectionType.OrderReject)));



            //order was rematched, so there is pending amount, but it's already in matching
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            //rejection happened
            dispatchGatewayMock.Verify(gateway => gateway.OnCounterpartyRejectedOrder(It.Is<CounterpartyRejectionInfo>(
                reject =>
                reject.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && reject.RejectedAmountBaseAbs == 50)), Times.Exactly(1));

            //And rematching happened
            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[1]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));

            //Leaving one last price in price book
            Assert.AreEqual(1, priceBook.Count);


            // last Partialfill cames for A
            externalOrdersMocks[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[0].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.PartiallyFilled,
                                                  new ExecutionInfo(
                                                      30,
                                                      2.7m,
                                                      null, null, null, DealDirection.Buy),
                                                      null));


            //order is still pending though (with last portion of order being in matching)
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 30 && deal.Price == 2.7m)), Times.Exactly(1));



            //Rematched order is fully filled
            externalOrdersMocks[1].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[1].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      50,
                                                      2.8m,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            //order is done
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 && deal.Price == 2.8m)), Times.Exactly(1));


            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorClientOrderUpdate(It.Is<BankPoolClientOrderUpdateInfo>(
                update =>
                update.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && update.OrderStatus == ClientOrderStatus.NotActiveInIntegrator), Symbol.GBP_HKD), Times.Exactly(1));


            //Only 3 total deals came in
            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(
                It.IsAny<IntegratorDealInternal>()), Times.Exactly(3));

            //Total number of 2 external orders were sent
            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Exactly(2));

            //And 2 total updates (opened, not active)
            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorClientOrderUpdate(It.IsAny<BankPoolClientOrderUpdateInfo>(), Symbol.GBP_HKD), Times.Exactly(2));
        }


        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterMarketOnImprovemntOrder_WillNotBeSatisfiedByExistingBestPriceWillMatchOnLaterImprovemnt()
        {
            //
            // SCENARIO: market on improvement order comes in - it doesn't consume the Price book top 
            //    it will wait for improvement

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.CHF_MXN,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);


            List<PriceObjectInternal> priceBook = new List<PriceObjectInternal>
                {
                    new NoIdentityQuote(2.5m, 150, PriceSide.Ask, Symbol.CHF_MXN, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.6m, 50, PriceSide.Ask, Symbol.CHF_MXN, Counterparty.DBK,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(20.7m, 100, PriceSide.Ask, Symbol.CHF_MXN, Counterparty.L01,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal
                };
            //price book copy in order to be able to assert
            List<PriceObjectInternal> priceBookCopy = new List<PriceObjectInternal>(priceBook);

            priceBookMock.Setup(pb => pb.RemoveIdentical(It.IsAny<PriceObjectInternal>()))
                         .Returns((PriceObjectInternal price) => priceBook.Remove(price));

            //Beware! we need to set the mock with method (priceBook.ToArray) not the value (priceBook.ToArray())
            // so that we don't get the old snapshot
            priceBookMock.Setup(pb => pb.GetSortedClone()).Returns(priceBook.ToArray);

            // Send price
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, priceBook[0], DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, priceBook[0]);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(priceBook[0]);



            //Let's submit order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarketOnImprovement(400, TimeSpan.FromMilliseconds(10000), DealDirection.Buy, Symbol.CHF_MXN);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the order was successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.CHF_MXN),
                Times.Exactly(1));


            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Never());

            Assert.AreEqual(1, ssdm.ActiveMarketIfImprovmentOrders);
            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);


            // Send new price
            NoIdentityQuote anotherQuote = new NoIdentityQuote(2.45m, 100, PriceSide.Ask, Symbol.CHF_MXN, Counterparty.CTI,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);

            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, anotherQuote.AskPrice, DateTime.UtcNow);

            //Nothing happened yet
            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Never());

            Assert.AreEqual(1, ssdm.ActiveMarketIfImprovmentOrders);
            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);

            //this will cause matching
            priceBookMock.Setup(pb => pb.IsImprovement).Returns(true);
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, anotherQuote.AskPrice, DateTime.UtcNow);
            priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, anotherQuote.AskPrice);
            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, anotherQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));


            NoIdentityQuote secondQuote = new NoIdentityQuote(2.45m, 500, PriceSide.Ask, Symbol.CHF_MXN, Counterparty.CRS,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, secondQuote.AskPrice, DateTime.UtcNow);
            priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, secondQuote.AskPrice);
            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, secondQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 300)),
                Times.Exactly(1));


            Assert.AreEqual(0, ssdm.ActiveMarketIfImprovmentOrders);
            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);

            //Now send deterioration - this should prevent rematching
            priceBookMock.Setup(pb => pb.IsImprovement).Returns(false);
            priceBookMock.Raise(pb => pb.ChangeableBookTopDeteriorated += null, secondQuote.AskPrice);

            externalOrdersMocks[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[0].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected,
                                                  null, new RejectionInfo("Rejected", externalOrdersMocks[0].Object.OrderRequestInfo.SizeBaseAbsInitial, RejectionType.OrderReject)));

            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Exactly(2));

            Assert.AreEqual(1, ssdm.ActiveMarketIfImprovmentOrders);
            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);

            //Now send improvement - this will cause rematch and will enable rematch on next reject
            NoIdentityQuote thirdQuote = new NoIdentityQuote(2.35m, 500, PriceSide.Ask, Symbol.CHF_MXN, Counterparty.MGS,
                                                         "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(thirdQuote.AskPrice);
            priceBookMock.Setup(pb => pb.IsImprovement).Returns(true);
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, thirdQuote.AskPrice, DateTime.UtcNow);
            priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, thirdQuote.AskPrice);
            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, thirdQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            Assert.AreEqual(0, ssdm.ActiveMarketIfImprovmentOrders);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            externalOrdersMocks[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[1].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected,
                                                  null, new RejectionInfo("Rejected", externalOrdersMocks[1].Object.OrderRequestInfo.SizeBaseAbsInitial, RejectionType.OrderReject)));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromQuote(extord.OrderRequestInfo, thirdQuote) && extord.OrderRequestInfo.SizeBaseAbsInitial == 300)),
                Times.Exactly(1));

            Assert.AreEqual(0, ssdm.ActiveMarketIfImprovmentOrders);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            //No deals yet

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.IsAny<IntegratorDealInternal>()), Times.Never());

            externalOrdersMocks[3].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[3].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks[3].Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks[3].Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            //This deal was executed immediately after rejection - it was liq. taker
            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 300 && deal.Price == 2.35m && deal.MarketOnImprovementResult == MarketOnImprovementResult.ExecutedOnLaterImprovement && deal.FlowSide == FlowSide.LiquidityTaker)), Times.Exactly(1));


            Assert.AreEqual(0, ssdm.ActiveMarketIfImprovmentOrders);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);

            externalOrdersMocks[2].Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMocks[2].Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMocks[2].Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMocks[2].Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));

            //this deal waited for next price - it was maker
            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.35m && deal.MarketOnImprovementResult == MarketOnImprovementResult.ExecutedOnLaterImprovement && deal.FlowSide == FlowSide.LiquidityMaker)), Times.Exactly(1));


            Assert.AreEqual(0, ssdm.ActiveMarketIfImprovmentOrders);
            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);

            //No more deals
            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.IsAny<IntegratorDealInternal>()), Times.Exactly(2));
            //No more Orders
            orderFlowSessionMock.Verify(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Exactly(4));
        }

        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterMarketOnImprovemntOrderWithZeroTiemout_WillBeImmediatelySatisfied()
        {
            //
            // SCENARIO: market order comes in - it immediately consumes the Price book top and then rest of the pricebook
            //    if it was not fully satisfied yet. Lastly it will be registered and wait for any incoming quotes.

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.USD_DKK,
                                                             DealDirection.Buy, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);


            List<PriceObjectInternal> priceBook = new List<PriceObjectInternal>
                {
                    new NoIdentityQuote(2.5m, 100, PriceSide.Ask, Symbol.USD_DKK, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.6m, 50, PriceSide.Ask, Symbol.USD_DKK, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal,
                    new NoIdentityQuote(20.7m, 100, PriceSide.Ask, Symbol.USD_DKK, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice as PriceObjectInternal
                };
            //price book copy in order to be able to assert
            List<PriceObjectInternal> priceBookCopy = new List<PriceObjectInternal>(priceBook);

            priceBookMock.Setup(pb => pb.RemoveIdentical(It.IsAny<PriceObjectInternal>()))
                         .Returns((PriceObjectInternal price) => priceBook.Remove(price));

            //Beware! we need to set the mock with method (priceBook.ToArray) not the value (priceBook.ToArray())
            // so that we don't get the old snapshot
            priceBookMock.Setup(pb => pb.GetSortedClone()).Returns(priceBook.ToArray);

            // Send price
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, priceBook[0], DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, priceBook[0]);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(priceBook[0]);



            //Let's submit order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarketOnImprovement(400, TimeSpan.FromMilliseconds(0), DealDirection.Buy, Symbol.USD_DKK);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the order was successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_DKK),
                Times.Exactly(1));


            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[0]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[1]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 50)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[2]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 100)),
                Times.Exactly(1));

            //One market order is still registered
            Assert.AreEqual(1, ssdm.ActiveMarketOrders);
            Assert.AreEqual(0, ssdm.InactivePendingOrdersCount);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.ActiveMarketIfImprovmentOrders);


            foreach (Mock<IIntegratorOrderExternal> externalOrdersMock in externalOrdersMocks)
            {
                externalOrdersMock.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMock.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMock.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMock.Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));
            }

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 2.5m && deal.MarketOnImprovementResult == MarketOnImprovementResult.ExecutedOnTimeout && deal.FlowSide == FlowSide.LiquidityTaker)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 && deal.Price == 2.6m && deal.MarketOnImprovementResult == MarketOnImprovementResult.ExecutedOnTimeout && deal.FlowSide == FlowSide.LiquidityTaker)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 100 && deal.Price == 20.7m && deal.MarketOnImprovementResult == MarketOnImprovementResult.ExecutedOnTimeout && deal.FlowSide == FlowSide.LiquidityTaker)), Times.Exactly(1));

        }


        [Test]
        public void SingleSymbolDirectionOrderManager_RegisterMarketWithSmallGranularity_WillBeImmediatelySatisfiedInSupportedChunks()
        {
            //
            // SCENARIO: market order comes in - it immediately consumes the Price book top and then rest of the pricebook
            //    if it was not fully satisfied yet. Lastly it will be registered and wait for any incoming quotes.

            var priceBookMock = new Mock<IChangeablePriceBook<PriceObjectInternal, Counterparty>>();
            var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
            var orderFlowSessionMock = new Mock<IOrderFlowSession>();
            List<Mock<IIntegratorOrderExternal>> externalOrdersMocks = new List<Mock<IIntegratorOrderExternal>>();
            var dispatchGatewayMock = new Mock<ITakerUnicastInfoForwarder>();
            orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), It.IsAny<Counterparty>(), out _dummyErrorParameter))
                                .Returns((OrderRequestInfo ori, Counterparty ctp, string dummyErrorParameter) =>
                                {
                                    var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
                                    externalOrdersMocks.Add(integratorOrderExternalMock);
                                    integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
                                                               .Returns(ori);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Counterparty)
                                                               .Returns(Counterparty.L19);
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
                                                               .Returns("extOrd1");
                                    integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
                                                               .Returns(ori.SizeBaseAbsInitial);
                                    return integratorOrderExternalMock.Object;
                                });
            orderFlowSessionMock.Setup(ofs => ofs.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(SubmissionResult.Success);
            orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);
            orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);
            orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);

            ILogger testLogger = new TestLogger(Assert.Fail) { FailTestOnUnexpectedLogs = true };

            var ssdm = new SingleSymbolDirectionOrderManager(OrderEvaluator.SellOrderEvaluator, priceBookMock.Object,
                                                             orderFlowSessionsListMock.Object, Symbol.USD_DKK,
                                                             DealDirection.Sell, testLogger, _nullRiskManager, MarketableClientOrdersMatchingStrategy.ImmediateMatching);


            List<PriceObjectInternal> priceBook = new List<PriceObjectInternal>
                {
                    new NoIdentityQuote(2.9m, 200000, PriceSide.Bid, Symbol.USD_DKK, Counterparty.L01,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.6m, 300000, PriceSide.Bid, Symbol.USD_DKK, Counterparty.LM2,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.2m, 500, PriceSide.Bid, Symbol.USD_DKK, Counterparty.HSB,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice as PriceObjectInternal,
                    new NoIdentityQuote(2.1m, 10000, PriceSide.Bid, Symbol.USD_DKK, Counterparty.CRS,
                                        "Subscription1", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice as PriceObjectInternal
                };
            //price book copy in order to be able to assert
            List<PriceObjectInternal> priceBookCopy = new List<PriceObjectInternal>(priceBook);

            priceBookMock.Setup(pb => pb.RemoveIdentical(It.IsAny<PriceObjectInternal>()))
                         .Returns((PriceObjectInternal price) => priceBook.Remove(price));

            //Beware! we need to set the mock with method (priceBook.ToArray) not the value (priceBook.ToArray())
            // so that we don't get the old snapshot
            priceBookMock.Setup(pb => pb.GetSortedClone()).Returns(priceBook.ToArray);

            // Send price
            priceBookMock.Raise(pb => pb.NewNodeArrived += null, priceBookMock.Object, priceBook[0], DateTime.UtcNow);
            //priceBookMock.Raise(pb => pb.ChangeableBookTopImproved += null, priceBook[0]);
            priceBookMock.Setup(pb => pb.MaxNodeInternal).Returns(priceBook[0]);



            //Let's submit order

            BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateMarket(19825.1m, DealDirection.Sell, Symbol.USD_DKK);
            IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

            ssdm.RegisterOrder(clientOrder1, dispatchGatewayMock.Object);

            //Verify that the order was successfuly accepted

            dispatchGatewayMock.Verify(
                gateway =>
                gateway.OnIntegratorClientOrderUpdate(
                    It.Is<ClientOrderUpdateInfo>(
                        info =>
                        info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
                        info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.USD_DKK),
                Times.Exactly(1));


            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[0]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 19000)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[2]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 500)),
                Times.Exactly(1));

            orderFlowSessionMock.Verify(
                ofs =>
                ofs.SubmitOrder(
                    It.Is<IIntegratorOrderExternal>(
                        extord => IsOrderCreatedFromPrice(extord.OrderRequestInfo, priceBookCopy[3]) && extord.OrderRequestInfo.SizeBaseAbsInitial == 325.1m)),
                Times.Exactly(1));

            //no order is still registered
            Assert.AreEqual(0, ssdm.ActiveMarketOrders);
            Assert.AreEqual(1, ssdm.InactivePendingOrdersCount);
            Assert.AreEqual(0, ssdm.ActiveLimitOrdersCount);
            Assert.AreEqual(0, ssdm.ActiveMarketIfImprovmentOrders);
            Assert.AreEqual(1, ssdm.ActiveOrdersCount);


            foreach (Mock<IIntegratorOrderExternal> externalOrdersMock in externalOrdersMocks)
            {
                externalOrdersMock.Raise(ordeExt => ordeExt.OnOrderChanged += null,
                                              externalOrdersMock.Object,
                                              new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
                                                  new ExecutionInfo(
                                                      externalOrdersMock.Object.OrderRequestInfo.SizeBaseAbsInitial,
                                                      externalOrdersMock.Object.OrderRequestInfo.RequestedPrice,
                                                      null, null, null, DealDirection.Buy),
                                                      null));
            }

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 19000 && deal.Price == 2.9m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 500 && deal.Price == 2.2m)), Times.Exactly(1));

            dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
                deal =>
                deal.ClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 325.1m && deal.Price == 2.1m)), Times.Exactly(1));

            Assert.AreEqual(0, ssdm.ActiveOrdersCount);
        }

    }
}
