﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using NUnit.Framework;

namespace Kreslik.Integrator.BusinessLayer.Tests
{
    [TestFixture]
    public class OrderBookTests
    {

        private List<object> _topOfBookDeclinedArgs;
        private List<object> _topOfBookImproovedArgs;
        private List<object> _topOfBookReplacedArgs;
        private List<IEnumerable<object>> _purgedArgs;

        private void OnTopOfBookDeclined<T>(IBookTop<T> sender, T arg, DateTime timestamp)
        {
            _topOfBookDeclinedArgs.Add(arg);
        }

        private void OnTopOfBookImprooved<T>(IBookTop<T> sender, T arg, DateTime timestamp)
        {
            _topOfBookImproovedArgs.Add(arg);
        }

        private void OnTopOfBookReplaced<T>(T arg, DateTime timestamp)
        {
            _topOfBookReplacedArgs.Add(arg);
        }

        private void OnItemsPurged<T>(IEnumerable<T> arg)
        {
            _purgedArgs.Add(arg.Cast<object>());
        }

        [SetUp]
        public void SetUp()
        {
            _topOfBookDeclinedArgs = new List<object>();
            _topOfBookImproovedArgs = new List<object>();
            _topOfBookReplacedArgs = new List<object>();
            _purgedArgs = new List<IEnumerable<object>>();
        }

        public class intclass : IPooledObject, IComparable
        {
            public int Value { get; set; }

            public intclass(int value)
            {
                this.Value = value;
            }

            public static implicit operator intclass(int value)
            {
                return new intclass(value);
            }

            public static implicit operator int(intclass value)
            {
                return value.Value;
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                {
                    return false;
                }

                if (obj is intclass)
                {
                    return (obj as intclass).Value == this.Value;
                }

                if (obj is int)
                {
                    return (int)obj == this.Value;
                }

                return false;
            }


            public void Acquire()
            {
                //
            }

            public void Release()
            {
                //
            }

            public bool AcquireIfNotReleased()
            {
                return true;
            }

            public int CompareTo(object obj)
            {
                return this.Value.CompareTo((obj as intclass).Value);
            }
        }

        [Test]
        public void OrderBook_RemoveAt_ReturnsFalse()
        {
            OrderBook<intclass> orderBook = new OrderBook<intclass>(10, Comparer<intclass>.Default, Comparer<intclass>.Default,
                                                              EqualityComparer<intclass>.Default, 0, TradingTargetType.Hotspot, new TestLogger(Assert.Fail, LogLevel.Info));

            orderBook.BookTopImproved += OnTopOfBookImprooved<intclass>;
            orderBook.BookTopDeterioratedInternal += OnTopOfBookDeclined;
            orderBook.NodesPurgedAway += OnItemsPurged;
            Assert.False(orderBook.RemoveIdentical(0));
            Assert.False(orderBook.RemoveIdentical(2));
            Assert.AreEqual(0, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(0, _topOfBookImproovedArgs.Count);
            Assert.AreEqual(0, _purgedArgs.Count);
        }

        [Test]
        public void OrderBook_Insert_PurgeInvokedAfterBookIsFull()
        {
            OrderBook<intclass> orderBook = new OrderBook<intclass>(5, Comparer<intclass>.Default, Comparer<intclass>.Default,
                                                              EqualityComparer<intclass>.Default, 0, TradingTargetType.Hotspot, new TestLogger(Assert.Fail, LogLevel.Info));
            orderBook.ItemsToRemoveDuringOverflowPurging = 2;

            orderBook.BookTopImproved += OnTopOfBookImprooved<intclass>;
            orderBook.BookTopDeterioratedInternal += OnTopOfBookDeclined;
            orderBook.NodesPurgedAway += OnItemsPurged;

            orderBook.AddNew(5);
            orderBook.AddNew(2);
            orderBook.AddNew(15);
            orderBook.AddNew(6);

            Assert.AreEqual(orderBook.GetSortedClone().Length, 4);
            Assert.AreEqual(0, _purgedArgs.Count);

            orderBook.AddNew(7);

            Assert.AreEqual(orderBook.GetSortedClone().Length, 3);
            Assert.AreEqual(1, _purgedArgs.Count);
        }
    }
}
