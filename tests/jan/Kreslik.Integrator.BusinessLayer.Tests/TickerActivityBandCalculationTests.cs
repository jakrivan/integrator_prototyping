﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.BusinessLayer.Systems;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.FIXMessaging;
using Moq;
using NUnit.Framework;

namespace Kreslik.Integrator.BusinessLayer.Tests
{
    //internal class LmaxTickerActivityStatsProviderUnderTest : LmaxTickerActivityStatsProvider
    //{
    //    public LmaxTickerActivityStatsProviderUnderTest(ITickerProvider lmaxTickerProvider,
    //        ISafeTimerFactory safeTimerFactory, IUsdConversionProvider usdConversionProvider, Symbol observeSymbol)
    //        : base(lmaxTickerProvider, safeTimerFactory, usdConversionProvider, observeSymbol)
    //    {
    //    }

    //    public void SetUtcNow(DateTime now)
    //    {
    //        _utcNow = now;
    //    }

    //    private DateTime _utcNow;

    //    protected override DateTime UtcNowInternal
    //    {
    //        get { return _utcNow; }
    //    }
    //}

    [TestFixture]
    public class TickerActivityBandCalculationTests
    {
        private class SafeTimerFactoryMock : ISafeTimer, ISafeTimerFactory
        {
            public Action Callback { get; private set; }
            public bool Activated { get; private set; }

            public ISafeTimer CreateSafeTimer(Action callback, TimeSpan dueTime, TimeSpan period)
            {
                this.Callback = callback;
                this.Activated = true;
                return this;
            }

            public ISafeTimer CreateSafeTimer(Action callback, TimeSpan dueTime, TimeSpan period, bool rescheduleAfterCallbackDone)
            {
                this.Callback = callback;
                this.Activated = true;
                return this;
            }

            public ISafeTimer CreateSafeTimer(Action callback)
            {
                this.Callback = callback;
                this.Activated = false;
                return this;
            }

            public ISafeTimer CreateSafeTimer(Action callback, TimeSpan period, bool rescheduleAfterCallbackDone)
            {
                this.Callback = callback;
                this.Activated = false;
                return this;
            }

            public void Change(TimeSpan dueTime, TimeSpan period)
            {
                //throw new NotImplementedException();
            }


            public void ExecuteSynchronouslyWithReschedulingIfRequested()
            {
                this.Callback();
                this.Activated = true;
            }
        }

        private static VenueDealObjectInternal GetNewTicker(decimal sizeBaseAbs, DateTime integratorReceivedTime)
        {
            var ticker = VenueDealObjectInternal.GetNewUninitializedObject();
            ticker.OverrideContent(Symbol.AUD_CAD, Counterparty.PX1, null, 0m, sizeBaseAbs, null, DateTime.MinValue,
                integratorReceivedTime, null);
            return ticker;
        }

        [SetUp]
        public void TestSetup()
        {
            LmaxTickerActivityMonitor.TestOnly__ClearInstanceCache();
        }


        [Test]
        public void TickerMonitor_CtorTest_Succeeds()
        {
            var tickerProvider = new Mock<ITickerProvider>();
            SafeTimerFactoryMock safeTimerMock = new SafeTimerFactoryMock();
            var usdConversionsProvider = new Mock<IUsdConversionProvider>();
            usdConversionsProvider.Setup(cp => cp.GetCurrentUsdConversionMultiplier()).Returns(2m);
            LmaxTickerActivityMonitor monitor =
                LmaxTickerActivityMonitor.GetNewLmaxTickerActivityMonitor(tickerProvider.Object, safeTimerMock,
                    usdConversionsProvider.Object, Symbol.AUD_CAD);
            LmaxTickerActivityMonitorWrapper monitorWrapper1 = new LmaxTickerActivityMonitorWrapper(monitor, TimeSpan.FromSeconds(15));
            LmaxTickerActivityMonitorWrapper monitorWrapper2 = new LmaxTickerActivityMonitorWrapper(monitor, TimeSpan.FromSeconds(5));
        }

        [Test]
        public void TickerActivity_FewTickesrNoTimerEvent_StatsAreZero()
        {
            int newActivityStatsCallCnt1 = 0;
            int newActivityStatsCallCnt2 = 0;

            var tickerProvider = new Mock<ITickerProvider>();
            SafeTimerFactoryMock safeTimerMock = new SafeTimerFactoryMock();
            var usdConversionsProvider = new Mock<IUsdConversionProvider>();
            usdConversionsProvider.Setup(cp => cp.GetCurrentUsdConversionMultiplier()).Returns(2m);
            LmaxTickerActivityMonitor monitor =
                LmaxTickerActivityMonitor.GetNewLmaxTickerActivityMonitor(tickerProvider.Object, safeTimerMock,
                    usdConversionsProvider.Object, Symbol.AUD_CAD);
            LmaxTickerActivityMonitorWrapper monitorWrapper1 = new LmaxTickerActivityMonitorWrapper(monitor, TimeSpan.FromSeconds(15));
            LmaxTickerActivityMonitorWrapper monitorWrapper2 = new LmaxTickerActivityMonitorWrapper(monitor, TimeSpan.FromSeconds(5));

            monitorWrapper1.NewActivityStats += stats => { newActivityStatsCallCnt1++; };
            monitorWrapper2.NewActivityStats += stats => { newActivityStatsCallCnt2++; };

            tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(1000, DateTime.UtcNow));
            tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(5, DateTime.UtcNow));
            tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(123456789, DateTime.UtcNow));

            Assert.AreEqual(0, newActivityStatsCallCnt1);
            Assert.AreEqual(0, newActivityStatsCallCnt2);
            Assert.AreEqual(0m, monitorWrapper1.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(0m, monitorWrapper1.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(false, monitorWrapper1.CurrentStats.HadEnoughData);
            Assert.AreEqual(0m, monitorWrapper2.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(0m, monitorWrapper2.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(false, monitorWrapper2.CurrentStats.HadEnoughData);
        }

        [Test]
        public void TickerActivity_FewTickersSingleTimerEvent_StatsAreNonZeroCountsOnlyHistoricChunks()
        {
            int newActivityStatsCallCnt1 = 0;
            int newActivityStatsCallCnt2 = 0;

            var tickerProvider = new Mock<ITickerProvider>();
            SafeTimerFactoryMock safeTimerMock = new SafeTimerFactoryMock();
            var usdConversionsProvider = new Mock<IUsdConversionProvider>();
            usdConversionsProvider.Setup(cp => cp.GetCurrentUsdConversionMultiplier()).Returns(2m);
            LmaxTickerActivityMonitor monitor =
                LmaxTickerActivityMonitor.GetNewLmaxTickerActivityMonitor(tickerProvider.Object, safeTimerMock,
                    usdConversionsProvider.Object, Symbol.AUD_CAD);
            LmaxTickerActivityMonitorWrapper monitorWrapper1 = new LmaxTickerActivityMonitorWrapper(monitor, TimeSpan.FromSeconds(15));
            LmaxTickerActivityMonitorWrapper monitorWrapper2 = new LmaxTickerActivityMonitorWrapper(monitor, TimeSpan.FromSeconds(5));

            monitorWrapper1.NewActivityStats += stats => { newActivityStatsCallCnt1++; };
            monitorWrapper2.NewActivityStats += stats => { newActivityStatsCallCnt2++; };


            tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(1000, DateTime.UtcNow));
            tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(5, DateTime.UtcNow));
            safeTimerMock.Callback();

            Assert.AreEqual(1, newActivityStatsCallCnt1);
            Assert.AreEqual(1, newActivityStatsCallCnt2);
            //usd multiplier is set to 2
            Assert.AreEqual(2010m, monitorWrapper1.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(1005m, monitorWrapper1.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(false, monitorWrapper1.CurrentStats.HadEnoughData);
            Assert.AreEqual(2010m, monitorWrapper2.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(1005m, monitorWrapper2.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(true, monitorWrapper2.CurrentStats.HadEnoughData);

            //this will not be already counted towards stats
            tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(123456789, DateTime.UtcNow));

            Assert.AreEqual(1, newActivityStatsCallCnt1);
            Assert.AreEqual(1, newActivityStatsCallCnt2);
            Assert.AreEqual(2010m, monitorWrapper1.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(1005m, monitorWrapper1.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(false, monitorWrapper1.CurrentStats.HadEnoughData);
            Assert.AreEqual(2010m, monitorWrapper2.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(1005m, monitorWrapper2.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(true, monitorWrapper2.CurrentStats.HadEnoughData);
        }


        [Test]
        public void TickerActivity_FewTickersFewTimerEvents_StatsAreNonZeroCountsOnlyRelevantHistoricChunks()
        {
            int newActivityStatsCallCnt1 = 0;
            int newActivityStatsCallCnt2 = 0;

            var tickerProvider = new Mock<ITickerProvider>();
            SafeTimerFactoryMock safeTimerMock = new SafeTimerFactoryMock();
            var usdConversionsProvider = new Mock<IUsdConversionProvider>();
            usdConversionsProvider.Setup(cp => cp.GetCurrentUsdConversionMultiplier()).Returns(2m);
            LmaxTickerActivityMonitor monitor =
                LmaxTickerActivityMonitor.GetNewLmaxTickerActivityMonitor(tickerProvider.Object, safeTimerMock,
                    usdConversionsProvider.Object, Symbol.AUD_CAD);
            LmaxTickerActivityMonitorWrapper monitorWrapper1 = new LmaxTickerActivityMonitorWrapper(monitor, TimeSpan.FromSeconds(15));
            LmaxTickerActivityMonitorWrapper monitorWrapper2 = new LmaxTickerActivityMonitorWrapper(monitor, TimeSpan.FromSeconds(5));

            monitorWrapper1.NewActivityStats += stats => { newActivityStatsCallCnt1++; };
            monitorWrapper2.NewActivityStats += stats => { newActivityStatsCallCnt2++; };

            tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(1000, DateTime.UtcNow));
            tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(5, DateTime.UtcNow));
            safeTimerMock.Callback();

            Assert.AreEqual(1, newActivityStatsCallCnt1);
            Assert.AreEqual(1, newActivityStatsCallCnt2);
            //usd multiplier is set to 2
            Assert.AreEqual(2010m, monitorWrapper1.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(1005m, monitorWrapper1.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(false, monitorWrapper1.CurrentStats.HadEnoughData);
            Assert.AreEqual(2010m, monitorWrapper2.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(1005m, monitorWrapper2.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(true, monitorWrapper2.CurrentStats.HadEnoughData);

            safeTimerMock.Callback();

            Assert.AreEqual(2, newActivityStatsCallCnt1);
            Assert.AreEqual(2, newActivityStatsCallCnt2);
            //usd multiplier is set to 2
            Assert.AreEqual(2010m, monitorWrapper1.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(1005m, monitorWrapper1.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(false, monitorWrapper1.CurrentStats.HadEnoughData);
            Assert.AreEqual(0m, monitorWrapper2.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(0m, monitorWrapper2.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(true, monitorWrapper2.CurrentStats.HadEnoughData);

            tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(10000, DateTime.UtcNow + TimeSpan.FromSeconds(26)));
            tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(10000, DateTime.UtcNow + TimeSpan.FromSeconds(26)));

            safeTimerMock.Callback();

            Assert.AreEqual(3, newActivityStatsCallCnt1);
            Assert.AreEqual(3, newActivityStatsCallCnt2);
            Assert.AreEqual(42010m, monitorWrapper1.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(10502.5m, monitorWrapper1.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(true, monitorWrapper1.CurrentStats.HadEnoughData);
            Assert.AreEqual(40000m, monitorWrapper2.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(20000m, monitorWrapper2.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(true, monitorWrapper2.CurrentStats.HadEnoughData);

            tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(1000, DateTime.UtcNow + TimeSpan.FromSeconds(36)));
            tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(1000, DateTime.UtcNow + TimeSpan.FromSeconds(36)));

            //After this the first interval is off-relevant
            safeTimerMock.Callback();

            Assert.AreEqual(4, newActivityStatsCallCnt1);
            Assert.AreEqual(4, newActivityStatsCallCnt2);
            Assert.AreEqual(44000m, monitorWrapper1.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(11000m, monitorWrapper1.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(true, monitorWrapper1.CurrentStats.HadEnoughData);
            Assert.AreEqual(4000m, monitorWrapper2.CurrentStats.TotalVolumUsd);
            Assert.AreEqual(2000m, monitorWrapper2.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(true, monitorWrapper2.CurrentStats.HadEnoughData);
        }


        [Test]
        public void TickerActivity_FewTickersFewTimerEvents_StatsAreNonZeroCountsCorrectlyWrapsAround()
        {

            var tickerProvider = new Mock<ITickerProvider>();
            SafeTimerFactoryMock safeTimerMock = new SafeTimerFactoryMock();
            var usdConversionsProvider = new Mock<IUsdConversionProvider>();
            usdConversionsProvider.Setup(cp => cp.GetCurrentUsdConversionMultiplier()).Returns(2m);
            LmaxTickerActivityMonitor monitor =
                LmaxTickerActivityMonitor.GetNewLmaxTickerActivityMonitor(tickerProvider.Object, safeTimerMock,
                    usdConversionsProvider.Object, Symbol.AUD_CAD);
            LmaxTickerActivityMonitorWrapper monitorWrapper1 = new LmaxTickerActivityMonitorWrapper(monitor, TimeSpan.FromSeconds(30));
            LmaxTickerActivityMonitorWrapper monitorWrapper2 = new LmaxTickerActivityMonitorWrapper(monitor, TimeSpan.FromSeconds(60));
            LmaxTickerActivityMonitorWrapper monitorWrapper3 = new LmaxTickerActivityMonitorWrapper(monitor, TimeSpan.FromSeconds(200));
            LmaxTickerActivityMonitorWrapper monitorWrapper4 = new LmaxTickerActivityMonitorWrapper(monitor, TimeSpan.FromSeconds(400));

            for (int i = 0; i < 900; i++)
            {
                tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(1000, DateTime.UtcNow));
                safeTimerMock.Callback();
            }

            for (int i = 0; i < 170; i++)
            {
                safeTimerMock.Callback();
            }

            for (int i = 1; i < 20; i++)
            {
                tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(i, DateTime.UtcNow));
                safeTimerMock.Callback();
            }


            Assert.AreEqual((19 + 19-6+1)*6m, monitorWrapper1.CurrentStats.TotalVolumUsd);
            Assert.AreEqual((decimal)(19 + 19 - 6+1), monitorWrapper1.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(true, monitorWrapper1.CurrentStats.HadEnoughData);

            Assert.AreEqual((19 + 19 - 12 + 1) * 12m, monitorWrapper2.CurrentStats.TotalVolumUsd);
            Assert.AreEqual((decimal)(19 + 19 - 12 + 1), monitorWrapper2.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(true, monitorWrapper2.CurrentStats.HadEnoughData);

            Assert.AreEqual((19 + 1) * 19m, monitorWrapper3.CurrentStats.TotalVolumUsd);
            Assert.AreEqual((decimal)(19 + 1), monitorWrapper3.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(true, monitorWrapper3.CurrentStats.HadEnoughData);

            Assert.AreEqual((19 + 1) * 19m, monitorWrapper4.CurrentStats.TotalVolumUsd);
            Assert.AreEqual((decimal)(19 + 1), monitorWrapper4.CurrentStats.AvgDealSizeUsd);
            Assert.AreEqual(true, monitorWrapper4.CurrentStats.HadEnoughData);
        }


        //[Test]
        //public void TickerActivity_CtorTest_Succeeds()
        //{
        //    var tickerProvider = new Mock<ITickerProvider>();
        //    SafeTimerFactoryMock safeTimerMock = new SafeTimerFactoryMock();
        //    var usdConversionsProvider = new Mock<IUsdConversionProvider>();
        //    usdConversionsProvider.Setup(cp => cp.GetCurrentUsdConversionMultiplier()).Returns(2m);
        //    LmaxTickerActivityStatsProviderUnderTest statsProvider = new LmaxTickerActivityStatsProviderUnderTest(tickerProvider.Object, safeTimerMock, usdConversionsProvider.Object, Symbol.AUD_CAD);
        //}

        //[Test]
        //public void TickerActivity_FewTickesrNoTimerEvent_StatsAreZero()
        //{
        //    int newActivityStatsCallCnt = 0;

        //    var tickerProvider = new Mock<ITickerProvider>();
        //    SafeTimerFactoryMock safeTimerMock = new SafeTimerFactoryMock();
        //    var usdConversionsProvider = new Mock<IUsdConversionProvider>();
        //    usdConversionsProvider.Setup(cp => cp.GetCurrentUsdConversionMultiplier()).Returns(2m);
        //    LmaxTickerActivityStatsProviderUnderTest statsProvider = new LmaxTickerActivityStatsProviderUnderTest(tickerProvider.Object, safeTimerMock, usdConversionsProvider.Object, Symbol.AUD_CAD);
        //    statsProvider.NewActivityStats += stats => { newActivityStatsCallCnt++; };

        //    statsProvider.SetLookBackDuration(TimeSpan.FromSeconds(55));

        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(1000, DateTime.UtcNow));
        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(5, DateTime.UtcNow));
        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(123456789, DateTime.UtcNow));

        //    Assert.AreEqual(0, newActivityStatsCallCnt);
        //    Assert.AreEqual(0m, statsProvider.CurrentStats.TotalVolumUsd);
        //    Assert.AreEqual(0m, statsProvider.CurrentStats.AvgDealSizeUsd);
        //    Assert.AreEqual(false, statsProvider.CurrentStats.HadEnoughData);
        //}

        //[Test]
        //public void TickerActivity_FewTickersSingleTimerEvent_StatsAreNonZeroCountsOnlyHistoricChunks()
        //{
        //    int newActivityStatsCallCnt = 0;

        //    var tickerProvider = new Mock<ITickerProvider>();
        //    SafeTimerFactoryMock safeTimerMock = new SafeTimerFactoryMock();
        //    var usdConversionsProvider = new Mock<IUsdConversionProvider>();
        //    usdConversionsProvider.Setup(cp => cp.GetCurrentUsdConversionMultiplier()).Returns(2m);
        //    LmaxTickerActivityStatsProviderUnderTest statsProvider = new LmaxTickerActivityStatsProviderUnderTest(tickerProvider.Object, safeTimerMock, usdConversionsProvider.Object, Symbol.AUD_CAD);
        //    statsProvider.NewActivityStats += stats => { newActivityStatsCallCnt++; };

        //    statsProvider.SetLookBackDuration(TimeSpan.FromSeconds(55));
        //    statsProvider.SetUtcNow(DateTime.UtcNow);

        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(1000, DateTime.UtcNow));
        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(5, DateTime.UtcNow));
        //    safeTimerMock.Callback();
        //    statsProvider.SetUtcNow(DateTime.UtcNow + TimeSpan.FromSeconds(16));
        //    Assert.AreEqual(1, newActivityStatsCallCnt);
        //    //usd multiplier is set to 2
        //    Assert.AreEqual(2010m, statsProvider.CurrentStats.TotalVolumUsd);
        //    Assert.AreEqual(1005m, statsProvider.CurrentStats.AvgDealSizeUsd);
        //    Assert.AreEqual(false, statsProvider.CurrentStats.HadEnoughData);

        //    //this will not be already counted towards stats
        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(123456789, DateTime.UtcNow));

        //    Assert.AreEqual(1, newActivityStatsCallCnt);
        //    Assert.AreEqual(2010m, statsProvider.CurrentStats.TotalVolumUsd);
        //    Assert.AreEqual(1005m, statsProvider.CurrentStats.AvgDealSizeUsd);
        //    Assert.AreEqual(false, statsProvider.CurrentStats.HadEnoughData);
        //}

        //[Test]
        //public void TickerActivity_FewTickersFewTimerEvents_StatsAreNonZeroCountsOnlyRelevantHistoricChunks()
        //{
        //    int newActivityStatsCallCnt = 0;

        //    var tickerProvider = new Mock<ITickerProvider>();
        //    SafeTimerFactoryMock safeTimerMock = new SafeTimerFactoryMock();
        //    var usdConversionsProvider = new Mock<IUsdConversionProvider>();
        //    usdConversionsProvider.Setup(cp => cp.GetCurrentUsdConversionMultiplier()).Returns(2m);
        //    LmaxTickerActivityStatsProviderUnderTest statsProvider = new LmaxTickerActivityStatsProviderUnderTest(tickerProvider.Object, safeTimerMock, usdConversionsProvider.Object, Symbol.AUD_CAD);
        //    statsProvider.NewActivityStats += stats => { newActivityStatsCallCnt++; };

        //    statsProvider.SetLookBackDuration(TimeSpan.FromSeconds(25));
        //    statsProvider.SetUtcNow(DateTime.UtcNow);

        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(1000, DateTime.UtcNow));
        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(5, DateTime.UtcNow));
        //    safeTimerMock.Callback();
        //    statsProvider.SetUtcNow(DateTime.UtcNow + TimeSpan.FromSeconds(16));
        //    Assert.AreEqual(1, newActivityStatsCallCnt);
        //    //usd multiplier is set to 2
        //    Assert.AreEqual(2010m, statsProvider.CurrentStats.TotalVolumUsd);
        //    Assert.AreEqual(1005m, statsProvider.CurrentStats.AvgDealSizeUsd);
        //    Assert.AreEqual(false, statsProvider.CurrentStats.HadEnoughData);

        //    safeTimerMock.Callback();
        //    statsProvider.SetUtcNow(DateTime.UtcNow + TimeSpan.FromSeconds(26));
        //    Assert.AreEqual(2, newActivityStatsCallCnt);
        //    //usd multiplier is set to 2
        //    Assert.AreEqual(2010m, statsProvider.CurrentStats.TotalVolumUsd);
        //    Assert.AreEqual(1005m, statsProvider.CurrentStats.AvgDealSizeUsd);
        //    Assert.AreEqual(false, statsProvider.CurrentStats.HadEnoughData);

        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(10000, DateTime.UtcNow + TimeSpan.FromSeconds(26)));
        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(10000, DateTime.UtcNow + TimeSpan.FromSeconds(26)));

        //    safeTimerMock.Callback();
        //    statsProvider.SetUtcNow(DateTime.UtcNow + TimeSpan.FromSeconds(36));
        //    Assert.AreEqual(3, newActivityStatsCallCnt);
        //    //usd multiplier is set to 2
        //    Assert.AreEqual(42010m, statsProvider.CurrentStats.TotalVolumUsd);
        //    Assert.AreEqual(10502.5m, statsProvider.CurrentStats.AvgDealSizeUsd);
        //    Assert.AreEqual(true, statsProvider.CurrentStats.HadEnoughData);

        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(10000, DateTime.UtcNow + TimeSpan.FromSeconds(36)));
        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(10000, DateTime.UtcNow + TimeSpan.FromSeconds(36)));

        //    //After this the first interval is off-relevant
        //    safeTimerMock.Callback();
        //    statsProvider.SetUtcNow(DateTime.UtcNow + TimeSpan.FromSeconds(46));
        //    Assert.AreEqual(4, newActivityStatsCallCnt);
        //    //usd multiplier is set to 2
        //    Assert.AreEqual(80000m, statsProvider.CurrentStats.TotalVolumUsd);
        //    Assert.AreEqual(20000m, statsProvider.CurrentStats.AvgDealSizeUsd);
        //    Assert.AreEqual(true, statsProvider.CurrentStats.HadEnoughData);
        //}


        //[Test]
        //public void TickerActivity_FewTickersFewTimerEventsAndRoundedInterval_StatsAreNonZeroCountsOnlyRelevantHistoricChunks()
        //{
        //    int newActivityStatsCallCnt = 0;

        //    var tickerProvider = new Mock<ITickerProvider>();
        //    SafeTimerFactoryMock safeTimerMock = new SafeTimerFactoryMock();
        //    var usdConversionsProvider = new Mock<IUsdConversionProvider>();
        //    usdConversionsProvider.Setup(cp => cp.GetCurrentUsdConversionMultiplier()).Returns(2m);
        //    LmaxTickerActivityStatsProviderUnderTest statsProvider = new LmaxTickerActivityStatsProviderUnderTest(tickerProvider.Object, safeTimerMock, usdConversionsProvider.Object, Symbol.AUD_CAD);
        //    statsProvider.NewActivityStats += stats => { newActivityStatsCallCnt++; };

        //    statsProvider.SetLookBackDuration(TimeSpan.FromSeconds(20));
        //    statsProvider.SetUtcNow(DateTime.UtcNow);

        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(1000, DateTime.UtcNow));
        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(5, DateTime.UtcNow));
        //    safeTimerMock.Callback();
        //    statsProvider.SetUtcNow(DateTime.UtcNow + TimeSpan.FromSeconds(10));
        //    Assert.AreEqual(1, newActivityStatsCallCnt);
        //    //usd multiplier is set to 2
        //    Assert.AreEqual(2010m, statsProvider.CurrentStats.TotalVolumUsd);
        //    Assert.AreEqual(1005m, statsProvider.CurrentStats.AvgDealSizeUsd);
        //    Assert.AreEqual(false, statsProvider.CurrentStats.HadEnoughData);

        //    safeTimerMock.Callback();
        //    statsProvider.SetUtcNow(DateTime.UtcNow + TimeSpan.FromSeconds(20));
        //    Assert.AreEqual(2, newActivityStatsCallCnt);
        //    //usd multiplier is set to 2
        //    Assert.AreEqual(2010m, statsProvider.CurrentStats.TotalVolumUsd);
        //    Assert.AreEqual(1005m, statsProvider.CurrentStats.AvgDealSizeUsd);
        //    Assert.AreEqual(false, statsProvider.CurrentStats.HadEnoughData);

        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(10000, DateTime.UtcNow + TimeSpan.FromSeconds(20)));
        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(10000, DateTime.UtcNow + TimeSpan.FromSeconds(20)));

        //    safeTimerMock.Callback();
        //    statsProvider.SetUtcNow(DateTime.UtcNow + TimeSpan.FromSeconds(30));
        //    Assert.AreEqual(3, newActivityStatsCallCnt);
        //    //usd multiplier is set to 2
        //    Assert.AreEqual(42010m, statsProvider.CurrentStats.TotalVolumUsd);
        //    Assert.AreEqual(10502.5m, statsProvider.CurrentStats.AvgDealSizeUsd);
        //    Assert.AreEqual(true, statsProvider.CurrentStats.HadEnoughData);

        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(10000, DateTime.UtcNow + TimeSpan.FromSeconds(30)));
        //    tickerProvider.Raise(tp => tp.NewVenueDeal += null, GetNewTicker(10000, DateTime.UtcNow + TimeSpan.FromSeconds(30)));

        //    //After this the first interval is off-relevant
        //    safeTimerMock.Callback();
        //    statsProvider.SetUtcNow(DateTime.UtcNow + TimeSpan.FromSeconds(40));
        //    Assert.AreEqual(4, newActivityStatsCallCnt);
        //    //usd multiplier is set to 2
        //    Assert.AreEqual(80000m, statsProvider.CurrentStats.TotalVolumUsd);
        //    Assert.AreEqual(20000m, statsProvider.CurrentStats.AvgDealSizeUsd);
        //    Assert.AreEqual(true, statsProvider.CurrentStats.HadEnoughData);
        //}

    }
}
