﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.LowLatencyUtils;
using Moq;
using NUnit.Framework;

namespace Kreslik.Integrator.BusinessLayer.Tests
{

    [TestFixture]
    public class SharedHeapTests
    {
        private List<object> _topOfBookDeclinedArgs;
        private List<object> _topOfBookImproovedArgs;
        private List<object> _topOfBookReplacedArgs;

        private void OnTopOfBookDeclined<T>(IBookTop<T> sender, T arg, DateTime timestamp)
        {
            _topOfBookDeclinedArgs.Add(arg);
        }

        private void OnTopOfBookImprooved<T>(IBookTop<T> sender, T arg, DateTime timestamp)
        {
            _topOfBookImproovedArgs.Add(arg);
        }

        private void OnTopOfBookReplaced<T>(T arg, DateTime timestamp)
        {
            _topOfBookReplacedArgs.Add(arg);
        }

        private void OnChangeableTopOfBookReplaced<T>(T arg)
        {
            _topOfBookReplacedArgs.Add(arg);
        }
            
        [SetUp]
        public void SetUp()
        {
            var dummy = Counterparty.L01;
            _topOfBookDeclinedArgs = new List<object>();
            _topOfBookImproovedArgs = new List<object>();
            _topOfBookReplacedArgs = new List<object>();
        }

        private class TestingPriceObject: PriceObjectInternal
        {
            private TestPoolingHelper _testPoolingHelper;

            public TestingPriceObject(int val, int idx)
                : this(val, (Common.Counterparty)idx) { }

            public TestingPriceObject(decimal val, Counterparty cpt)
                : this(val, 1000m, cpt)
            { }

            public TestingPriceObject(decimal price, decimal size, Counterparty cpt)
            {
                //this.DangerousTestingOnly__OverrideContentInternal(new AtomicDecimal(val), 1000m, null, null,
                //    PriceSide.Bid, Symbol.AUD_CAD, cpt, MarketDataRecordType.BankQuoteData, null, null,
                //    Guid.NewGuid(), DateTime.MinValue, DateTime.MinValue);

                this.DangerousTestingOnly__OverrideContentInternalAndAcquire(new AtomicDecimal(price), size, null, null,
                    PriceSide.Bid, Symbol.AUD_CAD, cpt, MarketDataRecordType.BankQuoteData, null, null,
                    Guid.NewGuid(), DateTime.MinValue, DateTime.MinValue);

                //OverrideContentAndAcquire

                _testPoolingHelper = new TestPoolingHelper();


                Type t = typeof(PriceObjectInternal);
                foreach (var member in t.GetMembers(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
                {
                    if (member.Name == "_poolingHelper")
                    {
                        // changing the variable
                        FieldInfo fi = (FieldInfo)member;
                        fi.SetValue(this, _testPoolingHelper);
                    }
                }
            }

            public static implicit operator TestingPriceObject(int value)
            {
                return new TestingPriceObject(value, value);
            }

            public override bool Equals(object obj)
            {
                try
                {
                    TestingPriceObject second;

                    if (obj is TestingPriceObject)
                        second = obj as TestingPriceObject;
                    else
                        second = (TestingPriceObject)obj;
                    return this.Price == second.Price && this.Counterparty == second.Counterparty;
                }
                catch (Exception)
                {
                    return false;
                }
                
            }

            public int AcquireCount { get { return this._testPoolingHelper.AcquireCount; } }
            public int ReleaseCount { get { return this._testPoolingHelper.ReleaseCount; } }

            private class TestPoolingHelper : IPoolingHelper
            {

                public int AcquireCount { get; private set; }
                public int ReleaseCount { get; private set; }

                public void Acquire()
                {
                    this.AcquireCount++;
                }

                public void SetValid()
                {
                    //throw new NotImplementedException();
                }

                public void AcquireInitial()
                {
                    this.AcquireCount++;
                }

                public void Release()
                {
                    this.ReleaseCount++;
                }

                public void ClearAcquireCount()
                {
                    //throw new NotImplementedException();
                }

                public void MarkReleaseDisallowed()
                {
                    throw new NotImplementedException();
                }

                public void ClearReleaseDisallowed()
                {
                    throw new NotImplementedException();
                }
            }
        }

        private PriceObjectInternal _nullNode = PriceObjectInternal.GetNullBidInternal(Symbol.AUD_CAD);

        private PriceBookInternal CreateTestHeap(int size)
        {
            return new PriceBookInternal(size, BidPriceComparerInternal.Default,
                BidTopPriceComparerInternal.Default, BidTopPriceComparerInternal.Default, PriceCounterpartEqualityComparerInternal.Default,
                _nullNode, new TestLogger(Assert.Fail, LogLevel.Info));
        }

        [Test]
        public void EmptyHeap_RemoveAt_ReturnsFalse()
        {
            PriceBookInternal heap = CreateTestHeap(5);
            heap.BookTopImproved += OnTopOfBookImprooved<PriceObjectInternal>;
            heap.BookTopDeterioratedInternal += OnTopOfBookDeclined;
            Assert.False(heap.Remove((Counterparty) 0));
            Assert.False(heap.Remove((Counterparty) 2));
            Assert.AreEqual(0, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(0, _topOfBookImproovedArgs.Count);
        }

        [Test]
        public void EmptyHeap_GetMax_ReturnsFalse()
        {
            PriceBookInternal heap = CreateTestHeap(5);
            Assert.AreEqual(_nullNode, heap.MaxNodeInternal);
        }

        [Test]
        public void EmptyHeap_GetSortedClone_ReturnsEmptyArray()
        {
            PriceBookInternal heap = CreateTestHeap(5);

            Assert.AreEqual(0, heap.GetSortedClone().Length);
            Assert.AreEqual(0, heap.GetSortedClone(null).Length);
            Assert.AreEqual(0, heap.GetSortedClone(i => true).Length);
        }

        [Test]
        public void FullHeap_GetSortedCloneConditional_ReturnsSubarray()
        {
            PriceBookInternal heap = CreateTestHeap(5);

            heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)5);
            heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)4);
            heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)6);

            Assert.AreEqual(3, heap.GetSortedClone().Length);
            Assert.AreEqual(3, heap.GetSortedClone(null).Length);
            

            var clone = heap.GetSortedClone(i => i.Price%2 == 0);
            Assert.AreEqual(2, clone.Length);
            Assert.AreEqual(6, clone[0].Price);
            Assert.AreEqual(4, clone[1].Price);
        }

        [Test]
        public void HeapInsert_MaximumIsMaintained()
        {
            PriceBookInternal heap = CreateTestHeap(5);
            heap.BookTopImproved += OnTopOfBookImprooved<PriceObjectInternal>;
            heap.BookTopDeterioratedInternal += OnTopOfBookDeclined;

            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)5));
            Assert.AreEqual((TestingPriceObject)5, heap.MaxNodeInternal);
            Assert.AreEqual(0, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(1, _topOfBookImproovedArgs.Count);
            Assert.AreEqual(_topOfBookImproovedArgs[0], (TestingPriceObject)5);

            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)6), 0);
            Assert.AreEqual((TestingPriceObject)6, heap.MaxNodeInternal);
            Assert.AreEqual(0, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(2, _topOfBookImproovedArgs.Count);
            Assert.AreEqual(_topOfBookImproovedArgs[1], (TestingPriceObject)6);

            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)4), 2);
            Assert.AreEqual((TestingPriceObject)6, heap.MaxNodeInternal);
            Assert.AreEqual(0, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(2, _topOfBookImproovedArgs.Count);
        }

        [Test]
        public void HeapRemoveAt_MaximumIsMaintained()
        {
            PriceBookInternal heap = CreateTestHeap(5);
            heap.BookTopImproved += OnTopOfBookImprooved<PriceObjectInternal>;
            heap.BookTopDeterioratedInternal += OnTopOfBookDeclined;

            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)6));
            Assert.AreEqual(1, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)5));
            Assert.AreEqual(2, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)4));

            Assert.AreEqual((TestingPriceObject)6, heap.MaxNodeInternal);
            Assert.AreEqual(0, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(1, _topOfBookImproovedArgs.Count);
            Assert.AreEqual(_topOfBookImproovedArgs[0], (TestingPriceObject)6);

            Assert.IsTrue(heap.Remove((Counterparty)6));
            Assert.AreEqual((TestingPriceObject)5, heap.MaxNodeInternal);
            Assert.AreEqual(1, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(_topOfBookDeclinedArgs[0], (TestingPriceObject)5);
            Assert.AreEqual(1, _topOfBookImproovedArgs.Count);

            Assert.IsTrue(heap.Remove((Counterparty)5));
            Assert.AreEqual((TestingPriceObject)4, heap.MaxNodeInternal);
            Assert.AreEqual(2, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(_topOfBookDeclinedArgs[1], (TestingPriceObject)4);
            Assert.AreEqual(1, _topOfBookImproovedArgs.Count);

            Assert.IsTrue(heap.Remove((Counterparty)4));
            Assert.AreEqual(_nullNode, heap.MaxNodeInternal);
            Assert.AreEqual(3, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(_topOfBookDeclinedArgs[2], _nullNode);
            Assert.AreEqual(1, _topOfBookImproovedArgs.Count);

            Assert.IsEmpty(heap.GetSortedClone());

            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)6));
            Assert.AreEqual(1, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)5));
            Assert.AreEqual(2, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)4));
            Assert.AreEqual(3, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)3));
            Assert.AreEqual(4, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)2));

            Assert.IsTrue(heap.Remove((Counterparty)5));
            Assert.AreEqual((TestingPriceObject)6, heap.MaxNodeInternal);
            Assert.AreEqual(3, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(2, _topOfBookImproovedArgs.Count);
            Assert.AreEqual(_topOfBookImproovedArgs[1], (TestingPriceObject)6);

            Assert.IsTrue(heap.Remove((Counterparty)4));
            Assert.AreEqual((TestingPriceObject)6, heap.MaxNodeInternal);
            Assert.AreEqual(3, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(2, _topOfBookImproovedArgs.Count);

            Assert.IsTrue(heap.Remove((Counterparty)6));
            Assert.AreEqual((TestingPriceObject)3, heap.MaxNodeInternal);
            Assert.AreEqual(4, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(_topOfBookDeclinedArgs[3], (TestingPriceObject)3);
            Assert.AreEqual(2, _topOfBookImproovedArgs.Count);
        }


        [Test]
        public void HeapInsert_InsertEqualToMaximum_MaximumIsUpdated()
        {
            PriceBookInternal heap = CreateTestHeap(5);
            heap.BookTopImproved += OnTopOfBookImprooved<PriceObjectInternal>;
            heap.BookTopDeterioratedInternal += OnTopOfBookDeclined;
            heap.ChangeableBookTopReplaced += OnChangeableTopOfBookReplaced;

            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)6));
            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)6));
            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)6));
            Assert.AreEqual(0, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(1, _topOfBookImproovedArgs.Count);
            Assert.AreEqual(_topOfBookImproovedArgs[0], (TestingPriceObject)6); // order is important - we need intclass equals
            Assert.AreEqual(2, _topOfBookReplacedArgs.Count);

            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)8));
            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)8));
            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)8));
            Assert.AreEqual(0, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(2, _topOfBookImproovedArgs.Count);
            Assert.AreEqual(_topOfBookImproovedArgs[1], (TestingPriceObject)8);
            Assert.AreEqual(4, _topOfBookReplacedArgs.Count);

            Assert.AreEqual(2, heap.GetSortedClone().Length);
        }


        [Test]
        public void HeapInsert_TopRemovalDuringTopChangeEvent_OrderOfEventsIsMaintained()
        {
            PriceBookInternal heap = CreateTestHeap(5);
            List<PriceObjectInternal> bookTopChanges = new List<PriceObjectInternal>();
            List<PriceObjectInternal> changeableBookTopChanges = new List<PriceObjectInternal>();

            List<PriceObjectInternal> bookTopImprovements = new List<PriceObjectInternal>();
            List<PriceObjectInternal> bookTopDeteriorations = new List<PriceObjectInternal>();
            List<PriceObjectInternal> changeableBookTopImprovements = new List<PriceObjectInternal>();
            List<PriceObjectInternal> changeableBookTopDeteriorations = new List<PriceObjectInternal>();

            heap.BookTopImproved += (sender, node, stamp) => bookTopChanges.Add(node);
            heap.BookTopImproved += (sender, node, stamp) => bookTopImprovements.Add(node);
            heap.BookTopDeterioratedInternal += (sender, node, stamp) => bookTopChanges.Add(node);
            heap.BookTopDeterioratedInternal += (sender, node, stamp) => bookTopDeteriorations.Add(node);

            heap.ChangeableBookTopImproved += i => changeableBookTopChanges.Add(i);
            heap.ChangeableBookTopImproved += i => changeableBookTopImprovements.Add(i);
            heap.ChangeableBookTopDeteriorated += i => changeableBookTopChanges.Add(i);
            heap.ChangeableBookTopDeteriorated += i => changeableBookTopDeteriorations.Add(i);
            heap.ChangeableBookTopReplaced += i => changeableBookTopChanges.Add(i);

            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)6));
            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)6));
            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)6));
            Assert.AreEqual(1, bookTopChanges.Count);
            Assert.AreEqual(1, bookTopImprovements.Count);
            Assert.AreEqual((TestingPriceObject)6, bookTopChanges[0]);
            Assert.AreEqual(3, changeableBookTopChanges.Count);
            Assert.AreEqual(1, changeableBookTopImprovements.Count);
            Assert.AreEqual((TestingPriceObject)6, changeableBookTopChanges[0]);

            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)8));
            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)8));
            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)8));
            Assert.AreEqual(2, bookTopChanges.Count);
            Assert.AreEqual(2, bookTopImprovements.Count);
            Assert.AreEqual((TestingPriceObject)8, bookTopChanges[1]);
            Assert.AreEqual(6, changeableBookTopChanges.Count);
            Assert.AreEqual(2, changeableBookTopImprovements.Count);
            Assert.AreEqual((TestingPriceObject)8, changeableBookTopChanges[3]);

            heap.ChangeableBookTopImproved += i => heap.RemoveIdentical(i);

            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly((TestingPriceObject)10));

            Assert.AreEqual(4, bookTopChanges.Count);
            Assert.AreEqual(3, bookTopImprovements.Count);
            Assert.AreEqual(1, bookTopDeteriorations.Count);

            //Cannot no more guarentee the order of recursive changes
            //Assert.AreEqual(10, bookTopChanges[2]);
            //Assert.AreEqual(8, bookTopChanges[3]);
            Assert.AreEqual((TestingPriceObject)10, bookTopImprovements[2]);
            Assert.AreEqual((TestingPriceObject)8, bookTopDeteriorations[0]);

            Assert.AreEqual(8, changeableBookTopChanges.Count);
            Assert.AreEqual(3, changeableBookTopImprovements.Count);
            Assert.AreEqual(1, changeableBookTopDeteriorations.Count);
            Assert.AreEqual((TestingPriceObject)10, changeableBookTopChanges[6]);
            Assert.AreEqual((TestingPriceObject)8, changeableBookTopChanges[7]);
            Assert.AreEqual((TestingPriceObject)10, changeableBookTopImprovements[2]);
            Assert.AreEqual((TestingPriceObject)8, changeableBookTopDeteriorations[0]);


            Assert.AreEqual(2, heap.GetSortedClone().Length);
        }
            
        [Test]
        public void HeapUpdateAt_MaximumIsMaintained()
        {
            PriceBookInternal heap = CreateTestHeap(5);
            heap.BookTopImproved += OnTopOfBookImprooved<PriceObjectInternal>;
            heap.BookTopDeterioratedInternal += OnTopOfBookDeclined;
            heap.ChangeableBookTopReplaced += OnChangeableTopOfBookReplaced;

            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(6, 1)), 0);
            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(5, 2)), 1);
            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(8, 3)), 0);

            Assert.AreEqual(heap.MaxNodeInternal.Price, 8);
            Assert.AreEqual(0, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(2, _topOfBookImproovedArgs.Count);
            Assert.AreEqual(new TestingPriceObject(8, 3), _topOfBookImproovedArgs[1]);

            //No change to top
            Assert.AreEqual(2, heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(0, 2)));
            Assert.AreEqual(heap.MaxNodeInternal.Price, 8);
            Assert.AreEqual(0, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(2, _topOfBookImproovedArgs.Count);

            //Decreasing the top - but it remains the top
            Assert.AreEqual(0, heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(7, 3)));
            Assert.AreEqual(heap.MaxNodeInternal.Price, 7);
            Assert.AreEqual(1, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(new TestingPriceObject(7, 3), _topOfBookDeclinedArgs[0]);

            //No change to top (even though there is second item of same rank as the top)
            Assert.AreEqual(1, heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(7, 2)));
            Assert.AreEqual(heap.MaxNodeInternal.Price, 7);
            Assert.AreEqual(1, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(2, _topOfBookImproovedArgs.Count);
            Assert.AreEqual(0, _topOfBookReplacedArgs.Count);

            //Decreasing the top - it is not the top anymore, but the new value is same
            Assert.AreEqual(2, heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(0, 3)));
            Assert.AreEqual(heap.MaxNodeInternal.Price, 7);
            Assert.AreEqual(1, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(2, _topOfBookImproovedArgs.Count);
            Assert.AreEqual(1, _topOfBookReplacedArgs.Count);
            Assert.AreEqual(new TestingPriceObject(7, 2), _topOfBookReplacedArgs[0]);

            //Decreasing the top - there will be a new top which is lower
            Assert.AreEqual(1, heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(5, 2)));
            Assert.AreEqual(heap.MaxNodeInternal.Price, 6);
            Assert.AreEqual(2, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(new TestingPriceObject(6, 1), _topOfBookDeclinedArgs[1]);
            Assert.AreEqual(2, _topOfBookImproovedArgs.Count);
            Assert.AreEqual(1, _topOfBookReplacedArgs.Count);
        }

        [Test]
        // This is important to prevent unnecessary events to clients but still keep BusinessLayer up to date
        public void HeapInsert_InsertEqualToMaximum_MaximumIsKeptButOnReplacedIsInvoked()
        {
            PriceBookInternal heap = CreateTestHeap(8);
            heap.BookTopImproved += OnTopOfBookImprooved<PriceObjectInternal>;
            heap.BookTopDeterioratedInternal += OnTopOfBookDeclined;
            heap.ChangeableBookTopReplaced += OnChangeableTopOfBookReplaced;

            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(6, 1)), 0);
            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(6, 2)), 0);
            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(6, 3)), 0);
            Assert.AreEqual(0, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(1, _topOfBookImproovedArgs.Count);
            Assert.AreEqual(new TestingPriceObject(6, 1), _topOfBookImproovedArgs[0]);

            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(8, 4)), 0);
            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(8, 5)), 0);
            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(new TestingPriceObject(8, 6)), 0);
            Assert.AreEqual(0, _topOfBookDeclinedArgs.Count);
            Assert.AreEqual(2, _topOfBookImproovedArgs.Count);
            Assert.AreEqual(new TestingPriceObject(8, 4), _topOfBookImproovedArgs[1]);

            Assert.AreEqual(4, _topOfBookReplacedArgs.Count);

            Assert.AreEqual(6, heap.GetSortedClone().Length);
        }

        [Test]
        public void HeapInsert_InsertsAndRemovesMaintainsAcquireRelease()
        {
            PriceBookInternal heap = CreateTestHeap(8);


            var item1a = new TestingPriceObject(6, 1);
            var item2a = new TestingPriceObject(6, 2);
            var item3a = new TestingPriceObject(6, 3);

            var item1b = new TestingPriceObject(8, 1);
            var item2b = new TestingPriceObject(8, 2);
            var item3b = new TestingPriceObject(8, 3);

            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(item1a), 0);
            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(item2a), 0);
            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(item3a), 0);
            //one acquire and release count is for the top event
            Assert.AreEqual(2, item1a.AcquireCount);
            Assert.AreEqual(1, item1a.ReleaseCount);
            Assert.AreEqual(2, item2a.AcquireCount);
            Assert.AreEqual(1, item2a.ReleaseCount);
            Assert.AreEqual(2, item3a.AcquireCount);
            Assert.AreEqual(1, item3a.ReleaseCount);

            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(item1b), 0);
            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(item2b), 1);
            Assert.AreEqual(heap.AddOrUpdate_ForTestsOnly(item3b), 2);
            //previous items released
            Assert.AreEqual(2, item1a.AcquireCount);
            Assert.AreEqual(2, item1a.ReleaseCount);
            Assert.AreEqual(2, item2a.AcquireCount);
            Assert.AreEqual(2, item2a.ReleaseCount);
            Assert.AreEqual(2, item3a.AcquireCount);
            Assert.AreEqual(2, item3a.ReleaseCount);
            //new items acquired
            //one acquire and release count is for the top event
            Assert.AreEqual(2, item1b.AcquireCount);
            Assert.AreEqual(1, item1b.ReleaseCount);
            Assert.AreEqual(1, item2b.AcquireCount);
            Assert.AreEqual(0, item2b.ReleaseCount);
            Assert.AreEqual(1, item3b.AcquireCount);
            Assert.AreEqual(0, item3b.ReleaseCount);

            Assert.IsTrue(heap.Remove((Counterparty)2));
            Assert.IsFalse(heap.RemoveIdentical(item2b));
            Assert.AreEqual(1, item2b.AcquireCount);
            Assert.AreEqual(1, item2b.ReleaseCount);

            Assert.IsTrue(heap.RemoveIdentical(item1b));

            //previous items released
            Assert.AreEqual(2, item1a.AcquireCount);
            Assert.AreEqual(2, item1a.ReleaseCount);
            Assert.AreEqual(2, item2a.AcquireCount);
            Assert.AreEqual(2, item2a.ReleaseCount);
            Assert.AreEqual(2, item3a.AcquireCount);
            Assert.AreEqual(2, item3a.ReleaseCount);
            //new items acquired and some released
            Assert.AreEqual(2, item1b.AcquireCount);
            Assert.AreEqual(2, item1b.ReleaseCount);
            Assert.AreEqual(1, item2b.AcquireCount);
            Assert.AreEqual(1, item2b.ReleaseCount);
            Assert.AreEqual(2, item3b.AcquireCount);
            Assert.AreEqual(1, item3b.ReleaseCount);
        }

        [Test]
        public void HeapGetIdentical_GetsCorrespondingIdenticalItem()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);

            //SharedHeapInternal priceBook = new SharedHeapInternal
            //    (Counterparty.ValuesCount, new BidPriceComparer(), new BidTopPriceComparer(), new PriceCounterpartEqualityComparerInternal(),
            //     PriceObjectInternal.GetNullBidInternal(Symbol.EUR_USD), new TestLogger(Assert.Fail, LogLevel.Info));

            PriceObjectInternal price1 = new TestingPriceObject(1.5m, Counterparty.NOM);
            PriceObjectInternal price2 = new TestingPriceObject(2.5m, Counterparty.DBK);

            priceBook.AddOrUpdate_ForTestsOnly(price1);
            priceBook.AddOrUpdate_ForTestsOnly(price2);

            var price = priceBook.GetIdentical(price1);

            Assert.AreSame(price, price1);
        }


        [Test]
        public void HeapGetWeightedPriceForSize_EmptyHeap_ReturnsZeroPriceAndSize()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            decimal maxSizeOnPrice = 10000;
            Assert.AreEqual(0m, priceBook.GetWeightedPriceForSize(ref maxSizeOnPrice, 3));
            Assert.AreEqual(0m, maxSizeOnPrice);
        }

        [Test]
        public void HeapGetSizeForPrice_EmptyHeap_ReturnsZeroSize()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            Assert.AreEqual(0m, priceBook.GetMaximumSizeForExactPrice(1000, 10, decimal.MaxValue));
        }

        [Test]
        public void HeapGetWeightedPriceForSize_NonEmptyHeapZeroSizeRequested_ReturnsZeroPriceAndSize()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 100000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 200000m, Counterparty.DBK));

            decimal maxSizeOnPrice = 0m;
            Assert.AreEqual(0m, priceBook.GetWeightedPriceForSize(ref maxSizeOnPrice, 3));
            Assert.AreEqual(0m, maxSizeOnPrice);
        }

        [Test]
        public void HeapGetSizeForPrice_NonEmptyHeapZeroSizeRequested_ReturnsZeroSize()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 100000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 200000m, Counterparty.DBK));

            Assert.AreEqual(0m, priceBook.GetMaximumSizeForExactPrice(0, 10, 0));
        }

        [Test]
        public void HeapGetSizeForPrice_NonEmptyHeapTooGoodPriceRequested_ReturnsZeroSize()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 100000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 200000m, Counterparty.DBK));

            Assert.AreEqual(0m, priceBook.GetMaximumSizeForExactPrice(100.9m, 10, decimal.MaxValue));
        }

        [Test]
        public void HeapGetWeightedPriceForSize_NonEmptyHeapZeroOrNegativeLayers_ReturnsZeroPriceAndSize()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 100000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 200000m, Counterparty.DBK));

            decimal maxSizeOnPrice = 2000m;
            Assert.AreEqual(0m, priceBook.GetWeightedPriceForSize(ref maxSizeOnPrice, 0));
            Assert.AreEqual(0m, maxSizeOnPrice);

            maxSizeOnPrice = 1000m;
            Assert.AreEqual(0m, priceBook.GetWeightedPriceForSize(ref maxSizeOnPrice, -5));
            Assert.AreEqual(0m, maxSizeOnPrice);
        }

        [Test]
        public void HeapGetSizeForPrice_NonEmptyHeapZeroOrNegativeLayers_ReturnsZeroSize()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 100000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 200000m, Counterparty.DBK));

            Assert.AreEqual(0m, priceBook.GetMaximumSizeForExactPrice(0.9m, 0, decimal.MaxValue));
            Assert.AreEqual(0m, priceBook.GetMaximumSizeForExactPrice(0.9m, -5, decimal.MaxValue));
        }

        [Test]
        public void HeapGetWeightedPriceForSize_NonEmptyHeapInsuficientLiquidityFewLayers_ReturnsPriceAndSmallerSize()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 1000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 2000m, Counterparty.DBK));

            decimal maxSizeOnPrice = 5000m;
            decimal expectedPrice = (1000m*1.5m + 2000m*2.5m)/3000m;
            Assert.AreEqual(expectedPrice, priceBook.GetWeightedPriceForSize(ref maxSizeOnPrice, 3));
            Assert.AreEqual(3000m, maxSizeOnPrice);
        }

        [Test]
        public void HeapGetSizeForPrice_NonEmptyHeapInsuficientLiquidityFewLayers_ReturnsSizeOfWholeHeap()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 1000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 2000m, Counterparty.DBK));

            Assert.AreEqual(3000m, priceBook.GetMaximumSizeForExactPrice(0.9m, 10, decimal.MaxValue));
        }

        [Test]
        public void HeapGetSizeForPrice_NonEmptyHeapSufficientLiquidityFewLayers_ReturnsSizeRequested()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(3.5m, 1000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 2000m, Counterparty.DBK));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 1000m, Counterparty.SOC));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(0.5m, 3000m, Counterparty.HSB));

            Assert.AreEqual(3333m, priceBook.GetMaximumSizeForExactPrice(0.9m, 10, 3333m));
        }

        [Test]
        public void HeapGetSizeForPrice_NonEmptyHeapSufficientLiquidityFewLayers_ReturnsSizeRequestedByWhitelist()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(3.5m, 1000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 2000m, Counterparty.DBK));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 1000m, Counterparty.SOC));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(0.5m, 3000m, Counterparty.HSB));

            Assert.AreEqual(0m, priceBook.GetMaximumSizeForExactPrice(3.0m, 10, 10000m, new Counterparty[]{Counterparty.DBK, Counterparty.SOC}));
            Assert.AreEqual(3000m, priceBook.GetMaximumSizeForExactPrice(2.0m, 10, 10000m, new Counterparty[] { Counterparty.DBK, Counterparty.SOC }));
            Assert.AreEqual(2500m, priceBook.GetMaximumSizeForExactPrice(2.3m, 10, 10000m, new Counterparty[] {Counterparty.DBK, Counterparty.SOC }));
        }

        [Test]
        public void HeapGetWeightedPriceForSize_NonEmptyHeapInsuficientLiquidityEnaughLayers_ReturnsPriceAndSmallerSize()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(3.5m, 1000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 2000m, Counterparty.DBK));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 1000m, Counterparty.SOC));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(0.5m, 3000m, Counterparty.HSB));

            decimal maxSizeOnPrice = 5000m;
            decimal expectedPrice = (1000m * 1.5m + 2000m * 2.5m + 1000m*3.5m) / 4000m;
            Assert.AreEqual(expectedPrice, priceBook.GetWeightedPriceForSize(ref maxSizeOnPrice, 3));
            Assert.AreEqual(4000m, maxSizeOnPrice);
        }

        [Test]
        public void HeapGetWeightedPriceForSize_LiquidityOnFirstLayer_ReturnsPriceAndSize()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(3.5m, 1000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 2000m, Counterparty.DBK));

            decimal maxSizeOnPrice = 500m;
            Assert.AreEqual(3.5m, priceBook.GetWeightedPriceForSize(ref maxSizeOnPrice, 3));
            Assert.AreEqual(500m, maxSizeOnPrice);

            maxSizeOnPrice = 1000m;
            Assert.AreEqual(3.5m, priceBook.GetWeightedPriceForSize(ref maxSizeOnPrice, 3));
            Assert.AreEqual(1000m, maxSizeOnPrice);

            maxSizeOnPrice = 1001m;
            Assert.AreEqual(3.5m, priceBook.GetWeightedPriceForSize(ref maxSizeOnPrice, 1));
            Assert.AreEqual(1000m, maxSizeOnPrice);
        }

        [Test]
        public void HeapGetWeightedPriceForSize_LiquidityOnMoreWholeLayers_ReturnsPriceAndSize()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(3.5m, 1000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 2000m, Counterparty.DBK));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 1000m, Counterparty.SOC));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(0.5m, 3000m, Counterparty.HSB));

            decimal maxSizeOnPrice = 4000m;
            decimal expectedPrice = (1000m * 1.5m + 2000m * 2.5m + 1000m * 3.5m) / 4000m;
            Assert.AreEqual(expectedPrice, priceBook.GetWeightedPriceForSize(ref maxSizeOnPrice, 5));
            Assert.AreEqual(4000m, maxSizeOnPrice);
        }

        [Test]
        public void HeapGetWeightedPriceForSize_LiquidityOnMoreLayersLastPartial_ReturnsPriceAndSize()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(3.5m, 1000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 2000m, Counterparty.DBK));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 5000m, Counterparty.SOC));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(0.5m, 3000m, Counterparty.HSB));

            decimal maxSizeOnPrice = 4000m;
            decimal expectedPrice = (1000m * 1.5m + 2000m * 2.5m + 1000m * 3.5m) / 4000m;
            Assert.AreEqual(expectedPrice, priceBook.GetWeightedPriceForSize(ref maxSizeOnPrice, 5));
            Assert.AreEqual(4000m, maxSizeOnPrice);
        }


        [Test]
        public void HeapGetSizeForPrice_MaxPriceExceededOnFirstLayer_ReturnsSizeOfFirstLayer()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(3.5m, 1000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 2000m, Counterparty.DBK));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 1000m, Counterparty.SOC));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(0.5m, 3000m, Counterparty.HSB));

            Assert.AreEqual(1000m, priceBook.GetMaximumSizeForExactPrice(3.5m, 10, 33333m));
        }

        [Test]
        public void HeapGetSizeForPrice_MaxPriceExceededOnSomeLayer_ReturnsSizeOfLayersWithOKPricePlusSomething()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(3.5m, 1000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 2000m, Counterparty.DBK));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 1000m, Counterparty.SOC));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(0.5m, 3000m, Counterparty.HSB));

            Assert.AreEqual(2000m, priceBook.GetMaximumSizeForExactPrice(3.0m, 10, 33333m));
        }

        [Test]
        public void HeapGetSizeForPrice_MaxPriceExceededOnSomeLayer_ReturnsSizeOfLayersWithOKPricePlusSomething2()
        {
            PriceBookInternal priceBook = CreateTestHeap(5);
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(3.5m, 1000m, Counterparty.NOM));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(2.5m, 2000m, Counterparty.DBK));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(1.5m, 1000m, Counterparty.SOC));
            priceBook.AddOrUpdate_ForTestsOnly(new TestingPriceObject(0.5m, 3000m, Counterparty.HSB));

            Assert.AreEqual(5333m + 1m/3m, priceBook.GetMaximumSizeForExactPrice(2.0m, 10, 33333m));
            //from 3rd layer we could take 5k not to spoil price - but we take just 1000
            // then from 4th layer we can take 1333.333...
        }


    }
}
