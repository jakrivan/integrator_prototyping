﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.BusinessLayer.Fakes;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Moq;
using NUnit.Framework;

namespace Kreslik.Integrator.BusinessLayer.Tests
{
    //[TestFixture]
    //public class CompositeOrdersManagerTests
    //{

    //    private static IRiskManager _nullRiskManager = new NullRiskManager();
    //    private static string _dummyErrorParameter = "foo";

    //    [Test]
    //    public void CompositeOrdersManager_constructionTest()
    //    {
    //        var priceBookMock = new Mock<IChangeablePriceBook<PriceObject, Counterparty>>();
    //        var priceBookStoreMock = new Mock<IPriceBookStore>();
    //        var orderFlowSessionsListMock = new Mock<List<IOrderFlowSession>>();
    //        var orderManagerMock = new Mock<IOrderManagement>();

    //        ILogger testLogger = new TestLogger(Assert.Fail);

    //        var comgr = new CompositeOrdersManager(testLogger, priceBookStoreMock.Object, orderFlowSessionsListMock.Object,
    //                                               _nullRiskManager, orderManagerMock.Object);
    //    }

    //    [Test]
    //    public void CompositeOrdersManager_RegisterCompositeOrderTest_WillRefuseInvalidOrders()
    //    {
    //        var priceBookStoreMock = new Mock<IPriceBookStore>();
    //        var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
    //        var orderManagerMock = new Mock<IOrderManagement>();
    //        var dispatchGatewayMock = new Mock<IUnicastInfoForwarder>();

    //        var orderFlowSessionMock = new Mock<IOrderFlowSession>();
    //        orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), out _dummyErrorParameter))
    //                            .Throws(new AssertionException("This test should not be creating external orders"));
    //        orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);

    //        ILogger testLogger = new TestLogger(Assert.Fail);

    //        var comgr = new CompositeOrdersManager(testLogger, priceBookStoreMock.Object, orderFlowSessionsListMock.Object,
    //                                               _nullRiskManager, orderManagerMock.Object);

    //        BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateLimit(200, 3, DealDirection.Buy, Symbol.EUR_SEK);
    //        IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateClientOrder(info1);

    //        comgr.RegisterCompositeOrder(clientOrder1, dispatchGatewayMock.Object);

    //        dispatchGatewayMock.Verify(
    //            gateway =>
    //            gateway.OnIntegratorClientOrderUpdate(
    //                It.Is<ClientOrderUpdateInfo>(
    //                    info =>
    //                    info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
    //                    info.OrderStatus == ClientOrderStatus.RejectedByIntegrator && info.SizeBaseAbsCancelled == 0), null),
    //            Times.Exactly(1));
    //    }


    //    [Test]
    //    public void CompositeOrdersManager_RegisterCompositeOrderTest_WillRefuseNonQuotedOrders()
    //    {
    //        var priceBookStoreMock = new Mock<IPriceBookStore>();
    //        var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
    //        var orderManagerMock = new Mock<IOrderManagement>();
    //        var dispatchGatewayMock = new Mock<IUnicastInfoForwarder>();

    //        var orderFlowSessionMock = new Mock<IOrderFlowSession>();
    //        orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), out _dummyErrorParameter))
    //                            .Throws(new AssertionException("This test should not be creating external orders"));
    //        orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);

    //        ILogger testLogger = new TestLogger(Assert.Fail);

    //        var comgr = new CompositeOrdersManager(testLogger, priceBookStoreMock.Object, orderFlowSessionsListMock.Object,
    //                                               _nullRiskManager, orderManagerMock.Object);

    //        PriceObject price = new NoIdentityQuote(1.5m, 1000, PriceSide.Ask, Symbol.EUR_SEK, Counterparty.NOM, "SubId", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice;

    //        BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateQuoted(200, 3, DealDirection.Buy, price);
    //        BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateLimit(200, 3, DealDirection.Buy, Symbol.EUR_SEK);
    //        IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateCompositeClientOrder(new List<BankPoolClientOrderRequestInfo>() { info1, info2 },
    //                                                    CompositeOrderStrategy.AllAtOnceOrImmediateAtMarket);

    //        comgr.RegisterCompositeOrder(clientOrder1, dispatchGatewayMock.Object);

    //        dispatchGatewayMock.Verify(
    //            gateway =>
    //            gateway.OnIntegratorClientOrderUpdate(
    //                It.Is<ClientOrderUpdateInfo>(
    //                    info =>
    //                    info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
    //                    info.OrderStatus == ClientOrderStatus.RejectedByIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_SEK),
    //            Times.Exactly(1));
    //    }

    //    [Test]
    //    public void CompositeOrdersManager_RegisterCompositeOrderTest_OnePriceDoesnotMeetAgeTest_NothingWillHappenAndOrderRejected()
    //    {
    //        var priceBookStoreMock = new Mock<IPriceBookStore>();
    //        var priceBookMock = new Mock<IChangeablePriceBook<PriceObject, Counterparty>>();
    //        var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
    //        var orderManagerMock = new Mock<IOrderManagement>();
    //        var dispatchGatewayMock = new Mock<IUnicastInfoForwarder>();

    //        var orderFlowSessionMock = new Mock<IOrderFlowSession>();
    //        int ordersCnt = 0;
    //        orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), out _dummyErrorParameter))
    //                            .Returns((OrderRequestInfo ori, string dummyErrorParameter) =>
    //                            {
    //                                var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
    //                                integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
    //                                                           .Returns(ori);
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.ParentSession)
    //                                                           .Returns(orderFlowSessionMock.Object);
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
    //                                                           .Returns(string.Format("extOrd{0}",
    //                                                                                  Interlocked.Increment(
    //                                                                                      ref ordersCnt)));
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
    //                                                           .Returns(ori.SizeBaseAbsInitial);
    //                                return integratorOrderExternalMock.Object;
    //                            });


    //        orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);
    //        orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);

    //        priceBookStoreMock.Setup(pbs => pbs.GetChangeableAskPriceBook(It.IsAny<Symbol>())).Returns(priceBookMock.Object);
    //        priceBookStoreMock.Setup(pbs => pbs.GetChangeableBidPriceBook(It.IsAny<Symbol>())).Returns(priceBookMock.Object);
    //        priceBookMock.Setup(pb => pb.GetIdentical(It.IsAny<PriceObject>())).Returns((PriceObject po) => po);

    //        ILogger testLogger = new TestLogger(Assert.Fail);

    //        var comgr = new CompositeOrdersManager(testLogger, priceBookStoreMock.Object, orderFlowSessionsListMock.Object,
    //                                               _nullRiskManager, orderManagerMock.Object);

    //        PriceObject price1 = new NoIdentityQuote(1.5m, 1000, PriceSide.Ask, Symbol.EUR_SEK, Counterparty.NOM, "SubId", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice;
    //        PriceObject price2 = new NoIdentityQuote(3.5m, 1000, PriceSide.Bid, Symbol.EUR_USD, Counterparty.NOM, "SubId", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice;

    //        BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateQuoted(200, 3, DealDirection.Buy, price1);
    //        BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateQuoted(200, 3, DealDirection.Sell, price2);
    //        info2.MaximumAcceptedPriceAge = TimeSpan.FromMilliseconds(-1);

    //        IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateCompositeClientOrder(new List<BankPoolClientOrderRequestInfo>() { info1, info2 },
    //                                                    CompositeOrderStrategy.AllAtOnceOrImmediateAtMarket);

    //        comgr.RegisterCompositeOrder(clientOrder1, dispatchGatewayMock.Object);

    //        dispatchGatewayMock.Verify(
    //            gateway =>
    //            gateway.OnIntegratorClientOrderUpdate(
    //                It.Is<ClientOrderUpdateInfo>(
    //                    info =>
    //                    info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
    //                    info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_SEK),
    //            Times.Exactly(1));

    //        dispatchGatewayMock.Verify(
    //            gateway =>
    //            gateway.OnIntegratorClientOrderUpdate(
    //                It.Is<ClientOrderUpdateInfo>(
    //                    info =>
    //                    info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
    //                    info.OrderStatus == ClientOrderStatus.RejectedByIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_SEK),
    //            Times.Exactly(1));

    //        orderFlowSessionMock.Verify(of => of.SubmitOrder(It.IsAny<IIntegratorOrderExternal>()), Times.Never());
    //    }


    //    [Test]
    //    public void CompositeOrdersManager_RegisterCompositeOrderTest_SuccessfullyFilled_WillInformClient()
    //    {
    //        var priceBookStoreMock = new Mock<IPriceBookStore>();
    //        var priceBookMock = new Mock<IChangeablePriceBook<PriceObject, Counterparty>>();
    //        var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
    //        var orderManagerMock = new Mock<IOrderManagement>();
    //        var dispatchGatewayMock = new Mock<IUnicastInfoForwarder>();

    //        var externalOrders = new List<Mock<IIntegratorOrderExternal>>();
    //        var orderFlowSessionMock = new Mock<IOrderFlowSession>();
    //        int ordersCnt = 0;
    //        orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), out _dummyErrorParameter))
    //                            .Returns((OrderRequestInfo ori, string dummyErrorParameter) =>
    //                            {
    //                                var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
    //                                integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
    //                                                           .Returns(ori);
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.ParentSession)
    //                                                           .Returns(orderFlowSessionMock.Object);
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
    //                                                           .Returns(string.Format("extOrd{0}",
    //                                                                                  Interlocked.Increment(
    //                                                                                      ref ordersCnt)));
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
    //                                                           .Returns(ori.SizeBaseAbsInitial);
    //                                externalOrders.Add(integratorOrderExternalMock);
    //                                return integratorOrderExternalMock.Object;
    //                            });
    //        orderFlowSessionMock.Setup(of => of.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(true);
    //        orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);


    //        orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);
    //        orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);

    //        priceBookStoreMock.Setup(pbs => pbs.GetChangeableAskPriceBook(It.IsAny<Symbol>())).Returns(priceBookMock.Object);
    //        priceBookStoreMock.Setup(pbs => pbs.GetChangeableBidPriceBook(It.IsAny<Symbol>())).Returns(priceBookMock.Object);
    //        priceBookMock.Setup(pb => pb.GetIdentical(It.IsAny<PriceObject>())).Returns((PriceObject po) => po);

    //        ILogger testLogger = new TestLogger(Assert.Fail);

    //        var comgr = new CompositeOrdersManager(testLogger, priceBookStoreMock.Object, orderFlowSessionsListMock.Object,
    //                                               _nullRiskManager, orderManagerMock.Object);

    //        PriceObject price1 = new NoIdentityQuote(1.5m, 1000, PriceSide.Ask, Symbol.EUR_SEK, Counterparty.NOM, "SubId", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice;
    //        PriceObject price2 = new NoIdentityQuote(3.5m, 1000, PriceSide.Bid, Symbol.EUR_USD, Counterparty.NOM, "SubId", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice;

    //        BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateQuoted(200, 3, DealDirection.Buy, price1);
    //        BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateQuoted(200, 3, DealDirection.Sell, price2);

    //        IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateCompositeClientOrder(new List<BankPoolClientOrderRequestInfo>() { info1, info2 },
    //                                                    CompositeOrderStrategy.AllAtOnceOrImmediateAtMarket);

    //        comgr.RegisterCompositeOrder(clientOrder1, dispatchGatewayMock.Object);

    //        dispatchGatewayMock.Verify(
    //            gateway =>
    //            gateway.OnIntegratorClientOrderUpdate(
    //                It.Is<ClientOrderUpdateInfo>(
    //                    info =>
    //                    info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
    //                    info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_SEK),
    //            Times.Exactly(1));

    //        dispatchGatewayMock.Verify(
    //            gateway =>
    //            gateway.OnIntegratorClientOrderUpdate(
    //                It.Is<ClientOrderUpdateInfo>(
    //                    info =>
    //                    info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
    //                    info.OrderStatus == ClientOrderStatus.RejectedByIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_SEK),
    //            Times.Never());

    //        //Now let's receive the exec report
    //        externalOrders[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
    //                                          externalOrders[0].Object,
    //                                          new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
    //                                              new ExecutionInfo(
    //                                                  externalOrders[0].Object.OrderRequestInfo.SizeBaseAbsInitial,
    //                                                  externalOrders[0].Object.OrderRequestInfo.RequestedPrice,
    //                                                  null, null, null),
    //                                                  null));

    //        dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
    //            deal =>
    //            deal.ClientOrderIdentity == clientOrder1.ChildOrders[0].ClientOrderIdentity &&
    //            deal.ParentClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 200 &&
    //            deal.Price == 1.5m)), Times.Exactly(1));


    //        //And the other exec report
    //        externalOrders[1].Raise(ordeExt => ordeExt.OnOrderChanged += null,
    //                                          externalOrders[1].Object,
    //                                          new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
    //                                              new ExecutionInfo(
    //                                                  externalOrders[1].Object.OrderRequestInfo.SizeBaseAbsInitial,
    //                                                  externalOrders[1].Object.OrderRequestInfo.RequestedPrice,
    //                                                  null, null, null),
    //                                                  null));

    //        dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
    //            deal =>
    //            deal.ClientOrderIdentity == clientOrder1.ChildOrders[1].ClientOrderIdentity &&
    //            deal.ParentClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 200 &&
    //            deal.Price == 3.5m)), Times.Exactly(1));
    //    }

    //    [Test]
    //    public void CompositeOrdersManager_RegisterCompositeOrderTest_OneOrderRejected_WillTransformToMarket()
    //    {
    //        var priceBookStoreMock = new Mock<IPriceBookStore>();
    //        var priceBookMock = new Mock<IChangeablePriceBook<PriceObject, Counterparty>>();
    //        var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
    //        var orderManagerMock = new Mock<IOrderManagement>();
    //        var dispatchGatewayMock = new Mock<IUnicastInfoForwarder>();

    //        var externalOrders = new List<Mock<IIntegratorOrderExternal>>();
    //        var orderFlowSessionMock = new Mock<IOrderFlowSession>();
    //        int ordersCnt = 0;
    //        orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), out _dummyErrorParameter))
    //                            .Returns((OrderRequestInfo ori, string dummyErrorParameter) =>
    //                            {
    //                                var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
    //                                integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
    //                                                           .Returns(ori);
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.ParentSession)
    //                                                           .Returns(orderFlowSessionMock.Object);
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
    //                                                           .Returns(string.Format("extOrd{0}",
    //                                                                                  Interlocked.Increment(
    //                                                                                      ref ordersCnt)));
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
    //                                                           .Returns(ori.SizeBaseAbsInitial);
    //                                externalOrders.Add(integratorOrderExternalMock);
    //                                return integratorOrderExternalMock.Object;
    //                            });
    //        orderFlowSessionMock.Setup(of => of.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(true);
    //        orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);


    //        orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);
    //        orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);

    //        priceBookStoreMock.Setup(pbs => pbs.GetChangeableAskPriceBook(It.IsAny<Symbol>())).Returns(priceBookMock.Object);
    //        priceBookStoreMock.Setup(pbs => pbs.GetChangeableBidPriceBook(It.IsAny<Symbol>())).Returns(priceBookMock.Object);
    //        priceBookMock.Setup(pb => pb.GetIdentical(It.IsAny<PriceObject>())).Returns((PriceObject po) => po);

    //        ILogger testLogger = new TestLogger(Assert.Fail);

    //        var comgr = new CompositeOrdersManager(testLogger, priceBookStoreMock.Object, orderFlowSessionsListMock.Object,
    //                                               _nullRiskManager, orderManagerMock.Object);

    //        PriceObject price1 = new NoIdentityQuote(1.5m, 1000, PriceSide.Ask, Symbol.EUR_SEK, Counterparty.NOM, "SubId", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice;
    //        PriceObject price2 = new NoIdentityQuote(3.5m, 1000, PriceSide.Bid, Symbol.EUR_USD, Counterparty.NOM, "SubId", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice;

    //        BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateQuoted(200, 3, DealDirection.Buy, price1);
    //        BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateQuoted(300, 3, DealDirection.Sell, price2);

    //        IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateCompositeClientOrder(new List<BankPoolClientOrderRequestInfo>() { info1, info2 },
    //                                                    CompositeOrderStrategy.AllAtOnceOrImmediateAtMarket);

    //        comgr.RegisterCompositeOrder(clientOrder1, dispatchGatewayMock.Object);

    //        dispatchGatewayMock.Verify(
    //            gateway =>
    //            gateway.OnIntegratorClientOrderUpdate(
    //                It.Is<ClientOrderUpdateInfo>(
    //                    info =>
    //                    info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
    //                    info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_SEK),
    //            Times.Exactly(1));

    //        dispatchGatewayMock.Verify(
    //            gateway =>
    //            gateway.OnIntegratorClientOrderUpdate(
    //                It.Is<ClientOrderUpdateInfo>(
    //                    info =>
    //                    info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
    //                    info.OrderStatus == ClientOrderStatus.RejectedByIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_SEK),
    //            Times.Never());

    //        //Now let's receive the exec report
    //        externalOrders[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
    //                                          externalOrders[0].Object,
    //                                          new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
    //                                              new ExecutionInfo(
    //                                                  externalOrders[0].Object.OrderRequestInfo.SizeBaseAbsInitial,
    //                                                  externalOrders[0].Object.OrderRequestInfo.RequestedPrice,
    //                                                  null, null, null),
    //                                                  null));

    //        dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
    //            deal =>
    //            deal.ClientOrderIdentity == clientOrder1.ChildOrders[0].ClientOrderIdentity &&
    //            deal.ParentClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 200 &&
    //            deal.Price == 1.5m)), Times.Exactly(1));


    //        externalOrders[1].Raise(ordeExt => ordeExt.OnOrderChanged += null,
    //                                          externalOrders[1].Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected, null,
    //                                              new RejectionInfo(null, externalOrders[1].Object.OrderRequestInfo.SizeBaseAbsInitial, RejectionType.OrderReject)));

    //        //orderManagerMock.Verify(
    //        //    om =>
    //        //    om.RegisterOrder(
    //        //        It.Is<IBankPoolClientOrder>(
    //        //            ord =>
    //        //            ord.ClientIdentity == clientOrder1.ChildOrders[1].ClientIdentity &&
    //        //            ord.OrderIdentity == clientOrder1.ChildOrders[1].OrderIdentity &&
    //        //            ord.OrderRequestInfo.TotalAmountBaseAbs == 300 &&
    //        //            ord.OrderRequestInfo.OrderType == OrderType.Market &&
    //        //            ord.OrderRequestInfo.Side == DealDirection.Sell && ord.OrderRequestInfo.Symbol == Symbol.EUR_USD),
    //        //        comgr, clientOrder1),
    //        //    Times.Exactly(1));

    //        orderManagerMock.Verify(
    //            om =>
    //            om.RegisterOrder(
    //                It.Is<IBankPoolClientOrder>(
    //                    ord =>
    //                    ord.ClientIdentity == clientOrder1.ChildOrders[1].ClientIdentity &&
    //                    ord.ClientOrderIdentity == clientOrder1.ChildOrders[1].ClientOrderIdentity &&
    //                    ord.OrderRequestInfo.SizeBaseAbsInitial == 300 &&
    //                    ord.OrderRequestInfo.OrderType == OrderType.Market &&
    //                    ord.OrderRequestInfo.Side == DealDirection.Sell && ord.OrderRequestInfo.Symbol == Symbol.EUR_USD),
    //                comgr,
    //                It.Is<IBankPoolClientOrder>(
    //                    ord =>
    //                    ord.IsComposite && ord.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
    //                    ord.ChildOrders[0].OrderRequestInfo == clientOrder1.ChildOrders[0].OrderRequestInfo
    //                //This will not be true - as we deep-cloned the original object so that we don't change it under client hands
    //                /*&&
    //                ord.ChildOrders[1].OrderRequestInfo == clientOrder1.ChildOrders[1].OrderRequestInfo*/)),
    //            Times.Exactly(1));


    //        //and now fill the market order

    //        comgr.OnIntegratorDealInternal(new IntegratorDealInternal(clientOrder1.ChildOrders[1].ClientOrderIdentity,
    //                                                   clientOrder1.ClientOrderIdentity, clientOrder1.ClientIdentity, string.Empty, string.Empty, string.Empty, 300, true, 0.5m, DealDirection.Sell,
    //                                                   Symbol.EUR_USD, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, Counterparty.NOM, null, null, TradingTargetType.BankPool, null, null));

    //        dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
    //            deal =>
    //            deal.ClientOrderIdentity == clientOrder1.ChildOrders[1].ClientOrderIdentity &&
    //            deal.ParentClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 300 &&
    //            deal.Price == 0.5m && !deal.IsPrimaryDealFromQuoted.Value)), Times.Exactly(1));
    //    }


    //    [Test]
    //    public void CompositeOrdersManager_RegisterCompositeOrderTest_OneOrderRejected_WillTransformToMarketAndAcceptMultipleFills()
    //    {
    //        var priceBookStoreMock = new Mock<IPriceBookStore>();
    //        var priceBookMock = new Mock<IChangeablePriceBook<PriceObject, Counterparty>>();
    //        var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
    //        var orderManagerMock = new Mock<IOrderManagement>();
    //        var dispatchGatewayMock = new Mock<IUnicastInfoForwarder>();

    //        var externalOrders = new List<Mock<IIntegratorOrderExternal>>();
    //        var orderFlowSessionMock = new Mock<IOrderFlowSession>();
    //        int ordersCnt = 0;
    //        orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), out _dummyErrorParameter))
    //                            .Returns((OrderRequestInfo ori, string dummyErrorParameter) =>
    //                            {
    //                                var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
    //                                integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
    //                                                           .Returns(ori);
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.ParentSession)
    //                                                           .Returns(orderFlowSessionMock.Object);
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
    //                                                           .Returns(string.Format("extOrd{0}",
    //                                                                                  Interlocked.Increment(
    //                                                                                      ref ordersCnt)));
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
    //                                                           .Returns(ori.SizeBaseAbsInitial);
    //                                externalOrders.Add(integratorOrderExternalMock);
    //                                return integratorOrderExternalMock.Object;
    //                            });
    //        orderFlowSessionMock.Setup(of => of.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(true);
    //        orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);


    //        orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);
    //        orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);

    //        priceBookStoreMock.Setup(pbs => pbs.GetChangeableAskPriceBook(It.IsAny<Symbol>())).Returns(priceBookMock.Object);
    //        priceBookStoreMock.Setup(pbs => pbs.GetChangeableBidPriceBook(It.IsAny<Symbol>())).Returns(priceBookMock.Object);
    //        priceBookMock.Setup(pb => pb.GetIdentical(It.IsAny<PriceObject>())).Returns((PriceObject po) => po);

    //        ILogger testLogger = new TestLogger(Assert.Fail);

    //        var comgr = new CompositeOrdersManager(testLogger, priceBookStoreMock.Object, orderFlowSessionsListMock.Object,
    //                                               _nullRiskManager, orderManagerMock.Object);

    //        PriceObject price1 = new NoIdentityQuote(1.5m, 1000, PriceSide.Ask, Symbol.EUR_SEK, Counterparty.NOM, "SubId", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice;

    //        BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateQuoted(200, 3, DealDirection.Buy, price1);

    //        IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateCompositeClientOrder(new List<BankPoolClientOrderRequestInfo>() { info1 },
    //                                                    CompositeOrderStrategy.AllAtOnceOrImmediateAtMarket);

    //        comgr.RegisterCompositeOrder(clientOrder1, dispatchGatewayMock.Object);

    //        dispatchGatewayMock.Verify(
    //            gateway =>
    //            gateway.OnIntegratorClientOrderUpdate(
    //                It.Is<ClientOrderUpdateInfo>(
    //                    info =>
    //                    info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
    //                    info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_SEK),
    //            Times.Exactly(1));

    //        dispatchGatewayMock.Verify(
    //            gateway =>
    //            gateway.OnIntegratorClientOrderUpdate(
    //                It.Is<ClientOrderUpdateInfo>(
    //                    info =>
    //                    info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
    //                    info.OrderStatus == ClientOrderStatus.RejectedByIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_SEK),
    //            Times.Never());

    //        externalOrders[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
    //                                          externalOrders[0].Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected, null,
    //                                              new RejectionInfo(null, externalOrders[0].Object.OrderRequestInfo.SizeBaseAbsInitial, RejectionType.OrderReject)));

    //        orderManagerMock.Verify(
    //            om =>
    //            om.RegisterOrder(
    //                It.Is<IBankPoolClientOrder>(
    //                    ord =>
    //                    ord.ClientIdentity == clientOrder1.ChildOrders[0].ClientIdentity &&
    //                    ord.ClientOrderIdentity == clientOrder1.ChildOrders[0].ClientOrderIdentity &&
    //                    ord.OrderRequestInfo.SizeBaseAbsInitial == 200 &&
    //                    ord.OrderRequestInfo.OrderType == OrderType.Market &&
    //                    ord.OrderRequestInfo.Side == DealDirection.Buy && ord.OrderRequestInfo.Symbol == Symbol.EUR_SEK),
    //                comgr,
    //                It.Is<IBankPoolClientOrder>(
    //                    ord =>
    //                    ord.IsComposite && ord.ClientOrderIdentity == clientOrder1.ClientOrderIdentity)),
    //            Times.Exactly(1));


    //        //and now partially fill the market order
    //        comgr.OnIntegratorDealInternal(new IntegratorDealInternal(clientOrder1.ChildOrders[0].ClientOrderIdentity,
    //                                                   clientOrder1.ClientOrderIdentity, clientOrder1.ClientIdentity, string.Empty, string.Empty, string.Empty, 150, false, 0.5m, DealDirection.Sell,
    //                                                   Symbol.EUR_USD, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, Counterparty.NOM, null, null, TradingTargetType.BankPool, null, null));

    //        dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
    //            deal =>
    //            deal.ClientOrderIdentity == clientOrder1.ChildOrders[0].ClientOrderIdentity &&
    //            deal.ParentClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 150 &&
    //            deal.Price == 0.5m && !deal.IsPrimaryDealFromQuoted.Value)), Times.Exactly(1));

    //        //and other partiall fill
    //        comgr.OnIntegratorDealInternal(new IntegratorDealInternal(clientOrder1.ChildOrders[0].ClientOrderIdentity,
    //                                                   clientOrder1.ClientOrderIdentity, clientOrder1.ClientIdentity, string.Empty, string.Empty, string.Empty, 50, true, 0.25m, DealDirection.Sell,
    //                                                   Symbol.EUR_USD, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, Counterparty.NOM, null, null, TradingTargetType.BankPool, null, null));

    //        dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
    //            deal =>
    //            deal.ClientOrderIdentity == clientOrder1.ChildOrders[0].ClientOrderIdentity &&
    //            deal.ParentClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 50 &&
    //            deal.Price == 0.25m && !deal.IsPrimaryDealFromQuoted.Value)), Times.Exactly(1));
    //    }


    //    [Test]
    //    public void CompositeOrdersManager_RegisterCompositeOrderWithImmediateOrKillStrategyTest_OneOrderRejected_WillNotBeProcessedFurther()
    //    {
    //        var priceBookStoreMock = new Mock<IPriceBookStore>();
    //        var priceBookMock = new Mock<IChangeablePriceBook<PriceObject, Counterparty>>();
    //        var orderFlowSessionsListMock = new Mock<IList<IOrderFlowSession>>();
    //        var orderManagerMock = new Mock<IOrderManagement>();
    //        var dispatchGatewayMock = new Mock<IUnicastInfoForwarder>();

    //        var externalOrders = new List<Mock<IIntegratorOrderExternal>>();
    //        var orderFlowSessionMock = new Mock<IOrderFlowSession>();
    //        int ordersCnt = 0;
    //        orderFlowSessionMock.Setup(ofs => ofs.GetNewExternalOrder(It.IsAny<OrderRequestInfo>(), out _dummyErrorParameter))
    //                            .Returns((OrderRequestInfo ori, string dummyErrorParameter) =>
    //                            {
    //                                var integratorOrderExternalMock = new Mock<IIntegratorOrderExternal>();
    //                                integratorOrderExternalMock.Setup(ordextern => ordextern.OrderRequestInfo)
    //                                                           .Returns(ori);
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.ParentSession)
    //                                                           .Returns(orderFlowSessionMock.Object);
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.Identity)
    //                                                           .Returns(string.Format("extOrd{0}",
    //                                                                                  Interlocked.Increment(
    //                                                                                      ref ordersCnt)));
    //                                integratorOrderExternalMock.Setup(ordeextern => ordeextern.FilledAmount)
    //                                                           .Returns(ori.SizeBaseAbsInitial);
    //                                externalOrders.Add(integratorOrderExternalMock);
    //                                return integratorOrderExternalMock.Object;
    //                            });
    //        orderFlowSessionMock.Setup(of => of.SubmitOrder(It.IsAny<IIntegratorOrderExternal>())).Returns(true);
    //        orderFlowSessionMock.Setup(ofs => ofs.Counterparty).Returns(Counterparty.CRS);


    //        orderFlowSessionsListMock.Setup(ofsl => ofsl.Count).Returns(Counterparty.ValuesCount);
    //        orderFlowSessionsListMock.Setup(ofsl => ofsl[It.IsAny<int>()]).Returns(orderFlowSessionMock.Object);

    //        priceBookStoreMock.Setup(pbs => pbs.GetChangeableAskPriceBook(It.IsAny<Symbol>())).Returns(priceBookMock.Object);
    //        priceBookStoreMock.Setup(pbs => pbs.GetChangeableBidPriceBook(It.IsAny<Symbol>())).Returns(priceBookMock.Object);
    //        priceBookMock.Setup(pb => pb.GetIdentical(It.IsAny<PriceObject>())).Returns((PriceObject po) => po);

    //        ILogger testLogger = new TestLogger(Assert.Fail);

    //        var comgr = new CompositeOrdersManager(testLogger, priceBookStoreMock.Object, orderFlowSessionsListMock.Object,
    //                                               _nullRiskManager, orderManagerMock.Object);

    //        PriceObject price1 = new NoIdentityQuote(1.5m, 1000, PriceSide.Ask, Symbol.EUR_SEK, Counterparty.NOM, "SubId", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).AskPrice;
    //        PriceObject price2 = new NoIdentityQuote(3.5m, 1000, PriceSide.Bid, Symbol.EUR_USD, Counterparty.NOM, "SubId", DateTime.UtcNow, MarketDataRecordType.BankQuoteData).BidPrice;

    //        BankPoolClientOrderRequestInfo info1 = OrderRequestInfoBuilder.Instance.CreateQuoted(200, 3, DealDirection.Buy, price1);
    //        BankPoolClientOrderRequestInfo info2 = OrderRequestInfoBuilder.Instance.CreateQuoted(300, 3, DealDirection.Sell, price2);

    //        IBankPoolClientOrder clientOrder1 = OrderRequestInfoBuilder.Instance.CreateCompositeClientOrder(new List<BankPoolClientOrderRequestInfo>() { info1, info2 },
    //                                                    CompositeOrderStrategy.AllAtOnceOrKill);

    //        comgr.RegisterCompositeOrder(clientOrder1, dispatchGatewayMock.Object);

    //        dispatchGatewayMock.Verify(
    //            gateway =>
    //            gateway.OnIntegratorClientOrderUpdate(
    //                It.Is<ClientOrderUpdateInfo>(
    //                    info =>
    //                    info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
    //                    info.OrderStatus == ClientOrderStatus.OpenInIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_SEK),
    //            Times.Exactly(1));

    //        dispatchGatewayMock.Verify(
    //            gateway =>
    //            gateway.OnIntegratorClientOrderUpdate(
    //                It.Is<ClientOrderUpdateInfo>(
    //                    info =>
    //                    info.ClientOrderIdentity == clientOrder1.ClientOrderIdentity &&
    //                    info.OrderStatus == ClientOrderStatus.RejectedByIntegrator && info.SizeBaseAbsCancelled == 0), Symbol.EUR_SEK),
    //            Times.Never());

    //        //Now let's receive the exec report
    //        externalOrders[0].Raise(ordeExt => ordeExt.OnOrderChanged += null,
    //                                          externalOrders[0].Object,
    //                                          new OrderChangeEventArgs(IntegratorOrderExternalChange.Filled,
    //                                              new ExecutionInfo(
    //                                                  externalOrders[0].Object.OrderRequestInfo.SizeBaseAbsInitial,
    //                                                  externalOrders[0].Object.OrderRequestInfo.RequestedPrice,
    //                                                  null, null, null),
    //                                                  null));

    //        dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.Is<IntegratorDealInternal>(
    //            deal =>
    //            deal.ClientOrderIdentity == clientOrder1.ChildOrders[0].ClientOrderIdentity &&
    //            deal.ParentClientOrderIdentity == clientOrder1.ClientOrderIdentity && deal.FilledAmountBaseAbs == 200 &&
    //            deal.Price == 1.5m)), Times.Exactly(1));


    //        //And the other exec report
    //        externalOrders[1].Raise(ordeExt => ordeExt.OnOrderChanged += null,
    //                                          externalOrders[1].Object, new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected, null,
    //                                              new RejectionInfo(null, externalOrders[1].Object.OrderRequestInfo.SizeBaseAbsInitial, RejectionType.OrderReject)));

    //        orderManagerMock.Verify(
    //            om =>
    //            om.RegisterOrder(
    //                It.IsAny<IBankPoolClientOrder>(),
    //                /*comgr*/It.IsAny<IUnicastInfoForwarder>(),
    //                It.IsAny<IBankPoolClientOrder>()),
    //            Times.Never());


    //        //and now fill the market order

    //        //this will internally error out
    //        comgr.OnIntegratorDealInternal(new IntegratorDealInternal(clientOrder1.ChildOrders[1].ClientOrderIdentity,
    //                                                   clientOrder1.ClientOrderIdentity, clientOrder1.ClientIdentity, string.Empty, string.Empty, string.Empty, 300, true, 0.5m, DealDirection.Sell,
    //                                                   Symbol.EUR_USD, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, Counterparty.NOM, null, null, TradingTargetType.BankPool, null, null));

    //        //No new deals invoking - this is just the first invoke
    //        dispatchGatewayMock.Verify(gateway => gateway.OnIntegratorDealInternal(It.IsAny<IntegratorDealInternal>()), Times.Exactly(1));
    //    }
    //}
}
