﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;


namespace Kreslik.Integrator.Common.Tests
{
    internal class TestingWorkItem
    {
        public ManualResetEvent MRE = new ManualResetEvent(false);

        public void DummyAction()
        {
            MRE.WaitOne();
        }
    }

    [TestFixture]
    public class ThreadPoolExTests
    {

        [Test]
        public void TimerTaskFlagUtils_AllBackAndForthConversions()
        {
            TimerTaskFlagUtils.TestSelf();
        }

        [Test]
        public void ThreadPoolEx_ScheduleWorkItem_Executed()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var counters = new DummyThreadPoolExPerformanceCounters();
            ThreadPoolEx threadPool = new ThreadPoolEx(1, 3, counters, testLogger);

            Assert.AreEqual(0, counters.QueuedTasksCount);
            Assert.AreEqual(2, counters.AvailableThreadsCount);
            Assert.AreEqual(2, counters.TotalThreadsCount);

            TestingWorkItem twe = new TestingWorkItem();
            threadPool.QueueWorkItem(new WorkItem(twe.DummyAction, TimerTaskFlagUtils.WorkItemPriority.Normal));
            Assert.AreEqual(1, counters.QueuedTasksCount);
            Assert.AreEqual(2, counters.TotalThreadsCount);
            Thread.Sleep(1);
            Assert.AreEqual(1, counters.AvailableThreadsCount);

            twe.MRE.Set();
            Thread.Sleep(1);

            Assert.AreEqual(0, counters.QueuedTasksCount);
            Assert.AreEqual(2, counters.AvailableThreadsCount);
            Assert.AreEqual(2, counters.TotalThreadsCount);
        }

        [Test]
        public void ThreadPoolEx_ScheduleMultipleHighPriWorkItem_AllExecuted()
        {
            ILogger testLogger = new TestLogger(Assert.Fail);
            var counters = new DummyThreadPoolExPerformanceCounters();
            ThreadPoolEx threadPool = new ThreadPoolEx(1, 4, counters, testLogger);

            Assert.AreEqual(0, counters.QueuedTasksCount);
            Assert.AreEqual(2, counters.AvailableThreadsCount);
            Assert.AreEqual(2, counters.TotalThreadsCount);

            TestingWorkItem twe1 = new TestingWorkItem();
            TestingWorkItem twe2 = new TestingWorkItem();
            TestingWorkItem twe3 = new TestingWorkItem();
            TestingWorkItem twe4 = new TestingWorkItem();
            threadPool.QueueWorkItem(new WorkItem(twe1.DummyAction, TimerTaskFlagUtils.WorkItemPriority.Highest));
            threadPool.QueueWorkItem(new WorkItem(twe2.DummyAction, TimerTaskFlagUtils.WorkItemPriority.Highest));
            threadPool.QueueWorkItem(new WorkItem(twe3.DummyAction, TimerTaskFlagUtils.WorkItemPriority.Highest));
            threadPool.QueueWorkItem(new WorkItem(twe4.DummyAction, TimerTaskFlagUtils.WorkItemPriority.Highest));
            //Assert.AreEqual(4, counters.QueuedTasksCount);
            Assert.AreEqual(4, counters.TotalThreadsCount);
            Thread.Sleep(1);
            Assert.AreEqual(0, counters.AvailableThreadsCount);

            twe1.MRE.Set();
            twe2.MRE.Set();
            twe3.MRE.Set();
            twe4.MRE.Set();
            Thread.Sleep(1);

            Assert.AreEqual(0, counters.QueuedTasksCount);
            Assert.AreEqual(4, counters.AvailableThreadsCount);
            Assert.AreEqual(4, counters.TotalThreadsCount);
        }
    }
}
