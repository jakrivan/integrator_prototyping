CREATE FUNCTION [dbo].[GetExternalOrdersTransmissionSwitchStateLastChangeDescription] 
(@SwitchId INT)
RETURNS NVARCHAR(MAX)
AS
BEGIN

	-- Declare the return variable here
	DECLARE @ChangeDescription NVARCHAR(MAX)
	SET @ChangeDescription = ''

	
	SELECT TOP 1
		@ChangeDescription = ('Order Transmission changed to: ' + CASE WHEN [TransmissionEnabled] = 1 THEN '''ON''' ELSE '''OFF''' END + ' on ' + CONVERT(VARCHAR, [Changed], 120) + ' UTC by ' + [ChangedBy] + ' from: [' + [ChangedFrom] + ']. Reason: [' + ISNULL([Reason], '<NOT SPECIFIED>') + ']')
	FROM 
		[dbo].[ExternalOrdersTransmissionSwitchHistory]
	WHERE
		[ExternalOrdersTransmissionSwitchId] = @SwitchId
	ORDER BY
		[Changed] DESC

	-- Return the result of the function
	RETURN @ChangeDescription
END
GO


ALTER TABLE [dbo].[ExternalOrdersTransmissionSwitch] ADD SwitchFriendlyName NVARCHAR(64) NOT NULL DEFAULT('NAME NOT SPECIFIED') 
GO

UPDATE [dbo].[ExternalOrdersTransmissionSwitch] SET [SwitchFriendlyName] = 'PrimeBroker' WHERE [SwitchName] = 'PrimebrokerKillSwitch'
GO
UPDATE [dbo].[ExternalOrdersTransmissionSwitch] SET [SwitchFriendlyName] = 'Manual' WHERE [SwitchName] = 'KGTManualKillSwitch'
GO
UPDATE [dbo].[ExternalOrdersTransmissionSwitch] SET [SwitchFriendlyName] = 'IntegratorAuto' WHERE [SwitchName] = 'KGTIntegratorKillSwitch'
GO


CREATE FUNCTION [dbo].[GetExternalOrdersTransmissionOverviewString] 
(@ChangedSwitchId INT)
RETURNS NVARCHAR(MAX)
AS
BEGIN

	DECLARE @Tab CHAR = CHAR(9)
	DECLARE @CR CHAR = CHAR(13)
	DECLARE @LF CHAR = CHAR(10)
	DECLARE @SpacesPadding CHAR(17) = '                 '

	DECLARE @Result NVARCHAR(MAX) = 'One of the Order Transmission switches changed its state:' + @CR + @LF + @CR + @LF 
	
	SELECT 
		TOP 1 @Result = @Result + SwitchFriendlyName + ' ' + [dbo].[GetExternalOrdersTransmissionSwitchStateLastChangeDescription](@ChangedSwitchId)
	FROM 
		[dbo].[ExternalOrdersTransmissionSwitch]
	WHERE
		[Id] = @ChangedSwitchId
	
	
	SET @Result = @Result + @CR + @LF + @CR + @LF + @CR + @LF + '===> General Order Transmission is ' + CASE WHEN [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]() = 1 THEN 'ON ' ELSE 'OFF' END + ' <==='


	SET @Result = @Result + @CR + @LF + @CR + @LF + @CR + @LF + 'Current states of all Order Transmission switches:'


	SET @Result = @Result + @CR + @LF + @CR + @LF + N'---------------------------------------------------------------------'


	SELECT TOP 100 
		@Result = @Result + @CR + @LF + 'Switch Name: ' + [SwitchFriendlyName]
		,@Result = @Result + @CR + @LF + 'Order Transmission Status: ' + CASE WHEN [TransmissionEnabled] = 1 THEN 'ON ' ELSE 'OFF' END
		,@Result = @Result + @CR + @LF + 'Last Change: ' + [dbo].[GetExternalOrdersTransmissionSwitchStateLastChangeDescription](Id) +  + @CR + @LF + N'---------------------------------------------------------------------'
	FROM 
		[dbo].[ExternalOrdersTransmissionSwitch] switch

	-- Return the result of the function
	RETURN @Result
END
GO

--WARNING!!
-- This is very important! As otherwise the impersonation of trigger below ends when crossing database boundary
ALTER DATABASE Integrator_Testing SET TRUSTWORTHY ON
GO
ALTER DATABASE Integrator SET TRUSTWORTHY ON
GO


CREATE TRIGGER [dbo].[ExternalOrdersTransmissionSwitch_Changing] ON [dbo].[ExternalOrdersTransmissionSwitch]
--needed to be able to execute sp_send_dbmail
WITH EXECUTE AS 'dbo'
AFTER INSERT, UPDATE
AS

BEGIN

	--we need to store the Id before commiting, as afterwards the inserted table will be empty
	DECLARE @updatedSwitchId INT
	SELECT TOP 1 @updatedSwitchId = [Id] FROM inserted

	-- commit the internal insert transaction to prevent problems during inserting critical data
	-- and also to make sure that following functions has fresh data
	COMMIT TRANSACTION

	--Extremely important to perform operation in try - catch to make sure that error happens just once
	begin try
	
        DECLARE @subject NVARCHAR(255) = 'OrderTrans ' + CASE WHEN [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]() = 1 THEN 'ON (' ELSE 'OFF (' END

		SELECT 
			@subject = @subject + [SwitchFriendlyName] + ' ' + CASE WHEN [TransmissionEnabled] = 1 THEN 'ON)' ELSE 'OFF)' END
		FROM
			[dbo].[ExternalOrdersTransmissionSwitch]
		WHERE
			[Id] = @updatedSwitchId

		DECLARE @body NVARCHAR(MAX) = [dbo].[GetExternalOrdersTransmissionOverviewString](@updatedSwitchId)

		EXEC msdb.dbo.sp_send_dbmail
				@recipients = 'support@kgtinv.com', 
				@profile_name = 'SQL Mail Profile',
				@subject = @subject,
				@body = @body;
    end try
    begin catch
        --select 0;
    end catch;

	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION

END
GO