BEGIN TRANSACTION Install_26_9_001;
GO

CREATE TABLE [dbo].[PriceDelaySettings](
	[CounterpartyId] [tinyint] NOT NULL,
	[ExpectedDelayTicks] [bigint] NOT NULL,
	[MaxAllowedAdditionalDelayTicks] [bigint] NOT NULL,
	[ExpectedDelay]  AS (dateadd(nanosecond,[ExpectedDelayTicks]*100,CONVERT([time](7),'00:00:00'))),
	[MaxAllowedAdditionalDelay]  AS (dateadd(nanosecond,[MaxAllowedAdditionalDelayTicks]*100,CONVERT([time](7),'00:00:00')))
) ON [PRIMARY]

ALTER TABLE [dbo].[PriceDelaySettings]  WITH CHECK ADD  CONSTRAINT [FK_PriceDelaySettings_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[PriceDelaySettings] CHECK CONSTRAINT [FK_PriceDelaySettings_Counterparty]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_PriceDelaySettings_CounterpartyId] ON [dbo].[PriceDelaySettings]
(
	[CounterpartyId] ASC
) ON [PRIMARY]
GO


INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'BNP'), 4300, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'BOA'), 4500, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'CRS'), -2500, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'CTI'), 1900, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'CZB'), 2500, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'DBK'), 10000, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FA1'), 331500, 120000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FC1'), 700, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FC2'), 330300, 120000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'GLS'), -6500, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HSB'), 29700, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HT3'), 335100, 120000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTA'), 331600, 120000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTF'), 331600, 120000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'JPM'), 3200, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LM1'), 2300, 60000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LM2'), 2300, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LM3'), 2300, 60000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'MGS'), 1900, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'NOM'), 15200, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'RBS'), 2300, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'SOC'), 37400, 40000)
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'UBS'), 1400, 40000)
GO


CREATE TABLE [dbo].[PriceDelayStats](
	[CounterpartyId] [tinyint] NOT NULL,
	[TotalReceivedPricesInLastMinute] [bigint] NOT NULL,
	[DelayedPricesInLastMinute] [bigint] NOT NULL,
	[TotalReceivedPricesInLastHour] [bigint] NOT NULL,
	[DelayedPricesInLastHour] [bigint] NOT NULL,
	[LastMinuteDelayedPricesPercentage] AS CASE WHEN [TotalReceivedPricesInLastMinute] = 0 THEN 0 ELSE CAST([DelayedPricesInLastMinute] AS NUMERIC)/[TotalReceivedPricesInLastMinute]*100 END,
	[LastHourDelayedPricesPercentage] AS CASE WHEN [TotalReceivedPricesInLastHour] = 0 THEN 0 ELSE CAST([DelayedPricesInLastHour] AS NUMERIC)/[TotalReceivedPricesInLastHour]*100 END
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PriceDelayStats]  WITH CHECK ADD  CONSTRAINT [FK_PriceDelayStats_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[PriceDelayStats] CHECK CONSTRAINT [FK_PriceDelayStats_Counterparty]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_PriceDelayStats_CounterpartyId] ON [dbo].[PriceDelayStats]
(
	[CounterpartyId] ASC
) ON [PRIMARY]
GO

INSERT INTO [dbo].[PriceDelayStats] ([CounterpartyId], [TotalReceivedPricesInLastMinute], [DelayedPricesInLastMinute], [TotalReceivedPricesInLastHour], [DelayedPricesInLastHour])
SELECT [CounterpartyId], 0, 0, 0, 0 FROM [dbo].[Counterparty]
GO

CREATE PROCEDURE [dbo].[GetPriceDelaySettings_SP] 
AS
BEGIN
	SELECT 
		ctp.[CounterpartyCode] AS Counterparty,
		[ExpectedDelayTicks],
		[MaxAllowedAdditionalDelayTicks]
	FROM 
		[dbo].[PriceDelaySettings] sett
		INNER JOIN [dbo].[Counterparty] ctp ON sett.[CounterpartyId] = ctp.[CounterpartyId]
END
GO

GRANT EXECUTE ON [dbo].[GetPriceDelaySettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


CREATE FUNCTION [dbo].[csvlist_to_codesandintquadruple_tbl] (@list varchar(MAX))
   RETURNS @tbl TABLE (code varchar(3) NOT NULL, num1 int NOT NULL, num2 int NOT NULL, num3 int NOT NULL, num4 int NOT NULL) AS
BEGIN
	-- Sample usage: [dbo].[csvlist_to_codesandintquadruple_tbl]('ARG:45,65,6565,45,BGZ:1,22,333,4')

	IF(@list IS NULL OR len(@list) = 0)
	BEGIN
		RETURN
	END
	
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1, @valuelen = 0

   WHILE @nextpos > 0 AND @nextpos <> len(@list)
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1

	  IF @nextpos > @pos + 1
	  BEGIN

	      DECLARE @code Varchar(3) = substring(@list, @pos + 1, 3)
		  SELECT @pos = @pos+4, @valuelen = @valuelen-4
	  
		  DECLARE @num1 int = CONVERT(INT, substring(@list, @pos + 1, @valuelen))
		  SELECT @pos = @nextpos

		  SELECT @nextpos = charindex(',', @list, @pos + 1)
		  SELECT @valuelen = CASE WHEN @nextpos > 0
								  THEN @nextpos
								  ELSE len(@list) + 1
							 END - @pos - 1

		  DECLARE @num2 int = CONVERT(int, substring(@list, @pos + 1, @valuelen))
		  SELECT @pos = @nextpos
		  
		  SELECT @nextpos = charindex(',', @list, @pos + 1)
		  SELECT @valuelen = CASE WHEN @nextpos > 0
								  THEN @nextpos
								  ELSE len(@list) + 1
							 END - @pos - 1

		  DECLARE @num3 int = CONVERT(int, substring(@list, @pos + 1, @valuelen))
		  SELECT @pos = @nextpos
		  
		  SELECT @nextpos = charindex(',', @list, @pos + 1)
		  SELECT @valuelen = CASE WHEN @nextpos > 0
								  THEN @nextpos
								  ELSE len(@list) + 1
							 END - @pos - 1

		  DECLARE @num4 int = CONVERT(int, substring(@list, @pos + 1, @valuelen))

		  INSERT @tbl (code,num1, num2, num3, num4)
			 VALUES (@code, @num1, @num2, @num3, @num4)
	  END
	  ELSE IF @nextpos <= 0
	  BEGIN
		select @num2 = cast('Invalid csv int pairs list ' + @list as int)
	  END
	  SELECT @pos = @nextpos
	END
   RETURN
END
GO


CREATE PROCEDURE [dbo].[UpdatePriceDelayStats_SP] 
(
	@CsvStatsArg VARCHAR(MAX)
)
AS
BEGIN

	UPDATE
		stats
	SET
		stats.[TotalReceivedPricesInLastMinute] = statsArg.num1,
		stats.[DelayedPricesInLastMinute] = statsArg.num2,
		stats.[TotalReceivedPricesInLastHour] = statsArg.num3,
		stats.[DelayedPricesInLastHour] = statsArg.num4
	FROM
		[dbo].[csvlist_to_codesandintquadruple_tbl](@CsvStatsArg) statsArg
		INNER JOIN [dbo].[Counterparty] ctp on ctp.[CounterpartyCode] = statsArg.Code
		INNER JOIN [dbo].[PriceDelayStats] stats on ctp.[CounterpartyId] = stats.[CounterpartyId] 
END
GO

GRANT EXECUTE ON [dbo].[UpdatePriceDelayStats_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


--ROLLBACK TRANSACTION Install_26_9_001;
--GO
--COMMIT TRANSACTION Install_26_9_001;
--GO