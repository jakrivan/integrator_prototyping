
DELETE FROM [dbo].[CounterpartySymbolSettings]
WHERE TradingTargetTypeId = 2



INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'AUDCAD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.001 FROM [dbo].[FxPair] where [FxpCode] = 'AUDJPY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'AUDNZD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'AUDUSD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.001 FROM [dbo].[FxPair] where [FxpCode] = 'CHFJPY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURAUD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURCAD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURCHF'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.0001 FROM [dbo].[FxPair] where [FxpCode] = 'EURCZK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURDKK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURGBP'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.001 FROM [dbo].[FxPair] where [FxpCode] = 'EURHUF'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.001 FROM [dbo].[FxPair] where [FxpCode] = 'EURJPY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURNOK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURNZD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURPLN'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURSEK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURUSD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'GBPAUD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'GBPCAD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'GBPCHF'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.001 FROM [dbo].[FxPair] where [FxpCode] = 'GBPJPY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'GBPUSD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'NOKSEK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'NZDUSD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDCAD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDCHF'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.0001 FROM [dbo].[FxPair] where [FxpCode] = 'USDCZK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDDKK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDHKD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.001 FROM [dbo].[FxPair] where [FxpCode] = 'USDHUF'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDILS'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.001 FROM [dbo].[FxPair] where [FxpCode] = 'USDJPY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDMXN'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDNOK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDPLN'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDRUB'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDSEK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDSGD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDTRY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDZAR'



INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.0001 FROM [dbo].[FxPair] where [FxpCode] = 'USDINR'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 0, [FxPairId], 1, 0.01, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDINR'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 2, [FxPairId], 1, 0.0001, 0.0001 FROM [dbo].[FxPair] where [FxpCode] = 'CHFMXN'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 0, [FxPairId], 1, 0.001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'CHFMXN'