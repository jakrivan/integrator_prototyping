
print 'Started:'
print GETUTCDATE()
GO

Alter Database FXTickDB Add FileGroup MarketDataFileGroup
GO

Alter Database FXTickDB Add File
(
Name=MarketDataFileGroup_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\MarketDataFileGroup_File01.ndf',
Size=5GB,
FileGrowth=10%
) To Filegroup MarketDataFileGroup
GO

print 'FileGroup added:'
print GETUTCDATE()
GO

CREATE PARTITION SCHEME MarketDataSymbolPartitionScheme_AnotherFileGroup
AS PARTITION MarketDataSymbolPartitioningFunction ALL TO ([MarketDataFileGroup]) 
GO


sp_rename MarketData, MarketData_unpartitioned
GO

DROP INDEX [IX_IntegratorPriceIdentity] ON [dbo].[MarketData_unpartitioned]
GO

print 'Unclustered index dropped:'
print GETUTCDATE()
GO

CREATE CLUSTERED INDEX IX_PairTime ON MarketData_unpartitioned
(
	[FXPairId] ASC,
	[IntegratorReceivedTimeUtc] ASC
)
WITH (DROP_EXISTING = ON)
ON MarketDataSymbolPartitionScheme_AnotherFileGroup(FXPairId)
GO

print 'Clustered index moved:'
print GETUTCDATE()
GO

CREATE NONCLUSTERED INDEX [IX_IntegratorPriceIdentity] ON [dbo].[MarketData_unpartitioned]
(
	[IntegratorPriceIdentity] ASC
) ON MarketDataSymbolPartitionScheme_AnotherFileGroup(FXPairId)
GO

print 'Unclustered index recreated:'
print GETUTCDATE()
GO

sp_rename MarketData_unpartitioned, MarketData

print 'DONE'