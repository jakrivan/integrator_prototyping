

UPDATE [dbo].[TableChangeLog] 
SET TableName = 'VenueCrossSettings'
WHERE TableName = 'LmaxCrossSettings'

exec sp_rename '[LmaxCrossSettings]', 'VenueCrossSettings'
GO

ALTER TRIGGER [dbo].[LastUpdatedTrigger_LmaxCrossSettings] ON [dbo].[VenueCrossSettings]
AFTER INSERT, UPDATE
AS

BEGIN

--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[VenueCrossSettings] Set [LastUpdatedUtc] = GETUTCDATE() from [dbo].[VenueCrossSettings] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[SymbolId] = myAlias.[SymbolId] AND triggerInsertedTable.[CounterpartyId] = myAlias.[CounterpartyId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueCrossSettings'

END
GO

ALTER TABLE [dbo].[VenueCrossSettings] ADD CounterpartyId [tinyint] NULL
GO

UPDATE [dbo].[VenueCrossSettings] SET CounterpartyId = (SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LM1')
GO

ALTER TABLE [VenueCrossSettings] ALTER COLUMN [CounterpartyId] [tinyint] NOT NULL
GO

DROP INDEX [IX_LmaxCrossSettingsSymbolId] ON [dbo].[VenueCrossSettings] WITH ( ONLINE = OFF )
GO


CREATE UNIQUE CLUSTERED INDEX [IX_VenueCrossSettingsSymbolPerCounterparty] ON [dbo].[VenueCrossSettings]
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
) ON [PRIMARY]
GO


ALTER TABLE [dbo].[VenueCrossSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueCrossSettings_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO

ALTER TABLE [dbo].[VenueCrossSettings] CHECK CONSTRAINT [FK_VenueCrossSettings_Counterparty]
GO


INSERT INTO 
	[dbo].[VenueCrossSettings]
           ([SymbolId]
		   ,[CounterpartyId]
           ,[MinimumSizeToTrade]
           ,[MaximumSizeToTrade]
           ,[MinimumDiscountBasisPointsToTrade]
           ,[MinimumTimeBetweenTrades_seconds]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[Enabled]
           ,[LastResetRequestedUtc])
	SELECT 
		[Id]
		,(SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FA1')
		,10000
	  ,100000
	  ,0.25
	  ,1
	  ,0
	  ,0
	  ,NULL
	FROM 
		[dbo].[Symbol]
	WHERE [ShortName] IN
	(
		SELECT 
			pair.FxpCode
		FROM [FXtickDB].[dbo].[CounterpartySymbolSettings] enabledSyms
		INNER JOIN [FXtickDB].[dbo].[FxPair] pair on enabledSyms.SymbolId = pair.FxPairId
		WHERE 
			[TradingTargetTypeId] = (SELECT [Id] FROM [FXtickDB].[dbo].[TradingTargetType] WHERE Code = 'FXall')
			AND Supported = 1
	)
GO

exec sp_rename '[GetUpdateLmaxSystemSettings_SP]', 'GetUpdatedVenueSystemSettings_SP'
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueCrossSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenTrades_seconds]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[Enabled]
			,[LastResetRequestedUtc]
			,[LastUpdatedUtc]
		FROM 
			[dbo].[VenueCrossSettings] sett
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			AND (@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name 
	END
END
GO

exec sp_rename '[LmaxSystemSettingsDisableAll_SP]', 'VenueCrossSystemSettingsDisableAll_SP'
GO

ALTER PROCEDURE [dbo].[VenueCrossSystemSettingsDisableAll_SP]
(@CounterpartyCode [char](3))
AS
BEGIN
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	UPDATE
		[dbo].[VenueCrossSettings]
	SET
		[Enabled] = 0
	WHERE
		[CounterpartyId] = @CounterpartyId
END
GO

exec sp_rename '[LmaxSystemSettingsDisableEnableOne_SP]', 'VenueCrossSystemSettingsDisableEnableOne_SP'
GO

ALTER PROCEDURE [dbo].[VenueCrossSystemSettingsDisableEnableOne_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled Bit
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	UPDATE
		[dbo].[VenueCrossSettings]
	SET
		[Enabled] = @Enabled
	WHERE
		[SymbolId] = @SymbolId
		AND [CounterpartyId] = @CounterpartyId
END
GO

exec sp_rename '[LmaxSystemSettingsReset_SP]', 'VenueCrossSystemSettingsReset_SP'
GO

ALTER PROCEDURE [dbo].[VenueCrossSystemSettingsReset_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3)
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	UPDATE
		[dbo].[VenueCrossSettings]
	SET
		[LastResetRequestedUtc] = GETUTCDATE()
	WHERE
		[SymbolId] = @SymbolId
		AND [CounterpartyId] = @CounterpartyId
		AND [Enabled] = 0
END
GO

exec sp_rename '[LmaxSystemSettingsUpdateSingle_SP]', 'VenueCrossSystemSettingsUpdateSingle_SP'
GO

ALTER PROCEDURE [dbo].[VenueCrossSystemSettingsUpdateSingle_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@LastUpdateTimeUtc DATETIME2,
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenTrades_seconds [int],
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit]
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	UPDATE
		[dbo].[VenueCrossSettings]
	SET
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[MinimumDiscountBasisPointsToTrade] = @MinimumDiscountBasisPointsToTrade,
		[MinimumTimeBetweenTrades_seconds] = @MinimumTimeBetweenTrades_seconds,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds,
		[Enabled] = @Enabled
	WHERE
		[SymbolId] = @SymbolId
		AND [CounterpartyId] = @CounterpartyId
		AND [LastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO


ALTER TABLE [dbo].[OrderExternal] DROP COLUMN FixMessageSentParsed
GO

ALTER TABLE [dbo].[ExecutionReportMessage] DROP COLUMN FixMessageReceivedParsed
GO

ALTER TABLE [dbo].[OrderExternalCancelRequest] DROP COLUMN FixMessageSentParsed
GO

ALTER TABLE [dbo].[AggregatedExternalExecutedTicket] DROP COLUMN FixMessageParsed
GO

ALTER TABLE [dbo].[DealExternalExecutedTicket] DROP COLUMN FixMessageParsed
GO



ALTER PROCEDURE [dbo].[InsertNewOrderExternal_SP]( 
	@ExternalOrderSentUtc [datetime2](7),
	@InternalOrderId [nvarchar](45),
	@InternalClientOrderId [nvarchar](32),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@QuoteReceivedToOrderSentInternalLatency [time](7),
	@FixMessageSentRaw nvarchar(1024),
	@OrderTypeName varchar(16),
	@OrderTimeInForceName varchar(16),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,0),
	@SourceIntegratorPriceIdentity uniqueidentifier = NULL,
	@SourceIntegratorPriceTimeUtc [datetime2](7) = NULL,
	@PegTypeName varchar(16) = NULL,
	@PegDifferenceBasePol decimal(18,9) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @OrderTypeId TINYINT
	DECLARE @OrderTimeInForceId TINYINT
	DECLARE @PegTypeId BIT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	SELECT @OrderTypeId = [OrderTypeId] FROM [dbo].[OrderType] WHERE [OrderTypeName] = @OrderTypeName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderType not found in DB: %s', 16, 2, @OrderTypeName) WITH SETERROR
	END
	
	SELECT @OrderTimeInForceId = [OrderTimeInForceId] FROM [dbo].[OrderTimeInForce] WHERE [OrderTimeInForceName] = @OrderTimeInForceName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderTimeInForce not found in DB: %s', 16, 2, @OrderTimeInForceName) WITH SETERROR
	END
	
	IF @OrderTypeName = 'Pegged'
	BEGIN
		SELECT @PegTypeId = [PegTypeId] FROM [dbo].[PegType] WHERE [PegTypeName] = @PegTypeName
		
		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'PegType not found in DB: %s', 16, 2, @PegTypeName) WITH SETERROR
		END
		
		IF @PegDifferenceBasePol IS NULL
		BEGIN
			RAISERROR (N'PegDifferenceBasePol was not specified for pegged order - but it is compulsory', 16, 2) WITH SETERROR
		END
	END

	INSERT INTO 
		[dbo].[OrderExternal]
           ([IntegratorSentExternalOrderUtc]
           ,[IntegratorExternalOrderPrivateId]
		   ,[ExternalOrderLabelForCounterparty]
           ,[CounterpartyId]
           ,[SymbolId]
		   ,[DealDirectionId]
           ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[FixMessageSentRaw]
		   ,[OrderTypeId]
		   ,[OrderTimeInForceId]
		   ,[RequestedPrice]
		   ,[RequestedAmountBaseAbs]
		   ,[SourceIntegratorPriceIdentity]
		   ,[SourceIntegratorPriceTimeUtc]
		   ,[PegTypeId]
		   ,[PegDifferenceBasePol])
     VALUES
           (@ExternalOrderSentUtc
           ,@InternalOrderId
		   ,@InternalClientOrderId
           ,@CounterpartyId
           ,@SymbolId
		   ,@DealDirectionId
           ,@QuoteReceivedToOrderSentInternalLatency
		   ,@FixMessageSentRaw
		   ,@OrderTypeId
		   ,@OrderTimeInForceId
		   ,@RequestedPrice
		   ,@RequestedAmountBaseAbs
		   ,@SourceIntegratorPriceIdentity
		   ,@SourceIntegratorPriceTimeUtc
		   ,@PegTypeId
		   ,@PegDifferenceBasePol)
END
GO


ALTER PROCEDURE [dbo].[InsertExecutionReportMessage_SP]( 
	@ExecutionReportReceivedUtc [datetime2](7),
	@ExecutionReportStatus [varchar](16),
	@InternalOrderId [nvarchar](45),
	@ExecutionReportCounterpartySentUtc [datetime2](7),
	@RawFIXMessage [nvarchar](1024)
	)
AS
BEGIN

	DECLARE @ExecutionReportStatusId INT

	SELECT @ExecutionReportStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ExecutionReportStatus

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ExecutionReportStatus not found in DB: %s', 16, 2, @ExecutionReportStatus) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[ExecutionReportMessage]
           ([IntegratorReceivedExecutionReportUtc]
           ,[ExecutionReportStatusId]
           ,[IntegratorExternalOrderPrivateId]
           ,[CounterpartySentExecutionReportUtc]
           ,[FixMessageReceivedRaw])
     VALUES
           (
		   @ExecutionReportReceivedUtc
           ,@ExecutionReportStatusId
           ,@InternalOrderId
           ,@ExecutionReportCounterpartySentUtc
           ,@RawFIXMessage
		   )
END
GO


ALTER PROCEDURE [dbo].[InsertOrderExternalCancelRequest_SP]( 
	@IntegratorExternalOrderPrivateId [nvarchar](45),
    @IntegratorSentCancelRequestUtc  DATETIME2(7),
    @OrderExternalCancelTypeName VARCHAR(16),
    @RawFIXMessage [nvarchar](1024)
	)
AS
BEGIN

	DECLARE @OrderExternalCancelTypeId TINYINT

	SELECT @OrderExternalCancelTypeId = [OrderExternalCancelTypeId] FROM [dbo].[OrderExternalCancelType] WHERE [OrderExternalCancelTypeName] = @OrderExternalCancelTypeName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderExternalCancelTypeName not found in DB: %s', 16, 2, @OrderExternalCancelTypeName) WITH SETERROR
	END


	INSERT INTO [dbo].[OrderExternalCancelRequest]
           ([IntegratorExternalOrderPrivateId]
           ,[IntegratorSentCancelRequestUtc]
           ,[OrderExternalCancelTypeId]
           ,[FixMessageSentRaw])
     VALUES
           (@IntegratorExternalOrderPrivateId
           ,@IntegratorSentCancelRequestUtc
           ,@OrderExternalCancelTypeId
           ,@RawFIXMessage)
END
GO

ALTER PROCEDURE [dbo].[InsertNewAggregatedDealTicket_SP]( 
	@AggregatedTradeReportCounterpartyId [nvarchar](50),
	@TicketIntegratorReceivedTimeUtc [datetime2](7),
	@CounterpartySentTimeUtc [datetime2](7),
	@AggregatedPrice decimal(18,10),
	@AggregatedSizeBaseAbs decimal(18,2),
	@StpCounterpartyName [varchar](16),
	@FixMessageRaw nvarchar(1024),
	@ChildTradeIdsList nvarchar(MAX)
	)
AS
BEGIN

	DECLARE @StpCounterpartyId TINYINT
	
	SELECT @StpCounterpartyId = [StpCounterpartyId] FROM [dbo].[StpCounterparty] WHERE [StpCounterpartyName] = @StpCounterpartyName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'StpCounterparty not found in DB: %s', 16, 2, @StpCounterpartyName) WITH SETERROR
	END

	INSERT INTO [dbo].[AggregatedExternalExecutedTicket]
           ([AggregatedExternalExecutedTicketCounterpartyId]
           ,[IntegratorReceivedTimeUtc]
           ,[CounterpartySentTimeUtc]
           ,[AggregatedSizeBaseAbs]
           ,[AggregatedPrice]
           ,[StpCounterpartyId]
           ,[FixMessageRaw])
     VALUES
           (@AggregatedTradeReportCounterpartyId
           ,@TicketIntegratorReceivedTimeUtc
           ,@CounterpartySentTimeUtc
           ,@AggregatedSizeBaseAbs
           ,@AggregatedPrice
           ,@StpCounterpartyId
           ,@FixMessageRaw)
		   
		   
	INSERT INTO [dbo].[AggregatedExternalExecutedTicketMap]
		([AggregatedExternalExecutedTicketCounterpartyId],
		[InternalTransactionIdentity])
	SELECT
		@AggregatedTradeReportCounterpartyId, Code
	FROM
		[dbo].[csvlist_to_codes_tbl](@ChildTradeIdsList)	
END
GO


ALTER PROCEDURE [dbo].[Deduplicate_AggregatedExternalExecutedTicket_SP] 
AS
BEGIN
	CREATE TABLE #Temp_Deduped_AggregatedExternalExecutedTicket(
		[AggregatedExternalExecutedTicketCounterpartyId] [nvarchar](50) NOT NULL,
		[IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
		[CounterpartySentTimeUtc] [datetime2](7) NULL,
		[AggregatedSizeBaseAbs] [decimal](18, 2) NOT NULL,
		[AggregatedPrice] [decimal](18, 10) NOT NULL,
		[StpCounterpartyId] [tinyint] NOT NULL,
		[FixMessageRaw] [nvarchar](1024) NOT NULL
	) 

	INSERT INTO #Temp_Deduped_AggregatedExternalExecutedTicket
			   ([AggregatedExternalExecutedTicketCounterpartyId]
			   ,[IntegratorReceivedTimeUtc]
			   ,[CounterpartySentTimeUtc]
			   ,[AggregatedSizeBaseAbs]
			   ,[AggregatedPrice]
			   ,[StpCounterpartyId]
			   ,[FixMessageRaw])
	SELECT DISTINCT
		[AggregatedExternalExecutedTicketCounterpartyId]
		  ,[IntegratorReceivedTimeUtc]
		  ,[CounterpartySentTimeUtc]
		  ,[AggregatedSizeBaseAbs]
		  ,[AggregatedPrice]
		  ,[StpCounterpartyId]
		  ,[FixMessageRaw]
	FROM [dbo].[AggregatedExternalExecutedTicket]

	TRUNCATE TABLE [dbo].[AggregatedExternalExecutedTicket]

	INSERT INTO [dbo].[AggregatedExternalExecutedTicket]
			   ([AggregatedExternalExecutedTicketCounterpartyId]
			   ,[IntegratorReceivedTimeUtc]
			   ,[CounterpartySentTimeUtc]
			   ,[AggregatedSizeBaseAbs]
			   ,[AggregatedPrice]
			   ,[StpCounterpartyId]
			   ,[FixMessageRaw])
	SELECT DISTINCT
		[AggregatedExternalExecutedTicketCounterpartyId]
		  ,[IntegratorReceivedTimeUtc]
		  ,[CounterpartySentTimeUtc]
		  ,[AggregatedSizeBaseAbs]
		  ,[AggregatedPrice]
		  ,[StpCounterpartyId]
		  ,[FixMessageRaw]
	FROM #Temp_Deduped_AggregatedExternalExecutedTicket

	DROP TABLE #Temp_Deduped_AggregatedExternalExecutedTicket
END
GO

ALTER PROCEDURE [dbo].[InsertNewDealTicket_SP]( 
	@InternalDealId [nvarchar](50),
	@TicketIntegratorSentOrReceivedTimeUtc [datetime2](7),
	@CounterpartySentTimeUtc [datetime2](7),
	@DealExternalExecutedTicketType [nvarchar](17),
	@StpCounterpartyName [varchar](16),
	@FixMessageRaw nvarchar(1024)
	)
AS
BEGIN

	DECLARE @DealExternalExecutedTicketTypeId INT
	DECLARE @StpCounterpartyId TINYINT

	SELECT @DealExternalExecutedTicketTypeId = [DealExternalExecutedTicketTypeId] FROM [dbo].[DealExternalExecutedTicketType] WHERE [DealExternalExecutedTicketTypeName] = @DealExternalExecutedTicketType

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealExternalExecutedTicketType not found in DB: %s', 16, 2, @DealExternalExecutedTicketType) WITH SETERROR
	END
	
	SELECT @StpCounterpartyId = [StpCounterpartyId] FROM [dbo].[StpCounterparty] WHERE [StpCounterpartyName] = @StpCounterpartyName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'StpCounterparty not found in DB: %s', 16, 2, @StpCounterpartyName) WITH SETERROR
	END

	INSERT INTO 
		[dbo].[DealExternalExecutedTicket]
           ([InternalTransactionIdentity]
           ,[IntegratorSentOrReceivedTimeUtc]
		   ,[CounterpartySentTimeUtc]
           ,[DealExternalExecutedTicketTypeId]
		   ,[StpCounterpartyId]
           ,[FixMessageRaw])
     VALUES
           (@InternalDealId
           ,@TicketIntegratorSentOrReceivedTimeUtc
		   ,@CounterpartySentTimeUtc
           ,@DealExternalExecutedTicketTypeId
		   ,@StpCounterpartyId
           ,@FixMessageRaw)
END
GO


--
--
--
--

DBCC CLEANTABLE ('Integrator', 'OrderExternal')
GO
DBCC CLEANTABLE ('Integrator', 'ExecutionReportMessage', 500000)
GO
DBCC CLEANTABLE ('Integrator', 'OrderExternalCancelRequest')
GO
DBCC CLEANTABLE ('Integrator', 'AggregatedExternalExecutedTicket')
GO
DBCC CLEANTABLE ('Integrator', 'DealExternalExecutedTicket')
GO