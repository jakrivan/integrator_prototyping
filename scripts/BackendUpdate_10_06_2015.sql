BEGIN TRANSACTION AAA

ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ALTER COLUMN [CommissionsPbUsd] [decimal](18,6)
ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ALTER COLUMN [CommissionsPbUsdPerMUsd] [decimal](18,6)
ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ALTER COLUMN [CommissionsCptUsd] [decimal](18,6)
ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ALTER COLUMN [CommissionsCptUsdPerMUsd] [decimal](18,6)
GO

ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	DECLARE @TempTradingSystemStatistics TABLE
	(
		[SymbolId] [tinyint] NOT NULL,
		[TradingSystemId] [int] NOT NULL,
		[NopBasePol] [decimal](18,2) NOT NULL,
		[VolumeUsdM] [decimal](18,6) NOT NULL,
		[DealsNum] [int]  NOT NULL,
		[LastDealUtc] [datetime2](7) NULL,
		[KGTRejectionsNum] [int]  NOT NULL,
		[LastKgtRejectionUtc] [datetime2](7) NULL,
		[CtpRejectionsNum] [int]  NOT NULL,
		[LastCptRejectionUtc] [datetime2](7) NULL,
		[PnlGrossUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlGrossUsd] [decimal](18,6) NOT NULL,
		[CommissionsPbUsdPerMUsd] [decimal](18,6) NOT NULL,
		[CommissionsPbUsd] [decimal](18,6) NOT NULL,
		[CommissionsCptUsdPerMUsd] [decimal](18,6) NOT NULL,
		[CommissionsCptUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsdPerMUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsd] [decimal](18,6) NOT NULL
	)

	INSERT INTO 
		@TempTradingSystemStatistics
		([SymbolId]
		,[TradingSystemId]
		,[NopBasePol]
		,[VolumeUsdM]
		,[DealsNum]
		,[LastDealUtc]
		,[KGTRejectionsNum]
		,[LastKgtRejectionUtc]
		,[CtpRejectionsNum]
		,[LastCptRejectionUtc]
		,[PnlGrossUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsPbUsdPerMUsd]
		,[CommissionsPbUsd]
		,[CommissionsCptUsdPerMUsd]
		,[CommissionsCptUsd]
		,[CommissionsUsdPerMUsd]
		,[CommissionsUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlNetUsd])
    exec [dbo].[GetTradingSystemsDailyStats_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingSystemDaily]
			([TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsPbUsdPerMUsd]
			,[CommissionsPbUsd]
			,[CommissionsCptUsdPerMUsd]
			,[CommissionsCptUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd])
		SELECT
			[TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsPbUsdPerMUsd]
			,[CommissionsPbUsd]
			,[CommissionsCptUsdPerMUsd]
			,[CommissionsCptUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd]
		FROM
			@TempTradingSystemStatistics
	COMMIT
END
GO

CREATE TABLE [dbo].[StreamingSettingsPerCounterparty](
	[CounterpartyId] [tinyint] NOT NULL,
	[MaxLLTimeEnforcedByDestination_milliseconds] [smallint] NOT NULL,
	[LLTransportReserve_milliseconds] [smallint] NOT NULL,
	[MaxLLTimeOnDestination_milliseconds] AS [MaxLLTimeEnforcedByDestination_milliseconds] - [LLTransportReserve_milliseconds]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[StreamingSettingsPerCounterparty]  WITH CHECK ADD  CONSTRAINT [FK_StreamingSettingsPerCounterparty_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO

ALTER TABLE [dbo].[StreamingSettingsPerCounterparty] CHECK CONSTRAINT [FK_StreamingSettingsPerCounterparty_Counterparty]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_StreamingSettingsPerCounterparty_CounterpartyId] ON [dbo].[StreamingSettingsPerCounterparty]
(
	[CounterpartyId] ASC
)
GO

INSERT INTO [dbo].[StreamingSettingsPerCounterparty] ([CounterpartyId], [MaxLLTimeEnforcedByDestination_milliseconds], [LLTransportReserve_milliseconds]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTA'), 200, 5)
INSERT INTO [dbo].[StreamingSettingsPerCounterparty] ([CounterpartyId], [MaxLLTimeEnforcedByDestination_milliseconds], [LLTransportReserve_milliseconds]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HT3'), 200, 5)
INSERT INTO [dbo].[StreamingSettingsPerCounterparty] ([CounterpartyId], [MaxLLTimeEnforcedByDestination_milliseconds], [LLTransportReserve_milliseconds]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FS1'), 100, 5)
INSERT INTO [dbo].[StreamingSettingsPerCounterparty] ([CounterpartyId], [MaxLLTimeEnforcedByDestination_milliseconds], [LLTransportReserve_milliseconds]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FS2'), 100, 5)
GO

CREATE PROCEDURE [dbo].[GetStreamingSettingsPerCounterparty_SP] 
(
	@Counterparty CHAR(3) = NULL
)
AS
BEGIN
	SELECT 
		ctp.[CounterpartyCode] AS Counterparty,
		[MaxLLTimeOnDestination_milliseconds]
	FROM 
		[dbo].[StreamingSettingsPerCounterparty] sett
		INNER JOIN [dbo].[Counterparty] ctp ON sett.[CounterpartyId] = ctp.[CounterpartyId]
	WHERE
		@Counterparty IS NULL OR ctp.[CounterpartyCode] = @Counterparty
END
GO
GRANT EXECUTE ON [dbo].[GetStreamingSettingsPerCounterparty_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetStreamingSettingsPerCounterparty_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

GRANT EXECUTE ON [dbo].[GetPriceDelaySettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO



DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO


CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_DBK" type="FIXChannel_DBKSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM2" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM3" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC1" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC2" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS1" type="FIXChannel_FCMMMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS2" type="FIXChannel_FCMMMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
      <xs:element minOccurs="1" maxOccurs="1" name="STP" type="STPSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_DBKSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DealConfirmationSupported" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="L01LayersNum" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IocApplyTtl" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ApplyMaxHoldTime" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxHoldTime" type="FXCMMaxHoldTime" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MaxPerSymbolPricesPerSecondSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultMaxAllowedPerSecondRate" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialSymbolRateSettings" type="ArrayOfSymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolRateSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="SymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolRateSetting">
    <xs:attribute name="Name" type="xs:string" />
    <xs:attribute name="MaxAllowedPerSecondRate" type="xs:int" use="required" />
  </xs:complexType>
  <xs:complexType name="RejectionRateDisconnectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumOrdersToApplyCheck" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAllowedRejectionRatePercent" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMMMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxTotalPricesPerSecond" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="MaxPerSymbolPricesPerSecondSetting" type="MaxPerSymbolPricesPerSecondSetting" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExpectedPricingDepth" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="RejectionRateDisconnectSettings" type="RejectionRateDisconnectSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXSTPChannel_CTIPBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNongenuineInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialRejectionsHandling" type="ArrayOfSpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SpecialRejection" type="SpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="TriggerPhrase" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="FXCMMaxHoldTime">
    <xs:restriction base="xs:string">
      <xs:enumeration value="UpTo1ms" />
      <xs:enumeration value="UpTo30ms" />
      <xs:enumeration value="UpTo100ms" />
      <xs:enumeration value="UpTo500ms" />
      <xs:enumeration value="UpTo3000ms" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="MarketSessionIntoSeparateProcess" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="STPDestinations" type="ArrayOfSTPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSTPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="STPDestination" nillable="true" type="STPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Name" type="STPCounterparty" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedTicketTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="ServedCounterparties" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="STPCounterparty">
    <xs:restriction base="xs:string">
      <xs:enumeration value="CTIPB" />
      <xs:enumeration value="Traiana" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

--MANUALLY!!!
--remove all MaximumDealConfirmationDelay_Milliseconds AND  MaxOrderHoldTime_Milliseconds
--

UPDATE
	sett
SET
	[KGTLLTime_milliseconds] = [KGTLLTime_milliseconds] - 35
FROM
	[dbo].[VenueStreamSettings] sett
	INNER JOIN [dbo].[Counterparty] cpt ON sett.[CounterpartyId] = cpt.[CounterpartyId]
WHERE 
	cpt.CounterpartyName = 'Hotspot'
	
	
		
ALTER TABLE [dbo].[PriceDelaySettings] ADD [AllowGoFlatOnlyFlipOnTimeStampOutOfBounds] [bit] NULL
ALTER TABLE [dbo].[PriceDelaySettings] ADD [MinAdditionalDelayTicksToTurnGFO] [bigint] NULL CHECK (MinAdditionalDelayTicksToTurnGFO >= 0)
ALTER TABLE [dbo].[PriceDelaySettings] ADD [MinNegativeDelayTicksFromExpectedToTurnGFO] [bigint] NULL CHECK (MinNegativeDelayTicksFromExpectedToTurnGFO >= 0)
GO

UPDATE [dbo].[PriceDelaySettings]
SET
	[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds] = 0,
	[MinAdditionalDelayTicksToTurnGFO] = 5000000,
	[MinNegativeDelayTicksFromExpectedToTurnGFO] = 1000000
GO	

ALTER TABLE [dbo].[PriceDelaySettings] ALTER COLUMN [AllowGoFlatOnlyFlipOnTimeStampOutOfBounds] [bit] NOT NULL
ALTER TABLE [dbo].[PriceDelaySettings] ALTER COLUMN [MinAdditionalDelayTicksToTurnGFO] [bigint] NOT NULL
ALTER TABLE [dbo].[PriceDelaySettings] ALTER COLUMN [MinNegativeDelayTicksFromExpectedToTurnGFO] [bigint] NOT NULL
GO

ALTER TABLE [dbo].[PriceDelaySettings]  WITH CHECK ADD CHECK  (([MinAdditionalDelayTicksToTurnGFO]>=([MaxAllowedAdditionalDelayTicks])))
GO


ALTER TABLE [dbo].[PriceDelaySettings] ADD [MinDelayTicksFromCptAllegedSentToTurnGFO] AS [ExpectedDelayTicks] + [MinAdditionalDelayTicksToTurnGFO]
ALTER TABLE [dbo].[PriceDelaySettings] ADD [MaxDelayTicksFromCptAllegedSentToTurnGFO] AS [ExpectedDelayTicks] - [MinNegativeDelayTicksFromExpectedToTurnGFO]
GO
ALTER TABLE [dbo].[PriceDelaySettings] ADD [MinAdditionalDelayToTurnGFO] AS (dateadd(nanosecond,[MinAdditionalDelayTicksToTurnGFO]%(10),dateadd(microsecond,[MinAdditionalDelayTicksToTurnGFO]/(10),CONVERT([time](7),'00:00:00'))))
ALTER TABLE [dbo].[PriceDelaySettings] ADD [MinAdditionalNegativeDelayToTurnGFO] AS (dateadd(nanosecond,[MinNegativeDelayTicksFromExpectedToTurnGFO]%(10),dateadd(microsecond,[MinNegativeDelayTicksFromExpectedToTurnGFO]/(10),CONVERT([time](7),'00:00:00'))))
ALTER TABLE [dbo].[PriceDelaySettings] ADD [MinDelayFromCptAllegedSentToTurnGFO] AS (dateadd(nanosecond,([ExpectedDelayTicks] + [MinAdditionalDelayTicksToTurnGFO])%(10),dateadd(microsecond,([ExpectedDelayTicks] + [MinAdditionalDelayTicksToTurnGFO])/(10),CONVERT([time](7),'00:00:00'))))
ALTER TABLE [dbo].[PriceDelaySettings] ADD [MinNegativeDelayFromCptAllegedSentToTurnGFO] AS (dateadd(nanosecond,([ExpectedDelayTicks] - [MinNegativeDelayTicksFromExpectedToTurnGFO])%(10),dateadd(microsecond,([ExpectedDelayTicks] - [MinNegativeDelayTicksFromExpectedToTurnGFO])/(10),CONVERT([time](7),'00:00:00'))))
GO

exec sp_rename '[dbo].[PriceDelaySettings]', DelaySettings
GO
exec sp_rename '[dbo].[GetPriceDelaySettings_SP]', GetDelaySettings_SP
GO

ALTER PROCEDURE [dbo].[GetDelaySettings_SP] 
AS
BEGIN
	SELECT 
		ctp.[CounterpartyCode] AS Counterparty,
		[ExpectedDelayTicks],
		[MaxAllowedAdditionalDelayTicks],
		[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds],
		[MinDelayTicksFromCptAllegedSentToTurnGFO],
		[MaxDelayTicksFromCptAllegedSentToTurnGFO]
	FROM 
		[dbo].[DelaySettings] sett
		INNER JOIN [dbo].[Counterparty] ctp ON sett.[CounterpartyId] = ctp.[CounterpartyId]
END
GO

ALTER PROCEDURE [dbo].[InsertTradeDecayAnalysisRequests_SP]
(
	@FromTimeUtc [datetime2](7) = NULL,
	@ToTimeUtc [datetime2](7) = NULL
)
AS
BEGIN

	IF @ToTimeUtc IS NULL
	BEGIN
		SET @ToTimeUtc = DATEADD(minute, -5, GETUTCDATE())
	END
	
	IF @FromTimeUtc IS NULL
	BEGIN
		SELECT @FromTimeUtc = [LastProcessedRecordUtc] FROM [dbo].[TradeDecay_LastProcessedRecord]
	END
	
	DECLARE @TypeCptDealId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'CptDeal')
	DECLARE @TypeCptRejectionId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'CptRejection')
	DECLARE @TypeKgtDealId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'KgtDeal')
	DECLARE @TypeKgtRejectionId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'KgtRejection')
	
	
	DECLARE @TempRequestsToAdd TABLE (
		[TradeDecayAnalysisTypeId] [tinyint] NOT NULL,
		[SourceEventGlobalRecordId] [int] NOT NULL,
		[SourceEventIndexTimeUtc] [datetime2](7) NOT NULL,
		[AnalysisStartTimeUtc] [datetime2](7) NOT NULL,
		[FXPairId] [tinyint] NOT NULL,
		[CounterpartyId] [tinyint] NOT NULL,
		[ReferencePrice] [decimal](18,9) NOT NULL,
		[PriceSideId] [bit] NOT NULL
	)
	
	INSERT INTO @TempRequestsToAdd(
		[TradeDecayAnalysisTypeId],
		[SourceEventGlobalRecordId],
		[SourceEventIndexTimeUtc],
		[AnalysisStartTimeUtc],
		[FXPairId],
		[CounterpartyId],
		[ReferencePrice],
		[PriceSideId]
	)
	SELECT
		@TypeCptDealId,
		[GlobalRecordId],
		[IntegratorReceivedExecutionReportUtc],
		CASE WHEN [FlowSideId] = 0 OR ([FlowSideId] IS NULL AND cpt.CounterpartyName IN ('Hotspot', 'FXall', 'FXCM')) 
		THEN DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExecutionReportUtc])
		ELSE [IntegratorSentExternalOrderUtc] END,
		symbolIdMap.[DCDBSymbolId],
		cptIdMap.[DCDBCounterpartyId],
		[Price],
		CASE WHEN [DealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN [dbo].[Counterparty] cpt ON deal.CounterpartyId = cpt.CounterpartyId
		INNER JOIN [dbo].[DelaySettings] delaySett ON deal.CounterpartyId = delaySett.CounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON deal.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON deal.SymbolId = symbolIdMap.IntegratorDBSymbolId
		--cannot use between due to inclusions
		WHERE [IntegratorReceivedExecutionReportUtc] > @FromTimeUtc AND [IntegratorReceivedExecutionReportUtc] < @ToTimeUtc
	UNION ALL
	SELECT
		@TypeCptRejectionId,
		[GlobalRecordId],
		[IntegratorReceivedExecutionReportUtc],
		CASE WHEN cpt.CounterpartyName IN ('Hotspot', 'FXall', 'FXCM') 
		THEN DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExecutionReportUtc]) 
		ELSE [IntegratorSentExternalOrderUtc] END,
		symbolIdMap.[DCDBSymbolId],
		cptIdMap.[DCDBCounterpartyId],
		[RejectedPrice],
		CASE WHEN [DealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
	FROM 
		[dbo].[DealExternalRejected] reject
		INNER JOIN [dbo].[Counterparty] cpt ON reject.CounterpartyId = cpt.CounterpartyId
		INNER JOIN [dbo].[DelaySettings] delaySett ON reject.CounterpartyId = delaySett.CounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON reject.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON reject.SymbolId = symbolIdMap.IntegratorDBSymbolId
		--cannot use between due to inclusions
		WHERE [IntegratorReceivedExecutionReportUtc] > @FromTimeUtc AND [IntegratorReceivedExecutionReportUtc] < @ToTimeUtc
		AND [RejectedPrice] IS NOT NULL
	UNION ALL
	SELECT
		CASE WHEN [IntegratorRejectionReasonId] IS NULL THEN @TypeKgtDealId ELSE @TypeKgtRejectionId END,
		[GlobalRecordId],
		[IntegratorReceivedExternalOrderUtc],
		DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExternalOrderUtc]),
		symbolIdMap.[DCDBSymbolId],
		cptIdMap.[DCDBCounterpartyId],
		[CounterpartyRequestedPrice],
		CASE WHEN [IntegratorDealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
	FROM 
		[dbo].[OrderExternalIncomingRejectable_Daily] cptOrder
		INNER JOIN [dbo].[DelaySettings] delaySett ON cptOrder.CounterpartyId = delaySett.CounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON cptOrder.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON cptOrder.SymbolId = symbolIdMap.IntegratorDBSymbolId
		--cannot use between due to inclusions
		WHERE [IntegratorReceivedExternalOrderUtc] > @FromTimeUtc AND [IntegratorReceivedExternalOrderUtc] < @ToTimeUtc
	
	IF DATEDIFF(day, @FromTimeUtc, GETUTCDATE()) != 0
	BEGIN
		INSERT INTO @TempRequestsToAdd(
			[TradeDecayAnalysisTypeId],
			[SourceEventGlobalRecordId],
			[SourceEventIndexTimeUtc],
			[AnalysisStartTimeUtc],
			[FXPairId],
			[CounterpartyId],
			[ReferencePrice],
			[PriceSideId]
		)
		SELECT
			CASE WHEN [IntegratorRejectionReasonId] IS NULL THEN @TypeKgtDealId ELSE @TypeKgtRejectionId END,
			[GlobalRecordId],
			[IntegratorReceivedExternalOrderUtc],
			DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExternalOrderUtc]),
			symbolIdMap.[DCDBSymbolId],
			cptIdMap.[DCDBCounterpartyId],
			[CounterpartyRequestedPrice],
			CASE WHEN [IntegratorDealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
		FROM 
			[dbo].[OrderExternalIncomingRejectable_Archive] cptOrder
			INNER JOIN [dbo].[DelaySettings] delaySett ON cptOrder.CounterpartyId = delaySett.CounterpartyId
			INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON cptOrder.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
			INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON cptOrder.SymbolId = symbolIdMap.IntegratorDBSymbolId
			--cannot use between due to inclusions
			WHERE [IntegratorReceivedExternalOrderUtc] > @FromTimeUtc AND [IntegratorReceivedExternalOrderUtc] < @ToTimeUtc
	END
	
	
	DECLARE  @LastRecordTimeUtc [datetime2](7)
	
	SELECT @LastRecordTimeUtc = MAX([SourceEventIndexTimeUtc]) FROM @TempRequestsToAdd
	
	IF @LastRecordTimeUtc IS NOT NULL
	BEGIN
		--try catch used so that execution flow is stopped after error
		BEGIN TRY
		
			INSERT INTO [FXtickDB].[dbo].[TradeDecayAnalysis_Pending]
				   ([TradeDecayAnalysisTypeId]
				   ,[SourceEventGlobalRecordId]
				   ,[AnalysisStartTimeUtc]
				   ,[FXPairId]
				   ,[CounterpartyId]
				   ,[ReferencePrice]
				   ,[PriceSideId])
			SELECT
				[TradeDecayAnalysisTypeId],
				[SourceEventGlobalRecordId],
				[AnalysisStartTimeUtc],
				[FXPairId],
				[CounterpartyId],
				[ReferencePrice],
				[PriceSideId]
			FROM
				@TempRequestsToAdd
				   
			UPDATE [dbo].[TradeDecay_LastProcessedRecord] SET [LastProcessedRecordUtc] = @LastRecordTimeUtc 

		END TRY
		BEGIN CATCH
			print 'error during storing';
			THROW;
		END CATCH
	END  
	ELSE
	BEGIN
		-- so that we do not search same intervals over and over if no trading
		UPDATE [dbo].[TradeDecay_LastProcessedRecord] SET [LastProcessedRecordUtc] = DATEADD(minute, -5, @ToTimeUtc)
	END
	
END
GO

INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HT3'), 'Glider')
INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTA'), 'Glider')
GO

ROLLBACK TRANSACTION AAA
GO
COMMIT TRANSACTION AAA
GO

----- Glider settings enrichement (INT-462) ------

BEGIN TRANSACTION AAA

ALTER TABLE [dbo].[VenueGliderSettings] ADD [MinimumBPGrossGainIfZeroGlideFill] [decimal](18, 6) NULL
ALTER TABLE [dbo].[VenueGliderSettings] ADD [PreferLLDestinationCheckToLmax] [bit] NULL
ALTER TABLE [dbo].[VenueGliderSettings_Changes] ADD [MinimumBPGrossGainIfZeroGlideFill] [decimal](18, 6) NULL
ALTER TABLE [dbo].[VenueGliderSettings_Changes] ADD [PreferLLDestinationCheckToLmax] [bit] NULL
GO

UPDATE [dbo].[VenueGliderSettings] SET 
	[MinimumBPGrossGainIfZeroGlideFill] = [MinimumBPGrossGain],
	[PreferLLDestinationCheckToLmax] = 1,
	[PreferLmaxDuringHedging] = 0
UPDATE [dbo].[VenueGliderSettings_Changes] SET 
	[MinimumBPGrossGainIfZeroGlideFill] = [MinimumBPGrossGain],
	[PreferLLDestinationCheckToLmax] = 1,
	[PreferLmaxDuringHedging] = 0
GO

ALTER TABLE [dbo].[VenueGliderSettings] ALTER COLUMN [MinimumBPGrossGainIfZeroGlideFill] [decimal](18, 6) NOT NULL
ALTER TABLE [dbo].[VenueGliderSettings] ALTER COLUMN [PreferLLDestinationCheckToLmax] [bit] NOT NULL
ALTER TABLE [dbo].[VenueGliderSettings_Changes] ALTER COLUMN [MinimumBPGrossGainIfZeroGlideFill] [decimal](18, 6) NOT NULL
ALTER TABLE [dbo].[VenueGliderSettings_Changes] ALTER COLUMN [PreferLLDestinationCheckToLmax] [bit] NOT NULL
GO

ALTER TRIGGER [dbo].[VenueGliderSettings_DeletedOrUpdated] ON [dbo].[VenueGliderSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueGliderSettings_Changes]
	   ([SymbolId]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[MaximumSizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade]
	   ,[KGTLLTime_milliseconds]
	   ,[MinimumBPGrossGain]
	   ,[PreferLmaxDuringHedging]
	   ,[MinimumIntegratorPriceLife_milliseconds]
	   ,[MinimumIntegratorGlidePriceLife_milliseconds]
	   ,[GlidingCutoffSpan_milliseconds]
	   ,[MinimumSizeToTrade]
	   ,[MinimumFillSize]
	   ,[MaximumSourceBookTiers]
	   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
	   ,[ReferenceLmaxBookOnly]
	   ,[BidEnabled]
	   ,[AskEnabled]
	   ,[MinimumBPGrossGainIfZeroGlideFill]
	   ,[PreferLLDestinationCheckToLmax]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[MaximumSizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[KGTLLTime_milliseconds]
		,deleted.[MinimumBPGrossGain]
		,deleted.[PreferLmaxDuringHedging]
		,deleted.[MinimumIntegratorPriceLife_milliseconds]
		,deleted.[MinimumIntegratorGlidePriceLife_milliseconds]
		,deleted.[GlidingCutoffSpan_milliseconds]
		,deleted.[MinimumSizeToTrade]
		,deleted.[MinimumFillSize]
		,deleted.[MaximumSourceBookTiers]
		,deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]
		,deleted.[ReferenceLmaxBookOnly]
		,deleted.[BidEnabled]
		,deleted.[AskEnabled]
		,deleted.[MinimumBPGrossGainIfZeroGlideFill]
	    ,deleted.[PreferLLDestinationCheckToLmax]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueGliderSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueGliderSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([KGTLLTime_milliseconds]) AND (topUpdatedChange.[KGTLLTime_milliseconds] IS NULL OR topUpdatedChange.[KGTLLTime_milliseconds] != deleted.[KGTLLTime_milliseconds]))
		OR (UPDATE([MinimumBPGrossGain]) AND (topUpdatedChange.[MinimumBPGrossGain] IS NULL OR topUpdatedChange.[MinimumBPGrossGain] != deleted.[MinimumBPGrossGain]))
		OR (UPDATE([PreferLmaxDuringHedging]) AND (topUpdatedChange.[PreferLmaxDuringHedging] IS NULL OR topUpdatedChange.[PreferLmaxDuringHedging] != deleted.[PreferLmaxDuringHedging]))
		OR (UPDATE([MinimumIntegratorPriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] != deleted.[MinimumIntegratorPriceLife_milliseconds]))
		OR (UPDATE([MinimumIntegratorGlidePriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorGlidePriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorGlidePriceLife_milliseconds] != deleted.[MinimumIntegratorGlidePriceLife_milliseconds]))
		OR (UPDATE([GlidingCutoffSpan_milliseconds]) AND (topUpdatedChange.[GlidingCutoffSpan_milliseconds] IS NULL OR topUpdatedChange.[GlidingCutoffSpan_milliseconds] != deleted.[GlidingCutoffSpan_milliseconds]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([MaximumSourceBookTiers]) AND (topUpdatedChange.[MaximumSourceBookTiers] IS NULL OR topUpdatedChange.[MaximumSourceBookTiers] != deleted.[MaximumSourceBookTiers]))
		OR (UPDATE([MinimumBasisPointsFromKgtPriceToFastCancel]) AND (topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] IS NULL OR topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] != deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]))
		OR (UPDATE([ReferenceLmaxBookOnly]) AND (topUpdatedChange.[ReferenceLmaxBookOnly] IS NULL OR topUpdatedChange.[ReferenceLmaxBookOnly] != deleted.[ReferenceLmaxBookOnly]))
		OR (UPDATE([BidEnabled]) AND (topUpdatedChange.[BidEnabled] IS NULL OR topUpdatedChange.[BidEnabled] != deleted.[BidEnabled]))
		OR (UPDATE([AskEnabled]) AND (topUpdatedChange.[AskEnabled] IS NULL OR topUpdatedChange.[AskEnabled] != deleted.[AskEnabled]))
		OR (UPDATE([MinimumBPGrossGainIfZeroGlideFill]) AND (topUpdatedChange.[MinimumBPGrossGainIfZeroGlideFill] IS NULL OR topUpdatedChange.[MinimumBPGrossGainIfZeroGlideFill] != deleted.[MinimumBPGrossGainIfZeroGlideFill]))
		OR (UPDATE([PreferLLDestinationCheckToLmax]) AND (topUpdatedChange.[PreferLLDestinationCheckToLmax] IS NULL OR topUpdatedChange.[PreferLLDestinationCheckToLmax] != deleted.[PreferLLDestinationCheckToLmax]))
END
GO


ALTER PROCEDURE [dbo].[GetUpdatedVenueGliderSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueGliderSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MaximumSizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[KGTLLTime_milliseconds]
			,[MinimumBPGrossGain]
			,[MinimumBPGrossGainIfZeroGlideFill]
			,[PreferLmaxDuringHedging]
			,[PreferLLDestinationCheckToLmax]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumIntegratorGlidePriceLife_milliseconds]
			,[GlidingCutoffSpan_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,[MinimumBasisPointsFromKgtPriceToFastCancel]
			,[ReferenceLmaxBookOnly]
			,[BidEnabled]
			,[AskEnabled]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueGliderSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name
	END
END
GO

ALTER PROCEDURE [dbo].[GetVenueGliderSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[MinimumBPGrossGain],
		sett.[MinimumBPGrossGain] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossGainDecimal],
		sett.[PreferLmaxDuringHedging],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumIntegratorGlidePriceLife_milliseconds],
		sett.[GlidingCutoffSpan_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[ReferenceLmaxBookOnly],
		sett.[BidEnabled],
		sett.[AskEnabled],
		sett.[MinimumBPGrossGainIfZeroGlideFill],
		sett.[MinimumBPGrossGainIfZeroGlideFill] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossGainIfZeroGlideFillDecimal],
		sett.[PreferLLDestinationCheckToLmax],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[VenueGliderSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@BidEnabled [bit],
	@AskEnabled [bit],
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@MinimumBPGrossGain  [decimal](18,6),
	@MinimumBPGrossGainIfZeroGlideFill [decimal](18,6),
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumIntegratorGlidePriceLife_milliseconds [int],
	@GlidingCutoffSpan_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel  [decimal](18,6),
	@ReferenceLmaxBookOnly [bit],
	@PreferLLDestinationCheckToLmax [bit],
	@PreferLmaxDuringHedging [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Glider'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Stream TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	
	BEGIN TRANSACTION
	BEGIN TRY
		
		INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

		SELECT @TradingSystemId = scope_identity();
		
		--make sure that default switches values are correct
		INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
		SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
		FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
		WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
		
		INSERT INTO [dbo].[VenueGliderSettings]
			   ([SymbolId]
			   ,[CounterpartyId]
			   ,[TradingSystemId]
			   ,[MaximumSizeToTrade]
			   ,[BestPriceImprovementOffsetBasisPoints]
			   ,[MaximumDiscountBasisPointsToTrade]
			   ,[KGTLLTime_milliseconds]
			   ,[MinimumBPGrossGain]
			   ,[MinimumBPGrossGainIfZeroGlideFill]
			   ,[PreferLmaxDuringHedging]
			   ,[PreferLLDestinationCheckToLmax]
			   ,[MinimumIntegratorPriceLife_milliseconds]
			   ,[MinimumIntegratorGlidePriceLife_milliseconds]
			   ,[GlidingCutoffSpan_milliseconds]
			   ,[MinimumSizeToTrade]
			   ,[MinimumFillSize]
			   ,[MaximumSourceBookTiers]
			   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
			   ,[ReferenceLmaxBookOnly]
			   ,[BidEnabled]
			   ,[AskEnabled])
		 VALUES
			   (@SymbolId
			   ,@CounterpartyId
			   ,@TradingSystemId
			   ,@MaximumSizeToTrade 
			   ,@BestPriceImprovementOffsetBasisPoints
			   ,@MaximumDiscountBasisPointsToTrade
			   ,@KGTLLTime_milliseconds
			   ,@MinimumBPGrossGain
			   ,@MinimumBPGrossGainIfZeroGlideFill
			   ,@PreferLmaxDuringHedging
			   ,@PreferLLDestinationCheckToLmax
			   ,@MinimumIntegratorPriceLife_milliseconds
			   ,@MinimumIntegratorGlidePriceLife_milliseconds
			   ,@GlidingCutoffSpan_milliseconds
			   ,@MinimumSizeToTrade
			   ,@MinimumFillSize
			   ,@MaximumSourceBookTiers
			   ,@MinimumBasisPointsFromKgtPriceToFastCancel
			   ,@ReferenceLmaxBookOnly
			   ,@BidEnabled
			   ,@AskEnabled)
		
	
		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[VenueGliderSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@MinimumBPGrossGain  [decimal](18,6),
	@MinimumBPGrossGainIfZeroGlideFill [decimal](18,6),
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumIntegratorGlidePriceLife_milliseconds [int],
	@GlidingCutoffSpan_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel [decimal](18,6),
	@ReferenceLmaxBookOnly [bit],
	@PreferLLDestinationCheckToLmax [bit],
	@PreferLmaxDuringHedging [bit],
	@BidEnabled [bit],
	@AskEnabled [bit]
)
AS
BEGIN
	UPDATE sett
	SET
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[KGTLLTime_milliseconds] = @KGTLLTime_milliseconds,
		[MinimumBPGrossGain] = @MinimumBPGrossGain,
		[MinimumBPGrossGainIfZeroGlideFill] = @MinimumBPGrossGainIfZeroGlideFill,
		[PreferLmaxDuringHedging] = @PreferLmaxDuringHedging,
		[PreferLLDestinationCheckToLmax] = @PreferLLDestinationCheckToLmax,
		[MinimumIntegratorPriceLife_milliseconds] = @MinimumIntegratorPriceLife_milliseconds,
		[MinimumIntegratorGlidePriceLife_milliseconds] = @MinimumIntegratorGlidePriceLife_milliseconds,
		[GlidingCutoffSpan_milliseconds] = @GlidingCutoffSpan_milliseconds,
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MinimumFillSize] = @MinimumFillSize,
		[MaximumSourceBookTiers] = @MaximumSourceBookTiers,
		[MinimumBasisPointsFromKgtPriceToFastCancel] = @MinimumBasisPointsFromKgtPriceToFastCancel,
		[ReferenceLmaxBookOnly] = @ReferenceLmaxBookOnly,
		[BidEnabled] = @BidEnabled,
		[AskEnabled] = @AskEnabled
	FROM
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

ALTER PROCEDURE [dbo].[GetLastVenueGliderSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MaximumSizeToTrade]
      ,[BestPriceImprovementOffsetBasisPoints]
      ,[MaximumDiscountBasisPointsToTrade]
      ,[KGTLLTime_milliseconds]
      ,[MinimumBPGrossGain]
	  ,[MinimumBPGrossGainIfZeroGlideFill]
	  ,[PreferLmaxDuringHedging]
	  ,[PreferLLDestinationCheckToLmax]
      ,[MinimumIntegratorPriceLife_milliseconds]
	  ,[MinimumIntegratorGlidePriceLife_milliseconds]
	  ,[GlidingCutoffSpan_milliseconds]
      ,[MinimumSizeToTrade]
      ,[MinimumFillSize]
      ,[MaximumSourceBookTiers]
      ,[MinimumBasisPointsFromKgtPriceToFastCancel]
      ,[ReferenceLmaxBookOnly]
      ,[BidEnabled]
      ,[AskEnabled]
	FROM 
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END
GO

--------------------------------------- Partial Fills in streaming ----------------------------------



ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] ADD [CounterpartyRequestedFillMinimumBaseAbs] decimal(18,2) NULL 
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] ADD [CounterpartyRequestedFillMinimumBaseAbs] decimal(18,2) NULL
GO

ALTER PROCEDURE [dbo].[Daily_ArchiveRejectableOrders_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[OrderExternalIncomingRejectable_Archive]
			([IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[CounterpartyRequestedFillMinimumBaseAbs],
			[IntegratorExecutionId],
			[CounterpartyExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[GlobalRecordId])
		SELECT
			[IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[CounterpartyRequestedFillMinimumBaseAbs],
			[IntegratorExecutionId],
			[CounterpartyExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[GlobalRecordId]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]
		
		--remember last identity
		DECLARE @LastId INT
		SELECT @LastId = IDENT_CURRENT ('OrderExternalIncomingRejectable_Daily')
		SET @LastId = @LastId + 1
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[OrderExternalIncomingRejectable_Daily]
		
		--and continue in identity
		DBCC CHECKIDENT ('OrderExternalIncomingRejectable_Daily', RESEED, @LastId)
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO


ALTER PROCEDURE [dbo].[InsertNewOrderExternalIncomingRejectable_SP]( 
    @ExternalOrderReceived [datetime2](7),
	@ExternalOrderSentUtc [datetime2](7),
	@IntegratorSentExecutionReportUtc [datetime2](7),
	@CounterpartyClientOrderId [varchar](32),
	@CounterpartyClientId [varchar](20),
	@TradingSystemId int = NULL,
	@SourceIntegratorPriceIdentity [varchar](20) = NULL,
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@RequestedFillMinimumBaseAbs decimal(18,2) = NULL,
	@IntegratorExecId [varchar](20) = NULL,
	@CounterpartyExecId [varchar](32) = NULL,
	@ValueDate [date] = NULL,
	@OrderReceivedToExecutionReportSentLatency [time](7),
	@IntegratorExecutedAmountBaseAbs [decimal](18, 2) = NULL,
	@IntegratorRejectedAmountBaseAbs [decimal](18, 2) = NULL,
	@RejectionReason [varchar](32) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WITH(NOLOCK) WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WITH(NOLOCK) WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WITH(NOLOCK) WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @RejectionReason IS NOT NULL
	BEGIN
		SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
		IF @@ROWCOUNT <> 1
		BEGIN
			INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
			SET @RejectionReasonId = @@IDENTITY
		END
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @IntegratorExecutedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,COALESCE(@IntegratorExecId, @CounterpartyExecId)
				,@ExternalOrderReceived)
		END
		ELSE IF @IntegratorRejectedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,ISNULL(@RejectionReason, '<NONE>')
				,COALESCE(@IntegratorExecId, @CounterpartyExecId)
				,@RejectionDirectionId
				,@ExternalOrderReceived)
		END

		INSERT INTO 
			[dbo].[OrderExternalIncomingRejectable_Daily]
			   ([IntegratorReceivedExternalOrderUtc]
			   ,[CounterpartySentExternalOrderUtc]
			   ,[IntegratorSentExecutionReportUtc]
			   ,[CounterpartyClientOrderId]
			   ,[CounterpartyClientId]
			   ,[TradingSystemId]
			   ,[SourceIntegratorPriceIdentity]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorDealDirectionId]
			   ,[CounterpartyRequestedPrice]
			   ,[CounterpartyRequestedAmountBasePol]
			   ,[CounterpartyRequestedFillMinimumBaseAbs]
			   ,[IntegratorExecutionId]
			   ,[CounterpartyExecutionId]
			   ,[ValueDate]
			   ,[OrderReceivedToExecutionReportSentLatency ]
			   ,[IntegratorExecutedAmountBasePol]
			   ,[IntegratorRejectedAmountBasePol]
			   ,[IntegratorRejectionReasonId])
		 VALUES
			   (@ExternalOrderReceived
			   ,@ExternalOrderSentUtc
			   ,@IntegratorSentExecutionReportUtc
			   ,@CounterpartyClientOrderId
			   ,@CounterpartyClientId
			   ,@TradingSystemId
			   ,@SourceIntegratorPriceIdentity
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@DealDirectionId
			   ,@RequestedPrice
			   ,CASE WHEN @DealDirectionId = 1 THEN -@RequestedAmountBaseAbs ELSE @RequestedAmountBaseAbs END
			   ,@RequestedFillMinimumBaseAbs
			   ,@IntegratorExecId
			   ,@CounterpartyExecId
			   ,@ValueDate
			   ,@OrderReceivedToExecutionReportSentLatency 
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
		       ,@RejectionReasonId)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH	   
END
GO

ALTER PROCEDURE [dbo].[StreamingSessionStatsToday_SP]
(
	@CounterpartyCode [char](3)
)
AS
BEGIN
	SELECT 
		SUM(CASE WHEN [IntegratorExecutedAmountBasePol] <= 0 THEN 1 ELSE 0 END) AS RejectionsNum,
		SUM(CASE WHEN [IntegratorExecutedAmountBasePol] > 0 THEN 1 ELSE 0 END) AS DealsNum
	FROM
		[dbo].[OrderExternalIncomingRejectable_Daily] ord
		INNER JOIN [dbo].[Counterparty] ctp on ord.CounterpartyId = ctp.CounterpartyId
	WHERE
		ctp.[CounterpartyCode] = @CounterpartyCode
END
GO


ALTER PROCEDURE [dbo].[GetStatisticsPerCounterparty_Daily_SP] (
	@Day DATE
	)
AS
BEGIN

	DECLARE @KGTRejectionDirectionId BIT
	SELECT @KGTRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'
	DECLARE @CtpRejectionDirectionId BIT
	SELECT @CtpRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'CtpRejectd'
	DECLARE @MinDate DATE = GETUTCDATE()	
		
	DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		KGTRejectionsNum int,
		LastKgtRejectionUtc datetime2(7),
		CtpRejectionsNum int,
		LastCptRejectionUtc datetime2(7),
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int
	)

	INSERT INTO @temp_rejections(CounterpartyId, KGTRejectionsNum, LastKgtRejectionUtc, CtpRejectionsNum, LastCptRejectionUtc, IntegratorLatencyNanoseconds, IntegratorLatencyCases)
	SELECT
		[CounterpartyId],
		SUM(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN 1 ELSE 0 END) AS KGTRejections,
		MAX(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN [IntegratorReceivedExecutionReportUtc] ELSE @MinDate END) AS LastKgtRejectionUtc,
		SUM(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN 1 ELSE 0 END) AS CtpRejections,
		MAX(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN [IntegratorReceivedExecutionReportUtc] ELSE @MinDate END) AS LastCptRejectionUtc,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
	FROM
		[dbo].[DealExternalRejected_DailyView] ctpRej WITH(NOEXPAND, NOLOCK)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]

	UNION ALL

	SELECT
			[CounterpartyId],
			COUNT([CounterpartyId]) AS KGTRejections,
			MAX([IntegratorReceivedExternalOrderUtc]) AS LastKgtRejectionUtc,
			NULL AS CtpRejections,
			NULL AS LastCptRejectionUtc,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily] kgtRej WITH(NOLOCK)
		WHERE
			[IntegratorExecutedAmountBasePol] <= 0
			AND [DatePartIntegratorReceivedOrderUtc] = @Day
			--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
		GROUP BY
			[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		CommissionPBBase decimal(18,2),
		CommissionCptBase decimal(18,2),
		DealsNum int,
		LastDealUtc datetime2(7),
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int,
		CounterpartyLatencyNanoseconds bigint,
		CounterpartyLatencyCases int
	)

	INSERT INTO 
		@temp_NOPs_pol
		(
		SymbolId, 
		CounterpartyId, 
		NopBasePol, 
		NopTermPol, 
		VolumeBaseAbs, 
		CommissionPBBase,
		CommissionCptBase,
		DealsNum, 
		LastDealUtc,
		IntegratorLatencyNanoseconds, 
		IntegratorLatencyCases, 
		CounterpartyLatencyNanoseconds, 
		CounterpartyLatencyCases
		)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		SUM(ABS([AmountBasePolExecuted]) * PbCommissionPpm / 1000000) AS CommissionPBBase,
		SUM(ABS([AmountBasePolExecuted]) * CounterpartyCommissionPpm / 1000000) AS CommissionCptBase,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum,
		MAX(deal.[IntegratorReceivedExecutionReportUtc]) AS LastDealUtc,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases,
		SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
		COUNT(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyCases
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
		INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
		--INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		deal.SymbolId,
		deal.CounterpartyId
		

	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.CommissionPbInUsd AS CommissionPbInUsd,
		DealsStatistics.CommissionCptInUsd AS CommissionCptInUsd,
		DealsStatistics.CommissionPbInUsd + DealsStatistics.CommissionCptInUsd  AS CommissionSumInUsd,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.LastDealUtc AS LastDealUtc,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		CASE WHEN COALESCE(RejectionsStatistics.KGTRejectionsNum,0) = 0 THEN NULL ELSE RejectionsStatistics.KGTRejectionsNum END AS KGTRejectionsNum,
		RejectionsStatistics.LastKgtRejectionUtc AS RejectionsStatistics,
		CASE WHEN COALESCE(RejectionsStatistics.CtpRejectionsNum,0) = 0 THEN NULL ELSE RejectionsStatistics.CtpRejectionsNum END AS CtpRejectionsNum,
		RejectionsStatistics.LastCptRejectionUtc AS LastCptRejectionUtc,
		CASE WHEN COALESCE(RejectionsStatistics.KGTRejectionsNum,0) = 0
		THEN
			 NULL
		ELSE
			RejectionsStatistics.KGTRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.KGTRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0))
		END AS KGTRejectionsRate,
		CASE WHEN COALESCE(RejectionsStatistics.CtpRejectionsNum,0) = 0
		THEN
			NULL
		ELSE
			RejectionsStatistics.CtpRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.CtpRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0))
		END AS CtpRejectionsRate,
		--this calculates average of both numbers if both are non-null; or average of the nonull case if just one is not null (as 2*A / 2*B = A/B); or NULL if both are NULL (avoids divide by zero exception)
		(COALESCE(RejectionsStatistics.IntegratorLatencyNanoseconds, DealsStatistics.IntegratorLatencyNanoseconds) + COALESCE(DealsStatistics.IntegratorLatencyNanoseconds, RejectionsStatistics.IntegratorLatencyNanoseconds))
		/
		(COALESCE(RejectionsStatistics.IntegratorLatencyCases, DealsStatistics.IntegratorLatencyCases) + COALESCE(DealsStatistics.IntegratorLatencyCases, RejectionsStatistics.IntegratorLatencyCases))
		AS AvgIntegratorLatencyNanoseconds,
		DealsStatistics.CounterpartyLatencyNanoseconds / DealsStatistics.CounterpartyLatencyCases AS AvgCounterpartyLatencyNanoseconds
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			-- from SHORTs
			SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs,
			--SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(CommissionPbInUsd) AS CommissionPbInUsd,
			SUM(CommissionCptInUsd) AS CommissionCptInUsd,
			SUM(DealsNum) AS DealsNum,
			MAX(LastDealUtc) AS LastDealUtc,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
			SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
			SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(CommissionPbInUsd) AS CommissionPbInUsd,
				SUM(CommissionCptInUsd) AS CommissionCptInUsd,
				SUM(DealsNum) AS DealsNum,
				MAX(LastDealUtc) AS LastDealUtc,
				SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
				SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
				SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
				SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.CommissionPBBase * ccy1.LastKnownUsdConversionMultiplier AS CommissionPbInUsd,
					basePos.CommissionCptBase * ccy1.LastKnownUsdConversionMultiplier AS CommissionCptInUsd,
					basePos.DealsNum AS DealsNum,
					basePos.LastDealUtc AS LastDealUtc,
					basePos.IntegratorLatencyNanoseconds AS IntegratorLatencyNanoseconds,
					basePos.IntegratorLatencyCases AS IntegratorLatencyCases,
					basePos.CounterpartyLatencyNanoseconds AS CounterpartyLatencyNanoseconds,
					basePos.CounterpartyLatencyCases AS CounterpartyLatencyCases
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd,
					0 AS CommissionPbInUsd,
					0 AS CommissionCptInUsd,
					0 AS DealsNum,
					NULL AS LastDealUtc,
					NULL AS IntegratorLatencyNanoseconds,
					NULL AS IntegratorLatencyCases,
					NULL AS CounterpartyLatencyNanoseconds,
					NULL AS CounterpartyLatencyCases
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode
END
GO

------------  LX1 and FL1 cpts -----------------------


INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (50, N'LMAX', N'LX1', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (51, N'FxAll', N'FL1', 0)
GO

INSERT INTO [dbo].[CommissionsSchedule_CptCommission] ([CounterpartyId], [CounterpartyCommissionPpm], [ValidFromUtc], [ValidToUtc]) VALUES (50, 5, '1900-01-01', '9999-12-31')
INSERT INTO [dbo].[CommissionsSchedule_CptCommission] ([CounterpartyId], [CounterpartyCommissionPpm], [ValidFromUtc], [ValidToUtc]) VALUES (51, 5, '1900-01-01', '9999-12-31')

INSERT INTO [dbo].[CommissionsSchedule_Mapping] ([CounterpartyId], [PrimeBrokerId], [ValidFromUtc], [ValidToUtc]) VALUES (50, 1, '2015-04-29', '9999-12-31')
INSERT INTO [dbo].[CommissionsSchedule_Mapping] ([CounterpartyId], [PrimeBrokerId], [ValidFromUtc], [ValidToUtc]) VALUES (51, 1, '2015-04-29', '9999-12-31')


INSERT INTO [dbo].[DelaySettings] 
	([CounterpartyId]
    ,[ExpectedDelayTicks]
      ,[MaxAllowedAdditionalDelayTicks]
      ,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
      ,[MinAdditionalDelayTicksToTurnGFO]
      ,[MinNegativeDelayTicksFromExpectedToTurnGFO])
SELECT
	50
	,[ExpectedDelayTicks]
	,[MaxAllowedAdditionalDelayTicks]
	,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
	,[MinAdditionalDelayTicksToTurnGFO]
	,[MinNegativeDelayTicksFromExpectedToTurnGFO]
FROM
	[dbo].[DelaySettings]
WHERE
	CounterpartyId = 23 -- l01
	
INSERT INTO [dbo].[DelaySettings] 
	([CounterpartyId]
    ,[ExpectedDelayTicks]
      ,[MaxAllowedAdditionalDelayTicks]
      ,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
      ,[MinAdditionalDelayTicksToTurnGFO]
      ,[MinNegativeDelayTicksFromExpectedToTurnGFO])
SELECT
	51
	,[ExpectedDelayTicks]
	,[MaxAllowedAdditionalDelayTicks]
	,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
	,[MinAdditionalDelayTicksToTurnGFO]
	,[MinNegativeDelayTicksFromExpectedToTurnGFO]
FROM
	[dbo].[DelaySettings]
WHERE
	CounterpartyId = 22 -- FA1

INSERT INTO [dbo].[PriceDelayStats] ([CounterpartyId], [TotalReceivedPricesInLastMinute], [DelayedPricesInLastMinute], [TotalReceivedPricesInLastHour], [DelayedPricesInLastHour]) VALUES (49, 0, 0, 0, 0)	
INSERT INTO [dbo].[PriceDelayStats] ([CounterpartyId], [TotalReceivedPricesInLastMinute], [DelayedPricesInLastMinute], [TotalReceivedPricesInLastHour], [DelayedPricesInLastHour]) VALUES (50, 0, 0, 0, 0)
INSERT INTO [dbo].[PriceDelayStats] ([CounterpartyId], [TotalReceivedPricesInLastMinute], [DelayedPricesInLastMinute], [TotalReceivedPricesInLastHour], [DelayedPricesInLastHour]) VALUES (51, 0, 0, 0, 0)
GO


INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'LX1_ORD_Fluent', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
ResetOnLogout=Y
#no messages replying
PersistMessages=N

#LMAX (LM3 now)
SenderCompID=kgt
SenderSubID=kgt_LM3

SSLEnable=N

[SESSION]
#DataDictionary=none.xml
RelaxValidationForMessageTypes=8
BeginString=FIXT.1.1
DefaultApplVerID=FIX.5.0SP2
TargetCompID=FluentStreamTrades
#5pm NYK
StartTime=17:02:05
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30
BypassParsing=Y
UseDataDictionary=N

SocketConnectPort=30284
SocketConnectHost=38.76.0.115', GETUTCDATE())
GO

INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'FL1_ORD_Fluent', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
ResetOnLogout=Y
#no messages replying
PersistMessages=N

SenderCompID=kgt
SenderSubID=kgt_FL1

SSLEnable=N

[SESSION]
DataDictionary=FIXT11_FCM.xml
TransportDataDictionary=FIXT11_FCM.xml
#BypassParsing=Y
#UseDataDictionary=N
RelaxValidationForMessageTypes=D,Q
BeginString=FIXT.1.1
DefaultApplVerID=FIX.5.0SP2
TargetCompID=FluentStreamTrades
#5pm NYK
StartTime=17:02:05
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=34567
SocketConnectHost=149.5.64.204', GETUTCDATE())
GO

INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'FL1_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=SaxoPrime9KGTLNLL_LD4_PRICE

[SESSION]
DataDictionary=FIX42_FCM.xml
BeginString=FIX.4.2
TargetCompID=FASTMATCH_LD4_PRICE
#5pm NYK
StartTime=17:28:00
EndTime=16:59:00
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=34567
SocketConnectHost=111.111.111.111', GETUTCDATE())
GO

DECLARE @LX1OrdFixSettingId INT
DECLARE @FL1OrdFixSettingId INT
DECLARE @FL1QuoFixSettingId INT

SELECT
	@LX1OrdFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'LX1_ORD_Fluent'

SELECT
	@FL1OrdFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'FL1_ORD_Fluent'
	
SELECT
	@FL1QuoFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'FL1_QUO'


INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [IntegratorEnvironmentId], @LX1OrdFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]	
	
INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [IntegratorEnvironmentId], @FL1OrdFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]

INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])

SELECT 
	DISTINCT [IntegratorEnvironmentId], @FL1QuoFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]
GO


DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO

--new schema!!

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_DBK" type="FIXChannel_DBKSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM2" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM3" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LX1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC1" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC2" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS1" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS2" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTAstream" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3stream" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FL1" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
      <xs:element minOccurs="1" maxOccurs="1" name="STP" type="STPSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_DBKSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DealConfirmationSupported" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="L01LayersNum" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IocApplyTtl" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ApplyMaxHoldTime" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxHoldTime" type="FXCMMaxHoldTime" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MaxPerSymbolPricesPerSecondSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultMaxAllowedPerSecondRate" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialSymbolRateSettings" type="ArrayOfSymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolRateSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="SymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolRateSetting">
    <xs:attribute name="Name" type="xs:string" />
    <xs:attribute name="MaxAllowedPerSecondRate" type="xs:int" use="required" />
  </xs:complexType>
  <xs:complexType name="RejectionRateDisconnectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumOrdersToApplyCheck" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAllowedRejectionRatePercent" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="StreamingChannelSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxTotalPricesPerSecond" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="MaxPerSymbolPricesPerSecondSetting" type="MaxPerSymbolPricesPerSecondSetting" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExpectedPricingDepth" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="RejectionRateDisconnectSettings" type="RejectionRateDisconnectSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXSTPChannel_CTIPBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNongenuineInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialRejectionsHandling" type="ArrayOfSpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SpecialRejection" type="SpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="TriggerPhrase" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="FXCMMaxHoldTime">
    <xs:restriction base="xs:string">
      <xs:enumeration value="UpTo1ms" />
      <xs:enumeration value="UpTo30ms" />
      <xs:enumeration value="UpTo100ms" />
      <xs:enumeration value="UpTo500ms" />
      <xs:enumeration value="UpTo3000ms" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="MarketSessionIntoSeparateProcess" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="STPDestinations" type="ArrayOfSTPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSTPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="STPDestination" nillable="true" type="STPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Name" type="STPCounterparty" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedTicketTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="ServedCounterparties" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="STPCounterparty">
    <xs:restriction base="xs:string">
      <xs:enumeration value="CTIPB" />
      <xs:enumeration value="Traiana" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'


-- manually !!
-- add (after </FIXChannel_FS2>):
<FIXChannel_HTAstream>
    <MaxTotalPricesPerSecond>100</MaxTotalPricesPerSecond>
    <ExpectedPricingDepth>2</ExpectedPricingDepth>
    <RejectionRateDisconnectSettings>
      <RiskCheckAllowed>true</RiskCheckAllowed>
      <MinimumOrdersToApplyCheck>100</MinimumOrdersToApplyCheck>
      <MaxAllowedRejectionRatePercent>99</MaxAllowedRejectionRatePercent>
    </RejectionRateDisconnectSettings>
</FIXChannel_HTAstream>
<FIXChannel_HT3stream>
    <MaxTotalPricesPerSecond>100</MaxTotalPricesPerSecond>
    <ExpectedPricingDepth>2</ExpectedPricingDepth>
    <RejectionRateDisconnectSettings>
      <RiskCheckAllowed>true</RiskCheckAllowed>
      <MinimumOrdersToApplyCheck>100</MinimumOrdersToApplyCheck>
      <MaxAllowedRejectionRatePercent>99</MaxAllowedRejectionRatePercent>
    </RejectionRateDisconnectSettings>
</FIXChannel_HT3stream>
<FIXChannel_FL1>
    <MaxTotalPricesPerSecond>100</MaxTotalPricesPerSecond>
    <ExpectedPricingDepth>2</ExpectedPricingDepth>
    <RejectionRateDisconnectSettings>
      <RiskCheckAllowed>true</RiskCheckAllowed>
      <MinimumOrdersToApplyCheck>100</MinimumOrdersToApplyCheck>
      <MaxAllowedRejectionRatePercent>99</MaxAllowedRejectionRatePercent>
    </RejectionRateDisconnectSettings>
</FIXChannel_FL1>
--add (after </FIXChannel_LM3>): 
<FIXChannel_LX1>
	<Username>aaa</Username>
	<Password>ppp</Password>
</FIXChannel_LX1>

--also add LX1 wherever is L01

<CounterpartSetting>
			  <ShortName>LX1</ShortName>
			  <SettingAction>Add</SettingAction>
		  </CounterpartSetting>
		  
--ROLLBACK TRANSACTION AAA
--GO
--COMMIT TRANSACTION AAA
--GO