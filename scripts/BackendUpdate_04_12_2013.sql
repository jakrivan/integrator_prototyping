CREATE PROCEDURE [dbo].[GetStatisticsPerPair_SP] (
	@Day DATE
	)
AS
BEGIN
	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
		--following two make more sense after extracting multipliers behind brackets
		SUM(deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) AS PnlTermPol,
		SUM((deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) * termCurrency.LastKnownUsdConversionMultiplier) AS PnlTermPolInUsd,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1,
		SUM(ABS(deal.[AmountBasePolExecuted])* baseCurrency.LastKnownUsdConversionMultiplier) AS VolumeInUsd
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]
END
GO

GRANT EXECUTE ON [dbo].[GetStatisticsPerPair_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerPair_SP] TO [IntegratorServiceAccount] AS [dbo]
GO




CREATE FUNCTION csvlist_to_codes_tbl (@list nvarchar(MAX))
   RETURNS @tbl TABLE (code varchar(3) NOT NULL) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1
      INSERT @tbl (code)
         VALUES (substring(@list, @pos + 1, @valuelen))
      SELECT @pos = @nextpos
   END
   RETURN
END
GO


--Version with filtering (commented out) - turns out to be less effective that selecting everything
CREATE PROCEDURE [dbo].[GetStatisticsPerCounterparty_SP] (
	@Day DATE
	--,@CounterpartyCodesList VARCHAR(100) = NULL
	)
AS
BEGIN
	
	--DECLARE @temp_ids TABLE
	--(
	--	CounterpartyId int
	--)

	--IF @CounterpartyCodesList IS NOT NULL
	--BEGIN
	--	INSERT INTO @temp_ids(CounterpartyId)
	--	SELECT 
	--		CounterpartyId 
	--	FROM 
	--		dbo.Counterparty ctp 
	--		JOIN dbo.csvlist_to_codes_tbl(@CounterpartyCodesList) list ON ctp.CounterpartyCode = list.code
	--END
	
	DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		RejectionsNum int
	)

	INSERT INTO @temp_rejections(CounterpartyId, RejectionsNum)
	SELECT
		[CounterpartyId],
		COUNT([IntegratorReceivedExecutionReportUtc]) AS NumberOfRejections
	FROM
		[dbo].[DealExternalRejected] rej
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		DealsNum int
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol, VolumeBaseAbs, DealsNum)
	SELECT
		symbol.[Id] AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		symbol.[Id],
		deal.CounterpartyId
		


	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		RejectionsStatistics.RejectionsNum AS RejectionsNum,
		RejectionsStatistics.RejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.RejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0)) AS RejectionsRate
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			--SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(DealsNum) AS DealsNum
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(DealsNum) AS DealsNum
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.DealsNum AS DealsNum
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode
END
GO


GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_SP] TO [IntegratorServiceAccount] AS [dbo]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalIgnored](
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL,
	[IntegratorDetectedOrderIsIgnoredUtc] [datetime2](7) NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL
) ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [IX_OrderExternalIgnored_IntegratorSentExternalOrderUtc] ON [dbo].[OrderExternalIgnored]
(
	[IntegratorSentExternalOrderUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[OrderExternalIgnored]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalIgnored_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO
ALTER TABLE [dbo].[OrderExternalIgnored] CHECK CONSTRAINT [FK_OrderExternalIgnored_OrderExternal]
GO


CREATE PROCEDURE [dbo].[InsertOrderExternalIgnored_SP]( 
	@ExternalOrderSentUtc [datetime2](7),
	@ExternalOrderIgnoreDetectedUtc [datetime2](7),
	@IntegratorExternalOrderPrivateId [nvarchar](45)
	)
AS
BEGIN
	INSERT INTO 
	[dbo].[OrderExternalIgnored]
           ([IntegratorSentExternalOrderUtc]
           ,[IntegratorDetectedOrderIsIgnoredUtc]
           ,[IntegratorExternalOrderPrivateId])
     VALUES
           (@ExternalOrderSentUtc
           ,@ExternalOrderIgnoreDetectedUtc
           ,@IntegratorExternalOrderPrivateId)
END
GO

GRANT EXECUTE ON [dbo].[InsertOrderExternalIgnored_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

exec sp_rename 'GetIgnoredOrders_SP', 'GetIgnoredOrders_OLD_SP'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetIgnoredOrders_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN
	
	SELECT 
		ord.[IntegratorSentExternalOrderUtc] AS IntegratorSentExternalOrderUtc
		,ord.ExternalOrderLabelForCounterparty AS ExternalOrderLabelForCounterparty
		,ord.RequestedAmountBaseAbs AS RequestedAmountBaseAbs
		,ord.RequestedPrice AS RequestedPrice
		,ctp.CounterpartyCode AS CounterpartyCode
		,symbol.Name AS Symbol
		,direction.Name AS Direction
	FROM
		[dbo].[OrderExternalIgnored] ignoredOrder
		INNER JOIN [dbo].[OrderExternal] ord ON ignoredOrder.IntegratorSentExternalOrderUtc = ord.IntegratorSentExternalOrderUtc AND ignoredOrder.IntegratorExternalOrderPrivateId = ord.IntegratorExternalOrderPrivateId
		INNER JOIN [dbo].[Counterparty] ctp on ctp.CounterpartyId = ord.CounterpartyId
		INNER JOIN [dbo].Symbol symbol on symbol.Id = ord.SymbolId
		INNER JOIN [dbo].[DealDirection] direction on direction.Id = ord.DealDirectionId
	WHERE 
		ignoredOrder.[IntegratorSentExternalOrderUtc] BETWEEN @StartTime AND @EndTime

END
GO

GRANT EXECUTE ON [dbo].[GetIgnoredOrders_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO