BEGIN TRANSACTION AAA


INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (54, N'PrimeXM', N'PX1', 1)
GO


INSERT INTO [dbo].[CommissionsSchedule_CptCommission] ([CounterpartyId], [CounterpartyCommissionPpm], [ValidFromUtc], [ValidToUtc]) VALUES (54, 5, '1900-01-01', '9999-12-31')

INSERT INTO [dbo].[CommissionsSchedule_Mapping] ([CounterpartyId], [PrimeBrokerId], [ValidFromUtc], [ValidToUtc]) VALUES (54, 1, '2015-09-01', '9999-12-31')


INSERT INTO [dbo].[DelaySettings] 
	([CounterpartyId]
    ,[ExpectedDelayTicks]
      ,[MaxAllowedAdditionalDelayTicks]
      ,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
      ,[MinAdditionalDelayTicksToTurnGFO]
      ,[MinNegativeDelayTicksFromExpectedToTurnGFO])
SELECT
	54
	,[ExpectedDelayTicks]
	,[MaxAllowedAdditionalDelayTicks]
	,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
	,[MinAdditionalDelayTicksToTurnGFO]
	,[MinNegativeDelayTicksFromExpectedToTurnGFO]
FROM
	[dbo].[DelaySettings]
WHERE
	CounterpartyId = 25 -- FC1
	
INSERT INTO [dbo].[PriceDelayStats] ([CounterpartyId], [TotalReceivedPricesInLastMinute], [DelayedPricesInLastMinute], [TotalReceivedPricesInLastHour], [DelayedPricesInLastHour]) VALUES (54, 0, 0, 0, 0)
GO


INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'PX1_ORD_Fluent', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
ResetOnLogout=Y
#no messages replying
PersistMessages=N

#LMAX (LM3 now)
SenderCompID=kgt
SenderSubID=kgt_PRIMEXM

SSLEnable=N

[SESSION]
#DataDictionary=none.xml
RelaxValidationForMessageTypes=8
BeginString=FIXT.1.1
DefaultApplVerID=FIX.5.0SP2
TargetCompID=FluentStreamTrades
#5pm NYK
StartTime=17:02:05
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30
BypassParsing=Y
UseDataDictionary=N

SocketConnectPort=30465
SocketConnectHost=38.76.0.115', GETUTCDATE())
GO

--PROD
--IP Address: 176.124.191.35
--Port: 20467
--LoginUsername: primexm_fluent_kgt_q
--LoginPassword: pfWiu6H5oJVqHAL
--SenderCompID: Q084
--TargetCompID: XC102

INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'PX1_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=SAXO-QUOTE6399376

SSLEnable=N

[SESSION]
#DataDictionary=FIX44_LMX.xml
UseDataDictionary=N
BeginString=FIX.4.4
TargetCompID=PXM-QUOTE6399376
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30
BypassParsing=Y

SocketConnectPort=30642
SocketConnectHost=176.124.191.23', GETUTCDATE())
GO


DECLARE @PX1OrdFixSettingId INT
DECLARE @PX1QuoFixSettingId INT

SELECT
	@PX1OrdFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'PX1_ORD_Fluent'

SELECT
	@PX1QuoFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'PX1_QUO'


INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [IntegratorEnvironmentId], @PX1OrdFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]	
	
INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [IntegratorEnvironmentId], @PX1QuoFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]


DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO

--new schema!!

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_DBK" type="FIXChannel_DBKSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM2" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM3" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LX1" type="FIXChannel_LMXSettings" />
	  <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LGA" type="FIXChannel_LMXSettings" />
	  <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LGC" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC1" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC2" type="FIXChannel_FCMSettings" />
	  <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_PX1" type="FIXChannel_PXMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS1" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS2" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTAstream" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3stream" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FL1" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
      <xs:element minOccurs="1" maxOccurs="1" name="STP" type="STPSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_DBKSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DealConfirmationSupported" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="L01LayersNum" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IocApplyTtl" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ApplyMaxHoldTime" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxHoldTime" type="FXCMMaxHoldTime" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_PXMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
	  <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MaxPerSymbolPricesPerSecondSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultMaxAllowedPerSecondRate" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialSymbolRateSettings" type="ArrayOfSymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolRateSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="SymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolRateSetting">
    <xs:attribute name="Name" type="xs:string" />
    <xs:attribute name="MaxAllowedPerSecondRate" type="xs:int" use="required" />
  </xs:complexType>
  <xs:complexType name="RejectionRateDisconnectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumOrdersToApplyCheck" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAllowedRejectionRatePercent" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="StreamingChannelSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxTotalPricesPerSecond" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="MaxPerSymbolPricesPerSecondSetting" type="MaxPerSymbolPricesPerSecondSetting" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExpectedPricingDepth" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="RejectionRateDisconnectSettings" type="RejectionRateDisconnectSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXSTPChannel_CTIPBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNongenuineInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialRejectionsHandling" type="ArrayOfSpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SpecialRejection" type="SpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="TriggerPhrase" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="FXCMMaxHoldTime">
    <xs:restriction base="xs:string">
      <xs:enumeration value="UpTo1ms" />
      <xs:enumeration value="UpTo30ms" />
      <xs:enumeration value="UpTo100ms" />
      <xs:enumeration value="UpTo500ms" />
      <xs:enumeration value="UpTo3000ms" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="MarketSessionIntoSeparateProcess" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="STPDestinations" type="ArrayOfSTPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSTPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="STPDestination" nillable="true" type="STPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Name" type="STPCounterparty" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedTicketTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="ServedCounterparties" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="STPCounterparty">
    <xs:restriction base="xs:string">
      <xs:enumeration value="CTIPB" />
      <xs:enumeration value="Traiana" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'



-- manually !!
--add (after </FIXChannel_FC2>): 
<FIXChannel_PX1>
	<Username>Quote6399376</Username>
	<Password>saxo_2015</Password>
	<IocTtlMs>10</IocTtlMs>
</FIXChannel_PX1>


<CounterpartSetting>
	<ShortName>PX1</ShortName>
	<SettingAction>Add</SettingAction>
</CounterpartSetting>

--also add PX1 wherever is FC2





-------------------------------------------
-- Quoting changes
-----------------------------------------

ALTER TABLE [dbo].[VenueQuotingSettings] ADD [InactivityTimeoutSec] [int] NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ADD [LiquidationSpreadBp] [decimal](18,6) NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ADD [LmaxTickerLookbackSec] [int] NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ADD [MinimumLmaxTickerVolumeUsd] [bigint] NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ADD [MaximumLmaxTickerVolumeUsd] [bigint] NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ADD [MinimumLmaxTickerAvgDealSizeUsd] [bigint] NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ADD [MaximumLmaxTickerAvgDealSizeUsd] [bigint] NULL

ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ADD [InactivityTimeoutSec] [int] NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ADD [LiquidationSpreadBp] [decimal](18,6) NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ADD [LmaxTickerLookbackSec] [int] NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ADD [MinimumLmaxTickerVolumeUsd] [bigint] NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ADD [MaximumLmaxTickerVolumeUsd] [bigint] NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ADD [MinimumLmaxTickerAvgDealSizeUsd] [bigint] NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ADD [MaximumLmaxTickerAvgDealSizeUsd] [bigint] NULL
GO

UPDATE [dbo].[VenueQuotingSettings] SET 
	[InactivityTimeoutSec] = 5,
	[LiquidationSpreadBp] = 0.01,
	[LmaxTickerLookbackSec] = 60,
	[MinimumLmaxTickerVolumeUsd] = 5000000,
	[MaximumLmaxTickerVolumeUsd] = 1000000000,
	[MinimumLmaxTickerAvgDealSizeUsd] = 0,
	[MaximumLmaxTickerAvgDealSizeUsd] = 100000
	
	
UPDATE [dbo].[VenueQuotingSettings_Changes] SET 
	[InactivityTimeoutSec] = 5,
	[LiquidationSpreadBp] = 0.01,
	[LmaxTickerLookbackSec] = 60,
	[MinimumLmaxTickerVolumeUsd] = 5000000,
	[MaximumLmaxTickerVolumeUsd] = 1000000000,
	[MinimumLmaxTickerAvgDealSizeUsd] = 0,
	[MaximumLmaxTickerAvgDealSizeUsd] = 100000
	
GO

ALTER TABLE [dbo].[VenueQuotingSettings] ALTER COLUMN [InactivityTimeoutSec] [int] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ALTER COLUMN [LiquidationSpreadBp] [decimal](18,6) NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ALTER COLUMN [LmaxTickerLookbackSec] [int] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ALTER COLUMN [MinimumLmaxTickerVolumeUsd] [bigint] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ALTER COLUMN [MaximumLmaxTickerVolumeUsd] [bigint] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ALTER COLUMN [MinimumLmaxTickerAvgDealSizeUsd] [bigint] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ALTER COLUMN [MaximumLmaxTickerAvgDealSizeUsd] [bigint] NOT NULL

ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ALTER COLUMN [InactivityTimeoutSec] [int] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ALTER COLUMN [LiquidationSpreadBp] [decimal](18,6) NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ALTER COLUMN [LmaxTickerLookbackSec] [int] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ALTER COLUMN [MinimumLmaxTickerVolumeUsd] [bigint] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ALTER COLUMN [MaximumLmaxTickerVolumeUsd] [bigint] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ALTER COLUMN [MinimumLmaxTickerAvgDealSizeUsd] [bigint] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ALTER COLUMN [MaximumLmaxTickerAvgDealSizeUsd] [bigint] NOT NULL
GO

ALTER TRIGGER [dbo].[VenueQuotingSettings_DeletedOrUpdated] ON [dbo].[VenueQuotingSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueQuotingSettings_Changes]
		([SymbolId]
		,[CounterpartyId]
		,[TradingSystemId]  
		,[MaximumPositionBaseAbs]
		,[FixedSpreadBasisPoints]
		,[StopLossNetInUsd]
		,[SpreadTypeId]
		,[MinimumDynamicSpreadBp]
		,[DynamicMarkupDecimal]
		,[EnableSkew]
		,[SkewDecimal]
		,[InactivityTimeoutSec]
		,[LiquidationSpreadBp]
		,[LmaxTickerLookbackSec]
		,[MinimumLmaxTickerVolumeUsd]
		,[MaximumLmaxTickerVolumeUsd]
		,[MinimumLmaxTickerAvgDealSizeUsd]
		,[MaximumLmaxTickerAvgDealSizeUsd]
		,[ValidFromUtc]
		,[ValidToUtc]
		,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[MaximumPositionBaseAbs]
		,deleted.[FixedSpreadBasisPoints]
		,deleted.[StopLossNetInUsd]
		,deleted.[SpreadTypeId]
		,deleted.[MinimumDynamicSpreadBp]
		,deleted.[DynamicMarkupDecimal]
		,deleted.[EnableSkew]
		,deleted.[SkewDecimal]
		,deleted.[InactivityTimeoutSec]
		,deleted.[LiquidationSpreadBp]
		,deleted.[LmaxTickerLookbackSec]
		,deleted.[MinimumLmaxTickerVolumeUsd]
		,deleted.[MaximumLmaxTickerVolumeUsd]
		,deleted.[MinimumLmaxTickerAvgDealSizeUsd]
		,deleted.[MaximumLmaxTickerAvgDealSizeUsd]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueQuotingSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueQuotingSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([MaximumPositionBaseAbs]) AND (topUpdatedChange.[MaximumPositionBaseAbs] IS NULL OR topUpdatedChange.[MaximumPositionBaseAbs] != deleted.[MaximumPositionBaseAbs]))
		OR (UPDATE([FixedSpreadBasisPoints]) AND (topUpdatedChange.[FixedSpreadBasisPoints] IS NULL OR topUpdatedChange.[FixedSpreadBasisPoints] != deleted.[FixedSpreadBasisPoints]))
		OR (UPDATE([StopLossNetInUsd]) AND (topUpdatedChange.[StopLossNetInUsd] IS NULL OR topUpdatedChange.[StopLossNetInUsd] != deleted.[StopLossNetInUsd]))	
		OR (UPDATE([SpreadTypeId]) AND (topUpdatedChange.[SpreadTypeId] IS NULL OR topUpdatedChange.[SpreadTypeId] != deleted.[SpreadTypeId]))
		OR (UPDATE([MinimumDynamicSpreadBp]) AND (topUpdatedChange.[MinimumDynamicSpreadBp] IS NULL OR topUpdatedChange.[MinimumDynamicSpreadBp] != deleted.[MinimumDynamicSpreadBp]))
		OR (UPDATE([DynamicMarkupDecimal]) AND (topUpdatedChange.[DynamicMarkupDecimal] IS NULL OR topUpdatedChange.[DynamicMarkupDecimal] != deleted.[DynamicMarkupDecimal]))
		OR (UPDATE([EnableSkew]) AND (topUpdatedChange.[EnableSkew] IS NULL OR topUpdatedChange.[EnableSkew] != deleted.[EnableSkew]))
		OR (UPDATE([SkewDecimal]) AND (topUpdatedChange.[SkewDecimal] IS NULL OR topUpdatedChange.[SkewDecimal] != deleted.[SkewDecimal]))
		OR (UPDATE([InactivityTimeoutSec]) AND (topUpdatedChange.[InactivityTimeoutSec] IS NULL OR topUpdatedChange.[InactivityTimeoutSec] != deleted.[InactivityTimeoutSec]))
		OR (UPDATE([LiquidationSpreadBp]) AND (topUpdatedChange.[LiquidationSpreadBp] IS NULL OR topUpdatedChange.[LiquidationSpreadBp] != deleted.[LiquidationSpreadBp]))
		OR (UPDATE([LmaxTickerLookbackSec]) AND (topUpdatedChange.[LmaxTickerLookbackSec] IS NULL OR topUpdatedChange.[LmaxTickerLookbackSec] != deleted.[LmaxTickerLookbackSec]))
		OR (UPDATE([MinimumLmaxTickerVolumeUsd]) AND (topUpdatedChange.[MinimumLmaxTickerVolumeUsd] IS NULL OR topUpdatedChange.[MinimumLmaxTickerVolumeUsd] != deleted.[MinimumLmaxTickerVolumeUsd]))
		OR (UPDATE([MaximumLmaxTickerVolumeUsd]) AND (topUpdatedChange.[MaximumLmaxTickerVolumeUsd] IS NULL OR topUpdatedChange.[MaximumLmaxTickerVolumeUsd] != deleted.[MaximumLmaxTickerVolumeUsd]))
		OR (UPDATE([MinimumLmaxTickerAvgDealSizeUsd]) AND (topUpdatedChange.[MinimumLmaxTickerAvgDealSizeUsd] IS NULL OR topUpdatedChange.[MinimumLmaxTickerAvgDealSizeUsd] != deleted.[MinimumLmaxTickerAvgDealSizeUsd]))
		OR (UPDATE([MaximumLmaxTickerAvgDealSizeUsd]) AND (topUpdatedChange.[MaximumLmaxTickerAvgDealSizeUsd] IS NULL OR topUpdatedChange.[MaximumLmaxTickerAvgDealSizeUsd] != deleted.[MaximumLmaxTickerAvgDealSizeUsd]))
END
GO


ALTER PROCEDURE [dbo].[GetUpdatedVenueQuotingSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueQuotingSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty			
			,[MaximumPositionBaseAbs]
			,[FixedSpreadBasisPoints]
			,[StopLossNetInUsd]	
			,spreadType.[VenueQuotingSettingsSpreadTypeName] AS SpreadType
			,[MinimumDynamicSpreadBp]
			,[DynamicMarkupDecimal]
			,[EnableSkew]
			,[SkewDecimal]
			,[InactivityTimeoutSec]
			,[LiquidationSpreadBp]
			,[LmaxTickerLookbackSec]
			,[MinimumLmaxTickerVolumeUsd]
			,[MaximumLmaxTickerVolumeUsd]
			,[MinimumLmaxTickerAvgDealSizeUsd]
			,[MaximumLmaxTickerAvgDealSizeUsd]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueQuotingSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
			INNER JOIN [dbo].[VenueQuotingSettingsSpreadType] spreadType ON sett.[SpreadTypeId] = spreadType.[VenueQuotingSettingsSpreadTypeId]
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name 
	END
END
GO

ALTER PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FixedSpreadBasisPoints],
		sett.[FixedSpreadBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [FixedSpreadDecimal],
		sett.[StopLossNetInUsd],
		spreadType.[VenueQuotingSettingsSpreadTypeName] AS SpreadType,
		sett.[MinimumDynamicSpreadBp],
		sett.[MinimumDynamicSpreadBp] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumDynamicSpreadDecimal],
		sett.[DynamicMarkupDecimal],
		sett.[EnableSkew],
		sett.[SkewDecimal],
		sett.[InactivityTimeoutSec],
		sett.[LiquidationSpreadBp],
		sett.[LiquidationSpreadBp] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [LiquidationSpreadDecimal],
		sett.[LmaxTickerLookbackSec],
		sett.[MinimumLmaxTickerVolumeUsd],
		sett.[MaximumLmaxTickerVolumeUsd],
		sett.[MinimumLmaxTickerAvgDealSizeUsd],
		sett.[MaximumLmaxTickerAvgDealSizeUsd],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		INNER JOIN [dbo].[VenueQuotingSettingsSpreadType] spreadType ON sett.[SpreadTypeId] = spreadType.[VenueQuotingSettingsSpreadTypeId]
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[VenueQuotingSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MaximumPositionBaseAbs [decimal](18,0),
	@FixedSpreadBasisPoints [decimal](18,6),
	@StopLossNetInUsd [bigint],
	@SpreadType [varchar](8),
	@MinimumDynamicSpreadBp [decimal](18,6),
	@DynamicMarkupDecimal [decimal](18,10),
	@EnableSkew [bit],
	@SkewDecimal [decimal](18,10),
	@InactivityTimeoutSec [int],
	@LiquidationSpreadBp [decimal](18,6),
	@LmaxTickerLookbackSec [int],
	@MinimumLmaxTickerVolumeUsd [bigint],
	@MaximumLmaxTickerVolumeUsd [bigint],
	@MinimumLmaxTickerAvgDealSizeUsd [bigint],
	@MaximumLmaxTickerAvgDealSizeUsd [bigint],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @SpreadTypeId BIT
	SELECT @SpreadTypeId = [VenueQuotingSettingsSpreadTypeId] FROM [dbo].[VenueQuotingSettingsSpreadType] WHERE [VenueQuotingSettingsSpreadTypeName] = @SpreadType
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'SpreadType not found in DB: %s', 16, 2, @SpreadType) WITH SETERROR
	END

	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Quoting'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Quoting TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;

		INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)
		SELECT @TradingSystemId = scope_identity();

		--make sure that default switches values are correct
		INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
		SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
		FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
		WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
		INSERT INTO [dbo].[VenueQuotingSettings]
			([SymbolId]
			,[MaximumPositionBaseAbs]
			,[FixedSpreadBasisPoints]
			,[StopLossNetInUsd]   
			,[SpreadTypeId]
			,[MinimumDynamicSpreadBp]
			,[DynamicMarkupDecimal]
			,[EnableSkew]
			,[SkewDecimal]
			,[InactivityTimeoutSec]
			,[LiquidationSpreadBp]
			,[LmaxTickerLookbackSec]
			,[MinimumLmaxTickerVolumeUsd]
			,[MaximumLmaxTickerVolumeUsd]
			,[MinimumLmaxTickerAvgDealSizeUsd]
			,[MaximumLmaxTickerAvgDealSizeUsd]
			,[CounterpartyId]
			,[TradingSystemId])
		 VALUES
			(@SymbolId
			,@MaximumPositionBaseAbs
			,@FixedSpreadBasisPoints
			,@StopLossNetInUsd
			,@SpreadTypeId 
			,@MinimumDynamicSpreadBp
			,@DynamicMarkupDecimal
			,@EnableSkew
			,@SkewDecimal
			,@InactivityTimeoutSec
			,@LiquidationSpreadBp
			,@LmaxTickerLookbackSec
			,@MinimumLmaxTickerVolumeUsd
			,@MaximumLmaxTickerVolumeUsd
			,@MinimumLmaxTickerAvgDealSizeUsd
			,@MaximumLmaxTickerAvgDealSizeUsd
			,@CounterpartyId
			,@TradingSystemId)

		COMMIT TRANSACTION;

	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO


ALTER PROCEDURE [dbo].[VenueQuotingSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumPositionBaseAbs [decimal](18,0),
	@FixedSpreadBasisPoints [decimal](18,6),
	@StopLossNetInUsd [bigint],
	@SpreadType [varchar](8),
	@MinimumDynamicSpreadBp [decimal](18,6),
	@DynamicMarkupDecimal [decimal](18,10),
	@EnableSkew [bit],
	@SkewDecimal [decimal](18,10),
	@InactivityTimeoutSec [int],
	@LiquidationSpreadBp [decimal](18,6),
	@LmaxTickerLookbackSec [int],
	@MinimumLmaxTickerVolumeUsd [bigint],
	@MaximumLmaxTickerVolumeUsd [bigint],
	@MinimumLmaxTickerAvgDealSizeUsd [bigint],
	@MaximumLmaxTickerAvgDealSizeUsd [bigint]
)
AS
BEGIN
	DECLARE @SpreadTypeId BIT
	SELECT @SpreadTypeId = [VenueQuotingSettingsSpreadTypeId] FROM [dbo].[VenueQuotingSettingsSpreadType] WHERE [VenueQuotingSettingsSpreadTypeName] = @SpreadType
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'SpreadType not found in DB: %s', 16, 2, @SpreadType) WITH SETERROR
	END

	UPDATE sett
	SET
		[MaximumPositionBaseAbs] = @MaximumPositionBaseAbs,
		[FixedSpreadBasisPoints] = @FixedSpreadBasisPoints,
		[StopLossNetInUsd] = @StopLossNetInUsd,
		[SpreadTypeId] = @SpreadTypeId,
		[MinimumDynamicSpreadBp] = @MinimumDynamicSpreadBp,
		[DynamicMarkupDecimal] = @DynamicMarkupDecimal,
		[EnableSkew] = @EnableSkew,
		[SkewDecimal] = @SkewDecimal,
		[InactivityTimeoutSec] = @InactivityTimeoutSec,
		[LiquidationSpreadBp] = @LiquidationSpreadBp,
		[LmaxTickerLookbackSec] = @LmaxTickerLookbackSec,
		[MinimumLmaxTickerVolumeUsd] = @MinimumLmaxTickerVolumeUsd,
		[MaximumLmaxTickerVolumeUsd] = @MaximumLmaxTickerVolumeUsd,
		[MinimumLmaxTickerAvgDealSizeUsd] = @MinimumLmaxTickerAvgDealSizeUsd,
		[MaximumLmaxTickerAvgDealSizeUsd] = @MaximumLmaxTickerAvgDealSizeUsd
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

ALTER PROCEDURE [dbo].[GetLastVenueQuotingSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MaximumPositionBaseAbs]
		,[FixedSpreadBasisPoints]
		,[StopLossNetInUsd]
		,spreadType.[VenueQuotingSettingsSpreadTypeName] AS SpreadType
		,[MinimumDynamicSpreadBp]
		,[DynamicMarkupDecimal]
		,[EnableSkew]
		,[SkewDecimal]
		,[InactivityTimeoutSec]
		,[LiquidationSpreadBp]
		,[LmaxTickerLookbackSec]
		,[MinimumLmaxTickerVolumeUsd]
		,[MaximumLmaxTickerVolumeUsd]
		,[MinimumLmaxTickerAvgDealSizeUsd]
		,[MaximumLmaxTickerAvgDealSizeUsd]
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[VenueQuotingSettingsSpreadType] spreadType ON sett.[SpreadTypeId] = spreadType.[VenueQuotingSettingsSpreadTypeId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END
GO


-------------------------------------------
-- Visualisation
------------------------------------------


CREATE TABLE [dbo].[VenueQuotingIntegratorStats](
	[TradingSystemId] [int] NOT NULL,
	[CurrentLmaxTickerVolumeUsd] [bigint] NULL,
	[CurrentLmaxTickerAvgDealSizeUsd] [bigint] NULL,
	[CalculatedFromFullInterval] [bit] NULL
 CONSTRAINT [UNQ__VenueQuotingIntegratorStats__TradingSystemId] UNIQUE NONCLUSTERED 
(
	[TradingSystemId] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FixedSpreadBasisPoints],
		sett.[FixedSpreadBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [FixedSpreadDecimal],
		sett.[StopLossNetInUsd],
		spreadType.[VenueQuotingSettingsSpreadTypeName] AS SpreadType,
		sett.[MinimumDynamicSpreadBp],
		sett.[MinimumDynamicSpreadBp] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumDynamicSpreadDecimal],
		sett.[DynamicMarkupDecimal],
		sett.[EnableSkew],
		sett.[SkewDecimal],
		sett.[InactivityTimeoutSec],
		sett.[LiquidationSpreadBp],
		sett.[LiquidationSpreadBp] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [LiquidationSpreadDecimal],
		sett.[LmaxTickerLookbackSec],
		sett.[MinimumLmaxTickerVolumeUsd],
		sett.[MaximumLmaxTickerVolumeUsd],
		sett.[MinimumLmaxTickerAvgDealSizeUsd],
		sett.[MaximumLmaxTickerAvgDealSizeUsd],
		integratorStats.[CurrentLmaxTickerVolumeUsd],
		integratorStats.[CurrentLmaxTickerAvgDealSizeUsd],
		integratorStats.[CalculatedFromFullInterval],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		INNER JOIN [dbo].[VenueQuotingSettingsSpreadType] spreadType ON sett.[SpreadTypeId] = spreadType.[VenueQuotingSettingsSpreadTypeId]
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
		LEFT JOIN [dbo].[VenueQuotingIntegratorStats] integratorStats ON integratorStats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO


CREATE PROCEDURE [dbo].[VenueQuotingIntegratorStatsUpdateSingle_SP]
(
	@TradingSystemId int,
	@CurrentLmaxTickerVolumeUsd [bigint],
	@CurrentLmaxTickerAvgDealSizeUsd [bigint],
	@CalculatedFromFullInterval [bit]
)
AS
BEGIN
	--WARNING - this doesn't solve concurrency as it expects to be executed serializable once every few seconds

	UPDATE
		[dbo].[VenueQuotingIntegratorStats]
	SET
		[CurrentLmaxTickerVolumeUsd] = @CurrentLmaxTickerVolumeUsd,
		[CurrentLmaxTickerAvgDealSizeUsd] = @CurrentLmaxTickerAvgDealSizeUsd,
		[CalculatedFromFullInterval] = @CalculatedFromFullInterval
	WHERE
		[TradingSystemId] = @TradingSystemId
		
	IF @@rowcount = 0
	BEGIN
		INSERT INTO [dbo].[VenueQuotingIntegratorStats] 
			([TradingSystemId],
			[CurrentLmaxTickerVolumeUsd],
			[CurrentLmaxTickerAvgDealSizeUsd],
			[CalculatedFromFullInterval])
		VALUES
			(@TradingSystemId,
			@CurrentLmaxTickerVolumeUsd,
			@CurrentLmaxTickerAvgDealSizeUsd,
			@CalculatedFromFullInterval)
	END
END
GO

GRANT EXECUTE ON [dbo].[VenueQuotingIntegratorStatsUpdateSingle_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

















--ROLLBACK TRANSACTION AAA
--GO
--COMMIT TRANSACTION AAA
--GO
