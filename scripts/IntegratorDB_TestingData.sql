USE [Integrator_Testing]
GO
SET IDENTITY_INSERT [dbo].[IntegratorEnvironmentType] ON 

GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (1, N'IntegratorService', N'Environment for IntegratorService')
GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (2, N'Client', N'Environment for Integrator Client started via IntegratorServiceHost')
GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (3, N'Dashboard', N'Environment for DiagnosticsDashboard (AKA Counterparty Monitor)')
GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (4, N'CommandsConsole', N'Environment for ConsoleRunner started solely for purposes of running commands')
GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (5, N'IntegratorWatchdog', N'Environment for IntegratorWatchdog windows service')
GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (6, N'IntegratorService_old', N'Environment for IntegratorService (previous version)')
GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (7, N'Splitter', N'Environment for Splitter')
GO
SET IDENTITY_INSERT [dbo].[IntegratorEnvironmentType] OFF
GO
SET IDENTITY_INSERT [dbo].[IntegratorEnvironment] ON 

GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [IntegratorEnvironmentTypeId], [Description]) VALUES (10, N'J_UAT_clients', 2, NULL)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [IntegratorEnvironmentTypeId], [Description]) VALUES (4, N'M_UAT_clients', 2, NULL)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [IntegratorEnvironmentTypeId], [Description]) VALUES (9, N'UAT_client_test01', 2, NULL)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [IntegratorEnvironmentTypeId], [Description]) VALUES (6, N'UAT_CommandsConsole', 4, N'command console on localhost')
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [IntegratorEnvironmentTypeId], [Description]) VALUES (5, N'UAT_Dashboard', 3, N'dashboard on localhost')
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [IntegratorEnvironmentTypeId], [Description]) VALUES (8, N'UAT_Dashboard_privateNet', 3, N'dashboard in LD4')
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [IntegratorEnvironmentTypeId], [Description]) VALUES (2, N'UAT_dev01', 1, NULL)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [IntegratorEnvironmentTypeId], [Description]) VALUES (3, N'UAT_dev02', 1, NULL)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [IntegratorEnvironmentTypeId], [Description]) VALUES (1, N'UAT_KrivanekDev', 1, NULL)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [IntegratorEnvironmentTypeId], [Description]) VALUES (12, N'UAT_M_HotspotSystem02_Client_PublicIP', 2, NULL)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [IntegratorEnvironmentTypeId], [Description]) VALUES (13, N'UAT_M_IntegratorOnlineView_Client_PublicIP', 2, NULL)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [IntegratorEnvironmentTypeId], [Description]) VALUES (11, N'UAT_M_Splitter_PublicIP', 7, NULL)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [IntegratorEnvironmentTypeId], [Description]) VALUES (7, N'UAT_Splitter', 7, N'splitter on localhost')
GO
SET IDENTITY_INSERT [dbo].[IntegratorEnvironment] OFF
GO
SET IDENTITY_INSERT [dbo].[Settings] ON 

GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (3, N'Kreslik.Integrator.RiskManagement.dll', NULL, 1, N'<RiskManagementSettings xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><ConfigLastUpdated>2013-11-08-1225-UTC</ConfigLastUpdated><ExternalOrdersSubmissionRate><RiskCheckAllowed>true</RiskCheckAllowed><MaximumAllowedInstancesPerTimeInterval>50</MaximumAllowedInstancesPerTimeInterval><TimeIntervalToCheck_Seconds>5</TimeIntervalToCheck_Seconds></ExternalOrdersSubmissionRate><ExternalOrdersRejectionRate><RiskCheckAllowed>true</RiskCheckAllowed><MaximumAllowedInstancesPerTimeInterval>100</MaximumAllowedInstancesPerTimeInterval><TimeIntervalToCheck_Seconds>40</TimeIntervalToCheck_Seconds></ExternalOrdersRejectionRate><ExternalOrderMaximumAllowedSize><RiskCheckAllowed>true</RiskCheckAllowed><MaximumAllowedBaseAbsSizeInUsd>5000000</MaximumAllowedBaseAbsSizeInUsd></ExternalOrderMaximumAllowedSize><PriceAndPosition><ExternalNOPRiskCheckAllowed>true</ExternalNOPRiskCheckAllowed><MaximumExternalNOPInUSD>1200000000</MaximumExternalNOPInUSD><MaximumAllowedNOPCalculationTime_Milliseconds>2</MaximumAllowedNOPCalculationTime_Milliseconds><PriceDeviationCheckAllowed>true</PriceDeviationCheckAllowed><OnlinePriceDeviationCheckMaxPriceAge_Minutes>15</OnlinePriceDeviationCheckMaxPriceAge_Minutes><ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed>true</ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed><Symbols><Symbol ShortName="AUDCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="AUDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="AUDHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="AUDCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="AUDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="AUDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="AUDNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="AUDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="AUDSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="AUDUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="CADDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="CADCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="CADJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="CADNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="CADNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="CADSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="DKKJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="DKKNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="DKKSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURAUD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURCNH" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURGBP" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURRUB" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURSKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURTRY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="EURZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="GBPAUD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="GBPCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="GBPCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="GBPDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="GBPHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="GBPCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="GBPJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="GBPNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="GBPNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="GBPPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="GBPSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="GBPUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="HKDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="CHFDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="CHFJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="CHFNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="CHFSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="MXNJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="NOKJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="NOKSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="NZDCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="NZDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="NZDCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="NZDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="NZDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="NZDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="NZDSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="NZDUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="SGDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDCNH" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDILS" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDRUB" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDSKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDTRY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="USDZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /><Symbol ShortName="ZARJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15" /></Symbols></PriceAndPosition></RiskManagementSettings>', CAST(0x0000A29000FCA4BD AS DateTime), 1)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (4, N'Kreslik.Integrator.BusinessLayer.dll', NULL, 1, N'<BusinessLayerSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><MarketableClientOrdersMatchingStrategy>ImmediateMatching</MarketableClientOrdersMatchingStrategy><ConsecutiveRejectionsStopStrategyBehavior><StrategyEnabled>true</StrategyEnabled><NumerOfRejectionsToTriggerStrategy>3</NumerOfRejectionsToTriggerStrategy><CounterpartyRankWeightMultiplier>20</CounterpartyRankWeightMultiplier><PriceAgeRankWeightMultiplier>1</PriceAgeRankWeightMultiplier><PriceBpRankWeightMultiplier>20</PriceBpRankWeightMultiplier><DefaultCounterpartyRank>10</DefaultCounterpartyRank><CounterpartyRanks><CounterpartyRank CounterpartyCode="UBS" Rank="5" /><CounterpartyRank CounterpartyCode="MGS" Rank="5.2" /><CounterpartyRank CounterpartyCode="RBS" Rank="5.5" /><CounterpartyRank CounterpartyCode="CTI" Rank="5.8" /><CounterpartyRank CounterpartyCode="CRS" Rank="6.1" /><CounterpartyRank CounterpartyCode="BOA" Rank="6.8" /><CounterpartyRank CounterpartyCode="CZB" Rank="7.1" /><CounterpartyRank CounterpartyCode="GLS" Rank="7.4" /><CounterpartyRank CounterpartyCode="JPM" Rank="7.7" /><CounterpartyRank CounterpartyCode="BNP" Rank="8.9" /><CounterpartyRank CounterpartyCode="BRX" Rank="9.5" /><CounterpartyRank CounterpartyCode="SOC" Rank="9.5" /><CounterpartyRank CounterpartyCode="HSB" Rank="10" /><CounterpartyRank CounterpartyCode="NOM" Rank="10" /></CounterpartyRanks></ConsecutiveRejectionsStopStrategyBehavior><StartWithSessionsStopped>false</StartWithSessionsStopped><AutoKillInternalOrdersOnOrdersTransmissionDisabled>true</AutoKillInternalOrdersOnOrdersTransmissionDisabled><StartStopEmailSubject>Integrator service on {0} is {1}</StartStopEmailSubject><StartStopEmailBody /><StartStopEmailTo>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</StartStopEmailTo><StartStopEmailCc /><IgnoredOrderCounterpartyContacts><IgnoredOrderCounterpartyContact CounterpartyCode="CRS" EmailContacts="list.efx-support@credit-suisse.com;ivan.dexeus@credit-suisse.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="UBS" EmailContacts="fxehelp@ubs.com;gianandrea.grassi@ubs.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="DBK" EmailContacts="gm-ecommerce.implementations@db.com;thomas-m.geist@db.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="CTI" EmailContacts="fifx.support@citi.com;kenneth.aina@citi.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="BOA" EmailContacts="efx.csg@baml.com;kenneth.aina@citi.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="MGS" EmailContacts="fxacctmgt@morganstanley.com;guy.hopkins@morganstanley.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="RBS" EmailContacts="GBMEMFXTradeTimeouts@rbs.com;Martin.Pilcher@rbs.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="HSB" EmailContacts="fix.support@hsbcib.com;olivier.werenne@hsbcib.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="JPM" EmailContacts="JPMorgan_FXEcom_Urgent@jpmorgan.com;jon.price@jpmorgan.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="GLS" EmailContacts="ficc-fx-edealing-integration@gs.com;tom.robinson@gs.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="BNP" EmailContacts="siamak.nouri@uk.bnpparibas.com;steve.earl@uk.bnpparibas.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="NOM" EmailContacts="FI-ECommerceTechFXSupport@nomura.com;nicola.dilena@nomura.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="CZB" EmailContacts="esupport@commerzbank.com;Roland.White@commerzbank.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="BRX" EmailContacts="BARXsupport@barclays.com;conor.daly@barclays.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="SOC" EmailContacts="FX-support@sgcib.com;leon.brown@sgcib.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="HTA" EmailContacts="HOTFX.PRODSUPPORT@knight.com;HOTFX.LIQUIDITY@knight.com;pgertler@kcg.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="HTF" EmailContacts="HOTFX.PRODSUPPORT@knight.com;HOTFX.LIQUIDITY@knight.com;pgertler@kcg.com" /></IgnoredOrderCounterpartyContacts><IgnoredOrderEmailCc>michal.kreslik@kgtinv.com;marek.fogiel@kgtinv.com;jan.krivanek@kgtinv.com</IgnoredOrderEmailCc><FatalErrorsEmailTo>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</FatalErrorsEmailTo><FatalErrorsEmailCc /><FatalErrorsEmailMinDelayBetweenEmails_Minutes>10</FatalErrorsEmailMinDelayBetweenEmails_Minutes><PreventMatchingToSameCounterpartyAfterOrderRejectBehavior><StrategyEnabled>true</StrategyEnabled><CounterpartyCodes><Counterparty>GLS</Counterparty></CounterpartyCodes></PreventMatchingToSameCounterpartyAfterOrderRejectBehavior></BusinessLayerSettings>', CAST(0x0000A2B200E19576 AS DateTime), 1)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (8, N'Kreslik.Integrator.Common.dll', NULL, 1, N'<CommonSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Logging><AvailableDiskSpaceThresholdToStopMDLogging_GB>15</AvailableDiskSpaceThresholdToStopMDLogging_GB></Logging><Automailer><SmtpServer>173.194.70.108</SmtpServer><SmtpPort>587</SmtpPort><BypassCertificationValidation>true</BypassCertificationValidation><SmtpUsername>automailer@kgtinv.com</SmtpUsername><SmtpPassword>Np9G3bBd</SmtpPassword><DirectoryForLocalEmails>LocalEmails</DirectoryForLocalEmails><SendEmailsOnlyToLocalFolder>true</SendEmailsOnlyToLocalFolder></Automailer></CommonSettings>', CAST(0x0000A292011B3CC7 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (9, N'Kreslik.Integrator.DAL.dll', N'Remote DAL (public IP)', 1, N'<DALSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><!--<ConnectionString>Data Source=192.168.75.66;Initial Catalog=Integrator;User ID=IntegratorKillSwitchUser;Password=aaa333+++</ConnectionString>--><ConnectionString>Data Source=37.46.6.116;Initial Catalog=Integrator_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString><!--<ConnectionString>Data Source=37.46.6.116;Initial Catalog=Integrator;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString>--><KillSwitchPollIntervalSeconds>3</KillSwitchPollIntervalSeconds><KillSwitchWatchDogIntervalSeconds>40</KillSwitchWatchDogIntervalSeconds><IntegratorSwitchName>KGTIntegratorKillSwitch</IntegratorSwitchName><CommandDistributorPollIntervalSeconds>3</CommandDistributorPollIntervalSeconds><DataCollection><DataCollectionBackendConnectionString>Data Source=37.46.6.106;Initial Catalog=DataCollection_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</DataCollectionBackendConnectionString><DataProviderId>48</DataProviderId><SourceFileId>932696</SourceFileId><EnableToBDataCollection>true</EnableToBDataCollection><EnableMarketDataCollection>true</EnableMarketDataCollection><MaxBcpBatchSize>10000</MaxBcpBatchSize><MaxInMemoryColectionSize>1000000</MaxInMemoryColectionSize><MaxIntervalBetweenUploads_Seconds>10</MaxIntervalBetweenUploads_Seconds><MaxUploadTimeAfterWhichSerializationStarts_Seconds>5</MaxUploadTimeAfterWhichSerializationStarts_Seconds><MaxProcessingTimeAfterShutdownSignalled_Seconds>30</MaxProcessingTimeAfterShutdownSignalled_Seconds><WaitIntervalBetweenBcpRetries_Seconds>2</WaitIntervalBetweenBcpRetries_Seconds><WaitIntervalBeforeShutdownStartsSerialization_Seconds>4</WaitIntervalBeforeShutdownStartsSerialization_Seconds></DataCollection></DALSettings>', CAST(0x0000A292011BEB22 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (10, N'Kreslik.Integrator.DiagnosticsDashboard.exe', N'J_ DiagDashboard default', 1, N'<DiagnosticsDashboardSettings xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><RefreshTimeout_Miliseconds>500</RefreshTimeout_Miliseconds><FirstSpreadAgeLimit_Seconds>10</FirstSpreadAgeLimit_Seconds><SecondSpreadAgeLimit_Seconds>60</SecondSpreadAgeLimit_Seconds><ThirdSpreadAgeLimit_Seconds>1200</ThirdSpreadAgeLimit_Seconds><CutOffSpreadAgeLimit_Hours>24</CutOffSpreadAgeLimit_Hours><EnabledSymbols><Symbol>AUD/CAD</Symbol><Symbol>AUD/CHF</Symbol><Symbol>AUD/DKK</Symbol><Symbol>AUD/HKD</Symbol><Symbol>AUD/JPY</Symbol><Symbol>AUD/NOK</Symbol><Symbol>AUD/NZD</Symbol><Symbol>AUD/SEK</Symbol><Symbol>AUD/SGD</Symbol><Symbol>AUD/USD</Symbol><Symbol>CAD/CHF</Symbol><Symbol>CAD/JPY</Symbol><Symbol>CAD/NOK</Symbol><Symbol>CAD/NZD</Symbol><Symbol>CAD/SEK</Symbol><Symbol>CHF/DKK</Symbol><Symbol>CHF/JPY</Symbol><Symbol>CHF/NOK</Symbol><Symbol>CHF/SEK</Symbol><Symbol>DKK/JPY</Symbol><Symbol>DKK/NOK</Symbol><Symbol>DKK/SEK</Symbol><Symbol>EUR/AUD</Symbol><Symbol>EUR/CAD</Symbol><Symbol>EUR/CHF</Symbol><Symbol>EUR/CNH</Symbol><Symbol>EUR/CZK</Symbol><Symbol>EUR/DKK</Symbol><Symbol>EUR/GBP</Symbol><Symbol>EUR/HKD</Symbol><Symbol>EUR/HUF</Symbol><Symbol>EUR/JPY</Symbol><Symbol>EUR/MXN</Symbol><Symbol>EUR/NOK</Symbol><Symbol>EUR/NZD</Symbol><Symbol>EUR/PLN</Symbol><Symbol>EUR/RUB</Symbol><Symbol>EUR/SEK</Symbol><Symbol>EUR/SKK</Symbol><Symbol>EUR/TRY</Symbol><Symbol>EUR/USD</Symbol><Symbol>EUR/ZAR</Symbol><Symbol>GBP/AUD</Symbol><Symbol>GBP/CAD</Symbol><Symbol>GBP/CHF</Symbol><Symbol>GBP/CZK</Symbol><Symbol>GBP/DKK</Symbol><Symbol>GBP/HUF</Symbol><Symbol>GBP/JPY</Symbol><Symbol>GBP/NOK</Symbol><Symbol>GBP/NZD</Symbol><Symbol>GBP/PLN</Symbol><Symbol>GBP/SEK</Symbol><Symbol>GBP/USD</Symbol><Symbol>HKD/JPY</Symbol><Symbol>MXN/JPY</Symbol><Symbol>NOK/JPY</Symbol><Symbol>NOK/SEK</Symbol><Symbol>NZD/CAD</Symbol><Symbol>NZD/CHF</Symbol><Symbol>NZD/DKK</Symbol><Symbol>NZD/JPY</Symbol><Symbol>NZD/NOK</Symbol><Symbol>NZD/SEK</Symbol><Symbol>NZD/SGD</Symbol><Symbol>NZD/USD</Symbol><Symbol>SGD/JPY</Symbol><Symbol>USD/CAD</Symbol><Symbol>USD/CHF</Symbol><Symbol>USD/CNH</Symbol><Symbol>USD/CZK</Symbol><Symbol>USD/DKK</Symbol><Symbol>USD/HKD</Symbol><Symbol>USD/HUF</Symbol><Symbol>USD/ILS</Symbol><Symbol>USD/JPY</Symbol><Symbol>USD/MXN</Symbol><Symbol>USD/NOK</Symbol><Symbol>USD/PLN</Symbol><Symbol>USD/RUB</Symbol><Symbol>USD/SEK</Symbol><Symbol>USD/SGD</Symbol><Symbol>USD/SKK</Symbol><Symbol>USD/TRY</Symbol><Symbol>USD/ZAR</Symbol><Symbol>ZAR/JPY</Symbol></EnabledSymbols><EnabledCounterparties><Counterparty>CRS</Counterparty><Counterparty>UBS</Counterparty><Counterparty>DBK</Counterparty><Counterparty>CTI</Counterparty><Counterparty>BOA</Counterparty><Counterparty>MGS</Counterparty><Counterparty>RBS</Counterparty><Counterparty>HSB</Counterparty><Counterparty>JPM</Counterparty><Counterparty>GLS</Counterparty><Counterparty>BNP</Counterparty><Counterparty>NOM</Counterparty><Counterparty>CZB</Counterparty><Counterparty>BRX</Counterparty><Counterparty>SOC</Counterparty><Counterparty>HTA</Counterparty><Counterparty>HTF</Counterparty></EnabledCounterparties></DiagnosticsDashboardSettings>', CAST(0x0000A292011BEB35 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (11, N'Kreslik.Integrator.IntegratorClientHost.exe', N'J_allTestingClientHosts', 1, N'<IntegratorClientHostSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><DefaultIntegratorClientCofigurationIdentity>BasicTrading</DefaultIntegratorClientCofigurationIdentity><IntegratorClientConfigurations><IntegratorClientConfiguration><ConfigurationIdentity>BasicTrading</ConfigurationIdentity><ClientInterfaceType>Console</ClientInterfaceType><!-- Defaults to MachineName + PID --><ClientFriendlyName>DummyTestingClientA</ClientFriendlyName><!-- NullGUIClient --><ClientTypeName>Kreslik.Integrator.BusinesLayerDriver.DummyIntegratorClient2</ClientTypeName><!-- This is optional. It is ignored if AssemblyFullPath is used, otherwise only the currently executing assembly is probed --><AssemblyNameWithoutExtension>Kreslik.Integrator.BusinesLayerDriver</AssemblyNameWithoutExtension><!-- This is optional, Standard assembly probing will be used if not specified --><!-- <AssemblyFullPath>E:\foo\bar.dll</AssemblyFullPath> --><!-- This is optional, Will be used only if commandline parameter will not be specified --><DefaultConfigurationParameter>FooBar</DefaultConfigurationParameter></IntegratorClientConfiguration><IntegratorClientConfiguration><ConfigurationIdentity>HotspotTrading</ConfigurationIdentity><ClientInterfaceType>Console</ClientInterfaceType><!-- Defaults to MachineName + PID --><ClientFriendlyName>DummyTestingClientA</ClientFriendlyName><!-- NullGUIClient --><ClientTypeName>Kreslik.Integrator.BusinesLayerDriver.HotspotOrdersTest</ClientTypeName><!-- This is optional. It is ignored if AssemblyFullPath is used, otherwise only the currently executing assembly is probed --><AssemblyNameWithoutExtension>Kreslik.Integrator.BusinesLayerDriver</AssemblyNameWithoutExtension><!-- This is optional, Standard assembly probing will be used if not specified --><!-- <AssemblyFullPath>E:\foo\bar.dll</AssemblyFullPath> --><!-- This is optional, Will be used only if commandline parameter will not be specified --><DefaultConfigurationParameter>FooBar</DefaultConfigurationParameter></IntegratorClientConfiguration><IntegratorClientConfiguration><ConfigurationIdentity>CompositeOrders</ConfigurationIdentity><ClientInterfaceType>Console</ClientInterfaceType><!-- Defaults to MachineName + PID --><ClientFriendlyName>DummyTestingClientA</ClientFriendlyName><!-- NullGUIClient --><ClientTypeName>Kreslik.Integrator.BusinesLayerDriver.CompositeOrdersTest</ClientTypeName><!-- This is optional. It is ignored if AssemblyFullPath is used, otherwise only the currently executing assembly is probed --><AssemblyNameWithoutExtension>Kreslik.Integrator.BusinesLayerDriver</AssemblyNameWithoutExtension><!-- This is optional, Standard assembly probing will be used if not specified --><!-- <AssemblyFullPath>E:\foo\bar.dll</AssemblyFullPath> --><!-- This is optional, Will be used only if commandline parameter will not be specified --><DefaultConfigurationParameter>FooBar</DefaultConfigurationParameter></IntegratorClientConfiguration><IntegratorClientConfiguration><ConfigurationIdentity>NullGUI</ConfigurationIdentity><ClientInterfaceType>Console</ClientInterfaceType><!-- Defaults to MachineName + PID --><ClientFriendlyName>DummyTestingClientA</ClientFriendlyName><!-- NullGUIClient --><ClientTypeName>Kreslik.Integrator.BusinesLayerDriver.NullGUIClient</ClientTypeName><!-- This is optional. It is ignored if AssemblyFullPath is used, otherwise only the currently executing assembly is probed --><AssemblyNameWithoutExtension>Kreslik.Integrator.BusinesLayerDriver</AssemblyNameWithoutExtension><!-- This is optional, Standard assembly probing will be used if not specified --><!-- <AssemblyFullPath>E:\foo\bar.dll</AssemblyFullPath> --><!-- This is optional, Will be used only if commandline parameter will not be specified --><DefaultConfigurationParameter>FooBar</DefaultConfigurationParameter></IntegratorClientConfiguration><IntegratorClientConfiguration><ConfigurationIdentity>VenueDeals</ConfigurationIdentity><ClientInterfaceType>Console</ClientInterfaceType><ClientFriendlyName>DummyTestingClientA</ClientFriendlyName><!-- VenueDealsTest --><ClientTypeName>Kreslik.Integrator.BusinesLayerDriver.VenueDealsTest</ClientTypeName><!-- This is optional. It is ignored if AssemblyFullPath is used, otherwise only the currently executing assembly is probed --><AssemblyNameWithoutExtension>Kreslik.Integrator.BusinesLayerDriver</AssemblyNameWithoutExtension></IntegratorClientConfiguration></IntegratorClientConfigurations></IntegratorClientHostSettings>', CAST(0x0000A2AB00DB1934 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (12, N'Kreslik.Integrator.IntegratorWatchDogSvc.exe', NULL, 1, N'<WatchdogSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceDiagnosticsEndpoints><IntegratorServiceDiagnosticsEndpoint><Endpoint>192.168.75.67</Endpoint><Port>9000</Port></IntegratorServiceDiagnosticsEndpoint><IntegratorServiceDiagnosticsEndpoint><Endpoint>192.168.75.68</Endpoint><Port>9000</Port></IntegratorServiceDiagnosticsEndpoint><IntegratorServiceDiagnosticsEndpoint><Endpoint>192.168.75.69</Endpoint><Port>9000</Port></IntegratorServiceDiagnosticsEndpoint></IntegratorServiceDiagnosticsEndpoints><IntegratorNotRunnigSendFirstEmailAfter_Minutes>1</IntegratorNotRunnigSendFirstEmailAfter_Minutes><IntegratorNotRunnigResendEmailAfter_Hours>3</IntegratorNotRunnigResendEmailAfter_Hours><EmailRecipients>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</EmailRecipients></WatchdogSettings>', CAST(0x0000A292011BEB55 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (13, N'Kreslik.Integrator.MessageBus.dll', NULL, 1, N'<MessageBusSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceHost>localhost</IntegratorServiceHost><IntegratorServicePort>9000</IntegratorServicePort><IntegratorServicePipeName>IntegratorService</IntegratorServicePipeName><MaximumAllowedDisconnectedInterval_Seconds>120</MaximumAllowedDisconnectedInterval_Seconds><DisconnectedSessionReconnectInterval_Seconds>5</DisconnectedSessionReconnectInterval_Seconds><SessionHealthCheckInterval_Seconds>10</SessionHealthCheckInterval_Seconds><!--<PriceStreamTransferMode>FastAndUnreliable</PriceStreamTransferMode>--><PriceStreamTransferMode>Reliable</PriceStreamTransferMode><PerSymbolPriceThrottlingInterval_Milliseconds>300</PerSymbolPriceThrottlingInterval_Milliseconds><ForceConnectRemotingClientOnContractVersionMismatch>false</ForceConnectRemotingClientOnContractVersionMismatch></MessageBusSettings>', CAST(0x0000A292011BEB65 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (14, N'Kreslik.Integrator.QuickItchN.dll', NULL, 1, N'<QuickItchNSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><HtaSessionSettings><Host>209.191.250.157</Host><Port>9013</Port><Username>kgt2</Username><Password>hotspot</Password><HeartBeatInterval_Seconds>14</HeartBeatInterval_Seconds></HtaSessionSettings><HtfSessionSettings><Host>209.191.250.157</Host><Port>9013</Port><Username>kgtitch2a</Username><Password>hotspot</Password><HeartBeatInterval_Seconds>14</HeartBeatInterval_Seconds></HtfSessionSettings></QuickItchNSettings>', CAST(0x0000A292011BEB76 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (15, N'Kreslik.Integrator.SessionManagement.dll', N'J_ Sessions on dev01', 1, N'<SessionsSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><FIXChannel_CRS><ClientId>csfx-kgt-user</ClientId><Account>CSFX_KGT</Account></FIXChannel_CRS><FIXChannel_UBS><Username>btobxpte0462</Username><Password>PJJ*OyEN1i66wejYSDQdwNKYay8nd*</Password><PartyId>KGT</PartyId></FIXChannel_UBS><FIXChannel_CTI><Password>citifxuat</Password><Account>35944156</Account></FIXChannel_CTI><FIXChannel_MGS><OnBehalfOfCompId>FIXTEST20</OnBehalfOfCompId><Account>TEST1</Account></FIXChannel_MGS><FIXChannel_RBS><Account>321809</Account></FIXChannel_RBS><FIXChannel_JPM><Password>jpmorgan1</Password><PasswordLength>9</PasswordLength><Account>TestAccount</Account></FIXChannel_JPM><FIXChannel_BNP><Password>kgt123</Password><Account>TESTACC1</Account></FIXChannel_BNP><FIXChannel_CZB><Account>RABOKGTUTR</Account></FIXChannel_CZB><FIXChannel_SOC><OnBehalfOfCompID>KGT</OnBehalfOfCompID></FIXChannel_SOC><FIXChannel_NOM><OnBehalfOfCompID>KGT Investments</OnBehalfOfCompID><SenderSubId>kgttestuser</SenderSubId><Account>KGT INV</Account></FIXChannel_NOM><FIXChannel_HTA><SenderSubId>kgtfix2</SenderSubId><Username>kgtfix2</Username><Password>hotspot</Password></FIXChannel_HTA><FIXChannel_HTF><SenderSubId>kgtfix2a</SenderSubId><Username>kgtfix2a</Username><Password>hotspot</Password></FIXChannel_HTF><UnexpectedMessagesTreating><EnableThreasholdingOfUnexpectedMessages>true</EnableThreasholdingOfUnexpectedMessages><MaximumAllowedInstancesPerTimeInterval>30</MaximumAllowedInstancesPerTimeInterval><TimeIntervalToCheck_Minutes>30</TimeIntervalToCheck_Minutes></UnexpectedMessagesTreating><MarketDataSession><MaxAutoResubscribeCount>4</MaxAutoResubscribeCount><RetryIntervalsSequence_Seconds>1</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>10</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>60</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>1800</RetryIntervalsSequence_Seconds><UnconfirmedSubscriptionTimeout_Seconds>180</UnconfirmedSubscriptionTimeout_Seconds><PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog></MarketDataSession><OrderFlowSession><UnconfirmedOrderTimeout_Seconds>10</UnconfirmedOrderTimeout_Seconds><PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog></OrderFlowSession><!--<SubscriptionSettingsRaw>
    <ResubscribeOnReconnect>true</ResubscribeOnReconnect>
    <GlobalSubscriptionSettings>
      <Symbols>
        <SymbolSetting>
          <ShortName>ALL</ShortName>
          <Size>1000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
        <SymbolSetting>
          <ShortName>EURUSD</ShortName>
          <Size>0</Size>
          <SettingAction>Remove</SettingAction>
        </SymbolSetting>
        <SymbolSetting>
          <ShortName>NOKSEK</ShortName>
          <Size>5000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
      </Symbols>
    </GlobalSubscriptionSettings>
    <CounterpartsSubscriptionSettings>
      <Counterparts>
        <CounterpartSetting>
          <ShortName>ALL</ShortName>
          <SettingAction>Add</SettingAction>
        </CounterpartSetting>
        <CounterpartSetting>
          <ShortName>HSB</ShortName>
          <SettingAction>Add</SettingAction>
          <Symbols>
            <SymbolSetting>
              <ShortName>EURCZK</ShortName>
              <Size>0</Size>
              <SettingAction>Remove</SettingAction>
            </SymbolSetting>
          </Symbols>
          <SenderSubId>KGT3</SenderSubId>
        </CounterpartSetting>
        <CounterpartSetting>
          <ShortName>MGS</ShortName>
          <SettingAction>Remove</SettingAction>
        </CounterpartSetting>
      </Counterparts>
    </CounterpartsSubscriptionSettings>
  </SubscriptionSettingsRaw>--><!--<SubscriptionSettingsRaw>
    <ResubscribeOnReconnect>true</ResubscribeOnReconnect>
    <GlobalSubscriptionSettings>
      <Symbols>
        <SymbolSetting>
          <ShortName>EURUSD</ShortName>
          <Size>1000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
        <SymbolSetting>
          <ShortName>EURCAD</ShortName>
          <Size>1000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
        <SymbolSetting>
          <ShortName>EURJPY</ShortName>
          <Size>1000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
      </Symbols>
    </GlobalSubscriptionSettings>
    <CounterpartsSubscriptionSettings>
      <Counterparts>
        <CounterpartSetting>
          <ShortName>HSB</ShortName>
          <SettingAction>Add</SettingAction>
          <SenderSubId>KGT3</SenderSubId>
        </CounterpartSetting>

        <CounterpartSetting>
          <ShortName>DBK</ShortName>
          <SettingAction>Add</SettingAction>
          <Symbols>
            <SymbolSetting>
              <ShortName>USDCZK</ShortName>
              <Size>1000000</Size>
              <SettingAction>Add</SettingAction>
            </SymbolSetting>
          </Symbols>
        </CounterpartSetting>

        <CounterpartSetting>
          <ShortName>CRS</ShortName>
          <SettingAction>Add</SettingAction>
          <Symbols>
            <SymbolSetting>
              <ShortName>USDCZK</ShortName>
              <Size>1000000</Size>
              <SettingAction>Add</SettingAction>
            </SymbolSetting>
          </Symbols>
        </CounterpartSetting>

        <CounterpartSetting>
          <ShortName>UBS</ShortName>
          <SettingAction>Add</SettingAction>
          <Symbols>
            <SymbolSetting>
              <ShortName>USDCZK</ShortName>
              <Size>1000000</Size>
              <SettingAction>Add</SettingAction>
            </SymbolSetting>
          </Symbols>
        </CounterpartSetting>
        
        <CounterpartSetting>
          <ShortName>MGS</ShortName>
          <SettingAction>Remove</SettingAction>
        </CounterpartSetting>
      </Counterparts>
    </CounterpartsSubscriptionSettings>
  </SubscriptionSettingsRaw>--><!--<SubscriptionSettingsRaw>
    <ResubscribeOnReconnect>true</ResubscribeOnReconnect>
    <GlobalSubscriptionSettings>
      <Symbols>
        <SymbolSetting>
          <ShortName>ALL</ShortName>
          <Size>1000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
        <SymbolSetting>
          <ShortName>NOKSEK</ShortName>
          <Size>5000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
      </Symbols>
    </GlobalSubscriptionSettings>
    <CounterpartsSubscriptionSettings>
      <Counterparts>
        <CounterpartSetting>
          <ShortName>ALL</ShortName>
          <SettingAction>Add</SettingAction>
        </CounterpartSetting>
        <CounterpartSetting>
          <ShortName>HSB</ShortName>
          <SettingAction>Add</SettingAction>
          <SenderSubId>KGT3</SenderSubId>
        </CounterpartSetting>

        <CounterpartSetting>
          <ShortName>CRS</ShortName>
          <SettingAction>Remove</SettingAction>
        </CounterpartSetting>
        <CounterpartSetting>
          <ShortName>HSB</ShortName>
          <SettingAction>Remove</SettingAction>
        </CounterpartSetting>

      </Counterparts>
    </CounterpartsSubscriptionSettings>
  </SubscriptionSettingsRaw>--><SubscriptionSettingsRaw><ResubscribeOnReconnect>true</ResubscribeOnReconnect><GlobalSubscriptionSettings><Symbols><SymbolSetting><ShortName>ALL</ShortName><Size>1000000</Size><SettingAction>Add</SettingAction></SymbolSetting><SymbolSetting><ShortName>NOKSEK</ShortName><Size>5000000</Size><SettingAction>Add</SettingAction></SymbolSetting><SymbolSetting><ShortName>AUDDKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>AUDNOK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>AUDSEK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>CADDKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>CADCHF</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>CADNOK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>CADNZD</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>CADSEK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>DKKJPY</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>DKKNOK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>DKKSEK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>EURCNH</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>EURSKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>GBPDKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>CHFDKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>MXNJPY</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>NOKJPY</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>NZDCAD</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>NZDDKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>NZDCHF</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>NZDNOK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>NZDSEK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>USDCNH</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>USDSKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting></Symbols></GlobalSubscriptionSettings><CounterpartsSubscriptionSettings><Counterparts><CounterpartSetting><ShortName>ALL</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>HSB</ShortName><SettingAction>Add</SettingAction><SenderSubId>KGT3</SenderSubId></CounterpartSetting><CounterpartSetting><ShortName>HTF</ShortName><SettingAction>Remove</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>DBK</ShortName><SettingAction>Remove</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>UBS</ShortName><SettingAction>Remove</SettingAction></CounterpartSetting></Counterparts></CounterpartsSubscriptionSettings></SubscriptionSettingsRaw></SessionsSettings>', CAST(0x0000A29600A8F4C5 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (16, N'Kreslik.Integrator.DAL.dll', N'Local DAL (private IP)', 1, N'<DALSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><!--<ConnectionString>Data Source=192.168.75.66;Initial Catalog=Integrator;User ID=IntegratorKillSwitchUser;Password=aaa333+++</ConnectionString>--><!--<ConnectionString>Data Source=192.168.75.56;Initial Catalog=Integrator_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString>--><ConnectionString>Data Source=192.168.75.66;Initial Catalog=Integrator_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString><KillSwitchPollIntervalSeconds>3</KillSwitchPollIntervalSeconds><KillSwitchWatchDogIntervalSeconds>7</KillSwitchWatchDogIntervalSeconds><IntegratorSwitchName>KGTIntegratorKillSwitch</IntegratorSwitchName><CommandDistributorPollIntervalSeconds>3</CommandDistributorPollIntervalSeconds><DataCollection><DataCollectionBackendConnectionString>Data Source=192.168.75.56;Initial Catalog=DataCollection_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</DataCollectionBackendConnectionString><DataProviderId>48</DataProviderId><SourceFileId>932696</SourceFileId><EnableToBDataCollection>true</EnableToBDataCollection><EnableMarketDataCollection>true</EnableMarketDataCollection><MaxBcpBatchSize>10000</MaxBcpBatchSize><MaxInMemoryColectionSize>1000000</MaxInMemoryColectionSize><MaxIntervalBetweenUploads_Seconds>10</MaxIntervalBetweenUploads_Seconds><MaxUploadTimeAfterWhichSerializationStarts_Seconds>5</MaxUploadTimeAfterWhichSerializationStarts_Seconds><MaxProcessingTimeAfterShutdownSignalled_Seconds>30</MaxProcessingTimeAfterShutdownSignalled_Seconds><WaitIntervalBetweenBcpRetries_Seconds>2</WaitIntervalBetweenBcpRetries_Seconds><WaitIntervalBeforeShutdownStartsSerialization_Seconds>4</WaitIntervalBeforeShutdownStartsSerialization_Seconds></DataCollection></DALSettings>', CAST(0x0000A2930107908B AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (17, N'Kreslik.Integrator.SessionManagement.dll', N'J_ Sessions on dev02', 1, N'<SessionsSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><FIXChannel_CRS><ClientId>csfx-kgt-user</ClientId><Account>CSFX_KGT</Account></FIXChannel_CRS><FIXChannel_UBS><Username>btobxpte0462</Username><Password>PJJ*OyEN1i66wejYSDQdwNKYay8nd*</Password><PartyId>KGT</PartyId></FIXChannel_UBS><FIXChannel_CTI><Password>citifxuat</Password><Account>35944156</Account></FIXChannel_CTI><FIXChannel_MGS><OnBehalfOfCompId>FIXTEST20</OnBehalfOfCompId><Account>TEST1</Account></FIXChannel_MGS><FIXChannel_RBS><Account>321809</Account></FIXChannel_RBS><FIXChannel_JPM><Password>jpmorgan1</Password><PasswordLength>9</PasswordLength><Account>TestAccount</Account></FIXChannel_JPM><FIXChannel_BNP><Password>kgt123</Password><Account>TESTACC1</Account></FIXChannel_BNP><FIXChannel_CZB><Account>RABOKGTUTR</Account></FIXChannel_CZB><FIXChannel_SOC><OnBehalfOfCompID>KGT</OnBehalfOfCompID></FIXChannel_SOC><FIXChannel_NOM><OnBehalfOfCompID>KGT Investments</OnBehalfOfCompID><SenderSubId>kgttestuser</SenderSubId><Account>KGT INV</Account></FIXChannel_NOM><FIXChannel_HTA><SenderSubId>kgtfix2</SenderSubId><Username>kgtfix2</Username><Password>hotspot</Password></FIXChannel_HTA><FIXChannel_HTF><SenderSubId>kgtfix2a</SenderSubId><Username>kgtfix2a</Username><Password>hotspot</Password></FIXChannel_HTF><UnexpectedMessagesTreating><EnableThreasholdingOfUnexpectedMessages>true</EnableThreasholdingOfUnexpectedMessages><MaximumAllowedInstancesPerTimeInterval>30</MaximumAllowedInstancesPerTimeInterval><TimeIntervalToCheck_Minutes>30</TimeIntervalToCheck_Minutes></UnexpectedMessagesTreating><MarketDataSession><MaxAutoResubscribeCount>4</MaxAutoResubscribeCount><RetryIntervalsSequence_Seconds>1</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>10</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>60</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>1800</RetryIntervalsSequence_Seconds><UnconfirmedSubscriptionTimeout_Seconds>180</UnconfirmedSubscriptionTimeout_Seconds><PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog></MarketDataSession><OrderFlowSession><UnconfirmedOrderTimeout_Seconds>10</UnconfirmedOrderTimeout_Seconds><PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog></OrderFlowSession><!--<SubscriptionSettingsRaw>
    <ResubscribeOnReconnect>true</ResubscribeOnReconnect>
    <GlobalSubscriptionSettings>
      <Symbols>
        <SymbolSetting>
          <ShortName>ALL</ShortName>
          <Size>1000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
        <SymbolSetting>
          <ShortName>EURUSD</ShortName>
          <Size>0</Size>
          <SettingAction>Remove</SettingAction>
        </SymbolSetting>
        <SymbolSetting>
          <ShortName>NOKSEK</ShortName>
          <Size>5000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
      </Symbols>
    </GlobalSubscriptionSettings>
    <CounterpartsSubscriptionSettings>
      <Counterparts>
        <CounterpartSetting>
          <ShortName>ALL</ShortName>
          <SettingAction>Add</SettingAction>
        </CounterpartSetting>
        <CounterpartSetting>
          <ShortName>HSB</ShortName>
          <SettingAction>Add</SettingAction>
          <Symbols>
            <SymbolSetting>
              <ShortName>EURCZK</ShortName>
              <Size>0</Size>
              <SettingAction>Remove</SettingAction>
            </SymbolSetting>
          </Symbols>
          <SenderSubId>KGT3</SenderSubId>
        </CounterpartSetting>
        <CounterpartSetting>
          <ShortName>MGS</ShortName>
          <SettingAction>Remove</SettingAction>
        </CounterpartSetting>
      </Counterparts>
    </CounterpartsSubscriptionSettings>
  </SubscriptionSettingsRaw>--><!--<SubscriptionSettingsRaw>
    <ResubscribeOnReconnect>true</ResubscribeOnReconnect>
    <GlobalSubscriptionSettings>
      <Symbols>
        <SymbolSetting>
          <ShortName>EURUSD</ShortName>
          <Size>1000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
        <SymbolSetting>
          <ShortName>EURCAD</ShortName>
          <Size>1000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
        <SymbolSetting>
          <ShortName>EURJPY</ShortName>
          <Size>1000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
      </Symbols>
    </GlobalSubscriptionSettings>
    <CounterpartsSubscriptionSettings>
      <Counterparts>
        <CounterpartSetting>
          <ShortName>HSB</ShortName>
          <SettingAction>Add</SettingAction>
          <SenderSubId>KGT3</SenderSubId>
        </CounterpartSetting>

        <CounterpartSetting>
          <ShortName>DBK</ShortName>
          <SettingAction>Add</SettingAction>
          <Symbols>
            <SymbolSetting>
              <ShortName>USDCZK</ShortName>
              <Size>1000000</Size>
              <SettingAction>Add</SettingAction>
            </SymbolSetting>
          </Symbols>
        </CounterpartSetting>

        <CounterpartSetting>
          <ShortName>CRS</ShortName>
          <SettingAction>Add</SettingAction>
          <Symbols>
            <SymbolSetting>
              <ShortName>USDCZK</ShortName>
              <Size>1000000</Size>
              <SettingAction>Add</SettingAction>
            </SymbolSetting>
          </Symbols>
        </CounterpartSetting>

        <CounterpartSetting>
          <ShortName>UBS</ShortName>
          <SettingAction>Add</SettingAction>
          <Symbols>
            <SymbolSetting>
              <ShortName>USDCZK</ShortName>
              <Size>1000000</Size>
              <SettingAction>Add</SettingAction>
            </SymbolSetting>
          </Symbols>
        </CounterpartSetting>
        
        <CounterpartSetting>
          <ShortName>MGS</ShortName>
          <SettingAction>Remove</SettingAction>
        </CounterpartSetting>
      </Counterparts>
    </CounterpartsSubscriptionSettings>
  </SubscriptionSettingsRaw>--><!--<SubscriptionSettingsRaw>
    <ResubscribeOnReconnect>true</ResubscribeOnReconnect>
    <GlobalSubscriptionSettings>
      <Symbols>
        <SymbolSetting>
          <ShortName>ALL</ShortName>
          <Size>1000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
        <SymbolSetting>
          <ShortName>NOKSEK</ShortName>
          <Size>5000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
      </Symbols>
    </GlobalSubscriptionSettings>
    <CounterpartsSubscriptionSettings>
      <Counterparts>
        <CounterpartSetting>
          <ShortName>ALL</ShortName>
          <SettingAction>Add</SettingAction>
        </CounterpartSetting>
        <CounterpartSetting>
          <ShortName>HSB</ShortName>
          <SettingAction>Add</SettingAction>
          <SenderSubId>KGT3</SenderSubId>
        </CounterpartSetting>

        <CounterpartSetting>
          <ShortName>CRS</ShortName>
          <SettingAction>Remove</SettingAction>
        </CounterpartSetting>
        <CounterpartSetting>
          <ShortName>HSB</ShortName>
          <SettingAction>Remove</SettingAction>
        </CounterpartSetting>

      </Counterparts>
    </CounterpartsSubscriptionSettings>
  </SubscriptionSettingsRaw>--><SubscriptionSettingsRaw><ResubscribeOnReconnect>true</ResubscribeOnReconnect><GlobalSubscriptionSettings><Symbols><SymbolSetting><ShortName>ALL</ShortName><Size>1000000</Size><SettingAction>Add</SettingAction></SymbolSetting><SymbolSetting><ShortName>NOKSEK</ShortName><Size>5000000</Size><SettingAction>Add</SettingAction></SymbolSetting><SymbolSetting><ShortName>AUDDKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>AUDNOK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>AUDSEK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>CADDKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>CADCHF</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>CADNOK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>CADNZD</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>CADSEK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>DKKJPY</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>DKKNOK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>DKKSEK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>EURCNH</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>EURSKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>GBPDKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>CHFDKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>MXNJPY</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>NOKJPY</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>NZDCAD</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>NZDDKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>NZDCHF</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>NZDNOK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>NZDSEK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>USDCNH</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>USDSKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting></Symbols></GlobalSubscriptionSettings><CounterpartsSubscriptionSettings><Counterparts><CounterpartSetting><ShortName>HTF</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>DBK</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>UBS</ShortName><SettingAction>Add</SettingAction></CounterpartSetting></Counterparts></CounterpartsSubscriptionSettings></SubscriptionSettingsRaw></SessionsSettings>', CAST(0x0000A29600A7FFAF AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (18, N'Kreslik.Integrator.DAL.dll', N'Local DAL, no DC', 1, N'<DALSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><!--<ConnectionString>Data Source=192.168.75.66;Initial Catalog=Integrator;User ID=IntegratorKillSwitchUser;Password=aaa333+++</ConnectionString>--><!--<ConnectionString>Data Source=192.168.75.56;Initial Catalog=Integrator_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString>--><ConnectionString>Data Source=192.168.75.66;Initial Catalog=Integrator_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString><KillSwitchPollIntervalSeconds>3</KillSwitchPollIntervalSeconds><KillSwitchWatchDogIntervalSeconds>7</KillSwitchWatchDogIntervalSeconds><IntegratorSwitchName>KGTIntegratorKillSwitch</IntegratorSwitchName><CommandDistributorPollIntervalSeconds>3</CommandDistributorPollIntervalSeconds><DataCollection><DataCollectionBackendConnectionString>Data Source=192.168.75.56;Initial Catalog=DataCollection_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</DataCollectionBackendConnectionString><DataProviderId>48</DataProviderId><SourceFileId>932696</SourceFileId><EnableToBDataCollection>false</EnableToBDataCollection><EnableMarketDataCollection>false</EnableMarketDataCollection><MaxBcpBatchSize>10000</MaxBcpBatchSize><MaxInMemoryColectionSize>1000000</MaxInMemoryColectionSize><MaxIntervalBetweenUploads_Seconds>10</MaxIntervalBetweenUploads_Seconds><MaxUploadTimeAfterWhichSerializationStarts_Seconds>5</MaxUploadTimeAfterWhichSerializationStarts_Seconds><MaxProcessingTimeAfterShutdownSignalled_Seconds>30</MaxProcessingTimeAfterShutdownSignalled_Seconds><WaitIntervalBetweenBcpRetries_Seconds>2</WaitIntervalBetweenBcpRetries_Seconds><WaitIntervalBeforeShutdownStartsSerialization_Seconds>4</WaitIntervalBeforeShutdownStartsSerialization_Seconds></DataCollection></DALSettings>', CAST(0x0000A29700F37B10 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (21, N'Kreslik.Integrator.DAL.dll', N'Integrator DAL', 2, N'<DALSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><KillSwitchPollIntervalSeconds>3</KillSwitchPollIntervalSeconds><KillSwitchWatchDogIntervalSeconds>40</KillSwitchWatchDogIntervalSeconds><IntegratorSwitchName>KGTIntegratorKillSwitch</IntegratorSwitchName><CommandDistributorPollIntervalSeconds>3</CommandDistributorPollIntervalSeconds><DataCollection><DataProviderId>22</DataProviderId><SourceFileId>932696</SourceFileId><MaxBcpBatchSize>10000</MaxBcpBatchSize><MaxInMemoryColectionSize>1000000</MaxInMemoryColectionSize><MaxIntervalBetweenUploads_Seconds>10</MaxIntervalBetweenUploads_Seconds><MaxUploadTimeAfterWhichSerializationStarts_Seconds>5</MaxUploadTimeAfterWhichSerializationStarts_Seconds><MaxProcessingTimeAfterShutdownSignalled_Seconds>30</MaxProcessingTimeAfterShutdownSignalled_Seconds><WaitIntervalBetweenBcpRetries_Seconds>2</WaitIntervalBetweenBcpRetries_Seconds><WaitIntervalBeforeShutdownStartsSerialization_Seconds>4</WaitIntervalBeforeShutdownStartsSerialization_Seconds></DataCollection></DALSettings>', CAST(0x0000A29801286E7C AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (22, N'Kreslik.Integrator.DAL_IntegratorConnection', N'Integrator DB connection (public)', 2, N'<DALSettings_IntegratorConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ConnectionString>Data Source=37.46.6.116;Initial Catalog=Integrator_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString></DALSettings_IntegratorConnection>', CAST(0x0000A298012876AA AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (23, N'Kreslik.Integrator.DAL_IntegratorConnection', N'Integrator DB connection (private)', 2, N'<DALSettings_IntegratorConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ConnectionString>Data Source=192.168.75.66;Initial Catalog=Integrator_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString></DALSettings_IntegratorConnection>', CAST(0x0000A29801287B8B AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (24, N'Kreslik.Integrator.DAL_DataCollectionConnection', N'Integrator DC DB connection (public)', 2, N'<DALSettings_DataCollectionConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ConnectionString>Data Source=37.46.6.106;Initial Catalog=DataCollection_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</ConnectionString></DALSettings_DataCollectionConnection>', CAST(0x0000A298012881D7 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (25, N'Kreslik.Integrator.DAL_DataCollectionConnection', N'Integrator DC DB connection (private)', 2, N'<DALSettings_DataCollectionConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ConnectionString>Data Source=192.168.75.56;Initial Catalog=DataCollection_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</ConnectionString></DALSettings_DataCollectionConnection>', CAST(0x0000A298012881E7 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (26, N'Kreslik.Integrator.DAL_DataCollectionEnabling', N'Integrator DC enabling (enabled)', 2, N'<DALSettings_DataCollectionEnabling xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><EnableToBDataCollection>true</EnableToBDataCollection><EnableMarketDataCollection>true</EnableMarketDataCollection></DALSettings_DataCollectionEnabling>', CAST(0x0000A298012881F7 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (27, N'Kreslik.Integrator.DAL_DataCollectionEnabling', N'Integrator DC enabling (disabled)', 2, N'<DALSettings_DataCollectionEnabling xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><EnableToBDataCollection>false</EnableToBDataCollection><EnableMarketDataCollection>false</EnableMarketDataCollection></DALSettings_DataCollectionEnabling>', CAST(0x0000A29801288206 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (29, N'Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo', N'Message Bus (default pipe)', 2, N'<MessageBusSettings_ClientConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServicePipeName>IntegratorService</IntegratorServicePipeName></MessageBusSettings_ClientConnectionInfo>', CAST(0x0000A29901222229 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (30, N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping', N'Message Bus', 2, N'<MessageBusSettings_ConnectionKeeping xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><MaximumAllowedDisconnectedInterval_Seconds>120</MaximumAllowedDisconnectedInterval_Seconds><DisconnectedSessionReconnectInterval_Seconds>5</DisconnectedSessionReconnectInterval_Seconds><SessionHealthCheckInterval_Seconds>10</SessionHealthCheckInterval_Seconds></MessageBusSettings_ConnectionKeeping>', CAST(0x0000A29901222237 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (31, N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior', N'Message Bus (reliable price transfer)', 2, N'<MessageBusSettings_PriceTransferBehavior xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><PriceStreamTransferMode>Reliable</PriceStreamTransferMode><PerSymbolPriceThrottlingInterval_Milliseconds>300</PerSymbolPriceThrottlingInterval_Milliseconds></MessageBusSettings_PriceTransferBehavior>', CAST(0x0000A29901222246 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (32, N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling', N'Message Bus (don''t force connect)', 2, N'<MessageBusSettings_ContractMismatchHandling xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ForceConnectRemotingClientOnContractVersionMismatch>false</ForceConnectRemotingClientOnContractVersionMismatch></MessageBusSettings_ContractMismatchHandling>', CAST(0x0000A29901222256 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (33, N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo', N'Message Bus (localhost)', 2, N'<MessageBusSettings_IntegratorEndpointConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceHost>localhost</IntegratorServiceHost><IntegratorServicePort>9000</IntegratorServicePort></MessageBusSettings_IntegratorEndpointConnectionInfo>', CAST(0x0000A299012AC082 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (34, N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo', N'Target UAT IP 01 (public)', 2, N'<MessageBusSettings_IntegratorEndpointConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceHost>37.46.6.111</IntegratorServiceHost><IntegratorServicePort>9000</IntegratorServicePort></MessageBusSettings_IntegratorEndpointConnectionInfo>', CAST(0x0000A2A7011CE980 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (36, N'Kreslik.Integrator.IntegratorClientHost.exe', N'MarkupSystemHotspot02 client', 2, N'<IntegratorClientHostSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><DefaultIntegratorClientCofigurationIdentity>MarkupSystemHotspot02</DefaultIntegratorClientCofigurationIdentity><IntegratorClientConfigurations><IntegratorClientConfiguration><ConfigurationIdentity>MarkupSystemHotspot02</ConfigurationIdentity><ClientInterfaceType>Form</ClientInterfaceType><ClientFriendlyName>MarkupSystemHotspot02</ClientFriendlyName><ClientTypeName>Kreslik.MarkupSystemHotspot02.IntegratorHost</ClientTypeName><AssemblyNameWithoutExtension>MarkupSystemHotspot02</AssemblyNameWithoutExtension></IntegratorClientConfiguration></IntegratorClientConfigurations></IntegratorClientHostSettings>', CAST(0x0000A2A7011CE980 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (37, N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo', N'Message Bus (localhost) duplicate', 2, N'<MessageBusSettings_IntegratorEndpointConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceHost>localhost</IntegratorServiceHost><IntegratorServicePort>9000</IntegratorServicePort></MessageBusSettings_IntegratorEndpointConnectionInfo>', CAST(0x0000A2A701321D08 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (38, N'Kreslik.Integrator.Common.dll', N'M_ Clients common (does not send fatal emails)', 1, N'<CommonSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Logging><AvailableDiskSpaceThresholdToStopMDLogging_GB>15</AvailableDiskSpaceThresholdToStopMDLogging_GB></Logging><Automailer><SmtpServer>173.194.70.108</SmtpServer><SmtpPort>587</SmtpPort><BypassCertificationValidation>true</BypassCertificationValidation><SmtpUsername>automailer@kgtinv.com</SmtpUsername><SmtpPassword>Np9G3bBd</SmtpPassword><DirectoryForLocalEmails>LocalEmails</DirectoryForLocalEmails><SendEmailsOnlyToLocalFolder>true</SendEmailsOnlyToLocalFolder></Automailer></CommonSettings>', CAST(0x0000A2AB0133B570 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (39, N'Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo', N'M_ Default pipe name', 2, N'<MessageBusSettings_ClientConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServicePipeName>IntegratorService</IntegratorServicePipeName></MessageBusSettings_ClientConnectionInfo>', CAST(0x0000A2AB0133B570 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (40, N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping', N'M_ Reliable connection', 2, N'<MessageBusSettings_ConnectionKeeping xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><MaximumAllowedDisconnectedInterval_Seconds>120</MaximumAllowedDisconnectedInterval_Seconds><DisconnectedSessionReconnectInterval_Seconds>5</DisconnectedSessionReconnectInterval_Seconds><SessionHealthCheckInterval_Seconds>10</SessionHealthCheckInterval_Seconds></MessageBusSettings_ConnectionKeeping>', CAST(0x0000A2AB0133B570 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (41, N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior', N'M_ Reliable price transfer', 2, N'<MessageBusSettings_PriceTransferBehavior xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><PriceStreamTransferMode>Reliable</PriceStreamTransferMode><PerSymbolPriceThrottlingInterval_Milliseconds>300</PerSymbolPriceThrottlingInterval_Milliseconds></MessageBusSettings_PriceTransferBehavior>', CAST(0x0000A2AB0133B570 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (42, N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling', N'M_ Contract mismatch NOT allowed', 2, N'<MessageBusSettings_ContractMismatchHandling xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ForceConnectRemotingClientOnContractVersionMismatch>false</ForceConnectRemotingClientOnContractVersionMismatch></MessageBusSettings_ContractMismatchHandling>', CAST(0x0000A2AB0133B570 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (43, N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo', N'M_ UAT_01_PublicIP', 2, N'<MessageBusSettings_IntegratorEndpointConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceHost>37.46.6.111</IntegratorServiceHost><IntegratorServicePort>9000</IntegratorServicePort></MessageBusSettings_IntegratorEndpointConnectionInfo>', CAST(0x0000A2AB0133B570 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (44, N'Kreslik.Integrator.IntegratorClientHost.exe', N'M_ Client (trading) - MarkupSystemHotspot02', 2, N'<IntegratorClientHostSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><DefaultIntegratorClientCofigurationIdentity>MarkupSystemHotspot02</DefaultIntegratorClientCofigurationIdentity><IntegratorClientConfigurations><IntegratorClientConfiguration><ConfigurationIdentity>MarkupSystemHotspot02</ConfigurationIdentity><ClientInterfaceType>Form</ClientInterfaceType><ClientFriendlyName>MarkupSystemHotspot02</ClientFriendlyName><ClientTypeName>Kreslik.MarkupSystemHotspot02.IntegratorHost</ClientTypeName><AssemblyNameWithoutExtension>MarkupSystemHotspot02</AssemblyNameWithoutExtension></IntegratorClientConfiguration></IntegratorClientConfigurations></IntegratorClientHostSettings>', CAST(0x0000A2AB0133B570 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (45, N'Kreslik.Integrator.DiagnosticsDashboard.exe', N'J_ Testing settings', 1, N'<DiagnosticsDashboardSettings xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><RefreshTimeout_Miliseconds>500</RefreshTimeout_Miliseconds><FirstSpreadAgeLimit_Seconds>10</FirstSpreadAgeLimit_Seconds><SecondSpreadAgeLimit_Seconds>60</SecondSpreadAgeLimit_Seconds><ThirdSpreadAgeLimit_Seconds>1200</ThirdSpreadAgeLimit_Seconds><CutOffSpreadAgeLimit_Hours>24</CutOffSpreadAgeLimit_Hours><EnabledSymbols><Symbol>AUD/CAD</Symbol><Symbol>AUD/DKK</Symbol><Symbol>AUD/HKD</Symbol><Symbol>AUD/CHF</Symbol><Symbol>CHF/JPY</Symbol><Symbol>CHF/NOK</Symbol><Symbol>CHF/SEK</Symbol><Symbol>MXN/JPY</Symbol><Symbol>NOK/JPY</Symbol><Symbol>NOK/SEK</Symbol><Symbol>NZD/CAD</Symbol><Symbol>NZD/DKK</Symbol><Symbol>NZD/CHF</Symbol><Symbol>NZD/JPY</Symbol><Symbol>USD/SKK</Symbol><Symbol>USD/TRY</Symbol><Symbol>USD/ZAR</Symbol><Symbol>ZAR/JPY</Symbol></EnabledSymbols><EnabledCounterparties><Counterparty>CRS</Counterparty><Counterparty>DBK</Counterparty><Counterparty>HTA</Counterparty><Counterparty>HTF</Counterparty><Counterparty>UBS</Counterparty></EnabledCounterparties></DiagnosticsDashboardSettings>', CAST(0x0000A2AC0111F693 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (47, N'Kreslik.Integrator.DiagnosticsDashboard.exe', N'J_ Testing settings 2', 1, N'<DiagnosticsDashboardSettings xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><RefreshTimeout_Miliseconds>500</RefreshTimeout_Miliseconds><FirstSpreadAgeLimit_Seconds>10</FirstSpreadAgeLimit_Seconds><SecondSpreadAgeLimit_Seconds>60</SecondSpreadAgeLimit_Seconds><ThirdSpreadAgeLimit_Seconds>1200</ThirdSpreadAgeLimit_Seconds><CutOffSpreadAgeLimit_Hours>24</CutOffSpreadAgeLimit_Hours><EnabledSymbols><Symbol>AUD/CAD</Symbol><Symbol>AUD/DKK</Symbol><Symbol>AUD/HKD</Symbol><Symbol>AUD/CHF</Symbol><Symbol>AUD/NOK</Symbol><Symbol>AUD/NZD</Symbol><Symbol>AUD/SEK</Symbol><Symbol>AUD/SGD</Symbol><Symbol>AUD/USD</Symbol><Symbol>CAD/DKK</Symbol><Symbol>CAD/CHF</Symbol><Symbol>CAD/JPY</Symbol><Symbol>CAD/NOK</Symbol><Symbol>CAD/NZD</Symbol><Symbol>CAD/SEK</Symbol><Symbol>DKK/JPY</Symbol><Symbol>MXN/JPY</Symbol><Symbol>NZD/JPY</Symbol><Symbol>USD/SKK</Symbol><Symbol>USD/TRY</Symbol><Symbol>USD/ZAR</Symbol><Symbol>ZAR/JPY</Symbol></EnabledSymbols><EnabledCounterparties><Counterparty>CRS</Counterparty><Counterparty>DBK</Counterparty><Counterparty>HTA</Counterparty><Counterparty>HTF</Counterparty><Counterparty>UBS</Counterparty></EnabledCounterparties></DiagnosticsDashboardSettings>', CAST(0x0000A2B2007C7EA5 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (48, N'Kreslik.Integrator.DiagnosticsDashboard.exe', N'M_ HUHLY_01', 1, N'<DiagnosticsDashboardSettings xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><RefreshTimeout_Miliseconds>500</RefreshTimeout_Miliseconds><FirstSpreadAgeLimit_Seconds>10</FirstSpreadAgeLimit_Seconds><SecondSpreadAgeLimit_Seconds>60</SecondSpreadAgeLimit_Seconds><ThirdSpreadAgeLimit_Seconds>1200</ThirdSpreadAgeLimit_Seconds><CutOffSpreadAgeLimit_Hours>24</CutOffSpreadAgeLimit_Hours><EnabledSymbols><Symbol>AUD/CAD</Symbol><Symbol>AUD/CHF</Symbol><Symbol>EUR/USD</Symbol><Symbol>NOK/SEK</Symbol><Symbol>SGD/JPY</Symbol></EnabledSymbols><EnabledCounterparties><Counterparty>BNP</Counterparty><Counterparty>BOA</Counterparty><Counterparty>BRX</Counterparty><Counterparty>CTI</Counterparty><Counterparty>CZB</Counterparty><Counterparty>HTA</Counterparty><Counterparty>HTF</Counterparty><Counterparty>JPM</Counterparty><Counterparty>MGS</Counterparty><Counterparty>NOM</Counterparty><Counterparty>RBS</Counterparty><Counterparty>SOC</Counterparty></EnabledCounterparties></DiagnosticsDashboardSettings>', CAST(0x0000A2AC012603C6 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (49, N'Kreslik.Integrator.DiagnosticsDashboard.exe', N'M_ HUHLY_02', 1, N'<DiagnosticsDashboardSettings xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><RefreshTimeout_Miliseconds>500</RefreshTimeout_Miliseconds><FirstSpreadAgeLimit_Seconds>10</FirstSpreadAgeLimit_Seconds><SecondSpreadAgeLimit_Seconds>60</SecondSpreadAgeLimit_Seconds><ThirdSpreadAgeLimit_Seconds>1200</ThirdSpreadAgeLimit_Seconds><CutOffSpreadAgeLimit_Hours>24</CutOffSpreadAgeLimit_Hours><EnabledSymbols><Symbol>AUD/CAD</Symbol><Symbol>AUD/CHF</Symbol><Symbol>AUD/SEK</Symbol><Symbol>AUD/SGD</Symbol><Symbol>NOK/SEK</Symbol></EnabledSymbols><EnabledCounterparties><Counterparty>BNP</Counterparty><Counterparty>BOA</Counterparty><Counterparty>BRX</Counterparty><Counterparty>CRS</Counterparty><Counterparty>CTI</Counterparty><Counterparty>CZB</Counterparty><Counterparty>HTA</Counterparty><Counterparty>JPM</Counterparty><Counterparty>MGS</Counterparty><Counterparty>NOM</Counterparty><Counterparty>RBS</Counterparty><Counterparty>SOC</Counterparty></EnabledCounterparties></DiagnosticsDashboardSettings>', CAST(0x0000A2AC01251B52 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (50, N'Kreslik.Integrator.IntegratorClientHost.exe', N'M_ Client (non-trading) - IntegratorOnlineView', 2, N'<IntegratorClientHostSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><DefaultIntegratorClientCofigurationIdentity>IntegratorOnlineView</DefaultIntegratorClientCofigurationIdentity><IntegratorClientConfigurations><IntegratorClientConfiguration><ConfigurationIdentity>IntegratorOnlineView</ConfigurationIdentity><ClientInterfaceType>Form</ClientInterfaceType><ClientFriendlyName>IntegratorOnlineView</ClientFriendlyName><ClientTypeName>Kreslik.IntegratorOnlineView.IntegratorHost</ClientTypeName><AssemblyNameWithoutExtension>IntegratorOnlineView</AssemblyNameWithoutExtension></IntegratorClientConfiguration></IntegratorClientConfigurations></IntegratorClientHostSettings>', CAST(0x0000A2B200FA9470 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (51, N'Kreslik.Integrator.SessionManagement.dll', N'J_ Subscribtion to all banks and Venues', 0, N'<SessionsSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><FIXChannel_CRS><ClientId>csfx-kgt-user</ClientId><Account>CSFX_KGT</Account></FIXChannel_CRS><FIXChannel_UBS><Username>btobxpte0462</Username><Password>PJJ*OyEN1i66wejYSDQdwNKYay8nd*</Password><PartyId>KGT</PartyId></FIXChannel_UBS><FIXChannel_CTI><Password>citifxuat</Password><Account>35944156</Account></FIXChannel_CTI><FIXChannel_MGS><OnBehalfOfCompId>FIXTEST20</OnBehalfOfCompId><Account>TEST1</Account></FIXChannel_MGS><FIXChannel_RBS><Account>321809</Account></FIXChannel_RBS><FIXChannel_JPM><Password>jpmorgan1</Password><PasswordLength>9</PasswordLength><Account>TestAccount</Account></FIXChannel_JPM><FIXChannel_BNP><Password>kgt123</Password><Account>TESTACC1</Account></FIXChannel_BNP><FIXChannel_CZB><Account>RABOKGTUTR</Account></FIXChannel_CZB><FIXChannel_SOC><OnBehalfOfCompID>KGT</OnBehalfOfCompID></FIXChannel_SOC><FIXChannel_NOM><OnBehalfOfCompID>KGT Investments</OnBehalfOfCompID><SenderSubId>kgttestuser</SenderSubId><Account>KGT INV</Account></FIXChannel_NOM><FIXChannel_HTA><SenderSubId>kgtfix2</SenderSubId><Username>kgtfix2</Username><Password>hotspot</Password></FIXChannel_HTA><FIXChannel_HTF><SenderSubId>kgtfix2a</SenderSubId><Username>kgtfix2a</Username><Password>hotspot</Password></FIXChannel_HTF><UnexpectedMessagesTreating><EnableThreasholdingOfUnexpectedMessages>true</EnableThreasholdingOfUnexpectedMessages><MaximumAllowedInstancesPerTimeInterval>30</MaximumAllowedInstancesPerTimeInterval><TimeIntervalToCheck_Minutes>30</TimeIntervalToCheck_Minutes></UnexpectedMessagesTreating><MarketDataSession><MaxAutoResubscribeCount>4</MaxAutoResubscribeCount><RetryIntervalsSequence_Seconds>1</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>10</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>60</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>1800</RetryIntervalsSequence_Seconds><UnconfirmedSubscriptionTimeout_Seconds>180</UnconfirmedSubscriptionTimeout_Seconds><PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog></MarketDataSession><OrderFlowSession><UnconfirmedOrderTimeout_Seconds>10</UnconfirmedOrderTimeout_Seconds><PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog></OrderFlowSession><SubscriptionSettingsRaw><ResubscribeOnReconnect>true</ResubscribeOnReconnect><GlobalSubscriptionSettings><Symbols><SymbolSetting><ShortName>ALL</ShortName><Size>1000000</Size><SettingAction>Add</SettingAction></SymbolSetting><SymbolSetting><ShortName>NOKSEK</ShortName><Size>5000000</Size><SettingAction>Add</SettingAction></SymbolSetting></Symbols></GlobalSubscriptionSettings><CounterpartsSubscriptionSettings><Counterparts><CounterpartSetting><ShortName>ALL</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>HSB</ShortName><SettingAction>Add</SettingAction><SenderSubId>KGT3</SenderSubId></CounterpartSetting></Counterparts></CounterpartsSubscriptionSettings></SubscriptionSettingsRaw></SessionsSettings>', CAST(0x0000A2B2013A5178 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (52, N'Kreslik.Integrator.SessionManagement.dll', N'J_ Subscribtion to all banks and NO Venues', 0, N'<SessionsSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><FIXChannel_CRS><ClientId>csfx-kgt-user</ClientId><Account>CSFX_KGT</Account></FIXChannel_CRS><FIXChannel_UBS><Username>btobxpte0462</Username><Password>PJJ*OyEN1i66wejYSDQdwNKYay8nd*</Password><PartyId>KGT</PartyId></FIXChannel_UBS><FIXChannel_CTI><Password>citifxuat</Password><Account>35944156</Account></FIXChannel_CTI><FIXChannel_MGS><OnBehalfOfCompId>FIXTEST20</OnBehalfOfCompId><Account>TEST1</Account></FIXChannel_MGS><FIXChannel_RBS><Account>321809</Account></FIXChannel_RBS><FIXChannel_JPM><Password>jpmorgan1</Password><PasswordLength>9</PasswordLength><Account>TestAccount</Account></FIXChannel_JPM><FIXChannel_BNP><Password>kgt123</Password><Account>TESTACC1</Account></FIXChannel_BNP><FIXChannel_CZB><Account>RABOKGTUTR</Account></FIXChannel_CZB><FIXChannel_SOC><OnBehalfOfCompID>KGT</OnBehalfOfCompID></FIXChannel_SOC><FIXChannel_NOM><OnBehalfOfCompID>KGT Investments</OnBehalfOfCompID><SenderSubId>kgttestuser</SenderSubId><Account>KGT INV</Account></FIXChannel_NOM><FIXChannel_HTA><SenderSubId>kgtfix2</SenderSubId><Username>kgtfix2</Username><Password>hotspot</Password></FIXChannel_HTA><FIXChannel_HTF><SenderSubId>kgtfix2a</SenderSubId><Username>kgtfix2a</Username><Password>hotspot</Password></FIXChannel_HTF><UnexpectedMessagesTreating><EnableThreasholdingOfUnexpectedMessages>true</EnableThreasholdingOfUnexpectedMessages><MaximumAllowedInstancesPerTimeInterval>30</MaximumAllowedInstancesPerTimeInterval><TimeIntervalToCheck_Minutes>30</TimeIntervalToCheck_Minutes></UnexpectedMessagesTreating><MarketDataSession><MaxAutoResubscribeCount>4</MaxAutoResubscribeCount><RetryIntervalsSequence_Seconds>1</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>10</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>60</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>1800</RetryIntervalsSequence_Seconds><UnconfirmedSubscriptionTimeout_Seconds>180</UnconfirmedSubscriptionTimeout_Seconds><PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog></MarketDataSession><OrderFlowSession><UnconfirmedOrderTimeout_Seconds>10</UnconfirmedOrderTimeout_Seconds><PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog></OrderFlowSession><SubscriptionSettingsRaw><ResubscribeOnReconnect>true</ResubscribeOnReconnect><GlobalSubscriptionSettings><Symbols><SymbolSetting><ShortName>ALL</ShortName><Size>1000000</Size><SettingAction>Add</SettingAction></SymbolSetting><SymbolSetting><ShortName>NOKSEK</ShortName><Size>5000000</Size><SettingAction>Add</SettingAction></SymbolSetting></Symbols></GlobalSubscriptionSettings><CounterpartsSubscriptionSettings><Counterparts><CounterpartSetting><ShortName>ALL</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>HSB</ShortName><SettingAction>Add</SettingAction><SenderSubId>KGT3</SenderSubId></CounterpartSetting><CounterpartSetting><ShortName>HTF</ShortName><SettingAction>Remove</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>HTA</ShortName><SettingAction>Remove</SettingAction></CounterpartSetting></Counterparts></CounterpartsSubscriptionSettings></SubscriptionSettingsRaw></SessionsSettings>', CAST(0x0000A2B2013A8F2D AS DateTime), 0)
GO
SET IDENTITY_INSERT [dbo].[Settings] OFF
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 3)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 10)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 14)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 21)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 22)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 24)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 26)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 30)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 51)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 3)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 10)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 14)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 15)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 21)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 23)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 25)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 26)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 30)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 3)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 10)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 11)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 14)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 17)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 21)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 23)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 25)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 27)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 30)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (3, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (4, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (4, 29)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (4, 30)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (4, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (4, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (4, 36)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (5, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (5, 22)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (5, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (5, 47)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (6, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (6, 21)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (6, 22)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (6, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (7, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (7, 29)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (7, 30)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (7, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (7, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (7, 37)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (8, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (8, 23)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (8, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (8, 48)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (10, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (10, 11)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (10, 29)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (10, 30)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (10, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (10, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (11, 38)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (11, 39)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (11, 40)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (11, 41)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (11, 42)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (11, 43)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 38)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 39)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 40)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 41)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 42)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 43)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 44)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (13, 38)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (13, 39)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (13, 40)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (13, 41)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (13, 42)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (13, 43)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (13, 50)
GO
SET IDENTITY_INSERT [dbo].[Settings_FIX] ON 

GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (1, N'BNP_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


SSLEnable=Y
SSLCertificate=BNPClientCertificate_UAT.pfx
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N

SenderCompID=KGTPrice

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_BNP.xml
BeginString=FIX.4.4
TargetCompID=BNPP
StartTime=00:00:00
EndTime=23:59:59
HeartBtInt=30


# stunnel port was 20004 
SocketConnectPort=26472
SocketConnectHost=155.140.120.200', CAST(0x0000A2930100849C AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (2, N'BNP_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y


SSLEnable=Y
SSLCertificate=BNPClientCertificate_UAT.pfx
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N

SenderCompID=KGTOrder

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_BNP.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=BNPP
StartTime=00:00:00
EndTime=23:59:59
HeartBtInt=30


# stunnel port was 20004 
SocketConnectPort=26473
SocketConnectHost=155.140.120.200', CAST(0x0000A293010084AC AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (3, N'BOA_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT-QTS-TEST


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX43_BOA.xml
BeginString=FIX.4.3
TargetCompID=BAML-FIX-FX-TEST
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=8371
SocketConnectHost=199.43.13.33', CAST(0x0000A293010084BC AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (4, N'BOA_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT-ORD-TEST


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX43_BOA.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.3
TargetCompID=BAML-FIX-FX-TEST
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=8372
SocketConnectHost=199.43.13.33', CAST(0x0000A293010084CB AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (5, N'BRX_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGTINV-PRICES-TEST


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_BRX.xml
BeginString=FIX.4.2
TargetCompID=BARX-PRICES-TEST
#5pm NYK
StartTime=17:15:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=54417
SocketConnectHost=141.228.53.67', CAST(0x0000A293010084DA AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (6, N'BRX_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGTINV-TRADES-TEST


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_BRX.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=BARX-TRADES-TEST
#5pm NYK
StartTime=17:15:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=54418
SocketConnectHost=141.228.53.67', CAST(0x0000A293010084EA AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (7, N'CRS_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=CSFX-KGT-QUO


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_CRS.xml
BeginString=FIX.4.2
TargetCompID=CSTEST
#Resets 06:05 - 06:10 Asian/Singapore time
#StartTime=06:10:10
#EndTime=06:04:50
#TimeZone=Singapore Standard Time
#There was a change to CRS session schedule (20:45UTC)
StartTime=20:47:00
EndTime=20:44:00
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=33039
SocketConnectHost=199.53.21.73', CAST(0x0000A293010084FD AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (8, N'CRS_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=CSFX-KGT-ORD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_CRS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=CSTEST
#Resets 06:05 - 06:10 Asian/Singapore time
#StartTime=06:10:10
#EndTime=06:04:50
#TimeZone=Singapore Standard Time
#There was a change to CRS session schedule (20:45UTC)
StartTime=20:47:00
EndTime=20:44:00

HeartBtInt=30

# no stunnel - dirrect connect:
SocketConnectPort=33039
SocketConnectHost=199.53.21.73
', CAST(0x0000A2930100850D AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (9, N'CTI_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SSLEnable=Y
SSLCertificate=
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N


SenderCompID=KGTINV-QUOTE
TargetSubID=FXSpot

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_CTI.xml
BeginString=FIX.4.4
TargetCompID=CITIFX-UAT
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 20001
SocketConnectPort=32504
SocketConnectHost=46.137.166.14', CAST(0x0000A2930100851D AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (10, N'CTI_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SSLEnable=Y
SSLCertificate=
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N


SenderCompID=KGTINV-TRADE
TargetSubID=FXSpot

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_CTI.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=CITIFX-UAT
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 20001
SocketConnectPort=32504
SocketConnectHost=46.137.166.14', CAST(0x0000A2930100852D AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (11, N'CZB_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


SSLEnable=Y
SSLCertificate=
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N


SenderCompID=KGTMD

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_CZB.xml
BeginString=FIX.4.4
TargetCompID=COBAFXMD
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 20003
SocketConnectPort=443
SocketConnectHost=62.129.126.204', CAST(0x0000A2930100853E AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (12, N'CZB_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y


SSLEnable=Y
SSLCertificate=
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N


SenderCompID=KGTORD

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_CZB.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=COBAFXORD
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 20003
SocketConnectPort=443
SocketConnectHost=62.129.126.205', CAST(0x0000A2930100854D AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (13, N'DBK_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


SSLEnable=Y
SSLCertificate=DBKClientCertificate1_UAT.pfx
SSLServerName=UAT.KGTI.FIX1
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N


SenderCompID=UAT.KGTI.FIX1

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_DBK.xml
BeginString=FIX.4.4
TargetCompID=ABFX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnel port was 20002
SocketConnectPort=443
SocketConnectHost=160.83.33.230', CAST(0x0000A2930100855D AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (14, N'DBK_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y


SSLEnable=Y
SSLCertificate=DBKClientCertificate2_UAT.pfx
SSLServerName=UAT.KGTI.FIX2
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N


SenderCompID=UAT.KGTI.FIX2

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_DBK.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=ABFX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnel port was 20002
SocketConnectPort=443
SocketConnectHost=160.83.33.230', CAST(0x0000A2930100856D AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (15, N'GLS_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGTDEV2


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_GLS.xml
BeginString=FIX.4.4
TargetCompID=GSFXDEVPRICES
#5pm NYK
StartTime=17:05:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=40428
SocketConnectHost=204.4.187.56', CAST(0x0000A2930100857C AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (16, N'GLS_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGTDEV2


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_GLS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=GSFXDEVTRADES
#5pm NYK
StartTime=17:05:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=40428
SocketConnectHost=204.4.187.56', CAST(0x0000A2930100858B AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (17, N'HSB_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SSLEnable=Y
SSLCertificate=HSBClientCertificate_UAT.pfx
SSLServerName=fix.hsbc.com
#SSLProtocols=
SSLCACertificate=HSBCACertificate_UAT.pfx
SSLValidateCertificates=Y
SSLValidateCertificatesOID=N


SenderCompID=KGTQLSSTR2

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_HSB.xml
BeginString=FIX.4.4
TargetCompID=HSBCAPFIX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnel port was 19997
SocketConnectPort=20443
SocketConnectHost=203.112.80.217', CAST(0x0000A293010085A1 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (18, N'HSB_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SSLEnable=Y
SSLCertificate=HSBClientCertificate_UAT.pfx
SSLServerName=fix.hsbc.com
#SSLProtocols=
SSLCACertificate=HSBCACertificate_UAT.pfx
SSLValidateCertificates=Y
SSLValidateCertificatesOID=N

SenderCompID=KGTQLSTRD2

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_HSB.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=HSBCAPFIX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 19997
SocketConnectPort=20443
SocketConnectHost=203.112.80.217

', CAST(0x0000A293010085B1 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (20, N'HTA_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y
#Hotspot does not want reply of any messages - so no point of storing them
PersistMessages=N

SenderCompID=kgt2

SSLEnable=N
#SSLCertificate=UBSClientCertificate_UAT.pfx
#SSLServerName=fxfixb2bpte1.ibb.ubstest.com
#SSLProtocols=
#SSLCACertificate=UBSCACertificate_UAT.pfx
#SSLValidateCertificates=Y
#SSLValidateCertificatesOID=N



[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_HOT.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
SessionQualifier=HTA
TargetCompID=FixServer
#5pm NYK
StartTime=17:02:05
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=8029
SocketConnectHost=209.191.250.157', CAST(0x0000A2930117C979 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (21, N'HTF_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y
#Hotspot does not want reply of any messages - so no point of storing them
PersistMessages=N

SenderCompID=kgt2

SSLEnable=N
#SSLCertificate=UBSClientCertificate_UAT.pfx
#SSLServerName=fxfixb2bpte1.ibb.ubstest.com
#SSLProtocols=
#SSLCACertificate=UBSCACertificate_UAT.pfx
#SSLValidateCertificates=Y
#SSLValidateCertificatesOID=N



[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_HOT.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
SessionQualifier=HTF
TargetCompID=FixServer
#5pm NYK
StartTime=17:02:05
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=8029
SocketConnectHost=209.191.250.157', CAST(0x0000A293010085E5 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (22, N'JPM_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT


SSLEnable=Y
SSLCertificate=JPMClientAndCACertificate_UAT.pfx
SSLServerName=GCCG - Integration
SSLProtocols=Ssl3
SSLCACertificate=JPMClientAndCACertificate_UAT.pfx
SSLValidateCertificates=Y
SSLValidateCertificatesOID=N


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_JPM.xml
BeginString=FIX.4.2
TargetCompID=JPM-UAT-MD
StartTime=00:00:00
EndTime=23:59:59
HeartBtInt=30

# stunnel port was 20006
SocketConnectPort=30014
SocketConnectHost=159.53.38.147', CAST(0x0000A293010085F4 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (23, N'JPM_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT


SSLEnable=Y
SSLCertificate=JPMClientAndCACertificate_UAT.pfx
SSLServerName=GCCG - Integration
SSLProtocols=Ssl3
SSLCACertificate=JPMClientAndCACertificate_UAT.pfx
SSLValidateCertificates=Y
SSLValidateCertificatesOID=N


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_JPM.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=JPM-UAT-OM
StartTime=00:00:00
EndTime=23:59:59
HeartBtInt=30

# stunnel port was 20006
SocketConnectPort=30013
SocketConnectHost=159.53.38.147', CAST(0x0000A29301008603 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (24, N'MGS_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT_QARATE


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_MGS.xml
BeginString=FIX.4.2
TargetCompID=MSFX
OnBehalfOfCompID=FIXTEST1
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=25138
SocketConnectHost=205.228.82.195', CAST(0x0000A29301008613 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (25, N'MGS_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT_QAORD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_MGS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=MSFX
OnBehalfOfCompID=FIXTEST1
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=25138
SocketConnectHost=205.228.82.195', CAST(0x0000A29301008622 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (26, N'NOM_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT_PRICE_TEST


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_NOM.xml
BeginString=FIX.4.4
TargetCompID=NOMURA_TEST
#5pm NYK
StartTime=17:15:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=25511
SocketConnectHost=85.119.26.7

# Fallback connection #1
#SocketConnectPort1=25511
#SocketConnectHost1=85.119.26.8', CAST(0x0000A29301008631 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (27, N'NOM_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT_TRADE_TEST


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_NOM.xml
BeginString=FIX.4.4
TargetCompID=NOMURA_TEST
#5pm NYK
StartTime=17:15:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=25511
SocketConnectHost=85.119.26.7

# Fallback connection #1
#SocketConnectPort1=25511
#SocketConnectHost1=85.119.26.7', CAST(0x0000A29301008640 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (28, N'RBS_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


SSLEnable=Y
SSLCertificate=RBSClientCertificate_UAT.pfx
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N



SenderCompID=kgt.fixtest_MD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_RBS.xml
BeginString=FIX.4.4
TargetCompID=RBS_FX
#5:01pm NYK
StartTime=17:01:59
EndTime=16:59:59
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnel connection port was 20000
SocketConnectPort=4321
SocketConnectHost=uatfixfx.rbsm.com', CAST(0x0000A29301008650 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (29, N'RBS_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y


SSLEnable=Y
SSLCertificate=RBSClientCertificate_UAT.pfx
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N



SenderCompID=kgt.fixtest_ORD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_RBS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=RBS_FX
#5:01pm NYK
StartTime=17:01:59
EndTime=16:59:59
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnel connection port was 20000
SocketConnectPort=4322
SocketConnectHost=uatfixfx.rbsm.com', CAST(0x0000A29301008660 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (30, N'SOC_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT-MKD

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_SOC.xml
BeginString=FIX.4.2
TargetCompID=SOCGEN-MKD
#5pm NYK
StartTime=17:05:10
EndTime=16:59:30
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=20000
SocketConnectHost=fx-streaming-uat.sgcib.com', CAST(0x0000A29301008671 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (31, N'SOC_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_SOC.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=SOCGEN
#5pm NYK
StartTime=17:05:10
EndTime=16:59:30
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=20000
SocketConnectHost=fx-streaming-uat.sgcib.com', CAST(0x0000A29301008680 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (32, N'UBS_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGTITEST-MDS-1

#SSLEnable=Y
#SSLCertificate=UBSClientCertificate_UAT.pfx
#SSLServerName=fxfixb2bpte1.ibb.ubstest.com
#SSLProtocols=
#SSLCACertificate=UBSCACertificate_UAT.pfx
#SSLValidateCertificates=Y
#SSLValidateCertificatesOID=N



[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX43_UBS.xml
BeginString=FIX.4.3
TargetCompID=FXALL
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnelport was 19999
SocketConnectPort=8090
SocketConnectHost=fixtrading.int.accelortrading.com', CAST(0x0000A2930100868F AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (33, N'UBS_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT_ORD

SSLEnable=Y
SSLCertificate=UBSClientCertificate_UAT.pfx
SSLServerName=fxfixb2bpte1.ibb.ubstest.com
#SSLProtocols=
SSLCACertificate=UBSCACertificate_UAT.pfx
SSLValidateCertificates=Y
SSLValidateCertificatesOID=N



[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX43_UBS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.3
TargetCompID=UBSFX2B_PTE
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnelport was 19999
SocketConnectPort=2500
SocketConnectHost=fxfixb2bpte1.ibb.ubstest.com', CAST(0x0000A2930100869F AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (36, N'UBBackup', 0, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT_MKD

SSLEnable=Y
SSLCertificate=UBSClientCertificate_UAT.pfx
SSLServerName=fxfixb2bpte1.ibb.ubstest.com
#SSLProtocols=
SSLCACertificate=UBSCACertificate_UAT.pfx
SSLValidateCertificates=Y
SSLValidateCertificatesOID=N



[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX43_UBS.xml
BeginString=FIX.4.3
TargetCompID=UBSFX2B_PTE
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnelport was 19999
SocketConnectPort=2500
SocketConnectHost=fxfixb2bpte1.ibb.ubstest.com', CAST(0x0000A2B600000000 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Settings_FIX] OFF
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 1)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 2)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 3)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 5)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 6)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 7)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 9)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 10)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 11)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 12)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 13)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 14)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 15)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 16)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 17)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 18)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 20)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 20)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 21)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 22)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 23)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 24)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 25)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 26)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 27)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 28)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 29)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 30)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 1)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 2)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 3)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 5)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 6)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 7)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 9)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 10)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 11)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 12)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 13)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 14)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 15)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 16)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 17)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 18)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 21)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 22)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 23)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 24)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 25)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 26)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 27)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 28)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 29)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 30)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 1)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 2)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 3)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 5)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 6)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 7)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 9)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 10)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 11)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 12)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 13)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 14)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 15)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 16)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 17)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 18)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 20)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 21)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 22)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 23)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 24)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 25)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 26)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 27)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 28)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 29)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 30)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (3, 33)
GO
INSERT [dbo].[CertificateFile] ([FileName], [FileContent]) VALUES (N'BNPClientCertificate_UAT.pfx', 0x308206910201033082065706092A864886F70D010701A082064804820644308206403082033F06092A864886F70D010706A08203303082032C0201003082032506092A864886F70D010701301C060A2A864886F70D010C0106300E04085A600E162302614902020800808202F8E5F874D4EC1AC4AFD40D855E519546D4627967915C455A8DA7CA75951C04AAB4632B43DBDABEC76DB85334798587AF29D535069CBD35EE9D831F38FC399705B827A7BF94B15AB8249E49F91326DF5F5E0AFBB5099DA97B62F0BCFDA72123D31E61995313C79DF90B93C8CA81541AD8B246567A1AE90739B307545765ADAB3B287A224866920113D084B52E9B24426E1AAE6F9F7C72C4B623168B8CDF453EFAAE6422DCC4661998C8B3650331731940B28F94D8687C13AE22E599857B9259E81AB4F1110AEE19006D421B9D1D9A65B14AA675AF2310CA5F3842E3EF444B437224BB50FD3DBE5949E46B65A48189CD0851638E637BA21880F292A4CACF6B9B98679AD8AB04B2431E9CA605A81FA5E45D54A51A9EB4EE17F887334F62AED65889888417106E58B193BC76F086D979125EF3AF13002F6A5BC7C8B0A93CC4EC3957948965F18803EA30C365AD5EB96304D3DA0241884A8B18B1603772FFF91D876F8827A9A73DEFB6AA8F6958546BFBD6008745D5913A0FBDC6DEA9BB41175445AAE3BCADBC76CA711F7079F10E994281764C7DB78374F3FDE081B6CA858285CFBA39708232D5605108A27CE98D688AA759D24CAE9FC70EF862F4B5A2205163674C94C2E094C3F358CBE52C338A7DA4F55FC13CC0B9F0F79CB7C37F781759DF48FFCD448F21E4B678E50DF48584717F9791C18BECD4B6F2316E839C0D7299FFA3B831F380F258BDCD2B83AB2603208CFA8E4D9973732BDA81EAC145BC53D20B12FE19E3764BA725FE2BE72CA2688E7D911F7EE7CDF58C6AB5C52F9010678B23D110590F6BE7678FA99BA84EF9DE7783744056BF5E4EC331E10188B2ABC5B8323EF3794F51E15E1CF047695A7E2871D3EAC46B134A27200D97C3E216C057E9559439DC51542B91A374DD895737B78D4580986780EBDDC2A9AFAFD2544259FCF0BF451C5A9EEA7BDDB9B382BAE81D490AFFBF29BC4E4F8A6F62E542D0558A744D4C5127DF8B45837220B0F42486E1FEAC637C8729C37F271E1B17941E18C013988745B00989AE771B9E3B4C58E5D82339CE21080A014F30B7AF81A7308202F906092A864886F70D010701A08202EA048202E6308202E2308202DE060B2A864886F70D010C0A0102A08202A6308202A2301C060A2A864886F70D010C0103300E0408260ECD89CDBB02CE020208000482028041EB59BC667E51958B3210495E71376CEEEC7F2CB78D8BCA79C054F9EBAAE35457A3FE56344AB1BE6063918C3011DD48530B8DDC79FB1D697EFE1C2F6FF7C32414B1DC61254E5FF4718501AF9D27875A8A7D44D20D54C421221A064A8344A6271BBE5E93B9AAC9897E36B1C0720621A1222E5CAA42196FD955D8C44C735132481B713F90118BD42EF058E5F7ECE2875ED05EC7776CC54AE22AB18439837E204D8F09DB4BC7264667E80B450C6CE8043F0FB2A6EB9E4523433C5C2E7DD872971724FB56A3D1EF09BBBC73324CDFEED33E2AFEEE36596143C0D33D10B774334A2910B4A17EE895E64180829E614257146F2E06C618878C0415C0487843DC33F5ACD0DCDFE425E93C7CABDA0BCB1F7D758B2A9EB6A4333CB156BDA7649D0E90E4F28A7EAD16BF2342BEB2C005C17CE6E077E23992E2215DA5BC2D6DFE7616B62E2C8FBAF7D1AD7E32E98FBE2FED9D29608EAE65BF29DB19528EE17E4883151D676E88F98B3529B8CE20447435BA646FC9B4D4FDAFEBA524DC910B8209081AB575207E701B9FFB797612ABD1F9EF9723BA5B95706C23BADB75DA44931C55D4964CC225991044C15BBD016A743B7113287578AC73B917EB222EB60B7BF17F102ACC7554CE7514D7D28F854C14992CB7BC90C53CBD84529431692ED2FD439754605D6B29F3023D08127E97A9BF1317EEAFCC1995F2CF74C09BF1880C9853E9EE2668574C62B5E9D73A1DAF91C266ED07F0D95280A0312C3DE20FDA24A96D8B0E476AAD4F87E22ACAB41F681F393CC33B5B2A5FEBE9633E1764E8E26FF4962344379BEC83C8CE1282B9BC064D18C75BE665D66F099E93F6118DE36A8A64E7BE5B1607CDCE0341A0A2E506410E61C416704AB21663D6D8F7929001E9BF5B8247790D717D3125302306092A864886F70D010915311604147D07F529479723CE843B13D95B1A7CA390612D3E30313021300906052B0E03021A0500041478C0F487EE4491E7A972DC1DE22E2B5CEE752D9B0408799FAA190502DC3E02020800)
GO
INSERT [dbo].[CertificateFile] ([FileName], [FileContent]) VALUES (N'DBKClientCertificate1_UAT.pfx', 0x308209C90201033082098F06092A864886F70D010701A08209800482097C308209783082067706092A864886F70D010706A0820668308206640201003082065D06092A864886F70D010701301C060A2A864886F70D010C0106300E0408E51596F2216F2AB10202080080820630D99928EE8F430F69A1ED93EF49B2CD01C23F0002F03E73C7A44F3056C753DE2D1E8483D148C86EBC3852F168476AA59532CAC998E43D01B641ED5C7BF8E6852CCA12633CE5E21126EB397D3DB6536400AF1BFC6370DEC69AE7E6EAD362A58324811EDD81DB1E132D6F8A69CE7A2B318DE780846D12F77B959A84E9E9B9D675DD73716C170EC7ECB6CB4C3F05ACF9297EDB93C664E87B5C513CF0DC701A8BA984EFB92E5BBF772F223CFF9777CD2C1C64917A9403C0AC15D08BD504A92386B7B3C56B4EEB013AB83748067DE51DB17491F6906CD2DB1C153273D9512CB9C2CC119F5552AC1AA7F9E0CDE7E285C2F32A19B65AF225D8776F2011E8C22180113B347DA22C0CB8FD60CAAA0FAD5175160D84413BE4DD6619FAED49495D6C82169752105747356348EEDFB0FB34D4F44EEAF1C886DDBBB4790F1441AFBB5AE864016370CA6D22C947CE6C97C1AF30C970FD188B875AA7BF109B136FCC7EFC4B26EE5F1A7DBEF6E99BB79D2DAD3700A399E78208F38CB92A4D84E13F705E5FF2ABDEEADFD46D98E6FD3A9F6837A02AEAB5DDB4DD2CC34D2A963D2A0ADCC5AC343CECCE574A4D2F1723648B93DB59AD240CDC158107C6C6C78B38B07E5832595C0D5AF3681272B0C089F151BC679198BA1D87772206A965A9FC0ED71315D58A12C87C49085BE11DB78523BD9C536B1756C147D164C27E31CA30853224B4E022A440C0430390770EA029038964070D0A43F391AC1589FF953D77B94F46FCF1F5AC2F2D322281ADC932FF4CCBDF9B4A1624AD0A8E2BBAD093D09F50A249867AB21D9455E8B87FF4C0A9B2D8CC31D9574FEDC9DE81A3CC2203A02AAC8BF0A37992A6003847A993D7900E2F20407679180D42502BC6D5E2F3BF0FD8F6C33F4724788D69A397F37500C62C8A902B718B67ACD1BC0F71D78FA6A2354CE52A15008D50673C3B8AC7EE5181977809D725DA9A83B011E980CCEE54E37D74A31E03FDA9BFD4659F3463DE54ACA95B776998E66812BC199D65D4D48A965EC34F4E6FD8512D73020C92E9065DC63127A091FC04D6F8578ABF888E369E49B2922F146189DC42680F555159442A7C1E23D934E6F764EB7EF0346D223CB7029B6C89EF5EA2137CA14934D7647B90F0CE9EA9DD97AE263FBF46A938098EB4811651835C8AF112AF140CF8F25FCE9BD680A851E1C78E8069F306DB1FC0F64C0AD08BE6F0A3074DD7D378C39401AEE646319B137712F3E55B95D9CA8AFEE7AC6F2FB94FFD040FA87FE458835D70FF2EF873988A0093928CDDE89D24A48FC80F7C516FD780060E9C06E6AA063EE0446481FCE57D07B787C3153D8C1DC38C73CEF9CB49C7F847D3850A012CB8807069F68988365031F0BD1548B810024705F83926BA99A6F4E9CDBBFB26590296351B2117DE97E3588A4730BC168158251229A54B6FBD4BB5368B750B9FEE81B50C4353D2F70563DFB03B64CDE5018F6B3EBD915CF571D814B9082A88AC7FA33D1A2AD1BCED9423E9AB468F82F2C802AE3C57DC32CBC1D564AA4B8F223798E5E0D8BA30039558266053D1B086E1300AE699F4DF2B05FAA8F2FC21673B164EA4866D51347EA051E477E478192CBDD64E3F14D57540C6F83EA06CBC4DA1B70F74361CA3AD5F756A510075A80B85E3B34DED1F380D65B15FAD6D6C233CF8606AFC5DFDC9F3223765B691DA2B5F1E13099ABF7C4590406A252CD3EB7DF76CA1A76507511CFDFCEDC7B99D7828C18D3269FB316DA117068C92C7F2C3F6222E9DBB584C180E1C14A223F3D085C33D544DD6208EFCADF3DCB3D9FFECA80C41C90F934403312B328D41C8887ABFE83054E11CD7F6361F5CCF3E635D44D6ACD5D461E982D981B6549DD37FE6218AF4E38D53489F1FB922FC8FF577CBE3FD03E82BC0D599BD8EFE790F99E2946FAC887B826EF2A647A8B0263F85B50589F17A85172722EF7CC1773361D355DC4455CF0CDAB80EE1386F86E593AF51837EBBFDFFD8B9C2262AE96F94C9E26EADE6EBCEE9B57550DBE938EB234D92BEE9F7495FD25E28D21F9395E1392D19164D9A45572C1BC34C2BAFCA5EC6CEFF6966116A59886A1F92795DB9E625553CFB63C75FB37C45A5226DCF858DB114E5CD75F7C0C9DB6D38B0E7F9965E0C97AB500B9CD8A9DA6A4912C2B80DE509EEE54EB58EBD402F2ECCF0BF172635859E4D62EA7AF1CD04EFDD5208E6C952B12A57C82AEFC601688873F301E7F3BF65F9C90B7BD36506F3E6E742F73A308202F906092A864886F70D010701A08202EA048202E6308202E2308202DE060B2A864886F70D010C0A0102A08202A6308202A2301C060A2A864886F70D010C0103300E040805087D34090D049802020800048202806694FB24EC97889601C7D9DFD67FE01DEC36EC6B0545001EC33B965CB776005131A7797B32E71BD513DE71D6E470B7A73432AA9B0792ED75DE66C3B155A04C9BA5EE76C161BDB5A92993A83F7E2A24F719248CB0F3841D06FFA12F1855EADF9A5FC6B798740D08271EA5BC5B7777BCCD2514D9E997894C6322841617111634031B8F932E54AB565E8A725C8A37CBDA15509B9895E889D9BEA5B863DD196F2DD2DC44199C27D1EEDE5EDB27C961B5B49C9A7E4B8A83BF28431F452DA83035223FEA00E9C9B8EDE1490FDAE3A8A170413C825854B58F1FAA828D01E88D7A17C1B1614A7B02B9BA84576263041745A9ABD955ECA846EAF0653992DE1C9F1684FED4F9750B8EFC1EEBE0E4FE160ECBAB426F409DAFD95307502B8A011F699283114091E0DC56514C1BE14E721A729514431CD80F7DDFF200253FC98D2EB4A30FFEFB48670AB7711B0C0BA7F82C6B105D705C0BB4CAB416030D7957C9542645524610DB587393A1C91B8267FE3077D33BCDC7A4BE914291ADC2C128255460A212BBDDFE341F6EB5D82B43D984FFE7A59633B506A2C21E18FE042C70534E8608AB388D742DD7A726D039E799FDD1AF27C7CA2721B9F19776F2F351EC34C4DC63EEF3C879024961E7AB861CB60185F0BDEDEC4F328B4C74EAF1F06F608DB83C4444EB6E23906C54F3CAA17284D7A0E4CC65BD9C2D99DA6AC2883E0ACCBB5B9521B967A0B4E0D5446CDA321AB98FCABC5CFF5C5FD63E39EA8B4695BC4A66020B7A76EFEE07870D76561571D7294CF1DC0A9F2494FBECB25A0EB2F232C167EBA5BF53B3A9DEB80DA932127401AD8414DF034BC5F2775A2215BEFB0D49066C6E7D6C9DFD6F4856E72D2468053694CB2E9C851B4FDFA96476575C0AD0301D7442EA3099A1F73125302306092A864886F70D01091531160414094B37E96E36A892B43929BFCBD991A1197D07C030313021300906052B0E03021A0500041413993F78EE046775BA2F9051C0F3B39BF860E8D704080397586E462A2D1B02020800)
GO
INSERT [dbo].[CertificateFile] ([FileName], [FileContent]) VALUES (N'DBKClientCertificate1_UAT2.pfx', 0x308209C90201033082098F06092A864886F70D010701A08209800482097C308209783082067706092A864886F70D010706A0820668308206640201003082065D06092A864886F70D010701301C060A2A864886F70D010C0106300E0408E51596F2216F2AB10202080080820630D99928EE8F430F69A1ED93EF49B2CD01C23F0002F03E73C7A44F3056C753DE2D1E8483D148C86EBC3852F168476AA59532CAC998E43D01B641ED5C7BF8E6852CCA12633CE5E21126EB397D3DB6536400AF1BFC6370DEC69AE7E6EAD362A58324811EDD81DB1E132D6F8A69CE7A2B318DE780846D12F77B959A84E9E9B9D675DD73716C170EC7ECB6CB4C3F05ACF9297EDB93C664E87B5C513CF0DC701A8BA984EFB92E5BBF772F223CFF9777CD2C1C64917A9403C0AC15D08BD504A92386B7B3C56B4EEB013AB83748067DE51DB17491F6906CD2DB1C153273D9512CB9C2CC119F5552AC1AA7F9E0CDE7E285C2F32A19B65AF225D8776F2011E8C22180113B347DA22C0CB8FD60CAAA0FAD5175160D84413BE4DD6619FAED49495D6C82169752105747356348EEDFB0FB34D4F44EEAF1C886DDBBB4790F1441AFBB5AE864016370CA6D22C947CE6C97C1AF30C970FD188B875AA7BF109B136FCC7EFC4B26EE5F1A7DBEF6E99BB79D2DAD3700A399E78208F38CB92A4D84E13F705E5FF2ABDEEADFD46D98E6FD3A9F6837A02AEAB5DDB4DD2CC34D2A963D2A0ADCC5AC343CECCE574A4D2F1723648B93DB59AD240CDC158107C6C6C78B38B07E5832595C0D5AF3681272B0C089F151BC679198BA1D87772206A965A9FC0ED71315D58A12C87C49085BE11DB78523BD9C536B1756C147D164C27E31CA30853224B4E022A440C0430390770EA029038964070D0A43F391AC1589FF953D77B94F46FCF1F5AC2F2D322281ADC932FF4CCBDF9B4A1624AD0A8E2BBAD093D09F50A249867AB21D9455E8B87FF4C0A9B2D8CC31D9574FEDC9DE81A3CC2203A02AAC8BF0A37992A6003847A993D7900E2F20407679180D42502BC6D5E2F3BF0FD8F6C33F4724788D69A397F37500C62C8A902B718B67ACD1BC0F71D78FA6A2354CE52A15008D50673C3B8AC7EE5181977809D725DA9A83B011E980CCEE54E37D74A31E03FDA9BFD4659F3463DE54ACA95B776998E66812BC199D65D4D48A965EC34F4E6FD8512D73020C92E9065DC63127A091FC04D6F8578ABF888E369E49B2922F146189DC42680F555159442A7C1E23D934E6F764EB7EF0346D223CB7029B6C89EF5EA2137CA14934D7647B90F0CE9EA9DD97AE263FBF46A938098EB4811651835C8AF112AF140CF8F25FCE9BD680A851E1C78E8069F306DB1FC0F64C0AD08BE6F0A3074DD7D378C39401AEE646319B137712F3E55B95D9CA8AFEE7AC6F2FB94FFD040FA87FE458835D70FF2EF873988A0093928CDDE89D24A48FC80F7C516FD780060E9C06E6AA063EE0446481FCE57D07B787C3153D8C1DC38C73CEF9CB49C7F847D3850A012CB8807069F68988365031F0BD1548B810024705F83926BA99A6F4E9CDBBFB26590296351B2117DE97E3588A4730BC168158251229A54B6FBD4BB5368B750B9FEE81B50C4353D2F70563DFB03B64CDE5018F6B3EBD915CF571D814B9082A88AC7FA33D1A2AD1BCED9423E9AB468F82F2C802AE3C57DC32CBC1D564AA4B8F223798E5E0D8BA30039558266053D1B086E1300AE699F4DF2B05FAA8F2FC21673B164EA4866D51347EA051E477E478192CBDD64E3F14D57540C6F83EA06CBC4DA1B70F74361CA3AD5F756A510075A80B85E3B34DED1F380D65B15FAD6D6C233CF8606AFC5DFDC9F3223765B691DA2B5F1E13099ABF7C4590406A252CD3EB7DF76CA1A76507511CFDFCEDC7B99D7828C18D3269FB316DA117068C92C7F2C3F6222E9DBB584C180E1C14A223F3D085C33D544DD6208EFCADF3DCB3D9FFECA80C41C90F934403312B328D41C8887ABFE83054E11CD7F6361F5CCF3E635D44D6ACD5D461E982D981B6549DD37FE6218AF4E38D53489F1FB922FC8FF577CBE3FD03E82BC0D599BD8EFE790F99E2946FAC887B826EF2A647A8B0263F85B50589F17A85172722EF7CC1773361D355DC4455CF0CDAB80EE1386F86E593AF51837EBBFDFFD8B9C2262AE96F94C9E26EADE6EBCEE9B57550DBE938EB234D92BEE9F7495FD25E28D21F9395E1392D19164D9A45572C1BC34C2BAFCA5EC6CEFF6966116A59886A1F92795DB9E625553CFB63C75FB37C45A5226DCF858DB114E5CD75F7C0C9DB6D38B0E7F9965E0C97AB500B9CD8A9DA6A4912C2B80DE509EEE54EB58EBD402F2ECCF0BF172635859E4D62EA7AF1CD04EFDD5208E6C952B12A57C82AEFC601688873F301E7F3BF65F9C90B7BD36506F3E6E742F73A308202F906092A864886F70D010701A08202EA048202E6308202E2308202DE060B2A864886F70D010C0A0102A08202A6308202A2301C060A2A864886F70D010C0103300E040805087D34090D049802020800048202806694FB24EC97889601C7D9DFD67FE01DEC36EC6B0545001EC33B965CB776005131A7797B32E71BD513DE71D6E470B7A73432AA9B0792ED75DE66C3B155A04C9BA5EE76C161BDB5A92993A83F7E2A24F719248CB0F3841D06FFA12F1855EADF9A5FC6B798740D08271EA5BC5B7777BCCD2514D9E997894C6322841617111634031B8F932E54AB565E8A725C8A37CBDA15509B9895E889D9BEA5B863DD196F2DD2DC44199C27D1EEDE5EDB27C961B5B49C9A7E4B8A83BF28431F452DA83035223FEA00E9C9B8EDE1490FDAE3A8A170413C825854B58F1FAA828D01E88D7A17C1B1614A7B02B9BA84576263041745A9ABD955ECA846EAF0653992DE1C9F1684FED4F9750B8EFC1EEBE0E4FE160ECBAB426F409DAFD95307502B8A011F699283114091E0DC56514C1BE14E721A729514431CD80F7DDFF200253FC98D2EB4A30FFEFB48670AB7711B0C0BA7F82C6B105D705C0BB4CAB416030D7957C9542645524610DB587393A1C91B8267FE3077D33BCDC7A4BE914291ADC2C128255460A212BBDDFE341F6EB5D82B43D984FFE7A59633B506A2C21E18FE042C70534E8608AB388D742DD7A726D039E799FDD1AF27C7CA2721B9F19776F2F351EC34C4DC63EEF3C879024961E7AB861CB60185F0BDEDEC4F328B4C74EAF1F06F608DB83C4444EB6E23906C54F3CAA17284D7A0E4CC65BD9C2D99DA6AC2883E0ACCBB5B9521B967A0B4E0D5446CDA321AB98FCABC5CFF5C5FD63E39EA8B4695BC4A66020B7A76EFEE07870D76561571D7294CF1DC0A9F2494FBECB25A0EB2F232C167EBA5BF53B3A9DEB80DA932127401AD8414DF034BC5F2775A2215BEFB0D49066C6E7D6C9DFD6F4856E72D2468053694CB2E9C851B4FDFA96476575C0AD0301D7442EA3099A1F73125302306092A864886F70D01091531160414094B37E96E36A892B43929BFCBD991A1197D07C030313021300906052B0E03021A0500041413993F78EE046775BA2F9051C0F3B39BF860E8D704080397586E462A2D1B02020800)
GO
INSERT [dbo].[CertificateFile] ([FileName], [FileContent]) VALUES (N'DBKClientCertificate2_UAT.pfx', 0x308209C90201033082098F06092A864886F70D010701A08209800482097C308209783082067706092A864886F70D010706A0820668308206640201003082065D06092A864886F70D010701301C060A2A864886F70D010C0106300E04087CCF90086FF3A7090202080080820630C01121E2EF4EEEC4446DDD9BDB9779A1C71D8C0D39A713E3A4289524EF52B54931F54B1B283A9B6002FC34849F4ED445D978F24AA3A153FE17C0A63A8E892095280CF2D7576ADE65D4CD997517ECAAAFE310E89550A4B245C15AD8A718C2A2BAC035C6E7936B1AB97CF8D92F9A2D8F33114229810FF8F9A2F1F2381C8DFE530383493398D50617559DFBAACB84E334AE168B6EFE49F99074997A48D378D65DEDD7C30F90E0C7E9B29334451167E2E638A5A2EA6DC1A884B4D86F5CFD68763B53E0778A077B1FF7D10F1EB71E97A390F40A1397606A69B48948F6190DF14B1D2AE2ACFE63472C56595BD5B0AE3356D738E410B2E00FB8508E5CB2B32CDAF6431F98EC307AE46B425EE961ED9D22ED965E6FFBA93710A8CDBFDA269507A6C4B47A6F8A12DFFE491E5A6472DD5FB9164DFCFE376E6F6E5D14A511A185CA10CD7C7F0F09F2D665DBE326642A22A45ABB272E53B883742A41E114B20F8ED9D761C94A27D3D109D4E4C10F7B63CE57328BE11B4173ADC691EDCF9B821EDC83684B851927B4BD774A77902349600C5927C6EF8DC20C26277EA43AD79677845DF1D0700FA0790240FC414A3C97E77447CAC34B3CF61C05B53005DAB83F570382DAA0B611D851907B95A70D24FE60D64DC2F38AA4EE625C7ED198F931B35A63818C14EE0F7978D6B95E67E9126D1E756BCB34B1F191D247E308E2AF62BA710D2BCAEE6E86625EBB3E56DB2478538C6F7A6BC09570A9594B31E269AB046AF335009E8FBDD11516BDD8FA1F22A485A1F071BBA604A1314F74E5439BA52BF526112679D75D9DD608040355C8ACE9C0ABA0E2971260FCB24B9111621C012AD6154AF27368B8BBC02EB5DBD316C10566AFAC95BBCD356DF331994D964F34FAC361B2E44C023941EBB9EE2D3D99A6B89CB8F929A224913BF341E0BDB77AE021DB08A743E81708765F9F2CE9FB6B5DA09843A83BD0E594161F1D0D769256CEA2DFAC1BFE2CA9F8EE40D53ECC35174642961A34C17DF8178336D0DEC27064223236DEAED6434D3F76859D2084FFAC3058916805AD9B18F6D6802C29CEF652A40E0A6658561D293ACE622C9D74C1B87544CBB91807118F16E3855365A34E7A3B2456BE7B9DCD3D3D5FE001AFE36225365E92B1EA62942FA78671A3748975452D8D04884056A716CDAA3A5A1FD56EB1A179D8DA9C4FBCF35B6B6C8CB2587FD0D2CB86CED45A2549CE5693389AC3D7056C37B0D5C1BB330EC0E4D5F2EDF48AC62D61CE6F67A98B382ABFE45D617580063501EAE963E399A29BD7BE1B5EF0C3E284EB2F40E34730FCA514E1519C5680C1084A5DFAED157FDE58FD575AECDCB36384CD63E946C4144D9A7713221A21A4CC31F3B22B3E5FBD5E398EC205328DDC433F0E06D3D8DEA0B81B8E2D1C15DF7C8FB279CADF6A59EE8EE195F9C91FC1419420D2883012642AA556957545D76A9715409F08CDF2123E804CA4106306C4B3B212EB0473D29DECB20CA5EAA5FC90E0E0907F28205E3BC8FC2535D1A831CA7F7EFE1DC63FE2201D2C7B855C467923B8A8B09BAD989430D836CC8D78A6966790EC0193D9753A77C56EB83A3F9125C270F4C7FC02D2FFE90DA8EB9532DB39CBAF406E99520D4251C9E69F2DA98468DE6BC816F5CEAE911D95B4FE5575423CF92D85B289D86BDBA4FEAD7BEE9B37173E926C8E851AEB7735C14D2FCE8A14128221EB88C50E880AA1872BEFCDE963414CAEB6F31A15331E29C966D55AFA65760FED22947B42DFAB72D8FF0AF74B8A05CB6CE5F9FE43F467CD63328DC870C6AD0CC4FCDC214871BD63DACAA7ADC9D58C96C1F301BF8263834C5A3FC181E7F1927F623201BBDB9FA0516D53514D329F7D6833ABE6A3A78445F63F14753978C53206F41B5096667625DA6FA7A3F118C2AF433BAA1798E22AECFBF708FCE8AE988949287F29A7A80682916C283581B5813F7C9701C7486C4B1B4761DA16CB6414AEA3621457D5263BD3B5AE2167A49ED269B420D247ED1680F86F0E03013E63A44D583B3A3CA50E5A75E2774CE61FA933F1B83C56DA0F7EFC8B80DCFD0AB7633192010A6C3AE413D3E8C2AA61FB5B05CC0330D71C11A2A5F3FA94B824775E585A40CE3C22DD2842635BACA9D253B128266D9A4CE59E971038BC1E9DC40B8EE26E0BAB3589E201C3BF5B993C74D7DC210478AB02BBCEEF9162868E26FD27E049E905D5E9742DA14E69B1089DDB9C671AEB6E9DEE64D1C8A82E4B9F93864201308202F906092A864886F70D010701A08202EA048202E6308202E2308202DE060B2A864886F70D010C0A0102A08202A6308202A2301C060A2A864886F70D010C0103300E04088638367C2D4D32920202080004820280F7B850684C155464C5C8DD017E03E8B4B58C68216AC8290FABAD11E5CAA30C14ED9A44354D95C85AC5E0BF2377328D090B830B57AE7D504BA1903707506F4EE296D1C654CEC7B0BC91DEBD5EC408C7A8DF29839817CDD1A86614E272563B27F630ABC6D6FA636766A52939843461737DF4268424DF096B75558AB921A97CD7673A196CA6EE8E7E162BBEB38A8F6480EF9B39D43D3E2702D8B34DDEC412E9ABE9A3D6132D6E231D897BF2CE3547C6F22F41934302F09CA3FEA6417C106B7D04757F1CFFD2B8260A9E1131222D12620BB1BCDFFDBCC993177C2F840C2AC633E029D554DD7ACA72AA20D5F28AE6BC52E1827FCA0026F9CCB3D25B4232FB17F24E77FE65FFFA9CF50257D6D92DBC106EADD1C369B7B009ECF21F13E5F093D822DE77BC260D3CC2EAD68BE16378FE6EBE1DC070FDDBC45418AFE780978DB5EA44D42289406D7D02D67D7B01B0534A2BF29F63FACACECBDAB2E8AE26EBA40E127CB74111A864AA9F8FB37A4DD600DBCFBEF5B326C2E3B231DEDA5D2BCB8826F1DD1F36DCB764ECC895F762774498D80F654C7C4AA76E33448952A40056FA97D76072C6E9FE7CE187E04B5F3E9F60B1FC72F99C30302FDA96B09CBF4FAA8FE07258A0AC56454D52C477F0F09F87EDEFC26780B536E28CFB11AA9405EAA9B1577631DFFC1436E0A3ECA9060A6A1CE7DE605CEC733522695B6189EFF9CE912DA397BAD923ED424F9F8DBCF8F893911D65A62F085A6CD8403DF26D05FE52C042C6A3ED733388BC1C1BA5B17EB9D2F8C850DFB972DE62BC3552AF2AA9CFB8B1FADB066724EAE68A55A00637456E1766C14E6398AB1AE6D8DB5F8777FFAFEBF29AA762BE7E9A3E3DC02FE3023B479606783234B0132349C9A885D2563C26FC8FDDDF0721E0CE3125302306092A864886F70D0109153116041430C35724063F8C2097ECEF5289B6004013E3982A30313021300906052B0E03021A050004140A9575FD9D843AA9683B04E44ECDD2CA790F6566040858AF81FAC2B1966502020800)
GO
INSERT [dbo].[CertificateFile] ([FileName], [FileContent]) VALUES (N'HSBCACertificate_UAT.pfx', 0x308204840201033082044A06092A864886F70D010701A082043B04820437308204333082042F06092A864886F70D010706A08204203082041C0201003082041506092A864886F70D010701301C060A2A864886F70D010C0106300E040859D54247F16A4E5C02020800808203E83E3E964F2ED33D8D3C7D2066B4CF0699C8D81F94C1A9C3D5CA32304961C5FDAF454ED68A49A00CC20CE91DB30178490EEA5897D3AC0CC24B95B6532B59577419E2530D738E01F8B965D3EF917C3F7458251C9CF2E2C44551890009C8A7CA4C52118A821F04B0CE051A53E8F46C6CA9675F4AAC6F4AE4C92968957CA356A0729CF21A0400CC4115B4EB1C14D564E7821813C05D5AA14164B68BDC405CDA83E5BF5EB7B3942D09587F69C3EDE934F1EE8F2BDB5606EC0D56C41D27B8C04ED2DFDB0134E62DEA044CC379D05A29A585728F705756852CBF72DF0FF7CFD57BAD0A17F139D6FCA92D0233D3FF37768A925643B5A35114719C278A429067C67C622EB4E232B1200D2917A987DC1E615A498EA7B66E48E5580E2B7B322108CDB162A5A3F25D710644B74469DD875C24B01DE9246CBEF6E5CC0931F7887335D2C1E056427A4200441B144A3B3B137EEE0C364647EDEA764214F6FC9704614D72EFE24F07C8E0AEEF3C25E0FC9C8A8B215ABC7A2A52664FB203F601B4B41B5887B9FD08BCFD7390CDC84BCB5E23F8EF3495E3EB365FB5CA3FE915A7B4422622F51799E898A6C622FD4101B962F373701345477FD795291D77C0490B96C6A2182C016AEC713F4CA1798CDA2C926427D3973BF573CA94C6B849A1CADD1A9638829B78D19C50AEACBFB3263F9803EC4D28329BD5F4E674A26BBEA7DB3D705BE2499F96F1457C3ED6A870FE3876BC66DE9A2EABB7195E301C69B8D4BB5AA732ED210182C8E1E83CE67EA2560D48D4E354816B47D2A7460E8744763C23BB71ECE0ABC7BEB8684453D94E2E97C587250B2CDD35775D44B68B34F4DD2E9554A7B1574BB5BC8FC705A28D6BE6E6162D18E8CAAF71E29AB53CB1CB7205D829369511015CCB19E521EB480E69EDC2CC99FE60FA97A5414BCD8CAAC410401C05E20D66423C5127B213132EE2398BC3907E1D353529CBDCC41EE142736D47925C61BDF0BCF8B46ECA9E2E9BD2B3BB06C1002AE52E26C65854578867CC3C417B5E98440872D0D083056AA85661BEF6067943CC2737397E0C489E46D09C9894C28DC9E8C7EB382CF62DAB121716013111E7DCC6950D1040B2CBF3F5F743FF64242C636718EC57A4D8B603FC372C875FFB2207771F9C34F6F490C58F07E8CC68411E83A007D329319E57AD5FEFB886764E19CDD2A7506DE816188A7C06DF3852577165F45AC631B6015672574600A7C20492E893AE38B7FAD8882E59FECA07F2CD38265799B26AF715F2A589DD1983424227C4830B7A130AA38127F4E7E9C2364C16E7EE2A1FAD7337512CC62AFCE7A3206C13E483BC885D30B987ACEE9D178F8E90A3C08C6D700A2ECE5E4F027C88217AE7F6F919AC4DE5F4001D2456F81A4D06528BFB86AC589642AFF0DCB6707617FCF67C1730313021300906052B0E03021A050004146533D324913E84655378115A34A3F66AF9BF36A304083EBD07F33D14C5C002020800)
GO
INSERT [dbo].[CertificateFile] ([FileName], [FileContent]) VALUES (N'HSBClientCertificate_UAT.pfx', 0x308206A10201033082066706092A864886F70D010701A082065804820654308206503082034F06092A864886F70D010706A08203403082033C0201003082033506092A864886F70D010701301C060A2A864886F70D010C0106300E0408F89B540EF9DB93760202080080820308A7D9951ED9C5F3482F6FDD0C435F71D0D29EF5ADC7EDC3395AD993CB67E4F8B84E5F43968560C4C4974C8706490734BC51F6E3F17BDB03ED259175F27FDFB4B3BEA6B8E0579F4B26119765AAB388A9B30AF2253262A2AB0350CEE14FCFC3276BC5C15B7E33A81AB315A86FB3D003BCB416AECA44DD72C337F708C885FEED357F18B788BBD8406ACCDABBB11C1833EC4F57602453CFC0F971EC05145F9A16585400615375DF8AB95ED23DA55729A03A603523D247706524441C8D1DD2275CD0F279C0F2D07A38E4631D48B07E95F8FDF1A60C9F49C2047BA8140568A90AF42839B49309930393E0E4C6AEFDD845B2509546997BC514C6FA1E29D43B41F5F30F735911F89E49B6A7DBEC41878998EEFC0ACCE216DCCFF7BD6BF2AF13AE4318EC50401EFD08EA308B41CC32891A715500AEBD86B28DB5B444BE4C3CA30A4D50E354B0811DF5196AB38DDC457127BCFEFAB63251AFD7A462AEB081F58BB679B5DDD02024DA5F0835D117A8E685FF0035EE2B8E77890DF383F053EA2F57A47A3A5B3471F2114ED3CD28C4D8582990B526C37167C1DE4224CBE43EAB657449FEB7C29DD20F13D6CEB83FA395EE5AC1B5EED2CD4CF527A567A62F5185BB1058A7933263740332DA1AF1720D66E4D06BA74013DA29FA5792CE92E28B507DBC73F601A7574ABA889802D084956A9BB0A87B15734594CBE096F1D343F83E2BF7E8F57C0C6533498EB834DB8B6B7F71EFB9CA1047B078A8167BF867523022FD3AF9988559FEBF3ADB93E5626F44875C747FD19EA2A10E61A08E61CC1CE58A0BBA1651644808CD7F5E8C20AE553468BBEE39DBD1226B6E5113D24975242727CA577C3209897334ABAD311CB2E6F517D55940D3B7918F1A49BD29005351F7BBA6BB63BCA8227C3D54FF334DE623163C7EF812D31AF3F6D583FAF1065B61559B0841E46F5BF809F766412C6639ADDCF0836DD22A689967CA8FACA366F16519D385475BBACB0466F76362571E4876F3210989413C469E499B8FCFB19DDF73C6D1543FB3C54B62FB5740BD1F8751A77FF99D8658CC7188FA434ABC3FF7851B10ADC9DD38A7EDAE2ECC541BF8561DDED7308202F906092A864886F70D010701A08202EA048202E6308202E2308202DE060B2A864886F70D010C0A0102A08202A6308202A2301C060A2A864886F70D010C0103300E0408379637506293029A02020800048202805CFA86FC0AABB11353F6FA2BDBD700F4DEC9DA2F90B1B1527979260034B413B610681289FF1EF403E254EF095A73017E2C5FF0D71C283060D1C3B5F5B52B6BF25818155638CDA5F249CE7BF16051BABB81C28E0707B57DB051431DE3312A626862CDF820718BAE661FB7F0D29FB37430C5D5F67EC726193E7860AF87300420545F1B28582F0B0FDF22B3D54C33DDCA4CEFEDB38085EFFE7E5CA1AE558F6839BB2E91B22F30584589D0AC066CB7A3E30E0D966BB44A0D50D91E44D8C05AAFCCA1A24C1D195211137F861CB774383C12081D52B47880012EB9D237AFACF1E30F1A294FEF149AD4594019F62ACF28AB1B41B86C18BF68ABEAADC91A14266CE1AFC6C9C07C028F6928C3627F41229D2ED998DACBBFA2BFAC7DCF344C350A6DC01473B8CE206156B3A1BB602BB49F315B2B6EE565127250BED1BBABF90C7849865C0FE16451325D337DF83F7C14741B342E056C6B572CA638C0064A2BB1CE4E69CF466AD0E22F09A51D2DFE600D079129CB635E9137AD2177F681792343064392C7B2F554085F4588DEF96B1ADECCE2C27F056D2187A9B2BD8B3CBD9FC04066C2A4A0757AF522481DAD3AA0DD78856766AE19102FBDC638C9F15AC040C2C3568F07F8CD3547B1443F4CE32FC3D7CEC617F822FCF6B508747DD05A3547D1E42966EB3C63E2948C3D64DBEB4038FB116FE675C4EF0DD99F069EA80ACA31C4EA658385FFB851E788DDF82FC2B81C7057D258BE2DA03B5B74DEFA9433EEA5C41B9DC34D0C4967E0E459A190556D8CBF0A0B3CCB0477996C7BFE63CB460DC878EFFE87AA2AE59669C94A50B7A01F6ECE9991D1E34ECBE4E9C7F30E2F93DD9E79C3F8EBC582359EA0246FA33758F90E8B600AF54BF9CA7EAFA621428C218F050EB83A94070F3125302306092A864886F70D01091531160414CE37EB9398F975A9AF27570EAE16CD7246AB0DDA30313021300906052B0E03021A050004146FAED35EF74BF53064558E9C4B2AFA9A8E04F36A0408046ED649F0CC803802020800)
GO
INSERT [dbo].[CertificateFile] ([FileName], [FileContent]) VALUES (N'JPMClientAndCACertificate_UAT.pfx', 0x3082042C020103308203F206092A864886F70D010701A08203E3048203DF308203DB308203D706092A864886F70D010706A08203C8308203C4020100308203BD06092A864886F70D010701301C060A2A864886F70D010C0106300E040843692F2834A262B40202080080820390D2FBA1B5A815C3FB61BD3B00E0073EA0DC90D1C5C3B5111CEE200586395DF38B9923A39F507F76298AD2DD46EB0928F5BF997A910D873AEE65BB8BC9FD5C86D23A5F16AA494399C4F92CF1D2AF3FAD7990EC569E867660B7748B2EE74057221DA494203B90DADE052339764E86F8DBF115902D4E69F2BDCB898512FAE000A171647E9F60FE291938C7ECCB661BF393910A888D50F6E68B48F6B1B97C58F89FA88BB62299DA90BA50E35BE42FE6CED439E60E6F7FA426B3D1116EEBE3C185BB0697871B4D105B3EDFD54FD9EA611C89717C32138333DEA170A96ECDF885BACEC5BCC4058E823E1F354C7701C2B12F173FDCB13BC5AD057BBEB4510FC0BCC0B47C3E01478A6DF378265F0D6960652F4A539B34182566DDBF384B9855138E6A2B58A0A11B0B07CE1B9ADBED1AA7859D8F8106A348DB713B41ED313B79A9A47430C6DFF731A94DC290DEF7E1243116A769C59FE91B5DBD9BE7660DADAF2EBBB9380ECFFB0E4CD25EC30116C0E9DFFCBFDE005EDDF9EA63B67CFE41672F5E0018A7F75CC192E87C67526457EF8458AF5953B5C80805CF9816135B11BA5DE64F94520A6D64D845270697BC81194C8D49E1A50FA796AC2C75AB82272D604CA32CAFDC2997E68080BF8244EB96F8A6CF89FAC63B277C213C4575B012FB90F9A522BC0E2F71CD85296D95892B6E5C7E6F09FDC3DAA8DDBDBFED4A36C87C280D15F18939B5640A9828934A04C5A9B587C9857FF88E877DBBBBAA06D0036ACF4A04A86DADFD69263BE2F3A6582682C2E4DC01AC2E05000E071978921DC7AF420E66A91B0486375BDB52E29AA376CDC88A907AD6952135693CFCCE7B4B1E81C7C7B2F7669F9C73D4DD695A0EBE60EDD370E45CFCE380DBE783B1E07225BA785B1B344FAE08989C17C638FF94C88A147A22453D403F337F5AF78E02AC23FD4866528E283361A447A5AC3E9A01CB8023C14D2F8E5EB792452EB1DE4CE9107A5DA85D61BEAA618DCB3A5EEE034DC8B218FD1632EEFFEC3BCCDE64712C6FB5416AF5CD0B8B36C7AD1BE29E3DC0C38A81457B3AE33D87505AB30FE2FAD4C061AFA687A6ABFB5A4761C0EDAEA2DF85DFC695BE7AAAAC7943B579BEAAEF1D4E479124378B750D22BB62275784DEFAC1353D75A93AC46F864E0BCE306D4ADC2BBBB1054C05E6D872BF46B7CEFF61C8613710CB6A90455860ABF7E27396648AF653E3A95823C7E34A7FC8BE2526B7AC1894E9E0D3138F220D76307D9D3E826C83BF4E3AD4C306520A2B78BAB42C1AAC83D662935B2FFBBA5EB65930313021300906052B0E03021A05000414091220FCA3F249872A26E4ACBF542CC8471B99F0040883739718FB0F336C02020800)
GO
INSERT [dbo].[CertificateFile] ([FileName], [FileContent]) VALUES (N'RBSClientCertificate_UAT.pfx', 0x308208610201033082082706092A864886F70D010701A082081804820814308208103082050F06092A864886F70D010706A0820500308204FC020100308204F506092A864886F70D010701301C060A2A864886F70D010C0106300E0408CDD31B5130F99CD702020800808204C8AA907096BD24E37BBC79EAA15C3BF820D35A00D468DFB87E5809A149CD8897EF9F95C823D213AE1FE209AF8333722A71F63FF1263455659EDC5557AE63B18A83B18B0055C4099C19DE8421E4586EF1D8912D2B526DDCEFD28812BC778A2683D3C37C92CCD1D6E595C5E25D7B8C1EF9B222E99F101DBEADC9F2FF773EAE96029F70201DC65A2D2ED4403EA928FF797419F25661ED9FF81EFE9EB7D3F7C8D0BF5EC89652361E1E0EEDA1C5F56C093862A4261F102228C91E4AE847B1BEE0F409D783FFC466E94DD1242A5B04A5B8B00E8A4D344B87A92F525A890F8D57E49009890E0513EC6ED0B89905036A899F002C8FF21CB98B9BA8DAEBFC4610826F49D3BDFD42883A25E8EC8422551B7A1DAA511D407CD188AD87DD8EB36AA900A1AD4FBD5CD7217E86DB2A9F70232DD8FD0CD67749AFB3A82E9E8C36C2785D1947E338BD0E8C37023CFB6F78A6C396471CD0232F6584BAA38D5DF33B3F3DB582FDE64CDF07AD019EA203CC3E10DA87D2623ACA5A89DDD4FCD7059A03A657091B1B1211F805668C6E18CF8DBED0C20AD9C5BC0CD42E550F214E79118281F11DE737FB97C016D6124479696584E593A2CACEDC1B2C4E60A5886617685B3C4C518A7E53884A8CD3B905E3A459ADDF5F2B61216422FEDC67B9A9DB244A5FE0D18043CF2872FA3340AEF787E4AC3DCA36BA2F69C4E60CF0699EC685016FB6C827BE870C57143D6EA6EA5042C23CC913DC848CC9144E97DE545F05AC075B4D4E99F8DC364F686C730495575CC914B4188974956C7FEB7C98746F1E0CD14A4B82CBF6A1D273109064F5885E732A75060C8340235DF6D6A4347A2B35D0700997033693A6ED96337536F66865150A57604FF3BA5DFBAA8AAF393DCDF433EF5C6153C8EA47DAB2F372A3DCF1C87D749FA9C01785DEBB619DA7021EBBFF3F437E8A4D856991D88A93FC366D3A53DDDD954F9DA2913ED687D847C76F71DF487DFAEB620417272EC0CE4A39B27903EA872D73A91F488BF52DF4B0906A9D6F2839FA5404A56EB8D218A9C301ABF7B5E9FB53793C03DA668E33D79DF0BC7BC95FAD25B95ECFAC3D615B5BA680C4401807CC9E805044F97D5ECB70B3D65300FE1BB6F49775CADEBDDE10D517B1B5BEB2A1719765AE69E7415719352D81D6A7B07167B15D58524B2052E562D3ABAD79440551E5A35D50725D1EF8758E28F990F254FE173EE22F08ED1D245CF08D4F4DC21A2EA60E6BA1EB8183A0931A7C19A78AA5D7B89186ACA95ED9D678A65B0698381A786B9957D26D95E5C0D493226A0E150011C93C512F4F94BC614BC74D19C5115438887DA3335D7F398395F04250B25395DB32EF4A8C7C1328F1022982D1AD4CDBB6975B1F985654D242ADF2C53EAD6E289DF28353970E1D3C3695E681DB9B112F8E4A77F785667DCE053DB74265E260F9F33B5DFF64C18B65D89237596073A04EB9861A6C135533AC0F8E32E4AD3F10D29DB0A43714B82B382B93C4D67C51D249D2A596E8911CBF3903833BA2BA1B0FD1CB381D7C45BB0CD2029674C76518CCDEBA5239D35B9DFD926C78C7177D766B576114E1CD4F8FE502775AED27E652DDB4CFD1E2B2EC0068ED6E9356CE85C44D06690CF1CCA1575B0864C827A15B425C4E88327FBECF618CFE92EC8CF32C5F0C0423386BB79492507E397F3C64B1DB950EF766BFB4A2083871EA096E35F781F2E0409F4D7DFFE061759A273F31BAD251067B2CD6308202F906092A864886F70D010701A08202EA048202E6308202E2308202DE060B2A864886F70D010C0A0102A08202A6308202A2301C060A2A864886F70D010C0103300E04083543BA4675A013F00202080004820280505AE8A94AD322493963685166E00C944DD60E847806F22460BC828CC45C0ECD7BDEAE1F831F5DAC42FF2C37B7402127DC6D4872AA97C45B90C0A494D33A81FB17224FB551DE8DF373B0AA4EDEEB87FE31F7C0AFCFE1C2E4593D6314C906B46799368CA79DABDD395CC612E07B1A8313EF87AD52D89046E3DBA40357638BB7ECC1BE9E90FBB43FCC28BF248B912533220573DAE58B4EC0C58ED9B5E9FDD1AE85BD3FA2A07B00ADDAB5E51EFDEBCE04C1346829607F2A9F22B396A2F1D6B074CC044F974ED3B5A2B9577F78F19BD5307E2E61834F8E35EF7BA735DC8087C1A6F93373BBB08D3348862837BC7D34230A81EDAB69AED61261AEFEFEB1ADCA822B893A5E2CEEE9705D1FECA2BAF390B7871F8FA18CB7A079FFD5DC8054E2315FDD9776CDECEC57A6859899AA8C8907FE10FB99711053291C714187A7594464B5AE5182E0DAB81A2262866634664940AFA47B3A965B51DE158860184BF46A5C63F267FF946703820BD35DD2EC9EC7CF27E814FC3C4644DF55379B6EE8F770F65901ACAD41F950F99AE4A9A3336D2FBEE69C02FEF0D039F49984B2740F9C74033EEEEE0C41A0E7D7F555E9B0E026C5FB35F09988CEF2ACA0D3675604AF29A130968ECFD1AFBBA930F61285F7EDDEA5C309313230A06D1474C1AE4F55E8F85A8867AB938E4571DAFFE986499C00FE2573147A939FDD4DBEA278FFA9D9D37890E54868128A9CD1176C0A1143AF8F377EB59F7852E22FB80786F6EA5E75962805CA7D844D424D49F755AD09A682EABBBC8088AA1D9BD18C8EF5B7711440920AB23265F375635A5D06ECD4AD7EB6F05D978D9A7217B1F58018CFA57F05EA7636D97133D1936441C97B1DDAF5D910C4CAFA606B52500B77E86D3F2788D7B3604B0292395F5C3125302306092A864886F70D010915311604144650F8798718BE7B218944ABA79C89EF53CC52C030313021300906052B0E03021A0500041499946EBDB9DE77F5A74BAC57B0B1B5993C2AE2FB0408805BDDA51F01ECAF02020800)
GO
INSERT [dbo].[CertificateFile] ([FileName], [FileContent]) VALUES (N'UBSCACertificate_UAT.pfx', 0x3082049C0201033082046206092A864886F70D010701A08204530482044F3082044B3082044706092A864886F70D010706A0820438308204340201003082042D06092A864886F70D010701301C060A2A864886F70D010C0106300E040874907F7FD8083E750202080080820400BF3F8D5B95F051E1DE1DA9161D80ACF723BB0B4B6E37C735F94DFA03CD3D5FCF094ED39F3D2439582ADB3DBB32C21007EDE5D771739D1A9E9457E30301A9EC1CF04880817A2922937FDB1F0BE069869C8467533D8B2860047363F15CAA8443418301582D5D44F88D40B20A47DF46E5DB5955C23F7D4B7058081046817A10B6409A7A593FE88893D5186ABB4C2698C94AB07B6A12793136C8C1CD00062D7D12ACFD1CC1D9C0CBAED47744DA301A6145A91AF946864AA747572C2A85FDFD1FC4F95C67E92E4ABE9AA6EE7B09F9240A1A67293BCD5BDDF683231D772EE045F00ADC70FB285D8291380E0C0B70A63A05275618A04527674207F9F825DDC2681A5309E1C292BDCAFD3545ECDEEE8EEEDE56B600389CC74F0F8BB58C34C0F33FA2BDC98DA4F456840BF94B42D2F1181FEF591C5D6367B827F741AF16F9CF59DC490EA9EB321E59E6A7B69EE3667D11B5349B1D011CE7BBB398F941E73358CF8A84331CF92DCE866674AEF79D171454A97B0FE0C6AF9161E43D953C92104BE75E18771AC2FC858F8E8724BFC71AB97C4C25B57D9AAEEE97F4AB31BFA8345482F0CBE07CC921AF2EB69B7E8CFEFEBF789E696A775546411B6C24B0E3FEAC97EB61E3BD00BF9A2C3D46B91DB31865695768B34755E509D31419C3237C02705B3C2604608136632623440D8719CB9B0D39F39C6CCE4D9CDD6545805018079DA7B4B0D9A2E85DB1ECEA6AFB82827C0AE94F30E10EFC75A072443E747E1A2C87ABD9A2AA608BCE9848DB57FD8058EB2FEFEB6FA170DF65F7492D02CBD8F25EFEFE88BE236D41B182447FEDF762B5244AFC3F8B97105D819EA822C5986ED711AEAB5F5BD53EE7B88D229616285A7DF874FDFE58119E54BB615401DE9D6D8CB5AE38DB4392BC57A7E8C9582A6F9D0F3312F383E006D3506A1C691D372FBE2C9AE73040F0007F3B389A08857191E62A4945C994075C995BB3A921141EC8F8D7D3ED25AEEE6E6544FC7D0EC4897B4D4FC67A56D1C41B30798DF86E3B91BC636AEAD6103EFE586D6550C95B0DE3A5B7D8D111A2DA5D065D3352C80EA4AA497F0BFB77590F1ED96464F96B553059A833433824B8E01889F54CEE2CE1F38C261E6D0546D27D6428E510224243845EFEE699CB3C50CF8A4F65C94E366E62B33A56A848624DEFDBB1A0384FA962D4D7E765F2FD8026B809573C1BB378449FFF7FCDA8292DB78E9B045F05069ED032E3668A05035EE98819BB5E26381FC76C12D8C6D4D60512E147651064BF4F5A9BF68F4C7B95643A9FE40868D0C6414674A589AF4D0D3F3158FE8436B988B2EC633AC0B73036DD12D41258D56DB486C2ECD9EB14CDE3037D2819C27913EF86F6D0D5EEF7B8EE84CC13454A2F4ECE61758E184D03718772A6B4E2F764C1B779263CEB301EDC22AC1B9D60CA42ED69F966C16509502E79E4B8B9C31318A930313021300906052B0E03021A05000414329BA99A4C5F5E0ED834344DB01F2C43F67489D50408F46EE4C0F313887002020800)
GO
INSERT [dbo].[CertificateFile] ([FileName], [FileContent]) VALUES (N'UBSClientCertificate_UAT.pfx', 0x308206910201033082065706092A864886F70D010701A082064804820644308206403082033F06092A864886F70D010706A08203303082032C0201003082032506092A864886F70D010701301C060A2A864886F70D010C0106300E04085A600E162302614902020800808202F8E5F874D4EC1AC4AFD40D855E519546D4627967915C455A8DA7CA75951C04AAB4632B43DBDABEC76DB85334798587AF29D535069CBD35EE9D831F38FC399705B827A7BF94B15AB8249E49F91326DF5F5E0AFBB5099DA97B62F0BCFDA72123D31E61995313C79DF90B93C8CA81541AD8B246567A1AE90739B307545765ADAB3B287A224866920113D084B52E9B24426E1AAE6F9F7C72C4B623168B8CDF453EFAAE6422DCC4661998C8B3650331731940B28F94D8687C13AE22E599857B9259E81AB4F1110AEE19006D421B9D1D9A65B14AA675AF2310CA5F3842E3EF444B437224BB50FD3DBE5949E46B65A48189CD0851638E637BA21880F292A4CACF6B9B98679AD8AB04B2431E9CA605A81FA5E45D54A51A9EB4EE17F887334F62AED65889888417106E58B193BC76F086D979125EF3AF13002F6A5BC7C8B0A93CC4EC3957948965F18803EA30C365AD5EB96304D3DA0241884A8B18B1603772FFF91D876F8827A9A73DEFB6AA8F6958546BFBD6008745D5913A0FBDC6DEA9BB41175445AAE3BCADBC76CA711F7079F10E994281764C7DB78374F3FDE081B6CA858285CFBA39708232D5605108A27CE98D688AA759D24CAE9FC70EF862F4B5A2205163674C94C2E094C3F358CBE52C338A7DA4F55FC13CC0B9F0F79CB7C37F781759DF48FFCD448F21E4B678E50DF48584717F9791C18BECD4B6F2316E839C0D7299FFA3B831F380F258BDCD2B83AB2603208CFA8E4D9973732BDA81EAC145BC53D20B12FE19E3764BA725FE2BE72CA2688E7D911F7EE7CDF58C6AB5C52F9010678B23D110590F6BE7678FA99BA84EF9DE7783744056BF5E4EC331E10188B2ABC5B8323EF3794F51E15E1CF047695A7E2871D3EAC46B134A27200D97C3E216C057E9559439DC51542B91A374DD895737B78D4580986780EBDDC2A9AFAFD2544259FCF0BF451C5A9EEA7BDDB9B382BAE81D490AFFBF29BC4E4F8A6F62E542D0558A744D4C5127DF8B45837220B0F42486E1FEAC637C8729C37F271E1B17941E18C013988745B00989AE771B9E3B4C58E5D82339CE21080A014F30B7AF81A7308202F906092A864886F70D010701A08202EA048202E6308202E2308202DE060B2A864886F70D010C0A0102A08202A6308202A2301C060A2A864886F70D010C0103300E0408260ECD89CDBB02CE020208000482028041EB59BC667E51958B3210495E71376CEEEC7F2CB78D8BCA79C054F9EBAAE35457A3FE56344AB1BE6063918C3011DD48530B8DDC79FB1D697EFE1C2F6FF7C32414B1DC61254E5FF4718501AF9D27875A8A7D44D20D54C421221A064A8344A6271BBE5E93B9AAC9897E36B1C0720621A1222E5CAA42196FD955D8C44C735132481B713F90118BD42EF058E5F7ECE2875ED05EC7776CC54AE22AB18439837E204D8F09DB4BC7264667E80B450C6CE8043F0FB2A6EB9E4523433C5C2E7DD872971724FB56A3D1EF09BBBC73324CDFEED33E2AFEEE36596143C0D33D10B774334A2910B4A17EE895E64180829E614257146F2E06C618878C0415C0487843DC33F5ACD0DCDFE425E93C7CABDA0BCB1F7D758B2A9EB6A4333CB156BDA7649D0E90E4F28A7EAD16BF2342BEB2C005C17CE6E077E23992E2215DA5BC2D6DFE7616B62E2C8FBAF7D1AD7E32E98FBE2FED9D29608EAE65BF29DB19528EE17E4883151D676E88F98B3529B8CE20447435BA646FC9B4D4FDAFEBA524DC910B8209081AB575207E701B9FFB797612ABD1F9EF9723BA5B95706C23BADB75DA44931C55D4964CC225991044C15BBD016A743B7113287578AC73B917EB222EB60B7BF17F102ACC7554CE7514D7D28F854C14992CB7BC90C53CBD84529431692ED2FD439754605D6B29F3023D08127E97A9BF1317EEAFCC1995F2CF74C09BF1880C9853E9EE2668574C62B5E9D73A1DAF91C266ED07F0D95280A0312C3DE20FDA24A96D8B0E476AAD4F87E22ACAB41F681F393CC33B5B2A5FEBE9633E1764E8E26FF4962344379BEC83C8CE1282B9BC064D18C75BE665D66F099E93F6118DE36A8A64E7BE5B1607CDCE0341A0A2E506410E61C416704AB21663D6D8F7929001E9BF5B8247790D717D3125302306092A864886F70D010915311604147D07F529479723CE843B13D95B1A7CA390612D3E30313021300906052B0E03021A0500041478C0F487EE4491E7A972DC1DE22E2B5CEE752D9B0408799FAA190502DC3E02020800)
GO
