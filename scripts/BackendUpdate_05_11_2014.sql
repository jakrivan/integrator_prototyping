BEGIN TRANSACTION Install_05_11_001;
GO

UPDATE
	[dbo].[Counterparty]
SET
	[CounterpartyCode] = 'L01'
WHERE
	[CounterpartyCode] = 'LM1'
	
--
-- fix settings
--   Probably NO - as we want session layer to know about discrepancy between layers and sessions
UPDATE
	[dbo].[Settings_FIX]
SET
	[Name] = 'L01_QUO'
WHERE
	[Name] = 'LM1_QUO'
	
UPDATE
	[dbo].[Settings_FIX]
SET
	[Name] = 'L01_ORD'
WHERE
	[Name] = 'LM1_ORD'
	

-- MANUALY!
-- in session settings rename LM1 -> L01 in Traiana settings <Counterparty>LM1</Counterparty>

-- AND all settings returned by:

SELECT [Id]
      ,[Name]
      ,[FriendlyName]
      ,[SettingsVersion]
      ,[SettingsXml]
      ,[LastUpdated]
      ,[IsOnlineMonitored]
  FROM [dbo].[Settings]
  where CAST([SettingsXml] AS nvarchar(max)) like '%LM1%'


INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (29, N'LMAX', N'L02', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (30, N'LMAX', N'L03', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (31, N'LMAX', N'L04', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (32, N'LMAX', N'L05', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (33, N'LMAX', N'L06', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (34, N'LMAX', N'L07', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (35, N'LMAX', N'L08', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (36, N'LMAX', N'L09', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (37, N'LMAX', N'L10', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (38, N'LMAX', N'L11', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (39, N'LMAX', N'L12', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (40, N'LMAX', N'L13', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (41, N'LMAX', N'L14', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (42, N'LMAX', N'L15', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (43, N'LMAX', N'L16', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (44, N'LMAX', N'L17', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (45, N'LMAX', N'L18', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (46, N'LMAX', N'L19', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (47, N'LMAX', N'L20', 1)
GO
	
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L02'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L03'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L04'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L05'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L06'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L07'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L08'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L09'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L10'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L11'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L12'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L13'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L14'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L15'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L16'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L17'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L18'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L19'), 8.25)
INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L20'), 8.25)
GO
	
	
INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LM2'), 'MM')	
	
--ROLLBACK TRANSACTION Install_05_11_001;
--GO
--COMMIT TRANSACTION Install_05_11_001;
--GO