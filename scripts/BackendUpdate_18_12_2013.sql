ALTER PROCEDURE [dbo].[GetStatisticsPerPair_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
		COUNT(deal.SymbolId) AS DealsNum,
		--following two make more sense after extracting multipliers behind brackets
		SUM(deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) AS PnlTermPol,
		SUM((deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) * termCurrency.LastKnownUsdConversionMultiplier) AS PnlTermPolInUsd,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1,
		SUM(ABS(deal.[AmountBasePolExecuted])* baseCurrency.LastKnownUsdConversionMultiplier) AS VolumeInUsd
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]
END
GO


ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsMap] 
ADD CONSTRAINT MappingIsUnique UNIQUE (IntegratorEnvironmentId, SettingsId); 
GO

ALTER TABLE [dbo].[IntegratorEnvironment] 
ADD Description NVARCHAR(200) NULL
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntegratorEnvironmentType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](200) NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IntegratorEnvironmentType_Id] ON [dbo].[IntegratorEnvironmentType]
(
	[Id] ASC
) ON [PRIMARY]
GO

INSERT INTO [dbo].[IntegratorEnvironmentType]
           ([Name]
		   ,[Description])
     VALUES(N'IntegratorService', N'Environment for IntegratorService')
GO

INSERT INTO [dbo].[IntegratorEnvironmentType]
           ([Name]
		   ,[Description])
     VALUES(N'IntegratorService_old', N'Environment for IntegratorService (previous version)')
GO

INSERT INTO [dbo].[IntegratorEnvironmentType]
           ([Name]
		   ,[Description])
     VALUES(N'Client', N'Environment for Integrator Client started via IntegratorServiceHost')
GO

INSERT INTO [dbo].[IntegratorEnvironmentType]
           ([Name]
		   ,[Description])
     VALUES(N'Dashboard', N'Environment for DiagnosticsDashboard (AKA Counterparty Monitor)')
GO

INSERT INTO [dbo].[IntegratorEnvironmentType]
           ([Name]
		   ,[Description])
     VALUES(N'CommandsConsole', N'Environment for ConsoleRunner started solely for purposes of running commands')
GO

INSERT INTO [dbo].[IntegratorEnvironmentType]
           ([Name]
		   ,[Description])
     VALUES(N'IntegratorWatchdog', N'Environment for IntegratorWatchdog windows service')
GO

INSERT INTO [dbo].[IntegratorEnvironmentType]
           ([Name]
		   ,[Description])
     VALUES(N'Splitter', N'Environment for Splitter')
GO

CREATE TABLE [dbo].[SettingsMandatoryInEnvironemntType](
	[IntegratorEnvironmentTypeId] [int] NOT NULL,
	[SettingsName] [nvarchar](100) NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SettingsMandatoryInEnvironemntType]  WITH CHECK ADD  CONSTRAINT [FK_SettingsMandatoryInEnvironemntType_IntegratorEnvironmentType] FOREIGN KEY([IntegratorEnvironmentTypeId])
REFERENCES [dbo].[IntegratorEnvironmentType] ([Id])
GO
ALTER TABLE [dbo].[SettingsMandatoryInEnvironemntType] CHECK CONSTRAINT [FK_SettingsMandatoryInEnvironemntType_IntegratorEnvironmentType]
GO

ALTER TABLE [dbo].[SettingsMandatoryInEnvironemntType] 
ADD CONSTRAINT MandatorySettingIsUnique UNIQUE (IntegratorEnvironmentTypeId, SettingsName); 
GO

--script for inserting mandatory mappings
INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	  SELECT [Id], N'Kreslik.Integrator.RiskManagement.dll' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'IntegratorService'	   
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
    SELECT [Id], N'Kreslik.Integrator.BusinessLayer.dll' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'IntegratorService'	
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.Common.dll' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'IntegratorService'		   
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.DAL.dll' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'IntegratorService'		   
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'IntegratorService'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'IntegratorService'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'IntegratorService'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'IntegratorService'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.QuickItchN.dll' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'IntegratorService'	
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.SessionManagement.dll' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'IntegratorService'	
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.DAL_IntegratorConnection' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'IntegratorService'	
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.DAL_DataCollectionConnection' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'IntegratorService'	
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.DAL_DataCollectionEnabling' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'IntegratorService'	
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.Common.dll' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Client'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.IntegratorClientHost.exe' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Client'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Client'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Client'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Client'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Client'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.Common.dll' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Dashboard'	   
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.DAL_IntegratorConnection' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Dashboard'	   
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Dashboard'	   
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.DiagnosticsDashboard.exe' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Dashboard'	   
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.Common.dll' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'CommandsConsole'	   
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.DAL_IntegratorConnection' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'CommandsConsole'	   
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.DAL.dll' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'CommandsConsole'	   
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'CommandsConsole'	   
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.Common.dll' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Splitter'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Splitter'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Splitter'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Splitter'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Splitter'
GO

INSERT INTO [dbo].[SettingsMandatoryInEnvironemntType]
           ([IntegratorEnvironmentTypeId]
           ,[SettingsName])
	SELECT [Id], N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling' FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = N'Splitter'
GO


ALTER TABLE [dbo].[IntegratorEnvironment] 
ADD IntegratorEnvironmentTypeId [int] NOT NULL DEFAULT(1)

ALTER TABLE [dbo].[IntegratorEnvironment]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironment_IntegratorEnvironmentType] FOREIGN KEY([IntegratorEnvironmentTypeId])
REFERENCES [dbo].[IntegratorEnvironmentType] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironment] CHECK CONSTRAINT [FK_IntegratorEnvironment_IntegratorEnvironmentType]
GO

CREATE PROCEDURE [dbo].[GetEnvironmentSettingsSet_SP] 
	@EnvironmentName NVARCHAR(100)
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		COALESCE(mandatorySettings.SettingsName, configuredSettings.SettingsName) AS SettingsName
		,CASE WHEN configuredSettings.SettingsName IS NULL THEN 0 ELSE 1 END AS IsConfigured
		,COUNT(configuredSettings.SettingsName) AS DistinctValuesCount
	FROM
		(
		SELECT
			mandSett.SettingsName AS SettingsName
		FROM
			[dbo].[IntegratorEnvironment] env 
			INNER JOIN [dbo].[IntegratorEnvironmentType] envtype ON env.IntegratorEnvironmentTypeId = envtype.Id
			INNER JOIN [dbo].[SettingsMandatoryInEnvironemntType] mandSett ON mandSett.IntegratorEnvironmentTypeId = envtype.Id
		WHERE
			env.Name = @EnvironmentName
		) mandatorySettings

		FULL OUTER JOIN

		(
		SELECT
			sett.Name AS SettingsName
		FROM
			[dbo].[IntegratorEnvironment] envint 
			INNER JOIN [dbo].[IntegratorEnvironmentToSettingsMap] map ON envint.Id = map.IntegratorEnvironmentId
			INNER JOIN [dbo].[Settings] sett ON sett.id = map.SettingsId
		WHERE
			envint.Name = @EnvironmentName
		) configuredSettings 
		
		ON mandatorySettings.SettingsName = configuredSettings.SettingsName

	GROUP BY
		mandatorySettings.SettingsName, configuredSettings.SettingsName
END
GO

GRANT EXECUTE ON [dbo].[GetEnvironmentSettingsSet_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


--new schemas

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DALv2.dll.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="DALSettings" nillable="true" type="DALSettings" />
  <xs:complexType name="DALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="KillSwitchPollIntervalSeconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="KillSwitchWatchDogIntervalSeconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="IntegratorSwitchName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="CommandDistributorPollIntervalSeconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="DataCollection" type="DataCollectionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="DataCollectionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="DataProviderId" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="SourceFileId" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxBcpBatchSize" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxInMemoryColectionSize" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxIntervalBetweenUploads_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxUploadTimeAfterWhichSerializationStarts_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxProcessingTimeAfterShutdownSignalled_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="WaitIntervalBetweenBcpRetries_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="WaitIntervalBeforeShutdownStartsSerialization_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DALv2_IntegratorConnection.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="DALSettings_IntegratorConnection" nillable="true" type="DALSettings_IntegratorConnection" />
  <xs:complexType name="DALSettings_IntegratorConnection">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ConnectionString" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DALv2_DataCollectionConnection.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="DALSettings_DataCollectionConnection" nillable="true" type="DALSettings_DataCollectionConnection" />
  <xs:complexType name="DALSettings_DataCollectionConnection">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ConnectionString" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DALv2_DataCollectionEnabling.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="DALSettings_DataCollectionEnabling" nillable="true" type="DALSettings_DataCollectionEnabling" />
  <xs:complexType name="DALSettings_DataCollectionEnabling">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableToBDataCollection" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="EnableMarketDataCollection" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO


CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="MessageBusSettings_IntegratorEndpointConnectionInfo" nillable="true" type="MessageBusSettings_IntegratorEndpointConnectionInfo" />
  <xs:complexType name="MessageBusSettings_IntegratorEndpointConnectionInfo">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IntegratorServiceHost" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IntegratorServicePort" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="MessageBusSettings_ClientConnectionInfo" nillable="true" type="MessageBusSettings_ClientConnectionInfo" />
  <xs:complexType name="MessageBusSettings_ClientConnectionInfo">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IntegratorServicePipeName" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_ConnectionKeeping.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="MessageBusSettings_ConnectionKeeping" nillable="true" type="MessageBusSettings_ConnectionKeeping" />
  <xs:complexType name="MessageBusSettings_ConnectionKeeping">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedDisconnectedInterval_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="DisconnectedSessionReconnectInterval_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="SessionHealthCheckInterval_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="MessageBusSettings_PriceTransferBehavior" nillable="true" type="MessageBusSettings_PriceTransferBehavior" />
  <xs:simpleType name="PriceStreamTransferMode">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Reliable" />
      <xs:enumeration value="FastAndUnreliable" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="MessageBusSettings_PriceTransferBehavior">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="PriceStreamTransferMode" type="PriceStreamTransferMode" />
      <xs:element minOccurs="0" maxOccurs="1" name="PerSymbolPriceThrottlingInterval_Milliseconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="MessageBusSettings_ContractMismatchHandling" nillable="true" type="MessageBusSettings_ContractMismatchHandling" />
  <xs:complexType name="MessageBusSettings_ContractMismatchHandling">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="ForceConnectRemotingClientOnContractVersionMismatch" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO


ALTER TRIGGER [dbo].[SchemaXsdCheckTrigger] ON [dbo].[Settings] AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @Type NVARCHAR(100)
	DECLARE @SettingsXml [xml]
	DECLARE @Version [int]
	
	DECLARE settingsCursor CURSOR FOR
		SELECT Name, SettingsVersion, SettingsXml FROM inserted
    
	OPEN settingsCursor
    FETCH NEXT FROM settingsCursor INTO @Type, @Version, @SettingsXml
    WHILE @@FETCH_STATUS = 0
    BEGIN

		IF(@Type = N'Kreslik.Integrator.BusinessLayer.dll')
		BEGIN
			DECLARE @x1 XML([dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd]) 
			SET @x1 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.Common.dll')
		BEGIN
			DECLARE @x2 XML([dbo].[Kreslik.Integrator.Common.dll.xsd]) 
			SET @x2 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL.dll' AND @Version = 1)
		BEGIN
			DECLARE @x3 XML([dbo].[Kreslik.Integrator.DAL.dll.xsd]) 
			SET @x3 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL.dll' AND @Version = 2)
		BEGIN
			DECLARE @x31 XML([dbo].[Kreslik.Integrator.DALv2.dll.xsd]) 
			SET @x31 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL_IntegratorConnection')
		BEGIN
			DECLARE @x32 XML([dbo].[Kreslik.Integrator.DALv2_IntegratorConnection.xsd]) 
			SET @x32 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL_DataCollectionConnection')
		BEGIN
			DECLARE @x33 XML([dbo].[Kreslik.Integrator.DALv2_DataCollectionConnection.xsd]) 
			SET @x33 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL_DataCollectionEnabling')
		BEGIN
			DECLARE @x34 XML([dbo].[Kreslik.Integrator.DALv2_DataCollectionEnabling.xsd]) 
			SET @x34 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DiagnosticsDashboard.exe')
		BEGIN
			DECLARE @x4 XML([dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd]) 
			SET @x4 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.IntegratorClientHost.exe')
		BEGIN
			DECLARE @x5 XML([dbo].[Kreslik.Integrator.IntegratorClientHost.exe.xsd]) 
			SET @x5 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.IntegratorWatchDogSvc.exe')
		BEGIN
			DECLARE @x6 XML([dbo].[Kreslik.Integrator.IntegratorWatchDogSvc.exe.xsd]) 
			SET @x6 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBus.dll')
		BEGIN
			DECLARE @x7 XML([dbo].[Kreslik.Integrator.MessageBus.dll.xsd]) 
			SET @x7 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo')
		BEGIN
			DECLARE @x7_1 XML([dbo].[Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo.xsd]) 
			SET @x7_1 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo')
		BEGIN
			DECLARE @x7_2 XML([dbo].[Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo.xsd]) 
			SET @x7_2 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping')
		BEGIN
			DECLARE @x7_3 XML([dbo].[Kreslik.Integrator.MessageBusSettings_ConnectionKeeping.xsd]) 
			SET @x7_3 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior')
		BEGIN
			DECLARE @x7_4 XML([dbo].[Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior.xsd]) 
			SET @x7_4 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling')
		BEGIN
			DECLARE @x7_5 XML([dbo].[Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling.xsd]) 
			SET @x7_5 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.QuickItchN.dll')
		BEGIN
			DECLARE @x8 XML([dbo].[Kreslik.Integrator.QuickItchN.dll.xsd]) 
			SET @x8 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.RiskManagement.dll')
		BEGIN
			DECLARE @x9 XML([dbo].[Kreslik.Integrator.RiskManagement.dll.xsd]) 
			SET @x9 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.SessionManagement.dll')
		BEGIN
			DECLARE @x10 XML([dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]) 
			SET @x10 = @SettingsXml
		END

		ELSE
		BEGIN
			RAISERROR (N'Setting with name: %s is unknown to DB', 16, 2, @Type ) WITH SETERROR
			ROLLBACK TRAN
		END

		FETCH NEXT FROM settingsCursor INTO @Type, @Version, @SettingsXml
    END
	CLOSE settingsCursor
    DEALLOCATE settingsCursor
END
GO



INSERT INTO [dbo].[Settings]
           ([Name]
		   ,[FriendlyName]
		   ,[SettingsVersion]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.DAL.dll'
			,N'Integrator DAL'
			,2
			,N'<DALSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <KillSwitchPollIntervalSeconds>3</KillSwitchPollIntervalSeconds>
  <KillSwitchWatchDogIntervalSeconds>40</KillSwitchWatchDogIntervalSeconds>
  <IntegratorSwitchName>KGTIntegratorKillSwitch</IntegratorSwitchName>
  <CommandDistributorPollIntervalSeconds>3</CommandDistributorPollIntervalSeconds>
  <DataCollection>
    <DataProviderId>22</DataProviderId>
    <SourceFileId>932696</SourceFileId>
    <MaxBcpBatchSize>10000</MaxBcpBatchSize>
    <MaxInMemoryColectionSize>1000000</MaxInMemoryColectionSize>
    <MaxIntervalBetweenUploads_Seconds>10</MaxIntervalBetweenUploads_Seconds>
    <MaxUploadTimeAfterWhichSerializationStarts_Seconds>5</MaxUploadTimeAfterWhichSerializationStarts_Seconds>
    <MaxProcessingTimeAfterShutdownSignalled_Seconds>30</MaxProcessingTimeAfterShutdownSignalled_Seconds>
    <WaitIntervalBetweenBcpRetries_Seconds>2</WaitIntervalBetweenBcpRetries_Seconds>
    <WaitIntervalBeforeShutdownStartsSerialization_Seconds>4</WaitIntervalBeforeShutdownStartsSerialization_Seconds>
  </DataCollection>
</DALSettings>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings]
           ([Name]
		   ,[FriendlyName]
		   ,[SettingsVersion]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.DAL_IntegratorConnection'
			,N'Integrator DB connection (public)'
			,2
			,N'<DALSettings_IntegratorConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ConnectionString>Data Source=37.46.6.116;Initial Catalog=Integrator;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString>
</DALSettings_IntegratorConnection>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings]
           ([Name]
		   ,[FriendlyName]
		   ,[SettingsVersion]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.DAL_IntegratorConnection'
			,N'Integrator DB connection (private)'
			,2
			,N'<DALSettings_IntegratorConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ConnectionString>Data Source=192.168.75.66;Initial Catalog=Integrator;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString>
</DALSettings_IntegratorConnection>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings]
           ([Name]
		   ,[FriendlyName]
		   ,[SettingsVersion]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.DAL_DataCollectionConnection'
			,N'Integrator DC DB connection (public)'
			,2
			,N'<DALSettings_DataCollectionConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ConnectionString>Data Source=37.46.6.116;Initial Catalog=FXtickDB;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</ConnectionString>
</DALSettings_DataCollectionConnection>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings]
           ([Name]
		   ,[FriendlyName]
		   ,[SettingsVersion]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.DAL_DataCollectionConnection'
			,N'Integrator DC DB connection (private)'
			,2
			,N'<DALSettings_DataCollectionConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ConnectionString>Data Source=192.168.75.66;Initial Catalog=FXtickDB;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</ConnectionString>
</DALSettings_DataCollectionConnection>'
			,GETUTCDATE()
			,0)
GO



INSERT INTO [dbo].[Settings]
           ([Name]
		   ,[FriendlyName]
		   ,[SettingsVersion]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.DAL_DataCollectionEnabling'
			,N'Integrator DC enabling (enabled)'
			,2
			,N'<DALSettings_DataCollectionEnabling xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <EnableToBDataCollection>true</EnableToBDataCollection>
    <EnableMarketDataCollection>true</EnableMarketDataCollection>
</DALSettings_DataCollectionEnabling>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings]
           ([Name]
		   ,[FriendlyName]
		   ,[SettingsVersion]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.DAL_DataCollectionEnabling'
			,N'Integrator DC enabling (ToB collection disabled)'
			,2
			,N'<DALSettings_DataCollectionEnabling xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <EnableToBDataCollection>false</EnableToBDataCollection>
    <EnableMarketDataCollection>true</EnableMarketDataCollection>
</DALSettings_DataCollectionEnabling>'
			,GETUTCDATE()
			,0)
GO


INSERT INTO [dbo].[Settings]
           ([Name]
		   ,[FriendlyName]
		   ,[SettingsVersion]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo'
			,N'Message Bus'
			,2
			,N'<MessageBusSettings_IntegratorEndpointConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <IntegratorServiceHost>localhost</IntegratorServiceHost>
  <IntegratorServicePort>9000</IntegratorServicePort>
</MessageBusSettings_IntegratorEndpointConnectionInfo>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings]
           ([Name]
		   ,[FriendlyName]
		   ,[SettingsVersion]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo'
			,N'Message Bus'
			,2
			,N'<MessageBusSettings_ClientConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <IntegratorServicePipeName>IntegratorService</IntegratorServicePipeName>
</MessageBusSettings_ClientConnectionInfo>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings]
           ([Name]
		   ,[FriendlyName]
		   ,[SettingsVersion]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping'
			,N'Message Bus'
			,2
			,N'<MessageBusSettings_ConnectionKeeping xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <MaximumAllowedDisconnectedInterval_Seconds>120</MaximumAllowedDisconnectedInterval_Seconds>
  <DisconnectedSessionReconnectInterval_Seconds>5</DisconnectedSessionReconnectInterval_Seconds>
  <SessionHealthCheckInterval_Seconds>10</SessionHealthCheckInterval_Seconds>
</MessageBusSettings_ConnectionKeeping>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings]
           ([Name]
		   ,[FriendlyName]
		   ,[SettingsVersion]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior'
			,N'Message Bus'
			,2
			,N'<MessageBusSettings_PriceTransferBehavior xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <PriceStreamTransferMode>Reliable</PriceStreamTransferMode>
  <PerSymbolPriceThrottlingInterval_Milliseconds>300</PerSymbolPriceThrottlingInterval_Milliseconds>
</MessageBusSettings_PriceTransferBehavior>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings]
           ([Name]
		   ,[FriendlyName]
		   ,[SettingsVersion]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling'
			,N'Message Bus'
			,2
			,N'<MessageBusSettings_ContractMismatchHandling xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ForceConnectRemotingClientOnContractVersionMismatch>false</ForceConnectRemotingClientOnContractVersionMismatch>
</MessageBusSettings_ContractMismatchHandling>'
			,GETUTCDATE()
			,0)
GO

