SET IDENTITY_INSERT [dbo].[LiquidityProvider] ON 
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (21, N'FXall', N'FXA')
GO
SET IDENTITY_INSERT [dbo].[LiquidityProvider] OFF
GO

SET IDENTITY_INSERT [dbo].[LiqProviderStream] ON 
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (77, N'FA1', 21)
GO
SET IDENTITY_INSERT [dbo].[LiqProviderStream] OFF
GO