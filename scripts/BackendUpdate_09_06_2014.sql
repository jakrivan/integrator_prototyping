


CREATE TABLE [dbo].[TradingSystem](
	[TradingSystemId] [int] IDENTITY(1,1) NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[Description] [nvarchar](1024) NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_TradingSystem_id] ON [dbo].[TradingSystem]
(
	[TradingSystemId] ASC
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[TradingSystemDealsExecuted_Archive](
	[TradingSystemId] [int] NOT NULL,
	[InternalTransactionIdentity] [nvarchar](50) NOT NULL,
	[AmountBasePolExecuted] [decimal](18,0) NOT NULL,
	[DatePartDealExecuted] [date] NOT NULL
) ON [LargeSlowDataFileGroup]
GO

CREATE CLUSTERED INDEX [IX_TradingSystemDealsExecuted_date] ON [dbo].[TradingSystemDealsExecuted_Archive]
(
	[DatePartDealExecuted] ASC
) ON [LargeSlowDataFileGroup]
GO

CREATE TABLE [dbo].[TradingSystemDealsExecuted_Daily](
	[TradingSystemId] [int] NOT NULL,
	[AmountBasePolExecuted] [decimal](18,0) NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[Price] [decimal](18,9) NOT NULL,
	[InternalTransactionIdentity] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [IX_TradingSystemDealsExecuted_TradingSystemSymbol] ON [dbo].[TradingSystemDealsExecuted_Daily]
(
	[TradingSystemId] ASC,
	[SymbolId] ASC
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CommissionsPerCounterparty](
	[CounterpartyId] [tinyint] NOT NULL,
	[CommissionPricePerMillion] [decimal](18,6) NOT NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_CommissionsPerCounterparty_CounterpartyId] ON [dbo].[CommissionsPerCounterparty]
(
	[CounterpartyId] ASC
) ON [PRIMARY]
GO

INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) SELECT [CounterpartyId], 3.25 FROM [dbo].[Counterparty]
UPDATE [dbo].[CommissionsPerCounterparty] SET [CommissionPricePerMillion] = 0 WHERE [CounterpartyId] = (SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'CTI')
UPDATE [dbo].[CommissionsPerCounterparty] SET [CommissionPricePerMillion] = 8.25 WHERE [CounterpartyId] = (SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FA1')
UPDATE [dbo].[CommissionsPerCounterparty] SET [CommissionPricePerMillion] = 8.25 WHERE [CounterpartyId] = (SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTF')
UPDATE [dbo].[CommissionsPerCounterparty] SET [CommissionPricePerMillion] = 8.25 WHERE [CounterpartyId] = (SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTA')
UPDATE [dbo].[CommissionsPerCounterparty] SET [CommissionPricePerMillion] = 8.25 WHERE [CounterpartyId] = (SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LM1')
GO

CREATE PROCEDURE [dbo].[Daily_ArchiveTradingSystemDeals_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Archive]
			([TradingSystemId],
			[InternalTransactionIdentity],
			[AmountBasePolExecuted],
			[DatePartDealExecuted])
		SELECT
			[TradingSystemId],
			[InternalTransactionIdentity],
			[AmountBasePolExecuted],
			DATEADD(DAY, -1, GETUTCDATE())
		FROM
			[dbo].[TradingSystemDealsExecuted_Daily]	
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[TradingSystemDealsExecuted_Daily]
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO


CREATE PROCEDURE [dbo].[Daily_DeleteOldSchedules_SP]
AS
BEGIN
	DELETE FROM
		[dbo].[SystemsTradingControlSchedule]
	WHERE
		IsDaily = 0 AND IsWeekly = 0 AND ScheduleTimeUtc < GETUTCDATE()
END
GO


CREATE FUNCTION [dbo].[csvlist_to_ints_tbl] (@list varchar(MAX))
   RETURNS @tbl TABLE (num int NOT NULL) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1
      INSERT @tbl (num)
         VALUES (CONVERT(int, substring(@list, @pos + 1, @valuelen)))
      SELECT @pos = @nextpos
   END
   RETURN
END
GO

CREATE FUNCTION [dbo].[csvlist_to_intpairs_tbl] (@list varchar(MAX))
   RETURNS @tbl TABLE (num1 int NOT NULL, num2 int) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1, @valuelen = 0

   WHILE @nextpos > 0 AND @nextpos <> len(@list)
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1

	  IF @nextpos > @pos + 1
	  BEGIN

		  DECLARE @num1 int = CONVERT(INT, substring(@list, @pos + 1, @valuelen))
		  SELECT @pos = @nextpos

		  SELECT @nextpos = charindex(',', @list, @pos + 1)
		  SELECT @valuelen = CASE WHEN @nextpos > 0
								  THEN @nextpos
								  ELSE len(@list) + 1
							 END - @pos - 1

		  DECLARE @num2 int = CONVERT(int, substring(@list, @pos + 1, @valuelen))

		  INSERT @tbl (num1, num2)
			 VALUES (@num1, @num2)
	  END
	  ELSE IF @nextpos <= 0
	  BEGIN
		select @num2 = cast('Invalid csv int pairs list ' + @list as int)
	  END
	  SELECT @pos = @nextpos
	END
   RETURN
END
GO


ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 0),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL,
	@SingleFillSystemId int = NULL,
	@MultipleFillsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END
	
	-- this is major case (one external deal for one internal order)
	IF @SingleFillSystemId IS NOT NULL
	BEGIN
		INSERT INTO
		[dbo].[TradingSystemDealsExecuted_Daily]
			([TradingSystemId]
			,[AmountBasePolExecuted]
			,[SymbolId]
			,[CounterpartyId]
			,[Price]
			,[InternalTransactionIdentity])
		VALUES
			(@SingleFillSystemId
			,@AmountBasePolExecuted
			,@SymbolId
			,@CounterpartyId
			,@Price
			,@InternalDealId)
	END
	ELSE IF @MultipleFillsSystemIdsAndAmountsBaseAbs IS NOT NULL
	BEGIN
		INSERT INTO
		[dbo].[TradingSystemDealsExecuted_Daily]
			([TradingSystemId]
			,[AmountBasePolExecuted]
			,[SymbolId]
			,[CounterpartyId]
			,[Price]
			,[InternalTransactionIdentity])
		SELECT
			intpair.num1
			,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
			,@SymbolId
			,@CounterpartyId
			,@Price
			,@InternalDealId
		FROM
		[dbo].[csvlist_to_intpairs_tbl] (@MultipleFillsSystemIdsAndAmountsBaseAbs) intpair
	END
	
	INSERT INTO 
	[dbo].[DealExternalExecuted]
           ([IntegratorReceivedExecutionReportUtc]
           ,[CounterpartyId]
           ,[SymbolId]
           ,[IntegratorExternalOrderPrivateId]
           ,[DealDirectionId]
           ,[AmountBasePolExecuted]
		   ,[AmountTermPolExecutedInUsd]
           ,[Price]
           ,[CounterpartyTransactionId]
           ,[CounterpartySuppliedExecutionTimeStampUtc]
           ,[ValueDateCounterpartySuppliedLocMktDate]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[IntegratorSentExternalOrderUtc]
		   ,[CounterpartySentExecutionReportUtc]
		   ,[InternalTransactionIdentity]
		   ,[FlowSideId])
     VALUES
           (@ExecutionReportReceivedUtc
           ,@CounterpartyId
           ,@SymbolId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolExecuted
		   ,@AmountTermPolExecutedInUsd
           ,@Price
           ,@CounterpartyTransactionId
           ,@CounterpartyExecutionReportTimeStampUtc
           ,@ValueDateCounterpartySuppliedLocMktDate
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@QuoteReceivedToOrderSentInternalLatency
		   ,@IntegratorSentExternalOrderUtc
		   ,@CounterpartySentExecutionReportUtc
		   ,@InternalDealId
		   ,@FlowSideId)
END
GO



USE msdb ;
GO

EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'ArchiveTradingSystemDeals',
    @subsystem = N'TSQL',
    @command = N'exec Daily_ArchiveTradingSystemDeals_SP',
	@database_name = N'Integrator',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO


EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'DeleteOldSchedules',
    @subsystem = N'TSQL',
    @command = N'exec Daily_DeleteOldSchedules_SP',
	@database_name = N'Integrator',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO

--MANUALY reaorder the steps - there is no other documented way than doing this through SSMS


USE INTEGRATOR;
GO




ALTER TABLE VenueCrossSettings ADD [TradingSystemId] [int]
ALTER TABLE VenueMMSettings ADD [TradingSystemId] [int]
GO

DROP INDEX [IX_VenueCrossSettingsSymbolPerCounterparty] ON [dbo].[VenueCrossSettings] WITH ( ONLINE = OFF )
GO

CREATE CLUSTERED INDEX [IX_VenueCrossSettingsSymbolPerCounterparty] ON [dbo].[VenueCrossSettings]
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
) ON [PRIMARY]
GO


DROP INDEX [IX_VenueMMSettingsSymbolPerCounterparty] ON [dbo].[VenueMMSettings] WITH ( ONLINE = OFF )
GO

CREATE CLUSTERED INDEX [IX_VenueMMSettingsSymbolPerCounterparty] ON [dbo].[VenueMMSettings]
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
) ON [PRIMARY]
GO

DROP TRIGGER [dbo].[LastUpdatedTrigger_VenueCrossSettings]
GO

DROP TRIGGER [dbo].[LastUpdatedTrigger_VenueMMSettings]
GO

SET IDENTITY_INSERT [dbo].[TradingSystem] ON
GO

DECLARE @tradingSystems TABLE (id int IDENTITY(1,1) NOT NULL, isCrossSystem bit NOT NULL, symbolId tinyint NOT NULL, counterpartyId tinyint NOT NULL, description nvarchar(1024) NULL)

INSERT INTO
	@tradingSystems (isCrossSystem, symbolId, counterpartyId, description)
SELECT 
	1,
	SymbolId,
	crossSystem.CounterpartyId,
	'Default_' + counterparty.CounterpartyCode + '_CrossSystem_' + symbol.ShortName
FROM 
	[dbo].[VenueCrossSettings] crossSystem
	INNER JOIN [dbo].[Counterparty] counterparty ON crossSystem.CounterpartyId = counterparty.CounterpartyId
	INNER JOIN [dbo].[Symbol] symbol ON crossSystem.SymbolId = symbol.Id


INSERT INTO
	@tradingSystems (isCrossSystem, symbolId, counterpartyId, description)
SELECT 
	0,
	SymbolId,
	mmSystem.CounterpartyId,
	'Default_' + counterparty.CounterpartyCode + '_MMSystem_' + symbol.ShortName
FROM 
	[dbo].[VenueMMSettings] mmSystem
	INNER JOIN [dbo].[Counterparty] counterparty ON mmSystem.CounterpartyId = counterparty.CounterpartyId
	INNER JOIN [dbo].[Symbol] symbol ON mmSystem.SymbolId = symbol.Id

	
INSERT INTO [dbo].[TradingSystem] ([TradingSystemId], [SymbolId], [Description])
SELECT id, symbolId, description
FROM @tradingSystems

UPDATE 
	sett
SET
	TradingSystemId = id
FROM
	[dbo].[VenueCrossSettings] sett
	INNER JOIN @tradingSystems systemsIds ON sett.symbolId = systemsIds.symbolId AND sett.counterpartyId = systemsIds.counterpartyId AND systemsIds.isCrossSystem = 1
	
UPDATE 
	sett
SET
	TradingSystemId = id
FROM
	[dbo].[VenueMMSettings] sett
	INNER JOIN @tradingSystems systemsIds ON sett.symbolId = systemsIds.symbolId AND sett.counterpartyId = systemsIds.counterpartyId AND systemsIds.isCrossSystem = 0
	

SET IDENTITY_INSERT [dbo].[TradingSystem] OFF
GO

ALTER TABLE [VenueCrossSettings] ALTER COLUMN [TradingSystemId] [int] NOT NULL
ALTER TABLE [VenueMMSettings] ALTER COLUMN [TradingSystemId] [int] NOT NULL
GO

ALTER TABLE [dbo].[VenueCrossSettings] ADD CONSTRAINT UNQ__VenueCrossSettings__TradingSystemId UNIQUE ([TradingSystemId])
ALTER TABLE [dbo].[VenueMMSettings] ADD CONSTRAINT UNQ__VenueMMSettings__TradingSystemId UNIQUE ([TradingSystemId])
GO


CREATE TRIGGER [dbo].[LastUpdatedTrigger_VenueCrossSettings] ON [dbo].[VenueCrossSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[VenueCrossSettings] Set [LastUpdatedUtc] = GETUTCDATE() from [dbo].[VenueCrossSettings] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueCrossSettings'

END
GO


CREATE TRIGGER [dbo].[LastUpdatedTrigger_VenueMMSettings] ON [dbo].[VenueMMSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[VenueMMSettings] Set [LastUpdatedUtc] = GETUTCDATE() from [dbo].[VenueMMSettings] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueMMSettings'

END
GO


CREATE TRIGGER [dbo].[LastDeletedTrigger_VenueCrossSettings] ON [dbo].[VenueCrossSettings]
AFTER DELETE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON
UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueCrossSettings'

END
GO


CREATE TRIGGER [dbo].[LastDeletedTrigger_VenueMMSettings] ON [dbo].[VenueMMSettings]
AFTER DELETE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON
UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueMMSettings'

END
GO


exec sp_rename '[GetUpdatedVenueSystemSettings_SP]', 'GetUpdatedVenueCrossSystemSettings_SP'
exec sp_rename '[GetUpdateVenueMMSystemSettings_SP]', 'GetUpdatedVenueMMSystemSettings_SP'
GO



ALTER PROCEDURE [dbo].[GetUpdatedVenueCrossSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueCrossSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			[TradingSystemId]
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenTrades_seconds]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[Enabled]
			,[LastResetRequestedUtc]
			,[LastUpdatedUtc]
		FROM 
			[dbo].[VenueCrossSettings] sett
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name 
	END
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueMMSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueMMSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			[TradingSystemId]
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice]
			,[SizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[Enabled]
			,[LastResetRequestedUtc]
			,[LastUpdatedUtc]
		FROM 
			[dbo].[VenueMMSettings] sett
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name
	END
END
GO

DROP PROCEDURE [dbo].[GetNopsPolSidedPerPair_SP]
GO


CREATE TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily]
(
	[TradingSystemId] [int] NOT NULL,
	[NopBasePol] [decimal](18,0) NOT NULL,
	[VolumeUsdM] [decimal](18,6) NOT NULL,
	[DealsNum] [int]  NOT NULL,
	[PnlGrossUsdPerMUsd] [decimal](18,6) NOT NULL,
	[PnlGrossUsd] [decimal](18,6) NOT NULL,
	[CommissionsUsdPerMUsd] [decimal](18,6) NOT NULL,
	[CommissionsUsd] [decimal](18,6) NOT NULL,
	[PnlNetUsdPerMUsd] [decimal](18,6) NOT NULL,
	[PnlNetUsd] [decimal](18,6) NOT NULL
)ON [PRIMARY]
GO

CREATE PROCEDURE [dbo].[GetTradingSystemsDailyStats_SP]
AS
BEGIN
	SELECT
		--systemStats.Symbol AS Symbol,
		systemStats.SymbolId AS SymbolId,
		systemStats.TradingSystemId AS TradingSystemId,
		--systemStats.TradingSystemDescription AS TradingSystemDescription,
		systemStats.NopBasePol AS NopBasePol,
		systemStats.VolumeInUsd/1000000 AS VolumeUsdM,
		systemStats.DealsNum as DealsNum,
		systemStats.PnlTermPolInUsd/(systemStats.VolumeInUsd/1000000) AS PnlGrossUsdPerMUsd,
		systemStats.PnlTermPolInUsd AS PnlGrossUsd,
		systemStats.CommissionInUSD/(systemStats.VolumeInUsd/1000000) AS CommissionsUsdPerMUsd,
		systemStats.CommissionInUSD AS CommissionsUsd,
		(systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD)/(systemStats.VolumeInUsd/1000000) AS PnlNetUsdPerMUsd,
		systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD AS PnlNetUsd
	FROM
		(
		SELECT
			--Symbol.Name AS Symbol,
			Symbol.Id AS SymbolId,
			tradingSystem.TradingSystemId AS TradingSystemId,
			--tradingSystem.Description AS TradingSystemDescription,
			systemsPositions.NopBasePol AS NopBasePol,
			systemsPositions.DealsNum as DealsNum,
			systemsPositions.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd,
			--PnL = term position + yield of immediate close of outstanding base position
			--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
			(systemsPositions.NopTermPol + (CASE WHEN systemsPositions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * systemsPositions.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
			systemsPositions.CommissionInCCY1 * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionInUSD
		FROM
			(
			SELECT 
				[TradingSystemId],
				SUM([AmountBasePolExecuted]) AS NopBasePol,
				SUM(-[AmountBasePolExecuted]* [Price]) AS NopTermPol,
				SUM(ABS([AmountBasePolExecuted])) AS VolumeInCcy1,
				SUM(ABS([AmountBasePolExecuted]) * CommissionPricePerMillion / 1000000) AS CommissionInCCY1,
				COUNT([TradingSystemId]) AS DealsNum
			FROM
				[dbo].[TradingSystemDealsExecuted_Daily] deal
				INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
			GROUP BY
				[TradingSystemId]
			) systemsPositions
			INNER JOIN [dbo].[TradingSystem] tradingSystem ON systemsPositions.TradingSystemId = tradingSystem.TradingSystemId
			INNER JOIN [dbo].[Symbol] symbol ON tradingSystem.SymbolId = symbol.Id
			INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
			INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
			) systemStats

	--ORDER BY
	--	systemStats.Symbol
END
GO

CREATE PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP]
AS
BEGIN

	--create the temp table before transaction to speed up things
	CREATE TABLE #TempTradingSystemStatistics
	(
		[SymbolId] [tinyint] NOT NULL,
		[TradingSystemId] [int] NOT NULL,
		[NopBasePol] [decimal](18,0) NOT NULL,
		[VolumeUsdM] [decimal](18,6) NOT NULL,
		[DealsNum] [int]  NOT NULL,
		[PnlGrossUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlGrossUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsdPerMUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsd] [decimal](18,6) NOT NULL
	)

	INSERT INTO 
		#TempTradingSystemStatistics
		([SymbolId]
		,[TradingSystemId]
		,[NopBasePol]
		,[VolumeUsdM]
		,[DealsNum]
		,[PnlGrossUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsdPerMUsd]
		,[CommissionsUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlNetUsd])
    exec [dbo].[GetTradingSystemsDailyStats_SP]

	--this needs to be in transaction in order not to see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingSystemDaily]
			([TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd])
		SELECT
			[TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd]
		FROM
			#TempTradingSystemStatistics
	COMMIT
END
GO

-- those permissions are needed so that the trigger can be executed
GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetTradingSystemsDailyStats_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT ALTER ON [dbo].[PersistedStatisticsPerTradingSystemDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[PersistedStatisticsPerTradingSystemDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT ALTER ON [dbo].[PersistedStatisticsPerTradingSystemDaily] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT SELECT ON [dbo].[PersistedStatisticsPerTradingSystemDaily] TO [IntegratorKillSwitchUser] AS [dbo]
GO


ALTER TRIGGER DealExternalExecutedInserted ON [dbo].[DealExternalExecuted] AFTER INSERT
AS
BEGIN
	-- commit the internal insert transaction to prevent deadlocks
	COMMIT TRANSACTION
	exec [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP]
	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION
END
GO

--shortening the name - as table has just 40 chars
INSERT INTO [dbo].[TableChangeLog] ([TableName], [LastUpdatedUtc]) VALUES ('PersistedStatisticPerTradingSystemDaily', '1/1/1900')
GO

CREATE PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]
AS
BEGIN
	UPDATE 
		[dbo].[TableChangeLog] 
	SET 
		[LastUpdatedUtc] = GETUTCDATE() 
	WHERE 
		[TableName] = 'PersistedStatisticPerTradingSystemDaily' 
		AND [LastUpdatedUtc] < DATEADD(SECOND, -2, GETUTCDATE())

	IF @@ROWCOUNT > 0
	BEGIN
		exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP]
	END
END
GO

GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[GetVenueCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumTimeBetweenTrades_seconds],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[Enabled],
		sett.[LastResetRequestedUtc],
		sett.[LastUpdatedUtc]
	FROM 
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

GRANT EXECUTE ON [dbo].[GetVenueCrossSystemSettingsAndStats_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[GetVenueMMSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice],
		sett.[SizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[Enabled],
		sett.[LastResetRequestedUtc],
		sett.[LastUpdatedUtc]
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

GRANT EXECUTE ON [dbo].[GetVenueMMSystemSettingsAndStats_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


ALTER PROCEDURE [dbo].[VenueCrossSystemSettingsDisableEnableOne_SP]
(
	@TradingSystemId int,
	@Enable Bit
)
AS
BEGIN

	UPDATE
		[dbo].[VenueCrossSettings]
	SET
		[Enabled] = @Enable
	WHERE
		[TradingSystemId] = @TradingSystemId
END
GO


ALTER PROCEDURE [dbo].[VenueMMSettingsDisableEnableOne_SP]
(
	@TradingSystemId int,
	@Enable Bit
)
AS
BEGIN
	
	UPDATE
		[dbo].[VenueMMSettings]
	SET
		[Enabled] = @Enable
	WHERE
		[TradingSystemId] = @TradingSystemId
END
GO


ALTER PROCEDURE [dbo].[VenueMMSettingsReset_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	UPDATE
		[dbo].[VenueMMSettings]
	SET
		[LastResetRequestedUtc] = GETUTCDATE()
	WHERE
		[TradingSystemId] = @TradingSystemId
		AND [Enabled] = 0
END
GO

ALTER PROCEDURE [dbo].[VenueCrossSystemSettingsReset_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	UPDATE
		[dbo].[VenueCrossSettings]
	SET
		[LastResetRequestedUtc] = GETUTCDATE()
	WHERE
		[TradingSystemId] = @TradingSystemId
		AND [Enabled] = 0
END
GO

CREATE PROCEDURE [dbo].[VenueMMSettingsDeleteOne_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	DELETE FROM
		[dbo].[VenueMMSettings]
	WHERE
		[TradingSystemId] = @TradingSystemId
		AND [Enabled] = 0
		
	IF @@ROWCOUNT > 0
	BEGIN
		DELETE FROM 
			[dbo].[TradingSystem]
		WHERE 
			[TradingSystemId] = @TradingSystemId
	END
END
GO

GRANT EXECUTE ON [dbo].[VenueMMSettingsDeleteOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[VenueCrossSettingsDeleteOne_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	DELETE FROM
		[dbo].[VenueCrossSettings]
	WHERE
		[TradingSystemId] = @TradingSystemId
		AND [Enabled] = 0
		
	IF @@ROWCOUNT > 0
	BEGIN
		DELETE FROM 
			[dbo].[TradingSystem]
		WHERE 
			[TradingSystemId] = @TradingSystemId
	END
END
GO

GRANT EXECUTE ON [dbo].[VenueCrossSettingsDeleteOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


ALTER PROCEDURE [dbo].[VenueMMSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit]
)
AS
BEGIN
	UPDATE
		[dbo].[VenueMMSettings]
	SET
		[SizeToTrade] = @SizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds,
		[Enabled] = @Enabled
	WHERE
		[TradingSystemId] = @TradingSystemId
		AND [LastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

ALTER PROCEDURE [dbo].[VenueCrossSystemSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenTrades_seconds [int],
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit]
)
AS
BEGIN
	UPDATE
		[dbo].[VenueCrossSettings]
	SET
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[MinimumDiscountBasisPointsToTrade] = @MinimumDiscountBasisPointsToTrade,
		[MinimumTimeBetweenTrades_seconds] = @MinimumTimeBetweenTrades_seconds,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds,
		[Enabled] = @Enabled
	WHERE
		[TradingSystemId] = @TradingSystemId
		AND [LastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO


CREATE PROCEDURE [dbo].[VenueMMSettingsInsertSingle_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId]) VALUES (@SymbolId)

	SELECT @TradingSystemId = scope_identity();

	INSERT INTO [dbo].[VenueMMSettings]
           ([SymbolId]
           ,[SizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[Enabled]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@SizeToTrade 
           ,@BestPriceImprovementOffsetBasisPoints
           ,@MaximumDiscountBasisPointsToTrade
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
           ,@Enabled
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

GRANT EXECUTE ON [dbo].[VenueMMSettingsInsertSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[VenueCrossSettingsInsertSingle_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenTrades_seconds [int],
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId]) VALUES (@SymbolId)

	SELECT @TradingSystemId = scope_identity();

	INSERT INTO [dbo].[VenueCrossSettings]
           ([SymbolId]
           ,[MinimumSizeToTrade]
           ,[MaximumSizeToTrade]
           ,[MinimumDiscountBasisPointsToTrade]
           ,[MinimumTimeBetweenTrades_seconds]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[Enabled]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MinimumSizeToTrade
           ,@MaximumSizeToTrade
           ,@MinimumDiscountBasisPointsToTrade
           ,@MinimumTimeBetweenTrades_seconds
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
           ,@Enabled
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

GRANT EXECUTE ON [dbo].[VenueCrossSettingsInsertSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO