DROP TABLE [dbo].[MarketData]
GO

/****** Object:  Table [dbo].[MarketData]    Script Date: 27. 9. 2013 15:02:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarketData](
	[RecordID] [bigint] IDENTITY(1,1) NOT NULL,
	[FXPairId] [tinyint] NOT NULL,
	[SideId] [bit] NULL,
	[Price] [decimal](18, 9) NULL,
	[Size] [decimal](18, 0) NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradeSideId] [bit] NULL,
	[CounterpartySentTimeUtc] [datetime2] NULL,	
	[IntegratorReceivedTimeUtc] [datetime2] NOT NULL,
	[CounterpartyPriceIdentity] [nvarchar](max) NULL,
	[IntegratorPriceIdentity] [uniqueidentifier] NULL,
	[MinimumSize] [decimal](18, 0) NULL,
	[Granularity] [decimal](18, 0) NULL,
	[RecordTypeId] [tinyint] NOT NULL,
	[IsLastInContinuousData] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

GRANT INSERT ON [dbo].[MarketData] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[MarketData] TO [IntegratorServiceAccount] AS [dbo]
GO


/****** Object:  Index [IX_PairTime]    Script Date: 27. 9. 2013 15:02:43 ******/
CREATE CLUSTERED INDEX [IX_PairTime] ON [dbo].[MarketData]
(
	[FXPairId] ASC,
	[IntegratorReceivedTimeUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_IntegratorPriceIdentity]    Script Date: 27. 9. 2013 15:19:31 ******/
CREATE NONCLUSTERED INDEX [IX_IntegratorPriceIdentity] ON [dbo].[MarketData]
(
	[IntegratorPriceIdentity] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_FxPair]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_LiqProviderStream] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_LiqProviderStream]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_MarketDataRecordType] FOREIGN KEY([RecordTypeId])
REFERENCES [dbo].[MarketDataRecordType] ([RecordTypeId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_MarketDataRecordType]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_PriceSide] FOREIGN KEY([SideId])
REFERENCES [dbo].[PriceSide] ([PriceSideId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_PriceSide]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_TradeSide] FOREIGN KEY([TradeSideId])
REFERENCES [dbo].[TradeSide] ([TradeSideId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_TradeSide]
GO