BEGIN TRANSACTION AAA

ALTER TABLE [dbo].[SystemsTradingControlSchedule] ADD IsImmediatelyOnce BIT NULL
GO
UPDATE [dbo].[SystemsTradingControlSchedule] SET IsImmediatelyOnce = 0
GO
ALTER TABLE [dbo].[SystemsTradingControlSchedule] ALTER COLUMN IsImmediatelyOnce BIT NOT NULL
GO

INSERT INTO [dbo].[SystemsTradingControlScheduleAction] ([ActionId], [ActionName]) VALUES (5, 'LogFatal')
GO

ALTER PROCEDURE [dbo].[SystemsTradingControlScheduleInsertSingle_SP]
(
	@Enable Bit,
	@IsDaily Bit,
	@IsWeekly Bit,
	@ScheduleTimeZoneSpecific datetime2(7),
	@TimeZoneCode char(3),
	@ActionName varchar(24),
	@ScheduleDescription NVARCHAR(1024),
	@ScheduleEntryId int OUTPUT
)
AS
BEGIN
	INSERT INTO [dbo].[SystemsTradingControlSchedule]
           ([IsEnabled]
		   ,[IsDaily]
           ,[IsWeekly]
           ,[ScheduleTimeZoneSpecific]
		   ,[TimeZoneCode]
           ,[ActionId]
           ,[ScheduleDescription]
		   ,[IsImmediatelyOnce])
     VALUES
           (@Enable
			,@IsDaily
           ,@IsWeekly
           ,@ScheduleTimeZoneSpecific
		   ,@TimeZoneCode
           ,(SELECT [ActionId] FROM [dbo].[SystemsTradingControlScheduleAction] WHERE [ActionName] = @ActionName)
           ,@ScheduleDescription
		   ,0)
	
	SELECT @ScheduleEntryId = scope_identity();
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedSystemsTradingControlSchedule_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@IsDailyRecords Bit = NULL,
	@IsWeeklyRecords Bit = NULL,
	@IsImmediateRecords Bit = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'SystemsTradingControlSchedule'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		SELECT
			[ScheduleEntryId]
			,[IsEnabled]
			,[IsDaily]
			,[IsWeekly]
			,[IsImmediatelyOnce]
			,[ScheduleTimeZoneSpecific]
			,[TimeZoneCode]
			,[ActionName]
			,[ScheduleDescription]
			,[LastUpdatedUtc]
		FROM 
			[dbo].[SystemsTradingControlSchedule] schedule
			INNER JOIN [dbo].[SystemsTradingControlScheduleAction] action on schedule.ActionId = action.ActionId
		WHERE
			(@IsDailyRecords IS NULL OR [IsDaily] = @IsDailyRecords)
			AND (@IsWeeklyRecords IS NULL OR [IsWeekly] = @IsWeeklyRecords)
			AND (@IsImmediateRecords IS NULL OR [IsImmediatelyOnce] = @IsImmediateRecords)
		ORDER BY
			[IsDaily],
			[IsWeekly],
			[ScheduleTimeZoneSpecific]
			
		IF @IsImmediateRecords IS NULL OR @IsImmediateRecords = 1
		BEGIN
		
			DELETE FROM 
				[dbo].[SystemsTradingControlSchedule]
			WHERE
				[IsImmediatelyOnce] = 1
		
		END
		
		
	END
END
GO

ALTER TRIGGER [dbo].[LastDeletedTrigger_SystemsTradingControlSchedule] ON [dbo].[SystemsTradingControlSchedule]
AFTER DELETE
AS
BEGIN

--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

IF EXISTS(SELECT * FROM deleted WHERE [IsImmediatelyOnce] != 1)
BEGIN
	UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'SystemsTradingControlSchedule'
END

END
GO


CREATE TABLE [dbo].[PrimeBrokerCreditLine](
	[PrimeBrokerId] [tinyint] NOT NULL,
	[CreditLineId] [tinyint] IDENTITY(1,1) NOT NULL,
	[CreditLineName] [varchar](255) NOT NULL,
	[MaxUnsettledNopUsd] [decimal](18, 2) NOT NULL,
	[MaxUnsettledNopUsdToTurnGFO] [decimal](18, 2) NOT NULL
) ON [primary]
GO

CREATE UNIQUE CLUSTERED INDEX [PK_PrimeBrokerCreditLines] ON [dbo].[PrimeBrokerCreditLine]
(
	[CreditLineId] ASC
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PrimeBrokerCreditLine] ADD CONSTRAINT UQ_CreditLineName UNIQUE NONCLUSTERED( [CreditLineName] )
GO

ALTER TABLE [dbo].[PrimeBrokerCreditLine]  WITH CHECK ADD  CONSTRAINT [FK_PrimeBrokerCreditLines_PrimeBroker] FOREIGN KEY([PrimeBrokerId])
REFERENCES [dbo].[PrimeBroker] ([PrimeBrokerId])
GO
ALTER TABLE [dbo].[PrimeBrokerCreditLine] CHECK CONSTRAINT [FK_PrimeBrokerCreditLines_PrimeBroker]
GO

CREATE PROCEDURE [dbo].[PrimeBrokerCreditLine_UpdateLimit_SP]
(
	@CreditLineName VARCHAR(255),
	@NewLimit decimal(18,2)
)
AS
BEGIN
	UPDATE 
		[dbo].[PrimeBrokerCreditLine]
	SET
		[MaxUnsettledNopUsd] = @NewLimit
		,[MaxUnsettledNopUsdToTurnGFO] = CASE WHEN @NewLimit - 2000000 > 0 THEN @NewLimit - 2000000 ELSE 0 END
	WHERE
		[CreditLineName] = @CreditLineName
END
GO

GRANT EXECUTE ON [dbo].[PrimeBrokerCreditLine_UpdateLimit_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE TABLE [dbo].[PrimeBrokerCreditLines_CptMapping](
	[CounterpartyId] [tinyint] NOT NULL,
	[CreditLineId] [tinyint] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PrimeBrokerCreditLines_CptMapping]  WITH CHECK ADD  CONSTRAINT [FK_PrimeBrokerCreditLines_CptMapping_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[PrimeBrokerCreditLines_CptMapping] CHECK CONSTRAINT [FK_PrimeBrokerCreditLines_CptMapping_Counterparty]
GO

ALTER TABLE [dbo].[PrimeBrokerCreditLines_CptMapping]  WITH CHECK ADD  CONSTRAINT [FK_PrimeBrokerCreditLines_CptMapping_CreditLineId] FOREIGN KEY([CreditLineId])
REFERENCES [dbo].[PrimeBrokerCreditLine] ([CreditLineId])
GO
ALTER TABLE [dbo].[PrimeBrokerCreditLines_CptMapping] CHECK CONSTRAINT [FK_PrimeBrokerCreditLines_CptMapping_CreditLineId]
GO


INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoLmaxProCreditLineUnstNOP', 90000000, 88000000)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoHotspotHtaCreditLineUnstNOP', 45000000, 43000000)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoHotspotHtfCreditLineUnstNOP', 0, 0)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoHotspotHt3CreditLineUnstNOP', 0, 0)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoLmaxInstCreditLineUnstNOP', 10000000, 8000000)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoFxallCreditLineUnstNOP', 15000000, 13000000)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'LmaxMrgAct')
		,'LmaxMrgCreditLineUnstNOP', 100000000, 98000000)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoPrimeXmCreditLineUnstNOP', 100000000, 98000000)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoJPMCreditLineUnstNOP', 50000000, 48000000)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoGLSCreditLineUnstNOP', 50000000, 48000000)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoCZBCreditLineUnstNOP', 50000000, 48000000)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoHotspotH4tCreditLineUnstNOP', 50000000, 48000000)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoHotspotH4mtCreditLineUnstNOP', 50000000, 48000000)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoFc1CreditLineUnstNOP', 10000000, 8000000)
		
INSERT INTO [dbo].[PrimeBrokerCreditLine]
		([PrimeBrokerId]
		,[CreditLineName]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO])
     VALUES
		((SELECT [PrimeBrokerId] FROM [dbo].[PrimeBroker] WHERE [PrimeBrokerName] = 'SaxoBank')
		,'SaxoFs1CreditLineUnstNOP', 90000000, 88000000)
GO	   
		   

		
INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'CZB'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoCZBCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FA1'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoFxallCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FC1'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoFc1CreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FS1'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoFs1CreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'GLS'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoGLSCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'H4M'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoHotspotH4mtCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'H4T'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoHotspotH4tCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HT3'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoHotspotHt3CreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTA'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoHotspotHtaCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTF'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoHotspotHtfCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'JPM'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoJPMCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L01'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L02'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L03'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L04'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L05'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L06'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L07'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L08'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L09'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L10'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L11'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L12'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L13'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L14'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L15'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L16'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L17'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L18'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L19'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'L20'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LGA'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'LmaxMrgCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LGC'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'LmaxMrgCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LM2'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxInstCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LX1'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoLmaxProCreditLineUnstNOP'))

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping] ([CounterpartyId], [CreditLineId])
VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'PX1'), (SELECT [CreditLineId] FROM [dbo].[PrimeBrokerCreditLine] WHERE [CreditLineName] = 'SaxoPrimeXmCreditLineUnstNOP'))
GO
		   
		   
		   


CREATE TABLE [dbo].[LastCurrentlyUnsettledSettlementDate](
	[SettlementDate] [Date] NOT NULL,
	[CurrencyId] [smallint] NULL
) ON [primary]
GO

ALTER TABLE [dbo].[LastCurrentlyUnsettledSettlementDate]  WITH CHECK ADD  CONSTRAINT [LastCurrentlyUnsettledSettlementDate_Currency] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currency_Internal] ([CurrencyID])
GO
ALTER TABLE [dbo].[LastCurrentlyUnsettledSettlementDate] CHECK CONSTRAINT [LastCurrentlyUnsettledSettlementDate_Currency]
GO

INSERT INTO [dbo].[LastCurrentlyUnsettledSettlementDate] ([SettlementDate], [CurrencyId]) 
    VALUES (DATEADD(DAY, -10, GETUTCDATE()), NULL)
INSERT INTO [dbo].[LastCurrentlyUnsettledSettlementDate] ([SettlementDate], [CurrencyId]) 
    VALUES (DATEADD(DAY, -10, GETUTCDATE()), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'NZD'))
GO

CREATE TABLE [dbo].[DealExternalExecutedUnsettled](
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[SettlementDateLocalMkt] [date] NOT NULL,
	[SizeBasePol] [decimal](18, 2) NOT NULL,
	[SizeTermPol] [decimal](18, 4) NOT NULL
) ON [Integrator_DailyData]
GO

ALTER TABLE [dbo].[DealExternalExecutedUnsettled]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecutedUnsettled_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[DealExternalExecutedUnsettled] CHECK CONSTRAINT [FK_DealExternalExecutedUnsettled_Counterparty]
GO

ALTER TABLE [dbo].[DealExternalExecutedUnsettled]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecutedUnsettled_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[DealExternalExecutedUnsettled] CHECK CONSTRAINT [FK_DealExternalExecutedUnsettled_Symbol]
GO


CREATE CLUSTERED INDEX [IX_DealExternalExecutedUnsettled_CounterpartySymbol] ON [dbo].[DealExternalExecutedUnsettled]
(
	[SettlementDateLocalMkt] ASC,
	[CounterpartyId] ASC,
	[SymbolId] ASC
) ON [Integrator_DailyData]
GO


CREATE TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine](
	[PrimeBrokerName] [varchar](10) NOT NULL,
	[CreditLineName] [varchar](255) NOT NULL,
	[UnsettledNopUsd] [decimal](18, 2) NOT NULL,
	[MaxUnsettledNopUsd] [decimal](18, 2) NOT NULL,
	[MaxUnsettledNopUsdToTurnGFO] [decimal](18, 2) NOT NULL
) ON [Integrator_DailyData]
GO


INSERT INTO [dbo].[TableChangeLog] ([TableName], [LastUpdatedUtc]) VALUES ('SettlementStatistics', '1900-01-01')
GO


CREATE PROCEDURE [UpdateSettlementStatisticsPerCreditLine_SP]
(
	@Force BIT = 0
)
AS
BEGIN

	UPDATE 
		[dbo].[TableChangeLog] 
	SET 
		[LastUpdatedUtc] = GETUTCDATE() 
	WHERE 
		[TableName] = 'SettlementStatistics' 
		AND 
		(@Force = 1 OR [LastUpdatedUtc] < DATEADD(SECOND, -2, GETUTCDATE()))

	IF @@ROWCOUNT > 0
	BEGIN

		DECLARE @temp_NOPs_pol TABLE
		(
			CreditLineId tinyint,
			BaseCurrencyId smallint,
			QuoteCurrencyId smallint,
			NopBasePol decimal(18,2),
			NopTermPol decimal(18,4)
		)
		
		DECLARE @temp_result TABLE
		(
			[PrimeBrokerName] [varchar](10),
			[CreditLineName] [varchar](255),
			[UnsettledNopUsd] [decimal](18, 2),
			[MaxUnsettledNopUsd] [decimal](18, 2),
			[MaxUnsettledNopUsdToTurnGFO] [decimal](18, 2)
		)

		INSERT INTO
			@temp_NOPs_pol
			(
				CreditLineId,
				BaseCurrencyId,
				QuoteCurrencyId,
				NopBasePol,
				NopTermPol
			)
		SELECT 
			[CreditLineId]
			,MAX(sym.BaseCurrencyId) AS BaseCurrencyId
			,MAX(sym.QuoteCurrencyId) AS QuoteCurrencyId
			,SUM([SizeBasePol]) AS [NopBasePol]
			,SUM([SizeTermPol]) AS [NopTermPol]
			--,COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate]) AS [SettlementDate]
		FROM
			[dbo].[DealExternalExecutedUnsettled] unsettled
			INNER JOIN [dbo].[PrimeBrokerCreditLines_CptMapping] creditLineMap ON unsettled.[CounterpartyId] = creditLineMap.[CounterpartyId]
			INNER JOIN Symbol sym ON sym.ID = unsettled.[SymbolId]
			LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateCurrencyMatch 
						ON settlDateCurrencyMatch.CurrencyID = sym.BaseCurrencyId OR settlDateCurrencyMatch.CurrencyID = sym.QuoteCurrencyId
			LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateOtherCurrencies 
						ON settlDateOtherCurrencies.CurrencyID IS NULL					
		WHERE
			[SettlementDateLocalMkt] >= COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate])
		GROUP BY
			[CreditLineId],
			[SymbolId]

		
		INSERT INTO
			@temp_result
			(
				[PrimeBrokerName],
				[CreditLineName],
				[UnsettledNopUsd],
				[MaxUnsettledNopUsd],
				[MaxUnsettledNopUsdToTurnGFO]
			)
		SELECT
			primeBroker.PrimeBrokerName,
			creditLine.[CreditLineName],
			ISNULL(nopsRollup.NopAbsInUSD, 0),
			creditLine.[MaxUnsettledNopUsd],
			creditLine.[MaxUnsettledNopUsdToTurnGFO]
		FROM
			(
			SELECT
				CreditLineId,
				SUM(ABS(NopPolInUSD))/2 AS NopAbsInUSD
			FROM
				(
				SELECT
					CreditLineId,
					nopsUnaggreggated.CurrencyId,
					SUM(NopPol * ccy.LastKnownUsdConversionMultiplier) AS NopPolInUSD
				FROM
					(
					SELECT
						CreditLineId,
						BaseCurrencyId AS CurrencyId,
						NopBasePol AS NopPol
					FROM
						@temp_NOPs_pol basePos

					UNION ALL

					SELECT
						CreditLineId,
						QuoteCurrencyId AS CurrencyId,
						NopTermPol AS NopPol
					FROM
						@temp_NOPs_pol basePos
					) nopsUnaggreggated
					INNER JOIN Currency ccy ON ccy.CurrencyID = nopsUnaggreggated.CurrencyId
				GROUP BY
					CreditLineId,
					nopsUnaggreggated.CurrencyId
				)nopsAggreggated
			GROUP BY
				CreditLineId
			) nopsRollup
			RIGHT JOIN [dbo].[PrimeBrokerCreditLine] creditLine ON creditLine.CreditLineId = nopsRollup.CreditLineId
			LEFT JOIN [dbo].[PrimeBroker] primeBroker ON creditLine.[PrimeBrokerId] = primeBroker.[PrimeBrokerId]
			
			
		BEGIN TRANSACTION
			TRUNCATE TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine]
		
			INSERT INTO
				[dbo].[PersistedSettlementStatisticsPerCreditLine]
				(
					[PrimeBrokerName],
					[CreditLineName],
					[UnsettledNopUsd],
					[MaxUnsettledNopUsd],
					[MaxUnsettledNopUsdToTurnGFO]
				)
			SELECT
				[PrimeBrokerName],
				[CreditLineName],
				[UnsettledNopUsd],
				[MaxUnsettledNopUsd],
				[MaxUnsettledNopUsdToTurnGFO]
			FROM
				@temp_result
		COMMIT	
		
		IF EXISTS(SELECT * FROM @temp_result WHERE [MaxUnsettledNopUsdToTurnGFO] > 0 AND [UnsettledNopUsd] > [MaxUnsettledNopUsdToTurnGFO])
		BEGIN
		
			DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()

			DECLARE @GoFlatOnlyOn BIT
			SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]

			DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn
			
			IF @TradingOn = 1
			BEGIN
			
				exec [dbo].[FlipSystemsTradingControl_SP] @GoFlatOnly = 1
			
				DECLARE @msg VARCHAR(MAX) = 'Credit Line(s) unsettled NOP limit is approaching and trading is enabled! Increase unsettled NOP limits or keep GFO to get rid of this message.'

				SELECT
					@msg = @msg + ' ' + [CreditLineName] + '(' + [PrimeBrokerName] + ') Unsettled NOP: ' + CAST([UnsettledNopUsd] AS VARCHAR) + 'USD (MAX: ' + CAST([MaxUnsettledNopUsd] AS VARCHAR) + '). '
				FROM 
					@temp_result
				WHERE 
					[MaxUnsettledNopUsdToTurnGFO] > 0 AND [UnsettledNopUsd] > [MaxUnsettledNopUsdToTurnGFO]

					
				INSERT INTO [dbo].[SystemsTradingControlSchedule]
				   ([IsEnabled]
				   ,[IsDaily]
				   ,[IsWeekly]
				   ,[ScheduleTimeZoneSpecific]
				   ,[TimeZoneCode]
				   ,[ActionId]
				   ,[ScheduleDescription]
				   ,[IsImmediatelyOnce])
			 VALUES
				   (1
					,0
				   ,0
				   ,GETUTCDATE()
				   ,'UTC'
				   ,(SELECT [ActionId] FROM [dbo].[SystemsTradingControlScheduleAction] WHERE [ActionName] = 'LogFatal')
				   ,@msg
				   ,1)	
			END
		END
	END
END 
GO

GRANT EXECUTE ON [dbo].[UpdateSettlementStatisticsPerCreditLine_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO




CREATE PROCEDURE [GetSettlementStatisticsPerCreditLine_Cached_SP]
(
	@Force BIT = 0
)
AS
BEGIN

	exec UpdateSettlementStatisticsPerCreditLine_SP @Force=@Force
	
	SELECT 
		[PrimeBrokerName]
		,[CreditLineName]
		,[UnsettledNopUsd]
		,[MaxUnsettledNopUsd]
		,[MaxUnsettledNopUsdToTurnGFO]
	FROM 
		[dbo].[PersistedSettlementStatisticsPerCreditLine]
	ORDER BY
		[PrimeBrokerName] ASC
		,[CreditLineName] ASC

END
GO

GRANT EXECUTE ON [dbo].[GetSettlementStatisticsPerCreditLine_Cached_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


GRANT ALTER ON [dbo].[PersistedSettlementStatisticsPerCreditLine] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[PersistedSettlementStatisticsPerCreditLine] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT ALTER ON [dbo].[PersistedSettlementStatisticsPerCreditLine] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT SELECT ON [dbo].[PersistedSettlementStatisticsPerCreditLine] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE TRIGGER [dbo].[DealExternalExecutedUnsettledInserted] ON [dbo].[DealExternalExecutedUnsettled] AFTER INSERT
AS
BEGIN
	-- commit the internal insert transaction to prevent deadlocks
	COMMIT TRANSACTION
	exec [dbo].[UpdateSettlementStatisticsPerCreditLine_SP] @Force=1
	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION
END
GO


CREATE PROCEDURE [Daily_PurgeAndRollupUnsettledDeals_SP]
AS
BEGIN

	DECLARE @temp_unsettledDeals TABLE
	(
		[CounterpartyId] [tinyint],
		[SymbolId] [tinyint],
		[SettlementDateLocalMkt] [date],
		[SizeBasePol] [decimal](18, 2),
		[SizeTermPol] [decimal](18, 4)
	)

	INSERT INTO
		@temp_unsettledDeals
		(
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SizeBasePol],
			[SizeTermPol]
		)
	SELECT 
		[CounterpartyId]
		,[SymbolId]
		,[SettlementDateLocalMkt]
		,SUM([SizeBasePol]) AS [NopBasePol]
		,SUM([SizeTermPol]) AS [NopTermPol]
	FROM
		[dbo].[DealExternalExecutedUnsettled] unsettled
		INNER JOIN Symbol sym ON sym.ID = unsettled.[SymbolId]
		LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateCurrencyMatch 
					ON settlDateCurrencyMatch.CurrencyID = sym.BaseCurrencyId OR settlDateCurrencyMatch.CurrencyID = sym.QuoteCurrencyId
		LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateOtherCurrencies 
					ON settlDateOtherCurrencies.CurrencyID IS NULL					
	WHERE
		[SettlementDateLocalMkt] >= COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate])
	GROUP BY
		[SettlementDateLocalMkt],
		[CounterpartyId],
		[SymbolId]
		
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
	
		INSERT INTO
			[dbo].[DealExternalExecutedUnsettled]
			(
				[CounterpartyId],
				[SymbolId],
				[SettlementDateLocalMkt],
				[SizeBasePol],
				[SizeTermPol]
			)
		SELECT
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SizeBasePol],
			[SizeTermPol]
		FROM
			@temp_unsettledDeals
	COMMIT	
END
GO


CREATE PROCEDURE [ForceRefreshAllUnsettledDeals_SP]
AS
BEGIN

	DECLARE @temp_unsettledDeals TABLE
	(
		[CounterpartyId] [tinyint],
		[SymbolId] [tinyint],
		[SettlementDateLocalMkt] [date],
		[SizeBasePol] [decimal](18, 2),
		[SizeTermPol] [decimal](18, 4)
	)

	INSERT INTO
		@temp_unsettledDeals
		(
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SizeBasePol],
			[SizeTermPol]
		)
	SELECT
		[CounterpartyId],
		[SymbolId],
		[ValueDate],
		SUM([SizeBasePol]),
		SUM([SizeTermPol])
	FROM
		(
		SELECT 
			deal.[CounterpartyId]
			,deal.[SymbolId]
			,deal.[ValueDateCounterpartySuppliedLocMktDate] AS [ValueDate]
			,deal.[AmountBasePolExecuted] AS [SizeBasePol]
			,-deal.[AmountBasePolExecuted]*deal.[Price] AS [SizeTermPol]
		FROM 
			[dbo].[DealExternalExecuted] deal
			INNER JOIN Symbol sym ON sym.ID = deal.[SymbolId]
			LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateCurrencyMatch 
						ON settlDateCurrencyMatch.CurrencyID = sym.BaseCurrencyId OR settlDateCurrencyMatch.CurrencyID = sym.QuoteCurrencyId
			LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateOtherCurrencies 
						ON settlDateOtherCurrencies.CurrencyID IS NULL
		WHERE
			IntegratorReceivedExecutionReportUtc > DATEADD(DAY, -10, GETUTCDATE())
			AND
			[ValueDateCounterpartySuppliedLocMktDate] >= COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate])

		UNION ALL

		SELECT 
			[CounterpartyId]
			,[SymbolId]
			,[ValueDate]
			,[IntegratorExecutedAmountBasePol] AS [SizeBasePol]
			,-[IntegratorExecutedAmountBasePol]*[CounterpartyRequestedPrice] AS [SizeTermPol]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]
		WHERE
			[IntegratorExecutedAmountBasePol] > 0

		UNION ALL

		SELECT 
			[CounterpartyId]
			,[SymbolId]
			,[ValueDate]
			,[IntegratorExecutedAmountBasePol] AS [SizeBasePol]
			,-[IntegratorExecutedAmountBasePol]*[CounterpartyRequestedPrice] AS [SizeTermPol]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Archive] deal
			INNER JOIN Symbol sym ON sym.ID = deal.[SymbolId]
			LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateCurrencyMatch 
						ON settlDateCurrencyMatch.CurrencyID = sym.BaseCurrencyId OR settlDateCurrencyMatch.CurrencyID = sym.QuoteCurrencyId
			LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateOtherCurrencies 
						ON settlDateOtherCurrencies.CurrencyID IS NULL
		WHERE
			[IntegratorReceivedExternalOrderUtc] > DATEADD(DAY, -10, GETUTCDATE())
			AND
			[IntegratorExecutedAmountBasePol] > 0
			AND
			[ValueDate] >= COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate])
		) allUnsettledDeals
	GROUP BY
		[ValueDate],
		[CounterpartyId],
		[SymbolId]

		
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
	
		INSERT INTO
			[dbo].[DealExternalExecutedUnsettled]
			(
				[CounterpartyId],
				[SymbolId],
				[SettlementDateLocalMkt],
				[SizeBasePol],
				[SizeTermPol]
			)
		SELECT
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SizeBasePol],
			[SizeTermPol]
		FROM
			@temp_unsettledDeals
	COMMIT	
END
GO


CREATE PROCEDURE [UpdateRolloverInfo_SP]
(
	@IsNYRollover BIT = 0,
	@IsNZRollover BIT = 0,
	@LastCurrentlyUnsettledSettlementDateForNY DATE,
	@LastCurrentlyUnsettledSettlementDateForNZ DATE
)
AS
BEGIN

	UPDATE [dbo].[LastCurrentlyUnsettledSettlementDate]
	SET 
		[SettlementDate] = @LastCurrentlyUnsettledSettlementDateForNY
	WHERE
		[CurrencyId] IS NULL
		
	UPDATE [dbo].[LastCurrentlyUnsettledSettlementDate]
	SET 
		[SettlementDate] = @LastCurrentlyUnsettledSettlementDateForNZ
	WHERE
		[CurrencyId] IS NOT NULL

END
GO

GRANT EXECUTE ON [dbo].[UpdateRolloverInfo_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 2),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL,
	@SingleFillSystemId int = NULL,
	@MultipleFillsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL,
	@CounterpartyClientId varchar(20) = NULL)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @MultipleFillsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleFillsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external deal for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@SingleFillSystemId, -1)
				,@AmountBasePolExecuted
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc)
		END
		
		INSERT INTO 
		[dbo].[DealExternalExecuted]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolExecuted]
			   ,[AmountTermPolExecutedInUsd]
			   ,[Price]
			   ,[CounterpartyTransactionId]
			   ,[CounterpartySuppliedExecutionTimeStampUtc]
			   ,[ValueDateCounterpartySuppliedLocMktDate]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[InternalTransactionIdentity]
			   ,[FlowSideId]
			   ,[CounterpartyClientId])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolExecuted
			   ,@AmountTermPolExecutedInUsd
			   ,@Price
			   ,@CounterpartyTransactionId
			   ,@CounterpartyExecutionReportTimeStampUtc
			   ,@ValueDateCounterpartySuppliedLocMktDate
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@CounterpartySentExecutionReportUtc
			   ,@InternalDealId
			   ,@FlowSideId
			   ,@CounterpartyClientId)
			   
		INSERT INTO [dbo].[DealExternalExecutedUnsettled]
           ([CounterpartyId]
           ,[SymbolId]
           ,[SettlementDateLocalMkt]
           ,[SizeBasePol]
           ,[SizeTermPol])
		VALUES
           (@CounterpartyId
           ,@SymbolId
           ,@ValueDateCounterpartySuppliedLocMktDate
           ,@AmountBasePolExecuted
           ,-@AmountBasePolExecuted*@Price)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO


ALTER PROCEDURE [dbo].[InsertNewOrderExternalIncomingRejectable_SP]( 
    @ExternalOrderReceived [datetime2](7),
	@ExternalOrderSentUtc [datetime2](7),
	@IntegratorSentExecutionReportUtc [datetime2](7),
	@CounterpartyClientOrderId [varchar](32),
	@CounterpartyClientId [varchar](20),
	@TradingSystemId int = NULL,
	@SourceIntegratorPriceIdentity [varchar](20) = NULL,
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@RequestedFillMinimumBaseAbs decimal(18,2) = NULL,
	@IntegratorExecId [varchar](20) = NULL,
	@CounterpartyExecId [varchar](32) = NULL,
	@ValueDate [date] = NULL,
	@OrderReceivedToExecutionReportSentLatency [time](7),
	@IntegratorExecutedAmountBaseAbs [decimal](18, 2) = NULL,
	@IntegratorRejectedAmountBaseAbs [decimal](18, 2) = NULL,
	@RejectionReason [varchar](32) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WITH(NOLOCK) WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WITH(NOLOCK) WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WITH(NOLOCK) WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @RejectionReason IS NOT NULL
	BEGIN
		SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
		IF @@ROWCOUNT <> 1
		BEGIN
			INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
			SET @RejectionReasonId = @@IDENTITY
		END
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @IntegratorExecutedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,COALESCE(@IntegratorExecId, @CounterpartyExecId)
				,@ExternalOrderReceived)
		END
		ELSE IF @IntegratorRejectedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,ISNULL(@RejectionReason, '<NONE>')
				,COALESCE(@IntegratorExecId, @CounterpartyExecId)
				,@RejectionDirectionId
				,@ExternalOrderReceived)
		END

		INSERT INTO 
			[dbo].[OrderExternalIncomingRejectable_Daily]
			   ([IntegratorReceivedExternalOrderUtc]
			   ,[CounterpartySentExternalOrderUtc]
			   ,[IntegratorSentExecutionReportUtc]
			   ,[CounterpartyClientOrderId]
			   ,[CounterpartyClientId]
			   ,[TradingSystemId]
			   ,[SourceIntegratorPriceIdentity]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorDealDirectionId]
			   ,[CounterpartyRequestedPrice]
			   ,[CounterpartyRequestedAmountBasePol]
			   ,[CounterpartyRequestedFillMinimumBaseAbs]
			   ,[IntegratorExecutionId]
			   ,[CounterpartyExecutionId]
			   ,[ValueDate]
			   ,[OrderReceivedToExecutionReportSentLatency ]
			   ,[IntegratorExecutedAmountBasePol]
			   ,[IntegratorRejectedAmountBasePol]
			   ,[IntegratorRejectionReasonId])
		 VALUES
			   (@ExternalOrderReceived
			   ,@ExternalOrderSentUtc
			   ,@IntegratorSentExecutionReportUtc
			   ,@CounterpartyClientOrderId
			   ,@CounterpartyClientId
			   ,@TradingSystemId
			   ,@SourceIntegratorPriceIdentity
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@DealDirectionId
			   ,@RequestedPrice
			   ,CASE WHEN @DealDirectionId = 1 THEN -@RequestedAmountBaseAbs ELSE @RequestedAmountBaseAbs END
			   ,@RequestedFillMinimumBaseAbs
			   ,@IntegratorExecId
			   ,@CounterpartyExecId
			   ,@ValueDate
			   ,@OrderReceivedToExecutionReportSentLatency 
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
		       ,@RejectionReasonId)
			   
		IF @ValueDate IS NOT NULL AND @IntegratorExecutedAmountBaseAbs != 0
		BEGIN
			INSERT INTO [dbo].[DealExternalExecutedUnsettled]
			   ([CounterpartyId]
			   ,[SymbolId]
			   ,[SettlementDateLocalMkt]
			   ,[SizeBasePol]
			   ,[SizeTermPol])
			VALUES
			   (@CounterpartyId
			   ,@SymbolId
			   ,@ValueDate
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN @IntegratorExecutedAmountBaseAbs ELSE -@IntegratorExecutedAmountBaseAbs END * @RequestedPrice)
		END
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH	   
END
GO

ALTER PROCEDURE [dbo].[RejectExecutedOrderExternalIncomingRejectable_SP]( 
	@IntegratorExecId  [varchar](20) = NULL,
	@CounterpartyExecId  [varchar](20) = NULL,
	@RejectionReason [varchar](20) = NULL
	)
AS
BEGIN
	DECLARE @OriginallyExecutedAmount decimal(18,2)
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT 
		@OriginallyExecutedAmount = [IntegratorExecutedAmountBasePol] 
	FROM 
		[dbo].[OrderExternalIncomingRejectable_Daily]
	WHERE 
		[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) 
		AND ([IntegratorExecutionId] = @IntegratorExecId OR [CounterpartyExecutionId] = @CounterpartyExecId)

	IF @OriginallyExecutedAmount IS NOT NULL
	BEGIN
	
		IF @RejectionReason IS NOT NULL
		BEGIN
			SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
			IF @@ROWCOUNT <> 1
			BEGIN
				INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
				SET @RejectionReasonId = @@IDENTITY
			END
		END
	
		BEGIN TRY

		BEGIN TRANSACTION;
	
			UPDATE
				[dbo].[OrderExternalIncomingRejectable_Daily]
			SET
				[IntegratorRejectedAmountBasePol] += @OriginallyExecutedAmount,
				[IntegratorExecutedAmountBasePol] = 0,
				[IntegratorRejectionReasonId] = @RejectionReasonId
			WHERE 
				[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) 
				AND ([IntegratorExecutionId] = @IntegratorExecId OR [CounterpartyExecutionId] = @CounterpartyExecId)

				
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			SELECT
				[TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,ISNULL(@RejectionReason, '<NONE>')
				,[InternalTransactionIdentity]
				,@RejectionDirectionId
				,[DealTimeUtc]
			FROM
				[dbo].[TradingSystemDealsExecuted_Daily]
			WHERE
				[InternalTransactionIdentity] = COALESCE(@IntegratorExecId, @CounterpartyExecId)
					
			DELETE FROM
				[dbo].[TradingSystemDealsExecuted_Daily]
			WHERE
				[InternalTransactionIdentity] = COALESCE(@IntegratorExecId, @CounterpartyExecId)
			
			--delete condition (inner join)
			DELETE TOP (1) unsettled FROM
				[dbo].[DealExternalExecutedUnsettled] unsettled
				INNER JOIN [dbo].[OrderExternalIncomingRejectable_Daily] deals
			ON
				unsettled.[CounterpartyId] = deals.[CounterpartyId] AND
				unsettled.[SymbolId] = deals.[SymbolId] AND
				unsettled.[SettlementDateLocalMkt] = deals.[ValueDate] AND
				unsettled.[SizeBasePol] = deals.[IntegratorExecutedAmountBasePol] AND
				unsettled.[SizeTermPol] = -deals.[IntegratorExecutedAmountBasePol]*deals.[CounterpartyRequestedPrice]
			WHERE
				[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) 
				AND ([IntegratorExecutionId] = @IntegratorExecId OR [CounterpartyExecutionId] = @CounterpartyExecId)

			
		COMMIT TRANSACTION;
		
		SELECT 
			[IntegratorReceivedExternalOrderUtc]
			,[CounterpartySentExternalOrderUtc]
			,[CounterpartyClientOrderId]
			,[TradingSystemId]
			,[SourceIntegratorPriceIdentity]
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.[Name] as Symbol
			,direction.[Name] as DealDirection
			,[CounterpartyRequestedPrice]
			,@OriginallyExecutedAmount AS OriginallyExecutedAmountBasePol
			,[IntegratorExecutionId]
			,[CounterpartyExecutionId]
			,[ValueDate]
		FROM 
			[dbo].[OrderExternalIncomingRejectable_Daily] deals
			INNER JOIN [dbo].[Symbol] symbol ON deals.[SymbolId] = symbol.[Id]
			INNER JOIN [dbo].[Counterparty] counterparty ON deals.[CounterpartyId] = counterparty.[CounterpartyId]
			INNER JOIN [dbo].[DealDirection] direction ON deals.[IntegratorDealDirectionId] = direction.Id
		WHERE 
			[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) 
			AND ([IntegratorExecutionId] = @IntegratorExecId OR [CounterpartyExecutionId] = @CounterpartyExecId)
		
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION;
			THROW
		END CATCH
		
	END
END
GO

CREATE TABLE [dbo].[DealExternalExecutedUnexpected](
	[IntegratorReceivedExecutionUtc] [datetime2](7) NOT NULL,
	[ExternalClientOrderId] [nvarchar](32) NOT NULL,
	[CounterpartySentExecutionUtc] [datetime2](7) NULL,
	[CounterpartyTransactionTimeUtc] [datetime2](7) NULL,
	[CounterpartySettlementTimeLocal] [date] NULL,
	[CounterpartyTransactionId] [nvarchar](64) NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[IntegratorDealDirectionId] [bit] NOT NULL,
	[FilledAmountBaseAbs] [decimal](18, 2) NULL,
	[FilledPrice] [decimal](18, 9) NULL
) ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [IX_DealExternalExecutedUnexpected_IntegratorReceivedExecutionUtc] ON [dbo].[DealExternalExecutedUnexpected]
(
	[IntegratorReceivedExecutionUtc] ASC
)ON [PRIMARY]
GO

CREATE PROCEDURE [dbo].[InsertDealExternalExecutedUnexpected_SP] (
	@IntegratorReceivedExecutionUtc [datetime2](7)
	,@ExternalClientOrderId [nvarchar](32)
	,@CounterpartySentExecutionUtc [datetime2](7)
	,@CounterpartyTransactionTimeUtc [datetime2](7)
	,@CounterpartySettlementTimeLocal [date]
	,@CounterpartyTransactionId [nvarchar](64)
	,@CounterpartyCode [char](3)
	,@Symbol NCHAR(7)
	,@DealDirection NVARCHAR(4)
	,@FilledAmountBaseAbs [decimal](18, 2)
	,@FilledPrice [decimal](18, 9)
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END

	INSERT INTO 
		[dbo].[DealExternalExecutedUnexpected]
		   ([IntegratorReceivedExecutionUtc]
		   ,[ExternalClientOrderId]
		   ,[CounterpartySentExecutionUtc]
		   ,[CounterpartyTransactionTimeUtc]
		   ,[CounterpartySettlementTimeLocal]
		   ,[CounterpartyTransactionId]
		   ,[CounterpartyId]
		   ,[SymbolId]
		   ,[IntegratorDealDirectionId]
		   ,[FilledAmountBaseAbs]
		   ,[FilledPrice])
	VALUES
	   (@IntegratorReceivedExecutionUtc
	    ,@ExternalClientOrderId
		,@CounterpartySentExecutionUtc
		,@CounterpartyTransactionTimeUtc
		,@CounterpartySettlementTimeLocal
		,@CounterpartyTransactionId
		,@CounterpartyId
		,@SymbolId
		,@DealDirectionId
		,@FilledAmountBaseAbs
		,@FilledPrice)

END
GO

GRANT EXECUTE ON [dbo].[InsertDealExternalExecutedUnexpected_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetTodayUnexpectedExecutions_SP] 
AS
BEGIN
	SELECT 
		[IntegratorReceivedExecutionUtc]
		,[ExternalClientOrderId]
		,[CounterpartySentExecutionUtc]
		,[CounterpartyTransactionTimeUtc]
		,[CounterpartySettlementTimeLocal]
		,[CounterpartyTransactionId]
		,ctp.CounterpartyCode AS CounterpartyCode
		,symbol.Name AS Symbol
		,direction.Name AS Direction
		,[FilledAmountBaseAbs]
		,[FilledPrice]
		,CAST(CASE WHEN ignoredOrder.ExternalOrderLabelForCounterparty IS NOT NULL THEN 1 ELSE 0 END AS BIT) AS WasPreviouslyBroken 
	FROM 
		[dbo].[DealExternalExecutedUnexpected] deal
		INNER JOIN [dbo].[Counterparty] ctp on ctp.CounterpartyId = deal.CounterpartyId
		INNER JOIN [dbo].Symbol symbol on symbol.Id = deal.SymbolId
		INNER JOIN [dbo].[DealDirection] direction on direction.Id = deal.IntegratorDealDirectionId
		LEFT JOIN [dbo].[OrderExternalIgnored] ignoredOrder 
			ON ignoredOrder.[IntegratorSentExternalOrderUtc] > CAST(GETUTCDATE() AS DATE)
			   AND deal.[ExternalClientOrderId] = ignoredOrder.ExternalOrderLabelForCounterparty
	WHERE
		[IntegratorReceivedExecutionUtc] > CAST(GETUTCDATE() AS DATE)
END
GO

GRANT EXECUTE ON [dbo].[GetTodayUnexpectedExecutions_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

USE msdb;
GO

EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'PurgeUnsettledDeals',
    @subsystem = N'TSQL',
    @command = N'exec Daily_PurgeAndRollupUnsettledDeals_SP',
	@database_name = N'Integrator',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO

--MANUALY reaorder the steps - there is no other documented way than doing this through SSMS


INSERT INTO [dbo].[TradingSystemType_Internal] ([CounterpartyId], [SystemName], [Enabled])
   VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LX1'), 'SmartCross', 1)
GO

--ROLLBACK TRANSACTION AAA
--GO
--COMMIT TRANSACTION AAA
--GO