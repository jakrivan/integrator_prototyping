

-- View
CREATE VIEW [dbo].[DealExternalExecuted_View]
AS
SELECT
	[OrderId]
	,[ExecutionTimeStampUtc]
	,[SymbolId]
	,[DealDirectionId]
	,[ExecutedAmountBaseAbs]
	,[ExecutedAmountBasePol]
	,[Price]
FROM [dbo].[ExecutedExternalDeals]
WHERE [ExecutionTimeStampUtc] < '2013-07-29 14:30:00.000'

  UNION ALL

SELECT 
	[IntegratorExternalOrderPrivateId] AS [OrderId]
	,[IntegratorReceivedExecutionReportUtc] AS [ExecutionTimeStampUtc]
	,[SymbolId]
	,[DealDirectionId]
	,ABS([AmountBasePolExecuted]) AS [ExecutedAmountBaseAbs]
	,[AmountBasePolExecuted] AS [ExecutedAmountBasePol]
	,[Price]
FROM [dbo].[DealExternalExecuted]
WHERE [IntegratorReceivedExecutionReportUtc] > '2013-07-29 14:30:00.000'
GO


CREATE PROCEDURE [dbo].[GetPolarizedNopPerPair_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN
	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[ExecutedAmountBasePol]) AS PolarizedNop
	FROM 
		[dbo].[DealExternalExecuted_View] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[ExecutionTimeStampUtc] BETWEEN @StartTime AND @EndTime
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]

END
GO

GRANT EXECUTE ON [dbo].[GetPolarizedNopPerPair_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


ALTER TABLE [dbo].[DealExternalExecuted] ADD [QuoteReceivedToOrderSentInternalLatency] [time](7) NULL
GO

ALTER TABLE [dbo].[DealExternalRejected] ADD [QuoteReceivedToOrderSentInternalLatency] [time](7) NULL
GO

ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 0),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7) = NULL,
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END

	INSERT INTO 
	[dbo].[DealExternalExecuted]
           ([IntegratorReceivedExecutionReportUtc]
           ,[CounterpartyId]
           ,[SymbolId]
           ,[IntegratorExternalOrderPrivateId]
           ,[DealDirectionId]
           ,[AmountBasePolExecuted]
		   ,[AmountTermPolExecutedInUsd]
           ,[Price]
           ,[CounterpartyTransactionId]
           ,[CounterpartySuppliedExecutionTimeStampUtc]
           ,[ValueDateCounterpartySuppliedLocMktDate]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[CounterpartySentExecutionReportUtc]
		   ,[InternalTransactionIdentity]
		   ,[FlowSideId])
     VALUES
           (@ExecutionReportReceivedUtc
           ,@CounterpartyId
           ,@SymbolId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolExecuted
		   ,@AmountTermPolExecutedInUsd
           ,@Price
           ,@CounterpartyTransactionId
           ,@CounterpartyExecutionReportTimeStampUtc
           ,@ValueDateCounterpartySuppliedLocMktDate
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@QuoteReceivedToOrderSentInternalLatency
		   ,@CounterpartySentExecutionReportUtc
		   ,@InternalDealId
		   ,@FlowSideId)
END
GO

ALTER PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 0),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7) = NULL,
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[DealExternalRejected]
           ([IntegratorReceivedExecutionReportUtc]
           ,[SymbolId]
           ,[CounterpartyId]
           ,[IntegratorExternalOrderPrivateId]
		   ,[DealDirectionId]
           ,[AmountBasePolRejected]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[QuoteReceivedToOrderSentInternalLatency]
           ,[RejectionReason]
		   ,[CounterpartySentExecutionReportUtc])
     VALUES
           (@ExecutionReportReceivedUtc
		   ,@SymbolId
           ,@CounterpartyId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolRejected
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@QuoteReceivedToOrderSentInternalLatency
		   ,@RejectionReason
		   ,@CounterpartySentExecutionReportUtc)
END
GO



ALTER PROCEDURE [dbo].[CreateDailyDealsView_SP] 
AS
BEGIN

	DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)
	DECLARE @YesterdayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), DATEADD(DAY, -1, GETUTCDATE()), 101)

	DECLARE @Query NVARCHAR(MAX) = N'
	CREATE VIEW [DealExternalExecutedDailyView_' + @TodayLiteral + ']
	WITH SCHEMABINDING 
	AS 
	SELECT 
		deal.[IntegratorReceivedExecutionReportUtc]
		,deal.[CounterpartyId]
		,deal.[SymbolId]
		,deal.[IntegratorExternalOrderPrivateId]
		,deal.[AmountBasePolExecuted]
		,deal.[Price]
		,CAST(
			(
				(datepart(HOUR, deal.QuoteReceivedToOrderSentInternalLatency)*60 + datepart(MINUTE, deal.QuoteReceivedToOrderSentInternalLatency))*60 
				+ datepart(SECOND, deal.QuoteReceivedToOrderSentInternalLatency)
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, deal.QuoteReceivedToOrderSentInternalLatency) 
		   AS IntegratorLatencyNanoseconds
		,CAST(
			(
				(datepart(HOUR, deal.[OrderSentToExecutionReportReceivedLatency])*60 + datepart(MINUTE, deal.[OrderSentToExecutionReportReceivedLatency]))*60 
				+ datepart(SECOND, deal.[OrderSentToExecutionReportReceivedLatency])
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, deal.[OrderSentToExecutionReportReceivedLatency]) 
		   AS CounterpartyLatencyNanoseconds
		,deal.[AmountTermPolExecutedInUsd]
		,deal.[DatePartIntegratorReceivedExecutionReportUtc]
	FROM 
		[dbo].[DealExternalExecuted] deal
	WHERE
		DatePartIntegratorReceivedExecutionReportUtc = CONVERT(DATE,''' + @TodayLiteral + ''', 101)'
		
		
	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE UNIQUE CLUSTERED INDEX [IX_ReceivedUtc_OrderId] ON [dbo].[DealExternalExecutedDailyView_' + @TodayLiteral + ']
	(
		[IntegratorReceivedExecutionReportUtc] ASC,
		[IntegratorExternalOrderPrivateId] ASC
	)ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalExecutedDailyView_' + @TodayLiteral + ']
	(
		[DatePartIntegratorReceivedExecutionReportUtc] ASC,
		[CounterpartyId] ASC,
		[SymbolId] ASC
	)
	INCLUDE 
	(
		[AmountBasePolExecuted],
		[Price],
		[AmountTermPolExecutedInUsd],
		[IntegratorLatencyNanoseconds],
		[CounterpartyLatencyNanoseconds])
	ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	IF EXISTS(SELECT * FROM  sys.synonyms s WHERE s.name = 'DealExternalExecuted_DailyView')
	BEGIN
		DROP SYNONYM DealExternalExecuted_DailyView
	END

	SET @Query = N'
	CREATE SYNONYM DealExternalExecuted_DailyView FOR [DealExternalExecutedDailyView_' + @TodayLiteral + ']'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	IF EXISTS(select * FROM sys.views where name = ''DealExternalExecutedDailyView_' + @YesterdayLiteral + ''')
	BEGIN
		DROP VIEW [DealExternalExecutedDailyView_' + @YesterdayLiteral + ']
	END
	'

	EXECUTE sp_executesql @Query

END
GO

ALTER PROCEDURE [dbo].[CreateDailyRejectionsView_SP] 
AS
BEGIN

	DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)
	DECLARE @YesterdayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), DATEADD(DAY, -1, GETUTCDATE()), 101)

	DECLARE @Query NVARCHAR(MAX) = N'
	CREATE VIEW [DealExternalRejectedDailyView_' + @TodayLiteral + ']
	WITH SCHEMABINDING 
	AS 
	SELECT 
		   reject.[IntegratorReceivedExecutionReportUtc]
		   ,reject.[IntegratorExternalOrderPrivateId]
		  ,reject.[CounterpartyId]
		  ,reject.[DatePartIntegratorReceivedExecutionReportUtc]
		  ,CAST(
			(
				(datepart(HOUR, reject.QuoteReceivedToOrderSentInternalLatency)*60 + datepart(MINUTE, reject.QuoteReceivedToOrderSentInternalLatency))*60 
				+ datepart(SECOND, reject.QuoteReceivedToOrderSentInternalLatency)
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, reject.QuoteReceivedToOrderSentInternalLatency) 
		   AS IntegratorLatencyNanoseconds
	FROM 
		[dbo].[DealExternalRejected] reject
	WHERE
		DatePartIntegratorReceivedExecutionReportUtc = CONVERT(DATE,''' + @TodayLiteral + ''', 101)'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE UNIQUE CLUSTERED INDEX [IX_ReceivedUtc_OrderId] ON [dbo].[DealExternalRejectedDailyView_' + @TodayLiteral + ']
	(
		[IntegratorReceivedExecutionReportUtc] ASC,
		[IntegratorExternalOrderPrivateId] ASC
	)ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalRejectedDailyView_' + @TodayLiteral + ']
	(
		[DatePartIntegratorReceivedExecutionReportUtc] ASC,
		[CounterpartyId] ASC
	)
	INCLUDE
	(
		[IntegratorLatencyNanoseconds]
	)
	ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	IF EXISTS(SELECT * FROM  sys.synonyms s WHERE s.name = 'DealExternalRejected_DailyView')
	BEGIN
		DROP SYNONYM DealExternalRejected_DailyView
	END

	SET @Query = N'
	CREATE SYNONYM DealExternalRejected_DailyView FOR [DealExternalRejectedDailyView_' + @TodayLiteral + ']'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	IF EXISTS(select * FROM sys.views where name = ''DealExternalRejectedDailyView_' + @YesterdayLiteral + ''')
	BEGIN
		DROP VIEW [DealExternalRejectedDailyView_' + @YesterdayLiteral + ']
	END
	'

	EXECUTE sp_executesql @Query

END
GO


CREATE TABLE [dbo].[SystemsTradingControlSchedule](
	[ScheduleEntryId] [int] IDENTITY(1,1) NOT NULL,
	[IsDaily] [bit] NOT NULL,
	[IsWeekly] [bit] NOT NULL,
	[ScheduleTimeUtc] [datetime2](7) NOT NULL,
	[TurnOnGoFlatOnly] [bit] NOT NULL,
	[ScheduleDescription] [nvarchar](1024) NULL,
	[LastUpdatedUtc] [datetime2](7) NULL,
) ON [PRIMARY]
GO


CREATE CLUSTERED INDEX [IX_SystemsTradingControlSchedule_DailyWeeklyTime] ON [dbo].[SystemsTradingControlSchedule]
(
	[IsDaily] ASC,
	[IsWeekly] ASC,
	[ScheduleTimeUtc] ASC
)
ON [PRIMARY]
GO


CREATE TRIGGER [dbo].[LastUpdatedTrigger_SystemsTradingControlSchedule] ON [dbo].[SystemsTradingControlSchedule]
AFTER INSERT, UPDATE
AS

BEGIN

--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[SystemsTradingControlSchedule] Set [LastUpdatedUtc] = GETUTCDATE() from [dbo].[SystemsTradingControlSchedule] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[ScheduleEntryId] = myAlias.[ScheduleEntryId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'SystemsTradingControlSchedule'

END
GO

INSERT INTO [dbo].[TableChangeLog] ([TableName], [LastUpdatedUtc]) VALUES ('SystemsTradingControlSchedule', '1/1/1900')
GO


CREATE PROCEDURE [dbo].[GetUpdatedSystemsTradingControlSchedule_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'SystemsTradingControlSchedule'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		SELECT
			[ScheduleEntryId]
			,[IsDaily]
			,[IsWeekly]
			,[ScheduleTimeUtc]
			,[TurnOnGoFlatOnly]
			,[ScheduleDescription]
			,[LastUpdatedUtc]
		FROM 
			[dbo].[SystemsTradingControlSchedule]
		--WHERE
		--	sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
		ORDER BY
			[IsDaily],
			[IsWeekly],
			[ScheduleTimeUtc]
	END
END
GO

GRANT EXECUTE ON [dbo].[GetUpdatedSystemsTradingControlSchedule_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetUpdatedSystemsTradingControlSchedule_SP] TO [IntegratorServiceAccount] AS [dbo]
GO



DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd] AS N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RiskManagementSettings" nillable="true" type="RiskManagementSettings" />
  <xs:complexType name="RiskManagementSettings">
    <xs:sequence>
      <xs:element name="ConfigLastUpdated"  nillable="true" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="DisallowTradingOnInstances" type="ArrayOfString" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAndPosition" type="PriceAndPositionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Instance" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersSubmissionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrderMaximumAllowedSizeSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedBaseAbsSizeInUsd" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="PriceAndPositionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumExternalNOPInUSD" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceDeviationCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="OnlinePriceDeviationCheckMaxPriceAge_Minutes" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="Symbols" type="ArrayOfSymbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbol">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="Symbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="Symbol">
    <xs:attribute name="ShortName" type="xs:string" />
    <xs:attribute name="NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
  </xs:complexType>
</xs:schema>'
GO

--Add 
-- <DisallowTradingOnInstances>
--	 <Instance>PRO_Instance_G_NonTrading</Instance>
-- </DisallowTradingOnInstances>

--update production from here on

ALTER TABLE [dbo].[DealExternalExecuted] ADD [IntegratorSentExternalOrderUtc] [datetime2](7) NULL
GO

ALTER TABLE [dbo].[DealExternalRejected] ADD [IntegratorSentExternalOrderUtc] [datetime2](7) NULL
GO


ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 0),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7) = NULL,
	@IntegratorSentExternalOrderUtc DATETIME2(7) = NULL,
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END

	INSERT INTO 
	[dbo].[DealExternalExecuted]
           ([IntegratorReceivedExecutionReportUtc]
           ,[CounterpartyId]
           ,[SymbolId]
           ,[IntegratorExternalOrderPrivateId]
           ,[DealDirectionId]
           ,[AmountBasePolExecuted]
		   ,[AmountTermPolExecutedInUsd]
           ,[Price]
           ,[CounterpartyTransactionId]
           ,[CounterpartySuppliedExecutionTimeStampUtc]
           ,[ValueDateCounterpartySuppliedLocMktDate]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[IntegratorSentExternalOrderUtc]
		   ,[CounterpartySentExecutionReportUtc]
		   ,[InternalTransactionIdentity]
		   ,[FlowSideId])
     VALUES
           (@ExecutionReportReceivedUtc
           ,@CounterpartyId
           ,@SymbolId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolExecuted
		   ,@AmountTermPolExecutedInUsd
           ,@Price
           ,@CounterpartyTransactionId
           ,@CounterpartyExecutionReportTimeStampUtc
           ,@ValueDateCounterpartySuppliedLocMktDate
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@QuoteReceivedToOrderSentInternalLatency
		   ,@IntegratorSentExternalOrderUtc
		   ,@CounterpartySentExecutionReportUtc
		   ,@InternalDealId
		   ,@FlowSideId)
END
GO

ALTER PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 0),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7) = NULL,
	@IntegratorSentExternalOrderUtc DATETIME2(7) = NULL,
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[DealExternalRejected]
           ([IntegratorReceivedExecutionReportUtc]
           ,[SymbolId]
           ,[CounterpartyId]
           ,[IntegratorExternalOrderPrivateId]
		   ,[DealDirectionId]
           ,[AmountBasePolRejected]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[IntegratorSentExternalOrderUtc]
           ,[RejectionReason]
		   ,[CounterpartySentExecutionReportUtc])
     VALUES
           (@ExecutionReportReceivedUtc
		   ,@SymbolId
           ,@CounterpartyId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolRejected
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@QuoteReceivedToOrderSentInternalLatency
		   ,@IntegratorSentExternalOrderUtc
		   ,@RejectionReason
		   ,@CounterpartySentExecutionReportUtc)
END
GO

ALTER TABLE [dbo].[ExecutionReportMessage] ADD [IntegratorSentExternalOrderUtc] [datetime2](7) NULL
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest] ADD [IntegratorSentExternalOrderUtc] [datetime2](7) NULL
GO
ALTER TABLE [dbo].[OrderExternalCancelResult] ADD [IntegratorSentExternalOrderUtc] [datetime2](7) NULL
GO

ALTER PROCEDURE [dbo].[InsertExecutionReportMessage_SP]( 
	@ExecutionReportReceivedUtc [datetime2](7),
	@ExecutionReportStatus [varchar](16),
	@InternalOrderId [nvarchar](45),
	@ExecutionReportCounterpartySentUtc [datetime2](7),
	@IntegratorSentExternalOrderUtc DATETIME2(7) = NULL,
	@RawFIXMessage [nvarchar](1024)
	)
AS
BEGIN

	DECLARE @ExecutionReportStatusId INT

	SELECT @ExecutionReportStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ExecutionReportStatus

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ExecutionReportStatus not found in DB: %s', 16, 2, @ExecutionReportStatus) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[ExecutionReportMessage]
           ([IntegratorReceivedExecutionReportUtc]
           ,[ExecutionReportStatusId]
           ,[IntegratorExternalOrderPrivateId]
           ,[CounterpartySentExecutionReportUtc]
		   ,[IntegratorSentExternalOrderUtc]
           ,[FixMessageReceivedRaw])
     VALUES
           (
		   @ExecutionReportReceivedUtc
           ,@ExecutionReportStatusId
           ,@InternalOrderId
           ,@ExecutionReportCounterpartySentUtc
		   ,@IntegratorSentExternalOrderUtc
           ,@RawFIXMessage
		   )
END
GO

ALTER PROCEDURE [dbo].[InsertOrderExternalCancelRequest_SP]( 
	@IntegratorExternalOrderPrivateId [nvarchar](45),
	@IntegratorSentExternalOrderUtc DATETIME2(7) = NULL,
    @IntegratorSentCancelRequestUtc  DATETIME2(7),
    @OrderExternalCancelTypeName VARCHAR(16),
    @RawFIXMessage [nvarchar](1024)
	)
AS
BEGIN

	DECLARE @OrderExternalCancelTypeId TINYINT

	SELECT @OrderExternalCancelTypeId = [OrderExternalCancelTypeId] FROM [dbo].[OrderExternalCancelType] WHERE [OrderExternalCancelTypeName] = @OrderExternalCancelTypeName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderExternalCancelTypeName not found in DB: %s', 16, 2, @OrderExternalCancelTypeName) WITH SETERROR
	END


	INSERT INTO [dbo].[OrderExternalCancelRequest]
           ([IntegratorExternalOrderPrivateId]
		   ,[IntegratorSentExternalOrderUtc]
           ,[IntegratorSentCancelRequestUtc]
           ,[OrderExternalCancelTypeId]
           ,[FixMessageSentRaw])
     VALUES
           (@IntegratorExternalOrderPrivateId
		   ,@IntegratorSentExternalOrderUtc
           ,@IntegratorSentCancelRequestUtc
           ,@OrderExternalCancelTypeId
           ,@RawFIXMessage)
END
GO

ALTER PROCEDURE [dbo].[InsertOrderExternalCancelResult_SP]( 
	@IntegratorExternalOrderPrivateId [nvarchar](45),
	@IntegratorSentExternalOrderUtc DATETIME2(7) = NULL,
    @CounterpartySentResultUtc  DATETIME2(7),
    @CancelledAmountBasePol decimal(18,0),
    @ResultStatusName VARCHAR(16)
	)
AS
BEGIN

	DECLARE @ResultStatusId TINYINT

	SELECT @ResultStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ResultStatusName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ResultStatusName not found in DB: %s', 16, 2, @ResultStatusName) WITH SETERROR
	END


	INSERT INTO [dbo].[OrderExternalCancelResult]
           ([IntegratorExternalOrderPrivateId]
		   ,[IntegratorSentExternalOrderUtc]
           ,[CounterpartySentResultUtc]
           ,[CanceledAmountBasePol]
           ,[ResultStatusId])
     VALUES
           (@IntegratorExternalOrderPrivateId
		   ,@IntegratorSentExternalOrderUtc
           ,@CounterpartySentResultUtc
           ,@CancelledAmountBasePol
           ,@ResultStatusId)
END
GO


ALTER TABLE [dbo].[SystemsTradingControlSchedule] ADD [IsEnabled] [bit] NULL
GO

UPDATE [dbo].[SystemsTradingControlSchedule] SET [IsEnabled] = 1

ALTER TABLE [SystemsTradingControlSchedule] ALTER COLUMN [IsEnabled] [bit] NOT NULL
GO
	

ALTER PROCEDURE [dbo].[GetUpdatedSystemsTradingControlSchedule_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@IsDailyRecords Bit = NULL,
	@IsWeeklyRecords Bit = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'SystemsTradingControlSchedule'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		SELECT
			[ScheduleEntryId]
			,[IsEnabled]
			,[IsDaily]
			,[IsWeekly]
			,[ScheduleTimeUtc]
			,[TurnOnGoFlatOnly]
			,[ScheduleDescription]
			,[LastUpdatedUtc]
		FROM 
			[dbo].[SystemsTradingControlSchedule]
		WHERE
		--	sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@IsDailyRecords IS NULL OR [IsDaily] = @IsDailyRecords)
			AND (@IsWeeklyRecords IS NULL OR [IsWeekly] = @IsWeeklyRecords)
		ORDER BY
			[IsDaily],
			[IsWeekly],
			[ScheduleTimeUtc]
	END
END
GO

CREATE PROCEDURE [dbo].[SystemsTradingControlScheduleDisableEnableOne_SP] (
	@ScheduleEntryId int,
	@Enable Bit
	)
AS
BEGIN

	UPDATE
		[dbo].[SystemsTradingControlSchedule]
	SET
		IsEnabled = @Enable
	WHERE
		ScheduleEntryId = @ScheduleEntryId
END
GO

GRANT EXECUTE ON [dbo].[SystemsTradingControlScheduleDisableEnableOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[SystemsTradingControlScheduleDeleteOne_SP] (
	@ScheduleEntryId int
	)
AS
BEGIN

	DELETE FROM
		[dbo].[SystemsTradingControlSchedule]
	WHERE
		ScheduleEntryId = @ScheduleEntryId
END
GO

GRANT EXECUTE ON [dbo].[SystemsTradingControlScheduleDeleteOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[SystemsTradingControlScheduleUpdateSingle_SP]
(
	@ScheduleEntryId int,
	@Enable Bit,
	@IsDaily Bit,
	@IsWeekly Bit,
	@ScheduleTimeUtc datetime2(7),
	@TurnOnGoFlatOnly Bit,
	@ScheduleDescription NVARCHAR(1024)
)
AS
BEGIN
	UPDATE 
		[dbo].[SystemsTradingControlSchedule]
	SET
		[IsEnabled] = @Enable
		,[IsDaily] = @IsDaily
		,[IsWeekly] = @IsWeekly
		,[ScheduleTimeUtc] = @ScheduleTimeUtc
		,[TurnOnGoFlatOnly] = @TurnOnGoFlatOnly
		,[ScheduleDescription] = @ScheduleDescription
	WHERE
		[ScheduleEntryId] = @ScheduleEntryId
END
GO

GRANT EXECUTE ON [dbo].[SystemsTradingControlScheduleUpdateSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[SystemsTradingControlScheduleInsertSingle_SP]
(
	@Enable Bit,
	@IsDaily Bit,
	@IsWeekly Bit,
	@ScheduleTimeUtc datetime2(7),
	@TurnOnGoFlatOnly Bit,
	@ScheduleDescription NVARCHAR(1024),
	@ScheduleEntryId int OUTPUT
)
AS
BEGIN
	INSERT INTO [dbo].[SystemsTradingControlSchedule]
           ([IsEnabled]
		   ,[IsDaily]
           ,[IsWeekly]
           ,[ScheduleTimeUtc]
           ,[TurnOnGoFlatOnly]
           ,[ScheduleDescription])
     VALUES
           (@Enable
			,@IsDaily
           ,@IsWeekly
           ,@ScheduleTimeUtc
           ,@TurnOnGoFlatOnly
           ,@ScheduleDescription)
	
	SELECT @ScheduleEntryId = scope_identity();
END
GO

GRANT EXECUTE ON [dbo].[SystemsTradingControlScheduleInsertSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE TRIGGER [dbo].[LastDeletedTrigger_SystemsTradingControlSchedule] ON [dbo].[SystemsTradingControlSchedule]
AFTER DELETE
AS
BEGIN

--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON
UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'SystemsTradingControlSchedule'

END
GO
