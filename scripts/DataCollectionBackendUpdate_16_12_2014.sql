
INSERT INTO [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (5, 'FXCMMM', 'FXCMMM')

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT 5
      ,[SymbolId]
      ,[Supported]
      ,[OnePipDecimalValue_Legacy]
      ,[MinimumPriceIncrement]
      ,[OrderMinimumSize]
      ,[OrderMinimumSizeIncrement]
  FROM [dbo].[CounterpartySymbolSettings]
  WHERE [TradingTargetTypeId] = 4