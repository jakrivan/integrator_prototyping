
--Remove FKs first
ALTER TABLE [dbo].[ExecutedExternalDeals] DROP CONSTRAINT [FK_ExecutedExternalDeals_Symbol]
GO

ALTER TABLE [dbo].[OrderExternal] DROP CONSTRAINT [FK_OrderExternal_Symbol]
GO

ALTER TABLE [dbo].[DealExternalRejected] DROP CONSTRAINT [FK_RejectedDealExternal_Symbol]
GO

ALTER TABLE [dbo].[DealExternalExecuted] DROP CONSTRAINT [FK_DealExternalExecuted_Symbol]
GO


--Remove SKK symbols
DELETE
FROM [dbo].[Symbol]
where [QuoteCurrencyId] = (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'SKK') OR [BaseCurrencyId] = (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'SKK')

--Add new symbols

INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [IsRaboTradeable], [Usage], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(87, N'EUR/SGD', 'EURSGD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'EUR'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'SGD'), 10000, 1, 1, 1.72886, GETUTCDATE())
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [IsRaboTradeable], [Usage], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(88, N'GBP/HKD', 'GBPHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'GBP'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'HKD'), 1000, 1, 1, 12.7303, GETUTCDATE())
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [IsRaboTradeable], [Usage], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(89, N'GBP/MXN', 'GBPMXN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'GBP'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'MXN'), 1000, 1, 1, 21.79734, GETUTCDATE())
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [IsRaboTradeable], [Usage], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(90, N'GBP/SGD', 'GBPSGD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'GBP'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'SGD'), 10000, 1, 1, 2.08222, GETUTCDATE())
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [IsRaboTradeable], [Usage], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(91, N'GBP/TRY', 'GBPTRY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'GBP'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'TRY'), 10000, 1, 1, 3.63257, GETUTCDATE())
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [IsRaboTradeable], [Usage], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(92, N'GBP/ZAR', 'GBPZAR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'GBP'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'ZAR'), 1000, 1, 1, 18.1361, GETUTCDATE())
GO




--alter xsd schemas and xml settings
--risk mgmt, session mgmt, diag dashboard
-- MANUALY:

	   --This way select settings that needs adjustments
	--SELECT 'UPDATE [dbo].[Settings] SET [SettingsXml] = ''' + 
	--CONVERT(VARCHAR(MAX), [SettingsXml]) +
	--''', [LastUpdated] = GETUTCDATE() WHERE [Name] = ''' + 
	--CONVERT(VARCHAR(MAX), [Name]) + ''' AND [Id] = ' + CONVERT(VARCHAR(5), [Id])
	--FROM [dbo].[Settings]
	  --where CAST([SettingsXml] AS NVARCHAR(MAX)) like '%SKK%'


		--This is for RiskMgmt updates
		--	<Symbol ShortName="EURSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15"/>
		--	<Symbol ShortName="GBPHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15"/>
		--	<Symbol ShortName="GBPMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15"/>
		--	<Symbol ShortName="GBPSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15"/>
		--	<Symbol ShortName="GBPTRY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15"/>
		--	<Symbol ShortName="GBPZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15"/>


--Add FKs
ALTER TABLE [dbo].[ExecutedExternalDeals]  WITH NOCHECK ADD  CONSTRAINT [FK_ExecutedExternalDeals_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[OrderExternal]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderExternal_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[DealExternalRejected]  WITH NOCHECK ADD  CONSTRAINT [FK_RejectedDealExternal_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[DealExternalExecuted]  WITH NOCHECK ADD  CONSTRAINT [FK_DealExternalExecuted_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO




DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LMX" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

--UPDATE session settings - manually