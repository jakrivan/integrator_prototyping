BEGIN TRANSACTION Install_28_11_001;
GO

--Disable FKs first
ALTER TABLE [dbo].[Tick] DROP CONSTRAINT [FK_Tick_FXPair]
GO
ALTER TABLE [dbo].[MarketData] DROP CONSTRAINT [FK_MarketData_FxPair]
GO

-- facilitate NULL iso codes (currentl max one paer table)
DROP INDEX [UniqueCurrencyIsoCode] ON [dbo].[Currency]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyIsoCode] ON [dbo].[Currency]
(
	[CurrencyISOCode] ASC
) 
WHERE [CurrencyISOCode] IS NOT NULL
ON [PRIMARY]
GO

-- Add new Currency
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES
           ('XVN', 'VEN', NULL, 1, GetUtcDate(), 0)
GO

--need to be dropped so that the underlying column can be changed
ALTER TABLE [dbo].[FxPair] DROP COLUMN [Decimals]
ALTER TABLE [dbo].[FxPair] ALTER COLUMN [Multiplier] [int] NULL
ALTER TABLE [dbo].[FxPair] ALTER COLUMN [ExamplePrice] [real] NULL
ALTER TABLE [dbo].[FxPair] ADD [Decimals] AS (log10([Multiplier])) PERSISTED
GO


--Add new symbols
SET IDENTITY_INSERT [dbo].[FxPair] ON 

INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (159, N'AUDZAR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('AUD/ZAR', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('AUD/ZAR', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (160, N'CADHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/HKD', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (161, N'CADMXN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/MXN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/MXN', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (162, N'CADPLN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/PLN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/PLN', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (163, N'CADSGD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/SGD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/SGD', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (164, N'CADZAR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/ZAR', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/ZAR', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (165, N'CZKJPY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CZK/JPY', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CZK/JPY', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (166, N'DKKHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('DKK/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('DKK/HKD', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (167, N'EURILS', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('EUR/ILS', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('EUR/ILS', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (168, N'GBPILS', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('GBP/ILS', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('GBP/ILS', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (169, N'HUFJPY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('HUF/JPY', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('HUF/JPY', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (170, N'CHFCZK', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/CZK', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/CZK', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (171, N'CHFHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/HKD', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (172, N'CHFHUF', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/HUF', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/HUF', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (173, N'CHFILS', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/ILS', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/ILS', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (174, N'CHFPLN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/PLN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/PLN', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (175, N'CHFSGD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/SGD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/SGD', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (176, N'CHFTRY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/TRY', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/TRY', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (177, N'CHFZAR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/ZAR', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/ZAR', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (178, N'NOKHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NOK/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NOK/HKD', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (179, N'NZDHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NZD/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NZD/HKD', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (180, N'NZDPLN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NZD/PLN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NZD/PLN', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (181, N'NZDZAR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NZD/ZAR', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NZD/ZAR', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (182, N'PLNHUF', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('PLN/HUF', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('PLN/HUF', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (183, N'PLNJPY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('PLN/JPY', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('PLN/JPY', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (184, N'SEKHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SEK/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SEK/HKD', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (185, N'SEKJPY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SEK/JPY', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SEK/JPY', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (186, N'SGDDKK', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/DKK', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/DKK', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (187, N'SGDHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/HKD', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (188, N'SGDMXN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/MXN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/MXN', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (189, N'SGDNOK', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/NOK', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/NOK', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (190, N'SGDSEK', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/SEK', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/SEK', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (191, N'TRYJPY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('TRY/JPY', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('TRY/JPY', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (192, N'USDGHS', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/GHS', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/GHS', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (193, N'USDKES', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/KES', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/KES', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (194, N'USDNGN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/NGN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/NGN', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (195, N'USDRON', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/RON', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/RON', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (196, N'USDUGX', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/UGX', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/UGX', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (197, N'USDZMK', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/ZMK', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/ZMK', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (198, N'ZARMXN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('ZAR/MXN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('ZAR/MXN', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (199, N'USDTHB', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/THB', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/THB', 5, 3)), NULL, NULL, NULL, NULL)
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (200, N'USDXVN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/XVN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/XVN', 5, 3)), NULL, NULL, NULL, NULL)
GO

SET IDENTITY_INSERT [dbo].[FxPair] OFF

--Adding back the FKs - IMPORTANT - the 'nocheck' is crucial
ALTER TABLE [dbo].[Tick]  WITH NOCHECK ADD  CONSTRAINT [FK_Tick_FXPair] FOREIGN KEY([FXPairID])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO

ALTER TABLE [dbo].[MarketData]  WITH NOCHECK ADD  CONSTRAINT [FK_MarketData_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO



SET IDENTITY_INSERT [dbo].[LiqProviderStream] ON 
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (105, N'FS1', 23)
GO
SET IDENTITY_INSERT [dbo].[LiqProviderStream] OFF
GO

--Add the precision info for new symbols (and fxcm, hotspot, lmax)
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDZAR' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADHKD' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADMXN' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADPLN' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADSGD' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADZAR' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CZKJPY' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'DKKHKD' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURILS' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPILS' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'HUFJPY' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFCZK' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFHKD' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFHUF' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFILS' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFPLN' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFSGD' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFTRY' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFZAR' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NOKHKD' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDHKD' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDPLN' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDZAR' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'PLNHUF' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'PLNJPY' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'SEKHKD' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'SEKJPY' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'SGDDKK' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'SGDHKD' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'SGDMXN' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'SGDNOK' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'SGDSEK' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'TRYJPY' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDRON' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'ZARMXN' where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001, 0.01, 0.01 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDTHB' where Name = 'Hotspot'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue_Legacy]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1000, 1000 FROM [dbo].[TradingTargetType] TargetType INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDXVN' where Name = 'LMAX'

--ROLLBACK TRANSACTION Install_28_11_001;
--GO
--COMMIT TRANSACTION Install_28_11_001;
--GO


print 'Started adding partition files:'
print GETUTCDATE()
GO

--Partitioning
--UAT: Q:\SQLData\Testing_canBeLost\DataCollection_Testing

Alter Database FXtickDB Add FileGroup MDFilegroup_AUDZAR
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_AUDZAR_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_AUDZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_AUDZAR
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CADHKD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CADHKD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CADHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CADHKD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CADMXN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CADMXN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CADMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CADMXN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CADPLN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CADPLN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CADPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CADPLN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CADSGD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CADSGD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CADSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CADSGD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CADZAR
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CADZAR_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CADZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CADZAR
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CZKJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CZKJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CZKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CZKJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_DKKHKD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_DKKHKD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_DKKHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_DKKHKD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURILS
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURILS_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURILS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURILS
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPILS
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPILS_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPILS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPILS
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_HUFJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_HUFJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_HUFJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_HUFJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CHFCZK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CHFCZK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CHFCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CHFCZK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CHFHKD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CHFHKD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CHFHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CHFHKD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CHFHUF
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CHFHUF_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CHFHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CHFHUF
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CHFILS
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CHFILS_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CHFILS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CHFILS
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CHFPLN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CHFPLN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CHFPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CHFPLN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CHFSGD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CHFSGD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CHFSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CHFSGD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CHFTRY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CHFTRY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CHFTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CHFTRY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CHFZAR
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CHFZAR_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CHFZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CHFZAR
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NOKHKD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NOKHKD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NOKHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NOKHKD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NZDHKD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NZDHKD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NZDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NZDHKD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NZDPLN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NZDPLN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NZDPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NZDPLN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NZDZAR
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NZDZAR_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NZDZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NZDZAR
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_PLNHUF
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_PLNHUF_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_PLNHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_PLNHUF
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_PLNJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_PLNJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_PLNJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_PLNJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_SEKHKD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_SEKHKD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_SEKHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_SEKHKD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_SEKJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_SEKJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_SEKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_SEKJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_SGDDKK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_SGDDKK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_SGDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_SGDDKK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_SGDHKD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_SGDHKD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_SGDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_SGDHKD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_SGDMXN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_SGDMXN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_SGDMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_SGDMXN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_SGDNOK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_SGDNOK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_SGDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_SGDNOK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_SGDSEK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_SGDSEK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_SGDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_SGDSEK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_TRYJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_TRYJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_TRYJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_TRYJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDGHS
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDGHS_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDGHS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDGHS
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDKES
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDKES_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDKES_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDKES
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDNGN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDNGN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDNGN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDNGN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDRON
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDRON_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDRON_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDRON
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDUGX
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDUGX_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDUGX_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDUGX
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDZMK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDZMK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDZMK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDZMK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_ZARMXN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_ZARMXN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_ZARMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_ZARMXN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDTHB
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDTHB_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDTHB_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDTHB
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDXVN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDXVN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDXVN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDXVN
GO 



--Ticks
--UAT: Q:\SQLData\Testing_canBeLost\DataCollection_Testing

Alter Database FXtickDB Add FileGroup TickFilegroup_AUDZAR
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_AUDZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_AUDZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_AUDZAR
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CADHKD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CADHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CADHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CADHKD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CADMXN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CADMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CADMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CADMXN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CADPLN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CADPLN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CADPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CADPLN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CADSGD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CADSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CADSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CADSGD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CADZAR
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CADZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CADZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CADZAR
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CZKJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CZKJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CZKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CZKJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_DKKHKD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_DKKHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_DKKHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_DKKHKD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURILS
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURILS_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURILS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURILS
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPILS
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPILS_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPILS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPILS
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_HUFJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_HUFJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_HUFJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_HUFJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CHFCZK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CHFCZK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CHFCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CHFCZK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CHFHKD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CHFHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CHFHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CHFHKD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CHFHUF
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CHFHUF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CHFHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CHFHUF
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CHFILS
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CHFILS_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CHFILS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CHFILS
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CHFPLN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CHFPLN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CHFPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CHFPLN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CHFSGD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CHFSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CHFSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CHFSGD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CHFTRY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CHFTRY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CHFTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CHFTRY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CHFZAR
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CHFZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CHFZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CHFZAR
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NOKHKD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NOKHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NOKHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NOKHKD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NZDHKD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NZDHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NZDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NZDHKD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NZDPLN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NZDPLN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NZDPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NZDPLN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NZDZAR
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NZDZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NZDZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NZDZAR
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_PLNHUF
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_PLNHUF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_PLNHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_PLNHUF
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_PLNJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_PLNJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_PLNJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_PLNJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_SEKHKD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_SEKHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_SEKHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_SEKHKD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_SEKJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_SEKJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_SEKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_SEKJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_SGDDKK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_SGDDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_SGDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_SGDDKK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_SGDHKD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_SGDHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_SGDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_SGDHKD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_SGDMXN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_SGDMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_SGDMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_SGDMXN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_SGDNOK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_SGDNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_SGDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_SGDNOK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_SGDSEK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_SGDSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_SGDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_SGDSEK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_TRYJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_TRYJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_TRYJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_TRYJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDGHS
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDGHS_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDGHS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDGHS
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDKES
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDKES_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDKES_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDKES
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDNGN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDNGN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDNGN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDNGN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDRON
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDRON_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDRON_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDRON
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDUGX
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDUGX_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDUGX_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDUGX
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDZMK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDZMK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDZMK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDZMK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_ZARMXN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_ZARMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_ZARMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_ZARMXN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDTHB
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDTHB_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDTHB_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDTHB
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDXVN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDXVN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDXVN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDXVN
GO 

print 'Files Added:'
print GETUTCDATE()
GO



--unfortunately not possible
--ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction (tinyint)
--AS RANGE LEFT FOR VALUES (3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 22, 43, 45, 46, 47, 48, 49, 50, 51, 56, 57, 58, 59, 60, 61, 62, 63, 65, 66, 67, 68, 70, 72, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 95, 96, 97, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 113, 117, 128, 129, 133, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156)
--GO

--adding partitions for new symbols
ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_AUDZAR]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_AUDZAR]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (159);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CADHKD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CADHKD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (160);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CADMXN]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CADMXN]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (161);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CADPLN]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CADPLN]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (162);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CADSGD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CADSGD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (163);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CADZAR]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CADZAR]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (164);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CZKJPY]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CZKJPY]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (165);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_DKKHKD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_DKKHKD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (166);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_EURILS]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_EURILS]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (167);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_GBPILS]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_GBPILS]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (168);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_HUFJPY]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_HUFJPY]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (169);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CHFCZK]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CHFCZK]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (170);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CHFHKD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CHFHKD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (171);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CHFHUF]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CHFHUF]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (172);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CHFILS]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CHFILS]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (173);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CHFPLN]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CHFPLN]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (174);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CHFSGD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CHFSGD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (175);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CHFTRY]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CHFTRY]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (176);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CHFZAR]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CHFZAR]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (177);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_NOKHKD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_NOKHKD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (178);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_NZDHKD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_NZDHKD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (179);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_NZDPLN]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_NZDPLN]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (180);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_NZDZAR]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_NZDZAR]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (181);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_PLNHUF]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_PLNHUF]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (182);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_PLNJPY]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_PLNJPY]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (183);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_SEKHKD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_SEKHKD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (184);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_SEKJPY]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_SEKJPY]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (185);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_SGDDKK]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_SGDDKK]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (186);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_SGDHKD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_SGDHKD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (187);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_SGDMXN]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_SGDMXN]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (188);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_SGDNOK]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_SGDNOK]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (189);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_SGDSEK]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_SGDSEK]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (190);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_TRYJPY]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_TRYJPY]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (191);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_USDGHS]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_USDGHS]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (192);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_USDKES]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_USDKES]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (193);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_USDNGN]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_USDNGN]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (194);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_USDRON]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_USDRON]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (195);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_USDUGX]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_USDUGX]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (196);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_USDZMK]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_USDZMK]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (197);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_ZARMXN]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_ZARMXN]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (198);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_USDTHB]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_USDTHB]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (199);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_USDXVN]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_USDXVN]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (200);
GO


-- And mark the 'out of range' right partitions - so that future symbols doesn't get merget into existing partitions
ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_FutureSymbols]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_FutureSymbols]
GO

print 'Partition scheme updated:'
print GETUTCDATE()
GO

--Allowing RON
UPDATE [dbo].[Currency]
SET [IsCitibankPbSupported] = 1
WHERE [CurrencyAlphaCode] = 'RON'