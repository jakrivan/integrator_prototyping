

CREATE PARTITION FUNCTION MarketDataSymbolPartitioningFunction (tinyint)
AS RANGE LEFT FOR VALUES (3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 22, 43, 45, 46, 47, 48, 49, 50, 51, 56, 57, 58, 59, 60, 61, 62, 63, 65, 66, 67, 68, 70, 72, 73, 74, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 95, 96, 97, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 113, 117, 128, 129, 133, 142, 143, 144, 145, 146, 147, 148, 149, 150)
GO


CREATE PARTITION SCHEME MarketDataSymbolPartitionScheme 
AS PARTITION MarketDataSymbolPartitioningFunction ALL TO ([PRIMARY]) 
GO


sp_rename MarketData, MarketData_unpartitioned

DROP INDEX [IX_IntegratorPriceIdentity] ON [dbo].[MarketData_unpartitioned]
GO

print GETUTCDATE()

CREATE CLUSTERED INDEX IX_PairTime ON MarketData_unpartitioned
(
	[FXPairId] ASC,
	[IntegratorReceivedTimeUtc] ASC
)
WITH (DROP_EXISTING = ON)
ON MarketDataSymbolPartitionScheme(FXPairId)
GO

print GETUTCDATE()

CREATE NONCLUSTERED INDEX [IX_IntegratorPriceIdentity] ON [dbo].[MarketData_unpartitioned]
(
	[IntegratorPriceIdentity] ASC
) ON MarketDataSymbolPartitionScheme(FXPairId)
GO

print GETUTCDATE()
GO

sp_rename MarketData_unpartitioned, MarketData

print 'DONE'