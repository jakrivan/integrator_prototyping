BEGIN TRANSACTION Install_18_02_001;
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] ADD RecordId INT IDENTITY(1,1) NOT NULL
print 'OrderExternalIncomingRejectable_Daily RecordId added:'
print GETUTCDATE()
ALTER TABLE [dbo].[DealExternalExecuted] ADD RecordId INT IDENTITY(1,1) NOT NULL
print 'DealExternalExecuted RecordId added:'
print GETUTCDATE()
ALTER TABLE [dbo].[DealExternalRejected] ADD RecordId INT IDENTITY(1,1) NOT NULL
print 'DealExternalRejected RecordId added:'
print GETUTCDATE()
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] ADD RecordIdTmp INT IDENTITY(1,1) NOT NULL
print 'OrderExternalIncomingRejectable_Archive RecordId added:'
print GETUTCDATE()
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] ADD RecordId INT NULL
GO

UPDATE [dbo].[OrderExternalIncomingRejectable_Archive] SET RecordId = RecordIdTmp
GO
print 'OrderExternalIncomingRejectable_Archive RecordId inserted (as not identity):'
print GETUTCDATE()

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] DROP COLUMN RecordIdTmp
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] ALTER COLUMN INT RecordId NOT NULL
GO
print 'OrderExternalIncomingRejectable_Archive columns updated:'
print GETUTCDATE()
GO

ALTER PROCEDURE [dbo].[Daily_ArchiveRejectableOrders_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[OrderExternalIncomingRejectable_Archive]
			([IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[RecordId])
		SELECT
			[IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[RecordId]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]	
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[OrderExternalIncomingRejectable_Daily]
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

CREATE TABLE [dbo].[TradeDecay_LastProcessedRecord](
	[LockX] [bit] NOT NULL DEFAULT 1,
	[LastProcessedRecordUtc] [datetime2](7) NOT NULL,
	
	--make sure the table is single rowed
	constraint PK_T1 PRIMARY KEY (LockX),
    constraint CK_T1_Locked CHECK (LockX=1)
)
ON [PRIMARY]
GO	

INSERT INTO [dbo].[TradeDecay_LastProcessedRecord](LastProcessedRecordUtc) VALUES ('1900-01-01')
GO



CREATE PROCEDURE [dbo].[InsertTradeDecayAnalysisRequests_SP]
(
	@FromTimeUtc [datetime2](7) = NULL,
	@ToTimeUtc [datetime2](7) = NULL
)
AS
BEGIN

	IF @ToTimeUtc IS NULL
	BEGIN
		SET @ToTimeUtc = DATEADD(minute, -5, GETUTCDATE())
	END
	
	IF @FromTimeUtc IS NULL
	BEGIN
		SELECT @FromTimeUtc = [LastProcessedRecordUtc] FROM [dbo].[TradeDecay_LastProcessedRecord]
	END
	
	DECLARE @TypeCptDealId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'CptDeal')
	DECLARE @TypeCptRejectionId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'CptRejection')
	DECLARE @TypeKgtDealId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'KgtDeal')
	DECLARE @TypeKgtRejectionId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'KgtRejection')
	
	
	DECLARE @TempRequestsToAdd TABLE (
		[TradeDecayAnalysisTypeId] [tinyint] NOT NULL,
		[SourceEventRecordId] [int] NOT NULL,
		[SourceEventIndexTimeUtc] [datetime2](7) NOT NULL,
		[AnalysisStartTimeUtc] [datetime2](7) NOT NULL,
		[FXPairId] [tinyint] NOT NULL,
		[CounterpartyId] [tinyint] NOT NULL,
		[ReferencePrice] [decimal](18,9) NOT NULL,
		[PriceSideId] [bit] NOT NULL
	)
	
	INSERT INTO @TempRequestsToAdd(
		[TradeDecayAnalysisTypeId],
		[SourceEventRecordId],
		[SourceEventIndexTimeUtc],
		[AnalysisStartTimeUtc],
		[FXPairId],
		[CounterpartyId],
		[ReferencePrice],
		[PriceSideId]
	)
	SELECT
		@TypeCptDealId,
		[RecordId],
		[IntegratorReceivedExecutionReportUtc],
		CASE WHEN [FlowSideId] = 0 OR ([FlowSideId] IS NULL AND cpt.CounterpartyName IN ('Hotspot', 'FXall', 'FXCM')) 
		THEN DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExecutionReportUtc])
		ELSE [IntegratorSentExternalOrderUtc] END,
		symbolIdMap.[DCDBSymbolId],
		cptIdMap.[DCDBCounterpartyId],
		[Price],
		CASE WHEN [DealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN [dbo].[Counterparty] cpt ON deal.CounterpartyId = cpt.CounterpartyId
		INNER JOIN [dbo].[PriceDelaySettings] delaySett ON deal.CounterpartyId = delaySett.CounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON deal.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON deal.SymbolId = symbolIdMap.IntegratorDBSymbolId
		--cannot use between due to inclusions
		WHERE [IntegratorReceivedExecutionReportUtc] > @FromTimeUtc AND [IntegratorReceivedExecutionReportUtc] < @ToTimeUtc
	UNION ALL
	SELECT
		@TypeCptRejectionId,
		[RecordId],
		[IntegratorReceivedExecutionReportUtc],
		CASE WHEN cpt.CounterpartyName IN ('Hotspot', 'FXall', 'FXCM') 
		THEN DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExecutionReportUtc]) 
		ELSE [IntegratorSentExternalOrderUtc] END,
		symbolIdMap.[DCDBSymbolId],
		cptIdMap.[DCDBCounterpartyId],
		[RejectedPrice],
		CASE WHEN [DealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
	FROM 
		[dbo].[DealExternalRejected] reject
		INNER JOIN [dbo].[Counterparty] cpt ON reject.CounterpartyId = cpt.CounterpartyId
		INNER JOIN [dbo].[PriceDelaySettings] delaySett ON reject.CounterpartyId = delaySett.CounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON reject.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON reject.SymbolId = symbolIdMap.IntegratorDBSymbolId
		--cannot use between due to inclusions
		WHERE [IntegratorReceivedExecutionReportUtc] > @FromTimeUtc AND [IntegratorReceivedExecutionReportUtc] < @ToTimeUtc
		AND [RejectedPrice] IS NOT NULL
	UNION ALL
	SELECT
		CASE WHEN [IntegratorRejectionReasonId] IS NULL THEN @TypeKgtDealId ELSE @TypeKgtRejectionId END,
		[RecordId],
		[IntegratorReceivedExternalOrderUtc],
		DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExternalOrderUtc]),
		symbolIdMap.[DCDBSymbolId],
		cptIdMap.[DCDBCounterpartyId],
		[CounterpartyRequestedPrice],
		CASE WHEN [IntegratorDealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
	FROM 
		[dbo].[OrderExternalIncomingRejectable_Daily] cptOrder
		INNER JOIN [dbo].[PriceDelaySettings] delaySett ON cptOrder.CounterpartyId = delaySett.CounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON cptOrder.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON cptOrder.SymbolId = symbolIdMap.IntegratorDBSymbolId
		--cannot use between due to inclusions
		WHERE [IntegratorReceivedExternalOrderUtc] > @FromTimeUtc AND [IntegratorReceivedExternalOrderUtc] < @ToTimeUtc
	
	IF DATEDIFF(day, @FromTimeUtc, GETUTCDATE()) != 0
	BEGIN
		INSERT INTO @TempRequestsToAdd(
			[TradeDecayAnalysisTypeId],
			[SourceEventRecordId],
			[SourceEventIndexTimeUtc],
			[AnalysisStartTimeUtc],
			[FXPairId],
			[CounterpartyId],
			[ReferencePrice],
			[PriceSideId]
		)
		SELECT
			CASE WHEN [IntegratorRejectionReasonId] IS NULL THEN @TypeKgtDealId ELSE @TypeKgtRejectionId END,
			[RecordId],
			[IntegratorReceivedExternalOrderUtc],
			DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExternalOrderUtc]),
			symbolIdMap.[DCDBSymbolId],
			cptIdMap.[DCDBCounterpartyId],
			[CounterpartyRequestedPrice],
			CASE WHEN [IntegratorDealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
		FROM 
			[dbo].[OrderExternalIncomingRejectable_Archive] cptOrder
			INNER JOIN [dbo].[PriceDelaySettings] delaySett ON cptOrder.CounterpartyId = delaySett.CounterpartyId
			INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON cptOrder.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
			INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON cptOrder.SymbolId = symbolIdMap.IntegratorDBSymbolId
			--cannot use between due to inclusions
			WHERE [IntegratorReceivedExternalOrderUtc] > @FromTimeUtc AND [IntegratorReceivedExternalOrderUtc] < @ToTimeUtc
	END
	
	
	DECLARE  @LastRecordTimeUtc [datetime2](7)
	
	SELECT @LastRecordTimeUtc = MAX([SourceEventIndexTimeUtc]) FROM @TempRequestsToAdd
	
	IF @LastRecordTimeUtc IS NOT NULL
	BEGIN
		--try catch used so that execution flow is stopped after error
		BEGIN TRY
		
			INSERT INTO [FXtickDB].[dbo].[TradeDecayAnalysis_Pending]
				   ([TradeDecayAnalysisTypeId]
				   ,[SourceEventRecordId]
				   ,[AnalysisStartTimeUtc]
				   ,[FXPairId]
				   ,[CounterpartyId]
				   ,[ReferencePrice]
				   ,[PriceSideId])
			SELECT
				[TradeDecayAnalysisTypeId],
				[SourceEventRecordId],
				[AnalysisStartTimeUtc],
				[FXPairId],
				[CounterpartyId],
				[ReferencePrice],
				[PriceSideId]
			FROM
				@TempRequestsToAdd
				   
			UPDATE [dbo].[TradeDecay_LastProcessedRecord] SET [LastProcessedRecordUtc] = @LastRecordTimeUtc 

		END TRY
		BEGIN CATCH
			print 'error during storing';
			THROW;
		END CATCH
	END  
	ELSE
	BEGIN
		-- so that we do not search same intervals over and over if no trading
		UPDATE [dbo].[TradeDecay_LastProcessedRecord] SET [LastProcessedRecordUtc] = DATEADD(minute, -5, @ToTimeUtc)
	END
	
END
GO

--ROLLBACK TRANSACTION Install_18_02_001;
--GO
--COMMIT TRANSACTION Install_18_02_001;
--GO



-- Blocked systems work
BEGIN TRANSACTION Install_23_02_001;
GO

ALTER TABLE [dbo].[TradingSystemActions] ADD HardBlocked BIT NOT NULL DEFAULT 0
GO

ALTER PROCEDURE [dbo].[VenueSystemReset_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	UPDATE
		[dbo].[TradingSystemActions]
	SET
		[LastResetRequestedUtc] = GETUTCDATE(),
		[HardBlocked] = 0
	WHERE
		[TradingSystemId] = @TradingSystemId
		AND [Enabled] = 0
END
GO

CREATE PROCEDURE [dbo].[VenueSystemHardblock_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	UPDATE
		[dbo].[TradingSystemActions]
	SET
		[Enabled] = 0,
		[HardBlocked] = 1
	WHERE
		[TradingSystemId] = @TradingSystemId
END
GO
GRANT EXECUTE ON [dbo].[VenueSystemHardblock_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

ALTER PROCEDURE [dbo].[TradingSystemsGroupDisableEnableMulti_SP]
(
	@Enable [bit],
	@SystemsGroupId [int],
	@SystemTypeId [int] = NULL
)
AS
BEGIN
	UPDATE 
		action
	SET
		[Enabled] = @Enable
	FROM
		[dbo].[TradingSystemActions] action
		INNER JOIN [dbo].[TradingSystem]  trsystem ON action.[TradingSystemId] = trsystem.[TradingSystemId]
	WHERE 
		trsystem.[TradingSystemGroupId] = @SystemsGroupId
		AND (@SystemTypeId IS NULL OR trsystem.[TradingSystemTypeId] = @SystemTypeId)
		AND [HardBlocked] = 0
END
GO

ALTER PROCEDURE [dbo].[VenueSystemDisableEnableOne_SP]
(
	@TradingSystemId int,
	@Enable Bit
)
AS
BEGIN
	UPDATE
		[dbo].[TradingSystemActions]
	SET
		[Enabled] = @Enable
	WHERE
		[TradingSystemId] = @TradingSystemId
		AND [HardBlocked] = 0
END
GO

ALTER PROCEDURE [dbo].[GetVenueCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumTimeBetweenSingleCounterpartySignals_seconds],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[MinimumFillSize],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueMMSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FairPriceImprovementSpreadBasisPoints],
		sett.[FairPriceImprovementSpreadBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [FairPriceImprovementSpreadDecimal],
		sett.[StopLossNetInUsd],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueStreamSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[PreferLLDestinationCheckToLmax],
		sett.[MinimumBPGrossToAccept],
		sett.[MinimumBPGrossToAccept] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossToAcceptDecimal],
		sett.[PreferLmaxDuringHedging],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP]
AS
BEGIN
	
	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()

	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]

	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn
	
	SELECT
		ROW_NUMBER() OVER(ORDER BY
			trgroup.[OrderingRank] ASC,
			[TradingSystemGroupName] ASC,
			counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] ASC,
			beginSymbol.Name ASC) AS Rank,
		trgroup.[TradingSystemGroupName] AS TradingSystemGroup,
		trgroup.[TradingSystemGroupId] As TradingSystemGroupId,
		counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] AS TradingSystemTypeName,
		counterparty.[CounterpartyCode] AS Counterparty,
		systemtype.[TradingSystemTypeId] AS TradingSystemTypeId,
		statsPerType.ConfiguredSystems,
		--statsPerType.EnabledSystems,
		--this should be called active - and upper stay enabled
		CASE WHEN @TradingOn = 1 THEN statsPerType.ActiveSystems ELSE 0 END AS EnabledSystems,
		statsPerType.HardBlockedSystems,
		statsPerType.VolumeUsdM,
		statsPerType.DealsNum,
		statsPerType.KGTRejectionsNum,
		statsPerType.CtpRejectionsNum,
		statsPerType.PnlGrossUsdPerMUsd,
		statsPerType.CommissionsUsdPerMUsd,
		statsPerType.PnlNetUsdPerMUsd,
		statsPerType.PnlGrossUsd,
		statsPerType.CommissionsUsd,
		statsPerType.PnlNetUsd,
		allowedType.IsOrderTransmissionDisabled,
		allowedType.IsGoFlatOnlyOn,
		monitorPages.TradingSystemMonitoringPageId AS PageId,
		beginSymbol.Name AS BeginSymbol,
		endSymbol.Name AS EndSymbol,
		monitorPages.StripsCount
	FROM
	(
	SELECT 
		allowedType.TradingSystemGroupId AS TradingSystemGroupId,
		allowedType.TradingSystemTypeId AS TradingSystemTypeId,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		COALESCE(SUM(CASE WHEN action.[Enabled] = 1 AND action.[GoFlatOnly] = 0 AND action.[OrderTransmissionDisabled] = 0 THEN 1 ELSE 0 END), 0) AS ActiveSystems,
		COALESCE(SUM(CONVERT(int, action.[HardBlocked])), 0) AS HardBlockedSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		SUM(stats.[KGTRejectionsNum]) AS KGTRejectionsNum,
		SUM(stats.[CtpRejectionsNum]) AS CtpRejectionsNum,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemTypeAllowedForGroup] allowedType_forJoinsOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = allowedType_forJoinsOnly.[TradingSystemTypeId] AND trsystem.TradingSystemGroupId = allowedType_forJoinsOnly.TradingSystemGroupId
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		--link in deleted systems if needed
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		-- but make sure that those are only the allowed ones
		LEFT JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON 
			allowedType.[TradingSystemTypeId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId])
			AND
			allowedType.[TradingSystemGroupId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemGroupId], trsystem_deleted.[TradingSystemGroupId])

	GROUP BY
		allowedType.TradingSystemGroupId, allowedType.TradingSystemTypeId
	)
	statsPerType
	INNER JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON allowedType.TradingSystemGroupId = statsPerType.TradingSystemGroupId AND allowedType.TradingSystemTypeId = statsPerType.TradingSystemTypeId
	INNER JOIN [dbo].[TradingSystemType] systemtype ON statsPerType.[TradingSystemTypeId] = systemtype.[TradingSystemTypeId]
	INNER JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	RIGHT JOIN TradingSystemGroup trgroup ON trgroup.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId
	LEFT JOIN TradingSystemMonitoringPage monitorPages ON monitorPages.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId AND monitorPages.[TradingSystemTypeId] = statsPerType.TradingSystemTypeId
	LEFT JOIN [dbo].[Symbol] beginSymbol ON monitorPages.BeginSymbolId = beginSymbol.Id
	LEFT JOIN [dbo].[Symbol] endSymbol ON monitorPages.EndSymbolId = endSymbol.Id
END
GO

ALTER TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily] ADD HardBlockedSystemsCount [int] NULL
GO

ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	DECLARE @TempTradingGroupsStatistics TABLE
	(
		[Rank] [int] NOT NULL,
		[TradingSystemGroupName] [NVARCHAR](32) NOT NULL,
		[TradingSystemGroupId] [int]  NOT NULL,
		[TradingSystemTypeName] [nvarchar](12) NULL,
		[Counterparty] [char](3) NULL,
		[TradingSystemTypeId] [tinyint] NULL,
		[ConfiguredSystemsCount] int NULL,
		[EnabledSystemsCount] int NULL,
		[HardBlockedSystemsCount] int NULL,
		[VolumeUsdM] [decimal](18, 6) NULL,
		[DealsNum] [int] NULL,
		[KGTRejectionsNum] [int] NULL,
		[CtpRejectionsNum] [int] NULL,
		[PnlGrossUsdPerMUsd] [decimal](18, 6) NULL,
		[CommissionsUsdPerMUsd] [decimal](18, 6) NULL,
		[PnlNetUsdPerMUsd] [decimal](18, 6) NULL,
		[PnlGrossUsd] [decimal](18, 6) NULL,
		[CommissionsUsd] [decimal](18, 6) NULL,	
		[PnlNetUsd] [decimal](18, 6) NULL,
		[IsOrderTransmissionDisabled] [bit] NULL,
		[IsGoFlatOnlyOn] [bit] NULL,
		[PageId] [int] NULL,
		[BeginSymbol] [nchar](7) NULL,
		[EndSymbol] [nchar](7) NULL,
		[StripsCount] INT
	)

	INSERT INTO 
		@TempTradingGroupsStatistics
		([Rank]
		,[TradingSystemGroupName]
		,[TradingSystemGroupId]
		,[TradingSystemTypeName]
		,[Counterparty]
		,[TradingSystemTypeId]
		,[ConfiguredSystemsCount]
		,[EnabledSystemsCount]
		,[HardBlockedSystemsCount]
		,[VolumeUsdM]
		,[DealsNum]
		,[KGTRejectionsNum]
		,[CtpRejectionsNum]
		,[PnlGrossUsdPerMUsd]
		,[CommissionsUsdPerMUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsd]
		,[PnlNetUsd]
		,[IsOrderTransmissionDisabled]
		,[IsGoFlatOnlyOn]
		,[PageId]
		,[BeginSymbol]
		,[EndSymbol]
		,[StripsCount])
    exec [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingGroupsDaily]
			([Rank]
			,[TradingSystemGroupName]
			,[TradingSystemGroupId]
			,[TradingSystemTypeName]
			,[Counterparty]
			,[TradingSystemTypeId]
			,[ConfiguredSystemsCount]
			,[EnabledSystemsCount]
			,[HardBlockedSystemsCount]
			,[VolumeUsdM]
			,[DealsNum]
			,[KGTRejectionsNum]
			,[CtpRejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[CommissionsUsdPerMUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsd]
			,[PnlNetUsd]
			,[IsOrderTransmissionDisabled]
			,[IsGoFlatOnlyOn]
			,[PageId]
			,[BeginSymbol]
			,[EndSymbol]
			,[StripsCount])
		SELECT
			[Rank]
			,[TradingSystemGroupName]
			,[TradingSystemGroupId]
			,[TradingSystemTypeName]
			,[Counterparty]
			,[TradingSystemTypeId]
			,[ConfiguredSystemsCount]
			,[EnabledSystemsCount]
			,[HardBlockedSystemsCount]
			,[VolumeUsdM]
			,[DealsNum]
			,[KGTRejectionsNum]
			,[CtpRejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[CommissionsUsdPerMUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsd]
			,[PnlNetUsd]
			,[IsOrderTransmissionDisabled]
			,[IsGoFlatOnlyOn]
			,[PageId]
			,[BeginSymbol]
			,[EndSymbol]
			,[StripsCount]
		FROM
			@TempTradingGroupsStatistics
	COMMIT
END
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsGroupOveralDailyStatsAndInfo_SP]
(
	@GroupId INT
)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]
	
	SELECT
		[TradingSystemGroupName] AS TradingSystemGroup
		,[TradingSystemGroupId] 
		,[TradingSystemTypeName]
		,[Counterparty]
		,[TradingSystemTypeId]
		,[ConfiguredSystemsCount] AS ConfiguredSystems
		,[EnabledSystemsCount] AS EnabledSystems
		,[HardBlockedSystemsCount] AS HardBlockedSystems
		,[VolumeUsdM]
		,[DealsNum]
		,[KGTRejectionsNum]
		,[CtpRejectionsNum]
		,[PnlGrossUsdPerMUsd]
		,[CommissionsUsdPerMUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsd]
		,[PnlNetUsd]
		,[IsOrderTransmissionDisabled]
		,[IsGoFlatOnlyOn]
		,[PageId]
		,[BeginSymbol]
		,[EndSymbol]
		,[StripsCount]
	FROM 
		[dbo].[PersistedStatisticsPerTradingGroupsDaily]
	WHERE
		[TradingSystemGroupId] = @GroupId
	ORDER BY
		Rank ASC
END
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsOveralDailyStats_SP]
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()

	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]

	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn	
	
	SELECT 
		ISNULL(counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName], 'Unknown') AS SystemName,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		--COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		--this should be called active - and upper stay enabled
		CASE WHEN @TradingOn = 1 
			THEN 
			COALESCE(SUM(CASE WHEN action.[Enabled] = 1 AND action.[GoFlatOnly] = 0 AND action.[OrderTransmissionDisabled] = 0 THEN 1 ELSE 0 END), 0)
			ELSE 
			0 
			END AS EnabledSystems,
		COALESCE(SUM(CONVERT(int, action.[HardBlocked])), 0) AS HardBlockedSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		SUM(stats.[KGTRejectionsNum]) AS KGTRejectionsNum,
		SUM(stats.[CtpRejectionsNum]) AS CtpRejectionsNum,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemType] systemtype_forJoinOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = systemtype_forJoinOnly.[TradingSystemTypeId]
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystemType] systemtype ON systemtype.[TradingSystemTypeId] = COALESCE(trsystem.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId], systemtype_forJoinOnly.[TradingSystemTypeId])
		LEFT JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	GROUP BY
		counterparty.[CounterpartyCode], systemtype.[SystemName]
	ORDER BY
		SystemName
END
GO

--ROLLBACK TRANSACTION Install_23_02_001;
--GO
--COMMIT TRANSACTION Install_23_02_001;
--GO

USE msdb ;
GO
EXEC dbo.sp_add_job
    @job_name = N'Ongoing Trade Decay Analysis',
	@enabled=1, 
	@notify_level_eventlog=2, 
	@notify_level_email=2, 
	@notify_level_netsend=0, 
	@notify_level_page=0, 
	@delete_level=0,  
	@notify_email_operator_name=N'kgt support';
GO
EXEC sp_add_jobstep
    @job_name = N'Ongoing Trade Decay Analysis',
    @step_name = N'Insert Requests into the queue',
    @subsystem = N'TSQL',
    @command = N'exec [dbo].[InsertTradeDecayAnalysisRequests_SP]',
	@database_name = N'Integrator',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO
EXEC sp_add_jobstep
    @job_name = N'Ongoing Trade Decay Analysis',
    @step_name = N'Process Requests from the queue',
    @subsystem = N'TSQL',
    @command = N'exec [dbo].[ProcessTradeDecayAnalysisQueue_SP]',
	@database_name = N'FXTickDB',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO
EXEC sp_add_jobstep
    @job_name = N'Ongoing Trade Decay Analysis',
    @step_name = N'Check Steps Status',
    @subsystem = N'TSQL',
    @command = N'
	SELECT  step_name, message
	FROM    msdb.dbo.sysjobhistory
	WHERE   instance_id > COALESCE((SELECT MAX(instance_id) FROM msdb.dbo.sysjobhistory
									WHERE job_id = $(ESCAPE_SQUOTE(JOBID)) AND step_id = 0), 0)
			AND job_id = $(ESCAPE_SQUOTE(JOBID))
			AND run_status <> 1 -- success

	IF      @@ROWCOUNT <> 0
			RAISERROR(''One of steps failed'', 16, 1)
	',
    @retry_attempts = 0,
    @retry_interval = 0;
GO

EXEC dbo.sp_add_schedule
    @schedule_name = N'RunEvery10Minutes',
	--Daily
    @freq_type = 4,
	--Every one day
	@freq_interval = 1,
	-- minutes
	@freq_subday_type = 4,
	-- every 10 minutes
	@freq_subday_interval= 10
	--no need for active start time and end time (and dates) => runs always
GO
USE msdb ;
GO
EXEC sp_attach_schedule
   @job_name = N'Ongoing Trade Decay Analysis',
   @schedule_name = N'RunEvery10Minutes';
GO
EXEC dbo.sp_add_jobserver
    @job_name = N'Ongoing Trade Decay Analysis';
GO
