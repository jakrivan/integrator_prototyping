

CREATE TABLE [dbo].[TableChangeLog](
	[TableName] [varchar](40) NOT NULL,
	[LastUpdatedUtc] [datetime2] NOT NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_TableChangeLogTableName] ON [dbo].[TableChangeLog]
(
	[TableName] ASC
) ON [PRIMARY]
GO

INSERT INTO [dbo].[TableChangeLog] ([TableName], [LastUpdatedUtc]) VALUES ('LmaxCrossSettings', '1/1/1900')

CREATE TABLE [dbo].[LmaxCrossSettings](
	[SymbolId] [tinyint] NOT NULL,
	[MinimumSizeToTrade] [decimal](18,0) NOT NULL,
	[MaximumSizeToTrade] [decimal](18,0) NOT NULL,
	[MinimumDiscountBasisPointsToTrade]  [decimal](18,6) NOT NULL,
	[MinimumTimeBetweenTrades_seconds] int  NOT NULL,
	[MaximumWaitTimeOnImprovementTick_milliseconds] int NOT NULL,
	[Enabled] [bit] NOT NULL,
	[LastResetRequestedUtc] [datetime2] NULL,
	[LastUpdatedUtc] [datetime2] NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_LmaxCrossSettingsSymbolId] ON [dbo].[LmaxCrossSettings]
(
	[SymbolId] ASC
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LmaxCrossSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_LmaxCrossSettings_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[LmaxCrossSettings] CHECK CONSTRAINT [FK_LmaxCrossSettings_Symbol]
GO

CREATE TRIGGER [dbo].[LastUpdatedTrigger_LmaxCrossSettings] ON [dbo].[LmaxCrossSettings]
AFTER INSERT, UPDATE
AS

BEGIN

--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[LmaxCrossSettings] Set [LastUpdatedUtc] = GETUTCDATE() from [dbo].[LmaxCrossSettings] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[SymbolId] = myAlias.[SymbolId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'LmaxCrossSettings'

END
GO


INSERT INTO 
	[dbo].[LmaxCrossSettings]
           ([SymbolId]
           ,[MinimumSizeToTrade]
           ,[MaximumSizeToTrade]
           ,[MinimumDiscountBasisPointsToTrade]
           ,[MinimumTimeBetweenTrades_seconds]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[Enabled]
           ,[LastResetRequestedUtc])
     SELECT 
		[Id]
      ,10000
	  ,100000
	  ,0.25
	  ,1
	  ,280
	  ,0
	  ,NULL
  FROM [dbo].[Symbol]
GO



CREATE PROCEDURE [dbo].[GetUpdateLmaxSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'LmaxCrossSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		SELECT 
			symbol.Name AS Symbol
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenTrades_seconds]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[Enabled]
			,[LastResetRequestedUtc]
			,[LastUpdatedUtc]
		FROM 
			[dbo].[LmaxCrossSettings] sett
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		WHERE
			sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
	END
END
GO

GRANT EXECUTE ON [dbo].[GetUpdateLmaxSystemSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetUpdateLmaxSystemSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[LmaxSystemSettingsDisableAll_SP]
AS
BEGIN

	UPDATE
		[dbo].[LmaxCrossSettings]
	SET
		[Enabled] = 0
END
GO

GRANT EXECUTE ON [dbo].[LmaxSystemSettingsDisableAll_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[LmaxSystemSettingsDisableEnableOne_SP]
(
	@Symbol CHAR(7),
	@Enabled Bit
)
AS
BEGIN

	DECLARE @SymbolId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	UPDATE
		[dbo].[LmaxCrossSettings]
	SET
		[Enabled] = @Enabled
	WHERE
		[SymbolId] = @SymbolId
END
GO

GRANT EXECUTE ON [dbo].[LmaxSystemSettingsDisableEnableOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[LmaxSystemSettingsReset_SP]
(
	@Symbol CHAR(7)
)
AS
BEGIN

	DECLARE @SymbolId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	UPDATE
		[dbo].[LmaxCrossSettings]
	SET
		[LastResetRequestedUtc] = GETUTCDATE()
	WHERE
		[SymbolId] = @SymbolId
		AND [Enabled] = 0
END
GO

GRANT EXECUTE ON [dbo].[LmaxSystemSettingsReset_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[LmaxSystemSettingsUpdateSingle_SP]
(
	@Symbol CHAR(7),
	@LastUpdateTimeUtc DATETIME2,
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenTrades_seconds [int],
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit]
)
AS
BEGIN

	DECLARE @SymbolId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	UPDATE
		[dbo].[LmaxCrossSettings]
	SET
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[MinimumDiscountBasisPointsToTrade] = @MinimumDiscountBasisPointsToTrade,
		[MinimumTimeBetweenTrades_seconds] = @MinimumTimeBetweenTrades_seconds,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds,
		[Enabled] = @Enabled
	WHERE
		[SymbolId] = @SymbolId
		AND [LastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

GRANT EXECUTE ON [dbo].[LmaxSystemSettingsUpdateSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO