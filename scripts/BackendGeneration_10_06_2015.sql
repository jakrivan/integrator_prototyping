USE [Integrator]
GO
/****** Object:  User [IntegratorKillSwitchUser]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE USER [IntegratorKillSwitchUser] FOR LOGIN [IntegratorKillSwitchUser] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [IntegratorServiceAccount]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE USER [IntegratorServiceAccount] FOR LOGIN [IntegratorServiceAccount] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="BusinessLayerSettings" type="BusinessLayerSettings" nillable="true" /><xsd:complexType name="ArrayOfCounterpartyRank"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="CounterpartyRank" type="CounterpartyRank" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfIgnoredOrderCounterpartyContact"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IgnoredOrderCounterpartyContact" type="IgnoredOrderCounterpartyContact" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfString"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Counterparty" type="xsd:string" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="BusinessLayerSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="MarketableClientOrdersMatchingStrategy" type="MarketableClientOrdersMatchingStrategy" /><xsd:element name="ConsecutiveRejectionsStopStrategyBehavior" type="ConsecutiveRejectionsStopStrategySettings" maxOccurs="unbounded" /><xsd:element name="StartWithSessionsStopped" type="xsd:boolean" /><xsd:element name="AutoKillInternalOrdersOnOrdersTransmissionDisabled" type="xsd:boolean" /><xsd:element name="StartStopEmailSubject" type="xsd:string" /><xsd:element name="StartStopEmailBody" type="xsd:string" /><xsd:element name="StartStopEmailTo" type="xsd:string" /><xsd:element name="StartStopEmailCc" type="xsd:string" /><xsd:element name="FatalRejectedOrderTriggerPhrasesCsv" type="xsd:string" /><xsd:element name="IgnoredOrderCounterpartyContacts" type="ArrayOfIgnoredOrderCounterpartyContact" /><xsd:element name="IgnoredOrderEmailCc" type="xsd:string" /><xsd:element name="PreventMatchingToSameCounterpartyAfterOrderRejectBehavior" type="PreventMatchingToSameCounterpartyAfterOrderRejectSettings" /><xsd:element name="TimeGapsWatchingBehavior" type="TimeGapsWatchingSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ConsecutiveRejectionsStopStrategySettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="StrategyEnabled" type="xsd:boolean" /><xsd:element name="NumerOfRejectionsToTriggerStrategy" type="xsd:int" /><xsd:element name="CounterpartyRankWeightMultiplier" type="xsd:decimal" /><xsd:element name="PriceAgeRankWeightMultiplier" type="xsd:decimal" /><xsd:element name="PriceBpRankWeightMultiplier" type="xsd:decimal" /><xsd:element name="DefaultCounterpartyRank" type="xsd:decimal" /><xsd:element name="CounterpartyRanks" type="ArrayOfCounterpartyRank" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="CounterpartyRank"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence /><xsd:attribute name="CounterpartyCode" type="xsd:string" /><xsd:attribute name="Rank" type="xsd:decimal" /></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="IgnoredOrderCounterpartyContact"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence /><xsd:attribute name="CounterpartyCode" type="xsd:string" /><xsd:attribute name="EmailContacts" type="xsd:string" /></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="PreventMatchingToSameCounterpartyAfterOrderRejectSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="StrategyEnabled" type="xsd:boolean" /><xsd:element name="CounterpartyCodes" type="ArrayOfString" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="TimeGapsWatchingSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="LogFatalsDuringOffBusinessHours" type="xsd:boolean" /><xsd:element name="MinimumTimeGapToWatchFor_Milliseconds" type="xsd:int" /><xsd:element name="MinimumTimeGapToLogFatal_Milliseconds" type="xsd:int" /><xsd:element name="EnableWatchingForGapsClusters" type="xsd:boolean" /><xsd:element name="NumberOfGapsInClusterToLogFatal" type="xsd:int" /><xsd:element name="ClusterDuration_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:simpleType name="MarketableClientOrdersMatchingStrategy"><xsd:restriction base="xsd:string"><xsd:enumeration value="ImmediateMatching" /><xsd:enumeration value="NextFreshPriceMatching" /></xsd:restriction></xsd:simpleType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.Common.dll.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.Common.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="CommonSettings" type="CommonSettings" nillable="true" /><xsd:complexType name="ArrayOfInt"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Delay_Seconds" type="xsd:int" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="AutomailerSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="SmtpServer" type="xsd:string" /><xsd:element name="SmtpPort" type="xsd:int" /><xsd:element name="BypassCertificationValidation" type="xsd:boolean" /><xsd:element name="SmtpUsername" type="xsd:string" /><xsd:element name="SmtpPassword" type="xsd:string" /><xsd:element name="DirectoryForLocalEmails" type="xsd:string" /><xsd:element name="SendEmailsOnlyToLocalFolder" type="xsd:boolean" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="CommonSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="AttemptToUseMeinbergClocks" type="xsd:boolean" /><xsd:element name="Logging" type="LoggingSettings" /><xsd:element name="Automailer" type="AutomailerSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="LoggingSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="AvailableDiskSpaceThresholdToStopMDLogging_GB" type="xsd:int" /><xsd:element name="FatalErrorsEmailTo" type="xsd:string" /><xsd:element name="FatalErrorsEmailCc" type="xsd:string" /><xsd:element name="FatalErrorsEmailMinDelaysLevelsBetweenEmails" type="ArrayOfInt" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.DAL.dll.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DAL.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="DALSettings" type="DALSettings" nillable="true" /><xsd:complexType name="ArrayOfString"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Counterparty" type="xsd:string" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="DALSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="KillSwitchPollIntervalSeconds" type="xsd:int" /><xsd:element name="KillSwitchWatchDogIntervalSeconds" type="xsd:int" /><xsd:element name="IntegratorSwitchName" type="xsd:string" /><xsd:element name="CommandDistributorPollIntervalSeconds" type="xsd:int" /><xsd:element name="DataCollection" type="DataCollectionSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="DataCollectionSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="DataProviderId" type="xsd:int" /><xsd:element name="SourceFileId" type="xsd:int" /><xsd:element name="MaxBcpBatchSize" type="xsd:int" /><xsd:element name="MaxInMemoryColectionSize" type="xsd:int" /><xsd:element name="MaxIntervalBetweenUploads_Seconds" type="xsd:int" /><xsd:element name="MaxUploadTimeAfterWhichSerializationStarts_Seconds" type="xsd:int" /><xsd:element name="MaxProcessingTimeAfterShutdownSignalled_Seconds" type="xsd:int" /><xsd:element name="WaitIntervalBetweenBcpRetries_Seconds" type="xsd:int" /><xsd:element name="WaitIntervalBeforeShutdownStartsSerialization_Seconds" type="xsd:int" /><xsd:element name="DataCollectionFolder" type="xsd:string" /><xsd:element name="ExcludedCounterparties" type="ArrayOfString" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.DALv2_DataCollectionConnection.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DALv2_DataCollectionConnection.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="DALSettings_DataCollectionConnection" type="DALSettings_DataCollectionConnection" nillable="true" /><xsd:complexType name="DALSettings_DataCollectionConnection"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ConnectionString" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.DALv2_DataCollectionEnabling.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DALv2_DataCollectionEnabling.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="DALSettings_DataCollectionEnabling" type="DALSettings_DataCollectionEnabling" nillable="true" /><xsd:complexType name="DALSettings_DataCollectionEnabling"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="EnableToBDataCollection" type="xsd:boolean" /><xsd:element name="EnableMarketDataCollection" type="xsd:boolean" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.DALv2_IntegratorConnection.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DALv2_IntegratorConnection.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="DALSettings_IntegratorConnection" type="DALSettings_IntegratorConnection" nillable="true" /><xsd:complexType name="DALSettings_IntegratorConnection"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ConnectionString" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="DiagnosticsDashboardSettings" type="DiagnosticsDashboardSettings" nillable="true" /><xsd:complexType name="ArrayOfCounterpartyInfoRowType"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="CounterpartyInfoRowType" type="CounterpartyInfoRowType" minOccurs="0" maxOccurs="unbounded" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfString"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Symbol" type="xsd:string" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfString1"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Counterparty" type="xsd:string" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="DiagnosticsDashboardSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="RefreshTimeout_Miliseconds" type="xsd:int" /><xsd:element name="FirstSpreadAgeLimit_Seconds" type="xsd:int" /><xsd:element name="SecondSpreadAgeLimit_Seconds" type="xsd:int" /><xsd:element name="ThirdSpreadAgeLimit_Seconds" type="xsd:int" /><xsd:element name="CutOffSpreadAgeLimit_Hours" type="xsd:int" /><xsd:element name="SessionActionTimeout_Seconds" type="xsd:int" /><xsd:element name="EnabledSymbols" type="ArrayOfString" /><xsd:element name="EnabledCounterparties" type="ArrayOfString1" /><xsd:element name="EnabledCounterpartyInfoRowTypes" type="ArrayOfCounterpartyInfoRowType" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:simpleType name="CounterpartyInfoRowType"><xsd:restriction base="xsd:string"><xsd:enumeration value="Active" /><xsd:enumeration value="MarketData" /><xsd:enumeration value="OrderFlow" /><xsd:enumeration value="SessionsAndDealsDividerRow" /><xsd:enumeration value="Deals" /><xsd:enumeration value="FirstDailyInfoRow" /><xsd:enumeration value="KGTRejections" /><xsd:enumeration value="CtpRejections" /><xsd:enumeration value="KGTRejectionRate" /><xsd:enumeration value="CtpRejectionRate" /><xsd:enumeration value="AvgDeal" /><xsd:enumeration value="Volume" /><xsd:enumeration value="VolumeShare" /><xsd:enumeration value="KgtAvgLat" /><xsd:enumeration value="CounterpartyAvgLat" /><xsd:enumeration value="LastDailyInfoRow" /><xsd:enumeration value="SpreadRank" /><xsd:enumeration value="DealsAndSymbolsDividerRow" /><xsd:enumeration value="MaximumItems" /></xsd:restriction></xsd:simpleType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.IntegratorClientHost.exe.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.IntegratorClientHost.exe.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="IntegratorClientHostSettings" type="IntegratorClientHostSettings" nillable="true" /><xsd:complexType name="ArrayOfIntegratorClientConfiguration"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IntegratorClientConfiguration" type="IntegratorClientConfiguration" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="IntegratorClientConfiguration"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ConfigurationIdentity" type="xsd:string" /><xsd:element name="ClientInterfaceType" type="ClientInterfaceType" /><xsd:element name="ClientFriendlyName" type="xsd:string" minOccurs="0" /><xsd:element name="ClientTypeName" type="xsd:string" /><xsd:element name="AssemblyNameWithoutExtension" type="xsd:string" minOccurs="0" /><xsd:element name="AssemblyFullPath" type="xsd:string" minOccurs="0" /><xsd:element name="DefaultConfigurationParameter" type="xsd:string" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="IntegratorClientHostSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="DefaultIntegratorClientCofigurationIdentity" type="xsd:string" minOccurs="0" /><xsd:element name="IntegratorClientConfigurations" type="ArrayOfIntegratorClientConfiguration" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:simpleType name="ClientInterfaceType"><xsd:restriction base="xsd:string"><xsd:enumeration value="Console" /><xsd:enumeration value="Form" /></xsd:restriction></xsd:simpleType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.IntegratorWatchDogSvc.exe.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.IntegratorWatchDogSvc.exe.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="WatchdogSettings" type="WatchdogSettings" nillable="true" /><xsd:complexType name="ArrayOfIntegratorServiceDiagnosticsEndpoint"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IntegratorServiceDiagnosticsEndpoint" type="IntegratorServiceDiagnosticsEndpoint" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="IntegratorServiceDiagnosticsEndpoint"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Endpoint" type="xsd:string" /><xsd:element name="Port" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="WatchdogSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IntegratorServiceDiagnosticsEndpoints" type="ArrayOfIntegratorServiceDiagnosticsEndpoint" /><xsd:element name="IntegratorNotRunnigSendFirstEmailAfter_Minutes" type="xsd:int" /><xsd:element name="IntegratorNotRunnigResendEmailAfter_Hours" type="xsd:int" /><xsd:element name="EmailRecipients" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.MessageBus.dll.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBus.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="MessageBusSettings" type="MessageBusSettings" nillable="true" /><xsd:complexType name="MessageBusSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IntegratorServiceHost" type="xsd:string" /><xsd:element name="IntegratorServicePort" type="xsd:int" /><xsd:element name="IntegratorServicePipeName" type="xsd:string" /><xsd:element name="MaximumAllowedDisconnectedInterval_Seconds" type="xsd:int" /><xsd:element name="DisconnectedSessionReconnectInterval_Seconds" type="xsd:int" /><xsd:element name="SessionHealthCheckInterval_Seconds" type="xsd:int" /><xsd:element name="PriceStreamTransferMode" type="PriceStreamTransferMode" /><xsd:element name="PerSymbolPriceThrottlingInterval_Milliseconds" type="xsd:int" minOccurs="0" /><xsd:element name="ForceConnectRemotingClientOnContractVersionMismatch" type="xsd:boolean" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:simpleType name="PriceStreamTransferMode"><xsd:restriction base="xsd:string"><xsd:enumeration value="Reliable" /><xsd:enumeration value="FastAndUnreliable" /></xsd:restriction></xsd:simpleType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="MessageBusSettings_ClientConnectionInfo" type="MessageBusSettings_ClientConnectionInfo" nillable="true" /><xsd:complexType name="MessageBusSettings_ClientConnectionInfo"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IntegratorServicePipeName" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.MessageBusSettings_ConnectionKeeping.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_ConnectionKeeping.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="MessageBusSettings_ConnectionKeeping" type="MessageBusSettings_ConnectionKeeping" nillable="true" /><xsd:complexType name="MessageBusSettings_ConnectionKeeping"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="MaximumAllowedDisconnectedInterval_Seconds" type="xsd:int" /><xsd:element name="DisconnectedSessionReconnectInterval_Seconds" type="xsd:int" /><xsd:element name="SessionHealthCheckInterval_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="MessageBusSettings_ContractMismatchHandling" type="MessageBusSettings_ContractMismatchHandling" nillable="true" /><xsd:complexType name="MessageBusSettings_ContractMismatchHandling"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ForceConnectRemotingClientOnContractVersionMismatch" type="xsd:boolean" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="MessageBusSettings_IntegratorEndpointConnectionInfo" type="MessageBusSettings_IntegratorEndpointConnectionInfo" nillable="true" /><xsd:complexType name="MessageBusSettings_IntegratorEndpointConnectionInfo"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ConnectionType" type="ConnectionType" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:simpleType name="ConnectionType"><xsd:restriction base="xsd:string"><xsd:enumeration value="Localhost" /><xsd:enumeration value="PrivateIPAddress" /><xsd:enumeration value="PublicIPAddress" /><xsd:enumeration value="Hostname" /></xsd:restriction></xsd:simpleType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="MessageBusSettings_PriceTransferBehavior" type="MessageBusSettings_PriceTransferBehavior" nillable="true" /><xsd:complexType name="MessageBusSettings_PriceTransferBehavior"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="PriceStreamTransferMode" type="PriceStreamTransferMode" /><xsd:element name="PerSymbolPriceThrottlingInterval_Milliseconds" type="xsd:int" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:simpleType name="PriceStreamTransferMode"><xsd:restriction base="xsd:string"><xsd:enumeration value="Reliable" /><xsd:enumeration value="FastAndUnreliable" /></xsd:restriction></xsd:simpleType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.QuickItchN.dll.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.QuickItchN.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="QuickItchNSettings" type="QuickItchNSettings" nillable="true" /><xsd:complexType name="QuickItchNSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="HtaSessionSettings" type="SessionSettings" /><xsd:element name="HtfSessionSettings" type="SessionSettings" /><xsd:element name="Ht3SessionSettings" type="SessionSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="SessionSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Host" type="xsd:string" /><xsd:element name="Port" type="xsd:int" /><xsd:element name="Username" type="xsd:string" /><xsd:element name="Password" type="xsd:string" /><xsd:element name="HeartBeatInterval_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="RiskManagementSettings" type="RiskManagementSettings" nillable="true" /><xsd:complexType name="ArrayOfString"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Instance" type="xsd:string" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfSymbol"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Symbol" type="Symbol" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ExternalOrderMaximumAllowedSizeSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="RiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumAllowedBaseAbsSizeInUsd" type="xsd:decimal" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ExternalOrdersRejectionRateSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="RiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumAllowedInstancesPerTimeInterval" type="xsd:int" /><xsd:element name="TimeIntervalToCheck_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ExternalOrdersSubmissionRateSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="RiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumAllowedInstancesPerTimeInterval" type="xsd:int" /><xsd:element name="TimeIntervalToCheck_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FatalErrorsRateSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="RiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumAllowedInstancesPerTimeInterval" type="xsd:int" /><xsd:element name="TimeIntervalToCheck_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="PriceAndPositionSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ExternalNOPRiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumExternalNOPInUSD" type="xsd:decimal" /><xsd:element name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xsd:int" /><xsd:element name="PriceDeviationCheckAllowed" type="xsd:boolean" /><xsd:element name="OnlinePriceDeviationCheckMaxPriceAge_Minutes" type="xsd:int" /><xsd:element name="SymbolTrustworthyCheckAllowed" type="xsd:boolean" /><xsd:element name="MinimumSymbolUntrustworthyPeriod_Seconds" type="xsd:int" /><xsd:element name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xsd:boolean" /><xsd:element name="DefaultNewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xsd:decimal" minOccurs="0" /><xsd:element name="Symbols" type="ArrayOfSymbol" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="RiskManagementSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ConfigLastUpdated" type="xsd:string" nillable="true" /><xsd:element name="DisallowTradingOnInstances" type="ArrayOfString" minOccurs="0" /><xsd:element name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" /><xsd:element name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" /><xsd:element name="FatalErrorsRate" type="FatalErrorsRateSettings" /><xsd:element name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" /><xsd:element name="PriceAndPosition" type="PriceAndPositionSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="Symbol"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence /><xsd:attribute name="ShortName" type="xsd:string" /><xsd:attribute name="NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xsd:decimal" /></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="SessionsSettings" type="SessionsSettings" nillable="true" /><xsd:complexType name="ArrayOfCounterpartSetting"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="CounterpartSetting" type="CounterpartSetting" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfSTPDestinationSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="STPDestination" type="STPDestinationSettings" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfSpecialRejectionRateSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="SpecialRejection" type="SpecialRejectionRateSettings" minOccurs="0" maxOccurs="unbounded" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfString"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Counterparty" type="xsd:string" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfSymbolRateSetting"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Symbol" type="SymbolRateSetting" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfSymbolSetting"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="SymbolSetting" type="SymbolSetting" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="CounterpartSetting"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ShortName" type="xsd:string" /><xsd:element name="SettingAction" type="SettingAction" /><xsd:element name="Symbols" type="ArrayOfSymbolSetting" minOccurs="0" /><xsd:element name="SenderSubId" type="xsd:string" minOccurs="0" /><xsd:element name="MarketSessionIntoSeparateProcess" type="xsd:boolean" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="CounterpartsSubscription"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Counterparts" type="ArrayOfCounterpartSetting" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ExternalOrdersRejectionRateSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="CheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumAllowedInstancesPerTimeInterval" type="xsd:int" /><xsd:element name="MaximumAllowedNongenuineInstancesPerTimeInterval" type="xsd:int" /><xsd:element name="TimeIntervalToCheck_Seconds" type="xsd:int" /><xsd:element name="SpecialRejectionsHandling" type="ArrayOfSpecialRejectionRateSettings" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_BNPSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Password" type="xsd:string" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_CRSSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ClientId" type="xsd:string" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_CTISettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Password" type="xsd:string" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_CZBSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_DBKSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_FALSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Username" type="xsd:string" /><xsd:element name="Password" type="xsd:string" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_FCMMMSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="MaxOrderHoldTime_Milliseconds" type="xsd:int" /><xsd:element name="MaxTotalPricesPerSecond" type="xsd:int" /><xsd:element name="MaxPerSymbolPricesPerSecondSetting" type="MaxPerSymbolPricesPerSecondSetting" minOccurs="0" /><xsd:element name="ExpectedPricingDepth" type="xsd:int" /><xsd:element name="RejectionRateDisconnectSettings" type="RejectionRateDisconnectSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_FCMSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IocApplyTtl" type="xsd:boolean" /><xsd:element name="IocTtlMs" type="xsd:int" /><xsd:element name="ApplyMaxHoldTime" type="xsd:boolean" /><xsd:element name="MaxHoldTime" type="FXCMMaxHoldTime" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_HOTSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="SenderSubId" type="xsd:string" /><xsd:element name="Username" type="xsd:string" /><xsd:element name="Password" type="xsd:string" /><xsd:element name="DealConfirmationSupported" type="xsd:boolean" /><xsd:element name="MaximumDealConfirmationDelay_Milliseconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_JPMSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Password" type="xsd:string" /><xsd:element name="PasswordLength" type="xsd:int" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_LMXSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="L01LayersNum" type="xsd:int" minOccurs="0" /><xsd:element name="Username" type="xsd:string" /><xsd:element name="Password" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_MGSSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="OnBehalfOfCompId" type="xsd:string" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_NOMSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="OnBehalfOfCompID" type="xsd:string" /><xsd:element name="SenderSubId" type="xsd:string" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_RBSSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_SOCSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="OnBehalfOfCompID" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_UBSSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Username" type="xsd:string" /><xsd:element name="Password" type="xsd:string" /><xsd:element name="PartyId" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXSTPChannel_CTIPBSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Password" type="xsd:string" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="GlobalSubscription"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Symbols" type="ArrayOfSymbolSetting" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="MarketDataSessionSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="MaxAutoResubscribeCount" type="xsd:int" /><xsd:element name="RetryIntervalsSequence_Seconds" type="xsd:int" maxOccurs="unbounded" /><xsd:element name="UnconfirmedSubscriptionTimeout_Seconds" type="xsd:int" /><xsd:element name="PutErrorsInLessImportantLog" type="xsd:boolean" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="MaxPerSymbolPricesPerSecondSetting"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="DefaultMaxAllowedPerSecondRate" type="xsd:int" /><xsd:element name="SpecialSymbolRateSettings" type="ArrayOfSymbolRateSetting" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="OrderFlowSessionSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="UnconfirmedOrderTimeout_Seconds" type="xsd:int" /><xsd:element name="PutErrorsInLessImportantLog" type="xsd:boolean" minOccurs="0" /><xsd:element name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="RejectionRateDisconnectSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="RiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MinimumOrdersToApplyCheck" type="xsd:int" /><xsd:element name="MaxAllowedRejectionRatePercent" type="xsd:decimal" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="STPDestinationSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Name" type="STPCounterparty" /><xsd:element name="UnconfirmedTicketTimeout_Seconds" type="xsd:int" /><xsd:element name="ServedCounterparties" type="ArrayOfString" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="STPSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="STPDestinations" type="ArrayOfSTPDestinationSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="SessionsSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="FIXChannel_CRS" type="FIXChannel_CRSSettings" /><xsd:element name="FIXChannel_DBK" type="FIXChannel_DBKSettings" /><xsd:element name="FIXChannel_UBS" type="FIXChannel_UBSSettings" /><xsd:element name="FIXChannel_CTI" type="FIXChannel_CTISettings" /><xsd:element name="FIXChannel_MGS" type="FIXChannel_MGSSettings" /><xsd:element name="FIXChannel_RBS" type="FIXChannel_RBSSettings" /><xsd:element name="FIXChannel_JPM" type="FIXChannel_JPMSettings" /><xsd:element name="FIXChannel_BNP" type="FIXChannel_BNPSettings" /><xsd:element name="FIXChannel_CZB" type="FIXChannel_CZBSettings" /><xsd:element name="FIXChannel_SOC" type="FIXChannel_SOCSettings" /><xsd:element name="FIXChannel_NOM" type="FIXChannel_NOMSettings" /><xsd:element name="FIXChannel_HTA" type="FIXChannel_HOTSettings" /><xsd:element name="FIXChannel_HTF" type="FIXChannel_HOTSettings" /><xsd:element name="FIXChannel_HT3" type="FIXChannel_HOTSettings" /><xsd:element name="FIXChannel_FAL" type="FIXChannel_FALSettings" /><xsd:element name="FIXChannel_LM1" type="FIXChannel_LMXSettings" /><xsd:element name="FIXChannel_LM2" type="FIXChannel_LMXSettings" /><xsd:element name="FIXChannel_LM3" type="FIXChannel_LMXSettings" /><xsd:element name="FIXChannel_FC1" type="FIXChannel_FCMSettings" /><xsd:element name="FIXChannel_FC2" type="FIXChannel_FCMSettings" /><xsd:element name="FIXChannel_FS1" type="FIXChannel_FCMMMSettings" /><xsd:element name="FIXChannel_FS2" type="FIXChannel_FCMMMSettings" /><xsd:element name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" /><xsd:element name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" /><xsd:element name="MarketDataSession" type="MarketDataSessionSettings" /><xsd:element name="OrderFlowSession" type="OrderFlowSessionSettings" /><xsd:element name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" minOccurs="0" /><xsd:element name="STP" type="STPSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="SpecialRejectionRateSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="TriggerPhrase" type="xsd:string" /><xsd:element name="MaximumAllowedInstancesPerTimeInterval" type="xsd:int" /><xsd:element name="TimeIntervalToCheck_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="SubscriptionSettingsBag"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ResubscribeOnReconnect" type="xsd:boolean" /><xsd:element name="GlobalSubscriptionSettings" type="GlobalSubscription" minOccurs="0" /><xsd:element name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="SymbolRateSetting"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence /><xsd:attribute name="Name" type="xsd:string" /><xsd:attribute name="MaxAllowedPerSecondRate" type="xsd:int" use="required" /></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="SymbolSetting"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ShortName" type="xsd:string" /><xsd:element name="Size" type="xsd:decimal" minOccurs="0" /><xsd:element name="SettingAction" type="SettingAction" /><xsd:element name="SenderSubId" type="xsd:string" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="UnexpectedMessagesTreatingSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="EnableThreasholdingOfUnexpectedMessages" type="xsd:boolean" /><xsd:element name="MaximumAllowedInstancesPerTimeInterval" type="xsd:int" /><xsd:element name="TimeIntervalToCheck_Minutes" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:simpleType name="FXCMMaxHoldTime"><xsd:restriction base="xsd:string"><xsd:enumeration value="UpTo1ms" /><xsd:enumeration value="UpTo30ms" /><xsd:enumeration value="UpTo100ms" /><xsd:enumeration value="UpTo500ms" /><xsd:enumeration value="UpTo3000ms" /></xsd:restriction></xsd:simpleType><xsd:simpleType name="STPCounterparty"><xsd:restriction base="xsd:string"><xsd:enumeration value="CTIPB" /><xsd:enumeration value="Traiana" /></xsd:restriction></xsd:simpleType><xsd:simpleType name="SettingAction"><xsd:restriction base="xsd:string"><xsd:enumeration value="Add" /><xsd:enumeration value="Remove" /></xsd:restriction></xsd:simpleType></xsd:schema>'
GO
/****** Object:  UserDefinedTableType [dbo].[IdsTable]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE TYPE [dbo].[IdsTable] AS TABLE(
	[Id] [int] NOT NULL
)
GO
ALTER AUTHORIZATION ON TYPE::[dbo].[IdsTable] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[AlterFIXBypassing_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AlterFIXBypassing_SP]( 
	@CTPID CHAR(3),
	@TurnBypassOff Bit
	)
AS
BEGIN

	DECLARE @CTPSEARCHPATTERN CHAR(7) = @CTPID + '[_]%'

	IF @TurnBypassOff = 1
	BEGIN

		UPDATE
			[dbo].[Settings_FIX]
		SET
			[Configuration] = REPLACE([Configuration], 'BypassParsing=Y', '#BypassParsing=Y')
		WHERE
			[Name] like @CTPSEARCHPATTERN
			AND [Configuration] like '%BypassParsing=Y%'
			AND [Configuration] NOT like '%#BypassParsing=Y%'

		UPDATE
			[dbo].[Settings_FIX]
		SET
			[Configuration] = REPLACE([Configuration], 'UseDataDictionary=N', '#UseDataDictionary=N')
		WHERE
			[Name] like @CTPSEARCHPATTERN
			AND [Configuration] like '%UseDataDictionary=N%'
			AND [Configuration] NOT like '%#UseDataDictionary=N%'
	
	
		UPDATE
			[dbo].[Settings_FIX]
		SET
			[Configuration] = REPLACE([Configuration], '#DataDictionary=F', 'DataDictionary=F')
		WHERE
			[Name] like @CTPSEARCHPATTERN
			AND [Configuration] like '%#DataDictionary=F%'

	END
	ELSE
	BEGIN

		UPDATE
			[dbo].[Settings_FIX]
		SET
			[Configuration] = REPLACE([Configuration], '#BypassParsing=Y', 'BypassParsing=Y')
		WHERE
			[Name] like @CTPSEARCHPATTERN
			AND [Configuration] like '%#BypassParsing=Y%'

		UPDATE
			[dbo].[Settings_FIX]
		SET
			[Configuration] = REPLACE([Configuration], '#UseDataDictionary=N', 'UseDataDictionary=N')
		WHERE
			[Name] like @CTPSEARCHPATTERN
			AND [Configuration] like '%#UseDataDictionary=N%'
	
	
		UPDATE
			[dbo].[Settings_FIX]
		SET
			[Configuration] = REPLACE([Configuration], 'DataDictionary=F', '#DataDictionary=F')
		WHERE
			[Name] like @CTPSEARCHPATTERN
			AND [Configuration] like '%DataDictionary=F%'	
			AND [Configuration] NOT like '%#DataDictionary=F%'	
	END


END

GO
ALTER AUTHORIZATION ON [dbo].[AlterFIXBypassing_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[CreateDailyDealsView_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[CreateDailyDealsView_SP] 
AS
BEGIN

	DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)
	DECLARE @YesterdayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), DATEADD(DAY, -1, GETUTCDATE()), 101)

	DECLARE @Query NVARCHAR(MAX) = N'
	CREATE VIEW [DealExternalExecutedDailyView_' + @TodayLiteral + ']
	WITH SCHEMABINDING 
	AS 
	SELECT 
		deal.[IntegratorReceivedExecutionReportUtc]
		,deal.[CounterpartyId]
		,deal.[SymbolId]
		,deal.[IntegratorExternalOrderPrivateId]
		,deal.[AmountBasePolExecuted]
		,deal.[Price]
		,CAST(
			(
				(datepart(HOUR, deal.QuoteReceivedToOrderSentInternalLatency)*60 + datepart(MINUTE, deal.QuoteReceivedToOrderSentInternalLatency))*60 
				+ datepart(SECOND, deal.QuoteReceivedToOrderSentInternalLatency)
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, deal.QuoteReceivedToOrderSentInternalLatency) 
		   AS IntegratorLatencyNanoseconds
		,CAST(
			(
				(datepart(HOUR, deal.[OrderSentToExecutionReportReceivedLatency])*60 + datepart(MINUTE, deal.[OrderSentToExecutionReportReceivedLatency]))*60 
				+ datepart(SECOND, deal.[OrderSentToExecutionReportReceivedLatency])
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, deal.[OrderSentToExecutionReportReceivedLatency]) 
		   AS CounterpartyLatencyNanoseconds
		,deal.[AmountTermPolExecutedInUsd]
		,deal.[DatePartIntegratorReceivedExecutionReportUtc]
	FROM 
		[dbo].[DealExternalExecuted] deal
	WHERE
		DatePartIntegratorReceivedExecutionReportUtc = CONVERT(DATE,''' + @TodayLiteral + ''', 101)'
		
		
	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE UNIQUE CLUSTERED INDEX [IX_ReceivedUtc_OrderId] ON [dbo].[DealExternalExecutedDailyView_' + @TodayLiteral + ']
	(
		[IntegratorReceivedExecutionReportUtc] ASC,
		[IntegratorExternalOrderPrivateId] ASC
	)ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalExecutedDailyView_' + @TodayLiteral + ']
	(
		[DatePartIntegratorReceivedExecutionReportUtc] ASC,
		[CounterpartyId] ASC,
		[SymbolId] ASC
	)
	INCLUDE 
	(
		[AmountBasePolExecuted],
		[Price],
		[AmountTermPolExecutedInUsd],
		[IntegratorLatencyNanoseconds],
		[CounterpartyLatencyNanoseconds])
	ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	IF EXISTS(SELECT * FROM  sys.synonyms s WHERE s.name = 'DealExternalExecuted_DailyView')
	BEGIN
		DROP SYNONYM DealExternalExecuted_DailyView
	END

	SET @Query = N'
	CREATE SYNONYM DealExternalExecuted_DailyView FOR [DealExternalExecutedDailyView_' + @TodayLiteral + ']'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	IF EXISTS(select * FROM sys.views where name = ''DealExternalExecutedDailyView_' + @YesterdayLiteral + ''')
	BEGIN
		DROP VIEW [DealExternalExecutedDailyView_' + @YesterdayLiteral + ']
	END
	'

	EXECUTE sp_executesql @Query

END

GO
ALTER AUTHORIZATION ON [dbo].[CreateDailyDealsView_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[CreateDailyRejectionsView_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateDailyRejectionsView_SP] 
AS
BEGIN

	DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)
	DECLARE @YesterdayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), DATEADD(DAY, -1, GETUTCDATE()), 101)

	DECLARE @Query NVARCHAR(MAX) = N'
	CREATE VIEW [DealExternalRejectedDailyView_' + @TodayLiteral + ']
	WITH SCHEMABINDING 
	AS 
	SELECT 
		   reject.[IntegratorReceivedExecutionReportUtc]
		   ,reject.[IntegratorExternalOrderPrivateId]
		  ,reject.[CounterpartyId]
		  ,reject.[DatePartIntegratorReceivedExecutionReportUtc]
		  ,CAST(
			(
				(datepart(HOUR, reject.QuoteReceivedToOrderSentInternalLatency)*60 + datepart(MINUTE, reject.QuoteReceivedToOrderSentInternalLatency))*60 
				+ datepart(SECOND, reject.QuoteReceivedToOrderSentInternalLatency)
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, reject.QuoteReceivedToOrderSentInternalLatency) 
		   AS IntegratorLatencyNanoseconds,
		   reject.[RejectionDirectionId]
	FROM 
		[dbo].[DealExternalRejected] reject
	WHERE
		DatePartIntegratorReceivedExecutionReportUtc = CONVERT(DATE,''' + @TodayLiteral + ''', 101)'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE UNIQUE CLUSTERED INDEX [IX_ReceivedUtc_OrderId] ON [dbo].[DealExternalRejectedDailyView_' + @TodayLiteral + ']
	(
		[IntegratorReceivedExecutionReportUtc] ASC,
		[IntegratorExternalOrderPrivateId] ASC
	)ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalRejectedDailyView_' + @TodayLiteral + ']
	(
		[DatePartIntegratorReceivedExecutionReportUtc] ASC,
		[CounterpartyId] ASC
	)
	INCLUDE
	(
		[IntegratorLatencyNanoseconds]
	)
	ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	IF EXISTS(SELECT * FROM  sys.synonyms s WHERE s.name = 'DealExternalRejected_DailyView')
	BEGIN
		DROP SYNONYM DealExternalRejected_DailyView
	END

	SET @Query = N'
	CREATE SYNONYM DealExternalRejected_DailyView FOR [DealExternalRejectedDailyView_' + @TodayLiteral + ']'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	IF EXISTS(select * FROM sys.views where name = ''DealExternalRejectedDailyView_' + @YesterdayLiteral + ''')
	BEGIN
		DROP VIEW [DealExternalRejectedDailyView_' + @YesterdayLiteral + ']
	END
	'

	EXECUTE sp_executesql @Query

END

GO
ALTER AUTHORIZATION ON [dbo].[CreateDailyRejectionsView_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[Daily_ArchiveRejectableOrders_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Daily_ArchiveRejectableOrders_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[OrderExternalIncomingRejectable_Archive]
			([IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[GlobalRecordId])
		SELECT
			[IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[GlobalRecordId]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]	
		
		--remember last identity
		DECLARE @LastId INT
		SELECT @LastId = IDENT_CURRENT ('OrderExternalIncomingRejectable_Daily')
		SET @LastId = @LastId + 1
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[OrderExternalIncomingRejectable_Daily]
		
		--and continue in identity
		DBCC CHECKIDENT ('OrderExternalIncomingRejectable_Daily', RESEED, @LastId)
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END

GO
ALTER AUTHORIZATION ON [dbo].[Daily_ArchiveRejectableOrders_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[Daily_ArchiveTradingSystemDeals_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Daily_ArchiveTradingSystemDeals_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Archive]
			([TradingSystemId],
			[InternalTransactionIdentity],
			[AmountBasePolExecuted],
			[DatePartDealExecuted])
		SELECT
			[TradingSystemId],
			[InternalTransactionIdentity],
			[AmountBasePolExecuted],
			DATEADD(DAY, -1, GETUTCDATE())
		FROM
			[dbo].[TradingSystemDealsExecuted_Daily]	
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[TradingSystemDealsExecuted_Daily]
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END

GO
ALTER AUTHORIZATION ON [dbo].[Daily_ArchiveTradingSystemDeals_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[Daily_ArchiveTradingSystemRejections_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Daily_ArchiveTradingSystemRejections_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[TradingSystemDealsRejected_Archive]
			([TradingSystemId],
			[IntegratorExternalOrderPrivateId],
			[AmountBasePolRejected],
			[RejectionDirectionId],
			[DatePartDealRejected])
		SELECT
			[TradingSystemId],
			[IntegratorExternalOrderPrivateId],
			[AmountBasePolRejected],
			[RejectionDirectionId],
			DATEADD(DAY, -1, GETUTCDATE())
		FROM
			[dbo].[TradingSystemDealsRejected_Daily]	
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[TradingSystemDealsRejected_Daily]
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END

GO
ALTER AUTHORIZATION ON [dbo].[Daily_ArchiveTradingSystemRejections_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[Daily_ClearPriceDelayStats_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Daily_ClearPriceDelayStats_SP]
AS
BEGIN
	UPDATE 
		[dbo].[PriceDelayStats]
	SET
		[TotalReceivedPricesInLastMinute] = 0,
		[DelayedPricesInLastMinute] = 0,
		[TotalReceivedPricesInLastHour] = 0,
		[DelayedPricesInLastHour] = 0
END

GO
ALTER AUTHORIZATION ON [dbo].[Daily_ClearPriceDelayStats_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[Daily_ClearPriceDelayStats_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[Daily_DeleteOldSchedules_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Daily_DeleteOldSchedules_SP]
AS
BEGIN
	DELETE FROM
		[dbo].[SystemsTradingControlSchedule]
	WHERE
		IsDaily = 0 AND IsWeekly = 0 AND ScheduleTimeZoneSpecific < GETUTCDATE()
END

GO
ALTER AUTHORIZATION ON [dbo].[Daily_DeleteOldSchedules_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[Deduplicate_AggregatedExternalExecutedTicket_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Deduplicate_AggregatedExternalExecutedTicket_SP] 
AS
BEGIN
	CREATE TABLE #Temp_Deduped_AggregatedExternalExecutedTicket(
		[AggregatedExternalExecutedTicketCounterpartyId] [nvarchar](50) NOT NULL,
		[IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
		[CounterpartySentTimeUtc] [datetime2](7) NULL,
		[AggregatedSizeBaseAbs] [decimal](18, 2) NOT NULL,
		[AggregatedPrice] [decimal](18, 10) NOT NULL,
		[StpCounterpartyId] [tinyint] NOT NULL,
		[FixMessageRaw] [nvarchar](1024) NOT NULL
	) 

	INSERT INTO #Temp_Deduped_AggregatedExternalExecutedTicket
			   ([AggregatedExternalExecutedTicketCounterpartyId]
			   ,[IntegratorReceivedTimeUtc]
			   ,[CounterpartySentTimeUtc]
			   ,[AggregatedSizeBaseAbs]
			   ,[AggregatedPrice]
			   ,[StpCounterpartyId]
			   ,[FixMessageRaw])
	SELECT DISTINCT
		[AggregatedExternalExecutedTicketCounterpartyId]
		  ,[IntegratorReceivedTimeUtc]
		  ,[CounterpartySentTimeUtc]
		  ,[AggregatedSizeBaseAbs]
		  ,[AggregatedPrice]
		  ,[StpCounterpartyId]
		  ,[FixMessageRaw]
	FROM [dbo].[AggregatedExternalExecutedTicket]

	TRUNCATE TABLE [dbo].[AggregatedExternalExecutedTicket]

	INSERT INTO [dbo].[AggregatedExternalExecutedTicket]
			   ([AggregatedExternalExecutedTicketCounterpartyId]
			   ,[IntegratorReceivedTimeUtc]
			   ,[CounterpartySentTimeUtc]
			   ,[AggregatedSizeBaseAbs]
			   ,[AggregatedPrice]
			   ,[StpCounterpartyId]
			   ,[FixMessageRaw])
	SELECT DISTINCT
		[AggregatedExternalExecutedTicketCounterpartyId]
		  ,[IntegratorReceivedTimeUtc]
		  ,[CounterpartySentTimeUtc]
		  ,[AggregatedSizeBaseAbs]
		  ,[AggregatedPrice]
		  ,[StpCounterpartyId]
		  ,[FixMessageRaw]
	FROM #Temp_Deduped_AggregatedExternalExecutedTicket

	DROP TABLE #Temp_Deduped_AggregatedExternalExecutedTicket
END

GO
ALTER AUTHORIZATION ON [dbo].[Deduplicate_AggregatedExternalExecutedTicket_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[DeleteIntegratorProcess_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeleteIntegratorProcess_SP]
(
	@ProcessId INT
)
AS
BEGIN
	DELETE	FROM 
		[dbo].[IntegratorProcess]
	WHERE
		Id = @ProcessId
END

GO
ALTER AUTHORIZATION ON [dbo].[DeleteIntegratorProcess_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[DeleteIntegratorProcess_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[DeleteTradingSystemMonitoringPages_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
		
		
CREATE PROCEDURE [dbo].[DeleteTradingSystemMonitoringPages_SP]
(
	@TradingSystemMonitoringPageId INT
)
AS
BEGIN

	DECLARE @TradingSystemIdsToDelete IdsTable
	
	INSERT INTO @TradingSystemIdsToDelete (Id)
	
	SELECT
		system.[TradingSystemId]
	FROM 
		[dbo].[TradingSystemMonitoringPage] page
		INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemGroupId] = page.[TradingSystemGroupId] AND system.[TradingSystemTypeId] = page.[TradingSystemTypeId]
		INNER JOIN [dbo].[TradingSystemActions] action ON action.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON action.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Symbol] beginSymbol ON page.[BeginSymbolId] = beginSymbol.Id
		INNER JOIN [dbo].[Symbol] endSymbol ON page.[EndSymbolId] = endSymbol.Id
	WHERE
		page.[TradingSystemMonitoringPageId] = @TradingSystemMonitoringPageId
		AND	symbol.[Name] BETWEEN beginSymbol.[Name] AND endSymbol.[Name]
		
	EXEC [dbo].[VenueSystemDeleteMulti_SP] @TradingSystemIdsToDelete

	DELETE FROM
		[dbo].[TradingSystemMonitoringPage]
	WHERE
		TradingSystemMonitoringPageId = @TradingSystemMonitoringPageId
END		

GO
ALTER AUTHORIZATION ON [dbo].[DeleteTradingSystemMonitoringPages_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[DeleteTradingSystemMonitoringPages_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[DisableExternalOrdersTransmission_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[DisableExternalOrdersTransmission_SP] 
	@SwitchName NVARCHAR(64),
	@ChangedBy NVARCHAR(256),
	@ChangedFrom NVARCHAR(256),
	@Reason NVARCHAR(MAX) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SwitchId INT 
	SELECT @SwitchId = [Id] FROM [dbo].[ExternalOrdersTransmissionSwitch] WHERE [SwitchName] = @SwitchName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Switch not found in DB: %s', @SwitchName, 16, 2) WITH SETERROR
	END

	SET @Reason = ISNULL(@Reason, 'Changed per request by ' + @ChangedBy)

	BEGIN TRANSACTION

	INSERT INTO [dbo].[ExternalOrdersTransmissionSwitchHistory]
           ([ExternalOrdersTransmissionSwitchId]
           ,[TransmissionEnabled]
           ,[Changed]
           ,[ChangedBy]
           ,[ChangedFrom]
		   ,[Reason])
     VALUES
           (@SwitchId
           ,0
           ,GETUTCDATE()
           ,@ChangedBy
		   ,@ChangedFrom
		   ,@Reason)

	UPDATE [dbo].[ExternalOrdersTransmissionSwitch]
	SET [TransmissionEnabled] = 0
	WHERE [Id] = @SwitchId

	COMMIT TRANSACTION
END

GO
ALTER AUTHORIZATION ON [dbo].[DisableExternalOrdersTransmission_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[DisableExternalOrdersTransmission_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[DisableExternalOrdersTransmission_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[EnableExternalOrdersTransmission_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EnableExternalOrdersTransmission_SP] 
	@SwitchName NVARCHAR(64),
	@ChangedBy NVARCHAR(256),
	@ChangedFrom NVARCHAR(256)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SwitchId INT 
	SELECT @SwitchId = [Id] FROM [dbo].[ExternalOrdersTransmissionSwitch] WHERE [SwitchName] = @SwitchName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Switch not found in DB: %s', @SwitchName, 16, 2) WITH SETERROR
	END

	BEGIN TRANSACTION
	BEGIN TRY

		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		INSERT INTO [dbo].[ExternalOrdersTransmissionSwitchHistory]
			   ([ExternalOrdersTransmissionSwitchId]
			   ,[TransmissionEnabled]
			   ,[Changed]
			   ,[ChangedBy]
			   ,[ChangedFrom])
		 VALUES
			   (@SwitchId
			   ,1
			   ,GETUTCDATE()
			   ,@ChangedBy
			   ,@ChangedFrom)

		UPDATE [dbo].[ExternalOrdersTransmissionSwitch]
		SET [TransmissionEnabled] = 1
		WHERE [Id] = @SwitchId

		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
	
END

GO
ALTER AUTHORIZATION ON [dbo].[EnableExternalOrdersTransmission_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[EnableExternalOrdersTransmission_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[FlipSystemsTradingControl_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FlipSystemsTradingControl_SP]
(
	@GoFlatOnly [bit]
)
AS
BEGIN

	IF @GoFlatOnly = 1
	BEGIN
		UPDATE [dbo].[SystemsTradingControl]
		SET [GoFlatOnly] = @GoFlatOnly
	END
	ELSE
	BEGIN
		BEGIN TRANSACTION
		BEGIN TRY
		
			UPDATE [dbo].[SystemsTradingControl]
			SET [GoFlatOnly] = @GoFlatOnly
			
			exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
			
			COMMIT TRANSACTION
		
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION;
			THROW;
		END CATCH
		
	END
	
	exec [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]
END

GO
ALTER AUTHORIZATION ON [dbo].[FlipSystemsTradingControl_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[FlipSystemsTradingControl_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[FlipSystemsTradingControl_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[ForceClaimSessionsForInstance_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ForceClaimSessionsForInstance_SP]( 
	@InstanceName varchar(50),
	@Counterparty char(3)
	)
AS
BEGIN
	DECLARE @CounterpartyId TINYINT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @Counterparty

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Counterparty not found in DB: %s', 16, 2, @Counterparty) WITH SETERROR
	END

	--First force release the session 
	DELETE FROM [dbo].[IntegratorInstanceSessionClaim]
	WHERE CounterpartyId = @CounterpartyId
	
	--If there is no replacement instance then we are done
	IF @InstanceName IS NOT NULL
	BEGIN
		DECLARE @InstanceId TINYINT
		
		SELECT @InstanceId = Id FROM [dbo].[IntegratorInstance] WHERE Name = @InstanceName

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'InstanceName not found in DB: %s', 16, 2, @InstanceName) WITH SETERROR
		END
		
		--Then claim it
		INSERT INTO [dbo].[IntegratorInstanceSessionClaim] (IntegratorInstanceId, CounterpartyId)
		VALUES (@InstanceId, @CounterpartyId)
	END
END

GO
ALTER AUTHORIZATION ON [dbo].[ForceClaimSessionsForInstance_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[ForceClaimSessionsForInstance_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GenerateUpdateSettingsScripts_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GenerateUpdateSettingsScripts_SP] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 'UPDATE [dbo].[Settings] SET [SettingsXml] = ''' + 
	CONVERT(VARCHAR(MAX), [SettingsXml]) +
	''', [LastUpdated] = GETUTCDATE() WHERE [Name] = ''' + 
	CONVERT(VARCHAR(MAX), [Name]) + ''' AND [Id] = ' + CONVERT(VARCHAR(5), [Id])
	FROM [dbo].[Settings]

	--where name like '%session%'

END


GO
ALTER AUTHORIZATION ON [dbo].[GenerateUpdateSettingsScripts_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetActiveSystemsCountForPagePlan_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetActiveSystemsCountForPagePlan_SP]
(
	@PageId [int]
)
AS
BEGIN
	SELECT
		--COUNT(action.TradingSystemId) AS ConfiguredSystems,
		COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems
	FROM 
		[dbo].[TradingSystemMonitoringPage] page
		INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemGroupId] = page.[TradingSystemGroupId] AND system.[TradingSystemTypeId] = page.[TradingSystemTypeId]
		INNER JOIN [dbo].[TradingSystemActions] action ON action.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON action.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Symbol] beginSymbol ON page.[BeginSymbolId] = beginSymbol.Id
		INNER JOIN [dbo].[Symbol] endSymbol ON page.[EndSymbolId] = endSymbol.Id
	WHERE
		page.[TradingSystemMonitoringPageId] = @PageId
		AND	symbol.[Name] BETWEEN beginSymbol.[Name] AND endSymbol.[Name]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetActiveSystemsCountForPagePlan_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetActiveSystemsCountForPagePlan_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetCertificateFile_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCertificateFile_SP] 
	@FileName NVARCHAR(100)
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		FileContent
	FROM
		[dbo].[CertificateFile]
	WHERE
		FileName = @FileName
END

GO
ALTER AUTHORIZATION ON [dbo].[GetCertificateFile_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetCertificateFile_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetClaimedSessionsForInstance_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetClaimedSessionsForInstance_SP]( 
	@InstanceName varchar(50)
	)
AS
BEGIN
	SELECT 
		ctp.CounterpartyCode
	FROM 
		[dbo].[IntegratorInstanceSessionClaim] map
		INNER JOIN [dbo].[Counterparty] ctp on ctp.CounterpartyId = map.CounterpartyId
		INNER JOIN [dbo].[IntegratorInstance] instance on instance.Id = map.IntegratorInstanceId
	WHERE
		instance.Name = @InstanceName	
END

GO
ALTER AUTHORIZATION ON [dbo].[GetClaimedSessionsForInstance_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetClaimedSessionsForInstance_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetCommandItems_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[GetCommandItems_SP] (
	@MaxCount INT
	)
AS
BEGIN
	DECLARE @TempCommandIds TABLE (
		Id INT
		,CommandText NVARCHAR(MAX)
		)

    DECLARE @CurrentUTCTime DATETIME

	SET @CurrentUTCTime = (SELECT GETUTCDATE())

	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

	BEGIN TRANSACTION

	INSERT INTO @TempCommandIds (
		Id
		,CommandText
		)
	SELECT TOP (@MaxCount) 
		Id, CommandText
	FROM 
		[dbo].[Command] cmd WITH (TABLOCKX)
		INNER JOIN [dbo].[NewCommand] newcmd WITH (TABLOCKX) ON cmd.id = newcmd.CommandId

	DELETE 
		newcmd
	FROM 
		[dbo].[NewCommand] newcmd
		INNER JOIN @TempCommandIds tmpcmd ON newcmd.CommandId = tmpcmd.Id

	COMMIT TRANSACTION

	INSERT INTO CommandStatus (
		CommandId
		,Status
		,Time
		)
	SELECT
		Id
		,N'Acquired'
		,@CurrentUTCTime
	FROM @TempCommandIds

	SELECT 
		Id
		,CommandText
	FROM @TempCommandIds
END

GO
ALTER AUTHORIZATION ON [dbo].[GetCommandItems_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetCommandItems_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetCommisionsList_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCommisionsList_SP] 
AS
BEGIN
	SELECT 
		counteprarty.CounterpartyCode AS Counterparty
		,[CommissionPricePerMillion]
	FROM 
		[dbo].[CommissionsPerCounterparty] commison
		INNER JOIN [dbo].[Counterparty] counteprarty ON commison.CounterpartyId = counteprarty.CounterpartyId
	WHERE
		counteprarty.Active = 1
END

GO
ALTER AUTHORIZATION ON [dbo].[GetCommisionsList_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetCommisionsList_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetCounterpartyMonitorSettingsList_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCounterpartyMonitorSettingsList_SP]
AS
BEGIN
	SELECT 
		[Id]
		,[FriendlyName]
	FROM 
		[dbo].[Settings]
	WHERE
		[Name] = 'Kreslik.Integrator.DiagnosticsDashboard.exe'
END

GO
ALTER AUTHORIZATION ON [dbo].[GetCounterpartyMonitorSettingsList_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetCounterpartyMonitorSettingsList_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetCounterpartyMonitorSettingsName_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCounterpartyMonitorSettingsName_SP] 
	@EnvironmentName NVARCHAR(100)
AS
BEGIN

	SELECT
		sett.FriendlyName
	FROM
		[dbo].[IntegratorEnvironment] envint 
		INNER JOIN [dbo].[IntegratorEnvironmentToSettingsMap] map ON envint.Id = map.IntegratorEnvironmentId
		INNER JOIN [dbo].[Settings] sett ON sett.id = map.SettingsId
	WHERE
		envint.Name = @EnvironmentName
		AND sett.Name = 'Kreslik.Integrator.DiagnosticsDashboard.exe'
END

GO
ALTER AUTHORIZATION ON [dbo].[GetCounterpartyMonitorSettingsName_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetCounterpartyMonitorSettingsName_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetDailyNetInfo_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDailyNetInfo_SP]
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

----------------
declare @TodayUtc DATE = GetUtcDate();

declare @NopTable table(
Symbol nchar(7),
NopBasePol decimal(18,0), 
NopTermPol decimal (18,9),
DealsNum int, 
LastDealUtc datetime2,
PnlTermPol decimal (18,9),
PnlTermPolInUsd decimal (18,9), 
VolumeInCcy1 decimal (18,9), 
VolumeInUsd decimal (18,9));

INSERT into @NopTable 
exec [Integrator].[dbo].[GetStatisticsPerPair_Daily_SP] @Day=@TodayUtc;
----------------

select 

nt.Symbol'symbol',
nt.VolumeInUsd/1000000'volumeUsdM',
nt.PnlTermPolInUsd'pnlGrossUsd',
comm.commissionsUsd'commUsd',
nt.PnlTermPolInUsd-comm.commissionsUSD'pnlNetUsd',
nt.PnlTermPolInUsd/(nt.VolumeInUsd/1000000)'pnlGrossUsdPerMUsd',
comm.commissionsUSD/(nt.VolumeInUsd/1000000)'commUsdPerMUsd',
(nt.PnlTermPolInUsd-comm.commissionsUSD)/(nt.VolumeInUsd/1000000)'pnlNetUsdPerMUsd',
nt.DealsNum'dealsCount',
nt.VolumeInUsd/nt.DealsNum/1000000'avgDealMUsd',
nt.PnlTermPolInUsd/nt.DealsNum'pnlGrossUsdPerDeal',
comm.commissionsUsd/nt.DealsNum'commUsdPerDeal',
(nt.PnlTermPolInUsd-comm.commissionsUSD)/nt.DealsNum'pnlNetUsdPerDeal'

from @NopTable nt join (
select s.Name,

-- commissions in USD:
--sum(termCcy.LastKnownUsdConversionMultiplier*(abs(AmountBasePolExecuted*Price*com.CommissionPricePerMillion/1000000)))'commissionsUsd',
sum(baseCcy.LastKnownUsdConversionMultiplier*(abs(AmountBasePolExecuted*com.CommissionPricePerMillion/1000000)))'commissionsUsd'

from 

(
select SymbolId, CounterpartyId, AmountBasePolExecuted 
from DealExternalExecuted_DailyView with(nolock)
union all
select SymbolId, CounterpartyId, IntegratorExecutedAmountBasePol from OrderExternalIncomingRejectable_Daily with(nolock) where ValueDate is not null -- executed rejectable deals only
) dee

join Symbol s with(nolock) on dee.Symbolid=s.Id 
join Counterparty c with(nolock) on dee.CounterpartyId=c.CounterpartyId
join CommissionsPerCounterparty com with(nolock) on c.CounterpartyId=com.CounterpartyId
--join Currency termCcy with(nolock) on s.QuoteCurrencyId=termCcy.CurrencyID
join Currency baseCcy  with(nolock) on s.BaseCurrencyId=baseCcy.CurrencyID
group by s.name) comm on nt.Symbol=comm.Name

order by nt.Symbol;

END
GO
ALTER AUTHORIZATION ON [dbo].[GetDailyNetInfo_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetDateRangeInfoByCounterparty_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDateRangeInfoByCounterparty_SP]
(
	@StartDateUtc DATE,
	@EndDateUtc DATE
)
AS
BEGIN
SET NOCOUNT ON;

---------------------------------------------------
--declare @LastTickOfDay time = '23:59:59.9999999';

--declare @StartUtc datetime2 = @StartDateUtc;
--declare @EndUtc datetime2 = DATEADD(day, DATEDIFF(day,'19000101',@EndDateUtc), CAST(@LastTickOfDay AS DATETIME2(7)));
---------------------------------------------------

-- TODO: commisions must be based on date

declare @StartDateUtcInternal DATE = @StartDateUtc;
declare @EndDateUtcInternal DATE = @EndDateUtc;

--------------------------------------------------
-- MEDIAN BEGIN:

declare @DealAndRejLatencies table(CounterpartyID tinyint, RtLatencyMillisecond decimal(38,10));

insert into @DealAndRejLatencies

select dee.CounterpartyId,
dbo.ConvertDateTime2ToTimeOfDayMicroseconds(dee.Latency)/1000'latencyMs'
from DealsExecuted_DateUtcRange_Tbl(@StartDateUtcInternal, @EndDateUtcInternal) dee
--where dee.IntegratorReceivedExecutionReportUtc between @StartDateTimeUtc and @EndDateTimeUtc
where dee.Latency > '00:00:00.0000000'
union all
select rej.CounterpartyId,
dbo.ConvertDateTime2ToTimeOfDayMicroseconds(rej.OrderSentToExecutionReportReceivedLatency)/1000'latencyMs'
from DealExternalRejected rej  with(nolock)
where rej.DatePartIntegratorReceivedExecutionReportUtc between @StartDateUtcInternal and @EndDateUtcInternal
--where rej.IntegratorReceivedExecutionReportUtc between @StartDateTimeUtc and @EndDateTimeUtc
and rej.OrderSentToExecutionReportReceivedLatency > '00:00:00.0000000';

declare @GroupedMedianLatencies table(CounterpartyID tinyint, MedianRtLatencyMs decimal(38,10));

WITH C AS
(
SELECT CounterpartyId,
COUNT(*) AS cnt,
(COUNT(*) - 1) / 2 AS offset_val,
2 - COUNT(*) % 2 AS fetch_val
FROM @DealAndRejLatencies
GROUP BY CounterpartyId
)

insert into @GroupedMedianLatencies

SELECT CounterpartyId, AVG(1. * RtLatencyMillisecond) AS MedianRtLatencyMs
FROM C
CROSS APPLY ( SELECT O.RtLatencyMillisecond
FROM @DealAndRejLatencies AS O
where O.CounterpartyId = C.CounterpartyId
order by O.RtLatencyMillisecond
OFFSET C.offset_val ROWS FETCH NEXT C.fetch_val ROWS ONLY ) AS A
GROUP BY CounterpartyId;

-- MEDIAN END
--------------------------------------------------


SELECT

allCounterparties.CounterpartyCode'Cparty',

coalesce(DealsAndRejections.volumeUsdM,0)'VolumeUsdM',

coalesce(DealsAndRejections.commissionsUsdPb,0)'CommissionsUsdPb',
coalesce(DealsAndRejections.commissionsUsdCparty,0)'CommissionsUsdCparty',
coalesce(DealsAndRejections.commissionsUsdSum,0)'CommissionsUsdSum',

coalesce(DealsAndRejections.dealsCount,0)'DealsCount',
coalesce(DealsAndRejections.dealtSymbolsCount,0)'DealtSymbolsCount',
DealsAndRejections.minDealLatencyMs'MinDealLatencyMs',
DealsAndRejections.avgDealLatencyMs'AvgDealLatencyMs',
coalesce(DealsAndRejections.avgDealUsdM,0)'AvgDealUsdM',

coalesce(DealsAndRejections.rejectionsCount,0)'RejectionsCount',
coalesce(DealsAndRejections.rejectedSymbolsCount,0)'RejectedSymbolsCount',
DealsAndRejections.minRejectionLatencyMs'MinRejectionLatencyMs',
DealsAndRejections.avgRejectionLatencyMs'AvgRejectionLatencyMs',
DealsAndRejections.rejectionsPct'RejectionPct',

DealsAndRejectionsMedianLatency.DealsAndRejectionsMedianLatencyMs


----------------------------------------------------

from
(select cp.CounterpartyCode from Counterparty cp with(nolock) where cp.Active=1) allCounterparties
left join (
select * from(
select
coalesce(deals.CounterpartyCode,rejections.CounterpartyCode)'cParty',
coalesce(deals.dealtSymbolsCount,0)'dealtSymbolsCount',
coalesce(deals.volumeUsdM,0)'volumeUsdM',
coalesce(deals.commissionsUsdPb,0)'commissionsUsdPb',
coalesce(deals.commissionsUsdCparty,0)'commissionsUsdCparty',
coalesce(deals.commissionsUsdSum,0)'commissionsUsdSum',
coalesce(deals.dealsCount,0)'dealsCount',
deals.minDealLatencyMicroseconds/1000.0'minDealLatencyMs',
deals.avgDealLatencyMicroseconds/1000.0'avgDealLatencyMs',
coalesce(deals.avgDealUsdM,0)'avgDealUsdM',
coalesce(rejections.rejectionsCount,0)'rejectionsCount',
coalesce(rejections.rejectedSymbolsCount,0)'rejectedSymbolsCount',
rejections.minRejectionLatencyMicroseconds/1000.0'minRejectionLatencyMs',
rejections.avgRejectionLatencyMicroseconds/1000.0'avgRejectionLatencyMs',
CASE WHEN
	coalesce(rejections.rejectionsCount,0) + coalesce(deals.dealsCount,0) > 0 -- sum of trade attempts
	THEN coalesce(rejections.rejectionsCount,0)*100.0 / (coalesce(rejections.rejectionsCount,0) + coalesce(deals.dealsCount,0))
	ELSE NULL -- there were no attempts to trade, so the rejectionPct would not express anything 
	END 'rejectionsPct'
from

(select 
c.CounterpartyCode,
count(distinct SymbolId)'dealtSymbolsCount',
sum(baseCcy.LastKnownUsdConversionMultiplier*(abs(AmountBasePolExecuted)))/1000000'volumeUsdM',
sum(baseCcy.LastKnownUsdConversionMultiplier*(abs(AmountBasePolExecuted*com.CommissionPricePerMillion)))/1000000'commissionsUsdSum',
--sum(baseCcy.LastKnownUsdConversionMultiplier*(abs(AmountBasePolExecuted*(CASE WHEN c.CounterpartyCode='CTI' THEN 0 ELSE 5.5 END))))/1000000'commissionsUsdPb',
sum(baseCcy.LastKnownUsdConversionMultiplier*(abs(AmountBasePolExecuted*com.PbCommissionPpm)))/1000000'commissionsUsdPb',
--sum(baseCcy.LastKnownUsdConversionMultiplier*(abs(AmountBasePolExecuted*(CASE WHEN c.CounterpartyCode='CTI' THEN 0 ELSE com.CommissionPricePerMillion-5.5 END))))/1000000'commissionsUsdCparty',
sum(baseCcy.LastKnownUsdConversionMultiplier*(abs(AmountBasePolExecuted*com.CounterpartyCommissionPpm)))/1000000'commissionsUsdCparty',
count(*)'dealsCount',
min(CASE WHEN dr.Latency = '00:00:00.0000000' THEN NULL ELSE dbo.ConvertDateTime2ToTimeOfDayMicroseconds(dr.Latency) END)'minDealLatencyMicroseconds',
avg(CASE WHEN dr.Latency = '00:00:00.0000000' THEN NULL ELSE dbo.ConvertDateTime2ToTimeOfDayMicroseconds(dr.Latency) END)'avgDealLatencyMicroseconds',
CASE WHEN count(*) > 0 THEN sum(baseCcy.LastKnownUsdConversionMultiplier*(abs(AmountBasePolExecuted))) / count(*) / 1000000 ELSE 0 END 'avgDealUsdM'
from DealsExecuted_DateUtcRange_Tbl(@StartDateUtcInternal, @EndDateUtcInternal) dr
join Counterparty c with(nolock) on dr.CounterpartyId=c.CounterpartyId
join Symbol s with(nolock) on dr.SymbolId=s.Id
join Currency BaseCcy with(nolock) on s.BaseCurrencyId=BaseCcy.CurrencyID
join CommissionsPerCounterparty com with(nolock) on c.CounterpartyId=com.CounterpartyId
group by c.CounterpartyCode) deals

full outer join

(select c.CounterpartyCode, count(*)'rejectionsCount',count(distinct SymbolId)'rejectedSymbolsCount',
min(CASE WHEN dr.Latency = '00:00:00.0000000' THEN NULL ELSE dbo.ConvertDateTime2ToTimeOfDayMicroseconds(dr.Latency) END)'minRejectionLatencyMicroseconds',
avg(CASE WHEN dr.Latency = '00:00:00.0000000' THEN NULL ELSE dbo.ConvertDateTime2ToTimeOfDayMicroseconds(dr.Latency) END)'avgRejectionLatencyMicroseconds'
from Rejections_DateUtcRange_Tbl(@StartDateUtcInternal, @EndDateUtcInternal) dr
join Counterparty c with(nolock) on dr.CounterpartyId=c.CounterpartyId
join Symbol s with(nolock) on dr.SymbolId=s.Id
join Currency BaseCcy with(nolock) on s.BaseCurrencyId=BaseCcy.CurrencyID
group by c.CounterpartyCode) rejections

on deals.CounterpartyCode=rejections.CounterpartyCode) dandr)

DealsAndRejections on allCounterparties.CounterpartyCode=DealsAndRejections.cParty

left join 
-- MEDIANS
(select c.CounterpartyCode,gmed.MedianRtLatencyMs'DealsAndRejectionsMedianLatencyMs'
from @GroupedMedianLatencies gmed
join Counterparty c with(nolock) on c.CounterpartyId=gmed.CounterpartyID
) DealsAndRejectionsMedianLatency 
-- MEDIANS

on DealsAndRejectionsMedianLatency.CounterpartyCode=allCounterparties.CounterpartyCode


order by allCounterparties.CounterpartyCode

-- we removed the bogus '00:00:00.0000000' latency values from statistics as it skews the results
END
GO
ALTER AUTHORIZATION ON [dbo].[GetDateRangeInfoByCounterparty_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetDateRangeInfoByTradeDate_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDateRangeInfoByTradeDate_SP]
(
	@StartDateUtc DATE,
	@EndDateUtc DATE
)
AS
BEGIN
SET NOCOUNT ON;

declare @StartDateUtcInternal DATE = @StartDateUtc;
declare @EndDateUtcInternal DATE = @EndDateUtc;

select
result.TradeDate'tradeDate',
datename(WEEKDAY,result.TradeDate)'dayOfWeek',
sum(result.VolumeInUsd)/1000000'volumeUsdM',
sum(result.PnlTermPolInUsd)'pnlGrossUsd',
sum(result.commissionsUsdSum)'commUsd',
sum(result.pnlNetUsd)'pnlNetUsd',
sum(result.PnlTermPolInUsd)/(sum(result.VolumeInUsd)/1000000)'pnlGrossUsdPerMUsd',
sum(result.commissionsUsdSum)/(sum(result.VolumeInUsd)/1000000)'commUsdPerMUsd',
sum(result.pnlNetUsd)/(sum(result.VolumeInUsd/1000000))'pnlNetUsdPerMUsd',
sum(result.DealsNum)'dealsCount',
sum(result.VolumeInUsd)/sum(result.DealsNum)/1000000'avgDealMUsd',
sum(result.PnlTermPolInUsd)/sum(result.DealsNum)'pnlGrossUsdPerDeal',
sum(result.commissionsUsdSum)/sum(result.DealsNum)'commUsdPerDeal',
sum(result.pnlNetUsd)/sum(result.DealsNum)'pnlNetUsdPerDeal'

from

(
	select 

	nt.TradeDate,
	nt.Symbol,
	nt.VolumeInUsd,
	nt.PnlTermPolInUsd,
	comm.commissionsUsdSum,
	nt.PnlTermPolInUsd-comm.commissionsUsdSum'pnlNetUsd',
	nt.DealsNum

	from 

	Integrator.dbo.PositionsPnlVolumeByTradeDateAndSymbol_DateUtcRange_Tbl(@StartDateUtcInternal, @EndDateUtcInternal) nt 
	join Integrator.dbo.CommissionsByTradeDateAndSymbol_DateUtcRange_Tbl(@StartDateUtcInternal, @EndDateUtcInternal) comm 
	on nt.Symbol=comm.Symbol and nt.TradeDate=comm.TradeDate

) result

group by result.TradeDate
order by result.TradeDate

END
GO
ALTER AUTHORIZATION ON [dbo].[GetDateRangeInfoByTradeDate_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetDateRangeInfoByTradeDateAndTradingSystemType_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDateRangeInfoByTradeDateAndTradingSystemType_SP]
(
	@StartDateUtc DATE,
	@EndDateUtc DATE,
	@TradingSystemType varchar(100)
)
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

declare @StartDateUtcInternal DATE = @StartDateUtc;
declare @EndDateUtcInternal DATE = @EndDateUtc;
declare @TradingSystemTypeInternal varchar(100) = @TradingSystemType;

select
result.TradeDate'tradeDate',
datename(WEEKDAY,result.TradeDate)'dayOfWeek',
sum(result.VolumeInUsd)/1000000'volumeUsdM',
sum(result.PnlTermPolInUsd)'pnlGrossUsd',
sum(result.CommissionsUsdSum)'commUsd',
sum(result.pnlNetUsd)'pnlNetUsd',
sum(result.PnlTermPolInUsd)/(sum(result.VolumeInUsd)/1000000)'pnlGrossUsdPerMUsd',
sum(result.CommissionsUsdSum)/(sum(result.VolumeInUsd)/1000000)'commUsdPerMUsd',
sum(result.pnlNetUsd)/(sum(result.VolumeInUsd/1000000))'pnlNetUsdPerMUsd',
sum(result.DealsNum)'dealsCount',
sum(result.VolumeInUsd)/sum(result.DealsNum)/1000000'avgDealMUsd',
sum(result.PnlTermPolInUsd)/sum(result.DealsNum)'pnlGrossUsdPerDeal',
sum(result.CommissionsUsdSum)/sum(result.DealsNum)'commUsdPerDeal',
sum(result.pnlNetUsd)/sum(result.DealsNum)'pnlNetUsdPerDeal'

from

(
select 
nt.TradeDate,
nt.Symbol,
nt.VolumeInUsd,
nt.PnlTermPolInUsd,
comm.CommissionsUsdSum,
nt.PnlTermPolInUsd-comm.CommissionsUsdSum'pnlNetUsd',
nt.DealsNum

from 
Integrator.dbo.SingleTradingSystemInfo_PositionsPnlVolumeByTradeDateAndSymbol_DateUtcRange_Tbl(@StartDateUtcInternal, @EndDateUtcInternal, @TradingSystemTypeInternal) nt 
join SingleTradingSystemInfo_Commissions_DateUtcRange_Tbl(@StartDateUtcInternal, @EndDateUtcInternal, @TradingSystemTypeInternal) comm 
on nt.Symbol=comm.Symbol and nt.TradeDate=comm.TradeDate) result

group by result.TradeDate
order by result.TradeDate

END
GO
ALTER AUTHORIZATION ON [dbo].[GetDateRangeInfoByTradeDateAndTradingSystemType_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetDateRangeInfoByTradingSystemType_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDateRangeInfoByTradingSystemType_SP]
(
	@StartDateUtc DATE,
	@EndDateUtc DATE
)
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

------------
declare @StartDateUtcInternal DATE = @StartDateUtc;
declare @EndDateUtcInternal DATE = @EndDateUtc;
------------


declare @LastTickOfDay time = '23:59:59.9999999';

declare @StartUtc datetime2 = @StartDateUtcInternal;
declare @EndUtc datetime2 = DATEADD(day, DATEDIFF(day,'19000101',@EndDateUtcInternal), CAST(@LastTickOfDay AS DATETIME2(7)));





select *
from TradingSystemsTypeInfo_UtcRange_Tbl(
@StartUtc,
@EndUtc)

END
GO
ALTER AUTHORIZATION ON [dbo].[GetDateRangeInfoByTradingSystemType_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetDateRangeNetInfo_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDateRangeNetInfo_SP]
(
	@StartDateUtc DATE,
	@EndDateUtc DATE

	--@StartDateUtc datetime2,
	--@EndDateUtc datetime2
)
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

declare @StartDateUtcInternal DATE = @StartDateUtc;
declare @EndDateUtcInternal DATE = @EndDateUtc;

select 

nt.Symbol'symbol',
nt.VolumeInUsd/1000000'volumeUsdM',
nt.PnlTermPolInUsd'pnlGrossUsd',
comm.commissionsUsd'commUsd',
nt.PnlTermPolInUsd-comm.commissionsUSD'pnlNetUsd',
nt.PnlTermPolInUsd/(nt.VolumeInUsd/1000000)'pnlGrossUsdPerMUsd',
comm.commissionsUSD/(nt.VolumeInUsd/1000000)'commUsdPerMUsd',
(nt.PnlTermPolInUsd-comm.commissionsUSD)/(nt.VolumeInUsd/1000000)'pnlNetUsdPerMUsd',
nt.DealsNum'dealsCount',
nt.VolumeInUsd/nt.DealsNum/1000000'avgDealMUsd',
nt.PnlTermPolInUsd/nt.DealsNum'pnlGrossUsdPerDeal',
comm.commissionsUsd/nt.DealsNum'commUsdPerDeal',
(nt.PnlTermPolInUsd-comm.commissionsUSD)/nt.DealsNum'pnlNetUsdPerDeal'

from 
(
	
		SELECT
			Symbol.Name AS Symbol,
			positions.NopBasePol,
			positions.NopTermPol,
					positions.dealsNum,
			positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * positions.NopBasePol AS PnlTermPol,
			(positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * positions.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
			positions.VolumeInCcy1 AS VolumeInCcy1,
			positions.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd
		FROM
			(
				SELECT
					deal.SymbolId AS SymbolId,
					count(*)'dealsNum',
					SUM(deal.AmountBasePolExecuted) AS NopBasePol,
					SUM(-deal.AmountBasePolExecuted* deal.Price) AS NopTermPol,
					SUM(ABS(deal.AmountBasePolExecuted)) AS VolumeInCcy1
				FROM 
				DealsExecuted_DateUtcRange_Tbl(@StartDateUtcInternal, @EndDateUtcInternal) deal
				GROUP BY
					deal.SymbolId
			) positions

		INNER JOIN Symbol symbol  with(nolock) ON positions.SymbolId = symbol.Id
		INNER JOIN Currency termCurrency  with(nolock) ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		INNER JOIN Currency baseCurrency  with(nolock) ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
	
)

nt join (
select s.Name,

sum(baseCcy.LastKnownUsdConversionMultiplier*(abs(dee.AmountBasePolExecuted*com.CommissionPricePerMillion/1000000)))'commissionsUsd'

from 

DealsExecuted_DateUtcRange_Tbl(@StartDateUtcInternal, @EndDateUtcInternal) dee

join Symbol s with(nolock) on dee.Symbolid=s.Id 
join Counterparty c with(nolock) on dee.CounterpartyId=c.CounterpartyId
join CommissionsPerCounterparty com  with(nolock) on c.CounterpartyId=com.CounterpartyId
join Currency baseCcy  with(nolock) on s.BaseCurrencyId=baseCcy.CurrencyID

group by s.name) comm on nt.Symbol=comm.Name

order by nt.Symbol;

END

GO
ALTER AUTHORIZATION ON [dbo].[GetDateRangeNetInfo_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetDateRangeVolume_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDateRangeVolume_SP]
(
	@StartDateUtc DATE,
	@EndDateUtc DATE
)
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

select 

dee.DatePartIntegratorReceivedExecutionReportUtc'TradeDate',
sum(abs(dee.AmountBasePolExecuted)*baseCurrency.LastKnownUsdConversionMultiplier)/1000000'VolumeUsdM'

from

DealExternalExecuted dee with(nolock)
join Symbol s with(nolock) on dee.SymbolId=s.Id
join Currency baseCurrency with(nolock) on s.BaseCurrencyId=baseCurrency.CurrencyID

where dee.DatePartIntegratorReceivedExecutionReportUtc between @StartDateUtc and @EndDateUtc

group by dee.DatePartIntegratorReceivedExecutionReportUtc

order by dee.DatePartIntegratorReceivedExecutionReportUtc

END
GO
ALTER AUTHORIZATION ON [dbo].[GetDateRangeVolume_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetDbMappingsForCounterparty_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDbMappingsForCounterparty_SP] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[CounterpartyId] AS DbId, 
		[CounterpartyCode] AS EnumName
	FROM
		[dbo].[Counterparty]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetDbMappingsForCounterparty_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetDbMappingsForCounterparty_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetDbMappingsForDealDirection_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetDbMappingsForDealDirection_SP] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CAST([Id] AS TINYINT) AS DbId, 
		[Name] AS EnumName
	FROM
		[dbo].[DealDirection]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetDbMappingsForDealDirection_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetDbMappingsForDealDirection_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetDbMappingsForOrderTimeInForce_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDbMappingsForOrderTimeInForce_SP] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[OrderTimeInForceId] AS DbId, 
		[OrderTimeInForceName] AS EnumName
	FROM
		[dbo].[OrderTimeInForce]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetDbMappingsForOrderTimeInForce_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetDbMappingsForOrderTimeInForce_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetDbMappingsForOrderType_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDbMappingsForOrderType_SP] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[OrderTypeId] AS DbId, 
		[OrderTypeName] AS EnumName
	FROM
		[dbo].[OrderType]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetDbMappingsForOrderType_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetDbMappingsForOrderType_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetDbMappingsForPegType_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetDbMappingsForPegType_SP] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CAST([PegTypeId] AS TINYINT) AS DbId, 
		[PegTypeName] AS EnumName
	FROM
		[dbo].[PegType]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetDbMappingsForPegType_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetDbMappingsForPegType_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetDbMappingsForSymbols_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDbMappingsForSymbols_SP] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[Id] AS DbId, 
		[Name] AS EnumName
	FROM
		[dbo].[Symbol]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetDbMappingsForSymbols_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetDbMappingsForSymbols_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetDealsAndRejections_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDealsAndRejections_SP]
	@FxPairNoSlash char(6),
	@StartDateTimeUtc DateTime2,
	@EndDateTimeUtc DateTime2
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

declare @SymbolId tinyint = (select Id from Symbol sym  with(nolock) where sym.ShortName=@FxPairNoSlash);

SELECT

		dee.IntegratorSentExternalOrderUtc,
		dee.CounterpartySentExecutionReportUtc,
		dee.IntegratorReceivedExecutionReportUtc,
		dee.Price,
		dee.AmountBasePolExecuted'SizeBasePol',
		dealCounterparty.CounterpartyCode,
		dir.Name'DealDirection',
		'Deal' 'Type',
		fs.FlowSideName'FlowSide',
		NULL'RejectionReason',
		dee.CounterpartyTransactionId'CounterpartyTransactionId',
		tsd.TradingSystemId,
		tradingSystemCounterparty.CounterpartyCode + ' ' + tst.SystemName'TradingSystemDescription',
		dee.QuoteReceivedToOrderSentInternalLatency,
		ts.TradingSystemGroupId'TradingSystemGroupId',
		tsGroup.TradingSystemGroupName'TradingSystemGroupName',
		dee.IntegratorExternalOrderPrivateId

FROM

		DealExternalExecuted dee with(nolock)
		join DealDirection dir with(nolock) on dee.DealDirectionId=dir.Id
		join Counterparty dealCounterparty  with(nolock) on dee.CounterpartyId=dealCounterparty.CounterpartyId
		left join FlowSide fs  with(nolock) on dee.FlowSideId=fs.FlowSideId
		left join dbo.TradingSystemDealsExecuted_All_Tbl() tsd on dee.InternalTransactionIdentity=tsd.InternalTransactionIdentity
		left join dbo.TradingSystem_All_Tbl() ts on tsd.TradingSystemId=ts.TradingSystemId
		left join TradingSystemType tst  with(nolock) on ts.TradingSystemTypeId=tst.TradingSystemTypeId
		left join Counterparty tradingSystemCounterparty  with(nolock) on tst.CounterpartyId=tradingSystemCounterparty.CounterpartyId
		left join dbo.TradingSystemsGroup_All_Tbl() tsGroup on tsGroup.TradingSystemGroupId=ts.TradingSystemGroupId

WHERE

		dee.IntegratorReceivedExecutionReportUtc between @StartDateTimeUtc and @EndDateTimeUtc
		and dee.SymbolId=@SymbolId

UNION ALL

SELECT
		rej.IntegratorSentExternalOrderUtc,
		rej.CounterpartySentExecutionReportUtc,
		rej.IntegratorReceivedExecutionReportUtc,
		rej.RejectedPrice, -- FIX: different name Price vs. RejectedPrice
		rej.AmountBasePolRejected'SizeBasePol',
		rejectionCounterparty.CounterpartyCode,
		dir.Name'DealDirection', -- FIX: this is a rejection, not a deal
		'Rejection' 'Type',
		NULL'FlowSide',
		rej.RejectionReason'RejectionReason',
		NULL'CounterpartyTransactionId',
		tsd.TradingSystemId,
		tradingSystemCounterparty.CounterpartyCode + ' ' + tst.SystemName'TradingSystemDescription',
		rej.QuoteReceivedToOrderSentInternalLatency,
		ts.TradingSystemGroupId'TradingSystemGroupId',
		tsGroup.TradingSystemGroupName'TradingSystemGroupName',
		rej.IntegratorExternalOrderPrivateId

FROM

		DealExternalRejected rej  with(nolock) 
		join DealDirection dir  with(nolock) on rej.DealDirectionId=dir.Id
		join Counterparty rejectionCounterparty  with(nolock) on rej.CounterpartyId=rejectionCounterparty.CounterpartyId
		left join dbo.TradingSystemDealsRejected_All_Tbl() tsd on rej.IntegratorExternalOrderPrivateId=tsd.IntegratorExternalOrderPrivateId
		left join dbo.TradingSystem_All_Tbl() ts on tsd.TradingSystemId=ts.TradingSystemId
		left join TradingSystemType tst  with(nolock) on ts.TradingSystemTypeId=tst.TradingSystemTypeId
		left join Counterparty tradingSystemCounterparty  with(nolock) on tst.CounterpartyId=tradingSystemCounterparty.CounterpartyId
		left join dbo.TradingSystemsGroup_All_Tbl() tsGroup on tsGroup.TradingSystemGroupId=ts.TradingSystemGroupId

WHERE

		rej.IntegratorReceivedExecutionReportUtc between  @StartDateTimeUtc and @EndDateTimeUtc
		and rej.SymbolId=@SymbolId

ORDER BY

		IntegratorReceivedExecutionReportUtc asc


END

GO
ALTER AUTHORIZATION ON [dbo].[GetDealsAndRejections_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetDealsAndRejectionsMedianLatencyByCounterparty_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDealsAndRejectionsMedianLatencyByCounterparty_SP]
(
	@StartDateUtc DATE,
	@EndDateUtc DATE
)
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements


declare @StartDateUtcInternal DATE = @StartDateUtc;
declare @EndDateUtcInternal DATE = @EndDateUtc;

declare @DealAndRejLatencies table(CounterpartyID tinyint, RtLatencyMillisecond decimal(38,10));

insert into @DealAndRejLatencies

select dee.CounterpartyId,
dbo.ConvertDateTime2ToTimeOfDayMicroseconds(dee.OrderSentToExecutionReportReceivedLatency)/1000'latencyMs'
from DealExternalExecuted dee with(nolock)
where dee.DatePartIntegratorReceivedExecutionReportUtc between @StartDateUtcInternal and @EndDateUtcInternal
--where dee.IntegratorReceivedExecutionReportUtc between @StartDateTimeUtc and @EndDateTimeUtc
and dee.OrderSentToExecutionReportReceivedLatency > '00:00:00.0000000'
union all
select rej.CounterpartyId,
dbo.ConvertDateTime2ToTimeOfDayMicroseconds(rej.OrderSentToExecutionReportReceivedLatency)/1000'latencyMs'
from DealExternalRejected rej  with(nolock)
where rej.DatePartIntegratorReceivedExecutionReportUtc between @StartDateUtcInternal and @EndDateUtcInternal
--where rej.IntegratorReceivedExecutionReportUtc between @StartDateTimeUtc and @EndDateTimeUtc
and rej.OrderSentToExecutionReportReceivedLatency > '00:00:00.0000000';

declare @GroupedMedianLatencies table(CounterpartyID tinyint, MedianRtLatencyMs decimal(38,10));

WITH C AS
(
SELECT CounterpartyId,
COUNT(*) AS cnt,
(COUNT(*) - 1) / 2 AS offset_val,
2 - COUNT(*) % 2 AS fetch_val
FROM @DealAndRejLatencies
GROUP BY CounterpartyId
)

insert into @GroupedMedianLatencies

SELECT CounterpartyId, AVG(1. * RtLatencyMillisecond) AS MedianRtLatencyMs
FROM C
CROSS APPLY ( SELECT O.RtLatencyMillisecond
FROM @DealAndRejLatencies AS O
where O.CounterpartyId = C.CounterpartyId
order by O.RtLatencyMillisecond
OFFSET C.offset_val ROWS FETCH NEXT C.fetch_val ROWS ONLY ) AS A
GROUP BY CounterpartyId;


select c.CounterpartyCode,gmed.MedianRtLatencyMs'DealsAndRejectionsMedianLatencyMs'
from @GroupedMedianLatencies gmed
join Counterparty c with(nolock) on c.CounterpartyId=gmed.CounterpartyID


END
GO
ALTER AUTHORIZATION ON [dbo].[GetDealsAndRejectionsMedianLatencyByCounterparty_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetDealsExecutedOnHotspot_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDealsExecutedOnHotspot_SP]
	@FxPairNoSlash char(6),
	@StartDateTime DateTime2,
	@EndDateTime DateTime2,
	@SortBySourceTimeStamps bit
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

declare @SymbolId tinyint = (select Id from Symbol sym where sym.ShortName=@FxPairNoSlash);

IF @SortBySourceTimeStamps=0

	BEGIN
		select 

		dee.CounterpartySentExecutionReportUtc,
		dee.IntegratorReceivedExecutionReportUtc,
		dee.Price,
		dir.Name'DealDirection'

		from 

		DealExternalExecuted dee
		join DealDirection dir on dee.DealDirectionId=dir.Id

		where 

		dee.CounterpartyId in (20, 21)
		and dee.IntegratorReceivedExecutionReportUtc between @StartDateTime and @EndDateTime
		and dee.SymbolId=@SymbolId

		order by 

		dee.IntegratorReceivedExecutionReportUtc asc
	END 

	ELSE

	BEGIN
		select 

		dee.CounterpartySentExecutionReportUtc,
		dee.IntegratorReceivedExecutionReportUtc,
		dee.Price,
		dir.Name'DealDirection'

		from 

		DealExternalExecuted dee
		join DealDirection dir on dee.DealDirectionId=dir.Id

		where 

		dee.CounterpartyId in (20, 21)
		and dee.IntegratorReceivedExecutionReportUtc between @StartDateTime and @EndDateTime
		and dee.SymbolId=@SymbolId

		order by 

		dee.CounterpartySentExecutionReportUtc asc
	END 


END

GO
ALTER AUTHORIZATION ON [dbo].[GetDealsExecutedOnHotspot_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetDetailsForRiskMgmtUser_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetDetailsForRiskMgmtUser_SP] 
	@username NVARCHAR(256)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @True BIT = 1

	SELECT 
		@True as Loged, usr.IsAdmin as IsAdmin, s.Name as RoleName
	FROM 
		[dbo].[KillSwitchUser] usr
		INNER JOIN [dbo].[KillSwitchRole] s ON usr.RoleId = s.Id
	WHERE
		usr.UserName = @username
END

GO
ALTER AUTHORIZATION ON [dbo].[GetDetailsForRiskMgmtUser_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetDetailsForRiskMgmtUser_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetEnvironmentExists_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetEnvironmentExists_SP]( 
	@EnvironmentName [nvarchar](100)
	)
AS
BEGIN
	SELECT 
		[Name] 
	FROM 
		[dbo].[IntegratorEnvironment]
	WHERE
		[Name] = @EnvironmentName
END

GO
ALTER AUTHORIZATION ON [dbo].[GetEnvironmentExists_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetEnvironmentExists_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetEnvironmentSettingsSet_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetEnvironmentSettingsSet_SP] 
	@EnvironmentName NVARCHAR(100)
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		COALESCE(mandatorySettings.SettingsName, configuredSettings.SettingsName) AS SettingsName
		,CASE WHEN configuredSettings.SettingsName IS NULL THEN 0 ELSE 1 END AS IsConfigured
		,COUNT(configuredSettings.SettingsName) AS DistinctValuesCount
	FROM
		(
		SELECT
			mandSett.SettingsName AS SettingsName
		FROM
			[dbo].[IntegratorEnvironment] env 
			INNER JOIN [dbo].[IntegratorEnvironmentType] envtype ON env.IntegratorEnvironmentTypeId = envtype.Id
			INNER JOIN [dbo].[SettingsMandatoryInEnvironemntType] mandSett ON mandSett.IntegratorEnvironmentTypeId = envtype.Id
		WHERE
			env.Name = @EnvironmentName
		) mandatorySettings

		FULL OUTER JOIN

		(
		SELECT
			sett.Name AS SettingsName
		FROM
			[dbo].[IntegratorEnvironment] envint 
			INNER JOIN [dbo].[IntegratorEnvironmentToSettingsMap] map ON envint.Id = map.IntegratorEnvironmentId
			INNER JOIN [dbo].[Settings] sett ON sett.id = map.SettingsId
		WHERE
			envint.Name = @EnvironmentName
		) configuredSettings 
		
		ON mandatorySettings.SettingsName = configuredSettings.SettingsName

	GROUP BY
		mandatorySettings.SettingsName, configuredSettings.SettingsName
END

GO
ALTER AUTHORIZATION ON [dbo].[GetEnvironmentSettingsSet_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetEnvironmentSettingsSet_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetExternalOrdersTransmissionSwitchState_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetExternalOrdersTransmissionSwitchState_SP] 
	@SwitchName NVARCHAR(64) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		[SwitchName],
		[TransmissionEnabled]
	FROM 
		[dbo].[ExternalOrdersTransmissionSwitch] WITH(NOLOCK)
	WHERE 
		@SwitchName IS NULL OR [SwitchName] = @SwitchName
END


GO
ALTER AUTHORIZATION ON [dbo].[GetExternalOrdersTransmissionSwitchState_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetExternalOrdersTransmissionSwitchState_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetExternalVolumeRatio_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetExternalVolumeRatio_SP]
(
	@StartDateUtc DATE,
	@EndDateUtc DATE
	--@VenueId tinyint
)
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

declare @StartDateUtcInternal DATE = @StartDateUtc;
declare @EndDateUtcInternal DATE = @EndDateUtc;

declare @VenueId tinyint = 20; -- TODO: FIX: make this a parameter (20 = Hotspot)

select 
kgtData.KgtTradeDateUtc'TradeDate',
kgtData.KgtVolumeUsd/venueData.ExternalVolumeUsd*100'KgtToVenueVolumePct'

from

(
select
deal.DatePartIntegratorReceivedExecutionReportUtc'KgtTradeDateUtc', 
sum(abs(deal.AmountBasePolExecuted)*c.LastKnownUsdConversionMultiplier)'KgtVolumeUsd' -- TODO: FIX: EOD USD rate

from
DealExternalExecuted deal with(nolock)
join Symbol s with(nolock) on s.Id=deal.SymbolId
join Currency c with(nolock) on c.CurrencyID=s.BaseCurrencyId

where
deal.DatePartIntegratorReceivedExecutionReportUtc between @StartDateUtc and @EndDateUtc

group by deal.DatePartIntegratorReceivedExecutionReportUtc) kgtData


join


(
select
ve.TradeDate'ExternalTradeDate', -- TODO: FIX: ExternalTradeData might not be compatible with Kgt data time zone
ve.VolumeUsd'ExternalVolumeUsd'

from 
VolumeExternal ve with(nolock) 

where 
ve.VenueId=@VenueId
and ve.TradeDate between @StartDateUtcInternal and @EndDateUtcInternal) venueData

on venueData.ExternalTradeDate=kgtData.KgtTradeDateUtc

order by venueData.ExternalTradeDate

END
GO
ALTER AUTHORIZATION ON [dbo].[GetExternalVolumeRatio_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetFixDictionaryXml_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetFixDictionaryXml_SP] 
	@DictionaryName NVARCHAR(32)
AS
BEGIN

	SELECT
		[DictionaryXml]
	FROM 
		[dbo].[Dictionary_FIX]
	WHERE
		[Name] = @DictionaryName
END

GO
ALTER AUTHORIZATION ON [dbo].[GetFixDictionaryXml_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetFixDictionaryXml_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetFixSettingsCfg_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetFixSettingsCfg_SP] 
	@EnvironmentName NVARCHAR(100),
	@SettingName NVARCHAR(100),
	@AllowFluentRedirection BIT = 0
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		TOP 1 cfg.Configuration, cfg.Name
	FROM
		[dbo].[IntegratorEnvironment] env 
		INNER JOIN [dbo].[IntegratorEnvironmentToSettingsFixMap] map ON env.Id = map.IntegratorEnvironmentId
		INNER JOIN [dbo].[Settings_FIX] cfg ON cfg.id = map.SettingsFixId
	WHERE
		env.Name = @EnvironmentName
		AND (cfg.Name = @SettingName OR (@AllowFluentRedirection = 1 AND cfg.Name = @SettingName + '_Fluent'))
	ORDER BY
		-- so that '_Fluent' cfgs are first
		cfg.Name DESC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetFixSettingsCfg_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetFixSettingsCfg_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetFluentCredentials_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetFluentCredentials_SP] 
AS
BEGIN
	SELECT
		TOP 1 cred.[UserName], cred.[Password], cred.[OnBehalfOfCompId]
	FROM
		[dbo].[Fluent_FixCredentials] cred 
END

GO
ALTER AUTHORIZATION ON [dbo].[GetFluentCredentials_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetFluentCredentials_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetFluentRedirectionState_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetFluentRedirectionState_SP] 
AS
BEGIN
	SELECT
		TOP 1 [IsFluentRedirectionOn]
	FROM
		[dbo].[Fluent_RedirectionState]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetFluentRedirectionState_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetFluentRedirectionState_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetIgnoredOrders_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetIgnoredOrders_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN
	
	SELECT 
		ignoredOrder.[IntegratorSentExternalOrderUtc] AS IntegratorSentExternalOrderUtc
		,ignoredOrder.ExternalOrderLabelForCounterparty AS ExternalOrderLabelForCounterparty
		,ignoredOrder.RequestedAmountBaseAbs AS RequestedAmountBaseAbs
		,ignoredOrder.RequestedPrice AS RequestedPrice
		,ctp.CounterpartyCode AS CounterpartyCode
		,symbol.Name AS Symbol
		,direction.Name AS Direction
	FROM
		[dbo].[OrderExternalIgnored] ignoredOrder
		INNER JOIN [dbo].[Counterparty] ctp on ctp.CounterpartyId = ignoredOrder.CounterpartyId
		INNER JOIN [dbo].Symbol symbol on symbol.Id = ignoredOrder.SymbolId
		INNER JOIN [dbo].[DealDirection] direction on direction.Id = ignoredOrder.DealDirectionId
	WHERE 
		ignoredOrder.[IntegratorSentExternalOrderUtc] BETWEEN @StartTime AND @EndTime

END

GO
ALTER AUTHORIZATION ON [dbo].[GetIgnoredOrders_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetIgnoredOrders_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetInstanceNames_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetInstanceNames_SP]
AS
BEGIN
	SELECT
		[Name]
	FROM 
		[dbo].[IntegratorInstance]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetInstanceNames_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetInstanceNames_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetIntegratorProcessDetails_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetIntegratorProcessDetails_SP](
	@InstanceName varchar(50),
	@ProcessName varchar(50)
	)
AS
BEGIN
	
	DECLARE @InstanceId TINYINT
	DECLARE @ProcessTypeId TINYINT

	SELECT @InstanceId = Id FROM [dbo].[IntegratorInstance] WHERE Name = @InstanceName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'InstanceName not found in DB: %s', 16, 2, @InstanceName) WITH SETERROR
	END

	SELECT @ProcessTypeId = [Id] FROM [dbo].[IntegratorProcessType] WHERE [Name] = @ProcessName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ProcessType not found in DB: %s', 16, 2, @ProcessName) WITH SETERROR
	END
	
	SELECT
		process.[Id] AS ProcessIdentifier,
		[PrivateIPString],
		[PublicIPString],
		endpoint.[Name] AS Hostname,
		[TradingAPIPort],
		[DiagnosticsAPIPort],
		state.[Name] AS State
	FROM
		[dbo].[IntegratorProcess] process
		INNER JOIN [dbo].[IntegratorEndpoint] endpoint ON process.IntegratorEndpointId = endpoint.Id
		INNER JOIN [dbo].[IntegratorProcessType] type ON process.IntegratorProcessTypeId = type.Id
		INNER JOIN [dbo].[IntegratorProcessState] state ON process.IntegratorProcessStateId = state.Id
	WHERE
		process.IntegratorInstanceId = @InstanceId
		AND process.IntegratorProcessTypeId = @ProcessTypeId
END

GO
ALTER AUTHORIZATION ON [dbo].[GetIntegratorProcessDetails_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetIntegratorProcessDetails_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetIntegratorProcessDetailsList_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetIntegratorProcessDetailsList_SP]
AS
BEGIN
	SELECT 
		process.[Id] AS ProcessId
		,instance.Name AS InstanceName
		,instance.Description AS InstanceDescription
		,processtype.Name AS ProcessName
		,processtype.Description AS ProcessDescription
		,state.Name AS State
		,endpoint.Name AS Endpoint
		,[LastHBTimeUtc]
		,CASE WHEN DATEADD(minute, 2, [LastHBTimeUtc]) < GETUTCDATE() THEN 1 ELSE 0 END AS IsOld
	FROM 
		[dbo].[IntegratorProcess] process
		INNER JOIN [dbo].[IntegratorInstance] instance ON process.IntegratorInstanceId = instance.Id
		INNER JOIN [dbo].[IntegratorProcessType] processtype ON process.IntegratorProcessTypeId = processtype.id
		INNER JOIN [dbo].[IntegratorProcessState] state ON process.IntegratorProcessStateId = state.Id
		INNER JOIN [dbo].[IntegratorEndpoint] endpoint ON process.IntegratorEndpointId = endpoint.Id
	ORDER BY
		instance.Id,
		[LastHBTimeUtc] DESC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetIntegratorProcessDetailsList_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetIntegratorProcessDetailsList_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetKgtLLDealsAndRejections_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetKgtLLDealsAndRejections_SP]
	@FxPairNoSlash char(6),
	@StartDateTimeUtc DateTime2,
	@EndDateTimeUtc DateTime2
AS
BEGIN
SET NOCOUNT ON;

declare @SymbolId tinyint = (select Id from Symbol sym  with(nolock) where sym.ShortName=@FxPairNoSlash);

SELECT 

		CounterpartySentExternalOrderUtc, -- NOT NULL
		IntegratorReceivedExternalOrderUtc, -- NOT NULL
		IntegratorSentExecutionReportUtc, -- NOT NULL
		CounterpartyClientOrderId, -- NOT NULL

		kdar.TradingSystemId, -- NULL
		tradingSystemCounterparty.CounterpartyCode + ' ' + tst.SystemName'TradingSystemDescription', -- NULL
		ts.TradingSystemGroupId'TradingSystemGroupId', -- NULL
		tsGroup.TradingSystemGroupName'TradingSystemGroupName', -- NULL

		SourceIntegratorPriceIdentity, -- NULL
		c.CounterpartyCode, -- CounterpartyId,  -- NOT NULL
		integratorDealDirection.Name'IntegratorDirection', -- IntegratorDealDirectionId,  -- NOT NULL
		CounterpartyRequestedPrice,  -- NOT NULL
		CounterpartyRequestedAmountBasePol,  -- NOT NULL
		IntegratorExecutionId,  -- NOT NULL
		ValueDate, -- NULL for KGTrejection, NOT NULL for KGTdeal
		OrderReceivedToExecutionReportSentLatency,  -- NOT NULL
		IntegratorExecutedAmountBasePol,  -- NOT NULL
		IntegratorRejectedAmountBasePol,  -- NOT NULL
		--IntegratorLatencyNanoseconds, -- NULL
		--DatePartIntegratorReceivedOrderUtc, -- NULL
		CounterpartyClientId, -- NULL
		irr.IntegratorRejectionReason -- NULL, IntegratorRejectionReasonId -- NULL
		--RecordId  -- NOT NULL

FROM

		Integrator.dbo.KGTLLDealsAndRejections_Tbl_UtcRange_Tbl(@StartDateTimeUtc, @EndDateTimeUtc, @SymbolId) kdar
		join Integrator.dbo.DealDirection integratorDealDirection with(nolock) on kdar.IntegratorDealDirectionId=integratorDealDirection.Id
		join Integrator.dbo.Counterparty c  with(nolock) on kdar.CounterpartyId=c.CounterpartyId
		left join Integrator.dbo.TradingSystem_All_Tbl() ts on kdar.TradingSystemId=ts.TradingSystemId
		left join Integrator.dbo.TradingSystemType tst  with(nolock) on ts.TradingSystemTypeId=tst.TradingSystemTypeId
		left join Integrator.dbo.Counterparty tradingSystemCounterparty  with(nolock) on tst.CounterpartyId=tradingSystemCounterparty.CounterpartyId
		left join Integrator.dbo.TradingSystemsGroup_All_Tbl() tsGroup on tsGroup.TradingSystemGroupId=ts.TradingSystemGroupId
		left join Integrator.dbo.IntegratorRejectionReason irr with(nolock) on irr.IntegratorRejectionReasonId=kdar.IntegratorRejectionReasonId

ORDER BY

		kdar.IntegratorReceivedExternalOrderUtc asc

END

GO
ALTER AUTHORIZATION ON [dbo].[GetKgtLLDealsAndRejections_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetKgtOnVenueVolumes_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetKgtOnVenueVolumes_SP]
(
	@StartUtc datetime2,
	@EndUtc datetime2,
	@VenueCode char(3)
)
AS
BEGIN
SET NOCOUNT ON;

IF @VenueCode not in ('FC1', 'FC2')
RETURN 0
ELSE


declare @StartUtcInternal datetime2 = @StartUtc;
declare @EndUtcInternal datetime2 = @EndUtc;
------------------

declare @Venues table(VenueCode char(3));

declare @UsualSymbols table(Symbol char(6));

IF @VenueCode='FC1' -- London LD4 pool, both taker (FC1) and LL-makers (FS1, FS2)
BEGIN
insert into @Venues (VenueCode) values ('FC1');
insert into @Venues (VenueCode) values ('FS1');
insert into @Venues (VenueCode) values ('FS2');

insert into @UsualSymbols (Symbol) values ('AUDCAD');
insert into @UsualSymbols (Symbol) values ('AUDCHF');
insert into @UsualSymbols (Symbol) values ('AUDJPY');
insert into @UsualSymbols (Symbol) values ('AUDNZD');
insert into @UsualSymbols (Symbol) values ('AUDUSD');
insert into @UsualSymbols (Symbol) values ('CADCHF');
insert into @UsualSymbols (Symbol) values ('CADJPY');
insert into @UsualSymbols (Symbol) values ('CHFJPY');
--insert into @UsualSymbols (Symbol) values ('CHFSEK');
--insert into @UsualSymbols (Symbol) values ('DKKSEK');
insert into @UsualSymbols (Symbol) values ('EURAUD');
insert into @UsualSymbols (Symbol) values ('EURCAD');
insert into @UsualSymbols (Symbol) values ('EURCHF');
insert into @UsualSymbols (Symbol) values ('EURCZK');
--insert into @UsualSymbols (Symbol) values ('EURDKK');
insert into @UsualSymbols (Symbol) values ('EURGBP');
insert into @UsualSymbols (Symbol) values ('EURHKD');
insert into @UsualSymbols (Symbol) values ('EURHUF');
insert into @UsualSymbols (Symbol) values ('EURJPY');
insert into @UsualSymbols (Symbol) values ('EURMXN');
insert into @UsualSymbols (Symbol) values ('EURNOK');
insert into @UsualSymbols (Symbol) values ('EURNZD');
insert into @UsualSymbols (Symbol) values ('EURPLN');
--insert into @UsualSymbols (Symbol) values ('EURRUB');
insert into @UsualSymbols (Symbol) values ('EURSEK');
insert into @UsualSymbols (Symbol) values ('EURSGD');
insert into @UsualSymbols (Symbol) values ('EURTRY');
insert into @UsualSymbols (Symbol) values ('EURUSD');
insert into @UsualSymbols (Symbol) values ('EURZAR');
insert into @UsualSymbols (Symbol) values ('GBPAUD');
insert into @UsualSymbols (Symbol) values ('GBPCAD');
insert into @UsualSymbols (Symbol) values ('GBPCHF');
insert into @UsualSymbols (Symbol) values ('GBPCZK');
insert into @UsualSymbols (Symbol) values ('GBPDKK');
insert into @UsualSymbols (Symbol) values ('GBPHKD');
insert into @UsualSymbols (Symbol) values ('GBPHUF');
insert into @UsualSymbols (Symbol) values ('GBPJPY');
insert into @UsualSymbols (Symbol) values ('GBPMXN');
insert into @UsualSymbols (Symbol) values ('GBPNOK');
insert into @UsualSymbols (Symbol) values ('GBPNZD');
insert into @UsualSymbols (Symbol) values ('GBPPLN');
insert into @UsualSymbols (Symbol) values ('GBPSEK');
insert into @UsualSymbols (Symbol) values ('GBPSGD');
insert into @UsualSymbols (Symbol) values ('GBPTRY');
insert into @UsualSymbols (Symbol) values ('GBPUSD');
insert into @UsualSymbols (Symbol) values ('GBPZAR');
insert into @UsualSymbols (Symbol) values ('NOKSEK');
insert into @UsualSymbols (Symbol) values ('NZDCAD');
insert into @UsualSymbols (Symbol) values ('NZDCHF');
insert into @UsualSymbols (Symbol) values ('NZDJPY');
insert into @UsualSymbols (Symbol) values ('NZDSGD');
insert into @UsualSymbols (Symbol) values ('NZDUSD');
insert into @UsualSymbols (Symbol) values ('USDCAD');
insert into @UsualSymbols (Symbol) values ('USDCHF');
insert into @UsualSymbols (Symbol) values ('USDCNH');
insert into @UsualSymbols (Symbol) values ('USDCZK');
insert into @UsualSymbols (Symbol) values ('USDDKK');
insert into @UsualSymbols (Symbol) values ('USDHKD');
insert into @UsualSymbols (Symbol) values ('USDHUF');
insert into @UsualSymbols (Symbol) values ('USDILS');
insert into @UsualSymbols (Symbol) values ('USDJPY');
insert into @UsualSymbols (Symbol) values ('USDMXN');
insert into @UsualSymbols (Symbol) values ('USDNOK');
insert into @UsualSymbols (Symbol) values ('USDPLN');
--insert into @UsualSymbols (Symbol) values ('USDRUB');
insert into @UsualSymbols (Symbol) values ('USDSEK');
insert into @UsualSymbols (Symbol) values ('USDSGD');
insert into @UsualSymbols (Symbol) values ('USDTRY');
insert into @UsualSymbols (Symbol) values ('USDZAR');
END

IF @VenueCode='FC2' -- New York NY4 Pool
BEGIN
insert into @Venues (VenueCode) values ('FC2');

insert into @UsualSymbols (Symbol) values ('AUDCAD');
insert into @UsualSymbols (Symbol) values ('AUDCHF');
insert into @UsualSymbols (Symbol) values ('AUDJPY');
insert into @UsualSymbols (Symbol) values ('AUDNZD');
insert into @UsualSymbols (Symbol) values ('AUDUSD');
insert into @UsualSymbols (Symbol) values ('CADCHF');
insert into @UsualSymbols (Symbol) values ('CADJPY');
--insert into @UsualSymbols (Symbol) values ('CADMXN');
insert into @UsualSymbols (Symbol) values ('CHFJPY');
insert into @UsualSymbols (Symbol) values ('CHFSEK');
insert into @UsualSymbols (Symbol) values ('DKKSEK');
insert into @UsualSymbols (Symbol) values ('EURAUD');
insert into @UsualSymbols (Symbol) values ('EURCAD');
insert into @UsualSymbols (Symbol) values ('EURCHF');
insert into @UsualSymbols (Symbol) values ('EURCZK');
insert into @UsualSymbols (Symbol) values ('EURDKK');
insert into @UsualSymbols (Symbol) values ('EURGBP');
insert into @UsualSymbols (Symbol) values ('EURHUF');
insert into @UsualSymbols (Symbol) values ('EURJPY');
insert into @UsualSymbols (Symbol) values ('EURMXN');
insert into @UsualSymbols (Symbol) values ('EURNOK');
insert into @UsualSymbols (Symbol) values ('EURNZD');
insert into @UsualSymbols (Symbol) values ('EURPLN');
insert into @UsualSymbols (Symbol) values ('EURRUB');
insert into @UsualSymbols (Symbol) values ('EURSEK');
insert into @UsualSymbols (Symbol) values ('EURSGD');
insert into @UsualSymbols (Symbol) values ('EURTRY');
insert into @UsualSymbols (Symbol) values ('EURUSD');
insert into @UsualSymbols (Symbol) values ('GBPAUD');
insert into @UsualSymbols (Symbol) values ('GBPCAD');
insert into @UsualSymbols (Symbol) values ('GBPCHF');
insert into @UsualSymbols (Symbol) values ('GBPJPY');
insert into @UsualSymbols (Symbol) values ('GBPMXN');
insert into @UsualSymbols (Symbol) values ('GBPNOK');
insert into @UsualSymbols (Symbol) values ('GBPNZD');
insert into @UsualSymbols (Symbol) values ('GBPSEK');
insert into @UsualSymbols (Symbol) values ('GBPUSD');
insert into @UsualSymbols (Symbol) values ('HKDJPY');
insert into @UsualSymbols (Symbol) values ('NOKJPY');
insert into @UsualSymbols (Symbol) values ('NOKSEK');
insert into @UsualSymbols (Symbol) values ('NZDCAD');
insert into @UsualSymbols (Symbol) values ('NZDCHF');
insert into @UsualSymbols (Symbol) values ('NZDJPY');
insert into @UsualSymbols (Symbol) values ('NZDSGD');
insert into @UsualSymbols (Symbol) values ('NZDUSD');
insert into @UsualSymbols (Symbol) values ('USDCAD');
insert into @UsualSymbols (Symbol) values ('USDCHF');
insert into @UsualSymbols (Symbol) values ('USDCNH');
insert into @UsualSymbols (Symbol) values ('USDCZK');
insert into @UsualSymbols (Symbol) values ('USDDKK');
insert into @UsualSymbols (Symbol) values ('USDHKD');
insert into @UsualSymbols (Symbol) values ('USDHUF');
insert into @UsualSymbols (Symbol) values ('USDILS');
insert into @UsualSymbols (Symbol) values ('USDJPY');
insert into @UsualSymbols (Symbol) values ('USDMXN');
insert into @UsualSymbols (Symbol) values ('USDNOK');
insert into @UsualSymbols (Symbol) values ('USDPLN');
insert into @UsualSymbols (Symbol) values ('USDRUB');
insert into @UsualSymbols (Symbol) values ('USDSEK');
insert into @UsualSymbols (Symbol) values ('USDSGD');
insert into @UsualSymbols (Symbol) values ('USDTRY');
insert into @UsualSymbols (Symbol) values ('USDZAR');
END


select * from(

select
coalesce(t.Symbol,u.Symbol)'Symbol',
coalesce(t.VenueSingleSideVolumeUsdM,0)'VenueSingleSideVolumeUsdM',
coalesce(t.KgtVolumeOnVenueUsdM,0)'KgtVolumeOnVenueUsdM',
coalesce(t.KgtVolumeOnVenuePct,0)'KgtVolumeOnVenuePct'

from @UsualSymbols u full outer join

(select
result.Symbol,
result.VenueSingleSideVolumeUsd/1000000'VenueSingleSideVolumeUsdM',
result.KgtVolumeOnVenueUsd/1000000'KgtVolumeOnVenueUsdM',
result.KgtVolumeOnVenueUsd/result.VenueSingleSideVolumeUsd*100'KgtVolumeOnVenuePct'

from
(select
venueVolume.FxpCode'Symbol',
coalesce(kgtVolume.KgtVolumeOnVenueUsd,0)'KgtVolumeOnVenueUsd',
venueVolume.venueSingleSideVolumeUsd

from

-- venue volume
(select 
f.FxpCode,
sum(md.Size*baseCcy.LastKnownUsdConversionMultiplier)/2.0'VenueSingleSideVolumeUsd'
from FXtickDB.dbo.MarketData md with(nolock)
join FXtickDB.dbo.FxPair f with(nolock) on f.FxPairId=md.FXPairId
join Integrator.dbo.Symbol s with(nolock) on s.ShortName=f.FxpCode
join Integrator.dbo.Currency baseCcy with(nolock) on baseCcy.CurrencyID=s.BaseCurrencyId
where md.FXPairId in (select f.FxPairId from FXtickDB.dbo.FxPair f)
and md.IntegratorReceivedTimeUtc between 
@StartUtcInternal and @EndUtcInternal
and md.CounterpartyId = (select c.LiqProviderStreamId from FXtickDB.dbo.Counterparty c with(nolock) where c.LiqProviderStreamName=@VenueCode)
and md.RecordTypeId=6 -- Venue trade data only (ticker)
group by f.FxpCode) venueVolume
--


left join

-- kgt volume
(select s.ShortName, sum(abs(dee.AmountBasePolExecuted)*baseCcy.LastKnownUsdConversionMultiplier)'KgtVolumeOnVenueUsd'
from Integrator.dbo.TradingSystemDealsExecuted_KgtMakerTaker_UtcRange_Tbl(@StartUtcInternal, @EndUtcInternal) dee
join Integrator.dbo.Symbol s with(nolock) on s.Id=dee.SymbolId
join Integrator.dbo.Currency baseCcy with(nolock) on baseCcy.CurrencyID=s.BaseCurrencyId
where dee.CounterpartyId in (select c.CounterpartyId from Integrator.dbo.Counterparty c with(nolock) where c.CounterpartyCode in (select VenueCode from @Venues))
group by s.ShortName) kgtVolume
--

on kgtVolume.ShortName=venueVolume.FxpCode) result) t on  t.Symbol=u.Symbol) t2

order by t2.Symbol

--order by result.Symbol;

END

GO
ALTER AUTHORIZATION ON [dbo].[GetKgtOnVenueVolumes_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetLastVenueCrossSettings_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetLastVenueCrossSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MinimumSizeToTrade]
      ,[MaximumSizeToTrade]
      ,[MinimumDiscountBasisPointsToTrade]
      ,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
      ,[MaximumWaitTimeOnImprovementTick_milliseconds]
      ,[MinimumFillSize]
	FROM 
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetLastVenueCrossSettings_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetLastVenueCrossSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetLastVenueGliderSettings_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetLastVenueGliderSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MaximumSizeToTrade]
      ,[BestPriceImprovementOffsetBasisPoints]
      ,[MaximumDiscountBasisPointsToTrade]
      ,[KGTLLTime_milliseconds]
      ,[MinimumBPGrossGain]
      ,[MinimumIntegratorPriceLife_milliseconds]
	  ,[MinimumIntegratorGlidePriceLife_milliseconds]
	  ,[GlidingCutoffSpan_milliseconds]
      ,[MinimumSizeToTrade]
      ,[MinimumFillSize]
      ,[MaximumSourceBookTiers]
      ,[MinimumBasisPointsFromKgtPriceToFastCancel]
      ,[ReferenceLmaxBookOnly]
      ,[BidEnabled]
      ,[AskEnabled]
	FROM 
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetLastVenueGliderSettings_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetLastVenueGliderSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetLastVenueMMSettings_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetLastVenueMMSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MaximumSizeToTrade]
		,[BestPriceImprovementOffsetBasisPoints]
		,[MaximumDiscountBasisPointsToTrade]
		,[MaximumWaitTimeOnImprovementTick_milliseconds]
		,[MinimumIntegratorPriceLife_milliseconds]
		,[MinimumSizeToTrade]
		,[MinimumFillSize]
		,[MaximumSourceBookTiers]
		,[MinimumBasisPointsFromKgtPriceToFastCancel]
		,[BidEnabled]
		,[AskEnabled]
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetLastVenueMMSettings_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetLastVenueMMSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetLastVenueQuotingSettings_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetLastVenueQuotingSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MaximumPositionBaseAbs]
      ,[FairPriceImprovementSpreadBasisPoints]
      ,[StopLossNetInUsd]
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetLastVenueQuotingSettings_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetLastVenueQuotingSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetLastVenueStreamSettings_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetLastVenueStreamSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MaximumSizeToTrade]
      ,[BestPriceImprovementOffsetBasisPoints]
      ,[MaximumDiscountBasisPointsToTrade]
      ,[KGTLLTime_milliseconds]
      ,[PreferLLDestinationCheckToLmax]
      ,[MinimumBPGrossToAccept]
      ,[PreferLmaxDuringHedging]
      ,[MinimumIntegratorPriceLife_milliseconds]
      ,[MinimumSizeToTrade]
      ,[MinimumFillSize]
      ,[MaximumSourceBookTiers]
      ,[MinimumBasisPointsFromKgtPriceToFastCancel]
      ,[ReferenceLmaxBookOnly]
      ,[BidEnabled]
      ,[AskEnabled]
	FROM 
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetLastVenueStreamSettings_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetLastVenueStreamSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetMedian_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMedian_SP]
(
	@ColumnName nvarchar(max),
	@TableName nvarchar(max)
)
AS
BEGIN

declare @sqlCommand nvarchar(max) = 'select ' + @ColumnName + ' from ' + @TableName;

declare @SourceColumnTable TABLE (ColumnForMedianCalc decimal(38,10));

insert into @SourceColumnTable exec sp_executesql @sqlCommand;


-- http://oreilly.com/catalog/transqlcook/chapter/ch08.html
-- TODO: create an aggregate function for median

DECLARE @Median decimal(38,10);

SELECT @Median =
	CASE

		-- odd count:
		WHEN COUNT(*) % 2 = 1
			THEN x.ColumnForMedianCalc 

		-- even count:
        ELSE 
			(x.ColumnForMedianCalc + MIN(CASE WHEN y.ColumnForMedianCalc > x.ColumnForMedianCalc 
                               THEN y.ColumnForMedianCalc 
                          END))/2.0 
	END

FROM 
@SourceColumnTable x, 
@SourceColumnTable y

GROUP BY x.ColumnForMedianCalc

HAVING

   SUM(CASE WHEN y.ColumnForMedianCalc <= x.ColumnForMedianCalc 
      THEN 1 ELSE 0 END) >= (count(*) + 1) / 2 AND

   SUM(CASE WHEN y.ColumnForMedianCalc >= x.ColumnForMedianCalc 
      THEN 1 ELSE 0 END) >= (count(*) / 2) + 1

select @Median;


END

GO
ALTER AUTHORIZATION ON [dbo].[GetMedian_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetNopAbsTotal_Daily_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetNopAbsTotal_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.Price) AS NopTermPol
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		deal.SymbolId 


	SELECT
		-- From SHORTs
		SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
	FROM
		(
		SELECT
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
		GROUP BY
			CCYPositions.CCY
		) AS CtpCCYPositions

END

GO
ALTER AUTHORIZATION ON [dbo].[GetNopAbsTotal_Daily_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetNopAbsTotal_Daily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetNopsPolPerCounterpartyAndCurrency_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetNopsPolPerCounterpartyAndCurrency_SP] (
	@Day Date
	)
AS
BEGIN

	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		deal.CounterpartyId,
		deal.SymbolId
		
		

		SELECT
			ctp.CounterpartyCode AS Counterparty,
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				basePos.CounterpartyId,
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT 
				termPos.CounterpartyId,
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
			INNER JOIN [dbo].[Counterparty] ctp ON ctp.CounterpartyId = CCYPositions.CounterpartyId
		GROUP BY
			ctp.CounterpartyCode
			,CCYPositions.CCY
		ORDER BY
			ctp.CounterpartyCode
			,CCYPositions.CCY

END

GO
ALTER AUTHORIZATION ON [dbo].[GetNopsPolPerCounterpartyAndCurrency_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetNopsPolPerCounterpartyAndCurrency_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetNopsPolSidedPerCurrency_Daily_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetNopsPolSidedPerCurrency_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,0),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		deal.SymbolId


	SELECT
		CCYPositions.CCY AS CCY,
		SUM(CCYPositions.NopPol) AS NopPol,
		SUM(CCYPositions.NopPolInUsd) AS NopPolInUsd
	FROM
		(
		SELECT 
			ccy1.CurrencyAlphaCode AS CCY,
			basePos.NopBasePol AS NopPol,
			basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
		FROM
			Currency ccy1
			INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
			INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

		UNION ALL

		SELECT 
			ccy2.CurrencyAlphaCode AS CCY,
			termPos.NopTermPol AS NopPol,
			termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
		FROM
			Currency ccy2
			INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
			INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
		) AS CCYPositions
	GROUP BY
		CCYPositions.CCY
	ORDER BY
		CCYPositions.CCY
END

GO
ALTER AUTHORIZATION ON [dbo].[GetNopsPolSidedPerCurrency_Daily_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetNopsPolSidedPerCurrency_Daily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetNumberOfConflicitngSymbolRangesForSystemType_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetNumberOfConflicitngSymbolRangesForSystemType_SP]
(
	@TradingSystemTypeId INT,
	@TradingSystemsGroupId [int],
	@PageIdToExclude INT = NULL,
	@BeginSymbol VARCHAR(7),
	@EndSymbol VARCHAR(7)
)
AS
BEGIN

	DECLARE @BeginSymbolId INT
	DECLARE @EndSymbolId INT
	DECLARE @BeginSymbolRank INT
	DECLARE @EndSymbolRank INT

	SELECT @BeginSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @BeginSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @BeginSymbol) WITH SETERROR
	END

	SELECT @EndSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @EndSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @EndSymbol) WITH SETERROR
	END

	DECLARE @SymbolRanks TABLE (SymbolName varchar(7), SymbolId int, SymbolAlphaRank int)

	INSERT INTO @SymbolRanks(SymbolName, SymbolId, SymbolAlphaRank)
	SELECT
		name, 
		id, 
		RANK() OVER (order by name asc) AS AlphaRank 
	FROM
		[dbo].[Symbol]

	SELECT @BeginSymbolRank = SymbolAlphaRank FROM @SymbolRanks WHERE SymbolId = @BeginSymbolId
	SELECT @EndSymbolRank = SymbolAlphaRank FROM @SymbolRanks WHERE SymbolId = @EndSymbolId

	SELECT
		COUNT(*)
	FROM
		[dbo].[TradingSystemMonitoringPage] sysPage
		INNER JOIN @SymbolRanks BeginSymbolRanks on sysPage.BeginSymbolId = BeginSymbolRanks.SymbolId
		INNER JOIN @SymbolRanks EndSymbolRanks on sysPage.EndSymbolId = EndSymbolRanks.SymbolId
	WHERE
		[TradingSystemTypeId] = @TradingSystemTypeId
		AND
		[TradingSystemGroupId] = @TradingSystemsGroupId
		AND
		(@PageIdToExclude IS NULL OR [TradingSystemMonitoringPageId] != @PageIdToExclude)
		AND
		(
			--start symbol of existing range is inside new tested range
			(BeginSymbolRanks.SymbolAlphaRank >= @BeginSymbolRank AND BeginSymbolRanks.SymbolAlphaRank <= @EndSymbolRank)
			OR
			--end symbol of existing range is inside new tested range
			(EndSymbolRanks.SymbolAlphaRank >= @BeginSymbolRank AND EndSymbolRanks.SymbolAlphaRank <= @EndSymbolRank)
		)
END

GO
ALTER AUTHORIZATION ON [dbo].[GetNumberOfConflicitngSymbolRangesForSystemType_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetNumberOfConflicitngSymbolRangesForSystemType_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetOpenPositionBaseSizePolToday_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetOpenPositionBaseSizePolToday_SP] 
	@FxPairNoSlash char(6),
	@CounterpartyCode char(3),
	@OpenPositionBaseSizePolToday decimal(18,0) output
	
AS
BEGIN

SET NOCOUNT ON;

DECLARE @Today DATE = GETUTCDATE()

SELECT 
	@OpenPositionBaseSizePolToday = 
	COALESCE(SUM(AmountBasePolExecuted),0) -- will always return number, never NULL

FROM 
	DealExternalExecuted deal
	INNER JOIN Symbol sym ON deal.SymbolId = sym.Id
	INNER JOIN Counterparty ctp ON deal.CounterpartyId = ctp.CounterpartyId
WHERE
	DatePartIntegratorReceivedExecutionReportUtc = @Today
	AND sym.ShortName = @FxPairNoSlash
	AND ctp.CounterpartyCode = @CounterpartyCode

END
GO
ALTER AUTHORIZATION ON [dbo].[GetOpenPositionBaseSizePolToday_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetPolarizedNopPerPair_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPolarizedNopPerPair_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[AmountBasePolExecuted]) AS PolarizedNop
	FROM 
		(
		SELECT [SymbolId], [AmountBasePolExecuted] 
		FROM [dbo].[DealExternalExecuted]
		WHERE [IntegratorReceivedExecutionReportUtc] BETWEEN @StartTime AND @EndTime
		UNION ALL
		SELECT [SymbolId], [IntegratorExecutedAmountBasePol] AS [AmountBasePolExecuted] 
		FROM [dbo].[OrderExternalIncomingRejectable_Daily]
		WHERE [IntegratorReceivedExternalOrderUtc] BETWEEN @StartTime AND @EndTime
		UNION ALL
		SELECT [SymbolId], [IntegratorExecutedAmountBasePol] AS [AmountBasePolExecuted] 
		FROM [dbo].[OrderExternalIncomingRejectable_Archive]
		WHERE [IntegratorReceivedExternalOrderUtc] BETWEEN @StartTime AND @EndTime
		) deal

		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]
END
GO
ALTER AUTHORIZATION ON [dbo].[GetPolarizedNopPerPair_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetPolarizedNopPerPair_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetPriceDelaySettings_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPriceDelaySettings_SP] 
AS
BEGIN
	SELECT 
		ctp.[CounterpartyCode] AS Counterparty,
		[ExpectedDelayTicks],
		[MaxAllowedAdditionalDelayTicks]
	FROM 
		[dbo].[PriceDelaySettings] sett
		INNER JOIN [dbo].[Counterparty] ctp ON sett.[CounterpartyId] = ctp.[CounterpartyId]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetPriceDelaySettings_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetPriceDelaySettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetPriceDelaySettingsInMilliseconds_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPriceDelaySettingsInMilliseconds_SP]
AS
BEGIN
SET NOCOUNT ON;

declare @PDSettingsWithoutAdditionalTiers table(
cParty char(3),
expectedDelayMilliseconds decimal(18,9),
maxAllowedAdditionalDelayMilliseconds decimal(18,9));

insert into @PDSettingsWithoutAdditionalTiers
select
c.CounterpartyCode'cParty',
pdset.ExpectedDelayTicks/10000.0'expectedDelayMilliseconds',
pdset.MaxAllowedAdditionalDelayTicks/10000.0'maxAllowedAdditionalDelayMilliseconds'
from 
Counterparty c with(nolock)
left join PriceDelaySettings pdset with(nolock) on pdset.CounterpartyId=c.CounterpartyId
where
--c.Active=1 and 
pdset.ExpectedDelayTicks is not null;

declare @L01name char(3) = 'L01';
declare @L01expectedDelayMilliseconds decimal(18,9) = (select expectedDelayMilliseconds from @PDSettingsWithoutAdditionalTiers where cParty=@L01name);
declare @L01maxAllowedAdditionalDelayMilliseconds decimal(18,9) = (select maxAllowedAdditionalDelayMilliseconds from @PDSettingsWithoutAdditionalTiers where cParty=@L01name);

insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L02', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L03', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L04', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L05', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L06', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L07', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L08', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L09', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L10', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L11', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L12', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L13', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L14', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L15', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L16', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L17', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L18', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L19', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);
insert into @PDSettingsWithoutAdditionalTiers (cParty, expectedDelayMilliseconds, maxAllowedAdditionalDelayMilliseconds) values ('L20', @L01expectedDelayMilliseconds, @L01maxAllowedAdditionalDelayMilliseconds);


select * from @PDSettingsWithoutAdditionalTiers order by cParty

END
GO
ALTER AUTHORIZATION ON [dbo].[GetPriceDelaySettingsInMilliseconds_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetRecentExternalOrdersTransmissionControlState_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetRecentExternalOrdersTransmissionControlState_SP] 
	@SwitchName NVARCHAR(64),
	@ChangedBy NVARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SwitchId INT 
	SELECT @SwitchId = [Id] FROM [dbo].[ExternalOrdersTransmissionSwitch] WHERE [SwitchName] = @SwitchName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Switch not found in DB: %s', @SwitchName, 16, 2) WITH SETERROR
	END


	SELECT TOP 10
		[TransmissionEnabled]
		,[Changed]
		,[ChangedBy]
		,[ChangedFrom]
		,[Reason]
	FROM 
		[dbo].[ExternalOrdersTransmissionSwitchHistory]
	WHERE
		[ExternalOrdersTransmissionSwitchId] = @SwitchId
		AND	@ChangedBy IS NULL OR [ChangedBy] = @ChangedBy
	ORDER BY
		[Changed] DESC

END

GO
ALTER AUTHORIZATION ON [dbo].[GetRecentExternalOrdersTransmissionControlState_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetRecentExternalOrdersTransmissionControlState_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetRecentKillSwitchLoginActivity_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetRecentKillSwitchLoginActivity_SP] 
	@roleName NVARCHAR(64),
	@username  NVARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @RoleId INT 
	SELECT @RoleId = [Id] FROM [dbo].[KillSwitchRole] WHERE [Name] = @roleName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Role not found in DB: %s', @roleName, 16, 2) WITH SETERROR
	END

	SELECT TOP 10 
		[UserName], 
		[ActionTime], 
		[Source], 
		[Success] 
	FROM 
		[dbo].[KillSwitchLogon] 
	WHERE 
		[RoleId] = @RoleId AND @username IS NULL OR [UserName] = @username 
	ORDER BY ActionTime DESC

END

GO
ALTER AUTHORIZATION ON [dbo].[GetRecentKillSwitchLoginActivity_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetRecentKillSwitchLoginActivity_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetSessionsClaimStatus_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetSessionsClaimStatus_SP]
AS
BEGIN
	SELECT
		ctp.CounterpartyCode AS CounterpartyCode,
		instance.Name AS InstanceName
	FROM
		[dbo].[Counterparty] ctp 
		LEFT JOIN [dbo].[IntegratorInstanceSessionClaim] claim ON claim.[CounterpartyId] = ctp.[CounterpartyId]
		LEFT JOIN [dbo].[IntegratorInstance] instance ON claim.[IntegratorInstanceId] = instance.[Id]
	WHERE
		ctp.Active = 1
	ORDER BY CounterpartyCode ASC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetSessionsClaimStatus_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetSessionsClaimStatus_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetSettingsXml_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetSettingsXml_SP] 
	@EnvironmentName NVARCHAR(100),
	@SettingName NVARCHAR(100),
	@UpdatedAfter Datetime
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		sett.SettingsXml
	FROM
		[dbo].[IntegratorEnvironment] env 
		INNER JOIN [dbo].[IntegratorEnvironmentToSettingsMap] map ON env.Id = map.IntegratorEnvironmentId
		INNER JOIN [dbo].[Settings] sett ON sett.id = map.SettingsId
	WHERE
		env.Name = @EnvironmentName
		AND sett.Name = @SettingName
		AND sett.[LastUpdated] > @UpdatedAfter
END

GO
ALTER AUTHORIZATION ON [dbo].[GetSettingsXml_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetSettingsXml_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetSettingsXml2_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSettingsXml2_SP] 
	@SettingName NVARCHAR(100),
	@SettingFriendlyName NVARCHAR(200)
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		sett.SettingsXml
	FROM
		[dbo].[Settings] sett
	WHERE
		sett.Name = @SettingName
		AND sett.[FriendlyName] = @SettingFriendlyName
END

GO
ALTER AUTHORIZATION ON [dbo].[GetSettingsXml2_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetSettingsXml2_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetStatisticsPerCounterparty_Daily_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetStatisticsPerCounterparty_Daily_SP] (
	@Day DATE
	)
AS
BEGIN

	DECLARE @KGTRejectionDirectionId BIT
	SELECT @KGTRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'
	DECLARE @CtpRejectionDirectionId BIT
	SELECT @CtpRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'CtpRejectd'
	DECLARE @MinDate DATE = GETUTCDATE()	
		
	DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		KGTRejectionsNum int,
		LastKgtRejectionUtc datetime2(7),
		CtpRejectionsNum int,
		LastCptRejectionUtc datetime2(7),
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int
	)

	INSERT INTO @temp_rejections(CounterpartyId, KGTRejectionsNum, LastKgtRejectionUtc, CtpRejectionsNum, LastCptRejectionUtc, IntegratorLatencyNanoseconds, IntegratorLatencyCases)
	SELECT
		[CounterpartyId],
		SUM(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN 1 ELSE 0 END) AS KGTRejections,
		MAX(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN [IntegratorReceivedExecutionReportUtc] ELSE @MinDate END) AS LastKgtRejectionUtc,
		SUM(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN 1 ELSE 0 END) AS CtpRejections,
		MAX(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN [IntegratorReceivedExecutionReportUtc] ELSE @MinDate END) AS LastCptRejectionUtc,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
	FROM
		[dbo].[DealExternalRejected_DailyView] ctpRej WITH(NOEXPAND, NOLOCK)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]

	UNION ALL

	SELECT
			[CounterpartyId],
			COUNT([CounterpartyId]) AS KGTRejections,
			MAX([IntegratorReceivedExternalOrderUtc]) AS LastKgtRejectionUtc,
			NULL AS CtpRejections,
			NULL AS LastCptRejectionUtc,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily] kgtRej WITH(NOLOCK)
		WHERE
			[IntegratorRejectedAmountBasePol] != 0
			AND [DatePartIntegratorReceivedOrderUtc] = @Day
			--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
		GROUP BY
			[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		CommissionPBBase decimal(18,2),
		CommissionCptBase decimal(18,2),
		DealsNum int,
		LastDealUtc datetime2(7),
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int,
		CounterpartyLatencyNanoseconds bigint,
		CounterpartyLatencyCases int
	)

	INSERT INTO 
		@temp_NOPs_pol
		(
		SymbolId, 
		CounterpartyId, 
		NopBasePol, 
		NopTermPol, 
		VolumeBaseAbs, 
		CommissionPBBase,
		CommissionCptBase,
		DealsNum, 
		LastDealUtc,
		IntegratorLatencyNanoseconds, 
		IntegratorLatencyCases, 
		CounterpartyLatencyNanoseconds, 
		CounterpartyLatencyCases
		)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		SUM(ABS([AmountBasePolExecuted]) * PbCommissionPpm / 1000000) AS CommissionPBBase,
		SUM(ABS([AmountBasePolExecuted]) * CounterpartyCommissionPpm / 1000000) AS CommissionCptBase,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum,
		MAX(deal.[IntegratorReceivedExecutionReportUtc]) AS LastDealUtc,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases,
		SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
		COUNT(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyCases
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
		INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
		--INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		deal.SymbolId,
		deal.CounterpartyId
		

	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.CommissionPbInUsd AS CommissionPbInUsd,
		DealsStatistics.CommissionCptInUsd AS CommissionCptInUsd,
		DealsStatistics.CommissionPbInUsd + DealsStatistics.CommissionCptInUsd  AS CommissionSumInUsd,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.LastDealUtc AS LastDealUtc,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		CASE WHEN COALESCE(RejectionsStatistics.KGTRejectionsNum,0) = 0 THEN NULL ELSE RejectionsStatistics.KGTRejectionsNum END AS KGTRejectionsNum,
		RejectionsStatistics.LastKgtRejectionUtc AS RejectionsStatistics,
		CASE WHEN COALESCE(RejectionsStatistics.CtpRejectionsNum,0) = 0 THEN NULL ELSE RejectionsStatistics.CtpRejectionsNum END AS CtpRejectionsNum,
		RejectionsStatistics.LastCptRejectionUtc AS LastCptRejectionUtc,
		CASE WHEN COALESCE(RejectionsStatistics.KGTRejectionsNum,0) = 0
		THEN
			 NULL
		ELSE
			RejectionsStatistics.KGTRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.KGTRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0))
		END AS KGTRejectionsRate,
		CASE WHEN COALESCE(RejectionsStatistics.CtpRejectionsNum,0) = 0
		THEN
			NULL
		ELSE
			RejectionsStatistics.CtpRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.CtpRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0))
		END AS CtpRejectionsRate,
		--this calculates average of both numbers if both are non-null; or average of the nonull case if just one is not null (as 2*A / 2*B = A/B); or NULL if both are NULL (avoids divide by zero exception)
		(COALESCE(RejectionsStatistics.IntegratorLatencyNanoseconds, DealsStatistics.IntegratorLatencyNanoseconds) + COALESCE(DealsStatistics.IntegratorLatencyNanoseconds, RejectionsStatistics.IntegratorLatencyNanoseconds))
		/
		(COALESCE(RejectionsStatistics.IntegratorLatencyCases, DealsStatistics.IntegratorLatencyCases) + COALESCE(DealsStatistics.IntegratorLatencyCases, RejectionsStatistics.IntegratorLatencyCases))
		AS AvgIntegratorLatencyNanoseconds,
		DealsStatistics.CounterpartyLatencyNanoseconds / DealsStatistics.CounterpartyLatencyCases AS AvgCounterpartyLatencyNanoseconds
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			-- from SHORTs
			SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs,
			--SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(CommissionPbInUsd) AS CommissionPbInUsd,
			SUM(CommissionCptInUsd) AS CommissionCptInUsd,
			SUM(DealsNum) AS DealsNum,
			MAX(LastDealUtc) AS LastDealUtc,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
			SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
			SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(CommissionPbInUsd) AS CommissionPbInUsd,
				SUM(CommissionCptInUsd) AS CommissionCptInUsd,
				SUM(DealsNum) AS DealsNum,
				MAX(LastDealUtc) AS LastDealUtc,
				SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
				SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
				SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
				SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.CommissionPBBase * ccy1.LastKnownUsdConversionMultiplier AS CommissionPbInUsd,
					basePos.CommissionCptBase * ccy1.LastKnownUsdConversionMultiplier AS CommissionCptInUsd,
					basePos.DealsNum AS DealsNum,
					basePos.LastDealUtc AS LastDealUtc,
					basePos.IntegratorLatencyNanoseconds AS IntegratorLatencyNanoseconds,
					basePos.IntegratorLatencyCases AS IntegratorLatencyCases,
					basePos.CounterpartyLatencyNanoseconds AS CounterpartyLatencyNanoseconds,
					basePos.CounterpartyLatencyCases AS CounterpartyLatencyCases
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd,
					0 AS CommissionPbInUsd,
					0 AS CommissionCptInUsd,
					0 AS DealsNum,
					NULL AS LastDealUtc,
					NULL AS IntegratorLatencyNanoseconds,
					NULL AS IntegratorLatencyCases,
					NULL AS CounterpartyLatencyNanoseconds,
					NULL AS CounterpartyLatencyCases
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode
END

GO
ALTER AUTHORIZATION ON [dbo].[GetStatisticsPerCounterparty_Daily_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_Daily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_Daily_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetStatisticsPerCounterparty_DailyCached_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetStatisticsPerCounterparty_DailyCached_SP]
AS
BEGIN
	SELECT 
		[Counterparty]
		,[NopAbs]
		,[VolumeBaseAbsInUsd]
		,[VolumeShare]
		,[CommissionsPbUsd]
		,[CommissionsCptUsd]
		,[CommissionsSumUsd]
		,[DealsNum]
		,[LastDealUtc]
		,[AvgDealSizeUsd]
		,[KGTRejectionsNum]
		,[LastKgtRejectionUtc]
		,[CtpRejectionsNum]
		,[LastCptRejectionUtc]
		,[KGTRejectionsRate]
		,[CtpRejectionsRate]
		,[AvgIntegratorLatencyNanoseconds]
		,[AvgCounterpartyLatencyNanoseconds]
	FROM 
		[dbo].[PersistedStatisticsPerCounterpartyDaily]
	ORDER BY
		[Counterparty]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetStatisticsPerCounterparty_DailyCached_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_DailyCached_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_DailyCached_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetStatisticsPerPair_Daily_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[GetStatisticsPerPair_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	SELECT
		Symbol.Name AS Symbol,
		positions.NopBasePol,
		positions.NopTermPol,
		positions.DealsNum as DealsNum,
		positions.LastDealUtc AS LastDealUtc,
		--PnL = term position + yield of immediate close of outstanding base position
		--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
		positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskMeanPrice ELSE Symbol.LastKnownBidMeanPrice END) * positions.NopBasePol AS PnlTermPol,
		(positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskMeanPrice ELSE Symbol.LastKnownBidMeanPrice END) * positions.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
		positions.VolumeInCcy1 AS VolumeInCcy1,
		positions.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd
	FROM
		(
		SELECT
			deal.SymbolId AS SymbolId,
			SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
			SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
			COUNT(deal.SymbolId) AS DealsNum,
			MAX(deal.IntegratorReceivedExecutionReportUtc) AS LastDealUtc,
			SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1
		FROM 
			DealExternalExecutedTakersAndMakers_DailyView deal
			
		WHERE
			[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		GROUP BY
			deal.SymbolId
		) positions
		INNER JOIN  [dbo].[Symbol] symbol ON positions.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
	ORDER BY
		symbol.Name
END

GO
ALTER AUTHORIZATION ON [dbo].[GetStatisticsPerPair_Daily_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerPair_Daily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetSymblTrustedRates_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSymblTrustedRates_SP]
AS
BEGIN
	SELECT
		symbol.[Name] AS Symbol,
		spreads.[MaximumTrustedSpreadBp] AS MaxTrustedSpreadBp,
		spreads.[MaximumTrustedSpreadDecimal] AS MaxTrustedSpreadDecimal,
		spreads.[MinimumTrustedSpreadBp] AS MinTrustedSpreadBp,
		spreads.[MinimumTrustedSpreadDecimal] AS MinTrustedSpreadDecimal,
		spreads.[MaximumAllowedTickAge_milliseconds] AS MaximumAllowedTickAge_milliseconds
	FROM
		[dbo].[Symbol_TrustedSpreads] spreads
		INNER JOIN [dbo].[Symbol] symbol ON spreads.SymbolId = symbol.Id
	WHERE
		[IsRaboTradeable] = 1
END

GO
ALTER AUTHORIZATION ON [dbo].[GetSymblTrustedRates_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetSymblTrustedRates_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetSymbolReferencePrices_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetSymbolReferencePrices_SP]
AS
BEGIN
	SELECT 
		[Name]
		,[LastKnownMeanReferencePrice]
		,[MeanReferencePriceUpdated]
	FROM 
		[dbo].[Symbol]
	WHERE
		[IsRaboTradeable] = 1
END

GO
ALTER AUTHORIZATION ON [dbo].[GetSymbolReferencePrices_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetSymbolReferencePrices_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetSystemsTradingControl_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSystemsTradingControl_SP]
AS
BEGIN
	SELECT 
		[GoFlatOnly]
	FROM
		[dbo].[SystemsTradingControl] with(NOLOCK)
END

GO
ALTER AUTHORIZATION ON [dbo].[GetSystemsTradingControl_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetSystemsTradingControl_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetSystemsTradingControl_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetTotalDailyVolume_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTotalDailyVolume_SP]
AS
BEGIN
	SELECT 
		SUM([VolumeBaseAbsInUsd])
	FROM 
		[dbo].[PersistedStatisticsPerCounterpartyDaily]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetTotalDailyVolume_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetTotalDailyVolume_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetTradingSystemGroupNames_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTradingSystemGroupNames_SP]
AS
BEGIN
	SELECT 
		[TradingSystemGroupId]
		,[TradingSystemGroupName]
	FROM 
		[dbo].[TradingSystemGroup]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetTradingSystemGroupNames_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetTradingSystemGroupNames_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetTradingSystemGroupNames_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetTradingSystemsDailyStats_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTradingSystemsDailyStats_SP]
AS
BEGIN

DECLARE @KGTRejectionDirectionId BIT
SELECT @KGTRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'
DECLARE @CtpRejectionDirectionId BIT
SELECT @CtpRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'CtpRejectd'
DECLARE @MinDate DATE = GETUTCDATE()

SELECT
		--BEWARE! There can be multiple symbols for same system id (this is especially true for system -1)
		systemStats.SymbolId AS SymbolId,
		systemStats.TradingSystemId AS TradingSystemId,
		COALESCE(systemStats.NopBasePol, 0) AS NopBasePol,
		COALESCE(systemStats.VolumeInUsd/1000000, 0) AS VolumeUsdM,
		COALESCE(systemStats.DealsNum, 0) as DealsNum,
		systemStats.LastDealUtc as LastDealUtc,
		COALESCE(systemStats.KGTRejectionsNum, 0) as KGTRejectionsNum,
		systemStats.LastKgtRejectionUtc as LastKgtRejectionUtc,
		COALESCE(systemStats.CtpRejectionsNum, 0) as CtpRejectionsNum,
		systemStats.LastCptRejectionUtc as LastCptRejectionUtc,
		COALESCE(systemStats.PnlTermPolInUsd/(systemStats.VolumeInUsd/1000000), 0) AS PnlGrossUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd, 0) AS PnlGrossUsd,
		COALESCE(systemStats.CommissionPBInUSD/(systemStats.VolumeInUsd/1000000), 0) AS CommissionsPbUsdPerMUsd,
		COALESCE(systemStats.CommissionPBInUSD, 0) AS CommissionsPbUsd,
		COALESCE(systemStats.CommissionCptInUSD/(systemStats.VolumeInUsd/1000000), 0) AS CommissionsCptUsdPerMUsd,
		COALESCE(systemStats.CommissionCptInUSD, 0) AS CommissionsCptUsd,
		COALESCE(systemStats.CommissionSUMInUSD/(systemStats.VolumeInUsd/1000000), 0) AS CommissionsSumUsdPerMUsd,
		COALESCE(systemStats.CommissionSUMInUSD, 0) AS CommissionsSumUsd,
		COALESCE((systemStats.PnlTermPolInUsd-systemStats.CommissionSUMInUSD)/(systemStats.VolumeInUsd/1000000), 0) AS PnlNetUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd-systemStats.CommissionSUMInUSD, 0) AS PnlNetUsd
	FROM
		(
		SELECT
			Symbol.Id AS SymbolId,
			systemsStats.TradingSystemId AS TradingSystemId,
			systemsStats.NopBasePol AS NopBasePol,
			systemsStats.DealsNum as DealsNum,
			systemsStats.LastDealUtc as LastDealUtc,
			systemsStats.KGTRejectionsNum as KGTRejectionsNum,
			systemsStats.LastKgtRejectionUtc as LastKgtRejectionUtc,
			systemsStats.CtpRejectionsNum as CtpRejectionsNum,
			systemsStats.LastCptRejectionUtc as LastCptRejectionUtc,
			systemsStats.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd,
			--PnL = term position + yield of immediate close of outstanding base position
			--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
			(systemsStats.NopTermPol + (CASE WHEN systemsStats.NopBasePol < 0 THEN Symbol.LastKnownAskMeanPrice ELSE Symbol.LastKnownBidMeanPrice END) * systemsStats.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
			systemsStats.CommissionPBInCCY1 * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionPBInUSD,
			systemsStats.CommissionCptInCCY1 * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionCptInUSD,
			(systemsStats.CommissionPBInCCY1 + systemsStats.CommissionCptInCCY1) * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionSUMInUSD
		FROM
			(
			SELECT
				COALESCE(systemsPositions.[TradingSystemId], systemRejections.[TradingSystemId]) AS [TradingSystemId],
				COALESCE(systemsPositions.[SymbolId], systemRejections.[SymbolId]) AS [SymbolId],
				NopBasePol,
				NopTermPol,
				VolumeInCcy1,
				CommissionPBInCCY1,
				CommissionCptInCCY1,
				DealsNum,
				LastDealUtc,
				KGTRejectionsNum,
				LastKgtRejectionUtc,
				CtpRejectionsNum,
				LastCptRejectionUtc
			FROM	
				(
				--allways include stats fro unknown systems
				SELECT 
					-1 AS [TradingSystemId], 
					0 AS [SymbolId],
					NULL AS NopBasePol, 
					NULL AS NopTermPol, 
					NULL AS VolumeInCcy1, 
					NULL AS CommissionPBInCCY1, 
					NULL AS CommissionCptInCCY1, 
					NULL AS DealsNum,
					NULL AS LastDealUtc
				UNION ALL
				SELECT 
					[TradingSystemId],
					[SymbolId],
					SUM([AmountBasePolExecuted]) AS NopBasePol,
					SUM(-[AmountBasePolExecuted]* [Price]) AS NopTermPol,
					SUM(ABS([AmountBasePolExecuted])) AS VolumeInCcy1,
					SUM(ABS([AmountBasePolExecuted]) * PbCommissionPpm / 1000000) AS CommissionPBInCCY1,
					SUM(ABS([AmountBasePolExecuted]) * CounterpartyCommissionPpm / 1000000) AS CommissionCptInCCY1,
					COUNT([TradingSystemId]) AS DealsNum,
					MAX([DealTimeUtc]) AS LastDealUtc
				FROM
					[dbo].[TradingSystemDealsExecuted_Daily] deal
					INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
				GROUP BY
					[TradingSystemId], [SymbolId]
				) systemsPositions
				FULL OUTER JOIN
				(
					SELECT 
						[TradingSystemId],
						[SymbolId],
						--COUNT([TradingSystemId]) AS RejectionsNum
						SUM(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN 1 ELSE 0 END) AS KGTRejectionsNum, 
						MAX(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN [RejectionTimeUtc] ELSE @MinDate END) AS LastKgtRejectionUtc, 
						SUM(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN 1 ELSE 0 END) AS CtpRejectionsNum,
						MAX(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN [RejectionTimeUtc] ELSE @MinDate END) AS LastCptRejectionUtc 
					FROM
						[dbo].[TradingSystemDealsRejected_Daily] rejects
					GROUP BY
						 [TradingSystemId], [SymbolId]
				)  systemRejections ON systemsPositions.TradingSystemId = systemRejections.TradingSystemId AND systemsPositions.[SymbolId] = systemRejections.[SymbolId]
			) systemsStats
			INNER JOIN [dbo].[Symbol] symbol ON systemsStats.SymbolId = symbol.Id
			INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
			INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
			) systemStats
END

GO
ALTER AUTHORIZATION ON [dbo].[GetTradingSystemsDailyStats_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetTradingSystemsDailyStats_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetTradingSystemsGroupOveralDailyStatsAndInfo_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetTradingSystemsGroupOveralDailyStatsAndInfo_SP]
(
	@GroupId INT
)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]
	
	SELECT
		[TradingSystemGroupName] AS TradingSystemGroup
		,[TradingSystemGroupId] 
		,[TradingSystemTypeName]
		,[Counterparty]
		,[TradingSystemTypeId]
		,[ConfiguredSystemsCount] AS ConfiguredSystems
		,[EnabledSystemsCount] AS EnabledSystems
		,[HardBlockedSystemsCount] AS HardBlockedSystems
		,[VolumeUsdM]
		,[DealsNum]
		,[LastDealUtc]
		,[KGTRejectionsNum]
		,[LastKgtRejectionUtc]
		,[CtpRejectionsNum]
		,[LastCptRejectionUtc]
		,[PnlGrossUsdPerMUsd]
		,[CommissionsUsdPerMUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsd]
		,[PnlNetUsd]
		,[IsOrderTransmissionDisabled]
		,[IsGoFlatOnlyOn]
		,[PageId]
		,[BeginSymbol]
		,[EndSymbol]
		,[StripsCount]
	FROM 
		[dbo].[PersistedStatisticsPerTradingGroupsDaily]
	WHERE
		[TradingSystemGroupId] = @GroupId
	ORDER BY
		Rank ASC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetTradingSystemsGroupOveralDailyStatsAndInfo_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetTradingSystemsGroupOveralDailyStatsAndInfo_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP]
AS
BEGIN
	
	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()

	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]

	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn
	
	SELECT
		ROW_NUMBER() OVER(ORDER BY
			trgroup.[OrderingRank] ASC,
			[TradingSystemGroupName] ASC,
			counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] ASC,
			beginSymbol.Name ASC) AS Rank,
		trgroup.[TradingSystemGroupName] AS TradingSystemGroup,
		trgroup.[TradingSystemGroupId] As TradingSystemGroupId,
		counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] AS TradingSystemTypeName,
		counterparty.[CounterpartyCode] AS Counterparty,
		systemtype.[TradingSystemTypeId] AS TradingSystemTypeId,
		statsPerType.ConfiguredSystems,
		--statsPerType.EnabledSystems,
		--this should be called active - and upper stay enabled
		CASE WHEN @TradingOn = 1 THEN statsPerType.ActiveSystems ELSE 0 END AS EnabledSystems,
		statsPerType.HardBlockedSystems,
		statsPerType.VolumeUsdM,
		statsPerType.DealsNum,
		statsPerType.LastDealUtc,
		statsPerType.KGTRejectionsNum,
		statsPerType.LastKgtRejectionUtc,
		statsPerType.CtpRejectionsNum,
		statsPerType.LastCptRejectionUtc,
		statsPerType.PnlGrossUsdPerMUsd,
		statsPerType.CommissionsUsdPerMUsd,
		statsPerType.PnlNetUsdPerMUsd,
		statsPerType.PnlGrossUsd,
		statsPerType.CommissionsUsd,
		statsPerType.PnlNetUsd,
		allowedType.IsOrderTransmissionDisabled,
		allowedType.IsGoFlatOnlyOn,
		monitorPages.TradingSystemMonitoringPageId AS PageId,
		beginSymbol.Name AS BeginSymbol,
		endSymbol.Name AS EndSymbol,
		monitorPages.StripsCount
	FROM
	(
	SELECT 
		allowedType.TradingSystemGroupId AS TradingSystemGroupId,
		allowedType.TradingSystemTypeId AS TradingSystemTypeId,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		COALESCE(SUM(CASE WHEN action.[Enabled] = 1 AND action.[GoFlatOnly] = 0 AND action.[OrderTransmissionDisabled] = 0 THEN 1 ELSE 0 END), 0) AS ActiveSystems,
		COALESCE(SUM(CONVERT(int, action.[HardBlocked])), 0) AS HardBlockedSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		MAX(stats.[LastDealUtc]) AS LastDealUtc,
		SUM(stats.[KGTRejectionsNum]) AS KGTRejectionsNum,
		MAX(stats.[LastKgtRejectionUtc]) AS LastKgtRejectionUtc,
		SUM(stats.[CtpRejectionsNum]) AS CtpRejectionsNum,
		MAX(stats.[LastCptRejectionUtc]) AS LastCptRejectionUtc,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemTypeAllowedForGroup] allowedType_forJoinsOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = allowedType_forJoinsOnly.[TradingSystemTypeId] AND trsystem.TradingSystemGroupId = allowedType_forJoinsOnly.TradingSystemGroupId
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		--link in deleted systems if needed
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		-- but make sure that those are only the allowed ones
		LEFT JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON 
			allowedType.[TradingSystemTypeId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId])
			AND
			allowedType.[TradingSystemGroupId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemGroupId], trsystem_deleted.[TradingSystemGroupId])

	GROUP BY
		allowedType.TradingSystemGroupId, allowedType.TradingSystemTypeId
	)
	statsPerType
	INNER JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON allowedType.TradingSystemGroupId = statsPerType.TradingSystemGroupId AND allowedType.TradingSystemTypeId = statsPerType.TradingSystemTypeId
	INNER JOIN [dbo].[TradingSystemType] systemtype ON statsPerType.[TradingSystemTypeId] = systemtype.[TradingSystemTypeId]
	INNER JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	RIGHT JOIN TradingSystemGroup trgroup ON trgroup.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId
	LEFT JOIN TradingSystemMonitoringPage monitorPages ON monitorPages.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId AND monitorPages.[TradingSystemTypeId] = statsPerType.TradingSystemTypeId
	LEFT JOIN [dbo].[Symbol] beginSymbol ON monitorPages.BeginSymbolId = beginSymbol.Id
	LEFT JOIN [dbo].[Symbol] endSymbol ON monitorPages.EndSymbolId = endSymbol.Id
END

GO
ALTER AUTHORIZATION ON [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetTradingSystemsInfoDaily_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTradingSystemsInfoDaily_SP]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP];

select * from
(select
s.Name'symbol',
'N/A' 'tradingSystemsGroup',  -- will be populated when group info is available in SQL
tstc.CounterpartyCode + ' ' + tst.SystemName'tradingSystemType',
pspts.TradingSystemId,
pspts.NopBasePol'openPositionBasePolarized',
pspts.VolumeUsdM,
pspts.PnlGrossUsd,
pspts.CommissionsUsd,
pspts.PnlNetUsd,
pspts.PnlGrossUsdPerMUsd,
pspts.CommissionsUsdPerMUsd,
pspts.PnlNetUsdPerMUsd,
pspts.DealsNum'dealsCount',
pspts.CtpRejectionsNum'rejectionsCount',
pspts.VolumeUsdM/pspts.DealsNum'avgDealMUsd',
pspts.PnlGrossUsd/pspts.DealsNum'pnlGrossUsdPerDeal',
pspts.CommissionsUsd/pspts.DealsNum'commUsdPerDeal',
pspts.PnlNetUsd/pspts.DealsNum'pnlNetUsdPerDeal'

-- TODO: display stats also for deleted trading systems
-- TODO: display stats for deals which are not linked to any trading system (Unknown)

from PersistedStatisticsPerTradingSystemDaily pspts with(nolock)

left join TradingSystem_All_Tbl() ts on pspts.TradingSystemId=ts.TradingSystemId
left join TradingSystemType tst  with(nolock) on ts.TradingSystemTypeId=tst.TradingSystemTypeId
left join Counterparty tstc  with(nolock) on tst.CounterpartyId=tstc.CounterpartyId
left join Symbol s with(nolock) on ts.SymbolId=s.Id

where pspts.DealsNum>0 -- workaround for "Unknown" TradingSystemId (-1) : FIX it
) t

where t.symbol is not null -- workaround for "Unknown" TradingSystemId (-1) : FIX it
END
GO
ALTER AUTHORIZATION ON [dbo].[GetTradingSystemsInfoDaily_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetTradingSystemsOveralDailyStats_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTradingSystemsOveralDailyStats_SP]
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()

	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]

	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn	
	
	SELECT 
		ISNULL(counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName], 'Unknown') AS SystemName,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		--COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		--this should be called active - and upper stay enabled
		CASE WHEN @TradingOn = 1 
			THEN 
			COALESCE(SUM(CASE WHEN action.[Enabled] = 1 AND action.[GoFlatOnly] = 0 AND action.[OrderTransmissionDisabled] = 0 THEN 1 ELSE 0 END), 0)
			ELSE 
			0 
			END AS EnabledSystems,
		COALESCE(SUM(CONVERT(int, action.[HardBlocked])), 0) AS HardBlockedSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		MAX(stats.[LastDealUtc]) AS LastDealUtc,
		SUM(stats.[KGTRejectionsNum]) AS KGTRejectionsNum,
		MAX(stats.[LastKgtRejectionUtc]) AS LastKgtRejectionUtc,
		SUM(stats.[CtpRejectionsNum]) AS CtpRejectionsNum,
		MAX(stats.[LastCptRejectionUtc]) AS LastCptRejectionUtc,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsPbUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsPbUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsCptUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsCptUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsPbUsd]) AS CommissionsPbUsd,
		SUM(stats.[CommissionsCptUsd]) AS CommissionsCptUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemType] systemtype_forJoinOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = systemtype_forJoinOnly.[TradingSystemTypeId]
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystemType] systemtype ON systemtype.[TradingSystemTypeId] = COALESCE(trsystem.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId], systemtype_forJoinOnly.[TradingSystemTypeId])
		LEFT JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	GROUP BY
		counterparty.[CounterpartyCode], systemtype.[SystemName]
	ORDER BY
		SystemName
END

GO
ALTER AUTHORIZATION ON [dbo].[GetTradingSystemsOveralDailyStats_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetTradingSystemsOveralDailyStats_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetTradingSystemTypeNames_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetTradingSystemTypeNames_SP]
AS
BEGIN
	SELECT
		[TradingSystemTypeId]
		,ctp.[CounterpartyCode] + ' ' + [SystemName] AS TradingSystemTypeName
	FROM 
		[dbo].[TradingSystemType] system
		INNER JOIN [dbo].[Counterparty] ctp ON ctp.CounterpartyId = system.CounterpartyId
END

GO
ALTER AUTHORIZATION ON [dbo].[GetTradingSystemTypeNames_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetTradingSystemTypeNames_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetUpdatedSystemsTradingControlSchedule_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUpdatedSystemsTradingControlSchedule_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@IsDailyRecords Bit = NULL,
	@IsWeeklyRecords Bit = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'SystemsTradingControlSchedule'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		SELECT
			[ScheduleEntryId]
			,[IsEnabled]
			,[IsDaily]
			,[IsWeekly]
			,[ScheduleTimeZoneSpecific]
			,[TimeZoneCode]
			,[ActionName]
			,[ScheduleDescription]
			,[LastUpdatedUtc]
		FROM 
			[dbo].[SystemsTradingControlSchedule] schedule
			INNER JOIN [dbo].[SystemsTradingControlScheduleAction] action on schedule.ActionId = action.ActionId
		WHERE
			(@IsDailyRecords IS NULL OR [IsDaily] = @IsDailyRecords)
			AND (@IsWeeklyRecords IS NULL OR [IsWeekly] = @IsWeeklyRecords)
		ORDER BY
			[IsDaily],
			[IsWeekly],
			[ScheduleTimeZoneSpecific]
	END
END

GO
ALTER AUTHORIZATION ON [dbo].[GetUpdatedSystemsTradingControlSchedule_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetUpdatedSystemsTradingControlSchedule_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetUpdatedSystemsTradingControlSchedule_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetUpdatedVenueCrossSystemSettings_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetUpdatedVenueCrossSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueCrossSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[MinimumFillSize]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueCrossSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name 
	END
END

GO
ALTER AUTHORIZATION ON [dbo].[GetUpdatedVenueCrossSystemSettings_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueCrossSystemSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueCrossSystemSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetUpdatedVenueGliderSystemSettings_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetUpdatedVenueGliderSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueGliderSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MaximumSizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[KGTLLTime_milliseconds]
			,[MinimumBPGrossGain]
			,[PreferLmaxDuringHedging]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumIntegratorGlidePriceLife_milliseconds]
			,[GlidingCutoffSpan_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,[MinimumBasisPointsFromKgtPriceToFastCancel]
			,[ReferenceLmaxBookOnly]
			,[BidEnabled]
			,[AskEnabled]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueGliderSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name
	END
END

GO
ALTER AUTHORIZATION ON [dbo].[GetUpdatedVenueGliderSystemSettings_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueGliderSystemSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueGliderSystemSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetUpdatedVenueMMSystemSettings_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUpdatedVenueMMSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueMMSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice]
			,[MaximumSizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,[MinimumBasisPointsFromKgtPriceToFastCancel]
			,[BidEnabled]
			,[AskEnabled]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueMMSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name
	END
END

GO
ALTER AUTHORIZATION ON [dbo].[GetUpdatedVenueMMSystemSettings_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueMMSystemSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueMMSystemSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetUpdatedVenueQuotingSystemSettings_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUpdatedVenueQuotingSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueQuotingSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty			
			,[MaximumPositionBaseAbs]
			,[FairPriceImprovementSpreadBasisPoints]
			,[StopLossNetInUsd]		
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueQuotingSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name 
	END
END

GO
ALTER AUTHORIZATION ON [dbo].[GetUpdatedVenueQuotingSystemSettings_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueQuotingSystemSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueQuotingSystemSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetUpdatedVenueStreamSystemSettings_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUpdatedVenueStreamSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueStreamSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MaximumSizeToTrade]
		    ,[BestPriceImprovementOffsetBasisPoints]
		    ,[MaximumDiscountBasisPointsToTrade] 
		    ,[KGTLLTime_milliseconds]
		    ,[PreferLLDestinationCheckToLmax]
		    ,[MinimumBPGrossToAccept]
		    ,[PreferLmaxDuringHedging]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,[MinimumBasisPointsFromKgtPriceToFastCancel]
			,[ReferenceLmaxBookOnly]
			,[BidEnabled]
			,[AskEnabled]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueStreamSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name
	END
END

GO
ALTER AUTHORIZATION ON [dbo].[GetUpdatedVenueStreamSystemSettings_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueStreamSystemSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueStreamSystemSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetVenueCrossSystemSettingsAndStats_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

---

CREATE PROCEDURE [dbo].[GetVenueCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumTimeBetweenSingleCounterpartySignals_seconds],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[MinimumFillSize],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetVenueCrossSystemSettingsAndStats_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetVenueCrossSystemSettingsAndStats_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetVenueGliderSystemSettingsAndStats_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetVenueGliderSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[MinimumBPGrossGain],
		sett.[MinimumBPGrossGain] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossGainDecimal],
		sett.[PreferLmaxDuringHedging],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumIntegratorGlidePriceLife_milliseconds],
		sett.[GlidingCutoffSpan_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[ReferenceLmaxBookOnly],
		sett.[BidEnabled],
		sett.[AskEnabled],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetVenueGliderSystemSettingsAndStats_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetVenueGliderSystemSettingsAndStats_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetVenueMMSystemSettingsAndStats_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetVenueMMSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[BidEnabled],
		sett.[AskEnabled],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetVenueMMSystemSettingsAndStats_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetVenueMMSystemSettingsAndStats_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetVenueQuotingSystemSettingsAndStats_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FairPriceImprovementSpreadBasisPoints],
		sett.[FairPriceImprovementSpreadBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [FairPriceImprovementSpreadDecimal],
		sett.[StopLossNetInUsd],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetVenueStreamSystemSettingsAndStats_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetVenueStreamSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[PreferLLDestinationCheckToLmax],
		sett.[MinimumBPGrossToAccept],
		sett.[MinimumBPGrossToAccept] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossToAcceptDecimal],
		sett.[PreferLmaxDuringHedging],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[ReferenceLmaxBookOnly],
		sett.[BidEnabled],
		sett.[AskEnabled],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END

GO
ALTER AUTHORIZATION ON [dbo].[GetVenueStreamSystemSettingsAndStats_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetVenueStreamSystemSettingsAndStats_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetVolumeExternal_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetVolumeExternal_SP]
(
	@StartDateUtc DATE,
	@EndDateUtc DATE,
	@VenueId tinyint
)
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

declare @StartDateUtcInternal DATE = @StartDateUtc;
declare @EndDateUtcInternal DATE = @EndDateUtc;

select
ve.TradeDate,ve.VolumeUsd

from 
VolumeExternal ve with(nolock) 

where 
ve.VenueId=@VenueId
and ve.TradeDate between @StartDateUtcInternal and @EndDateUtcInternal

order by
ve.TradeDate

END
GO
ALTER AUTHORIZATION ON [dbo].[GetVolumeExternal_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[InsertCommandItem_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[InsertCommandItem_SP] (
	@CommandText NVARCHAR(MAX)
	)
AS
BEGIN
	BEGIN TRANSACTION
	   DECLARE @CommandId int;

		INSERT INTO [dbo].[Command]
			   ([CommandText]
			   ,[CreationTime])
		 VALUES
			   (@CommandText
			   ,GETUTCDATE())

	   SELECT @CommandId = scope_identity();

	   INSERT INTO [dbo].[NewCommand]
           ([CommandId])
		VALUES
           (@CommandId)
	COMMIT
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertCommandItem_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertCommandItem_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertDealExternalExecuted_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 2),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL,
	@SingleFillSystemId int = NULL,
	@MultipleFillsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL,
	@CounterpartyClientId varchar(20) = NULL)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @MultipleFillsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleFillsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external deal for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@SingleFillSystemId, -1)
				,@AmountBasePolExecuted
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc)
		END
		
		
		INSERT INTO 
		[dbo].[DealExternalExecuted]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolExecuted]
			   ,[AmountTermPolExecutedInUsd]
			   ,[Price]
			   ,[CounterpartyTransactionId]
			   ,[CounterpartySuppliedExecutionTimeStampUtc]
			   ,[ValueDateCounterpartySuppliedLocMktDate]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[InternalTransactionIdentity]
			   ,[FlowSideId]
			   ,[CounterpartyClientId])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolExecuted
			   ,@AmountTermPolExecutedInUsd
			   ,@Price
			   ,@CounterpartyTransactionId
			   ,@CounterpartyExecutionReportTimeStampUtc
			   ,@ValueDateCounterpartySuppliedLocMktDate
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@CounterpartySentExecutionReportUtc
			   ,@InternalDealId
			   ,@FlowSideId
			   ,@CounterpartyClientId)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertDealExternalExecuted_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertDealExternalExecuted_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertDealExternalRejected_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 2),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@RejectedPrice decimal(18, 9),
	@SingleRejectionSystemId int = NULL,
	@MultipleRejectionsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL,
	@RejectionDirection varchar(16),
	@CounterpartyClientId varchar(20) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionDirectionId BIT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = @RejectionDirection
	
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'RejectionDirection not found in DB: %s', 16, 2, @RejectionDirection) WITH SETERROR
	END

	BEGIN TRY
	
		BEGIN TRANSACTION tr1;
		
		IF @MultipleRejectionsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@RejectedPrice
				,@RejectionReason
				,@InternalOrderId
				,@RejectionDirectionId
				,@ExecutionReportReceivedUtc
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleRejectionsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external rejection for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			VALUES
				(ISNULL(@SingleRejectionSystemId, -1)
				,@AmountBasePolRejected
				,@SymbolId
				,@CounterpartyId
				,@RejectedPrice
				,@RejectionReason
				,@InternalOrderId
				,@RejectionDirectionId
				,@ExecutionReportReceivedUtc)
		END

		INSERT INTO 
		[dbo].[DealExternalRejected]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[SymbolId]
			   ,[CounterpartyId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolRejected]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[RejectionReason]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[RejectedPrice]
			   ,[RejectionDirectionId]
			   ,[CounterpartyClientId])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@SymbolId
			   ,@CounterpartyId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolRejected
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@RejectionReason
			   ,@CounterpartySentExecutionReportUtc
			   ,@RejectedPrice
			   ,@RejectionDirectionId
			   ,@CounterpartyClientId)
		
		COMMIT TRANSACTION tr1;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION tr1;
		THROW
	END CATCH
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertDealExternalRejected_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertDealExternalRejected_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertExecutedExternalDeal_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertExecutedExternalDeal_SP]( 
	@OrderId NVARCHAR(32),
	@ExecutionTimeStampUtc DATETIME,
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@ExecutedAmountBaseAbs DECIMAL(18,0),
	@ExecutedAmountBasePol DECIMAL(18,0),
	@Price decimal(18, 9)
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
		[dbo].[ExecutedExternalDeals]
           ([OrderId]
           ,[ExecutionTimeStampUtc]
           ,[SymbolId]
           ,[DealDirectionId]
           ,[ExecutedAmountBaseAbs]
           ,[ExecutedAmountBasePol]
           ,[Price])
     VALUES
           (@OrderId
           ,@ExecutionTimeStampUtc
           ,@SymbolId
           ,@DealDirectionId 
           ,@ExecutedAmountBaseAbs
           ,@ExecutedAmountBasePol
           ,@Price)
END
GO
ALTER AUTHORIZATION ON [dbo].[InsertExecutedExternalDeal_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertExecutedExternalDeal_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertExecutionReportMessage_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertExecutionReportMessage_SP]( 
	@ExecutionReportReceivedUtc [datetime2](7),
	@ExecutionReportStatus [varchar](16),
	@InternalOrderId [nvarchar](45),
	@ExecutionReportCounterpartySentUtc [datetime2](7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@RawFIXMessage [nvarchar](1024)
	)
AS
BEGIN

	DECLARE @ExecutionReportStatusId INT

	SELECT @ExecutionReportStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ExecutionReportStatus

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ExecutionReportStatus not found in DB: %s', 16, 2, @ExecutionReportStatus) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[ExecutionReportMessage]
           ([IntegratorReceivedExecutionReportUtc]
           ,[ExecutionReportStatusId]
           ,[IntegratorExternalOrderPrivateId]
           ,[CounterpartySentExecutionReportUtc]
		   ,[IntegratorSentExternalOrderUtc]
           ,[FixMessageReceivedRaw])
     VALUES
           (
		   @ExecutionReportReceivedUtc
           ,@ExecutionReportStatusId
           ,@InternalOrderId
           ,@ExecutionReportCounterpartySentUtc
		   ,@IntegratorSentExternalOrderUtc
           ,@RawFIXMessage
		   )
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertExecutionReportMessage_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertExecutionReportMessage_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertNewAggregatedDealTicket_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertNewAggregatedDealTicket_SP]( 
	@AggregatedTradeReportCounterpartyId [nvarchar](50),
	@TicketIntegratorReceivedTimeUtc [datetime2](7),
	@CounterpartySentTimeUtc [datetime2](7),
	@AggregatedPrice decimal(18,10),
	@AggregatedSizeBaseAbs decimal(18,2),
	@StpCounterpartyName [varchar](16),
	@FixMessageRaw nvarchar(1024),
	@ChildTradeIdsList nvarchar(MAX)
	)
AS
BEGIN

	DECLARE @StpCounterpartyId TINYINT
	
	SELECT @StpCounterpartyId = [StpCounterpartyId] FROM [dbo].[StpCounterparty] WHERE [StpCounterpartyName] = @StpCounterpartyName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'StpCounterparty not found in DB: %s', 16, 2, @StpCounterpartyName) WITH SETERROR
	END

	INSERT INTO [dbo].[AggregatedExternalExecutedTicket]
           ([AggregatedExternalExecutedTicketCounterpartyId]
           ,[IntegratorReceivedTimeUtc]
           ,[CounterpartySentTimeUtc]
           ,[AggregatedSizeBaseAbs]
           ,[AggregatedPrice]
           ,[StpCounterpartyId]
           ,[FixMessageRaw])
     VALUES
           (@AggregatedTradeReportCounterpartyId
           ,@TicketIntegratorReceivedTimeUtc
           ,@CounterpartySentTimeUtc
           ,@AggregatedSizeBaseAbs
           ,@AggregatedPrice
           ,@StpCounterpartyId
           ,@FixMessageRaw)
		   
		   
	INSERT INTO [dbo].[AggregatedExternalExecutedTicketMap]
		([AggregatedExternalExecutedTicketCounterpartyId],
		[InternalTransactionIdentity])
	SELECT
		@AggregatedTradeReportCounterpartyId, Code
	FROM
		[dbo].[csvlist_to_codes_tbl](@ChildTradeIdsList)	
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertNewAggregatedDealTicket_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertNewAggregatedDealTicket_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertNewDealTicket_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertNewDealTicket_SP]( 
	@InternalDealId [nvarchar](50),
	@TicketIntegratorSentOrReceivedTimeUtc [datetime2](7),
	@CounterpartySentTimeUtc [datetime2](7),
	@DealExternalExecutedTicketType [nvarchar](17),
	@StpCounterpartyName [varchar](16),
	@FixMessageRaw nvarchar(1024)
	)
AS
BEGIN

	DECLARE @DealExternalExecutedTicketTypeId INT
	DECLARE @StpCounterpartyId TINYINT

	SELECT @DealExternalExecutedTicketTypeId = [DealExternalExecutedTicketTypeId] FROM [dbo].[DealExternalExecutedTicketType] WHERE [DealExternalExecutedTicketTypeName] = @DealExternalExecutedTicketType

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealExternalExecutedTicketType not found in DB: %s', 16, 2, @DealExternalExecutedTicketType) WITH SETERROR
	END
	
	SELECT @StpCounterpartyId = [StpCounterpartyId] FROM [dbo].[StpCounterparty] WHERE [StpCounterpartyName] = @StpCounterpartyName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'StpCounterparty not found in DB: %s', 16, 2, @StpCounterpartyName) WITH SETERROR
	END

	INSERT INTO 
		[dbo].[DealExternalExecutedTicket]
           ([InternalTransactionIdentity]
           ,[IntegratorSentOrReceivedTimeUtc]
		   ,[CounterpartySentTimeUtc]
           ,[DealExternalExecutedTicketTypeId]
		   ,[StpCounterpartyId]
           ,[FixMessageRaw])
     VALUES
           (@InternalDealId
           ,@TicketIntegratorSentOrReceivedTimeUtc
		   ,@CounterpartySentTimeUtc
           ,@DealExternalExecutedTicketTypeId
		   ,@StpCounterpartyId
           ,@FixMessageRaw)
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertNewDealTicket_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertNewDealTicket_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertNewOrderExternal_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertNewOrderExternal_SP]( 
	@ExternalOrderSentUtc [datetime2](7),
	@InternalOrderId [nvarchar](45),
	@InternalClientOrderId [nvarchar](32),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@QuoteReceivedToOrderSentInternalLatency [time](7),
	@FixMessageSentRaw nvarchar(1024),
	@OrderTypeName varchar(16),
	@OrderTimeInForceName varchar(16),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@SourceIntegratorPriceIdentity uniqueidentifier = NULL,
	@SourceIntegratorPriceTimeUtc [datetime2](7) = NULL,
	@PegTypeName varchar(16) = NULL,
	@PegDifferenceBasePol decimal(18,9) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @OrderTypeId TINYINT
	DECLARE @OrderTimeInForceId TINYINT
	DECLARE @PegTypeId BIT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	SELECT @OrderTypeId = [OrderTypeId] FROM [dbo].[OrderType] WHERE [OrderTypeName] = @OrderTypeName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderType not found in DB: %s', 16, 2, @OrderTypeName) WITH SETERROR
	END
	
	SELECT @OrderTimeInForceId = [OrderTimeInForceId] FROM [dbo].[OrderTimeInForce] WHERE [OrderTimeInForceName] = @OrderTimeInForceName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderTimeInForce not found in DB: %s', 16, 2, @OrderTimeInForceName) WITH SETERROR
	END
	
	IF @OrderTypeName = 'Pegged'
	BEGIN
		SELECT @PegTypeId = [PegTypeId] FROM [dbo].[PegType] WHERE [PegTypeName] = @PegTypeName
		
		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'PegType not found in DB: %s', 16, 2, @PegTypeName) WITH SETERROR
		END
		
		IF @PegDifferenceBasePol IS NULL
		BEGIN
			RAISERROR (N'PegDifferenceBasePol was not specified for pegged order - but it is compulsory', 16, 2) WITH SETERROR
		END
	END

	INSERT INTO 
		[dbo].[OrderExternal]
           ([IntegratorSentExternalOrderUtc]
           ,[IntegratorExternalOrderPrivateId]
		   ,[ExternalOrderLabelForCounterparty]
           ,[CounterpartyId]
           ,[SymbolId]
		   ,[DealDirectionId]
           ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[FixMessageSentRaw]
		   ,[OrderTypeId]
		   ,[OrderTimeInForceId]
		   ,[RequestedPrice]
		   ,[RequestedAmountBaseAbs]
		   ,[SourceIntegratorPriceIdentity]
		   ,[SourceIntegratorPriceTimeUtc]
		   ,[PegTypeId]
		   ,[PegDifferenceBasePol])
     VALUES
           (@ExternalOrderSentUtc
           ,@InternalOrderId
		   ,@InternalClientOrderId
           ,@CounterpartyId
           ,@SymbolId
		   ,@DealDirectionId
           ,@QuoteReceivedToOrderSentInternalLatency
		   ,@FixMessageSentRaw
		   ,@OrderTypeId
		   ,@OrderTimeInForceId
		   ,@RequestedPrice
		   ,@RequestedAmountBaseAbs
		   ,@SourceIntegratorPriceIdentity
		   ,@SourceIntegratorPriceTimeUtc
		   ,@PegTypeId
		   ,@PegDifferenceBasePol)
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertNewOrderExternal_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertNewOrderExternal_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertNewOrderExternalIncomingRejectable_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
CREATE PROCEDURE [dbo].[InsertNewOrderExternalIncomingRejectable_SP]( 
    @ExternalOrderReceived [datetime2](7),
	@ExternalOrderSentUtc [datetime2](7),
	@IntegratorSentExecutionReportUtc [datetime2](7),
	@CounterpartyClientOrderId [varchar](32),
	@CounterpartyClientId [varchar](20),
	@TradingSystemId int = NULL,
	@SourceIntegratorPriceIdentity [varchar](20) = NULL,
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@IntegratorExecId [varchar](20),
	@ValueDate [date] = NULL,
	@OrderReceivedToExecutionReportSentLatency [time](7),
	@IntegratorExecutedAmountBaseAbs [decimal](18, 2) = NULL,
	@IntegratorRejectedAmountBaseAbs [decimal](18, 2) = NULL,
	@RejectionReason [varchar](32) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WITH(NOLOCK) WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WITH(NOLOCK) WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WITH(NOLOCK) WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @RejectionReason IS NOT NULL
	BEGIN
		SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
		IF @@ROWCOUNT <> 1
		BEGIN
			INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
			SET @RejectionReasonId = @@IDENTITY
		END
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @IntegratorExecutedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,@IntegratorExecId
				,@ExternalOrderReceived)
		END
		
		IF @IntegratorRejectedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,ISNULL(@RejectionReason, '<NONE>')
				,@IntegratorExecId
				,@RejectionDirectionId
				,@ExternalOrderReceived)
		END

		INSERT INTO 
			[dbo].[OrderExternalIncomingRejectable_Daily]
			   ([IntegratorReceivedExternalOrderUtc]
			   ,[CounterpartySentExternalOrderUtc]
			   ,[IntegratorSentExecutionReportUtc]
			   ,[CounterpartyClientOrderId]
			   ,[CounterpartyClientId]
			   ,[TradingSystemId]
			   ,[SourceIntegratorPriceIdentity]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorDealDirectionId]
			   ,[CounterpartyRequestedPrice]
			   ,[CounterpartyRequestedAmountBasePol]
			   ,[IntegratorExecutionId]
			   ,[ValueDate]
			   ,[OrderReceivedToExecutionReportSentLatency ]
			   ,[IntegratorExecutedAmountBasePol]
			   ,[IntegratorRejectedAmountBasePol]
			   ,[IntegratorRejectionReasonId])
		 VALUES
			   (@ExternalOrderReceived
			   ,@ExternalOrderSentUtc
			   ,@IntegratorSentExecutionReportUtc
			   ,@CounterpartyClientOrderId
			   ,@CounterpartyClientId
			   ,@TradingSystemId
			   ,@SourceIntegratorPriceIdentity
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@DealDirectionId
			   ,@RequestedPrice
			   ,CASE WHEN @DealDirectionId = 1 THEN -@RequestedAmountBaseAbs ELSE @RequestedAmountBaseAbs END
			   ,@IntegratorExecId
			   ,@ValueDate
			   ,@OrderReceivedToExecutionReportSentLatency 
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
		       ,@RejectionReasonId)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH	   
		   	   
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertNewOrderExternalIncomingRejectable_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertNewOrderExternalIncomingRejectable_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertNewOrderExternalIncomingRejectableTimeout_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertNewOrderExternalIncomingRejectableTimeout_SP]( 
    @TimeoutReceivedUtc [datetime2](7),
	@TimeoutSentUtc [datetime2](7),
	@CounterpartyClientOrderId [varchar](32)
	)
AS
BEGIN

	INSERT INTO 
		[dbo].[OrderExternalIncomingTimeoutsFromCounterparty]
		   ([IntegratorReceivedTimeoutUtc]
		   ,[CounterpartySentTimeoutUtc]
		   ,[CounterpartyClientOrderId])
	 VALUES
		   (@TimeoutReceivedUtc
		   ,@TimeoutSentUtc
		   ,@CounterpartyClientOrderId)  
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertNewOrderExternalIncomingRejectableTimeout_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertNewOrderExternalIncomingRejectableTimeout_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertOrderExternalCancelRequest_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertOrderExternalCancelRequest_SP]( 
	@IntegratorExternalOrderPrivateId [nvarchar](45),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
    @IntegratorSentCancelRequestUtc  DATETIME2(7),
    @OrderExternalCancelTypeName VARCHAR(16),
    @RawFIXMessage [nvarchar](1024)
	)
AS
BEGIN

	DECLARE @OrderExternalCancelTypeId TINYINT

	SELECT @OrderExternalCancelTypeId = [OrderExternalCancelTypeId] FROM [dbo].[OrderExternalCancelType] WHERE [OrderExternalCancelTypeName] = @OrderExternalCancelTypeName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderExternalCancelTypeName not found in DB: %s', 16, 2, @OrderExternalCancelTypeName) WITH SETERROR
	END


	INSERT INTO [dbo].[OrderExternalCancelRequest]
           ([IntegratorExternalOrderPrivateId]
		   ,[IntegratorSentExternalOrderUtc]
           ,[IntegratorSentCancelRequestUtc]
           ,[OrderExternalCancelTypeId]
           ,[FixMessageSentRaw])
     VALUES
           (@IntegratorExternalOrderPrivateId
		   ,@IntegratorSentExternalOrderUtc
           ,@IntegratorSentCancelRequestUtc
           ,@OrderExternalCancelTypeId
           ,@RawFIXMessage)
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertOrderExternalCancelRequest_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertOrderExternalCancelRequest_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertOrderExternalCancelResult_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertOrderExternalCancelResult_SP]( 
	@IntegratorExternalOrderPrivateId [nvarchar](45),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
    @CounterpartySentResultUtc  DATETIME2(7),
    @CancelledAmountBasePol decimal(18,2),
    @ResultStatusName VARCHAR(16)
	)
AS
BEGIN

	DECLARE @ResultStatusId TINYINT

	SELECT @ResultStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ResultStatusName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ResultStatusName not found in DB: %s', 16, 2, @ResultStatusName) WITH SETERROR
	END


	INSERT INTO [dbo].[OrderExternalCancelResult]
           ([IntegratorExternalOrderPrivateId]
		   ,[IntegratorSentExternalOrderUtc]
           ,[CounterpartySentResultUtc]
           ,[CanceledAmountBasePol]
           ,[ResultStatusId])
     VALUES
           (@IntegratorExternalOrderPrivateId
		   ,@IntegratorSentExternalOrderUtc
           ,@CounterpartySentResultUtc
           ,@CancelledAmountBasePol
           ,@ResultStatusId)
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertOrderExternalCancelResult_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertOrderExternalCancelResult_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertOrderExternalIgnored_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[InsertOrderExternalIgnored_SP]( 
	@ExternalOrderSentUtc [datetime2](7),
	@ExternalOrderIgnoreDetectedUtc [datetime2](7),
	@IntegratorExternalOrderPrivateId [nvarchar](45),
	@InternalClientOrderId [nvarchar](32),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4)
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END

	INSERT INTO 
	[dbo].[OrderExternalIgnored]
           ([IntegratorSentExternalOrderUtc]
           ,[IntegratorDetectedOrderIsIgnoredUtc]
           ,[IntegratorExternalOrderPrivateId]
		   ,[ExternalOrderLabelForCounterparty]
		   ,[RequestedAmountBaseAbs]
		   ,[RequestedPrice]
		   ,[CounterpartyId]
		   ,[SymbolId]
		   ,[DealDirectionId])
     VALUES
           (@ExternalOrderSentUtc
           ,@ExternalOrderIgnoreDetectedUtc
           ,@IntegratorExternalOrderPrivateId
		   ,@InternalClientOrderId
		   ,@RequestedAmountBaseAbs
		   ,@RequestedPrice
		   ,@CounterpartyId
		   ,@SymbolId
		   ,@DealDirectionId)
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertOrderExternalIgnored_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertOrderExternalIgnored_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertTradeDecayAnalysisRequests_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertTradeDecayAnalysisRequests_SP]
(
	@FromTimeUtc [datetime2](7) = NULL,
	@ToTimeUtc [datetime2](7) = NULL
)
AS
BEGIN

	IF @ToTimeUtc IS NULL
	BEGIN
		SET @ToTimeUtc = DATEADD(minute, -5, GETUTCDATE())
	END
	
	IF @FromTimeUtc IS NULL
	BEGIN
		SELECT @FromTimeUtc = [LastProcessedRecordUtc] FROM [dbo].[TradeDecay_LastProcessedRecord]
	END
	
	DECLARE @TypeCptDealId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'CptDeal')
	DECLARE @TypeCptRejectionId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'CptRejection')
	DECLARE @TypeKgtDealId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'KgtDeal')
	DECLARE @TypeKgtRejectionId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'KgtRejection')
	
	
	DECLARE @TempRequestsToAdd TABLE (
		[TradeDecayAnalysisTypeId] [tinyint] NOT NULL,
		[SourceEventGlobalRecordId] [int] NOT NULL,
		[SourceEventIndexTimeUtc] [datetime2](7) NOT NULL,
		[AnalysisStartTimeUtc] [datetime2](7) NOT NULL,
		[FXPairId] [tinyint] NOT NULL,
		[CounterpartyId] [tinyint] NOT NULL,
		[ReferencePrice] [decimal](18,9) NOT NULL,
		[PriceSideId] [bit] NOT NULL
	)
	
	INSERT INTO @TempRequestsToAdd(
		[TradeDecayAnalysisTypeId],
		[SourceEventGlobalRecordId],
		[SourceEventIndexTimeUtc],
		[AnalysisStartTimeUtc],
		[FXPairId],
		[CounterpartyId],
		[ReferencePrice],
		[PriceSideId]
	)
	SELECT
		@TypeCptDealId,
		[GlobalRecordId],
		[IntegratorReceivedExecutionReportUtc],
		CASE WHEN [FlowSideId] = 0 OR ([FlowSideId] IS NULL AND cpt.CounterpartyName IN ('Hotspot', 'FXall', 'FXCM')) 
		THEN DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExecutionReportUtc])
		ELSE [IntegratorSentExternalOrderUtc] END,
		symbolIdMap.[DCDBSymbolId],
		cptIdMap.[DCDBCounterpartyId],
		[Price],
		CASE WHEN [DealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN [dbo].[Counterparty] cpt ON deal.CounterpartyId = cpt.CounterpartyId
		INNER JOIN [dbo].[PriceDelaySettings] delaySett ON deal.CounterpartyId = delaySett.CounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON deal.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON deal.SymbolId = symbolIdMap.IntegratorDBSymbolId
		--cannot use between due to inclusions
		WHERE [IntegratorReceivedExecutionReportUtc] > @FromTimeUtc AND [IntegratorReceivedExecutionReportUtc] < @ToTimeUtc
	UNION ALL
	SELECT
		@TypeCptRejectionId,
		[GlobalRecordId],
		[IntegratorReceivedExecutionReportUtc],
		CASE WHEN cpt.CounterpartyName IN ('Hotspot', 'FXall', 'FXCM') 
		THEN DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExecutionReportUtc]) 
		ELSE [IntegratorSentExternalOrderUtc] END,
		symbolIdMap.[DCDBSymbolId],
		cptIdMap.[DCDBCounterpartyId],
		[RejectedPrice],
		CASE WHEN [DealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
	FROM 
		[dbo].[DealExternalRejected] reject
		INNER JOIN [dbo].[Counterparty] cpt ON reject.CounterpartyId = cpt.CounterpartyId
		INNER JOIN [dbo].[PriceDelaySettings] delaySett ON reject.CounterpartyId = delaySett.CounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON reject.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON reject.SymbolId = symbolIdMap.IntegratorDBSymbolId
		--cannot use between due to inclusions
		WHERE [IntegratorReceivedExecutionReportUtc] > @FromTimeUtc AND [IntegratorReceivedExecutionReportUtc] < @ToTimeUtc
		AND [RejectedPrice] IS NOT NULL
	UNION ALL
	SELECT
		CASE WHEN [IntegratorRejectionReasonId] IS NULL THEN @TypeKgtDealId ELSE @TypeKgtRejectionId END,
		[GlobalRecordId],
		[IntegratorReceivedExternalOrderUtc],
		DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExternalOrderUtc]),
		symbolIdMap.[DCDBSymbolId],
		cptIdMap.[DCDBCounterpartyId],
		[CounterpartyRequestedPrice],
		CASE WHEN [IntegratorDealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
	FROM 
		[dbo].[OrderExternalIncomingRejectable_Daily] cptOrder
		INNER JOIN [dbo].[PriceDelaySettings] delaySett ON cptOrder.CounterpartyId = delaySett.CounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON cptOrder.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON cptOrder.SymbolId = symbolIdMap.IntegratorDBSymbolId
		--cannot use between due to inclusions
		WHERE [IntegratorReceivedExternalOrderUtc] > @FromTimeUtc AND [IntegratorReceivedExternalOrderUtc] < @ToTimeUtc
	
	IF DATEDIFF(day, @FromTimeUtc, GETUTCDATE()) != 0
	BEGIN
		INSERT INTO @TempRequestsToAdd(
			[TradeDecayAnalysisTypeId],
			[SourceEventGlobalRecordId],
			[SourceEventIndexTimeUtc],
			[AnalysisStartTimeUtc],
			[FXPairId],
			[CounterpartyId],
			[ReferencePrice],
			[PriceSideId]
		)
		SELECT
			CASE WHEN [IntegratorRejectionReasonId] IS NULL THEN @TypeKgtDealId ELSE @TypeKgtRejectionId END,
			[GlobalRecordId],
			[IntegratorReceivedExternalOrderUtc],
			DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExternalOrderUtc]),
			symbolIdMap.[DCDBSymbolId],
			cptIdMap.[DCDBCounterpartyId],
			[CounterpartyRequestedPrice],
			CASE WHEN [IntegratorDealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
		FROM 
			[dbo].[OrderExternalIncomingRejectable_Archive] cptOrder
			INNER JOIN [dbo].[PriceDelaySettings] delaySett ON cptOrder.CounterpartyId = delaySett.CounterpartyId
			INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON cptOrder.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
			INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON cptOrder.SymbolId = symbolIdMap.IntegratorDBSymbolId
			--cannot use between due to inclusions
			WHERE [IntegratorReceivedExternalOrderUtc] > @FromTimeUtc AND [IntegratorReceivedExternalOrderUtc] < @ToTimeUtc
	END
	
	
	DECLARE  @LastRecordTimeUtc [datetime2](7)
	
	SELECT @LastRecordTimeUtc = MAX([SourceEventIndexTimeUtc]) FROM @TempRequestsToAdd
	
	IF @LastRecordTimeUtc IS NOT NULL
	BEGIN
		--try catch used so that execution flow is stopped after error
		BEGIN TRY
		
			INSERT INTO [FXtickDB].[dbo].[TradeDecayAnalysis_Pending]
				   ([TradeDecayAnalysisTypeId]
				   ,[SourceEventGlobalRecordId]
				   ,[AnalysisStartTimeUtc]
				   ,[FXPairId]
				   ,[CounterpartyId]
				   ,[ReferencePrice]
				   ,[PriceSideId])
			SELECT
				[TradeDecayAnalysisTypeId],
				[SourceEventGlobalRecordId],
				[AnalysisStartTimeUtc],
				[FXPairId],
				[CounterpartyId],
				[ReferencePrice],
				[PriceSideId]
			FROM
				@TempRequestsToAdd
				   
			UPDATE [dbo].[TradeDecay_LastProcessedRecord] SET [LastProcessedRecordUtc] = @LastRecordTimeUtc 

		END TRY
		BEGIN CATCH
			print 'error during storing';
			THROW;
		END CATCH
	END  
	ELSE
	BEGIN
		-- so that we do not search same intervals over and over if no trading
		UPDATE [dbo].[TradeDecay_LastProcessedRecord] SET [LastProcessedRecordUtc] = DATEADD(minute, -5, @ToTimeUtc)
	END
	
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertTradeDecayAnalysisRequests_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[InsertTradingSystemMonitoringPages_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[InsertTradingSystemMonitoringPages_SP]
(
	@BeginSymbol NCHAR(7),
	@EndSymbol NCHAR(7),
	@StripsCount INT,
	@SystemGroupId INT,
	@SystemTypeId INT,
	@TradingSystemMonitoringPageId INT OUTPUT
)
AS
BEGIN

	DECLARE @BeginSymbolId INT
	DECLARE @EndSymbolId INT

	SELECT @BeginSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @BeginSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @BeginSymbol) WITH SETERROR
	END
	
	SELECT @EndSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @EndSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @EndSymbol) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystemMonitoringPage]
           (TradingSystemGroupId
		   ,[TradingSystemTypeId]
           ,[BeginSymbolId]
           ,[EndSymbolId]
           ,[StripsCount])
     VALUES
           (@SystemGroupId,
		   @SystemTypeId,
           @BeginSymbolId,
           @EndSymbolId,
           @StripsCount)
	
	SELECT @TradingSystemMonitoringPageId = scope_identity()
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertTradingSystemMonitoringPages_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertTradingSystemMonitoringPages_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[IsKnownPrivateIP_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--We might need multiple instances in different states (e.g. ShuttingDown and Starting)
--ALTER TABLE [dbo].[IntegratorProcess] ADD  CONSTRAINT [OneProcessPerInstanceConstraint] UNIQUE NONCLUSTERED 
--(
--	[IntegratorInstanceId] ASC,
--	[IntegratorProcessTypeId] ASC
--) ON [PRIMARY]
--GO


CREATE PROCEDURE [dbo].[IsKnownPrivateIP_SP]( 
	@PrivateIPAsString varchar(15)
	)
AS
BEGIN
	SELECT
		1
	FROM
		[dbo].[IntegratorEndpoint]
	WHERE
		[PrivateIPBinary] = [dbo].[GetBinaryIPv4FromString](@PrivateIPAsString)
END

GO
ALTER AUTHORIZATION ON [dbo].[IsKnownPrivateIP_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[IsKnownPrivateIP_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[NewCommissionScheduleHandling_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NewCommissionScheduleHandling_SP]
AS
BEGIN
	
	--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
	SET NOCOUNT ON
	
	IF EXISTS(
		SELECT 1

		FROM
			(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY [PrimeBrokerId], [ValidFromUtc]) AS Schedule1RowId,
				[PrimeBrokerId]
				,[ValidFromUtc]
				--,[ValidToUtc]
			FROM 
				[dbo].[CommissionsSchedule_PBCommission]
			) schedule1

			INNER JOIN

			(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY [PrimeBrokerId], [ValidFromUtc]) AS Schedule2RowId,
				[PrimeBrokerId]
				,[ValidFromUtc]
				,[ValidToUtc]
			FROM 
				[dbo].[CommissionsSchedule_PBCommission]
			) schedule2
			ON schedule1.PrimeBrokerId = schedule2.PrimeBrokerId AND schedule1.Schedule1RowId != schedule2.Schedule2RowId

		WHERE
			schedule1.[ValidFromUtc] >= schedule2.[ValidFromUtc] AND schedule1.[ValidFromUtc] <= schedule2.[ValidToUtc]
		)
	BEGIN
		RAISERROR (N'Multiple currently valid records for single Counterparty', 16, 2 ) WITH SETERROR
		ROLLBACK TRAN
	END
	
	IF EXISTS(
		SELECT 1

		FROM
			(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY [CounterpartyId], [ValidFromUtc]) AS Schedule1RowId,
				[CounterpartyId]
				,[ValidFromUtc]
				--,[ValidToUtc]
			FROM 
				[dbo].[CommissionsSchedule_CptCommission]
			) schedule1

			INNER JOIN

			(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY [CounterpartyId], [ValidFromUtc]) AS Schedule2RowId,
				[CounterpartyId]
				,[ValidFromUtc]
				,[ValidToUtc]
			FROM 
				[dbo].[CommissionsSchedule_CptCommission]
			) schedule2
			ON schedule1.CounterpartyId = schedule2.CounterpartyId AND schedule1.Schedule1RowId != schedule2.Schedule2RowId

		WHERE
			schedule1.[ValidFromUtc] >= schedule2.[ValidFromUtc] AND schedule1.[ValidFromUtc] <= schedule2.[ValidToUtc]
		)
	BEGIN
		RAISERROR (N'Multiple currently valid records for single Counterparty', 16, 2 ) WITH SETERROR
		ROLLBACK TRAN
	END
	
	IF EXISTS(
		SELECT 1

		FROM
			(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY [CounterpartyId], [PrimeBrokerId], [ValidFromUtc]) AS Schedule1RowId,
				[PrimeBrokerId],
				[CounterpartyId]
				,[ValidFromUtc]
				--,[ValidToUtc]
			FROM 
				[dbo].[CommissionsSchedule_Mapping]
			) schedule1

			INNER JOIN

			(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY [CounterpartyId], [PrimeBrokerId], [ValidFromUtc]) AS Schedule2RowId,
				[PrimeBrokerId],
				[CounterpartyId]
				,[ValidFromUtc]
				,[ValidToUtc]
			FROM 
				[dbo].[CommissionsSchedule_Mapping]
			) schedule2
			ON 
				schedule1.CounterpartyId = schedule2.CounterpartyId 
				AND schedule1.Schedule1RowId != schedule2.Schedule2RowId

		WHERE
			schedule1.[ValidFromUtc] >= schedule2.[ValidFromUtc] AND schedule1.[ValidFromUtc] <= schedule2.[ValidToUtc]
		)
	BEGIN
		RAISERROR (N'Multiple currently valid records for single Counterparty', 16, 2 ) WITH SETERROR
		ROLLBACK TRAN
	END
	
	DECLARE @TodayUtc DATE = GETUTCDATE()

	
	TRUNCATE TABLE [dbo].[CommissionsPerCounterparty] 
	
	INSERT INTO [dbo].[CommissionsPerCounterparty] 
		([PrimeBrokerId], [CounterpartyId], [CounterpartyCommissionPpm], [PbCommissionPpm])
	SELECT 
		map.[PrimeBrokerId],
		map.[CounterpartyId],
		cpt.[CounterpartyCommissionPpm],
		pb.[PbCommissionPpm]
	FROM 
		[dbo].[CommissionsSchedule_Mapping] map
		INNER JOIN [dbo].[CommissionsSchedule_CptCommission] cpt ON map.[CounterpartyId] = cpt.CounterpartyId
		INNER JOIN [dbo].[CommissionsSchedule_PBCommission] pb ON map.[PrimeBrokerId] = pb.[PrimeBrokerId]
	WHERE
		map.[ValidFromUtc] <= @TodayUtc AND map.[ValidToUtc] >= @TodayUtc
		AND cpt.[ValidFromUtc] <= @TodayUtc AND cpt.[ValidToUtc] >= @TodayUtc
		AND pb.[ValidFromUtc] <= @TodayUtc AND pb.[ValidToUtc] >= @TodayUtc
END

GO
ALTER AUTHORIZATION ON [dbo].[NewCommissionScheduleHandling_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[RegisterIntegratorProcess_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RegisterIntegratorProcess_SP](
	@InstanceName varchar(50),
	@ProcessName varchar(50),
	@PrivateIPAsString varchar(15)
	)
AS
BEGIN
	
	DECLARE @InstanceId TINYINT
	DECLARE @ProcessTypeId TINYINT
	DECLARE @EndpointId TINYINT

	SELECT @InstanceId = Id FROM [dbo].[IntegratorInstance] WHERE Name = @InstanceName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'InstanceName not found in DB: %s', 16, 2, @InstanceName) WITH SETERROR
	END

	SELECT @ProcessTypeId = [Id] FROM [dbo].[IntegratorProcessType] WHERE [Name] = @ProcessName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ProcessType not found in DB: %s', 16, 2, @ProcessName) WITH SETERROR
	END
	
	SELECT @EndpointId = [Id] FROM [dbo].[IntegratorEndpoint] WHERE [PrivateIPBinary] = [dbo].[GetBinaryIPv4FromString](@PrivateIPAsString)

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Endpoint not found in DB: %s', 16, 2, @PrivateIPAsString) WITH SETERROR
	END
	
	INSERT INTO [dbo].[IntegratorProcess] (IntegratorInstanceId, IntegratorProcessTypeId, IntegratorProcessStateId, IntegratorEndpointId, LastHBTimeUtc)
	VALUES (@InstanceId, @ProcessTypeId, (SELECT [Id] FROM [dbo].[IntegratorProcessState] WHERE Name = 'Starting'), @EndpointId, GetUtcDate())
	
	DECLARE @ProcessId INT = SCOPE_IDENTITY()
	
	exec [dbo].[GetIntegratorProcessDetails_SP] @InstanceName = @InstanceName, @ProcessName = @ProcessName
	
	return @ProcessId
END

GO
ALTER AUTHORIZATION ON [dbo].[RegisterIntegratorProcess_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[RegisterIntegratorProcess_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[RejectExecutedOrderExternalIncomingRejectable_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[RejectExecutedOrderExternalIncomingRejectable_SP]( 
	@IntegratorExecId  [varchar](20),
	@RejectionReason [varchar](20) = NULL
	)
AS
BEGIN
	DECLARE @OriginallyExecutedAmount decimal(18,2)
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT 
		@OriginallyExecutedAmount = [IntegratorExecutedAmountBasePol] 
	FROM 
		[dbo].[OrderExternalIncomingRejectable_Daily]
	WHERE 
		[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) AND [IntegratorExecutionId] = @IntegratorExecId

	IF @OriginallyExecutedAmount IS NOT NULL
	BEGIN
	
		IF @RejectionReason IS NOT NULL
		BEGIN
			SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
			IF @@ROWCOUNT <> 1
			BEGIN
				INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
				SET @RejectionReasonId = @@IDENTITY
			END
		END
	
		BEGIN TRY

		BEGIN TRANSACTION;
	
			UPDATE
				[dbo].[OrderExternalIncomingRejectable_Daily]
			SET
				[IntegratorRejectedAmountBasePol] += @OriginallyExecutedAmount,
				[IntegratorExecutedAmountBasePol] = 0,
				[IntegratorRejectionReasonId] = @RejectionReasonId
			WHERE 
				[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) 
				AND [IntegratorExecutionId] = @IntegratorExecId

				
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			SELECT
				[TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,ISNULL(@RejectionReason, '<NONE>')
				,[InternalTransactionIdentity]
				,@RejectionDirectionId
				,[DealTimeUtc]
			FROM
				[dbo].[TradingSystemDealsExecuted_Daily]
			WHERE
				[InternalTransactionIdentity] = @IntegratorExecId
					
			DELETE FROM
				[dbo].[TradingSystemDealsExecuted_Daily]
			WHERE
				[InternalTransactionIdentity] = @IntegratorExecId
			
		COMMIT TRANSACTION;
		
		SELECT 
			[IntegratorReceivedExternalOrderUtc]
			,[CounterpartySentExternalOrderUtc]
			,[CounterpartyClientOrderId]
			,[TradingSystemId]
			,[SourceIntegratorPriceIdentity]
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.[Name] as Symbol
			,direction.[Name] as DealDirection
			,[CounterpartyRequestedPrice]
			,@OriginallyExecutedAmount AS OriginallyExecutedAmountBasePol
			,[IntegratorExecutionId]
			,[ValueDate]
		FROM 
			[dbo].[OrderExternalIncomingRejectable_Daily] deals
			INNER JOIN [dbo].[Symbol] symbol ON deals.[SymbolId] = symbol.[Id]
			INNER JOIN [dbo].[Counterparty] counterparty ON deals.[CounterpartyId] = counterparty.[CounterpartyId]
			INNER JOIN [dbo].[DealDirection] direction ON deals.[IntegratorDealDirectionId] = direction.Id
		WHERE 
			[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) AND [IntegratorExecutionId] = @IntegratorExecId
		
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION;
			THROW
		END CATCH
		
	END
END

GO
ALTER AUTHORIZATION ON [dbo].[RejectExecutedOrderExternalIncomingRejectable_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[RejectExecutedOrderExternalIncomingRejectable_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[ReleaseSessionFromInstance_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ReleaseSessionFromInstance_SP]( 
	@InstanceName varchar(50),
	@Counterparty char(3)
	)
AS
BEGIN
	DELETE FROM [dbo].[IntegratorInstanceSessionClaim]
	FROM 
		[dbo].[IntegratorInstanceSessionClaim] map
		INNER JOIN [dbo].[Counterparty] ctp on ctp.CounterpartyId = map.CounterpartyId
		INNER JOIN [dbo].[IntegratorInstance] instance on instance.Id = map.IntegratorInstanceId
	WHERE
		instance.Name = @InstanceName
		AND ctp.CounterpartyCode = @Counterparty
END

GO
ALTER AUTHORIZATION ON [dbo].[ReleaseSessionFromInstance_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[ReleaseSessionFromInstance_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[SettlementDatesClearAll_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SettlementDatesClearAll_SP]
AS
BEGIN
	UPDATE
		[dbo].[SettlementDate]
	SET
		[SettlementDate] = NULL
END

GO
ALTER AUTHORIZATION ON [dbo].[SettlementDatesClearAll_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[SettlementDatesClearAll_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[SettlementDatesClearSelected_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SettlementDatesClearSelected_SP]
(
	@SymbolsCsvList varchar(MAX)
)
AS
BEGIN
	UPDATE
		settlDate
	SET
		settlDate.[SettlementDate] = NULL
	FROM
		[dbo].[csvlist_to_codes_tbl](@SymbolsCsvList) symbolNames
		INNER JOIN Symbol sym ON sym.ShortName = symbolNames.Code
		INNER JOIN [dbo].[SettlementDate] settlDate ON settlDate.SymbolId = sym.Id
END

GO
ALTER AUTHORIZATION ON [dbo].[SettlementDatesClearSelected_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[SettlementDatesClearSelected_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[SettlementDatesUpdateSelected_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SettlementDatesUpdateSelected_SP]
(
	@SymbolsAndDatesList varchar(MAX)
)
AS
BEGIN	
	UPDATE
		settlDate
	SET
		settlDate.[SettlementDate] = dates.dateValue
	FROM
		[dbo].[csvlist_to_codesanddates_tbl](@SymbolsAndDatesList) dates
		INNER JOIN Symbol sym ON sym.ShortName = dates.code
		INNER JOIN [dbo].[SettlementDate] settlDate ON settlDate.SymbolId = sym.Id
END

GO
ALTER AUTHORIZATION ON [dbo].[SettlementDatesUpdateSelected_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[SettlementDatesUpdateSelected_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[StreamingSessionStatsToday_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[StreamingSessionStatsToday_SP]
(
	@CounterpartyCode [char](3)
)
AS
BEGIN
	SELECT 
		SUM(CASE WHEN [IntegratorRejectedAmountBasePol] > 0 THEN 1 ELSE 0 END) AS RejectionsNum,
		SUM(CASE WHEN [IntegratorExecutedAmountBasePol] > 0 THEN 1 ELSE 0 END) AS DealsNum
	FROM
		[dbo].[OrderExternalIncomingRejectable_Daily] ord
		INNER JOIN [dbo].[Counterparty] ctp on ord.CounterpartyId = ctp.CounterpartyId
	WHERE
		ctp.[CounterpartyCode] = @CounterpartyCode
END

GO
ALTER AUTHORIZATION ON [dbo].[StreamingSessionStatsToday_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[StreamingSessionStatsToday_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[SystemsTradingControlScheduleDeleteOne_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SystemsTradingControlScheduleDeleteOne_SP] (
	@ScheduleEntryId int
	)
AS
BEGIN

	DELETE FROM
		[dbo].[SystemsTradingControlSchedule]
	WHERE
		ScheduleEntryId = @ScheduleEntryId
END

GO
ALTER AUTHORIZATION ON [dbo].[SystemsTradingControlScheduleDeleteOne_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[SystemsTradingControlScheduleDeleteOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[SystemsTradingControlScheduleDisableEnableOne_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SystemsTradingControlScheduleDisableEnableOne_SP] (
	@ScheduleEntryId int,
	@Enable Bit
	)
AS
BEGIN

	UPDATE
		[dbo].[SystemsTradingControlSchedule]
	SET
		IsEnabled = @Enable
	WHERE
		ScheduleEntryId = @ScheduleEntryId
END

GO
ALTER AUTHORIZATION ON [dbo].[SystemsTradingControlScheduleDisableEnableOne_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[SystemsTradingControlScheduleDisableEnableOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[SystemsTradingControlScheduleInsertSingle_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SystemsTradingControlScheduleInsertSingle_SP]
(
	@Enable Bit,
	@IsDaily Bit,
	@IsWeekly Bit,
	@ScheduleTimeZoneSpecific datetime2(7),
	@TimeZoneCode char(3),
	@ActionName varchar(24),
	@ScheduleDescription NVARCHAR(1024),
	@ScheduleEntryId int OUTPUT
)
AS
BEGIN
	INSERT INTO [dbo].[SystemsTradingControlSchedule]
           ([IsEnabled]
		   ,[IsDaily]
           ,[IsWeekly]
           ,[ScheduleTimeZoneSpecific]
		   ,[TimeZoneCode]
           ,[ActionId]
           ,[ScheduleDescription])
     VALUES
           (@Enable
			,@IsDaily
           ,@IsWeekly
           ,@ScheduleTimeZoneSpecific
		   ,@TimeZoneCode
           ,(SELECT [ActionId] FROM [dbo].[SystemsTradingControlScheduleAction] WHERE [ActionName] = @ActionName)
           ,@ScheduleDescription)
	
	SELECT @ScheduleEntryId = scope_identity();
END

GO
ALTER AUTHORIZATION ON [dbo].[SystemsTradingControlScheduleInsertSingle_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[SystemsTradingControlScheduleInsertSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[SystemsTradingControlScheduleUpdateSingle_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SystemsTradingControlScheduleUpdateSingle_SP]
(
	@ScheduleEntryId int,
	@Enable Bit,
	@IsDaily Bit,
	@IsWeekly Bit,
	@ScheduleTimeZoneSpecific datetime2(7),
	@TimeZoneCode char(3),
	@ActionName varchar(24),
	@ScheduleDescription NVARCHAR(1024)
)
AS
BEGIN
	UPDATE 
		[dbo].[SystemsTradingControlSchedule]
	SET
		[IsEnabled] = @Enable
		,[IsDaily] = @IsDaily
		,[IsWeekly] = @IsWeekly
		,[ScheduleTimeZoneSpecific] = @ScheduleTimeZoneSpecific
		,[TimeZoneCode] = @TimeZoneCode
		,[ActionId] = (SELECT [ActionId] FROM [dbo].[SystemsTradingControlScheduleAction] WHERE [ActionName] = @ActionName)
		,[ScheduleDescription] = @ScheduleDescription
	WHERE
		[ScheduleEntryId] = @ScheduleEntryId
END

GO
ALTER AUTHORIZATION ON [dbo].[SystemsTradingControlScheduleUpdateSingle_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[SystemsTradingControlScheduleUpdateSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[Test1]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Test1]
(
	@Day Date,
	@CounterpartyCodesList VARCHAR(100) = NULL
)
AS

BEGIN

	DECLARE @temp_ids TABLE
	(
		CounterpartyId int
	)

	IF @CounterpartyCodesList IS NOT NULL
	BEGIN
		INSERT INTO @temp_ids(CounterpartyId)
		SELECT 
			CounterpartyId 
		FROM 
			dbo.Counterparty ctp 
			JOIN dbo.csvlist_to_codes_tbl(@CounterpartyCodesList) list ON ctp.CounterpartyCode = list.code
	END

	DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		RejectionsNum int
	)

	INSERT INTO @temp_rejections(CounterpartyId, RejectionsNum)
	SELECT
		[CounterpartyId],
		COUNT([IntegratorReceivedExecutionReportUtc]) AS NumberOfRejections
	FROM
		[dbo].[DealExternalRejected] rej
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		DealsNum int
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol, VolumeBaseAbs, DealsNum)
	SELECT
		symbol.[Id] AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		symbol.[Id],
		deal.CounterpartyId
		


	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		RejectionsStatistics.RejectionsNum AS RejectionsNum,
		RejectionsStatistics.RejectionsNum * 1.0 / (RejectionsStatistics.RejectionsNum + DealsStatistics.DealsNum) AS RejectionsRate
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			--SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(DealsNum) AS DealsNum
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(DealsNum) AS DealsNum
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.DealsNum AS DealsNum
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

					--@temp_NOPs_pol basePos
					--INNER JOIN Symbol sym ON basePos.SymbolId = sym.Id
					--INNER JOIN Currency ccy1 ON ccy1.CurrencyID = sym.BaseCurrencyId

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode

END
GO
ALTER AUTHORIZATION ON [dbo].[Test1] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[Test2]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Test2]
(
	@Day Date--,
	--@CounterpartyCodesList VARCHAR(100) = NULL
)
AS

BEGIN

	DECLARE @temp_ids TABLE
	(
		CounterpartyId int
	)

	--IF @CounterpartyCodesList IS NOT NULL
	--BEGIN
	--	INSERT INTO @temp_ids(CounterpartyId)
	--	SELECT 
	--		CounterpartyId 
	--	FROM 
	--		dbo.Counterparty ctp 
	--		JOIN dbo.csvlist_to_codes_tbl(@CounterpartyCodesList) list ON ctp.CounterpartyCode = list.code
	--END

	DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		RejectionsNum int
	)

	INSERT INTO @temp_rejections(CounterpartyId, RejectionsNum)
	SELECT
		[CounterpartyId],
		COUNT([IntegratorReceivedExecutionReportUtc]) AS NumberOfRejections
	FROM
		[dbo].[DealExternalRejected] rej
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		DealsNum int
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol, VolumeBaseAbs, DealsNum)
	SELECT
		symbol.[Id] AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		symbol.[Id],
		deal.CounterpartyId
		


	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		RejectionsStatistics.RejectionsNum AS RejectionsNum,
		RejectionsStatistics.RejectionsNum * 1.0 / (RejectionsStatistics.RejectionsNum + DealsStatistics.DealsNum) AS RejectionsRate
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			--SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(DealsNum) AS DealsNum
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(DealsNum) AS DealsNum
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.DealsNum AS DealsNum
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

					--@temp_NOPs_pol basePos
					--INNER JOIN Symbol sym ON basePos.SymbolId = sym.Id
					--INNER JOIN Currency ccy1 ON ccy1.CurrencyID = sym.BaseCurrencyId

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode

END
GO
ALTER AUTHORIZATION ON [dbo].[Test2] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[TradingSystemGroupAdd_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TradingSystemGroupAdd_SP]
(
	@NewName NVARCHAR(32)
)
AS
BEGIN

	INSERT INTO
		[dbo].[TradingSystemGroup] (TradingSystemGroupName)
	VALUES
		(@NewName)
END

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemGroupAdd_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TradingSystemGroupAdd_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TradingSystemGroupAddType_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TradingSystemGroupAddType_SP]
(
	@GroupId INT,
	@TypeId INT
)
AS
BEGIN

	INSERT INTO [dbo].[TradingSystemTypeAllowedForGroup]
           ([TradingSystemTypeId]
           ,[TradingSystemGroupId]
           ,[IsGoFlatOnlyOn]
           ,[IsOrderTransmissionDisabled])
     VALUES
           (@TypeId
           ,@GroupId
           ,0
           ,0)
END

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemGroupAddType_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TradingSystemGroupAddType_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TradingSystemGroupDelete_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TradingSystemGroupDelete_SP]
(
	@TradingSystemGroupId INT
)
AS
BEGIN

	DECLARE @TradingSystemIdsToDelete IdsTable
	
	INSERT INTO @TradingSystemIdsToDelete (Id)
	SELECT 
		[TradingSystemId]
	FROM
		[dbo].[TradingSystem]
	WHERE
		TradingSystemGroupId = @TradingSystemGroupId
		
	EXEC [dbo].[VenueSystemDeleteMulti_SP] @TradingSystemIdsToDelete
	
	DELETE FROM [dbo].[TradingSystemTypeAllowedForGroup]
    WHERE TradingSystemGroupId = @TradingSystemGroupId
	
	DELETE FROM
		[dbo].[TradingSystemMonitoringPage]
	WHERE
		TradingSystemGroupId = @TradingSystemGroupId
	
	DELETE FROM [dbo].[TradingSystemGroup]
	WHERE TradingSystemGroupId = @TradingSystemGroupId

END

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemGroupDelete_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TradingSystemGroupDelete_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TradingSystemGroupDeleteSubtype_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TradingSystemGroupDeleteSubtype_SP]
(
	@TradingSystemGroupId INT,
	@TradingSystemTypeId INT
)
AS
BEGIN

	DECLARE @TradingSystemIdsToDelete IdsTable
	
	INSERT INTO @TradingSystemIdsToDelete (Id)
	SELECT 
		[TradingSystemId]
	FROM
		[dbo].[TradingSystem]
	WHERE
		TradingSystemTypeId = @TradingSystemTypeId AND TradingSystemGroupId = @TradingSystemGroupId
		
	EXEC [dbo].[VenueSystemDeleteMulti_SP] @TradingSystemIdsToDelete
	
	DELETE FROM [dbo].[TradingSystemTypeAllowedForGroup]
    WHERE TradingSystemTypeId = @TradingSystemTypeId AND TradingSystemGroupId = @TradingSystemGroupId
	
	DELETE FROM
		[dbo].[TradingSystemMonitoringPage]
	WHERE
		TradingSystemTypeId = @TradingSystemTypeId AND TradingSystemGroupId = @TradingSystemGroupId
END

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemGroupDeleteSubtype_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TradingSystemGroupDeleteSubtype_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TradingSystemGroupHasActiveSystems_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TradingSystemGroupHasActiveSystems_SP]
(
	@GroupId INT
)
AS
BEGIN
	SELECT 
		COUNT(system.[TradingSystemId])
	FROM 
		[dbo].[TradingSystem] system
		INNER JOIN [dbo].[TradingSystemActions] action ON action.TradingSystemId = system.TradingSystemId
	WHERE
		system.TradingSystemGroupId = @GroupId AND action.Enabled = 1
END

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemGroupHasActiveSystems_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TradingSystemGroupHasActiveSystems_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TradingSystemGroupRename_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TradingSystemGroupRename_SP]
(
	@TradingSystemGroupId INT,
	@NewName NVARCHAR(32)
)
AS
BEGIN
	UPDATE 
		[dbo].[TradingSystemGroup]
	SET
		[TradingSystemGroupName] = @NewName
	WHERE 
		TradingSystemGroupId = @TradingSystemGroupId 
END

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemGroupRename_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TradingSystemGroupRename_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TradingSystemPageExists_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--disallow detail pages from actions if hidden

CREATE PROCEDURE [dbo].[TradingSystemPageExists_SP]
(
	@GroupId INT,
	@PageId INT
)
AS
BEGIN
	SELECT CONVERT(bit, 1) AS IsPresent
	FROM [dbo].[TradingSystemMonitoringPage]
	WHERE [TradingSystemMonitoringPageId] = @PageId AND [TradingSystemGroupId] = @GroupId
END

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemPageExists_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TradingSystemPageExists_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TradingSystemsGroupDisableEnableMulti_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TradingSystemsGroupDisableEnableMulti_SP]
(
	@Enable [bit],
	@SystemsGroupId [int],
	@SystemTypeId [int] = NULL
)
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
	
		UPDATE 
			action
		SET
			[Enabled] = @Enable
		FROM
			[dbo].[TradingSystemActions] action
			INNER JOIN [dbo].[TradingSystem]  trsystem ON action.[TradingSystemId] = trsystem.[TradingSystemId]
		WHERE 
			trsystem.[TradingSystemGroupId] = @SystemsGroupId
			AND (@SystemTypeId IS NULL OR trsystem.[TradingSystemTypeId] = @SystemTypeId)
			AND [HardBlocked] = 0
	
		IF @Enable = 1
			exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemsGroupDisableEnableMulti_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TradingSystemsGroupDisableEnableMulti_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TradingSystemsGroupResetMulti_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TradingSystemsGroupResetMulti_SP]
(
	@SystemsGroupId [int],
	@SystemTypeId [int] = NULL
)
AS
BEGIN
	UPDATE 
		action
	SET
		[LastResetRequestedUtc] = GETUTCDATE(),
		[HardBlocked] = 0
	FROM
		[dbo].[TradingSystemActions] action
		INNER JOIN [dbo].[TradingSystem]  trsystem ON action.[TradingSystemId] = trsystem.[TradingSystemId]
	WHERE 
		trsystem.[TradingSystemGroupId] = @SystemsGroupId
		AND (@SystemTypeId IS NULL OR trsystem.[TradingSystemTypeId] = @SystemTypeId)
		AND [Enabled] = 0
END

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemsGroupResetMulti_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TradingSystemsGroupResetMulti_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TradingSystemsGroupUpdateGoFlatOnly_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TradingSystemsGroupUpdateGoFlatOnly_SP]
(
	@GoFlatOnlyOn [bit],
	@SystemsGroupId [int],
	@SystemTypeId [int] = NULL
)
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
	
		UPDATE 
			[dbo].[TradingSystemTypeAllowedForGroup]
		SET
			[IsGoFlatOnlyOn] = @GoFlatOnlyOn
		WHERE 
			TradingSystemGroupId = @SystemsGroupId
			AND (@SystemTypeId IS NULL OR TradingSystemTypeId = @SystemTypeId)
	
		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemsGroupUpdateGoFlatOnly_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TradingSystemsGroupUpdateGoFlatOnly_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TradingSystemsGroupUpdateTransmissionControl_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TradingSystemsGroupUpdateTransmissionControl_SP]
(
	@OrderTransmissionDisabled [bit],
	@SystemsGroupId [int],
	@SystemTypeId [int] = NULL
)
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
	
		UPDATE 
			[dbo].[TradingSystemTypeAllowedForGroup]
		SET
			[IsOrderTransmissionDisabled] = @OrderTransmissionDisabled
		WHERE 
			TradingSystemGroupId = @SystemsGroupId
			AND (@SystemTypeId IS NULL OR TradingSystemTypeId = @SystemTypeId)
	
		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemsGroupUpdateTransmissionControl_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TradingSystemsGroupUpdateTransmissionControl_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TryClaimSessionsForInstance_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[TryClaimSessionsForInstance_SP]( 
	@InstanceName varchar(50),
	@Counterparty char(3)
	)
AS
BEGIN
	DECLARE @InstanceId TINYINT
	DECLARE @CounterpartyId TINYINT

	SELECT @InstanceId = Id FROM [dbo].[IntegratorInstance] WHERE Name = @InstanceName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'InstanceName not found in DB: %s', 16, 2, @InstanceName) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @Counterparty

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Counterparty not found in DB: %s', 16, 2, @Counterparty) WITH SETERROR
	END

	BEGIN TRY
		INSERT INTO [dbo].[IntegratorInstanceSessionClaim] (IntegratorInstanceId, CounterpartyId)
		VALUES (@InstanceId, @CounterpartyId)
	END TRY
	BEGIN CATCH
			SELECT 
				instance.Name
			FROM 
				[dbo].[IntegratorInstanceSessionClaim] map
				INNER JOIN [dbo].[IntegratorInstance] instance on instance.Id = map.IntegratorInstanceId
			WHERE
				map.CounterpartyId = @CounterpartyId
	END CATCH
END

GO
ALTER AUTHORIZATION ON [dbo].[TryClaimSessionsForInstance_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TryClaimSessionsForInstance_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TryLoginKillSwitchUser_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TryLoginKillSwitchUser_SP] 
	@username NVARCHAR(256),
	@password NVARCHAR(256),
	@identity NVARCHAR(256),
	@siteName NVARCHAR(64)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SiteId INT 
	SELECT @SiteId = [Id] FROM [dbo].[KillSwitchSite] WHERE [Name] = @siteName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Site not found in DB: %s', @siteName, 16, 2) WITH SETERROR
	END

    DECLARE @t TABLE (Loged BIT, IsAdmin BIT)
	DECLARE @Loged BIT
	DECLARE @IsAdmin BIT
	SET @Loged = 0
	SET @IsAdmin = 0

	SELECT 
		@Loged = 1, @IsAdmin = usr.IsAdmin
	FROM 
		[dbo].[KillSwitchUser] usr
	WHERE
		usr.UserName = @username AND usr.Password = @password AND usr.SiteId = @SiteId


	INSERT INTO 
		[dbo].[KillSwitchLogon]
           ([UserName]
           ,[ActionTime]
           ,[Source]
           ,[Success]
		   ,[SiteId])
     VALUES
           (@username
           ,GETUTCDATE()
           ,@identity
           ,@Loged
		   ,@SiteId)

	INSERT INTO @t VALUES (@Loged,@IsAdmin)
	SELECT Loged, IsAdmin FROM @t
END

GO
ALTER AUTHORIZATION ON [dbo].[TryLoginKillSwitchUser_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TryLoginKillSwitchUser_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TryLoginRiskMgmtUser_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TryLoginRiskMgmtUser_SP] 
	@username NVARCHAR(256),
	@password NVARCHAR(256),
	@identity NVARCHAR(256)
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @t TABLE (Loged BIT, IsAdmin BIT, RoleName nvarchar(64))
	DECLARE @Loged BIT
	DECLARE @IsAdmin BIT
	DECLARE @RoleName NVARCHAR(64)
	DECLARE @RoleId INT
	SET @Loged = 0
	SET @IsAdmin = 0
	SET @RoleId = 2

		--first select the role id (so that it's correct even for unsuccessful logons)
	SELECT 
		@RoleId = s.Id
	FROM 
		[dbo].[KillSwitchUser] usr
		INNER JOIN [dbo].[KillSwitchRole] s ON usr.RoleId = s.Id
	WHERE
		usr.UserName = @username

	SELECT 
		@Loged = 1, @IsAdmin = usr.IsAdmin, @RoleName = s.Name
	FROM 
		[dbo].[KillSwitchUser] usr
		INNER JOIN [dbo].[KillSwitchRole] s ON usr.RoleId = s.Id
	WHERE
		usr.UserName = @username AND usr.Password = @password

	INSERT INTO 
		[dbo].[KillSwitchLogon]
           ([UserName]
           ,[ActionTime]
           ,[Source]
           ,[Success]
		   ,[RoleId])
     VALUES
           (@username
           ,GETUTCDATE()
           ,@identity
           ,@Loged
		   ,@RoleId)

	INSERT INTO @t VALUES (@Loged,@IsAdmin,@RoleName)
	SELECT Loged, IsAdmin, RoleName FROM @t
END

GO
ALTER AUTHORIZATION ON [dbo].[TryLoginRiskMgmtUser_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TryLoginRiskMgmtUser_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateCommandItem_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[UpdateCommandItem_SP] (
	@CommandId int,
	@UpdateStatus NVARCHAR(32)
	)
AS
BEGIN
	INSERT INTO [dbo].[CommandStatus]
           ([CommandId]
           ,[Status]
           ,[Time])
     VALUES
           (@CommandId
           ,@UpdateStatus
           ,GETUTCDATE())
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdateCommandItem_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateCommandItem_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateCounterpartyMonitorSettings_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UpdateCounterpartyMonitorSettings_SP] 
	@EnvironmentName NVARCHAR(100),
	@SettingsFriendlyName NVARCHAR(200),
	@SettingsXml Xml
AS
BEGIN
	DECLARE @CurrentDiagDashboardSettingId INT
	DECLARE @CurrentDiagDashboardSettingFriendlyName NVARCHAR(200)
	DECLARE @EnvironmentId INT


	SELECT
		@CurrentDiagDashboardSettingId = sett.Id,
		@CurrentDiagDashboardSettingFriendlyName = sett.FriendlyName,
		@EnvironmentId = envint.Id
	FROM
		[dbo].[IntegratorEnvironment] envint 
		INNER JOIN [dbo].[IntegratorEnvironmentToSettingsMap] map ON envint.Id = map.IntegratorEnvironmentId
		INNER JOIN [dbo].[Settings] sett ON sett.id = map.SettingsId
	WHERE
		envint.Name = @EnvironmentName
		AND sett.Name = 'Kreslik.Integrator.DiagnosticsDashboard.exe'

	IF @CurrentDiagDashboardSettingFriendlyName = @SettingsFriendlyName
	BEGIN
		-- This is update to current Diag dashboar setting
		UPDATE 
		[dbo].[Settings] 
		SET 
			[SettingsXml] = @SettingsXml, 
			[LastUpdated] = GETUTCDATE()
		WHERE
			[Id] = @CurrentDiagDashboardSettingId

		--and we are done
	END
	ELSE
	BEGIN
		DECLARE @NewDiagDashboardSettingId INT = NULL

		SELECT
			@NewDiagDashboardSettingId = sett.Id
		FROM
			[dbo].[Settings] sett
		WHERE
			sett.Name = 'Kreslik.Integrator.DiagnosticsDashboard.exe'
			AND sett.FriendlyName = @SettingsFriendlyName

		IF @NewDiagDashboardSettingId IS NULL
		BEGIN
			-- This is creation of fresh new setting 
			
			INSERT INTO [dbo].[Settings]
				([Name]
				,[FriendlyName]
				,[SettingsVersion]
				,[SettingsXml]
				,[LastUpdated]
				,[IsOnlineMonitored])
			VALUES
				(N'Kreslik.Integrator.DiagnosticsDashboard.exe'
				,@SettingsFriendlyName
				,1
				,@SettingsXml
				,GETUTCDATE()
				,0
				)

			SELECT @NewDiagDashboardSettingId = SCOPE_IDENTITY()

		END
		ELSE
		BEGIN
			-- This is update of existing setting outside current environment
			
			UPDATE 
			[dbo].[Settings] 
			SET 
				[SettingsXml] = @SettingsXml, 
				[LastUpdated] = GETUTCDATE()
			WHERE
				[Id] = @NewDiagDashboardSettingId
		END

		-- Now we need to remap environment

		UPDATE 
			[dbo].[IntegratorEnvironmentToSettingsMap]
		SET 
			[SettingsId] = @NewDiagDashboardSettingId
		WHERE
			[IntegratorEnvironmentId] = @EnvironmentId
			AND [SettingsId] = @CurrentDiagDashboardSettingId
	END
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdateCounterpartyMonitorSettings_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateCounterpartyMonitorSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateEodRates_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdateEodRates_SP]
AS
BEGIN
	UPDATE 
		eodConversions
	SET
		eodConversions.[BidPrice] = rates.[LastKnownBidMeanPrice]
		,eodConversions.[AskPrice] = rates.[LastKnownAskMeanPrice]
	FROM
		[FXTickDB].[dbo].[EODRate] eodConversions
		INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] map ON eodConversions.[FXPairId] = map.DCDBSymbolId
		INNER JOIN [dbo].[Symbol_Rates] rates ON rates.[Id] = map.[IntegratorDBSymbolId]
	WHERE
		[Date] = CAST(GETUTCDATE() AS DATE)
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdateEodRates_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateEodRates_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateEodUsdRates_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdateEodUsdRates_SP]
AS
BEGIN

	UPDATE 
		eodConversions
	SET
		eodConversions.[BidPrice] = rates.[LastKnownUsdConversionMultiplier]
		,eodConversions.[AskPrice] = rates.[LastKnownUsdConversionMultiplier]
	FROM
		[FXTickDB].[dbo].[EODUsdConversions] eodConversions
		INNER JOIN [FXTickDB].[dbo].[_Internal_CurrencyIdsMapping] map ON eodConversions.[CurrencyID] = map.[DCDBCurrencyId] 
		INNER JOIN [dbo].[Currency_Rates] rates ON rates.[CurrencyID] = map.[IntegratorDBCurrencyId]
	WHERE
		[Date] = CAST(GETUTCDATE() AS DATE)
		AND rates.[LastKnownUsdConversionMultiplier] IS NOT NULL

END

GO
ALTER AUTHORIZATION ON [dbo].[UpdateEodUsdRates_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateEodUsdRates_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateIntegratorProcessHBTime_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UpdateIntegratorProcessHBTime_SP](
	@ProcessIdentifier INT
	)
AS
BEGIN
	UPDATE [dbo].[IntegratorProcess]
	SET [LastHBTimeUtc] = GetUtcDate()
	WHERE [Id] = @ProcessIdentifier
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdateIntegratorProcessHBTime_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateIntegratorProcessHBTime_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateIntegratorProcessState_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateIntegratorProcessState_SP](
	@ProcessIdentifier INT,
	@NewProcessState varchar(25)
	)
AS
BEGIN
	DECLARE @StateId TINYINT

	SELECT @StateId = Id FROM [dbo].[IntegratorProcessState] WHERE Name = @NewProcessState

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ProcessState not found in DB: %s', 16, 2, @NewProcessState) WITH SETERROR
	END
	
	UPDATE [dbo].[IntegratorProcess]
	SET 
		[IntegratorProcessStateId] = @StateId,
		[LastHBTimeUtc] = GetUtcDate()
	WHERE [Id] = @ProcessIdentifier
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdateIntegratorProcessState_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateIntegratorProcessState_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
AS
BEGIN

	--create the temp table before transaction to speed up things
	
	CREATE TABLE #TempCounterpartyStatistics(
		[Counterparty] [varchar](3) NOT NULL,
		[NopAbs] decimal(18,2) NULL,
		[VolumeBaseAbsInUsd] decimal(18, 2) NULL,
		[VolumeShare] decimal(18, 4) NULL,
		[CommissionsPbUsd] decimal(18, 2) NULL,
		[CommissionsCptUsd] decimal(18, 2) NULL,
		[CommissionsSumUsd] decimal(18, 2) NULL,
		[DealsNum] int NULL,
		[LastDealUtc] datetime2(7) NULL,
		[AvgDealSizeUsd] decimal(18,2) NULL,
		[KGTRejectionsNum] int NULL,
		[LastKgtRejectionUtc] datetime2(7) NULL,
		[CtpRejectionsNum] int NULL,
		[LastCptRejectionUtc] datetime2(7) NULL,
		[KGTRejectionsRate] decimal(18,4) NULL,
		[CtpRejectionsRate] decimal(18,4) NULL,
		[AvgIntegratorLatencyNanoseconds] bigint NULL,
		[AvgCounterpartyLatencyNanoseconds] bigint
	)

	DECLARE @Today DATE = GETUTCDATE()
	
	INSERT INTO 
		#TempCounterpartyStatistics
		(
		[Counterparty],
		[NopAbs],
		[VolumeBaseAbsInUsd],
		[VolumeShare],
		[CommissionsPbUsd],
		[CommissionsCptUsd],
		[CommissionsSumUsd],
		[DealsNum],
		[LastDealUtc],
		[AvgDealSizeUsd],
		[KGTRejectionsNum],
		[LastKgtRejectionUtc],
		[CtpRejectionsNum],
		[LastCptRejectionUtc],
		[KGTRejectionsRate],
		[CtpRejectionsRate],
		[AvgIntegratorLatencyNanoseconds],
		[AvgCounterpartyLatencyNanoseconds]
		)
	exec [dbo].[GetStatisticsPerCounterparty_Daily_SP] @Today

	BEGIN TRANSACTION
	   TRUNCATE TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily]

	   INSERT INTO 
			[dbo].[PersistedStatisticsPerCounterpartyDaily]
			(
			[Counterparty],
			[NopAbs],
			[VolumeBaseAbsInUsd],
			[VolumeShare],
			[CommissionsPbUsd],
			[CommissionsCptUsd],
			[CommissionsSumUsd],
			[DealsNum],
			[LastDealUtc],
			[AvgDealSizeUsd],
			[KGTRejectionsNum],
			[LastKgtRejectionUtc],
			[CtpRejectionsNum],
			[LastCptRejectionUtc],
			[KGTRejectionsRate],
			[CtpRejectionsRate],
			[AvgIntegratorLatencyNanoseconds],
			[AvgCounterpartyLatencyNanoseconds]
			)
		SELECT
			[Counterparty],
			[NopAbs],
			[VolumeBaseAbsInUsd],
			[VolumeShare],
			[CommissionsPbUsd],
			[CommissionsCptUsd],
			[CommissionsSumUsd],
			[DealsNum],
			[LastDealUtc],
			[AvgDealSizeUsd],
			[KGTRejectionsNum],
			[LastKgtRejectionUtc],
			[CtpRejectionsNum],
			[LastCptRejectionUtc],
			[KGTRejectionsRate],
			[CtpRejectionsRate],
			[AvgIntegratorLatencyNanoseconds],
			[AvgCounterpartyLatencyNanoseconds]
		FROM
			#TempCounterpartyStatistics

	COMMIT
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	DECLARE @TempTradingGroupsStatistics TABLE
	(
		[Rank] [int] NOT NULL,
		[TradingSystemGroupName] [NVARCHAR](32) NOT NULL,
		[TradingSystemGroupId] [int]  NOT NULL,
		[TradingSystemTypeName] [nvarchar](12) NULL,
		[Counterparty] [char](3) NULL,
		[TradingSystemTypeId] [tinyint] NULL,
		[ConfiguredSystemsCount] int NULL,
		[EnabledSystemsCount] int NULL,
		[HardBlockedSystemsCount] int NULL,
		[VolumeUsdM] [decimal](18, 6) NULL,
		[DealsNum] [int] NULL,
		[LastDealUtc] [datetime2](7) NULL,
		[KGTRejectionsNum] [int] NULL,
		[LastKgtRejectionUtc] [datetime2](7) NULL,
		[CtpRejectionsNum] [int] NULL,
		[LastCptRejectionUtc] [datetime2](7) NULL,
		[PnlGrossUsdPerMUsd] [decimal](18, 6) NULL,
		[CommissionsUsdPerMUsd] [decimal](18, 6) NULL,
		[PnlNetUsdPerMUsd] [decimal](18, 6) NULL,
		[PnlGrossUsd] [decimal](18, 6) NULL,
		[CommissionsUsd] [decimal](18, 6) NULL,	
		[PnlNetUsd] [decimal](18, 6) NULL,
		[IsOrderTransmissionDisabled] [bit] NULL,
		[IsGoFlatOnlyOn] [bit] NULL,
		[PageId] [int] NULL,
		[BeginSymbol] [nchar](7) NULL,
		[EndSymbol] [nchar](7) NULL,
		[StripsCount] INT
	)

	INSERT INTO 
		@TempTradingGroupsStatistics
		([Rank]
		,[TradingSystemGroupName]
		,[TradingSystemGroupId]
		,[TradingSystemTypeName]
		,[Counterparty]
		,[TradingSystemTypeId]
		,[ConfiguredSystemsCount]
		,[EnabledSystemsCount]
		,[HardBlockedSystemsCount]
		,[VolumeUsdM]
		,[DealsNum]
		,[LastDealUtc]
		,[KGTRejectionsNum]
		,[LastKgtRejectionUtc]
		,[CtpRejectionsNum]
		,[LastCptRejectionUtc]
		,[PnlGrossUsdPerMUsd]
		,[CommissionsUsdPerMUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsd]
		,[PnlNetUsd]
		,[IsOrderTransmissionDisabled]
		,[IsGoFlatOnlyOn]
		,[PageId]
		,[BeginSymbol]
		,[EndSymbol]
		,[StripsCount])
    exec [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingGroupsDaily]
			([Rank]
			,[TradingSystemGroupName]
			,[TradingSystemGroupId]
			,[TradingSystemTypeName]
			,[Counterparty]
			,[TradingSystemTypeId]
			,[ConfiguredSystemsCount]
			,[EnabledSystemsCount]
			,[HardBlockedSystemsCount]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[CommissionsUsdPerMUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsd]
			,[PnlNetUsd]
			,[IsOrderTransmissionDisabled]
			,[IsGoFlatOnlyOn]
			,[PageId]
			,[BeginSymbol]
			,[EndSymbol]
			,[StripsCount])
		SELECT
			[Rank]
			,[TradingSystemGroupName]
			,[TradingSystemGroupId]
			,[TradingSystemTypeName]
			,[Counterparty]
			,[TradingSystemTypeId]
			,[ConfiguredSystemsCount]
			,[EnabledSystemsCount]
			,[HardBlockedSystemsCount]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[CommissionsUsdPerMUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsd]
			,[PnlNetUsd]
			,[IsOrderTransmissionDisabled]
			,[IsGoFlatOnlyOn]
			,[PageId]
			,[BeginSymbol]
			,[EndSymbol]
			,[StripsCount]
		FROM
			@TempTradingGroupsStatistics
	COMMIT
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	DECLARE @TempTradingSystemStatistics TABLE
	(
		[SymbolId] [tinyint] NOT NULL,
		[TradingSystemId] [int] NOT NULL,
		[NopBasePol] [decimal](18,2) NOT NULL,
		[VolumeUsdM] [decimal](18,6) NOT NULL,
		[DealsNum] [int]  NOT NULL,
		[LastDealUtc] [datetime2](7) NULL,
		[KGTRejectionsNum] [int]  NOT NULL,
		[LastKgtRejectionUtc] [datetime2](7) NULL,
		[CtpRejectionsNum] [int]  NOT NULL,
		[LastCptRejectionUtc] [datetime2](7) NULL,
		[PnlGrossUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlGrossUsd] [decimal](18,6) NOT NULL,
		[CommissionsPbUsdPerMUsd] [decimal](18,2) NOT NULL,
		[CommissionsPbUsd] [decimal](18,2) NOT NULL,
		[CommissionsCptUsdPerMUsd] [decimal](18,2) NOT NULL,
		[CommissionsCptUsd] [decimal](18,2) NOT NULL,
		[CommissionsUsdPerMUsd] [decimal](18,2) NOT NULL,
		[CommissionsUsd] [decimal](18,2) NOT NULL,
		[PnlNetUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsd] [decimal](18,6) NOT NULL
	)

	INSERT INTO 
		@TempTradingSystemStatistics
		([SymbolId]
		,[TradingSystemId]
		,[NopBasePol]
		,[VolumeUsdM]
		,[DealsNum]
		,[LastDealUtc]
		,[KGTRejectionsNum]
		,[LastKgtRejectionUtc]
		,[CtpRejectionsNum]
		,[LastCptRejectionUtc]
		,[PnlGrossUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsPbUsdPerMUsd]
		,[CommissionsPbUsd]
		,[CommissionsCptUsdPerMUsd]
		,[CommissionsCptUsd]
		,[CommissionsUsdPerMUsd]
		,[CommissionsUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlNetUsd])
    exec [dbo].[GetTradingSystemsDailyStats_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingSystemDaily]
			([TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsPbUsdPerMUsd]
			,[CommissionsPbUsd]
			,[CommissionsCptUsdPerMUsd]
			,[CommissionsCptUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd])
		SELECT
			[TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsPbUsdPerMUsd]
			,[CommissionsPbUsd]
			,[CommissionsCptUsdPerMUsd]
			,[CommissionsCptUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd]
		FROM
			@TempTradingSystemStatistics
	COMMIT
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP]
AS
BEGIN
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP]
	exec [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]
AS
BEGIN
	UPDATE 
		[dbo].[TableChangeLog] 
	SET 
		[LastUpdatedUtc] = GETUTCDATE() 
	WHERE 
		[TableName] = 'PersistedStatisticPerTradingSystemDaily' 
		AND [LastUpdatedUtc] < DATEADD(SECOND, -2, GETUTCDATE())

	IF @@ROWCOUNT > 0
	BEGIN
		exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP]
	END
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdatePriceDelayStats_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UpdatePriceDelayStats_SP] 
(
	@CsvStatsArg VARCHAR(MAX)
)
AS
BEGIN

	UPDATE
		stats
	SET
		stats.[TotalReceivedPricesInLastMinute] = statsArg.num1,
		stats.[DelayedPricesInLastMinute] = statsArg.num2,
		stats.[TotalReceivedPricesInLastHour] = statsArg.num3,
		stats.[DelayedPricesInLastHour] = statsArg.num4
	FROM
		[dbo].[csvlist_to_codesandintquadruple_tbl](@CsvStatsArg) statsArg
		INNER JOIN [dbo].[Counterparty] ctp on ctp.[CounterpartyCode] = statsArg.Code
		INNER JOIN [dbo].[PriceDelayStats] stats on ctp.[CounterpartyId] = stats.[CounterpartyId] 
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdatePriceDelayStats_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdatePriceDelayStats_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateSettingsXml_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[UpdateSettingsXml_SP] (
	@SettingName NVARCHAR(100),
	@SettingsXml Xml
	)
AS
BEGIN
	UPDATE 
	[dbo].[Settings] SET 
		[SettingsXml] = @SettingsXml, 
		[LastUpdated] = GETUTCDATE()
	WHERE
		[Name] = @SettingName
END


GO
ALTER AUTHORIZATION ON [dbo].[UpdateSettingsXml_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateSettingsXml_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateSymbolPrices_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdateSymbolPrices_SP] (
	@AskPricesList VARCHAR(MAX),
	@BidPricesList VARCHAR(MAX)
	)
AS
BEGIN

	DECLARE @now DATETIME2 = GETUTCDATE()
	
	UPDATE
		sym
	SET
		sym.LastKnownAskMeanPrice = prices.price,
		sym.AskMeanPriceUpdated = @now
	FROM
		[dbo].[csvlist_to_codesandprices_tbl](@AskPricesList) prices
		INNER JOIN Symbol sym on sym.ShortName = prices.Code

	UPDATE
		sym
	SET
		sym.LastKnownBidMeanPrice = prices.price,
		sym.BidMeanPriceUpdated = @now
	FROM
		[dbo].[csvlist_to_codesandprices_tbl](@BidPricesList) prices
		INNER JOIN Symbol sym on sym.ShortName = prices.Code
		
	
	DECLARE @USDId SMALLINT
	SELECT @USDId = [CurrencyID] FROM [dbo].[Currency_Internal] WITH (NOLOCK) WHERE [CurrencyAlphaCode] = 'USD'
	
		
	UPDATE
		dirrectUsdConversion
	SET
		dirrectUsdConversion.LastKnownUsdConversionMultiplier = sym.[LastKnownMeanReferencePrice],
		dirrectUsdConversion.UsdConversionMultiplierUpdated = sym.MeanReferencePriceUpdated
	FROM
		[dbo].[Currency_Rates] dirrectUsdConversion
		INNER JOIN [dbo].[Symbol] sym ON sym.[QuoteCurrencyId] = @USDId AND sym.[BaseCurrencyId] = dirrectUsdConversion.[CurrencyID]
		
	UPDATE
		invertedUsdConversion
	SET
		invertedUsdConversion.LastKnownUsdConversionMultiplier = 1/sym.[LastKnownMeanReferencePrice],
		invertedUsdConversion.UsdConversionMultiplierUpdated = sym.MeanReferencePriceUpdated
	FROM
		[dbo].[Currency_Rates] invertedUsdConversion
		INNER JOIN [dbo].[Symbol] sym ON sym.[BaseCurrencyId] = @USDId AND sym.[QuoteCurrencyId] = invertedUsdConversion.[CurrencyID]
		
		
	exec [dbo].[UpdateEodUsdRates_SP]
	exec [dbo].[UpdateEodRates_SP]
	

END

GO
ALTER AUTHORIZATION ON [dbo].[UpdateSymbolPrices_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateSymbolPrices_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateTradingSystemMonitoringPages_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdateTradingSystemMonitoringPages_SP]
(
	@TradingSystemMonitoringPageId INT,
	@BeginSymbol NCHAR(7),
	@EndSymbol NCHAR(7),
	@StripsCount INT
)
AS
BEGIN

	DECLARE @BeginSymbolId INT
	DECLARE @EndSymbolId INT

	SELECT @BeginSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @BeginSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @BeginSymbol) WITH SETERROR
	END
	
	SELECT @EndSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @EndSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @EndSymbol) WITH SETERROR
	END

	UPDATE
		[dbo].[TradingSystemMonitoringPage]
	SET
		BeginSymbolId = @BeginSymbolId,
		EndSymbolId = @EndSymbolId,
		StripsCount = @StripsCount
	WHERE
		TradingSystemMonitoringPageId = @TradingSystemMonitoringPageId
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdateTradingSystemMonitoringPages_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateTradingSystemMonitoringPages_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueCrossSettingsInsertSingle_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueCrossSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenSingleCounterpartySignals_seconds [decimal](18,9),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@MinimumFillSize [decimal](18,0),
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Cross'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Cross TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueCrossSettings]
           ([SymbolId]
           ,[MinimumSizeToTrade]
           ,[MaximumSizeToTrade]
           ,[MinimumDiscountBasisPointsToTrade]
           ,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
		   ,[MinimumFillSize]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MinimumSizeToTrade
           ,@MaximumSizeToTrade
           ,@MinimumDiscountBasisPointsToTrade
           ,@MinimumTimeBetweenSingleCounterpartySignals_seconds
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
		   ,@MinimumFillSize
           ,@CounterpartyId
           ,@TradingSystemId)
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueCrossSettingsInsertSingle_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueCrossSettingsInsertSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueCrossSystemSettingsUpdateSingle_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueCrossSystemSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenSingleCounterpartySignals_seconds [decimal](18,9),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@MinimumFillSize [decimal](18,0)
)
AS
BEGIN
	UPDATE sett
	SET
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[MinimumDiscountBasisPointsToTrade] = @MinimumDiscountBasisPointsToTrade,
		[MinimumTimeBetweenSingleCounterpartySignals_seconds] = @MinimumTimeBetweenSingleCounterpartySignals_seconds,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds,
		[MinimumFillSize] = @MinimumFillSize
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueCrossSystemSettingsUpdateSingle_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueCrossSystemSettingsUpdateSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueGliderSettingsInsertSingle_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueGliderSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@BidEnabled [bit],
	@AskEnabled [bit],
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@MinimumBPGrossGain  [decimal](18,6),
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumIntegratorGlidePriceLife_milliseconds [int],
	@GlidingCutoffSpan_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel  [decimal](18,6),
	@ReferenceLmaxBookOnly [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Glider'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Stream TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	
	BEGIN TRANSACTION
	BEGIN TRY
		
		INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

		SELECT @TradingSystemId = scope_identity();
		
		--make sure that default switches values are correct
		INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
		SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
		FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
		WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
		
		INSERT INTO [dbo].[VenueGliderSettings]
			   ([SymbolId]
			   ,[CounterpartyId]
			   ,[TradingSystemId]
			   ,[MaximumSizeToTrade]
			   ,[BestPriceImprovementOffsetBasisPoints]
			   ,[MaximumDiscountBasisPointsToTrade]
			   ,[KGTLLTime_milliseconds]
			   ,[MinimumBPGrossGain]
			   ,[PreferLmaxDuringHedging]
			   ,[MinimumIntegratorPriceLife_milliseconds]
			   ,[MinimumIntegratorGlidePriceLife_milliseconds]
			   ,[GlidingCutoffSpan_milliseconds]
			   ,[MinimumSizeToTrade]
			   ,[MinimumFillSize]
			   ,[MaximumSourceBookTiers]
			   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
			   ,[ReferenceLmaxBookOnly]
			   ,[BidEnabled]
			   ,[AskEnabled])
		 VALUES
			   (@SymbolId
			   ,@CounterpartyId
			   ,@TradingSystemId
			   ,@MaximumSizeToTrade 
			   ,@BestPriceImprovementOffsetBasisPoints
			   ,@MaximumDiscountBasisPointsToTrade
			   ,@KGTLLTime_milliseconds
			   ,@MinimumBPGrossGain
			   ,0
			   ,@MinimumIntegratorPriceLife_milliseconds
			   ,@MinimumIntegratorGlidePriceLife_milliseconds
			   ,@GlidingCutoffSpan_milliseconds
			   ,@MinimumSizeToTrade
			   ,@MinimumFillSize
			   ,@MaximumSourceBookTiers
			   ,@MinimumBasisPointsFromKgtPriceToFastCancel
			   ,@ReferenceLmaxBookOnly
			   ,@BidEnabled
			   ,@AskEnabled)
		
	
		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueGliderSettingsInsertSingle_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueGliderSettingsInsertSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueGliderSettingsUpdateSingle_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueGliderSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@MinimumBPGrossGain  [decimal](18,6),
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumIntegratorGlidePriceLife_milliseconds [int],
	@GlidingCutoffSpan_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel [decimal](18,6),
	@ReferenceLmaxBookOnly [bit],
	@BidEnabled [bit],
	@AskEnabled [bit]
)
AS
BEGIN
	UPDATE sett
	SET
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[KGTLLTime_milliseconds] = @KGTLLTime_milliseconds,
		[MinimumBPGrossGain] = @MinimumBPGrossGain,
		[PreferLmaxDuringHedging] = 0,
		[MinimumIntegratorPriceLife_milliseconds] = @MinimumIntegratorPriceLife_milliseconds,
		[MinimumIntegratorGlidePriceLife_milliseconds] = @MinimumIntegratorGlidePriceLife_milliseconds,
		[GlidingCutoffSpan_milliseconds] = @GlidingCutoffSpan_milliseconds,
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MinimumFillSize] = @MinimumFillSize,
		[MaximumSourceBookTiers] = @MaximumSourceBookTiers,
		[MinimumBasisPointsFromKgtPriceToFastCancel] = @MinimumBasisPointsFromKgtPriceToFastCancel,
		[ReferenceLmaxBookOnly] = @ReferenceLmaxBookOnly,
		[BidEnabled] = @BidEnabled,
		[AskEnabled] = @AskEnabled
	FROM
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueGliderSettingsUpdateSingle_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueGliderSettingsUpdateSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueMMSettingsInsertSingle_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueMMSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,	
	@MinimumBasisPointsFromKgtPriceToFastCancel  [decimal](18,6),
	@BidEnabled [bit],
	@AskEnabled [bit],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'MM'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'MM TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueMMSettings]
           ([SymbolId]
           ,[MaximumSizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
		   ,[MinimumIntegratorPriceLife_milliseconds]
		   ,[MinimumSizeToTrade]
		   ,[MinimumFillSize]
		   ,[MaximumSourceBookTiers]
		   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
		   ,[BidEnabled]
		   ,[AskEnabled]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MaximumSizeToTrade 
           ,@BestPriceImprovementOffsetBasisPoints
           ,@MaximumDiscountBasisPointsToTrade
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
		   ,@MinimumIntegratorPriceLife_milliseconds
		   ,@MinimumSizeToTrade
		   ,@MinimumFillSize
		   ,@MaximumSourceBookTiers
		   ,@MinimumBasisPointsFromKgtPriceToFastCancel
		   ,@BidEnabled
		   ,@AskEnabled
           ,@CounterpartyId
           ,@TradingSystemId)
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueMMSettingsInsertSingle_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueMMSettingsInsertSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueMMSettingsUpdateSingle_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueMMSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel [decimal](18,6),
	@BidEnabled [bit],
	@AskEnabled [bit]
)
AS
BEGIN
	UPDATE sett
	SET
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds,
		[MinimumIntegratorPriceLife_milliseconds] = @MinimumIntegratorPriceLife_milliseconds,
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MinimumFillSize] = @MinimumFillSize,
		[MaximumSourceBookTiers] = @MaximumSourceBookTiers,
		[MinimumBasisPointsFromKgtPriceToFastCancel] = @MinimumBasisPointsFromKgtPriceToFastCancel,
		[BidEnabled] = @BidEnabled,
		[AskEnabled] = @AskEnabled
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueMMSettingsUpdateSingle_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueMMSettingsUpdateSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueQuotingSettingsInsertSingle_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueQuotingSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MaximumPositionBaseAbs [decimal](18,0),
	@FairPriceImprovementSpreadBasisPoints [decimal](18,6),
	@StopLossNetInUsd [bigint],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Quoting'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Quoting TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueQuotingSettings]
           ([SymbolId]
           ,[MaximumPositionBaseAbs]
           ,[FairPriceImprovementSpreadBasisPoints]
           ,[StopLossNetInUsd]   
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MaximumPositionBaseAbs
           ,@FairPriceImprovementSpreadBasisPoints
           ,@StopLossNetInUsd
           ,@CounterpartyId
           ,@TradingSystemId)
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueQuotingSettingsInsertSingle_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueQuotingSettingsInsertSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueQuotingSystemSettingsUpdateSingle_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueQuotingSystemSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumPositionBaseAbs [decimal](18,0),
	@FairPriceImprovementSpreadBasisPoints [decimal](18,6),
	@StopLossNetInUsd [bigint]
)
AS
BEGIN
	UPDATE sett
	SET
		[MaximumPositionBaseAbs] = @MaximumPositionBaseAbs,
		[FairPriceImprovementSpreadBasisPoints] = @FairPriceImprovementSpreadBasisPoints,
		[StopLossNetInUsd] = @StopLossNetInUsd
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueQuotingSystemSettingsUpdateSingle_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueQuotingSystemSettingsUpdateSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueStreamSettingsInsertSingle_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueStreamSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@BidEnabled [bit],
	@AskEnabled [bit],
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel  [decimal](18,6),
	@ReferenceLmaxBookOnly [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Stream'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Stream TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	
	BEGIN TRANSACTION
	BEGIN TRY
		
		INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

		SELECT @TradingSystemId = scope_identity();
		
		--make sure that default switches values are correct
		INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
		SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
		FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
		WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
		
		INSERT INTO [dbo].[VenueStreamSettings]
			   ([SymbolId]
			   ,[CounterpartyId]
			   ,[TradingSystemId]
			   ,[MaximumSizeToTrade]
			   ,[BestPriceImprovementOffsetBasisPoints]
			   ,[MaximumDiscountBasisPointsToTrade]
			   ,[KGTLLTime_milliseconds]
			   ,[PreferLLDestinationCheckToLmax]
			   ,[MinimumBPGrossToAccept]
			   ,[PreferLmaxDuringHedging]
			   ,[MinimumIntegratorPriceLife_milliseconds]
			   ,[MinimumSizeToTrade]
			   ,[MinimumFillSize]
			   ,[MaximumSourceBookTiers]
			   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
			   ,[ReferenceLmaxBookOnly]
			   ,[BidEnabled]
			   ,[AskEnabled])
		 VALUES
			   (@SymbolId
			   ,@CounterpartyId
			   ,@TradingSystemId
			   ,@MaximumSizeToTrade 
			   ,@BestPriceImprovementOffsetBasisPoints
			   ,@MaximumDiscountBasisPointsToTrade
			   ,@KGTLLTime_milliseconds
			   ,@PreferLLDestinationCheckToLmax
			   ,@MinimumBPGrossToAccept
			   ,@PreferLmaxDuringHedging
			   ,@MinimumIntegratorPriceLife_milliseconds
			   ,@MinimumSizeToTrade
			   ,@MinimumFillSize
			   ,@MaximumSourceBookTiers
			   ,@MinimumBasisPointsFromKgtPriceToFastCancel
			   ,@ReferenceLmaxBookOnly
			   ,@BidEnabled
			   ,@AskEnabled)
		
	
		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueStreamSettingsInsertSingle_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueStreamSettingsInsertSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueStreamSettingsUpdateSingle_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueStreamSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel [decimal](18,6),
	@ReferenceLmaxBookOnly [bit],
	@BidEnabled [bit],
	@AskEnabled [bit]
)
AS
BEGIN
	UPDATE sett
	SET
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[KGTLLTime_milliseconds] = @KGTLLTime_milliseconds,
		[PreferLLDestinationCheckToLmax] = @PreferLLDestinationCheckToLmax,
		[MinimumBPGrossToAccept] = @MinimumBPGrossToAccept,
		[PreferLmaxDuringHedging] = @PreferLmaxDuringHedging,
		[MinimumIntegratorPriceLife_milliseconds] = @MinimumIntegratorPriceLife_milliseconds,
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MinimumFillSize] = @MinimumFillSize,
		[MaximumSourceBookTiers] = @MaximumSourceBookTiers,
		[MinimumBasisPointsFromKgtPriceToFastCancel] = @MinimumBasisPointsFromKgtPriceToFastCancel,
		[ReferenceLmaxBookOnly] = @ReferenceLmaxBookOnly,
		[BidEnabled] = @BidEnabled,
		[AskEnabled] = @AskEnabled
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueStreamSettingsUpdateSingle_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueStreamSettingsUpdateSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueStreamSystemsCheckAllowedCount_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
AS
BEGIN

	DECLARE @CntOfAllowedEnabledSystemsPerSymbol INT = 3

	DECLARE @ErrorMsg VARCHAR(MAX) = ''
	DECLARE @IsError BIT = 0
	DECLARE @Tab CHAR = CHAR(9)
	DECLARE @CR CHAR = CHAR(13)
	DECLARE @LF CHAR = CHAR(10)

	;WITH systemCounts AS
	(
		SELECT 
			sym.[Name] AS Symbol, 
			cpt.[CounterpartyCode] AS Counterparty,
			streamSys.[TradingSystemId], 
			COUNT(sym.[Name]) OVER (PARTITION BY sym.[Name], cpt.[CounterpartyCode]) AS systemsForSymbolCnt
		FROM 
			(
			SELECT [TradingSystemId], [SymbolId], [CounterpartyId] FROM [dbo].[VenueStreamSettings]
			UNION ALL
			SELECT [TradingSystemId], [SymbolId], [CounterpartyId] FROM [dbo].[VenueGliderSettings]
			) streamSys
			INNER JOIN [dbo].[TradingSystemActions] tsa ON streamSys.[TradingSystemId] = tsa.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] sym on streamSys.[SymbolId] = sym.[Id]
			INNER JOIN [dbo].Counterparty cpt on streamSys.[CounterpartyId] = cpt.[CounterpartyId]
		WHERE 
			tsa.[Enabled] = 1
			--AND tsa.[GoFlatOnly] = 0
			--AND tsa.[OrderTransmissionDisabled] = 0
	)
	SELECT
		@IsError = 1,
		@ErrorMsg = @ErrorMsg + @CR + @LF + 'Symbol: ' + Symbol + ', Destination: ' + Counterparty + ', Group Name: ' + grp.[TradingSystemGroupName] + ',' + @Tab + ' SystemId: ' + CAST(systemCounts.TradingSystemId AS VARCHAR(10)) + ',' + @Tab + ' Total systems per this symbol: ' + CAST(systemsForSymbolCnt AS VARCHAR(4)) + ',' + @Tab + ' Total systems per this symbol allowed: ' + CAST(@CntOfAllowedEnabledSystemsPerSymbol AS VARCHAR(4))
	FROM
		systemCounts
		INNER JOIN [dbo].[TradingSystem] sys ON systemCounts.[TradingSystemId] = sys.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystemGroup] grp ON sys.[TradingSystemGroupId] = grp.[TradingSystemGroupId]
	WHERE
		systemsForSymbolCnt > @CntOfAllowedEnabledSystemsPerSymbol
	ORDER BY
		systemsForSymbolCnt DESC,
		grp.[TradingSystemGroupName] ASC,
		Counterparty ASC,
		sys.[TradingSystemId] ASC

	IF @IsError = 1
		RAISERROR ('Error - Action reverted - There would be too many enabled stream systmes per some symbols:%s', 16, 1, @ErrorMsg)

END

GO
ALTER AUTHORIZATION ON [dbo].[VenueStreamSystemsCheckAllowedCount_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[VenueSystemDeleteMulti_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueSystemDeleteMulti_SP]
(
	@TradingSystemId IdsTable READONLY
)
AS
BEGIN

	BEGIN TRANSACTION;
	
	DECLARE @DeletedCnt INT = 0;
	
	DELETE sett
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
		

	DELETE FROM
		[dbo].[TradingSystemActions]
	WHERE 
		[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
		
	DELETE FROM 
		[dbo].[TradingSystem]
	WHERE 
		[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	COMMIT TRANSACTION;
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueSystemDeleteMulti_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueSystemDeleteMulti_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueSystemDeleteOne_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueSystemDeleteOne_SP]
(
	@TradingSystemId int
)
AS
BEGIN

	BEGIN TRANSACTION;
	
	DECLARE @DeletedCnt INT = 0;
	
	DELETE sett
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
		
	DELETE sett
	FROM
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	IF @DeletedCnt > 0
	BEGIN
		DELETE FROM
			[dbo].[TradingSystemActions]
		WHERE 
			[TradingSystemId] = @TradingSystemId
			
		DELETE FROM 
			[dbo].[TradingSystem]
		WHERE 
			[TradingSystemId] = @TradingSystemId
	END
	
	
	COMMIT TRANSACTION;
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueSystemDeleteOne_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueSystemDeleteOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueSystemDisableEnableOne_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueSystemDisableEnableOne_SP]
(
	@TradingSystemId int,
	@Enable Bit
)
AS
BEGIN

	BEGIN TRANSACTION
	BEGIN TRY
	
		UPDATE
			[dbo].[TradingSystemActions]
		SET
			[Enabled] = @Enable
		WHERE
			[TradingSystemId] = @TradingSystemId
			AND [HardBlocked] = 0
	
		IF @Enable = 1
			exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueSystemDisableEnableOne_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueSystemDisableEnableOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueSystemHardblock_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueSystemHardblock_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	UPDATE
		[dbo].[TradingSystemActions]
	SET
		[Enabled] = 0,
		[HardBlocked] = 1
	WHERE
		[TradingSystemId] = @TradingSystemId
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueSystemHardblock_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueSystemHardblock_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[VenueSystemReset_SP]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VenueSystemReset_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	UPDATE
		[dbo].[TradingSystemActions]
	SET
		[LastResetRequestedUtc] = GETUTCDATE(),
		[HardBlocked] = 0
	WHERE
		[TradingSystemId] = @TradingSystemId
		AND [Enabled] = 0
END

GO
ALTER AUTHORIZATION ON [dbo].[VenueSystemReset_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[VenueSystemReset_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  UserDefinedFunction [dbo].[ConvertBpToDecimal]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ConvertBpToDecimal](@symbolId tinyint, @bpValue [decimal] (18,4))
RETURNS [decimal] (18,9)
AS 
BEGIN
	DECLARE @decimalConvert [decimal] (18,9)

	SELECT @decimalConvert = @bpValue * symbol.LastKnownMeanReferencePrice / 10000.0
	FROM [dbo].[Symbol_Rates] symbol 
	WHERE symbol.Id = @symbolId

	RETURN @decimalConvert
END

GO
ALTER AUTHORIZATION ON [dbo].[ConvertBpToDecimal] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[ConvertDateTime2ToTimeOfDayMicroseconds]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ConvertDateTime2ToTimeOfDayMicroseconds] 
(
	@DateTime2Input DATETIME2
)
RETURNS decimal(38,1)
AS
BEGIN
-- converts datetime2 (time of day only) to microseconds

RETURN
(CONVERT(bigint, DATEPART(HOUR, @DateTime2Input))*36000000000
+ CONVERT(bigint, DATEPART(MINUTE, @DateTime2Input))*600000000
+ CONVERT(bigint, DATEPART(SECOND, @DateTime2Input))*10000000
+ CONVERT(bigint, DATEPART(NANOSECOND, @DateTime2Input))/100)/10

END
GO
ALTER AUTHORIZATION ON [dbo].[ConvertDateTime2ToTimeOfDayMicroseconds] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[ConvertDateTime2ToTimeOfDayTicks]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ConvertDateTime2ToTimeOfDayTicks] 
(
	@DateTime2Input DATETIME2
)
RETURNS bigint
AS
BEGIN
-- converts datetime2 (time of day only) to ticks (units of 100ns)

RETURN
CONVERT(bigint, DATEPART(HOUR, @DateTime2Input))*36000000000
+ CONVERT(bigint, DATEPART(MINUTE, @DateTime2Input))*600000000
+ CONVERT(bigint, DATEPART(SECOND, @DateTime2Input))*10000000
+ CONVERT(bigint, DATEPART(NANOSECOND, @DateTime2Input))/100

END
GO
ALTER AUTHORIZATION ON [dbo].[ConvertDateTime2ToTimeOfDayTicks] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[csvlist_to_codes_tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[csvlist_to_codes_tbl] (@list nvarchar(MAX))
   RETURNS @tbl TABLE (code varchar(50) NOT NULL) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1
      INSERT @tbl (code)
         VALUES (CONVERT(varchar(50), substring(@list, @pos + 1, @valuelen)))
      SELECT @pos = @nextpos
   END
   RETURN
END

GO
ALTER AUTHORIZATION ON [dbo].[csvlist_to_codes_tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[csvlist_to_codesanddates_tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[csvlist_to_codesanddates_tbl] (@list varchar(MAX))
   RETURNS @tbl TABLE (code varchar(6) NOT NULL, dateValue date) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex('|', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1

	  IF @nextpos > @pos + 1
	  BEGIN

		  DECLARE @Code Varchar(6) = substring(@list, @pos + 1, @valuelen)
		  SELECT @pos = @nextpos

		  SELECT @nextpos = charindex('|', @list, @pos + 1)
		  SELECT @valuelen = CASE WHEN @nextpos > 0
								  THEN @nextpos
								  ELSE len(@list) + 1
							 END - @pos - 1

		  DECLARE @DateValue DATE = CONVERT(DATE, substring(@list, @pos + 1, @valuelen))
		  SELECT @pos = @nextpos

		  INSERT @tbl (code, dateValue)
			 VALUES (@Code, @DateValue)
	  END
	END
   RETURN
END

GO
ALTER AUTHORIZATION ON [dbo].[csvlist_to_codesanddates_tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[csvlist_to_codesandintquadruple_tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[csvlist_to_codesandintquadruple_tbl] (@list varchar(MAX))
   RETURNS @tbl TABLE (code varchar(3) NOT NULL, num1 int NOT NULL, num2 int NOT NULL, num3 int NOT NULL, num4 int NOT NULL) AS
BEGIN
	-- Sample usage: [dbo].[csvlist_to_codesandintquadruple_tbl]('ARG:45,65,6565,45,BGZ:1,22,333,4')

	IF(@list IS NULL OR len(@list) = 0)
	BEGIN
		RETURN
	END
	
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1, @valuelen = 0

   WHILE @nextpos > 0 AND @nextpos <> len(@list)
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1

	  IF @nextpos > @pos + 1
	  BEGIN

	      DECLARE @code Varchar(3) = substring(@list, @pos + 1, 3)
		  SELECT @pos = @pos+4, @valuelen = @valuelen-4
	  
		  DECLARE @num1 int = CONVERT(INT, substring(@list, @pos + 1, @valuelen))
		  SELECT @pos = @nextpos

		  SELECT @nextpos = charindex(',', @list, @pos + 1)
		  SELECT @valuelen = CASE WHEN @nextpos > 0
								  THEN @nextpos
								  ELSE len(@list) + 1
							 END - @pos - 1

		  DECLARE @num2 int = CONVERT(int, substring(@list, @pos + 1, @valuelen))
		  SELECT @pos = @nextpos
		  
		  SELECT @nextpos = charindex(',', @list, @pos + 1)
		  SELECT @valuelen = CASE WHEN @nextpos > 0
								  THEN @nextpos
								  ELSE len(@list) + 1
							 END - @pos - 1

		  DECLARE @num3 int = CONVERT(int, substring(@list, @pos + 1, @valuelen))
		  SELECT @pos = @nextpos
		  
		  SELECT @nextpos = charindex(',', @list, @pos + 1)
		  SELECT @valuelen = CASE WHEN @nextpos > 0
								  THEN @nextpos
								  ELSE len(@list) + 1
							 END - @pos - 1

		  DECLARE @num4 int = CONVERT(int, substring(@list, @pos + 1, @valuelen))

		  INSERT @tbl (code,num1, num2, num3, num4)
			 VALUES (@code, @num1, @num2, @num3, @num4)
	  END
	  ELSE IF @nextpos <= 0
	  BEGIN
		select @num2 = cast('Invalid csv int pairs list ' + @list as int)
	  END
	  SELECT @pos = @nextpos
	END
   RETURN
END

GO
ALTER AUTHORIZATION ON [dbo].[csvlist_to_codesandintquadruple_tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[csvlist_to_codesandprices_tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[csvlist_to_codesandprices_tbl] (@list varchar(MAX))
   RETURNS @tbl TABLE (code varchar(6) NOT NULL, price decimal(18,9)) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex('|', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1

	  IF @nextpos > @pos + 1
	  BEGIN

		  DECLARE @Code Varchar(6) = substring(@list, @pos + 1, @valuelen)
		  SELECT @pos = @nextpos

		  SELECT @nextpos = charindex('|', @list, @pos + 1)
		  SELECT @valuelen = CASE WHEN @nextpos > 0
								  THEN @nextpos
								  ELSE len(@list) + 1
							 END - @pos - 1

		  DECLARE @Price DECIMAL(18,9) = CONVERT(DECIMAL(18,9), substring(@list, @pos + 1, @valuelen))
		  SELECT @pos = @nextpos

		  INSERT @tbl (code, price)
			 VALUES (@Code, @Price)
	  END
	END
   RETURN
END

GO
ALTER AUTHORIZATION ON [dbo].[csvlist_to_codesandprices_tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[csvlist_to_intpairs_tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[csvlist_to_intpairs_tbl] (@list varchar(MAX))
   RETURNS @tbl TABLE (num1 int NOT NULL, num2 int) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1, @valuelen = 0

   WHILE @nextpos > 0 AND @nextpos <> len(@list)
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1

	  IF @nextpos > @pos + 1
	  BEGIN

		  DECLARE @num1 int = CONVERT(INT, substring(@list, @pos + 1, @valuelen))
		  SELECT @pos = @nextpos

		  SELECT @nextpos = charindex(',', @list, @pos + 1)
		  SELECT @valuelen = CASE WHEN @nextpos > 0
								  THEN @nextpos
								  ELSE len(@list) + 1
							 END - @pos - 1

		  DECLARE @num2 int = CONVERT(int, substring(@list, @pos + 1, @valuelen))

		  INSERT @tbl (num1, num2)
			 VALUES (@num1, @num2)
	  END
	  ELSE IF @nextpos <= 0
	  BEGIN
		select @num2 = cast('Invalid csv int pairs list ' + @list as int)
	  END
	  SELECT @pos = @nextpos
	END
   RETURN
END

GO
ALTER AUTHORIZATION ON [dbo].[csvlist_to_intpairs_tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[csvlist_to_ints_tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[csvlist_to_ints_tbl] (@list varchar(MAX))
   RETURNS @tbl TABLE (num int NOT NULL) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1
      INSERT @tbl (num)
         VALUES (CONVERT(int, substring(@list, @pos + 1, @valuelen)))
      SELECT @pos = @nextpos
   END
   RETURN
END

GO
ALTER AUTHORIZATION ON [dbo].[csvlist_to_ints_tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[GetBinaryIPv4FromString]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetBinaryIPv4FromString](@ip AS VARCHAR(15)) RETURNS BINARY(4)
AS
BEGIN
    DECLARE @bin AS BINARY(4)

    SELECT @bin = CAST( CAST( PARSENAME( @ip, 4 ) AS INTEGER) AS BINARY(1))
                + CAST( CAST( PARSENAME( @ip, 3 ) AS INTEGER) AS BINARY(1))
                + CAST( CAST( PARSENAME( @ip, 2 ) AS INTEGER) AS BINARY(1))
                + CAST( CAST( PARSENAME( @ip, 1 ) AS INTEGER) AS BINARY(1))

    RETURN @bin
END

GO
ALTER AUTHORIZATION ON [dbo].[GetBinaryIPv4FromString] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[GetExternalOrdersTransmissionOverviewString]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetExternalOrdersTransmissionOverviewString] 
(@ChangedSwitchId INT)
RETURNS NVARCHAR(MAX)
AS
BEGIN

	DECLARE @Tab CHAR = CHAR(9)
	DECLARE @CR CHAR = CHAR(13)
	DECLARE @LF CHAR = CHAR(10)
	DECLARE @SpacesPadding CHAR(17) = '                 '

	DECLARE @Result NVARCHAR(MAX) = 'One of the Order Transmission switches changed its state:' + @CR + @LF + @CR + @LF 
	
	SELECT 
		TOP 1 @Result = @Result + SwitchFriendlyName + ' ' + [dbo].[GetExternalOrdersTransmissionSwitchStateLastChangeDescription](@ChangedSwitchId)
	FROM 
		[dbo].[ExternalOrdersTransmissionSwitch]
	WHERE
		[Id] = @ChangedSwitchId
	
	
	SET @Result = @Result + @CR + @LF + @CR + @LF + @CR + @LF + '===> General Order Transmission is ' + CASE WHEN [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]() = 1 THEN 'ON ' ELSE 'OFF' END + ' <==='


	SET @Result = @Result + @CR + @LF + @CR + @LF + @CR + @LF + 'Current states of all Order Transmission switches:'


	SET @Result = @Result + @CR + @LF + @CR + @LF + N'---------------------------------------------------------------------'


	SELECT TOP 100 
		@Result = @Result + @CR + @LF + 'Switch Name: ' + [SwitchFriendlyName]
		,@Result = @Result + @CR + @LF + 'Order Transmission Status: ' + CASE WHEN [TransmissionEnabled] = 1 THEN 'ON ' ELSE 'OFF' END
		,@Result = @Result + @CR + @LF + 'Last Change: ' + [dbo].[GetExternalOrdersTransmissionSwitchStateLastChangeDescription](Id) +  + @CR + @LF + N'---------------------------------------------------------------------'
	FROM 
		[dbo].[ExternalOrdersTransmissionSwitch] switch

	-- Return the result of the function
	RETURN @Result
END

GO
ALTER AUTHORIZATION ON [dbo].[GetExternalOrdersTransmissionOverviewString] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState] 
()
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultState BIT
	SET @ResultState = 0


	 SELECT 
	 @ResultState = 
	 CASE 
		WHEN SUM(CASE WHEN [TransmissionEnabled] = 0 THEN 1 ELSE 0 END) > 0 
		THEN 0 
		ELSE 1 
		END 
	FROM [dbo].[ExternalOrdersTransmissionSwitch] WITH (NOLOCK)


	-- Return the result of the function
	RETURN @ResultState
END

GO
ALTER AUTHORIZATION ON [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  UserDefinedFunction [dbo].[GetExternalOrdersTransmissionSwitchState]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetExternalOrdersTransmissionSwitchState] 
(@SwitchName NVARCHAR(64))
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultState BIT
	SET @ResultState = 0


	SELECT TOP 1
		@ResultState = [TransmissionEnabled]
	FROM [dbo].[ExternalOrdersTransmissionSwitch]
	WHERE [SwitchName] = @SwitchName
	ORDER BY Id DESC

	-- Return the result of the function
	RETURN @ResultState
END

GO
ALTER AUTHORIZATION ON [dbo].[GetExternalOrdersTransmissionSwitchState] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetExternalOrdersTransmissionSwitchState] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  UserDefinedFunction [dbo].[GetExternalOrdersTransmissionSwitchStateLastChangeDescription]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetExternalOrdersTransmissionSwitchStateLastChangeDescription] 
(@SwitchId INT)
RETURNS NVARCHAR(MAX)
AS
BEGIN

	-- Declare the return variable here
	DECLARE @ChangeDescription NVARCHAR(MAX)
	SET @ChangeDescription = ''

	
	SELECT TOP 1
		@ChangeDescription = ('Order Transmission changed to: ' + CASE WHEN [TransmissionEnabled] = 1 THEN '''ON''' ELSE '''OFF''' END + ' on ' + CONVERT(VARCHAR, [Changed], 120) + ' UTC by ' + [ChangedBy] + ' from: [' + [ChangedFrom] + ']. Reason: [' + ISNULL([Reason], '<NOT SPECIFIED>') + ']')
	FROM 
		[dbo].[ExternalOrdersTransmissionSwitchHistory]
	WHERE
		[ExternalOrdersTransmissionSwitchId] = @SwitchId
	ORDER BY
		[Changed] DESC

	-- Return the result of the function
	RETURN @ChangeDescription
END

GO
ALTER AUTHORIZATION ON [dbo].[GetExternalOrdersTransmissionSwitchStateLastChangeDescription] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[GetIsKillSwitchUserAdmin]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetIsKillSwitchUserAdmin] 
(
	@UserName NVARCHAR(256)
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultState BIT
	SET @ResultState = 0


	SELECT 
		@ResultState = [IsAdmin]
	FROM 
		[dbo].[KillSwitchUser]
	WHERE
		UserName = @UserName

	-- Return the result of the function
	RETURN @ResultState
END

GO
ALTER AUTHORIZATION ON [dbo].[GetIsKillSwitchUserAdmin] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetIsKillSwitchUserAdmin] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  UserDefinedFunction [dbo].[GetPbId]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetPbId](@counterpartyId tinyint, @date [date])
RETURNS [tinyint]
AS 
BEGIN
	DECLARE @pbId [tinyint]
	
	SELECT 
		@pbId = map.[PrimeBrokerId]
	FROM 
		[dbo].[CommissionsSchedule_Mapping] map
	WHERE
		map.[ValidFromUtc] <= @date AND map.[ValidToUtc] >= @date
		AND map.[CounterpartyId] = @counterpartyId

	RETURN @pbId
END

GO
ALTER AUTHORIZATION ON [dbo].[GetPbId] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[GetStringIPv4FromBinary]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetStringIPv4FromBinary](@ip AS BINARY(4)) RETURNS VARCHAR(15)
AS
BEGIN
    DECLARE @str AS VARCHAR(15) 

    SELECT @str = CAST( CAST( SUBSTRING( @ip, 1, 1) AS INTEGER) AS VARCHAR(3) ) + '.'
                + CAST( CAST( SUBSTRING( @ip, 2, 1) AS INTEGER) AS VARCHAR(3) ) + '.'
                + CAST( CAST( SUBSTRING( @ip, 3, 1) AS INTEGER) AS VARCHAR(3) ) + '.'
                + CAST( CAST( SUBSTRING( @ip, 4, 1) AS INTEGER) AS VARCHAR(3) );

    RETURN @str
END

GO
ALTER AUTHORIZATION ON [dbo].[GetStringIPv4FromBinary] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[AggregatedExternalExecutedTicket]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AggregatedExternalExecutedTicket](
	[AggregatedExternalExecutedTicketCounterpartyId] [nvarchar](50) NOT NULL,
	[IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
	[CounterpartySentTimeUtc] [datetime2](7) NULL,
	[AggregatedSizeBaseAbs] [decimal](18, 2) NOT NULL,
	[AggregatedPrice] [decimal](18, 10) NOT NULL,
	[StpCounterpartyId] [tinyint] NOT NULL,
	[FixMessageRaw] [nvarchar](1024) NOT NULL,
	[DatePartIntegratorReceivedTimeUtc]  AS (CONVERT([date],[IntegratorReceivedTimeUtc])) PERSISTED
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[AggregatedExternalExecutedTicket] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[AggregatedExternalExecutedTicketMap]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AggregatedExternalExecutedTicketMap](
	[AggregatedExternalExecutedTicketCounterpartyId] [nvarchar](50) NOT NULL,
	[InternalTransactionIdentity] [nvarchar](50) NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[AggregatedExternalExecutedTicketMap] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[CertificateFile]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CertificateFile](
	[FileName] [nvarchar](100) NOT NULL,
	[FileContent] [varbinary](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[CertificateFile] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Command]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Command](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[CommandText] [nvarchar](max) NOT NULL,
	[CreationTime] [datetime] NOT NULL,
 CONSTRAINT [PK_Command] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Command] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[CommandStatus]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommandStatus](
	[CommandId] [int] NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[Time] [datetime] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[CommandStatus] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[CommissionsPerCounterparty]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommissionsPerCounterparty](
	[CounterpartyId] [tinyint] NOT NULL,
	[CounterpartyCommissionPpm] [decimal](18, 6) NOT NULL,
	[PbCommissionPpm] [decimal](18, 6) NOT NULL,
	[CommissionPricePerMillion]  AS ([CounterpartyCommissionPpm]+[PbCommissionPpm]),
	[PrimeBrokerId] [tinyint] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[CommissionsPerCounterparty] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[CommissionsSchedule_CptCommission]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommissionsSchedule_CptCommission](
	[CounterpartyId] [tinyint] NOT NULL,
	[CounterpartyCommissionPpm] [decimal](18, 6) NOT NULL,
	[ValidFromUtc] [date] NOT NULL,
	[ValidToUtc] [date] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[CommissionsSchedule_CptCommission] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[CommissionsSchedule_Mapping]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommissionsSchedule_Mapping](
	[CounterpartyId] [tinyint] NOT NULL,
	[PrimeBrokerId] [tinyint] NOT NULL,
	[ValidFromUtc] [date] NOT NULL,
	[ValidToUtc] [date] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[CommissionsSchedule_Mapping] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[CommissionsSchedule_PBCommission]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommissionsSchedule_PBCommission](
	[PrimeBrokerId] [tinyint] NOT NULL,
	[PbCommissionPpm] [decimal](18, 6) NOT NULL,
	[ValidFromUtc] [date] NOT NULL,
	[ValidToUtc] [date] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[CommissionsSchedule_PBCommission] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Counterparty]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Counterparty](
	[CounterpartyId] [tinyint] NOT NULL,
	[CounterpartyName] [nchar](50) NOT NULL,
	[CounterpartyCode] [char](3) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Counterparty] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Currency_Internal]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency_Internal](
	[CurrencyID] [smallint] IDENTITY(1,1) NOT NULL,
	[CurrencyAlphaCode] [nvarchar](50) NOT NULL,
	[CurrencyName] [nvarchar](250) NOT NULL,
	[CurrencyISOCode] [smallint] NULL,
	[Valid] [bit] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[CurrencyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Currency_Internal] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Currency_Rates]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency_Rates](
	[CurrencyID] [smallint] NOT NULL,
	[LastKnownUsdConversionMultiplier] [decimal](18, 9) NULL,
	[UsdConversionMultiplierUpdated] [datetime2](7) NULL,
 CONSTRAINT [PK_Currency_Rates] PRIMARY KEY CLUSTERED 
(
	[CurrencyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Currency_Rates] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[DealDirection]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealDirection](
	[Id] [bit] NOT NULL,
	[Name] [nvarchar](4) NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[DealDirection] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[DealExternalExecuted]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DealExternalExecuted](
	[IntegratorReceivedExecutionReportUtc] [datetime2](7) NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[AmountBasePolExecuted] [decimal](18, 2) NULL,
	[Price] [decimal](18, 9) NOT NULL,
	[CounterpartyTransactionId] [nvarchar](64) NOT NULL,
	[CounterpartySuppliedExecutionTimeStampUtc] [datetime2](7) NULL,
	[ValueDateCounterpartySuppliedLocMktDate] [date] NOT NULL,
	[OrderSentToExecutionReportReceivedLatency] [time](7) NOT NULL,
	[CounterpartySentExecutionReportUtc] [datetime2](7) NULL,
	[AmountTermPolExecutedInUsd] [decimal](18, 2) NULL,
	[DatePartIntegratorReceivedExecutionReportUtc]  AS (CONVERT([date],[IntegratorReceivedExecutionReportUtc])) PERSISTED,
	[InternalTransactionIdentity] [nvarchar](50) NULL,
	[FlowSideId] [bit] NULL,
	[QuoteReceivedToOrderSentInternalLatency] [time](7) NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL,
	[CounterpartyClientId] [varchar](20) NULL,
	[RecordId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalRecordId]  AS ([RecordId]*(4)+(0)) PERSISTED,
	[PrimeBrokerId]  AS ([dbo].[GetPbId]([CounterpartyId],CONVERT([date],[IntegratorReceivedExecutionReportUtc]))),
	[CounterpartyClientIdPostTrade] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[DealExternalExecuted] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[DealExternalExecutedTicket]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DealExternalExecutedTicket](
	[InternalTransactionIdentity] [nvarchar](50) NOT NULL,
	[IntegratorSentOrReceivedTimeUtc] [datetime2](7) NOT NULL,
	[CounterpartySentTimeUtc] [datetime2](7) NULL,
	[DealExternalExecutedTicketTypeId] [tinyint] NOT NULL,
	[FixMessageRaw] [nvarchar](1024) NOT NULL,
	[DatePartIntegratorSentOrReceivedTimeUtc]  AS (CONVERT([date],[IntegratorSentOrReceivedTimeUtc])) PERSISTED,
	[StpCounterpartyId] [tinyint] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[DealExternalExecutedTicket] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[DealExternalExecutedTicketType]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealExternalExecutedTicketType](
	[DealExternalExecutedTicketTypeId] [tinyint] NOT NULL,
	[DealExternalExecutedTicketTypeName] [nvarchar](17) NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[DealExternalExecutedTicketType] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[DealExternalRejected]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DealExternalRejected](
	[IntegratorReceivedExecutionReportUtc] [datetime2](7) NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[AmountBasePolRejected] [decimal](18, 2) NULL,
	[OrderSentToExecutionReportReceivedLatency] [time](7) NOT NULL,
	[RejectionReason] [nvarchar](64) NOT NULL,
	[CounterpartySentExecutionReportUtc] [datetime2](7) NULL,
	[DatePartIntegratorReceivedExecutionReportUtc]  AS (CONVERT([date],[IntegratorReceivedExecutionReportUtc])) PERSISTED,
	[QuoteReceivedToOrderSentInternalLatency] [time](7) NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL,
	[RejectedPrice] [decimal](18, 9) NULL,
	[RecordId] [int] IDENTITY(1,1) NOT NULL,
	[RejectionDirectionId] [bit] NOT NULL,
	[CounterpartyClientId] [varchar](20) NULL,
	[GlobalRecordId]  AS ([RecordId]*(4)+(1)) PERSISTED,
	[PrimeBrokerId]  AS ([dbo].[GetPbId]([CounterpartyId],CONVERT([date],[IntegratorReceivedExecutionReportUtc]))),
	[CounterpartyClientIdPostTrade] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[DealExternalRejected] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Dictionary_FIX]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dictionary_FIX](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
	[SettingsVersion] [int] NOT NULL,
	[DictionaryXml] [xml] NOT NULL,
	[LastUpdated] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Dictionary_FIX] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[ExecutedExternalDeals]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExecutedExternalDeals](
	[OrderId] [nvarchar](32) NOT NULL,
	[ExecutionTimeStampUtc] [datetime] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[ExecutedAmountBaseAbs] [decimal](18, 0) NOT NULL,
	[ExecutedAmountBasePol] [decimal](18, 0) NOT NULL,
	[Price] [decimal](18, 9) NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[ExecutedExternalDeals] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[ExecutionReportMessage]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExecutionReportMessage](
	[IntegratorReceivedExecutionReportUtc] [datetime2](7) NOT NULL,
	[ExecutionReportStatusId] [tinyint] NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[CounterpartySentExecutionReportUtc] [datetime2](7) NOT NULL,
	[FixMessageReceivedRaw] [nvarchar](1024) NOT NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL
) ON [LargeSlowDataFileGroup]

GO
ALTER AUTHORIZATION ON [dbo].[ExecutionReportMessage] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[ExecutionReportMessage_Archive]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExecutionReportMessage_Archive](
	[IntegratorReceivedExecutionReportUtc] [datetime2](7) NOT NULL,
	[ExecutionReportStatusId] [tinyint] NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[CounterpartySentExecutionReportUtc] [datetime2](7) NOT NULL,
	[FixMessageReceivedRaw] [nvarchar](1024) NOT NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL
) ON [LargeSlowDataFileGroup]

GO
ALTER AUTHORIZATION ON [dbo].[ExecutionReportMessage_Archive] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[ExecutionReportMessage_Archive2]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExecutionReportMessage_Archive2](
	[IntegratorReceivedExecutionReportUtc] [datetime2](7) NOT NULL,
	[ExecutionReportStatusId] [tinyint] NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[CounterpartySentExecutionReportUtc] [datetime2](7) NOT NULL,
	[FixMessageReceivedRaw] [nvarchar](1024) NOT NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL
) ON [LargeSlowDataFileGroup]

GO
ALTER AUTHORIZATION ON [dbo].[ExecutionReportMessage_Archive2] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[ExecutionReportStatus]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExecutionReportStatus](
	[Id] [tinyint] NOT NULL,
	[Name] [varchar](16) NOT NULL,
 CONSTRAINT [IX_ExecutionReportStatusName] UNIQUE NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[ExecutionReportStatus] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[ExternalOrdersTransmissionSwitch]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExternalOrdersTransmissionSwitch](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SwitchName] [nvarchar](64) NOT NULL,
	[TransmissionEnabled] [bit] NOT NULL,
	[SwitchFriendlyName] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_ExternalOrdersTransmissionSwitch] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[ExternalOrdersTransmissionSwitch] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[ExternalOrdersTransmissionSwitchHistory]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExternalOrdersTransmissionSwitchHistory](
	[ExternalOrdersTransmissionSwitchId] [int] NOT NULL,
	[TransmissionEnabled] [bit] NOT NULL,
	[Changed] [datetime] NOT NULL,
	[ChangedBy] [nvarchar](256) NOT NULL,
	[ChangedFrom] [nvarchar](256) NOT NULL,
	[Reason] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[ExternalOrdersTransmissionSwitchHistory] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[FlowSide]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FlowSide](
	[FlowSideId] [bit] NOT NULL,
	[FlowSideName] [varchar](16) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[FlowSide] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Fluent_FixCredentials]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Fluent_FixCredentials](
	[LockX] [bit] NOT NULL,
	[UserName] [varchar](20) NULL,
	[Password] [varchar](20) NULL,
	[OnBehalfOfCompId] [varchar](20) NULL,
 CONSTRAINT [PK_FCred] PRIMARY KEY CLUSTERED 
(
	[LockX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Fluent_FixCredentials] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Fluent_RedirectionState]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fluent_RedirectionState](
	[LockX] [bit] NOT NULL,
	[IsFluentRedirectionOn] [bit] NOT NULL,
 CONSTRAINT [PK_FT1] PRIMARY KEY CLUSTERED 
(
	[LockX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Fluent_RedirectionState] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorEndpoint]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntegratorEndpoint](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[PrivateIPBinary] [binary](4) NOT NULL,
	[PublicIPBinary] [binary](4) NOT NULL,
	[Name] [varchar](50) NULL,
	[PrivateIPString]  AS ([dbo].[GetStringIPv4FromBinary]([PrivateIPBinary])),
	[PublicIPString]  AS ([dbo].[GetStringIPv4FromBinary]([PublicIPBinary]))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[IntegratorEndpoint] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorEnvironment]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntegratorEnvironment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[IntegratorEnvironmentTypeId] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[IntegratorEnvironment] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorEnvironmentToSettingsFixMap]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap](
	[IntegratorEnvironmentId] [int] NOT NULL,
	[SettingsFixId] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[IntegratorEnvironmentToSettingsFixMap] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorEnvironmentToSettingsMap]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntegratorEnvironmentToSettingsMap](
	[IntegratorEnvironmentId] [int] NOT NULL,
	[SettingsId] [int] NOT NULL,
 CONSTRAINT [MappingIsUnique] UNIQUE NONCLUSTERED 
(
	[IntegratorEnvironmentId] ASC,
	[SettingsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[IntegratorEnvironmentToSettingsMap] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorEnvironmentType]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntegratorEnvironmentType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](200) NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[IntegratorEnvironmentType] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorInstance]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntegratorInstance](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[IntegratorInstance] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorInstanceSessionClaim]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntegratorInstanceSessionClaim](
	[IntegratorInstanceId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
 CONSTRAINT [OneInstancePerSessionConstraint] UNIQUE NONCLUSTERED 
(
	[CounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[IntegratorInstanceSessionClaim] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorProcess]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntegratorProcess](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IntegratorInstanceId] [tinyint] NOT NULL,
	[IntegratorProcessTypeId] [tinyint] NOT NULL,
	[IntegratorProcessStateId] [tinyint] NOT NULL,
	[IntegratorEndpointId] [tinyint] NOT NULL,
	[LastHBTimeUtc] [datetime] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[IntegratorProcess] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorProcessState]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntegratorProcessState](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](25) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[IntegratorProcessState] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorProcessType]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntegratorProcessType](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[TradingAPIPort] [int] NULL,
	[DiagnosticsAPIPort] [int] NULL,
	[IsUniquePerInstance] [bit] NOT NULL,
	[Description] [nvarchar](200) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[IntegratorProcessType] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorRejectionReason]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IntegratorRejectionReason](
	[IntegratorRejectionReasonId] [tinyint] IDENTITY(1,1) NOT NULL,
	[IntegratorRejectionReason] [varchar](32) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[IntegratorRejectionReason] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[KillSwitchLogon]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KillSwitchLogon](
	[UserName] [nvarchar](256) NOT NULL,
	[ActionTime] [datetime] NOT NULL,
	[Source] [nvarchar](256) NOT NULL,
	[Success] [bit] NOT NULL,
	[RoleId] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[KillSwitchLogon] TO  SCHEMA OWNER 
GO
GRANT SELECT ON [dbo].[KillSwitchLogon] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  Table [dbo].[KillSwitchRole]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KillSwitchRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_KillSwitchRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[KillSwitchRole] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[KillSwitchUser]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KillSwitchUser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[Password] [nvarchar](256) NOT NULL,
	[IsAdmin] [bit] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_KillSwitchUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_KillSwitchUser] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[KillSwitchUser] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[NewCommand]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewCommand](
	[CommandId] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[NewCommand] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternal]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderExternal](
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[ExternalOrderLabelForCounterparty] [nvarchar](32) NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[QuoteReceivedToOrderSentInternalLatency] [time](7) NULL,
	[FixMessageSentRaw] [nvarchar](1024) NULL,
	[SourceIntegratorPriceIdentity] [uniqueidentifier] NULL,
	[OrderTypeId] [tinyint] NULL,
	[OrderTimeInForceId] [tinyint] NULL,
	[RequestedPrice] [decimal](18, 9) NULL,
	[RequestedAmountBaseAbs] [decimal](18, 0) NULL,
	[SourceIntegratorPriceTimeUtc] [datetime2](7) NULL,
	[PegTypeId] [bit] NULL,
	[PegDifferenceBasePol] [decimal](18, 9) NULL,
	[DatePartIntegratorSentExternalOrderUtc]  AS (CONVERT([date],[IntegratorSentExternalOrderUtc])) PERSISTED
) ON [LargeSlowDataFileGroup]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[OrderExternal] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternal_Archive]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderExternal_Archive](
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[ExternalOrderLabelForCounterparty] [nvarchar](32) NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[QuoteReceivedToOrderSentInternalLatency] [time](7) NULL,
	[FixMessageSentRaw] [nvarchar](1024) NULL,
	[SourceIntegratorPriceIdentity] [uniqueidentifier] NULL,
	[OrderTypeId] [tinyint] NULL,
	[OrderTimeInForceId] [tinyint] NULL,
	[RequestedPrice] [decimal](18, 9) NULL,
	[RequestedAmountBaseAbs] [decimal](18, 0) NULL,
	[SourceIntegratorPriceTimeUtc] [datetime2](7) NULL,
	[PegTypeId] [bit] NULL,
	[PegDifferenceBasePol] [decimal](18, 9) NULL,
	[DatePartIntegratorSentExternalOrderUtc]  AS (CONVERT([date],[IntegratorSentExternalOrderUtc])) PERSISTED
) ON [LargeSlowDataFileGroup]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[OrderExternal_Archive] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternal_Archive2]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderExternal_Archive2](
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[ExternalOrderLabelForCounterparty] [nvarchar](32) NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[QuoteReceivedToOrderSentInternalLatency] [time](7) NULL,
	[FixMessageSentRaw] [nvarchar](1024) NULL,
	[SourceIntegratorPriceIdentity] [uniqueidentifier] NULL,
	[OrderTypeId] [tinyint] NULL,
	[OrderTimeInForceId] [tinyint] NULL,
	[RequestedPrice] [decimal](18, 9) NULL,
	[RequestedAmountBaseAbs] [decimal](18, 0) NULL,
	[SourceIntegratorPriceTimeUtc] [datetime2](7) NULL,
	[PegTypeId] [bit] NULL,
	[PegDifferenceBasePol] [decimal](18, 9) NULL,
	[DatePartIntegratorSentExternalOrderUtc]  AS (CONVERT([date],[IntegratorSentExternalOrderUtc])) PERSISTED
) ON [LargeSlowDataFileGroup]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[OrderExternal_Archive2] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalCancelRequest]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalCancelRequest](
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NULL,
	[IntegratorSentCancelRequestUtc] [datetime2](7) NOT NULL,
	[OrderExternalCancelTypeId] [tinyint] NOT NULL,
	[FixMessageSentRaw] [nvarchar](1024) NOT NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NULL
) ON [LargeSlowDataFileGroup]

GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalCancelRequest] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalCancelRequest_Archive]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalCancelRequest_Archive](
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NULL,
	[IntegratorSentCancelRequestUtc] [datetime2](7) NOT NULL,
	[OrderExternalCancelTypeId] [tinyint] NOT NULL,
	[FixMessageSentRaw] [nvarchar](1024) NOT NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NULL
) ON [LargeSlowDataFileGroup]

GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalCancelRequest_Archive] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalCancelRequest_Archive2]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalCancelRequest_Archive2](
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NULL,
	[IntegratorSentCancelRequestUtc] [datetime2](7) NOT NULL,
	[OrderExternalCancelTypeId] [tinyint] NOT NULL,
	[FixMessageSentRaw] [nvarchar](1024) NOT NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NULL
) ON [LargeSlowDataFileGroup]

GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalCancelRequest_Archive2] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalCancelResult]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalCancelResult](
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[CounterpartySentResultUtc] [datetime2](7) NOT NULL,
	[CanceledAmountBasePol] [decimal](18, 2) NULL,
	[ResultStatusId] [tinyint] NOT NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL
) ON [LargeSlowDataFileGroup]

GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalCancelResult] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalCancelResult_Archive]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalCancelResult_Archive](
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[CounterpartySentResultUtc] [datetime2](7) NOT NULL,
	[CanceledAmountBasePol] [decimal](18, 0) NOT NULL,
	[ResultStatusId] [tinyint] NOT NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL
) ON [LargeSlowDataFileGroup]

GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalCancelResult_Archive] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalCancelResult_Archive2]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalCancelResult_Archive2](
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[CounterpartySentResultUtc] [datetime2](7) NOT NULL,
	[CanceledAmountBasePol] [decimal](18, 0) NOT NULL,
	[ResultStatusId] [tinyint] NOT NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL
) ON [LargeSlowDataFileGroup]

GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalCancelResult_Archive2] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalCancelType]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderExternalCancelType](
	[OrderExternalCancelTypeId] [tinyint] NOT NULL,
	[OrderExternalCancelTypeName] [varchar](16) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalCancelType] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalIgnored]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalIgnored](
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL,
	[IntegratorDetectedOrderIsIgnoredUtc] [datetime2](7) NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[ExternalOrderLabelForCounterparty] [nvarchar](32) NOT NULL,
	[RequestedAmountBaseAbs] [decimal](18, 2) NULL,
	[RequestedPrice] [decimal](18, 9) NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[DealDirectionId] [bit] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalIgnored] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalIncomingRejectable_Archive]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderExternalIncomingRejectable_Archive](
	[IntegratorReceivedExternalOrderUtc] [datetime2](7) NOT NULL,
	[CounterpartySentExternalOrderUtc] [datetime2](7) NOT NULL,
	[IntegratorSentExecutionReportUtc] [datetime2](7) NOT NULL,
	[CounterpartyClientOrderId] [varchar](32) NOT NULL,
	[TradingSystemId] [int] NULL,
	[SourceIntegratorPriceIdentity] [varchar](20) NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[IntegratorDealDirectionId] [bit] NOT NULL,
	[CounterpartyRequestedPrice] [decimal](18, 9) NOT NULL,
	[CounterpartyRequestedAmountBasePol] [decimal](18, 2) NOT NULL,
	[IntegratorExecutionId] [varchar](20) NOT NULL,
	[ValueDate] [date] NULL,
	[OrderReceivedToExecutionReportSentLatency] [time](7) NOT NULL,
	[IntegratorExecutedAmountBasePol] [decimal](18, 2) NOT NULL,
	[IntegratorRejectedAmountBasePol] [decimal](18, 2) NOT NULL,
	[IntegratorLatencyNanoseconds]  AS (CONVERT([bigint],(datepart(hour,[OrderReceivedToExecutionReportSentLatency])*(60)+datepart(minute,[OrderReceivedToExecutionReportSentLatency]))*(60)+datepart(second,[OrderReceivedToExecutionReportSentLatency]))*(1000000000)+datepart(nanosecond,[OrderReceivedToExecutionReportSentLatency])) PERSISTED,
	[DatePartIntegratorReceivedOrderUtc]  AS (CONVERT([date],[IntegratorReceivedExternalOrderUtc])) PERSISTED,
	[CounterpartyClientId] [varchar](20) NULL,
	[IntegratorRejectionReasonId] [tinyint] NULL,
	[GlobalRecordId] [int] NOT NULL,
	[PrimeBrokerId]  AS ([dbo].[GetPbId]([CounterpartyId],CONVERT([date],[IntegratorReceivedExternalOrderUtc])))
) ON [LargeSlowDataFileGroup]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalIncomingRejectable_Archive] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalIncomingRejectable_Daily]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderExternalIncomingRejectable_Daily](
	[IntegratorReceivedExternalOrderUtc] [datetime2](7) NOT NULL,
	[CounterpartySentExternalOrderUtc] [datetime2](7) NOT NULL,
	[IntegratorSentExecutionReportUtc] [datetime2](7) NOT NULL,
	[CounterpartyClientOrderId] [varchar](32) NOT NULL,
	[TradingSystemId] [int] NULL,
	[SourceIntegratorPriceIdentity] [varchar](20) NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[IntegratorDealDirectionId] [bit] NOT NULL,
	[CounterpartyRequestedPrice] [decimal](18, 9) NOT NULL,
	[CounterpartyRequestedAmountBasePol] [decimal](18, 2) NOT NULL,
	[IntegratorExecutionId] [varchar](20) NOT NULL,
	[ValueDate] [date] NULL,
	[OrderReceivedToExecutionReportSentLatency] [time](7) NOT NULL,
	[IntegratorExecutedAmountBasePol] [decimal](18, 2) NOT NULL,
	[IntegratorRejectedAmountBasePol] [decimal](18, 2) NOT NULL,
	[IntegratorLatencyNanoseconds]  AS (CONVERT([bigint],(datepart(hour,[OrderReceivedToExecutionReportSentLatency])*(60)+datepart(minute,[OrderReceivedToExecutionReportSentLatency]))*(60)+datepart(second,[OrderReceivedToExecutionReportSentLatency]))*(1000000000)+datepart(nanosecond,[OrderReceivedToExecutionReportSentLatency])) PERSISTED,
	[DatePartIntegratorReceivedOrderUtc]  AS (CONVERT([date],[IntegratorReceivedExternalOrderUtc])) PERSISTED,
	[CounterpartyClientId] [varchar](20) NULL,
	[IntegratorRejectionReasonId] [tinyint] NULL,
	[RecordId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalRecordId]  AS ([RecordId]*(4)+(2)) PERSISTED,
	[PrimeBrokerId]  AS ([dbo].[GetPbId]([CounterpartyId],CONVERT([date],[IntegratorReceivedExternalOrderUtc])))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalIncomingRejectable_Daily] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalIncomingTimeoutsFromCounterparty]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderExternalIncomingTimeoutsFromCounterparty](
	[IntegratorReceivedTimeoutUtc] [datetime2](7) NOT NULL,
	[CounterpartySentTimeoutUtc] [datetime2](7) NOT NULL,
	[CounterpartyClientOrderId] [varchar](32) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalIncomingTimeoutsFromCounterparty] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderTimeInForce]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderTimeInForce](
	[OrderTimeInForceId] [tinyint] NOT NULL,
	[OrderTimeInForceName] [varchar](16) NOT NULL,
 CONSTRAINT [IX_OrderTimeInForceName] UNIQUE NONCLUSTERED 
(
	[OrderTimeInForceName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[OrderTimeInForce] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderType]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderType](
	[OrderTypeId] [tinyint] NOT NULL,
	[OrderTypeName] [varchar](32) NOT NULL,
 CONSTRAINT [IX_OrderTypeName] UNIQUE NONCLUSTERED 
(
	[OrderTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[OrderType] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[PegType]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PegType](
	[PegTypeId] [bit] NOT NULL,
	[PegTypeName] [varchar](16) NOT NULL,
 CONSTRAINT [IX_PegTypeName] UNIQUE NONCLUSTERED 
(
	[PegTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[PegType] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[PersistedStatisticsPerCounterpartyDaily]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily](
	[Counterparty] [varchar](3) NOT NULL,
	[NopAbs] [decimal](18, 2) NULL,
	[VolumeBaseAbsInUsd] [decimal](18, 2) NULL,
	[VolumeShare] [decimal](18, 4) NULL,
	[DealsNum] [int] NULL,
	[AvgDealSizeUsd] [decimal](18, 2) NULL,
	[AvgIntegratorLatencyNanoseconds] [bigint] NULL,
	[AvgCounterpartyLatencyNanoseconds] [bigint] NULL,
	[KGTRejectionsNum] [int] NULL,
	[CtpRejectionsNum] [int] NULL,
	[KGTRejectionsRate] [decimal](18, 4) NULL,
	[CtpRejectionsRate] [decimal](18, 4) NULL,
	[LastDealUtc] [datetime2](7) NULL,
	[LastCptRejectionUtc] [datetime2](7) NULL,
	[LastKgtRejectionUtc] [datetime2](7) NULL,
	[CommissionsPbUsd] [decimal](18, 2) NULL,
	[CommissionsCptUsd] [decimal](18, 2) NULL,
	[CommissionsSumUsd] [decimal](18, 2) NULL
) ON [Integrator_DailyData]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[PersistedStatisticsPerCounterpartyDaily] TO  SCHEMA OWNER 
GO
GRANT ALTER ON [dbo].[PersistedStatisticsPerCounterpartyDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[PersistedStatisticsPerCounterpartyDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Table [dbo].[PersistedStatisticsPerTradingGroupsDaily]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily](
	[Rank] [int] NOT NULL,
	[TradingSystemGroupName] [nvarchar](32) NOT NULL,
	[TradingSystemGroupId] [int] NOT NULL,
	[TradingSystemTypeName] [nvarchar](12) NULL,
	[Counterparty] [char](3) NULL,
	[TradingSystemTypeId] [int] NULL,
	[ConfiguredSystemsCount] [int] NULL,
	[EnabledSystemsCount] [int] NULL,
	[VolumeUsdM] [decimal](18, 6) NULL,
	[DealsNum] [int] NULL,
	[PnlGrossUsdPerMUsd] [decimal](18, 6) NULL,
	[CommissionsUsdPerMUsd] [decimal](18, 6) NULL,
	[PnlNetUsdPerMUsd] [decimal](18, 6) NULL,
	[PnlGrossUsd] [decimal](18, 6) NULL,
	[CommissionsUsd] [decimal](18, 6) NULL,
	[PnlNetUsd] [decimal](18, 6) NULL,
	[IsOrderTransmissionDisabled] [bit] NULL,
	[IsGoFlatOnlyOn] [bit] NULL,
	[PageId] [int] NULL,
	[BeginSymbol] [nchar](7) NULL,
	[EndSymbol] [nchar](7) NULL,
	[StripsCount] [int] NULL,
	[KGTRejectionsNum] [int] NULL,
	[CtpRejectionsNum] [int] NULL,
	[HardBlockedSystemsCount] [int] NULL,
	[LastDealUtc] [datetime2](7) NULL,
	[LastCptRejectionUtc] [datetime2](7) NULL,
	[LastKgtRejectionUtc] [datetime2](7) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[PersistedStatisticsPerTradingGroupsDaily] TO  SCHEMA OWNER 
GO
GRANT ALTER ON [dbo].[PersistedStatisticsPerTradingGroupsDaily] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT SELECT ON [dbo].[PersistedStatisticsPerTradingGroupsDaily] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT ALTER ON [dbo].[PersistedStatisticsPerTradingGroupsDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[PersistedStatisticsPerTradingGroupsDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Table [dbo].[PersistedStatisticsPerTradingSystemDaily]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily](
	[TradingSystemId] [int] NOT NULL,
	[NopBasePol] [decimal](18, 2) NULL,
	[VolumeUsdM] [decimal](18, 6) NOT NULL,
	[DealsNum] [int] NOT NULL,
	[PnlGrossUsdPerMUsd] [decimal](18, 6) NOT NULL,
	[PnlGrossUsd] [decimal](18, 6) NOT NULL,
	[CommissionsUsdPerMUsd] [decimal](18, 6) NOT NULL,
	[CommissionsUsd] [decimal](18, 6) NOT NULL,
	[PnlNetUsdPerMUsd] [decimal](18, 6) NOT NULL,
	[PnlNetUsd] [decimal](18, 6) NOT NULL,
	[KGTRejectionsNum] [int] NOT NULL,
	[CtpRejectionsNum] [int] NOT NULL,
	[LastDealUtc] [datetime2](7) NULL,
	[LastCptRejectionUtc] [datetime2](7) NULL,
	[LastKgtRejectionUtc] [datetime2](7) NULL,
	[CommissionsPbUsd] [decimal](18, 2) NULL,
	[CommissionsPbUsdPerMUsd] [decimal](18, 2) NULL,
	[CommissionsCptUsd] [decimal](18, 2) NULL,
	[CommissionsCptUsdPerMUsd] [decimal](18, 2) NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[PersistedStatisticsPerTradingSystemDaily] TO  SCHEMA OWNER 
GO
GRANT ALTER ON [dbo].[PersistedStatisticsPerTradingSystemDaily] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT SELECT ON [dbo].[PersistedStatisticsPerTradingSystemDaily] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT ALTER ON [dbo].[PersistedStatisticsPerTradingSystemDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[PersistedStatisticsPerTradingSystemDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Table [dbo].[PnlByDate]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PnlByDate](
	[DateUtc] [date] NOT NULL,
	[MidnightPnlNet] [decimal](18, 2) NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[PnlByDate] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[PriceDelaySettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PriceDelaySettings](
	[CounterpartyId] [tinyint] NOT NULL,
	[ExpectedDelayTicks] [bigint] NOT NULL,
	[MaxAllowedAdditionalDelayTicks] [bigint] NOT NULL,
	[ExpectedDelay]  AS (dateadd(nanosecond,[ExpectedDelayTicks]*(100),CONVERT([time](7),'00:00:00'))),
	[MaxAllowedAdditionalDelay]  AS (dateadd(nanosecond,[ExpectedDelayTicks]%(10),dateadd(microsecond,[MaxAllowedAdditionalDelayTicks]/(10),CONVERT([time](7),'00:00:00'))))
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[PriceDelaySettings] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[PriceDelayStats]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PriceDelayStats](
	[CounterpartyId] [tinyint] NOT NULL,
	[TotalReceivedPricesInLastMinute] [bigint] NOT NULL,
	[DelayedPricesInLastMinute] [bigint] NOT NULL,
	[TotalReceivedPricesInLastHour] [bigint] NOT NULL,
	[DelayedPricesInLastHour] [bigint] NOT NULL,
	[LastMinuteDelayedPricesPercentage]  AS (case when [TotalReceivedPricesInLastMinute]=(0) then (0) else (CONVERT([numeric],[DelayedPricesInLastMinute])/[TotalReceivedPricesInLastMinute])*(100) end),
	[LastHourDelayedPricesPercentage]  AS (case when [TotalReceivedPricesInLastHour]=(0) then (0) else (CONVERT([numeric],[DelayedPricesInLastHour])/[TotalReceivedPricesInLastHour])*(100) end)
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[PriceDelayStats] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[PrimeBroker]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrimeBroker](
	[PrimeBrokerId] [tinyint] NOT NULL,
	[PrimeBrokerName] [varchar](10) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[PrimeBroker] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[RejectionDirection]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RejectionDirection](
	[RejectionDirectionId] [bit] NOT NULL,
	[RejectionDirectionDescription] [varchar](16) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[RejectionDirection] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Settings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[FriendlyName] [nvarchar](200) NULL,
	[SettingsVersion] [int] NOT NULL,
	[SettingsXml] [xml] NOT NULL,
	[LastUpdated] [datetime] NULL,
	[IsOnlineMonitored] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Settings] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Settings_FIX]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings_FIX](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[SettingsVersion] [int] NOT NULL,
	[Configuration] [nvarchar](max) NOT NULL,
	[LastUpdated] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Settings_FIX] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[SettingsMandatoryInEnvironemntType]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SettingsMandatoryInEnvironemntType](
	[IntegratorEnvironmentTypeId] [int] NOT NULL,
	[SettingsName] [nvarchar](100) NOT NULL,
 CONSTRAINT [MandatorySettingIsUnique] UNIQUE NONCLUSTERED 
(
	[IntegratorEnvironmentTypeId] ASC,
	[SettingsName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[SettingsMandatoryInEnvironemntType] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[SettlementDate]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SettlementDate](
	[SymbolId] [tinyint] NOT NULL,
	[SettlementDate] [date] NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[SettlementDate] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[StpCounterparty]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StpCounterparty](
	[StpCounterpartyId] [tinyint] NOT NULL,
	[StpCounterpartyName] [varchar](16) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[StpCounterparty] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Symbol_Internal]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Symbol_Internal](
	[Id] [tinyint] NOT NULL,
	[Name] [nchar](7) NOT NULL,
	[ShortName] [nchar](6) NOT NULL,
	[BaseCurrencyId] [smallint] NOT NULL,
	[QuoteCurrencyId] [smallint] NOT NULL,
	[IsRaboTradeable] [bit] NULL,
	[Usage] [tinyint] NULL,
 CONSTRAINT [PK_Symbol] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Symbol_Internal] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Symbol_Rates]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Symbol_Rates](
	[Id] [tinyint] NOT NULL,
	[LastKnownMeanReferencePrice]  AS (([LastKnownAskMeanPrice]+[LastKnownBidMeanPrice])/(2)) PERSISTED,
	[LastKnownAskMeanPrice] [decimal](18, 9) NULL,
	[LastKnownBidMeanPrice] [decimal](18, 9) NULL,
	[AskMeanPriceUpdated] [datetime2](7) NULL,
	[BidMeanPriceUpdated] [datetime2](7) NULL,
 CONSTRAINT [PK_Symbol_Rates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Symbol_Rates] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Symbol_TrustedSpreads]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Symbol_TrustedSpreads](
	[SymbolId] [tinyint] NOT NULL,
	[MaximumTrustedSpreadBp] [decimal](18, 4) NULL,
	[MaximumTrustedSpreadDecimal]  AS ([dbo].[ConvertBpToDecimal]([SymbolId],[MaximumTrustedSpreadBp])),
	[MinimumTrustedSpreadBp] [decimal](18, 4) NULL,
	[MinimumTrustedSpreadDecimal]  AS ([dbo].[ConvertBpToDecimal]([SymbolId],[MinimumTrustedSpreadBp])),
	[MaximumAllowedTickAge_milliseconds] [int] NULL,
	[MaximumAllowedTickAge]  AS (dateadd(millisecond,[MaximumAllowedTickAge_milliseconds],CONVERT([time](7),'00:00:00'))),
 CONSTRAINT [PK_Symbol_TrustedSpreads_Rates] PRIMARY KEY CLUSTERED 
(
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Symbol_TrustedSpreads] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[SystemsTradingControl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemsTradingControl](
	[GoFlatOnly] [bit] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[SystemsTradingControl] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[SystemsTradingControlSchedule]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SystemsTradingControlSchedule](
	[ScheduleEntryId] [int] IDENTITY(1,1) NOT NULL,
	[IsDaily] [bit] NOT NULL,
	[IsWeekly] [bit] NOT NULL,
	[ScheduleTimeZoneSpecific] [datetime2](7) NOT NULL,
	[ScheduleDescription] [nvarchar](1024) NULL,
	[LastUpdatedUtc] [datetime2](7) NULL,
	[IsEnabled] [bit] NOT NULL,
	[TimeZoneCode] [char](3) NOT NULL,
	[ActionId] [char](3) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[SystemsTradingControlSchedule] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[SystemsTradingControlScheduleAction]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SystemsTradingControlScheduleAction](
	[ActionId] [tinyint] NOT NULL,
	[ActionName] [varchar](24) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[SystemsTradingControlScheduleAction] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TableChangeLog]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TableChangeLog](
	[TableName] [varchar](40) NOT NULL,
	[LastUpdatedUtc] [datetime2](7) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[TableChangeLog] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TradeDecay_LastProcessedRecord]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradeDecay_LastProcessedRecord](
	[LockX] [bit] NOT NULL,
	[LastProcessedRecordUtc] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_T1] PRIMARY KEY CLUSTERED 
(
	[LockX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[TradeDecay_LastProcessedRecord] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TradingSystem]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradingSystem](
	[TradingSystemId] [int] IDENTITY(1,1) NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[Description] [nvarchar](1024) NULL,
	[TradingSystemTypeId] [tinyint] NOT NULL,
	[TradingSystemGroupId] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystem] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TradingSystem_Deleted]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradingSystem_Deleted](
	[TradingSystemId] [int] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[Description] [nvarchar](1024) NULL,
	[DeletedUtc] [datetime2](7) NOT NULL,
	[TradingSystemTypeId] [tinyint] NOT NULL,
	[TradingSystemGroupId] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystem_Deleted] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TradingSystemActions]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradingSystemActions](
	[TradingSystemId] [int] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[GoFlatOnly] [bit] NOT NULL,
	[OrderTransmissionDisabled] [bit] NOT NULL,
	[SettingsLastUpdatedUtc] [datetime2](7) NULL,
	[LastResetRequestedUtc] [datetime2](7) NULL,
	[HardBlocked] [bit] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemActions] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TradingSystemDealsExecuted_Archive]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradingSystemDealsExecuted_Archive](
	[TradingSystemId] [int] NOT NULL,
	[InternalTransactionIdentity] [nvarchar](50) NOT NULL,
	[AmountBasePolExecuted] [decimal](18, 2) NULL,
	[DatePartDealExecuted] [date] NOT NULL
) ON [LargeSlowDataFileGroup]

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemDealsExecuted_Archive] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TradingSystemDealsExecuted_Daily]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradingSystemDealsExecuted_Daily](
	[TradingSystemId] [int] NOT NULL,
	[AmountBasePolExecuted] [decimal](18, 2) NULL,
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[Price] [decimal](18, 9) NOT NULL,
	[InternalTransactionIdentity] [nvarchar](50) NOT NULL,
	[DealTimeUtc] [datetime2](7) NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemDealsExecuted_Daily] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TradingSystemDealsRejected_Archive]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradingSystemDealsRejected_Archive](
	[TradingSystemId] [int] NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[AmountBasePolRejected] [decimal](18, 2) NULL,
	[DatePartDealRejected] [date] NOT NULL,
	[RejectionDirectionId] [bit] NOT NULL
) ON [LargeSlowDataFileGroup]

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemDealsRejected_Archive] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TradingSystemDealsRejected_Daily]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradingSystemDealsRejected_Daily](
	[TradingSystemId] [int] NOT NULL,
	[AmountBasePolRejected] [decimal](18, 2) NULL,
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[RejectedPrice] [decimal](18, 9) NOT NULL,
	[RejectionReason] [nvarchar](64) NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[RejectionDirectionId] [bit] NOT NULL,
	[RejectionTimeUtc] [datetime2](7) NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemDealsRejected_Daily] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TradingSystemGroup]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradingSystemGroup](
	[TradingSystemGroupId] [int] IDENTITY(1,1) NOT NULL,
	[TradingSystemGroupName] [nvarchar](32) NOT NULL,
	[OrderingRank] [int] NULL,
 CONSTRAINT [UniqueTradingSystemGroupName] UNIQUE NONCLUSTERED 
(
	[TradingSystemGroupName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemGroup] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TradingSystemGroup_Deleted]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradingSystemGroup_Deleted](
	[TradingSystemGroupId] [int] NOT NULL,
	[TradingSystemGroupName] [nvarchar](32) NOT NULL,
	[DeletedUtc] [datetime2](7) NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemGroup_Deleted] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TradingSystemMonitoringPage]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradingSystemMonitoringPage](
	[TradingSystemMonitoringPageId] [int] IDENTITY(1,1) NOT NULL,
	[TradingSystemTypeId] [tinyint] NOT NULL,
	[BeginSymbolId] [tinyint] NOT NULL,
	[EndSymbolId] [tinyint] NOT NULL,
	[StripsCount] [tinyint] NOT NULL,
	[TradingSystemGroupId] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemMonitoringPage] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TradingSystemType]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TradingSystemType](
	[TradingSystemTypeId] [tinyint] IDENTITY(1,1) NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SystemName] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemType] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[TradingSystemTypeAllowedForGroup]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradingSystemTypeAllowedForGroup](
	[TradingSystemTypeId] [tinyint] NOT NULL,
	[TradingSystemGroupId] [int] NOT NULL,
	[IsGoFlatOnlyOn] [bit] NOT NULL,
	[IsOrderTransmissionDisabled] [bit] NOT NULL,
 CONSTRAINT [UniqueTradingSystemTypePerGroup] UNIQUE NONCLUSTERED 
(
	[TradingSystemGroupId] ASC,
	[TradingSystemTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemTypeAllowedForGroup] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[VenueCrossSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VenueCrossSettings](
	[SymbolId] [tinyint] NOT NULL,
	[MinimumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MaximumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MinimumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[MinimumTimeBetweenSingleCounterpartySignals_seconds] [decimal](18, 9) NULL,
	[MaximumWaitTimeOnImprovementTick_milliseconds] [int] NOT NULL,
	[CounterpartyId] [tinyint] NULL,
	[TradingSystemId] [int] NOT NULL,
	[MinimumFillSize] [decimal](18, 0) NOT NULL,
 CONSTRAINT [UNQ__VenueCrossSettings__TradingSystemId] UNIQUE NONCLUSTERED 
(
	[TradingSystemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[VenueCrossSettings] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[VenueCrossSettings_Changes]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VenueCrossSettings_Changes](
	[SymbolId] [tinyint] NOT NULL,
	[MinimumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MaximumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MinimumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[MinimumTimeBetweenSingleCounterpartySignals_seconds] [int] NOT NULL,
	[MaximumWaitTimeOnImprovementTick_milliseconds] [int] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[ValidFromUtc] [datetime2](7) NOT NULL,
	[ValidToUtc] [datetime2](7) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[MinimumFillSize] [decimal](18, 0) NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[VenueCrossSettings_Changes] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[VenueGliderSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VenueGliderSettings](
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[MaximumSizeToTrade] [decimal](18, 0) NOT NULL,
	[BestPriceImprovementOffsetBasisPoints] [decimal](18, 6) NOT NULL,
	[MaximumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[KGTLLTime_milliseconds] [int] NOT NULL,
	[MinimumBPGrossGain] [decimal](18, 6) NOT NULL,
	[PreferLmaxDuringHedging] [bit] NOT NULL,
	[MinimumIntegratorPriceLife_milliseconds] [int] NOT NULL,
	[MinimumIntegratorGlidePriceLife_milliseconds] [int] NOT NULL,
	[GlidingCutoffSpan_milliseconds] [int] NOT NULL,
	[MinimumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MinimumFillSize] [decimal](18, 0) NOT NULL,
	[MaximumSourceBookTiers] [int] NOT NULL,
	[MinimumBasisPointsFromKgtPriceToFastCancel] [decimal](18, 6) NOT NULL,
	[ReferenceLmaxBookOnly] [bit] NOT NULL,
	[BidEnabled] [bit] NOT NULL,
	[AskEnabled] [bit] NOT NULL,
 CONSTRAINT [UNQ__VenueGliderSettings__TradingSystemId] UNIQUE NONCLUSTERED 
(
	[TradingSystemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[VenueGliderSettings] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[VenueGliderSettings_Changes]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VenueGliderSettings_Changes](
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[MaximumSizeToTrade] [decimal](18, 0) NOT NULL,
	[BestPriceImprovementOffsetBasisPoints] [decimal](18, 6) NOT NULL,
	[MaximumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[KGTLLTime_milliseconds] [int] NOT NULL,
	[MinimumBPGrossGain] [decimal](18, 6) NOT NULL,
	[PreferLmaxDuringHedging] [bit] NOT NULL,
	[MinimumIntegratorPriceLife_milliseconds] [int] NOT NULL,
	[MinimumIntegratorGlidePriceLife_milliseconds] [int] NOT NULL,
	[GlidingCutoffSpan_milliseconds] [int] NOT NULL,
	[MinimumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MinimumFillSize] [decimal](18, 0) NOT NULL,
	[MaximumSourceBookTiers] [int] NOT NULL,
	[MinimumBasisPointsFromKgtPriceToFastCancel] [decimal](18, 6) NOT NULL,
	[ReferenceLmaxBookOnly] [bit] NOT NULL,
	[BidEnabled] [bit] NOT NULL,
	[AskEnabled] [bit] NOT NULL,
	[ValidFromUtc] [datetime2](7) NOT NULL,
	[ValidToUtc] [datetime2](7) NOT NULL,
	[Deleted] [bit] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[VenueGliderSettings_Changes] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[VenueMMSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VenueMMSettings](
	[SymbolId] [tinyint] NOT NULL,
	[MaximumSizeToTrade] [decimal](18, 0) NOT NULL,
	[BestPriceImprovementOffsetBasisPoints] [decimal](18, 6) NOT NULL,
	[MaximumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[MaximumWaitTimeOnImprovementTick_milliseconds] [int] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[MinimumIntegratorPriceLife_milliseconds] [int] NOT NULL,
	[MinimumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MinimumFillSize] [decimal](18, 0) NOT NULL,
	[MaximumSourceBookTiers] [int] NOT NULL,
	[MinimumBasisPointsFromKgtPriceToFastCancel] [decimal](18, 6) NOT NULL,
	[BidEnabled] [bit] NOT NULL,
	[AskEnabled] [bit] NOT NULL,
 CONSTRAINT [UNQ__VenueMMSettings__TradingSystemId] UNIQUE NONCLUSTERED 
(
	[TradingSystemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[VenueMMSettings] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[VenueMMSettings_Changes]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VenueMMSettings_Changes](
	[SymbolId] [tinyint] NOT NULL,
	[MaximumSizeToTrade] [decimal](18, 0) NOT NULL,
	[BestPriceImprovementOffsetBasisPoints] [decimal](18, 6) NOT NULL,
	[MaximumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[MaximumWaitTimeOnImprovementTick_milliseconds] [int] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[ValidFromUtc] [datetime2](7) NOT NULL,
	[ValidToUtc] [datetime2](7) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[MinimumIntegratorPriceLife_milliseconds] [int] NOT NULL,
	[MinimumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MinimumFillSize] [decimal](18, 0) NOT NULL,
	[MaximumSourceBookTiers] [int] NOT NULL,
	[MinimumBasisPointsFromKgtPriceToFastCancel] [decimal](18, 6) NOT NULL,
	[BidEnabled] [bit] NOT NULL,
	[AskEnabled] [bit] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[VenueMMSettings_Changes] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[VenueQuotingSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VenueQuotingSettings](
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NULL,
	[TradingSystemId] [int] NOT NULL,
	[MaximumPositionBaseAbs] [decimal](18, 0) NOT NULL,
	[FairPriceImprovementSpreadBasisPoints] [decimal](18, 6) NOT NULL,
	[StopLossNetInUsd] [bigint] NOT NULL,
 CONSTRAINT [UNQ__VenueQuotingSettings__SymbolId] UNIQUE NONCLUSTERED 
(
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UNQ__VenueQuotingSettings__TradingSystemId] UNIQUE NONCLUSTERED 
(
	[TradingSystemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[VenueQuotingSettings] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[VenueQuotingSettings_Changes]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VenueQuotingSettings_Changes](
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[MaximumPositionBaseAbs] [decimal](18, 0) NOT NULL,
	[FairPriceImprovementSpreadBasisPoints] [decimal](18, 6) NOT NULL,
	[StopLossNetInUsd] [bigint] NOT NULL,
	[ValidFromUtc] [datetime2](7) NOT NULL,
	[ValidToUtc] [datetime2](7) NOT NULL,
	[Deleted] [bit] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[VenueQuotingSettings_Changes] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[VenueStreamSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VenueStreamSettings](
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[MaximumSizeToTrade] [decimal](18, 0) NOT NULL,
	[BestPriceImprovementOffsetBasisPoints] [decimal](18, 6) NOT NULL,
	[MaximumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[KGTLLTime_milliseconds] [int] NOT NULL,
	[PreferLLDestinationCheckToLmax] [bit] NOT NULL,
	[MinimumBPGrossToAccept] [decimal](18, 6) NOT NULL,
	[PreferLmaxDuringHedging] [bit] NOT NULL,
	[MinimumIntegratorPriceLife_milliseconds] [int] NOT NULL,
	[MinimumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MinimumFillSize] [decimal](18, 0) NOT NULL,
	[MaximumSourceBookTiers] [int] NOT NULL,
	[MinimumBasisPointsFromKgtPriceToFastCancel] [decimal](18, 6) NOT NULL,
	[ReferenceLmaxBookOnly] [bit] NOT NULL,
	[BidEnabled] [bit] NOT NULL,
	[AskEnabled] [bit] NOT NULL,
 CONSTRAINT [UNQ__VenueStreamSettings__TradingSystemId] UNIQUE NONCLUSTERED 
(
	[TradingSystemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[VenueStreamSettings] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[VenueStreamSettings_Changes]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VenueStreamSettings_Changes](
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[MaximumSizeToTrade] [decimal](18, 0) NOT NULL,
	[BestPriceImprovementOffsetBasisPoints] [decimal](18, 6) NOT NULL,
	[MaximumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[KGTLLTime_milliseconds] [int] NOT NULL,
	[PreferLLDestinationCheckToLmax] [bit] NOT NULL,
	[MinimumBPGrossToAccept] [decimal](18, 6) NOT NULL,
	[PreferLmaxDuringHedging] [bit] NOT NULL,
	[ValidFromUtc] [datetime2](7) NOT NULL,
	[ValidToUtc] [datetime2](7) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[MinimumIntegratorPriceLife_milliseconds] [int] NOT NULL,
	[MinimumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MinimumFillSize] [decimal](18, 0) NOT NULL,
	[MaximumSourceBookTiers] [int] NOT NULL,
	[MinimumBasisPointsFromKgtPriceToFastCancel] [decimal](18, 6) NOT NULL,
	[ReferenceLmaxBookOnly] [bit] NOT NULL,
	[BidEnabled] [bit] NOT NULL,
	[AskEnabled] [bit] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[VenueStreamSettings_Changes] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[VolumeExternal]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VolumeExternal](
	[VenueId] [tinyint] NOT NULL,
	[TradeDate] [date] NOT NULL,
	[VolumeUsd] [decimal](18, 2) NOT NULL,
	[TradeDateTimeZone] [tinyint] NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[VolumeExternal] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystem_All_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystem_All_Tbl] (	)
RETURNS TABLE 
AS
RETURN 
(
select ts.TradingSystemId,ts.SymbolId,ts.Description,ts.TradingSystemTypeId,ts.TradingSystemGroupId
from TradingSystem ts with(nolock)

union all

select ts_deleted.TradingSystemId,ts_deleted.SymbolId,ts_deleted.Description,ts_deleted.TradingSystemTypeId,ts_deleted.TradingSystemGroupId
from TradingSystem_Deleted ts_deleted with(nolock)
)

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystem_All_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  View [dbo].[Symbol]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Symbol]
AS
SELECT
	s.[Id]
	,[Name]
	,[ShortName]
	,[BaseCurrencyId]
	,[QuoteCurrencyId]
	,[IsRaboTradeable]
	,[Usage]
	,[LastKnownMeanReferencePrice]
	,CASE WHEN [AskMeanPriceUpdated] > [BidMeanPriceUpdated] THEN [AskMeanPriceUpdated] ELSE [BidMeanPriceUpdated] END AS [MeanReferencePriceUpdated]
	,[LastKnownAskMeanPrice]
	,[LastKnownAskMeanPrice] AS LastKnownAskToBMeanPrice
	,[AskMeanPriceUpdated]
	,[LastKnownBidMeanPrice]
	,[LastKnownBidMeanPrice] AS LastKnownBidToBMeanPrice
	,[BidMeanPriceUpdated]
FROM 
	[dbo].[Symbol_Internal] s WITH(NOLOCK)
	INNER JOIN [dbo].[Symbol_Rates] sr WITH(NOLOCK) ON s.Id = sr.Id


GO
ALTER AUTHORIZATION ON [dbo].[Symbol] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystemDealsExecuted_KgtMaker_UtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystemDealsExecuted_KgtMaker_UtcRange_Tbl]
(
@StartUtc datetime2,
@EndUtc datetime2
)
RETURNS TABLE 
AS
RETURN 
(
select
TradingSystemId, 
IntegratorExecutedAmountBasePol'AmountBasePolExecuted', 
CounterpartyRequestedPrice'Price',
IntegratorReceivedExternalOrderUtc'IntegratorReceivedUtc', 
CounterpartyId,
DatePartIntegratorReceivedOrderUtc'DatePartIntegratorReceivedUtc',
SymbolId
from
OrderExternalIncomingRejectable_Daily with (nolock) -- DAILY
where ValueDate is not null -- EXECUTED DEALS ONLY
and IntegratorReceivedExternalOrderUtc between @StartUtc and @EndUtc

union all 

select
TradingSystemId,
IntegratorExecutedAmountBasePol, 
CounterpartyRequestedPrice,
IntegratorReceivedExternalOrderUtc'IntegratorReceivedUtc',
CounterpartyId,
DatePartIntegratorReceivedOrderUtc'DatePartIntegratorReceivedUtc',
SymbolId
from
OrderExternalIncomingRejectable_Archive with (nolock) -- ARCHIVE
where ValueDate is not null -- EXECUTED DEALS ONLY 
and IntegratorReceivedExternalOrderUtc between @StartUtc and @EndUtc
)
GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemDealsExecuted_KgtMaker_UtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystemDealsExecuted_All_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystemDealsExecuted_All_Tbl] (	)
RETURNS TABLE 
AS
RETURN 
(
select a.TradingSystemId,a.AmountBasePolExecuted,a.InternalTransactionIdentity
from TradingSystemDealsExecuted_Archive a with(nolock)

union all

select d.TradingSystemId,d.AmountBasePolExecuted,d.InternalTransactionIdentity from
TradingSystemDealsExecuted_Daily d with(nolock)
)

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemDealsExecuted_All_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystemDealsExecuted_KgtTaker_UtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystemDealsExecuted_KgtTaker_UtcRange_Tbl]
(
@StartUtc datetime2,
@EndUtc datetime2
)
RETURNS TABLE 
AS
RETURN 
(

select 
tbl.TradingSystemId,
tbl.AmountBasePolExecuted,
dee.Price,
dee.IntegratorReceivedExecutionReportUtc'IntegratorReceivedUtc',
dee.CounterpartyId,
dee.DatePartIntegratorReceivedExecutionReportUtc'DatePartIntegratorReceivedUtc',
SymbolId
from 
DealExternalExecuted dee with(nolock)
left join TradingSystemDealsExecuted_All_Tbl() tbl
on tbl.InternalTransactionIdentity=dee.InternalTransactionIdentity

WHERE
IntegratorReceivedExecutionReportUtc between @StartUtc and @EndUtc
)
GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemDealsExecuted_KgtTaker_UtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystemDealsExecuted_KgtMakerTaker_UtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystemDealsExecuted_KgtMakerTaker_UtcRange_Tbl]
(
@StartUtc datetime2,
@EndUtc datetime2
)
RETURNS TABLE 
AS
RETURN 
(
select
TradingSystemId, AmountBasePolExecuted, Price, IntegratorReceivedUtc, CounterpartyId, DatePartIntegratorReceivedUtc, SymbolId
from TradingSystemDealsExecuted_KgtMaker_UtcRange_Tbl(@StartUtc, @EndUtc)

union all

select
TradingSystemId, AmountBasePolExecuted, Price, IntegratorReceivedUtc, CounterpartyId, DatePartIntegratorReceivedUtc, SymbolId
from TradingSystemDealsExecuted_KgtTaker_UtcRange_Tbl(@StartUtc, @EndUtc)
)
GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemDealsExecuted_KgtMakerTaker_UtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystemsPositions_UtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystemsPositions_UtcRange_Tbl]
(
@StartUtc datetime2,
@EndUtc datetime2
)
RETURNS TABLE 

AS

RETURN 
(
	--SELECT 

	--	TradingSystemId,
	--	SUM(AmountBasePolExecuted) AS NopBasePol,
	--	SUM(-AmountBasePolExecuted* [Price]) AS NopTermPol,
	--	SUM(ABS(AmountBasePolExecuted)) AS VolumeInCcy1,
	--	SUM(ABS(AmountBasePolExecuted) * CommissionPricePerMillion / 1000000.00) AS CommissionInCCY1,
	--	COUNT(TradingSystemId) AS DealsNum

	--FROM

	--	Integrator.dbo.TradingSystemDealsExecuted_KgtMakerTaker_UtcRange_Tbl(@StartUtc, @EndUtc) deal
	--	INNER JOIN CommissionsPerCounterparty commisions with(nolock) ON deal.CounterpartyId = commisions.CounterpartyId

	--GROUP BY

	--	TradingSystemId


	--SELECT 

	--	dee.TradingSystemId,
	--	SUM(AmountBasePolExecuted) AS NopBasePol,
	--	SUM(-AmountBasePolExecuted* [Price]) AS NopTermPol,
	--	SUM(ABS(AmountBasePolExecuted)) AS VolumeInCcy1,
	--	SUM(ABS(AmountBasePolExecuted) * (commCpt.CounterpartyCommissionPpm+commPb.PbCommissionPpm)/1000000.0) AS CommissionInCCY1,
	--	COUNT(TradingSystemId) AS DealsNum

	--FROM

	--	Integrator.dbo.TradingSystemDealsExecuted_KgtMakerTaker_UtcRange_Tbl(@StartUtc, @EndUtc) dee

	--	join Integrator.dbo.CommissionsSchedule_CptCommission commCpt with(nolock) on commCpt.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commCpt.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and dee.CounterpartyId=commCpt.CounterpartyId
	--	join Integrator.dbo.CommissionsSchedule_Mapping commPbMap with(nolock) on commPbMap.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commPbMap.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and dee.CounterpartyId=commPbMap.CounterpartyId
	--	join Integrator.dbo.CommissionsSchedule_PBCommission commPb with(nolock) on commPb.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commpb.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and commPbMap.PrimeBrokerId=commPb.PrimeBrokerId
		

	--GROUP BY

	--	TradingSystemId

----------------------------------------------------------------------------
	--SELECT 

	--	dee.TradingSystemId,

	--	sum(AmountBasePolExecuted) AS NopBasePol,
	--	sum(-AmountBasePolExecuted* [Price]) AS NopTermPol,

	--	sum(ABS(AmountBasePolExecuted)) AS VolumeInCcy1,
	--	sum(ABS(AmountBasePolExecuted) * baseCcyEodConversionToUsd.MidPrice) as VolumeInUsd,

	--	sum(ABS(AmountBasePolExecuted) * (commCpt.CounterpartyCommissionPpm+commPb.PbCommissionPpm)/1000000.0) AS CommissionInCCY1,
	--	sum(ABS(AmountBasePolExecuted) * (commCpt.CounterpartyCommissionPpm+commPb.PbCommissionPpm)/1000000.0 * baseCcyEodConversionToUsd.MidPrice) AS CommissionInUsd,

	--	COUNT(TradingSystemId) AS DealsNum


	--FROM

	--	Integrator.dbo.TradingSystemDealsExecuted_KgtMakerTaker_UtcRange_Tbl(@StartUtc, @EndUtc) dee

	--	join Integrator.dbo.CommissionsSchedule_CptCommission commCpt with(nolock) on commCpt.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commCpt.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and dee.CounterpartyId=commCpt.CounterpartyId
	--	join Integrator.dbo.CommissionsSchedule_Mapping commPbMap with(nolock) on commPbMap.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commPbMap.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and dee.CounterpartyId=commPbMap.CounterpartyId
	--	join Integrator.dbo.CommissionsSchedule_PBCommission commPb with(nolock) on commPb.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commpb.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and commPbMap.PrimeBrokerId=commPb.PrimeBrokerId
		
	--	join Integrator.[dbo].[Symbol] symbol  with(nolock) ON dee.SymbolId = symbol.Id
	--	join FXtickDB.dbo.FxPair f with(nolock) on f.FxpCode=symbol.ShortName
	--	join FXtickDB.dbo.Currency baseCcy with(nolock) on baseCcy.CurrencyID=f.BaseCurrencyId
	--	join FXtickDB.dbo.EODUsdConversions baseCcyEodConversionToUsd with(nolock) on baseCcyEodConversionToUsd.Date=dee.DatePartIntegratorReceivedUtc and baseCcyEodConversionToUsd.CurrencyId=baseCcy.CurrencyID


	--GROUP BY 		TradingSystemId
-------------------------------------------------------------------------------------

select 

positions.TradingSystemId,

sum(positions.NopBasePol)'NopBasePol',
sum(positions.NopTermPol)'NopTermPol',

sum(positions.VolumeInCcy1)'VolumeInCcy1',
sum(positions.VolumeInUsd)'VolumeInUsd',

sum(positions.CommissionInCCY1)'CommissionInCCY1',
sum(positions.CommissionInUsd)'CommissionInUsd',

sum(positions.DealsNum)'DealsNum',

sum(positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN eodr.AskPrice ELSE eodr.BidPrice END) * positions.NopBasePol) AS PnlTermPol,
sum((positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN eodr.AskPrice ELSE eodr.BidPrice END) * positions.NopBasePol) * termCcyEodConversionToUsd.MidPrice) AS PnlTermPolInUsd

from

(
	SELECT 

		dee.DatePartIntegratorReceivedUtc,
		dee.TradingSystemId,
		f.FxPairId,
		f.QuoteCurrencyId,

		sum(AmountBasePolExecuted) AS NopBasePol,
		sum(-AmountBasePolExecuted* [Price]) AS NopTermPol,

		sum(ABS(AmountBasePolExecuted)) AS VolumeInCcy1,
		sum(ABS(AmountBasePolExecuted) * baseCcyEodConversionToUsd.MidPrice) as VolumeInUsd,

		sum(ABS(AmountBasePolExecuted) * (commCpt.CounterpartyCommissionPpm+commPb.PbCommissionPpm)/1000000.0) AS CommissionInCCY1,
		sum(ABS(AmountBasePolExecuted) * (commCpt.CounterpartyCommissionPpm+commPb.PbCommissionPpm)/1000000.0 * baseCcyEodConversionToUsd.MidPrice) AS CommissionInUsd,

		COUNT(TradingSystemId) AS DealsNum

	FROM

		Integrator.dbo.TradingSystemDealsExecuted_KgtMakerTaker_UtcRange_Tbl(@StartUtc, @EndUtc) dee

		join Integrator.dbo.CommissionsSchedule_CptCommission commCpt with(nolock) on commCpt.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commCpt.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and dee.CounterpartyId=commCpt.CounterpartyId
		join Integrator.dbo.CommissionsSchedule_Mapping commPbMap with(nolock) on commPbMap.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commPbMap.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and dee.CounterpartyId=commPbMap.CounterpartyId
		join Integrator.dbo.CommissionsSchedule_PBCommission commPb with(nolock) on commPb.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commpb.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and commPbMap.PrimeBrokerId=commPb.PrimeBrokerId
		
		join Integrator.[dbo].[Symbol] symbol  with(nolock) ON dee.SymbolId = symbol.Id
		join FXtickDB.dbo.FxPair f with(nolock) on f.FxpCode=symbol.ShortName
		join FXtickDB.dbo.Currency baseCcy with(nolock) on baseCcy.CurrencyID=f.BaseCurrencyId
		join FXtickDB.dbo.EODUsdConversions baseCcyEodConversionToUsd with(nolock) on baseCcyEodConversionToUsd.Date=dee.DatePartIntegratorReceivedUtc and baseCcyEodConversionToUsd.CurrencyId=baseCcy.CurrencyID

	GROUP BY dee.DatePartIntegratorReceivedUtc,dee.TradingSystemId,f.FxPairId,f.QuoteCurrencyId
) positions

join FXtickDB.dbo.EODRate eodr with(nolock) on eodr.Date=positions.DatePartIntegratorReceivedUtc and eodr.FXPairId=positions.FxPairId
join FXtickDB.dbo.Currency termCcy with(nolock) on termCcy.CurrencyID=positions.QuoteCurrencyId
join FXtickDB.dbo.EODUsdConversions termCcyEodConversionToUsd with(nolock) on termCcyEodConversionToUsd.Date=positions.DatePartIntegratorReceivedUtc and termCcyEodConversionToUsd.CurrencyId=termCcy.CurrencyID


GROUP BY TradingSystemId
having TradingSystemId > -1 -- TODO handle unassigned trading systems ("Unknown"), too
)
GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemsPositions_UtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystemsTypeInfo_UtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystemsTypeInfo_UtcRange_Tbl]
(
@StartUtc datetime2,
@EndUtc datetime2
)
RETURNS TABLE 
AS
RETURN 
(
select 
d.TradingSystemType,
sum(d.VolumeUsd/1000000.0)'VolumeUsdM',
sum(d.PnlGrossUsd)'PnlGrossUsd',
sum(d.CommissionsUsd)'CommissionsUsd',
sum(d.PnlNetUsd)'PnlNetUsd',
sum(d.DealsCount)'DealsCount',
--sum(d.RejectionsCount)'RejectionsCount',

sum(d.PnlGrossUsd)/sum(d.VolumeUsd/1000000.0)'PnlGrossUsdPerMUsd',
sum(d.CommissionsUsd)/sum(d.VolumeUsd/1000000.0)'CommissionsUsdPerMUsd',
sum(d.PnlNetUsd)/sum(d.VolumeUsd/1000000.0)'PnlNetUsdPerMUsd',

sum(d.VolumeUsd/1000000.0)/sum(d.DealsCount)'AvgDealMUsd',
sum(d.PnlGrossUsd)/sum(d.DealsCount)'PnlGrossUsdPerDeal',
sum(d.CommissionsUsd)/sum(d.DealsCount)'CommUsdPerDeal',
sum(d.PnlNetUsd)/sum(d.DealsCount)'PnlNetUsdPerDeal'

from
(
select 
--'N/A' 'tradingSystemsGroup',  -- will be populated when group info is available in SQL
tstc.CounterpartyCode + ' ' + tst.SystemName'TradingSystemType',
pspts.VolumeUsd,
pspts.PnlGrossUsd,
pspts.CommissionsUsd,
pspts.PnlNetUsd,
pspts.DealsNum'DealsCount'
--pspts.RejectionsCount
---1'rejectionsCount',-- ready for upcoming changes

-- TODO: display stats for deals which are not linked to any trading system (Unknown)

from
-------------------

	(SELECT
		systemStats.TradingSystemId,
		systemStats.VolumeInUsd AS VolumeUsd,
		systemStats.DealsNum as DealsNum,
		systemStats.PnlTermPolInUsd AS PnlGrossUsd,
		systemStats.CommissionInUSD AS CommissionsUsd,
		systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD AS PnlNetUsd
		--coalesce(systemStats.RejectionsCount,0)'RejectionsCount'
	FROM
		(
		SELECT
			tradingSystem.TradingSystemId AS TradingSystemId,
			positions.DealsNum as DealsNum,
			positions.VolumeInUsd,
			positions.PnlTermPolInUsd,
			--(positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * positions.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
			positions.CommissionInUsd
			--rPerTsId.RejectionsCount'RejectionsCount'
		FROM
			TradingSystemsPositions_UtcRange_Tbl(@StartUtc, @EndUtc) positions
			left JOIN TradingSystem_All_Tbl() tradingSystem ON positions.TradingSystemId = tradingSystem.TradingSystemId

			--INNER JOIN [dbo].[Symbol] symbol with(nolock) ON tradingSystem.SymbolId = symbol.Id
			--INNER JOIN [dbo].[Currency] termCurrency with(nolock) ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
			--INNER JOIN [dbo].[Currency] baseCurrency with(nolock) ON symbol.BaseCurrencyId = baseCurrency.CurrencyID


			----- rejections:
			--	left join
			--	(select tblrej.TradingSystemId,count(*)'RejectionsCount' from DealExternalRejected rej with(nolock)
			--		join TradingSystemDealsRejected_All_Tbl() tblrej on rej.IntegratorExternalOrderPrivateId=tblrej.IntegratorExternalOrderPrivateId
			--		where rej.IntegratorReceivedExecutionReportUtc between @StartUtc and @EndUtc
			--		group by tblrej.TradingSystemId) rPerTsId on systemsPositions.TradingSystemId=rPerTsId.TradingSystemId
			----- rejections

			) systemStats) pspts
-------------------

left join TradingSystem_All_Tbl() ts on pspts.TradingSystemId=ts.TradingSystemId
left join TradingSystemType tst  with(nolock) on ts.TradingSystemTypeId=tst.TradingSystemTypeId
left join Counterparty tstc  with(nolock) on tst.CounterpartyId=tstc.CounterpartyId) d
group by d.TradingSystemType
)
GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemsTypeInfo_UtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystemId_TradingSystemType_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystemId_TradingSystemType_Tbl] (	)
RETURNS TABLE 
AS
RETURN 
(
select
tsa.TradingSystemId, 
tstn.TradingSystemTypeName

from TradingSystem_All_Tbl() tsa
join 

(select 
tst.TradingSystemTypeId,
CAST(c.CounterpartyCode + ' ' + tst.SystemName AS varchar(100)) 'TradingSystemTypeName' 
from 
TradingSystemType tst with(nolock) 
join Counterparty c with(nolock) on tst.CounterpartyId=c.CounterpartyId) tstn

on tsa.TradingSystemTypeId=tstn.TradingSystemTypeId
)

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemId_TradingSystemType_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  View [dbo].[Currency]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Currency]
AS
SELECT
	c.[CurrencyID]
	,[CurrencyAlphaCode]
	,[CurrencyName]
	,[CurrencyISOCode]
	,[Valid]
	,[UpdatedOn]
	,[LastKnownUsdConversionMultiplier]
	,[UsdConversionMultiplierUpdated]
FROM 
	[dbo].[Currency_Internal] c WITH(NOLOCK)
	INNER JOIN [dbo].[Currency_Rates] cr WITH(NOLOCK) ON c.CurrencyID = cr.CurrencyID

GO
ALTER AUTHORIZATION ON [dbo].[Currency] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystemsInfo_UtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystemsInfo_UtcRange_Tbl]
(
@StartUtc datetime2,
@EndUtc datetime2
)
RETURNS TABLE 
AS
RETURN 
(
	select 

	s.Name'Symbol',
	--'N/A' 'tradingSystemsGroup',  -- will be populated when group info is available in SQL
	tstc.CounterpartyCode + ' ' + tst.SystemName'TradingSystemType',
	pspts.TradingSystemId,
	pspts.NopBasePol'OpenPositionBasePolarized',
	pspts.VolumeUsdM,
	pspts.PnlGrossUsd,
	pspts.CommissionsUsd,
	pspts.PnlNetUsd,
	pspts.PnlGrossUsdPerMUsd,
	pspts.CommissionsUsdPerMUsd,
	pspts.PnlNetUsdPerMUsd,
	pspts.DealsNum'DealsCount',
	---1'rejectionsCount',-- ready for upcoming changes
	pspts.VolumeUsdM/pspts.DealsNum'AvgDealMUsd',
	pspts.PnlGrossUsd/pspts.DealsNum'PnlGrossUsdPerDeal',
	pspts.CommissionsUsd/pspts.DealsNum'CommUsdPerDeal',
	pspts.PnlNetUsd/pspts.DealsNum'PnlNetUsdPerDeal'

	-- TODO: display stats for deals which are not linked to any trading system (Unknown)

	from
	-------------------

		(SELECT
			--systemStats.Symbol AS Symbol,
			systemStats.SymbolId AS SymbolId,
			systemStats.TradingSystemId AS TradingSystemId,
			--systemStats.TradingSystemDescription AS TradingSystemDescription,
			systemStats.NopBasePol AS NopBasePol,
			systemStats.VolumeInUsd/1000000.0 AS VolumeUsdM,
			systemStats.DealsNum as DealsNum,
			systemStats.PnlTermPolInUsd/(systemStats.VolumeInUsd/1000000) AS PnlGrossUsdPerMUsd,
			systemStats.PnlTermPolInUsd AS PnlGrossUsd,
			systemStats.CommissionInUSD/(systemStats.VolumeInUsd/1000000) AS CommissionsUsdPerMUsd,
			systemStats.CommissionInUSD AS CommissionsUsd,
			(systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD)/(systemStats.VolumeInUsd/1000000) AS PnlNetUsdPerMUsd,
			systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD AS PnlNetUsd
		FROM
			(
			SELECT

				--Symbol.Name AS Symbol,
				Symbol.Id AS SymbolId,
				tradingSystem.TradingSystemId AS TradingSystemId,
				--tradingSystem.Description AS TradingSystemDescription,
				systemsPositions.NopBasePol AS NopBasePol,
				systemsPositions.DealsNum as DealsNum,
				systemsPositions.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd,
				--PnL = term position + yield of immediate close of outstanding base position
				--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
				(systemsPositions.NopTermPol + (CASE WHEN systemsPositions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * systemsPositions.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
				systemsPositions.CommissionInCCY1 * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionInUSD

			FROM

				TradingSystemsPositions_UtcRange_Tbl(@StartUtc,@EndUtc) systemsPositions
				left JOIN TradingSystem_All_Tbl() tradingSystem ON systemsPositions.TradingSystemId = tradingSystem.TradingSystemId
				INNER JOIN [dbo].[Symbol] symbol ON tradingSystem.SymbolId = symbol.Id
				INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
				INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID

				) systemStats) pspts

	-------------------

	left join TradingSystem_All_Tbl() ts on pspts.TradingSystemId=ts.TradingSystemId
	left join TradingSystemType tst  with(nolock) on ts.TradingSystemTypeId=tst.TradingSystemTypeId
	left join Counterparty tstc  with(nolock) on tst.CounterpartyId=tstc.CounterpartyId
	left join Symbol s with(nolock) on ts.SymbolId=s.Id
)
GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemsInfo_UtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystemDealsExecuted_KgtMaker_DateUtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystemDealsExecuted_KgtMaker_DateUtcRange_Tbl]
(
@StartDateUtc DATE,
@EndDateUtc DATE
)
RETURNS TABLE 
AS
RETURN 
(
select
TradingSystemId, 
IntegratorExecutedAmountBasePol'AmountBasePolExecuted', 
CounterpartyRequestedPrice'Price',
IntegratorReceivedExternalOrderUtc'IntegratorReceivedUtc', 
CounterpartyId,
DatePartIntegratorReceivedOrderUtc'DatePartIntegratorReceivedUtc',
SymbolId
from
OrderExternalIncomingRejectable_Daily with (nolock) -- DAILY
where ValueDate is not null -- EXECUTED DEALS ONLY
and DatePartIntegratorReceivedOrderUtc between @StartDateUtc and @EndDateUtc

union all 

select
TradingSystemId,
IntegratorExecutedAmountBasePol, 
CounterpartyRequestedPrice,
IntegratorReceivedExternalOrderUtc'IntegratorReceivedUtc',
CounterpartyId,
DatePartIntegratorReceivedOrderUtc'DatePartIntegratorReceivedUtc',
SymbolId
from
OrderExternalIncomingRejectable_Archive with (nolock) -- ARCHIVE
where ValueDate is not null -- EXECUTED DEALS ONLY 
and DatePartIntegratorReceivedOrderUtc between @StartDateUtc and @EndDateUtc
)
GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemDealsExecuted_KgtMaker_DateUtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystemDealsExecuted_KgtTaker_DateUtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystemDealsExecuted_KgtTaker_DateUtcRange_Tbl]
(
@StartDateUtc DATE,
@EndDateUtc DATE
)
RETURNS TABLE 
AS
RETURN 
(

select 
tbl.TradingSystemId,
tbl.AmountBasePolExecuted,
dee.Price,
dee.IntegratorReceivedExecutionReportUtc'IntegratorReceivedUtc',
dee.CounterpartyId,
dee.DatePartIntegratorReceivedExecutionReportUtc'DatePartIntegratorReceivedUtc',
SymbolId
from 
DealExternalExecuted dee with(nolock)
left join TradingSystemDealsExecuted_All_Tbl() tbl
on tbl.InternalTransactionIdentity=dee.InternalTransactionIdentity

WHERE
DatePartIntegratorReceivedExecutionReportUtc between @StartDateUtc and @EndDateUtc
)
GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemDealsExecuted_KgtTaker_DateUtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystemDealsExecuted_KgtMakerTaker_DateUtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystemDealsExecuted_KgtMakerTaker_DateUtcRange_Tbl]
(
@StartDateUtc DATE,
@EndDateUtc DATE
)
RETURNS TABLE 
AS
RETURN 
(
select
TradingSystemId, AmountBasePolExecuted, Price, IntegratorReceivedUtc, CounterpartyId, DatePartIntegratorReceivedUtc, SymbolId
from TradingSystemDealsExecuted_KgtMaker_DateUtcRange_Tbl(@StartDateUtc, @EndDateUtc)

union all

select
TradingSystemId, AmountBasePolExecuted, Price, IntegratorReceivedUtc, CounterpartyId, DatePartIntegratorReceivedUtc, SymbolId
from TradingSystemDealsExecuted_KgtTaker_DateUtcRange_Tbl(@StartDateUtc, @EndDateUtc)
)
GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemDealsExecuted_KgtMakerTaker_DateUtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[SingleTradingSystemInfo_DateUtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[SingleTradingSystemInfo_DateUtcRange_Tbl] 
(
	@StartDateUtc DATE,
	@EndDateUtc DATE,
	@TradingSystemType varchar(100)
)
RETURNS TABLE 
AS
RETURN 
(
		SELECT

			deal.DatePartIntegratorReceivedUtc'TradeDate',
			deal.SymbolId AS SymbolId,
			count(*)'dealsNum',
			SUM(deal.AmountBasePolExecuted) AS NopBasePol,
			SUM(-deal.AmountBasePolExecuted* deal.Price) AS NopTermPol,
			SUM(ABS(deal.AmountBasePolExecuted)) AS VolumeInCcy1

		FROM
		 
			TradingSystemDealsExecuted_KgtMakerTaker_DateUtcRange_Tbl(@StartDateUtc, @EndDateUtc) deal

		WHERE

			deal.TradingSystemId in (select TradingSystemId from dbo.TradingSystemId_TradingSystemType_Tbl() t where t.TradingSystemTypeName=@TradingSystemType)

		GROUP BY

			deal.DatePartIntegratorReceivedUtc,
			deal.SymbolId
)
GO
ALTER AUTHORIZATION ON [dbo].[SingleTradingSystemInfo_DateUtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[SingleTradingSystemInfo_Commissions_DateUtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[SingleTradingSystemInfo_Commissions_DateUtcRange_Tbl]
(
	@StartDateUtc DATE,
	@EndDateUtc DATE,
	@TradingSystemType varchar(100)
)

RETURNS TABLE 

AS

RETURN 
(
	--select
	--dee.DatePartIntegratorReceivedUtc'TradeDate',
	--s.Name,

	--sum(baseCcy.LastKnownUsdConversionMultiplier*(abs(dee.AmountBasePolExecuted*com.CommissionPricePerMillion/1000000)))'CommissionsUsdSum'

	--from TradingSystemDealsExecuted_KgtMakerTaker_DateUtcRange_Tbl(@StartDateUtc, @EndDateUtc) dee 
	--join Symbol s with(nolock) on dee.Symbolid=s.Id 
	--join Counterparty c with(nolock) on dee.CounterpartyId=c.CounterpartyId
	--join CommissionsPerCounterparty com  with(nolock) on c.CounterpartyId=com.CounterpartyId
	--join Currency baseCcy  with(nolock) on s.BaseCurrencyId=baseCcy.CurrencyID

	--and dee.TradingSystemId in (select TradingSystemId from dbo.TradingSystemId_TradingSystemType_Tbl() t where t.TradingSystemTypeName=@TradingSystemType)

	--group by dee.DatePartIntegratorReceivedUtc,s.name

SELECT

		dee.DatePartIntegratorReceivedUtc'TradeDate',
		s.Name'Symbol',
		sum(eodBaseConversionToUsd.MidPrice*(abs(AmountBasePolExecuted*(commCpt.CounterpartyCommissionPpm+commPb.PbCommissionPpm)/1000000.0)))'CommissionsUsdSum',
		sum(eodBaseConversionToUsd.MidPrice*(abs(AmountBasePolExecuted*commPb.PbCommissionPpm/1000000.0)))'CommissionsUsdPb',
		sum(eodBaseConversionToUsd.MidPrice*(abs(AmountBasePolExecuted*commCpt.CounterpartyCommissionPpm/1000000.0)))'CommissionsUsdCpt'

FROM

	Integrator.dbo.TradingSystemDealsExecuted_KgtMakerTaker_DateUtcRange_Tbl(@StartDateUtc, @EndDateUtc) dee 

	join Integrator.dbo.CommissionsSchedule_CptCommission commCpt with(nolock) on commCpt.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commCpt.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and dee.CounterpartyId=commCpt.CounterpartyId
	join Integrator.dbo.CommissionsSchedule_Mapping commPbMap with(nolock) on commPbMap.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commPbMap.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and dee.CounterpartyId=commPbMap.CounterpartyId
	join Integrator.dbo.CommissionsSchedule_PBCommission commPb with(nolock) on commPb.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commpb.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and commPbMap.PrimeBrokerId=commPb.PrimeBrokerId

	join Integrator.dbo.Symbol s with(nolock) on dee.Symbolid=s.Id
	join FXtickDB.dbo.FxPair f with(nolock) on s.ShortName=f.FxpCode
	join FXtickDB.dbo.Currency baseCurr with(nolock) on baseCurr.CurrencyID=f.BaseCurrencyId
	join FXtickDB.dbo.EODUsdConversions eodBaseConversionToUsd with(nolock) on eodBaseConversionToUsd.Date=dee.DatePartIntegratorReceivedUtc and eodBaseConversionToUsd.CurrencyId=baseCurr.CurrencyID

WHERE

	dee.TradingSystemId in (select TradingSystemId from dbo.TradingSystemId_TradingSystemType_Tbl() t where t.TradingSystemTypeName=@TradingSystemType)

GROUP BY

	dee.DatePartIntegratorReceivedUtc,s.name
)
GO
ALTER AUTHORIZATION ON [dbo].[SingleTradingSystemInfo_Commissions_DateUtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[DealsExecuted_DateUtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[DealsExecuted_DateUtcRange_Tbl] 
(
	@StartDateUtc DATE,
	@EndDateUtc DATE
)
RETURNS TABLE 
AS
RETURN 
(
select SymbolId, AmountBasePolExecuted, DatePartIntegratorReceivedExecutionReportUtc'DatePartIntegratorReceivedUtc', CounterpartyId, Price, OrderSentToExecutionReportReceivedLatency'Latency' from DealExternalExecuted with(nolock)
where DatePartIntegratorReceivedExecutionReportUtc between @StartDateUtc and @EndDateUtc

union all

select SymbolId, IntegratorExecutedAmountBasePol, DatePartIntegratorReceivedOrderUtc, CounterpartyId, CounterpartyRequestedPrice, OrderReceivedToExecutionReportSentLatency'Latency' from OrderExternalIncomingRejectable_Daily with (nolock) where ValueDate is not null
and DatePartIntegratorReceivedOrderUtc between @StartDateUtc and @EndDateUtc

union all

select SymbolId, IntegratorExecutedAmountBasePol, DatePartIntegratorReceivedOrderUtc, CounterpartyId, CounterpartyRequestedPrice, OrderReceivedToExecutionReportSentLatency'Latency' from OrderExternalIncomingRejectable_Archive with (nolock) where ValueDate is not null
and DatePartIntegratorReceivedOrderUtc between @StartDateUtc and @EndDateUtc
)
GO
ALTER AUTHORIZATION ON [dbo].[DealsExecuted_DateUtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[CommissionsByTradeDateAndSymbol_DateUtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[CommissionsByTradeDateAndSymbol_DateUtcRange_Tbl] 
(
	@StartDateUtc DATE,
	@EndDateUtc DATE
)
RETURNS TABLE 
AS
RETURN 
(

--SELECT

--		dee.DatePartIntegratorReceivedUtc'TradeDate',
--		s.Name'Symbol',
--		sum(baseCcy.LastKnownUsdConversionMultiplier*(abs(AmountBasePolExecuted*com.CommissionPricePerMillion/1000000)))'CommissionsUsdSum'

--FROM
--		Integrator.dbo.DealsExecuted_DateUtcRange_Tbl(@StartDateUtc, @EndDateUtc) dee
--		join Integrator.dbo.Symbol s with(nolock) on dee.Symbolid=s.Id 
--		join Integrator.dbo.Counterparty c with(nolock) on dee.CounterpartyId=c.CounterpartyId
--		join Integrator.dbo.CommissionsPerCounterparty com  with(nolock) on c.CounterpartyId=com.CounterpartyId
--		join Integrator.dbo.Currency baseCcy  with(nolock) on s.BaseCurrencyId=baseCcy.CurrencyID

--GROUP BY

--		dee.DatePartIntegratorReceivedUtc,s.name

SELECT

		dee.DatePartIntegratorReceivedUtc'TradeDate',
		s.Name'Symbol',
		sum(eodBaseConversionToUsd.MidPrice*(abs(AmountBasePolExecuted*(commCpt.CounterpartyCommissionPpm+commPb.PbCommissionPpm)/1000000.0)))'CommissionsUsdSum',
		sum(eodBaseConversionToUsd.MidPrice*(abs(AmountBasePolExecuted*commPb.PbCommissionPpm/1000000.0)))'CommissionsUsdPb',
		sum(eodBaseConversionToUsd.MidPrice*(abs(AmountBasePolExecuted*commCpt.CounterpartyCommissionPpm/1000000.0)))'CommissionsUsdCpt'

FROM

		Integrator.dbo.DealsExecuted_DateUtcRange_Tbl(@StartDateUtc, @EndDateUtc) dee

		join Integrator.dbo.CommissionsSchedule_CptCommission commCpt with(nolock) on commCpt.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commCpt.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and dee.CounterpartyId=commCpt.CounterpartyId
		join Integrator.dbo.CommissionsSchedule_Mapping commPbMap with(nolock) on commPbMap.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commPbMap.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and dee.CounterpartyId=commPbMap.CounterpartyId
		join Integrator.dbo.CommissionsSchedule_PBCommission commPb with(nolock) on commPb.ValidFromUtc <= dee.DatePartIntegratorReceivedUtc and commpb.ValidToUtc >= dee.DatePartIntegratorReceivedUtc and commPbMap.PrimeBrokerId=commPb.PrimeBrokerId

		join Integrator.dbo.Symbol s with(nolock) on dee.Symbolid=s.Id
		join FXtickDB.dbo.FxPair f with(nolock) on s.ShortName=f.FxpCode
		join FXtickDB.dbo.Currency baseCurr with(nolock) on baseCurr.CurrencyID=f.BaseCurrencyId
		join FXtickDB.dbo.EODUsdConversions eodBaseConversionToUsd with(nolock) on eodBaseConversionToUsd.Date=dee.DatePartIntegratorReceivedUtc and eodBaseConversionToUsd.CurrencyId=baseCurr.CurrencyID

GROUP BY	
		
		dee.DatePartIntegratorReceivedUtc,s.name
)
GO
ALTER AUTHORIZATION ON [dbo].[CommissionsByTradeDateAndSymbol_DateUtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[PositionsByTradeDateAndSymbol_DateUtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[PositionsByTradeDateAndSymbol_DateUtcRange_Tbl] 
(
	@StartDateUtc DATE,
	@EndDateUtc DATE
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT

		deal.DatePartIntegratorReceivedUtc'TradeDate',
		deal.SymbolId AS SymbolId,
		count(*)'DealsNum',
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1

	FROM 

		Integrator.dbo.DealsExecuted_DateUtcRange_Tbl(@StartDateUtc, @EndDateUtc) deal

	GROUP BY

		deal.DatePartIntegratorReceivedUtc,
		deal.SymbolId
)
GO
ALTER AUTHORIZATION ON [dbo].[PositionsByTradeDateAndSymbol_DateUtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[PositionsPnlVolumeByTradeDateAndSymbol_DateUtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[PositionsPnlVolumeByTradeDateAndSymbol_DateUtcRange_Tbl] 
(
	@StartDateUtc DATE,
	@EndDateUtc DATE
)
RETURNS TABLE 
AS
RETURN 
(
		--SELECT

		--	positions.TradeDate,
		--	Symbol.Name AS Symbol,
		--	positions.NopBasePol,
		--	positions.NopTermPol,
		--	positions.dealsNum,
		--	positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * positions.NopBasePol AS PnlTermPol,
		--	(positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * positions.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
		--	positions.VolumeInCcy1 AS VolumeInCcy1,
		--	positions.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd

		--FROM

		--	Integrator.dbo.PositionsByTradeDateAndSymbol_DateUtcRange_Tbl(@StartDateUtc, @EndDateUtc) positions
		--	INNER JOIN Integrator.[dbo].[Symbol] symbol  with(nolock) ON positions.SymbolId = symbol.Id
		--	INNER JOIN Integrator.[dbo].[Currency] termCurrency with(nolock) ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		--	INNER JOIN Integrator.[dbo].[Currency] baseCurrency with(nolock) ON symbol.BaseCurrencyId = baseCurrency.CurrencyID

	SELECT
		positions.TradeDate,
			Symbol.Name AS Symbol,
			positions.NopBasePol,
			positions.NopTermPol,
			positions.dealsNum,

			positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN eodr.AskPrice ELSE eodr.BidPrice END) * positions.NopBasePol AS PnlTermPol,
			(positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN eodr.AskPrice ELSE eodr.BidPrice END) * positions.NopBasePol) * termCcyEodConversionToUsd.MidPrice AS PnlTermPolInUsd,

			positions.VolumeInCcy1 AS VolumeInCcy1,
			positions.VolumeInCcy1 * baseCcyEodConversionToUsd.MidPrice AS VolumeInUsd

		FROM

			Integrator.dbo.PositionsByTradeDateAndSymbol_DateUtcRange_Tbl(@StartDateUtc, @EndDateUtc) positions

			inner join Integrator.[dbo].[Symbol] symbol  with(nolock) ON positions.SymbolId = symbol.Id
			inner join FXtickDB.dbo.FxPair f with(nolock) on f.FxpCode=symbol.ShortName
			inner join FXtickDB.dbo.Currency baseCcy with(nolock) on baseCcy.CurrencyID=f.BaseCurrencyId
			inner join FXtickDB.dbo.Currency termCcy with(nolock) on termCcy.CurrencyID=f.QuoteCurrencyId
			inner join FXtickDB.dbo.EODRate eodr with(nolock) on eodr.Date=positions.TradeDate and eodr.FXPairId=f.FxPairId
			inner join FXtickDB.dbo.EODUsdConversions baseCcyEodConversionToUsd with(nolock) on baseCcyEodConversionToUsd.Date=positions.TradeDate and baseCcyEodConversionToUsd.CurrencyId=baseCcy.CurrencyID
			inner join FXtickDB.dbo.EODUsdConversions termCcyEodConversionToUsd with(nolock) on termCcyEodConversionToUsd.Date=positions.TradeDate and termCcyEodConversionToUsd.CurrencyId=termCcy.CurrencyID
)
GO
ALTER AUTHORIZATION ON [dbo].[PositionsPnlVolumeByTradeDateAndSymbol_DateUtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[SingleTradingSystemInfo_PositionsPnlVolumeByTradeDateAndSymbol_DateUtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[SingleTradingSystemInfo_PositionsPnlVolumeByTradeDateAndSymbol_DateUtcRange_Tbl] 
(
	@StartDateUtc DATE,
	@EndDateUtc DATE,
	@TradingSystemType varchar(100)
)
RETURNS TABLE 
AS
RETURN 
(

		--SELECT

		--positions.TradeDate,
		--Symbol.Name AS Symbol,
		--positions.NopBasePol,
		--positions.NopTermPol,
		--positions.dealsNum,
		--positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * positions.NopBasePol AS PnlTermPol,
		--(positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * positions.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
		--positions.VolumeInCcy1 AS VolumeInCcy1,
		--positions.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd

		--FROM

		--Integrator.dbo.SingleTradingSystemInfo_DateUtcRange_Tbl(@StartDateUtc, @EndDateUtc, @TradingSystemType) positions

		--INNER JOIN [dbo].[Symbol] symbol  with(nolock) ON positions.SymbolId = symbol.Id
		--INNER JOIN [dbo].[Currency] termCurrency with(nolock) ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		--INNER JOIN [dbo].[Currency] baseCurrency with(nolock) ON symbol.BaseCurrencyId = baseCurrency.CurrencyID

		SELECT

		positions.TradeDate,
			Symbol.Name AS Symbol,
			positions.NopBasePol,
			positions.NopTermPol,
			positions.dealsNum,
			positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN eodr.AskPrice ELSE eodr.BidPrice END) * positions.NopBasePol AS PnlTermPol,
			(positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN eodr.AskPrice ELSE eodr.BidPrice END) * positions.NopBasePol) * termCcyEodConversionToUsd.MidPrice AS PnlTermPolInUsd,
			positions.VolumeInCcy1 AS VolumeInCcy1,
			positions.VolumeInCcy1 * baseCcyEodConversionToUsd.MidPrice AS VolumeInUsd

		FROM

		Integrator.dbo.SingleTradingSystemInfo_DateUtcRange_Tbl(@StartDateUtc, @EndDateUtc, @TradingSystemType) positions

		inner join Integrator.[dbo].[Symbol] symbol  with(nolock) ON positions.SymbolId = symbol.Id
		inner join FXtickDB.dbo.FxPair f with(nolock) on f.FxpCode=symbol.ShortName
		inner join FXtickDB.dbo.Currency baseCcy with(nolock) on baseCcy.CurrencyID=f.BaseCurrencyId
		inner join FXtickDB.dbo.Currency termCcy with(nolock) on termCcy.CurrencyID=f.QuoteCurrencyId
		inner join FXtickDB.dbo.EODRate eodr with(nolock) on eodr.Date=positions.TradeDate and eodr.FXPairId=f.FxPairId
		inner join FXtickDB.dbo.EODUsdConversions baseCcyEodConversionToUsd with(nolock) on baseCcyEodConversionToUsd.Date=positions.TradeDate and baseCcyEodConversionToUsd.CurrencyId=baseCcy.CurrencyID
		inner join FXtickDB.dbo.EODUsdConversions termCcyEodConversionToUsd with(nolock) on termCcyEodConversionToUsd.Date=positions.TradeDate and termCcyEodConversionToUsd.CurrencyId=termCcy.CurrencyID
)
GO
ALTER AUTHORIZATION ON [dbo].[SingleTradingSystemInfo_PositionsPnlVolumeByTradeDateAndSymbol_DateUtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[DealsExecuted_All_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[DealsExecuted_All_Tbl] (	)
RETURNS TABLE 
AS
RETURN 
(
select SymbolId, AmountBasePolExecuted, DatePartIntegratorReceivedExecutionReportUtc'DatePartIntegratorReceivedUtc', CounterpartyId, Price, OrderSentToExecutionReportReceivedLatency'Latency' from DealExternalExecuted with(nolock)
union all
select SymbolId, IntegratorExecutedAmountBasePol, DatePartIntegratorReceivedOrderUtc, CounterpartyId, CounterpartyRequestedPrice, OrderReceivedToExecutionReportSentLatency'Latency' from OrderExternalIncomingRejectable_Daily with (nolock) where ValueDate is not null
union all
select SymbolId, IntegratorExecutedAmountBasePol, DatePartIntegratorReceivedOrderUtc, CounterpartyId, CounterpartyRequestedPrice, OrderReceivedToExecutionReportSentLatency'Latency' from OrderExternalIncomingRejectable_Archive with (nolock) where ValueDate is not null
)

GO
ALTER AUTHORIZATION ON [dbo].[DealsExecuted_All_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[KGTLLDealsAndRejections_Tbl_UtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[KGTLLDealsAndRejections_Tbl_UtcRange_Tbl]
(
@StartUtc datetime2,
@EndUtc datetime2,
@SymbolId tinyint
)
RETURNS TABLE 
AS
RETURN 
(

SELECT 

IntegratorReceivedExternalOrderUtc,
CounterpartySentExternalOrderUtc,
IntegratorSentExecutionReportUtc,
CounterpartyClientOrderId,
TradingSystemId,
SourceIntegratorPriceIdentity,
CounterpartyId,
IntegratorDealDirectionId,
CounterpartyRequestedPrice,
CounterpartyRequestedAmountBasePol,
IntegratorExecutionId,
ValueDate,
OrderReceivedToExecutionReportSentLatency,
IntegratorExecutedAmountBasePol,
IntegratorRejectedAmountBasePol,
IntegratorLatencyNanoseconds,
DatePartIntegratorReceivedOrderUtc,
CounterpartyClientId,
IntegratorRejectionReasonId
--RecordId

FROM 

Integrator.dbo.OrderExternalIncomingRejectable_Archive with(nolock)

WHERE

IntegratorReceivedExternalOrderUtc between @StartUtc AND @EndUtc
AND SymbolId = @SymbolId

UNION ALL

SELECT 

IntegratorReceivedExternalOrderUtc,
CounterpartySentExternalOrderUtc,
IntegratorSentExecutionReportUtc,
CounterpartyClientOrderId,
TradingSystemId,
SourceIntegratorPriceIdentity,
CounterpartyId,
IntegratorDealDirectionId,
CounterpartyRequestedPrice,
CounterpartyRequestedAmountBasePol,
IntegratorExecutionId,
ValueDate,
OrderReceivedToExecutionReportSentLatency,
IntegratorExecutedAmountBasePol,
IntegratorRejectedAmountBasePol,
IntegratorLatencyNanoseconds,
DatePartIntegratorReceivedOrderUtc,
CounterpartyClientId,
IntegratorRejectionReasonId
--RecordId

FROM

Integrator.dbo.OrderExternalIncomingRejectable_Daily with(nolock)

WHERE

IntegratorReceivedExternalOrderUtc between @StartUtc AND @EndUtc
AND SymbolId = @SymbolId
)
GO
ALTER AUTHORIZATION ON [dbo].[KGTLLDealsAndRejections_Tbl_UtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[Rejections_DateUtcRange_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Rejections_DateUtcRange_Tbl] 
(
	@StartDateUtc DATE,
	@EndDateUtc DATE
)
RETURNS TABLE 
AS
RETURN 
(
select SymbolId, AmountBasePolRejected, DatePartIntegratorReceivedExecutionReportUtc'DatePartIntegratorReceivedUtc', CounterpartyId, RejectedPrice, OrderSentToExecutionReportReceivedLatency'Latency' from DealExternalRejected with(nolock)
where DatePartIntegratorReceivedExecutionReportUtc between @StartDateUtc and @EndDateUtc

union all

select SymbolId, IntegratorRejectedAmountBasePol, DatePartIntegratorReceivedOrderUtc, CounterpartyId, CounterpartyRequestedPrice, OrderReceivedToExecutionReportSentLatency'Latency' from OrderExternalIncomingRejectable_Daily with (nolock) where ValueDate is null
and DatePartIntegratorReceivedOrderUtc between @StartDateUtc and @EndDateUtc

union all

select SymbolId, IntegratorRejectedAmountBasePol, DatePartIntegratorReceivedOrderUtc, CounterpartyId, CounterpartyRequestedPrice, OrderReceivedToExecutionReportSentLatency'Latency' from OrderExternalIncomingRejectable_Archive with (nolock) where ValueDate is null
and DatePartIntegratorReceivedOrderUtc between @StartDateUtc and @EndDateUtc
)

GO
ALTER AUTHORIZATION ON [dbo].[Rejections_DateUtcRange_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystemDealsRejected_All_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystemDealsRejected_All_Tbl] (	)
RETURNS TABLE 
AS
RETURN 
(
select a.TradingSystemId,a.AmountBasePolRejected,a.IntegratorExternalOrderPrivateId
from TradingSystemDealsRejected_Archive a with(nolock)

union all

select d.TradingSystemId,d.AmountBasePolRejected,d.IntegratorExternalOrderPrivateId from
TradingSystemDealsRejected_Daily d with(nolock)
)
GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemDealsRejected_All_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[TradingSystemsGroup_All_Tbl]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TradingSystemsGroup_All_Tbl] (	)
RETURNS TABLE 
AS
RETURN 
(
select tsGroup.TradingSystemGroupId,tsGroup.TradingSystemGroupName 
from TradingSystemGroup tsGroup with(nolock)

union all

select deleted.TradingSystemGroupId,deleted.TradingSystemGroupName 
from TradingSystemGroup_Deleted deleted with(nolock)
)

GO
ALTER AUTHORIZATION ON [dbo].[TradingSystemsGroup_All_Tbl] TO  SCHEMA OWNER 
GO
/****** Object:  View [dbo].[DealExternalExecutedTakersAndMakers_DailyView]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
CREATE VIEW [dbo].[DealExternalExecutedTakersAndMakers_DailyView]
AS
SELECT
	[CounterpartyId]
	,[SymbolId]
	,[AmountBasePolExecuted]
	,[Price]
	,[IntegratorLatencyNanoseconds]
	,[CounterpartyLatencyNanoseconds]
	,[DatePartIntegratorReceivedExecutionReportUtc]
	,[IntegratorReceivedExecutionReportUtc]
FROM 
	[dbo].[DealExternalExecuted_DailyView] WITH(NOEXPAND, NOLOCK)

UNION ALL

SELECT 
	[CounterpartyId]
	,[SymbolId]
	,[IntegratorExecutedAmountBasePol] AS [AmountBasePolExecuted]
	,[CounterpartyRequestedPrice] AS [Price]
	,[IntegratorLatencyNanoseconds]
	,NULL
	,[DatePartIntegratorReceivedOrderUtc] AS [DatePartIntegratorReceivedExecutionReportUtc]
	,[IntegratorReceivedExternalOrderUtc]
FROM 
	[dbo].[OrderExternalIncomingRejectable_Daily] WITH(NOLOCK)
WHERE
	[IntegratorExecutedAmountBasePol] != 0

GO
ALTER AUTHORIZATION ON [dbo].[DealExternalExecutedTakersAndMakers_DailyView] TO  SCHEMA OWNER 
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_AggregatedExternalExecutedTicket_DatePartIntegratorReceivedTimeUtc]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_AggregatedExternalExecutedTicket_DatePartIntegratorReceivedTimeUtc] ON [dbo].[AggregatedExternalExecutedTicket]
(
	[DatePartIntegratorReceivedTimeUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CertificateFile_FileName]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [CertificateFile_FileName] ON [dbo].[CertificateFile]
(
	[FileName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CommissionsPerCounterparty_CounterpartyId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CommissionsPerCounterparty_CounterpartyId] ON [dbo].[CommissionsPerCounterparty]
(
	[CounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CptCommision_CptStartDate]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_CptCommision_CptStartDate] ON [dbo].[CommissionsSchedule_CptCommission]
(
	[CounterpartyId] ASC,
	[ValidFromUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CommisionMapping_CptStartDate]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_CommisionMapping_CptStartDate] ON [dbo].[CommissionsSchedule_Mapping]
(
	[CounterpartyId] ASC,
	[ValidFromUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_PBCommision_PbStartDate]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_PBCommision_PbStartDate] ON [dbo].[CommissionsSchedule_PBCommission]
(
	[PrimeBrokerId] ASC,
	[ValidFromUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PK_Counterparty]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [PK_Counterparty] ON [dbo].[Counterparty]
(
	[CounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DealExternalExecuted_IntegratorReceivedExecutionReportUtc]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_DealExternalExecuted_IntegratorReceivedExecutionReportUtc] ON [dbo].[DealExternalExecuted]
(
	[IntegratorReceivedExecutionReportUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_DealExternalExecutedTicket_DatePartIntegratorSentOrReceivedTimeUtc]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_DealExternalExecutedTicket_DatePartIntegratorSentOrReceivedTimeUtc] ON [dbo].[DealExternalExecutedTicket]
(
	[DatePartIntegratorSentOrReceivedTimeUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DealExternalExecutedTicketTypeId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_DealExternalExecutedTicketTypeId] ON [dbo].[DealExternalExecutedTicketType]
(
	[DealExternalExecutedTicketTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DealExternalRejected_IntegratorReceivedExecutionReportUtc]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_DealExternalRejected_IntegratorReceivedExecutionReportUtc] ON [dbo].[DealExternalRejected]
(
	[IntegratorReceivedExecutionReportUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ExecutionTimeStampUtc_SymbolId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_ExecutionTimeStampUtc_SymbolId] ON [dbo].[ExecutedExternalDeals]
(
	[ExecutionTimeStampUtc] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ExecutionReportMessage_IntegratorReceivedExecutionReportUtc]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_ExecutionReportMessage_IntegratorReceivedExecutionReportUtc] ON [dbo].[ExecutionReportMessage]
(
	[IntegratorReceivedExecutionReportUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [LargeSlowDataFileGroup]
GO
/****** Object:  Index [PK_ExecutionReportStatus]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [PK_ExecutionReportStatus] ON [dbo].[ExecutionReportStatus]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FlowSideId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_FlowSideId] ON [dbo].[FlowSide]
(
	[FlowSideId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IntegratorEndpoint_Id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IntegratorEndpoint_Id] ON [dbo].[IntegratorEndpoint]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IntegratorEnvironment_Name]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IntegratorEnvironment_Name] ON [dbo].[IntegratorEnvironment]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IntegratorEnvironmentType_Id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IntegratorEnvironmentType_Id] ON [dbo].[IntegratorEnvironmentType]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IntegratorInstance_Name]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IntegratorInstance_Name] ON [dbo].[IntegratorInstance]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IntegratorProcessState_Id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IntegratorProcessState_Id] ON [dbo].[IntegratorProcessState]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IntegratorProcessType_Id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IntegratorProcessType_Id] ON [dbo].[IntegratorProcessType]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_IntegratorRejectionReason_Id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_IntegratorRejectionReason_Id] ON [dbo].[IntegratorRejectionReason]
(
	[IntegratorRejectionReasonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderExternal_IntegratorSentExternalOrderUtc]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_OrderExternal_IntegratorSentExternalOrderUtc] ON [dbo].[OrderExternal]
(
	[IntegratorSentExternalOrderUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [LargeSlowDataFileGroup]
GO
/****** Object:  Index [IX_OrderExternalCancelRequest_IntegratorSentCancelRequestUtc]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_OrderExternalCancelRequest_IntegratorSentCancelRequestUtc] ON [dbo].[OrderExternalCancelRequest]
(
	[IntegratorSentCancelRequestUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [LargeSlowDataFileGroup]
GO
/****** Object:  Index [IX_OrderExternalCancelResult_CounterpartySentResultUtc]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_OrderExternalCancelResult_CounterpartySentResultUtc] ON [dbo].[OrderExternalCancelResult]
(
	[CounterpartySentResultUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [LargeSlowDataFileGroup]
GO
/****** Object:  Index [IX_OrderExternalCancelTypeId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_OrderExternalCancelTypeId] ON [dbo].[OrderExternalCancelType]
(
	[OrderExternalCancelTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderExternalIgnored_IntegratorSentExternalOrderUtc]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_OrderExternalIgnored_IntegratorSentExternalOrderUtc] ON [dbo].[OrderExternalIgnored]
(
	[IntegratorSentExternalOrderUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderExternalIncomingRejectable_Archive_IntegratorReceivedExternalOrderUtc]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_OrderExternalIncomingRejectable_Archive_IntegratorReceivedExternalOrderUtc] ON [dbo].[OrderExternalIncomingRejectable_Archive]
(
	[IntegratorReceivedExternalOrderUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [LargeSlowDataFileGroup]
GO
/****** Object:  Index [IX_OrderExternalIncomingRejectable_Daily_IntegratorReceivedExternalOrderUtc]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_OrderExternalIncomingRejectable_Daily_IntegratorReceivedExternalOrderUtc] ON [dbo].[OrderExternalIncomingRejectable_Daily]
(
	[IntegratorReceivedExternalOrderUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderExternalIncomingTimeoutsFromCounterparty_IntegratorReceivedTimeoutUtc]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_OrderExternalIncomingTimeoutsFromCounterparty_IntegratorReceivedTimeoutUtc] ON [dbo].[OrderExternalIncomingTimeoutsFromCounterparty]
(
	[IntegratorReceivedTimeoutUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderTimeInForceId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_OrderTimeInForceId] ON [dbo].[OrderTimeInForce]
(
	[OrderTimeInForceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderType]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_OrderType] ON [dbo].[OrderType]
(
	[OrderTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_PegType]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_PegType] ON [dbo].[PegType]
(
	[PegTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_PersistedStatisticsPerTradingGroupsDaily_GroupAndRank]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_PersistedStatisticsPerTradingGroupsDaily_GroupAndRank] ON [dbo].[PersistedStatisticsPerTradingGroupsDaily]
(
	[TradingSystemGroupId] ASC,
	[Rank] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Date]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_Date] ON [dbo].[PnlByDate]
(
	[DateUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_PriceDelaySettings_CounterpartyId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_PriceDelaySettings_CounterpartyId] ON [dbo].[PriceDelaySettings]
(
	[CounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_PriceDelayStats_CounterpartyId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_PriceDelayStats_CounterpartyId] ON [dbo].[PriceDelayStats]
(
	[CounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PK_PrimeBroker]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [PK_PrimeBroker] ON [dbo].[PrimeBroker]
(
	[PrimeBrokerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_RejectionDirection]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_RejectionDirection] ON [dbo].[RejectionDirection]
(
	[RejectionDirectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [Settings_Id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [Settings_Id] ON [dbo].[Settings]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [SettingsFIX_Id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [SettingsFIX_Id] ON [dbo].[Settings_FIX]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SettlementDate_SymbolId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_SettlementDate_SymbolId] ON [dbo].[SettlementDate]
(
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_StpCounterpartyId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_StpCounterpartyId] ON [dbo].[StpCounterparty]
(
	[StpCounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SystemsTradingControlSchedule_DailyWeeklyTime]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_SystemsTradingControlSchedule_DailyWeeklyTime] ON [dbo].[SystemsTradingControlSchedule]
(
	[IsDaily] ASC,
	[IsWeekly] ASC,
	[ScheduleTimeZoneSpecific] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [SystemsTradingControlScheduleAction_Id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [SystemsTradingControlScheduleAction_Id] ON [dbo].[SystemsTradingControlScheduleAction]
(
	[ActionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TableChangeLogTableName]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_TableChangeLogTableName] ON [dbo].[TableChangeLog]
(
	[TableName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TradingSystem_id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_TradingSystem_id] ON [dbo].[TradingSystem]
(
	[TradingSystemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TradingSystem_id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_TradingSystem_id] ON [dbo].[TradingSystem_Deleted]
(
	[TradingSystemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TradingSystemActions_TradingSystemId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_TradingSystemActions_TradingSystemId] ON [dbo].[TradingSystemActions]
(
	[TradingSystemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TradingSystemDealsExecuted_date]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_TradingSystemDealsExecuted_date] ON [dbo].[TradingSystemDealsExecuted_Archive]
(
	[DatePartDealExecuted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [LargeSlowDataFileGroup]
GO
/****** Object:  Index [IX_TradingSystemDealsExecuted_TradingSystemSymbol]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_TradingSystemDealsExecuted_TradingSystemSymbol] ON [dbo].[TradingSystemDealsExecuted_Daily]
(
	[TradingSystemId] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TradingSystemDealsRejected_date]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_TradingSystemDealsRejected_date] ON [dbo].[TradingSystemDealsRejected_Archive]
(
	[DatePartDealRejected] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [LargeSlowDataFileGroup]
GO
/****** Object:  Index [IX_TradingSystemDealsRejected_TradingSystemSymbol]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_TradingSystemDealsRejected_TradingSystemSymbol] ON [dbo].[TradingSystemDealsRejected_Daily]
(
	[TradingSystemId] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TradingSystemGroup_TradingSystemGroupId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_TradingSystemGroup_TradingSystemGroupId] ON [dbo].[TradingSystemGroup]
(
	[TradingSystemGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TradingSystemMonitoringPage_id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_TradingSystemMonitoringPage_id] ON [dbo].[TradingSystemMonitoringPage]
(
	[TradingSystemMonitoringPageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TradingSystemType_id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_TradingSystemType_id] ON [dbo].[TradingSystemType]
(
	[TradingSystemTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VenueCrossSettingsSymbolPerCounterparty]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_VenueCrossSettingsSymbolPerCounterparty] ON [dbo].[VenueCrossSettings]
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VenueCrossSettings_Changes]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_VenueCrossSettings_Changes] ON [dbo].[VenueCrossSettings_Changes]
(
	[TradingSystemId] ASC,
	[ValidToUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VenueGliderSettingsSymbolPerCounterparty]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_VenueGliderSettingsSymbolPerCounterparty] ON [dbo].[VenueGliderSettings]
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VenueGliderSettings_Changes]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_VenueGliderSettings_Changes] ON [dbo].[VenueGliderSettings_Changes]
(
	[TradingSystemId] ASC,
	[ValidToUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VenueMMSettingsSymbolPerCounterparty]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_VenueMMSettingsSymbolPerCounterparty] ON [dbo].[VenueMMSettings]
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VenueMMSettings_Changes]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_VenueMMSettings_Changes] ON [dbo].[VenueMMSettings_Changes]
(
	[TradingSystemId] ASC,
	[ValidToUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VenueQuotingSettingsSymbolPerCounterparty]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_VenueQuotingSettingsSymbolPerCounterparty] ON [dbo].[VenueQuotingSettings]
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VenueQuotingSettings_Changes]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_VenueQuotingSettings_Changes] ON [dbo].[VenueQuotingSettings_Changes]
(
	[TradingSystemId] ASC,
	[ValidToUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VenueStreamSettingsSymbolPerCounterparty]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_VenueStreamSettingsSymbolPerCounterparty] ON [dbo].[VenueStreamSettings]
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VenueStreamSettings_Changes]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE CLUSTERED INDEX [IX_VenueStreamSettings_Changes] ON [dbo].[VenueStreamSettings_Changes]
(
	[TradingSystemId] ASC,
	[ValidToUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [Unique_VenueId_TradeDate]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE CLUSTERED INDEX [Unique_VenueId_TradeDate] ON [dbo].[VolumeExternal]
(
	[VenueId] ASC,
	[TradeDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AggregatedExternalExecutedTicket_AggregatedExternalExecutedTicketCounterpartyId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE NONCLUSTERED INDEX [IX_AggregatedExternalExecutedTicket_AggregatedExternalExecutedTicketCounterpartyId] ON [dbo].[AggregatedExternalExecutedTicket]
(
	[AggregatedExternalExecutedTicketCounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CounterpartyCode]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_CounterpartyCode] ON [dbo].[Counterparty]
(
	[CounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueCurrencyAlphaCode]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyAlphaCode] ON [dbo].[Currency_Internal]
(
	[CurrencyAlphaCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueCurrencyName]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyName] ON [dbo].[Currency_Internal]
(
	[CurrencyName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DealDirection]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_DealDirection] ON [dbo].[DealDirection]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_DatePart_Symbol]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE NONCLUSTERED INDEX [IX_DatePart_Symbol] ON [dbo].[DealExternalExecuted]
(
	[DatePartIntegratorReceivedExecutionReportUtc] ASC,
	[SymbolId] ASC
)
INCLUDE ( 	[AmountBasePolExecuted],
	[Price],
	[AmountTermPolExecutedInUsd],
	[CounterpartyId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_DealExternalExecutedTicket_DealExternalExecutedTicketId]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE NONCLUSTERED INDEX [IX_DealExternalExecutedTicket_DealExternalExecutedTicketId] ON [dbo].[DealExternalExecutedTicket]
(
	[InternalTransactionIdentity] ASC
)
INCLUDE ( 	[DealExternalExecutedTicketTypeId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_DealExternalExecutedTicketTypeName]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_DealExternalExecutedTicketTypeName] ON [dbo].[DealExternalExecutedTicketType]
(
	[DealExternalExecutedTicketTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_DatePart_Counterparty]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalRejected]
(
	[DatePartIntegratorReceivedExecutionReportUtc] ASC,
	[CounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_FlowSideName]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_FlowSideName] ON [dbo].[FlowSide]
(
	[FlowSideName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IntegratorEndpoint_PrivateIP]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IntegratorEndpoint_PrivateIP] ON [dbo].[IntegratorEndpoint]
(
	[PrivateIPBinary] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IntegratorEnvironment_Id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IntegratorEnvironment_Id] ON [dbo].[IntegratorEnvironment]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IntegratorInstance_Id]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IntegratorInstance_Id] ON [dbo].[IntegratorInstance]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IntegratorProcessState_Name]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IntegratorProcessState_Name] ON [dbo].[IntegratorProcessState]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IntegratorProcessType_Name]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IntegratorProcessType_Name] ON [dbo].[IntegratorProcessType]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_IntegratorRejectionReason_Reason]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_IntegratorRejectionReason_Reason] ON [dbo].[IntegratorRejectionReason]
(
	[IntegratorRejectionReason] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_KillSwitchLogon]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE NONCLUSTERED INDEX [IX_KillSwitchLogon] ON [dbo].[KillSwitchLogon]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_OrderExternalCancelTypeName]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_OrderExternalCancelTypeName] ON [dbo].[OrderExternalCancelType]
(
	[OrderExternalCancelTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_DatePart_Counterparty]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[OrderExternalIncomingRejectable_Daily]
(
	[DatePartIntegratorReceivedOrderUtc] ASC,
	[CounterpartyId] ASC,
	[SymbolId] ASC
)
INCLUDE ( 	[IntegratorExecutedAmountBasePol],
	[IntegratorRejectedAmountBasePol],
	[CounterpartyRequestedPrice],
	[IntegratorLatencyNanoseconds]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [settings_friendlyName]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [settings_friendlyName] ON [dbo].[Settings]
(
	[FriendlyName] ASC
)
WHERE ([FriendlyName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_StpCounterpartyName]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_StpCounterpartyName] ON [dbo].[StpCounterparty]
(
	[StpCounterpartyName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Symbol]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Symbol] ON [dbo].[Symbol_Internal]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Symbol_Name]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [Symbol_Name] ON [dbo].[Symbol_Internal]
(
	[Name] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Symbol_ShortName]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [Symbol_ShortName] ON [dbo].[Symbol_Internal]
(
	[ShortName] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SystemsTradingControlScheduleAction_Name]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [SystemsTradingControlScheduleAction_Name] ON [dbo].[SystemsTradingControlScheduleAction]
(
	[ActionName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TradingSystemType_Name]    Script Date: 10. 6. 2015 15:02:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TradingSystemType_Name] ON [dbo].[TradingSystemType]
(
	[CounterpartyId] ASC,
	[SystemName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Dictionary_FIX] ADD  DEFAULT ((1)) FOR [SettingsVersion]
GO
ALTER TABLE [dbo].[ExternalOrdersTransmissionSwitch] ADD  DEFAULT ('NAME NOT SPECIFIED') FOR [SwitchFriendlyName]
GO
ALTER TABLE [dbo].[Fluent_FixCredentials] ADD  DEFAULT ((1)) FOR [LockX]
GO
ALTER TABLE [dbo].[Fluent_RedirectionState] ADD  DEFAULT ((1)) FOR [LockX]
GO
ALTER TABLE [dbo].[IntegratorEnvironment] ADD  DEFAULT ((1)) FOR [IntegratorEnvironmentTypeId]
GO
ALTER TABLE [dbo].[KillSwitchLogon] ADD  CONSTRAINT [DF_KillSwitchLogon_SiteId]  DEFAULT ((1)) FOR [RoleId]
GO
ALTER TABLE [dbo].[KillSwitchUser] ADD  CONSTRAINT [DF_KillSwitchUser_SiteId]  DEFAULT ((1)) FOR [RoleId]
GO
ALTER TABLE [dbo].[Settings] ADD  CONSTRAINT [DF__Settings___Setti__04AFB25B]  DEFAULT ((1)) FOR [SettingsVersion]
GO
ALTER TABLE [dbo].[Settings_FIX] ADD  DEFAULT ((1)) FOR [SettingsVersion]
GO
ALTER TABLE [dbo].[TradeDecay_LastProcessedRecord] ADD  DEFAULT ((1)) FOR [LockX]
GO
ALTER TABLE [dbo].[TradingSystemActions] ADD  DEFAULT ((0)) FOR [HardBlocked]
GO
ALTER TABLE [dbo].[TradingSystemTypeAllowedForGroup] ADD  DEFAULT ((0)) FOR [IsGoFlatOnlyOn]
GO
ALTER TABLE [dbo].[TradingSystemTypeAllowedForGroup] ADD  DEFAULT ((0)) FOR [IsOrderTransmissionDisabled]
GO
ALTER TABLE [dbo].[AggregatedExternalExecutedTicket]  WITH CHECK ADD  CONSTRAINT [FK_AggregatedExternalExecutedTicket_StpCounterparty] FOREIGN KEY([StpCounterpartyId])
REFERENCES [dbo].[StpCounterparty] ([StpCounterpartyId])
GO
ALTER TABLE [dbo].[AggregatedExternalExecutedTicket] CHECK CONSTRAINT [FK_AggregatedExternalExecutedTicket_StpCounterparty]
GO
ALTER TABLE [dbo].[CommandStatus]  WITH CHECK ADD  CONSTRAINT [FK_CommandStatus_Command] FOREIGN KEY([CommandId])
REFERENCES [dbo].[Command] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CommandStatus] CHECK CONSTRAINT [FK_CommandStatus_Command]
GO
ALTER TABLE [dbo].[CommissionsPerCounterparty]  WITH CHECK ADD  CONSTRAINT [FK_CommissionsPerCounterparty_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[CommissionsPerCounterparty] CHECK CONSTRAINT [FK_CommissionsPerCounterparty_Counterparty]
GO
ALTER TABLE [dbo].[CommissionsPerCounterparty]  WITH CHECK ADD  CONSTRAINT [FK_CommissionsPerCounterparty_PrimeBroker] FOREIGN KEY([PrimeBrokerId])
REFERENCES [dbo].[PrimeBroker] ([PrimeBrokerId])
GO
ALTER TABLE [dbo].[CommissionsPerCounterparty] CHECK CONSTRAINT [FK_CommissionsPerCounterparty_PrimeBroker]
GO
ALTER TABLE [dbo].[CommissionsSchedule_CptCommission]  WITH CHECK ADD  CONSTRAINT [FK_CommissionsSchedule_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[CommissionsSchedule_CptCommission] CHECK CONSTRAINT [FK_CommissionsSchedule_Counterparty]
GO
ALTER TABLE [dbo].[CommissionsSchedule_Mapping]  WITH CHECK ADD  CONSTRAINT [FK_CommissionsScheduleMapping_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[CommissionsSchedule_Mapping] CHECK CONSTRAINT [FK_CommissionsScheduleMapping_Counterparty]
GO
ALTER TABLE [dbo].[CommissionsSchedule_PBCommission]  WITH CHECK ADD  CONSTRAINT [FK_CommissionsSchedule_PrimeBroker] FOREIGN KEY([PrimeBrokerId])
REFERENCES [dbo].[PrimeBroker] ([PrimeBrokerId])
GO
ALTER TABLE [dbo].[CommissionsSchedule_PBCommission] CHECK CONSTRAINT [FK_CommissionsSchedule_PrimeBroker]
GO
ALTER TABLE [dbo].[Currency_Rates]  WITH NOCHECK ADD  CONSTRAINT [FK_Currency_Rates_Currency] FOREIGN KEY([CurrencyID])
REFERENCES [dbo].[Currency_Internal] ([CurrencyID])
GO
ALTER TABLE [dbo].[Currency_Rates] CHECK CONSTRAINT [FK_Currency_Rates_Currency]
GO
ALTER TABLE [dbo].[DealExternalExecuted]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecuted_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[DealExternalExecuted] CHECK CONSTRAINT [FK_DealExternalExecuted_Counterparty]
GO
ALTER TABLE [dbo].[DealExternalExecuted]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecuted_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[DealExternalExecuted] CHECK CONSTRAINT [FK_DealExternalExecuted_DealDirection]
GO
ALTER TABLE [dbo].[DealExternalExecuted]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecuted_FlowSide] FOREIGN KEY([FlowSideId])
REFERENCES [dbo].[FlowSide] ([FlowSideId])
GO
ALTER TABLE [dbo].[DealExternalExecuted] CHECK CONSTRAINT [FK_DealExternalExecuted_FlowSide]
GO
ALTER TABLE [dbo].[DealExternalExecuted]  WITH NOCHECK ADD  CONSTRAINT [FK_DealExternalExecuted_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[DealExternalExecuted] CHECK CONSTRAINT [FK_DealExternalExecuted_Symbol]
GO
ALTER TABLE [dbo].[DealExternalExecutedTicket]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecutedTicket_DealExternalExecutedTicketType] FOREIGN KEY([DealExternalExecutedTicketTypeId])
REFERENCES [dbo].[DealExternalExecutedTicketType] ([DealExternalExecutedTicketTypeId])
GO
ALTER TABLE [dbo].[DealExternalExecutedTicket] CHECK CONSTRAINT [FK_DealExternalExecutedTicket_DealExternalExecutedTicketType]
GO
ALTER TABLE [dbo].[DealExternalExecutedTicket]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecutedTicket_StpCounterparty] FOREIGN KEY([StpCounterpartyId])
REFERENCES [dbo].[StpCounterparty] ([StpCounterpartyId])
GO
ALTER TABLE [dbo].[DealExternalExecutedTicket] CHECK CONSTRAINT [FK_DealExternalExecutedTicket_StpCounterparty]
GO
ALTER TABLE [dbo].[DealExternalRejected]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalRejected_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[DealExternalRejected] CHECK CONSTRAINT [FK_DealExternalRejected_DealDirection]
GO
ALTER TABLE [dbo].[DealExternalRejected]  WITH CHECK ADD  CONSTRAINT [FK_RejectedDealExternal_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[DealExternalRejected] CHECK CONSTRAINT [FK_RejectedDealExternal_Counterparty]
GO
ALTER TABLE [dbo].[DealExternalRejected]  WITH NOCHECK ADD  CONSTRAINT [FK_RejectedDealExternal_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[DealExternalRejected] CHECK CONSTRAINT [FK_RejectedDealExternal_Symbol]
GO
ALTER TABLE [dbo].[ExecutedExternalDeals]  WITH CHECK ADD  CONSTRAINT [FK_ExecutedExternalDeals_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[ExecutedExternalDeals] CHECK CONSTRAINT [FK_ExecutedExternalDeals_DealDirection]
GO
ALTER TABLE [dbo].[ExecutedExternalDeals]  WITH NOCHECK ADD  CONSTRAINT [FK_ExecutedExternalDeals_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[ExecutedExternalDeals] CHECK CONSTRAINT [FK_ExecutedExternalDeals_Symbol]
GO
ALTER TABLE [dbo].[ExecutionReportMessage]  WITH CHECK ADD  CONSTRAINT [FK_ExecutionReportMessage_ExecutionReportStatus] FOREIGN KEY([ExecutionReportStatusId])
REFERENCES [dbo].[ExecutionReportStatus] ([Id])
GO
ALTER TABLE [dbo].[ExecutionReportMessage] CHECK CONSTRAINT [FK_ExecutionReportMessage_ExecutionReportStatus]
GO
ALTER TABLE [dbo].[ExecutionReportMessage_Archive]  WITH CHECK ADD  CONSTRAINT [FK_ExecutionReportMessageArchive_ExecutionReportStatus] FOREIGN KEY([ExecutionReportStatusId])
REFERENCES [dbo].[ExecutionReportStatus] ([Id])
GO
ALTER TABLE [dbo].[ExecutionReportMessage_Archive] CHECK CONSTRAINT [FK_ExecutionReportMessageArchive_ExecutionReportStatus]
GO
ALTER TABLE [dbo].[ExecutionReportMessage_Archive2]  WITH CHECK ADD  CONSTRAINT [FK_ExecutionReportMessageArchive2_ExecutionReportStatus] FOREIGN KEY([ExecutionReportStatusId])
REFERENCES [dbo].[ExecutionReportStatus] ([Id])
GO
ALTER TABLE [dbo].[ExecutionReportMessage_Archive2] CHECK CONSTRAINT [FK_ExecutionReportMessageArchive2_ExecutionReportStatus]
GO
ALTER TABLE [dbo].[ExternalOrdersTransmissionSwitchHistory]  WITH CHECK ADD  CONSTRAINT [FK_ExternalOrdersTransmissionSwitchHistory_ExternalOrdersTransmissionSwitch] FOREIGN KEY([ExternalOrdersTransmissionSwitchId])
REFERENCES [dbo].[ExternalOrdersTransmissionSwitch] ([Id])
GO
ALTER TABLE [dbo].[ExternalOrdersTransmissionSwitchHistory] CHECK CONSTRAINT [FK_ExternalOrdersTransmissionSwitchHistory_ExternalOrdersTransmissionSwitch]
GO
ALTER TABLE [dbo].[IntegratorEnvironment]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironment_IntegratorEnvironmentType] FOREIGN KEY([IntegratorEnvironmentTypeId])
REFERENCES [dbo].[IntegratorEnvironmentType] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironment] CHECK CONSTRAINT [FK_IntegratorEnvironment_IntegratorEnvironmentType]
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironmentToSettingsFixMap_Environment] FOREIGN KEY([IntegratorEnvironmentId])
REFERENCES [dbo].[IntegratorEnvironment] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap] CHECK CONSTRAINT [FK_IntegratorEnvironmentToSettingsFixMap_Environment]
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironmentToSettingsFixMap_SettingsFix] FOREIGN KEY([SettingsFixId])
REFERENCES [dbo].[Settings_FIX] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap] CHECK CONSTRAINT [FK_IntegratorEnvironmentToSettingsFixMap_SettingsFix]
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsMap]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironmentToSettingsMap_Environment] FOREIGN KEY([IntegratorEnvironmentId])
REFERENCES [dbo].[IntegratorEnvironment] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsMap] CHECK CONSTRAINT [FK_IntegratorEnvironmentToSettingsMap_Environment]
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsMap]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironmentToSettingsMap_Settings] FOREIGN KEY([SettingsId])
REFERENCES [dbo].[Settings] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsMap] CHECK CONSTRAINT [FK_IntegratorEnvironmentToSettingsMap_Settings]
GO
ALTER TABLE [dbo].[IntegratorInstanceSessionClaim]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorInstanceSessionClaim_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[IntegratorInstanceSessionClaim] CHECK CONSTRAINT [FK_IntegratorInstanceSessionClaim_Counterparty]
GO
ALTER TABLE [dbo].[IntegratorInstanceSessionClaim]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorInstanceSessionClaim_IntegratorInstance] FOREIGN KEY([IntegratorInstanceId])
REFERENCES [dbo].[IntegratorInstance] ([Id])
GO
ALTER TABLE [dbo].[IntegratorInstanceSessionClaim] CHECK CONSTRAINT [FK_IntegratorInstanceSessionClaim_IntegratorInstance]
GO
ALTER TABLE [dbo].[IntegratorProcess]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEndpoint_IntegratorInstance] FOREIGN KEY([IntegratorEndpointId])
REFERENCES [dbo].[IntegratorEndpoint] ([Id])
GO
ALTER TABLE [dbo].[IntegratorProcess] CHECK CONSTRAINT [FK_IntegratorEndpoint_IntegratorInstance]
GO
ALTER TABLE [dbo].[IntegratorProcess]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorProcess_IntegratorInstance] FOREIGN KEY([IntegratorInstanceId])
REFERENCES [dbo].[IntegratorInstance] ([Id])
GO
ALTER TABLE [dbo].[IntegratorProcess] CHECK CONSTRAINT [FK_IntegratorProcess_IntegratorInstance]
GO
ALTER TABLE [dbo].[IntegratorProcess]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorProcessState_IntegratorInstance] FOREIGN KEY([IntegratorProcessStateId])
REFERENCES [dbo].[IntegratorProcessState] ([Id])
GO
ALTER TABLE [dbo].[IntegratorProcess] CHECK CONSTRAINT [FK_IntegratorProcessState_IntegratorInstance]
GO
ALTER TABLE [dbo].[IntegratorProcess]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorProcessType_IntegratorInstance] FOREIGN KEY([IntegratorProcessTypeId])
REFERENCES [dbo].[IntegratorProcessType] ([Id])
GO
ALTER TABLE [dbo].[IntegratorProcess] CHECK CONSTRAINT [FK_IntegratorProcessType_IntegratorInstance]
GO
ALTER TABLE [dbo].[KillSwitchLogon]  WITH CHECK ADD  CONSTRAINT [FK_KillSwitchLogon_KillSwitchRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[KillSwitchRole] ([Id])
GO
ALTER TABLE [dbo].[KillSwitchLogon] CHECK CONSTRAINT [FK_KillSwitchLogon_KillSwitchRole]
GO
ALTER TABLE [dbo].[KillSwitchUser]  WITH CHECK ADD  CONSTRAINT [FK_KillSwitchUser_KillSwitchRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[KillSwitchRole] ([Id])
GO
ALTER TABLE [dbo].[KillSwitchUser] CHECK CONSTRAINT [FK_KillSwitchUser_KillSwitchRole]
GO
ALTER TABLE [dbo].[NewCommand]  WITH CHECK ADD  CONSTRAINT [FK_NewCommand_Command] FOREIGN KEY([CommandId])
REFERENCES [dbo].[Command] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NewCommand] CHECK CONSTRAINT [FK_NewCommand_Command]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_Counterparty]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_DealDirection]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_OrderTimeInForce] FOREIGN KEY([OrderTimeInForceId])
REFERENCES [dbo].[OrderTimeInForce] ([OrderTimeInForceId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_OrderTimeInForce]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_OrderType] FOREIGN KEY([OrderTypeId])
REFERENCES [dbo].[OrderType] ([OrderTypeId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_OrderType]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_PegType] FOREIGN KEY([PegTypeId])
REFERENCES [dbo].[PegType] ([PegTypeId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_PegType]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderExternal_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_Symbol]
GO
ALTER TABLE [dbo].[OrderExternal_Archive]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalArchive_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[OrderExternal_Archive] CHECK CONSTRAINT [FK_OrderExternalArchive_Counterparty]
GO
ALTER TABLE [dbo].[OrderExternal_Archive]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalArchive_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[OrderExternal_Archive] CHECK CONSTRAINT [FK_OrderExternalArchive_DealDirection]
GO
ALTER TABLE [dbo].[OrderExternal_Archive]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalArchive_OrderTimeInForce] FOREIGN KEY([OrderTimeInForceId])
REFERENCES [dbo].[OrderTimeInForce] ([OrderTimeInForceId])
GO
ALTER TABLE [dbo].[OrderExternal_Archive] CHECK CONSTRAINT [FK_OrderExternalArchive_OrderTimeInForce]
GO
ALTER TABLE [dbo].[OrderExternal_Archive]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalArchive_OrderType] FOREIGN KEY([OrderTypeId])
REFERENCES [dbo].[OrderType] ([OrderTypeId])
GO
ALTER TABLE [dbo].[OrderExternal_Archive] CHECK CONSTRAINT [FK_OrderExternalArchive_OrderType]
GO
ALTER TABLE [dbo].[OrderExternal_Archive]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalArchive_PegType] FOREIGN KEY([PegTypeId])
REFERENCES [dbo].[PegType] ([PegTypeId])
GO
ALTER TABLE [dbo].[OrderExternal_Archive] CHECK CONSTRAINT [FK_OrderExternalArchive_PegType]
GO
ALTER TABLE [dbo].[OrderExternal_Archive]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderExternalArchive_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[OrderExternal_Archive] CHECK CONSTRAINT [FK_OrderExternalArchive_Symbol]
GO
ALTER TABLE [dbo].[OrderExternal_Archive2]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalArchive2_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[OrderExternal_Archive2] CHECK CONSTRAINT [FK_OrderExternalArchive2_Counterparty]
GO
ALTER TABLE [dbo].[OrderExternal_Archive2]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalArchive2_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[OrderExternal_Archive2] CHECK CONSTRAINT [FK_OrderExternalArchive2_DealDirection]
GO
ALTER TABLE [dbo].[OrderExternal_Archive2]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalArchive2_OrderTimeInForce] FOREIGN KEY([OrderTimeInForceId])
REFERENCES [dbo].[OrderTimeInForce] ([OrderTimeInForceId])
GO
ALTER TABLE [dbo].[OrderExternal_Archive2] CHECK CONSTRAINT [FK_OrderExternalArchive2_OrderTimeInForce]
GO
ALTER TABLE [dbo].[OrderExternal_Archive2]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalArchive2_OrderType] FOREIGN KEY([OrderTypeId])
REFERENCES [dbo].[OrderType] ([OrderTypeId])
GO
ALTER TABLE [dbo].[OrderExternal_Archive2] CHECK CONSTRAINT [FK_OrderExternalArchive2_OrderType]
GO
ALTER TABLE [dbo].[OrderExternal_Archive2]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalArchive2_PegType] FOREIGN KEY([PegTypeId])
REFERENCES [dbo].[PegType] ([PegTypeId])
GO
ALTER TABLE [dbo].[OrderExternal_Archive2] CHECK CONSTRAINT [FK_OrderExternalArchive2_PegType]
GO
ALTER TABLE [dbo].[OrderExternal_Archive2]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderExternalArchive2_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[OrderExternal_Archive2] CHECK CONSTRAINT [FK_OrderExternalArchive2_Symbol]
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternalCancelType] FOREIGN KEY([OrderExternalCancelTypeId])
REFERENCES [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId])
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest] CHECK CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternalCancelType]
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest_Archive]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelRequestArchive_OrderExternalCancelType] FOREIGN KEY([OrderExternalCancelTypeId])
REFERENCES [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId])
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest_Archive] CHECK CONSTRAINT [FK_OrderExternalCancelRequestArchive_OrderExternalCancelType]
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest_Archive2]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelRequestArchive2_OrderExternalCancelType] FOREIGN KEY([OrderExternalCancelTypeId])
REFERENCES [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId])
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest_Archive2] CHECK CONSTRAINT [FK_OrderExternalCancelRequestArchive2_OrderExternalCancelType]
GO
ALTER TABLE [dbo].[OrderExternalCancelResult]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelResult_ExecutionReportStatus] FOREIGN KEY([ResultStatusId])
REFERENCES [dbo].[ExecutionReportStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderExternalCancelResult] CHECK CONSTRAINT [FK_OrderExternalCancelResult_ExecutionReportStatus]
GO
ALTER TABLE [dbo].[OrderExternalCancelResult_Archive]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelResultArchive_ExecutionReportStatus] FOREIGN KEY([ResultStatusId])
REFERENCES [dbo].[ExecutionReportStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderExternalCancelResult_Archive] CHECK CONSTRAINT [FK_OrderExternalCancelResultArchive_ExecutionReportStatus]
GO
ALTER TABLE [dbo].[OrderExternalCancelResult_Archive2]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelResultArchive2_ExecutionReportStatus] FOREIGN KEY([ResultStatusId])
REFERENCES [dbo].[ExecutionReportStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderExternalCancelResult_Archive2] CHECK CONSTRAINT [FK_OrderExternalCancelResultArchive2_ExecutionReportStatus]
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_Counterparty]
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_DealDirection] FOREIGN KEY([IntegratorDealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_DealDirection]
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_Symbol]
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_Counterparty]
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_DealDirection] FOREIGN KEY([IntegratorDealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_DealDirection]
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_IntegratorRejectionReason] FOREIGN KEY([IntegratorRejectionReasonId])
REFERENCES [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReasonId])
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_IntegratorRejectionReason]
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_Symbol]
GO
ALTER TABLE [dbo].[PriceDelaySettings]  WITH CHECK ADD  CONSTRAINT [FK_PriceDelaySettings_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[PriceDelaySettings] CHECK CONSTRAINT [FK_PriceDelaySettings_Counterparty]
GO
ALTER TABLE [dbo].[PriceDelayStats]  WITH CHECK ADD  CONSTRAINT [FK_PriceDelayStats_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[PriceDelayStats] CHECK CONSTRAINT [FK_PriceDelayStats_Counterparty]
GO
ALTER TABLE [dbo].[SettingsMandatoryInEnvironemntType]  WITH CHECK ADD  CONSTRAINT [FK_SettingsMandatoryInEnvironemntType_IntegratorEnvironmentType] FOREIGN KEY([IntegratorEnvironmentTypeId])
REFERENCES [dbo].[IntegratorEnvironmentType] ([Id])
GO
ALTER TABLE [dbo].[SettingsMandatoryInEnvironemntType] CHECK CONSTRAINT [FK_SettingsMandatoryInEnvironemntType_IntegratorEnvironmentType]
GO
ALTER TABLE [dbo].[SettlementDate]  WITH NOCHECK ADD  CONSTRAINT [FK_SettlementDate_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[SettlementDate] CHECK CONSTRAINT [FK_SettlementDate_Symbol]
GO
ALTER TABLE [dbo].[Symbol_Internal]  WITH CHECK ADD  CONSTRAINT [FK_Symbol_Currency] FOREIGN KEY([BaseCurrencyId])
REFERENCES [dbo].[Currency_Internal] ([CurrencyID])
GO
ALTER TABLE [dbo].[Symbol_Internal] CHECK CONSTRAINT [FK_Symbol_Currency]
GO
ALTER TABLE [dbo].[Symbol_Internal]  WITH CHECK ADD  CONSTRAINT [FK_Symbol_Currency1] FOREIGN KEY([QuoteCurrencyId])
REFERENCES [dbo].[Currency_Internal] ([CurrencyID])
GO
ALTER TABLE [dbo].[Symbol_Internal] CHECK CONSTRAINT [FK_Symbol_Currency1]
GO
ALTER TABLE [dbo].[Symbol_Rates]  WITH NOCHECK ADD  CONSTRAINT [FK_Symbol_Rates_Symbol] FOREIGN KEY([Id])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[Symbol_Rates] CHECK CONSTRAINT [FK_Symbol_Rates_Symbol]
GO
ALTER TABLE [dbo].[Symbol_TrustedSpreads]  WITH NOCHECK ADD  CONSTRAINT [FK_Symbol_TrustedSpreads_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[Symbol_TrustedSpreads] CHECK CONSTRAINT [FK_Symbol_TrustedSpreads_Symbol]
GO
ALTER TABLE [dbo].[TradingSystem]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystem_TradingSystemGroup] FOREIGN KEY([TradingSystemGroupId])
REFERENCES [dbo].[TradingSystemGroup] ([TradingSystemGroupId])
GO
ALTER TABLE [dbo].[TradingSystem] CHECK CONSTRAINT [FK_TradingSystem_TradingSystemGroup]
GO
ALTER TABLE [dbo].[TradingSystemActions]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemActions_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[TradingSystemActions] CHECK CONSTRAINT [FK_TradingSystemActions_Counterparty]
GO
ALTER TABLE [dbo].[TradingSystemActions]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemActions_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[TradingSystemActions] CHECK CONSTRAINT [FK_TradingSystemActions_Symbol]
GO
ALTER TABLE [dbo].[TradingSystemActions]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemActions_TradingSystemActions] FOREIGN KEY([TradingSystemId])
REFERENCES [dbo].[TradingSystemActions] ([TradingSystemId])
GO
ALTER TABLE [dbo].[TradingSystemActions] CHECK CONSTRAINT [FK_TradingSystemActions_TradingSystemActions]
GO
ALTER TABLE [dbo].[TradingSystemMonitoringPage]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemMonitoringPage_BeginSymbol] FOREIGN KEY([BeginSymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[TradingSystemMonitoringPage] CHECK CONSTRAINT [FK_TradingSystemMonitoringPage_BeginSymbol]
GO
ALTER TABLE [dbo].[TradingSystemMonitoringPage]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemMonitoringPage_EndSymbol] FOREIGN KEY([EndSymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[TradingSystemMonitoringPage] CHECK CONSTRAINT [FK_TradingSystemMonitoringPage_EndSymbol]
GO
ALTER TABLE [dbo].[TradingSystemMonitoringPage]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemMonitoringPage_TradingSystemGroup] FOREIGN KEY([TradingSystemGroupId])
REFERENCES [dbo].[TradingSystemGroup] ([TradingSystemGroupId])
GO
ALTER TABLE [dbo].[TradingSystemMonitoringPage] CHECK CONSTRAINT [FK_TradingSystemMonitoringPage_TradingSystemGroup]
GO
ALTER TABLE [dbo].[TradingSystemMonitoringPage]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemMonitoringPage_TradingSystemTypeId] FOREIGN KEY([TradingSystemTypeId])
REFERENCES [dbo].[TradingSystemType] ([TradingSystemTypeId])
GO
ALTER TABLE [dbo].[TradingSystemMonitoringPage] CHECK CONSTRAINT [FK_TradingSystemMonitoringPage_TradingSystemTypeId]
GO
ALTER TABLE [dbo].[TradingSystemType]  WITH CHECK ADD  CONSTRAINT [FK_TradingSystemType_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[TradingSystemType] CHECK CONSTRAINT [FK_TradingSystemType_Counterparty]
GO
ALTER TABLE [dbo].[TradingSystemTypeAllowedForGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemTypeAllowedForGroup_TradingSystemGroup] FOREIGN KEY([TradingSystemGroupId])
REFERENCES [dbo].[TradingSystemGroup] ([TradingSystemGroupId])
GO
ALTER TABLE [dbo].[TradingSystemTypeAllowedForGroup] CHECK CONSTRAINT [FK_TradingSystemTypeAllowedForGroup_TradingSystemGroup]
GO
ALTER TABLE [dbo].[TradingSystemTypeAllowedForGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemTypeAllowedForGroup_TradingSystemType] FOREIGN KEY([TradingSystemTypeId])
REFERENCES [dbo].[TradingSystemType] ([TradingSystemTypeId])
GO
ALTER TABLE [dbo].[TradingSystemTypeAllowedForGroup] CHECK CONSTRAINT [FK_TradingSystemTypeAllowedForGroup_TradingSystemType]
GO
ALTER TABLE [dbo].[VenueCrossSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_LmaxCrossSettings_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueCrossSettings] CHECK CONSTRAINT [FK_LmaxCrossSettings_Symbol]
GO
ALTER TABLE [dbo].[VenueCrossSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueCrossSettings_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueCrossSettings] CHECK CONSTRAINT [FK_VenueCrossSettings_Counterparty]
GO
ALTER TABLE [dbo].[VenueCrossSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueCrossSettings_Changes_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueCrossSettings_Changes] CHECK CONSTRAINT [FK_VenueCrossSettings_Changes_Counterparty]
GO
ALTER TABLE [dbo].[VenueCrossSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueCrossSettings_Changes_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueCrossSettings_Changes] CHECK CONSTRAINT [FK_VenueCrossSettings_Changes_Symbol]
GO
ALTER TABLE [dbo].[VenueGliderSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueGliderSettings_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueGliderSettings] CHECK CONSTRAINT [FK_VenueGliderSettings_Counterparty]
GO
ALTER TABLE [dbo].[VenueGliderSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueGliderSettings_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueGliderSettings] CHECK CONSTRAINT [FK_VenueGliderSettings_Symbol]
GO
ALTER TABLE [dbo].[VenueGliderSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueGliderSettings_Changes_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueGliderSettings_Changes] CHECK CONSTRAINT [FK_VenueGliderSettings_Changes_Counterparty]
GO
ALTER TABLE [dbo].[VenueGliderSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueGliderSettings_Changes_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueGliderSettings_Changes] CHECK CONSTRAINT [FK_VenueGliderSettings_Changes_Symbol]
GO
ALTER TABLE [dbo].[VenueMMSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_HotspotMMSettings_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueMMSettings] CHECK CONSTRAINT [FK_HotspotMMSettings_Symbol]
GO
ALTER TABLE [dbo].[VenueMMSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueMMSettings_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueMMSettings] CHECK CONSTRAINT [FK_VenueMMSettings_Counterparty]
GO
ALTER TABLE [dbo].[VenueMMSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueMMSettings_Changes_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueMMSettings_Changes] CHECK CONSTRAINT [FK_VenueMMSettings_Changes_Counterparty]
GO
ALTER TABLE [dbo].[VenueMMSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueMMSettings_Changes_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueMMSettings_Changes] CHECK CONSTRAINT [FK_VenueMMSettings_Changes_Symbol]
GO
ALTER TABLE [dbo].[VenueQuotingSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueQuotingSettings_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueQuotingSettings] CHECK CONSTRAINT [FK_VenueQuotingSettings_Counterparty]
GO
ALTER TABLE [dbo].[VenueQuotingSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueQuotingSettings_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueQuotingSettings] CHECK CONSTRAINT [FK_VenueQuotingSettings_Symbol]
GO
ALTER TABLE [dbo].[VenueQuotingSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueQuotingSettings_Changes_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] CHECK CONSTRAINT [FK_VenueQuotingSettings_Changes_Counterparty]
GO
ALTER TABLE [dbo].[VenueQuotingSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueQuotingSettings_Changes_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] CHECK CONSTRAINT [FK_VenueQuotingSettings_Changes_Symbol]
GO
ALTER TABLE [dbo].[VenueStreamSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueStreamSettings_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueStreamSettings] CHECK CONSTRAINT [FK_VenueStreamSettings_Counterparty]
GO
ALTER TABLE [dbo].[VenueStreamSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueStreamSettings_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueStreamSettings] CHECK CONSTRAINT [FK_VenueStreamSettings_Symbol]
GO
ALTER TABLE [dbo].[VenueStreamSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueStreamSettings_Changes_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueStreamSettings_Changes] CHECK CONSTRAINT [FK_VenueStreamSettings_Changes_Counterparty]
GO
ALTER TABLE [dbo].[VenueStreamSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueStreamSettings_Changes_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueStreamSettings_Changes] CHECK CONSTRAINT [FK_VenueStreamSettings_Changes_Symbol]
GO
ALTER TABLE [dbo].[CommissionsSchedule_CptCommission]  WITH CHECK ADD  CONSTRAINT [CK_CommissionsSchedule_CptCommissionRangeValid] CHECK  (([ValidFromUtc]<=[ValidToUtc]))
GO
ALTER TABLE [dbo].[CommissionsSchedule_CptCommission] CHECK CONSTRAINT [CK_CommissionsSchedule_CptCommissionRangeValid]
GO
ALTER TABLE [dbo].[CommissionsSchedule_Mapping]  WITH CHECK ADD  CONSTRAINT [CK_CommissionsSchedule_MappingRangeValid] CHECK  (([ValidFromUtc]<=[ValidToUtc]))
GO
ALTER TABLE [dbo].[CommissionsSchedule_Mapping] CHECK CONSTRAINT [CK_CommissionsSchedule_MappingRangeValid]
GO
ALTER TABLE [dbo].[CommissionsSchedule_PBCommission]  WITH CHECK ADD  CONSTRAINT [CK_CommissionsSchedule_PBCommissionRangeValid] CHECK  (([ValidFromUtc]<=[ValidToUtc]))
GO
ALTER TABLE [dbo].[CommissionsSchedule_PBCommission] CHECK CONSTRAINT [CK_CommissionsSchedule_PBCommissionRangeValid]
GO
ALTER TABLE [dbo].[Fluent_FixCredentials]  WITH CHECK ADD  CONSTRAINT [CK_FCred_Locked] CHECK  (([LockX]=(1)))
GO
ALTER TABLE [dbo].[Fluent_FixCredentials] CHECK CONSTRAINT [CK_FCred_Locked]
GO
ALTER TABLE [dbo].[Fluent_RedirectionState]  WITH CHECK ADD  CONSTRAINT [CK_FT1_Locked] CHECK  (([LockX]=(1)))
GO
ALTER TABLE [dbo].[Fluent_RedirectionState] CHECK CONSTRAINT [CK_FT1_Locked]
GO
ALTER TABLE [dbo].[Symbol_TrustedSpreads]  WITH CHECK ADD  CONSTRAINT [MaximumAllowedTickAge_milliseconds_Positive] CHECK  (([MaximumAllowedTickAge_milliseconds]>(0)))
GO
ALTER TABLE [dbo].[Symbol_TrustedSpreads] CHECK CONSTRAINT [MaximumAllowedTickAge_milliseconds_Positive]
GO
ALTER TABLE [dbo].[Symbol_TrustedSpreads]  WITH CHECK ADD  CONSTRAINT [MaximumTrustedSpreadBp_NonNegative] CHECK  (([MaximumTrustedSpreadBp]>=(0)))
GO
ALTER TABLE [dbo].[Symbol_TrustedSpreads] CHECK CONSTRAINT [MaximumTrustedSpreadBp_NonNegative]
GO
ALTER TABLE [dbo].[Symbol_TrustedSpreads]  WITH CHECK ADD  CONSTRAINT [MinimumTrustedSpreadBp_NonPositive] CHECK  (([MinimumTrustedSpreadBp]<=(0)))
GO
ALTER TABLE [dbo].[Symbol_TrustedSpreads] CHECK CONSTRAINT [MinimumTrustedSpreadBp_NonPositive]
GO
ALTER TABLE [dbo].[TradeDecay_LastProcessedRecord]  WITH CHECK ADD  CONSTRAINT [CK_T1_Locked] CHECK  (([LockX]=(1)))
GO
ALTER TABLE [dbo].[TradeDecay_LastProcessedRecord] CHECK CONSTRAINT [CK_T1_Locked]
GO
/****** Object:  Trigger [dbo].[NewCommissionScheduleInserted_Cpt]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[NewCommissionScheduleInserted_Cpt] ON [dbo].[CommissionsSchedule_CptCommission] AFTER INSERT, UPDATE
AS
BEGIN
	--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
	SET NOCOUNT ON
	exec [dbo].[NewCommissionScheduleHandling_SP]
END

GO
/****** Object:  Trigger [dbo].[NewCommissionScheduleInserted_Map]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[NewCommissionScheduleInserted_Map] ON [dbo].[CommissionsSchedule_Mapping] AFTER INSERT, UPDATE
AS
BEGIN
	--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
	SET NOCOUNT ON
	exec [dbo].[NewCommissionScheduleHandling_SP]
END

GO
/****** Object:  Trigger [dbo].[NewCommissionScheduleInserted_Pb]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[NewCommissionScheduleInserted_Pb] ON [dbo].[CommissionsSchedule_PBCommission] AFTER INSERT, UPDATE
AS
BEGIN
	--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
	SET NOCOUNT ON
	exec [dbo].[NewCommissionScheduleHandling_SP]
END

GO
/****** Object:  Trigger [dbo].[DealExternalExecutedInserted]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[DealExternalExecutedInserted] ON [dbo].[DealExternalExecuted] AFTER INSERT
AS
BEGIN
	-- commit the internal insert transaction to prevent deadlocks
	COMMIT TRANSACTION
	exec [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP]
	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION
END

GO
/****** Object:  Trigger [dbo].[DealExternalRejectedInserted]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[DealExternalRejectedInserted] ON [dbo].[DealExternalRejected] AFTER INSERT
AS
BEGIN
	-- commit the internal insert transaction to prevent deadlocks
	COMMIT TRANSACTION
	exec [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP]
	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION
END

GO
/****** Object:  Trigger [dbo].[ExternalOrdersTransmissionSwitch_Changing]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[ExternalOrdersTransmissionSwitch_Changing] ON [dbo].[ExternalOrdersTransmissionSwitch]
--needed to be able to execute sp_send_dbmail
WITH EXECUTE AS 'dbo'
AFTER INSERT, UPDATE
AS

BEGIN

	--we need to store the Id before commiting, as afterwards the inserted table will be empty
	DECLARE @updatedSwitchId INT
	SELECT TOP 1 @updatedSwitchId = [Id] FROM inserted

	-- commit the internal insert transaction to prevent problems during inserting critical data
	-- and also to make sure that following functions has fresh data
	COMMIT TRANSACTION

	--Extremely important to perform operation in try - catch to make sure that error happens just once
	begin try
	
        DECLARE @subject NVARCHAR(255) = 'KGT OrderTrans ' + CASE WHEN [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]() = 1 THEN 'ON (' ELSE 'OFF (' END

		SELECT 
			@subject = @subject + [SwitchFriendlyName] + ' ' + CASE WHEN [TransmissionEnabled] = 1 THEN 'ON)' ELSE 'OFF)' END
		FROM
			[dbo].[ExternalOrdersTransmissionSwitch]
		WHERE
			[Id] = @updatedSwitchId

		DECLARE @body NVARCHAR(MAX) = [dbo].[GetExternalOrdersTransmissionOverviewString](@updatedSwitchId)

		DECLARE @pbContactList NVARCHAR(MAX) = 
'citifx.primebrokerage@citi.com; fxpb.mo@citi.com; charles.wells@citi.com; dan.shaw@citi.com; daniel.walters@citi.com; john.comatas@citi.com; kevin.y.lin@citi.com; leonard.levine@citi.com; michael.glen.jones@citi.com; mike.clement@citi.com; narasimha.rao@citi.com; paul.cusack@citi.com; ritchie.ward@citi.com; satomi.takiguchi@citi.com; sherrie.low@citi.com; william.fearon@citi.com; william.c.undertajlo@citi.com; jeremy.lee@citi.com'

		DECLARE @kgtContactList NVARCHAR(MAX) = 'michal.kreslik@kgtinv.com; marek.fogiel@kgtinv.com; jan.krivanek@kgtinv.com'

		DECLARE @copyRecipientsList NVARCHAR(MAX) = @kgtContactList

		--For prime broker switch changes add PB contacts to cc
		SELECT
			@copyRecipientsList = @copyRecipientsList + '; ' + @pbContactList
		FROM
			[dbo].[ExternalOrdersTransmissionSwitch]
		WHERE
			[Id] = @updatedSwitchId AND [SwitchFriendlyName] = 'PrimeBroker'

		EXEC msdb.dbo.sp_send_dbmail
				@recipients = 'support@kgtinv.com', 
				@copy_recipients = @copyRecipientsList,
				@profile_name = 'SQL Mail Profile',
				@subject = @subject,
				@body = @body;
    end try
    begin catch
        --select 0;
    end catch;

	exec [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]

	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION

END

GO
/****** Object:  Trigger [dbo].[OrderExternalIncomingRejectable_DailyInserted]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[OrderExternalIncomingRejectable_DailyInserted] ON [dbo].[OrderExternalIncomingRejectable_Daily] AFTER INSERT
AS
BEGIN
	-- commit the internal insert transaction to prevent deadlocks
	COMMIT TRANSACTION
	exec [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP]
	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION
END

GO
/****** Object:  Trigger [dbo].[LastUpdatedTrigger_Settings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[LastUpdatedTrigger_Settings] ON [dbo].[Settings]
AFTER INSERT, UPDATE
AS

BEGIN

Update [dbo].[Settings] Set [LastUpdated] = CURRENT_TIMESTAMP from [dbo].[Settings] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[Id] = myAlias.[Id]

END

GO
/****** Object:  Trigger [dbo].[SchemaXsdCheckTrigger]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[SchemaXsdCheckTrigger] ON [dbo].[Settings] AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @Type NVARCHAR(100)
	DECLARE @SettingsXml [xml]
	DECLARE @Version [int]
	
	DECLARE settingsCursor CURSOR FOR
		SELECT Name, SettingsVersion, SettingsXml FROM inserted
    
	OPEN settingsCursor
    FETCH NEXT FROM settingsCursor INTO @Type, @Version, @SettingsXml
    WHILE @@FETCH_STATUS = 0
    BEGIN

		IF(@Type = N'Kreslik.Integrator.BusinessLayer.dll')
		BEGIN
			DECLARE @x1 XML([dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd]) 
			SET @x1 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.Common.dll')
		BEGIN
			DECLARE @x2 XML([dbo].[Kreslik.Integrator.Common.dll.xsd]) 
			SET @x2 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL.dll')
		BEGIN
			DECLARE @x3 XML([dbo].[Kreslik.Integrator.DAL.dll.xsd]) 
			SET @x3 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL_IntegratorConnection')
		BEGIN
			DECLARE @x32 XML([dbo].[Kreslik.Integrator.DALv2_IntegratorConnection.xsd]) 
			SET @x32 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL_DataCollectionConnection')
		BEGIN
			DECLARE @x33 XML([dbo].[Kreslik.Integrator.DALv2_DataCollectionConnection.xsd]) 
			SET @x33 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL_DataCollectionEnabling')
		BEGIN
			DECLARE @x34 XML([dbo].[Kreslik.Integrator.DALv2_DataCollectionEnabling.xsd]) 
			SET @x34 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DiagnosticsDashboard.exe')
		BEGIN
			DECLARE @x4 XML([dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd]) 
			SET @x4 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.IntegratorClientHost.exe')
		BEGIN
			DECLARE @x5 XML([dbo].[Kreslik.Integrator.IntegratorClientHost.exe.xsd]) 
			SET @x5 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.IntegratorWatchDogSvc.exe')
		BEGIN
			DECLARE @x6 XML([dbo].[Kreslik.Integrator.IntegratorWatchDogSvc.exe.xsd]) 
			SET @x6 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBus.dll')
		BEGIN
			DECLARE @x7 XML([dbo].[Kreslik.Integrator.MessageBus.dll.xsd]) 
			SET @x7 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo')
		BEGIN
			DECLARE @x7_1 XML([dbo].[Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo.xsd]) 
			SET @x7_1 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo')
		BEGIN
			DECLARE @x7_2 XML([dbo].[Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo.xsd]) 
			SET @x7_2 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping')
		BEGIN
			DECLARE @x7_3 XML([dbo].[Kreslik.Integrator.MessageBusSettings_ConnectionKeeping.xsd]) 
			SET @x7_3 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior')
		BEGIN
			DECLARE @x7_4 XML([dbo].[Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior.xsd]) 
			SET @x7_4 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling')
		BEGIN
			DECLARE @x7_5 XML([dbo].[Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling.xsd]) 
			SET @x7_5 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.QuickItchN.dll')
		BEGIN
			DECLARE @x8 XML([dbo].[Kreslik.Integrator.QuickItchN.dll.xsd]) 
			SET @x8 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.RiskManagement.dll')
		BEGIN
			DECLARE @x9 XML([dbo].[Kreslik.Integrator.RiskManagement.dll.xsd]) 
			SET @x9 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.SessionManagement.dll')
		BEGIN
			DECLARE @x10 XML([dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]) 
			SET @x10 = @SettingsXml
		END

		ELSE
		BEGIN
			RAISERROR (N'Setting with name: %s is unknown to DB', 16, 2, @Type ) WITH SETERROR
			ROLLBACK TRAN
		END

		FETCH NEXT FROM settingsCursor INTO @Type, @Version, @SettingsXml
    END
	CLOSE settingsCursor
    DEALLOCATE settingsCursor
END

GO
/****** Object:  Trigger [dbo].[LastUpdatedTrigger_Settings_FIX]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[LastUpdatedTrigger_Settings_FIX] ON [dbo].[Settings_FIX]
AFTER INSERT, UPDATE
AS

BEGIN

Update [dbo].[Settings_FIX] Set [LastUpdated] = CURRENT_TIMESTAMP from [dbo].[Settings_FIX] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[Id] = myAlias.[Id]

END

GO
/****** Object:  Trigger [dbo].[LastDeletedTrigger_SystemsTradingControlSchedule]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[LastDeletedTrigger_SystemsTradingControlSchedule] ON [dbo].[SystemsTradingControlSchedule]
AFTER DELETE
AS
BEGIN

--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON
UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'SystemsTradingControlSchedule'

END

GO
/****** Object:  Trigger [dbo].[LastUpdatedTrigger_SystemsTradingControlSchedule]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[LastUpdatedTrigger_SystemsTradingControlSchedule] ON [dbo].[SystemsTradingControlSchedule]
AFTER INSERT, UPDATE
AS

BEGIN

--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[SystemsTradingControlSchedule] Set [LastUpdatedUtc] = GETUTCDATE() from [dbo].[SystemsTradingControlSchedule] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[ScheduleEntryId] = myAlias.[ScheduleEntryId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'SystemsTradingControlSchedule'

END

GO
/****** Object:  Trigger [dbo].[TradingSystem_ArchiveDeleted]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TradingSystem_ArchiveDeleted] ON [dbo].[TradingSystem] AFTER DELETE
AS
BEGIN
	INSERT INTO
		[dbo].[TradingSystem_Deleted]
			([TradingSystemId],
			[TradingSystemTypeId],
			[TradingSystemGroupId],
			[SymbolId],
			[Description],
			[DeletedUtc])
	SELECT
		[TradingSystemId],
		[TradingSystemTypeId],
		[TradingSystemGroupId],
		[SymbolId],
		[Description],
		GETUTCDATE()
	FROM
		deleted
END

GO
/****** Object:  Trigger [dbo].[LastUpdatedTrigger_TradingSystemActions]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[LastUpdatedTrigger_TradingSystemActions] ON [dbo].[TradingSystemActions]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE 
	[dbo].[TableChangeLog] 
SET 
	[LastUpdatedUtc] = GETUTCDATE() 
WHERE 
	[TableName] IN
	(	
	SELECT
		'Venue' + tst.SystemName + 'Settings'
	FROM 
		inserted insertedAction
		INNER JOIN [dbo].[TradingSystem] ts ON insertedAction.TradingSystemId = ts.TradingSystemId
		INNER JOIN [dbo].[TradingSystemType] tst ON ts.TradingSystemTypeId = tst.TradingSystemTypeId
	)
END

GO
/****** Object:  Trigger [dbo].[TradingSystemGroup_ArchiveDeleted]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TradingSystemGroup_ArchiveDeleted] ON [dbo].[TradingSystemGroup] AFTER DELETE
AS
BEGIN
	INSERT INTO
		[dbo].[TradingSystemGroup_Deleted]
			([TradingSystemGroupId],
			[TradingSystemGroupName],
			[DeletedUtc])
	SELECT
		[TradingSystemGroupId],
		[TradingSystemGroupName],
		GETUTCDATE()
	FROM
		deleted
END

GO
/****** Object:  Trigger [dbo].[TradingSystemTypeAllowedForGroup_SwitchChanged]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[TradingSystemTypeAllowedForGroup_SwitchChanged] ON [dbo].[TradingSystemTypeAllowedForGroup]
AFTER UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

	UPDATE
		action
	SET
		action.GoFlatOnly = inserted.IsGoFlatOnlyOn,
		action.OrderTransmissionDisabled = inserted.IsOrderTransmissionDisabled
	FROM
		inserted
		INNER JOIN [dbo].[TradingSystem] trsystem ON inserted.TradingSystemGroupId = trsystem.TradingSystemGroupId AND inserted.TradingSystemTypeId = trsystem.TradingSystemTypeId
		INNER JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		UPDATE([IsGoFlatOnlyOn]) OR UPDATE([IsOrderTransmissionDisabled])
END

GO
/****** Object:  Trigger [dbo].[LastDeletedTrigger_VenueCrossSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[LastDeletedTrigger_VenueCrossSettings] ON [dbo].[VenueCrossSettings]
AFTER DELETE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON
UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueCrossSettings'

END

GO
/****** Object:  Trigger [dbo].[LastUpdatedTrigger_VenueCrossSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[LastUpdatedTrigger_VenueCrossSettings] ON [dbo].[VenueCrossSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueCrossSettings'

END

GO
/****** Object:  Trigger [dbo].[VenueCrossSettings_DeletedOrUpdated]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[VenueCrossSettings_DeletedOrUpdated] ON [dbo].[VenueCrossSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueCrossSettings_Changes]
	   ([SymbolId]
	   ,[MinimumSizeToTrade]
	   ,[MaximumSizeToTrade]
	   ,[MinimumDiscountBasisPointsToTrade]
	   ,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
	   ,[MaximumWaitTimeOnImprovementTick_milliseconds]
	   ,[MinimumFillSize]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[MinimumSizeToTrade]
		,deleted.[MaximumSizeToTrade]
		,deleted.[MinimumDiscountBasisPointsToTrade]
		,deleted.[MinimumTimeBetweenSingleCounterpartySignals_seconds]
		,deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]
		,deleted.[MinimumFillSize]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueCrossSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueCrossSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([MinimumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MinimumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MinimumDiscountBasisPointsToTrade] != deleted.[MinimumDiscountBasisPointsToTrade]))
		OR (UPDATE([MinimumTimeBetweenSingleCounterpartySignals_seconds]) AND (topUpdatedChange.[MinimumTimeBetweenSingleCounterpartySignals_seconds] IS NULL OR topUpdatedChange.[MinimumTimeBetweenSingleCounterpartySignals_seconds] != deleted.[MinimumTimeBetweenSingleCounterpartySignals_seconds]))
		OR (UPDATE([MaximumWaitTimeOnImprovementTick_milliseconds]) AND (topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] IS NULL OR topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] != deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
END

GO
/****** Object:  Trigger [dbo].[LastDeletedTrigger_VenueGliderSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[LastDeletedTrigger_VenueGliderSettings] ON [dbo].[VenueGliderSettings]
AFTER DELETE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON
UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueGliderSettings'

END

GO
/****** Object:  Trigger [dbo].[LastUpdatedTrigger_VenueGliderSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[LastUpdatedTrigger_VenueGliderSettings] ON [dbo].[VenueGliderSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueGliderSettings'

END

GO
/****** Object:  Trigger [dbo].[VenueGliderSettings_DeletedOrUpdated]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[VenueGliderSettings_DeletedOrUpdated] ON [dbo].[VenueGliderSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueGliderSettings_Changes]
	   ([SymbolId]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[MaximumSizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade]
	   ,[KGTLLTime_milliseconds]
	   ,[MinimumBPGrossGain]
	   ,[PreferLmaxDuringHedging]
	   ,[MinimumIntegratorPriceLife_milliseconds]
	   ,[MinimumIntegratorGlidePriceLife_milliseconds]
	   ,[GlidingCutoffSpan_milliseconds]
	   ,[MinimumSizeToTrade]
	   ,[MinimumFillSize]
	   ,[MaximumSourceBookTiers]
	   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
	   ,[ReferenceLmaxBookOnly]
	   ,[BidEnabled]
	   ,[AskEnabled]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[MaximumSizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[KGTLLTime_milliseconds]
		,deleted.[MinimumBPGrossGain]
		,deleted.[PreferLmaxDuringHedging]
		,deleted.[MinimumIntegratorPriceLife_milliseconds]
		,deleted.[MinimumIntegratorGlidePriceLife_milliseconds]
		,deleted.[GlidingCutoffSpan_milliseconds]
		,deleted.[MinimumSizeToTrade]
		,deleted.[MinimumFillSize]
		,deleted.[MaximumSourceBookTiers]
		,deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]
		,deleted.[ReferenceLmaxBookOnly]
		,deleted.[BidEnabled]
		,deleted.[AskEnabled]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueGliderSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueGliderSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([KGTLLTime_milliseconds]) AND (topUpdatedChange.[KGTLLTime_milliseconds] IS NULL OR topUpdatedChange.[KGTLLTime_milliseconds] != deleted.[KGTLLTime_milliseconds]))
		OR (UPDATE([MinimumBPGrossGain]) AND (topUpdatedChange.[MinimumBPGrossGain] IS NULL OR topUpdatedChange.[MinimumBPGrossGain] != deleted.[MinimumBPGrossGain]))
		OR (UPDATE([PreferLmaxDuringHedging]) AND (topUpdatedChange.[PreferLmaxDuringHedging] IS NULL OR topUpdatedChange.[PreferLmaxDuringHedging] != deleted.[PreferLmaxDuringHedging]))
		OR (UPDATE([MinimumIntegratorPriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] != deleted.[MinimumIntegratorPriceLife_milliseconds]))
		OR (UPDATE([MinimumIntegratorGlidePriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorGlidePriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorGlidePriceLife_milliseconds] != deleted.[MinimumIntegratorGlidePriceLife_milliseconds]))
		OR (UPDATE([GlidingCutoffSpan_milliseconds]) AND (topUpdatedChange.[GlidingCutoffSpan_milliseconds] IS NULL OR topUpdatedChange.[GlidingCutoffSpan_milliseconds] != deleted.[GlidingCutoffSpan_milliseconds]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([MaximumSourceBookTiers]) AND (topUpdatedChange.[MaximumSourceBookTiers] IS NULL OR topUpdatedChange.[MaximumSourceBookTiers] != deleted.[MaximumSourceBookTiers]))
		OR (UPDATE([MinimumBasisPointsFromKgtPriceToFastCancel]) AND (topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] IS NULL OR topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] != deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]))
		OR (UPDATE([ReferenceLmaxBookOnly]) AND (topUpdatedChange.[ReferenceLmaxBookOnly] IS NULL OR topUpdatedChange.[ReferenceLmaxBookOnly] != deleted.[ReferenceLmaxBookOnly]))
		OR (UPDATE([BidEnabled]) AND (topUpdatedChange.[BidEnabled] IS NULL OR topUpdatedChange.[BidEnabled] != deleted.[BidEnabled]))
		OR (UPDATE([AskEnabled]) AND (topUpdatedChange.[AskEnabled] IS NULL OR topUpdatedChange.[AskEnabled] != deleted.[AskEnabled]))
END

GO
/****** Object:  Trigger [dbo].[LastDeletedTrigger_VenueMMSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[LastDeletedTrigger_VenueMMSettings] ON [dbo].[VenueMMSettings]
AFTER DELETE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON
UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueMMSettings'

END

GO
/****** Object:  Trigger [dbo].[LastUpdatedTrigger_VenueMMSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[LastUpdatedTrigger_VenueMMSettings] ON [dbo].[VenueMMSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueMMSettings'

END

GO
/****** Object:  Trigger [dbo].[VenueMMSettings_DeletedOrUpdated]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[VenueMMSettings_DeletedOrUpdated] ON [dbo].[VenueMMSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueMMSettings_Changes]
	   ([SymbolId]
	   ,[MaximumSizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade]
	   ,[MaximumWaitTimeOnImprovementTick_milliseconds]
	   ,[MinimumIntegratorPriceLife_milliseconds]
	   ,[MinimumSizeToTrade]
	   ,[MinimumFillSize]
	   ,[MaximumSourceBookTiers]
	   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
	   ,[BidEnabled]
	   ,[AskEnabled]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[MaximumSizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]
		,deleted.[MinimumIntegratorPriceLife_milliseconds]
		,deleted.[MinimumSizeToTrade]
	    ,deleted.[MinimumFillSize]
		,deleted.[MaximumSourceBookTiers]
		,deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]
		,deleted.[BidEnabled]
		,deleted.[AskEnabled]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueMMSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueMMSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([MaximumWaitTimeOnImprovementTick_milliseconds]) AND (topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] IS NULL OR topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] != deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]))
		OR (UPDATE([MinimumIntegratorPriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] != deleted.[MinimumIntegratorPriceLife_milliseconds]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([MaximumSourceBookTiers]) AND (topUpdatedChange.[MaximumSourceBookTiers] IS NULL OR topUpdatedChange.[MaximumSourceBookTiers] != deleted.[MaximumSourceBookTiers]))
		OR (UPDATE([MinimumBasisPointsFromKgtPriceToFastCancel]) AND (topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] IS NULL OR topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] != deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]))
		OR (UPDATE([BidEnabled]) AND (topUpdatedChange.[BidEnabled] IS NULL OR topUpdatedChange.[BidEnabled] != deleted.[BidEnabled]))
		OR (UPDATE([AskEnabled]) AND (topUpdatedChange.[AskEnabled] IS NULL OR topUpdatedChange.[AskEnabled] != deleted.[AskEnabled]))
END

GO
/****** Object:  Trigger [dbo].[LastDeletedTrigger_VenueQuotingSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[LastDeletedTrigger_VenueQuotingSettings] ON [dbo].[VenueQuotingSettings]
AFTER DELETE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON
UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueQuotingSettings'

END

GO
/****** Object:  Trigger [dbo].[LastUpdatedTrigger_VenueQuotingSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[LastUpdatedTrigger_VenueQuotingSettings] ON [dbo].[VenueQuotingSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueQuotingSettings'

END

GO
/****** Object:  Trigger [dbo].[VenueQuotingSettings_DeletedOrUpdated]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE TRIGGER [dbo].[VenueQuotingSettings_DeletedOrUpdated] ON [dbo].[VenueQuotingSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueQuotingSettings_Changes]
	   ([SymbolId]
	   ,[CounterpartyId]
	   ,[TradingSystemId]  
	   ,[MaximumPositionBaseAbs]
	   ,[FairPriceImprovementSpreadBasisPoints]
	   ,[StopLossNetInUsd]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[MaximumPositionBaseAbs]
		,deleted.[FairPriceImprovementSpreadBasisPoints]
		,deleted.[StopLossNetInUsd]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueQuotingSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueQuotingSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([MaximumPositionBaseAbs]) AND (topUpdatedChange.[MaximumPositionBaseAbs] IS NULL OR topUpdatedChange.[MaximumPositionBaseAbs] != deleted.[MaximumPositionBaseAbs]))
		OR (UPDATE([FairPriceImprovementSpreadBasisPoints]) AND (topUpdatedChange.[FairPriceImprovementSpreadBasisPoints] IS NULL OR topUpdatedChange.[FairPriceImprovementSpreadBasisPoints] != deleted.[FairPriceImprovementSpreadBasisPoints]))
		OR (UPDATE([StopLossNetInUsd]) AND (topUpdatedChange.[StopLossNetInUsd] IS NULL OR topUpdatedChange.[StopLossNetInUsd] != deleted.[StopLossNetInUsd]))	
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
END

GO
/****** Object:  Trigger [dbo].[LastDeletedTrigger_VenueStreamSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[LastDeletedTrigger_VenueStreamSettings] ON [dbo].[VenueStreamSettings]
AFTER DELETE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON
UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueStreamSettings'

END

GO
/****** Object:  Trigger [dbo].[LastUpdatedTrigger_VenueStreamSettings]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[LastUpdatedTrigger_VenueStreamSettings] ON [dbo].[VenueStreamSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueStreamSettings'

END

GO
/****** Object:  Trigger [dbo].[VenueStreamSettings_DeletedOrUpdated]    Script Date: 10. 6. 2015 15:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[VenueStreamSettings_DeletedOrUpdated] ON [dbo].[VenueStreamSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueStreamSettings_Changes]
	   ([SymbolId]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[MaximumSizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade] 
	   ,[KGTLLTime_milliseconds]
	   ,[PreferLLDestinationCheckToLmax]
	   ,[MinimumBPGrossToAccept]
	   ,[PreferLmaxDuringHedging]   
	   ,[MinimumIntegratorPriceLife_milliseconds]
	   ,[MinimumSizeToTrade]
	   ,[MinimumFillSize]
	   ,[MaximumSourceBookTiers]
	   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
	   ,[ReferenceLmaxBookOnly]
	   ,[BidEnabled]
	   ,[AskEnabled]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[MaximumSizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[KGTLLTime_milliseconds]
	    ,deleted.[PreferLLDestinationCheckToLmax]
	    ,deleted.[MinimumBPGrossToAccept]
	    ,deleted.[PreferLmaxDuringHedging]
		,deleted.[MinimumIntegratorPriceLife_milliseconds]
		,deleted.[MinimumSizeToTrade]
	    ,deleted.[MinimumFillSize]
		,deleted.[MaximumSourceBookTiers]
		,deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]
		,deleted.[ReferenceLmaxBookOnly]
		,deleted.[BidEnabled]
		,deleted.[AskEnabled]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueStreamSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueStreamSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([KGTLLTime_milliseconds]) AND (topUpdatedChange.[KGTLLTime_milliseconds] IS NULL OR topUpdatedChange.[KGTLLTime_milliseconds] != deleted.[KGTLLTime_milliseconds]))
		OR (UPDATE([PreferLLDestinationCheckToLmax]) AND (topUpdatedChange.[PreferLLDestinationCheckToLmax] IS NULL OR topUpdatedChange.[PreferLLDestinationCheckToLmax] != deleted.[PreferLLDestinationCheckToLmax]))
		OR (UPDATE([MinimumBPGrossToAccept]) AND (topUpdatedChange.[MinimumBPGrossToAccept] IS NULL OR topUpdatedChange.[MinimumBPGrossToAccept] != deleted.[MinimumBPGrossToAccept]))
		OR (UPDATE([PreferLmaxDuringHedging]) AND (topUpdatedChange.[PreferLmaxDuringHedging] IS NULL OR topUpdatedChange.[PreferLmaxDuringHedging] != deleted.[PreferLmaxDuringHedging]))
		OR (UPDATE([MinimumIntegratorPriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] != deleted.[MinimumIntegratorPriceLife_milliseconds]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([MaximumSourceBookTiers]) AND (topUpdatedChange.[MaximumSourceBookTiers] IS NULL OR topUpdatedChange.[MaximumSourceBookTiers] != deleted.[MaximumSourceBookTiers]))
		OR (UPDATE([MinimumBasisPointsFromKgtPriceToFastCancel]) AND (topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] IS NULL OR topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] != deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]))
		OR (UPDATE([ReferenceLmaxBookOnly]) AND (topUpdatedChange.[ReferenceLmaxBookOnly] IS NULL OR topUpdatedChange.[ReferenceLmaxBookOnly] != deleted.[ReferenceLmaxBookOnly]))
		OR (UPDATE([BidEnabled]) AND (topUpdatedChange.[BidEnabled] IS NULL OR topUpdatedChange.[BidEnabled] != deleted.[BidEnabled]))
		OR (UPDATE([AskEnabled]) AND (topUpdatedChange.[AskEnabled] IS NULL OR topUpdatedChange.[AskEnabled] != deleted.[AskEnabled]))
END

GO
EXEC sys.sp_addextendedproperty @name=N'ISO', @value=N'ISO 4217, http://www.iso.org/iso/en/prods-services/popstds/currencycodeslist.html' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency_Internal'
GO


-------------------------
-- DATA 
------------------------

USE [Integrator]
GO
SET IDENTITY_INSERT [dbo].[Currency_Internal] ON 

GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (4, N'ALL', N'Lek', 8, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (5, N'DZD', N'Algerian Dinar', 12, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (6, N'ARS', N'Argentine Peso', 32, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (7, N'AUD', N'Australian Dollar', 36, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (8, N'BSD', N'Bahamian Dollar', 44, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (9, N'BHD', N'Bahraini Dinar', 48, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (10, N'BDT', N'Taka', 50, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (11, N'AMD', N'Armenian Dram', 51, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (12, N'BBD', N'Barbados Dollar', 52, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (13, N'BMD', N'Bermudian Dollar', 60, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (14, N'BTN', N'Ngultrum', 64, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (15, N'BOB', N'Boliviano', 68, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (16, N'BWP', N'Pula', 72, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (17, N'BZD', N'Belize Dollar', 84, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (18, N'SBD', N'Solomon Islands Dollar', 90, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (19, N'BND', N'Brunei Dollar', 96, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (20, N'MMK', N'Kyat', 104, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (21, N'BIF', N'Burundi Franc', 108, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (22, N'KHR', N'Riel', 116, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (23, N'CAD', N'Canadian Dollar', 124, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (24, N'CVE', N'Cape Verde Escudo', 132, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (25, N'KYD', N'Cayman Islands Dollar', 136, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (26, N'LKR', N'Sri Lanka Rupee', 144, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (27, N'CLP', N'Chilean Peso', 152, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (28, N'CNY', N'Yuan Renminbi', 156, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (29, N'COP', N'Colombian Peso', 170, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (30, N'KMF', N'Comoro Franc', 174, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (31, N'CRC', N'Costa Rican Colon', 188, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (32, N'HRK', N'Croatian Kuna', 191, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (33, N'CUP', N'Cuban Peso', 192, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (34, N'CYP', N'Cyprus Pound', 196, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (35, N'CZK', N'Czech Koruna', 203, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (36, N'DKK', N'Danish Krone', 208, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (37, N'DOP', N'Dominican Peso', 214, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (38, N'SVC', N'El Salvador Colon', 222, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (39, N'ETB', N'Ethiopian Birr', 230, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (40, N'ERN', N'Nakfa', 232, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (41, N'EEK', N'Kroon', 233, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (42, N'FKP', N'Falkland Islands Pound', 238, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (43, N'FJD', N'Fiji Dollar', 242, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (44, N'DJF', N'Djibouti Franc', 262, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (45, N'GMD', N'Dalasi', 270, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (46, N'GHC', N'Cedi', 288, 0, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (47, N'GIP', N'Gibraltar Pound', 292, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (48, N'GTQ', N'Quetzal', 320, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (49, N'GNF', N'Guinea Franc', 324, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (50, N'GYD', N'Guyana Dollar', 328, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (51, N'HTG', N'Gourde', 332, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (52, N'HNL', N'Lempira', 340, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (53, N'HKD', N'Hong Kong Dollar', 344, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (54, N'HUF', N'Forint', 348, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (55, N'ISK', N'Iceland Krona', 352, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (56, N'INR', N'Indian Rupee', 356, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (57, N'IDR', N'Rupiah', 360, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (58, N'IRR', N'Iranian Rial', 364, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (59, N'IQD', N'Iraqi Dinar', 368, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (60, N'ILS', N'New Israeli Sheqel', 376, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (61, N'JMD', N'Jamaican Dollar', 388, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (62, N'JPY', N'Yen', 392, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (63, N'KZT', N'Tenge', 398, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (64, N'JOD', N'Jordanian Dinar', 400, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (65, N'KES', N'Kenyan Shilling', 404, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (66, N'KPW', N'North Korean Won', 408, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (67, N'KRW', N'Won', 410, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (68, N'KWD', N'Kuwaiti Dinar', 414, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (69, N'KGS', N'Som', 417, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (70, N'LAK', N'Kip', 418, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (71, N'LBP', N'Lebanese Pound', 422, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (72, N'LSL', N'Loti', 426, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (73, N'LVL', N'Latvian Lats', 428, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (74, N'LRD', N'Liberian Dollar', 430, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (75, N'LYD', N'Libyan Dinar', 434, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (76, N'LTL', N'Lithuanian Litas', 440, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (77, N'MOP', N'Pataca', 446, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (78, N'MWK', N'Kwacha (Malawi)', 454, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (79, N'MYR', N'Malaysian Ringgit', 458, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (80, N'MVR', N'Rufiyaa', 462, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (81, N'MTL', N'Maltese Lira', 470, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (82, N'MRO', N'Ouguiya', 478, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (83, N'MUR', N'Mauritius Rupee', 480, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (84, N'MXN', N'Mexican Peso', 484, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (85, N'MNT', N'Tugrik', 496, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (86, N'MDL', N'Moldovan Leu', 498, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (87, N'MAD', N'Moroccan Dirham', 504, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (88, N'OMR', N'Rial Omani', 512, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (89, N'NAD', N'Namibian Dollar', 516, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (90, N'NPR', N'Nepalese Rupee', 524, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (91, N'ANG', N'Netherlands Antillian Guilder', 532, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (92, N'AWG', N'Aruban Guilder', 533, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (93, N'VUV', N'Vatu', 548, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (94, N'NZD', N'New Zealand Dollar', 554, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (95, N'NIO', N'Cordoba Oro', 558, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (96, N'NGN', N'Naira', 566, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (97, N'NOK', N'Norwegian Krone', 578, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (98, N'PKR', N'Pakistan Rupee', 586, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (99, N'PAB', N'Balboa', 590, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (100, N'PGK', N'Kina', 598, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (101, N'PYG', N'Guarani', 600, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (102, N'PEN', N'Nuevo Sol', 604, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (103, N'PHP', N'Philippine Peso', 608, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (104, N'GWP', N'Guinea-Bissau Peso', 624, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (105, N'QAR', N'Qatari Rial', 634, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (106, N'ROL', N'Old Leu', 642, 0, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (107, N'RUB', N'Russian Ruble', 643, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (108, N'RWF', N'Rwanda Franc', 646, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (109, N'SHP', N'Saint Helena Pound', 654, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (110, N'STD', N'Dobra', 678, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (111, N'SAR', N'Saudi Riyal', 682, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (112, N'SCR', N'Seychelles Rupee', 690, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (113, N'SLL', N'Leone', 694, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (114, N'SGD', N'Singapore Dollar', 702, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (115, N'SKK', N'Slovak Koruna', 703, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (116, N'VND', N'Dong', 704, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (117, N'SIT', N'Tolar', 705, 0, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (118, N'SOS', N'Somali Shilling', 706, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (119, N'ZAR', N'Rand', 710, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (120, N'ZWD', N'Zimbabwe Dollar', 716, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (121, N'SDD', N'Sudanese Dinar', 736, 0, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (122, N'SZL', N'Lilangeni', 748, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (123, N'SEK', N'Swedish Krona', 752, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (124, N'CHF', N'Swiss Franc', 756, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (125, N'SYP', N'Syrian Pound', 760, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (126, N'THB', N'Baht', 764, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (127, N'TOP', N'Pa anga', 776, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (128, N'TTD', N'Trinidad and Tobago Dollar', 780, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (129, N'AED', N'UAE Dirham', 784, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (130, N'TND', N'Tunisian Dinar', 788, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (131, N'TMM', N'Manat', 795, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (132, N'UGX', N'Uganda Shilling', 800, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (133, N'MKD', N'Denar', 807, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (134, N'EGP', N'Egyptian Pound', 818, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (135, N'GBP', N'Pound Sterling', 826, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (136, N'TZS', N'Tanzanian Shilling', 834, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (137, N'USD', N'US Dollar', 840, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (138, N'UYU', N'Peso Uruguayo', 858, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (139, N'UZS', N'Uzbekistan Sum', 860, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (140, N'VEB', N'Bolivar', 862, 0, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (141, N'WST', N'Tala', 882, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (142, N'YER', N'Yemeni Rial', 886, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (143, N'CSD', N'Serbian Dinar (former)', 891, 0, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (144, N'ZMK', N'Kwacha (Zambia)', 894, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (145, N'TWD', N'New Taiwan Dollar', 901, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (146, N'MZN', N'Metical', 943, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (147, N'AZN', N'Azerbaijanian Manat', 944, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (148, N'RON', N'New Leu', 946, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (149, N'CHE', N'WIR Euro', 947, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (150, N'CHW', N'WIR Franc', 948, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (151, N'TRY', N'New Turkish Lira', 949, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (152, N'XAF', N'CFA Franc BEAC', 950, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (153, N'XCD', N'East Caribbean Dollar', 951, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (154, N'XOF', N'CFA Franc BCEAO', 952, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (155, N'XPF', N'CFP Franc', 953, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (156, N'XDR', N'SDR', 960, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (157, N'SRD', N'Surinam Dollar', 968, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (158, N'MGA', N'Malagascy Ariary', 969, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (159, N'COU', N'Unidad de Valor Real', 970, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (160, N'AFN', N'Afghani', 971, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (161, N'TJS', N'Somoni', 972, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (162, N'AOA', N'Kwanza', 973, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (163, N'BYR', N'Belarussian Ruble', 974, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (164, N'BGN', N'Bulgarian Lev', 975, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (165, N'CDF', N'Franc Congolais', 976, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (166, N'BAM', N'Convertible Marks', 977, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (167, N'EUR', N'Euro', 978, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (168, N'MXV', N'Mexican Unidad de Inversion (UID)', 979, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (169, N'UAH', N'Hryvnia', 980, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (170, N'GEL', N'Lari', 981, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (171, N'BOV', N'Mvdol', 984, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (172, N'PLN', N'Zloty', 985, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (173, N'BRL', N'Brazilian Real', 986, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (174, N'CLF', N'Unidades de formento', 990, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (175, N'USN', N'US Dollar (Next day)', 997, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (176, N'USS', N'US Dollar (Same day)', 998, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (177, N'GHS', N'Ghana Cedi', 936, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (179, N'SDG', N'Sudanese Pound', 938, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (180, N'UYI', N'Uruguay Peso en Unidades Indexadas', 940, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (181, N'VEF', N'Bolivar Fuerte', 937, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (183, N'RSD', N'Serbian Dinar', 941, 1, CAST(0x000099E100DBF1F5 AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (184, N'CNH', N'Offshore (Hong Kong) Chinese Renminbi Yuan', NULL, 1, CAST(0x0000A24900F9F060 AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (186, N'XVN', N'VEN', NULL, 1, CAST(0x0000A3F400EE5CD6 AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (187, N'FTS', N'UK 100', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (188, N'DJI', N'Wall Street 30', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (189, N'SPX', N'US SPX 500', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (190, N'SPY', N'US SPX 500 (Mini)', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (191, N'NDX', N'US Tech 100', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (192, N'DAX', N'Germany 30', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (193, N'DXY', N'Germany 30 (Mini)', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (194, N'CAC', N'France 40', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (195, N'STX', N'Europe 50', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (196, N'NIK', N'Japan 225', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (197, N'AUS', N'Australia 200', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (198, N'XAU', N'Gold (Spot)', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (199, N'XAG', N'Silver (Spot)', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (200, N'WTI', N'US Crude (Spot)', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (201, N'BRE', N'UK Brent (Spot)', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (202, N'XPT', N'Platinum', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (203, N'XPD', N'Palladium', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
INSERT [dbo].[Currency_Internal] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (204, N'GAS', N'US Natural Gas (Spot)', NULL, 1, CAST(0x0000A4590138FD2B AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Currency_Internal] OFF
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (0, N'AUD/CAD', N'AUDCAD', 7, 23, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (1, N'AUD/CHF', N'AUDCHF', 7, 124, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (2, N'AUD/JPY', N'AUDJPY', 7, 62, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (3, N'AUD/NZD', N'AUDNZD', 7, 94, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (4, N'AUD/SGD', N'AUDSGD', 7, 114, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (5, N'AUD/USD', N'AUDUSD', 7, 137, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (6, N'CAD/JPY', N'CADJPY', 23, 62, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (7, N'CHF/JPY', N'CHFJPY', 124, 62, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (8, N'EUR/AUD', N'EURAUD', 167, 7, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (9, N'EUR/CAD', N'EURCAD', 167, 23, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (10, N'EUR/CHF', N'EURCHF', 167, 124, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (11, N'EUR/CZK', N'EURCZK', 167, 35, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (12, N'EUR/DKK', N'EURDKK', 167, 36, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (13, N'EUR/GBP', N'EURGBP', 167, 135, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (14, N'EUR/HUF', N'EURHUF', 167, 54, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (15, N'EUR/JPY', N'EURJPY', 167, 62, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (16, N'EUR/MXN', N'EURMXN', 167, 84, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (17, N'EUR/NOK', N'EURNOK', 167, 97, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (18, N'EUR/NZD', N'EURNZD', 167, 94, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (19, N'EUR/PLN', N'EURPLN', 167, 172, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (20, N'EUR/SEK', N'EURSEK', 167, 123, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (21, N'EUR/USD', N'EURUSD', 167, 137, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (22, N'EUR/ZAR', N'EURZAR', 167, 119, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (23, N'GBP/AUD', N'GBPAUD', 135, 7, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (24, N'GBP/CAD', N'GBPCAD', 135, 23, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (25, N'GBP/CHF', N'GBPCHF', 135, 124, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (26, N'GBP/JPY', N'GBPJPY', 135, 62, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (27, N'GBP/NOK', N'GBPNOK', 135, 97, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (28, N'GBP/NZD', N'GBPNZD', 135, 94, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (29, N'GBP/USD', N'GBPUSD', 135, 137, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (30, N'HKD/JPY', N'HKDJPY', 53, 62, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (31, N'NOK/SEK', N'NOKSEK', 97, 123, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (32, N'NZD/JPY', N'NZDJPY', 94, 62, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (33, N'NZD/SGD', N'NZDSGD', 94, 114, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (34, N'NZD/USD', N'NZDUSD', 94, 137, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (35, N'SGD/JPY', N'SGDJPY', 114, 62, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (36, N'USD/CAD', N'USDCAD', 137, 23, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (37, N'USD/CHF', N'USDCHF', 137, 124, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (38, N'USD/CZK', N'USDCZK', 137, 35, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (39, N'USD/HKD', N'USDHKD', 137, 53, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (40, N'USD/HUF', N'USDHUF', 137, 54, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (41, N'USD/ILS', N'USDILS', 137, 60, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (42, N'USD/JPY', N'USDJPY', 137, 62, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (43, N'USD/MXN', N'USDMXN', 137, 84, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (44, N'USD/NOK', N'USDNOK', 137, 97, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (45, N'USD/PLN', N'USDPLN', 137, 172, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (46, N'USD/SEK', N'USDSEK', 137, 123, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (47, N'USD/SGD', N'USDSGD', 137, 114, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (48, N'USD/TRY', N'USDTRY', 137, 151, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (49, N'USD/ZAR', N'USDZAR', 137, 119, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (50, N'ZAR/JPY', N'ZARJPY', 119, 62, 1, 2)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (51, N'USD/DKK', N'USDDKK', 137, 36, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (52, N'CAD/CHF', N'CADCHF', 23, 124, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (55, N'CAD/NZD', N'CADNZD', 23, 94, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (56, N'NZD/CHF', N'NZDCHF', 94, 124, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (57, N'AUD/DKK', N'AUDDKK', 7, 36, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (58, N'AUD/NOK', N'AUDNOK', 7, 97, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (59, N'EUR/TRY', N'EURTRY', 167, 151, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (60, N'NZD/CAD', N'NZDCAD', 94, 23, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (61, N'AUD/SEK', N'AUDSEK', 7, 123, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (62, N'CAD/DKK', N'CADDKK', 23, 36, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (63, N'CAD/NOK', N'CADNOK', 23, 97, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (64, N'CAD/SEK', N'CADSEK', 23, 123, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (65, N'CHF/DKK', N'CHFDKK', 124, 36, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (66, N'CHF/NOK', N'CHFNOK', 124, 97, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (67, N'CHF/SEK', N'CHFSEK', 124, 123, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (68, N'DKK/JPY', N'DKKJPY', 36, 62, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (69, N'DKK/NOK', N'DKKNOK', 36, 97, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (70, N'DKK/SEK', N'DKKSEK', 36, 123, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (71, N'GBP/DKK', N'GBPDKK', 135, 36, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (72, N'GBP/SEK', N'GBPSEK', 135, 123, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (73, N'MXN/JPY', N'MXNJPY', 84, 62, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (74, N'NOK/JPY', N'NOKJPY', 97, 62, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (75, N'NZD/DKK', N'NZDDKK', 94, 36, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (76, N'NZD/NOK', N'NZDNOK', 94, 97, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (77, N'NZD/SEK', N'NZDSEK', 94, 123, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (78, N'AUD/HKD', N'AUDHKD', 7, 53, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (79, N'EUR/HKD', N'EURHKD', 167, 53, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (80, N'EUR/RUB', N'EURRUB', 167, 107, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (81, N'GBP/CZK', N'GBPCZK', 135, 35, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (82, N'GBP/HUF', N'GBPHUF', 135, 54, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (83, N'USD/RUB', N'USDRUB', 137, 107, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (84, N'GBP/PLN', N'GBPPLN', 135, 172, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (85, N'EUR/CNH', N'EURCNH', 167, 184, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (86, N'USD/CNH', N'USDCNH', 137, 184, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (87, N'EUR/SGD', N'EURSGD', 167, 114, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (88, N'GBP/HKD', N'GBPHKD', 135, 53, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (89, N'GBP/MXN', N'GBPMXN', 135, 84, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (90, N'GBP/SGD', N'GBPSGD', 135, 114, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (91, N'GBP/TRY', N'GBPTRY', 135, 151, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (92, N'GBP/ZAR', N'GBPZAR', 135, 119, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (93, N'CHF/MXN', N'CHFMXN', 124, 84, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (94, N'USD/INR', N'USDINR', 137, 56, 1, 1)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (95, N'AUD/ZAR', N'AUDZAR', 7, 119, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (96, N'CAD/HKD', N'CADHKD', 23, 53, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (97, N'CAD/MXN', N'CADMXN', 23, 84, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (98, N'CAD/PLN', N'CADPLN', 23, 172, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (99, N'CAD/SGD', N'CADSGD', 23, 114, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (100, N'CAD/ZAR', N'CADZAR', 23, 119, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (101, N'CZK/JPY', N'CZKJPY', 35, 62, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (102, N'DKK/HKD', N'DKKHKD', 36, 53, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (103, N'EUR/ILS', N'EURILS', 167, 60, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (104, N'GBP/ILS', N'GBPILS', 135, 60, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (105, N'HUF/JPY', N'HUFJPY', 54, 62, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (106, N'CHF/CZK', N'CHFCZK', 124, 35, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (107, N'CHF/HKD', N'CHFHKD', 124, 53, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (108, N'CHF/HUF', N'CHFHUF', 124, 54, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (109, N'CHF/ILS', N'CHFILS', 124, 60, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (110, N'CHF/PLN', N'CHFPLN', 124, 172, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (111, N'CHF/SGD', N'CHFSGD', 124, 114, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (112, N'CHF/TRY', N'CHFTRY', 124, 151, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (113, N'CHF/ZAR', N'CHFZAR', 124, 119, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (114, N'NOK/HKD', N'NOKHKD', 97, 53, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (115, N'NZD/HKD', N'NZDHKD', 94, 53, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (116, N'NZD/PLN', N'NZDPLN', 94, 172, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (117, N'NZD/ZAR', N'NZDZAR', 94, 119, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (118, N'PLN/HUF', N'PLNHUF', 172, 54, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (119, N'PLN/JPY', N'PLNJPY', 172, 62, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (120, N'SEK/HKD', N'SEKHKD', 123, 53, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (121, N'SEK/JPY', N'SEKJPY', 123, 62, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (122, N'SGD/DKK', N'SGDDKK', 114, 36, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (123, N'SGD/HKD', N'SGDHKD', 114, 53, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (124, N'SGD/MXN', N'SGDMXN', 114, 84, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (125, N'SGD/NOK', N'SGDNOK', 114, 97, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (126, N'SGD/SEK', N'SGDSEK', 114, 123, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (127, N'TRY/JPY', N'TRYJPY', 151, 62, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (128, N'USD/GHS', N'USDGHS', 137, 177, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (129, N'USD/KES', N'USDKES', 137, 65, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (130, N'USD/NGN', N'USDNGN', 137, 96, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (131, N'USD/RON', N'USDRON', 137, 148, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (132, N'USD/UGX', N'USDUGX', 137, 132, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (133, N'USD/ZMK', N'USDZMK', 137, 144, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (134, N'ZAR/MXN', N'ZARMXN', 119, 84, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (135, N'USD/THB', N'USDTHB', 137, 126, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (136, N'USD/XVN', N'USDXVN', 137, 186, 1, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (137, N'FTS/GBP', N'FTSGBP', 187, 135, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (138, N'DJI/USD', N'DJIUSD', 188, 137, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (139, N'SPX/USD', N'SPXUSD', 189, 137, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (140, N'SPY/USD', N'SPYUSD', 190, 137, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (141, N'NDX/USD', N'NDXUSD', 191, 137, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (142, N'DAX/EUR', N'DAXEUR', 192, 167, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (143, N'DXY/EUR', N'DXYEUR', 193, 167, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (144, N'CAC/EUR', N'CACEUR', 194, 167, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (145, N'STX/EUR', N'STXEUR', 195, 167, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (146, N'NIK/JPY', N'NIKJPY', 196, 62, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (147, N'AUS/AUD', N'AUSAUD', 197, 7, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (148, N'XAU/USD', N'XAUUSD', 198, 137, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (149, N'XAG/USD', N'XAGUSD', 199, 137, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (150, N'XAU/AUD', N'XAUAUD', 198, 7, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (151, N'XAG/AUD', N'XAGAUD', 199, 7, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (152, N'WTI/USD', N'WTIUSD', 200, 137, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (153, N'BRE/USD', N'BREUSD', 201, 137, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (154, N'XPT/USD', N'XPTUSD', 202, 137, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (155, N'XPD/USD', N'XPDUSD', 203, 137, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (156, N'XAU/EUR', N'XAUEUR', 198, 167, NULL, NULL)
GO
INSERT [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [IsRaboTradeable], [Usage]) VALUES (157, N'GAS/USD', N'GASUSD', 204, 137, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (4, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (5, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (6, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (7, CAST(0.775155000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (8, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (9, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (10, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (11, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (12, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (13, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (14, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (15, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (16, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (17, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (18, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (19, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (20, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (21, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (22, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (23, CAST(0.818333954 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (24, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (25, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (26, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (27, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (28, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (29, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (30, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (31, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (32, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (33, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (34, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (35, CAST(0.041403239 AS Decimal(18, 9)), CAST(0x0750E838E46D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (36, CAST(0.151607379 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (37, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (38, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (39, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (40, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (41, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (42, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (43, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (44, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (45, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (46, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (47, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (48, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (49, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (50, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (51, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (52, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (53, CAST(0.128983411 AS Decimal(18, 9)), CAST(0x0710CAA1526D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (54, CAST(0.003622532 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (55, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (56, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (57, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (58, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (59, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (60, CAST(0.261489181 AS Decimal(18, 9)), CAST(0x071055CFD56D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (61, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (62, CAST(0.008148996 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (63, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (64, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (65, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (66, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (67, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (68, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (69, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (70, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (71, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (72, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (73, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (74, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (75, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (76, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (77, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (78, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (79, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (80, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (81, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (82, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (83, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (84, CAST(0.064863021 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (85, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (86, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (87, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (88, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (89, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (90, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (91, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (92, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (93, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (94, CAST(0.719095000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (95, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (96, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (97, CAST(0.129769348 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (98, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (99, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (100, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (101, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (102, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (103, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (104, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (105, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (106, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (107, CAST(0.016649878 AS Decimal(18, 9)), CAST(0x07C06BB1224B0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (108, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (109, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (110, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (111, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (112, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (113, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (114, CAST(0.744548971 AS Decimal(18, 9)), CAST(0x0750E838E46D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (115, CAST(0.044885117 AS Decimal(18, 9)), CAST(0x0780BC1CD693D2370B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (116, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (117, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (118, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (119, CAST(0.081125042 AS Decimal(18, 9)), CAST(0x07900C6BDF6D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (120, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (121, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (122, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (123, CAST(0.121033871 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (124, CAST(1.077655883 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (125, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (126, CAST(0.030499428 AS Decimal(18, 9)), CAST(0x0760986A146F5A390B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (127, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (128, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (129, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (130, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (131, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (132, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (133, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (134, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (135, CAST(1.551155000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (136, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (137, CAST(1.000000000 AS Decimal(18, 9)), CAST(0x0790484B0937D6370B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (138, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (139, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (140, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (141, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (142, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (143, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (144, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (145, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (146, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (147, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (148, CAST(0.257363822 AS Decimal(18, 9)), CAST(0x0790112ADA4A80390B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (149, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (150, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (151, CAST(0.365328165 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (152, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (153, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (154, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (155, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (156, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (157, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (158, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (159, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (160, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (161, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (162, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (163, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (164, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (165, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (166, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (167, CAST(1.131270000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (168, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (169, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (170, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (171, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (172, CAST(0.273392113 AS Decimal(18, 9)), CAST(0x07C0976DE56D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (173, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (174, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (175, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (176, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (177, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (179, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (180, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (181, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (183, NULL, NULL)
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (184, CAST(0.161020224 AS Decimal(18, 9)), CAST(0x0770FAD1E16D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Currency_Rates] ([CurrencyID], [LastKnownUsdConversionMultiplier], [UsdConversionMultiplierUpdated]) VALUES (186, NULL, NULL)
GO
SET ANSI_PADDING ON
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (0, CAST(0.947340000 AS Decimal(18, 9)), CAST(0.947140000 AS Decimal(18, 9)), CAST(0x07C0976DE56D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (1, CAST(0.719370000 AS Decimal(18, 9)), CAST(0.719180000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (2, CAST(95.130000000 AS Decimal(18, 9)), CAST(95.116000000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (3, CAST(1.078100000 AS Decimal(18, 9)), CAST(1.077960000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (4, CAST(1.092760000 AS Decimal(18, 9)), CAST(1.092450000 AS Decimal(18, 9)), CAST(0x0770CAF5DE4A80390B AS DateTime2), CAST(0x0770CAF5DE4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (5, CAST(0.775190000 AS Decimal(18, 9)), CAST(0.775120000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (6, CAST(100.428000000 AS Decimal(18, 9)), CAST(100.412000000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (7, CAST(132.260000000 AS Decimal(18, 9)), CAST(132.232000000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (8, CAST(1.459540000 AS Decimal(18, 9)), CAST(1.459310000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (9, CAST(1.382550000 AS Decimal(18, 9)), CAST(1.382310000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (10, CAST(1.049800000 AS Decimal(18, 9)), CAST(1.049700000 AS Decimal(18, 9)), CAST(0x07C0976DE56D0E3A0B AS DateTime2), CAST(0x0750D236DE6D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (11, CAST(27.330700000 AS Decimal(18, 9)), CAST(27.316700000 AS Decimal(18, 9)), CAST(0x07C0976DE56D0E3A0B AS DateTime2), CAST(0x07C0976DE56D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (12, CAST(7.461840000 AS Decimal(18, 9)), CAST(7.461580000 AS Decimal(18, 9)), CAST(0x0740FA52AA6D0E3A0B AS DateTime2), CAST(0x0710F7819F6D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (13, CAST(0.729330000 AS Decimal(18, 9)), CAST(0.729300000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (14, CAST(312.480000000 AS Decimal(18, 9)), CAST(312.104000000 AS Decimal(18, 9)), CAST(0x0770FAD1E16D0E3A0B AS DateTime2), CAST(0x07F0239DE06D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (15, CAST(138.828000000 AS Decimal(18, 9)), CAST(138.818000000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (16, CAST(17.443000000 AS Decimal(18, 9)), CAST(17.438000000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (17, CAST(8.718200000 AS Decimal(18, 9)), CAST(8.716200000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (18, CAST(1.573320000 AS Decimal(18, 9)), CAST(1.573010000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (19, CAST(4.138300000 AS Decimal(18, 9)), CAST(4.137230000 AS Decimal(18, 9)), CAST(0x07C0976DE56D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (20, CAST(9.348360000 AS Decimal(18, 9)), CAST(9.344520000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (21, CAST(1.131290000 AS Decimal(18, 9)), CAST(1.131250000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (22, CAST(13.946600000 AS Decimal(18, 9)), CAST(13.942500000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (23, CAST(2.001250000 AS Decimal(18, 9)), CAST(2.000920000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (24, CAST(1.895690000 AS Decimal(18, 9)), CAST(1.895360000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (25, CAST(1.439480000 AS Decimal(18, 9)), CAST(1.439160000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (26, CAST(190.359000000 AS Decimal(18, 9)), CAST(190.342000000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07C0976DE56D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (27, CAST(11.955300000 AS Decimal(18, 9)), CAST(11.950700000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (28, CAST(2.157310000 AS Decimal(18, 9)), CAST(2.156770000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (29, CAST(1.551190000 AS Decimal(18, 9)), CAST(1.551120000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (30, CAST(15.125450000 AS Decimal(18, 9)), CAST(15.123400000 AS Decimal(18, 9)), CAST(0x0770CAF5DE4A80390B AS DateTime2), CAST(0x0770CAF5DE4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (31, CAST(1.072470000 AS Decimal(18, 9)), CAST(1.072100000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (32, CAST(88.253000000 AS Decimal(18, 9)), CAST(88.237000000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (33, CAST(0.966000000 AS Decimal(18, 9)), CAST(0.965700000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (34, CAST(0.719140000 AS Decimal(18, 9)), CAST(0.719050000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (35, CAST(88.078000000 AS Decimal(18, 9)), CAST(88.051500000 AS Decimal(18, 9)), CAST(0x0770CAF5DE4A80390B AS DateTime2), CAST(0x0770CAF5DE4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (36, CAST(1.222050000 AS Decimal(18, 9)), CAST(1.221940000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (37, CAST(0.928010000 AS Decimal(18, 9)), CAST(0.927870000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (38, CAST(24.160400000 AS Decimal(18, 9)), CAST(24.145000000 AS Decimal(18, 9)), CAST(0x0750E838E46D0E3A0B AS DateTime2), CAST(0x0750E838E46D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (39, CAST(7.752980000 AS Decimal(18, 9)), CAST(7.752890000 AS Decimal(18, 9)), CAST(0x0710CAA1526D0E3A0B AS DateTime2), CAST(0x0790C63B506D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (40, CAST(276.170000000 AS Decimal(18, 9)), CAST(275.930000000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07C0976DE56D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (41, CAST(3.825500000 AS Decimal(18, 9)), CAST(3.823000000 AS Decimal(18, 9)), CAST(0x0730EF99CE6D0E3A0B AS DateTime2), CAST(0x071055CFD56D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (42, CAST(122.719000000 AS Decimal(18, 9)), CAST(122.710000000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (43, CAST(15.418150000 AS Decimal(18, 9)), CAST(15.416060000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (44, CAST(7.707400000 AS Decimal(18, 9)), CAST(7.704560000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (45, CAST(3.658500000 AS Decimal(18, 9)), CAST(3.657000000 AS Decimal(18, 9)), CAST(0x07C0976DE56D0E3A0B AS DateTime2), CAST(0x07C0976DE56D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (46, CAST(8.263560000 AS Decimal(18, 9)), CAST(8.260740000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (47, CAST(1.343210000 AS Decimal(18, 9)), CAST(1.342980000 AS Decimal(18, 9)), CAST(0x07709805E36D0E3A0B AS DateTime2), CAST(0x0750E838E46D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (48, CAST(2.737450000 AS Decimal(18, 9)), CAST(2.737080000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (49, CAST(12.328300000 AS Decimal(18, 9)), CAST(12.325000000 AS Decimal(18, 9)), CAST(0x07900C6BDF6D0E3A0B AS DateTime2), CAST(0x07900C6BDF6D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (50, CAST(10.135000000 AS Decimal(18, 9)), CAST(10.127000000 AS Decimal(18, 9)), CAST(0x074012C2D74A80390B AS DateTime2), CAST(0x07507EC0CB4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (51, CAST(6.596330000 AS Decimal(18, 9)), CAST(6.595640000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (52, CAST(0.759440000 AS Decimal(18, 9)), CAST(0.759240000 AS Decimal(18, 9)), CAST(0x07C0976DE56D0E3A0B AS DateTime2), CAST(0x07C0976DE56D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (55, CAST(1.073485000 AS Decimal(18, 9)), CAST(1.073095000 AS Decimal(18, 9)), CAST(0x07D04B5EDB4A80390B AS DateTime2), CAST(0x07D04B5EDB4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (56, CAST(0.667380000 AS Decimal(18, 9)), CAST(0.667170000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (57, CAST(5.264770000 AS Decimal(18, 9)), CAST(5.264040000 AS Decimal(18, 9)), CAST(0x0730F28DDC4A80390B AS DateTime2), CAST(0x07702CC2DD4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (58, CAST(6.221015000 AS Decimal(18, 9)), CAST(6.212255000 AS Decimal(18, 9)), CAST(0x0790112ADA4A80390B AS DateTime2), CAST(0x0730F28DDC4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (59, CAST(3.097760000 AS Decimal(18, 9)), CAST(3.097280000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (60, CAST(0.878870000 AS Decimal(18, 9)), CAST(0.878660000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (61, CAST(6.665220000 AS Decimal(18, 9)), CAST(6.659775000 AS Decimal(18, 9)), CAST(0x074012C2D74A80390B AS DateTime2), CAST(0x074012C2D74A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (62, CAST(5.360700000 AS Decimal(18, 9)), CAST(5.360000000 AS Decimal(18, 9)), CAST(0x07804CF6D84A80390B AS DateTime2), CAST(0x07D04B5EDB4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (63, CAST(6.334650000 AS Decimal(18, 9)), CAST(6.326100000 AS Decimal(18, 9)), CAST(0x0790112ADA4A80390B AS DateTime2), CAST(0x07D04B5EDB4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (64, CAST(6.786875000 AS Decimal(18, 9)), CAST(6.781475000 AS Decimal(18, 9)), CAST(0x07702CC2DD4A80390B AS DateTime2), CAST(0x07702CC2DD4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (65, CAST(7.434860000 AS Decimal(18, 9)), CAST(7.421950000 AS Decimal(18, 9)), CAST(0x07702CC2DD4A80390B AS DateTime2), CAST(0x07702CC2DD4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (66, CAST(8.784365000 AS Decimal(18, 9)), CAST(8.758445000 AS Decimal(18, 9)), CAST(0x07702CC2DD4A80390B AS DateTime2), CAST(0x07702CC2DD4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (67, CAST(9.410415000 AS Decimal(18, 9)), CAST(9.392150000 AS Decimal(18, 9)), CAST(0x07702CC2DD4A80390B AS DateTime2), CAST(0x07702CC2DD4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (68, CAST(18.278825000 AS Decimal(18, 9)), CAST(18.275960000 AS Decimal(18, 9)), CAST(0x0770CAF5DE4A80390B AS DateTime2), CAST(0x0770CAF5DE4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (69, CAST(1.181650000 AS Decimal(18, 9)), CAST(1.180140000 AS Decimal(18, 9)), CAST(0x0730F28DDC4A80390B AS DateTime2), CAST(0x0730F28DDC4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (70, CAST(1.266060000 AS Decimal(18, 9)), CAST(1.265120000 AS Decimal(18, 9)), CAST(0x074012C2D74A80390B AS DateTime2), CAST(0x074012C2D74A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (71, CAST(10.231820000 AS Decimal(18, 9)), CAST(10.230680000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (72, CAST(12.818260000 AS Decimal(18, 9)), CAST(12.812560000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (73, CAST(8.020000000 AS Decimal(18, 9)), CAST(8.017000000 AS Decimal(18, 9)), CAST(0x0770CAF5DE4A80390B AS DateTime2), CAST(0x07D04B5EDB4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (74, CAST(15.485150000 AS Decimal(18, 9)), CAST(15.466300000 AS Decimal(18, 9)), CAST(0x07702CC2DD4A80390B AS DateTime2), CAST(0x07702CC2DD4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (75, CAST(4.994500000 AS Decimal(18, 9)), CAST(4.993500000 AS Decimal(18, 9)), CAST(0x074012C2D74A80390B AS DateTime2), CAST(0x07804CF6D84A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (76, CAST(5.901900000 AS Decimal(18, 9)), CAST(5.892900000 AS Decimal(18, 9)), CAST(0x07D04B5EDB4A80390B AS DateTime2), CAST(0x07D04B5EDB4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (77, CAST(6.323775000 AS Decimal(18, 9)), CAST(6.317805000 AS Decimal(18, 9)), CAST(0x0730F28DDC4A80390B AS DateTime2), CAST(0x0730F28DDC4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (78, CAST(6.362300000 AS Decimal(18, 9)), CAST(6.361500000 AS Decimal(18, 9)), CAST(0x074012C2D74A80390B AS DateTime2), CAST(0x07D04B5EDB4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (79, CAST(8.770990000 AS Decimal(18, 9)), CAST(8.770250000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (80, CAST(76.686230000 AS Decimal(18, 9)), CAST(72.272210000 AS Decimal(18, 9)), CAST(0x070022C7E625FF390B AS DateTime2), CAST(0x070022C7E625FF390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (81, CAST(37.476800000 AS Decimal(18, 9)), CAST(37.452100000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (82, CAST(428.380000000 AS Decimal(18, 9)), CAST(427.980000000 AS Decimal(18, 9)), CAST(0x07C0976DE56D0E3A0B AS DateTime2), CAST(0x07C0976DE56D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (83, CAST(61.832500000 AS Decimal(18, 9)), CAST(58.288500000 AS Decimal(18, 9)), CAST(0x07C06BB1224B0E3A0B AS DateTime2), CAST(0x07C06BB1224B0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (84, CAST(5.674500000 AS Decimal(18, 9)), CAST(5.672400000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (85, CAST(7.216850000 AS Decimal(18, 9)), CAST(7.214900000 AS Decimal(18, 9)), CAST(0x074012C2D74A80390B AS DateTime2), CAST(0x074012C2D74A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (86, CAST(6.210600000 AS Decimal(18, 9)), CAST(6.210200000 AS Decimal(18, 9)), CAST(0x0770FAD1E16D0E3A0B AS DateTime2), CAST(0x07708CC7C36D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (87, CAST(1.519550000 AS Decimal(18, 9)), CAST(1.519240000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (88, CAST(12.026300000 AS Decimal(18, 9)), CAST(12.025200000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (89, CAST(23.917000000 AS Decimal(18, 9)), CAST(23.910900000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (90, CAST(2.083600000 AS Decimal(18, 9)), CAST(2.083100000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (91, CAST(4.248250000 AS Decimal(18, 9)), CAST(4.246500000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (92, CAST(19.123100000 AS Decimal(18, 9)), CAST(19.117300000 AS Decimal(18, 9)), CAST(0x07D05CA1E66D0E3A0B AS DateTime2), CAST(0x07D05CA1E66D0E3A0B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (93, CAST(16.937700000 AS Decimal(18, 9)), CAST(16.909950000 AS Decimal(18, 9)), CAST(0x0790112ADA4A80390B AS DateTime2), CAST(0x0790112ADA4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (94, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (95, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (96, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (97, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (98, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (99, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (100, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (101, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (102, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (103, CAST(4.852195000 AS Decimal(18, 9)), CAST(4.834380000 AS Decimal(18, 9)), CAST(0x07703625F9715A390B AS DateTime2), CAST(0x07A0D856FA715A390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (104, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (105, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (106, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (107, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (108, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (109, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (110, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (111, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (112, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (113, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (114, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (115, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (116, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (117, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (118, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (119, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (120, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (121, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (122, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (123, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (124, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (125, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (126, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (127, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (128, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (129, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (130, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (131, CAST(3.887400000 AS Decimal(18, 9)), CAST(3.883700000 AS Decimal(18, 9)), CAST(0x074012C2D74A80390B AS DateTime2), CAST(0x0790112ADA4A80390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (132, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (133, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (134, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (135, CAST(32.810000000 AS Decimal(18, 9)), CAST(32.765000000 AS Decimal(18, 9)), CAST(0x0760986A146F5A390B AS DateTime2), CAST(0x0760986A146F5A390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (136, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (137, CAST(6776.200000000 AS Decimal(18, 9)), CAST(6775.500000000 AS Decimal(18, 9)), CAST(0x0710CFE4EF55B8390B AS DateTime2), CAST(0x0790F8AFEE55B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (138, CAST(17816.000000000 AS Decimal(18, 9)), CAST(17814.000000000 AS Decimal(18, 9)), CAST(0x07D0C27FE755B8390B AS DateTime2), CAST(0x07D0C27FE755B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (139, CAST(2059.600000000 AS Decimal(18, 9)), CAST(2059.100000000 AS Decimal(18, 9)), CAST(0x0790F8AFEE55B8390B AS DateTime2), CAST(0x0790F8AFEE55B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (140, CAST(2059.600000000 AS Decimal(18, 9)), CAST(2059.000000000 AS Decimal(18, 9)), CAST(0x07D0677FED55B8390B AS DateTime2), CAST(0x07D0677FED55B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (141, CAST(4325.900000000 AS Decimal(18, 9)), CAST(4325.400000000 AS Decimal(18, 9)), CAST(0x07D0677FED55B8390B AS DateTime2), CAST(0x07D0677FED55B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (142, CAST(12028.300000000 AS Decimal(18, 9)), CAST(12025.500000000 AS Decimal(18, 9)), CAST(0x07B0204BF255B8390B AS DateTime2), CAST(0x07B08217F155B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (143, CAST(12028.100000000 AS Decimal(18, 9)), CAST(12027.500000000 AS Decimal(18, 9)), CAST(0x07B08217F155B8390B AS DateTime2), CAST(0x07B0204BF255B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (144, CAST(5055.100000000 AS Decimal(18, 9)), CAST(5054.100000000 AS Decimal(18, 9)), CAST(0x07B08217F155B8390B AS DateTime2), CAST(0x0710CFE4EF55B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (145, CAST(3692.200000000 AS Decimal(18, 9)), CAST(3689.800000000 AS Decimal(18, 9)), CAST(0x07D0A4265055B8390B AS DateTime2), CAST(0x07101F45C255B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (146, CAST(19373.000000000 AS Decimal(18, 9)), CAST(19363.000000000 AS Decimal(18, 9)), CAST(0x07D0677FED55B8390B AS DateTime2), CAST(0x07500BE4DD55B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (147, CAST(5811.500000000 AS Decimal(18, 9)), CAST(5808.500000000 AS Decimal(18, 9)), CAST(0x0750A75F5D55B8390B AS DateTime2), CAST(0x07F0740AF054B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (148, CAST(1158.060000000 AS Decimal(18, 9)), CAST(1157.880000000 AS Decimal(18, 9)), CAST(0x07B08217F155B8390B AS DateTime2), CAST(0x07B08217F155B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (149, CAST(15.666000000 AS Decimal(18, 9)), CAST(15.657000000 AS Decimal(18, 9)), CAST(0x07902D4BEC55B8390B AS DateTime2), CAST(0x07D0C27FE755B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (150, CAST(1516.870000000 AS Decimal(18, 9)), CAST(1516.410000000 AS Decimal(18, 9)), CAST(0x07B08217F155B8390B AS DateTime2), CAST(0x07B08217F155B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (151, CAST(20.517000000 AS Decimal(18, 9)), CAST(20.506000000 AS Decimal(18, 9)), CAST(0x07902D4BEC55B8390B AS DateTime2), CAST(0x07D0C27FE755B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (152, CAST(47.010000000 AS Decimal(18, 9)), CAST(46.960000000 AS Decimal(18, 9)), CAST(0x07B0204BF255B8390B AS DateTime2), CAST(0x07B08217F155B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (153, CAST(55.100000000 AS Decimal(18, 9)), CAST(55.050000000 AS Decimal(18, 9)), CAST(0x07B0204BF255B8390B AS DateTime2), CAST(0x07B08217F155B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (154, CAST(1115.900000000 AS Decimal(18, 9)), CAST(1114.700000000 AS Decimal(18, 9)), CAST(0x07902D4BEC55B8390B AS DateTime2), CAST(0x07C0F0E0D155B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (155, CAST(788.600000000 AS Decimal(18, 9)), CAST(787.100000000 AS Decimal(18, 9)), CAST(0x0780859AC954B8390B AS DateTime2), CAST(0x07D0C27FE755B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (156, CAST(1098.440000000 AS Decimal(18, 9)), CAST(1098.180000000 AS Decimal(18, 9)), CAST(0x07B08217F155B8390B AS DateTime2), CAST(0x07B08217F155B8390B AS DateTime2))
GO
INSERT [dbo].[Symbol_Rates] ([Id], [LastKnownAskMeanPrice], [LastKnownBidMeanPrice], [AskMeanPriceUpdated], [BidMeanPriceUpdated]) VALUES (157, CAST(2.753000000 AS Decimal(18, 9)), CAST(2.749000000 AS Decimal(18, 9)), CAST(0x0790F8AFEE55B8390B AS DateTime2), CAST(0x0790F8AFEE55B8390B AS DateTime2))
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (0, CAST(4.5882 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (1, CAST(6.0791 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (2, CAST(3.2488 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (3, CAST(16.3814 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (4, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (5, CAST(2.5890 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (6, CAST(4.0900 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (7, CAST(4.0658 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (8, CAST(3.0424 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (9, CAST(4.1781 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (10, CAST(3.4633 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (11, CAST(21.7116 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (12, CAST(3.4457 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (13, CAST(2.4020 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (14, CAST(25.6134 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (15, CAST(1.5099 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (16, CAST(5.7878 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (17, CAST(14.9721 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (18, CAST(4.5831 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (19, CAST(25.6844 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (20, CAST(16.0525 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (21, CAST(1.9505 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (22, CAST(10.0761 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (23, CAST(2.9645 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (24, CAST(4.1526 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (25, CAST(5.4310 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (26, CAST(1.9725 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (27, CAST(13.7637 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (28, CAST(4.8866 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (29, CAST(1.7953 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (30, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (31, CAST(24.0413 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (32, CAST(4.5908 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (33, CAST(10.8476 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (34, CAST(4.6815 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (35, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (36, CAST(3.1804 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (37, CAST(4.1986 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (38, CAST(17.8402 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (39, CAST(1.1463 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (40, CAST(28.7567 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (41, CAST(39.1754 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (42, CAST(1.4798 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (43, CAST(8.0599 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (44, CAST(13.9266 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (45, CAST(23.6481 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (46, CAST(12.9144 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (47, CAST(7.0203 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (48, CAST(16.3751 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (49, CAST(10.8048 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (50, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (51, CAST(3.5293 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (52, CAST(9.2240 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (55, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (56, CAST(6.9465 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (57, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (58, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (59, CAST(18.8512 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (60, CAST(6.5354 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (61, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (62, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (63, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (64, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (65, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (66, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (67, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (68, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (69, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (70, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (71, CAST(4.0913 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (72, CAST(12.5735 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (73, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (74, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (75, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (76, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (77, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (78, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (79, CAST(2.3927 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (80, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (81, CAST(20.4266 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (82, CAST(22.9258 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (83, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (84, CAST(18.6409 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (85, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (86, CAST(2.4449 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (87, CAST(6.8002 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (88, CAST(2.4951 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (89, CAST(5.8580 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (90, CAST(6.2052 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (91, CAST(16.7534 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (92, CAST(10.1307 AS Decimal(18, 4)), CAST(-0.0001 AS Decimal(18, 4)), 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (93, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (94, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (95, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (96, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (97, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (98, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (99, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (100, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (101, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (102, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (103, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (104, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (105, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (106, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (107, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (108, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (109, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (110, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (111, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (112, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (113, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (114, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (115, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (116, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (117, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (118, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (119, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (120, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (121, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (122, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (123, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (124, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (125, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (126, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (127, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (128, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (129, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (130, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (131, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (132, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (133, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (134, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (135, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (136, NULL, NULL, 300000)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (137, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (138, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (139, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (140, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (141, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (142, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (143, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (144, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (145, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (146, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (147, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (148, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (149, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (150, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (151, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (152, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (153, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (154, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (155, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (156, NULL, NULL, NULL)
GO
INSERT [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds]) VALUES (157, NULL, NULL, NULL)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (1, N'Bank of America Merrill Lynch                     ', N'BOA', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (2, N'Citibank                                          ', N'CTI', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (3, N'Royal Bank of Scotland                            ', N'RBS', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (4, N'JPMorgan Chase                                    ', N'JPM', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (5, N'Goldman Sachs                                     ', N'GLS', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (6, N'Deutsche Bank                                     ', N'DBK', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (7, N'UBS                                               ', N'UBS', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (8, N'Commerzbank                                       ', N'CZB', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (9, N'Rabobank                                          ', N'RAB', 0)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (10, N'Morgan Stanley                                    ', N'MGS', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (11, N'Natixis                                           ', N'NTX', 0)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (12, N'Nomura                                            ', N'NOM', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (13, N'Credit Suisse                                     ', N'CRS', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (14, N'BNP Paribas                                       ', N'BNP', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (15, N'Barclays                                          ', N'BRX', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (16, N'CAPH                                              ', N'CPH', 0)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (17, N'Societe Generale                                  ', N'SOC', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (18, N'Last Atlantis                                     ', N'LTA', 0)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (19, N'HSBC                                              ', N'HSB', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (20, N'Hotspot                                           ', N'HTA', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (21, N'Hotspot                                           ', N'HTF', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (22, N'FXall                                             ', N'FA1', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (23, N'LMAX                                              ', N'L01', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (24, N'LMAX                                              ', N'LM2', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (25, N'FXCM                                              ', N'FC1', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (26, N'FXCM                                              ', N'FC2', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (27, N'LMAX                                              ', N'LM3', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (28, N'Hotspot                                           ', N'HT3', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (29, N'LMAX                                              ', N'L02', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (30, N'LMAX                                              ', N'L03', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (31, N'LMAX                                              ', N'L04', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (32, N'LMAX                                              ', N'L05', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (33, N'LMAX                                              ', N'L06', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (34, N'LMAX                                              ', N'L07', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (35, N'LMAX                                              ', N'L08', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (36, N'LMAX                                              ', N'L09', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (37, N'LMAX                                              ', N'L10', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (38, N'LMAX                                              ', N'L11', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (39, N'LMAX                                              ', N'L12', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (40, N'LMAX                                              ', N'L13', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (41, N'LMAX                                              ', N'L14', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (42, N'LMAX                                              ', N'L15', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (43, N'LMAX                                              ', N'L16', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (44, N'LMAX                                              ', N'L17', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (45, N'LMAX                                              ', N'L18', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (46, N'LMAX                                              ', N'L19', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (47, N'LMAX                                              ', N'L20', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (48, N'FXCM                                              ', N'FS1', 1)
GO
INSERT [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (49, N'FXCM                                              ', N'FS2', 1)
GO
INSERT [dbo].[DealDirection] ([Id], [Name]) VALUES (0, N'Buy')
GO
INSERT [dbo].[DealDirection] ([Id], [Name]) VALUES (1, N'Sell')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (0, N'NotSubmitted')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (1, N'Submitted')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (2, N'Filled')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (3, N'Rejected')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (4, N'InBrokenState')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (5, N'PendingCancel')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (6, N'Cancelled')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (7, N'CancelRejected')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (8, N'PartiallyFilled')
GO
INSERT [dbo].[FlowSide] ([FlowSideId], [FlowSideName]) VALUES (0, N'LiquidityMaker')
GO
INSERT [dbo].[FlowSide] ([FlowSideId], [FlowSideName]) VALUES (1, N'LiquidityTaker')
GO
INSERT [dbo].[OrderTimeInForce] ([OrderTimeInForceId], [OrderTimeInForceName]) VALUES (1, N'Day')
GO
INSERT [dbo].[OrderTimeInForce] ([OrderTimeInForceId], [OrderTimeInForceName]) VALUES (0, N'ImmediateOrKill')
GO
INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (1, N'Limit')
GO
INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (3, N'Market')
GO
INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (4, N'MarketOnImprovement')
GO
INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (5, N'MarketPreferLmax')
GO
INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (2, N'Pegged')
GO
INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (0, N'Quoted')
GO
INSERT [dbo].[PegType] ([PegTypeId], [PegTypeName]) VALUES (0, N'MarketPeg')
GO
INSERT [dbo].[PegType] ([PegTypeId], [PegTypeName]) VALUES (1, N'PrimaryPeg')
GO

