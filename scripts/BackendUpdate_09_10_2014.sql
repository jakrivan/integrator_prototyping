UPDATE
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], 'HeartBtInt=2', 'HeartBtInt=2
BypassParsing=Y')
WHERE
	[Name] like 'LM_[_]%'
	AND [Configuration] NOT like '%BypassParsing%'
	AND [Configuration] like '%HeartBtInt=2%'


UPDATE
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], 'DataDictionary=FIX44_LMX.xml', '#DataDictionary=FIX44_LMX.xml
UseDataDictionary=N')
WHERE
	[Name] like 'LM_[_]%'
	AND [Configuration] like '%BypassParsing%'
	AND [Configuration] like '%DataDictionary=FIX44_LMX.xml%'