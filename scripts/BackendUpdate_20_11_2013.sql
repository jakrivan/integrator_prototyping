
ALTER TABLE [dbo].[ExternalOrdersTransmissionSwitchHistory] ADD Reason NVARCHAR(MAX) NULL
GO


ALTER PROCEDURE [dbo].[DisableExternalOrdersTransmission_SP] 
	@SwitchName NVARCHAR(64),
	@ChangedBy NVARCHAR(256),
	@ChangedFrom NVARCHAR(256),
	@Reason NVARCHAR(MAX) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SwitchId INT 
	SELECT @SwitchId = [Id] FROM [dbo].[ExternalOrdersTransmissionSwitch] WHERE [SwitchName] = @SwitchName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Switch not found in DB: %s', @SwitchName, 16, 2) WITH SETERROR
	END

	SET @Reason = ISNULL(@Reason, 'Changed per request by ' + @ChangedBy)

	BEGIN TRANSACTION

	INSERT INTO [dbo].[ExternalOrdersTransmissionSwitchHistory]
           ([ExternalOrdersTransmissionSwitchId]
           ,[TransmissionEnabled]
           ,[Changed]
           ,[ChangedBy]
           ,[ChangedFrom]
		   ,[Reason])
     VALUES
           (@SwitchId
           ,0
           ,GETUTCDATE()
           ,@ChangedBy
		   ,@ChangedFrom
		   ,@Reason)

	UPDATE [dbo].[ExternalOrdersTransmissionSwitch]
	SET [TransmissionEnabled] = 0
	WHERE [Id] = @SwitchId

	COMMIT TRANSACTION
END
GO


ALTER PROCEDURE [dbo].[GetRecentExternalOrdersTransmissionControlState_SP] 
	@SwitchName NVARCHAR(64),
	@ChangedBy NVARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SwitchId INT 
	SELECT @SwitchId = [Id] FROM [dbo].[ExternalOrdersTransmissionSwitch] WHERE [SwitchName] = @SwitchName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Switch not found in DB: %s', @SwitchName, 16, 2) WITH SETERROR
	END


	SELECT TOP 10
		[TransmissionEnabled]
		,[Changed]
		,[ChangedBy]
		,[ChangedFrom]
		,[Reason]
	FROM 
		[dbo].[ExternalOrdersTransmissionSwitchHistory]
	WHERE
		[ExternalOrdersTransmissionSwitchId] = @SwitchId
		AND	@ChangedBy IS NULL OR [ChangedBy] = @ChangedBy
	ORDER BY
		[Changed] DESC

END
GO


ALTER PROCEDURE [dbo].[GetNopAbsTotal_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		symbol.[Id] AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.Price) AS NopTermPol
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Id]


	SELECT
		CASE WHEN LongsNop > ShortsNop THEN LongsNop  ELSE ShortsNop END AS NopAbs
	FROM
	(
		SELECT
			SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS LongsNop
			,SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS ShortsNop
		FROM
			(
			SELECT
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
			FROM
				(
				SELECT 
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CCYPositions.CCY
			) AS CtpCCYPositions
	) nops

END
GO