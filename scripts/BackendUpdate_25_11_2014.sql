
DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd] AS N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="DiagnosticsDashboardSettings" nillable="true" type="DiagnosticsDashboardSettings" />
  <xs:complexType name="DiagnosticsDashboardSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RefreshTimeout_Miliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="FirstSpreadAgeLimit_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="SecondSpreadAgeLimit_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ThirdSpreadAgeLimit_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="CutOffSpreadAgeLimit_Hours" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="SessionActionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="EnabledSymbols" type="ArrayOfString" />
      <xs:element minOccurs="1" maxOccurs="1" name="EnabledCounterparties" type="ArrayOfString1" />
      <xs:element minOccurs="1" maxOccurs="1" name="EnabledCounterpartyInfoRowTypes" type="ArrayOfCounterpartyInfoRowType" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString1">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartyInfoRowType">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartyInfoRowType" type="CounterpartyInfoRowType" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="CounterpartyInfoRowType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Active" />
      <xs:enumeration value="MarketData" />
      <xs:enumeration value="OrderFlow" />
      <xs:enumeration value="SessionsAndDealsDividerRow" />
      <xs:enumeration value="Deals" />
      <xs:enumeration value="FirstDailyInfoRow" />
      <xs:enumeration value="KGTRejections" />
      <xs:enumeration value="CtpRejections" />
      <xs:enumeration value="KGTRejectionRate" />
      <xs:enumeration value="CtpRejectionRate" />
      <xs:enumeration value="AvgDeal" />
      <xs:enumeration value="Volume" />
      <xs:enumeration value="VolumeShare" />
      <xs:enumeration value="KgtAvgLat" />
      <xs:enumeration value="CounterpartyAvgLat" />
      <xs:enumeration value="LastDailyInfoRow" />
      <xs:enumeration value="SpreadRank" />
      <xs:enumeration value="DealsAndSymbolsDividerRow" />
      <xs:enumeration value="MaximumItems" />
    </xs:restriction>
  </xs:simpleType>
</xs:schema>'
GO


DISABLE TRIGGER [SchemaXsdCheckTrigger] ON [dbo].[Settings]
GO

UPDATE [dbo].[Settings]
SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '>Rejections<', '>CtpRejections<')
WHERE
	[Name] = 'Kreslik.Integrator.DiagnosticsDashboard.exe'
	AND CAST([SettingsXml] AS NVARCHAR(MAX)) like '%>Rejections<%'

UPDATE [dbo].[Settings]
SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '>RejectionRate<', '>CtpRejectionRate<')
WHERE
	[Name] = 'Kreslik.Integrator.DiagnosticsDashboard.exe'
	AND CAST([SettingsXml] AS NVARCHAR(MAX)) like '%>RejectionRate<%'
GO
	
ENABLE TRIGGER [SchemaXsdCheckTrigger] ON [dbo].[Settings]
GO

--dummy change to force xsd check
UPDATE [dbo].[Settings]
SET [SettingsXml] = [SettingsXml]
WHERE
	[Name] = 'Kreslik.Integrator.DiagnosticsDashboard.exe'


DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_DBK" type="FIXChannel_DBKSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM2" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM3" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC1" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC2" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS1" type="FIXChannel_FCMMMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
      <xs:element minOccurs="1" maxOccurs="1" name="STP" type="STPSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_DBKSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DealConfirmationSupported" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumDealConfirmationDelay_Milliseconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IocApplyTtl" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ApplyMaxHoldTime" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxHoldTime" type="FXCMMaxHoldTime" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="RejectionRateDisconnectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumOrdersToApplyCheck" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAllowedRejectionRatePercent" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMMMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxOrderHoldTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxPricesPerSecond" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExpectedPricingDepth" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="RejectionRateDisconnectSettings" type="RejectionRateDisconnectSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXSTPChannel_CTIPBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNongenuineInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialRejectionsHandling" type="ArrayOfSpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SpecialRejection" type="SpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="TriggerPhrase" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="FXCMMaxHoldTime">
    <xs:restriction base="xs:string">
      <xs:enumeration value="UpTo1ms" />
      <xs:enumeration value="UpTo30ms" />
      <xs:enumeration value="UpTo100ms" />
      <xs:enumeration value="UpTo500ms" />
      <xs:enumeration value="UpTo3000ms" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="MarketSessionIntoSeparateProcess" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="STPDestinations" type="ArrayOfSTPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSTPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="STPDestination" nillable="true" type="STPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Name" type="STPCounterparty" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedTicketTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="ServedCounterparties" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="STPCounterparty">
    <xs:restriction base="xs:string">
      <xs:enumeration value="CTIPB" />
      <xs:enumeration value="Traiana" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO


DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd] AS N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="BusinessLayerSettings" nillable="true" type="BusinessLayerSettings" />
  <xs:complexType name="BusinessLayerSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MarketableClientOrdersMatchingStrategy" type="MarketableClientOrdersMatchingStrategy" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="ConsecutiveRejectionsStopStrategyBehavior" type="ConsecutiveRejectionsStopStrategySettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartWithSessionsStopped" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="AutoKillInternalOrdersOnOrdersTransmissionDisabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailSubject" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailBody" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalRejectedOrderTriggerPhrasesCsv" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderCounterpartyContacts" type="ArrayOfIgnoredOrderCounterpartyContact" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PreventMatchingToSameCounterpartyAfterOrderRejectBehavior" type="PreventMatchingToSameCounterpartyAfterOrderRejectSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeGapsWatchingBehavior" type="TimeGapsWatchingSettings" /> 
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="MarketableClientOrdersMatchingStrategy">
    <xs:restriction base="xs:string">
      <xs:enumeration value="ImmediateMatching" />
      <xs:enumeration value="NextFreshPriceMatching" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ConsecutiveRejectionsStopStrategySettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="StrategyEnabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="NumerOfRejectionsToTriggerStrategy" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAgeRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceBpRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultCounterpartyRank" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRanks" type="ArrayOfCounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartyRank">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartyRank" nillable="true" type="CounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartyRank">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="Rank" type="xs:decimal" />
  </xs:complexType>
  <xs:complexType name="ArrayOfIgnoredOrderCounterpartyContact">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="IgnoredOrderCounterpartyContact" nillable="true" type="IgnoredOrderCounterpartyContact" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="IgnoredOrderCounterpartyContact">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="EmailContacts" type="xs:string" />
  </xs:complexType>
  <xs:complexType name="PreventMatchingToSameCounterpartyAfterOrderRejectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="StrategyEnabled" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartyCodes" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TimeGapsWatchingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="LogFatalsDuringOffBusinessHours" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumTimeGapToWatchFor_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumTimeGapToLogFatal_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="EnableWatchingForGapsClusters" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="NumberOfGapsInClusterToLogFatal" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ClusterDuration_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

-- Update manually SessionSettings.xml:
--add:
--<FIXChannel_FS1>
--	<MaxOrderHoldTime_Milliseconds>300</MaxOrderHoldTime_Milliseconds>
--	<MaxPricesPerSecond>200</MaxPricesPerSecond>
--</FIXChannel_FS1>
--and add FC1 to counterparts settings and citipb destinations - basicaly everywhere where we have fa1


-- Update manually Kreslik.Integrator.BusinessLayer.dll.xml:
--add:

--<TimeGapsWatchingBehavior>
--	<LogFatalsDuringOffBusinessHours>false</LogFatalsDuringOffBusinessHours>
--	<MinimumTimeGapToWatchFor_Milliseconds>20</MinimumTimeGapToWatchFor_Milliseconds>
--	<MinimumTimeGapToLogFatal_Milliseconds>200</MinimumTimeGapToLogFatal_Milliseconds>
--	<EnableWatchingForGapsClusters>true</EnableWatchingForGapsClusters>
--	<NumberOfGapsInClusterToLogFatal>10</NumberOfGapsInClusterToLogFatal>
--	<ClusterDuration_Seconds>600</ClusterDuration_Seconds>
--</TimeGapsWatchingBehavior>





ALTER PROCEDURE [dbo].[GetPolarizedNopPerPair_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[AmountBasePolExecuted]) AS PolarizedNop
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[IntegratorReceivedExecutionReportUtc] BETWEEN @StartTime AND @EndTime
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]
END
GO

DROP VIEW [dbo].[DealExternalExecuted_View]
GO

CREATE TABLE [dbo].[OrderExternalIncomingTimeoutsFromCounterparty](
	[IntegratorReceivedTimeoutUtc] [datetime2](7) NOT NULL,
	[CounterpartySentTimeoutUtc] [datetime2](7) NOT NULL,
	[CounterpartyClientOrderId] [varchar](32) NOT NULL
) ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [IX_OrderExternalIncomingTimeoutsFromCounterparty_IntegratorReceivedTimeoutUtc] ON [dbo].[OrderExternalIncomingTimeoutsFromCounterparty]
(
	[IntegratorReceivedTimeoutUtc] ASC
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[OrderExternalIncomingRejectable_Daily](
	[IntegratorReceivedExternalOrderUtc] [datetime2](7) NOT NULL,
	[CounterpartySentExternalOrderUtc] [datetime2](7) NOT NULL,
	[IntegratorSentExecutionReportUtc] [datetime2](7) NOT NULL,
	[CounterpartyClientOrderId] [varchar](32) NOT NULL,
	[TradingSystemId] [int] NULL,
	[SourceIntegratorPriceIdentity] [varchar](20) NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[IntegratorDealDirectionId] [bit] NOT NULL,
	[CounterpartyRequestedPrice] [decimal](18, 9) NOT NULL,
	[CounterpartyRequestedAmountBasePol] [decimal](18, 2) NOT NULL,
	[IntegratorExecutionId] [varchar](20) NOT NULL,
	[ValueDate] [date] NULL,
	[OrderReceivedToExecutionReportSentLatency] [time](7) NOT NULL,
	[IntegratorExecutedAmountBasePol] [decimal](18, 2) NOT NULL,
	[IntegratorRejectedAmountBasePol] [decimal](18, 2) NOT NULL,
	[IntegratorRejectionReason] [varchar](32) NULL,
	[IntegratorLatencyNanoseconds] AS
	CAST(
			(
				(datepart(HOUR, [OrderReceivedToExecutionReportSentLatency])*60 + datepart(MINUTE, [OrderReceivedToExecutionReportSentLatency]))*60 
				+ datepart(SECOND, [OrderReceivedToExecutionReportSentLatency])
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, [OrderReceivedToExecutionReportSentLatency]) PERSISTED,
	[DatePartIntegratorReceivedOrderUtc]  AS (CONVERT([date],[IntegratorReceivedExternalOrderUtc])) PERSISTED
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_TradingSystem] FOREIGN KEY([TradingSystemId])
REFERENCES [dbo].[TradingSystem] ([TradingSystemId])
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_TradingSystem]
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_Counterparty]
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_DealDirection] FOREIGN KEY([IntegratorDealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_DealDirection]
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_Symbol]
GO


CREATE CLUSTERED INDEX [IX_OrderExternalIncomingRejectable_Daily_IntegratorReceivedExternalOrderUtc] ON [dbo].[OrderExternalIncomingRejectable_Daily]
(
	[IntegratorReceivedExternalOrderUtc] ASC
) ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[OrderExternalIncomingRejectable_Daily]
(
	[DatePartIntegratorReceivedOrderUtc] ASC,
	[CounterpartyId] ASC,
	[SymbolId] ASC
)
INCLUDE ( 	[IntegratorExecutedAmountBasePol],
	[IntegratorRejectedAmountBasePol],
	[CounterpartyRequestedPrice],
	[IntegratorLatencyNanoseconds]) ON [PRIMARY]
GO


CREATE TABLE [dbo].[OrderExternalIncomingRejectable_Archive](
	[IntegratorReceivedExternalOrderUtc] [datetime2](7) NOT NULL,
	[CounterpartySentExternalOrderUtc] [datetime2](7) NOT NULL,
	[IntegratorSentExecutionReportUtc] [datetime2](7) NOT NULL,
	[CounterpartyClientOrderId] [varchar](32) NOT NULL,
	[TradingSystemId] [int] NULL,
	[SourceIntegratorPriceIdentity] [varchar](20) NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[IntegratorDealDirectionId] [bit] NOT NULL,
	[CounterpartyRequestedPrice] [decimal](18, 9) NOT NULL,
	[CounterpartyRequestedAmountBasePol] [decimal](18, 2) NOT NULL,
	[IntegratorExecutionId] [varchar](20) NOT NULL,
	[ValueDate] [date] NULL,
	[OrderReceivedToExecutionReportSentLatency] [time](7) NOT NULL,
	[IntegratorExecutedAmountBasePol] [decimal](18, 2) NOT NULL,
	[IntegratorRejectedAmountBasePol] [decimal](18, 2) NOT NULL,
	[IntegratorRejectionReason] [varchar](32) NULL,
	[IntegratorLatencyNanoseconds] AS
	CAST(
			(
				(datepart(HOUR, [OrderReceivedToExecutionReportSentLatency])*60 + datepart(MINUTE, [OrderReceivedToExecutionReportSentLatency]))*60 
				+ datepart(SECOND, [OrderReceivedToExecutionReportSentLatency])
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, [OrderReceivedToExecutionReportSentLatency]) PERSISTED,
	[DatePartIntegratorReceivedOrderUtc]  AS (CONVERT([date],[IntegratorReceivedExternalOrderUtc])) PERSISTED
) ON [LargeSlowDataFileGroup]
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_TradingSystem] FOREIGN KEY([TradingSystemId])
REFERENCES [dbo].[TradingSystem] ([TradingSystemId])
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_TradingSystem]
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_Counterparty]
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_DealDirection] FOREIGN KEY([IntegratorDealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_DealDirection]
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_Symbol]
GO


CREATE CLUSTERED INDEX [IX_OrderExternalIncomingRejectable_Archive_IntegratorReceivedExternalOrderUtc] ON [dbo].[OrderExternalIncomingRejectable_Archive]
(
	[IntegratorReceivedExternalOrderUtc] ASC
) ON [LargeSlowDataFileGroup]
GO


CREATE PROCEDURE [dbo].[Daily_ArchiveRejectableOrders_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[OrderExternalIncomingRejectable_Archive]
			([IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReason])
		SELECT
			[IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReason]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]	
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[OrderExternalIncomingRejectable_Daily]
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

USE msdb;
GO

EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'ArchiveRejectableOrders',
    @subsystem = N'TSQL',
    @command = N'exec Daily_ArchiveRejectableOrders_SP',
	@database_name = N'Integrator',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO

--MANUALY reaorder the steps - there is no other documented way than doing this through SSMS


USE INTEGRATOR;
GO

CREATE TABLE [dbo].[RejectionDirection](
	[RejectionDirectionId] [bit] NOT NULL,
	[RejectionDirectionDescription] [varchar](16) NOT NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_RejectionDirection] ON [dbo].[RejectionDirection]
(
	[RejectionDirectionId] ASC
)
GO

INSERT INTO [dbo].[RejectionDirection] ([RejectionDirectionId], [RejectionDirectionDescription]) VALUES (0, 'CtpRejectd')
INSERT INTO [dbo].[RejectionDirection] ([RejectionDirectionId], [RejectionDirectionDescription]) VALUES (1, 'KGTRejectd')
GO

ALTER TABLE [dbo].[TradingSystemDealsRejected_Daily] ADD [RejectionDirectionId] [bit] NULL
ALTER TABLE [dbo].[TradingSystemDealsRejected_Archive] ADD [RejectionDirectionId] [bit] NULL
GO

UPDATE [dbo].[TradingSystemDealsRejected_Daily] SET [RejectionDirectionId] = 0
UPDATE [dbo].[TradingSystemDealsRejected_Archive] SET [RejectionDirectionId] = 0
GO

ALTER TABLE [dbo].[TradingSystemDealsRejected_Daily] ALTER COLUMN [RejectionDirectionId] [bit] NOT NULL
ALTER TABLE [dbo].[TradingSystemDealsRejected_Archive] ALTER COLUMN [RejectionDirectionId] [bit] NOT NULL
GO

ALTER PROCEDURE [dbo].[Daily_ArchiveTradingSystemRejections_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[TradingSystemDealsRejected_Archive]
			([TradingSystemId],
			[IntegratorExternalOrderPrivateId],
			[AmountBasePolRejected],
			[RejectionDirectionId],
			[DatePartDealRejected])
		SELECT
			[TradingSystemId],
			[IntegratorExternalOrderPrivateId],
			[AmountBasePolRejected],
			[RejectionDirectionId],
			DATEADD(DAY, -1, GETUTCDATE())
		FROM
			[dbo].[TradingSystemDealsRejected_Daily]	
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[TradingSystemDealsRejected_Daily]
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 2),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@RejectedPrice decimal(18, 9),
	@SingleRejectionSystemId int = NULL,
	@MultipleRejectionsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'CtpRejectd'

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END

	BEGIN TRY
	
		BEGIN TRANSACTION;
		
		IF @MultipleRejectionsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@RejectedPrice
				,@RejectionReason
				,@InternalOrderId
				,@RejectionDirectionId
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleRejectionsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external rejection for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId])
			VALUES
				(ISNULL(@SingleRejectionSystemId, -1)
				,@AmountBasePolRejected
				,@SymbolId
				,@CounterpartyId
				,@RejectedPrice
				,@RejectionReason
				,@InternalOrderId
				,@RejectionDirectionId)
		END

		INSERT INTO 
		[dbo].[DealExternalRejected]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[SymbolId]
			   ,[CounterpartyId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolRejected]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[RejectionReason]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[RejectedPrice])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@SymbolId
			   ,@CounterpartyId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolRejected
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@RejectionReason
			   ,@CounterpartySentExecutionReportUtc
			   ,@RejectedPrice)
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

CREATE PROCEDURE [dbo].[InsertNewOrderExternalIncomingRejectableTimeout_SP]( 
    @TimeoutReceivedUtc [datetime2](7),
	@TimeoutSentUtc [datetime2](7),
	@CounterpartyClientOrderId [varchar](32)
	)
AS
BEGIN

	INSERT INTO 
		[dbo].[OrderExternalIncomingTimeoutsFromCounterparty]
		   ([IntegratorReceivedTimeoutUtc]
		   ,[CounterpartySentTimeoutUtc]
		   ,[CounterpartyClientOrderId])
	 VALUES
		   (@TimeoutReceivedUtc
		   ,@TimeoutSentUtc
		   ,@CounterpartyClientOrderId)  
END
GO

GRANT EXECUTE ON [dbo].[InsertNewOrderExternalIncomingRejectableTimeout_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


CREATE PROCEDURE [dbo].[InsertNewOrderExternalIncomingRejectable_SP]( 
    @ExternalOrderReceived [datetime2](7),
	@ExternalOrderSentUtc [datetime2](7),
	@IntegratorSentExecutionReportUtc [datetime2](7),
	@CounterpartyClientOrderId [varchar](32),
	@TradingSystemId int = NULL,
	@SourceIntegratorPriceIdentity [varchar](20) = NULL,
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@IntegratorExecId [varchar](20),
	@ValueDate [date] = NULL,
	@OrderReceivedToExecutionReportSentLatency [time](7),
	@IntegratorExecutedAmountBaseAbs [decimal](18, 2) = NULL,
	@IntegratorRejectedAmountBaseAbs [decimal](18, 2) = NULL,
	@RejectionReason [varchar](32) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WITH(NOLOCK) WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WITH(NOLOCK) WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WITH(NOLOCK) WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @IntegratorExecutedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,@IntegratorExecId)
		END
		
		IF @IntegratorRejectedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,ISNULL(@RejectionReason, '<NONE>')
				,@IntegratorExecId
				,@RejectionDirectionId)
		END

		INSERT INTO 
			[dbo].[OrderExternalIncomingRejectable_Daily]
			   ([IntegratorReceivedExternalOrderUtc]
			   ,[CounterpartySentExternalOrderUtc]
			   ,[IntegratorSentExecutionReportUtc]
			   ,[CounterpartyClientOrderId]
			   ,[TradingSystemId]
			   ,[SourceIntegratorPriceIdentity]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorDealDirectionId]
			   ,[CounterpartyRequestedPrice]
			   ,[CounterpartyRequestedAmountBasePol]
			   ,[IntegratorExecutionId]
			   ,[ValueDate]
			   ,[OrderReceivedToExecutionReportSentLatency ]
			   ,[IntegratorExecutedAmountBasePol]
			   ,[IntegratorRejectedAmountBasePol]
			   ,[IntegratorRejectionReason])
		 VALUES
			   (@ExternalOrderReceived
			   ,@ExternalOrderSentUtc
			   ,@IntegratorSentExecutionReportUtc
			   ,@CounterpartyClientOrderId
			   ,@TradingSystemId
			   ,@SourceIntegratorPriceIdentity
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@DealDirectionId
			   ,@RequestedPrice
			   ,CASE WHEN @DealDirectionId = 1 THEN -@RequestedAmountBaseAbs ELSE @RequestedAmountBaseAbs END
			   ,@IntegratorExecId
			   ,@ValueDate
			   ,@OrderReceivedToExecutionReportSentLatency 
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
		       ,@RejectionReason)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH	   
		   	   
END
GO

GRANT EXECUTE ON [dbo].[InsertNewOrderExternalIncomingRejectable_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


CREATE PROCEDURE [dbo].[RejectExecutedOrderExternalIncomingRejectable_SP]( 
	@IntegratorExecId  [varchar](20),
	@RejectionReason [varchar](20) = NULL
	)
AS
BEGIN
	DECLARE @OriginallyExecutedAmount decimal(18,2)
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT 
		@OriginallyExecutedAmount = [IntegratorExecutedAmountBasePol] 
	FROM 
		[dbo].[OrderExternalIncomingRejectable_Daily]
	WHERE 
		[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) AND [IntegratorExecutionId] = @IntegratorExecId

	IF @OriginallyExecutedAmount IS NOT NULL
	BEGIN
	
		BEGIN TRY

		BEGIN TRANSACTION;
	
			UPDATE
				[dbo].[OrderExternalIncomingRejectable_Daily]
			SET
				[IntegratorRejectedAmountBasePol] += @OriginallyExecutedAmount,
				[IntegratorExecutedAmountBasePol] = 0,
				[IntegratorRejectionReason] = @RejectionReason
			WHERE 
				[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) 
				AND [IntegratorExecutionId] = @IntegratorExecId

				
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId])
			SELECT
				[TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,ISNULL(@RejectionReason, '<NONE>')
				,[InternalTransactionIdentity]
				,@RejectionDirectionId
			FROM
				[dbo].[TradingSystemDealsExecuted_Daily]
			WHERE
				[InternalTransactionIdentity] = @IntegratorExecId
					
			DELETE FROM
				[dbo].[TradingSystemDealsExecuted_Daily]
			WHERE
				[InternalTransactionIdentity] = @IntegratorExecId
			
		COMMIT TRANSACTION;
		
		SELECT 
			[IntegratorReceivedExternalOrderUtc]
			,[CounterpartySentExternalOrderUtc]
			,[CounterpartyClientOrderId]
			,[TradingSystemId]
			,[SourceIntegratorPriceIdentity]
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.[Name] as Symbol
			,direction.[Name] as DealDirection
			,[CounterpartyRequestedPrice]
			,@OriginallyExecutedAmount AS OriginallyExecutedAmountBasePol
			,[IntegratorExecutionId]
			,[ValueDate]
		FROM 
			[dbo].[OrderExternalIncomingRejectable_Daily] deals
			INNER JOIN [dbo].[Symbol] symbol ON deals.[SymbolId] = symbol.[Id]
			INNER JOIN [dbo].[Counterparty] counterparty ON deals.[CounterpartyId] = counterparty.[CounterpartyId]
			INNER JOIN [dbo].[DealDirection] direction ON deals.[IntegratorDealDirectionId] = direction.Id
		WHERE 
			[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) AND [IntegratorExecutionId] = @IntegratorExecId
		
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION;
			THROW
		END CATCH
		
	END
END
GO

GRANT EXECUTE ON [dbo].[RejectExecutedOrderExternalIncomingRejectable_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE TRIGGER [dbo].[OrderExternalIncomingRejectable_DailyInserted] ON [dbo].[OrderExternalIncomingRejectable_Daily] AFTER INSERT
AS
BEGIN
	-- commit the internal insert transaction to prevent deadlocks
	COMMIT TRANSACTION
	exec [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP]
	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION
END
GO

DROP PROCEDURE [dbo].[GetNopsPolSidedPerCurrency_SP]
DROP PROCEDURE [dbo].[GetDealsStatistics_SP]
DROP PROCEDURE [dbo].[GetRejectionsStatistics_SP]
DROP PROCEDURE [dbo].[GetIgnoredOrders_OLD_SP]
DROP PROCEDURE [dbo].[GetNopAbsTotal_SP]
DROP PROCEDURE [dbo].[GetStatisticsPerCounterparty_SP]
DROP PROCEDURE [dbo].[GetStatisticsPerPair_SP]
DROP PROCEDURE [dbo].[GetNopsAbsPerCounterparty_SP]

ALTER PROCEDURE [dbo].[GetPolarizedNopPerPair_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[AmountBasePolExecuted]) AS PolarizedNop
	FROM 
		(
		SELECT [SymbolId], [AmountBasePolExecuted] 
		FROM [dbo].[DealExternalExecuted]
		WHERE [IntegratorReceivedExecutionReportUtc] BETWEEN @StartTime AND @EndTime
		UNION ALL
		SELECT [SymbolId], [IntegratorExecutedAmountBasePol] AS [AmountBasePolExecuted] 
		FROM [dbo].[OrderExternalIncomingRejectable_Daily]
		WHERE [IntegratorReceivedExternalOrderUtc] BETWEEN @StartTime AND @EndTime
		) deal

		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]
END
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsDailyStats_SP]
AS
BEGIN

DECLARE @KGTRejectionDirectionId BIT
SELECT @KGTRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'
DECLARE @CtpRejectionDirectionId BIT
SELECT @CtpRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'CtpRejectd'

SELECT
		--BEWARE! There can be multiple symbols for same system id (this is especially true for system -1)
		systemStats.SymbolId AS SymbolId,
		systemStats.TradingSystemId AS TradingSystemId,
		COALESCE(systemStats.NopBasePol, 0) AS NopBasePol,
		COALESCE(systemStats.VolumeInUsd/1000000, 0) AS VolumeUsdM,
		COALESCE(systemStats.DealsNum, 0) as DealsNum,
		COALESCE(systemStats.KGTRejectionsNum, 0) as KGTRejectionsNum,
		COALESCE(systemStats.CtpRejectionsNum, 0) as CtpRejectionsNum,
		COALESCE(systemStats.PnlTermPolInUsd/(systemStats.VolumeInUsd/1000000), 0) AS PnlGrossUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd, 0) AS PnlGrossUsd,
		COALESCE(systemStats.CommissionInUSD/(systemStats.VolumeInUsd/1000000), 0) AS CommissionsUsdPerMUsd,
		COALESCE(systemStats.CommissionInUSD, 0) AS CommissionsUsd,
		COALESCE((systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD)/(systemStats.VolumeInUsd/1000000), 0) AS PnlNetUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD, 0) AS PnlNetUsd
	FROM
		(
		SELECT
			Symbol.Id AS SymbolId,
			systemsStats.TradingSystemId AS TradingSystemId,
			systemsStats.NopBasePol AS NopBasePol,
			systemsStats.DealsNum as DealsNum,
			systemsStats.KGTRejectionsNum as KGTRejectionsNum,
			systemsStats.CtpRejectionsNum as CtpRejectionsNum,
			systemsStats.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd,
			--PnL = term position + yield of immediate close of outstanding base position
			--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
			(systemsStats.NopTermPol + (CASE WHEN systemsStats.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * systemsStats.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
			systemsStats.CommissionInCCY1 * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionInUSD
		FROM
			(
			SELECT
				COALESCE(systemsPositions.[TradingSystemId], systemRejections.[TradingSystemId]) AS [TradingSystemId],
				COALESCE(systemsPositions.[SymbolId], systemRejections.[SymbolId]) AS [SymbolId],
				NopBasePol,
				NopTermPol,
				VolumeInCcy1,
				CommissionInCCY1,
				DealsNum,
				KGTRejectionsNum,
				CtpRejectionsNum
			FROM	
				(
				--allways include stats fro unknown systems
				SELECT 
					-1 AS [TradingSystemId], 
					0 AS [SymbolId],
					NULL AS NopBasePol, 
					NULL AS NopTermPol, 
					NULL AS VolumeInCcy1, 
					NULL AS CommissionInCCY1, 
					NULL AS DealsNum
				UNION ALL
				SELECT 
					[TradingSystemId],
					[SymbolId],
					SUM([AmountBasePolExecuted]) AS NopBasePol,
					SUM(-[AmountBasePolExecuted]* [Price]) AS NopTermPol,
					SUM(ABS([AmountBasePolExecuted])) AS VolumeInCcy1,
					SUM(ABS([AmountBasePolExecuted]) * CommissionPricePerMillion / 1000000) AS CommissionInCCY1,
					COUNT([TradingSystemId]) AS DealsNum
				FROM
					[dbo].[TradingSystemDealsExecuted_Daily] deal
					INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
				GROUP BY
					[TradingSystemId], [SymbolId]
				) systemsPositions
				FULL OUTER JOIN
				(
					SELECT 
						[TradingSystemId],
						[SymbolId],
						--COUNT([TradingSystemId]) AS RejectionsNum
						SUM(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN 1 ELSE 0 END) AS KGTRejectionsNum, 
						SUM(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN 1 ELSE 0 END) AS CtpRejectionsNum
					FROM
						[dbo].[TradingSystemDealsRejected_Daily] rejects
					GROUP BY
						 [TradingSystemId], [SymbolId]
				)  systemRejections ON systemsPositions.TradingSystemId = systemRejections.TradingSystemId AND systemsPositions.[SymbolId] = systemRejections.[SymbolId]
			) systemsStats
			INNER JOIN [dbo].[Symbol] symbol ON systemsStats.SymbolId = symbol.Id
			INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
			INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
			) systemStats
END
GO

ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] DROP COLUMN RejectionsNum
TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily]
ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ADD KGTRejectionsNum [INT] NOT NULL
ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ADD CtpRejectionsNum [INT] NOT NULL
GO

ALTER TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily] DROP COLUMN RejectionsNum
TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily]
ALTER TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily] ADD KGTRejectionsNum [INT] NULL
ALTER TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily] ADD CtpRejectionsNum [INT] NULL
GO

ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	DECLARE @TempTradingSystemStatistics TABLE
	(
		[SymbolId] [tinyint] NOT NULL,
		[TradingSystemId] [int] NOT NULL,
		[NopBasePol] [decimal](18,2) NOT NULL,
		[VolumeUsdM] [decimal](18,6) NOT NULL,
		[DealsNum] [int]  NOT NULL,
		[KGTRejectionsNum] [int]  NOT NULL,
		[CtpRejectionsNum] [int]  NOT NULL,
		[PnlGrossUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlGrossUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsdPerMUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsd] [decimal](18,6) NOT NULL
	)

	INSERT INTO 
		@TempTradingSystemStatistics
		([SymbolId]
		,[TradingSystemId]
		,[NopBasePol]
		,[VolumeUsdM]
		,[DealsNum]
		,[KGTRejectionsNum]
		,[CtpRejectionsNum]
		,[PnlGrossUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsdPerMUsd]
		,[CommissionsUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlNetUsd])
    exec [dbo].[GetTradingSystemsDailyStats_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingSystemDaily]
			([TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[KGTRejectionsNum]
			,[CtpRejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd])
		SELECT
			[TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[KGTRejectionsNum]
			,[CtpRejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd]
		FROM
			@TempTradingSystemStatistics
	COMMIT
END
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP]
AS
BEGIN
	
	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()

	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]

	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn
	
	SELECT
		ROW_NUMBER() OVER(ORDER BY
			trgroup.[OrderingRank] ASC,
			[TradingSystemGroupName] ASC,
			counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] ASC,
			beginSymbol.Name ASC) AS Rank,
		trgroup.[TradingSystemGroupName] AS TradingSystemGroup,
		trgroup.[TradingSystemGroupId] As TradingSystemGroupId,
		counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] AS TradingSystemTypeName,
		counterparty.[CounterpartyCode] AS Counterparty,
		systemtype.[TradingSystemTypeId] AS TradingSystemTypeId,
		statsPerType.ConfiguredSystems,
		--statsPerType.EnabledSystems,
		--this should be called active - and upper stay enabled
		CASE WHEN @TradingOn = 1 THEN statsPerType.ActiveSystems ELSE 0 END AS EnabledSystems,
		statsPerType.VolumeUsdM,
		statsPerType.DealsNum,
		statsPerType.KGTRejectionsNum,
		statsPerType.CtpRejectionsNum,
		statsPerType.PnlGrossUsdPerMUsd,
		statsPerType.CommissionsUsdPerMUsd,
		statsPerType.PnlNetUsdPerMUsd,
		statsPerType.PnlGrossUsd,
		statsPerType.CommissionsUsd,
		statsPerType.PnlNetUsd,
		allowedType.IsOrderTransmissionDisabled,
		allowedType.IsGoFlatOnlyOn,
		monitorPages.TradingSystemMonitoringPageId AS PageId,
		beginSymbol.Name AS BeginSymbol,
		endSymbol.Name AS EndSymbol,
		monitorPages.StripsCount
	FROM
	(
	SELECT 
		allowedType.TradingSystemGroupId AS TradingSystemGroupId,
		allowedType.TradingSystemTypeId AS TradingSystemTypeId,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		COALESCE(SUM(CASE WHEN action.[Enabled] = 1 AND action.[GoFlatOnly] = 0 AND action.[OrderTransmissionDisabled] = 0 THEN 1 ELSE 0 END), 0) AS ActiveSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		SUM(stats.[KGTRejectionsNum]) AS KGTRejectionsNum,
		SUM(stats.[CtpRejectionsNum]) AS CtpRejectionsNum,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemTypeAllowedForGroup] allowedType_forJoinsOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = allowedType_forJoinsOnly.[TradingSystemTypeId] AND trsystem.TradingSystemGroupId = allowedType_forJoinsOnly.TradingSystemGroupId
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		--link in deleted systems if needed
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		-- but make sure that those are only the allowed ones
		LEFT JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON 
			allowedType.[TradingSystemTypeId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId])
			AND
			allowedType.[TradingSystemGroupId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemGroupId], trsystem_deleted.[TradingSystemGroupId])

	GROUP BY
		allowedType.TradingSystemGroupId, allowedType.TradingSystemTypeId
	)
	statsPerType
	INNER JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON allowedType.TradingSystemGroupId = statsPerType.TradingSystemGroupId AND allowedType.TradingSystemTypeId = statsPerType.TradingSystemTypeId
	INNER JOIN [dbo].[TradingSystemType] systemtype ON statsPerType.[TradingSystemTypeId] = systemtype.[TradingSystemTypeId]
	INNER JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	RIGHT JOIN TradingSystemGroup trgroup ON trgroup.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId
	LEFT JOIN TradingSystemMonitoringPage monitorPages ON monitorPages.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId AND monitorPages.[TradingSystemTypeId] = statsPerType.TradingSystemTypeId
	LEFT JOIN [dbo].[Symbol] beginSymbol ON monitorPages.BeginSymbolId = beginSymbol.Id
	LEFT JOIN [dbo].[Symbol] endSymbol ON monitorPages.EndSymbolId = endSymbol.Id
END
GO

ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	DECLARE @TempTradingGroupsStatistics TABLE
	(
		[Rank] [int] NOT NULL,
		[TradingSystemGroupName] [NVARCHAR](32) NOT NULL,
		[TradingSystemGroupId] [int]  NOT NULL,
		[TradingSystemTypeName] [nvarchar](12) NULL,
		[Counterparty] [char](3) NULL,
		[TradingSystemTypeId] [tinyint] NULL,
		[ConfiguredSystemsCount] int NULL,
		[EnabledSystemsCount] int NULL,
		[VolumeUsdM] [decimal](18, 6) NULL,
		[DealsNum] [int] NULL,
		[KGTRejectionsNum] [int] NULL,
		[CtpRejectionsNum] [int] NULL,
		[PnlGrossUsdPerMUsd] [decimal](18, 6) NULL,
		[CommissionsUsdPerMUsd] [decimal](18, 6) NULL,
		[PnlNetUsdPerMUsd] [decimal](18, 6) NULL,
		[PnlGrossUsd] [decimal](18, 6) NULL,
		[CommissionsUsd] [decimal](18, 6) NULL,	
		[PnlNetUsd] [decimal](18, 6) NULL,
		[IsOrderTransmissionDisabled] [bit] NULL,
		[IsGoFlatOnlyOn] [bit] NULL,
		[PageId] [int] NULL,
		[BeginSymbol] [nchar](7) NULL,
		[EndSymbol] [nchar](7) NULL,
		[StripsCount] INT
	)

	INSERT INTO 
		@TempTradingGroupsStatistics
		([Rank]
		,[TradingSystemGroupName]
		,[TradingSystemGroupId]
		,[TradingSystemTypeName]
		,[Counterparty]
		,[TradingSystemTypeId]
		,[ConfiguredSystemsCount]
		,[EnabledSystemsCount]
		,[VolumeUsdM]
		,[DealsNum]
		,[KGTRejectionsNum]
		,[CtpRejectionsNum]
		,[PnlGrossUsdPerMUsd]
		,[CommissionsUsdPerMUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsd]
		,[PnlNetUsd]
		,[IsOrderTransmissionDisabled]
		,[IsGoFlatOnlyOn]
		,[PageId]
		,[BeginSymbol]
		,[EndSymbol]
		,[StripsCount])
    exec [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingGroupsDaily]
			([Rank]
			,[TradingSystemGroupName]
			,[TradingSystemGroupId]
			,[TradingSystemTypeName]
			,[Counterparty]
			,[TradingSystemTypeId]
			,[ConfiguredSystemsCount]
			,[EnabledSystemsCount]
			,[VolumeUsdM]
			,[DealsNum]
			,[KGTRejectionsNum]
			,[CtpRejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[CommissionsUsdPerMUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsd]
			,[PnlNetUsd]
			,[IsOrderTransmissionDisabled]
			,[IsGoFlatOnlyOn]
			,[PageId]
			,[BeginSymbol]
			,[EndSymbol]
			,[StripsCount])
		SELECT
			[Rank]
			,[TradingSystemGroupName]
			,[TradingSystemGroupId]
			,[TradingSystemTypeName]
			,[Counterparty]
			,[TradingSystemTypeId]
			,[ConfiguredSystemsCount]
			,[EnabledSystemsCount]
			,[VolumeUsdM]
			,[DealsNum]
			,[KGTRejectionsNum]
			,[CtpRejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[CommissionsUsdPerMUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsd]
			,[PnlNetUsd]
			,[IsOrderTransmissionDisabled]
			,[IsGoFlatOnlyOn]
			,[PageId]
			,[BeginSymbol]
			,[EndSymbol]
			,[StripsCount]
		FROM
			@TempTradingGroupsStatistics
	COMMIT
END
GO

ALTER PROCEDURE [dbo].[GetVenueCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumTimeBetweenSingleCounterpartySignals_seconds],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER TRIGGER [dbo].[VenueMMSettings_DeletedOrUpdated] ON [dbo].[VenueMMSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueMMSettings_Changes]
	   ([SymbolId]
	   ,[SizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade]
	   ,[MaximumWaitTimeOnImprovementTick_milliseconds]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[SizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueMMSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueMMSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([SizeToTrade]) AND (topUpdatedChange.[SizeToTrade] IS NULL OR topUpdatedChange.[SizeToTrade] != deleted.[SizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([MaximumWaitTimeOnImprovementTick_milliseconds]) AND (topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] IS NULL OR topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] != deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
END
GO

ALTER TABLE [dbo].[VenueMMSettings] DROP COLUMN CancelOrderOnDeterioration
ALTER TABLE [dbo].[VenueMMSettings_Changes] DROP COLUMN CancelOrderOnDeterioration

ALTER PROCEDURE [dbo].[GetVenueMMSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[SizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueMMSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueMMSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice]
			,[SizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueMMSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name
	END
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'MM'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'MM TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueMMSettings]
           ([SymbolId]
           ,[SizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@SizeToTrade 
           ,@BestPriceImprovementOffsetBasisPoints
           ,@MaximumDiscountBasisPointsToTrade
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int]
)
AS
BEGIN
	UPDATE sett
	SET
		[SizeToTrade] = @SizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

ALTER PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FairPriceImprovementSpreadBasisPoints],
		sett.[FairPriceImprovementSpreadBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [FairPriceImprovementSpreadDecimal],
		sett.[StopLossNetInUsd],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsGroupOveralDailyStatsAndInfo_SP]
(
	@GroupId INT
)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]
	
	SELECT
		[TradingSystemGroupName] AS TradingSystemGroup
		,[TradingSystemGroupId] 
		,[TradingSystemTypeName]
		,[Counterparty]
		,[TradingSystemTypeId]
		,[ConfiguredSystemsCount] AS ConfiguredSystems
		,[EnabledSystemsCount] AS EnabledSystems
		,[VolumeUsdM]
		,[DealsNum]
		,[KGTRejectionsNum]
		,[CtpRejectionsNum]
		,[PnlGrossUsdPerMUsd]
		,[CommissionsUsdPerMUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsd]
		,[PnlNetUsd]
		,[IsOrderTransmissionDisabled]
		,[IsGoFlatOnlyOn]
		,[PageId]
		,[BeginSymbol]
		,[EndSymbol]
		,[StripsCount]
	FROM 
		[dbo].[PersistedStatisticsPerTradingGroupsDaily]
	WHERE
		[TradingSystemGroupId] = @GroupId
	ORDER BY
		Rank ASC
END
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsOveralDailyStats_SP]
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()

	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]

	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn	
	
	SELECT 
		ISNULL(counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName], 'Unknown') AS SystemName,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		--COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		--this should be called active - and upper stay enabled
		CASE WHEN @TradingOn = 1 
			THEN 
			COALESCE(SUM(CASE WHEN action.[Enabled] = 1 AND action.[GoFlatOnly] = 0 AND action.[OrderTransmissionDisabled] = 0 THEN 1 ELSE 0 END), 0)
			ELSE 
			0 
			END AS EnabledSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		SUM(stats.[KGTRejectionsNum]) AS KGTRejectionsNum,
		SUM(stats.[CtpRejectionsNum]) AS CtpRejectionsNum,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemType] systemtype_forJoinOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = systemtype_forJoinOnly.[TradingSystemTypeId]
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystemType] systemtype ON systemtype.[TradingSystemTypeId] = COALESCE(trsystem.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId], systemtype_forJoinOnly.[TradingSystemTypeId])
		LEFT JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	GROUP BY
		counterparty.[CounterpartyCode], systemtype.[SystemName]
	ORDER BY
		SystemName
END
GO

CREATE VIEW [dbo].[DealExternalExecutedTakersAndMakers_DailyView]
AS
SELECT
	[CounterpartyId]
	,[SymbolId]
	,[AmountBasePolExecuted]
	,[Price]
	,[IntegratorLatencyNanoseconds]
	,[CounterpartyLatencyNanoseconds]
	,[DatePartIntegratorReceivedExecutionReportUtc]
FROM 
	[dbo].[DealExternalExecuted_DailyView] WITH(NOEXPAND, NOLOCK)

UNION ALL

SELECT 
	[CounterpartyId]
	,[SymbolId]
	,[IntegratorExecutedAmountBasePol] AS [AmountBasePolExecuted]
	,[CounterpartyRequestedPrice] AS [Price]
	,[IntegratorLatencyNanoseconds]
	,NULL
	,[DatePartIntegratorReceivedOrderUtc] AS [DatePartIntegratorReceivedExecutionReportUtc]
FROM 
	[dbo].[OrderExternalIncomingRejectable_Daily] WITH(NOLOCK)
WHERE
	[IntegratorExecutedAmountBasePol] != 0
GO


ALTER PROCEDURE [dbo].[GetNopAbsTotal_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.Price) AS NopTermPol
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		deal.SymbolId 


	SELECT
		-- From SHORTs
		SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
	FROM
		(
		SELECT
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
		GROUP BY
			CCYPositions.CCY
		) AS CtpCCYPositions

END
GO

ALTER PROCEDURE [dbo].[GetNopsPolPerCounterpartyAndCurrency_SP] (
	@Day Date
	)
AS
BEGIN

	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		deal.CounterpartyId,
		deal.SymbolId
		
		

		SELECT
			ctp.CounterpartyCode AS Counterparty,
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				basePos.CounterpartyId,
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT 
				termPos.CounterpartyId,
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
			INNER JOIN [dbo].[Counterparty] ctp ON ctp.CounterpartyId = CCYPositions.CounterpartyId
		GROUP BY
			ctp.CounterpartyCode
			,CCYPositions.CCY
		ORDER BY
			ctp.CounterpartyCode
			,CCYPositions.CCY

END
GO



ALTER PROCEDURE [dbo].[GetNopsPolSidedPerCurrency_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,0),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		deal.SymbolId


	SELECT
		CCYPositions.CCY AS CCY,
		SUM(CCYPositions.NopPol) AS NopPol,
		SUM(CCYPositions.NopPolInUsd) AS NopPolInUsd
	FROM
		(
		SELECT 
			ccy1.CurrencyAlphaCode AS CCY,
			basePos.NopBasePol AS NopPol,
			basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
		FROM
			Currency ccy1
			INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
			INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

		UNION ALL

		SELECT 
			ccy2.CurrencyAlphaCode AS CCY,
			termPos.NopTermPol AS NopPol,
			termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
		FROM
			Currency ccy2
			INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
			INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
		) AS CCYPositions
	GROUP BY
		CCYPositions.CCY
	ORDER BY
		CCYPositions.CCY
END
GO

ALTER PROCEDURE [dbo].[GetStatisticsPerPair_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	SELECT
		Symbol.Name AS Symbol,
		positions.NopBasePol,
		positions.NopTermPol,
		positions.DealsNum as DealsNum,
		--PnL = term position + yield of immediate close of outstanding base position
		--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
		positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * positions.NopBasePol AS PnlTermPol,
		(positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * positions.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
		positions.VolumeInCcy1 AS VolumeInCcy1,
		positions.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd
	FROM
		(
		SELECT
			deal.SymbolId AS SymbolId,
			SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
			SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
			COUNT(deal.SymbolId) AS DealsNum,
			SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1
		FROM 
			DealExternalExecutedTakersAndMakers_DailyView deal
			
		WHERE
			[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		GROUP BY
			deal.SymbolId
		) positions
		INNER JOIN  [dbo].[Symbol] symbol ON positions.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
	ORDER BY
		symbol.Name
END
GO





ALTER PROCEDURE [dbo].[GetStatisticsPerCounterparty_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		KGTRejectionsNum int,
		CtpRejectionsNum int,
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int
	)

	INSERT INTO @temp_rejections(CounterpartyId, KGTRejectionsNum, CtpRejectionsNum, IntegratorLatencyNanoseconds, IntegratorLatencyCases)
	SELECT
		[CounterpartyId],
		NULL AS KGTRejections,
		COUNT([CounterpartyId]) AS CtpRejections,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
	FROM
		[dbo].[DealExternalRejected_DailyView] ctpRej WITH(NOEXPAND, NOLOCK)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]

	UNION ALL

	SELECT
			[CounterpartyId],
			COUNT([CounterpartyId]) AS KGTRejections,
			NULL AS CtpRejections,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily] kgtRej WITH(NOLOCK)
		WHERE
			[IntegratorRejectedAmountBasePol] != 0
			AND [DatePartIntegratorReceivedOrderUtc] = @Day
			--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
		GROUP BY
			[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		DealsNum int,
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int,
		CounterpartyLatencyNanoseconds bigint,
		CounterpartyLatencyCases int
	)

	INSERT INTO 
		@temp_NOPs_pol
		(
		SymbolId, 
		CounterpartyId, 
		NopBasePol, 
		NopTermPol, 
		VolumeBaseAbs, 
		DealsNum, 
		IntegratorLatencyNanoseconds, 
		IntegratorLatencyCases, 
		CounterpartyLatencyNanoseconds, 
		CounterpartyLatencyCases
		)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases,
		SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
		COUNT(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyCases
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
		--INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		deal.SymbolId,
		deal.CounterpartyId
		

	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		RejectionsStatistics.KGTRejectionsNum AS KGTRejectionsNum,
		RejectionsStatistics.CtpRejectionsNum AS CtpRejectionsNum,
		RejectionsStatistics.KGTRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.KGTRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0)) AS KGTRejectionsRate,
		RejectionsStatistics.CtpRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.CtpRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0)) AS CtpRejectionsRate,
		--this calculates average of both numbers if both are non-null; or average of the nonull case if just one is not null (as 2*A / 2*B = A/B); or NULL if both are NULL (avoids divide by zero exception)
		(COALESCE(RejectionsStatistics.IntegratorLatencyNanoseconds, DealsStatistics.IntegratorLatencyNanoseconds) + COALESCE(DealsStatistics.IntegratorLatencyNanoseconds, RejectionsStatistics.IntegratorLatencyNanoseconds))
		/
		(COALESCE(RejectionsStatistics.IntegratorLatencyCases, DealsStatistics.IntegratorLatencyCases) + COALESCE(DealsStatistics.IntegratorLatencyCases, RejectionsStatistics.IntegratorLatencyCases))
		AS AvgIntegratorLatencyNanoseconds,
		DealsStatistics.CounterpartyLatencyNanoseconds / DealsStatistics.CounterpartyLatencyCases AS AvgCounterpartyLatencyNanoseconds
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			-- from SHORTs
			SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs,
			--SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(DealsNum) AS DealsNum,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
			SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
			SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(DealsNum) AS DealsNum,
				SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
				SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
				SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
				SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.DealsNum AS DealsNum,
					basePos.IntegratorLatencyNanoseconds AS IntegratorLatencyNanoseconds,
					basePos.IntegratorLatencyCases AS IntegratorLatencyCases,
					basePos.CounterpartyLatencyNanoseconds AS CounterpartyLatencyNanoseconds,
					basePos.CounterpartyLatencyCases AS CounterpartyLatencyCases
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd,
					NULL AS IntegratorLatencyNanoseconds,
					NULL AS IntegratorLatencyCases,
					NULL AS CounterpartyLatencyNanoseconds,
					NULL AS CounterpartyLatencyCases
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode
END
GO

ALTER TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily] DROP COLUMN RejectionsNum
ALTER TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily] DROP COLUMN RejectionsRate
TRUNCATE TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily]
ALTER TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily] ADD KGTRejectionsNum [INT] NULL
ALTER TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily] ADD CtpRejectionsNum [INT] NULL
ALTER TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily] ADD KGTRejectionsRate [decimal](18,4) NULL
ALTER TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily] ADD CtpRejectionsRate [decimal](18,4) NULL
GO

ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
AS
BEGIN

	--create the temp table before transaction to speed up things
	
	CREATE TABLE #TempCounterpartyStatistics(
		[Counterparty] [varchar](3) NOT NULL,
		[NopAbs] decimal(18,2) NULL,
		[VolumeBaseAbsInUsd] decimal(18, 2) NULL,
		[VolumeShare] decimal(18, 4) NULL,
		[DealsNum] int NULL,
		[AvgDealSizeUsd] decimal(18,2) NULL,
		[KGTRejectionsNum] int NULL,
		[CtpRejectionsNum] int NULL,
		[KGTRejectionsRate] decimal(18,4) NULL,
		[CtpRejectionsRate] decimal(18,4) NULL,
		[AvgIntegratorLatencyNanoseconds] bigint NULL,
		[AvgCounterpartyLatencyNanoseconds] bigint
	)

	DECLARE @Today DATE = GETUTCDATE()
	
	INSERT INTO 
		#TempCounterpartyStatistics
		(
		[Counterparty],
		[NopAbs],
		[VolumeBaseAbsInUsd],
		[VolumeShare],
		[DealsNum],
		[AvgDealSizeUsd],
		[KGTRejectionsNum],
		[CtpRejectionsNum],
		[KGTRejectionsRate],
		[CtpRejectionsRate],
		[AvgIntegratorLatencyNanoseconds],
		[AvgCounterpartyLatencyNanoseconds]
		)
	exec [dbo].[GetStatisticsPerCounterparty_Daily_SP] @Today

	BEGIN TRANSACTION
	   TRUNCATE TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily]

	   INSERT INTO 
			[dbo].[PersistedStatisticsPerCounterpartyDaily]
			(
			[Counterparty],
			[NopAbs],
			[VolumeBaseAbsInUsd],
			[VolumeShare],
			[DealsNum],
			[AvgDealSizeUsd],
			[KGTRejectionsNum],
			[CtpRejectionsNum],
			[KGTRejectionsRate],
			[CtpRejectionsRate],
			[AvgIntegratorLatencyNanoseconds],
			[AvgCounterpartyLatencyNanoseconds]
			)
		SELECT
			[Counterparty],
			[NopAbs],
			[VolumeBaseAbsInUsd],
			[VolumeShare],
			[DealsNum],
			[AvgDealSizeUsd],
			[KGTRejectionsNum],
			[CtpRejectionsNum],
			[KGTRejectionsRate],
			[CtpRejectionsRate],
			[AvgIntegratorLatencyNanoseconds],
			[AvgCounterpartyLatencyNanoseconds]
		FROM
			#TempCounterpartyStatistics

	COMMIT
END
GO

ALTER PROCEDURE [dbo].[GetStatisticsPerCounterparty_DailyCached_SP]
AS
BEGIN

	SELECT 
		[Counterparty]
		,[NopAbs]
		,[VolumeBaseAbsInUsd]
		,[VolumeShare]
		,[DealsNum]
		,[AvgDealSizeUsd]
		,[KGTRejectionsNum]
		,[CtpRejectionsNum]
		,[KGTRejectionsRate]
		,[CtpRejectionsRate]
		,[AvgIntegratorLatencyNanoseconds]
		,[AvgCounterpartyLatencyNanoseconds]
	FROM 
		[dbo].[PersistedStatisticsPerCounterpartyDaily]
	ORDER BY
		[Counterparty]

END
GO

INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (5, N'MarketPreferLmax')
GO


CREATE TABLE [dbo].[VenueStreamSettings](
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[SizeToTrade] [decimal](18, 0) NOT NULL,
	[BestPriceImprovementOffsetBasisPoints] [decimal](18, 6) NOT NULL,
	[MaximumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[KGTLLTime_milliseconds] [int] NOT NULL,
	[PreferLLDestinationCheckToLmax] [bit] NOT NULL,
	[MinimumBPGrossToAccept] [decimal](18, 6) NOT NULL,
	[PreferLmaxDuringHedging] [bit] NOT NULL,
 CONSTRAINT [UNQ__VenueStreamSettings__TradingSystemId] UNIQUE NONCLUSTERED 
(
	[TradingSystemId] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VenueStreamSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueStreamSettings_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[VenueStreamSettings] CHECK CONSTRAINT [FK_VenueStreamSettings_Symbol]
GO

ALTER TABLE [dbo].[VenueStreamSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueStreamSettings_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO

ALTER TABLE [dbo].[VenueStreamSettings] CHECK CONSTRAINT [FK_VenueStreamSettings_Counterparty]
GO

CREATE CLUSTERED INDEX [IX_VenueStreamSettingsSymbolPerCounterparty] ON [dbo].[VenueStreamSettings]
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[VenueStreamSettings_Changes](
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[SizeToTrade] [decimal](18, 0) NOT NULL,
	[BestPriceImprovementOffsetBasisPoints] [decimal](18, 6) NOT NULL,
	[MaximumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[KGTLLTime_milliseconds] [int] NOT NULL,
	[PreferLLDestinationCheckToLmax] [bit] NOT NULL,
	[MinimumBPGrossToAccept] [decimal](18, 6) NOT NULL,
	[PreferLmaxDuringHedging] [bit] NOT NULL,
	[ValidFromUtc] [datetime2](7) NOT NULL,
	[ValidToUtc] [datetime2](7) NOT NULL,
	[Deleted] [bit] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VenueStreamSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueStreamSettings_Changes_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[VenueStreamSettings_Changes] CHECK CONSTRAINT [FK_VenueStreamSettings_Changes_Symbol]
GO

ALTER TABLE [dbo].[VenueStreamSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueStreamSettings_Changes_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO

ALTER TABLE [dbo].[VenueStreamSettings_Changes] CHECK CONSTRAINT [FK_VenueStreamSettings_Changes_Counterparty]
GO

CREATE CLUSTERED INDEX [IX_VenueStreamSettings_Changes] ON [dbo].[VenueStreamSettings_Changes]
(
	[TradingSystemId] ASC,
	[ValidToUtc] ASC
)ON [PRIMARY]
GO

INSERT INTO [dbo].[TableChangeLog] ([TableName], [LastUpdatedUtc]) VALUES ('VenueStreamSettings', '1/1/1900')
GO

CREATE TRIGGER [dbo].[LastDeletedTrigger_VenueStreamSettings] ON [dbo].[VenueStreamSettings]
AFTER DELETE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON
UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueStreamSettings'

END
GO


CREATE TRIGGER [dbo].[LastUpdatedTrigger_VenueStreamSettings] ON [dbo].[VenueStreamSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueStreamSettings'

END
GO


CREATE TRIGGER [dbo].[VenueStreamSettings_DeletedOrUpdated] ON [dbo].[VenueStreamSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueStreamSettings_Changes]
	   ([SymbolId]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[SizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade] 
	   ,[KGTLLTime_milliseconds]
	   ,[PreferLLDestinationCheckToLmax]
	   ,[MinimumBPGrossToAccept]
	   ,[PreferLmaxDuringHedging]   
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[SizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[KGTLLTime_milliseconds]
	    ,deleted.[PreferLLDestinationCheckToLmax]
	    ,deleted.[MinimumBPGrossToAccept]
	    ,deleted.[PreferLmaxDuringHedging]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueStreamSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueStreamSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
		OR (UPDATE([SizeToTrade]) AND (topUpdatedChange.[SizeToTrade] IS NULL OR topUpdatedChange.[SizeToTrade] != deleted.[SizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([KGTLLTime_milliseconds]) AND (topUpdatedChange.[KGTLLTime_milliseconds] IS NULL OR topUpdatedChange.[KGTLLTime_milliseconds] != deleted.[KGTLLTime_milliseconds]))
		OR (UPDATE([PreferLLDestinationCheckToLmax]) AND (topUpdatedChange.[PreferLLDestinationCheckToLmax] IS NULL OR topUpdatedChange.[PreferLLDestinationCheckToLmax] != deleted.[PreferLLDestinationCheckToLmax]))
		OR (UPDATE([MinimumBPGrossToAccept]) AND (topUpdatedChange.[MinimumBPGrossToAccept] IS NULL OR topUpdatedChange.[MinimumBPGrossToAccept] != deleted.[MinimumBPGrossToAccept]))
		OR (UPDATE([PreferLmaxDuringHedging]) AND (topUpdatedChange.[PreferLmaxDuringHedging] IS NULL OR topUpdatedChange.[PreferLmaxDuringHedging] != deleted.[PreferLmaxDuringHedging]))
END
GO

INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FS1'), 'Stream')
GO


CREATE PROCEDURE [dbo].[GetUpdatedVenueStreamSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueStreamSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[SizeToTrade]
		    ,[BestPriceImprovementOffsetBasisPoints]
		    ,[MaximumDiscountBasisPointsToTrade] 
		    ,[KGTLLTime_milliseconds]
		    ,[PreferLLDestinationCheckToLmax]
		    ,[MinimumBPGrossToAccept]
		    ,[PreferLmaxDuringHedging]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueStreamSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name
	END
END
GO

GRANT EXECUTE ON [dbo].[GetUpdatedVenueStreamSystemSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetVenueStreamSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[SizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[PreferLLDestinationCheckToLmax],
		sett.[MinimumBPGrossToAccept],
		sett.[MinimumBPGrossToAccept] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossToAcceptDecimal],
		sett.[PreferLmaxDuringHedging],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

GRANT EXECUTE ON [dbo].[GetVenueStreamSystemSettingsAndStats_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[VenueStreamSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Stream'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Stream TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueStreamSettings]
           ([SymbolId]
		   ,[CounterpartyId]
           ,[TradingSystemId]
           ,[SizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
		   ,[KGTLLTime_milliseconds]
		   ,[PreferLLDestinationCheckToLmax]
		   ,[MinimumBPGrossToAccept]
		   ,[PreferLmaxDuringHedging])
     VALUES
           (@SymbolId
		   ,@CounterpartyId
           ,@TradingSystemId
           ,@SizeToTrade 
           ,@BestPriceImprovementOffsetBasisPoints
           ,@MaximumDiscountBasisPointsToTrade
		   ,@KGTLLTime_milliseconds
		   ,@PreferLLDestinationCheckToLmax
		   ,@MinimumBPGrossToAccept
		   ,@PreferLmaxDuringHedging)
END
GO

GRANT EXECUTE ON [dbo].[VenueStreamSettingsInsertSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[VenueStreamSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit]
)
AS
BEGIN
	UPDATE sett
	SET
		[SizeToTrade] = @SizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[KGTLLTime_milliseconds] = @KGTLLTime_milliseconds,
		[PreferLLDestinationCheckToLmax] = @PreferLLDestinationCheckToLmax,
		[MinimumBPGrossToAccept] = @MinimumBPGrossToAccept,
		[PreferLmaxDuringHedging] = @PreferLmaxDuringHedging
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

GRANT EXECUTE ON [dbo].[VenueStreamSettingsUpdateSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROCEDURE [dbo].[VenueSystemDeleteOne_SP]
(
	@TradingSystemId int
)
AS
BEGIN

	BEGIN TRANSACTION;
	
	DECLARE @DeletedCnt INT = 0;
	
	DELETE sett
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
		
	
	IF @DeletedCnt > 0
	BEGIN
		DELETE FROM
			[dbo].[TradingSystemActions]
		WHERE 
			[TradingSystemId] = @TradingSystemId
			
		DELETE FROM 
			[dbo].[TradingSystem]
		WHERE 
			[TradingSystemId] = @TradingSystemId
	END
	
	
	COMMIT TRANSACTION;
END
GO

ALTER PROCEDURE [dbo].[VenueSystemDeleteMulti_SP]
(
	@TradingSystemId IdsTable READONLY
)
AS
BEGIN

	BEGIN TRANSACTION;
	
	DECLARE @DeletedCnt INT = 0;
	
	DELETE sett
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
		

	DELETE FROM
		[dbo].[TradingSystemActions]
	WHERE 
		[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
		
	DELETE FROM 
		[dbo].[TradingSystem]
	WHERE 
		[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	COMMIT TRANSACTION;
END
GO



CREATE PROCEDURE [dbo].[StreamingSessionStatsToday_SP]
(
	@CounterpartyCode [char](3)
)
AS
BEGIN
	SELECT 
		SUM(CASE WHEN [IntegratorRejectedAmountBasePol] > 0 THEN 1 ELSE 0 END) AS RejectionsNum,
		SUM(CASE WHEN [IntegratorExecutedAmountBasePol] > 0 THEN 1 ELSE 0 END) AS DealsNum
	FROM
		[dbo].[OrderExternalIncomingRejectable_Daily] ord
		INNER JOIN [dbo].[Counterparty] ctp on ord.CounterpartyId = ctp.CounterpartyId
	WHERE
		ctp.[CounterpartyCode] = @CounterpartyCode
END
GO

GRANT EXECUTE ON [dbo].[StreamingSessionStatsToday_SP] TO [IntegratorServiceAccount] AS [dbo]
GO



UPDATE
	[dbo].[Dictionary_FIX]
SET
	[DictionaryXml] = N'<fix major="4" minor="2">
  <header>
    <field name="BeginString" required="Y" />
    <field name="BodyLength" required="Y" />
    <field name="MsgType" required="Y" />
    <field name="SenderCompID" required="Y" />
    <field name="TargetCompID" required="Y" />
    <field name="OnBehalfOfCompID" required="N" />
    <field name="DeliverToCompID" required="N" />
    <field name="SecureDataLen" required="N" />
    <field name="SecureData" required="N" />
    <field name="MsgSeqNum" required="Y" />
    <field name="SenderSubID" required="N" />
    <field name="SenderLocationID" required="N" />
    <field name="TargetSubID" required="N" />
    <field name="TargetLocationID" required="N" />
    <field name="OnBehalfOfSubID" required="N" />
    <field name="OnBehalfOfLocationID" required="N" />
    <field name="DeliverToSubID" required="N" />
    <field name="DeliverToLocationID" required="N" />
    <field name="PossDupFlag" required="N" />
    <field name="PossResend" required="N" />
    <field name="SendingTime" required="Y" />
    <field name="OrigSendingTime" required="N" />
    <field name="XmlDataLen" required="N" />
    <field name="XmlData" required="N" />
    <field name="MessageEncoding" required="N" />
    <field name="LastMsgSeqNumProcessed" required="N" />
    <field name="OnBehalfOfSendingTime" required="N" />
  </header>
  <trailer>
    <field name="SignatureLength" required="N" />
    <field name="Signature" required="N" />
    <field name="CheckSum" required="Y" />
  </trailer>
  <messages>
    <message name="Heartbeat" msgtype="0" msgcat="admin">
      <field name="TestReqID" required="N" />
    </message>
    <message name="Logon" msgtype="A" msgcat="admin">
      <field name="EncryptMethod" required="Y" />
      <field name="HeartBtInt" required="Y" />
      <field name="RawDataLength" required="N" />
      <field name="RawData" required="N" />
      <field name="ResetSeqNumFlag" required="N" />
      <field name="MaxMessageSize" required="N" />
      <group name="NoMsgTypes" required="N">
        <field name="RefMsgType" required="N" />
        <field name="MsgDirection" required="N" />
      </group>
    </message>
    <message name="TestRequest" msgtype="1" msgcat="admin">
      <field name="TestReqID" required="Y" />
    </message>
    <message name="ResendRequest" msgtype="2" msgcat="admin">
      <field name="BeginSeqNo" required="Y" />
      <field name="EndSeqNo" required="Y" />
    </message>
    <message name="Reject" msgtype="3" msgcat="admin">
      <field name="RefSeqNum" required="Y" />
      <field name="RefTagID" required="N" />
      <field name="RefMsgType" required="N" />
      <field name="SessionRejectReason" required="N" />
      <field name="Text" required="N" />
      <field name="EncodedTextLen" required="N" />
      <field name="EncodedText" required="N" />
    </message>
    <message name="SequenceReset" msgtype="4" msgcat="admin">
      <field name="GapFillFlag" required="N" />
      <field name="NewSeqNo" required="Y" />
    </message>
    <message name="Logout" msgtype="5" msgcat="admin">
      <field name="Text" required="N" />
      <field name="EncodedTextLen" required="N" />
      <field name="EncodedText" required="N" />
    </message>
    <message name="MarketDataRequest" msgtype="V" msgcat="app">
      <field name="MDReqID" required="Y" />
      <field name="SubscriptionRequestType" required="Y" />
      <field name="MarketDepth" required="Y" />
      <field name="MDUpdateType" required="N" />
      <field name="AggregatedBook" required="N" />
      <group name="NoMDEntryTypes" required="Y">
        <field name="MDEntryType" required="Y" />
      </group>
      <group name="NoRelatedSym" required="Y">
        <field name="Symbol" required="Y" />
        <field name="SymbolSfx" required="N" />
        <field name="SecurityID" required="N" />
        <field name="IDSource" required="N" />
        <field name="SecurityType" required="N" />
        <field name="MaturityMonthYear" required="N" />
        <field name="MaturityDay" required="N" />
        <field name="PutOrCall" required="N" />
        <field name="StrikePrice" required="N" />
        <field name="OptAttribute" required="N" />
        <field name="ContractMultiplier" required="N" />
        <field name="CouponRate" required="N" />
        <field name="SecurityExchange" required="N" />
        <field name="Issuer" required="N" />
        <field name="EncodedIssuerLen" required="N" />
        <field name="EncodedIssuer" required="N" />
        <field name="SecurityDesc" required="N" />
        <field name="EncodedSecurityDescLen" required="N" />
        <field name="EncodedSecurityDesc" required="N" />
        <field name="TradingSessionID" required="N" />
      </group>
    </message>
    <message name="MarketDataSnapshotFullRefresh" msgtype="W" msgcat="app">
      <field name="MDReqID" required="N" />
      <field name="Symbol" required="Y" />
      <field name="SymbolSfx" required="N" />
      <field name="SecurityID" required="N" />
      <field name="IDSource" required="N" />
      <field name="SecurityType" required="N" />
      <field name="MaturityMonthYear" required="N" />
      <field name="MaturityDay" required="N" />
      <field name="PutOrCall" required="N" />
      <field name="StrikePrice" required="N" />
      <field name="OptAttribute" required="N" />
      <field name="ContractMultiplier" required="N" />
      <field name="CouponRate" required="N" />
      <field name="SecurityExchange" required="N" />
      <field name="Issuer" required="N" />
      <field name="EncodedIssuerLen" required="N" />
      <field name="EncodedIssuer" required="N" />
      <field name="SecurityDesc" required="N" />
      <field name="EncodedSecurityDescLen" required="N" />
      <field name="EncodedSecurityDesc" required="N" />
      <field name="FinancialStatus" required="N" />
      <field name="CorporateAction" required="N" />
      <field name="TotalVolumeTraded" required="N" />
      <group name="NoMDEntries" required="Y">
        <field name="MDEntryType" required="Y" />
        <field name="MDEntryPx" required="Y" />
        <field name="Currency" required="N" />
        <field name="MDEntrySize" required="N" />
        <field name="MDEntryDate" required="N" />
        <field name="MDEntryTime" required="N" />
        <field name="TickDirection" required="N" />
        <field name="MDMkt" required="N" />
        <field name="TradingSessionID" required="N" />
        <field name="QuoteCondition" required="N" />
        <field name="TradeCondition" required="N" />
        <field name="MDEntryOriginator" required="N" />
        <field name="LocationID" required="N" />
        <field name="DeskID" required="N" />
        <field name="OpenCloseSettleFlag" required="N" />
        <field name="TimeInForce" required="N" />
        <field name="ExpireDate" required="N" />
        <field name="ExpireTime" required="N" />
        <field name="MinQty" required="N" />
        <field name="ExecInst" required="N" />
        <field name="SellerDays" required="N" />
        <field name="OrderID" required="N" />
        <field name="QuoteEntryID" required="N" />
        <field name="MDEntryBuyer" required="N" />
        <field name="MDEntrySeller" required="N" />
        <field name="NumberOfOrders" required="N" />
        <field name="MDEntryPositionNo" required="N" />
        <field name="Text" required="N" />
        <field name="EncodedTextLen" required="N" />
        <field name="EncodedText" required="N" />
      </group>
    </message>
    <message name="MarketDataIncrementalRefresh" msgtype="X" msgcat="app">
      <field name="MDReqID" required="N" />
      <group name="NoMDEntries" required="Y">
        <field name="MDUpdateAction" required="Y" />
        <field name="DeleteReason" required="N" />
        <field name="MDEntryType" required="N" />
        <field name="MDEntryID" required="N" />
        <field name="MDEntryRefID" required="N" />
        <field name="Symbol" required="N" />
        <field name="SymbolSfx" required="N" />
        <field name="SecurityID" required="N" />
        <field name="IDSource" required="N" />
        <field name="SecurityType" required="N" />
        <field name="MaturityMonthYear" required="N" />
        <field name="MaturityDay" required="N" />
        <field name="PutOrCall" required="N" />
        <field name="StrikePrice" required="N" />
        <field name="OptAttribute" required="N" />
        <field name="ContractMultiplier" required="N" />
        <field name="CouponRate" required="N" />
        <field name="SecurityExchange" required="N" />
        <field name="Issuer" required="N" />
        <field name="EncodedIssuerLen" required="N" />
        <field name="EncodedIssuer" required="N" />
        <field name="SecurityDesc" required="N" />
        <field name="EncodedSecurityDescLen" required="N" />
        <field name="EncodedSecurityDesc" required="N" />
        <field name="FinancialStatus" required="N" />
        <field name="CorporateAction" required="N" />
        <field name="MDEntryPx" required="N" />
        <field name="Currency" required="N" />
        <field name="MDEntrySize" required="N" />
        <field name="MDEntryDate" required="N" />
        <field name="MDEntryTime" required="N" />
        <field name="TickDirection" required="N" />
        <field name="MDMkt" required="N" />
        <field name="TradingSessionID" required="N" />
        <field name="QuoteCondition" required="N" />
        <field name="TradeCondition" required="N" />
        <field name="MDEntryOriginator" required="N" />
        <field name="LocationID" required="N" />
        <field name="DeskID" required="N" />
        <field name="OpenCloseSettleFlag" required="N" />
        <field name="TimeInForce" required="N" />
        <field name="ExpireDate" required="N" />
        <field name="ExpireTime" required="N" />
        <field name="MinQty" required="N" />
        <field name="ExecInst" required="N" />
        <field name="SellerDays" required="N" />
        <field name="OrderID" required="N" />
        <field name="QuoteEntryID" required="N" />
        <field name="MDEntryBuyer" required="N" />
        <field name="MDEntrySeller" required="N" />
        <field name="NumberOfOrders" required="N" />
        <field name="MDEntryPositionNo" required="N" />
        <field name="TotalVolumeTraded" required="N" />
        <field name="Text" required="N" />
        <field name="EncodedTextLen" required="N" />
        <field name="EncodedText" required="N" />
        <field name="MatchTime" required="N" />
        <field name="MaturityTime" required="N" />
        <field name="NoQuoteQualifiers" required="N" />
        <field name="QuoteQualifier" required="N" />
      </group>
    </message>
    <message name="MarketDataRequestReject" msgtype="Y" msgcat="app">
      <field name="MDReqID" required="Y" />
      <field name="MDReqRejReason" required="N" />
      <field name="Text" required="N" />
      <field name="EncodedTextLen" required="N" />
      <field name="EncodedText" required="N" />
    </message>
    <message name="SecurityListRequest" msgtype="x" msgcat="app">
      <field name="SecurityReqID" required="Y" />
      <field name="SecurityListRequestType" required="Y" />
      <field name="Symbol" required="Y" />
      <field name="Product" required="Y" />
    </message>
    <message name="SecurityList" msgtype="y" msgcat="app">
      <field name="SecurityReqID" required="Y" />
      <field name="SecurityResponseID" required="Y" />
      <field name="SecurityRequestResult" required="Y" />
      <field name="TotalNumSecurities" required="N" />
      <group name="NoRelatedSym" required="Y">
        <field name="Symbol" required="Y" />
        <field name="SymbolSfx" required="Y" />
        <field name="FutSettDate" required="Y" />
        <group name="NoInstrAttrib" required="Y">
          <field name="InstrAttribType" required="Y" />
          <field name="InstrAttribValue" required="Y" />
        </group>
      </group>
    </message>
    <message name="NewOrderSingle" msgtype="D" msgcat="app">
      <field name="ClOrdID" required="Y" />
	  <group name="NoPartyIDs" required="N">
        <field name="PartyID" required="N" />
        <field name="PartyIDSource" required="N" />
		<field name="PartyRole" required="N" />
      </group>
      <field name="ClientID" required="N" />
      <field name="ExecBroker" required="N" />
      <field name="Account" required="N" />
      <group name="NoAllocs" required="N">
        <field name="AllocAccount" required="N" />
        <field name="AllocShares" required="N" />
      </group>
      <field name="SettlmntTyp" required="N" />
      <field name="FutSettDate" required="N" />
      <field name="HandlInst" required="Y" />
      <field name="ExecInst" required="N" />
      <field name="MinQty" required="N" />
      <field name="MaxFloor" required="N" />
      <field name="ExDestination" required="N" />
      <group name="NoTradingSessions" required="N">
        <field name="TradingSessionID" required="N" />
      </group>
      <field name="ProcessCode" required="N" />
      <field name="Symbol" required="Y" />
      <field name="SymbolSfx" required="N" />
      <field name="SecurityID" required="N" />
      <field name="IDSource" required="N" />
      <field name="SecurityType" required="N" />
      <field name="MaturityMonthYear" required="N" />
      <field name="MaturityDay" required="N" />
      <field name="PutOrCall" required="N" />
      <field name="StrikePrice" required="N" />
      <field name="OptAttribute" required="N" />
      <field name="ContractMultiplier" required="N" />
      <field name="CouponRate" required="N" />
      <field name="SecurityExchange" required="N" />
      <field name="Issuer" required="N" />
      <field name="EncodedIssuerLen" required="N" />
      <field name="EncodedIssuer" required="N" />
      <field name="SecurityDesc" required="N" />
      <field name="EncodedSecurityDescLen" required="N" />
      <field name="EncodedSecurityDesc" required="N" />
      <field name="PrevClosePx" required="N" />
      <field name="Side" required="Y" />
      <field name="LocateReqd" required="N" />
      <field name="TransactTime" required="Y" />
      <field name="OrderQty" required="N" />
      <field name="CashOrderQty" required="N" />
      <field name="OrdType" required="Y" />
      <field name="Price" required="N" />
      <field name="StopPx" required="N" />
      <field name="Currency" required="N" />
      <field name="ComplianceID" required="N" />
      <field name="SolicitedFlag" required="N" />
      <field name="IOIid" required="N" />
      <field name="QuoteID" required="N" />
      <field name="TimeInForce" required="N" />
      <field name="EffectiveTime" required="N" />
      <field name="ExpireDate" required="N" />
      <field name="ExpireTime" required="N" />
      <field name="GTBookingInst" required="N" />
      <field name="Commission" required="N" />
      <field name="CommType" required="N" />
      <field name="Rule80A" required="N" />
      <field name="ForexReq" required="N" />
      <field name="SettlCurrency" required="N" />
      <field name="Text" required="N" />
      <field name="EncodedTextLen" required="N" />
      <field name="EncodedText" required="N" />
      <field name="FutSettDate2" required="N" />
      <field name="OrderQty2" required="N" />
      <field name="OpenClose" required="N" />
      <field name="CoveredOrUncovered" required="N" />
      <field name="CustomerOrFirm" required="N" />
      <field name="MaxShow" required="N" />
      <field name="PegDifference" required="N" />
      <field name="DiscretionInst" required="N" />
      <field name="DiscretionOffset" required="N" />
      <field name="ClearingFirm" required="N" />
      <field name="ClearingAccount" required="N" />
    </message>
    <message name="ExecutionReport" msgtype="8" msgcat="app">
      <field name="OrderID" required="N" />
      <field name="SecondaryOrderID" required="N" />
      <field name="ClOrdID" required="N" />
      <field name="OrigClOrdID" required="N" />
      <field name="ClientID" required="N" />
      <field name="ExecBroker" required="N" />
      <group name="NoContraBrokers" required="N">
        <field name="ContraBroker" required="N" />
        <field name="ContraTrader" required="N" />
        <field name="ContraTradeQty" required="N" />
        <field name="ContraTradeTime" required="N" />
      </group>
      <field name="ListID" required="N" />
      <field name="ExecID" required="Y" />
      <field name="ExecTransType" required="Y" />
      <field name="ExecRefID" required="N" />
      <field name="ExecType" required="Y" />
      <field name="OrdStatus" required="Y" />
      <field name="OrdRejReason" required="N" />
      <field name="ExecRestatementReason" required="N" />
      <field name="Account" required="N" />
      <field name="SettlmntTyp" required="N" />
      <field name="FutSettDate" required="N" />
      <field name="Symbol" required="N" />
      <field name="SymbolSfx" required="N" />
      <field name="SecurityID" required="N" />
      <field name="IDSource" required="N" />
      <field name="SecurityType" required="N" />
      <field name="MaturityMonthYear" required="N" />
      <field name="MaturityDay" required="N" />
      <field name="PutOrCall" required="N" />
      <field name="StrikePrice" required="N" />
      <field name="OptAttribute" required="N" />
      <field name="ContractMultiplier" required="N" />
      <field name="CouponRate" required="N" />
      <field name="SecurityExchange" required="N" />
      <field name="Issuer" required="N" />
      <field name="EncodedIssuerLen" required="N" />
      <field name="EncodedIssuer" required="N" />
      <field name="SecurityDesc" required="N" />
      <field name="EncodedSecurityDescLen" required="N" />
      <field name="EncodedSecurityDesc" required="N" />
      <field name="Side" required="N" />
      <field name="OrderQty" required="N" />
      <field name="CashOrderQty" required="N" />
      <field name="OrdType" required="N" />
      <field name="Price" required="N" />
      <field name="StopPx" required="N" />
      <field name="PegDifference" required="N" />
      <field name="DiscretionInst" required="N" />
      <field name="DiscretionOffset" required="N" />
      <field name="Currency" required="N" />
      <field name="ComplianceID" required="N" />
      <field name="SolicitedFlag" required="N" />
      <field name="TimeInForce" required="N" />
      <field name="EffectiveTime" required="N" />
      <field name="ExpireDate" required="N" />
      <field name="ExpireTime" required="N" />
      <field name="ExecInst" required="N" />
      <field name="Rule80A" required="N" />
      <field name="LastShares" required="N" />
      <field name="LastPx" required="N" />
      <field name="LastSpotRate" required="N" />
      <field name="LastForwardPoints" required="N" />
      <field name="LastMkt" required="N" />
      <field name="TradingSessionID" required="N" />
      <field name="LastCapacity" required="N" />
      <field name="LeavesQty" required="N" />
      <field name="CumQty" required="N" />
      <field name="AvgPx" required="Y" />
      <field name="DayOrderQty" required="N" />
      <field name="DayCumQty" required="N" />
      <field name="DayAvgPx" required="N" />
      <field name="GTBookingInst" required="N" />
      <field name="TradeDate" required="N" />
      <field name="TransactTime" required="N" />
      <field name="ReportToExch" required="N" />
      <field name="Commission" required="N" />
      <field name="CommType" required="N" />
      <field name="GrossTradeAmt" required="N" />
      <field name="SettlCurrAmt" required="N" />
      <field name="SettlCurrency" required="N" />
      <field name="SettlCurrFxRate" required="N" />
      <field name="SettlCurrFxRateCalc" required="N" />
      <field name="HandlInst" required="N" />
      <field name="MinQty" required="N" />
      <field name="MaxFloor" required="N" />
      <field name="OpenClose" required="N" />
      <field name="MaxShow" required="N" />
      <field name="Text" required="N" />
      <field name="EncodedTextLen" required="N" />
      <field name="EncodedText" required="N" />
      <field name="FutSettDate2" required="N" />
      <field name="OrderQty2" required="N" />
      <field name="ClearingFirm" required="N" />
      <field name="ClearingAccount" required="N" />
      <field name="MultiLegReportingType" required="N" />
      <field name="QuoteID" required="N" />
      <field name="ContraBroker" required="N" />
      <field name="MaturityTime" required="N" />
      <field name="LiquidityIndicator" required="N" />
      <field name="ContraID" required="N" />
      <field name="CommissionFXCM" required="N" />
      <field name="LastMktPx" required="N" />
    </message>
    <message name="OrderCancelReplaceRequest" msgtype="G" msgcat="app">
      <field name="OrderID" required="N" />
      <field name="ClientID" required="N" />
      <field name="ExecBroker" required="N" />
      <field name="OrigClOrdID" required="Y" />
      <field name="ClOrdID" required="Y" />
      <field name="ListID" required="N" />
      <field name="Account" required="N" />
      <group name="NoAllocs" required="N">
        <field name="AllocAccount" required="N" />
        <field name="AllocShares" required="N" />
      </group>
      <field name="SettlmntTyp" required="N" />
      <field name="FutSettDate" required="N" />
      <field name="HandlInst" required="Y" />
      <field name="ExecInst" required="N" />
      <field name="MinQty" required="N" />
      <field name="MaxFloor" required="N" />
      <field name="ExDestination" required="N" />
      <group name="NoTradingSessions" required="N">
        <field name="TradingSessionID" required="N" />
      </group>
      <field name="Symbol" required="Y" />
      <field name="SymbolSfx" required="N" />
      <field name="SecurityID" required="N" />
      <field name="IDSource" required="N" />
      <field name="SecurityType" required="N" />
      <field name="MaturityMonthYear" required="N" />
      <field name="MaturityDay" required="N" />
      <field name="PutOrCall" required="N" />
      <field name="StrikePrice" required="N" />
      <field name="OptAttribute" required="N" />
      <field name="ContractMultiplier" required="N" />
      <field name="CouponRate" required="N" />
      <field name="SecurityExchange" required="N" />
      <field name="Issuer" required="N" />
      <field name="EncodedIssuerLen" required="N" />
      <field name="EncodedIssuer" required="N" />
      <field name="SecurityDesc" required="N" />
      <field name="EncodedSecurityDescLen" required="N" />
      <field name="EncodedSecurityDesc" required="N" />
      <field name="Side" required="Y" />
      <field name="TransactTime" required="Y" />
      <field name="OrderQty" required="N" />
      <field name="CashOrderQty" required="N" />
      <field name="OrdType" required="Y" />
      <field name="Price" required="N" />
      <field name="StopPx" required="N" />
      <field name="PegDifference" required="N" />
      <field name="DiscretionInst" required="N" />
      <field name="DiscretionOffset" required="N" />
      <field name="ComplianceID" required="N" />
      <field name="SolicitedFlag" required="N" />
      <field name="Currency" required="N" />
      <field name="TimeInForce" required="N" />
      <field name="EffectiveTime" required="N" />
      <field name="ExpireDate" required="N" />
      <field name="ExpireTime" required="N" />
      <field name="GTBookingInst" required="N" />
      <field name="Commission" required="N" />
      <field name="CommType" required="N" />
      <field name="Rule80A" required="N" />
      <field name="ForexReq" required="N" />
      <field name="SettlCurrency" required="N" />
      <field name="Text" required="N" />
      <field name="EncodedTextLen" required="N" />
      <field name="EncodedText" required="N" />
      <field name="FutSettDate2" required="N" />
      <field name="OrderQty2" required="N" />
      <field name="OpenClose" required="N" />
      <field name="CoveredOrUncovered" required="N" />
      <field name="CustomerOrFirm" required="N" />
      <field name="MaxShow" required="N" />
      <field name="LocateReqd" required="N" />
      <field name="ClearingFirm" required="N" />
      <field name="ClearingAccount" required="N" />
    </message>
    <message name="OrderCancelRequest" msgtype="F" msgcat="app">
      <field name="OrigClOrdID" required="Y" />
      <field name="OrderID" required="N" />
      <field name="ClOrdID" required="Y" />
      <field name="ListID" required="N" />
      <field name="Account" required="N" />
      <field name="ClientID" required="N" />
      <field name="ExecBroker" required="N" />
      <field name="Symbol" required="Y" />
      <field name="SymbolSfx" required="N" />
      <field name="SecurityID" required="N" />
      <field name="IDSource" required="N" />
      <field name="SecurityType" required="N" />
      <field name="MaturityMonthYear" required="N" />
      <field name="MaturityDay" required="N" />
      <field name="PutOrCall" required="N" />
      <field name="StrikePrice" required="N" />
      <field name="OptAttribute" required="N" />
      <field name="ContractMultiplier" required="N" />
      <field name="CouponRate" required="N" />
      <field name="SecurityExchange" required="N" />
      <field name="Issuer" required="N" />
      <field name="EncodedIssuerLen" required="N" />
      <field name="EncodedIssuer" required="N" />
      <field name="SecurityDesc" required="N" />
      <field name="EncodedSecurityDescLen" required="N" />
      <field name="EncodedSecurityDesc" required="N" />
      <field name="Side" required="Y" />
      <field name="TransactTime" required="Y" />
      <field name="OrderQty" required="N" />
      <field name="CashOrderQty" required="N" />
      <field name="ComplianceID" required="N" />
      <field name="SolicitedFlag" required="N" />
      <field name="Text" required="N" />
      <field name="EncodedTextLen" required="N" />
      <field name="EncodedText" required="N" />
    </message>
    <message name="OrderCancelReject" msgtype="9" msgcat="app">
      <field name="OrderID" required="Y" />
      <field name="SecondaryOrderID" required="N" />
      <field name="ClOrdID" required="Y" />
      <field name="OrigClOrdID" required="Y" />
      <field name="OrdStatus" required="Y" />
      <field name="ClientID" required="N" />
      <field name="ExecBroker" required="N" />
      <field name="ListID" required="N" />
      <field name="Account" required="N" />
      <field name="TransactTime" required="N" />
      <field name="CxlRejResponseTo" required="Y" />
      <field name="CxlRejReason" required="N" />
      <field name="Text" required="N" />
      <field name="EncodedTextLen" required="N" />
      <field name="EncodedText" required="N" />
    </message>
    <message name="OrderStatusRequest" msgtype="H" msgcat="app">
      <field name="OrderID" required="N" />
      <field name="ClOrdID" required="Y" />
      <field name="ClientID" required="N" />
      <field name="Account" required="N" />
      <field name="ExecBroker" required="N" />
      <field name="Symbol" required="Y" />
      <field name="SymbolSfx" required="N" />
      <field name="SecurityID" required="N" />
      <field name="IDSource" required="N" />
      <field name="SecurityType" required="N" />
      <field name="MaturityMonthYear" required="N" />
      <field name="MaturityDay" required="N" />
      <field name="PutOrCall" required="N" />
      <field name="StrikePrice" required="N" />
      <field name="OptAttribute" required="N" />
      <field name="ContractMultiplier" required="N" />
      <field name="CouponRate" required="N" />
      <field name="SecurityExchange" required="N" />
      <field name="Issuer" required="N" />
      <field name="EncodedIssuerLen" required="N" />
      <field name="EncodedIssuer" required="N" />
      <field name="SecurityDesc" required="N" />
      <field name="EncodedSecurityDescLen" required="N" />
      <field name="EncodedSecurityDesc" required="N" />
      <field name="Side" required="Y" />
    </message>
    <message name="ListStatus" msgtype="N" msgcat="app">
      <field name="ListID" required="Y" />
      <field name="ListStatusType" required="Y" />
      <field name="NoRpts" required="Y" />
      <field name="ListOrderStatus" required="Y" />
      <field name="RptSeq" required="Y" />
      <field name="ListStatusText" required="N" />
      <field name="EncodedListStatusTextLen" required="N" />
      <field name="EncodedListStatusText" required="N" />
      <field name="TransactTime" required="N" />
      <field name="TotNoOrders" required="Y" />
      <group name="NoOrders" required="Y">
        <field name="ClOrdID" required="Y" />
        <field name="CumQty" required="Y" />
        <field name="OrdStatus" required="Y" />
        <field name="LeavesQty" required="Y" />
        <field name="CxlQty" required="Y" />
        <field name="AvgPx" required="Y" />
        <field name="OrdRejReason" required="N" />
        <field name="Text" required="N" />
        <field name="EncodedTextLen" required="N" />
        <field name="EncodedText" required="N" />
      </group>
    </message>
    <message name="ListExecute" msgtype="L" msgcat="app">
      <field name="ListID" required="Y" />
      <field name="ClientBidID" required="N" />
      <field name="BidID" required="N" />
      <field name="TransactTime" required="Y" />
      <field name="Text" required="N" />
      <field name="EncodedTextLen" required="N" />
      <field name="EncodedText" required="N" />
    </message>
    <message name="ListStatusRequest" msgtype="M" msgcat="app">
      <field name="ListID" required="Y" />
      <field name="Text" required="N" />
      <field name="EncodedTextLen" required="N" />
      <field name="EncodedText" required="N" />
    </message>
    <message name="BusinessMessageReject" msgtype="j" msgcat="app">
      <field name="RefSeqNum" required="N" />
      <field name="RefMsgType" required="Y" />
      <field name="BusinessRejectRefID" required="N" />
      <field name="BusinessRejectReason" required="Y" />
      <field name="Text" required="N" />
      <field name="EncodedTextLen" required="N" />
      <field name="EncodedText" required="N" />
    </message>
	  <message name="OrderTimeout" msgtype="OT" msgcat="app">
		  <field name="ClOrdID" required="Y" />
	  </message>
	  <message name="DontKnowTrade" msgtype="Q" msgcat="app">
      <field name="OrderID" required="Y" />
      <field name="ExecID" required="Y" />
	  <field name="ClOrdID" required="N" />
      <field name="DKReason" required="Y" />
      <field name="Symbol" required="Y" />
      <field name="Side" required="Y" />
      <field name="LastShares" required="N" />
      <field name="Text" required="N" />
    </message>
  </messages>
  <fields>
    <field number="1" name="Account" type="STRING" />
    <field number="2" name="AdvId" type="STRING" />
    <field number="3" name="AdvRefID" type="STRING" />
    <field number="4" name="AdvSide" type="CHAR">
      <value enum="B" description="BUY" />
      <value enum="S" description="SELL" />
      <value enum="X" description="CROSS" />
      <value enum="T" description="TRADE" />
    </field>
    <field number="5" name="AdvTransType" type="STRING">
      <value enum="N" description="NEW" />
      <value enum="C" description="CANCEL" />
      <value enum="R" description="REPLACE" />
    </field>
    <field number="6" name="AvgPx" type="PRICE" />
    <field number="7" name="BeginSeqNo" type="INT" />
    <field number="8" name="BeginString" type="STRING" />
    <field number="9" name="BodyLength" type="INT" />
    <field number="10" name="CheckSum" type="STRING" />
    <field number="11" name="ClOrdID" type="STRING" />
    <field number="12" name="Commission" type="AMT" />
    <field number="13" name="CommType" type="CHAR">
      <value enum="1" description="PER_SHARE" />
      <value enum="2" description="PERCENTAGE" />
      <value enum="3" description="ABSOLUTE" />
    </field>
    <field number="14" name="CumQty" type="QTY" />
    <field number="15" name="Currency" type="CURRENCY" />
    <field number="16" name="EndSeqNo" type="INT" />
    <field number="17" name="ExecID" type="STRING" />
    <field number="18" name="ExecInst" type="MULTIPLEVALUESTRING">
      <value enum="1" description="NOT_HELD" />
      <value enum="2" description="WORK" />
      <value enum="3" description="GO_ALONG" />
      <value enum="4" description="OVER_THE_DAY" />
      <value enum="5" description="HELD" />
      <value enum="6" description="PARTICIPATE_DONT_INITIATE" />
      <value enum="7" description="STRICT_SCALE" />
      <value enum="8" description="TRY_TO_SCALE" />
      <value enum="9" description="STAY_ON_BIDSIDE" />
      <value enum="0" description="STAY_ON_OFFERSIDE" />
      <value enum="A" description="NO_CROSS" />
      <value enum="B" description="OK_TO_CROSS" />
      <value enum="C" description="CALL_FIRST" />
      <value enum="D" description="PERCENT_OF_VOLUME" />
      <value enum="E" description="DO_NOT_INCREASE_DNI" />
      <value enum="F" description="DO_NOT_REDUCE_DNR" />
      <value enum="G" description="ALL_OR_NONE_AON" />
      <value enum="I" description="INSTITUTIONS_ONLY" />
      <value enum="L" description="LAST_PEG" />
      <value enum="M" description="MIDPRICE_PEG" />
      <value enum="N" description="NONNEGOTIABLE" />
      <value enum="O" description="OPENING_PEG" />
      <value enum="P" description="MARKET_PEG" />
      <value enum="R" description="PRIMARY_PEG" />
      <value enum="S" description="SUSPEND" />
      <value enum="T" description="FIXED_PEG" />
      <value enum="U" description="CUSTOMER_DISPLAY_INSTRUCTION" />
      <value enum="V" description="NETTING" />
      <value enum="W" description="PEG_TO_VWAP" />
    </field>
    <field number="19" name="ExecRefID" type="STRING" />
    <field number="20" name="ExecTransType" type="CHAR">
      <value enum="0" description="NEW" />
      <value enum="1" description="CANCEL" />
      <value enum="2" description="CORRECT" />
      <value enum="3" description="STATUS" />
    </field>
    <field number="21" name="HandlInst" type="CHAR">
      <value enum="1" description="AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION" />
      <value enum="2" description="AUTOMATED_EXECUTION_ORDER_PUBLIC_BROKER_INTERVENTION_OK" />
      <value enum="3" description="MANUAL_ORDER_BEST_EXECUTION" />
    </field>
    <field number="22" name="IDSource" type="STRING">
      <value enum="1" description="CUSIP" />
      <value enum="2" description="SEDOL" />
      <value enum="3" description="QUIK" />
      <value enum="4" description="ISIN_NUMBER" />
      <value enum="5" description="RIC_CODE" />
      <value enum="6" description="ISO_CURRENCY_CODE" />
      <value enum="7" description="ISO_COUNTRY_CODE" />
      <value enum="8" description="EXCHANGE_SYMBOL" />
      <value enum="9" description="CONSOLIDATED_TAPE_ASSOCIATION" />
    </field>
    <field number="23" name="IOIid" type="STRING" />
    <field number="24" name="IOIOthSvc" type="CHAR" />
    <field number="25" name="IOIQltyInd" type="CHAR">
      <value enum="L" description="LOW" />
      <value enum="M" description="MEDIUM" />
      <value enum="H" description="HIGH" />
    </field>
    <field number="26" name="IOIRefID" type="STRING" />
    <field number="27" name="IOIShares" type="STRING" />
    <field number="28" name="IOITransType" type="CHAR">
      <value enum="N" description="NEW" />
      <value enum="C" description="CANCEL" />
      <value enum="R" description="REPLACE" />
    </field>
    <field number="29" name="LastCapacity" type="CHAR">
      <value enum="1" description="AGENT" />
      <value enum="2" description="CROSS_AS_AGENT" />
      <value enum="3" description="CROSS_AS_PRINCIPAL" />
      <value enum="4" description="PRINCIPAL" />
    </field>
    <field number="30" name="LastMkt" type="EXCHANGE" />
    <field number="31" name="LastPx" type="PRICE" />
    <field number="32" name="LastShares" type="QTY" />
    <field number="33" name="LinesOfText" type="INT" />
    <field number="34" name="MsgSeqNum" type="INT" />
    <field number="35" name="MsgType" type="STRING" />
    <field number="36" name="NewSeqNo" type="INT" />
    <field number="37" name="OrderID" type="STRING" />
    <field number="38" name="OrderQty" type="QTY" />
    <field number="39" name="OrdStatus" type="CHAR">
      <value enum="0" description="NEW" />
      <value enum="1" description="PARTIALLY_FILLED" />
      <value enum="2" description="FILLED" />
      <value enum="3" description="DONE_FOR_DAY" />
      <value enum="4" description="CANCELED" />
      <value enum="5" description="REPLACED" />
      <value enum="6" description="PENDING_CANCEL" />
      <value enum="7" description="STOPPED" />
      <value enum="8" description="REJECTED" />
      <value enum="9" description="SUSPENDED" />
      <value enum="A" description="PENDING_NEW" />
      <value enum="B" description="CALCULATED" />
      <value enum="C" description="EXPIRED" />
      <value enum="D" description="ACCEPTED_FOR_BIDDING" />
      <value enum="E" description="PENDING_REPLACE" />
    </field>
    <field number="40" name="OrdType" type="CHAR">
      <value enum="1" description="MARKET" />
      <value enum="2" description="LIMIT" />
      <value enum="3" description="STOP" />
      <value enum="4" description="STOP_LIMIT" />
      <value enum="5" description="MARKET_ON_CLOSE" />
      <value enum="6" description="WITH_OR_WITHOUT" />
      <value enum="7" description="LIMIT_OR_BETTER" />
      <value enum="8" description="LIMIT_WITH_OR_WITHOUT" />
      <value enum="9" description="ON_BASIS" />
      <value enum="A" description="ON_CLOSE" />
      <value enum="B" description="LIMIT_ON_CLOSE" />
      <value enum="C" description="FOREX_MARKET" />
      <value enum="D" description="PREVIOUSLY_QUOTED" />
      <value enum="E" description="PREVIOUSLY_INDICATED" />
      <value enum="F" description="FOREX_LIMIT" />
      <value enum="G" description="FOREX_SWAP" />
      <value enum="H" description="FOREX_PREVIOUSLY_QUOTED" />
      <value enum="I" description="FUNARI" />
      <value enum="P" description="PEGGED" />
    </field>
    <field number="41" name="OrigClOrdID" type="STRING" />
    <field number="42" name="OrigTime" type="UTCTIMESTAMP" />
    <field number="43" name="PossDupFlag" type="BOOLEAN">
      <value enum="Y" description="POSSIBLE_DUPLICATE" />
      <value enum="N" description="ORIGINAL_TRANSMISSION" />
    </field>
    <field number="44" name="Price" type="PRICE" />
    <field number="45" name="RefSeqNum" type="INT" />
    <field number="46" name="RelatdSym" type="STRING" />
    <field number="47" name="Rule80A" type="CHAR">
      <value enum="A" description="AGENCY_SINGLE_ORDER" />
      <value enum="B" description="SHORT_EXEMPT_TRANSACTION_B" />
      <value enum="C" description="PROGRAM_ORDER_NONINDEX_ARB_FOR_MEMBER_FIRMORG" />
      <value enum="D" description="PROGRAM_ORDER_INDEX_ARB_FOR_MEMBER_FIRMORG" />
      <value enum="E" description="REGISTERED_EQUITY_MARKET_MAKER_TRADES" />
      <value enum="F" description="SHORT_EXEMPT_TRANSACTION_F" />
      <value enum="H" description="SHORT_EXEMPT_TRANSACTION_H" />
      <value enum="J" description="PROGRAM_ORDER_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER" />
      <value enum="K" description="PROGRAM_ORDER_NONINDEX_ARB_FOR_INDIVIDUAL_CUSTOMER" />
      <value enum="L" description="SHORT_EXEMPT_AFFILIATED" />
      <value enum="M" description="PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_MEMBER" />
      <value enum="N" description="PROGRAM_ORDER_NONINDEX_ARB_FOR_OTHER_MEMBER" />
      <value enum="O" description="COMPETING_DEALER_TRADES_O" />
      <value enum="P" description="PRINCIPAL" />
      <value enum="R" description="COMPETING_DEALER_TRADES_R" />
      <value enum="S" description="SPECIALIST_TRADES" />
      <value enum="T" description="COMPETING_DEALER_TRADES_T" />
      <value enum="U" description="PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_AGENCY" />
      <value enum="W" description="ALL_OTHER_ORDERS_AS_AGENT_FOR_OTHER_MEMBER" />
      <value enum="X" description="SHORT_EXEMPT_NOT_AFFILIATED" />
      <value enum="Y" description="PROGRAM_ORDER_NONINDEX_ARB_FOR_OTHER_AGENCY" />
      <value enum="Z" description="SHORT_EXEMPT_NONMEMBER" />
    </field>
    <field number="48" name="SecurityID" type="STRING" />
    <field number="49" name="SenderCompID" type="STRING" />
    <field number="50" name="SenderSubID" type="STRING" />
    <field number="52" name="SendingTime" type="UTCTIMESTAMP" />
    <field number="53" name="Shares" type="QTY" />
    <field number="54" name="Side" type="CHAR">
      <value enum="1" description="BUY" />
      <value enum="2" description="SELL" />
      <value enum="3" description="BUY_MINUS" />
      <value enum="4" description="SELL_PLUS" />
      <value enum="5" description="SELL_SHORT" />
      <value enum="6" description="SELL_SHORT_EXEMPT" />
      <value enum="7" description="D" />
      <value enum="8" description="CROSS" />
      <value enum="9" description="CROSS_SHORT" />
    </field>
    <field number="55" name="Symbol" type="STRING" />
    <field number="56" name="TargetCompID" type="STRING" />
    <field number="57" name="TargetSubID" type="STRING" />
    <field number="58" name="Text" type="STRING" />
    <field number="59" name="TimeInForce" type="CHAR">
      <value enum="0" description="DAY" />
      <value enum="1" description="GOOD_TILL_CANCEL" />
      <value enum="2" description="AT_THE_OPENING" />
      <value enum="3" description="IMMEDIATE_OR_CANCEL" />
      <value enum="4" description="FILL_OR_KILL" />
      <value enum="5" description="GOOD_TILL_CROSSING" />
      <value enum="6" description="GOOD_TILL_DATE" />
    </field>
    <field number="60" name="TransactTime" type="UTCTIMESTAMP" />
    <field number="61" name="Urgency" type="CHAR">
      <value enum="0" description="NORMAL" />
      <value enum="1" description="FLASH" />
      <value enum="2" description="BACKGROUND" />
    </field>
    <field number="62" name="ValidUntilTime" type="UTCTIMESTAMP" />
    <field number="63" name="SettlmntTyp" type="CHAR">
      <value enum="0" description="REGULAR" />
      <value enum="1" description="CASH" />
      <value enum="2" description="NEXT_DAY" />
      <value enum="3" description="TPLUS2" />
      <value enum="4" description="TPLUS3" />
      <value enum="5" description="TPLUS4" />
      <value enum="6" description="FUTURE" />
      <value enum="7" description="WHEN_ISSUED" />
      <value enum="8" description="SELLERS_OPTION" />
      <value enum="9" description="TPLUS5" />
    </field>
    <field number="64" name="FutSettDate" type="LOCALMKTDATE" />
    <field number="65" name="SymbolSfx" type="STRING" />
    <field number="66" name="ListID" type="STRING" />
    <field number="67" name="ListSeqNo" type="INT" />
    <field number="68" name="TotNoOrders" type="INT" />
    <field number="69" name="ListExecInst" type="STRING" />
    <field number="70" name="AllocID" type="STRING" />
    <field number="71" name="AllocTransType" type="CHAR">
      <value enum="0" description="NEW" />
      <value enum="1" description="REPLACE" />
      <value enum="2" description="CANCEL" />
      <value enum="3" description="PRELIMINARY" />
      <value enum="4" description="CALCULATED" />
      <value enum="5" description="CALCULATED_WITHOUT_PRELIMINARY" />
    </field>
    <field number="72" name="RefAllocID" type="STRING" />
    <field number="73" name="NoOrders" type="INT" />
    <field number="74" name="AvgPrxPrecision" type="INT" />
    <field number="75" name="TradeDate" type="LOCALMKTDATE" />
    <field number="76" name="ExecBroker" type="STRING" />
    <field number="77" name="OpenClose" type="CHAR">
      <value enum="O" description="OPEN" />
      <value enum="C" description="CLOSE" />
    </field>
    <field number="78" name="NoAllocs" type="INT" />
    <field number="79" name="AllocAccount" type="STRING" />
    <field number="80" name="AllocShares" type="QTY" />
    <field number="81" name="ProcessCode" type="CHAR">
      <value enum="0" description="REGULAR" />
      <value enum="1" description="SOFT_DOLLAR" />
      <value enum="2" description="STEPIN" />
      <value enum="3" description="STEPOUT" />
      <value enum="4" description="SOFTDOLLAR_STEPIN" />
      <value enum="5" description="SOFTDOLLAR_STEPOUT" />
      <value enum="6" description="PLAN_SPONSOR" />
    </field>
    <field number="82" name="NoRpts" type="INT" />
    <field number="83" name="RptSeq" type="INT" />
    <field number="84" name="CxlQty" type="QTY" />
    <field number="85" name="NoDlvyInst" type="INT" />
    <field number="86" name="DlvyInst" type="STRING" />
    <field number="87" name="AllocStatus" type="INT">
      <value enum="0" description="ACCEPTED" />
      <value enum="1" description="REJECTED" />
      <value enum="2" description="PARTIAL_ACCEPT" />
      <value enum="3" description="RECEIVED" />
    </field>
    <field number="88" name="AllocRejCode" type="INT">
      <value enum="0" description="UNKNOWN_ACCOUNT" />
      <value enum="1" description="INCORRECT_QUANTITY" />
      <value enum="2" description="INCORRECT_AVERAGE_PRICE" />
      <value enum="3" description="UNKNOWN_EXECUTING_BROKER_MNEMONIC" />
      <value enum="4" description="COMMISSION_DIFFERENCE" />
      <value enum="5" description="UNKNOWN_ORDERID" />
      <value enum="6" description="UNKNOWN_LISTID" />
      <value enum="7" description="OTHER" />
    </field>
    <field number="89" name="Signature" type="DATA" />
    <field number="90" name="SecureDataLen" type="INT" />
    <field number="91" name="SecureData" type="DATA" />
    <field number="92" name="BrokerOfCredit" type="STRING" />
    <field number="93" name="SignatureLength" type="INT" />
    <field number="94" name="EmailType" type="CHAR">
      <value enum="0" description="NEW" />
      <value enum="1" description="REPLY" />
      <value enum="2" description="ADMIN_REPLY" />
    </field>
    <field number="95" name="RawDataLength" type="INT" />
    <field number="96" name="RawData" type="DATA" />
    <field number="97" name="PossResend" type="BOOLEAN" />
    <field number="98" name="EncryptMethod" type="INT">
      <value enum="0" description="NONE_OTHER" />
      <value enum="1" description="PKCS" />
      <value enum="2" description="DES" />
      <value enum="3" description="PKCSDES" />
      <value enum="4" description="PGPDES" />
      <value enum="5" description="PGPDESMD5" />
      <value enum="6" description="PEMDESMD5" />
    </field>
    <field number="99" name="StopPx" type="PRICE" />
    <field number="100" name="ExDestination" type="EXCHANGE" />
    <field number="102" name="CxlRejReason" type="INT">
      <value enum="0" description="TOO_LATE_TO_CANCEL" />
      <value enum="1" description="UNKNOWN_ORDER" />
      <value enum="2" description="BROKER_OPTION" />
      <value enum="3" description="ALREADY_PENDING" />
      <value enum="6" description="DUPLICATE_ID" />
      <value enum="99" description="OTHER" />
    </field>
    <field number="103" name="OrdRejReason" type="INT">
      <value enum="0" description="BROKER_OPTION" />
      <value enum="1" description="UNKNOWN_SYMBOL" />
      <value enum="2" description="EXCHANGE_CLOSED" />
      <value enum="3" description="ORDER_EXCEEDS_LIMIT" />
      <value enum="4" description="TOO_LATE_TO_ENTER" />
      <value enum="5" description="UNKNOWN_ORDER" />
      <value enum="6" description="DUPLICATE_ORDER" />
      <value enum="7" description="DUPLICATE_VERBALYES" />
      <value enum="8" description="STALE_ORDER" />
      <value enum="99" description="OTHER" />
    </field>
    <field number="104" name="IOIQualifier" type="CHAR">
      <value enum="A" description="ALL_OR_NONE" />
      <value enum="C" description="AT_THE_CLOSE" />
      <value enum="I" description="IN_TOUCH_WITH" />
      <value enum="L" description="LIMIT" />
      <value enum="M" description="MORE_BEHIND" />
      <value enum="O" description="AT_THE_OPEN" />
      <value enum="P" description="TAKING_A_POSITION" />
      <value enum="Q" description="AT_THE_MARKET" />
      <value enum="R" description="READY_TO_TRADE" />
      <value enum="S" description="PORTFOLIO_SHOWN" />
      <value enum="T" description="THROUGH_THE_DAY" />
      <value enum="V" description="VERSUS" />
      <value enum="W" description="INDICATION_WORKING_AWAY" />
      <value enum="X" description="CROSSING_OPPORTUNITY" />
      <value enum="Y" description="AT_THE_MIDPOINT" />
      <value enum="Z" description="PREOPEN" />
    </field>
    <field number="105" name="WaveNo" type="STRING" />
    <field number="106" name="Issuer" type="STRING" />
    <field number="107" name="SecurityDesc" type="STRING" />
    <field number="108" name="HeartBtInt" type="INT" />
    <field number="109" name="ClientID" type="STRING" />
    <field number="110" name="MinQty" type="QTY" />
    <field number="111" name="MaxFloor" type="QTY" />
    <field number="112" name="TestReqID" type="STRING" />
    <field number="113" name="ReportToExch" type="BOOLEAN">
      <value enum="Y" description="YES" />
      <value enum="N" description="NO" />
    </field>
    <field number="114" name="LocateReqd" type="BOOLEAN">
      <value enum="Y" description="YES" />
      <value enum="N" description="NO" />
    </field>
    <field number="115" name="OnBehalfOfCompID" type="STRING" />
    <field number="116" name="OnBehalfOfSubID" type="STRING" />
    <field number="117" name="QuoteID" type="STRING" />
    <field number="118" name="NetMoney" type="AMT" />
    <field number="119" name="SettlCurrAmt" type="AMT" />
    <field number="120" name="SettlCurrency" type="CURRENCY" />
    <field number="121" name="ForexReq" type="BOOLEAN">
      <value enum="Y" description="YES" />
      <value enum="N" description="NO" />
    </field>
    <field number="122" name="OrigSendingTime" type="UTCTIMESTAMP" />
    <field number="123" name="GapFillFlag" type="BOOLEAN">
      <value enum="Y" description="GAP_FILL_MESSAGE_MSGSEQNUM_FIELD_VALID" />
      <value enum="N" description="SEQUENCE_RESET_IGNORE_MSGSEQNUM" />
    </field>
    <field number="124" name="NoExecs" type="INT" />
    <field number="125" name="CxlType" type="CHAR" />
    <field number="126" name="ExpireTime" type="UTCTIMESTAMP" />
    <field number="127" name="DKReason" type="CHAR">
      <value enum="A" description="UNKNOWN_SYMBOL" />
      <value enum="B" description="WRONG_SIDE" />
      <value enum="C" description="QUANTITY_EXCEEDS_ORDER" />
      <value enum="D" description="NO_MATCHING_ORDER" />
      <value enum="E" description="PRICE_EXCEEDS_LIMIT" />
      <value enum="Z" description="OTHER" />
    </field>
    <field number="128" name="DeliverToCompID" type="STRING" />
    <field number="129" name="DeliverToSubID" type="STRING" />
    <field number="130" name="IOINaturalFlag" type="BOOLEAN">
      <value enum="Y" description="NATURAL" />
      <value enum="N" description="NOT_NATURAL" />
    </field>
    <field number="131" name="QuoteReqID" type="STRING" />
    <field number="132" name="BidPx" type="PRICE" />
    <field number="133" name="OfferPx" type="PRICE" />
    <field number="134" name="BidSize" type="QTY" />
    <field number="135" name="OfferSize" type="QTY" />
    <field number="136" name="NoMiscFees" type="INT" />
    <field number="137" name="MiscFeeAmt" type="AMT" />
    <field number="138" name="MiscFeeCurr" type="CURRENCY" />
    <field number="139" name="MiscFeeType" type="CHAR">
      <value enum="1" description="REGULATORY" />
      <value enum="2" description="TAX" />
      <value enum="3" description="LOCAL_COMMISSION" />
      <value enum="4" description="EXCHANGE_FEES" />
      <value enum="5" description="STAMP" />
      <value enum="6" description="LEVY" />
      <value enum="7" description="OTHER" />
      <value enum="8" description="MARKUP" />
      <value enum="9" description="CONSUMPTION_TAX" />
    </field>
    <field number="140" name="PrevClosePx" type="PRICE" />
    <field number="141" name="ResetSeqNumFlag" type="BOOLEAN">
      <value enum="Y" description="YES_RESET_SEQUENCE_NUMBERS" />
      <value enum="N" description="NO" />
    </field>
    <field number="142" name="SenderLocationID" type="STRING" />
    <field number="143" name="TargetLocationID" type="STRING" />
    <field number="144" name="OnBehalfOfLocationID" type="STRING" />
    <field number="145" name="DeliverToLocationID" type="STRING" />
    <field number="146" name="NoRelatedSym" type="INT" />
    <field number="147" name="Subject" type="STRING" />
    <field number="148" name="Headline" type="STRING" />
    <field number="149" name="URLLink" type="STRING" />
    <field number="150" name="ExecType" type="CHAR">
      <value enum="0" description="NEW" />
      <value enum="1" description="PARTIAL_FILL" />
      <value enum="2" description="FILL" />
      <value enum="3" description="DONE_FOR_DAY" />
      <value enum="4" description="CANCELED" />
      <value enum="5" description="REPLACE" />
      <value enum="6" description="PENDING_CANCEL" />
      <value enum="7" description="STOPPED" />
      <value enum="8" description="REJECTED" />
      <value enum="9" description="SUSPENDED" />
      <value enum="A" description="PENDING_NEW" />
      <value enum="B" description="CALCULATED" />
      <value enum="C" description="EXPIRED" />
      <value enum="D" description="RESTATED" />
      <value enum="E" description="PENDING_REPLACE" />
    </field>
    <field number="151" name="LeavesQty" type="QTY" />
    <field number="152" name="CashOrderQty" type="QTY" />
    <field number="153" name="AllocAvgPx" type="PRICE" />
    <field number="154" name="AllocNetMoney" type="AMT" />
    <field number="155" name="SettlCurrFxRate" type="FLOAT" />
    <field number="156" name="SettlCurrFxRateCalc" type="CHAR">
      <value enum="M" description="MULTIPLY" />
      <value enum="D" description="DIVIDE" />
    </field>
    <field number="157" name="NumDaysInterest" type="INT" />
    <field number="158" name="AccruedInterestRate" type="FLOAT" />
    <field number="159" name="AccruedInterestAmt" type="AMT" />
    <field number="160" name="SettlInstMode" type="CHAR">
      <value enum="0" description="DEFAULT" />
      <value enum="1" description="STANDING_INSTRUCTIONS_PROVIDED" />
      <value enum="2" description="SPECIFIC_ALLOCATION_ACCOUNT_OVERRIDING" />
      <value enum="3" description="SPECIFIC_ALLOCATION_ACCOUNT_STANDING" />
    </field>
    <field number="161" name="AllocText" type="STRING" />
    <field number="162" name="SettlInstID" type="STRING" />
    <field number="163" name="SettlInstTransType" type="CHAR">
      <value enum="N" description="NEW" />
      <value enum="C" description="CANCEL" />
      <value enum="R" description="REPLACE" />
    </field>
    <field number="164" name="EmailThreadID" type="STRING" />
    <field number="165" name="SettlInstSource" type="CHAR">
      <value enum="1" description="BROKER" />
      <value enum="2" description="INSTITUTION" />
    </field>
    <field number="166" name="SettlLocation" type="STRING">
      <value enum="CED" description="CEDEL" />
      <value enum="DTC" description="DEPOSITORY_TRUST_COMPANY" />
      <value enum="EUR" description="EUROCLEAR" />
      <value enum="FED" description="FEDERAL_BOOK_ENTRY" />
      <value enum="PNY" description="PHYSICAL" />
      <value enum="PTC" description="PARTICIPANT_TRUST_COMPANY" />
      <value enum="ISO" description="LOCAL_MARKET_SETTLE_LOCATION" />
    </field>
    <field number="167" name="SecurityType" type="STRING">
      <value enum="BA" description="BANKERS_ACCEPTANCE" />
      <value enum="CB" description="CONVERTIBLE_BOND" />
      <value enum="CD" description="CERTIFICATE_OF_DEPOSIT" />
      <value enum="CMO" description="COLLATERALIZE_MORTGAGE_OBLIGATION" />
      <value enum="CORP" description="CORPORATE_BOND" />
      <value enum="CP" description="COMMERCIAL_PAPER" />
      <value enum="CPP" description="CORPORATE_PRIVATE_PLACEMENT" />
      <value enum="CS" description="COMMON_STOCK" />
      <value enum="FHA" description="FEDERAL_HOUSING_AUTHORITY" />
      <value enum="FHL" description="FEDERAL_HOME_LOAN" />
      <value enum="FN" description="FEDERAL_NATIONAL_MORTGAGE_ASSOCIATION" />
      <value enum="FOR" description="FOREIGN_EXCHANGE_CONTRACT" />
      <value enum="FUT" description="FUTURE" />
      <value enum="GN" description="GOVERNMENT_NATIONAL_MORTGAGE_ASSOCIATION" />
      <value enum="GOVT" description="TREASURIES_PLUS_AGENCY_DEBENTURE" />
      <value enum="MF" description="MUTUAL_FUND" />
      <value enum="MIO" description="MORTGAGE_INTEREST_ONLY" />
      <value enum="MPO" description="MORTGAGE_PRINCIPAL_ONLY" />
      <value enum="MPP" description="MORTGAGE_PRIVATE_PLACEMENT" />
      <value enum="MPT" description="MISCELLANEOUS_PASSTHRU" />
      <value enum="MUNI" description="MUNICIPAL_BOND" />
      <value enum="NONE" description="NO_ISITC_SECURITY_TYPE" />
      <value enum="OPT" description="OPTION" />
      <value enum="PS" description="PREFERRED_STOCK" />
      <value enum="RP" description="REPURCHASE_AGREEMENT" />
      <value enum="RVRP" description="REVERSE_REPURCHASE_AGREEMENT" />
      <value enum="SL" description="STUDENT_LOAN_MARKETING_ASSOCIATION" />
      <value enum="TD" description="TIME_DEPOSIT" />
      <value enum="USTB" description="US_TREASURY_BILL" />
      <value enum="WAR" description="WARRANT" />
      <value enum="ZOO" description="CATS_TIGERS" />
    </field>
    <field number="168" name="EffectiveTime" type="UTCTIMESTAMP" />
    <field number="169" name="StandInstDbType" type="INT">
      <value enum="0" description="OTHER" />
      <value enum="1" description="DTC_SID" />
      <value enum="2" description="THOMSON_ALERT" />
      <value enum="3" description="A_GLOBAL_CUSTODIAN" />
    </field>
    <field number="170" name="StandInstDbName" type="STRING" />
    <field number="171" name="StandInstDbID" type="STRING" />
    <field number="172" name="SettlDeliveryType" type="INT" />
    <field number="173" name="SettlDepositoryCode" type="STRING" />
    <field number="174" name="SettlBrkrCode" type="STRING" />
    <field number="175" name="SettlInstCode" type="STRING" />
    <field number="176" name="SecuritySettlAgentName" type="STRING" />
    <field number="177" name="SecuritySettlAgentCode" type="STRING" />
    <field number="178" name="SecuritySettlAgentAcctNum" type="STRING" />
    <field number="179" name="SecuritySettlAgentAcctName" type="STRING" />
    <field number="180" name="SecuritySettlAgentContactName" type="STRING" />
    <field number="181" name="SecuritySettlAgentContactPhone" type="STRING" />
    <field number="182" name="CashSettlAgentName" type="STRING" />
    <field number="183" name="CashSettlAgentCode" type="STRING" />
    <field number="184" name="CashSettlAgentAcctNum" type="STRING" />
    <field number="185" name="CashSettlAgentAcctName" type="STRING" />
    <field number="186" name="CashSettlAgentContactName" type="STRING" />
    <field number="187" name="CashSettlAgentContactPhone" type="STRING" />
    <field number="188" name="BidSpotRate" type="PRICE" />
    <field number="189" name="BidForwardPoints" type="PRICEOFFSET" />
    <field number="190" name="OfferSpotRate" type="PRICE" />
    <field number="191" name="OfferForwardPoints" type="PRICEOFFSET" />
    <field number="192" name="OrderQty2" type="QTY" />
    <field number="193" name="FutSettDate2" type="LOCALMKTDATE" />
    <field number="194" name="LastSpotRate" type="PRICE" />
    <field number="195" name="LastForwardPoints" type="PRICEOFFSET" />
    <field number="196" name="AllocLinkID" type="STRING" />
    <field number="197" name="AllocLinkType" type="INT">
      <value enum="0" description="FX_NETTING" />
      <value enum="1" description="FX_SWAP" />
    </field>
    <field number="198" name="SecondaryOrderID" type="STRING" />
    <field number="199" name="NoIOIQualifiers" type="INT" />
    <field number="200" name="MaturityMonthYear" type="MONTHYEAR" />
    <field number="201" name="PutOrCall" type="INT">
      <value enum="0" description="PUT" />
      <value enum="1" description="CALL" />
    </field>
    <field number="202" name="StrikePrice" type="PRICE" />
    <field number="203" name="CoveredOrUncovered" type="INT">
      <value enum="0" description="COVERED" />
      <value enum="1" description="UNCOVERED" />
    </field>
    <field number="204" name="CustomerOrFirm" type="INT">
      <value enum="0" description="CUSTOMER" />
      <value enum="1" description="FIRM" />
    </field>
    <field number="205" name="MaturityDay" type="DAYOFMONTH" />
    <field number="206" name="OptAttribute" type="CHAR" />
    <field number="207" name="SecurityExchange" type="EXCHANGE" />
    <field number="208" name="NotifyBrokerOfCredit" type="BOOLEAN">
      <value enum="Y" description="DETAILS_SHOULD_BE_COMMUNICATED" />
      <value enum="N" description="DETAILS_SHOULD_NOT_BE_COMMUNICATED" />
    </field>
    <field number="209" name="AllocHandlInst" type="INT">
      <value enum="1" description="MATCH" />
      <value enum="2" description="FORWARD" />
      <value enum="3" description="FORWARD_AND_MATCH" />
    </field>
    <field number="210" name="MaxShow" type="QTY" />
    <field number="211" name="PegDifference" type="PRICEOFFSET" />
    <field number="212" name="XmlDataLen" type="INT" />
    <field number="213" name="XmlData" type="DATA" />
    <field number="214" name="SettlInstRefID" type="STRING" />
    <field number="215" name="NoRoutingIDs" type="INT" />
    <field number="216" name="RoutingType" type="INT">
      <value enum="1" description="TARGET_FIRM" />
      <value enum="2" description="TARGET_LIST" />
      <value enum="3" description="BLOCK_FIRM" />
      <value enum="4" description="BLOCK_LIST" />
    </field>
    <field number="217" name="RoutingID" type="STRING" />
    <field number="218" name="SpreadToBenchmark" type="PRICEOFFSET" />
    <field number="219" name="Benchmark" type="CHAR">
      <value enum="1" description="CURVE" />
      <value enum="2" description="FIVEYR" />
      <value enum="3" description="OLD5" />
      <value enum="4" description="TENYR" />
      <value enum="5" description="OLD10" />
      <value enum="6" description="THIRTYYR" />
      <value enum="7" description="OLD30" />
      <value enum="8" description="THREEMOLIBOR" />
      <value enum="9" description="SIXMOLIBOR" />
    </field>
    <field number="223" name="CouponRate" type="FLOAT" />
    <field number="231" name="ContractMultiplier" type="FLOAT" />
    <field number="262" name="MDReqID" type="STRING" />
    <field number="263" name="SubscriptionRequestType" type="CHAR">
      <value enum="0" description="SNAPSHOT" />
      <value enum="1" description="SNAPSHOT_PLUS_UPDATES" />
      <value enum="2" description="DISABLE_PREVIOUS" />
    </field>
    <field number="264" name="MarketDepth" type="INT">
      <!--
      Temporarily commented out until we can handle
      N>1 scenario

      <value enum="0" description="FULL_BOOK"/>
      <value enum="1" description="TOP_OF_BOOK"/>
-->
    </field>
    <field number="265" name="MDUpdateType" type="INT">
      <value enum="0" description="FULL_REFRESH" />
      <value enum="1" description="INCREMENTAL_REFRESH" />
    </field>
    <field number="266" name="AggregatedBook" type="BOOLEAN">
      <value enum="Y" description="ONE_BOOK_ENTRY_PER_SIDE_PER_PRICE" />
      <value enum="N" description="MULTIPLE_ENTRIES_PER_SIDE_PER_PRICE_ALLOWED" />
    </field>
    <field number="267" name="NoMDEntryTypes" type="INT" />
    <field number="268" name="NoMDEntries" type="INT" />
    <field number="269" name="MDEntryType" type="CHAR">
      <value enum="0" description="BID" />
      <value enum="1" description="OFFER" />
      <value enum="2" description="TRADE" />
      <value enum="3" description="INDEX_VALUE" />
      <value enum="4" description="OPENING_PRICE" />
      <value enum="5" description="CLOSING_PRICE" />
      <value enum="6" description="SETTLEMENT_PRICE" />
      <value enum="7" description="TRADING_SESSION_HIGH_PRICE" />
      <value enum="8" description="TRADING_SESSION_LOW_PRICE" />
      <value enum="9" description="TRADING_SESSION_VWAP_PRICE" />
    </field>
    <field number="270" name="MDEntryPx" type="PRICE" />
    <field number="271" name="MDEntrySize" type="QTY" />
    <field number="272" name="MDEntryDate" type="UTCDATE" />
    <field number="273" name="MDEntryTime" type="UTCTIMEONLY" />
    <field number="274" name="TickDirection" type="CHAR">
      <value enum="0" description="PLUS_TICK" />
      <value enum="1" description="ZEROPLUS_TICK" />
      <value enum="2" description="MINUS_TICK" />
      <value enum="3" description="ZEROMINUS_TICK" />
    </field>
    <field number="275" name="MDMkt" type="EXCHANGE" />
    <field number="276" name="QuoteCondition" type="MULTIPLEVALUESTRING">
      <value enum="A" description="OPEN_ACTIVE" />
      <value enum="B" description="CLOSED_INACTIVE" />
      <value enum="C" description="EXCHANGE_BEST" />
      <value enum="D" description="CONSOLIDATED_BEST" />
      <value enum="E" description="LOCKED" />
      <value enum="F" description="CROSSED" />
      <value enum="G" description="DEPTH" />
      <value enum="H" description="FAST_TRADING" />
      <value enum="I" description="NONFIRM" />
    </field>
    <field number="277" name="TradeCondition" type="MULTIPLEVALUESTRING">
      <value enum="A" description="CASH" />
      <value enum="B" description="AVERAGE_PRICE_TRADE" />
      <value enum="C" description="CASH_TRADE" />
      <value enum="D" description="NEXT_DAY" />
      <value enum="E" description="OPENING_REOPENING_TRADE_DETAIL" />
      <value enum="F" description="INTRADAY_TRADE_DETAIL" />
      <value enum="G" description="RULE_127_TRADE" />
      <value enum="H" description="RULE_155_TRADE" />
      <value enum="I" description="SOLD_LAST" />
      <value enum="J" description="NEXT_DAY_TRADE" />
      <value enum="K" description="OPENED" />
      <value enum="L" description="SELLER" />
      <value enum="M" description="SOLD" />
      <value enum="N" description="STOPPED_STOCK" />
    </field>
    <field number="278" name="MDEntryID" type="STRING" />
    <field number="279" name="MDUpdateAction" type="CHAR">
      <value enum="0" description="NEW" />
      <value enum="1" description="CHANGE" />
      <value enum="2" description="DELETE" />
    </field>
    <field number="280" name="MDEntryRefID" type="STRING" />
    <field number="281" name="MDReqRejReason" type="CHAR">
      <value enum="0" description="UNKNOWN_SYMBOL" />
      <value enum="1" description="DUPLICATE_MDREQID" />
      <value enum="2" description="INSUFFICIENT_BANDWIDTH" />
      <value enum="3" description="INSUFFICIENT_PERMISSIONS" />
      <value enum="4" description="UNSUPPORTED_SUBSCRIPTIONREQUESTTYPE" />
      <value enum="5" description="UNSUPPORTED_MARKETDEPTH" />
      <value enum="6" description="UNSUPPORTED_MDUPDATETYPE" />
      <value enum="7" description="UNSUPPORTED_AGGREGATEDBOOK" />
      <value enum="8" description="UNSUPPORTED_MDENTRYTYPE" />
    </field>
    <field number="282" name="MDEntryOriginator" type="STRING" />
    <field number="283" name="LocationID" type="STRING" />
    <field number="284" name="DeskID" type="STRING" />
    <field number="285" name="DeleteReason" type="CHAR">
      <value enum="0" description="CANCELATION_TRADE_BUST" />
      <value enum="1" description="ERROR" />
    </field>
    <field number="286" name="OpenCloseSettleFlag" type="CHAR">
      <value enum="0" description="DAILY_OPEN_CLOSE__SETTLEMENT_PRICE" />
      <value enum="1" description="SESSION_OPEN_CLOSE__SETTLEMENT_PRICE" />
      <value enum="2" description="DELIVERY_SETTLEMENT_PRICE" />
    </field>
    <field number="287" name="SellerDays" type="INT" />
    <field number="288" name="MDEntryBuyer" type="STRING" />
    <field number="289" name="MDEntrySeller" type="STRING" />
    <field number="290" name="MDEntryPositionNo" type="INT" />
    <field number="291" name="FinancialStatus" type="CHAR">
      <value enum="1" description="BANKRUPT" />
    </field>
    <field number="292" name="CorporateAction" type="CHAR">
      <value enum="A" description="EXDIVIDEND" />
      <value enum="B" description="EXDISTRIBUTION" />
      <value enum="C" description="EXRIGHTS" />
      <value enum="D" description="NEW" />
      <value enum="E" description="EXINTEREST" />
    </field>
    <field number="293" name="DefBidSize" type="QTY" />
    <field number="294" name="DefOfferSize" type="QTY" />
    <field number="295" name="NoQuoteEntries" type="INT" />
    <field number="296" name="NoQuoteSets" type="INT" />
    <field number="297" name="QuoteAckStatus" type="INT" />
    <field number="298" name="QuoteCancelType" type="INT" />
    <field number="299" name="QuoteEntryID" type="STRING" />
    <field number="300" name="QuoteRejectReason" type="INT">
      <value enum="1" description="UNKNOWN_SYMBOL" />
      <value enum="2" description="EXCHANGE" />
      <value enum="3" description="QUOTE_REQUEST_EXCEEDS_LIMIT" />
      <value enum="4" description="TOO_LATE_TO_ENTER" />
      <value enum="5" description="UNKNOWN_QUOTE" />
      <value enum="6" description="DUPLICATE_QUOTE_7" />
      <value enum="8" description="INVALID_PRICE" />
      <value enum="9" description="NOT_AUTHORIZED_TO_QUOTE_SECURITY" />
    </field>
    <field number="301" name="QuoteResponseLevel" type="INT" />
    <field number="302" name="QuoteSetID" type="STRING" />
    <field number="303" name="QuoteRequestType" type="INT" />
    <field number="304" name="TotQuoteEntries" type="INT" />
    <field number="305" name="UnderlyingIDSource" type="STRING" />
    <field number="306" name="UnderlyingIssuer" type="STRING" />
    <field number="307" name="UnderlyingSecurityDesc" type="STRING" />
    <field number="308" name="UnderlyingSecurityExchange" type="EXCHANGE" />
    <field number="309" name="UnderlyingSecurityID" type="STRING" />
    <field number="310" name="UnderlyingSecurityType" type="STRING" />
    <field number="311" name="UnderlyingSymbol" type="STRING" />
    <field number="312" name="UnderlyingSymbolSfx" type="STRING" />
    <field number="313" name="UnderlyingMaturityMonthYear" type="MONTHYEAR" />
    <field number="314" name="UnderlyingMaturityDay" type="DAYOFMONTH" />
    <field number="315" name="UnderlyingPutOrCall" type="INT" />
    <field number="316" name="UnderlyingStrikePrice" type="PRICE" />
    <field number="317" name="UnderlyingOptAttribute" type="CHAR" />
    <field number="318" name="UnderlyingCurrency" type="CURRENCY" />
    <field number="319" name="RatioQty" type="QTY" />
    <field number="320" name="SecurityReqID" type="STRING" />
    <field number="321" name="SecurityRequestType" type="INT">
      <value enum="0" description="REQUEST_SECURITY_IDENTITY_AND_SPECIFICATIONS" />
      <value enum="1" description="REQUEST_SECURITY_IDENTITY_FOR_THE_SPECIFICATIONS_PROVIDED" />
      <value enum="2" description="REQUEST_LIST_SECURITY_TYPES" />
      <value enum="3" description="REQUEST_LIST_SECURITIES" />
    </field>
    <field number="322" name="SecurityResponseID" type="STRING" />
    <field number="323" name="SecurityResponseType" type="INT">
      <value enum="1" description="ACCEPT_SECURITY_PROPOSAL_AS_IS" />
      <value enum="2" description="ACCEPT_SECURITY_PROPOSAL_WITH_REVISIONS_AS_INDICATED_IN_THE_MESSAGE" />
      <value enum="3" description="LIST_OF_SECURITY_TYPES_RETURNED_PER_REQUEST" />
      <value enum="4" description="LIST_OF_SECURITIES_RETURNED_PER_REQUEST" />
      <value enum="5" description="REJECT_SECURITY_PROPOSAL" />
      <value enum="6" description="CAN_NOT_MATCH_SELECTION_CRITERIA" />
    </field>
    <field number="324" name="SecurityStatusReqID" type="STRING" />
    <field number="325" name="UnsolicitedIndicator" type="BOOLEAN">
      <value enum="Y" description="MESSAGE_IS_BEING_SENT_UNSOLICITED" />
      <value enum="N" description="MESSAGE_IS_BEING_SENT_AS_A_RESULT_OF_A_PRIOR_REQUEST" />
    </field>
    <field number="326" name="SecurityTradingStatus" type="INT">
      <value enum="1" description="OPENING_DELAY" />
      <value enum="2" description="TRADING_HALT" />
      <value enum="3" description="RESUME" />
      <value enum="4" description="NO_OPENNO_RESUME" />
      <value enum="5" description="PRICE_INDICATION" />
      <value enum="6" description="TRADING_RANGE_INDICATION" />
      <value enum="7" description="MARKET_IMBALANCE_BUY" />
      <value enum="8" description="MARKET_IMBALANCE_SELL" />
      <value enum="9" description="MARKET_ON_CLOSE_IMBALANCE_BUY" />
      <value enum="10" description="MARKET_ON_CLOSE_IMBALANCE_SELL" />
      <value enum="11" description="NOT_ASSIGNED" />
      <value enum="12" description="NO_MARKET_IMBALANCE" />
      <value enum="13" description="NO_MARKET_ON_CLOSE_IMBALANCE" />
      <value enum="14" description="ITS_PREOPENING" />
      <value enum="15" description="NEW_PRICE_INDICATION" />
      <value enum="16" description="TRADE_DISSEMINATION_TIME" />
      <value enum="17" description="READY_TO_TRADE" />
      <value enum="18" description="NOT_AVAILABLE_FOR_TRADING" />
      <value enum="19" description="NOT_TRADED_ON_THIS_MARKET" />
      <value enum="20" description="UNKNOWN_OR_INVALID" />
    </field>
    <field number="327" name="HaltReason" type="CHAR">
      <value enum="I" description="ORDER_IMBALANCE" />
      <value enum="X" description="EQUIPMENT_CHANGEOVER" />
      <value enum="P" description="NEWS_PENDING" />
      <value enum="D" description="NEWS_DISSEMINATION" />
      <value enum="E" description="ORDER_INFLUX" />
      <value enum="M" description="ADDITIONAL_INFORMATION" />
    </field>
    <field number="328" name="InViewOfCommon" type="BOOLEAN">
      <value enum="Y" description="HALT_WAS_DUE_TO_COMMON_STOCK_BEING_HALTED" />
      <value enum="N" description="HALT_WAS_NOT_RELATED_TO_A_HALT_OF_THE_COMMON_STOCK" />
    </field>
    <field number="329" name="DueToRelated" type="BOOLEAN">
      <value enum="Y" description="HALT_WAS_DUE_TO_RELATED_SECURITY_BEING_HALTED" />
      <value enum="N" description="HALT_WAS_NOT_RELATED_TO_A_HALT_OF_THE_RELATED_SECURITY" />
    </field>
    <field number="330" name="BuyVolume" type="QTY" />
    <field number="331" name="SellVolume" type="QTY" />
    <field number="332" name="HighPx" type="PRICE" />
    <field number="333" name="LowPx" type="PRICE" />
    <field number="334" name="Adjustment" type="INT">
      <value enum="1" description="CANCEL" />
      <value enum="2" description="ERROR" />
      <value enum="3" description="CORRECTION" />
    </field>
    <field number="335" name="TradSesReqID" type="STRING" />
    <field number="336" name="TradingSessionID" type="STRING" />
    <field number="337" name="ContraTrader" type="STRING" />
    <field number="338" name="TradSesMethod" type="INT">
      <value enum="1" description="ELECTRONIC" />
      <value enum="2" description="OPEN_OUTCRY" />
      <value enum="3" description="TWO_PARTY" />
    </field>
    <field number="339" name="TradSesMode" type="INT">
      <value enum="1" description="TESTING" />
      <value enum="2" description="SIMULATED" />
      <value enum="3" description="PRODUCTION" />
    </field>
    <field number="340" name="TradSesStatus" type="INT">
      <value enum="1" description="HALTED" />
      <value enum="2" description="OPEN" />
      <value enum="3" description="CLOSED" />
      <value enum="4" description="PREOPEN" />
      <value enum="5" description="PRECLOSE" />
    </field>
    <field number="341" name="TradSesStartTime" type="UTCTIMESTAMP" />
    <field number="342" name="TradSesOpenTime" type="UTCTIMESTAMP" />
    <field number="343" name="TradSesPreCloseTime" type="UTCTIMESTAMP" />
    <field number="344" name="TradSesCloseTime" type="UTCTIMESTAMP" />
    <field number="345" name="TradSesEndTime" type="UTCTIMESTAMP" />
    <field number="346" name="NumberOfOrders" type="INT" />
    <field number="347" name="MessageEncoding" type="STRING" />
    <field number="348" name="EncodedIssuerLen" type="INT" />
    <field number="349" name="EncodedIssuer" type="DATA" />
    <field number="350" name="EncodedSecurityDescLen" type="INT" />
    <field number="351" name="EncodedSecurityDesc" type="DATA" />
    <field number="352" name="EncodedListExecInstLen" type="INT" />
    <field number="353" name="EncodedListExecInst" type="DATA" />
    <field number="354" name="EncodedTextLen" type="INT" />
    <field number="355" name="EncodedText" type="DATA" />
    <field number="356" name="EncodedSubjectLen" type="INT" />
    <field number="357" name="EncodedSubject" type="DATA" />
    <field number="358" name="EncodedHeadlineLen" type="INT" />
    <field number="359" name="EncodedHeadline" type="DATA" />
    <field number="360" name="EncodedAllocTextLen" type="INT" />
    <field number="361" name="EncodedAllocText" type="DATA" />
    <field number="362" name="EncodedUnderlyingIssuerLen" type="INT" />
    <field number="363" name="EncodedUnderlyingIssuer" type="DATA" />
    <field number="364" name="EncodedUnderlyingSecurityDescLen" type="INT" />
    <field number="365" name="EncodedUnderlyingSecurityDesc" type="DATA" />
    <field number="366" name="AllocPrice" type="PRICE" />
    <field number="367" name="QuoteSetValidUntilTime" type="UTCTIMESTAMP" />
    <field number="368" name="QuoteEntryRejectReason" type="INT">
      <value enum="1" description="UNKNOWN_SYMBOL" />
      <value enum="2" description="EXCHANGE" />
      <value enum="3" description="QUOTE_EXCEEDS_LIMIT" />
      <value enum="4" description="TOO_LATE_TO_ENTER" />
      <value enum="5" description="UNKNOWN_QUOTE" />
      <value enum="6" description="DUPLICATE_QUOTE" />
      <value enum="7" description="INVALID_BIDASK_SPREAD" />
      <value enum="8" description="INVALID_PRICE" />
      <value enum="9" description="NOT_AUTHORIZED_TO_QUOTE_SECURITY" />
    </field>
    <field number="369" name="LastMsgSeqNumProcessed" type="INT" />
    <field number="370" name="OnBehalfOfSendingTime" type="UTCTIMESTAMP" />
    <field number="371" name="RefTagID" type="INT" />
    <field number="372" name="RefMsgType" type="STRING" />
    <field number="373" name="SessionRejectReason" type="INT">
      <value enum="0" description="INVALID_TAG_NUMBER" />
      <value enum="1" description="REQUIRED_TAG_MISSING" />
      <value enum="2" description="TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE" />
      <value enum="3" description="UNDEFINED_TAG" />
      <value enum="4" description="TAG_SPECIFIED_WITHOUT_A_VALUE" />
      <value enum="5" description="VALUE_IS_INCORRECT" />
      <value enum="6" description="INCORRECT_DATA_FORMAT_FOR_VALUE" />
      <value enum="7" description="DECRYPTION_PROBLEM" />
      <value enum="8" description="SIGNATURE_PROBLEM" />
      <value enum="9" description="COMPID_PROBLEM" />
      <value enum="10" description="SENDINGTIME_ACCURACY_PROBLEM" />
      <value enum="11" description="E" />
    </field>
    <field number="374" name="BidRequestTransType" type="CHAR">
      <value enum="N" description="NEW" />
      <value enum="C" description="CANCEL" />
    </field>
    <field number="375" name="ContraBroker" type="STRING" />
    <field number="376" name="ComplianceID" type="STRING" />
    <field number="377" name="SolicitedFlag" type="BOOLEAN">
      <value enum="Y" description="WAS_SOLCITIED" />
      <value enum="N" description="WAS_NOT_SOLICITED" />
    </field>
    <field number="378" name="ExecRestatementReason" type="INT">
      <value enum="0" description="GT_CORPORATE_ACTION" />
      <value enum="1" description="GT_RENEWAL_RESTATEMENT" />
      <value enum="2" description="VERBAL_CHANGE" />
      <value enum="3" description="REPRICING_OF_ORDER" />
      <value enum="4" description="BROKER_OPTION" />
      <value enum="5" description="PARTIAL_DECLINE_OF_ORDERQTY" />
    </field>
    <field number="379" name="BusinessRejectRefID" type="STRING" />
    <field number="380" name="BusinessRejectReason" type="INT">
      <value enum="0" description="OTHER" />
      <value enum="1" description="UNKOWN_ID" />
      <value enum="2" description="UNKNOWN_SECURITY" />
      <value enum="3" description="UNSUPPORTED_MESSAGE_TYPE" />
      <value enum="4" description="APPLICATION_NOT_AVAILABLE" />
      <value enum="5" description="CONDITIONALLY_REQUIRED_FIELD_MISSING" />
    </field>
    <field number="381" name="GrossTradeAmt" type="AMT" />
    <field number="382" name="NoContraBrokers" type="INT" />
    <field number="383" name="MaxMessageSize" type="INT" />
    <field number="384" name="NoMsgTypes" type="INT" />
    <field number="385" name="MsgDirection" type="CHAR">
      <value enum="S" description="SEND" />
      <value enum="R" description="RECEIVE" />
    </field>
    <field number="386" name="NoTradingSessions" type="INT" />
    <field number="387" name="TotalVolumeTraded" type="QTY" />
    <field number="388" name="DiscretionInst" type="CHAR">
      <value enum="0" description="RELATED_TO_DISPLAYED_PRICE" />
      <value enum="1" description="RELATED_TO_MARKET_PRICE" />
      <value enum="2" description="RELATED_TO_PRIMARY_PRICE" />
      <value enum="3" description="RELATED_TO_LOCAL_PRIMARY_PRICE" />
      <value enum="4" description="RELATED_TO_MIDPOINT_PRICE" />
      <value enum="5" description="RELATED_TO_LAST_TRADE_PRICE" />
    </field>
    <field number="389" name="DiscretionOffset" type="PRICEOFFSET" />
    <field number="390" name="BidID" type="STRING" />
    <field number="391" name="ClientBidID" type="STRING" />
    <field number="392" name="ListName" type="STRING" />
    <field number="393" name="TotalNumSecurities" type="INT" />
    <field number="394" name="BidType" type="INT" />
    <field number="395" name="NumTickets" type="INT" />
    <field number="396" name="SideValue1" type="AMT" />
    <field number="397" name="SideValue2" type="AMT" />
    <field number="398" name="NoBidDescriptors" type="INT" />
    <field number="399" name="BidDescriptorType" type="INT" />
    <field number="400" name="BidDescriptor" type="STRING" />
    <field number="401" name="SideValueInd" type="INT" />
    <field number="402" name="LiquidityPctLow" type="FLOAT" />
    <field number="403" name="LiquidityPctHigh" type="FLOAT" />
    <field number="404" name="LiquidityValue" type="AMT" />
    <field number="405" name="EFPTrackingError" type="FLOAT" />
    <field number="406" name="FairValue" type="AMT" />
    <field number="407" name="OutsideIndexPct" type="FLOAT" />
    <field number="408" name="ValueOfFutures" type="AMT" />
    <field number="409" name="LiquidityIndType" type="INT" />
    <field number="410" name="WtAverageLiquidity" type="FLOAT" />
    <field number="411" name="ExchangeForPhysical" type="BOOLEAN">
      <value enum="Y" description="TRUE" />
      <value enum="N" description="FALSE" />
    </field>
    <field number="412" name="OutMainCntryUIndex" type="AMT" />
    <field number="413" name="CrossPercent" type="FLOAT" />
    <field number="414" name="ProgRptReqs" type="INT" />
    <field number="415" name="ProgPeriodInterval" type="INT" />
    <field number="416" name="IncTaxInd" type="INT" />
    <field number="417" name="NumBidders" type="INT" />
    <field number="418" name="TradeType" type="CHAR" />
    <field number="419" name="BasisPxType" type="CHAR" />
    <field number="420" name="NoBidComponents" type="INT" />
    <field number="421" name="Country" type="STRING" />
    <field number="422" name="TotNoStrikes" type="INT" />
    <field number="423" name="PriceType" type="INT" />
    <field number="424" name="DayOrderQty" type="QTY" />
    <field number="425" name="DayCumQty" type="QTY" />
    <field number="426" name="DayAvgPx" type="PRICE" />
    <field number="427" name="GTBookingInst" type="INT">
      <value enum="0" description="BOOK_OUT_ALL_TRADES_ON_DAY_OF_EXECUTION" />
      <value enum="1" description="ACCUMULATE_EXECUTIONS_UNTIL_ORDER_IS_FILLED_OR_EXPIRES" />
      <value enum="2" description="ACCUMULATE_UNTIL_VERBALLY_NOTIFIED_OTHERWISE" />
    </field>
    <field number="428" name="NoStrikes" type="INT" />
    <field number="429" name="ListStatusType" type="INT" />
    <field number="430" name="NetGrossInd" type="INT" />
    <field number="431" name="ListOrderStatus" type="INT" />
    <field number="432" name="ExpireDate" type="LOCALMKTDATE" />
    <field number="433" name="ListExecInstType" type="CHAR" />
    <field number="434" name="CxlRejResponseTo" type="CHAR" />
    <field number="435" name="UnderlyingCouponRate" type="FLOAT" />
    <field number="436" name="UnderlyingContractMultiplier" type="FLOAT" />
    <field number="437" name="ContraTradeQty" type="QTY" />
    <field number="438" name="ContraTradeTime" type="UTCTIMESTAMP" />
    <field number="439" name="ClearingFirm" type="STRING" />
    <field number="440" name="ClearingAccount" type="STRING" />
    <field number="441" name="LiquidityNumSecurities" type="INT" />
    <field number="442" name="MultiLegReportingType" type="CHAR" />
    <field number="443" name="StrikeTime" type="UTCTIMESTAMP" />
    <field number="444" name="ListStatusText" type="STRING" />
    <field number="445" name="EncodedListStatusTextLen" type="INT" />
    <field number="446" name="EncodedListStatusText" type="DATA" />
	<field number="447" name="PartyIDSource" type="CHAR">
      <value enum="B" description="BIC" />
      <value enum="C" description="GENERALLY_ACCEPTED_MARKET_PARTICIPANT_IDENTIFIER" />
      <value enum="D" description="PROPRIETARY" />
      <value enum="E" description="ISO_COUNTRY_CODE" />
      <value enum="F" description="SETTLEMENT_ENTITY_LOCATION" />
      <value enum="1" description="KOREAN_INVESTOR_ID" />
      <value enum="2" description="TAIWANESE_QUALIFIED_FOREIGN_INVESTOR_ID_QFII" />
      <value enum="3" description="TAIWANESE_TRADING_ACCOUNT" />
      <value enum="4" description="MALAYSIAN_CENTRAL_DEPOSITORY" />
      <value enum="5" description="CHINESE_B_SHARE" />
      <value enum="6" description="UK_NATIONAL_INSURANCE_OR_PENSION_NUMBER" />
      <value enum="7" description="US_SOCIAL_SECURITY_NUMBER" />
      <value enum="8" description="US_EMPLOYER_IDENTIFICATION_NUMBER" />
      <value enum="9" description="AUSTRALIAN_BUSINESS_NUMBER" />
      <value enum="A" description="AUSTRALIAN_TAX_FILE_NUMBER" />
    </field>
    <field number="448" name="PartyID" type="STRING" />
	<field number="452" name="PartyRole" type="INT">
      <value enum="1" description="EXECUTING_FIRM" />
      <value enum="2" description="BROKER_OF_CREDIT" />
      <value enum="3" description="CLIENT_ID" />
      <value enum="4" description="CLEARING_FIRM" />
      <value enum="5" description="INVESTOR_ID" />
      <value enum="6" description="INTRODUCING_FIRM" />
      <value enum="7" description="ENTERING_FIRM" />
      <value enum="8" description="LOCATE" />
      <value enum="9" description="FUND_MANAGER_CLIENT_ID" />
      <value enum="10" description="SETTLEMENT_LOCATION" />
      <value enum="11" description="ORDER_ORIGINATION_TRADER" />
      <value enum="12" description="EXECUTING_TRADER" />
      <value enum="13" description="ORDER_ORIGINATION_FIRM" />
      <value enum="14" description="GIVEUP_CLEARING_FIRM" />
      <value enum="15" description="CORRESPONDANT_CLEARING_FIRM" />
      <value enum="16" description="EXECUTING_SYSTEM" />
      <value enum="17" description="CONTRA_FIRM" />
      <value enum="18" description="CONTRA_CLEARING_FIRM" />
      <value enum="19" description="SPONSORING_FIRM" />
      <value enum="20" description="UNDERLYING_CONTRA_FIRM" />
    </field>
    <field number="453" name="NoPartyIDs" type="NUMINGROUP" />
    <field number="460" name="Product" type="INT">
      <value enum="1" description="AGENCY" />
      <value enum="2" description="COMMODITY" />
      <value enum="3" description="CORPORATE" />
      <value enum="4" description="CURRENCY" />
      <value enum="5" description="EQUITY" />
      <value enum="6" description="GOVERNMENT" />
      <value enum="7" description="INDEX" />
      <value enum="8" description="LOAN" />
      <value enum="9" description="MONEYMARKET" />
      <value enum="10" description="MORTGAGE" />
      <value enum="11" description="MUNICIPAL" />
      <value enum="12" description="OTHER" />
      <value enum="13" description="FINANCING" />
    </field>
    <field number="559" name="SecurityListRequestType" type="INT">
      <value enum="0" description="SYMBOL" />
      <value enum="1" description="SECURITYTYPE_AND_OR_CFICODE" />
      <value enum="2" description="PRODUCT" />
      <value enum="3" description="TRADINGSESSIONID" />
      <value enum="4" description="ALL_SECURITIES" />
    </field>
    <field number="560" name="SecurityRequestResult" type="INT">
      <value enum="0" description="VALID_REQUEST" />
      <value enum="1" description="INVALID_OR_UNSUPPORTED_REQUEST" />
      <value enum="2" description="NO_INSTRUMENTS_FOUND_THAT_MATCH_SELECTION_CRITERIA" />
      <value enum="3" description="NOT_AUTHORIZED_TO_RETRIEVE_INSTRUMENT_DATA" />
      <value enum="4" description="INSTRUMENT_DATA_TEMPORARILY_UNAVAILABLE" />
      <value enum="5" description="REQUEST_FOR_INSTRUMENT_DATA_NOT_SUPPORTED" />
    </field>
    <field number="695" name="QuoteQualifier" type="INT" />
    <field number="735" name="NoQuoteQualifiers" type="NUMINGROUP" />
    <field number="870" name="NoInstrAttrib" type="NUMINGROUP" />
    <field number="871" name="InstrAttribType" type="INT">
      <value enum="1" description="FLAT" />
      <value enum="2" description="ZERO_COUPON" />
      <value enum="3" description="INTEREST_BEARING" />
      <value enum="4" description="NO_PERIODIC_PAYMENTS" />
      <value enum="5" description="VARIABLE_RATE" />
      <value enum="6" description="LESS_FEE_FOR_PUT" />
      <value enum="7" description="STEPPED_COUPON" />
      <value enum="8" description="COUPON_PERIOD" />
      <value enum="9" description="WHEN_AND_IF_ISSUED" />
      <value enum="99" description="UNKNOWN" />
    </field>
    <field number="872" name="InstrAttribValue" type="STRING" />
    <field number="1079" name="MaturityTime" type="STRING" />
    <field number="9200" name="LiquidityIndicator" type="CHAR" />
    <field number="9300" name="ContraID" type="INT" />
    <field number="9500" name="CommissionFXCM" type="QTY" />
    <field number="9600" name="LastMktPx" type="PRICE" />
    <field number="9700" name="MatchTime" type="UTCTIMESTAMP" />
  </fields>
</fix>'
WHERE
	[Name] = 'FIX42_FCM.xml'
