

ALTER Database Integrator Add FileGroup Integrator_DailyData
GO

ALTER Database Integrator Add File
(
Name=Integrator_DailyData_File01,
FileName='C:\SQLDATA\USERDB\Integrator\Integrator_DailyData_File01.ndf',
Size=5GB,
FileGrowth=10%
) To Filegroup Integrator_DailyData
GO


ALTER TABLE [dbo].[OrderExternal] ADD [DatePartIntegratorSentExternalOrderUtc]  AS (CONVERT([date],[IntegratorSentExternalOrderUtc])) PERSISTED
GO


CREATE PROCEDURE CreateDailyOrdersView_SP 
AS
BEGIN

	DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)
	DECLARE @YesterdayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), DATEADD(DAY, -1, GETUTCDATE()), 101)

	DECLARE @Query NVARCHAR(MAX) = N'
	CREATE VIEW [OrderExternalDailyView_' + @TodayLiteral + ']
	WITH SCHEMABINDING 
	AS 
	SELECT 
		   [IntegratorSentExternalOrderUtc]
		  ,[DatePartIntegratorSentExternalOrderUtc]
		  ,[IntegratorExternalOrderPrivateId]
		  ,[CounterpartyId]
		  ,[QuoteReceivedToOrderSentInternalLatency]
	FROM [dbo].[OrderExternal]
	WHERE
		DatePartIntegratorSentExternalOrderUtc = CONVERT(DATE,''' + @TodayLiteral + ''', 101)'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE UNIQUE CLUSTERED INDEX [IX_OrderId] ON [dbo].[OrderExternalDailyView_' + @TodayLiteral + ']
	(
		[IntegratorExternalOrderPrivateId] ASC
	)ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

--	SET @Query = N'
--	CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[OrderExternalDailyView_' + @TodayLiteral + ']
--	(
--		[DatePartIntegratorSentExternalOrderUtc] ASC,
--		[CounterpartyId] ASC
--	)
--	INCLUDE ([QuoteReceivedToOrderSentInternalLatency]) 
--	ON [Integrator_DailyData]'


--	EXECUTE sp_executesql @Query

	IF EXISTS(SELECT * FROM  sys.synonyms s WHERE s.name = 'OrderExternal_DailyView')
	BEGIN
		DROP SYNONYM OrderExternal_DailyView
	END

	SET @Query = N'
	CREATE SYNONYM OrderExternal_DailyView FOR [OrderExternalDailyView_' + @TodayLiteral + ']'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	IF EXISTS(select * FROM sys.views where name = ''OrderExternalDailyView_' + @YesterdayLiteral + ''')
	BEGIN
		DROP VIEW [OrderExternalDailyView_' + @YesterdayLiteral + ']
	END
	'

	EXECUTE sp_executesql @Query

END
GO


CREATE PROCEDURE CreateDailyRejectionsView_SP 
AS
BEGIN

	DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)
	DECLARE @YesterdayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), DATEADD(DAY, -1, GETUTCDATE()), 101)

	DECLARE @Query NVARCHAR(MAX) = N'
	CREATE VIEW [DealExternalRejectedDailyView_' + @TodayLiteral + ']
	WITH SCHEMABINDING 
	AS 
	SELECT 
		   reject.[IntegratorReceivedExecutionReportUtc]
		   ,reject.[IntegratorExternalOrderPrivateId]
		  ,reject.[CounterpartyId]
		  ,reject.[DatePartIntegratorReceivedExecutionReportUtc]
		  ,CAST(
			(
				(datepart(HOUR, ordr.QuoteReceivedToOrderSentInternalLatency)*60 + datepart(MINUTE, ordr.QuoteReceivedToOrderSentInternalLatency))*60 
				+ datepart(SECOND, ordr.QuoteReceivedToOrderSentInternalLatency)
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, ordr.QuoteReceivedToOrderSentInternalLatency) 
		   AS IntegratorLatencyNanoseconds
	FROM 
		[dbo].[DealExternalRejected] reject
		INNER JOIN [dbo].[OrderExternal] ordr ON reject.[IntegratorExternalOrderPrivateId] = ordr.[IntegratorExternalOrderPrivateId]
	WHERE
		DatePartIntegratorReceivedExecutionReportUtc = CONVERT(DATE,''' + @TodayLiteral + ''', 101)'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE UNIQUE CLUSTERED INDEX [IX_ReceivedUtc_OrderId] ON [dbo].[DealExternalRejectedDailyView_' + @TodayLiteral + ']
	(
		[IntegratorReceivedExecutionReportUtc] ASC,
		[IntegratorExternalOrderPrivateId] ASC
	)ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalRejectedDailyView_' + @TodayLiteral + ']
	(
		[DatePartIntegratorReceivedExecutionReportUtc] ASC,
		[CounterpartyId] ASC
	)
	INCLUDE
	(
		[IntegratorLatencyNanoseconds]
	)
	ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	IF EXISTS(SELECT * FROM  sys.synonyms s WHERE s.name = 'DealExternalRejected_DailyView')
	BEGIN
		DROP SYNONYM DealExternalRejected_DailyView
	END

	SET @Query = N'
	CREATE SYNONYM DealExternalRejected_DailyView FOR [DealExternalRejectedDailyView_' + @TodayLiteral + ']'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	IF EXISTS(select * FROM sys.views where name = ''DealExternalRejectedDailyView_' + @YesterdayLiteral + ''')
	BEGIN
		DROP VIEW [DealExternalRejectedDailyView_' + @YesterdayLiteral + ']
	END
	'

	EXECUTE sp_executesql @Query

END
GO


CREATE PROCEDURE CreateDailyDealsView_SP 
AS
BEGIN

	DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)
	DECLARE @YesterdayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), DATEADD(DAY, -1, GETUTCDATE()), 101)

	DECLARE @Query NVARCHAR(MAX) = N'
	CREATE VIEW [DealExternalExecutedDailyView_' + @TodayLiteral + ']
	WITH SCHEMABINDING 
	AS 
	SELECT 
		deal.[IntegratorReceivedExecutionReportUtc]
		,deal.[CounterpartyId]
		,deal.[SymbolId]
		,deal.[IntegratorExternalOrderPrivateId]
		,deal.[AmountBasePolExecuted]
		,deal.[Price]
		,CAST(
			(
				(datepart(HOUR, ordr.QuoteReceivedToOrderSentInternalLatency)*60 + datepart(MINUTE, ordr.QuoteReceivedToOrderSentInternalLatency))*60 
				+ datepart(SECOND, ordr.QuoteReceivedToOrderSentInternalLatency)
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, ordr.QuoteReceivedToOrderSentInternalLatency) 
		   AS IntegratorLatencyNanoseconds
		,CAST(
			(
				(datepart(HOUR, deal.[OrderSentToExecutionReportReceivedLatency])*60 + datepart(MINUTE, deal.[OrderSentToExecutionReportReceivedLatency]))*60 
				+ datepart(SECOND, deal.[OrderSentToExecutionReportReceivedLatency])
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, deal.[OrderSentToExecutionReportReceivedLatency]) 
		   AS CounterpartyLatencyNanoseconds
		,deal.[AmountTermPolExecutedInUsd]
		,deal.[DatePartIntegratorReceivedExecutionReportUtc]
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN [dbo].[OrderExternal] ordr ON deal.[IntegratorExternalOrderPrivateId] = ordr.[IntegratorExternalOrderPrivateId]
	WHERE
		DatePartIntegratorReceivedExecutionReportUtc = CONVERT(DATE,''' + @TodayLiteral + ''', 101)'
		
		
	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE UNIQUE CLUSTERED INDEX [IX_ReceivedUtc_OrderId] ON [dbo].[DealExternalExecutedDailyView_' + @TodayLiteral + ']
	(
		[IntegratorReceivedExecutionReportUtc] ASC,
		[IntegratorExternalOrderPrivateId] ASC
	)ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalExecutedDailyView_' + @TodayLiteral + ']
	(
		[DatePartIntegratorReceivedExecutionReportUtc] ASC,
		[CounterpartyId] ASC,
		[SymbolId] ASC
	)
	INCLUDE 
	(
		[AmountBasePolExecuted],
		[Price],
		[AmountTermPolExecutedInUsd],
		[IntegratorLatencyNanoseconds],
		[CounterpartyLatencyNanoseconds])
	ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	IF EXISTS(SELECT * FROM  sys.synonyms s WHERE s.name = 'DealExternalExecuted_DailyView')
	BEGIN
		DROP SYNONYM DealExternalExecuted_DailyView
	END

	SET @Query = N'
	CREATE SYNONYM DealExternalExecuted_DailyView FOR [DealExternalExecutedDailyView_' + @TodayLiteral + ']'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	IF EXISTS(select * FROM sys.views where name = ''DealExternalExecutedDailyView_' + @YesterdayLiteral + ''')
	BEGIN
		DROP VIEW [DealExternalExecutedDailyView_' + @YesterdayLiteral + ']
	END
	'

	EXECUTE sp_executesql @Query

END
GO


USE msdb ;
GO
EXEC dbo.sp_add_job
    @job_name = N'Daily Creation of Current Day Views' ;
GO
EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'Add OrderExternal Daily View',
    @subsystem = N'TSQL',
    @command = N'exec CreateDailyOrdersView_SP',
	@database_name = N'Integrator',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO
EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'Add DealExternalRejected Daily View',
    @subsystem = N'TSQL',
    @command = N'exec CreateDailyRejectionsView_SP',
	@database_name = N'Integrator',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO
EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'Add DealExternalExecuted Daily View',
    @subsystem = N'TSQL',
    @command = N'exec CreateDailyDealsView_SP',
	@database_name = N'Integrator',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO
EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'Check Steps Status',
    @subsystem = N'TSQL',
    @command = N'
	SELECT  step_name, message
	FROM    msdb.dbo.sysjobhistory
	WHERE   instance_id > COALESCE((SELECT MAX(instance_id) FROM msdb.dbo.sysjobhistory
									WHERE job_id = $(ESCAPE_SQUOTE(JOBID)) AND step_id = 0), 0)
			AND job_id = $(ESCAPE_SQUOTE(JOBID))
			AND run_status <> 1 -- success

	IF      @@ROWCOUNT <> 0
			RAISERROR(''One of steps failed'', 16, 1)
	',
    @retry_attempts = 0,
    @retry_interval = 0;
GO

EXEC dbo.sp_add_schedule
    @schedule_name = N'RunDailyAfterUtcMidnight',
	--Daily
    @freq_type = 4,
	--Every one day
	@freq_interval = 1,
	--HHMMSS
    @active_start_time = 000030 ;
GO
USE msdb ;
GO
EXEC sp_attach_schedule
   @job_name = N'Daily Creation of Current Day Views',
   @schedule_name = N'RunDailyAfterUtcMidnight';
GO
EXEC dbo.sp_add_jobserver
    @job_name = N'Daily Creation of Current Day Views';
GO



-- SPs using the materialized views on SSD
--  What is added compared to previous SPs - selection from alias (instead of table) with the WITH(NOEXPAND) hint

CREATE PROCEDURE [dbo].[GetStatisticsPerCounterparty_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		RejectionsNum int,
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int
	)

	INSERT INTO @temp_rejections(CounterpartyId, RejectionsNum, IntegratorLatencyNanoseconds, IntegratorLatencyCases)
	SELECT
		[CounterpartyId],
		COUNT([CounterpartyId]) AS NumberOfRejections,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
	FROM
		DealExternalRejected_DailyView rej WITH(NOEXPAND)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		DealsNum int,
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int,
		CounterpartyLatencyNanoseconds bigint,
		CounterpartyLatencyCases int
	)

	INSERT INTO 
		@temp_NOPs_pol
		(
		SymbolId, 
		CounterpartyId, 
		NopBasePol, 
		NopTermPol, 
		VolumeBaseAbs, 
		DealsNum, 
		IntegratorLatencyNanoseconds, 
		IntegratorLatencyCases, 
		CounterpartyLatencyNanoseconds, 
		CounterpartyLatencyCases
		)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases,
		SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
		COUNT(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyCases
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
		--INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		deal.SymbolId,
		deal.CounterpartyId
		

	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		RejectionsStatistics.RejectionsNum AS RejectionsNum,
		RejectionsStatistics.RejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.RejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0)) AS RejectionsRate,
		--this calculates average of both numbers if both are non-null; or average of the nonull case if just one is not null (as 2*A / 2*B = A/B); or NULL if both are NULL (avoids divide by zero exception)
		(COALESCE(RejectionsStatistics.IntegratorLatencyNanoseconds, DealsStatistics.IntegratorLatencyNanoseconds) + COALESCE(DealsStatistics.IntegratorLatencyNanoseconds, RejectionsStatistics.IntegratorLatencyNanoseconds))
		/
		(COALESCE(RejectionsStatistics.IntegratorLatencyCases, DealsStatistics.IntegratorLatencyCases) + COALESCE(DealsStatistics.IntegratorLatencyCases, RejectionsStatistics.IntegratorLatencyCases))
		AS AvgIntegratorLatencyNanoseconds,
		DealsStatistics.CounterpartyLatencyNanoseconds / DealsStatistics.CounterpartyLatencyCases AS AvgCounterpartyLatencyNanoseconds
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			--SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(DealsNum) AS DealsNum,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
			SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
			SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(DealsNum) AS DealsNum,
				SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
				SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
				SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
				SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.DealsNum AS DealsNum,
					basePos.IntegratorLatencyNanoseconds AS IntegratorLatencyNanoseconds,
					basePos.IntegratorLatencyCases AS IntegratorLatencyCases,
					basePos.CounterpartyLatencyNanoseconds AS CounterpartyLatencyNanoseconds,
					basePos.CounterpartyLatencyCases AS CounterpartyLatencyCases
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd,
					NULL AS IntegratorLatencyNanoseconds,
					NULL AS IntegratorLatencyCases,
					NULL AS CounterpartyLatencyNanoseconds,
					NULL AS CounterpartyLatencyCases
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode
END
GO

GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_Daily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_Daily_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetStatisticsPerPair_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
		--following two make more sense after extracting multipliers behind brackets
		SUM(deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) AS PnlTermPol,
		SUM((deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) * termCurrency.LastKnownUsdConversionMultiplier) AS PnlTermPolInUsd,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1,
		SUM(ABS(deal.[AmountBasePolExecuted])* baseCurrency.LastKnownUsdConversionMultiplier) AS VolumeInUsd
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]
END
GO

GRANT EXECUTE ON [dbo].[GetStatisticsPerPair_Daily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetNopsPolSidedPerCurrency_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,0),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		deal.SymbolId


	SELECT
		CCYPositions.CCY AS CCY,
		SUM(CCYPositions.NopPol) AS NopPol,
		SUM(CCYPositions.NopPolInUsd) AS NopPolInUsd
	FROM
		(
		SELECT 
			ccy1.CurrencyAlphaCode AS CCY,
			basePos.NopBasePol AS NopPol,
			basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
		FROM
			Currency ccy1
			INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
			INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

		UNION ALL

		SELECT 
			ccy2.CurrencyAlphaCode AS CCY,
			termPos.NopTermPol AS NopPol,
			termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
		FROM
			Currency ccy2
			INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
			INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
		) AS CCYPositions
	GROUP BY
		CCYPositions.CCY
	ORDER BY
		CCYPositions.CCY
END
GO

GRANT EXECUTE ON [dbo].[GetNopsPolSidedPerCurrency_Daily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[GetNopAbsTotal_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.Price) AS NopTermPol
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		deal.SymbolId 


	SELECT
		SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
	FROM
		(
		SELECT
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
		GROUP BY
			CCYPositions.CCY
		) AS CtpCCYPositions

END
GO

GRANT EXECUTE ON [dbo].[GetNopAbsTotal_Daily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO



ALTER PROCEDURE [dbo].[GetNopsAbsPerCounterparty_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
	GROUP BY
		deal.SymbolId,
		deal.CounterpartyId
		


	SELECT
		CtpCCYPositions.Counterparty AS Counterparty,
		SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs
		--SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
	FROM
		(
		SELECT
			ctp.CounterpartyCode AS Counterparty,
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			Counterparty ctp
			LEFT JOIN
			(
			SELECT 
				basePos.CounterpartyId,
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT 
				termPos.CounterpartyId,
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions ON ctp.CounterpartyId = CCYPositions.CounterpartyId
		WHERE
			ctp.Active = 1
		GROUP BY
			ctp.CounterpartyCode
			,CCYPositions.CCY
		) AS CtpCCYPositions
	GROUP BY
		CtpCCYPositions.Counterparty
	ORDER BY
		CtpCCYPositions.Counterparty
END
GO

ALTER PROCEDURE [dbo].[GetNopsPolSidedPerPair_SP] (
	@Day DATE
	)
AS
BEGIN
	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
		--following two make more sense after extracting multipliers behind brackets
		SUM(deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) AS PnlTermPol,
		SUM((deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) * termCurrency.LastKnownUsdConversionMultiplier) AS PnlTermPolInUsd,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1,
		SUM(ABS(deal.[AmountBasePolExecuted])* deal.[Price] * termCurrency.LastKnownUsdConversionMultiplier) AS VolumeInUsd
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]
END
GO

CREATE TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily](
	[Counterparty] [varchar](3) NOT NULL,
	[NopAbs] decimal(18,2) NULL,
	[VolumeBaseAbsInUsd] decimal(18, 2) NULL,
	[VolumeShare] decimal(18, 4) NULL,
	[DealsNum] int NULL,
	[AvgDealSizeUsd] decimal(18,2) NULL,
	[RejectionsNum] int NULL,
	[RejectionsRate] decimal(18,4) NULL,
	[AvgIntegratorLatencyNanoseconds] bigint NULL,
	[AvgCounterpartyLatencyNanoseconds] bigint
) ON [Integrator_DailyData]
GO

CREATE PROCEDURE [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
AS
BEGIN

	--create the temp table before transaction to speed up things
	
	CREATE TABLE #TempCounterpartyStatistics(
		[Counterparty] [varchar](3) NOT NULL,
		[NopAbs] decimal(18,2) NULL,
		[VolumeBaseAbsInUsd] decimal(18, 2) NULL,
		[VolumeShare] decimal(18, 4) NULL,
		[DealsNum] int NULL,
		[AvgDealSizeUsd] decimal(18,2) NULL,
		[RejectionsNum] int NULL,
		[RejectionsRate] decimal(18,4) NULL,
		[AvgIntegratorLatencyNanoseconds] bigint NULL,
		[AvgCounterpartyLatencyNanoseconds] bigint
	)
	
	DECLARE @Today DATE = GETUTCDATE()
	
	INSERT INTO 
		#TempCounterpartyStatistics
		(
		[Counterparty],
		[NopAbs],
		[VolumeBaseAbsInUsd],
		[VolumeShare],
		[DealsNum],
		[AvgDealSizeUsd],
		[RejectionsNum],
		[RejectionsRate],
		[AvgIntegratorLatencyNanoseconds],
		[AvgCounterpartyLatencyNanoseconds]
		)
	exec [dbo].[GetStatisticsPerCounterparty_Daily_SP]  @Today

	BEGIN TRANSACTION
	   TRUNCATE TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily]

	   INSERT INTO 
			[dbo].[PersistedStatisticsPerCounterpartyDaily]
			(
			[Counterparty],
			[NopAbs],
			[VolumeBaseAbsInUsd],
			[VolumeShare],
			[DealsNum],
			[AvgDealSizeUsd],
			[RejectionsNum],
			[RejectionsRate],
			[AvgIntegratorLatencyNanoseconds],
			[AvgCounterpartyLatencyNanoseconds]
			)
		SELECT
			[Counterparty],
			[NopAbs],
			[VolumeBaseAbsInUsd],
			[VolumeShare],
			[DealsNum],
			[AvgDealSizeUsd],
			[RejectionsNum],
			[RejectionsRate],
			[AvgIntegratorLatencyNanoseconds],
			[AvgCounterpartyLatencyNanoseconds]
		FROM
			#TempCounterpartyStatistics

	COMMIT
END
GO

-- this permision is needed so that the trigger can be executed
GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_Daily_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT ALTER ON [dbo].[PersistedStatisticsPerCounterpartyDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[PersistedStatisticsPerCounterpartyDaily] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE TRIGGER DealExternalExecutedInserted ON [dbo].[DealExternalExecuted] AFTER INSERT
AS
BEGIN
	-- commit the internal insert transaction to prevent deadlocks
	COMMIT TRANSACTION
	exec [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION
END
GO	


CREATE TRIGGER DealExternalRejectedInserted ON [dbo].[DealExternalRejected] AFTER INSERT
AS
BEGIN
	-- commit the internal insert transaction to prevent deadlocks
	COMMIT TRANSACTION
	exec [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION
END
GO	



USE msdb ;
GO
EXEC dbo.sp_add_job
    @job_name = N'Ongoing Refresh of Persited Statistics' ;
GO
EXEC sp_add_jobstep
    @job_name = N'Ongoing Refresh of Persited Statistics',
    @step_name = N'Refresh Counterparty Statistics',
    @subsystem = N'TSQL',
    @command = N'exec [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]',
	@database_name = N'Integrator',
    @retry_attempts = 0,
    @retry_interval = 0;
GO

EXEC dbo.sp_add_schedule
    @schedule_name = N'RunEvery2Minutes',
	--Daily
    @freq_type = 4,
	--Every one day
	@freq_interval = 1,
	-- seconds
	@freq_subday_type = 2,
	-- every 120 seconds
	@freq_subday_interval= 120
	--no need for active start time and end time (and dates) => runs allways
GO
USE msdb ;
GO
EXEC sp_attach_schedule
   @job_name = N'Ongoing Refresh of Persited Statistics',
   @schedule_name = N'RunEvery2Minutes';
GO
EXEC dbo.sp_add_jobserver
    @job_name = N'Ongoing Refresh of Persited Statistics';
GO

CREATE PROCEDURE [dbo].[GetStatisticsPerCounterparty_DailyCached_SP]
AS
BEGIN

	SELECT 
		[Counterparty]
		,[NopAbs]
		,[VolumeBaseAbsInUsd]
		,[VolumeShare]
		,[DealsNum]
		,[AvgDealSizeUsd]
		,[RejectionsNum]
		,[RejectionsRate]
		,[AvgIntegratorLatencyNanoseconds]
		,[AvgCounterpartyLatencyNanoseconds]
	FROM 
		[dbo].[PersistedStatisticsPerCounterpartyDaily]
	ORDER BY
		[Counterparty]

END
GO

GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_DailyCached_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_DailyCached_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


ALTER PROCEDURE [dbo].[GetNopsPolPerCounterpartyAndCurrency_SP] (
	@Day Date
	)
AS
BEGIN

	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		deal.CounterpartyId,
		deal.SymbolId
		
		

		SELECT
			ctp.CounterpartyCode AS Counterparty,
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				basePos.CounterpartyId,
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT 
				termPos.CounterpartyId,
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
			INNER JOIN [dbo].[Counterparty] ctp ON ctp.CounterpartyId = CCYPositions.CounterpartyId
		GROUP BY
			ctp.CounterpartyCode
			,CCYPositions.CCY
		ORDER BY
			ctp.CounterpartyCode
			,CCYPositions.CCY

END
GO
