
CREATE PROCEDURE [dbo].[GetCommisionsList_SP] 
AS
BEGIN
	SELECT 
		counteprarty.CounterpartyCode AS Counterparty
		,[CommissionPricePerMillion]
	FROM 
		[dbo].[CommissionsPerCounterparty] commison
		INNER JOIN [dbo].[Counterparty] counteprarty ON commison.CounterpartyId = counteprarty.CounterpartyId
	WHERE
		counteprarty.Active = 1
END
GO

GRANT EXECUTE ON [dbo].[GetCommisionsList_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


DELETE 
	commison
FROM 
	[dbo].[CommissionsPerCounterparty] commison
	INNER JOIN [dbo].[Counterparty] counteprarty ON commison.CounterpartyId = counteprarty.CounterpartyId
WHERE
	counteprarty.Active = 0
	
	
CREATE TABLE [dbo].[VenueQuotingSettings](
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NULL,
	[TradingSystemId] [int] NOT NULL,
	[MaximumPositionBaseAbs] [decimal](18, 0) NOT NULL,
	[FairPriceImprovementSpreadBasisPoints] [decimal](18, 6) NOT NULL,
	[StopLossNetInUsd] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[LastResetRequestedUtc] [datetime2](7) NULL,
	[LastUpdatedUtc] [datetime2](7) NULL,
 CONSTRAINT [UNQ__VenueQuotingSettings__TradingSystemId] UNIQUE NONCLUSTERED 
(
	[TradingSystemId] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

--this is temp way how to prevent multiple systems on same symbol
ALTER TABLE [dbo].[VenueQuotingSettings] ADD  CONSTRAINT [UNQ__VenueQuotingSettings__SymbolId] UNIQUE NONCLUSTERED 
(
	[SymbolId] ASC
) ON [PRIMARY]
GO


ALTER TABLE [dbo].[VenueQuotingSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueQuotingSettings_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[VenueQuotingSettings] CHECK CONSTRAINT [FK_VenueQuotingSettings_Symbol]
GO

ALTER TABLE [dbo].[VenueQuotingSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueQuotingSettings_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO

ALTER TABLE [dbo].[VenueQuotingSettings] CHECK CONSTRAINT [FK_VenueQuotingSettings_Counterparty]
GO

CREATE CLUSTERED INDEX [IX_VenueQuotingSettingsSymbolPerCounterparty] ON [dbo].[VenueQuotingSettings]
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[VenueQuotingSettings_Changes](
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[MaximumPositionBaseAbs] [decimal](18, 0) NOT NULL,
	[FairPriceImprovementSpreadBasisPoints] [decimal](18, 6) NOT NULL,
	[StopLossNetInUsd] [bigint] NOT NULL,
	[ValidFromUtc] [datetime2](7) NOT NULL,
	[ValidToUtc] [datetime2](7) NOT NULL,
	[Deleted] [bit] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VenueQuotingSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueQuotingSettings_Changes_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO

ALTER TABLE [dbo].[VenueQuotingSettings_Changes] CHECK CONSTRAINT [FK_VenueQuotingSettings_Changes_Counterparty]
GO

ALTER TABLE [dbo].[VenueQuotingSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueQuotingSettings_Changes_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[VenueQuotingSettings_Changes] CHECK CONSTRAINT [FK_VenueQuotingSettings_Changes_Symbol]
GO


CREATE CLUSTERED INDEX [IX_VenueQuotingSettings_Changes] ON [dbo].[VenueQuotingSettings_Changes]
(
	[TradingSystemId] ASC,
	[ValidToUtc] ASC
) ON [PRIMARY]
GO


INSERT INTO [dbo].[TableChangeLog] ([TableName], [LastUpdatedUtc]) VALUES ('VenueQuotingSettings', '1/1/1900')
GO

CREATE TRIGGER [dbo].[LastDeletedTrigger_VenueQuotingSettings] ON [dbo].[VenueQuotingSettings]
AFTER DELETE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON
UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueQuotingSettings'

END
GO


CREATE TRIGGER [dbo].[LastUpdatedTrigger_VenueQuotingSettings] ON [dbo].[VenueQuotingSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[VenueQuotingSettings] Set [LastUpdatedUtc] = GETUTCDATE() from [dbo].[VenueQuotingSettings] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueQuotingSettings'

END
GO



CREATE TRIGGER [dbo].[VenueQuotingSettings_DeletedOrUpdated] ON [dbo].[VenueQuotingSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueQuotingSettings_Changes]
	   ([SymbolId]
	   ,[CounterpartyId]
	   ,[TradingSystemId]  
	   ,[MaximumPositionBaseAbs]
	   ,[FairPriceImprovementSpreadBasisPoints]
	   ,[StopLossNetInUsd]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[MaximumPositionBaseAbs]
		,deleted.[FairPriceImprovementSpreadBasisPoints]
		,deleted.[StopLossNetInUsd]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueQuotingSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueQuotingSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([MaximumPositionBaseAbs]) AND (topUpdatedChange.[MaximumPositionBaseAbs] IS NULL OR topUpdatedChange.[MaximumPositionBaseAbs] != deleted.[MaximumPositionBaseAbs]))
		OR (UPDATE([FairPriceImprovementSpreadBasisPoints]) AND (topUpdatedChange.[FairPriceImprovementSpreadBasisPoints] IS NULL OR topUpdatedChange.[FairPriceImprovementSpreadBasisPoints] != deleted.[FairPriceImprovementSpreadBasisPoints]))
		OR (UPDATE([StopLossNetInUsd]) AND (topUpdatedChange.[StopLossNetInUsd] IS NULL OR topUpdatedChange.[StopLossNetInUsd] != deleted.[StopLossNetInUsd]))	
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
END
GO

CREATE PROCEDURE [dbo].[GetUpdatedVenueQuotingSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueQuotingSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			[TradingSystemId]
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty			
			,[MaximumPositionBaseAbs]
			,[FairPriceImprovementSpreadBasisPoints]
			,[StopLossNetInUsd]		
			,[Enabled]
			,[LastResetRequestedUtc]
			,[LastUpdatedUtc]
		FROM 
			[dbo].[VenueQuotingSettings] sett
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name 
	END
END
GO

GRANT EXECUTE ON [dbo].[GetUpdatedVenueQuotingSystemSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE PROCEDURE [dbo].[VenueQuotingSettingsDeleteOne_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	DELETE FROM
		[dbo].[VenueQuotingSettings]
	WHERE
		[TradingSystemId] = @TradingSystemId
		AND [Enabled] = 0
		
	IF @@ROWCOUNT > 0
	BEGIN
		DELETE FROM 
			[dbo].[TradingSystem]
		WHERE 
			[TradingSystemId] = @TradingSystemId
	END
END
GO

GRANT EXECUTE ON [dbo].[VenueQuotingSettingsDeleteOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[VenueQuotingSettingsInsertSingle_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MaximumPositionBaseAbs [decimal](18,0),
	@FairPriceImprovementSpreadBasisPoints [decimal](18,6),
	@StopLossNetInUsd [bigint],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Quoting'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Quoting TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId]) VALUES (@SymbolId, @TradingSystemTypeId)

	SELECT @TradingSystemId = scope_identity();

	INSERT INTO [dbo].[VenueQuotingSettings]
           ([SymbolId]
           ,[MaximumPositionBaseAbs]
           ,[FairPriceImprovementSpreadBasisPoints]
           ,[StopLossNetInUsd]   
           ,[Enabled]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MaximumPositionBaseAbs
           ,@FairPriceImprovementSpreadBasisPoints
           ,@StopLossNetInUsd
           ,@Enabled
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

GRANT EXECUTE ON [dbo].[VenueQuotingSettingsInsertSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[VenueQuotingSystemSettingsDisableEnableOne_SP]
(
	@TradingSystemId int,
	@Enable Bit
)
AS
BEGIN
	UPDATE
		[dbo].[VenueQuotingSettings]
	SET
		[Enabled] = @Enable
	WHERE
		[TradingSystemId] = @TradingSystemId
END
GO

GRANT EXECUTE ON [dbo].[VenueQuotingSystemSettingsDisableEnableOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[VenueQuotingSystemSettingsReset_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	UPDATE
		[dbo].[VenueQuotingSettings]
	SET
		[LastResetRequestedUtc] = GETUTCDATE()
	WHERE
		[TradingSystemId] = @TradingSystemId
		AND [Enabled] = 0
END
GO

GRANT EXECUTE ON [dbo].[VenueQuotingSystemSettingsReset_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[VenueQuotingSystemSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumPositionBaseAbs [decimal](18,0),
	@FairPriceImprovementSpreadBasisPoints [decimal](18,6),
	@StopLossNetInUsd [bigint],
	@Enabled [bit]
)
AS
BEGIN
	UPDATE
		[dbo].[VenueQuotingSettings]
	SET
		[MaximumPositionBaseAbs] = @MaximumPositionBaseAbs,
		[FairPriceImprovementSpreadBasisPoints] = @FairPriceImprovementSpreadBasisPoints,
		[StopLossNetInUsd] = @StopLossNetInUsd,
		[Enabled] = @Enabled
	WHERE
		[TradingSystemId] = @TradingSystemId
		AND [LastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

GRANT EXECUTE ON [dbo].[VenueQuotingSystemSettingsUpdateSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


ALTER PROCEDURE [dbo].[GetTradingSystemsOveralDailyStats_SP]
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

		SELECT 
			counterparty.[CounterpartyCode] + ' Cross' AS SystemName,
			count(sett.[TradingSystemId]) AS ConfiguredSystems,
			SUM(CONVERT(int, sett.[Enabled])) AS EnabledSystems,
			SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
			SUM(stats.[DealsNum]) AS DealsNum,
			SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) AS PnlGrossUsdPerMUsd,
			SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) AS CommissionsUsdPerMUsd,
			SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) AS PnlNetUsdPerMUsd,
			SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
			SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
			SUM(stats.[PnlNetUsd]) AS PnlNetUsd
		FROM 
			[dbo].[VenueCrossSettings] sett
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
			LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = sett.[TradingSystemId]
		GROUP BY
			counterparty.[CounterpartyCode]
	UNION ALL
		SELECT 
			counterparty.[CounterpartyCode]  + ' MM' AS SystemName,
			count(sett.[TradingSystemId]) AS ConfiguredSystems,
			SUM(CONVERT(int, sett.[Enabled])) AS EnabledSystems,
			SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
			SUM(stats.[DealsNum]) AS DealsNum,
			SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) AS PnlGrossUsdPerMUsd,
			SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) AS CommissionsUsdPerMUsd,
			SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) AS PnlNetUsdPerMUsd,
			SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
			SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
			SUM(stats.[PnlNetUsd]) AS PnlNetUsd
		FROM 
			[dbo].[VenueMMSettings] sett
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
			LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = sett.[TradingSystemId]
		GROUP BY
			counterparty.[CounterpartyCode]
	UNION ALL
		SELECT 
			counterparty.[CounterpartyCode]  + ' Quoting' AS SystemName,
			count(sett.[TradingSystemId]) AS ConfiguredSystems,
			SUM(CONVERT(int, sett.[Enabled])) AS EnabledSystems,
			SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
			SUM(stats.[DealsNum]) AS DealsNum,
			SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) AS PnlGrossUsdPerMUsd,
			SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) AS CommissionsUsdPerMUsd,
			SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) AS PnlNetUsdPerMUsd,
			SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
			SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
			SUM(stats.[PnlNetUsd]) AS PnlNetUsd
		FROM 
			[dbo].[VenueQuotingSettings] sett
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
			LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = sett.[TradingSystemId]
		GROUP BY
			counterparty.[CounterpartyCode]
	ORDER BY SystemName
END
GO

INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LM1'), 'Quoting')
GO



CREATE PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FairPriceImprovementSpreadBasisPoints],
		sett.[StopLossNetInUsd],
		sett.[Enabled],
		sett.[LastResetRequestedUtc],
		sett.[LastUpdatedUtc]
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

GRANT EXECUTE ON [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO