

DROP TABLE [dbo].[Settings]
GO


DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.RiskManagement.dll.Schema]
GO

/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.RiskManagement.dll.Schema]    Script Date: 1. 10. 2013 11:11:22 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.RiskManagement.dll.Schema] AS 
N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RiskManagementSettings" nillable="true" type="RiskManagementSettings" />
  <xs:complexType name="RiskManagementSettings">
    <xs:sequence>
      <xs:element name="ConfigLastUpdated"  nillable="true" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAndPosition" type="PriceAndPositionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersSubmissionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrderMaximumAllowedSizeSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedBaseAbsSizeInUsd" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="PriceAndPositionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumExternalNOPInUSD" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceDeviationCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="Symbols" type="ArrayOfSymbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbol">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="Symbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="Symbol">
    <xs:attribute name="ShortName" type="xs:string" />
    <xs:attribute name="MidPrice" type="xs:decimal" use="optional" />
    <xs:attribute name="NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent" type="xs:decimal" use="optional" />
  </xs:complexType>
</xs:schema>'
GO

CREATE TABLE [dbo].[Settings](
	[Name] [nvarchar](100) NOT NULL,
	[SettingsXml] [xml](CONTENT [dbo].[Kreslik.Integrator.RiskManagement.dll.Schema]) NOT NULL,
	[LastUpdated] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


INSERT INTO [dbo].[Settings]
           ([Name]
           ,[SettingsXml]
		   ,[LastUpdated])
     VALUES
           (N'Kreslik.Integrator.RiskManagement.dll'
           ,
N'<RiskManagementSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ConfigLastUpdated>2013-10-01-0846-UTC</ConfigLastUpdated>
  <ExternalOrdersSubmissionRate>
    <RiskCheckAllowed>true</RiskCheckAllowed>
    <MaximumAllowedInstancesPerTimeInterval>50</MaximumAllowedInstancesPerTimeInterval>
    <TimeIntervalToCheck_Seconds>5</TimeIntervalToCheck_Seconds>
  </ExternalOrdersSubmissionRate>
  <ExternalOrdersRejectionRate>
    <RiskCheckAllowed>true</RiskCheckAllowed>
    <MaximumAllowedInstancesPerTimeInterval>100</MaximumAllowedInstancesPerTimeInterval>
    <TimeIntervalToCheck_Seconds>40</TimeIntervalToCheck_Seconds>
  </ExternalOrdersRejectionRate>
  <ExternalOrderMaximumAllowedSize>
    <RiskCheckAllowed>true</RiskCheckAllowed>
    <MaximumAllowedBaseAbsSizeInUsd>2500000</MaximumAllowedBaseAbsSizeInUsd>
  </ExternalOrderMaximumAllowedSize>
  <PriceAndPosition>
    <ExternalNOPRiskCheckAllowed>true</ExternalNOPRiskCheckAllowed>
    <MaximumExternalNOPInUSD>10000000</MaximumExternalNOPInUSD>
    <MaximumAllowedNOPCalculationTime_Milliseconds>2</MaximumAllowedNOPCalculationTime_Milliseconds>
    <PriceDeviationCheckAllowed>true</PriceDeviationCheckAllowed>
    <ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed>true</ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed>
    <Symbols>
      <Symbol ShortName="AUDCAD" MidPrice="0.970895" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="AUDCHF" MidPrice="0.849425" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="AUDDKK" MidPrice="5.1756" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="AUDHKD" MidPrice="7.25922" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="AUDJPY" MidPrice="92.29449" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="AUDNOK" MidPrice="5.6519" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="AUDNZD" MidPrice="1.133515" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="AUDSEK" MidPrice="5.9941" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="AUDSGD" MidPrice="1.179435" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="AUDUSD" MidPrice="0.942085" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="CADCHF" MidPrice="0.8748" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="CADDKK" MidPrice="5.3366" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="CADJPY" MidPrice="95.0635" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="CADNOK" MidPrice="5.8252" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="CADNZD" MidPrice="1.1671" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="CADSEK" MidPrice="6.1791" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="CHFDKK" MidPrice="6.1015" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="CHFJPY" MidPrice="108.656" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="CHFNOK" MidPrice="6.6588" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="CHFSEK" MidPrice="7.0631" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="DKKJPY" MidPrice="17.806" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="DKKNOK" MidPrice="1.0912" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="DKKSEK" MidPrice="1.158" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURAUD" MidPrice="1.43919" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURCAD" MidPrice="1.397245" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURCHF" MidPrice="1.222465" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURCNH" MidPrice="8.2849" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURCZK" MidPrice="25.7045" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURDKK" MidPrice="7.458505" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURGBP" MidPrice="0.83543" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURHKD" MidPrice="10.48485" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURHUF" MidPrice="296.681" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURJPY" MidPrice="132.83" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURMXN" MidPrice="17.73615" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURNOK" MidPrice="8.157125" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURNZD" MidPrice="1.631335" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURPLN" MidPrice="4.2204" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURRUB" MidPrice="43.3967" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURSEK" MidPrice="8.650305" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURSKK" MidPrice="30.1258" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURTRY" MidPrice="2.7286" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURUSD" MidPrice="1.35581" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="EURZAR" MidPrice="13.53362" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="GBPAUD" MidPrice="1.722665" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="GBPCAD" MidPrice="1.67252" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="GBPCHF" MidPrice="1.46324" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="GBPCZK" MidPrice="30.8005" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="GBPDKK" MidPrice="8.9256" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="GBPHUF" MidPrice="356.4313" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="GBPJPY" MidPrice="158.996" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="GBPNOK" MidPrice="9.7638" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="GBPNZD" MidPrice="1.95265" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="GBPPLN" MidPrice="5.0486" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="GBPSEK" MidPrice="10.3336" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="GBPUSD" MidPrice="1.622895" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="HKDJPY" MidPrice="12.63375" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="MXNJPY" MidPrice="7.5011" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="NOKJPY" MidPrice="16.3136" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="NOKSEK" MidPrice="1.060485" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="NZDCAD" MidPrice="0.8569" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="NZDCHF" MidPrice="0.7498" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="NZDDKK" MidPrice="4.5736" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="NZDJPY" MidPrice="81.422" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="NZDNOK" MidPrice="4.9905" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="NZDSEK" MidPrice="5.2929" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="NZDSGD" MidPrice="1.040555" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="NZDUSD" MidPrice="0.83109" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="SGDJPY" MidPrice="78.256" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDCAD" MidPrice="1.03056" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDCHF" MidPrice="0.901655" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDCNH" MidPrice="6.1172" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDCZK" MidPrice="18.959" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDDKK" MidPrice="5.4934" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDHKD" MidPrice="7.754795" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDHUF" MidPrice="218.825" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDILS" MidPrice="3.5201" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDJPY" MidPrice="97.9705" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDMXN" MidPrice="13.08125" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDNOK" MidPrice="6.016045" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDPLN" MidPrice="3.1129" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDRUB" MidPrice="32.4515" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDSEK" MidPrice="6.3798" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDSGD" MidPrice="1.25192" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDSKK" MidPrice="22.2791" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDTRY" MidPrice="2.01205" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="USDZAR" MidPrice="9.982124" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
      <Symbol ShortName="ZARJPY" MidPrice="9.815001" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10"/>
    </Symbols>
  </PriceAndPosition>
</RiskManagementSettings>'
,
GETUTCDATE())
GO