
INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'LM2_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=kgtinvestINS1

SSLEnable=Y
SSLValidateCertificates=N
SSLValidateCertificatesOID=N

[SESSION]
DataDictionary=FIX44_LMX.xml
BeginString=FIX.4.4
TargetCompID=LMXBLM
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=443
SocketConnectHost=91.215.165.19', GETUTCDATE())
GO


INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'LM2_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=kgtinvestINS1

SSLEnable=Y
SSLValidateCertificates=N
SSLValidateCertificatesOID=N

[SESSION]
DataDictionary=FIX44_LMX.xml
BeginString=FIX.4.4
TargetCompID=LMXBL
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=443
SocketConnectHost=91.215.165.18', GETUTCDATE())
GO

INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (24, N'LMAX', N'LM2', 1)
GO


DECLARE @F1QuoFixSettingId INT
DECLARE @F1OrdFixSettingId INT

SELECT
	@F1QuoFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'LM2_QUO'

SELECT
	@F1OrdFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'LM2_ORD'


INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [IntegratorEnvironmentId], @F1QuoFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]

INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [IntegratorEnvironmentId], @F1OrdFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]
GO


INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LM2'), 8.25)


DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM2" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
      <xs:element minOccurs="1" maxOccurs="1" name="STP" type="STPSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DealConfirmationSupported" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumDealConfirmationDelay_Milliseconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXSTPChannel_CTIPBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNongenuineInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialRejectionsHandling" type="ArrayOfSpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SpecialRejection" type="SpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="TriggerPhrase" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="MarketSessionIntoSeparateProcess" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="STPDestinations" type="ArrayOfSTPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSTPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="STPDestination" nillable="true" type="STPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Name" type="STPCounterparty" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedTicketTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="ServedCounterparties" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="STPCounterparty">
    <xs:restriction base="xs:string">
      <xs:enumeration value="CTIPB" />
      <xs:enumeration value="Traiana" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

-- Update manually SessionSettings.xml:

--  replace:

--  <FIXChannel_LMX>
--    <Username>kgtinvestfix</Username>
--    <Password>p5%w!rD4u</Password>
--  </FIXChannel_LMX>
  
--with:
  
--  <FIXChannel_LM1>
--    <Username>kgtinvestfix</Username>
--    <Password>p5%w!rD4u</Password>
--  </FIXChannel_LM1>
--  <FIXChannel_LM2>
--    <Username>kgtinvestINS1</Username>
--    <Password>password1</Password>
--  </FIXChannel_LM2>


--and add LM2 to counterparts settings and Traiana destinations - basicaly everywhere where we have LM1


--USED BY INTEGRATOR!
--DROP PROCEDURE [dbo].[GetUpdatedVenueCrossSystemSettings_SP]
--DROP PROCEDURE [dbo].[GetUpdatedVenueMMSystemSettings_SP]
GO

CREATE PROCEDURE [dbo].[GetTradingSystemsOveralDailyStats_SP]
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

		SELECT 
			counterparty.[CounterpartyCode] + ' Cross' AS SystemName,
			count(sett.[TradingSystemId]) AS ConfiguredSystems,
			SUM(CONVERT(int, sett.[Enabled])) AS EnabledSystems,
			SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
			SUM(stats.[DealsNum]) AS DealsNum,
			SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) AS PnlGrossUsdPerMUsd,
			SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) AS CommissionsUsdPerMUsd,
			SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) AS PnlNetUsdPerMUsd,
			SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
			SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
			SUM(stats.[PnlNetUsd]) AS PnlNetUsd
		FROM 
			[dbo].[VenueCrossSettings] sett
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
			LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = sett.[TradingSystemId]
		GROUP BY
			counterparty.[CounterpartyCode]
	UNION ALL
		SELECT 
			counterparty.[CounterpartyCode]  + ' MM' AS SystemName,
			count(sett.[TradingSystemId]) AS ConfiguredSystems,
			SUM(CONVERT(int, sett.[Enabled])) AS EnabledSystems,
			SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
			SUM(stats.[DealsNum]) AS DealsNum,
			SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) AS PnlGrossUsdPerMUsd,
			SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) AS CommissionsUsdPerMUsd,
			SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) AS PnlNetUsdPerMUsd,
			SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
			SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
			SUM(stats.[PnlNetUsd]) AS PnlNetUsd
		FROM 
			[dbo].[VenueMMSettings] sett
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
			LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = sett.[TradingSystemId]
		GROUP BY
			counterparty.[CounterpartyCode]
	ORDER BY SystemName
END
GO

GRANT EXECUTE ON [dbo].[GetTradingSystemsOveralDailyStats_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetTotalDailyVolume_SP]
AS
BEGIN
	SELECT 
		SUM([VolumeBaseAbsInUsd])
	FROM 
		[dbo].[PersistedStatisticsPerCounterpartyDaily]
END
GO

GRANT EXECUTE ON [dbo].[GetTotalDailyVolume_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO



CREATE TABLE [dbo].[TradingSystemType](
	[TradingSystemTypeId] [tinyint] IDENTITY(1,1) NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SystemName] [varchar](8) NOT NULL,
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_TradingSystemType_id] ON [dbo].[TradingSystemType]
(
	[TradingSystemTypeId] ASC
) ON [PRIMARY]
GO

CREATE UNIQUE INDEX [IX_TradingSystemType_Name] ON [dbo].[TradingSystemType]
(
	[CounterpartyId] ASC,
	[SystemName] ASC
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TradingSystemType]  WITH CHECK ADD  CONSTRAINT [FK_TradingSystemType_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO

ALTER TABLE [dbo].[TradingSystemType] CHECK CONSTRAINT [FK_TradingSystemType_Counterparty]
GO

INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTF'), 'Cross')
INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTF'), 'MM')
INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTA'), 'MM')
INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FA1'), 'Cross')
INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FA1'), 'MM')
INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LM1'), 'Cross')
INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LM2'), 'Cross')
GO

CREATE TABLE [dbo].[TradingSystemMonitoringPage](
	[TradingSystemMonitoringPageId] [int] IDENTITY(1,1) NOT NULL,
	[TradingSystemTypeId] [tinyint] NOT NULL,
	[BeginSymbolId] [tinyint] NOT NULL,
	[EndSymbolId] [tinyint] NOT NULL,
	[StripsCount] [tinyint] NOT NULL,
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_TradingSystemMonitoringPage_id] ON [dbo].[TradingSystemMonitoringPage]
(
	[TradingSystemMonitoringPageId] ASC
) ON [PRIMARY]
GO


ALTER TABLE [dbo].[TradingSystemMonitoringPage]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemMonitoringPage_TradingSystemTypeId] FOREIGN KEY([TradingSystemTypeId])
REFERENCES [dbo].[TradingSystemType] ([TradingSystemTypeId])
GO

ALTER TABLE [dbo].[TradingSystemMonitoringPage] CHECK CONSTRAINT [FK_TradingSystemMonitoringPage_TradingSystemTypeId]
GO



ALTER TABLE [dbo].[TradingSystemMonitoringPage]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemMonitoringPage_BeginSymbol] FOREIGN KEY([BeginSymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[TradingSystemMonitoringPage] CHECK CONSTRAINT [FK_TradingSystemMonitoringPage_BeginSymbol]
GO


ALTER TABLE [dbo].[TradingSystemMonitoringPage]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemMonitoringPage_EndSymbol] FOREIGN KEY([EndSymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[TradingSystemMonitoringPage] CHECK CONSTRAINT [FK_TradingSystemMonitoringPage_EndSymbol]
GO

CREATE PROCEDURE [dbo].[GetTradingSystemMonitoringPages_SP]
AS
BEGIN
	SELECT
		ctp.CounterpartyCode + ' ' + systemType.SystemName AS SystemType
		,systemType.TradingSystemTypeId AS SystemTypeId
		,ctp.CounterpartyCode AS Counterparty
		,TradingSystemMonitoringPageId AS PageId
		,beginSymbol.Name AS BeginSymbol
		,endSymbol.Name AS EndSymbol
		,StripsCount
	FROM 
		[dbo].[TradingSystemType] systemType
		LEFT JOIN [dbo].[Counterparty] ctp ON systemType.CounterpartyId = ctp.CounterpartyId
		LEFT JOIN  [dbo].[TradingSystemMonitoringPage] systemPage ON systemType.TradingSystemTypeId = systemPage.TradingSystemTypeId
		LEFT JOIN [dbo].[Symbol] beginSymbol ON systemPage.BeginSymbolId = beginSymbol.Id
		LEFT JOIN [dbo].[Symbol] endSymbol ON systemPage.EndSymbolId = endSymbol.Id
	ORDER BY
		ctp.CounterpartyCode + ' ' + systemType.SystemName,
		beginSymbol.Name ASC
END
GO

GRANT EXECUTE ON [dbo].[GetTradingSystemMonitoringPages_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[UpdateTradingSystemMonitoringPages_SP]
(
	@TradingSystemMonitoringPageId INT,
	@BeginSymbol NCHAR(7),
	@EndSymbol NCHAR(7),
	@StripsCount INT
)
AS
BEGIN

	DECLARE @BeginSymbolId INT
	DECLARE @EndSymbolId INT

	SELECT @BeginSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @BeginSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @BeginSymbol) WITH SETERROR
	END
	
	SELECT @EndSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @EndSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @EndSymbol) WITH SETERROR
	END

	UPDATE
		[dbo].[TradingSystemMonitoringPage]
	SET
		BeginSymbolId = @BeginSymbolId,
		EndSymbolId = @EndSymbolId,
		StripsCount = @StripsCount
	WHERE
		TradingSystemMonitoringPageId = @TradingSystemMonitoringPageId
END
GO

GRANT EXECUTE ON [dbo].[UpdateTradingSystemMonitoringPages_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[InsertTradingSystemMonitoringPages_SP]
(
	@BeginSymbol NCHAR(7),
	@EndSymbol NCHAR(7),
	@StripsCount INT,
	@SystemTypeId VARCHAR(16),
	@TradingSystemMonitoringPageId INT OUTPUT
)
AS
BEGIN

	DECLARE @BeginSymbolId INT
	DECLARE @EndSymbolId INT

	SELECT @BeginSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @BeginSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @BeginSymbol) WITH SETERROR
	END
	
	SELECT @EndSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @EndSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @EndSymbol) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystemMonitoringPage]
           ([TradingSystemTypeId]
           ,[BeginSymbolId]
           ,[EndSymbolId]
           ,[StripsCount])
     VALUES
           (@SystemTypeId,
           @BeginSymbolId,
           @EndSymbolId,
           @StripsCount)
	
	SELECT @TradingSystemMonitoringPageId = scope_identity()
END
GO

GRANT EXECUTE ON [dbo].[InsertTradingSystemMonitoringPages_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[DeleteTradingSystemMonitoringPages_SP]
(
	@TradingSystemMonitoringPageId INT
)
AS
BEGIN

	DELETE FROM
		[dbo].[TradingSystemMonitoringPage]
	WHERE
		TradingSystemMonitoringPageId = @TradingSystemMonitoringPageId
END
GO

GRANT EXECUTE ON [dbo].[DeleteTradingSystemMonitoringPages_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE TABLE [dbo].[TradingSystem_Deleted](
	[TradingSystemId] [int] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[Description] [nvarchar](1024) NULL,
	[DeletedUtc] [datetime2] NOT NULL
) ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [IX_TradingSystem_id] ON [dbo].[TradingSystem_Deleted]
(
	[TradingSystemId] ASC
)ON [PRIMARY]
GO


CREATE TRIGGER [dbo].[TradingSystem_ArchiveDeleted] ON [dbo].[TradingSystem] AFTER DELETE
AS
BEGIN
	INSERT INTO
		[dbo].[TradingSystem_Deleted]
			([TradingSystemId],
			[SymbolId],
			[Description],
			[DeletedUtc])
	SELECT
		[TradingSystemId],
		[SymbolId],
		[Description],
		GETUTCDATE()
	FROM
		deleted
END
GO

CREATE PROCEDURE [dbo].[GetNumberOfConflicitngSymbolRangesForSystemType_SP]
(
	@TradingSystemTypeId INT,
	@PageIdToExclude INT = NULL,
	@BeginSymbol VARCHAR(7),
	@EndSymbol VARCHAR(7)
)
AS
BEGIN

	DECLARE @BeginSymbolId INT
	DECLARE @EndSymbolId INT
	DECLARE @BeginSymbolRank INT
	DECLARE @EndSymbolRank INT

	SELECT @BeginSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @BeginSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @BeginSymbol) WITH SETERROR
	END

	SELECT @EndSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @EndSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @EndSymbol) WITH SETERROR
	END

	DECLARE @SymbolRanks TABLE (SymbolName varchar(7), SymbolId int, SymbolAlphaRank int)

	INSERT INTO @SymbolRanks(SymbolName, SymbolId, SymbolAlphaRank)
	SELECT
		name, 
		id, 
		RANK() OVER (order by name asc) AS AlphaRank 
	FROM
		[dbo].[Symbol]

	SELECT @BeginSymbolRank = SymbolAlphaRank FROM @SymbolRanks WHERE SymbolId = @BeginSymbolId
	SELECT @EndSymbolRank = SymbolAlphaRank FROM @SymbolRanks WHERE SymbolId = @EndSymbolId

	SELECT
		COUNT(*)
	FROM
		[dbo].[TradingSystemMonitoringPage] sysPage
		INNER JOIN @SymbolRanks BeginSymbolRanks on sysPage.BeginSymbolId = BeginSymbolRanks.SymbolId
		INNER JOIN @SymbolRanks EndSymbolRanks on sysPage.EndSymbolId = EndSymbolRanks.SymbolId
	WHERE
		[TradingSystemTypeId] = @TradingSystemTypeId
		AND
		(@PageIdToExclude IS NULL OR [TradingSystemMonitoringPageId] != @PageIdToExclude)
		AND
		(
			--start symbol of existing range is inside new tested range
			(BeginSymbolRanks.SymbolAlphaRank >= @BeginSymbolRank AND BeginSymbolRanks.SymbolAlphaRank <= @EndSymbolRank)
			OR
			--end symbol of existing range is inside new tested range
			(EndSymbolRanks.SymbolAlphaRank >= @BeginSymbolRank AND EndSymbolRanks.SymbolAlphaRank <= @EndSymbolRank)
		)
END
GO

GRANT EXECUTE ON [dbo].[GetNumberOfConflicitngSymbolRangesForSystemType_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO



CREATE TABLE [dbo].[VenueCrossSettings_Changes](
	[SymbolId] [tinyint] NOT NULL,
	[MinimumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MaximumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MinimumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[MinimumTimeBetweenTrades_seconds] [int] NOT NULL,
	[MaximumWaitTimeOnImprovementTick_milliseconds] [int] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[ValidFromUtc] [datetime2](7) NOT NULL,
	[ValidToUtc] [datetime2](7) NOT NULL,
	[Deleted] [Bit] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[VenueCrossSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueCrossSettings_Changes_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[VenueCrossSettings_Changes] CHECK CONSTRAINT [FK_VenueCrossSettings_Changes_Symbol]
GO

ALTER TABLE [dbo].[VenueCrossSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueCrossSettings_Changes_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO

ALTER TABLE [dbo].[VenueCrossSettings_Changes] CHECK CONSTRAINT [FK_VenueCrossSettings_Changes_Counterparty]
GO

CREATE CLUSTERED INDEX [IX_VenueCrossSettings_Changes] ON [dbo].[VenueCrossSettings_Changes]
(
	[TradingSystemId] ASC,
	[ValidToUtc] ASC
) ON [PRIMARY]
GO


CREATE TRIGGER [dbo].[VenueCrossSettings_DeletedOrUpdated] ON [dbo].[VenueCrossSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueCrossSettings_Changes]
	   ([SymbolId]
	   ,[MinimumSizeToTrade]
	   ,[MaximumSizeToTrade]
	   ,[MinimumDiscountBasisPointsToTrade]
	   ,[MinimumTimeBetweenTrades_seconds]
	   ,[MaximumWaitTimeOnImprovementTick_milliseconds]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[MinimumSizeToTrade]
		,deleted.[MaximumSizeToTrade]
		,deleted.[MinimumDiscountBasisPointsToTrade]
		,deleted.[MinimumTimeBetweenTrades_seconds]
		,deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueCrossSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueCrossSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([MinimumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MinimumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MinimumDiscountBasisPointsToTrade] != deleted.[MinimumDiscountBasisPointsToTrade]))
		OR (UPDATE([MinimumTimeBetweenTrades_seconds]) AND (topUpdatedChange.[MinimumTimeBetweenTrades_seconds] IS NULL OR topUpdatedChange.[MinimumTimeBetweenTrades_seconds] != deleted.[MinimumTimeBetweenTrades_seconds]))
		OR (UPDATE([MaximumWaitTimeOnImprovementTick_milliseconds]) AND (topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] IS NULL OR topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] != deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
END
GO





CREATE TABLE [dbo].[VenueMMSettings_Changes](
	[SymbolId] [tinyint] NOT NULL,
	[SizeToTrade] [decimal](18, 0) NOT NULL,
	[BestPriceImprovementOffsetBasisPoints] [decimal](18, 6) NOT NULL,
	[MaximumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[MaximumWaitTimeOnImprovementTick_milliseconds] [int] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[ValidFromUtc] [datetime2](7) NOT NULL,
	[ValidToUtc] [datetime2](7) NOT NULL,
	[Deleted] [Bit] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VenueMMSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueMMSettings_Changes_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[VenueMMSettings_Changes] CHECK CONSTRAINT [FK_VenueMMSettings_Changes_Symbol]
GO

ALTER TABLE [dbo].[VenueMMSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueMMSettings_Changes_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO

ALTER TABLE [dbo].[VenueMMSettings_Changes] CHECK CONSTRAINT [FK_VenueMMSettings_Changes_Counterparty]
GO

CREATE CLUSTERED INDEX [IX_VenueMMSettings_Changes] ON [dbo].[VenueMMSettings_Changes]
(
	[TradingSystemId] ASC,
	[ValidToUtc] ASC
) ON [PRIMARY]
GO


CREATE TRIGGER [dbo].[VenueMMSettings_DeletedOrUpdated] ON [dbo].[VenueMMSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueMMSettings_Changes]
	   ([SymbolId]
	   ,[SizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade]
	   ,[MaximumWaitTimeOnImprovementTick_milliseconds]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[SizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueMMSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueMMSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([SizeToTrade]) AND (topUpdatedChange.[SizeToTrade] IS NULL OR topUpdatedChange.[SizeToTrade] != deleted.[SizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([MaximumWaitTimeOnImprovementTick_milliseconds]) AND (topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] IS NULL OR topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] != deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
END
GO

--
ALTER TABLE [dbo].[TradingSystem] ADD TradingSystemTypeId TINYINT
GO

UPDATE
	system
SET
	system.[TradingSystemTypeId] = systemType.TradingSystemTypeId
FROM
	[dbo].[TradingSystem] system
	INNER JOIN [dbo].[VenueCrossSettings] crossSettings ON system.TradingSystemId = crossSettings.TradingSystemId
	INNER JOIN [dbo].[TradingSystemType] systemType ON crossSettings.CounterpartyId = systemType.CounterpartyId AND systemType.SystemName = 'Cross'
	
UPDATE
	system
SET
	system.[TradingSystemTypeId] = systemType.TradingSystemTypeId
FROM
	[dbo].[TradingSystem] system
	INNER JOIN [dbo].[VenueCrossSettings_Changes] crossSettings ON system.TradingSystemId = crossSettings.TradingSystemId
	INNER JOIN [dbo].[TradingSystemType] systemType ON crossSettings.CounterpartyId = systemType.CounterpartyId AND systemType.SystemName = 'Cross'
	
UPDATE
	system
SET
	system.[TradingSystemTypeId] = systemType.TradingSystemTypeId
FROM
	[dbo].[TradingSystem] system
	INNER JOIN [dbo].[VenueMMSettings] mmSettings ON system.TradingSystemId = mmSettings.TradingSystemId
	INNER JOIN [dbo].[TradingSystemType] systemType ON mmSettings.CounterpartyId = systemType.CounterpartyId AND systemType.SystemName = 'MM'
	
UPDATE
	system
SET
	system.[TradingSystemTypeId] = systemType.TradingSystemTypeId
FROM
	[dbo].[TradingSystem] system
	INNER JOIN [dbo].[VenueMMSettings_Changes] mmSettings ON system.TradingSystemId = mmSettings.TradingSystemId
	INNER JOIN [dbo].[TradingSystemType] systemType ON mmSettings.CounterpartyId = systemType.CounterpartyId AND systemType.SystemName = 'MM'
	
--Same change would be needed for [dbo].[TradingSystem_Deleted] - not repopulating since it is still empty in prod

ALTER TABLE [dbo].[TradingSystem] ALTER COLUMN TradingSystemTypeId TINYINT NOT NULL
GO

ALTER TABLE [dbo].[TradingSystem_Deleted] ADD TradingSystemTypeId TINYINT NOT NULL
GO

ALTER TRIGGER [dbo].[TradingSystem_ArchiveDeleted] ON [dbo].[TradingSystem] AFTER DELETE
AS
BEGIN
	INSERT INTO
		[dbo].[TradingSystem_Deleted]
			([TradingSystemId],
			[TradingSystemTypeId],
			[SymbolId],
			[Description],
			[DeletedUtc])
	SELECT
		[TradingSystemId],
		[TradingSystemTypeId],
		[SymbolId],
		[Description],
		GETUTCDATE()
	FROM
		deleted
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsInsertSingle_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'MM'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'MM TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId]) VALUES (@SymbolId, @TradingSystemTypeId)

	SELECT @TradingSystemId = scope_identity();

	INSERT INTO [dbo].[VenueMMSettings]
           ([SymbolId]
           ,[SizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[Enabled]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@SizeToTrade 
           ,@BestPriceImprovementOffsetBasisPoints
           ,@MaximumDiscountBasisPointsToTrade
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
           ,@Enabled
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

ALTER PROCEDURE [dbo].[VenueCrossSettingsInsertSingle_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenTrades_seconds [int],
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Cross'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Cross TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId]) VALUES (@SymbolId, @TradingSystemTypeId)

	SELECT @TradingSystemId = scope_identity();

	INSERT INTO [dbo].[VenueCrossSettings]
           ([SymbolId]
           ,[MinimumSizeToTrade]
           ,[MaximumSizeToTrade]
           ,[MinimumDiscountBasisPointsToTrade]
           ,[MinimumTimeBetweenTrades_seconds]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[Enabled]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MinimumSizeToTrade
           ,@MaximumSizeToTrade
           ,@MinimumDiscountBasisPointsToTrade
           ,@MinimumTimeBetweenTrades_seconds
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
           ,@Enabled
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO