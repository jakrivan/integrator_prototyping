
--rename
EXEC sp_RENAME 'CounterpartySymbolSettings.[MinimumPriceGranularity]' , 'MinimumPriceIncrement', 'COLUMN'
GO

--add columns
ALTER TABLE [dbo].[CounterpartySymbolSettings] ADD OrderMinimumSize DECIMAL(18, 6)
ALTER TABLE [dbo].[CounterpartySymbolSettings] ADD OrderMinimumSizeIncrement DECIMAL(18, 6)
GO

--update SP accordingly
ALTER PROCEDURE [dbo].[GetCounterpartySymbolSettings_SP] 
AS
BEGIN

	SELECT 
		targetType.Code AS TradingTargetType
		,pair.FxpCode AS Symbol
		,settings.[Supported] AS Supported
		,settings.[MinimumPriceIncrement] AS MinimumPriceIncrement
		,settings.[OrderMinimumSize] AS OrderMinimumSize
		,settings.[OrderMinimumSizeIncrement] AS OrderMinimumSizeIncrement
	FROM 
		[dbo].[CounterpartySymbolSettings] settings
		INNER JOIN [dbo].[FxPair] pair ON pair.FxPairId = settings.[SymbolId]
		INNER JOIN [dbo].[TradingTargetType] targetType ON targetType.Id = settings.[TradingTargetTypeId]

END
GO

--now update all columns with new values
UPDATE
	symInfo
SET
	[OrderMinimumSize] = 1,
	[OrderMinimumSizeIncrement] = 1
FROM
	[dbo].[CounterpartySymbolSettings] symInfo
	INNER JOIN [dbo].[TradingTargetType] targetType ON symInfo.TradingTargetTypeId = targetType.Id
WHERE
	targetType.Code = 'BankPool'
	

UPDATE
	symInfo
SET
	[OrderMinimumSize] = 1000,
	[OrderMinimumSizeIncrement] = 1000
FROM
	[dbo].[CounterpartySymbolSettings] symInfo
	INNER JOIN [dbo].[TradingTargetType] targetType ON symInfo.TradingTargetTypeId = targetType.Id
WHERE
	targetType.Code = 'LMAX'
	
UPDATE
	symInfo
SET
	[OrderMinimumSize] = 0.01,
	[OrderMinimumSizeIncrement] = 0.01
FROM
	[dbo].[CounterpartySymbolSettings] symInfo
	INNER JOIN [dbo].[TradingTargetType] targetType ON symInfo.TradingTargetTypeId = targetType.Id
WHERE
	targetType.Code = 'Hotspot'
	
UPDATE
	symInfo
SET
	[OrderMinimumSize] = 0.01,
	[OrderMinimumSizeIncrement] = 0.01
FROM
	[dbo].[CounterpartySymbolSettings] symInfo
	INNER JOIN [dbo].[TradingTargetType] targetType ON symInfo.TradingTargetTypeId = targetType.Id
WHERE
	targetType.Code = 'FXall'