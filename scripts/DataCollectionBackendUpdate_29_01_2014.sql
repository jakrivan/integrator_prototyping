
--
--       FOLLOWING CODE USED TO GENERATETHE SCRIPT
--
--
--        private static string GetCounterpartySymbolSettingsCommand(int counterpartyId)
--        {
--            List<SymbolInfo> symbolInfos = new List<SymbolInfo>();

--            foreach (string line in File.ReadAllLines(@"E:\KGT\src\Integrator_daily\build\bin\Release\Prototype\logs\2014-01-29\FA1_QUO.log"))
--            {
--                if (line.Contains("SymbolSetings"))
--                {
--                    string[] parts = line.Split(',');
--                    if (parts.Length != 3)
--                        throw new Exception();

--                    Symbol symbol = (Symbol)parts[0].Split(':')[3];

--                    if (!symbolInfos.Any(si => si.Symbol == symbol))
--                    {

--                        symbolInfos.Add(new SymbolInfo()
--                        {
--                            Symbol = (Symbol)parts[0].Split(':')[3],
--                            BasisPoints =
--                                parts[1].Split(':')[1] == "?" ? null : new int?(int.Parse(parts[1].Split(':')[1])),
--                            RatePrecision =
--                                parts[2].Split(':')[1].Contains("?")
--                                    ? null
--                                    : new int?(int.Parse(parts[2].Split(':')[1]))
--                        });
--                    }
--                }
--            }

--            string sqlTemplate = @"INSERT INTO [dbo].[CounterpartySymbolSettings]
--           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
--SELECT {0}, [FxPairId], {2}, {3} FROM [dbo].[FxPair] where [FxpCode] = '{1}'

--";

--            string cmd = string.Empty;

--            foreach (SymbolInfo symbolInfo in symbolInfos.Where(si => si.BasisPoints != null && si.RatePrecision != null))
--            {
--                cmd += string.Format(sqlTemplate, counterpartyId, symbolInfo.Symbol.ShortName,
--                                     ((decimal)(1.0 / Math.Pow(10, symbolInfo.BasisPoints.Value))).ToString().Replace(',','.'),
--                                     ((decimal)(1.0 / Math.Pow(10, symbolInfo.RatePrecision.Value))).ToString().Replace(',','.'));
--            }

--            return cmd;
--        }




INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.001 FROM [dbo].[FxPair] where [FxpCode] = 'AUDJPY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'AUDNZD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'AUDUSD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.001 FROM [dbo].[FxPair] where [FxpCode] = 'CADJPY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.001 FROM [dbo].[FxPair] where [FxpCode] = 'CHFJPY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURAUD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURCAD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURCHF'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.0001 FROM [dbo].[FxPair] where [FxpCode] = 'EURCZK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURDKK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURGBP'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.001 FROM [dbo].[FxPair] where [FxpCode] = 'EURJPY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURNOK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURNZD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURPLN'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURRUB'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURSEK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURTRY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'EURUSD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'GBPAUD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'GBPCAD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'GBPCHF'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.001 FROM [dbo].[FxPair] where [FxpCode] = 'GBPJPY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'GBPUSD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'NOKSEK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'NZDUSD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDCAD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDCHF'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDHKD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDILS'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.001 FROM [dbo].[FxPair] where [FxpCode] = 'USDJPY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDMXN'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDNOK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDPLN'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDRUB'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDSEK'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDSGD'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDTRY'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([CounterpartyId], [SymbolId], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 21, [FxPairId], 0.0001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'USDZAR'






SET IDENTITY_INSERT [dbo].[LiquidityProvider] ON 
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (22, N'LMAX', N'LMX')
GO
SET IDENTITY_INSERT [dbo].[LiquidityProvider] OFF
GO

SET IDENTITY_INSERT [dbo].[LiqProviderStream] ON 
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (78, N'LM1', 22)
GO
SET IDENTITY_INSERT [dbo].[LiqProviderStream] OFF
GO
