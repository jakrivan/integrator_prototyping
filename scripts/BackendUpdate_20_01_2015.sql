


CREATE FUNCTION dbo.ConvertBpToDecimal(@symbolId tinyint, @bpValue [decimal] (18,4))
RETURNS [decimal] (18,9)
AS 
BEGIN
	DECLARE @decimalConvert [decimal] (18,9)

	SELECT @decimalConvert = @bpValue * symbol.LastKnownMeanReferencePrice / 10000.0
	FROM [dbo].[Symbol_Rates] symbol 
	WHERE symbol.Id = @symbolId

	RETURN @decimalConvert
END
GO


CREATE TABLE [dbo].[Symbol_TrustedSpreads](
	[SymbolId] [tinyint] NOT NULL,
	[MaximumTrustedSpreadBp] [decimal] (18,4) NULL,
	[MaximumTrustedSpreadDecimal] AS [dbo].[ConvertBpToDecimal] ([SymbolId], [MaximumTrustedSpreadBp]),
	[MinimumTrustedSpreadBp] [decimal] (18,4) NULL,
	[MinimumTrustedSpreadDecimal] AS [dbo].[ConvertBpToDecimal] ([SymbolId], [MinimumTrustedSpreadBp]),
	[MaximumAllowedTickAge_milliseconds] INT, 
	[MaximumAllowedTickAge] AS (dateadd(millisecond,[MaximumAllowedTickAge_milliseconds],CONVERT([time](7),'00:00:00')))
 CONSTRAINT [PK_Symbol_TrustedSpreads_Rates] PRIMARY KEY CLUSTERED 
(
	[SymbolId] ASC
)
) ON [PRIMARY]


ALTER TABLE [dbo].[Symbol_TrustedSpreads]  WITH NOCHECK ADD  CONSTRAINT [FK_Symbol_TrustedSpreads_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO

ALTER TABLE [dbo].[Symbol_TrustedSpreads] CHECK CONSTRAINT [FK_Symbol_TrustedSpreads_Symbol]
GO

ALTER TABLE [dbo].[Symbol_TrustedSpreads] ADD CONSTRAINT MaximumTrustedSpreadBp_NonNegative CHECK (MaximumTrustedSpreadBp>=0)
ALTER TABLE [dbo].[Symbol_TrustedSpreads] ADD CONSTRAINT MinimumTrustedSpreadBp_NonPositive CHECK (MinimumTrustedSpreadBp<=0)
ALTER TABLE [dbo].[Symbol_TrustedSpreads] ADD CONSTRAINT MaximumAllowedTickAge_milliseconds_Positive CHECK (MaximumAllowedTickAge_milliseconds>0)
GO



INSERT INTO [dbo].[Symbol_TrustedSpreads] ([SymbolId], [MaximumTrustedSpreadBp], [MinimumTrustedSpreadBp], [MaximumAllowedTickAge_milliseconds])
SELECT [Id], 100, -0.0011, 300000 FROM [dbo].[Symbol]
GO

CREATE PROCEDURE [dbo].[GetSymblTrustedRates_SP]
AS
BEGIN
	SELECT
		symbol.[Name] AS Symbol,
		spreads.[MaximumTrustedSpreadBp] AS MaxTrustedSpreadBp,
		spreads.[MaximumTrustedSpreadDecimal] AS MaxTrustedSpreadDecimal,
		spreads.[MinimumTrustedSpreadBp] AS MinTrustedSpreadBp,
		spreads.[MinimumTrustedSpreadDecimal] AS MinTrustedSpreadDecimal,
		spreads.[MaximumAllowedTickAge_milliseconds] AS MaximumAllowedTickAge_milliseconds
	FROM
		[dbo].[Symbol_TrustedSpreads] spreads
		INNER JOIN [dbo].[Symbol] symbol ON spreads.SymbolId = symbol.Id
END
GO

GRANT EXECUTE ON [dbo].[GetSymblTrustedRates_SP] TO [IntegratorServiceAccount] AS [dbo]
GO




	
DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd] AS N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RiskManagementSettings" nillable="true" type="RiskManagementSettings" />
  <xs:complexType name="RiskManagementSettings">
    <xs:sequence>
      <xs:element name="ConfigLastUpdated"  nillable="true" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="DisallowTradingOnInstances" type="ArrayOfString" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsRate" type="FatalErrorsRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAndPosition" type="PriceAndPositionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Instance" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersSubmissionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FatalErrorsRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrderMaximumAllowedSizeSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedBaseAbsSizeInUsd" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="PriceAndPositionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumExternalNOPInUSD" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceDeviationCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="OnlinePriceDeviationCheckMaxPriceAge_Minutes" type="xs:int" />
	  <xs:element minOccurs="1" maxOccurs="1" name="SymbolTrustworthyCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumSymbolUntrustworthyPeriod_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="Symbols" type="ArrayOfSymbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbol">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="Symbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="Symbol">
    <xs:attribute name="ShortName" type="xs:string" />
    <xs:attribute name="NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
  </xs:complexType>
</xs:schema>'
GO


UPDATE [dbo].[Settings]
SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '</OnlinePriceDeviationCheckMaxPriceAge_Minutes>', '</OnlinePriceDeviationCheckMaxPriceAge_Minutes>
<SymbolTrustworthyCheckAllowed>true</SymbolTrustworthyCheckAllowed>
<MinimumSymbolUntrustworthyPeriod_Seconds>5</MinimumSymbolUntrustworthyPeriod_Seconds>')
WHERE
	[Name] = 'Kreslik.Integrator.RiskManagement.dll'
	AND CAST([SettingsXml] AS NVARCHAR(MAX)) like '%</OnlinePriceDeviationCheckMaxPriceAge_Minutes>%'
	
	

ALTER TABLE [dbo].[DealExternalExecuted] ADD CounterpartyClientId [VARCHAR](20) NULL
GO

ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 2),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL,
	@SingleFillSystemId int = NULL,
	@MultipleFillsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL,
	@CounterpartyClientId varchar(20) = NULL)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @MultipleFillsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleFillsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external deal for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity])
			VALUES
				(ISNULL(@SingleFillSystemId, -1)
				,@AmountBasePolExecuted
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId)
		END
		
		
		INSERT INTO 
		[dbo].[DealExternalExecuted]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolExecuted]
			   ,[AmountTermPolExecutedInUsd]
			   ,[Price]
			   ,[CounterpartyTransactionId]
			   ,[CounterpartySuppliedExecutionTimeStampUtc]
			   ,[ValueDateCounterpartySuppliedLocMktDate]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[InternalTransactionIdentity]
			   ,[FlowSideId]
			   ,[CounterpartyClientId])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolExecuted
			   ,@AmountTermPolExecutedInUsd
			   ,@Price
			   ,@CounterpartyTransactionId
			   ,@CounterpartyExecutionReportTimeStampUtc
			   ,@ValueDateCounterpartySuppliedLocMktDate
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@CounterpartySentExecutionReportUtc
			   ,@InternalDealId
			   ,@FlowSideId
			   ,@CounterpartyClientId)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO
	