ALTER TRIGGER [dbo].[ExternalOrdersTransmissionSwitch_Changing] ON [dbo].[ExternalOrdersTransmissionSwitch]
--needed to be able to execute sp_send_dbmail
WITH EXECUTE AS 'dbo'
AFTER INSERT, UPDATE
AS

BEGIN

	--we need to store the Id before commiting, as afterwards the inserted table will be empty
	DECLARE @updatedSwitchId INT
	SELECT TOP 1 @updatedSwitchId = [Id] FROM inserted

	-- commit the internal insert transaction to prevent problems during inserting critical data
	-- and also to make sure that following functions has fresh data
	COMMIT TRANSACTION

	--Extremely important to perform operation in try - catch to make sure that error happens just once
	begin try
	
        DECLARE @subject NVARCHAR(255) = 'OrderTrans ' + CASE WHEN [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]() = 1 THEN 'ON (' ELSE 'OFF (' END

		SELECT 
			@subject = @subject + [SwitchFriendlyName] + ' ' + CASE WHEN [TransmissionEnabled] = 1 THEN 'ON)' ELSE 'OFF)' END
		FROM
			[dbo].[ExternalOrdersTransmissionSwitch]
		WHERE
			[Id] = @updatedSwitchId

		DECLARE @body NVARCHAR(MAX) = [dbo].[GetExternalOrdersTransmissionOverviewString](@updatedSwitchId)

		DECLARE @pbContactList NVARCHAR(MAX) = 
'citifx.primebrokerage@citi.com; fxpb.mo@citi.com; charles.wells@citi.com; dan.shaw@citi.com; daniel.walters@citi.com; john.comatas@citi.com; kevin.y.lin@citi.com; leonard.levine@citi.com; michael.glen.jones@citi.com; mike.clement@citi.com; narasimha.rao@citi.com; paul.cusack@citi.com; ritchie.ward@citi.com; satomi.takiguchi@citi.com; sherrie.low@citi.com; william.fearon@citi.com; william.c.undertajlo@citi.com; jeremy.lee@citi.com'

		DECLARE @kgtContactList NVARCHAR(MAX) = 'michal.kreslik@kgtinv.com; marek.fogiel@kgtinv.com; jan.krivanek@kgtinv.com'

		DECLARE @copyRecipientsList NVARCHAR(MAX) = @kgtContactList

		--For prime broker switch changes add PB contacts to cc
		SELECT
			@copyRecipientsList = @copyRecipientsList + '; ' + @pbContactList
		FROM
			[dbo].[ExternalOrdersTransmissionSwitch]
		WHERE
			[Id] = @updatedSwitchId AND [SwitchFriendlyName] = 'PrimeBroker'

		EXEC msdb.dbo.sp_send_dbmail
				@recipients = 'support@kgtinv.com', 
				@copy_recipients = @copyRecipientsList,
				@profile_name = 'SQL Mail Profile',
				@subject = @subject,
				@body = @body;
    end try
    begin catch
        --select 0;
    end catch;

	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION

END
GO



DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LMX" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
      <xs:element minOccurs="1" maxOccurs="1" name="STP" type="STPSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXSTPChannel_CTIPBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="STPDestinations" type="ArrayOfSTPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSTPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="STPDestination" nillable="true" type="STPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Name" type="STPCounterparty" />
      <xs:element minOccurs="0" maxOccurs="1" name="ServedCounterparties" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="STPCounterparty">
    <xs:restriction base="xs:string">
      <xs:enumeration value="CTIPB" />
      <xs:enumeration value="Traiana" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO



	--manually update settings by adding following elements
  
  --<STP>
  --  <STPDestinations>
  --    <STPDestination>
  --      <Name>CTIPB</Name>
  --      <ServedCounterparties>
		--	<Counterparty>CRS</Counterparty>
		--	<Counterparty>UBS</Counterparty>
		--	<Counterparty>DBK</Counterparty>
		--	<Counterparty>CTI</Counterparty>
		--	<Counterparty>BOA</Counterparty>
		--	<Counterparty>MGS</Counterparty>
		--	<Counterparty>RBS</Counterparty>
		--	<Counterparty>HSB</Counterparty>
		--	<Counterparty>JPM</Counterparty>
		--	<Counterparty>GLS</Counterparty>
		--	<Counterparty>BNP</Counterparty>
		--	<Counterparty>NOM</Counterparty>
		--	<Counterparty>CZB</Counterparty>
		--	<Counterparty>BRX</Counterparty>
		--	<Counterparty>SOC</Counterparty>
		--	<Counterparty>HTA</Counterparty>
		--	<Counterparty>HTF</Counterparty>
		--	<Counterparty>FA1</Counterparty>
  --      </ServedCounterparties>
  --    </STPDestination>
  --    <STPDestination>
  --      <Name>Traiana</Name>
  --      <ServedCounterparties>
  --        <Counterparty>LM1</Counterparty>
  --      </ServedCounterparties>
  --    </STPDestination>
  --  </STPDestinations>
  --</STP>
  
 
--new table with stp counterparties 
CREATE TABLE [dbo].[StpCounterparty](
	[StpCounterpartyId] [tinyint] NOT NULL,
	[StpCounterpartyName] [varchar](16) NOT NULL
) ON [PRIMARY]
GO

--index it
CREATE UNIQUE CLUSTERED INDEX [IX_StpCounterpartyId] ON [dbo].[StpCounterparty]
(
	[StpCounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_StpCounterpartyName] ON [dbo].[StpCounterparty]
(
	[StpCounterpartyName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

INSERT [dbo].[StpCounterparty] ([StpCounterpartyId], [StpCounterpartyName]) VALUES (0, N'CTIPB')
GO
INSERT [dbo].[StpCounterparty] ([StpCounterpartyId], [StpCounterpartyName]) VALUES (1, N'Traiana')
GO
  

--add column to tickets table + keys  
ALTER TABLE [dbo].[DealExternalExecutedTicket] ADD StpCounterpartyId TINYINT

ALTER TABLE [dbo].[DealExternalExecutedTicket]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecutedTicket_StpCounterparty] FOREIGN KEY([StpCounterpartyId])
REFERENCES [dbo].[StpCounterparty] ([StpCounterpartyId])
GO

ALTER TABLE [dbo].[DealExternalExecutedTicket] CHECK CONSTRAINT [FK_DealExternalExecutedTicket_StpCounterparty]
GO
  
--remove nulls and change null constraint  
UPDATE [dbo].[DealExternalExecutedTicket] SET [StpCounterpartyId] = 0
GO

ALTER TABLE [dbo].[DealExternalExecutedTicket] ALTER COLUMN [StpCounterpartyId] TINYINT NOT NULL

--now update SP to populate this column
ALTER PROCEDURE [dbo].[InsertNewDealTicket_SP]( 
	@InternalDealId [nvarchar](50),
	@TicketIntegratorSentOrReceivedTimeUtc [datetime2](7),
	@CounterpartySentTimeUtc [datetime2](7),
	@DealExternalExecutedTicketType [nvarchar](16),
	@StpCounterpartyName [varchar](16),
	@FixMessageRaw nvarchar(1024),
	@FixMessageParsed nvarchar(MAX)
	)
AS
BEGIN

	DECLARE @DealExternalExecutedTicketTypeId INT
	DECLARE @StpCounterpartyId TINYINT

	SELECT @DealExternalExecutedTicketTypeId = [DealExternalExecutedTicketTypeId] FROM [dbo].[DealExternalExecutedTicketType] WHERE [DealExternalExecutedTicketTypeName] = @DealExternalExecutedTicketType

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealExternalExecutedTicketType not found in DB: %s', 16, 2, @DealExternalExecutedTicketType) WITH SETERROR
	END
	
	SELECT @StpCounterpartyId = [StpCounterpartyId] FROM [dbo].[StpCounterparty] WHERE [StpCounterpartyName] = @StpCounterpartyName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'StpCounterparty not found in DB: %s', 16, 2, @StpCounterpartyName) WITH SETERROR
	END

	INSERT INTO 
		[dbo].[DealExternalExecutedTicket]
           ([DealExternalExecutedTicketId]
           ,[IntegratorSentOrReceivedTimeUtc]
		   ,[CounterpartySentTimeUtc]
           ,[DealExternalExecutedTicketTypeId]
		   ,[StpCounterpartyId]
           ,[FixMessageRaw]
		   ,[FixMessageParsed])
     VALUES
           (@InternalDealId
           ,@TicketIntegratorSentOrReceivedTimeUtc
		   ,@CounterpartySentTimeUtc
           ,@DealExternalExecutedTicketTypeId
		   ,@StpCounterpartyId
           ,@FixMessageRaw
		   ,@FixMessageParsed)
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntegratorInstance](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IntegratorInstance_Name] ON [dbo].[IntegratorInstance]
(
	[Name] ASC
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IntegratorInstance_Id] ON [dbo].[IntegratorInstance]
(
	[Id] ASC
) ON [PRIMARY]
GO

INSERT INTO [dbo].[IntegratorInstance] (Name) VALUES ('PRO_Instance_A_Trading')
INSERT INTO [dbo].[IntegratorInstance] (Name) VALUES ('PRO_Instance_G_NonTrading')


CREATE TABLE [dbo].[IntegratorInstanceSessionClaim](
	[IntegratorInstanceId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[IntegratorInstanceSessionClaim] ADD  CONSTRAINT [OneInstancePerSessionConstraint] UNIQUE NONCLUSTERED 
(
	[CounterpartyId] ASC
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[IntegratorInstanceSessionClaim]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorInstanceSessionClaim_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO

ALTER TABLE [dbo].[IntegratorInstanceSessionClaim] CHECK CONSTRAINT [FK_IntegratorInstanceSessionClaim_Counterparty]
GO

ALTER TABLE [dbo].[IntegratorInstanceSessionClaim]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorInstanceSessionClaim_IntegratorInstance] FOREIGN KEY([IntegratorInstanceId])
REFERENCES [dbo].[IntegratorInstance] ([Id])
GO

ALTER TABLE [dbo].[IntegratorInstanceSessionClaim] CHECK CONSTRAINT [FK_IntegratorInstanceSessionClaim_IntegratorInstance]
GO

INSERT INTO [dbo].[IntegratorInstanceSessionClaim] (IntegratorInstanceId, CounterpartyId) 
SELECT (SELECT [Id] FROM [dbo].[IntegratorInstance] where name = 'PRO_Instance_A_Trading'), [CounterpartyId] FROM [dbo].[Counterparty] where [CounterpartyCode] in ('BOA','BNP', 'CRS','CTI','CZB','FA1','HTA','HTF','JPM', 'MGS', 'NOM','RBS','SOC') 
GO

INSERT INTO [dbo].[IntegratorInstanceSessionClaim] (IntegratorInstanceId, CounterpartyId) 
SELECT (SELECT [Id] FROM [dbo].[IntegratorInstance] where name = 'PRO_Instance_G_NonTrading'), [CounterpartyId] FROM [dbo].[Counterparty] where [CounterpartyCode] in ('GLS','HSB', 'LM1','UBS')
GO

CREATE PROCEDURE [dbo].[GetClaimedSessionsForInstance_SP]( 
	@InstanceName varchar(50)
	)
AS
BEGIN
	SELECT 
		ctp.CounterpartyCode
	FROM 
		[dbo].[IntegratorInstanceSessionClaim] map
		INNER JOIN [dbo].[Counterparty] ctp on ctp.CounterpartyId = map.CounterpartyId
		INNER JOIN [dbo].[IntegratorInstance] instance on instance.Id = map.IntegratorInstanceId
	WHERE
		instance.Name = @InstanceName	
END
GO

GRANT EXECUTE ON [dbo].[GetClaimedSessionsForInstance_SP] TO [IntegratorServiceAccount] AS [dbo]
GO



CREATE PROCEDURE [dbo].[TryClaimSessionsForInstance_SP]( 
	@InstanceName varchar(50),
	@Counterparty char(3)
	)
AS
BEGIN
	DECLARE @InstanceId TINYINT
	DECLARE @CounterpartyId TINYINT

	SELECT @InstanceId = Id FROM [dbo].[IntegratorInstance] WHERE Name = @InstanceName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'InstanceName not found in DB: %s', 16, 2, @InstanceName) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @Counterparty

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Counterparty not found in DB: %s', 16, 2, @Counterparty) WITH SETERROR
	END

	BEGIN TRY
		INSERT INTO [dbo].[IntegratorInstanceSessionClaim] (IntegratorInstanceId, CounterpartyId)
		VALUES (@InstanceId, @CounterpartyId)
	END TRY
	BEGIN CATCH
			SELECT 
				instance.Name
			FROM 
				[dbo].[IntegratorInstanceSessionClaim] map
				INNER JOIN [dbo].[IntegratorInstance] instance on instance.Id = map.IntegratorInstanceId
			WHERE
				map.CounterpartyId = @CounterpartyId
	END CATCH
END
GO

GRANT EXECUTE ON [dbo].[TryClaimSessionsForInstance_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


CREATE PROCEDURE [dbo].[ReleaseSessionFromInstance_SP]( 
	@InstanceName varchar(50),
	@Counterparty char(3)
	)
AS
BEGIN
	DELETE FROM [dbo].[IntegratorInstanceSessionClaim]
	FROM 
		[dbo].[IntegratorInstanceSessionClaim] map
		INNER JOIN [dbo].[Counterparty] ctp on ctp.CounterpartyId = map.CounterpartyId
		INNER JOIN [dbo].[IntegratorInstance] instance on instance.Id = map.IntegratorInstanceId
	WHERE
		instance.Name = @InstanceName
		AND ctp.CounterpartyCode = @Counterparty
END
GO

GRANT EXECUTE ON [dbo].[ReleaseSessionFromInstance_SP] TO [IntegratorServiceAccount] AS [dbo]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntegratorProcessType](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[TradingAPIPort] INT NULL,
	[DiagnosticsAPIPort] INT NULL,
	[IsUniquePerInstance] BIT NOT NULL,
	[Description] [nvarchar](200) NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IntegratorProcessType_Id] ON [dbo].[IntegratorProcessType]
(
	[Id] ASC
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IntegratorProcessType_Name] ON [dbo].[IntegratorProcessType]
(
	[Name] ASC
) ON [PRIMARY]
GO

INSERT INTO [dbo].[IntegratorProcessType] (Name, TradingAPIPort, DiagnosticsAPIPort, IsUniquePerInstance, Description) VALUES ('BusinessLogic', 9000, 9100, 1, 'Main engine with all business logic and curently also all OF and majority MD sessions')
INSERT INTO [dbo].[IntegratorProcessType] (Name, TradingAPIPort, DiagnosticsAPIPort, IsUniquePerInstance, Description) VALUES ('HotspotMarketData', 9001, 9101, 1, 'Engine connecting to Hotspot Itch, building and maintaining order books and providing data')
INSERT INTO [dbo].[IntegratorProcessType] (Name, TradingAPIPort, DiagnosticsAPIPort, IsUniquePerInstance, Description) VALUES ('DataCollection', NULL, NULL, 1, 'DataCollection process, currently not having any meaningfull API endpoint')


CREATE TABLE [dbo].[IntegratorProcessState](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](25) NOT NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IntegratorProcessState_Id] ON [dbo].[IntegratorProcessState]
(
	[Id] ASC
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IntegratorProcessState_Name] ON [dbo].[IntegratorProcessState]
(
	[Name] ASC
) ON [PRIMARY]
GO

INSERT INTO [dbo].[IntegratorProcessState] (Name) VALUES ('Inactive')
INSERT INTO [dbo].[IntegratorProcessState] (Name) VALUES ('Starting')
INSERT INTO [dbo].[IntegratorProcessState] (Name) VALUES ('Running')
INSERT INTO [dbo].[IntegratorProcessState] (Name) VALUES ('ShuttingDown')
INSERT INTO [dbo].[IntegratorProcessState] (Name) VALUES ('Unknown')


CREATE FUNCTION [dbo].[GetBinaryIPv4FromString](@ip AS VARCHAR(15)) RETURNS BINARY(4)
AS
BEGIN
    DECLARE @bin AS BINARY(4)

    SELECT @bin = CAST( CAST( PARSENAME( @ip, 4 ) AS INTEGER) AS BINARY(1))
                + CAST( CAST( PARSENAME( @ip, 3 ) AS INTEGER) AS BINARY(1))
                + CAST( CAST( PARSENAME( @ip, 2 ) AS INTEGER) AS BINARY(1))
                + CAST( CAST( PARSENAME( @ip, 1 ) AS INTEGER) AS BINARY(1))

    RETURN @bin
END
GO

CREATE FUNCTION [dbo].[GetStringIPv4FromBinary](@ip AS BINARY(4)) RETURNS VARCHAR(15)
AS
BEGIN
    DECLARE @str AS VARCHAR(15) 

    SELECT @str = CAST( CAST( SUBSTRING( @ip, 1, 1) AS INTEGER) AS VARCHAR(3) ) + '.'
                + CAST( CAST( SUBSTRING( @ip, 2, 1) AS INTEGER) AS VARCHAR(3) ) + '.'
                + CAST( CAST( SUBSTRING( @ip, 3, 1) AS INTEGER) AS VARCHAR(3) ) + '.'
                + CAST( CAST( SUBSTRING( @ip, 4, 1) AS INTEGER) AS VARCHAR(3) );

    RETURN @str
END
GO

CREATE TABLE [dbo].[IntegratorEndpoint](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[PrivateIPBinary] BINARY(4) NOT NULL,
	[PublicIPBinary] BINARY(4) NOT NULL,
	[Name] VARCHAR(50),
	[PrivateIPString]  AS ([dbo].[GetStringIPv4FromBinary]([PrivateIPBinary])),
	[PublicIPString]  AS ([dbo].[GetStringIPv4FromBinary]([PublicIPBinary]))
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IntegratorEndpoint_Id] ON [dbo].[IntegratorEndpoint]
(
	[Id] ASC
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IntegratorEndpoint_PrivateIP] ON [dbo].[IntegratorEndpoint]
(
	[PrivateIPBinary] ASC
) ON [PRIMARY]
GO

INSERT INTO [dbo].[IntegratorEndpoint] (PrivateIPBinary, PublicIPBinary, Name) VALUES ([dbo].[GetBinaryIPv4FromString]('192.168.75.67'), [dbo].[GetBinaryIPv4FromString]('37.46.6.117'), 'INT01.kgtinv.com')
INSERT INTO [dbo].[IntegratorEndpoint] (PrivateIPBinary, PublicIPBinary, Name) VALUES ([dbo].[GetBinaryIPv4FromString]('192.168.75.68'), [dbo].[GetBinaryIPv4FromString]('37.46.6.118'), 'INT02.kgtinv.com')
INSERT INTO [dbo].[IntegratorEndpoint] (PrivateIPBinary, PublicIPBinary, Name) VALUES ([dbo].[GetBinaryIPv4FromString]('192.168.75.69'), [dbo].[GetBinaryIPv4FromString]('37.46.6.119'), 'INT03.kgtinv.com')

INSERT INTO [dbo].[IntegratorEndpoint] (PrivateIPBinary, PublicIPBinary, Name) VALUES ([dbo].[GetBinaryIPv4FromString]('192.168.75.61'), [dbo].[GetBinaryIPv4FromString]('37.46.6.111'), 'DEV01.kgtinv.com')
INSERT INTO [dbo].[IntegratorEndpoint] (PrivateIPBinary, PublicIPBinary, Name) VALUES ([dbo].[GetBinaryIPv4FromString]('192.168.75.64'), [dbo].[GetBinaryIPv4FromString]('37.46.6.114'), 'DEV02.kgtinv.com')
INSERT INTO [dbo].[IntegratorEndpoint] (PrivateIPBinary, PublicIPBinary, Name) VALUES ([dbo].[GetBinaryIPv4FromString]('192.168.75.53'), [dbo].[GetBinaryIPv4FromString]('37.46.6.103'), 'DEV03.kgtinv.com')

INSERT INTO [dbo].[IntegratorEndpoint] (PrivateIPBinary, PublicIPBinary, Name) VALUES ([dbo].[GetBinaryIPv4FromString]('192.168.0.100'), [dbo].[GetBinaryIPv4FromString]('89.176.148.118'), 'ip-89-176-148-118.net.upcbroadband.cz')


CREATE TABLE [dbo].[IntegratorProcess](
	[Id] INT IDENTITY(1,1) NOT NULL,
	[IntegratorInstanceId] TINYINT NOT NULL,
	[IntegratorProcessTypeId] TINYINT NOT NULL,
	[IntegratorProcessStateId] TINYINT NOT NULL,
	[IntegratorEndpointId] TINYINT NOT NULL,
	[LastHBTimeUtc] datetime NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[IntegratorProcess]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorProcess_IntegratorInstance] FOREIGN KEY([IntegratorInstanceId])
REFERENCES [dbo].[IntegratorInstance] ([Id])
GO

ALTER TABLE [dbo].[IntegratorProcess] CHECK CONSTRAINT [FK_IntegratorProcess_IntegratorInstance]
GO

ALTER TABLE [dbo].[IntegratorProcess]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorProcessType_IntegratorInstance] FOREIGN KEY([IntegratorProcessTypeId])
REFERENCES [dbo].[IntegratorProcessType] ([Id])
GO

ALTER TABLE [dbo].[IntegratorProcess] CHECK CONSTRAINT [FK_IntegratorProcessType_IntegratorInstance]
GO

ALTER TABLE [dbo].[IntegratorProcess]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorProcessState_IntegratorInstance] FOREIGN KEY([IntegratorProcessStateId])
REFERENCES [dbo].[IntegratorProcessState] ([Id])
GO

ALTER TABLE [dbo].[IntegratorProcess] CHECK CONSTRAINT [FK_IntegratorProcessState_IntegratorInstance]
GO

ALTER TABLE [dbo].[IntegratorProcess]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEndpoint_IntegratorInstance] FOREIGN KEY([IntegratorEndpointId])
REFERENCES [dbo].[IntegratorEndpoint] ([Id])
GO

ALTER TABLE [dbo].[IntegratorProcess] CHECK CONSTRAINT [FK_IntegratorEndpoint_IntegratorInstance]
GO

--We might need multiple instances in different states (e.g. ShuttingDown and Starting)
--ALTER TABLE [dbo].[IntegratorProcess] ADD  CONSTRAINT [OneProcessPerInstanceConstraint] UNIQUE NONCLUSTERED 
--(
--	[IntegratorInstanceId] ASC,
--	[IntegratorProcessTypeId] ASC
--) ON [PRIMARY]
--GO


CREATE PROCEDURE [dbo].[IsKnownPrivateIP_SP]( 
	@PrivateIPAsString varchar(15)
	)
AS
BEGIN
	SELECT
		1
	FROM
		[dbo].[IntegratorEndpoint]
	WHERE
		[PrivateIPBinary] = [dbo].[GetBinaryIPv4FromString](@PrivateIPAsString)
END
GO

GRANT EXECUTE ON [dbo].[IsKnownPrivateIP_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


CREATE PROCEDURE [dbo].[GetIntegratorProcessDetails_SP](
	@InstanceName varchar(50),
	@ProcessName varchar(50)
	)
AS
BEGIN
	
	DECLARE @InstanceId TINYINT
	DECLARE @ProcessTypeId TINYINT

	SELECT @InstanceId = Id FROM [dbo].[IntegratorInstance] WHERE Name = @InstanceName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'InstanceName not found in DB: %s', 16, 2, @InstanceName) WITH SETERROR
	END

	SELECT @ProcessTypeId = [Id] FROM [dbo].[IntegratorProcessType] WHERE [Name] = @ProcessName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ProcessType not found in DB: %s', 16, 2, @ProcessName) WITH SETERROR
	END
	
	SELECT
		process.[Id] AS ProcessIdentifier,
		[PrivateIPString],
		[PublicIPString],
		endpoint.[Name] AS Hostname,
		[TradingAPIPort],
		[DiagnosticsAPIPort],
		state.[Name] AS State
	FROM
		[dbo].[IntegratorProcess] process
		INNER JOIN [dbo].[IntegratorEndpoint] endpoint ON process.IntegratorEndpointId = endpoint.Id
		INNER JOIN [dbo].[IntegratorProcessType] type ON process.IntegratorProcessTypeId = type.Id
		INNER JOIN [dbo].[IntegratorProcessState] state ON process.IntegratorProcessStateId = state.Id
	WHERE
		process.IntegratorInstanceId = @InstanceId
		AND process.IntegratorProcessTypeId = @ProcessTypeId
END
GO

GRANT EXECUTE ON [dbo].[GetIntegratorProcessDetails_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE PROCEDURE [dbo].[RegisterIntegratorProcess_SP](
	@InstanceName varchar(50),
	@ProcessName varchar(50),
	@PrivateIPAsString varchar(15)
	)
AS
BEGIN
	
	DECLARE @InstanceId TINYINT
	DECLARE @ProcessTypeId TINYINT
	DECLARE @EndpointId TINYINT

	SELECT @InstanceId = Id FROM [dbo].[IntegratorInstance] WHERE Name = @InstanceName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'InstanceName not found in DB: %s', 16, 2, @InstanceName) WITH SETERROR
	END

	SELECT @ProcessTypeId = [Id] FROM [dbo].[IntegratorProcessType] WHERE [Name] = @ProcessName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ProcessType not found in DB: %s', 16, 2, @ProcessName) WITH SETERROR
	END
	
	SELECT @EndpointId = [Id] FROM [dbo].[IntegratorEndpoint] WHERE [PrivateIPBinary] = [dbo].[GetBinaryIPv4FromString](@PrivateIPAsString)

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Endpoint not found in DB: %s', 16, 2, @PrivateIPAsString) WITH SETERROR
	END
	
	INSERT INTO [dbo].[IntegratorProcess] (IntegratorInstanceId, IntegratorProcessTypeId, IntegratorProcessStateId, IntegratorEndpointId, LastHBTimeUtc)
	VALUES (@InstanceId, @ProcessTypeId, (SELECT [Id] FROM [dbo].[IntegratorProcessState] WHERE Name = 'Starting'), @EndpointId, GetUtcDate())
	
	DECLARE @ProcessId INT = SCOPE_IDENTITY()
	
	exec [dbo].[GetIntegratorProcessDetails_SP] @InstanceName = @InstanceName, @ProcessName = @ProcessName
	
	return @ProcessId
END
GO

GRANT EXECUTE ON [dbo].[RegisterIntegratorProcess_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


CREATE PROCEDURE [dbo].[UpdateIntegratorProcessState_SP](
	@ProcessIdentifier INT,
	@NewProcessState varchar(25)
	)
AS
BEGIN
	DECLARE @StateId TINYINT

	SELECT @StateId = Id FROM [dbo].[IntegratorProcessState] WHERE Name = @NewProcessState

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ProcessState not found in DB: %s', 16, 2, @NewProcessState) WITH SETERROR
	END
	
	UPDATE [dbo].[IntegratorProcess]
	SET 
		[IntegratorProcessStateId] = @StateId,
		[LastHBTimeUtc] = GetUtcDate()
	WHERE [Id] = @ProcessIdentifier
END
GO

GRANT EXECUTE ON [dbo].[UpdateIntegratorProcessState_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


CREATE PROCEDURE [dbo].[UpdateIntegratorProcessHBTime_SP](
	@ProcessIdentifier INT
	)
AS
BEGIN
	UPDATE [dbo].[IntegratorProcess]
	SET [LastHBTimeUtc] = GetUtcDate()
	WHERE [Id] = @ProcessIdentifier
END
GO

GRANT EXECUTE ON [dbo].[UpdateIntegratorProcessHBTime_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

INSERT INTO [dbo].[CertificateFile]
           ([FileName], [FileContent])
     SELECT N'TraianaClientCertificate_PRD.pfx', BulkColumn FROM OPENROWSET(BULK
     N'C:\Data\Temp\CertificatesToInsert\TraianaClientCertificate_PRD.pfx', SINGLE_BLOB) AS Document
GO

INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'Traiana', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SSLEnable=Y
SSLCertificate=TraianaClientCertificate_PRD.pfx
SSLCertificatePassword=bGB2yIPB
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N


SenderCompID=KGT

[SESSION]
DataDictionary=FIX44_CTI.xml
BeginString=FIX.4.4
TargetCompID=TRAIANA
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=443
SocketConnectHost=fix-uat.traiana.com', GETUTCDATE())
GO

DECLARE @TraianaFixSettingId INT

SELECT
	@TraianaFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'Traiana'


INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
VALUES (1, @TraianaFixSettingId)
INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
VALUES (2, @TraianaFixSettingId)


INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'Traiana', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SSLEnable=Y
SSLCertificate=TraianaClientCertificate_PRD.pfx
SSLCertificatePassword=bGB2yIPB
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N


SenderCompID=KGT1

[SESSION]
DataDictionary=FIX44_CTI.xml
BeginString=FIX.4.4
TargetCompID=TRAIANA
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=443
SocketConnectHost=fix-uat.traiana.com', GETUTCDATE())
GO