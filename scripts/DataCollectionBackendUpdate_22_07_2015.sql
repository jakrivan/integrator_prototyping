
SET IDENTITY_INSERT [dbo].[LiqProviderStream] ON 
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (109, N'LGA', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (110, N'LGC', 22)
GO
SET IDENTITY_INSERT [dbo].[LiqProviderStream] OFF
GO


INSERT INTO [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode])
SELECT 
	integratorCpt.[CounterpartyId] AS IntegratorDBCounterpartyId,
	dcCpt.[LiqProviderStreamId] AS DCDBCounterpartyId,
    dcCpt.[LiqProviderStreamName] AS CounterpartyCode
FROM
	[dbo].[LiqProviderStream] dcCpt
	INNER JOIN [Integrator].[dbo].[Counterparty] integratorCpt ON dcCpt.LiqProviderStreamName = integratorCpt.CounterpartyCode
WHERE
	dcCpt.[LiqProviderStreamId] NOT IN (SELECT DCDBCounterpartyId FROM [dbo].[_Internal_CounterpartyIdsMapping])