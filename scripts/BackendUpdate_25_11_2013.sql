
/****** Object:  Table [dbo].[PegType]    Script Date: 25. 11. 2013 17:41:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PegType](
	[PegTypeId] [BIT] NOT NULL,
	[PegTypeName] [varchar](16) NOT NULL
) ON [PRIMARY]
GO

/****** Object:  Index [IX_PegType]    Script Date: 25. 11. 2013 17:41:51 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_PegType] ON [dbo].[PegType]
(
	[PegTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/****** Object:  Index [IX_PegTypeName]    Script Date: 25. 11. 2013 17:41:51 ******/
ALTER TABLE [dbo].[PegType] ADD  CONSTRAINT [IX_PegTypeName] UNIQUE NONCLUSTERED 
(
	[PegTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


INSERT [dbo].[PegType] ([PegTypeId], [PegTypeName]) VALUES (0, N'MarketPeg')
GO
INSERT [dbo].[PegType] ([PegTypeId], [PegTypeName]) VALUES (1, N'PrimaryPeg')
GO

ALTER TABLE [dbo].[OrderExternal] ADD PegTypeId BIT NULL
ALTER TABLE [dbo].[OrderExternal] ADD PegDifferenceBasePol DECIMAL(18, 9) NULL
GO

ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_PegType] FOREIGN KEY([PegTypeId])
REFERENCES [dbo].[PegType] ([PegTypeId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_PegType]
GO



/****** Object:  StoredProcedure [dbo].[InsertNewOrderExternal_SP]    Script Date: 25. 11. 2013 13:53:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Stored procedure
ALTER PROCEDURE [dbo].[InsertNewOrderExternal_SP]( 
	@ExternalOrderSentUtc [datetime2](7),
	@InternalOrderId [nvarchar](45),
	@InternalClientOrderId [nvarchar](32),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@QuoteReceivedToOrderSentInternalLatency [time](7),
	@FixMessageSentRaw nvarchar(1024),
	@FixMessageSentParsed nvarchar(MAX),
	@OrderTypeName varchar(16),
	@OrderTimeInForceName varchar(16),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,0),
	@SourceIntegratorPriceIdentity uniqueidentifier = NULL,
	@SourceIntegratorPriceTimeUtc [datetime2](7) = NULL,
	@PegTypeName varchar(16) = NULL,
	@PegDifferenceBasePol decimal(18,9) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @OrderTypeId TINYINT
	DECLARE @OrderTimeInForceId TINYINT
	DECLARE @PegTypeId BIT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	SELECT @OrderTypeId = [OrderTypeId] FROM [dbo].[OrderType] WHERE [OrderTypeName] = @OrderTypeName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderType not found in DB: %s', 16, 2, @OrderTypeName) WITH SETERROR
	END
	
	SELECT @OrderTimeInForceId = [OrderTimeInForceId] FROM [dbo].[OrderTimeInForce] WHERE [OrderTimeInForceName] = @OrderTimeInForceName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderTimeInForce not found in DB: %s', 16, 2, @OrderTimeInForceName) WITH SETERROR
	END
	
	IF @OrderTypeName = 'Pegged'
	BEGIN
		SELECT @PegTypeId = [PegTypeId] FROM [dbo].[PegType] WHERE [PegTypeName] = @PegTypeName
		
		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'PegType not found in DB: %s', 16, 2, @PegTypeName) WITH SETERROR
		END
		
		IF @PegDifferenceBasePol IS NULL
		BEGIN
			RAISERROR (N'PegDifferenceBasePol was not specified for pegged order - but it is compulsory', 16, 2) WITH SETERROR
		END
	END

	INSERT INTO 
		[dbo].[OrderExternal]
           ([IntegratorSentExternalOrderUtc]
           ,[IntegratorExternalOrderPrivateId]
		   ,[ExternalOrderLabelForCounterparty]
           ,[CounterpartyId]
           ,[SymbolId]
		   ,[DealDirectionId]
           ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[FixMessageSentRaw]
		   ,[FixMessageSentParsed]
		   ,[OrderTypeId]
		   ,[OrderTimeInForceId]
		   ,[RequestedPrice]
		   ,[RequestedAmountBaseAbs]
		   ,[SourceIntegratorPriceIdentity]
		   ,[SourceIntegratorPriceTimeUtc]
		   ,[PegTypeId]
		   ,[PegDifferenceBasePol])
     VALUES
           (@ExternalOrderSentUtc
           ,@InternalOrderId
		   ,@InternalClientOrderId
           ,@CounterpartyId
           ,@SymbolId
		   ,@DealDirectionId
           ,@QuoteReceivedToOrderSentInternalLatency
		   ,@FixMessageSentRaw
		   ,@FixMessageSentParsed
		   ,@OrderTypeId
		   ,@OrderTimeInForceId
		   ,@RequestedPrice
		   ,@RequestedAmountBaseAbs
		   ,@SourceIntegratorPriceIdentity
		   ,@SourceIntegratorPriceTimeUtc
		   ,@PegTypeId
		   ,@PegDifferenceBasePol)
END
GO





SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetIgnoredOrders_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN
	
	SELECT 
		ord.[IntegratorSentExternalOrderUtc] AS IntegratorSentExternalOrderUtc
		,ord.ExternalOrderLabelForCounterparty AS ExternalOrderLabelForCounterparty
		,ord.RequestedAmountBaseAbs AS RequestedAmountBaseAbs
		,ctp.CounterpartyName AS CounterpartyName
		,symbol.Name AS Symbol
		,direction.Name AS Direction
	FROM
		[dbo].[OrderExternal] ord
		LEFT JOIN [dbo].[DealExternalExecuted] deal on deal.[IntegratorExternalOrderPrivateId] = ord.[IntegratorExternalOrderPrivateId]
		LEFT JOIN [dbo].[DealExternalRejected] reject on reject.[IntegratorExternalOrderPrivateId] = ord.[IntegratorExternalOrderPrivateId]
		LEFT JOIN [dbo].[OrderExternalCancelResult] cancel on cancel.[IntegratorExternalOrderPrivateId] = ord.[IntegratorExternalOrderPrivateId]
		INNER JOIN [dbo].[Counterparty] ctp on ctp.CounterpartyId = ord.CounterpartyId
		INNER JOIN [dbo].Symbol symbol on symbol.Id = ord.SymbolId
		INNER JOIN [dbo].[DealDirection] direction on direction.Id = ord.DealDirectionId
	WHERE 
		ord.[IntegratorSentExternalOrderUtc] BETWEEN @StartTime AND @EndTime
		AND ord.[IntegratorSentExternalOrderUtc] < DATEADD(SECOND, -120, GETUTCDATE())
		AND deal.IntegratorExternalOrderPrivateId IS NULL AND reject.IntegratorExternalOrderPrivateId IS NULL AND cancel.[IntegratorExternalOrderPrivateId] IS NULL
		
	
	--
	-- Here goes version with Index hints, however resulting plan is even worse
	--
	
	--SELECT 
		--[IntegratorSentExternalOrderUtc]
	--FROM [dbo].[OrderExternal] ord
	--LEFT JOIN [dbo].[DealExternalExecuted] deal WITH(INDEX(IX_DealExternalExecuted_OrderId)) on deal.[IntegratorExternalOrderPrivateId] = ord.[IntegratorExternalOrderPrivateId]
	--LEFT JOIN [dbo].[DealExternalRejected] reject WITH(INDEX(IX_DealExternalRejected_OrderId)) on reject.[IntegratorExternalOrderPrivateId] = ord.[IntegratorExternalOrderPrivateId]
	--LEFT JOIN [dbo].[OrderExternalCancelResult] cancel WITH(INDEX(IX_OrderExternalCancelResult_OrderId)) on cancel.[IntegratorExternalOrderPrivateId] = ord.[IntegratorExternalOrderPrivateId]
	--WHERE ord.[IntegratorSentExternalOrderUtc] BETWEEN @StartTime AND @EndTime
	--AND deal.IntegratorExternalOrderPrivateId IS NULL AND reject.IntegratorExternalOrderPrivateId IS NULL AND cancel.[IntegratorExternalOrderPrivateId] IS NULL

END
GO

GRANT EXECUTE ON [dbo].[GetIgnoredOrders_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetNopsPolPerCounterpartyAndCurrency_SP] (
	@Day Date
	)
AS
BEGIN

	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol)
	SELECT
		symbol.[Id] AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
	GROUP BY
		symbol.[Id],
		deal.CounterpartyId
		

		SELECT
			ctp.CounterpartyCode AS Counterparty,
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				basePos.CounterpartyId,
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT 
				termPos.CounterpartyId,
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
			INNER JOIN [dbo].[Counterparty] ctp ON ctp.CounterpartyId = CCYPositions.CounterpartyId
		GROUP BY
			ctp.CounterpartyCode
			,CCYPositions.CCY
		ORDER BY
			ctp.CounterpartyCode
			,CCYPositions.CCY

END
GO

GRANT EXECUTE ON [dbo].[GetNopsPolPerCounterpartyAndCurrency_SP] TO [IntegratorServiceAccount] AS [dbo]
GO