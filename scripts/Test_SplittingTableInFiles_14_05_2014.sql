

--C:sqldata\sysdba

--Alter Database SplittingTest Add FileGroup SplittingTest_MDFilegroup
--GO

--Alter Database SplittingTest Add File
--(
--Name=MDFilegroup_File01,
--FileName='C:\sqldata\sysdb\MDFilegroup_File01.ndf',
--Size=50MB,
--FileGrowth=10%
--) To Filegroup SplittingTest_MDFilegroup
--GO



--CREATE PARTITION FUNCTION MarketDataSymbolPartitioningFunction (tinyint)
--AS RANGE LEFT FOR VALUES (3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 22, 43, 45, 46, 47, 48, 49, 50, 51, 56, 57, 58, 59, 60, 61, 62, 63, 65, 66, 67, 68, 70, 72, 73, 74, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 95, 96, 97, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 113, 117, 128, 129, 133, 142, 143, 144, 145, 146, 147, 148, 149, 150)
--GO


--CREATE PARTITION SCHEME MarketDataSymbolPartitionScheme_AnotherFileGroup
--AS PARTITION MarketDataSymbolPartitioningFunction ALL TO ([SplittingTest_MDFilegroup]) 
--GO

--CREATE TABLE MDPartitioned(
--	[RecordID] [bigint] IDENTITY(1,1) NOT NULL,
--	[FXPairId] [tinyint] NOT NULL,
--	[CId] [nvarchar](max) NULL
--) ON [MarketDataSymbolPartitionScheme_AnotherFileGroup]([FXPairId])



--CREATE CLUSTERED INDEX IX_PairRecordId ON MDPartitioned
--(
--	[FXPairId] ASC,
--	[RecordID] ASC
--)
--ON MarketDataSymbolPartitionScheme_AnotherFileGroup(FXPairId)
--GO


--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (3, 'bah1a')
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (4, 'bah1s') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (5, 'bah1d') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (3, 'bah1f') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (4, 'bah1f') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (3, 'bah1v') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (8, 'bah1sd') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (3, 'bahf1')
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (8, 'badfh1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (3, 'baweh1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (5, 'baaeh1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (5, 'bath1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (11, 'bahfsd1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (3, 'bagfh1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (3, 'bacdh1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (3, 'ba h1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (3, 'bahfd1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (3, 'bahwu1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (11, 'baiuh1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (3, 'ba445h1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (3, 'b65ah1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (150, 'ba5h1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (3, 'ba6h1') 
--GO
--INSERT INTO [dbo].[MDPartitioned] ([FXPairId], [CId]) VALUES (150, 'b56ah1') 
--GO



--NOW THE SPLIT:

-- -------------------------------------------------------------------------
--                                                                        --
--     Option 1: Switch partition by partition to staging table and back  --
--                                                                        --
-- -------------------------------------------------------------------------

--0) Rename the source table
--0) Disable FKs

DECLARE @SymbolToMove tinyint = 3

--1) create an identical staging table on the source filegroup

CREATE TABLE dbo.MDPartitioned_Staging_3(
	[RecordID] [bigint] IDENTITY(1,1) NOT NULL,
	[FXPairId] [tinyint] NOT NULL,
	[CId] [nvarchar](max) NULL
) ON [SplittingTest_MDFilegroup]



CREATE CLUSTERED INDEX MDPartitioned_Staging_3_Idx ON dbo.MDPartitioned_Staging_3
(
	[FXPairId] ASC,
	[RecordID] ASC
) ON [SplittingTest_MDFilegroup]
GO

--2) switch the source partition into the staging table

ALTER TABLE dbo.MDPartitioned
SWITCH PARTITION $PARTITION.MarketDataSymbolPartitioningFunction(@SymbolToMove)
TO dbo.MDPartitioned_Staging_3

--3) merge the partition to be moved (now empty)
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
MERGE RANGE (@SymbolToMove);

--4) Create the target file and filegroup
Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_3
GO

Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_3_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_3_File01.ndf',
Size=50MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_3
GO

--5) alter the partition scheme NEXT USED to specify the target filegroup
ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_AnotherFileGroup]
NEXT USED SplittingTest_MDSplittedFilegroup_3;

--6) split the partition function to create a new empty partition
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
SPLIT RANGE (@SymbolToMove);

--7) Move the staging table data to the target filegroup. (create indexdrop existing)
CREATE CLUSTERED INDEX MDPartitioned_Staging_3_Idx ON dbo.MDPartitioned_Staging_3
(
	[FXPairId] ASC,
	[RecordID] ASC
)
WITH (DROP_EXISTING = ON)
ON MarketDataSymbolPartitionScheme_AnotherFileGroup([FXPairId]);
      --ON SplittingTest_MDSplittedFilegroup_3 --

--8) switch the staging table into the empty target partition
ALTER TABLE dbo.MDPartitioned_Staging_3
      SWITCH PARTITION $PARTITION.MarketDataSymbolPartitioningFunction(@SymbolToMove)
      TO dbo.MDPartitioned PARTITION $PARTITION.MarketDataSymbolPartitioningFunction(@SymbolToMove);
	  
	  
-- -------------------------------------------------------------------------
--                                                                        --
--                         END OF Option 1                                --
--                                                                        --
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
--                                                                        --
--     Option 2: Recreate the table all at once                           --
--                                                                        --
-- -------------------------------------------------------------------------

-- 0) C# code for generating script:



--            string sqlTemplateString = @"

--Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_{0}
--GO
--Alter Database SplittingTest Add File
--(
--Name=MDSplittedFilegroup_{0}_File01,
--FileName='C:\sqldata\sysdb\MDFilegroup_{0}_File01.ndf',
--Size=1MB,
--FileGrowth=10%
--) To Filegroup SplittingTest_MDSplittedFilegroup_{0}
--GO ";

--            string sqlCmd = null;

--            foreach (Symbol symbol in Symbol.Values)
--            {
--                sqlCmd += string.Format(sqlTemplateString, symbol.ShortName);
--            }

--            string partitionschemeTemplate =
--                @"CREATE PARTITION SCHEME MarketDataSymbolPartitionScheme_IndividualFileGroups
--AS PARTITION MarketDataSymbolPartitioningFunction TO ({0});
--GO";

--            string partitionSchemeCmd = string.Format(partitionschemeTemplate,
--                                                      string.Join(", ",
--                                                                  Symbol.Values.Select(
--                                                                      symbol =>
--                                                                      string.Format(
--                                                                          "SplittingTest_MDSplittedFilegroup_{0}",
--                                                                          symbol.ShortName)))); 

-- 1) Create correct partition function - but probably not this in prod:

--ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction (tinyint)
--AS RANGE LEFT FOR VALUES (3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 22, 43, 45, 46, 47, 48, 49, 50, 51, 56, 57, 58, 59, 60, 61, 62, 63, 65, 66, 67, 68, 70, 72, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 95, 96, 97, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 113, 117, 128, 129, 133, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158)
--GO




--1) Create filegroups:

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_AUDCAD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_AUDCAD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_AUDCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_AUDCAD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_AUDCHF
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_AUDCHF_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_AUDCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_AUDCHF
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_AUDDKK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_AUDDKK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_AUDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_AUDDKK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_AUDHKD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_AUDHKD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_AUDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_AUDHKD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_AUDJPY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_AUDJPY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_AUDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_AUDJPY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_AUDNOK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_AUDNOK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_AUDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_AUDNOK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_AUDNZD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_AUDNZD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_AUDNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_AUDNZD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_AUDSEK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_AUDSEK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_AUDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_AUDSEK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_AUDSGD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_AUDSGD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_AUDSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_AUDSGD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_AUDUSD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_AUDUSD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_AUDUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_AUDUSD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_CADCHF
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_CADCHF_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_CADCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_CADCHF
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_CADDKK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_CADDKK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_CADDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_CADDKK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_CADJPY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_CADJPY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_CADJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_CADJPY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_CADNOK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_CADNOK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_CADNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_CADNOK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_CADNZD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_CADNZD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_CADNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_CADNZD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_CADSEK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_CADSEK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_CADSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_CADSEK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_CHFDKK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_CHFDKK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_CHFDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_CHFDKK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_CHFJPY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_CHFJPY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_CHFJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_CHFJPY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_CHFNOK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_CHFNOK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_CHFNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_CHFNOK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_CHFSEK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_CHFSEK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_CHFSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_CHFSEK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_DKKJPY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_DKKJPY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_DKKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_DKKJPY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_DKKNOK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_DKKNOK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_DKKNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_DKKNOK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_DKKSEK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_DKKSEK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_DKKSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_DKKSEK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURAUD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURAUD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURAUD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURCAD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURCAD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURCAD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURCHF
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURCHF_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURCHF
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURCNH
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURCNH_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURCNH_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURCNH
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURCZK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURCZK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURCZK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURDKK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURDKK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURDKK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURGBP
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURGBP_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURGBP_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURGBP
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURHKD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURHKD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURHKD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURHUF
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURHUF_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURHUF
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURJPY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURJPY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURJPY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURMXN
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURMXN_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURMXN
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURNOK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURNOK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURNOK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURNZD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURNZD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURNZD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURPLN
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURPLN_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURPLN
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURRUB
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURRUB_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURRUB_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURRUB
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURSEK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURSEK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURSEK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURTRY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURTRY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURTRY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURUSD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURUSD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURUSD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURZAR
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURZAR_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURZAR
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPAUD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPAUD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPAUD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPCAD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPCAD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPCAD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPCHF
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPCHF_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPCHF
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPCZK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPCZK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPCZK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPDKK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPDKK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPDKK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPHUF
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPHUF_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPHUF
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPJPY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPJPY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPJPY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPNOK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPNOK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPNOK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPNZD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPNZD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPNZD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPPLN
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPPLN_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPPLN
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPSEK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPSEK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPSEK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPUSD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPUSD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPUSD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_HKDJPY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_HKDJPY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_HKDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_HKDJPY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_MXNJPY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_MXNJPY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_MXNJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_MXNJPY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_NOKJPY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_NOKJPY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_NOKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_NOKJPY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_NOKSEK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_NOKSEK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_NOKSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_NOKSEK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_NZDCAD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_NZDCAD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_NZDCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_NZDCAD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_NZDCHF
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_NZDCHF_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_NZDCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_NZDCHF
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_NZDDKK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_NZDDKK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_NZDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_NZDDKK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_NZDJPY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_NZDJPY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_NZDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_NZDJPY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_NZDNOK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_NZDNOK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_NZDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_NZDNOK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_NZDSEK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_NZDSEK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_NZDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_NZDSEK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_NZDSGD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_NZDSGD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_NZDSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_NZDSGD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_NZDUSD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_NZDUSD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_NZDUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_NZDUSD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_SGDJPY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_SGDJPY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_SGDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_SGDJPY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDCAD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDCAD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDCAD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDCHF
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDCHF_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDCHF
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDCNH
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDCNH_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDCNH_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDCNH
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDCZK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDCZK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDCZK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDDKK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDDKK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDDKK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDHKD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDHKD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDHKD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDHUF
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDHUF_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDHUF
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDILS
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDILS_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDILS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDILS
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDJPY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDJPY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDJPY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDMXN
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDMXN_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDMXN
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDNOK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDNOK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDNOK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDPLN
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDPLN_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDPLN
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDRUB
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDRUB_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDRUB_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDRUB
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDSEK
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDSEK_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDSEK
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDSGD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDSGD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDSGD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDTRY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDTRY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDTRY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDZAR
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDZAR_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDZAR
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_ZARJPY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_ZARJPY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_ZARJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_ZARJPY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_EURSGD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_EURSGD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_EURSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_EURSGD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPHKD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPHKD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPHKD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPMXN
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPMXN_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPMXN
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPSGD
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPSGD_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPSGD
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPTRY
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPTRY_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPTRY
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_GBPZAR
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_GBPZAR_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_GBPZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_GBPZAR
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_CHFMXN
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_CHFMXN_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_CHFMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_CHFMXN
GO 

Alter Database SplittingTest Add FileGroup SplittingTest_MDSplittedFilegroup_USDINR
GO
Alter Database SplittingTest Add File
(
Name=MDSplittedFilegroup_USDINR_File01,
FileName='C:\sqldata\sysdb\MDFilegroup_USDINR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup SplittingTest_MDSplittedFilegroup_USDINR
GO 

-- 2) Create new partition schema
-- BE CAREFUL to have partitions in correct order!

--CMD fro creating this cmd:
--  DECLARE @cmd NVARCHAR(MAX)  = 'CREATE PARTITION SCHEME MarketDataSymbolPartitionScheme_IndividualFileGroups
--AS PARTITION MarketDataSymbolPartitioningFunction TO ('
--  
--  SELECT @cmd = @cmd + 'SplittingTest_MDSplittedFilegroup_' + FxpCode + ', '
--  FROM [FXtickDB].[dbo].[FxPair] where FxpCode IN ('AUDCAD', 'AUDCHF', 'AUDDKK', 'AUDHKD', 'AUDJPY', 'AUDNOK', 'AUDNZD', 'AUDSEK', 'AUDSGD', 'AUDUSD', 'CADCHF', 'CADDKK', 'CADJPY', 'CADNOK', 'CADNZD', 'CADSEK', 'CHFDKK', 'CHFJPY', 'CHFNOK', 'CHFSEK', 'DKKJPY', 'DKKNOK', 'DKKSEK', 'EURAUD', 'EURCAD', 'EURCHF', 'EURCNH', 'EURCZK', 'EURDKK', 'EURGBP', 'EURHKD', 'EURHUF', 'EURJPY', 'EURMXN', 'EURNOK', 'EURNZD', 'EURPLN', 'EURRUB', 'EURSEK', 'EURTRY', 'EURUSD', 'EURZAR', 'GBPAUD', 'GBPCAD', 'GBPCHF', 'GBPCZK', 'GBPDKK', 'GBPHUF', 'GBPJPY', 'GBPNOK', 'GBPNZD', 'GBPPLN', 'GBPSEK', 'GBPUSD', 'HKDJPY', 'MXNJPY', 'NOKJPY', 'NOKSEK', 'NZDCAD', 'NZDCHF', 'NZDDKK', 'NZDJPY', 'NZDNOK', 'NZDSEK', 'NZDSGD', 'NZDUSD', 'SGDJPY', 'USDCAD', 'USDCHF', 'USDCNH', 'USDCZK', 'USDDKK', 'USDHKD', 'USDHUF', 'USDILS', 'USDJPY', 'USDMXN', 'USDNOK', 'USDPLN', 'USDRUB', 'USDSEK', 'USDSGD', 'USDTRY', 'USDZAR', 'ZARJPY', 'EURSGD', 'GBPHKD', 'GBPMXN', 'GBPSGD', 'GBPTRY', 'GBPZAR', 'CHFMXN', 'USDINR')
--  ORDER BY FxPairId
--
--  SET @cmd = @cmd + ');
--GO
--'
--
--SELECT @cmd

CREATE PARTITION SCHEME MarketDataSymbolPartitionScheme_IndividualFileGroups
AS PARTITION MarketDataSymbolPartitioningFunction TO (SplittingTest_MDSplittedFilegroup_EURUSD, SplittingTest_MDSplittedFilegroup_GBPUSD, SplittingTest_MDSplittedFilegroup_AUDJPY, SplittingTest_MDSplittedFilegroup_AUDUSD, SplittingTest_MDSplittedFilegroup_CHFJPY, SplittingTest_MDSplittedFilegroup_EURAUD, SplittingTest_MDSplittedFilegroup_EURCHF, SplittingTest_MDSplittedFilegroup_EURGBP, SplittingTest_MDSplittedFilegroup_EURJPY, SplittingTest_MDSplittedFilegroup_GBPCHF, SplittingTest_MDSplittedFilegroup_GBPJPY, SplittingTest_MDSplittedFilegroup_NZDUSD, SplittingTest_MDSplittedFilegroup_USDCAD, SplittingTest_MDSplittedFilegroup_USDCHF, SplittingTest_MDSplittedFilegroup_USDJPY, SplittingTest_MDSplittedFilegroup_NZDJPY, SplittingTest_MDSplittedFilegroup_CADJPY, SplittingTest_MDSplittedFilegroup_EURCAD, SplittingTest_MDSplittedFilegroup_EURSEK, SplittingTest_MDSplittedFilegroup_USDDKK, SplittingTest_MDSplittedFilegroup_USDPLN, SplittingTest_MDSplittedFilegroup_USDNOK, SplittingTest_MDSplittedFilegroup_USDCZK, SplittingTest_MDSplittedFilegroup_AUDNZD, SplittingTest_MDSplittedFilegroup_EURNOK, SplittingTest_MDSplittedFilegroup_AUDCAD, SplittingTest_MDSplittedFilegroup_AUDCHF, SplittingTest_MDSplittedFilegroup_CADCHF, SplittingTest_MDSplittedFilegroup_EURCZK, SplittingTest_MDSplittedFilegroup_EURHUF, SplittingTest_MDSplittedFilegroup_EURNZD, SplittingTest_MDSplittedFilegroup_EURPLN, SplittingTest_MDSplittedFilegroup_GBPAUD, SplittingTest_MDSplittedFilegroup_GBPCAD, SplittingTest_MDSplittedFilegroup_USDSEK, SplittingTest_MDSplittedFilegroup_USDMXN, SplittingTest_MDSplittedFilegroup_EURDKK, SplittingTest_MDSplittedFilegroup_USDHUF, SplittingTest_MDSplittedFilegroup_CADNZD, SplittingTest_MDSplittedFilegroup_GBPNZD, SplittingTest_MDSplittedFilegroup_NZDCHF, SplittingTest_MDSplittedFilegroup_AUDDKK, SplittingTest_MDSplittedFilegroup_AUDNOK, SplittingTest_MDSplittedFilegroup_EURTRY, SplittingTest_MDSplittedFilegroup_EURZAR, SplittingTest_MDSplittedFilegroup_NOKSEK, SplittingTest_MDSplittedFilegroup_NZDCAD, SplittingTest_MDSplittedFilegroup_USDHKD, SplittingTest_MDSplittedFilegroup_USDSGD, SplittingTest_MDSplittedFilegroup_USDTRY, SplittingTest_MDSplittedFilegroup_EURMXN, SplittingTest_MDSplittedFilegroup_USDZAR, SplittingTest_MDSplittedFilegroup_AUDSEK, SplittingTest_MDSplittedFilegroup_CADDKK, SplittingTest_MDSplittedFilegroup_CADNOK, SplittingTest_MDSplittedFilegroup_CADSEK, SplittingTest_MDSplittedFilegroup_CHFDKK, SplittingTest_MDSplittedFilegroup_CHFNOK, SplittingTest_MDSplittedFilegroup_CHFSEK, SplittingTest_MDSplittedFilegroup_DKKJPY, SplittingTest_MDSplittedFilegroup_DKKNOK, SplittingTest_MDSplittedFilegroup_DKKSEK, SplittingTest_MDSplittedFilegroup_GBPDKK, SplittingTest_MDSplittedFilegroup_GBPNOK, SplittingTest_MDSplittedFilegroup_GBPSEK, SplittingTest_MDSplittedFilegroup_HKDJPY, SplittingTest_MDSplittedFilegroup_MXNJPY, SplittingTest_MDSplittedFilegroup_NOKJPY, SplittingTest_MDSplittedFilegroup_NZDDKK, SplittingTest_MDSplittedFilegroup_NZDNOK, SplittingTest_MDSplittedFilegroup_NZDSEK, SplittingTest_MDSplittedFilegroup_AUDSGD, SplittingTest_MDSplittedFilegroup_ZARJPY, SplittingTest_MDSplittedFilegroup_NZDSGD, SplittingTest_MDSplittedFilegroup_SGDJPY, SplittingTest_MDSplittedFilegroup_USDILS, SplittingTest_MDSplittedFilegroup_AUDHKD, SplittingTest_MDSplittedFilegroup_EURHKD, SplittingTest_MDSplittedFilegroup_EURRUB, SplittingTest_MDSplittedFilegroup_GBPCZK, SplittingTest_MDSplittedFilegroup_GBPHUF, SplittingTest_MDSplittedFilegroup_USDRUB, SplittingTest_MDSplittedFilegroup_GBPPLN, SplittingTest_MDSplittedFilegroup_EURCNH, SplittingTest_MDSplittedFilegroup_USDCNH, SplittingTest_MDSplittedFilegroup_EURSGD, SplittingTest_MDSplittedFilegroup_GBPHKD, SplittingTest_MDSplittedFilegroup_GBPMXN, SplittingTest_MDSplittedFilegroup_GBPSGD, SplittingTest_MDSplittedFilegroup_GBPTRY, SplittingTest_MDSplittedFilegroup_GBPZAR, SplittingTest_MDSplittedFilegroup_CHFMXN, SplittingTest_MDSplittedFilegroup_USDINR);
GO

-- 3) Now Re-Create index on the new filegroup:

CREATE CLUSTERED INDEX IX_PairRecordId ON dbo.MDPartitioned
(
	[FXPairId] ASC,
	[RecordID] ASC
)
WITH (DROP_EXISTING = ON)
ON MarketDataSymbolPartitionScheme_IndividualFileGroups([FXPairId]);


-- 4) Verify result:


--DECLARE @TableName sysname = 'MDPartitioned';
--SELECT p.partition_number, fg.name, p.rows FROM sys.partitions p  INNER JOIN sys.allocation_units au  ON au.container_id = p.hobt_id  INNER JOIN sys.filegroups fg  ON fg.data_space_id = au.data_space_id WHERE p.object_id = OBJECT_ID(@TableName)


-- -------------------------------------------------------------------------
--                                                                        --
--                         END OF Option 2                                --
--                                                                        --
-- -------------------------------------------------------------------------