
UPDATE dee
SET 
	AmountTermPolExecutedInUsd = -AmountBasePolExecuted * Price
FROM
	DealExternalExecuted dee
	INNER JOIN Symbol s ON dee.SymbolId = s.id
	INNER JOIN Currency termCur ON s.QuoteCurrencyId = termCur.CurrencyID
WHERE 
	termCur.[CurrencyAlphaCode] = 'USD'
	
	
CREATE PROCEDURE GetCounterpartyMonitorSettingsList_SP
AS
BEGIN
	SELECT 
		[Id]
		,[FriendlyName]
	FROM 
		[dbo].[Settings]
	WHERE
		[Name] = 'Kreslik.Integrator.DiagnosticsDashboard.exe'
END
GO

GRANT EXECUTE ON [dbo].[GetCounterpartyMonitorSettingsList_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


CREATE UNIQUE NONCLUSTERED INDEX settings_friendlyName
ON [dbo].[Settings](FriendlyName)
WHERE FriendlyName IS NOT NULL
GO


CREATE PROCEDURE [dbo].[GetSettingsXml2_SP] 
	@SettingName NVARCHAR(100),
	@SettingFriendlyName NVARCHAR(200)
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		sett.SettingsXml
	FROM
		[dbo].[Settings] sett
	WHERE
		sett.Name = @SettingName
		AND sett.[FriendlyName] = @SettingFriendlyName
END
GO

GRANT EXECUTE ON [dbo].[GetSettingsXml2_SP] TO [IntegratorServiceAccount] AS [dbo]
GO





CREATE PROCEDURE [dbo].[UpdateCounterpartyMonitorSettings_SP] 
	@EnvironmentName NVARCHAR(100),
	@SettingsFriendlyName NVARCHAR(200),
	@SettingsXml Xml
AS
BEGIN
	DECLARE @CurrentDiagDashboardSettingId INT
	DECLARE @CurrentDiagDashboardSettingFriendlyName NVARCHAR(200)
	DECLARE @EnvironmentId INT


	SELECT
		@CurrentDiagDashboardSettingId = sett.Id,
		@CurrentDiagDashboardSettingFriendlyName = sett.FriendlyName,
		@EnvironmentId = envint.Id
	FROM
		[dbo].[IntegratorEnvironment] envint 
		INNER JOIN [dbo].[IntegratorEnvironmentToSettingsMap] map ON envint.Id = map.IntegratorEnvironmentId
		INNER JOIN [dbo].[Settings] sett ON sett.id = map.SettingsId
	WHERE
		envint.Name = @EnvironmentName
		AND sett.Name = 'Kreslik.Integrator.DiagnosticsDashboard.exe'

	IF @CurrentDiagDashboardSettingFriendlyName = @SettingsFriendlyName
	BEGIN
		-- This is update to current Diag dashboar setting
		UPDATE 
		[dbo].[Settings] 
		SET 
			[SettingsXml] = @SettingsXml, 
			[LastUpdated] = GETUTCDATE()
		WHERE
			[Id] = @CurrentDiagDashboardSettingId

		--and we are done
	END
	ELSE
	BEGIN
		DECLARE @NewDiagDashboardSettingId INT = NULL

		SELECT
			@NewDiagDashboardSettingId = sett.Id
		FROM
			[dbo].[Settings] sett
		WHERE
			sett.Name = 'Kreslik.Integrator.DiagnosticsDashboard.exe'
			AND sett.FriendlyName = @SettingsFriendlyName

		IF @NewDiagDashboardSettingId IS NULL
		BEGIN
			-- This is creation of fresh new setting 
			
			INSERT INTO [dbo].[Settings]
				([Name]
				,[FriendlyName]
				,[SettingsVersion]
				,[SettingsXml]
				,[LastUpdated]
				,[IsOnlineMonitored])
			VALUES
				(N'Kreslik.Integrator.DiagnosticsDashboard.exe'
				,@SettingsFriendlyName
				,1
				,@SettingsXml
				,GETUTCDATE()
				,0
				)

			SELECT @NewDiagDashboardSettingId = SCOPE_IDENTITY()

		END
		ELSE
		BEGIN
			-- This is update of existing setting outside current environment
			
			UPDATE 
			[dbo].[Settings] 
			SET 
				[SettingsXml] = @SettingsXml, 
				[LastUpdated] = GETUTCDATE()
			WHERE
				[Id] = @NewDiagDashboardSettingId
		END

		-- Now we need to remap environment

		UPDATE 
			[dbo].[IntegratorEnvironmentToSettingsMap]
		SET 
			[SettingsId] = @NewDiagDashboardSettingId
		WHERE
			[IntegratorEnvironmentId] = @EnvironmentId
			AND [SettingsId] = @CurrentDiagDashboardSettingId
	END
END
GO

GRANT EXECUTE ON [dbo].[UpdateCounterpartyMonitorSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetCounterpartyMonitorSettingsName_SP] 
	@EnvironmentName NVARCHAR(100)
AS
BEGIN

	SELECT
		sett.FriendlyName
	FROM
		[dbo].[IntegratorEnvironment] envint 
		INNER JOIN [dbo].[IntegratorEnvironmentToSettingsMap] map ON envint.Id = map.IntegratorEnvironmentId
		INNER JOIN [dbo].[Settings] sett ON sett.id = map.SettingsId
	WHERE
		envint.Name = @EnvironmentName
		AND sett.Name = 'Kreslik.Integrator.DiagnosticsDashboard.exe'
END
GO

GRANT EXECUTE ON [dbo].[GetCounterpartyMonitorSettingsName_SP] TO [IntegratorServiceAccount] AS [dbo]
GO