

SET IDENTITY_INSERT [dbo].[LiquidityProvider] ON 
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (23, N'FXCM', N'FCM')
GO
SET IDENTITY_INSERT [dbo].[LiquidityProvider] OFF
GO

SET IDENTITY_INSERT [dbo].[LiqProviderStream] ON 
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (82, N'FC1', 23)
GO
SET IDENTITY_INSERT [dbo].[LiqProviderStream] OFF
GO


INSERT INTO [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (4, 'FXCM', 'FXCM')
GO



INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDCAD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDCHF'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDDKK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDHKD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDJPY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDNOK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDNZD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDSEK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDSGD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDUSD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADCHF'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADDKK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADJPY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADNOK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADSEK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFDKK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFJPY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFMXN'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFNOK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFSEK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'DKKJPY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'DKKNOK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'DKKSEK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURAUD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCAD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCHF'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCZK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURDKK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURGBP'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURHKD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURHUF'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURJPY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURMXN'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURNOK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURNZD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURPLN'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURRUB'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURSEK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURSGD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURTRY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURUSD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURZAR'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPAUD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPCAD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPCHF'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPCZK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPDKK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPHKD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPHUF'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPJPY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPMXN'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPNOK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPNZD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPPLN'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPSEK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPSGD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPTRY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPUSD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPZAR'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'HKDJPY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.000001, 0.000001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'MXNJPY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NOKJPY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NOKSEK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDCAD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDCHF'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDDKK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDJPY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDNOK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDSEK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDSGD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDUSD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'SGDJPY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCAD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCHF'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCNH'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCZK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDDKK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDHKD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDHUF'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDILS'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDJPY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDMXN'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDNOK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDPLN'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDRUB'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDSEK'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDSGD'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDTRY'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDZAR'
 where Name = 'FXCM'

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceIncrement]
           ,[OrderMinimumSize]
           ,[OrderMinimumSizeIncrement])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001, 1, 1
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'ZARJPY'
 where Name = 'FXCM'


EXEC sp_rename 'CounterpartySymbolSettings.OnePipDecimalValue', 'OnePipDecimalValue_Legacy', 'COLUMN'
