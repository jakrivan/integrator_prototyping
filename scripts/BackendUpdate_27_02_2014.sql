
ALTER TRIGGER [dbo].[SchemaXsdCheckTrigger] ON [dbo].[Settings] AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @Type NVARCHAR(100)
	DECLARE @SettingsXml [xml]
	DECLARE @Version [int]
	
	DECLARE settingsCursor CURSOR FOR
		SELECT Name, SettingsVersion, SettingsXml FROM inserted
    
	OPEN settingsCursor
    FETCH NEXT FROM settingsCursor INTO @Type, @Version, @SettingsXml
    WHILE @@FETCH_STATUS = 0
    BEGIN

		IF(@Type = N'Kreslik.Integrator.BusinessLayer.dll')
		BEGIN
			DECLARE @x1 XML([dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd]) 
			SET @x1 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.Common.dll')
		BEGIN
			DECLARE @x2 XML([dbo].[Kreslik.Integrator.Common.dll.xsd]) 
			SET @x2 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL.dll')
		BEGIN
			DECLARE @x3 XML([dbo].[Kreslik.Integrator.DAL.dll.xsd]) 
			SET @x3 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL_IntegratorConnection')
		BEGIN
			DECLARE @x32 XML([dbo].[Kreslik.Integrator.DALv2_IntegratorConnection.xsd]) 
			SET @x32 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL_DataCollectionConnection')
		BEGIN
			DECLARE @x33 XML([dbo].[Kreslik.Integrator.DALv2_DataCollectionConnection.xsd]) 
			SET @x33 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL_DataCollectionEnabling')
		BEGIN
			DECLARE @x34 XML([dbo].[Kreslik.Integrator.DALv2_DataCollectionEnabling.xsd]) 
			SET @x34 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DiagnosticsDashboard.exe')
		BEGIN
			DECLARE @x4 XML([dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd]) 
			SET @x4 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.IntegratorClientHost.exe')
		BEGIN
			DECLARE @x5 XML([dbo].[Kreslik.Integrator.IntegratorClientHost.exe.xsd]) 
			SET @x5 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.IntegratorWatchDogSvc.exe')
		BEGIN
			DECLARE @x6 XML([dbo].[Kreslik.Integrator.IntegratorWatchDogSvc.exe.xsd]) 
			SET @x6 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBus.dll')
		BEGIN
			DECLARE @x7 XML([dbo].[Kreslik.Integrator.MessageBus.dll.xsd]) 
			SET @x7 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo')
		BEGIN
			DECLARE @x7_1 XML([dbo].[Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo.xsd]) 
			SET @x7_1 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo')
		BEGIN
			DECLARE @x7_2 XML([dbo].[Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo.xsd]) 
			SET @x7_2 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping')
		BEGIN
			DECLARE @x7_3 XML([dbo].[Kreslik.Integrator.MessageBusSettings_ConnectionKeeping.xsd]) 
			SET @x7_3 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior')
		BEGIN
			DECLARE @x7_4 XML([dbo].[Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior.xsd]) 
			SET @x7_4 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling')
		BEGIN
			DECLARE @x7_5 XML([dbo].[Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling.xsd]) 
			SET @x7_5 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.QuickItchN.dll')
		BEGIN
			DECLARE @x8 XML([dbo].[Kreslik.Integrator.QuickItchN.dll.xsd]) 
			SET @x8 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.RiskManagement.dll')
		BEGIN
			DECLARE @x9 XML([dbo].[Kreslik.Integrator.RiskManagement.dll.xsd]) 
			SET @x9 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.SessionManagement.dll')
		BEGIN
			DECLARE @x10 XML([dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]) 
			SET @x10 = @SettingsXml
		END

		ELSE
		BEGIN
			RAISERROR (N'Setting with name: %s is unknown to DB', 16, 2, @Type ) WITH SETERROR
			ROLLBACK TRAN
		END

		FETCH NEXT FROM settingsCursor INTO @Type, @Version, @SettingsXml
    END
	CLOSE settingsCursor
    DEALLOCATE settingsCursor
END


DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.DAL.dll.xsd]
GO

DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.DALv2.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DAL.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="DALSettings" nillable="true" type="DALSettings" />
  <xs:complexType name="DALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="KillSwitchPollIntervalSeconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="KillSwitchWatchDogIntervalSeconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="IntegratorSwitchName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="CommandDistributorPollIntervalSeconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="DataCollection" type="DataCollectionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="DataCollectionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="DataProviderId" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="SourceFileId" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxBcpBatchSize" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxInMemoryColectionSize" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxIntervalBetweenUploads_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxUploadTimeAfterWhichSerializationStarts_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxProcessingTimeAfterShutdownSignalled_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="WaitIntervalBetweenBcpRetries_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="WaitIntervalBeforeShutdownStartsSerialization_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="DataCollectionFolder" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO



UPDATE [dbo].[Settings] SET [SettingsXml] = '<DALSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <KillSwitchPollIntervalSeconds>3</KillSwitchPollIntervalSeconds>
  <KillSwitchWatchDogIntervalSeconds>40</KillSwitchWatchDogIntervalSeconds>
  <IntegratorSwitchName>KGTIntegratorKillSwitch</IntegratorSwitchName>
  <CommandDistributorPollIntervalSeconds>3</CommandDistributorPollIntervalSeconds>
  <DataCollection>
    <DataProviderId>22</DataProviderId>
    <SourceFileId>932696</SourceFileId>
    <MaxBcpBatchSize>100000</MaxBcpBatchSize>
    <MaxInMemoryColectionSize>1000000</MaxInMemoryColectionSize>
    <MaxIntervalBetweenUploads_Seconds>45</MaxIntervalBetweenUploads_Seconds>
    <MaxUploadTimeAfterWhichSerializationStarts_Seconds>5</MaxUploadTimeAfterWhichSerializationStarts_Seconds>
    <MaxProcessingTimeAfterShutdownSignalled_Seconds>30</MaxProcessingTimeAfterShutdownSignalled_Seconds>
    <WaitIntervalBetweenBcpRetries_Seconds>2</WaitIntervalBetweenBcpRetries_Seconds>
    <WaitIntervalBeforeShutdownStartsSerialization_Seconds>4</WaitIntervalBeforeShutdownStartsSerialization_Seconds>
	<DataCollectionFolder><![CDATA[..\DataCollection]]></DataCollectionFolder>
  </DataCollection>
</DALSettings>', [LastUpdated] = GETUTCDATE() WHERE [Name] = 'Kreslik.Integrator.DAL.dll' AND [Id] = 21


--let's use new DC backend

UPDATE [dbo].[Settings] SET [SettingsXml] = '<DALSettings_DataCollectionConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ConnectionString>Data Source=37.46.6.111;Initial Catalog=DataCollection_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</ConnectionString>
</DALSettings_DataCollectionConnection>', [LastUpdated] = GETUTCDATE() WHERE [Name] = 'Kreslik.Integrator.DAL_DataCollectionConnection' AND [Id] = 24

UPDATE [dbo].[Settings] SET [SettingsXml] = '<DALSettings_DataCollectionConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ConnectionString>Data Source=192.168.75.53;Initial Catalog=DataCollection_Testing;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</ConnectionString>
</DALSettings_DataCollectionConnection>', [LastUpdated] = GETUTCDATE() WHERE [Name] = 'Kreslik.Integrator.DAL_DataCollectionConnection' AND [Id] = 25


--Web related changes

UPDATE [dbo].[ExternalOrdersTransmissionSwitch]
   SET [SwitchName] = 'PrimebrokerKillSwitch'
 WHERE SwitchName = 'RaboKillSwitch'
GO

DROP PROCEDURE [dbo].[TryLoginKillSwitchUser_SP]
GO


CREATE TABLE [dbo].[KillSwitchRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_KillSwitchRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET IDENTITY_INSERT [dbo].[KillSwitchRole] ON 

GO
INSERT [dbo].[KillSwitchRole] ([Id], [Name]) VALUES (1, N'KGT')
GO
INSERT [dbo].[KillSwitchRole] ([Id], [Name]) VALUES (2, N'Primebroker')
GO
SET IDENTITY_INSERT [dbo].[KillSwitchRole] OFF
GO



ALTER PROCEDURE [dbo].[GetDetailsForRiskMgmtUser_SP] 
	@username NVARCHAR(256)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @True BIT = 1

	SELECT 
		@True as Loged, usr.IsAdmin as IsAdmin, s.Name as RoleName
	FROM 
		[dbo].[KillSwitchUser] usr
		INNER JOIN [dbo].[KillSwitchRole] s ON usr.RoleId = s.Id
	WHERE
		usr.UserName = @username
END
GO

ALTER PROCEDURE [dbo].[GetRecentKillSwitchLoginActivity_SP] 
	@roleName NVARCHAR(64),
	@username  NVARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @RoleId INT 
	SELECT @RoleId = [Id] FROM [dbo].[KillSwitchRole] WHERE [Name] = @roleName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Role not found in DB: %s', @roleName, 16, 2) WITH SETERROR
	END

	SELECT TOP 10 
		[UserName], 
		[ActionTime], 
		[Source], 
		[Success] 
	FROM 
		[dbo].[KillSwitchLogon] 
	WHERE 
		[RoleId] = @RoleId AND @username IS NULL OR [UserName] = @username 
	ORDER BY ActionTime DESC

END
GO



ALTER PROCEDURE [dbo].[TryLoginRiskMgmtUser_SP] 
	@username NVARCHAR(256),
	@password NVARCHAR(256),
	@identity NVARCHAR(256)
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @t TABLE (Loged BIT, IsAdmin BIT, RoleName nvarchar(64))
	DECLARE @Loged BIT
	DECLARE @IsAdmin BIT
	DECLARE @RoleName NVARCHAR(64)
	DECLARE @RoleId INT
	SET @Loged = 0
	SET @IsAdmin = 0
	SET @RoleId = 2

		--first select the role id (so that it's correct even for unsuccessful logons)
	SELECT 
		@RoleId = s.Id
	FROM 
		[dbo].[KillSwitchUser] usr
		INNER JOIN [dbo].[KillSwitchRole] s ON usr.RoleId = s.Id
	WHERE
		usr.UserName = @username

	SELECT 
		@Loged = 1, @IsAdmin = usr.IsAdmin, @RoleName = s.Name
	FROM 
		[dbo].[KillSwitchUser] usr
		INNER JOIN [dbo].[KillSwitchRole] s ON usr.RoleId = s.Id
	WHERE
		usr.UserName = @username AND usr.Password = @password

	INSERT INTO 
		[dbo].[KillSwitchLogon]
           ([UserName]
           ,[ActionTime]
           ,[Source]
           ,[Success]
		   ,[RoleId])
     VALUES
           (@username
           ,GETUTCDATE()
           ,@identity
           ,@Loged
		   ,@RoleId)

	INSERT INTO 
		[dbo].[KillSwitchLogon]
           ([UserName]
           ,[ActionTime]
           ,[Source]
           ,[Success]
		   ,[RoleId])
     VALUES
           (@username
           ,GETUTCDATE()
           ,@identity
           ,@Loged
		   ,@RoleId)

	INSERT INTO @t VALUES (@Loged,@IsAdmin,@RoleName)
	SELECT Loged, IsAdmin, RoleName FROM @t
END
GO