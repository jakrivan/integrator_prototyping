


/****** Object:  Table [dbo].[DealDirection]    Script Date: 3. 7. 2013 19:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealDirection](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](4) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExecutedExternalDeals]    Script Date: 3. 7. 2013 19:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExecutedExternalDeals](
	[OrderId] [nvarchar](32) NOT NULL,
	[ExecutionTimeStampUtc] [datetime] NOT NULL,
	[SymbolId] [int] NOT NULL,
	[DealDirectionId] [int] NOT NULL,
	[ExecutedAmountBaseAbs] [decimal](18, 0) NOT NULL,
	[ExecutedAmountBasePol] [decimal](18, 0) NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Symbol]    Script Date: 3. 7. 2013 19:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Symbol](
	[Id] [int] NOT NULL,
	[Name] [nchar](7) NOT NULL,
	[ShortName] [nchar](6) NOT NULL
) ON [PRIMARY]

GO


/****** Object:  Index [IX_ExecutionTimeStampUtc_SymbolId]    Script Date: 3. 7. 2013 19:52:01 ******/
CREATE CLUSTERED INDEX [IX_ExecutionTimeStampUtc_SymbolId] ON [dbo].[ExecutedExternalDeals]
(
	[ExecutionTimeStampUtc] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_DealDirection]    Script Date: 3. 7. 2013 19:52:01 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_DealDirection] ON [dbo].[DealDirection]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Symbol]    Script Date: 3. 7. 2013 19:52:01 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Symbol] ON [dbo].[Symbol]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


ALTER TABLE [dbo].[ExecutedExternalDeals]  WITH CHECK ADD  CONSTRAINT [FK_ExecutedExternalDeals_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[ExecutedExternalDeals] CHECK CONSTRAINT [FK_ExecutedExternalDeals_DealDirection]
GO
ALTER TABLE [dbo].[ExecutedExternalDeals]  WITH CHECK ADD  CONSTRAINT [FK_ExecutedExternalDeals_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO
ALTER TABLE [dbo].[ExecutedExternalDeals] CHECK CONSTRAINT [FK_ExecutedExternalDeals_Symbol]
GO



/****** Object:  StoredProcedure [dbo].[InsertExecutedExternalDeal_SP]    Script Date: 3. 7. 2013 19:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertExecutedExternalDeal_SP]( 
	@OrderId NVARCHAR(32),
	@ExecutionTimeStampUtc DATETIME,
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@ExecutedAmountBaseAbs DECIMAL(18,0),
	@ExecutedAmountBasePol DECIMAL(18,0)
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
		[dbo].[ExecutedExternalDeals]
           ([OrderId]
           ,[ExecutionTimeStampUtc]
           ,[SymbolId]
           ,[DealDirectionId]
           ,[ExecutedAmountBaseAbs]
           ,[ExecutedAmountBasePol])
     VALUES
           (@OrderId
           ,@ExecutionTimeStampUtc
           ,@SymbolId
           ,@DealDirectionId 
           ,@ExecutedAmountBaseAbs
           ,@ExecutedAmountBasePol)
END

GO
GRANT EXECUTE ON [dbo].[InsertExecutedExternalDeal_SP] TO [IntegratorServiceAccount] AS [dbo]
GO








--DealDirection
INSERT INTO [dbo].[DealDirection] ([Id], [Name]) VALUES (0, N'Buy')
GO
INSERT INTO [dbo].[DealDirection] ([Id], [Name]) VALUES (1, N'Sell')
GO

--Symbols
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(0, N'AUD/CAD', 'AUDCAD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(1, N'AUD/CHF', 'AUDCHF')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(2, N'AUD/JPY', 'AUDJPY')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(3, N'AUD/NZD', 'AUDNZD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(4, N'AUD/SGD', 'AUDSGD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(5, N'AUD/USD', 'AUDUSD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(6, N'CAD/JPY', 'CADJPY')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(7, N'CHF/JPY', 'CHFJPY')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(8, N'EUR/AUD', 'EURAUD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(9, N'EUR/CAD', 'EURCAD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(10, N'EUR/CHF', 'EURCHF')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(11, N'EUR/CZK', 'EURCZK')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(12, N'EUR/DKK', 'EURDKK')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(13, N'EUR/GBP', 'EURGBP')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(14, N'EUR/HUF', 'EURHUF')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(15, N'EUR/JPY', 'EURJPY')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(16, N'EUR/MXN', 'EURMXN')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(17, N'EUR/NOK', 'EURNOK')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(18, N'EUR/NZD', 'EURNZD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(19, N'EUR/PLN', 'EURPLN')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(20, N'EUR/SEK', 'EURSEK')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(21, N'EUR/USD', 'EURUSD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(22, N'EUR/ZAR', 'EURZAR')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(23, N'GBP/AUD', 'GBPAUD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(24, N'GBP/CAD', 'GBPCAD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(25, N'GBP/CHF', 'GBPCHF')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(26, N'GBP/JPY', 'GBPJPY')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(27, N'GBP/NOK', 'GBPNOK')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(28, N'GBP/NZD', 'GBPNZD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(29, N'GBP/USD', 'GBPUSD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(30, N'HKD/JPY', 'HKDJPY')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(31, N'NOK/SEK', 'NOKSEK')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(32, N'NZD/JPY', 'NZDJPY')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(33, N'NZD/SGD', 'NZDSGD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(34, N'NZD/USD', 'NZDUSD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(35, N'SGD/JPY', 'SGDJPY')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(36, N'USD/CAD', 'USDCAD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(37, N'USD/CHF', 'USDCHF')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(38, N'USD/CZK', 'USDCZK')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(39, N'USD/HKD', 'USDHKD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(40, N'USD/HUF', 'USDHUF')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(41, N'USD/ILS', 'USDILS')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(42, N'USD/JPY', 'USDJPY')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(43, N'USD/MXN', 'USDMXN')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(44, N'USD/NOK', 'USDNOK')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(45, N'USD/PLN', 'USDPLN')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(46, N'USD/SEK', 'USDSEK')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(47, N'USD/SGD', 'USDSGD')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(48, N'USD/TRY', 'USDTRY')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(49, N'USD/ZAR', 'USDZAR')
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName]) VALUES(50, N'ZAR/JPY', 'ZARJPY')
GO
