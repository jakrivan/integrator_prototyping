
print 'Started:'
print GETUTCDATE()
GO


print 'Creating Index to speed up repetitive lookups:'
print GETUTCDATE()
GO

CREATE NONCLUSTERED INDEX [IX_OrderExternal_IntegratorExternalOrderPrivateId] ON [dbo].[OrderExternal]
(
	[IntegratorExternalOrderPrivateId] ASC
)
INCLUDE
(
	[IntegratorSentExternalOrderUtc],
	[QuoteReceivedToOrderSentInternalLatency]
) ON [LargeSlowDataFileGroup]
GO

print 'Index done:'
print GETUTCDATE()
GO



print 'Altering SPs not to default to nulls:'
print GETUTCDATE()
GO

ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 0),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END

	INSERT INTO 
	[dbo].[DealExternalExecuted]
           ([IntegratorReceivedExecutionReportUtc]
           ,[CounterpartyId]
           ,[SymbolId]
           ,[IntegratorExternalOrderPrivateId]
           ,[DealDirectionId]
           ,[AmountBasePolExecuted]
		   ,[AmountTermPolExecutedInUsd]
           ,[Price]
           ,[CounterpartyTransactionId]
           ,[CounterpartySuppliedExecutionTimeStampUtc]
           ,[ValueDateCounterpartySuppliedLocMktDate]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[IntegratorSentExternalOrderUtc]
		   ,[CounterpartySentExecutionReportUtc]
		   ,[InternalTransactionIdentity]
		   ,[FlowSideId])
     VALUES
           (@ExecutionReportReceivedUtc
           ,@CounterpartyId
           ,@SymbolId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolExecuted
		   ,@AmountTermPolExecutedInUsd
           ,@Price
           ,@CounterpartyTransactionId
           ,@CounterpartyExecutionReportTimeStampUtc
           ,@ValueDateCounterpartySuppliedLocMktDate
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@QuoteReceivedToOrderSentInternalLatency
		   ,@IntegratorSentExternalOrderUtc
		   ,@CounterpartySentExecutionReportUtc
		   ,@InternalDealId
		   ,@FlowSideId)
END
GO

ALTER PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 0),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[DealExternalRejected]
           ([IntegratorReceivedExecutionReportUtc]
           ,[SymbolId]
           ,[CounterpartyId]
           ,[IntegratorExternalOrderPrivateId]
		   ,[DealDirectionId]
           ,[AmountBasePolRejected]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[IntegratorSentExternalOrderUtc]
           ,[RejectionReason]
		   ,[CounterpartySentExecutionReportUtc])
     VALUES
           (@ExecutionReportReceivedUtc
		   ,@SymbolId
           ,@CounterpartyId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolRejected
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@QuoteReceivedToOrderSentInternalLatency
		   ,@IntegratorSentExternalOrderUtc
		   ,@RejectionReason
		   ,@CounterpartySentExecutionReportUtc)
END
GO

ALTER PROCEDURE [dbo].[InsertExecutionReportMessage_SP]( 
	@ExecutionReportReceivedUtc [datetime2](7),
	@ExecutionReportStatus [varchar](16),
	@InternalOrderId [nvarchar](45),
	@ExecutionReportCounterpartySentUtc [datetime2](7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@RawFIXMessage [nvarchar](1024)
	)
AS
BEGIN

	DECLARE @ExecutionReportStatusId INT

	SELECT @ExecutionReportStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ExecutionReportStatus

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ExecutionReportStatus not found in DB: %s', 16, 2, @ExecutionReportStatus) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[ExecutionReportMessage]
           ([IntegratorReceivedExecutionReportUtc]
           ,[ExecutionReportStatusId]
           ,[IntegratorExternalOrderPrivateId]
           ,[CounterpartySentExecutionReportUtc]
		   ,[IntegratorSentExternalOrderUtc]
           ,[FixMessageReceivedRaw])
     VALUES
           (
		   @ExecutionReportReceivedUtc
           ,@ExecutionReportStatusId
           ,@InternalOrderId
           ,@ExecutionReportCounterpartySentUtc
		   ,@IntegratorSentExternalOrderUtc
           ,@RawFIXMessage
		   )
END
GO

ALTER PROCEDURE [dbo].[InsertOrderExternalCancelRequest_SP]( 
	@IntegratorExternalOrderPrivateId [nvarchar](45),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
    @IntegratorSentCancelRequestUtc  DATETIME2(7),
    @OrderExternalCancelTypeName VARCHAR(16),
    @RawFIXMessage [nvarchar](1024)
	)
AS
BEGIN

	DECLARE @OrderExternalCancelTypeId TINYINT

	SELECT @OrderExternalCancelTypeId = [OrderExternalCancelTypeId] FROM [dbo].[OrderExternalCancelType] WHERE [OrderExternalCancelTypeName] = @OrderExternalCancelTypeName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderExternalCancelTypeName not found in DB: %s', 16, 2, @OrderExternalCancelTypeName) WITH SETERROR
	END


	INSERT INTO [dbo].[OrderExternalCancelRequest]
           ([IntegratorExternalOrderPrivateId]
		   ,[IntegratorSentExternalOrderUtc]
           ,[IntegratorSentCancelRequestUtc]
           ,[OrderExternalCancelTypeId]
           ,[FixMessageSentRaw])
     VALUES
           (@IntegratorExternalOrderPrivateId
		   ,@IntegratorSentExternalOrderUtc
           ,@IntegratorSentCancelRequestUtc
           ,@OrderExternalCancelTypeId
           ,@RawFIXMessage)
END
GO

ALTER PROCEDURE [dbo].[InsertOrderExternalCancelResult_SP]( 
	@IntegratorExternalOrderPrivateId [nvarchar](45),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
    @CounterpartySentResultUtc  DATETIME2(7),
    @CancelledAmountBasePol decimal(18,0),
    @ResultStatusName VARCHAR(16)
	)
AS
BEGIN

	DECLARE @ResultStatusId TINYINT

	SELECT @ResultStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ResultStatusName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ResultStatusName not found in DB: %s', 16, 2, @ResultStatusName) WITH SETERROR
	END


	INSERT INTO [dbo].[OrderExternalCancelResult]
           ([IntegratorExternalOrderPrivateId]
		   ,[IntegratorSentExternalOrderUtc]
           ,[CounterpartySentResultUtc]
           ,[CanceledAmountBasePol]
           ,[ResultStatusId])
     VALUES
           (@IntegratorExternalOrderPrivateId
		   ,@IntegratorSentExternalOrderUtc
           ,@CounterpartySentResultUtc
           ,@CancelledAmountBasePol
           ,@ResultStatusId)
END
GO


print 'procedures updated:'
print GETUTCDATE()
GO	


UPDATE 
	deal
SET 
	deal.QuoteReceivedToOrderSentInternalLatency = ordr.QuoteReceivedToOrderSentInternalLatency,
	deal.IntegratorSentExternalOrderUtc = ordr.IntegratorSentExternalOrderUtc
FROM 
	[dbo].[DealExternalExecuted] AS deal
	INNER JOIN [dbo].[OrderExternal] AS ordr ON deal.IntegratorExternalOrderPrivateId = ordr.IntegratorExternalOrderPrivateId

print 'Deals table updated:'
print GETUTCDATE()
GO	
	

UPDATE 
	reject
SET 
	reject.QuoteReceivedToOrderSentInternalLatency = ordr.QuoteReceivedToOrderSentInternalLatency,
	reject.IntegratorSentExternalOrderUtc = ordr.IntegratorSentExternalOrderUtc
FROM 
	[dbo].[DealExternalRejected] AS reject
	INNER JOIN [dbo].[OrderExternal] AS ordr ON reject.IntegratorExternalOrderPrivateId = ordr.IntegratorExternalOrderPrivateId

print 'Rejects table updated:'
print GETUTCDATE()
GO	
	

UPDATE 
	msg
SET 
	msg.IntegratorSentExternalOrderUtc = ordr.IntegratorSentExternalOrderUtc
FROM 
	[dbo].[ExecutionReportMessage] AS msg
	INNER JOIN [dbo].[OrderExternal] AS ordr ON msg.IntegratorExternalOrderPrivateId = ordr.IntegratorExternalOrderPrivateId	

print 'ExecReports table updated:'
print GETUTCDATE()
GO		
	
	
UPDATE 
	cancelReq
SET 
	cancelReq.IntegratorSentExternalOrderUtc = ordr.IntegratorSentExternalOrderUtc
FROM 
	[dbo].[OrderExternalCancelRequest] AS cancelReq
	INNER JOIN [dbo].[OrderExternal] AS ordr ON cancelReq.IntegratorExternalOrderPrivateId = ordr.IntegratorExternalOrderPrivateId	

print 'Cancel requests table updated:'
print GETUTCDATE()
GO	
	
UPDATE 
	cancelAck
SET 
	cancelAck.IntegratorSentExternalOrderUtc = ordr.IntegratorSentExternalOrderUtc
FROM 
	[dbo].[OrderExternalCancelResult] AS cancelAck
	INNER JOIN [dbo].[OrderExternal] AS ordr ON cancelAck.IntegratorExternalOrderPrivateId = ordr.IntegratorExternalOrderPrivateId		

print 'Cancel results table updated:'
print GETUTCDATE()
GO		

print 'Altering added columns to NOT NULL (cancel request and QuoteReceivedToOrderSentInternalLatency needs to keep null):'
print GETUTCDATE()
GO

ALTER TABLE [DealExternalExecuted] ALTER COLUMN [IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL
GO
ALTER TABLE [DealExternalRejected] ALTER COLUMN [IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL
GO
ALTER TABLE [ExecutionReportMessage] ALTER COLUMN [IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL
GO
ALTER TABLE [OrderExternalCancelResult] ALTER COLUMN [IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL
GO

print 'DONE'
print GETUTCDATE()
GO

EXEC msdb.dbo.sp_send_dbmail
				@recipients = 'jan.krivanek@kgtinv.com; michal.kreslik@kgtinv.com', 
				@profile_name = 'SQL Mail Profile',
				@subject = 'Data reconstruction script is done';
GO

--DROP INDEX [IX_OrderExternal_IntegratorExternalOrderPrivateId] ON [dbo].[OrderExternal] WITH ( ONLINE = OFF )
--GO
print 'IFF everything executed WITHOUT errors, drop the helper index by running:'
print 'DROP INDEX [IX_OrderExternal_IntegratorExternalOrderPrivateId] ON [dbo].[OrderExternal] WITH ( ONLINE = OFF )'
print 'GO'
GO
