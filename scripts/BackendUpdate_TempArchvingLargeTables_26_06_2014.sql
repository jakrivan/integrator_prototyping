
DROP VIEW [dbo].[OrderExternalDailyView_06/26/2014]
GO



exec sp_rename '[dbo].[ExecutionReportMessage]', 'ExecutionReportMessage_Archive'
exec sp_rename '[dbo].[OrderExternal]', 'OrderExternal_Archive'
exec sp_rename '[dbo].[OrderExternalCancelRequest]', 'OrderExternalCancelRequest_Archive'
exec sp_rename '[dbo].[OrderExternalCancelResult]', 'OrderExternalCancelResult_Archive'
GO



sp_rename 'dbo.FK_ExecutionReportMessage_ExecutionReportStatus', 'FK_ExecutionReportMessageArchive_ExecutionReportStatus', 'OBJECT'
GO
sp_rename 'dbo.FK_OrderExternal_Counterparty', 'FK_OrderExternalArchive_Counterparty', 'OBJECT'
GO
sp_rename 'dbo.FK_OrderExternal_DealDirection', 'FK_OrderExternalArchive_DealDirection', 'OBJECT'
GO
sp_rename 'dbo.FK_OrderExternal_OrderTimeInForce', 'FK_OrderExternalArchive_OrderTimeInForce', 'OBJECT'
GO
sp_rename 'dbo.FK_OrderExternal_OrderType', 'FK_OrderExternalArchive_OrderType', 'OBJECT'
GO
sp_rename 'dbo.FK_OrderExternal_PegType', 'FK_OrderExternalArchive_PegType', 'OBJECT'
GO
sp_rename 'dbo.FK_OrderExternal_Symbol', 'FK_OrderExternalArchive_Symbol', 'OBJECT'
GO
sp_rename 'dbo.FK_OrderExternalCancelRequest_OrderExternalCancelType', 'FK_OrderExternalCancelRequestArchive_OrderExternalCancelType', 'OBJECT'
GO
sp_rename 'dbo.FK_OrderExternalCancelResult_ExecutionReportStatus', 'FK_OrderExternalCancelResultArchive_ExecutionReportStatus', 'OBJECT'
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExecutionReportMessage](
	[IntegratorReceivedExecutionReportUtc] [datetime2](7) NOT NULL,
	[ExecutionReportStatusId] [tinyint] NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[CounterpartySentExecutionReportUtc] [datetime2](7) NOT NULL,
	[FixMessageReceivedRaw] [nvarchar](1024) NOT NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL
) ON [LargeSlowDataFileGroup]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderExternal](
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[ExternalOrderLabelForCounterparty] [nvarchar](32) NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[QuoteReceivedToOrderSentInternalLatency] [time](7) NULL,
	[FixMessageSentRaw] [nvarchar](1024) NULL,
	[SourceIntegratorPriceIdentity] [uniqueidentifier] NULL,
	[OrderTypeId] [tinyint] NULL,
	[OrderTimeInForceId] [tinyint] NULL,
	[RequestedPrice] [decimal](18, 9) NULL,
	[RequestedAmountBaseAbs] [decimal](18, 0) NULL,
	[SourceIntegratorPriceTimeUtc] [datetime2](7) NULL,
	[PegTypeId] [bit] NULL,
	[PegDifferenceBasePol] [decimal](18, 9) NULL,
	[DatePartIntegratorSentExternalOrderUtc]  AS (CONVERT([date],[IntegratorSentExternalOrderUtc])) PERSISTED
) ON [LargeSlowDataFileGroup]

GO
SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalCancelRequest](
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NULL,
	[IntegratorSentCancelRequestUtc] [datetime2](7) NOT NULL,
	[OrderExternalCancelTypeId] [tinyint] NOT NULL,
	[FixMessageSentRaw] [nvarchar](1024) NOT NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NULL
) ON [LargeSlowDataFileGroup]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalCancelResult](
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[CounterpartySentResultUtc] [datetime2](7) NOT NULL,
	[CanceledAmountBasePol] [decimal](18, 0) NOT NULL,
	[ResultStatusId] [tinyint] NOT NULL,
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL
) ON [LargeSlowDataFileGroup]

GO

ALTER TABLE [dbo].[ExecutionReportMessage]  WITH CHECK ADD  CONSTRAINT [FK_ExecutionReportMessage_ExecutionReportStatus] FOREIGN KEY([ExecutionReportStatusId])
REFERENCES [dbo].[ExecutionReportStatus] ([Id])
GO
ALTER TABLE [dbo].[ExecutionReportMessage] CHECK CONSTRAINT [FK_ExecutionReportMessage_ExecutionReportStatus]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_Counterparty]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_DealDirection]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_OrderTimeInForce] FOREIGN KEY([OrderTimeInForceId])
REFERENCES [dbo].[OrderTimeInForce] ([OrderTimeInForceId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_OrderTimeInForce]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_OrderType] FOREIGN KEY([OrderTypeId])
REFERENCES [dbo].[OrderType] ([OrderTypeId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_OrderType]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_PegType] FOREIGN KEY([PegTypeId])
REFERENCES [dbo].[PegType] ([PegTypeId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_PegType]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderExternal_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_Symbol]
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternalCancelType] FOREIGN KEY([OrderExternalCancelTypeId])
REFERENCES [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId])
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest] CHECK CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternalCancelType]
GO
ALTER TABLE [dbo].[OrderExternalCancelResult]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelResult_ExecutionReportStatus] FOREIGN KEY([ResultStatusId])
REFERENCES [dbo].[ExecutionReportStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderExternalCancelResult] CHECK CONSTRAINT [FK_OrderExternalCancelResult_ExecutionReportStatus]
GO






CREATE VIEW [dbo].[OrderExternalDailyView_06/26/2014]
WITH SCHEMABINDING 
AS 
SELECT 
	   [IntegratorSentExternalOrderUtc]
	  ,[DatePartIntegratorSentExternalOrderUtc]
	  ,[IntegratorExternalOrderPrivateId]
	  ,[CounterpartyId]
	  ,[QuoteReceivedToOrderSentInternalLatency]
FROM [dbo].[OrderExternal]
WHERE
	DatePartIntegratorSentExternalOrderUtc = CONVERT(DATE,'06/26/2014', 101)
GO

SET ANSI_PADDING ON
CREATE UNIQUE CLUSTERED INDEX [IX_OrderId] ON [dbo].[OrderExternalDailyView_06/26/2014]
(
	[IntegratorExternalOrderPrivateId] ASC
) ON [Integrator_DailyData]
GO