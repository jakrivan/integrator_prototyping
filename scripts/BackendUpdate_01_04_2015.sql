
ALTER PROCEDURE [dbo].[GetPolarizedNopPerPair_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[AmountBasePolExecuted]) AS PolarizedNop
	FROM 
		(
		SELECT [SymbolId], [AmountBasePolExecuted] 
		FROM [dbo].[DealExternalExecuted]
		WHERE [IntegratorReceivedExecutionReportUtc] BETWEEN @StartTime AND @EndTime
		UNION ALL
		SELECT [SymbolId], [IntegratorExecutedAmountBasePol] AS [AmountBasePolExecuted] 
		FROM [dbo].[OrderExternalIncomingRejectable_Daily]
		WHERE [IntegratorReceivedExternalOrderUtc] BETWEEN @StartTime AND @EndTime
		UNION ALL
		SELECT [SymbolId], [IntegratorExecutedAmountBasePol] AS [AmountBasePolExecuted] 
		FROM [dbo].[OrderExternalIncomingRejectable_Archive]
		WHERE [IntegratorReceivedExternalOrderUtc] BETWEEN @StartTime AND @EndTime
		) deal

		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]
END