

SET IDENTITY_INSERT [dbo].[LiquidityProvider] ON 
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (20, N'Hotspot', N'HOT')
GO
SET IDENTITY_INSERT [dbo].[LiquidityProvider] OFF
GO

SET IDENTITY_INSERT [dbo].[LiqProviderStream] ON 
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (75, N'HOT', 20)
GO
SET IDENTITY_INSERT [dbo].[LiqProviderStream] OFF
GO