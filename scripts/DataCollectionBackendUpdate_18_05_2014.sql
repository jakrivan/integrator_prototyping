

-- updating the column type - enormous requirements on log file

--print 'Convert started:'
--print GETUTCDATE()
--GO

--sp_rename MarketData, MarketData_ColConvert
--GO

--ALTER TABLE dbo.MarketData_ColConvert ALTER COLUMN CounterpartyPriceIdentity VARCHAR(64);
--GO

--sp_rename MarketData_ColConvert, MarketData
--GO

--print 'Convert DONE:'
--print GETUTCDATE()
--




--log on share

--   sp_configure 'xp_cmdshell',1; 
--Go 
--RECONFIGURE WITH OVERRIDE; 
--Go
--use master

--EXEC XP_CMDSHELL 'net use W: \\dev03\sharedScratch s9F3cx7Gk /USER:kgtsql01\Administrator2'

--EXEC XP_CMDSHELL 'net use V: \\dev01\drop dW9g3s5C6b /USER:dev01\Administrator'

--ALTER DATABASE Integrator_Testing 
--ADD LOG FILE 
--(
--    NAME = remotelog,
--    FILENAME = 'V:\remotelog_testing.ldf',
--    SIZE = 5MB,
--    MAXSIZE = 100MB,
--    FILEGROWTH = 5MB
--)

--EXEC XP_CMDSHELL 'net use Q: /delete'



-- 1) Create Filegroups

print 'Started:'
print GETUTCDATE()
GO



Alter Database FXtickDB Add FileGroup TickFilegroup_AUDCAD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_AUDCAD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_AUDCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_AUDCAD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_AUDCHF
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_AUDCHF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_AUDCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_AUDCHF
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_AUDDKK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_AUDDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_AUDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_AUDDKK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_AUDHKD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_AUDHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_AUDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_AUDHKD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_AUDJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_AUDJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_AUDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_AUDJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_AUDNOK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_AUDNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_AUDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_AUDNOK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_AUDNZD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_AUDNZD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_AUDNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_AUDNZD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_AUDSEK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_AUDSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_AUDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_AUDSEK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_AUDSGD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_AUDSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_AUDSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_AUDSGD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_AUDUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_AUDUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_AUDUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_AUDUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CADCHF
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CADCHF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CADCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CADCHF
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CADDKK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CADDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CADDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CADDKK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CADJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CADJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CADJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CADJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CADNOK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CADNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CADNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CADNOK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CADNZD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CADNZD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CADNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CADNZD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CADSEK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CADSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CADSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CADSEK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CHFDKK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CHFDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CHFDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CHFDKK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CHFJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CHFJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CHFJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CHFJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CHFNOK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CHFNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CHFNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CHFNOK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CHFSEK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CHFSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CHFSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CHFSEK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_DKKJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_DKKJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_DKKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_DKKJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_DKKNOK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_DKKNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_DKKNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_DKKNOK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_DKKSEK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_DKKSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_DKKSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_DKKSEK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURAUD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURAUD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURAUD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURCAD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURCAD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURCAD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURCHF
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURCHF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURCHF
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURCNH
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURCNH_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURCNH_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURCNH
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURCZK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURCZK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURCZK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURDKK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURDKK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURGBP
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURGBP_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURGBP_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURGBP
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURHKD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURHKD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURHUF
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURHUF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURHUF
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURJPY_File01,
FileName='S:\SQLDATA\USERDB\FXtickDB\TickPartitions\TickFilegroup_EURJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURMXN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURMXN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURNOK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURNOK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURNZD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURNZD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURNZD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURPLN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURPLN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURPLN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURRUB
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURRUB_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURRUB_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURRUB
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURSEK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURSEK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURTRY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURTRY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURTRY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURUSD_File01,
FileName='S:\SQLDATA\USERDB\FXtickDB\TickPartitions\TickFilegroup_EURUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURZAR
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURZAR
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPAUD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPAUD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPAUD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPCAD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPCAD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPCAD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPCHF
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPCHF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPCHF
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPCZK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPCZK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPCZK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPDKK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPDKK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPHUF
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPHUF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPHUF
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPNOK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPNOK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPNZD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPNZD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPNZD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPPLN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPPLN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPPLN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPSEK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPSEK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_HKDJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_HKDJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_HKDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_HKDJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_MXNJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_MXNJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_MXNJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_MXNJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NOKJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NOKJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NOKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NOKJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NOKSEK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NOKSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NOKSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NOKSEK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NZDCAD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NZDCAD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NZDCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NZDCAD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NZDCHF
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NZDCHF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NZDCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NZDCHF
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NZDDKK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NZDDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NZDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NZDDKK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NZDJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NZDJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NZDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NZDJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NZDNOK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NZDNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NZDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NZDNOK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NZDSEK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NZDSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NZDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NZDSEK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NZDSGD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NZDSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NZDSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NZDSGD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NZDUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NZDUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NZDUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NZDUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_SGDJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_SGDJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_SGDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_SGDJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDCAD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDCAD_File01,
FileName='S:\SQLDATA\USERDB\FXtickDB\TickPartitions\TickFilegroup_USDCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDCAD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDCHF
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDCHF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDCHF
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDCNH
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDCNH_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDCNH_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDCNH
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDCZK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDCZK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDCZK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDDKK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDDKK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDHKD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDHKD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDHUF
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDHUF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDHUF
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDILS
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDILS_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDILS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDILS
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDMXN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDMXN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDNOK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDNOK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDPLN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDPLN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDPLN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDRUB
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDRUB_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDRUB_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDRUB
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDSEK
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDSEK
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDSGD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDSGD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDTRY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDTRY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDTRY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDZAR
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDZAR
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_ZARJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_ZARJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_ZARJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_ZARJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_EURSGD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_EURSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_EURSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_EURSGD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPHKD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPHKD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPMXN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPMXN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPSGD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPSGD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPTRY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPTRY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPTRY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GBPZAR
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GBPZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GBPZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GBPZAR
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CHFMXN
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CHFMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CHFMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CHFMXN
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_USDINR
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_USDINR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_USDINR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_USDINR
GO 

--Carefull Schema needs 1 more filegroup (for far right range)
Alter Database FXtickDB Add FileGroup TickFilegroup_FutureSymbols
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_FutureSymbols_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_FutureSymbols_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_FutureSymbols
GO 

print 'Files Added:'
print GETUTCDATE()
GO

CREATE PARTITION SCHEME TicksSymbolPartitionScheme_IndividualFileGroups
AS PARTITION MarketDataSymbolPartitioningFunction TO (TickFilegroup_EURUSD, TickFilegroup_GBPUSD, TickFilegroup_AUDJPY, TickFilegroup_AUDUSD, TickFilegroup_CHFJPY, TickFilegroup_EURAUD, TickFilegroup_EURCHF, TickFilegroup_EURGBP, TickFilegroup_EURJPY, TickFilegroup_GBPCHF, TickFilegroup_GBPJPY, TickFilegroup_NZDUSD, TickFilegroup_USDCAD, TickFilegroup_USDCHF, TickFilegroup_USDJPY, TickFilegroup_NZDJPY, TickFilegroup_CADJPY, TickFilegroup_EURCAD, TickFilegroup_EURSEK, TickFilegroup_USDDKK, TickFilegroup_USDPLN, TickFilegroup_USDNOK, TickFilegroup_USDCZK, TickFilegroup_AUDNZD, TickFilegroup_EURNOK, TickFilegroup_AUDCAD, TickFilegroup_AUDCHF, TickFilegroup_CADCHF, TickFilegroup_EURCZK, TickFilegroup_EURHUF, TickFilegroup_EURNZD, TickFilegroup_EURPLN, TickFilegroup_GBPAUD, TickFilegroup_GBPCAD, TickFilegroup_USDSEK, TickFilegroup_USDMXN, TickFilegroup_EURDKK, TickFilegroup_USDHUF, TickFilegroup_CADNZD, TickFilegroup_GBPNZD, TickFilegroup_NZDCHF, TickFilegroup_AUDDKK, TickFilegroup_AUDNOK, TickFilegroup_EURTRY, TickFilegroup_EURZAR, TickFilegroup_NOKSEK, TickFilegroup_NZDCAD, TickFilegroup_USDHKD, TickFilegroup_USDSGD, TickFilegroup_USDTRY, TickFilegroup_EURMXN, TickFilegroup_USDZAR, TickFilegroup_AUDSEK, TickFilegroup_CADDKK, TickFilegroup_CADNOK, TickFilegroup_CADSEK, TickFilegroup_CHFDKK, TickFilegroup_CHFNOK, TickFilegroup_CHFSEK, TickFilegroup_DKKJPY, TickFilegroup_DKKNOK, TickFilegroup_DKKSEK, TickFilegroup_GBPDKK, TickFilegroup_GBPNOK, TickFilegroup_GBPSEK, TickFilegroup_HKDJPY, TickFilegroup_MXNJPY, TickFilegroup_NOKJPY, TickFilegroup_NZDDKK, TickFilegroup_NZDNOK, TickFilegroup_NZDSEK, TickFilegroup_AUDSGD, TickFilegroup_ZARJPY, TickFilegroup_NZDSGD, TickFilegroup_SGDJPY, TickFilegroup_USDILS, TickFilegroup_AUDHKD, TickFilegroup_EURHKD, TickFilegroup_EURRUB, TickFilegroup_GBPCZK, TickFilegroup_GBPHUF, TickFilegroup_USDRUB, TickFilegroup_GBPPLN, TickFilegroup_EURCNH, TickFilegroup_USDCNH, TickFilegroup_EURSGD, TickFilegroup_GBPHKD, TickFilegroup_GBPMXN, TickFilegroup_GBPSGD, TickFilegroup_GBPTRY, TickFilegroup_GBPZAR, TickFilegroup_CHFMXN, TickFilegroup_USDINR, TickFilegroup_FutureSymbols);
GO

print 'Partition scheme created:'
print GETUTCDATE()
GO

sp_rename Tick__, Tick_Splitting
GO

DROP INDEX [SourceFile] ON [dbo].[Tick_Splitting]
GO

print 'Unclustered index dropped:'
print GETUTCDATE()
GO


CREATE CLUSTERED INDEX [ProviderPairTime] ON Tick_Splitting
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
)WITH (DROP_EXISTING = ON) 
ON [TicksSymbolPartitionScheme_IndividualFileGroups](FXPairID)
GO

print 'Clustered index moved:'
print GETUTCDATE()
GO

CREATE NONCLUSTERED INDEX [SourceFile] ON Tick_Splitting
(
	[SourceFileID] ASC
) 
ON [TicksSymbolPartitionScheme_IndividualFileGroups](FXPairID)
GO

print 'Unclustered index recreated:'
print GETUTCDATE()
GO

sp_rename Tick_Splitting, Tick
GO

print 'Split DONE:'
print GETUTCDATE()
GO

-- 4) Verify result:
--DECLARE @TableName sysname = 'MarketData';
--SELECT p.partition_number, fg.name, p.rows FROM sys.partitions p  INNER JOIN sys.allocation_units au  ON au.container_id = p.hobt_id  INNER JOIN sys.filegroups fg  ON fg.data_space_id = au.data_space_id WHERE p.object_id = OBJECT_ID(@TableName)


-- to release empty space:
--EXEC sp_spaceused @updateusage = N'TRUE';
--GO

--SELECT file_id, name
--FROM sys.database_files;
--GO
--DBCC SHRINKFILE (1, TRUNCATEONLY);
--if not help:
--DBCC SHRINKFILE (1);


--Check errors with
--DBCC CHECKFILEGROUP (1) WITH ALL_ERRORMSGS 
--Repair errors with:
--DBCC CHECKALLOC (FXtickDB, REPAIR_ALLOW_DATA_LOSS)

--shrink log file:
--CHECKPOINT;
--GO
--CHECKPOINT; -- run twice to ensure file wrap-around
--GO
--DBCC SHRINKFILE(Integrator_log, 200);
--GO