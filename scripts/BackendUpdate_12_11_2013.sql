
DROP TABLE [dbo].[Settings]
GO

DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.XsdSchemas]
GO


/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.XsdSchemas]    Script Date: 09. 11. 2013 14:05:16 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.XsdSchemas] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RiskManagementSettings" nillable="true" type="RiskManagementSettings" />
  <xs:complexType name="RiskManagementSettings">
    <xs:sequence>
      <xs:element name="ConfigLastUpdated"  nillable="true" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAndPosition" type="PriceAndPositionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersSubmissionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrderMaximumAllowedSizeSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedBaseAbsSizeInUsd" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="PriceAndPositionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumExternalNOPInUSD" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceDeviationCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="OnlinePriceDeviationCheckMaxPriceAge_Minutes" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="Symbols" type="ArrayOfSymbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbol">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="Symbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="Symbol">
    <xs:attribute name="ShortName" type="xs:string" />
    <xs:attribute name="NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
  </xs:complexType>
</xs:schema>

<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="BusinessLayerSettings" nillable="true" type="BusinessLayerSettings" />
  <xs:complexType name="BusinessLayerSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MarketableClientOrdersMatchingStrategy" type="MarketableClientOrdersMatchingStrategy" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="ConsecutiveRejectionsStopStrategyBehavior" type="ConsecutiveRejectionsStopStrategySettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartWithSessionsStopped" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="AutoKillInternalOrdersOnOrdersTransmissionDisabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailSubject" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailBody" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailMinDelayBetweenEmails_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="MarketableClientOrdersMatchingStrategy">
    <xs:restriction base="xs:string">
      <xs:enumeration value="ImmediateMatching" />
      <xs:enumeration value="NextFreshPriceMatching" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ConsecutiveRejectionsStopStrategySettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="StrategyEnabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="NumerOfRejectionsToTriggerStrategy" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAgeRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceBpRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultCounterpartyRank" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRanks" type="ArrayOfCounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartyRank">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartyRank" nillable="true" type="CounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartyRank">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="Rank" type="xs:decimal" />
  </xs:complexType>
</xs:schema>'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[Name] [nvarchar](100) NOT NULL,
	[SettingsXml] [xml](CONTENT [dbo].[Kreslik.Integrator.XsdSchemas]) NOT NULL,
	[LastUpdated] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


INSERT INTO [dbo].[Settings]
           ([Name]
           ,[SettingsXml]
		   ,[LastUpdated])
     VALUES
           (N'Kreslik.Integrator.RiskManagement.dll'
           ,
N'<RiskManagementSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ConfigLastUpdated>2013-11-13-1011-UTC</ConfigLastUpdated>
  <ExternalOrdersSubmissionRate>
    <RiskCheckAllowed>true</RiskCheckAllowed>
    <MaximumAllowedInstancesPerTimeInterval>1000</MaximumAllowedInstancesPerTimeInterval>
    <TimeIntervalToCheck_Seconds>5</TimeIntervalToCheck_Seconds>
  </ExternalOrdersSubmissionRate>
  <ExternalOrdersRejectionRate>
    <RiskCheckAllowed>true</RiskCheckAllowed>
    <MaximumAllowedInstancesPerTimeInterval>2000</MaximumAllowedInstancesPerTimeInterval>
    <TimeIntervalToCheck_Seconds>60</TimeIntervalToCheck_Seconds>
  </ExternalOrdersRejectionRate>
  <ExternalOrderMaximumAllowedSize>
    <RiskCheckAllowed>true</RiskCheckAllowed>
    <MaximumAllowedBaseAbsSizeInUsd>2500000</MaximumAllowedBaseAbsSizeInUsd>
  </ExternalOrderMaximumAllowedSize>
  <PriceAndPosition>
    <ExternalNOPRiskCheckAllowed>true</ExternalNOPRiskCheckAllowed>
    <MaximumExternalNOPInUSD>100000000</MaximumExternalNOPInUSD>
    <MaximumAllowedNOPCalculationTime_Milliseconds>5</MaximumAllowedNOPCalculationTime_Milliseconds>
    <PriceDeviationCheckAllowed>true</PriceDeviationCheckAllowed>
    <OnlinePriceDeviationCheckMaxPriceAge_Minutes>480</OnlinePriceDeviationCheckMaxPriceAge_Minutes>
    <ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed>true</ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed>
    <Symbols>
      <Symbol ShortName="AUDCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="AUDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="AUDHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="AUDCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="AUDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="AUDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="AUDNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="AUDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="AUDSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="AUDUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="CADDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="CADCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="CADJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="CADNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="CADNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="CADSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="DKKJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="DKKNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="DKKSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURAUD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURCNH" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURGBP" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURRUB" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURSKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURTRY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="EURZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="GBPAUD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="GBPCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="GBPCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="GBPDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="GBPHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="GBPCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="GBPJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="GBPNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="GBPNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="GBPPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="GBPSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="GBPUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="HKDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="CHFDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="CHFJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="CHFNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="CHFSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="MXNJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="NOKJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="NOKSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="NZDCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="NZDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="NZDCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="NZDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="NZDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="NZDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="NZDSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="NZDUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="SGDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDCNH" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDILS" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDRUB" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDSKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDTRY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="USDZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
      <Symbol ShortName="ZARJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" />
    </Symbols>
  </PriceAndPosition>
</RiskManagementSettings>'
,
GETUTCDATE())
GO



INSERT INTO [dbo].[Settings]
           ([Name]
           ,[SettingsXml]
		   ,[LastUpdated])
     VALUES
           (N'Kreslik.Integrator.BusinessLayer.dll'
           ,
N'<BusinessLayerSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <MarketableClientOrdersMatchingStrategy>ImmediateMatching</MarketableClientOrdersMatchingStrategy>
  <ConsecutiveRejectionsStopStrategyBehavior>
    <StrategyEnabled>true</StrategyEnabled>
    <NumerOfRejectionsToTriggerStrategy>2</NumerOfRejectionsToTriggerStrategy>
    <CounterpartyRankWeightMultiplier>20</CounterpartyRankWeightMultiplier>
    <PriceAgeRankWeightMultiplier>1</PriceAgeRankWeightMultiplier>
    <PriceBpRankWeightMultiplier>20</PriceBpRankWeightMultiplier>
    <DefaultCounterpartyRank>7</DefaultCounterpartyRank>
    <CounterpartyRanks>
      <CounterpartyRank CounterpartyCode="UBS" Rank="5" />
      <CounterpartyRank CounterpartyCode="MGS" Rank="5.2" />
      <CounterpartyRank CounterpartyCode="RBS" Rank="5.5" />
      <CounterpartyRank CounterpartyCode="CTI" Rank="5.8" />
      <CounterpartyRank CounterpartyCode="CRS" Rank="6.1" />
      <CounterpartyRank CounterpartyCode="BOA" Rank="6.8" />
      <CounterpartyRank CounterpartyCode="CZB" Rank="7.1" />
      <CounterpartyRank CounterpartyCode="GLS" Rank="7.4" />
      <CounterpartyRank CounterpartyCode="JPM" Rank="7.7" />
      <CounterpartyRank CounterpartyCode="BNP" Rank="8.9" />
      <CounterpartyRank CounterpartyCode="BRX" Rank="9.5" />
      <CounterpartyRank CounterpartyCode="SOC" Rank="9.5" />
      <CounterpartyRank CounterpartyCode="HSB" Rank="10" />
      <CounterpartyRank CounterpartyCode="NOM" Rank="10" />
    </CounterpartyRanks>
  </ConsecutiveRejectionsStopStrategyBehavior>
  <StartWithSessionsStopped>false</StartWithSessionsStopped>
  <AutoKillInternalOrdersOnOrdersTransmissionDisabled>true</AutoKillInternalOrdersOnOrdersTransmissionDisabled>
 
  <StartStopEmailSubject>Integrator service on {0} is {1}</StartStopEmailSubject>
  <StartStopEmailBody></StartStopEmailBody>
  <StartStopEmailTo>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</StartStopEmailTo>
  <StartStopEmailCc></StartStopEmailCc>
  <IgnoredOrderEmailTo>michal.kreslik@kgtinv.com;marek.fogiel@kgtinv.com</IgnoredOrderEmailTo>
  <IgnoredOrderEmailCc>jan.krivanek@kgtinv.com</IgnoredOrderEmailCc>
  <FatalErrorsEmailTo>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</FatalErrorsEmailTo>
  <FatalErrorsEmailCc></FatalErrorsEmailCc>
  <FatalErrorsEmailMinDelayBetweenEmails_Minutes>10</FatalErrorsEmailMinDelayBetweenEmails_Minutes>
</BusinessLayerSettings>'
,
GETUTCDATE())
GO


ALTER TABLE [dbo].[Symbol] DROP COLUMN [ExamplePrice]
GO

ALTER TABLE [dbo].[Symbol] ADD LastKnownMeanReferencePrice decimal(18,9) NULL
GO

ALTER TABLE [dbo].[Symbol] ADD MeanReferencePriceUpdated datetime2
GO

CREATE PROC [dbo].[UpdateSymbolReferencePrice_SP] (
	@SymbolName NCHAR(7),
	@Price decimal(18,9)
	)
AS
BEGIN
	UPDATE [dbo].[Symbol]
	SET
      [LastKnownMeanReferencePrice] = @Price
      ,[MeanReferencePriceUpdated] = GETUTCDATE()
	WHERE 
		[Name] = @SymbolName
END
GO

GRANT EXECUTE ON [dbo].[UpdateSymbolReferencePrice_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE PROC [dbo].[GetSymbolReferencePrices_SP]
AS
BEGIN
	SELECT 
		[Name]
		,[LastKnownMeanReferencePrice]
		,[MeanReferencePriceUpdated]
	FROM 
		[dbo].[Symbol]
END

GRANT EXECUTE ON [dbo].[GetSymbolReferencePrices_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'AUD/CAD', @Price= '0.97843'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'AUD/DKK', @Price= '5.19556'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'AUD/HKD', @Price= '7.23431'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'AUD/CHF', @Price= '0.8592'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'AUD/JPY', @Price= '92.882'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'AUD/NOK', @Price= '5.74485'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'AUD/NZD', @Price= '1.134215'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'AUD/SEK', @Price= '6.13195'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'AUD/SGD', @Price= '1.16467'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'AUD/USD', @Price= '0.93307'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'CAD/DKK', @Price= '5.3104'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'CAD/CHF', @Price= '0.878195'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'CAD/JPY', @Price= '94.9315'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'CAD/NOK', @Price= '5.8716'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'CAD/NZD', @Price= '1.1592415'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'CAD/SEK', @Price= '6.26705'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'DKK/JPY', @Price= '17.878'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'DKK/NOK', @Price= '1.105655'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'DKK/SEK', @Price= '1.18012'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/AUD', @Price= '1.43555'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/CAD', @Price= '1.40458'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/CNH', @Price= '8.1425'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/CZK', @Price= '27.024'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/DKK', @Price= '7.458545'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/GBP', @Price= '0.83867'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/HKD', @Price= '10.385275'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/HUF', @Price= '298.4935'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/CHF', @Price= '1.233385'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/JPY', @Price= '133.335'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/MXN', @Price= '17.71595'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/NOK', @Price= '8.24504'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/NZD', @Price= '1.62821'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/PLN', @Price= '4.214645'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/RUB', @Price= '44.02065'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/SEK', @Price= '8.801825'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/SKK', @Price= '30.1258'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/TRY', @Price= '2.73764'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/USD', @Price= '1.3395'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'EUR/ZAR', @Price= '13.90705'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'GBP/AUD', @Price= '1.711675'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'GBP/CAD', @Price= '1.67478'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'GBP/CZK', @Price= '32.21875'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'GBP/DKK', @Price= '8.89335'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'GBP/HUF', @Price= '355.91'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'GBP/CHF', @Price= '1.4707'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'GBP/JPY', @Price= '158.987'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'GBP/NOK', @Price= '9.8328'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'GBP/NZD', @Price= '1.94137'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'GBP/PLN', @Price= '5.02473'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'GBP/SEK', @Price= '10.494435'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'GBP/USD', @Price= '1.59715'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'HKD/JPY', @Price= '12.839685'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'CHF/DKK', @Price= '6.046975'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'CHF/JPY', @Price= '108.1045'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'CHF/NOK', @Price= '6.685805'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'CHF/SEK', @Price= '7.13601'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'MXN/JPY', @Price= '7.52665'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'NOK/JPY', @Price= '16.1682'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'NOK/SEK', @Price= '1.067315'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'NZD/CAD', @Price= '0.86272'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'NZD/DKK', @Price= '4.58082'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'NZD/CHF', @Price= '0.75753'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'NZD/JPY', @Price= '81.8935'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'NZD/NOK', @Price= '5.06505'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'NZD/SEK', @Price= '5.4062'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'NZD/SGD', @Price= '1.0269'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'NZD/USD', @Price= '0.82264'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'SGD/JPY', @Price= '79.751'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/CAD', @Price= '1.04857'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/CNH', @Price= '6.079235'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/CZK', @Price= '20.1734'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/DKK', @Price= '5.568325'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/HKD', @Price= '7.75306'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/HUF', @Price= '222.849'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/CHF', @Price= '0.920845'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/ILS', @Price= '3.53585'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/JPY', @Price= '99.5445'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/MXN', @Price= '13.2263'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/NOK', @Price= '6.1574'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/PLN', @Price= '3.145725'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/RUB', @Price= '32.8849'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/SEK', @Price= '6.572135'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/SGD', @Price= '1.24821'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/SKK', @Price= '22.2791'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/TRY', @Price= '2.04416'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'USD/ZAR', @Price= '10.38605'
exec [UpdateSymbolReferencePrice_SP] @SymbolName = 'ZAR/JPY', @Price= '9.59'
GO

ALTER TABLE [dbo].[DealExternalExecuted] ADD AmountTermPolExecutedInUsd decimal(18, 0) NULL
GO

ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolExecuted decimal(18, 0),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END

	INSERT INTO 
	[dbo].[DealExternalExecuted]
           ([IntegratorReceivedExecutionReportUtc]
           ,[CounterpartyId]
           ,[SymbolId]
           ,[IntegratorExternalOrderPrivateId]
           ,[DealDirectionId]
           ,[AmountBasePolExecuted]
		   ,[AmountTermPolExecutedInUsd]
           ,[Price]
           ,[CounterpartyTransactionId]
           ,[CounterpartySuppliedExecutionTimeStampUtc]
           ,[ValueDateCounterpartySuppliedLocMktDate]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[CounterpartySentExecutionReportUtc])
     VALUES
           (@ExecutionReportReceivedUtc
           ,@CounterpartyId
           ,@SymbolId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolExecuted
		   ,@AmountTermPolExecutedInUsd
           ,@Price
           ,@CounterpartyTransactionId
           ,@CounterpartyExecutionReportTimeStampUtc
           ,@ValueDateCounterpartySuppliedLocMktDate
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@CounterpartySentExecutionReportUtc)
END


ALTER TABLE [dbo].[DealExternalExecuted] ADD DatePartIntegratorReceivedExecutionReportUtc AS CONVERT(DATE, [IntegratorReceivedExecutionReportUtc]) PERSISTED
GO

SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO

/****** Object:  Index [IX_DatePart_Symbol]    Script Date: 15. 11. 2013 16:24:00 ******/
CREATE NONCLUSTERED INDEX [IX_DatePart_Symbol] ON [dbo].[DealExternalExecuted]
(
	[DatePartIntegratorReceivedExecutionReportUtc] ASC,
	[SymbolId] ASC
)
INCLUDE ( 	[AmountBasePolExecuted],
	[Price],
	[AmountTermPolExecutedInUsd],
	[CounterpartyId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


CREATE PROCEDURE [dbo].[GetNopsPolSidedPerPair_SP] (
	@Day DATE
	)
AS
BEGIN
	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]
END
GO

GRANT EXECUTE ON [dbo].[GetNopsPolSidedPerPair_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetNopsPolSidedPerCurrency_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,0),
		NopTermPol decimal(18,0),
		NopBasePolInUSD decimal(18,2),
		NopTermPolInUSD decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol, NopBasePolInUSD, NopTermPolInUSD)
	SELECT
		symbol.[Id] AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
		SUM(-deal.[AmountTermPolExecutedInUsd]) AS NopBasePolInUsd,
		SUM(deal.[AmountTermPolExecutedInUsd]) AS NopTermPolInUsd
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Id]


	SELECT
		CCYPositions.CCY AS CCY,
		SUM(CCYPositions.NopPol) AS NopPol,
		SUM(CCYPositions.NopPolInUsd) AS NopPolInUsd
	FROM
		(
		SELECT 
			ccy1.CurrencyAlphaCode AS CCY,
			basePos.NopBasePol AS NopPol,
			basePos.NopBasePolInUSD AS NopPolInUsd
		FROM
			Currency ccy1
			INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
			INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

		UNION ALL

		SELECT 
			ccy2.CurrencyAlphaCode AS CCY,
			termPos.NopTermPol AS NopPol,
			termPos.NopTermPolInUSD AS NopPolInUsd
		FROM
			Currency ccy2
			INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
			INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
		) AS CCYPositions
	GROUP BY
		CCYPositions.CCY
	ORDER BY
		CCYPositions.CCY
END
GO

GRANT EXECUTE ON [dbo].[GetNopsPolSidedPerCurrency_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO




CREATE PROCEDURE [dbo].[GetNopsAbsPerCounterparty_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePolInUSD decimal(18,2),
		NopTermPolInUSD decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePolInUSD, NopTermPolInUSD)
	SELECT
		symbol.[Id] AS SymbolId,
		deal.CounterpartyId,
		SUM(-deal.[AmountTermPolExecutedInUsd]) AS NopBasePolInUsd,
		SUM(deal.[AmountTermPolExecutedInUsd]) AS NopTermPolInUsd
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
	GROUP BY
		symbol.[Id],
		deal.CounterpartyId
		


	SELECT
		CtpCCYPositions.Counterparty AS Counterparty,
		SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
	FROM
		(
		SELECT
			ctp.CounterpartyCode AS Counterparty,
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				basePos.CounterpartyId,
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePolInUSD AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT 
				termPos.CounterpartyId,
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPolInUSD AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
			INNER JOIN Counterparty ctp ON ctp.CounterpartyId = CCYPositions.CounterpartyId
		GROUP BY
			ctp.CounterpartyCode
			,CCYPositions.CCY
		) AS CtpCCYPositions
	GROUP BY
		CtpCCYPositions.Counterparty
	ORDER BY
		CtpCCYPositions.Counterparty
END
GO

GRANT EXECUTE ON [dbo].[GetNopsAbsPerCounterparty_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO





CREATE PROCEDURE [dbo].[GetNopAbsTotal_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePolInUSD decimal(18,2),
		NopTermPolInUSD decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePolInUSD, NopTermPolInUSD)
	SELECT
		symbol.[Id] AS SymbolId,
		SUM(-deal.[AmountTermPolExecutedInUsd]) AS NopBasePolInUsd,
		SUM(deal.[AmountTermPolExecutedInUsd]) AS NopTermPolInUsd
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Id]


	SELECT
		SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
	FROM
		(
		SELECT
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePolInUSD AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPolInUSD AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
		GROUP BY
			CCYPositions.CCY
		) AS CtpCCYPositions

END
GO

GRANT EXECUTE ON [dbo].[GetNopAbsTotal_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


