
print 'Started:'
print GETUTCDATE()
GO

Alter Database Integrator Add FileGroup LargeSlowDataFileGroup
GO

--Z = scratch; T = raid
Alter Database Integrator Add File
(
Name=LargeSlowDataFileGroup_File01,
FileName='Z:\SQLDATA\USERDB\Integrator\LargeSlowDataFileGroup_File01.ndf',
Size=100GB,
FileGrowth=10%
) To Filegroup LargeSlowDataFileGroup
GO

print 'FileGroup added:'
print GETUTCDATE()
GO

sp_rename ExecutionReportMessage, ExecutionReportMessage_Moving
GO

DROP INDEX [IX_ExecutionReportMessage_IntegratorReceivedExecutionReportUtc] ON [dbo].[ExecutionReportMessage_Moving]
GO

print 'Nonclustered index dropped:'
print GETUTCDATE()
GO


CREATE CLUSTERED INDEX [IX_ExecutionReportMessage_OrderId] ON [dbo].[ExecutionReportMessage_Moving]
(
	[IntegratorExternalOrderPrivateId] ASC
) WITH (DROP_EXISTING = ON) 
ON [LargeSlowDataFileGroup]
GO

print 'Clustered index moved:'
print GETUTCDATE()
GO


-- nonclustered index - not important
CREATE NONCLUSTERED INDEX [IX_ExecutionReportMessage_IntegratorReceivedExecutionReportUtc] ON [dbo].[ExecutionReportMessage_Moving]
(
	[IntegratorReceivedExecutionReportUtc] ASC
) ON [LargeSlowDataFileGroup]
GO

print 'Unclustered index recreated:'
print GETUTCDATE()
GO

sp_rename ExecutionReportMessage_Moving, ExecutionReportMessage
print 'DONE'


-- DROP FKs to OrderExternal ------


ALTER TABLE [dbo].[DealExternalExecuted] DROP CONSTRAINT [FK_DealExternalExecuted_OrderExternal]
GO
ALTER TABLE [dbo].[DealExternalRejected] DROP CONSTRAINT [FK_RejectedDealExternal_OrderExternal]
GO
ALTER TABLE [dbo].[ExecutionReportMessage] DROP CONSTRAINT [FK_ExecutionReportMessage_OrderExternal]
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest] DROP CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternal]
GO
ALTER TABLE [dbo].[OrderExternalCancelResult] DROP CONSTRAINT [FK_OrderExternalCancelResult_OrderExternal]
GO
ALTER TABLE [dbo].[OrderExternalIgnored] DROP CONSTRAINT [FK_OrderExternalIgnored_OrderExternal]
GO

--------

print 'OrderExternalCancelRequest started:'
print GETUTCDATE()
GO


sp_rename OrderExternalCancelRequest, OrderExternalCancelRequest_Moving
GO

DROP INDEX [IX_OrderExternalCancelRequest_IntegratorSentCancelRequestUtc] ON [dbo].[OrderExternalCancelRequest_Moving]
GO

print 'Nonclustered index dropped:'
print GETUTCDATE()
GO


CREATE CLUSTERED INDEX [IX_OrderExternalCancelRequest_OrderId] ON [dbo].[OrderExternalCancelRequest_Moving]
(
	[IntegratorExternalOrderPrivateId] ASC
) WITH (DROP_EXISTING = ON) 
ON [LargeSlowDataFileGroup]
GO

print 'Clustered index moved:'
print GETUTCDATE()
GO


-- nonclustered index - not important
CREATE NONCLUSTERED INDEX [IX_OrderExternalCancelRequest_IntegratorSentCancelRequestUtc] ON [dbo].[OrderExternalCancelRequest_Moving]
(
	[IntegratorSentCancelRequestUtc] ASC
) ON [LargeSlowDataFileGroup]
GO

print 'Unclustered index recreated:'
print GETUTCDATE()
GO

sp_rename OrderExternalCancelRequest_Moving, OrderExternalCancelRequest


------

print 'OrderExternal started:'
print GETUTCDATE()
GO


sp_rename OrderExternal, OrderExternal_Moving
GO

DROP INDEX [IX_OrderExternal_OrderId] ON [dbo].[OrderExternal_Moving]
GO

print 'Nonclustered index dropped:'
print GETUTCDATE()
GO


CREATE CLUSTERED INDEX [IX_OrderExternal_IntegratorSentExternalOrderUtc] ON [dbo].[OrderExternal_Moving]
(
	[IntegratorSentExternalOrderUtc] ASC
) WITH (DROP_EXISTING = ON) 
ON [LargeSlowDataFileGroup]
GO

print 'Clustered index moved:'
print GETUTCDATE()
GO


-- nonclustered index - not important
CREATE NONCLUSTERED INDEX [IX_OrderExternal_OrderId] ON [dbo].[OrderExternal_Moving]
(
	[IntegratorExternalOrderPrivateId] ASC
) ON [LargeSlowDataFileGroup]
GO

print 'Unclustered index recreated:'
print GETUTCDATE()
GO

sp_rename OrderExternal_Moving, OrderExternal


------

print 'OrderExternalCancelResult started:'
print GETUTCDATE()
GO


sp_rename OrderExternalCancelResult, OrderExternalCancelResult_Moving
GO

DROP INDEX [IX_OrderExternalCancelResult_CounterpartySentResultUtc] ON [dbo].[OrderExternalCancelResult_Moving]
GO

print 'Nonclustered index dropped:'
print GETUTCDATE()
GO


CREATE CLUSTERED INDEX [IX_OrderExternalCancelResult_OrderId] ON [dbo].[OrderExternalCancelResult_Moving]
(
	[IntegratorExternalOrderPrivateId] ASC
) WITH (DROP_EXISTING = ON) 
ON [LargeSlowDataFileGroup]
GO

print 'Clustered index moved:'
print GETUTCDATE()
GO


-- nonclustered index - not important
CREATE NONCLUSTERED INDEX [IX_OrderExternalCancelResult_CounterpartySentResultUtc] ON [dbo].[OrderExternalCancelResult_Moving]
(
	[CounterpartySentResultUtc] ASC
) ON [LargeSlowDataFileGroup]
GO

print 'Unclustered index recreated:'
print GETUTCDATE()
GO

sp_rename OrderExternalCancelResult_Moving, OrderExternalCancelResult


------

print 'AggregatedExternalExecutedTicket started:'
print GETUTCDATE()
GO


sp_rename AggregatedExternalExecutedTicket, AggregatedExternalExecutedTicket_Moving
GO


CREATE CLUSTERED INDEX [IX_AggregatedExternalExecutedTicket_DatePartIntegratorReceivedTimeUtc] ON [dbo].[AggregatedExternalExecutedTicket_Moving]
(
	[DatePartIntegratorReceivedTimeUtc] ASC
) WITH (DROP_EXISTING = ON) 
ON [PRIMARY]
GO

print 'Clustered index moved:'
print GETUTCDATE()
GO

sp_rename AggregatedExternalExecutedTicket_Moving, AggregatedExternalExecutedTicket


------

print 'DealExternalExecutedTicket started:'
print GETUTCDATE()
GO


sp_rename DealExternalExecutedTicket, DealExternalExecutedTicket_Moving
GO


CREATE CLUSTERED INDEX [IX_DealExternalExecutedTicket_DatePartIntegratorSentOrReceivedTimeUtc] ON [dbo].[DealExternalExecutedTicket_Moving]
(
	[DatePartIntegratorSentOrReceivedTimeUtc] ASC
) WITH (DROP_EXISTING = ON) 
ON [PRIMARY]
GO

print 'Clustered index moved:'
print GETUTCDATE()
GO

sp_rename DealExternalExecutedTicket_Moving, DealExternalExecutedTicket



print 'DONE'