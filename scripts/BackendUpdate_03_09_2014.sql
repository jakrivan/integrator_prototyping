


CREATE TABLE [dbo].[TradingSystemDealsRejected_Archive](
	[TradingSystemId] [int] NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[AmountBasePolRejected] [decimal](18,0) NOT NULL,
	[DatePartDealRejected] [date] NOT NULL
) ON [LargeSlowDataFileGroup]
GO

CREATE CLUSTERED INDEX [IX_TradingSystemDealsRejected_date] ON [dbo].[TradingSystemDealsRejected_Archive]
(
	[DatePartDealRejected] ASC
) ON [LargeSlowDataFileGroup]
GO

CREATE TABLE [dbo].[TradingSystemDealsRejected_Daily](
	[TradingSystemId] [int] NOT NULL,
	[AmountBasePolRejected] [decimal](18,0) NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[RejectedPrice] [decimal](18,9) NOT NULL,
	[RejectionReason] [nvarchar](64) NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL
) ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [IX_TradingSystemDealsRejected_TradingSystemSymbol] ON [dbo].[TradingSystemDealsRejected_Daily]
(
	[TradingSystemId] ASC,
	[SymbolId] ASC
) ON [PRIMARY]
GO


CREATE PROCEDURE [dbo].[Daily_ArchiveTradingSystemRejections_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[TradingSystemDealsRejected_Archive]
			([TradingSystemId],
			[IntegratorExternalOrderPrivateId],
			[AmountBasePolRejected],
			[DatePartDealRejected])
		SELECT
			[TradingSystemId],
			[IntegratorExternalOrderPrivateId],
			[AmountBasePolRejected],
			DATEADD(DAY, -1, GETUTCDATE())
		FROM
			[dbo].[TradingSystemDealsRejected_Daily]	
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[TradingSystemDealsRejected_Daily]
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

USE msdb ;
GO

EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'ArchiveTradingSystemRejections',
    @subsystem = N'TSQL',
    @command = N'exec Daily_ArchiveTradingSystemRejections_SP',
	@database_name = N'Integrator',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO

--MANUALY reorder the steps - there is no other documented way than doing this through SSMS


USE INTEGRATOR;
GO



ALTER PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 0),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@RejectedPrice decimal(18, 9),
	@SingleRejectionSystemId int = NULL,
	@MultipleRejectionsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END

	-- this is major case (one external rejection for one internal order)
	IF @SingleRejectionSystemId IS NOT NULL
	BEGIN
		INSERT INTO
		[dbo].[TradingSystemDealsRejected_Daily]
			([TradingSystemId]
			,[AmountBasePolRejected]
			,[SymbolId]
			,[CounterpartyId]
			,[RejectedPrice]
			,[RejectionReason]
			,[IntegratorExternalOrderPrivateId])
		VALUES
			(@SingleRejectionSystemId
			,@AmountBasePolRejected
			,@SymbolId
			,@CounterpartyId
			,@RejectedPrice
			,@RejectionReason
			,@InternalOrderId)
	END
	ELSE IF @MultipleRejectionsSystemIdsAndAmountsBaseAbs IS NOT NULL
	BEGIN
		INSERT INTO
		[dbo].[TradingSystemDealsRejected_Daily]
			([TradingSystemId]
			,[AmountBasePolRejected]
			,[SymbolId]
			,[CounterpartyId]
			,[RejectedPrice]
			,[RejectionReason]
			,[IntegratorExternalOrderPrivateId])
		SELECT
			intpair.num1
			,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
			,@SymbolId
			,@CounterpartyId
			,@RejectedPrice
			,@RejectionReason
			,@InternalOrderId
		FROM
		[dbo].[csvlist_to_intpairs_tbl] (@MultipleRejectionsSystemIdsAndAmountsBaseAbs) intpair
	END

	INSERT INTO 
	[dbo].[DealExternalRejected]
           ([IntegratorReceivedExecutionReportUtc]
           ,[SymbolId]
           ,[CounterpartyId]
           ,[IntegratorExternalOrderPrivateId]
		   ,[DealDirectionId]
           ,[AmountBasePolRejected]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[IntegratorSentExternalOrderUtc]
           ,[RejectionReason]
		   ,[CounterpartySentExecutionReportUtc]
		   ,[RejectedPrice])
     VALUES
           (@ExecutionReportReceivedUtc
		   ,@SymbolId
           ,@CounterpartyId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolRejected
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@QuoteReceivedToOrderSentInternalLatency
		   ,@IntegratorSentExternalOrderUtc
		   ,@RejectionReason
		   ,@CounterpartySentExecutionReportUtc
		   ,@RejectedPrice)
END
GO







DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_DBK" type="FIXChannel_DBKSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM2" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC1" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC2" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
      <xs:element minOccurs="1" maxOccurs="1" name="STP" type="STPSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_DBKSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DealConfirmationSupported" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumDealConfirmationDelay_Milliseconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IocApplyTtl" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ApplyMaxHoldTime" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxHoldTime" type="FXCMMaxHoldTime" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXSTPChannel_CTIPBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNongenuineInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialRejectionsHandling" type="ArrayOfSpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SpecialRejection" type="SpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="TriggerPhrase" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="FXCMMaxHoldTime">
    <xs:restriction base="xs:string">
      <xs:enumeration value="UpTo1ms" />
      <xs:enumeration value="UpTo30ms" />
      <xs:enumeration value="UpTo100ms" />
      <xs:enumeration value="UpTo500ms" />
      <xs:enumeration value="UpTo3000ms" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="MarketSessionIntoSeparateProcess" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="STPDestinations" type="ArrayOfSTPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSTPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="STPDestination" nillable="true" type="STPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Name" type="STPCounterparty" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedTicketTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="ServedCounterparties" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="STPCounterparty">
    <xs:restriction base="xs:string">
      <xs:enumeration value="CTIPB" />
      <xs:enumeration value="Traiana" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO


-- Update manually SessionSettings.xml:
--add (after crs):
--<FIXChannel_DBK>
--	<Account>EU0097916</Account>
--</FIXChannel_DBK>


INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FC2'), 'MM')

UPDATE 
	[dbo].[Dictionary_FIX]
SET
	[DictionaryXml] = N'<fix major="4" minor="4">
  <header>
    <field name="BeginString" required="Y" />
    <field name="BodyLength" required="Y" />
    <field name="MsgSeqNum" required="Y" />
    <field name="MsgType" required="Y" />
    <field name="SenderCompID" required="Y" />
    <field name="SendingTime" required="Y" />
    <field name="TargetCompID" required="Y" />
    <field name="OrigSendingTime" required="N" />
    <field name="DeliverToCompID" required="N" />
    <field name="PossDupFlag" required="N" />
    <field name="PossResend" required="N" />
  </header>
  <trailer>
    <field name="CheckSum" required="Y" />
  </trailer>
  <messages>
    <message name="Heartbeat" msgtype="0" msgcat="admin">
      <field name="TestReqID" required="N" />
    </message>
    <message name="Logon" msgtype="A" msgcat="admin">
      <field name="EncryptMethod" required="Y" />
      <field name="HeartBtInt" required="Y" />
      <field name="ResetSeqNumFlag" required="N" />
    </message>
    <message name="TestRequest" msgtype="1" msgcat="admin">
      <field name="TestReqID" required="Y" />
    </message>
    <message name="ResendRequest" msgtype="2" msgcat="admin">
      <field name="BeginSeqNo" required="Y" />
      <field name="EndSeqNo" required="Y" />
    </message>
    <message name="Reject" msgtype="3" msgcat="admin">
      <field name="RefSeqNum" required="Y" />
      <field name="RefTagID" required="N" />
      <field name="RefMsgType" required="N" />
      <field name="SessionRejectReason" required="N" />
      <field name="Text" required="N" />
    </message>
    <message name="SequenceReset" msgtype="4" msgcat="admin">
      <field name="GapFillFlag" required="N" />
      <field name="NewSeqNo" required="Y" />
    </message>
    <message name="Logout" msgtype="5" msgcat="admin">
      <field name="Text" required="N" />
    </message>
    <message name="BusinessMessageReject" msgtype="j" msgcat="app">
      <field name="RefSeqNum" required="N" />
      <field name="RefMsgType" required="Y" />
      <field name="BusinessRejectRefID" required="N" />
      <field name="BusinessRejectReason" required="Y" />
      <field name="Text" required="N" />
      <field name="EncodedTextLen" required="N" />
      <field name="EncodedText" required="N" />
      <field name="ROOT_ORD_ID" required="N" />
    </message>
    <message name="MarketDataRequest" msgtype="V" msgcat="app">
      <field name="MDReqID" required="Y" />
      <field name="SubscriptionRequestType" required="Y" />
      <field name="MarketDepth" required="Y" />
      <field name="MDUpdateType" required="N" />
      <field name="AggregatedBook" required="N" />
      <field name="OpenCloseSettleFlag" required="N" />
      <field name="Scope" required="N" />
      <field name="MDImplicitDelete" required="N" />
      <group name="NoMDEntryTypes" required="Y">
        <field name="MDEntryType" required="Y" />
      </group>
      <group name="NoRelatedSym" required="Y">
        <component name="Instrument" required="Y" />
      </group>
      <group name="NoTradingSessions" required="N">
        <field name="TradingSessionID" required="N" />
        <field name="TradingSessionSubID" required="N" />
      </group>
      <field name="ORIGINATOR_ACC" required="N" />
    </message>
    <message name="MarketDataSnapshotFullRefresh" msgtype="W" msgcat="app">
      <field name="MDReqID" required="N" />
      <field name="TICK_SIZE" required="N" />
      <field name="Symbol" required="Y" />
      <group name="NoMDEntries" required="Y">
        <field name="MDEntryType" required="Y" />
        <field name="MDEntryPx" required="N" />
        <field name="Currency" required="N" />
        <field name="MDEntrySize" required="N" />
		<field name="QuoteEntryId" required="N" />
        <field name="SETTL_DATE" required="N" />
      </group>
    </message>
    <message name="MarketDataRequestReject" msgtype="Y" msgcat="app">
      <field name="MDReqID" required="Y" />
      <field name="MDReqRejReason" required="N" />
      <field name="Text" required="N" />
    </message>
    <message name="NewOrderSingle" msgtype="D" msgcat="app">
      <field name="ClOrdID" required="Y" />
      <field name="SecondaryClOrdID" required="N" />
      <field name="ClOrdLinkID" required="N" />
      <field name="TradeOriginationDate" required="N" />
      <field name="Account" required="N" />
      <field name="AccountType" required="N" />
      <field name="DayBookingInst" required="N" />
      <field name="BookingUnit" required="N" />
      <field name="PreallocMethod" required="N" />
      <field name="SettlmntTyp" required="N" />
      <field name="FutSettDate" required="N" />
      <field name="CashMargin" required="N" />
      <field name="ClearingFeeIndicator" required="N" />
      <field name="HandlInst" required="Y" />
      <field name="ExecInst" required="N" />
      <field name="MinQty" required="N" />
      <field name="MaxFloor" required="N" />
      <field name="ExDestination" required="N" />
      <field name="ProcessCode" required="N" />
      <component name="Instrument" required="Y" />
      <field name="PrevClosePx" required="N" />
      <field name="Side" required="Y" />
      <field name="LocateReqd" required="N" />
      <field name="TransactTime" required="N" />
      <field name="QuantityType" required="N" />
      <field name="OrderQty" required="N" />
      <field name="OrdType" required="Y" />
      <field name="PriceType" required="N" />
      <field name="Price" required="N" />
      <field name="StopPx" required="N" />
      <field name="Currency" required="N" />
      <field name="ComplianceID" required="N" />
      <field name="SolicitedFlag" required="N" />
      <field name="IOIid" required="N" />
      <field name="QuoteID" required="N" />
      <field name="TimeInForce" required="N" />
      <field name="EffectiveTime" required="N" />
      <field name="ExpireDate" required="N" />
      <field name="ExpireTime" required="N" />
      <field name="GTBookingInst" required="N" />
      <field name="OrderCapacity" required="N" />
      <field name="OrderRestrictions" required="N" />
      <field name="CustOrderCapacity" required="N" />
      <field name="Rule80A" required="N" />
      <field name="ForexReq" required="N" />
      <field name="SettlCurrency" required="N" />
      <field name="Text" required="N" />
      <field name="EncodedTextLen" required="N" />
      <field name="EncodedText" required="N" />
      <field name="FutSettDate2" required="N" />
      <field name="OrderQty2" required="N" />
      <field name="Price2" required="N" />
      <field name="PositionEffect" required="N" />
      <field name="CoveredOrUncovered" required="N" />
      <field name="MaxShow" required="N" />
      <field name="PegDifference" required="N" />
      <field name="DiscretionInst" required="N" />
      <field name="DiscretionOffset" required="N" />
      <field name="CancellationRights" required="N" />
      <field name="MoneyLaunderingStatus" required="N" />
      <field name="RegistID" required="N" />
      <field name="Designation" required="N" />
    </message>
    <message name="ExecutionReport" msgtype="8" msgcat="app">
      <field name="Account" required="N" />
      <field name="AvgPx" required="N" />
      <field name="ClOrdID" required="N" />
      <field name="CumQty" required="N" />
      <field name="Currency" required="N" />
      <field name="ExecID" required="Y" />
      <field name="OrderID" required="Y" />
      <field name="HandlInst" required="N" />
      <field name="OrdType" required="N" />
		<field name="FutSettDate" required="N" />
		<field name="TradeDate" required="N" />
      <field name="TimeInForce" required="N" />
      <field name="LastPx" required="N" />
      <field name="LastQty" required="N" />
      <field name="OrderQty" required="N" />
      <field name="OrdStatus" required="Y" />
      <field name="Price" required="N" />
      <field name="Side" required="Y" />
      <field name="Symbol" required="Y" />
      <field name="Text" required="N" />
      <field name="FutSettDate" required="N" />
      <field name="StopPx" required="N" />
      <field name="OrdRejReason" required="N" />
      <field name="ExpireTime" required="N" />
      <field name="ExecType" required="Y" />
      <field name="LeavesQty" required="N" />
	  <field name="TransactTime" required="N" />
      <field name="LastSpotRate" required="N" />
      <field name="QUOTED_QUANTITY" required="N" />
      <field name="ORD_STATUS_REQ_ID" required="N" />
    </message>
  </messages>
  <components>
    <component name="Instrument">
      <field name="Symbol" required="Y" />
      <field name="CURRENCY" required="N" />
      <field name="ORDER_QTY" required="N" />
      <field name="TENOR_VALUE" required="N" />
    </component>
  </components>
  <fields>
    <field number="1" name="Account" type="STRING" />
    <field number="6" name="AvgPx" type="PRICE" />
    <field number="7" name="BeginSeqNo" type="SEQNUM" />
    <field number="8" name="BeginString" type="STRING" />
    <field number="9" name="BodyLength" type="LENGTH" />
    <field number="10" name="CheckSum" type="STRING" />
    <field number="11" name="ClOrdID" type="STRING" />
    <field number="14" name="CumQty" type="QTY" />
    <field number="15" name="Currency" type="CURRENCY" />
    <field number="16" name="EndSeqNo" type="SEQNUM" />
    <field number="17" name="ExecID" type="STRING" />
    <field number="18" name="ExecInst" type="MULTIPLEVALUESTRING">
      <value enum="1" description="NOT_HELD" />
      <value enum="2" description="WORK" />
      <value enum="3" description="GO_ALONG" />
      <value enum="4" description="OVER_THE_DAY" />
      <value enum="5" description="HELD" />
      <value enum="6" description="PARTICIPATE_DONT_INITIATE" />
      <value enum="7" description="STRICT_SCALE" />
      <value enum="8" description="TRY_TO_SCALE" />
      <value enum="9" description="STAY_ON_BIDSIDE" />
      <value enum="0" description="STAY_ON_OFFERSIDE" />
      <value enum="A" description="NO_CROSS" />
      <value enum="B" description="OK_TO_CROSS" />
      <value enum="C" description="CALL_FIRST" />
      <value enum="D" description="PERCENT_OF_VOLUME" />
      <value enum="E" description="DO_NOT_INCREASE" />
      <value enum="F" description="DO_NOT_REDUCE" />
      <value enum="G" description="ALL_OR_NONE" />
      <value enum="H" description="REINSTATE_ON_SYSTEM_FAILURE" />
      <value enum="I" description="INSTITUTIONS_ONLY" />
      <value enum="J" description="REINSTATE_ON_TRADING_HALT" />
      <value enum="K" description="CANCEL_ON_TRADING_HALT" />
      <value enum="L" description="LAST_PEG" />
      <value enum="M" description="MID_PRICE_PEG" />
      <value enum="N" description="NON_NEGOTIABLE" />
      <value enum="O" description="OPENING_PEG" />
      <value enum="P" description="MARKET_PEG" />
      <value enum="Q" description="CANCEL_ON_SYSTEM_FAILURE" />
      <value enum="R" description="PRIMARY_PEG" />
      <value enum="S" description="SUSPEND" />
      <value enum="T" description="FIXED_PEG" />
      <value enum="U" description="CUSTOMER_DISPLAY_INSTRUCTION" />
      <value enum="V" description="NETTING" />
      <value enum="W" description="PEG_TO_VWAP" />
      <value enum="X" description="TRADE_ALONG" />
      <value enum="Y" description="TRY_TO_STOP" />
    </field>
    <field number="21" name="HandlInst" type="CHAR">
      <value enum="1" description="AUTOMATED_EXECUTION_ORDER_PRIVATE" />
      <value enum="2" description="AUTOMATED_EXECUTION_ORDER_PUBLIC" />
      <value enum="3" description="MANUAL_ORDER" />
    </field>
    <field number="23" name="IOIid" type="STRING" />
    <field number="31" name="LastPx" type="PRICE" />
    <field number="32" name="LastQty" type="QTY" />
    <field number="34" name="MsgSeqNum" type="SEQNUM" />
    <field number="35" name="MsgType" type="STRING">
      <value enum="0" description="HEARTBEAT" />
      <value enum="1" description="TEST_REQUEST" />
      <value enum="2" description="RESEND_REQUEST" />
      <value enum="3" description="REJECT" />
      <value enum="4" description="SEQUENCE_RESET" />
      <value enum="5" description="LOGOUT" />
      <value enum="6" description="INDICATION_OF_INTEREST" />
      <value enum="7" description="ADVERTISEMENT" />
      <value enum="8" description="EXECUTION_REPORT" />
      <value enum="9" description="ORDER_CANCEL_REJECT" />
      <value enum="A" description="LOGON" />
      <value enum="B" description="NEWS" />
      <value enum="C" description="EMAIL" />
      <value enum="D" description="ORDER_SINGLE" />
      <value enum="E" description="ORDER_LIST" />
      <value enum="F" description="ORDER_CANCEL_REQUEST" />
      <value enum="G" description="ORDER_CANCEL" />
      <value enum="H" description="ORDER_STATUS_REQUEST" />
      <value enum="J" description="ALLOCATION" />
      <value enum="K" description="LIST_CANCEL_REQUEST" />
      <value enum="L" description="LIST_EXECUTE" />
      <value enum="M" description="LIST_STATUS_REQUEST" />
      <value enum="N" description="LIST_STATUS" />
      <value enum="P" description="ALLOCATION_ACK" />
      <value enum="Q" description="DONT_KNOW_TRADE" />
      <value enum="R" description="QUOTE_REQUEST" />
      <value enum="S" description="QUOTE" />
      <value enum="T" description="SETTLEMENT_INSTRUCTIONS" />
      <value enum="V" description="MARKET_DATA_REQUEST" />
      <value enum="W" description="MARKET_DATA_SNAPSHOT" />
      <value enum="X" description="MARKET_DATA_INCREMENTAL_REFRESH" />
      <value enum="Y" description="MARKET_DATA_REQUEST_REJECT" />
      <value enum="Z" description="QUOTE_CANCEL" />
      <value enum="a" description="QUOTE_STATUS_REQUEST" />
      <value enum="b" description="MASS_QUOTE_ACKNOWLEDGEMENT" />
      <value enum="c" description="SECURITY_DEFINITION_REQUEST" />
      <value enum="d" description="SECURITY_DEFINITION" />
      <value enum="e" description="SECURITY_STATUS_REQUEST" />
      <value enum="f" description="SECURITY_STATUS" />
      <value enum="g" description="TRADING_SESSION_STATUS_REQUEST" />
      <value enum="h" description="TRADING_SESSION_STATUS" />
      <value enum="i" description="MASS_QUOTE" />
      <value enum="j" description="BUSINESS_MESSAGE_REJECT" />
      <value enum="k" description="BID_REQUEST" />
      <value enum="l" description="BID_RESPONSE" />
      <value enum="m" description="LIST_STRIKE_PRICE" />
      <value enum="n" description="XML_MESSAGE" />
      <value enum="o" description="REGISTRATION_INSTRUCTIONS" />
      <value enum="p" description="REGISTRATION_INSTRUCTIONS_RESPONSE" />
      <value enum="q" description="ORDER_MASS_CANCEL_REQUEST" />
      <value enum="r" description="ORDER_MASS_CANCEL_REPORT" />
      <value enum="s" description="NEW_ORDER_CROSS" />
      <value enum="t" description="CROSS_ORDER_CANCEL" />
      <value enum="u" description="CROSS_ORDER_CANCEL_REQUEST" />
      <value enum="v" description="SECURITY_TYPE_REQUEST" />
      <value enum="w" description="SECURITY_TYPES" />
      <value enum="x" description="SECURITY_LIST_REQUEST" />
      <value enum="y" description="SECURITY_LIST" />
      <value enum="z" description="DERIVATIVE_SECURITY_LIST_REQUEST" />
      <value enum="AA" description="DERIVATIVE_SECURITY_LIST" />
      <value enum="AB" description="NEW_ORDER_MULTILEG" />
      <value enum="AC" description="MULTILEG_ORDER_CANCEL" />
      <value enum="AD" description="TRADE_CAPTURE_REPORT_REQUEST" />
      <value enum="AE" description="TRADE_CAPTURE_REPORT" />
      <value enum="AF" description="ORDER_MASS_STATUS_REQUEST" />
      <value enum="AG" description="QUOTE_REQUEST_REJECT" />
      <value enum="AH" description="RFQ_REQUEST" />
      <value enum="AI" description="QUOTE_STATUS_REPORT" />
    </field>
    <field number="36" name="NewSeqNo" type="SEQNUM" />
    <field number="37" name="OrderID" type="STRING" />
    <field number="38" name="OrderQty" type="QTY" />
    <field number="39" name="OrdStatus" type="CHAR">
      <value enum="0" description="NEW" />
      <value enum="1" description="PARTIALLY_FILLED" />
      <value enum="2" description="FILLED" />
      <value enum="3" description="DONE_FOR_DAY" />
      <value enum="4" description="CANCELED" />
      <value enum="5" description="REPLACED" />
      <value enum="6" description="PENDING_CANCEL" />
      <value enum="7" description="STOPPED" />
      <value enum="8" description="REJECTED" />
      <value enum="9" description="SUSPENDED" />
      <value enum="A" description="PENDING_NEW" />
      <value enum="B" description="CALCULATED" />
      <value enum="C" description="EXPIRED" />
      <value enum="D" description="ACCEPTED_FOR_BIDDING" />
      <value enum="E" description="PENDING_REPLACE" />
    </field>
    <field number="40" name="OrdType" type="CHAR">
      <value enum="1" description="MARKET" />
      <value enum="2" description="LIMIT" />
      <value enum="3" description="STOP" />
      <value enum="4" description="STOP_LIMIT" />
      <value enum="5" description="MARKET_ON_CLOSE" />
      <value enum="6" description="WITH_OR_WITHOUT" />
      <value enum="7" description="LIMIT_OR_BETTER" />
      <value enum="8" description="LIMIT_WITH_OR_WITHOUT" />
      <value enum="9" description="ON_BASIS" />
      <value enum="A" description="ON_CLOSE" />
      <value enum="B" description="LIMIT_ON_CLOSE" />
      <value enum="C" description="FOREX_MARKET" />
      <value enum="D" description="PREVIOUSLY_QUOTED" />
      <value enum="E" description="PREVIOUSLY_INDICATED" />
      <value enum="F" description="FOREX_LIMIT" />
      <value enum="G" description="FOREX_SWAP" />
      <value enum="H" description="FOREX_PREVIOUSLY_QUOTED" />
      <value enum="I" description="FUNARI" />
      <value enum="J" description="MARKET_IF_TOUCHED" />
      <value enum="K" description="MARKET_WITH_LEFTOVER_AS_LIMIT" />
      <value enum="L" description="PREVIOUS_FUND_VALUATION_POINT" />
      <value enum="M" description="NEXT_FUND_VALUATION_POINT" />
      <value enum="P" description="PEGGED" />
    </field>
    <field number="43" name="PossDupFlag" type="BOOLEAN" />
    <field number="44" name="Price" type="PRICE" />
    <field number="45" name="RefSeqNum" type="SEQNUM" />
    <field type="CHAR" name="Rule80A" number="47">
      <value enum="A" description="AGENCY_SINGLE_ORDER" />
      <value enum="B" description="SHORT_EXEMPT_TRANSACTION_REFER_TO_A_TYPE" />
      <value enum="C" description="PROGRAM_ORDER_NON_INDEX_ARB_FOR_MEMBER_FIRM" />
      <value enum="D" description="PROGRAM_ORDER_INDEX_ARB_FOR_MEMBER_FIRM" />
      <value enum="E" description="SHORT_EXEMPT_TRANSACTION_FOR_PRINCIPAL" />
      <value enum="F" description="SHORT_EXEMPT_TRANSACTION_REFER_TO_W_TYPE" />
      <value enum="H" description="SHORT_EXEMPT_TRANSACTION_REFER_TO_I_TYPE" />
      <value enum="I" description="INDIVIDUAL_INVESTOR" />
      <value enum="J" description="PROGRAM_ORDER_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER" />
      <value enum="K" description="PROGRAM_ORDER_NON_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER" />
      <value enum="L" description="SHORT_EXEMPT_AFFILIATED" />
      <value enum="M" description="PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_MEMBER" />
      <value enum="N" description="PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_MEMBER" />
      <value enum="O" description="PROPRIETARY_AFFILIATED" />
      <value enum="P" description="PRINCIPAL" />
      <value enum="R" description="TRANSACTIONS_NON_MEMBER" />
      <value enum="S" description="SPECIALIST_TRADES" />
      <value enum="T" description="TRANSACTIONS_UNAFFILIATED_MEMBER" />
      <value enum="U" description="PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_AGENCY" />
      <value enum="W" description="ALL_OTHER_ORDERS_AS_AGENT_FOR_OTHER_MEMBER" />
      <value enum="X" description="SHORT_EXEMPT_NOT_AFFILIATED" />
      <value enum="Y" description="PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_AGENCY" />
      <value enum="Z" description="SHORT_EXEMPT_NONMEMBER" />
    </field>
    <field number="49" name="SenderCompID" type="STRING" />
    <field number="52" name="SendingTime" type="UTCTIMESTAMP" />
    <field number="54" name="Side" type="CHAR">
      <value enum="1" description="BUY" />
      <value enum="2" description="SELL" />
      <value enum="3" description="BUY_MINUS" />
      <value enum="4" description="SELL_PLUS" />
      <value enum="5" description="SELL_SHORT" />
      <value enum="6" description="SELL_SHORT_EXEMPT" />
      <value enum="7" description="UNDISCLOSED" />
      <value enum="8" description="CROSS" />
      <value enum="9" description="CROSS_SHORT" />
      <value enum="A" description="CROSS_SHORT_EXEMPT" />
      <value enum="B" description="AS_DEFINED" />
      <value enum="C" description="OPPOSITE" />
    </field>
    <field number="55" name="Symbol" type="STRING" />
    <field number="56" name="TargetCompID" type="STRING" />
    <field number="58" name="Text" type="STRING" />
    <field number="59" name="TimeInForce" type="CHAR">
      <value enum="0" description="DAY" />
      <value enum="1" description="GOOD_TILL_CANCEL" />
      <value enum="2" description="AT_THE_OPENING" />
      <value enum="3" description="IMMEDIATE_OR_CANCEL" />
      <value enum="4" description="FILL_OR_KILL" />
      <value enum="5" description="GOOD_TILL_CROSSING" />
      <value enum="6" description="GOOD_TILL_DATE" />
      <value enum="7" description="AT_THE_CLOSE" />
    </field>
    <field number="60" name="TransactTime" type="UTCTIMESTAMP" />
    <field number="63" name="SettlmntTyp" type="CHAR">
      <value enum="0" description="REGULAR" />
      <value enum="1" description="CASH" />
      <value enum="2" description="NEXT_DAY" />
      <value description="T_PLUS_2" enum="3" />
      <value description="T_PLUS_3" enum="4" />
      <value description="T_PLUS_4" enum="5" />
      <value enum="6" description="FUTURE" />
      <value enum="7" description="WHEN_AND_IF_ISSUED" />
      <value enum="8" description="SELLERS_OPTION" />
      <value description="T_PLUS_5" enum="9" />
      <value description="T_PLUS_1" enum="A" />
    </field>
    <field number="64" name="FutSettDate" type="LOCALMKTDATE" />
	<field number="75" name="TradeDate" type="LOCALMKTDATE" />
    <field number="77" name="PositionEffect" type="CHAR" />
    <field number="81" name="ProcessCode" type="CHAR">
      <value enum="0" description="REGULAR" />
      <value enum="1" description="SOFT_DOLLAR" />
      <value enum="2" description="STEP_IN" />
      <value enum="3" description="STEP_OUT" />
      <value enum="4" description="SOFT_DOLLAR_STEP_IN" />
      <value enum="5" description="SOFT_DOLLAR_STEP_OUT" />
      <value enum="6" description="PLAN_SPONSOR" />
    </field>
    <field number="97" name="PossResend" type="BOOLEAN" />
    <field number="98" name="EncryptMethod" type="INT">
      <value enum="0" description="NONE" />
      <value enum="1" description="PKCS_PROPRIETARY" />
      <value enum="2" description="DES" />
      <value enum="3" description="PKCS_DES" />
      <value enum="4" description="PGP_DES" />
      <value enum="5" description="PGP_DES_MD5" />
      <value enum="6" description="PEM" />
    </field>
    <field number="99" name="StopPx" type="PRICE" />
    <field number="100" name="ExDestination" type="EXCHANGE" />
    <field number="103" name="OrdRejReason" type="INT">
      <value enum="0" description="BROKER_OPTION" />
      <value enum="1" description="UNKNOWN_SYMBOL" />
      <value enum="2" description="EXCHANGE_CLOSED" />
      <value enum="3" description="ORDER_EXCEEDS_LIMIT" />
      <value enum="4" description="TOO_LATE_TO_ENTER" />
      <value enum="5" description="UNKNOWN_ORDER" />
      <value enum="6" description="DUPLICATE_ORDER" />
      <value enum="7" description="DUPLICATE_VERBAL" />
      <value enum="8" description="STALE_ORDER" />
      <value enum="9" description="TRADE_ALONG_REQUIRED" />
      <value enum="10" description="INVALID_INVESTOR_ID" />
      <value enum="11" description="UNSUPPORTED_ORDER_CHARACTERISTIC" />
      <value enum="12" description="SURVEILLENCE_OPTION" />
		<value enum="13" description="INCORRECT_QTY" />
		<value enum="99" description="OTHER" />
    </field>
    <field number="108" name="HeartBtInt" type="INT" />
    <field number="110" name="MinQty" type="QTY" />
    <field number="111" name="MaxFloor" type="QTY" />
    <field number="112" name="TestReqID" type="STRING" />
    <field number="114" name="LocateReqd" type="BOOLEAN" />
    <field number="117" name="QuoteID" type="STRING" />
    <field number="120" name="SettlCurrency" type="CURRENCY" />
    <field number="121" name="ForexReq" type="BOOLEAN" />
    <field number="122" name="OrigSendingTime" type="UTCTIMESTAMP" />
    <field number="123" name="GapFillFlag" type="BOOLEAN" />
    <field number="126" name="ExpireTime" type="UTCTIMESTAMP" />
    <field number="128" name="DeliverToCompID" type="STRING" />
    <field number="140" name="PrevClosePx" type="PRICE" />
    <field number="141" name="ResetSeqNumFlag" type="BOOLEAN" />
    <field number="146" name="NoRelatedSym" type="NUMINGROUP" />
    <field number="150" name="ExecType" type="CHAR">
      <value enum="0" description="NEW" />
      <value enum="1" description="PARTIAL_FILL" />
      <value enum="2" description="FILL" />
      <value enum="3" description="DONE_FOR_DAY" />
      <value enum="4" description="CANCELED" />
      <value enum="5" description="REPLACE" />
      <value enum="6" description="PENDING_CANCEL" />
      <value enum="7" description="STOPPED" />
      <value enum="8" description="REJECTED" />
      <value enum="9" description="SUSPENDED" />
      <value enum="A" description="PENDING_NEW" />
      <value enum="B" description="CALCULATED" />
      <value enum="C" description="EXPIRED" />
      <value enum="D" description="RESTATED" />
      <value enum="E" description="PENDING_REPLACE" />
      <value enum="F" description="TRADE" />
      <value enum="G" description="TRADE_CORRECT" />
      <value enum="H" description="TRADE_CANCEL" />
      <value enum="I" description="ORDER_STATUS" />
    </field>
    <field number="151" name="LeavesQty" type="QTY" />
    <field number="168" name="EffectiveTime" type="UTCTIMESTAMP" />
    <field type="QTY" name="OrderQty2" number="192" />
    <field type="LOCALMKTDATE" name="FutSettDate2" number="193" />
    <field number="194" name="LastSpotRate" type="PRICE" />
    <field number="203" name="CoveredOrUncovered" type="INT">
      <value enum="0" description="COVERED" />
      <value enum="1" description="UNCOVERED" />
    </field>
    <field number="210" name="MaxShow" type="QTY" />
    <field number="211" name="PegDifference" type="PRICEOFFSET" />
    <field number="229" name="TradeOriginationDate" type="UTCDATE" />
    <field number="262" name="MDReqID" type="STRING" />
    <field number="263" name="SubscriptionRequestType" type="CHAR">
      <value enum="0" description="SNAPSHOT" />
      <value enum="1" description="SNAPSHOT_PLUS_UPDATES" />
      <value enum="2" description="DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST" />
    </field>
    <field number="264" name="MarketDepth" type="INT">
      <!--
      Temporarily commented out until we can handle
      N>1 scenario

      <value enum="0" description="FULL_BOOK"/>
      <value enum="1" description="TOP_OF_BOOK"/>
-->
    </field>
    <field number="265" name="MDUpdateType" type="INT">
      <value enum="0" description="FULL_REFRESH" />
      <value enum="1" description="INCREMENTAL_REFRESH" />
    </field>
    <field number="266" name="AggregatedBook" type="BOOLEAN" />
    <field number="267" name="NoMDEntryTypes" type="NUMINGROUP" />
    <field number="268" name="NoMDEntries" type="NUMINGROUP" />
    <field number="269" name="MDEntryType" type="CHAR">
      <value enum="0" description="BID" />
      <value enum="1" description="OFFER" />
      <value enum="2" description="TRADE" />
      <value enum="3" description="INDEX_VALUE" />
      <value enum="4" description="OPENING_PRICE" />
      <value enum="5" description="CLOSING_PRICE" />
      <value enum="6" description="SETTLEMENT_PRICE" />
      <value enum="7" description="TRADING_SESSION_HIGH_PRICE" />
      <value enum="8" description="TRADING_SESSION_LOW_PRICE" />
      <value enum="9" description="TRADING_SESSION_VWAP_PRICE" />
      <value enum="A" description="IMBALANCE" />
    </field>
    <field number="270" name="MDEntryPx" type="PRICE" />
    <field number="271" name="MDEntrySize" type="QTY" />
    <field number="272" name="MDEntryDate" type="UTCDATEONLY" />
    <field number="276" name="QuoteCondition" type="MULTIPLEVALUESTRING">
      <value enum="A" description="OPEN" />
      <value enum="B" description="CLOSED" />
      <value enum="C" description="EXCHANGE_BEST" />
      <value enum="D" description="CONSOLIDATED_BEST" />
      <value enum="E" description="LOCKED" />
      <value enum="F" description="CROSSED" />
      <value enum="G" description="DEPTH" />
      <value enum="H" description="FAST_TRADING" />
      <value enum="I" description="NON_FIRM" />
    </field>
    <field number="281" name="MDReqRejReason" type="CHAR">
      <value enum="0" description="UNKNOWN_SYMBOL" />
      <value enum="1" description="DUPLICATE_MDREQID" />
      <value enum="2" description="INSUFFICIENT_BANDWIDTH" />
      <value enum="3" description="INSUFFICIENT_PERMISSIONS" />
      <value enum="4" description="UNSUPPORTED_SUBSCRIPTIONREQUESTTYPE" />
      <value enum="5" description="UNSUPPORTED_MARKETDEPTH" />
      <value enum="6" description="UNSUPPORTED_MDUPDATETYPE" />
      <value enum="7" description="UNSUPPORTED_AGGREGATEDBOOK" />
      <value enum="8" description="UNSUPPORTED_MDENTRYTYPE" />
      <value enum="9" description="UNSUPPORTED_TRADINGSESSIONID" />
      <value enum="A" description="UNSUPPORTED_SCOPE" />
      <value enum="B" description="UNSUPPORTED_OPENCLOSESETTLEFLAG" />
      <value enum="C" description="UNSUPPORTED_MDIMPLICITDELETE" />
    </field>
    <field number="286" name="OpenCloseSettleFlag" type="MULTIPLEVALUESTRING">
      <value enum="0" description="DAILY_OPEN" />
      <value enum="1" description="SESSION_OPEN" />
      <value enum="2" description="DELIVERY_SETTLEMENT_PRICE" />
      <value enum="3" description="EXPECTED_PRICE" />
      <value enum="4" description="PRICE_FROM_PREVIOUS_BUSINESS_DAY" />
    </field>
	<field number="299" name="QuoteEntryId" type="STRING" />
    <field number="336" name="TradingSessionID" type="STRING" />
    <field number="354" name="EncodedTextLen" type="LENGTH" />
    <field number="355" name="EncodedText" type="DATA" />
    <field number="371" name="RefTagID" type="INT" />
    <field number="372" name="RefMsgType" type="STRING" />
    <field number="373" name="SessionRejectReason" type="INT">
      <value enum="0" description="INVALID_TAG_NUMBER" />
      <value enum="1" description="REQUIRED_TAG_MISSING" />
      <value enum="2" description="TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE" />
      <value enum="3" description="UNDEFINED_TAG" />
      <value enum="4" description="TAG_SPECIFIED_WITHOUT_A_VALUE" />
      <value enum="5" description="VALUE_IS_INCORRECT" />
      <value enum="6" description="INCORRECT_DATA_FORMAT_FOR_VALUE" />
      <value enum="7" description="DECRYPTION_PROBLEM" />
      <value enum="8" description="SIGNATURE_PROBLEM" />
      <value enum="9" description="COMPID_PROBLEM" />
      <value enum="10" description="SENDINGTIME_ACCURACY_PROBLEM" />
      <value enum="11" description="INVALID_MSGTYPE" />
      <value enum="12" description="XML_VALIDATION_ERROR" />
      <value enum="13" description="TAG_APPEARS_MORE_THAN_ONCE" />
      <value enum="14" description="TAG_SPECIFIED_OUT_OF_REQUIRED_ORDER" />
      <value enum="15" description="REPEATING_GROUP_FIELDS_OUT_OF_ORDER" />
      <value enum="16" description="INCORRECT_NUMINGROUP_COUNT_FOR_REPEATING_GROUP" />
      <value enum="17" description="NON_DATA_VALUE_INCLUDES_FIELD_DELIMITER" />
    </field>
    <field number="376" name="ComplianceID" type="STRING" />
    <field number="377" name="SolicitedFlag" type="BOOLEAN" />
    <field number="379" name="BusinessRejectRefID" type="STRING" />
    <field number="380" name="BusinessRejectReason" type="INT">
      <value enum="0" description="OTHER" />
      <value enum="1" description="UNKOWN_ID" />
      <value enum="2" description="UNKNOWN_SECURITY" />
      <value enum="3" description="UNSUPPORTED_MESSAGE_TYPE" />
      <value enum="4" description="APPLICATION_NOT_AVAILABLE" />
      <value enum="5" description="CONDITIONALLY_REQUIRED_FIELD_MISSING" />
      <value enum="6" description="NOT_AUTHORIZED" />
      <value enum="7" description="DELIVERTO_FIRM_NOT_AVAILABLE_AT_THIS_TIME" />
    </field>
    <field number="386" name="NoTradingSessions" type="NUMINGROUP" />
    <field number="388" name="DiscretionInst" type="CHAR">
      <value enum="0" description="RELATED_TO_DISPLAYED_PRICE" />
      <value enum="1" description="RELATED_TO_MARKET_PRICE" />
      <value enum="2" description="RELATED_TO_PRIMARY_PRICE" />
      <value enum="3" description="RELATED_TO_LOCAL_PRIMARY_PRICE" />
      <value enum="4" description="RELATED_TO_MIDPOINT_PRICE" />
      <value enum="5" description="RELATED_TO_LAST_TRADE_PRICE" />
    </field>
    <field number="389" name="DiscretionOffset" type="PRICEOFFSET" />
    <field type="AMT" name="SideValue1" number="396" />
    <field type="AMT" name="SideValue2" number="397" />
    <field number="423" name="PriceType" type="INT">
      <value enum="1" description="PERCENTAGE" />
      <value enum="2" description="PER_SHARE" />
      <value enum="3" description="FIXED_AMOUNT" />
      <value enum="4" description="DISCOUNT" />
      <value enum="5" description="PREMIUM" />
      <value enum="6" description="BASIS_POINTS_RELATIVE_TO_BENCHMARK" />
      <value enum="7" description="TED_PRICE" />
      <value enum="8" description="TED_YIELD" />
    </field>
    <field number="427" name="GTBookingInst" type="INT">
      <value enum="0" description="BOOK_OUT_ALL_TRADES_ON_DAY_OF_EXECUTION" />
      <value enum="1" description="ACCUMULATE_EXECUTIONS_UNTIL_ORDER_IS_FILLED_OR_EXPIRES" />
      <value enum="2" description="ACCUMULATE_UNTIL_VERBALLY_NOTIFIED_OTHERWISE" />
    </field>
    <field number="432" name="ExpireDate" type="LOCALMKTDATE" />
    <field number="465" name="QuantityType" type="INT">
      <value enum="1" description="SHARES" />
      <value enum="2" description="BONDS" />
      <value enum="3" description="CURRENTFACE" />
      <value enum="4" description="ORIGINALFACE" />
      <value enum="5" description="CURRENCY" />
      <value enum="6" description="CONTRACTS" />
      <value enum="7" description="OTHER" />
      <value enum="8" description="PAR" />
    </field>
    <field number="480" name="CancellationRights" type="CHAR" />
    <field number="481" name="MoneyLaunderingStatus" type="CHAR">
      <value enum="Y" description="PASSED" />
      <value enum="N" description="NOT_CHECKED" />
      <value enum="1" description="EXEMPT_BELOW_THE_LIMIT" />
      <value enum="2" description="EXEMPT_CLIENT_MONEY_TYPE_EXEMPTION" />
      <value enum="3" description="EXEMPT_AUTHORISED_CREDIT_OR_FINANCIAL_INSTITUTION" />
    </field>
    <field number="494" name="Designation" type="STRING" />
    <field number="513" name="RegistID" type="STRING" />
    <field number="526" name="SecondaryClOrdID" type="STRING" />
    <field number="528" name="OrderCapacity" type="CHAR">
      <value enum="A" description="AGENCY" />
      <value enum="G" description="PROPRIETARY" />
      <value enum="I" description="INDIVIDUAL" />
      <value enum="P" description="PRINCIPAL" />
      <value enum="R" description="RISKLESS_PRINCIPAL" />
      <value enum="W" description="AGENT_FOR_OTHER_MEMBER" />
    </field>
    <field number="529" name="OrderRestrictions" type="MULTIPLEVALUESTRING">
      <value enum="1" description="PROGRAM_TRADE" />
      <value enum="2" description="INDEX_ARBITRAGE" />
      <value enum="3" description="NON_INDEX_ARBITRAGE" />
      <value enum="4" description="COMPETING_MARKET_MAKER" />
      <value enum="5" description="ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_SECURITY" />
      <value enum="6" description="ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_UNDERLYING_SECURITY_OF_A_DERIVATIVE_SECURITY" />
      <value enum="7" description="FOREIGN_ENTITY" />
      <value enum="8" description="EXTERNAL_MARKET_PARTICIPANT" />
      <value enum="9" description="EXTERNAL_INTER_CONNECTED_MARKET_LINKAGE" />
      <value enum="A" description="RISKLESS_ARBITRAGE" />
    </field>
    <field number="544" name="CashMargin" type="CHAR">
      <value enum="1" description="CASH" />
      <value enum="2" description="MARGIN_OPEN" />
      <value enum="3" description="MARGIN_CLOSE" />
    </field>
    <field number="546" name="Scope" type="MULTIPLEVALUESTRING">
      <value enum="1" description="LOCAL" />
      <value enum="2" description="NATIONAL" />
      <value enum="3" description="GLOBAL" />
    </field>
    <field number="547" name="MDImplicitDelete" type="BOOLEAN" />
    <field number="581" name="AccountType" type="INT">
      <value enum="1" description="ACCOUNT_IS_CARRIED_ON_CUSTOMER_SIDE_OF_BOOKS" />
      <value enum="2" description="ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS" />
      <value enum="3" description="HOUSE_TRADER" />
      <value enum="4" description="FLOOR_TRADER" />
      <value enum="6" description="ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS_AND_IS_CROSS_MARGINED" />
      <value enum="7" description="ACCOUNT_IS_HOUSE_TRADER_AND_IS_CROSS_MARGINED" />
      <value enum="8" description="JOINT_BACKOFFICE_ACCOUNT" />
    </field>
    <field number="582" name="CustOrderCapacity" type="INT" />
    <field number="583" name="ClOrdLinkID" type="STRING" />
    <field number="589" name="DayBookingInst" type="CHAR" />
    <field number="590" name="BookingUnit" type="CHAR" />
    <field number="591" name="PreallocMethod" type="CHAR" />
    <field number="625" name="TradingSessionSubID" type="STRING" />
    <field number="635" name="ClearingFeeIndicator" type="STRING" />
    <field type="PRICE" name="Price2" number="640" />
	<field number="5020" name="SETTL_DATE" type="STRING" />
	  <field number="6138" name="TICK_SIZE" type="QTY" />
    <field number="6215" name="TENOR_VALUE" type="STRING" />
    <field number="5232" name="CURRENCY" type="CURRENCY" />
    <field number="5233" name="ORDER_QTY" type="QTY" />
    <field number="6054" name="QUOTED_QUANTITY" type="QTY" />
    <field number="6233" name="ORD_STATUS_REQ_ID" type="STRING" />
    <field number="7706" name="ORIGINATOR_ACC" type="STRING" />
    <field number="8030" name="ROOT_ORD_ID" type="STRING" />
  </fields>
</fix>'
    ,[LastUpdated] = GETUTCDATE()
WHERE
	[Name] = 'FIX44_DBK.xml'
