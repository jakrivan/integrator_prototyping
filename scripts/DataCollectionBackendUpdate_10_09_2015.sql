


SET IDENTITY_INSERT [dbo].[LiqProviderStream] ON 
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (112, N'H4T', 20)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (113, N'H4M', 20)
GO
SET IDENTITY_INSERT [dbo].[LiqProviderStream] OFF
GO


INSERT INTO [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode])
SELECT 
	integratorCpt.[CounterpartyId] AS IntegratorDBCounterpartyId,
	dcCpt.[LiqProviderStreamId] AS DCDBCounterpartyId,
    dcCpt.[LiqProviderStreamName] AS CounterpartyCode
FROM
	[dbo].[LiqProviderStream] dcCpt
	INNER JOIN [Integrator].[dbo].[Counterparty] integratorCpt ON dcCpt.LiqProviderStreamName = integratorCpt.CounterpartyCode
WHERE
	dcCpt.[LiqProviderStreamId] NOT IN (SELECT DCDBCounterpartyId FROM [dbo].[_Internal_CounterpartyIdsMapping])