
DELETE FROM [dbo].[CounterpartySymbolSettings]
WHERE [TradingTargetTypeId] = 3 AND SymbolId = (SELECT [FxPairId] FROM [dbo].[FxPair] where [FxpCode] = 'NOKSEK')
GO

INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity])
SELECT 3, [FxPairId], 1, 0.00001, 0.00001 FROM [dbo].[FxPair] where [FxpCode] = 'NOKSEK'
GO