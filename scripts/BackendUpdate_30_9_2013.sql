
BEGIN TRANSACTION OrdersBackendSchemaUpdates WITH MARK N'External orders tables, stored procedures, views and keys renamings and additions';

--
-- OrderExternal table
--

-- Table
EXEC sp_RENAME 'OrderExternal.[ExternalOrderSentUtc]' , 'IntegratorSentExternalOrderUtc', 'COLUMN'
EXEC sp_RENAME 'OrderExternal.[InternalOrderId]' , 'IntegratorExternalOrderPrivateId', 'COLUMN'
GO

ALTER TABLE OrderExternal  ADD FixMessageSentRaw nvarchar(1024) NULL
ALTER TABLE OrderExternal  ADD FixMessageSentParsed nvarchar(MAX) NULL
ALTER TABLE OrderExternal  ADD SourceIntegratorPriceIdentity uniqueidentifier NULL
GO

-- Stored procedure
ALTER PROCEDURE [dbo].[InsertNewOrderExternal_SP]( 
	@ExternalOrderSentUtc [datetime2](7),
	@InternalOrderId [nvarchar](45),
	@InternalClientOrderId [nvarchar](32),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@QuoteReceivedToOrderSentInternalLatency [time](7),
	@FixMessageSentRaw nvarchar(1024) = NULL,
	@FixMessageSentParsed nvarchar(MAX) = NULL,
	@SourceIntegratorPriceIdentity uniqueidentifier = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
		[dbo].[OrderExternal]
           ([IntegratorSentExternalOrderUtc]
           ,[IntegratorExternalOrderPrivateId]
		   ,[ExternalOrderLabelForCounterparty]
           ,[CounterpartyId]
           ,[SymbolId]
		   ,[DealDirectionId]
           ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[FixMessageSentRaw]
		   ,[FixMessageSentParsed]
		   ,[SourceIntegratorPriceIdentity])
     VALUES
           (@ExternalOrderSentUtc
           ,@InternalOrderId
		   ,@InternalClientOrderId
           ,@CounterpartyId
           ,@SymbolId
		   ,@DealDirectionId
           ,@QuoteReceivedToOrderSentInternalLatency
		   ,@FixMessageSentRaw
		   ,@FixMessageSentParsed
		   ,@SourceIntegratorPriceIdentity)
END
GO

--
-- End of OrderExternal table
--

--
-- ExecutionReportMessage table
--

-- Table
EXEC sp_RENAME 'ExecutionReportMessage.[ExecutionReportReceivedUtc]' , 'IntegratorReceivedExecutionReportUtc', 'COLUMN'
EXEC sp_RENAME 'ExecutionReportMessage.[InternalOrderId]' , 'IntegratorExternalOrderPrivateId', 'COLUMN'
EXEC sp_RENAME 'ExecutionReportMessage.[ExecutionReportCounterpartySentUtc]' , 'CounterpartySentExecutionReportUtc', 'COLUMN'
EXEC sp_RENAME 'ExecutionReportMessage.[RawFIXMessage]' , 'FixMessageReceivedRaw', 'COLUMN'
EXEC sp_RENAME 'ExecutionReportMessage.[ParsedFIXMessage]' , 'FixMessageReceivedParsed', 'COLUMN'
GO

--Stored Procedure
ALTER PROCEDURE [dbo].[InsertExecutionReportMessage_SP]( 
	@ExecutionReportReceivedUtc [datetime2](7),
	@ExecutionReportStatus [varchar](16),
	@InternalOrderId [nvarchar](45),
	@ExecutionReportCounterpartySentUtc [datetime2](7),
	@RawFIXMessage [nvarchar](1024),
	@ParsedFIXMessage [nvarchar](MAX)
	)
AS
BEGIN

	DECLARE @ExecutionReportStatusId INT

	SELECT @ExecutionReportStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ExecutionReportStatus

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ExecutionReportStatus not found in DB: %s', 16, 2, @ExecutionReportStatus) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[ExecutionReportMessage]
           ([IntegratorReceivedExecutionReportUtc]
           ,[ExecutionReportStatusId]
           ,[IntegratorExternalOrderPrivateId]
           ,[CounterpartySentExecutionReportUtc]
           ,[FixMessageReceivedRaw]
           ,[FixMessageReceivedParsed])
     VALUES
           (
		   @ExecutionReportReceivedUtc
           ,@ExecutionReportStatusId
           ,@InternalOrderId
           ,@ExecutionReportCounterpartySentUtc
           ,@RawFIXMessage
           ,@ParsedFIXMessage
		   )
END
GO

--
-- End of ExecutionReportMessage table
--


--
-- DealExternalExecuted table
--

-- Table
EXEC sp_RENAME 'DealExternalExecuted.[ExecutionReportReceivedUtc]' , 'IntegratorReceivedExecutionReportUtc', 'COLUMN'
EXEC sp_RENAME 'DealExternalExecuted.[InternalOrderId]' , 'IntegratorExternalOrderPrivateId', 'COLUMN'
EXEC sp_RENAME 'DealExternalExecuted.[CounterpartyExecutionReportTimeStampUtc]' , 'CounterpartySuppliedExecutionTimeStampUtc', 'COLUMN'
GO

ALTER TABLE DealExternalExecuted  ADD CounterpartySentExecutionReportUtc datetime2 NULL
GO

-- View
ALTER VIEW [dbo].[DealExternalExecuted_View]
AS
SELECT
	[OrderId]
	,[ExecutionTimeStampUtc]
	,[SymbolId]
	,[DealDirectionId]
	,[ExecutedAmountBaseAbs]
	,[ExecutedAmountBasePol]
	,[Price]
FROM [dbo].[ExecutedExternalDeals]
WHERE [ExecutionTimeStampUtc] < '2013-07-29 14:30:00.000'

  UNION ALL

SELECT 
	[IntegratorExternalOrderPrivateId] AS [OrderId]
	,[IntegratorReceivedExecutionReportUtc] AS [ExecutionTimeStampUtc]
	,[SymbolId]
	,[DealDirectionId]
	,ABS([AmountBasePolExecuted]) AS [ExecutedAmountBaseAbs]
	,[AmountBasePolExecuted] AS [ExecutedAmountBasePol]
	,[Price]
FROM [dbo].[DealExternalExecuted]
WHERE [IntegratorReceivedExecutionReportUtc] > '2013-07-29 14:30:00.000'
GO

-- Stored Procedure
ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolExecuted decimal(18, 0),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END

	INSERT INTO 
	[dbo].[DealExternalExecuted]
           ([IntegratorReceivedExecutionReportUtc]
           ,[CounterpartyId]
           ,[SymbolId]
           ,[IntegratorExternalOrderPrivateId]
           ,[DealDirectionId]
           ,[AmountBasePolExecuted]
           ,[Price]
           ,[CounterpartyTransactionId]
           ,[CounterpartySuppliedExecutionTimeStampUtc]
           ,[ValueDateCounterpartySuppliedLocMktDate]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[CounterpartySentExecutionReportUtc])
     VALUES
           (@ExecutionReportReceivedUtc
           ,@CounterpartyId
           ,@SymbolId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolExecuted
           ,@Price
           ,@CounterpartyTransactionId
           ,@CounterpartyExecutionReportTimeStampUtc
           ,@ValueDateCounterpartySuppliedLocMktDate
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@CounterpartySentExecutionReportUtc)
END
GO

--
-- End of DealExternalExecuted table
--


--
-- DealExternalRejected table
--

-- Table
EXEC sp_RENAME 'DealExternalRejected.[ExecutionReportReceivedUtc]' , 'IntegratorReceivedExecutionReportUtc', 'COLUMN'
EXEC sp_RENAME 'DealExternalRejected.[InternalOrderId]' , 'IntegratorExternalOrderPrivateId', 'COLUMN'
GO

ALTER TABLE DealExternalRejected  ADD CounterpartySentExecutionReportUtc datetime2 NULL
GO

-- Stored Procedure
ALTER PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 0),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[DealExternalRejected]
           ([IntegratorReceivedExecutionReportUtc]
           ,[SymbolId]
           ,[CounterpartyId]
           ,[IntegratorExternalOrderPrivateId]
		   ,[DealDirectionId]
           ,[AmountBasePolRejected]
           ,[OrderSentToExecutionReportReceivedLatency]
           ,[RejectionReason]
		   ,[CounterpartySentExecutionReportUtc])
     VALUES
           (@ExecutionReportReceivedUtc
		   ,@SymbolId
           ,@CounterpartyId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolRejected
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@RejectionReason
		   ,@CounterpartySentExecutionReportUtc)
END
GO

--
-- End of DealExternalRejected table
--

-- In case we would detect errors
-- IF *** ROLLBACK TRANSACTION OrdersBackendSchemaUpdates

COMMIT TRANSACTION OrdersBackendSchemaUpdates;
GO

--
-- Data reconstruction
--

UPDATE
    [dbo].[DealExternalExecuted]
SET
    [dbo].[DealExternalExecuted].[CounterpartySentExecutionReportUtc] = msg.CounterpartySentExecutionReportUtc
FROM 
	[dbo].[DealExternalExecuted] deal
	INNER JOIN [dbo].[ExecutionReportMessage] msg ON deal.IntegratorExternalOrderPrivateId = msg.IntegratorExternalOrderPrivateId
	INNER JOIN [dbo].[ExecutionReportStatus] stat ON stat.Id = msg.ExecutionReportStatusId
WHERE stat.Name = 'Filled'


UPDATE
    [dbo].[DealExternalRejected]
SET
    [dbo].[DealExternalRejected].[CounterpartySentExecutionReportUtc] = msg.CounterpartySentExecutionReportUtc
FROM 
	[dbo].[DealExternalRejected] reject
	INNER JOIN [dbo].[ExecutionReportMessage] msg ON reject.IntegratorExternalOrderPrivateId = msg.IntegratorExternalOrderPrivateId
	INNER JOIN [dbo].[ExecutionReportStatus] stat ON stat.Id = msg.ExecutionReportStatusId
WHERE stat.Name = 'Rejected'