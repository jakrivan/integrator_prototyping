title "KGT Command prompt"
@echo Started KGT command prompt
@echo Run 'list' to list all available macros

@echo Off
doskey list=doskey /macros
doskey all=doskey /macros
doskey np=notepad $*
doskey npp="C:\Program Files (x86)\Notepad++\notepad++.exe" $*
doskey up=cd ..
doskey up2=cd .. & cd ..
doskey up3=cd .. & cd .. & cd ..
doskey ..=cd ..
doskey ...='cd .. & cd ..'
doskey windiff="E:\GDr\Temp\Tools\installs\windiff\WinDiff.Exe"
doskey home=cd /d "E:\GDr\KGT\src\Integrator_daily"
doskey src=cd /d "E:\GDr\KGT\src\Integrator_daily\src"
doskey prototype=cd /d "E:\GDr\KGT\src\Integrator_Prototyping"
doskey vs="C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\devenv.exe" $*
doskey vs11="C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\devenv.exe" $*
doskey vs12="C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\devenv.exe" $*
doskey findsql=findstr /d:"E:\GDr\KGT\src\Integrator_daily\scripts" /spin $1 "*.sql" 
@echo On