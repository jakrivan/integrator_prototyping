BEGIN TRANSACTION Install_06_01_001;
GO

CREATE TABLE [dbo].[VenueGliderSettings](
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[MaximumSizeToTrade] [decimal](18, 0) NOT NULL,
	[BestPriceImprovementOffsetBasisPoints] [decimal](18, 6) NOT NULL,
	[MaximumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[KGTLLTime_milliseconds] [int] NOT NULL,
	[MinimumBPGrossGain] [decimal](18, 6) NOT NULL,
	[PreferLmaxDuringHedging] [bit] NOT NULL,
	[MinimumIntegratorPriceLife_milliseconds] [int] NOT NULL,
	[MinimumIntegratorGlidePriceLife_milliseconds] [int] NOT NULL,
	[GlidingCutoffSpan_milliseconds] [int] NOT NULL,
	[MinimumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MinimumFillSize] [decimal](18, 0) NOT NULL,
	[MaximumSourceBookTiers] [int] NOT NULL,
	[MinimumBasisPointsFromKgtPriceToFastCancel] [decimal](18, 6) NOT NULL,
	[ReferenceLmaxBookOnly] [bit] NOT NULL,
	[BidEnabled] [bit] NOT NULL,
	[AskEnabled] [bit] NOT NULL,
 CONSTRAINT [UNQ__VenueGliderSettings__TradingSystemId] UNIQUE NONCLUSTERED 
(
	[TradingSystemId] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[VenueGliderSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueGliderSettings_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueGliderSettings] CHECK CONSTRAINT [FK_VenueGliderSettings_Counterparty]
GO

ALTER TABLE [dbo].[VenueGliderSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueGliderSettings_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueGliderSettings] CHECK CONSTRAINT [FK_VenueGliderSettings_Symbol]
GO

CREATE CLUSTERED INDEX [IX_VenueGliderSettingsSymbolPerCounterparty] ON [dbo].[VenueGliderSettings]
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
) ON [PRIMARY]
GO



CREATE TABLE [dbo].[VenueGliderSettings_Changes](
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[MaximumSizeToTrade] [decimal](18, 0) NOT NULL,
	[BestPriceImprovementOffsetBasisPoints] [decimal](18, 6) NOT NULL,
	[MaximumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[KGTLLTime_milliseconds] [int] NOT NULL,
	[MinimumBPGrossGain] [decimal](18, 6) NOT NULL,
	[PreferLmaxDuringHedging] [bit] NOT NULL,
	[MinimumIntegratorPriceLife_milliseconds] [int] NOT NULL,
	[MinimumIntegratorGlidePriceLife_milliseconds] [int] NOT NULL,
	[GlidingCutoffSpan_milliseconds] [int] NOT NULL,
	[MinimumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MinimumFillSize] [decimal](18, 0) NOT NULL,
	[MaximumSourceBookTiers] [int] NOT NULL,
	[MinimumBasisPointsFromKgtPriceToFastCancel] [decimal](18, 6) NOT NULL,
	[ReferenceLmaxBookOnly] [bit] NOT NULL,
	[BidEnabled] [bit] NOT NULL,
	[AskEnabled] [bit] NOT NULL,
	[ValidFromUtc] [datetime2](7) NOT NULL,
	[ValidToUtc] [datetime2](7) NOT NULL,
	[Deleted] [bit] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VenueGliderSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueGliderSettings_Changes_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueGliderSettings_Changes] CHECK CONSTRAINT [FK_VenueGliderSettings_Changes_Counterparty]
GO

ALTER TABLE [dbo].[VenueGliderSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueGliderSettings_Changes_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueGliderSettings_Changes] CHECK CONSTRAINT [FK_VenueGliderSettings_Changes_Symbol]
GO

CREATE CLUSTERED INDEX [IX_VenueGliderSettings_Changes] ON [dbo].[VenueGliderSettings_Changes]
(
	[TradingSystemId] ASC,
	[ValidToUtc] ASC
)ON [PRIMARY]
GO


INSERT INTO [dbo].[TableChangeLog] ([TableName], [LastUpdatedUtc]) VALUES ('VenueGliderSettings', '1/1/1900')
GO

CREATE TRIGGER [dbo].[LastDeletedTrigger_VenueGliderSettings] ON [dbo].[VenueGliderSettings]
AFTER DELETE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON
UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueGliderSettings'

END
GO


CREATE TRIGGER [dbo].[LastUpdatedTrigger_VenueGliderSettings] ON [dbo].[VenueGliderSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueGliderSettings'

END
GO


CREATE TRIGGER [dbo].[VenueGliderSettings_DeletedOrUpdated] ON [dbo].[VenueGliderSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueGliderSettings_Changes]
	   ([SymbolId]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[MaximumSizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade]
	   ,[KGTLLTime_milliseconds]
	   ,[MinimumBPGrossGain]
	   ,[PreferLmaxDuringHedging]
	   ,[MinimumIntegratorPriceLife_milliseconds]
	   ,[MinimumIntegratorGlidePriceLife_milliseconds]
	   ,[GlidingCutoffSpan_milliseconds]
	   ,[MinimumSizeToTrade]
	   ,[MinimumFillSize]
	   ,[MaximumSourceBookTiers]
	   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
	   ,[ReferenceLmaxBookOnly]
	   ,[BidEnabled]
	   ,[AskEnabled]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[MaximumSizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[KGTLLTime_milliseconds]
		,deleted.[MinimumBPGrossGain]
		,deleted.[PreferLmaxDuringHedging]
		,deleted.[MinimumIntegratorPriceLife_milliseconds]
		,deleted.[MinimumIntegratorGlidePriceLife_milliseconds]
		,deleted.[GlidingCutoffSpan_milliseconds]
		,deleted.[MinimumSizeToTrade]
		,deleted.[MinimumFillSize]
		,deleted.[MaximumSourceBookTiers]
		,deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]
		,deleted.[ReferenceLmaxBookOnly]
		,deleted.[BidEnabled]
		,deleted.[AskEnabled]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueGliderSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueGliderSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([KGTLLTime_milliseconds]) AND (topUpdatedChange.[KGTLLTime_milliseconds] IS NULL OR topUpdatedChange.[KGTLLTime_milliseconds] != deleted.[KGTLLTime_milliseconds]))
		OR (UPDATE([MinimumBPGrossGain]) AND (topUpdatedChange.[MinimumBPGrossGain] IS NULL OR topUpdatedChange.[MinimumBPGrossGain] != deleted.[MinimumBPGrossGain]))
		OR (UPDATE([PreferLmaxDuringHedging]) AND (topUpdatedChange.[PreferLmaxDuringHedging] IS NULL OR topUpdatedChange.[PreferLmaxDuringHedging] != deleted.[PreferLmaxDuringHedging]))
		OR (UPDATE([MinimumIntegratorPriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] != deleted.[MinimumIntegratorPriceLife_milliseconds]))
		OR (UPDATE([MinimumIntegratorGlidePriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorGlidePriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorGlidePriceLife_milliseconds] != deleted.[MinimumIntegratorGlidePriceLife_milliseconds]))
		OR (UPDATE([GlidingCutoffSpan_milliseconds]) AND (topUpdatedChange.[GlidingCutoffSpan_milliseconds] IS NULL OR topUpdatedChange.[GlidingCutoffSpan_milliseconds] != deleted.[GlidingCutoffSpan_milliseconds]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([MaximumSourceBookTiers]) AND (topUpdatedChange.[MaximumSourceBookTiers] IS NULL OR topUpdatedChange.[MaximumSourceBookTiers] != deleted.[MaximumSourceBookTiers]))
		OR (UPDATE([MinimumBasisPointsFromKgtPriceToFastCancel]) AND (topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] IS NULL OR topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] != deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]))
		OR (UPDATE([ReferenceLmaxBookOnly]) AND (topUpdatedChange.[ReferenceLmaxBookOnly] IS NULL OR topUpdatedChange.[ReferenceLmaxBookOnly] != deleted.[ReferenceLmaxBookOnly]))
		OR (UPDATE([BidEnabled]) AND (topUpdatedChange.[BidEnabled] IS NULL OR topUpdatedChange.[BidEnabled] != deleted.[BidEnabled]))
		OR (UPDATE([AskEnabled]) AND (topUpdatedChange.[AskEnabled] IS NULL OR topUpdatedChange.[AskEnabled] != deleted.[AskEnabled]))
END
GO

INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FS1'), 'Glider')
INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FS2'), 'Glider')
GO



CREATE PROCEDURE [dbo].[GetUpdatedVenueGliderSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueGliderSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MaximumSizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[KGTLLTime_milliseconds]
			,[MinimumBPGrossGain]
			,[PreferLmaxDuringHedging]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumIntegratorGlidePriceLife_milliseconds]
			,[GlidingCutoffSpan_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,[MinimumBasisPointsFromKgtPriceToFastCancel]
			,[ReferenceLmaxBookOnly]
			,[BidEnabled]
			,[AskEnabled]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueGliderSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name
	END
END
GO

GRANT EXECUTE ON [dbo].[GetUpdatedVenueGliderSystemSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueGliderSystemSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetVenueGliderSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[MinimumBPGrossGain],
		sett.[MinimumBPGrossGain] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossGainDecimal],
		sett.[PreferLmaxDuringHedging],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumIntegratorGlidePriceLife_milliseconds],
		sett.[GlidingCutoffSpan_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[ReferenceLmaxBookOnly],
		sett.[BidEnabled],
		sett.[AskEnabled],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

GRANT EXECUTE ON [dbo].[GetVenueGliderSystemSettingsAndStats_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[VenueGliderSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@BidEnabled [bit],
	@AskEnabled [bit],
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@MinimumBPGrossGain  [decimal](18,6),
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumIntegratorGlidePriceLife_milliseconds [int],
	@GlidingCutoffSpan_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel  [decimal](18,6),
	@ReferenceLmaxBookOnly [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Glider'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Stream TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	
	BEGIN TRANSACTION
	BEGIN TRY
		
		INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

		SELECT @TradingSystemId = scope_identity();
		
		--make sure that default switches values are correct
		INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
		SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
		FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
		WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
		
		INSERT INTO [dbo].[VenueGliderSettings]
			   ([SymbolId]
			   ,[CounterpartyId]
			   ,[TradingSystemId]
			   ,[MaximumSizeToTrade]
			   ,[BestPriceImprovementOffsetBasisPoints]
			   ,[MaximumDiscountBasisPointsToTrade]
			   ,[KGTLLTime_milliseconds]
			   ,[MinimumBPGrossGain]
			   ,[PreferLmaxDuringHedging]
			   ,[MinimumIntegratorPriceLife_milliseconds]
			   ,[MinimumIntegratorGlidePriceLife_milliseconds]
			   ,[GlidingCutoffSpan_milliseconds]
			   ,[MinimumSizeToTrade]
			   ,[MinimumFillSize]
			   ,[MaximumSourceBookTiers]
			   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
			   ,[ReferenceLmaxBookOnly]
			   ,[BidEnabled]
			   ,[AskEnabled])
		 VALUES
			   (@SymbolId
			   ,@CounterpartyId
			   ,@TradingSystemId
			   ,@MaximumSizeToTrade 
			   ,@BestPriceImprovementOffsetBasisPoints
			   ,@MaximumDiscountBasisPointsToTrade
			   ,@KGTLLTime_milliseconds
			   ,@MinimumBPGrossGain
			   ,0
			   ,@MinimumIntegratorPriceLife_milliseconds
			   ,@MinimumIntegratorGlidePriceLife_milliseconds
			   ,@GlidingCutoffSpan_milliseconds
			   ,@MinimumSizeToTrade
			   ,@MinimumFillSize
			   ,@MaximumSourceBookTiers
			   ,@MinimumBasisPointsFromKgtPriceToFastCancel
			   ,@ReferenceLmaxBookOnly
			   ,@BidEnabled
			   ,@AskEnabled)
		
	
		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END
GO

GRANT EXECUTE ON [dbo].[VenueGliderSettingsInsertSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[VenueGliderSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@MinimumBPGrossGain  [decimal](18,6),
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumIntegratorGlidePriceLife_milliseconds [int],
	@GlidingCutoffSpan_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel [decimal](18,6),
	@ReferenceLmaxBookOnly [bit],
	@BidEnabled [bit],
	@AskEnabled [bit]
)
AS
BEGIN
	UPDATE sett
	SET
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[KGTLLTime_milliseconds] = @KGTLLTime_milliseconds,
		[MinimumBPGrossGain] = @MinimumBPGrossGain,
		[PreferLmaxDuringHedging] = 0,
		[MinimumIntegratorPriceLife_milliseconds] = @MinimumIntegratorPriceLife_milliseconds,
		[MinimumIntegratorGlidePriceLife_milliseconds] = @MinimumIntegratorGlidePriceLife_milliseconds,
		[GlidingCutoffSpan_milliseconds] = @GlidingCutoffSpan_milliseconds,
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MinimumFillSize] = @MinimumFillSize,
		[MaximumSourceBookTiers] = @MaximumSourceBookTiers,
		[MinimumBasisPointsFromKgtPriceToFastCancel] = @MinimumBasisPointsFromKgtPriceToFastCancel,
		[ReferenceLmaxBookOnly] = @ReferenceLmaxBookOnly,
		[BidEnabled] = @BidEnabled,
		[AskEnabled] = @AskEnabled
	FROM
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

GRANT EXECUTE ON [dbo].[VenueGliderSettingsUpdateSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROCEDURE [dbo].[VenueSystemDeleteOne_SP]
(
	@TradingSystemId int
)
AS
BEGIN

	BEGIN TRANSACTION;
	
	DECLARE @DeletedCnt INT = 0;
	
	DELETE sett
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
		
	DELETE sett
	FROM
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	IF @DeletedCnt > 0
	BEGIN
		DELETE FROM
			[dbo].[TradingSystemActions]
		WHERE 
			[TradingSystemId] = @TradingSystemId
			
		DELETE FROM 
			[dbo].[TradingSystem]
		WHERE 
			[TradingSystemId] = @TradingSystemId
	END
	
	
	COMMIT TRANSACTION;
END
GO

ALTER PROCEDURE [dbo].[VenueSystemDeleteMulti_SP]
(
	@TradingSystemId IdsTable READONLY
)
AS
BEGIN

	BEGIN TRANSACTION;
	
	DECLARE @DeletedCnt INT = 0;
	
	DELETE sett
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
		

	DELETE FROM
		[dbo].[TradingSystemActions]
	WHERE 
		[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
		
	DELETE FROM 
		[dbo].[TradingSystem]
	WHERE 
		[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	COMMIT TRANSACTION;
END
GO

CREATE PROCEDURE [dbo].[GetLastVenueGliderSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MaximumSizeToTrade]
      ,[BestPriceImprovementOffsetBasisPoints]
      ,[MaximumDiscountBasisPointsToTrade]
      ,[KGTLLTime_milliseconds]
      ,[MinimumBPGrossGain]
      ,[MinimumIntegratorPriceLife_milliseconds]
	  ,[MinimumIntegratorGlidePriceLife_milliseconds]
	  ,[GlidingCutoffSpan_milliseconds]
      ,[MinimumSizeToTrade]
      ,[MinimumFillSize]
      ,[MaximumSourceBookTiers]
      ,[MinimumBasisPointsFromKgtPriceToFastCancel]
      ,[ReferenceLmaxBookOnly]
      ,[BidEnabled]
      ,[AskEnabled]
	FROM 
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END
GO

GRANT EXECUTE ON [dbo].[GetLastVenueGliderSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROCEDURE [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
AS
BEGIN

	DECLARE @CntOfAllowedEnabledSystemsPerSymbol INT = 3

	DECLARE @ErrorMsg VARCHAR(MAX) = ''
	DECLARE @IsError BIT = 0
	DECLARE @Tab CHAR = CHAR(9)
	DECLARE @CR CHAR = CHAR(13)
	DECLARE @LF CHAR = CHAR(10)

	;WITH systemCounts AS
	(
		SELECT 
			sym.[Name] AS Symbol, 
			cpt.[CounterpartyCode] AS Counterparty,
			streamSys.[TradingSystemId], 
			COUNT(sym.[Name]) OVER (PARTITION BY sym.[Name], cpt.[CounterpartyCode]) AS systemsForSymbolCnt
		FROM 
			(
			SELECT [TradingSystemId], [SymbolId], [CounterpartyId] FROM [dbo].[VenueStreamSettings]
			UNION ALL
			SELECT [TradingSystemId], [SymbolId], [CounterpartyId] FROM [dbo].[VenueGliderSettings]
			) streamSys
			INNER JOIN [dbo].[TradingSystemActions] tsa ON streamSys.[TradingSystemId] = tsa.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] sym on streamSys.[SymbolId] = sym.[Id]
			INNER JOIN [dbo].Counterparty cpt on streamSys.[CounterpartyId] = cpt.[CounterpartyId]
		WHERE 
			tsa.[Enabled] = 1
			--AND tsa.[GoFlatOnly] = 0
			--AND tsa.[OrderTransmissionDisabled] = 0
	)
	SELECT
		@IsError = 1,
		@ErrorMsg = @ErrorMsg + @CR + @LF + 'Symbol: ' + Symbol + ', Destination: ' + Counterparty + ', Group Name: ' + grp.[TradingSystemGroupName] + ',' + @Tab + ' SystemId: ' + CAST(systemCounts.TradingSystemId AS VARCHAR(10)) + ',' + @Tab + ' Total systems per this symbol: ' + CAST(systemsForSymbolCnt AS VARCHAR(4)) + ',' + @Tab + ' Total systems per this symbol allowed: ' + CAST(@CntOfAllowedEnabledSystemsPerSymbol AS VARCHAR(4))
	FROM
		systemCounts
		INNER JOIN [dbo].[TradingSystem] sys ON systemCounts.[TradingSystemId] = sys.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystemGroup] grp ON sys.[TradingSystemGroupId] = grp.[TradingSystemGroupId]
	WHERE
		systemsForSymbolCnt > @CntOfAllowedEnabledSystemsPerSymbol
	ORDER BY
		systemsForSymbolCnt DESC,
		grp.[TradingSystemGroupName] ASC,
		Counterparty ASC,
		sys.[TradingSystemId] ASC

	IF @IsError = 1
		RAISERROR ('Error - Action reverted - There would be too many enabled stream systmes per some symbols:%s', 16, 1, @ErrorMsg)

END
GO

--COMMIT TRANSACTION Install_06_01_001;
--GO
ROLLBACK TRANSACTION Install_06_01_001;
GO