--Disable FKs first
ALTER TABLE [dbo].[Tick] DROP CONSTRAINT [FK_Tick_FXPair]
GO
ALTER TABLE [dbo].[MarketData] DROP CONSTRAINT [FK_MarketData_FxPair]
GO


--Add new symbols
SET IDENTITY_INSERT [dbo].[FxPair] ON 

INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (157, N'CHFMXN', 124, 84, 1000, 14.9533, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (158, N'USDINR', 137, 56, 100, 61.1750, NULL, NULL)
GO

SET IDENTITY_INSERT [dbo].[FxPair] OFF



--unfortunately not possible
--ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction (tinyint)
--AS RANGE LEFT FOR VALUES (3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 22, 43, 45, 46, 47, 48, 49, 50, 51, 56, 57, 58, 59, 60, 61, 62, 63, 65, 66, 67, 68, 70, 72, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 95, 96, 97, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 113, 117, 128, 129, 133, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156)
--GO

--adding partitions for new symbols
ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme NEXT USED [MarketDataFileGroup]
GO

ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (157);
GO

ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme NEXT USED [MarketDataFileGroup]
GO

ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction() 
    SPLIT RANGE (158);
GO


--Adding back the FKs
ALTER TABLE [dbo].[Tick]  WITH NOCHECK ADD  CONSTRAINT [FK_Tick_FXPair] FOREIGN KEY([FXPairID])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO

ALTER TABLE [dbo].[MarketData]  WITH NOCHECK ADD  CONSTRAINT [FK_MarketData_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO