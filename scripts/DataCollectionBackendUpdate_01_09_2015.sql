
SET IDENTITY_INSERT [dbo].[LiquidityProvider] ON 
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (24, N'PrimeXM', N'PXM')
GO
SET IDENTITY_INSERT [dbo].[LiquidityProvider] OFF
GO


SET IDENTITY_INSERT [dbo].[LiqProviderStream] ON 
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (111, N'PX1', 24)
GO
SET IDENTITY_INSERT [dbo].[LiqProviderStream] OFF
GO


INSERT INTO [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode])
SELECT 
	integratorCpt.[CounterpartyId] AS IntegratorDBCounterpartyId,
	dcCpt.[LiqProviderStreamId] AS DCDBCounterpartyId,
    dcCpt.[LiqProviderStreamName] AS CounterpartyCode
FROM
	[dbo].[LiqProviderStream] dcCpt
	INNER JOIN [Integrator].[dbo].[Counterparty] integratorCpt ON dcCpt.LiqProviderStreamName = integratorCpt.CounterpartyCode
WHERE
	dcCpt.[LiqProviderStreamId] NOT IN (SELECT DCDBCounterpartyId FROM [dbo].[_Internal_CounterpartyIdsMapping])