SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetLastBankPricesForPair_SP
	@TopCount INT,
	@SymbolName CHAR(6)
AS
BEGIN
	DECLARE @SymbolId INT

	SELECT @SymbolId = [FxPairId] FROM [dbo].[FxPair] WHERE [FxpCode] = @SymbolName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @SymbolName) WITH SETERROR
	END

	SELECT TOP (@TopCount)
		[Price],
		[IntegratorReceivedTimeUtc]
	FROM 
		[dbo].[MarketData] WITH(NOLOCK)
	WHERE 
		[FXPairId] = @SymbolId
		AND [RecordTypeId] = 0
	ORDER BY 
		[IntegratorReceivedTimeUtc] DESC
END
GO

GO
GRANT EXECUTE ON [dbo].[GetLastBankPricesForPair_SP] TO [IntegratorServiceAccount] AS [dbo]
GO