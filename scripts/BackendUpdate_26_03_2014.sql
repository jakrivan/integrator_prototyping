
ALTER TABLE [dbo].[Symbol] ADD LastKnownAskToBMeanPrice DECIMAL(18, 9) NULL
ALTER TABLE [dbo].[Symbol] ADD AskToBMeanPriceUpdated DATETIME2 NULL
ALTER TABLE [dbo].[Symbol] ADD LastKnownBidToBMeanPrice DECIMAL(18, 9) NULL
ALTER TABLE [dbo].[Symbol] ADD BidToBMeanPriceUpdated DATETIME2 NULL
GO


CREATE UNIQUE NONCLUSTERED INDEX [Symbol_Name] ON [dbo].[Symbol]
(
	[Name] ASC
) 
INCLUDE
(
	[Id]
)
ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [Symbol_ShortName] ON [dbo].[Symbol]
(
	[ShortName] ASC
) 
INCLUDE
(
	[Id]
)
ON [PRIMARY]
GO


CREATE FUNCTION [dbo].[csvlist_to_codesandprices_tbl] (@list varchar(MAX))
   RETURNS @tbl TABLE (code varchar(6) NOT NULL, price decimal(18,9)) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex('|', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1

	  IF @nextpos > @pos + 1
	  BEGIN

		  DECLARE @Code Varchar(6) = substring(@list, @pos + 1, @valuelen)
		  SELECT @pos = @nextpos

		  SELECT @nextpos = charindex('|', @list, @pos + 1)
		  SELECT @valuelen = CASE WHEN @nextpos > 0
								  THEN @nextpos
								  ELSE len(@list) + 1
							 END - @pos - 1

		  DECLARE @Price DECIMAL(18,9) = CONVERT(DECIMAL(18,9), substring(@list, @pos + 1, @valuelen))
		  SELECT @pos = @nextpos

		  INSERT @tbl (code, price)
			 VALUES (@Code, @Price)
	  END
	END
   RETURN
END
GO

CREATE PROCEDURE [dbo].[UpdateSymbolToBPrices_SP] (
	@AskPricesList VARCHAR(MAX),
	@BidPricesList VARCHAR(MAX)
	)
AS
BEGIN

	DECLARE @now DATETIME2 = GETUTCDATE()
	
	UPDATE
		sym
	SET
		sym.LastKnownAskToBMeanPrice = prices.price,
		sym.AskToBMeanPriceUpdated = @now
	FROM
		[dbo].[csvlist_to_codesandprices_tbl](@AskPricesList) prices
		INNER JOIN Symbol sym on sym.ShortName = prices.Code

	UPDATE
		sym
	SET
		sym.LastKnownBidToBMeanPrice = prices.price,
		sym.BidToBMeanPriceUpdated = @now
	FROM
		[dbo].[csvlist_to_codesandprices_tbl](@BidPricesList) prices
		INNER JOIN Symbol sym on sym.ShortName = prices.Code

END
GO

GRANT EXECUTE ON [dbo].[UpdateSymbolToBPrices_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


ALTER PROCEDURE [dbo].[GetStatisticsPerPair_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	SELECT
		Symbol.Name AS Symbol,
		positions.NopBasePol,
		positions.NopTermPol,
		positions.DealsNum as DealsNum,
		--PnL = term position + yield of immediate close of outstanding base position
		--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
		positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * positions.NopBasePol AS PnlTermPol,
		(positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * positions.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
		positions.VolumeInCcy1 AS VolumeInCcy1,
		positions.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd
	FROM
		(
		SELECT
			deal.SymbolId AS SymbolId,
			SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
			SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
			COUNT(deal.SymbolId) AS DealsNum,
			SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1
		FROM 
			DealExternalExecuted_DailyView deal WITH(NOEXPAND)
			
		WHERE
			[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		GROUP BY
			deal.SymbolId
		) positions
		INNER JOIN  [dbo].[Symbol] symbol ON positions.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
	ORDER BY
		symbol.Name
END
GO

ALTER PROCEDURE [dbo].[GetStatisticsPerPair_SP] (
	@Day DATE
	)
AS
BEGIN
	SELECT
		Symbol.Name AS Symbol,
		positions.NopBasePol,
		positions.NopTermPol,
		--PnL = term position + yield of immediate close of outstanding base position
		--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
		positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * positions.NopBasePol AS PnlTermPol,
		(positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * positions.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
		positions.VolumeInCcy1 AS VolumeInCcy1,
		positions.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd
	FROM
		(
		SELECT
			deal.SymbolId AS SymbolId,
			SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
			SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
			SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1
		FROM 
			[dbo].[DealExternalExecuted] deal	
		WHERE
			[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		GROUP BY
			deal.SymbolId
		) positions
		INNER JOIN  [dbo].[Symbol] symbol ON positions.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
	ORDER BY
		symbol.Name
END
GO



CREATE PROCEDURE [dbo].[GetSessionsClaimStatus_SP]
AS
BEGIN
	SELECT
		ctp.CounterpartyCode AS CounterpartyCode,
		instance.Name AS InstanceName
	FROM
		[dbo].[Counterparty] ctp 
		LEFT JOIN [dbo].[IntegratorInstanceSessionClaim] claim ON claim.[CounterpartyId] = ctp.[CounterpartyId]
		LEFT JOIN [dbo].[IntegratorInstance] instance ON claim.[IntegratorInstanceId] = instance.[Id]
	WHERE
		ctp.Active = 1
	ORDER BY CounterpartyCode ASC
END
GO

GRANT EXECUTE ON [dbo].[GetSessionsClaimStatus_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetInstanceNames_SP]
AS
BEGIN
	SELECT
		[Name]
	FROM 
		[dbo].[IntegratorInstance]
END
GO

GRANT EXECUTE ON [dbo].[GetInstanceNames_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[ForceClaimSessionsForInstance_SP]( 
	@InstanceName varchar(50),
	@Counterparty char(3)
	)
AS
BEGIN
	DECLARE @CounterpartyId TINYINT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @Counterparty

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Counterparty not found in DB: %s', 16, 2, @Counterparty) WITH SETERROR
	END

	--First force release the session 
	DELETE FROM [dbo].[IntegratorInstanceSessionClaim]
	WHERE CounterpartyId = @CounterpartyId
	
	--If there is no replacement instance then we are done
	IF @InstanceName IS NOT NULL
	BEGIN
		DECLARE @InstanceId TINYINT
		
		SELECT @InstanceId = Id FROM [dbo].[IntegratorInstance] WHERE Name = @InstanceName

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'InstanceName not found in DB: %s', 16, 2, @InstanceName) WITH SETERROR
		END
		
		--Then claim it
		INSERT INTO [dbo].[IntegratorInstanceSessionClaim] (IntegratorInstanceId, CounterpartyId)
		VALUES (@InstanceId, @CounterpartyId)
	END
END
GO

GRANT EXECUTE ON [dbo].[ForceClaimSessionsForInstance_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROC [dbo].[UpdateUsdConversionMultiplier_SP] (
	@PricesList VARCHAR(MAX)
	)
AS
BEGIN
	DECLARE @now DATETIME2 = GETUTCDATE()
	
	UPDATE
		curr
	SET
		curr.LastKnownUsdConversionMultiplier = prices.price,
		curr.UsdConversionMultiplierUpdated = @now
	FROM
		[dbo].[csvlist_to_codesandprices_tbl](@PricesList) prices
		INNER JOIN [dbo].[Currency] curr on curr.CurrencyAlphaCode = prices.Code
END
GO

EXEC sp_rename UpdateUsdConversionMultiplier_SP, UpdateUsdConversionMultipliers_SP
GO

EXEC sp_rename 'dbo.DealExternalExecutedTicket.DealExternalExecutedTicketId', 'InternalTransactionIdentity', 'COLUMN';
GO


ALTER PROC [dbo].[UpdateSymbolReferencePrice_SP] (
	@PricesList VARCHAR(MAX)
	)
AS
BEGIN
	DECLARE @now DATETIME2 = GETUTCDATE()
	
	UPDATE
		sym
	SET
		sym.LastKnownMeanReferencePrice = prices.price,
		sym.MeanReferencePriceUpdated = @now
	FROM
		[dbo].[csvlist_to_codesandprices_tbl](@PricesList) prices
		INNER JOIN Symbol sym on sym.ShortName = prices.Code
END
GO

EXEC sp_rename UpdateSymbolReferencePrice_SP, UpdateSymbolReferencePrices_SP
GO

ALTER TABLE [dbo].[DealExternalExecutedTicketType] ALTER COLUMN DealExternalExecutedTicketTypeName NVARCHAR(17) NOT NULL
GO

ALTER PROCEDURE [dbo].[InsertNewDealTicket_SP]( 
	@InternalDealId [nvarchar](50),
	@TicketIntegratorSentOrReceivedTimeUtc [datetime2](7),
	@CounterpartySentTimeUtc [datetime2](7),
	@DealExternalExecutedTicketType [nvarchar](17),
	@StpCounterpartyName [varchar](16),
	@FixMessageRaw nvarchar(1024),
	@FixMessageParsed nvarchar(MAX)
	)
AS
BEGIN

	DECLARE @DealExternalExecutedTicketTypeId INT
	DECLARE @StpCounterpartyId TINYINT

	SELECT @DealExternalExecutedTicketTypeId = [DealExternalExecutedTicketTypeId] FROM [dbo].[DealExternalExecutedTicketType] WHERE [DealExternalExecutedTicketTypeName] = @DealExternalExecutedTicketType

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealExternalExecutedTicketType not found in DB: %s', 16, 2, @DealExternalExecutedTicketType) WITH SETERROR
	END
	
	SELECT @StpCounterpartyId = [StpCounterpartyId] FROM [dbo].[StpCounterparty] WHERE [StpCounterpartyName] = @StpCounterpartyName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'StpCounterparty not found in DB: %s', 16, 2, @StpCounterpartyName) WITH SETERROR
	END

	INSERT INTO 
		[dbo].[DealExternalExecutedTicket]
           ([InternalTransactionIdentity]
           ,[IntegratorSentOrReceivedTimeUtc]
		   ,[CounterpartySentTimeUtc]
           ,[DealExternalExecutedTicketTypeId]
		   ,[StpCounterpartyId]
           ,[FixMessageRaw]
		   ,[FixMessageParsed])
     VALUES
           (@InternalDealId
           ,@TicketIntegratorSentOrReceivedTimeUtc
		   ,@CounterpartySentTimeUtc
           ,@DealExternalExecutedTicketTypeId
		   ,@StpCounterpartyId
           ,@FixMessageRaw
		   ,@FixMessageParsed)
END
GO

INSERT [dbo].[DealExternalExecutedTicketType] ([DealExternalExecutedTicketTypeId], [DealExternalExecutedTicketTypeName]) VALUES (5, N'SentToTraiana')
INSERT [dbo].[DealExternalExecutedTicketType] ([DealExternalExecutedTicketTypeId], [DealExternalExecutedTicketTypeName]) VALUES (6, N'AcceptedByTraiana')
INSERT [dbo].[DealExternalExecutedTicketType] ([DealExternalExecutedTicketTypeId], [DealExternalExecutedTicketTypeName]) VALUES (7, N'RejectedByTraiana')
GO


CREATE TABLE [dbo].[AggregatedExternalExecutedTicket](
	[AggregatedExternalExecutedTicketCounterpartyId] [nvarchar](50) NOT NULL,
	[IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
	[CounterpartySentTimeUtc] [datetime2](7) NULL,
	[AggregatedSize] decimal(18,2) NOT NULL,
	[AggregatedPrice] decimal(18,10) NOT NULL,
	[StpCounterpartyId] [tinyint] NOT NULL,
	[FixMessageRaw] [nvarchar](1024) NOT NULL,
	[FixMessageParsed] [nvarchar](max) NOT NULL,
	[DatePartIntegratorReceivedTimeUtc]  AS (CONVERT([date],[IntegratorReceivedTimeUtc])) PERSISTED
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[AggregatedExternalExecutedTicket]  WITH CHECK ADD  CONSTRAINT [FK_AggregatedExternalExecutedTicket_StpCounterparty] FOREIGN KEY([StpCounterpartyId])
REFERENCES [dbo].[StpCounterparty] ([StpCounterpartyId])
GO

ALTER TABLE [dbo].[AggregatedExternalExecutedTicket] CHECK CONSTRAINT [FK_AggregatedExternalExecutedTicket_StpCounterparty]
GO


CREATE CLUSTERED INDEX [IX_AggregatedExternalExecutedTicket_DatePartIntegratorReceivedTimeUtc] ON [dbo].[AggregatedExternalExecutedTicket]
(
	[DatePartIntegratorReceivedTimeUtc] ASC
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_AggregatedExternalExecutedTicket_AggregatedExternalExecutedTicketCounterpartyId] ON [dbo].[AggregatedExternalExecutedTicket]
(
	[AggregatedExternalExecutedTicketCounterpartyId] ASC
) ON [PRIMARY]
GO



CREATE TABLE [dbo].[AggregatedExternalExecutedTicketMap](
	[AggregatedExternalExecutedTicketCounterpartyId] [nvarchar](50) NOT NULL,
	[InternalTransactionIdentity] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO

--no FKs as we don't have PKs in source tables (for perf reasons)
--ALTER TABLE [dbo].[AggregatedExternalExecutedTicket]  WITH CHECK ADD  CONSTRAINT [FK_AggregatedExternalExecutedTicket_StpCounterparty] FOREIGN KEY([StpCounterpartyId])
--ALTER TABLE [dbo].[AggregatedExternalExecutedTicketMap]  WITH CHECK ADD  CONSTRAINT [FK_AggregatedExternalExecutedTicketMap_AggregatedExternalExecutedTicketCounterpartyId] FOREIGN KEY([AggregatedExternalExecutedTicketCounterpartyId])
--REFERENCES [dbo].[AggregatedExternalExecutedTicket] ([AggregatedExternalExecutedTicketCounterpartyId])
--GO

--ALTER TABLE [dbo].[AggregatedExternalExecutedTicketMap] CHECK CONSTRAINT [FK_AggregatedExternalExecutedTicketMap_AggregatedExternalExecutedTicketCounterpartyId]
--GO

--ALTER TABLE [dbo].[AggregatedExternalExecutedTicketMap]  WITH CHECK ADD  CONSTRAINT [FK_AggregatedExternalExecutedTicketMap_InternalTransactionIdentity] FOREIGN KEY([InternalTransactionIdentity])
--REFERENCES [dbo].[AggregatedExternalExecutedTicket] ([AggregatedExternalExecutedTicketCounterpartyId])
--GO

--ALTER TABLE [dbo].[AggregatedExternalExecutedTicketMap] CHECK CONSTRAINT [FK_AggregatedExternalExecutedTicketMap_InternalTransactionIdentity]
--GO

ALTER FUNCTION [dbo].[csvlist_to_codes_tbl] (@list nvarchar(MAX))
   RETURNS @tbl TABLE (code varchar(50) NOT NULL) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1
      INSERT @tbl (code)
         VALUES (substring(@list, @pos + 1, @valuelen))
      SELECT @pos = @nextpos
   END
   RETURN
END
GO

CREATE PROCEDURE [dbo].[InsertNewAggregatedDealTicket_SP]( 
	@AggregatedTradeReportCounterpartyId [nvarchar](50),
	@TicketIntegratorReceivedTimeUtc [datetime2](7),
	@CounterpartySentTimeUtc [datetime2](7),
	@AggregatedPrice decimal(18,10),
	@AggregatedSize decimal(18,2),
	@StpCounterpartyName [varchar](16),
	@FixMessageRaw nvarchar(1024),
	@FixMessageParsed nvarchar(MAX),
	@ChildTradeIdsList nvarchar(MAX)
	)
AS
BEGIN

	DECLARE @StpCounterpartyId TINYINT
	
	SELECT @StpCounterpartyId = [StpCounterpartyId] FROM [dbo].[StpCounterparty] WHERE [StpCounterpartyName] = @StpCounterpartyName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'StpCounterparty not found in DB: %s', 16, 2, @StpCounterpartyName) WITH SETERROR
	END

	INSERT INTO [dbo].[AggregatedExternalExecutedTicket]
           ([AggregatedExternalExecutedTicketCounterpartyId]
           ,[IntegratorReceivedTimeUtc]
           ,[CounterpartySentTimeUtc]
           ,[AggregatedSize]
           ,[AggregatedPrice]
           ,[StpCounterpartyId]
           ,[FixMessageRaw]
           ,[FixMessageParsed])
     VALUES
           (@AggregatedTradeReportCounterpartyId
           ,@TicketIntegratorReceivedTimeUtc
           ,@CounterpartySentTimeUtc
           ,@AggregatedSize
           ,@AggregatedPrice
           ,@StpCounterpartyId
           ,@FixMessageRaw
           ,@FixMessageParsed)
		   
		   
	INSERT INTO [dbo].[AggregatedExternalExecutedTicketMap]
		([AggregatedExternalExecutedTicketCounterpartyId],
		[InternalTransactionIdentity])
	SELECT
		@AggregatedTradeReportCounterpartyId, Code
	FROM
		[dbo].[csvlist_to_codes_tbl](@ChildTradeIdsList)	
END
GO

GRANT EXECUTE ON [dbo].[InsertNewAggregatedDealTicket_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="MessageBusSettings_IntegratorEndpointConnectionInfo" nillable="true" type="MessageBusSettings_IntegratorEndpointConnectionInfo" />
  <xs:complexType name="MessageBusSettings_IntegratorEndpointConnectionInfo">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ConnectionType" type="ConnectionType" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="ConnectionType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Localhost" />
      <xs:enumeration value="PrivateIPAddress" />
      <xs:enumeration value="PublicIPAddress" />
      <xs:enumeration value="Hostname" />
    </xs:restriction>
  </xs:simpleType>
</xs:schema>'
GO


--manual update of xsd settings

 --   SELECT 'UPDATE [dbo].[Settings] SET [SettingsXml] = ''' + 
	--CONVERT(VARCHAR(MAX), [SettingsXml]) +
	--''', [LastUpdated] = GETUTCDATE() WHERE [Name] = ''' + 
	--CONVERT(VARCHAR(MAX), [Name]) + ''' AND [Id] = ' + CONVERT(VARCHAR(5), [Id])
	--FROM [dbo].[Settings]

	--where name like '%Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo
	
INSERT INTO
	[dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName])
SELECT
	[Id], 'Kreslik.Integrator.DAL_IntegratorConnection' 
FROM [dbo].[IntegratorEnvironmentType]
WHERE
	Name = 'Splitter'
	
	
	
DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="BusinessLayerSettings" nillable="true" type="BusinessLayerSettings" />
  <xs:complexType name="BusinessLayerSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MarketableClientOrdersMatchingStrategy" type="MarketableClientOrdersMatchingStrategy" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="ConsecutiveRejectionsStopStrategyBehavior" type="ConsecutiveRejectionsStopStrategySettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartWithSessionsStopped" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="AutoKillInternalOrdersOnOrdersTransmissionDisabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailSubject" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailBody" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalRejectedOrderTriggerPhrasesCsv" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderCounterpartyContacts" type="ArrayOfIgnoredOrderCounterpartyContact" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PreventMatchingToSameCounterpartyAfterOrderRejectBehavior" type="PreventMatchingToSameCounterpartyAfterOrderRejectSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="MarketableClientOrdersMatchingStrategy">
    <xs:restriction base="xs:string">
      <xs:enumeration value="ImmediateMatching" />
      <xs:enumeration value="NextFreshPriceMatching" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ConsecutiveRejectionsStopStrategySettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="StrategyEnabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="NumerOfRejectionsToTriggerStrategy" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAgeRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceBpRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultCounterpartyRank" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRanks" type="ArrayOfCounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartyRank">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartyRank" nillable="true" type="CounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartyRank">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="Rank" type="xs:decimal" />
  </xs:complexType>
  <xs:complexType name="ArrayOfIgnoredOrderCounterpartyContact">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="IgnoredOrderCounterpartyContact" nillable="true" type="IgnoredOrderCounterpartyContact" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="IgnoredOrderCounterpartyContact">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="EmailContacts" type="xs:string" />
  </xs:complexType>
  <xs:complexType name="PreventMatchingToSameCounterpartyAfterOrderRejectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="StrategyEnabled" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartyCodes" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

--manualy update of xml - addition of <FatalRejectedOrderTriggerPhrasesCsv>throttl</FatalRejectedOrderTriggerPhrasesCsv>




CREATE PROCEDURE [dbo].[GetIntegratorProcessDetailsList_SP]
AS
BEGIN
	SELECT 
		process.[Id] AS ProcessId
		,instance.Name AS InstanceName
		,instance.Description AS InstanceDescription
		,processtype.Name AS ProcessName
		,processtype.Description AS ProcessDescription
		,state.Name AS State
		,endpoint.Name AS Endpoint
		,[LastHBTimeUtc]
		,CASE WHEN DATEADD(minute, 2, [LastHBTimeUtc]) < GETUTCDATE() THEN 1 ELSE 0 END AS IsOld
	FROM 
		[dbo].[IntegratorProcess] process
		INNER JOIN [dbo].[IntegratorInstance] instance ON process.IntegratorInstanceId = instance.Id
		INNER JOIN [dbo].[IntegratorProcessType] processtype ON process.IntegratorProcessTypeId = processtype.id
		INNER JOIN [dbo].[IntegratorProcessState] state ON process.IntegratorProcessStateId = state.Id
		INNER JOIN [dbo].[IntegratorEndpoint] endpoint ON process.IntegratorEndpointId = endpoint.Id
	ORDER BY
		instance.Id,
		[LastHBTimeUtc] DESC
END
GO

GRANT EXECUTE ON [dbo].[GetIntegratorProcessDetailsList_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[DeleteIntegratorProcess_SP]
(
	@ProcessId INT
)
AS
BEGIN
	DELETE	FROM 
		[dbo].[IntegratorProcess]
	WHERE
		Id = @ProcessId
END
GO

GRANT EXECUTE ON [dbo].[DeleteIntegratorProcess_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

INSERT [dbo].[Dictionary_FIX] ([Name], [SettingsVersion], [DictionaryXml], [LastUpdated]) VALUES (N'FIX44_Traiana.xml', 1, N'<!--name=FIX44-TRAIANA-V2.0;version=${build.version.major}.${build.version.minor}.${build.version.sp}.${build.version.patch}-->
<fix major="4" minor="4">
	<header>
		<field name="BeginString" required="Y"/>
		<field name="BodyLength" required="Y"/>
		<field name="MsgType" required="Y"/>
		<field name="SenderCompID" required="Y"/>
		<field name="TargetCompID" required="Y"/>
		<field name="OnBehalfOfCompID" required="N"/>
		<field name="DeliverToCompID" required="N"/>
		<field name="SecureDataLen" required="N"/>
		<field name="SecureData" required="N"/>
		<field name="MsgSeqNum" required="Y"/>
		<field name="SenderSubID" required="N"/>
		<field name="SenderLocationID" required="N"/>
		<field name="TargetSubID" required="N"/>
		<field name="TargetLocationID" required="N"/>
		<field name="OnBehalfOfSubID" required="N"/>
		<field name="OnBehalfOfLocationID" required="N"/>
		<field name="DeliverToSubID" required="N"/>
		<field name="DeliverToLocationID" required="N"/>
		<field name="PossDupFlag" required="N"/>
		<field name="PossResend" required="N"/>
		<field name="SendingTime" required="Y"/>
		<field name="OrigSendingTime" required="N"/>
		<field name="XmlDataLen" required="N"/>
		<field name="XmlData" required="N"/>
		<field name="MessageEncoding" required="N"/>
		<field name="LastMsgSeqNumProcessed" required="N"/>
		<group name="NoHops" required="N">
			<field name="HopCompID" required="N"/>
			<field name="HopSendingTime" required="N"/>
			<field name="HopRefID" required="N"/>
		</group>
	</header>
	<trailer>
		<field name="SignatureLength" required="N"/>
		<field name="Signature" required="N"/>
		<field name="CheckSum" required="Y"/>
	</trailer>
	<messages>
		<message name="Heartbeat" msgtype="0" msgcat="admin">
			<field name="TestReqID" required="N"/>
		</message>
		<message name="Logon" msgtype="A" msgcat="admin">
			<field name="EncryptMethod" required="Y"/>
			<field name="HeartBtInt" required="Y"/>
			<field name="RawDataLength" required="N"/>
			<field name="RawData" required="N"/>
			<field name="ResetSeqNumFlag" required="N"/>
			<field name="NextExpectedMsgSeqNum" required="N"/>
			<field name="MaxMessageSize" required="N"/>
			<group name="NoMsgTypes" required="N">
				<field name="RefMsgType" required="N"/>
				<field name="MsgDirection" required="N"/>
			</group>
			<field name="TestMessageIndicator" required="N"/>
			<field name="Username" required="N"/>
			<field name="Password" required="N"/>
		</message>
		<message name="TestRequest" msgtype="1" msgcat="admin">
			<field name="TestReqID" required="Y"/>
		</message>
		<message name="ResendRequest" msgtype="2" msgcat="admin">
			<field name="BeginSeqNo" required="Y"/>
			<field name="EndSeqNo" required="Y"/>
		</message>
		<message name="Reject" msgtype="3" msgcat="admin">
			<field name="RefSeqNum" required="Y"/>
			<field name="RefTagID" required="N"/>
			<field name="RefMsgType" required="N"/>
			<field name="SessionRejectReason" required="N"/>
			<field name="Text" required="N"/>
			<field name="EncodedTextLen" required="N"/>
			<field name="EncodedText" required="N"/>
		</message>
		<message name="SequenceReset" msgtype="4" msgcat="admin">
			<field name="GapFillFlag" required="N"/>
			<field name="NewSeqNo" required="Y"/>
		</message>
		<message name="Logout" msgtype="5" msgcat="admin">
			<field name="Text" required="N"/>
			<field name="EncodedTextLen" required="N"/>
			<field name="EncodedText" required="N"/>
		</message>
		<message name="BusinessMessageReject" msgtype="j" msgcat="app">
			<field name="RefSeqNum" required="N"/>
			<field name="RefMsgType" required="Y"/>
			<field name="BusinessRejectRefID" required="N"/>
			<field name="BusinessRejectReason" required="Y"/>
			<field name="Text" required="N"/>
			<field name="EncodedTextLen" required="N"/>
			<field name="EncodedText" required="N"/>
		</message>


		<!-- Traiana custom AE message -->
		<message name="TradeCaptureReport" msgtype="AE" msgcat="app">
			<field name="TradeReportID" required="Y"/>
			<field name="TradeReportTransType" required="N"/>
			<field name="TradeReportType" required="N"/>
			<field name="TradeRequestID" required="N"/>
			<field name="TrdType" required="N"/>
			<field name="TrdSubType" required="N"/>
			<field name="SecondaryTrdType" required="N"/>
			<field name="TransferReason" required="N"/>
			<field name="ExecType" required="N"/>
			<field name="TotNumTradeReports" required="N"/>
			<field name="LastRptRequested" required="N"/>
			<field name="UnsolicitedIndicator" required="N"/>
			<field name="SubscriptionRequestType" required="N"/>
			<field name="TradeReportRefID" required="N"/>
			<field name="SecondaryTradeReportRefID" required="N"/>
			<field name="SecondaryTradeReportID" required="N"/>
			<field name="TradeLinkID" required="N"/>
			<field name="TrdMatchID" required="N"/>
			<field name="ExecID" required="N"/>
			<field name="OrdStatus" required="N"/>
			<field name="SecondaryExecID" required="N"/>
			<field name="ExecRestatementReason" required="N"/>
			<field name="PreviouslyReported" required="Y"/>
			<field name="PriceType" required="N"/>
			<component name="Instrument" required="Y"/>
			<component name="FinancingDetails" required="N"/>
			<component name="OrderQtyData" required="N"/>
			<field name="QtyType" required="N"/>
			<component name="YieldData" required="N"/>
			<group name="NoUnderlyings" required="N">
				<component name="UnderlyingInstrument" required="N"/>
			</group>
			<field name="UnderlyingTradingSessionID" required="N"/>
			<field name="UnderlyingTradingSessionSubID" required="N"/>
			<field name="LastQty" required="Y"/>
			<field name="LastPx" required="Y"/>
			<field name="LastParPx" required="N"/>
			<field name="LastSpotRate" required="N"/>
			<field name="LastForwardPoints" required="N"/>
			<field name="LastMkt" required="N"/>
			<field name="TradeDate" required="Y"/>
			<field name="ClearingBusinessDate" required="N"/>
			<field name="AvgPx" required="N"/>
			<component name="SpreadOrBenchmarkCurveData" required="N"/>
			<field name="AvgPxIndicator" required="N"/>
			<component name="PositionAmountData" required="N"/>
			<field name="MultiLegReportingType" required="N"/>
			<field name="TradeLegRefID" required="N"/>
			<group name="NoLegs" required="N">
				<component name="InstrumentLeg" required="N"/>
				<field name="LegQty" required="N"/>
				<field name="LegSwapType" required="N"/>
				<component name="LegStipulations" required="N"/>
				<field name="LegPositionEffect" required="N"/>
				<field name="LegCoveredOrUncovered" required="N"/>
				<component name="NestedParties" required="N"/>
				<field name="LegRefID" required="N"/>
				<field name="LegPrice" required="N"/>
				<field name="LegSettlType" required="N"/>
				<field name="LegSettlDate" required="N"/>
				<field name="LegLastPx" required="N"/>
			</group>
			<field name="TransactTime" required="Y"/>
			<component name="TrdRegTimestamps" required="N"/>
			<field name="SettlType" required="N"/>
			<field name="SettlDate" required="N"/>
			<field name="MatchStatus" required="N"/>
			<field name="MatchType" required="N"/>
			<group name="NoSides" required="Y">
				<field name="Side" required="Y"/>
				<field name="OrderID" required="Y"/>
				<field name="SecondaryOrderID" required="N"/>
				<field name="ClOrdID" required="N"/>
				<field name="SecondaryClOrdID" required="N"/>
				<field name="ListID" required="N"/>
				<component name="Parties" required="N"/>
				<field name="Account" required="N"/>
				<field name="AcctIDSource" required="N"/>
				<field name="AccountType" required="N"/>
				<field name="ProcessCode" required="N"/>
				<field name="OddLot" required="N"/>
				<group name="NoClearingInstructions" required="N">
					<field name="ClearingInstruction" required="N"/>
				</group>
				<field name="ClearingFeeIndicator" required="N"/>
				<field name="TradeInputSource" required="N"/>
				<field name="TradeInputDevice" required="N"/>
				<field name="OrderInputDevice" required="N"/>
				<field name="Currency" required="N"/>
				<field name="ComplianceID" required="N"/>
				<field name="SolicitedFlag" required="N"/>
				<field name="OrderCapacity" required="N"/>
				<field name="OrderRestrictions" required="N"/>
				<field name="CustOrderCapacity" required="N"/>
				<field name="OrdType" required="N"/>
				<field name="ExecInst" required="N"/>
				<field name="TransBkdTime" required="N"/>
				<field name="TradingSessionID" required="N"/>
				<field name="TradingSessionSubID" required="N"/>
				<field name="TimeBracket" required="N"/>
				<component name="CommissionData" required="N"/>
				<field name="GrossTradeAmt" required="N"/>
				<field name="NumDaysInterest" required="N"/>
				<field name="ExDate" required="N"/>
				<field name="AccruedInterestRate" required="N"/>
				<field name="AccruedInterestAmt" required="N"/>
				<field name="InterestAtMaturity" required="N"/>
				<field name="EndAccruedInterestAmt" required="N"/>
				<field name="StartCash" required="N"/>
				<field name="EndCash" required="N"/>
				<field name="Concession" required="N"/>
				<field name="TotalTakedown" required="N"/>
				<field name="NetMoney" required="N"/>
				<field name="SettlCurrAmt" required="N"/>
				<field name="SettlCurrency" required="N"/>
				<field name="SettlCurrFxRate" required="N"/>
				<field name="SettlCurrFxRateCalc" required="N"/>
				<field name="PositionEffect" required="N"/>
				<field name="Text" required="N"/>
				<field name="EncodedTextLen" required="N"/>
				<field name="EncodedText" required="N"/>
				<field name="SideMultiLegReportingType" required="N"/>
				<group name="NoContAmts" required="N">
					<field name="ContAmtType" required="N"/>
					<field name="ContAmtValue" required="N"/>
					<field name="ContAmtCurr" required="N"/>
				</group>
				<component name="Stipulations" required="N"/>
				<group name="NoMiscFees" required="N">
					<field name="MiscFeeAmt" required="N"/>
					<field name="MiscFeeCurr" required="N"/>
					<field name="MiscFeeType" required="N"/>
					<field name="MiscFeeBasis" required="N"/>
				</group>
				<field name="ExchangeRule" required="N"/>
				<field name="TradeAllocIndicator" required="N"/>
				<field name="PreallocMethod" required="N"/>
				<field name="AllocID" required="N"/>
				<group name="NoAllocs" required="N">
					<field name="AllocAccount" required="N"/>
					<field name="AllocAcctIDSource" required="N"/>
					<field name="AllocSettlCurrency" required="N"/>
					<field name="IndividualAllocID" required="N"/>
					<component name="NestedParties2" required="N"/>
					<field name="AllocQty" required="N"/>
				</group>
			</group>
			<field name="CopyMsgIndicator" required="N"/>
			<field name="PublishTrdIndicator" required="N"/>
			<field name="ShortSaleReason" required="N"/>
			<!-- custom Traiana -->
			<field name="Account" required="N"/>
			<field name="FutSettDate2" required="N"/>
			<field name="OrderQuantity1" required="N"/>
			<field name="OrderQuantity2" required="N"/>
			<field name="LegCalculatedCcyLastQty" required="N"/>
			<field name="CalculatedCcyLastQty" required="N"/>
			<field name="MarketZone" required="N"/>
			<field name="EndPaymentDate" required="N"/>
			<field name="LastPx2" required="N"/>
			<field name="FXOptionStyle" required="N"/>
			<field name="CallOrPut" required="N"/>
			<field name="ExpiryTime" required="N"/>
			<field name="OptionDeliveryType" required="N"/>
			<field name="OptionProductType" required="N"/>
			<field name="ExerciseEndDate" required="N"/>
			<field name="TransactionCostAmt" required="N"/>
			<field name="TransactionCostCurrency" required="N"/>
			<field name="Deal Link Reference" required="N"/>
			<field name="SpotRate" required="N"/>
			<field name="NearFwdPoints" required="N"/>
			<field name="CompetingQuote" required="N"/>
			<field name="FarForwardPoints" required="N"/>
			<field name="CompetingQuoteLeg2" required="N"/>
			<group name="NoNettedTrades" required="N">
				<field name="ParentID" required="N"/>
				<field name="OrderId" required="N"/>
				<field name="ChildID" required="N"/>
			</group>
			<field name="State" required="N"/>
			<field name="BarrierStyle" required="N"/>
			<field name="BarrierLevel" required="N"/>
			<field name="BarrierDirection" required="N"/>
			<field name="BarrierRebate" required="N"/>
			<field name="BarrierLevel2" required="N"/>
			<field name="BarrierRebate2" required="N"/>
			<field name="BarrierRebateCurrency" required="N"/>
			<field name="BarrierRebateCurrency2" required="N"/>
			<field name="TouchType" required="N"/>
			<!-- -->
			<field name="TradingStyle" required="N"/>
			<field name="UniqueSwapIdentifier" required="N"/>
			<field name="UniqueSwapIdentifierFar" required="N"/>
			<field name="SwapID" required="N"/>
			<field name="ExecutionTime" required="N"/>
			<field name="ExecutingPlatform" required="N"/>
			<field name="ExecutionVenueType" required="N"/>
			<group name="NoTradeRepositories" required="N">
				<field name="TradeRepositoryName" required="N"/>
			</group>
		</message>
		<!-- end custom traiana AE message -->
		<message name="TradeCaptureReportAck" msgtype="AR" msgcat="app">
			<field name="TradeReportID" required="Y"/>
			<field name="TradeReportTransType" required="N"/>
			<field name="TradeReportType" required="N"/>
			<field name="TrdType" required="N"/>
			<field name="TrdSubType" required="N"/>
			<field name="SecondaryTrdType" required="N"/>
			<field name="TransferReason" required="N"/>
			<field name="ExecType" required="N"/>
			<field name="TradeReportRefID" required="N"/>
			<field name="SecondaryTradeReportRefID" required="N"/>
			<field name="TrdRptStatus" required="N"/>
			<field name="TradeReportRejectReason" required="N"/>
			<field name="SecondaryTradeReportID" required="N"/>
			<field name="SubscriptionRequestType" required="N"/>
			<field name="TradeLinkID" required="N"/>
			<field name="TrdMatchID" required="N"/>
			<field name="ExecID" required="N"/>
			<field name="SecondaryExecID" required="N"/>
			<component name="Instrument" required="N"/>
			<field name="TransactTime" required="N"/>
			<component name="TrdRegTimestamps" required="N"/>
			<field name="ResponseTransportType" required="N"/>
			<field name="ResponseDestination" required="N"/>
			<field name="Text" required="N"/>
			<field name="EncodedTextLen" required="N"/>
			<field name="EncodedText" required="N"/>
			<group name="NoLegs" required="N">
				<component name="InstrumentLeg" required="N"/>
				<field name="LegQty" required="N"/>
				<field name="LegSwapType" required="N"/>
				<component name="LegStipulations" required="N"/>
				<field name="LegPositionEffect" required="N"/>
				<field name="LegCoveredOrUncovered" required="N"/>
				<component name="NestedParties" required="N"/>
				<field name="LegRefID" required="N"/>
				<field name="LegPrice" required="N"/>
				<field name="LegSettlType" required="N"/>
				<field name="LegSettlDate" required="N"/>
				<field name="LegLastPx" required="N"/>
			</group>
			<field name="ClearingFeeIndicator" required="N"/>
			<field name="OrderCapacity" required="N"/>
			<field name="OrderRestrictions" required="N"/>
			<field name="CustOrderCapacity" required="N"/>
			<field name="Account" required="N"/>
			<field name="AcctIDSource" required="N"/>
			<field name="AccountType" required="N"/>
			<field name="PositionEffect" required="N"/>
			<field name="PreallocMethod" required="N"/>
			<group name="NoAllocs" required="N">
				<field name="AllocAccount" required="N"/>
				<field name="AllocAcctIDSource" required="N"/>
				<field name="AllocSettlCurrency" required="N"/>
				<field name="IndividualAllocID" required="N"/>
				<component name="NestedParties2" required="N"/>
				<field name="AllocQty" required="N"/>
			</group>
		</message>
	</messages>
	
	<components>
		<component name="Instrument">
			<field name="Symbol" required="Y"/>
			<field name="SymbolSfx" required="N"/>
			<field name="SecurityID" required="N"/>
			<field name="SecurityIDSource" required="N"/>
			<group name="NoSecurityAltID" required="N">
				<field name="SecurityAltID" required="N"/>
				<field name="SecurityAltIDSource" required="N"/>
			</group>
			<field name="Product" required="N"/>
			<field name="CFICode" required="N"/>
			<field name="SecurityType" required="N"/>
			<field name="SecuritySubType" required="N"/>
			<field name="MaturityMonthYear" required="N"/>
			<field name="MaturityDate" required="N"/>
			<field name="CouponPaymentDate" required="N"/>
			<field name="IssueDate" required="N"/>
			<field name="RepoCollateralSecurityType" required="N"/>
			<field name="RepurchaseTerm" required="N"/>
			<field name="RepurchaseRate" required="N"/>
			<field name="Factor" required="N"/>
			<field name="CreditRating" required="N"/>
			<field name="InstrRegistry" required="N"/>
			<field name="CountryOfIssue" required="N"/>
			<field name="StateOrProvinceOfIssue" required="N"/>
			<field name="LocaleOfIssue" required="N"/>
			<field name="RedemptionDate" required="N"/>
			<field name="StrikePrice" required="N"/>
			<field name="StrikeCurrency" required="N"/>
			<field name="OptAttribute" required="N"/>
			<field name="ContractMultiplier" required="N"/>
			<field name="CouponRate" required="N"/>
			<field name="SecurityExchange" required="N"/>
			<field name="Issuer" required="N"/>
			<field name="EncodedIssuerLen" required="N"/>
			<field name="EncodedIssuer" required="N"/>
			<field name="SecurityDesc" required="N"/>
			<field name="EncodedSecurityDescLen" required="N"/>
			<field name="EncodedSecurityDesc" required="N"/>
			<field name="Pool" required="N"/>
			<field name="ContractSettlMonth" required="N"/>
			<field name="CPProgram" required="N"/>
			<field name="CPRegType" required="N"/>
			<group name="NoEvents" required="N">
				<field name="EventType" required="N"/>
				<field name="EventDate" required="N"/>
				<field name="EventPx" required="N"/>
				<field name="EventText" required="N"/>
			</group>
			<field name="DatedDate" required="N"/>
			<field name="InterestAccrualDate" required="N"/>
		</component>
		<component name="UnderlyingInstrument">
			<field name="UnderlyingSymbol" required="Y"/>
			<field name="UnderlyingSymbolSfx" required="N"/>
			<field name="UnderlyingSecurityID" required="N"/>
			<field name="UnderlyingSecurityIDSource" required="N"/>
			<group name="NoUnderlyingSecurityAltID" required="N">
				<field name="UnderlyingSecurityAltID" required="N"/>
				<field name="UnderlyingSecurityAltIDSource" required="N"/>
			</group>
			<field name="UnderlyingProduct" required="N"/>
			<field name="UnderlyingCFICode" required="N"/>
			<field name="UnderlyingSecurityType" required="N"/>
			<field name="UnderlyingSecuritySubType" required="N"/>
			<field name="UnderlyingMaturityMonthYear" required="N"/>
			<field name="UnderlyingMaturityDate" required="N"/>
			<field name="UnderlyingCouponPaymentDate" required="N"/>
			<field name="UnderlyingIssueDate" required="N"/>
			<field name="UnderlyingRepoCollateralSecurityType" required="N"/>
			<field name="UnderlyingRepurchaseTerm" required="N"/>
			<field name="UnderlyingRepurchaseRate" required="N"/>
			<field name="UnderlyingFactor" required="N"/>
			<field name="UnderlyingCreditRating" required="N"/>
			<field name="UnderlyingInstrRegistry" required="N"/>
			<field name="UnderlyingCountryOfIssue" required="N"/>
			<field name="UnderlyingStateOrProvinceOfIssue" required="N"/>
			<field name="UnderlyingLocaleOfIssue" required="N"/>
			<field name="UnderlyingRedemptionDate" required="N"/>
			<field name="UnderlyingStrikePrice" required="N"/>
			<field name="UnderlyingStrikeCurrency" required="N"/>
			<field name="UnderlyingOptAttribute" required="N"/>
			<field name="UnderlyingContractMultiplier" required="N"/>
			<field name="UnderlyingCouponRate" required="N"/>
			<field name="UnderlyingSecurityExchange" required="N"/>
			<field name="UnderlyingIssuer" required="N"/>
			<field name="EncodedUnderlyingIssuerLen" required="N"/>
			<field name="EncodedUnderlyingIssuer" required="N"/>
			<field name="UnderlyingSecurityDesc" required="N"/>
			<field name="EncodedUnderlyingSecurityDescLen" required="N"/>
			<field name="EncodedUnderlyingSecurityDesc" required="N"/>
			<field name="UnderlyingCPProgram" required="N"/>
			<field name="UnderlyingCPRegType" required="N"/>
			<field name="UnderlyingCurrency" required="N"/>
			<field name="UnderlyingQty" required="N"/>
			<field name="UnderlyingPx" required="N"/>
			<field name="UnderlyingDirtyPrice" required="N"/>
			<field name="UnderlyingEndPrice" required="N"/>
			<field name="UnderlyingStartValue" required="N"/>
			<field name="UnderlyingCurrentValue" required="N"/>
			<field name="UnderlyingEndValue" required="N"/>
			<component name="UnderlyingStipulations" required="N"/>
		</component>
		<component name="InstrumentLeg">
			<field name="LegSymbol" required="N"/>
			<field name="LegSymbolSfx" required="N"/>
			<field name="LegSecurityID" required="N"/>
			<field name="LegSecurityIDSource" required="N"/>
			<group name="NoLegSecurityAltID" required="N">
				<field name="LegSecurityAltID" required="N"/>
				<field name="LegSecurityAltIDSource" required="N"/>
			</group>
			<field name="LegProduct" required="N"/>
			<field name="LegCFICode" required="N"/>
			<field name="LegSecurityType" required="N"/>
			<field name="LegSecuritySubType" required="N"/>
			<field name="LegMaturityMonthYear" required="N"/>
			<field name="LegMaturityDate" required="N"/>
			<field name="LegCouponPaymentDate" required="N"/>
			<field name="LegIssueDate" required="N"/>
			<field name="LegRepoCollateralSecurityType" required="N"/>
			<field name="LegRepurchaseTerm" required="N"/>
			<field name="LegRepurchaseRate" required="N"/>
			<field name="LegFactor" required="N"/>
			<field name="LegCreditRating" required="N"/>
			<field name="LegInstrRegistry" required="N"/>
			<field name="LegCountryOfIssue" required="N"/>
			<field name="LegStateOrProvinceOfIssue" required="N"/>
			<field name="LegLocaleOfIssue" required="N"/>
			<field name="LegRedemptionDate" required="N"/>
			<field name="LegStrikePrice" required="N"/>
			<field name="LegStrikeCurrency" required="N"/>
			<field name="LegOptAttribute" required="N"/>
			<field name="LegContractMultiplier" required="N"/>
			<field name="LegCouponRate" required="N"/>
			<field name="LegSecurityExchange" required="N"/>
			<field name="LegIssuer" required="N"/>
			<field name="EncodedLegIssuerLen" required="N"/>
			<field name="EncodedLegIssuer" required="N"/>
			<field name="LegSecurityDesc" required="N"/>
			<field name="EncodedLegSecurityDescLen" required="N"/>
			<field name="EncodedLegSecurityDesc" required="N"/>
			<field name="LegRatioQty" required="N"/>
			<field name="LegSide" required="N"/>
			<field name="LegCurrency" required="N"/>
			<field name="LegPool" required="N"/>
			<field name="LegDatedDate" required="N"/>
			<field name="LegContractSettlMonth" required="N"/>
			<field name="LegInterestAccrualDate" required="N"/>
		</component>
		<component name="OrderQtyData">
			<field name="OrderQty" required="N"/>
			<field name="CashOrderQty" required="N"/>
			<field name="OrderPercent" required="N"/>
			<field name="RoundingDirection" required="N"/>
			<field name="RoundingModulus" required="N"/>
		</component>
		<component name="CommissionData">
			<field name="Commission" required="N"/>
			<field name="CommType" required="N"/>
			<field name="CommCurrency" required="N"/>
			<field name="FundRenewWaiv" required="N"/>
		</component>
		<component name="Parties">
			<group name="NoPartyIDs" required="N">
				<field name="PartyID" required="N"/>
				<field name="PartyIDSource" required="N"/>
				<field name="PartyRole" required="N"/>
				<group name="NoPartySubIDs" required="N">
					<field name="PartySubID" required="N"/>
					<field name="PartySubIDType" required="N"/>
				</group>
			</group>
		</component>
		<component name="NestedParties">
			<group name="NoNestedPartyIDs" required="N">
				<field name="NestedPartyID" required="N"/>
				<field name="NestedPartyIDSource" required="N"/>
				<field name="NestedPartyRole" required="N"/>
				<group name="NoNestedPartySubIDs" required="N">
					<field name="NestedPartySubID" required="N"/>
					<field name="NestedPartySubIDType" required="N"/>
				</group>
			</group>
		</component>
		<component name="NestedParties2">
			<group name="NoNested2PartyIDs" required="N">
				<field name="Nested2PartyID" required="N"/>
				<field name="Nested2PartyIDSource" required="N"/>
				<field name="Nested2PartyRole" required="N"/>
				<group name="NoNested2PartySubIDs" required="N">
					<field name="Nested2PartySubID" required="N"/>
					<field name="Nested2PartySubIDType" required="N"/>
				</group>
			</group>
		</component>
		<component name="SpreadOrBenchmarkCurveData">
			<field name="Spread" required="N"/>
			<field name="BenchmarkCurveCurrency" required="N"/>
			<field name="BenchmarkCurveName" required="N"/>
			<field name="BenchmarkCurvePoint" required="N"/>
			<field name="BenchmarkPrice" required="N"/>
			<field name="BenchmarkPriceType" required="N"/>
			<field name="BenchmarkSecurityID" required="N"/>
			<field name="BenchmarkSecurityIDSource" required="N"/>
		</component>
		<component name="Stipulations">
			<group name="NoStipulations" required="N">
				<field name="StipulationType" required="N"/>
				<field name="StipulationValue" required="N"/>
			</group>
		</component>
		<component name="UnderlyingStipulations">
			<group name="NoUnderlyingStips" required="N">
				<field name="UnderlyingStipType" required="N"/>
				<field name="UnderlyingStipValue" required="N"/>
			</group>
		</component>
		<component name="LegStipulations">
			<group name="NoLegStipulations" required="N">
				<field name="LegStipulationType" required="N"/>
				<field name="LegStipulationValue" required="N"/>
			</group>
		</component>
		<component name="YieldData">
			<field name="YieldType" required="N"/>
			<field name="Yield" required="N"/>
			<field name="YieldCalcDate" required="N"/>
			<field name="YieldRedemptionDate" required="N"/>
			<field name="YieldRedemptionPrice" required="N"/>
			<field name="YieldRedemptionPriceType" required="N"/>
		</component>
		<component name="PositionAmountData">
			<group name="NoPosAmt" required="Y">
				<field name="PosAmtType" required="Y"/>
				<field name="PosAmt" required="Y"/>
			</group>
		</component>
		<component name="TrdRegTimestamps">
			<group name="NoTrdRegTimestamps" required="Y">
				<field name="TrdRegTimestamp" required="N"/>
				<field name="TrdRegTimestampType" required="N"/>
				<field name="TrdRegTimestampOrigin" required="N"/>
			</group>
		</component>
		<component name="FinancingDetails">
			<field name="AgreementDesc" required="N"/>
			<field name="AgreementID" required="N"/>
			<field name="AgreementDate" required="N"/>
			<field name="AgreementCurrency" required="N"/>
			<field name="TerminationType" required="N"/>
			<field name="StartDate" required="N"/>
			<field name="EndDate" required="N"/>
			<field name="DeliveryType" required="N"/>
			<field name="MarginRatio" required="N"/>
		</component>
	</components>
	
	<fields>
		<field number="1" name="Account" type="STRING"/>
		<field number="6" name="AvgPx" type="PRICE"/>
		<field number="7" name="BeginSeqNo" type="SEQNUM"/>
		<field number="8" name="BeginString" type="STRING"/>
		<field number="9" name="BodyLength" type="LENGTH"/>
		<field number="10" name="CheckSum" type="STRING"/>
		<field number="11" name="ClOrdID" type="STRING"/>
		<field number="12" name="Commission" type="AMT"/>
		<field number="13" name="CommType" type="CHAR">
			<value enum="1" description="PER_UNIT"/>
			<value enum="2" description="PERCENTAGE"/>
			<value enum="3" description="ABSOLUTE"/>
			<value enum="4" description="PERCENTAGE_WAIVED_CASH_DISCOUNT"/>
			<value enum="5" description="PERCENTAGE_WAIVED_ENHANCED_UNITS"/>
			<value enum="6" description="POINTS_PER_BOND_OR_OR_CONTRACT"/>
		</field>
		<field number="15" name="Currency" type="CURRENCY"/>
		<field number="16" name="EndSeqNo" type="SEQNUM"/>
		<field number="17" name="ExecID" type="STRING"/>
		<field number="18" name="ExecInst" type="MULTIPLEVALUESTRING">
			<value enum="1" description="NOT_HELD"/>
			<value enum="2" description="WORK"/>
			<value enum="3" description="GO_ALONG"/>
			<value enum="4" description="OVER_THE_DAY"/>
			<value enum="5" description="HELD"/>
			<value enum="6" description="PARTICIPATE_DONT_INITIATE"/>
			<value enum="7" description="STRICT_SCALE"/>
			<value enum="8" description="TRY_TO_SCALE"/>
			<value enum="9" description="STAY_ON_BIDSIDE"/>
			<value enum="0" description="STAY_ON_OFFERSIDE"/>
			<value enum="A" description="NO_CROSS"/>
			<value enum="B" description="OK_TO_CROSS"/>
			<value enum="C" description="CALL_FIRST"/>
			<value enum="D" description="PERCENT_OF_VOLUME"/>
			<value enum="E" description="DO_NOT_INCREASE"/>
			<value enum="F" description="DO_NOT_REDUCE"/>
			<value enum="G" description="ALL_OR_NONE"/>
			<value enum="H" description="REINSTATE_ON_SYSTEM_FAILURE"/>
			<value enum="I" description="INSTITUTIONS_ONLY"/>
			<value enum="J" description="REINSTATE_ON_TRADING_HALT"/>
			<value enum="K" description="CANCEL_ON_TRADING_HALT"/>
			<value enum="L" description="LAST_PEG"/>
			<value enum="M" description="MID_PRICE"/>
			<value enum="N" description="NON_NEGOTIABLE"/>
			<value enum="O" description="OPENING_PEG"/>
			<value enum="P" description="MARKET_PEG"/>
			<value enum="Q" description="CANCEL_ON_SYSTEM_FAILURE"/>
			<value enum="R" description="PRIMARY_PEG"/>
			<value enum="S" description="SUSPEND"/>
			<value enum="T" description="FIXED_PEG_TO_LOCAL_BEST_BID_OR_OFFER_AT_TIME_OF_ORDER"/>
			<value enum="U" description="CUSTOMER_DISPLAY_INSTRUCTION"/>
			<value enum="V" description="NETTING"/>
			<value enum="W" description="PEG_TO_VWAP"/>
			<value enum="X" description="TRADE_ALONG"/>
			<value enum="Y" description="TRY_TO_STOP"/>
			<value enum="Z" description="CANCEL_IF_NOT_BEST"/>
			<value enum="a" description="TRAILING_STOP_PEG"/>
			<value enum="b" description="STRICT_LIMIT"/>
			<value enum="c" description="IGNORE_PRICE_VALIDITY_CHECKS"/>
			<value enum="d" description="PEG_TO_LIMIT_PRICE"/>
			<value enum="e" description="WORK_TO_TARGET_STRATEGY"/>
		</field>
		<field number="22" name="SecurityIDSource" type="STRING">
			<value enum="1" description="CUSIP"/>
			<value enum="2" description="SEDOL"/>
			<value enum="3" description="QUIK"/>
			<value enum="4" description="ISIN_NUMBER"/>
			<value enum="5" description="RIC_CODE"/>
			<value enum="6" description="ISO_CURRENCY_CODE"/>
			<value enum="7" description="ISO_COUNTRY_CODE"/>
			<value enum="8" description="EXCHANGE_SYMBOL"/>
			<value enum="9" description="CONSOLIDATED_TAPE_ASSOCIATION"/>
			<value enum="A" description="BLOOMBERG_SYMBOL"/>
			<value enum="B" description="WERTPAPIER"/>
			<value enum="C" description="DUTCH"/>
			<value enum="D" description="VALOREN"/>
			<value enum="E" description="SICOVAM"/>
			<value enum="F" description="BELGIAN"/>
			<value enum="G" description="COMMON"/>
			<value enum="H" description="CLEARING_HOUSE_CLEARING_ORGANIZATION"/>
			<value enum="I" description="ISDA_FPML_PRODUCT_SPECIFICATION"/>
			<value enum="J" description="OPTIONS_PRICE_REPORTING_AUTHORITY"/>
		</field>
		<field number="30" name="LastMkt" type="EXCHANGE"/>
		<field number="31" name="LastPx" type="PRICE"/>
		<field number="32" name="LastQty" type="QTY"/>
		<field number="34" name="MsgSeqNum" type="SEQNUM"/>
		<field number="35" name="MsgType" type="STRING">
			<value enum="0" description="HEARTBEAT"/>
			<value enum="1" description="TEST_REQUEST"/>
			<value enum="2" description="RESEND_REQUEST"/>
			<value enum="3" description="REJECT"/>
			<value enum="4" description="SEQUENCE_RESET"/>
			<value enum="5" description="LOGOUT"/>
			<value enum="6" description="INDICATION_OF_INTEREST"/>
			<value enum="7" description="ADVERTISEMENT"/>
			<value enum="8" description="EXECUTION_REPORT"/>
			<value enum="9" description="ORDER_CANCEL_REJECT"/>
			<value enum="A" description="LOGON"/>
			<value enum="B" description="NEWS"/>
			<value enum="C" description="EMAIL"/>
			<value enum="D" description="ORDER_SINGLE"/>
			<value enum="E" description="ORDER_LIST"/>
			<value enum="F" description="ORDER_CANCEL_REQUEST"/>
			<value enum="G" description="ORDER_CANCEL_REPLACE_REQUEST"/>
			<value enum="H" description="ORDER_STATUS_REQUEST"/>
			<value enum="J" description="ALLOCATION_INSTRUCTION"/>
			<value enum="K" description="LIST_CANCEL_REQUEST"/>
			<value enum="L" description="LIST_EXECUTE"/>
			<value enum="M" description="LIST_STATUS_REQUEST"/>
			<value enum="N" description="LIST_STATUS"/>
			<value enum="P" description="ALLOCATION_INSTRUCTION_ACK"/>
			<value enum="Q" description="DONT_KNOW_TRADE"/>
			<value enum="R" description="QUOTE_REQUEST"/>
			<value enum="S" description="QUOTE"/>
			<value enum="T" description="SETTLEMENT_INSTRUCTIONS"/>
			<value enum="V" description="MARKET_DATA_REQUEST"/>
			<value enum="W" description="MARKET_DATA_SNAPSHOT_FULL_REFRESH"/>
			<value enum="X" description="MARKET_DATA_INCREMENTAL_REFRESH"/>
			<value enum="Y" description="MARKET_DATA_REQUEST_REJECT"/>
			<value enum="Z" description="QUOTE_CANCEL"/>
			<value enum="a" description="QUOTE_STATUS_REQUEST"/>
			<value enum="b" description="MASS_QUOTE_ACKNOWLEDGEMENT"/>
			<value enum="c" description="SECURITY_DEFINITION_REQUEST"/>
			<value enum="d" description="SECURITY_DEFINITION"/>
			<value enum="e" description="SECURITY_STATUS_REQUEST"/>
			<value enum="f" description="SECURITY_STATUS"/>
			<value enum="g" description="TRADING_SESSION_STATUS_REQUEST"/>
			<value enum="h" description="TRADING_SESSION_STATUS"/>
			<value enum="i" description="MASS_QUOTE"/>
			<value enum="j" description="BUSINESS_MESSAGE_REJECT"/>
			<value enum="k" description="BID_REQUEST"/>
			<value enum="l" description="BID_RESPONSE"/>
			<value enum="m" description="LIST_STRIKE_PRICE"/>
			<value enum="n" description="XML_MESSAGE"/>
			<value enum="o" description="REGISTRATION_INSTRUCTIONS"/>
			<value enum="p" description="REGISTRATION_INSTRUCTIONS_RESPONSE"/>
			<value enum="q" description="ORDER_MASS_CANCEL_REQUEST"/>
			<value enum="r" description="ORDER_MASS_CANCEL_REPORT"/>
			<value enum="s" description="NEW_ORDER_CROSS"/>
			<value enum="t" description="CROSS_ORDER_CANCEL_REPLACE_REQUEST"/>
			<value enum="u" description="CROSS_ORDER_CANCEL_REQUEST"/>
			<value enum="v" description="SECURITY_TYPE_REQUEST"/>
			<value enum="w" description="SECURITY_TYPES"/>
			<value enum="x" description="SECURITY_LIST_REQUEST"/>
			<value enum="y" description="SECURITY_LIST"/>
			<value enum="z" description="DERIVATIVE_SECURITY_LIST_REQUEST"/>
			<value enum="AA" description="DERIVATIVE_SECURITY_LIST"/>
			<value enum="AB" description="NEW_ORDER_MULTILEG"/>
			<value enum="AC" description="MULTILEG_ORDER_CANCEL_REPLACE"/>
			<value enum="AD" description="TRADE_CAPTURE_REPORT_REQUEST"/>
			<value enum="AE" description="TRADE_CAPTURE_REPORT"/>
			<value enum="AF" description="ORDER_MASS_STATUS_REQUEST"/>
			<value enum="AG" description="QUOTE_REQUEST_REJECT"/>
			<value enum="AH" description="RFQ_REQUEST"/>
			<value enum="AI" description="QUOTE_STATUS_REPORT"/>
			<value enum="AJ" description="QUOTE_RESPONSE"/>
			<value enum="AK" description="CONFIRMATION"/>
			<value enum="AL" description="POSITION_MAINTENANCE_REQUEST"/>
			<value enum="AM" description="POSITION_MAINTENANCE_REPORT"/>
			<value enum="AN" description="REQUEST_FOR_POSITIONS"/>
			<value enum="AO" description="REQUEST_FOR_POSITIONS_ACK"/>
			<value enum="AP" description="POSITION_REPORT"/>
			<value enum="AQ" description="TRADE_CAPTURE_REPORT_REQUEST_ACK"/>
			<value enum="AR" description="TRADE_CAPTURE_REPORT_ACK"/>
			<value enum="AS" description="ALLOCATION_REPORT"/>
			<value enum="AT" description="ALLOCATION_REPORT_ACK"/>
			<value enum="AU" description="CONFIRMATION_ACK"/>
			<value enum="AV" description="SETTLEMENT_INSTRUCTION_REQUEST"/>
			<value enum="AW" description="ASSIGNMENT_REPORT"/>
			<value enum="AX" description="COLLATERAL_REQUEST"/>
			<value enum="AY" description="COLLATERAL_ASSIGNMENT"/>
			<value enum="AZ" description="COLLATERAL_RESPONSE"/>
			<value enum="BA" description="COLLATERAL_REPORT"/>
			<value enum="BB" description="COLLATERAL_INQUIRY"/>
			<value enum="BC" description="NETWORK_STATUS_REQUEST"/>
			<value enum="BD" description="NETWORK_STATUS_RESPONSE"/>
			<value enum="BE" description="USER_REQUEST"/>
			<value enum="BF" description="USER_RESPONSE"/>
			<value enum="BG" description="COLLATERAL_INQUIRY_ACK"/>
			<value enum="BH" description="CONFIRMATION_REQUEST"/>
		</field>
		<field number="36" name="NewSeqNo" type="SEQNUM"/>
		<field number="37" name="OrderID" type="STRING"/>
		<field number="38" name="OrderQty" type="QTY"/>
		<field number="39" name="OrdStatus" type="CHAR">
			<value enum="0" description="NEW"/>
			<value enum="1" description="PARTIALLY_FILLED"/>
			<value enum="2" description="FILLED"/>
			<value enum="3" description="DONE_FOR_DAY"/>
			<value enum="4" description="CANCELED"/>
			<value enum="5" description="REPLACED"/>
			<value enum="6" description="PENDING_CANCEL"/>
			<value enum="7" description="STOPPED"/>
			<value enum="8" description="REJECTED"/>
			<value enum="9" description="SUSPENDED"/>
			<value enum="A" description="PENDING_NEW"/>
			<value enum="B" description="CALCULATED"/>
			<value enum="C" description="EXPIRED"/>
			<value enum="D" description="ACCEPTED_FOR_BIDDING"/>
			<value enum="E" description="PENDING_REPLACE"/>
		</field>
		<field number="40" name="OrdType" type="CHAR">
			<value enum="1" description="MARKET"/>
			<value enum="2" description="LIMIT"/>
			<value enum="3" description="STOP"/>
			<value enum="4" description="STOP_LIMIT"/>
			<value enum="5" description="MARKET_ON_CLOSE"/>
			<value enum="6" description="WITH_OR_WITHOUT"/>
			<value enum="7" description="LIMIT_OR_BETTER"/>
			<value enum="8" description="LIMIT_WITH_OR_WITHOUT"/>
			<value enum="9" description="ON_BASIS"/>
			<value enum="A" description="ON_CLOSE"/>
			<value enum="B" description="LIMIT_ON_CLOSE"/>
			<value enum="C" description="FOREX_MARKET"/>
			<value enum="D" description="PREVIOUSLY_QUOTED"/>
			<value enum="E" description="PREVIOUSLY_INDICATED"/>
			<value enum="F" description="FOREX_LIMIT"/>
			<value enum="G" description="FOREX_SWAP"/>
			<value enum="H" description="FOREX_PREVIOUSLY_QUOTED"/>
			<value enum="I" description="FUNARI"/>
			<value enum="J" description="MARKET_IF_TOUCHED"/>
			<value enum="K" description="MARKET_WITH_LEFTOVER_AS_LIMIT"/>
			<value enum="L" description="PREVIOUS_FUND_VALUATION_POINT"/>
			<value enum="M" description="NEXT_FUND_VALUATION_POINT"/>
			<value enum="P" description="PEGGED"/>
		</field>
		<field number="43" name="PossDupFlag" type="BOOLEAN"/>
		<field number="45" name="RefSeqNum" type="SEQNUM"/>
		<field number="48" name="SecurityID" type="STRING"/>
		<field number="49" name="SenderCompID" type="STRING"/>
		<field number="50" name="SenderSubID" type="STRING"/>
		<field number="52" name="SendingTime" type="UTCTIMESTAMP"/>
		<field number="54" name="Side" type="CHAR">
			<value enum="1" description="BUY"/>
			<value enum="2" description="SELL"/>
			<value enum="3" description="BUY_MINUS"/>
			<value enum="4" description="SELL_PLUS"/>
			<value enum="5" description="SELL_SHORT"/>
			<value enum="6" description="SELL_SHORT_EXEMPT"/>
			<value enum="7" description="UNDISCLOSED"/>
			<value enum="8" description="CROSS"/>
			<value enum="9" description="CROSS_SHORT"/>
			<value enum="A" description="CROSS_SHORT_EXEMPT"/>
			<value enum="B" description="AS_DEFINED"/>
			<value enum="C" description="OPPOSITE"/>
			<value enum="D" description="SUBSCRIBE"/>
			<value enum="E" description="REDEEM"/>
			<value enum="F" description="LEND"/>
			<value enum="G" description="BORROW"/>
		</field>
		<field number="55" name="Symbol" type="STRING"/>
		<field number="56" name="TargetCompID" type="STRING"/>
		<field number="57" name="TargetSubID" type="STRING"/>
		<field number="58" name="Text" type="STRING"/>
		<field number="60" name="TransactTime" type="UTCTIMESTAMP"/>
		<field number="63" name="SettlType" type="CHAR">
			<value enum="0" description="REGULAR"/>
			<value enum="1" description="CASH"/>
			<value enum="2" description="NEXT_DAY"/>
			<value enum="3" description="T_PLUS_2"/>
			<value enum="4" description="T_PLUS_3"/>
			<value enum="5" description="T_PLUS_4"/>
			<value enum="6" description="FUTURE"/>
			<value enum="7" description="WHEN_AND_IF_ISSUED"/>
			<value enum="8" description="SELLERS_OPTION"/>
			<value enum="9" description="T_PLUS_5"/>
		</field>
		<field number="64" name="SettlDate" type="LOCALMKTDATE"/>
		<field number="65" name="SymbolSfx" type="STRING">
			<value enum="WI" description="WHEN_ISSUED"/>
			<value enum="CD" description="A_EUCP_WITH_LUMP_SUM_INTEREST"/>
		</field>
		<field number="66" name="ListID" type="STRING"/>
		<field number="70" name="AllocID" type="STRING"/>
		<field number="75" name="TradeDate" type="LOCALMKTDATE"/>
		<field number="77" name="PositionEffect" type="CHAR">
			<value enum="O" description="OPEN"/>
			<value enum="C" description="CLOSE"/>
			<value enum="R" description="ROLLED"/>
			<value enum="F" description="FIFO"/>
		</field>
		<field number="78" name="NoAllocs" type="NUMINGROUP"/>
		<field number="79" name="AllocAccount" type="STRING"/>
		<field number="80" name="AllocQty" type="QTY"/>
		<field number="81" name="ProcessCode" type="CHAR">
			<value enum="0" description="REGULAR"/>
			<value enum="1" description="SOFT_DOLLAR"/>
			<value enum="2" description="STEP_IN"/>
			<value enum="3" description="STEP_OUT"/>
			<value enum="4" description="SOFT_DOLLAR_STEP_IN"/>
			<value enum="5" description="SOFT_DOLLAR_STEP_OUT"/>
			<value enum="6" description="PLAN_SPONSOR"/>
		</field>
		<field number="89" name="Signature" type="DATA"/>
		<field number="90" name="SecureDataLen" type="LENGTH"/>
		<field number="91" name="SecureData" type="DATA"/>
		<field number="93" name="SignatureLength" type="LENGTH"/>
		<field number="95" name="RawDataLength" type="LENGTH"/>
		<field number="96" name="RawData" type="DATA"/>
		<field number="97" name="PossResend" type="BOOLEAN"/>
		<field number="98" name="EncryptMethod" type="INT">
			<value enum="0" description="NONE_OTHER"/>
			<value enum="1" description="PKCS"/>
			<value enum="2" description="DES"/>
			<value enum="3" description="PKCS_DES"/>
			<value enum="4" description="PGP_DES"/>
			<value enum="5" description="PGP_DES_MD5"/>
			<value enum="6" description="PEM_DES_MD5"/>
		</field>
		<field number="106" name="Issuer" type="STRING"/>
		<field number="107" name="SecurityDesc" type="STRING"/>
		<field number="108" name="HeartBtInt" type="INT"/>
		<field number="112" name="TestReqID" type="STRING"/>
		<field number="115" name="OnBehalfOfCompID" type="STRING"/>
		<field number="116" name="OnBehalfOfSubID" type="STRING"/>
		<field number="118" name="NetMoney" type="AMT"/>
		<field number="119" name="SettlCurrAmt" type="AMT"/>
		<field number="120" name="SettlCurrency" type="CURRENCY"/>
		<field number="122" name="OrigSendingTime" type="UTCTIMESTAMP"/>
		<field number="123" name="GapFillFlag" type="BOOLEAN"/>
		<field number="128" name="DeliverToCompID" type="STRING"/>
		<field number="129" name="DeliverToSubID" type="STRING"/>
		<field number="136" name="NoMiscFees" type="NUMINGROUP"/>
		<field number="137" name="MiscFeeAmt" type="AMT"/>
		<field number="138" name="MiscFeeCurr" type="CURRENCY"/>
		<field number="139" name="MiscFeeType" type="STRING"><!--
			<value enum="1" description="REGULATORY"/>
			<value enum="2" description="TAX"/>
			<value enum="3" description="LOCAL_COMMISSION"/>
			<value enum="4" description="EXCHANGE_FEES"/>
			<value enum="5" description="STAMP"/>
			<value enum="6" description="LEVY"/>
			<value enum="7" description="OTHER"/>
			<value enum="8" description="MARKUP"/>
			<value enum="9" description="CONSUMPTION_TAX"/>-->
		</field>
		<field number="141" name="ResetSeqNumFlag" type="BOOLEAN"/>
		<field number="142" name="SenderLocationID" type="STRING"/>
		<field number="143" name="TargetLocationID" type="STRING"/>
		<field number="144" name="OnBehalfOfLocationID" type="STRING"/>
		<field number="145" name="DeliverToLocationID" type="STRING"/>
		<field number="150" name="ExecType" type="CHAR">
			<value enum="0" description="NEW"/>
			<value enum="1" description="PARTIAL_FILL"/>
			<value enum="2" description="FILL"/>
			<value enum="3" description="DONE_FOR_DAY"/>
			<value enum="4" description="CANCELED"/>
			<value enum="5" description="REPLACE"/>
			<value enum="6" description="PENDING_CANCEL"/>
			<value enum="7" description="STOPPED"/>
			<value enum="8" description="REJECTED"/>
			<value enum="9" description="SUSPENDED"/>
			<value enum="A" description="PENDING_NEW"/>
			<value enum="B" description="CALCULATED"/>
			<value enum="C" description="EXPIRED"/>
			<value enum="D" description="RESTATED"/>
			<value enum="E" description="PENDING_REPLACE"/>
			<value enum="F" description="TRADE"/>
			<value enum="G" description="TRADE_CORRECT"/>
			<value enum="H" description="TRADE_CANCEL"/>
			<value enum="I" description="ORDER_STATUS"/>
		</field>
		<field number="152" name="CashOrderQty" type="QTY"/>
		<field number="155" name="SettlCurrFxRate" type="FLOAT"/>
		<field number="156" name="SettlCurrFxRateCalc" type="CHAR">
			<value enum="M" description="MULTIPLY"/>
			<value enum="D" description="DIVIDE"/>
		</field>
		<field number="157" name="NumDaysInterest" type="INT"/>
		<field number="158" name="AccruedInterestRate" type="PERCENTAGE"/>
		<field number="159" name="AccruedInterestAmt" type="AMT"/>
		<field number="167" name="SecurityType" type="STRING"/>
<!--field number="167" name="SecurityType" type="STRING">
			<value enum="EUSUPRA" description="EURO_SUPRANATIONAL_COUPONS"/>
			<value enum="FAC" description="FEDERAL_AGENCY_COUPON"/>
			<value enum="FADN" description="FEDERAL_AGENCY_DISCOUNT_NOTE"/>
			<value enum="PEF" description="PRIVATE_EXPORT_FUNDING"/>
			<value enum="SUPRA" description="USD_SUPRANATIONAL_COUPONS"/>
			<value enum="FUT" description="FUTURE"/>
			<value enum="OPT" description="OPTION"/>
			<value enum="CORP" description="CORPORATE_BOND"/>
			<value enum="CPP" description="CORPORATE_PRIVATE_PLACEMENT"/>
			<value enum="CB" description="CONVERTIBLE_BOND"/>
			<value enum="DUAL" description="DUAL_CURRENCY"/>
			<value enum="EUCORP" description="EURO_CORPORATE_BOND"/>
			<value enum="XLINKD" description="INDEXED_LINKED"/>
			<value enum="STRUCT" description="STRUCTURED_NOTES"/>
			<value enum="YANK" description="YANKEE_CORPORATE_BOND"/>
			<value enum="FOR" description="FOREIGN_EXCHANGE_CONTRACT"/>
			<value enum="CS" description="COMMON_STOCK"/>
			<value enum="PS" description="PREFERRED_STOCK"/>
			<value enum="BRADY" description="BRADY_BOND"/>
			<value enum="EUSOV" description="EURO_SOVEREIGNS"/>
			<value enum="TBOND" description="US_TREASURY_BOND"/>
			<value enum="TINT" description="INTEREST_STRIP_FROM_ANY_BOND_OR_NOTE"/>
			<value enum="TIPS" description="TREASURY_INFLATION_PROTECTED_SECURITIES"/>
			<value enum="TCAL" description="PRINCIPAL_STRIP_OF_A_CALLABLE_BOND_OR_NOTE"/>
			<value enum="TPRN" description="PRINCIPAL_STRIP_FROM_A_NON_CALLABLE_BOND_OR_NOTE"/>
			<value enum="TNOTE" description="US_TREASURY_NOTE"/>
			<value enum="TBILL" description="US_TREASURY_BILL"/>
			<value enum="REPO" description="REPURCHASE"/>
			<value enum="FORWARD" description="FORWARD"/>
			<value enum="BUYSELL" description="BUY_SELLBACK"/>
			<value enum="SECLOAN" description="SECURITIES_LOAN"/>
			<value enum="SECPLEDGE" description="SECURITIES_PLEDGE"/>
			<value enum="TERM" description="TERM_LOAN"/>
			<value enum="RVLV" description="REVOLVER_LOAN"/>
			<value enum="RVLVTRM" description="REVOLVER_TERM_LOAN"/>
			<value enum="BRIDGE" description="BRIDGE_LOAN"/>
			<value enum="LOFC" description="LETTER_OF_CREDIT"/>
			<value enum="SWING" description="SWING_LINE_FACILITY"/>
			<value enum="DINP" description="DEBTOR_IN_POSSESSION"/>
			<value enum="DEFLTED" description="DEFAULTED"/>
			<value enum="WITHDRN" description="WITHDRAWN"/>
			<value enum="REPLACD" description="REPLACED"/>
			<value enum="MATURED" description="MATURED"/>
			<value enum="AMENDED" description="AMENDED_AND_RESTATED"/>
			<value enum="RETIRED" description="RETIRED"/>
			<value enum="BA" description="BANKERS_ACCEPTANCE"/>
			<value enum="BN" description="BANK_NOTES"/>
			<value enum="BOX" description="BILL_OF_EXCHANGES"/>
			<value enum="CD" description="CERTIFICATE_OF_DEPOSIT"/>
			<value enum="CL" description="CALL_LOANS"/>
			<value enum="CP" description="COMMERCIAL_PAPER"/>
			<value enum="DN" description="DEPOSIT_NOTES"/>
			<value enum="EUCD" description="EURO_CERTIFICATE_OF_DEPOSIT"/>
			<value enum="EUCP" description="EURO_COMMERCIAL_PAPER"/>
			<value enum="LQN" description="LIQUIDITY_NOTE"/>
			<value enum="MTN" description="MEDIUM_TERM_NOTES"/>
			<value enum="ONITE" description="OVERNIGHT"/>
			<value enum="PN" description="PROMISSORY_NOTE"/>
			<value enum="PZFJ" description="PLAZOS_FIJOS"/>
			<value enum="STN" description="SHORT_TERM_LOAN_NOTE"/>
			<value enum="TD" description="TIME_DEPOSIT"/>
			<value enum="XCN" description="EXTENDED_COMM_NOTE"/>
			<value enum="YCD" description="YANKEE_CERTIFICATE_OF_DEPOSIT"/>
			<value enum="ABS" description="ASSET_BACKED_SECURITIES"/>
			<value enum="CMBS" description="CORP_MORTGAGE_BACKED_SECURITIES"/>
			<value enum="CMO" description="COLLATERALIZED_MORTGAGE_OBLIGATION"/>
			<value enum="IET" description="IOETTE_MORTGAGE"/>
			<value enum="MBS" description="MORTGAGE_BACKED_SECURITIES"/>
			<value enum="MIO" description="MORTGAGE_INTEREST_ONLY"/>
			<value enum="MPO" description="MORTGAGE_PRINCIPAL_ONLY"/>
			<value enum="MPP" description="MORTGAGE_PRIVATE_PLACEMENT"/>
			<value enum="MPT" description="MISCELLANEOUS_PASS_THROUGH"/>
			<value enum="PFAND" description="PFANDBRIEFE"/>
			<value enum="TBA" description="TO_BE_ANNOUNCED"/>
			<value enum="AN" description="OTHER_ANTICIPATION_NOTES"/>
			<value enum="COFO" description="CERTIFICATE_OF_OBLIGATION"/>
			<value enum="COFP" description="CERTIFICATE_OF_PARTICIPATION"/>
			<value enum="GO" description="GENERAL_OBLIGATION_BONDS"/>
			<value enum="MT" description="MANDATORY_TENDER"/>
			<value enum="RAN" description="REVENUE_ANTICIPATION_NOTE"/>
			<value enum="REV" description="REVENUE_BONDS"/>
			<value enum="SPCLA" description="SPECIAL_ASSESSMENT"/>
			<value enum="SPCLO" description="SPECIAL_OBLIGATION"/>
			<value enum="SPCLT" description="SPECIAL_TAX"/>
			<value enum="TAN" description="TAX_ANTICIPATION_NOTE"/>
			<value enum="TAXA" description="TAX_ALLOCATION"/>
			<value enum="TECP" description="TAX_EXEMPT_COMMERCIAL_PAPER"/>
			<value enum="TRAN" description="TAX_AND_REVENUE_ANTICIPATION_NOTE"/>
			<value enum="VRDN" description="VARIABLE_RATE_DEMAND_NOTE"/>
			<value enum="WAR" description="WARRANT"/>
			<value enum="MF" description="MUTUAL_FUND"/>
			<value enum="MLEG" description="MULTI_LEG_INSTRUMENT"/>
			<value enum="NONE" description="NO_SECURITY_TYPE"/>
			<value enum="?" description="WILDCARD"/>
		</field-->
		<field number="194" name="LastSpotRate" type="PRICE"/>
		<field number="195" name="LastForwardPoints" type="PRICEOFFSET"/>
		<field number="198" name="SecondaryOrderID" type="STRING"/>
		<field number="200" name="MaturityMonthYear" type="MONTHYEAR"/>
		<field number="202" name="StrikePrice" type="PRICE"/>
		<field number="206" name="OptAttribute" type="CHAR"/>
		<field number="207" name="SecurityExchange" type="EXCHANGE"/>
		<field number="212" name="XmlDataLen" type="LENGTH"/>
		<field number="213" name="XmlData" type="DATA"/>
		<field number="218" name="Spread" type="PRICEOFFSET"/>
		<field number="220" name="BenchmarkCurveCurrency" type="CURRENCY"/>
		<field number="221" name="BenchmarkCurveName" type="STRING">
			<value enum="MuniAAA" description="MUNIAAA"/>
			<value enum="FutureSWAP" description="FUTURESWAP"/>
			<value enum="LIBID" description="LIBID"/>
			<value enum="LIBOR" description="LIBOR"/>
			<value enum="OTHER" description="OTHER"/>
			<value enum="SWAP" description="SWAP"/>
			<value enum="Treasury" description="TREASURY"/>
			<value enum="Euribor" description="EURIBOR"/>
			<value enum="Pfandbriefe" description="PFANDBRIEFE"/>
			<value enum="EONIA" description="EONIA"/>
			<value enum="SONIA" description="SONIA"/>
			<value enum="EUREPO" description="EUREPO"/>
		</field>
		<field number="222" name="BenchmarkCurvePoint" type="STRING"/>
		<field number="223" name="CouponRate" type="PERCENTAGE"/>
		<field number="224" name="CouponPaymentDate" type="LOCALMKTDATE"/>
		<field number="225" name="IssueDate" type="LOCALMKTDATE"/>
		<field number="226" name="RepurchaseTerm" type="INT"/>
		<field number="227" name="RepurchaseRate" type="PERCENTAGE"/>
		<field number="228" name="Factor" type="FLOAT"/>
		<field number="230" name="ExDate" type="LOCALMKTDATE"/>
		<field number="231" name="ContractMultiplier" type="FLOAT"/>
		<field number="232" name="NoStipulations" type="NUMINGROUP"/>
		<field number="233" name="StipulationType" type="STRING">
			<value enum="AMT" description="AMT"/>
			<value enum="AUTOREINV" description="AUTO_REINVESTMENT_AT_OR_BETTER"/>
			<value enum="BANKQUAL" description="BANK_QUALIFIED"/>
			<value enum="BGNCON" description="BARGAIN_CONDITIONS"/>
			<value enum="COUPON" description="COUPON_RANGE"/>
			<value enum="CURRENCY" description="ISO_CURRENCY_CODE"/>
			<value enum="CUSTOMDATE" description="CUSTOM_START_END_DATE"/>
			<value enum="GEOG" description="GEOGRAPHICS_AND_PERCENT_RANGE"/>
			<value enum="HAIRCUT" description="VALUATION_DISCOUNT"/>
			<value enum="INSURED" description="INSURED"/>
			<value enum="ISSUE" description="YEAR_OR_YEAR_MONTH_OF_ISSUE"/>
			<value enum="ISSUER" description="ISSUERS_TICKER"/>
			<value enum="ISSUESIZE" description="ISSUE_SIZE_RANGE"/>
			<value enum="LOOKBACK" description="LOOKBACK_DAYS"/>
			<value enum="LOT" description="EXPLICIT_LOT_IDENTIFIER"/>
			<value enum="LOTVAR" description="LOT_VARIANCE"/>
			<value enum="MAT" description="MATURITY_YEAR_AND_MONTH"/>
			<value enum="MATURITY" description="MATURITY_RANGE"/>
			<value enum="MAXSUBS" description="MAXIMUM_SUBSTITUTIONS"/>
			<value enum="MINQTY" description="MINIMUM_QUANTITY"/>
			<value enum="MININCR" description="MINIMUM_INCREMENT"/>
			<value enum="MINDNOM" description="MINIMUM_DENOMINATION"/>
			<value enum="PAYFREQ" description="PAYMENT_FREQUENCY_CALENDAR"/>
			<value enum="PIECES" description="NUMBER_OF_PIECES"/>
			<value enum="PMAX" description="POOLS_MAXIMUM"/>
			<value enum="PPM" description="POOLS_PER_MILLION"/>
			<value enum="PPL" description="POOLS_PER_LOT"/>
			<value enum="PPT" description="POOLS_PER_TRADE"/>
			<value enum="PRICE" description="PRICE_RANGE"/>
			<value enum="PRICEFREQ" description="PRICING_FREQUENCY"/>
			<value enum="PROD" description="PRODUCTION_YEAR"/>
			<value enum="PROTECT" description="CALL_PROTECTION"/>
			<value enum="PURPOSE" description="PURPOSE"/>
			<value enum="PXSOURCE" description="BENCHMARK_PRICE_SOURCE"/>
			<value enum="RATING" description="RATING_SOURCE_AND_RANGE"/>
			<value enum="RESTRICTED" description="RESTRICTED"/>
			<value enum="SECTOR" description="MARKET_SECTOR"/>
			<value enum="SECTYPE" description="SECURITYTYPE_INCLUDED_OR_EXCLUDED"/>
			<value enum="STRUCT" description="STRUCTURE"/>
			<value enum="SUBSFREQ" description="SUBSTITUTIONS_FREQUENCY"/>
			<value enum="SUBSLEFT" description="SUBSTITUTIONS_LEFT"/>
			<value enum="TEXT" description="FREEFORM_TEXT"/>
			<value enum="TRDVAR" description="TRADE_VARIANCE"/>
			<value enum="WAC" description="WEIGHTED_AVERAGE_COUPON"/>
			<value enum="WAL" description="WEIGHTED_AVERAGE_LIFE_COUPON"/>
			<value enum="WALA" description="WEIGHTED_AVERAGE_LOAN_AGE"/>
			<value enum="WAM" description="WEIGHTED_AVERAGE_MATURITY"/>
			<value enum="WHOLE" description="WHOLE_POOL"/>
			<value enum="YIELD" description="YIELD_RANGE"/>
			<value enum="SMM" description="SINGLE_MONTHLY_MORTALITY"/>
			<value enum="CPR" description="CONSTANT_PREPAYMENT_RATE"/>
			<value enum="CPY" description="CONSTANT_PREPAYMENT_YIELD"/>
			<value enum="CPP" description="CONSTANT_PREPAYMENT_PENALTY"/>
			<value enum="ABS" description="ABSOLUTE_PREPAYMENT_SPEED"/>
			<value enum="MPR" description="MONTHLY_PREPAYMENT_RATE"/>
			<value enum="PSA" description="PERCENT_OF_BMA_PREPAYMENT_CURVE"/>
			<value enum="PPC" description="PERCENT_OF_PROSPECTUS_PREPAYMENT_CURVE"/>
			<value enum="MHP" description="PERCENT_OF_MANUFACTURED_HOUSING_PREPAYMENT_CURVE"/>
			<value enum="HEP" description="FINAL_CPR_OF_HOME_EQUITY_PREPAYMENT_CURVE"/>
		</field>
		<field number="234" name="StipulationValue" type="STRING">
			<value enum="CD" description="SPECIAL_CUM_DIVIDEND"/>
			<value enum="XD" description="SPECIAL_EX_DIVIDEND"/>
			<value enum="CC" description="SPECIAL_CUM_COUPON"/>
			<value enum="XC" description="SPECIAL_EX_COUPON"/>
			<value enum="CB" description="SPECIAL_CUM_BONUS"/>
			<value enum="XB" description="SPECIAL_EX_BONUS"/>
			<value enum="CR" description="SPECIAL_CUM_RIGHTS"/>
			<value enum="XR" description="SPECIAL_EX_RIGHTS"/>
			<value enum="CP" description="SPECIAL_CUM_CAPITAL_REPAYMENTS"/>
			<value enum="XP" description="SPECIAL_EX_CAPITAL_REPAYMENTS"/>
			<value enum="CS" description="CASH_SETTLEMENT"/>
			<value enum="SP" description="SPECIAL_PRICE"/>
			<value enum="TR" description="REPORT_FOR_EUROPEAN_EQUITY_MARKET_SECURITIES"/>
			<value enum="GD" description="GUARANTEED_DELIVERY"/>
		</field>
		<field number="235" name="YieldType" type="STRING">
			<value enum="AFTERTAX" description="AFTER_TAX_YIELD"/>
			<value enum="ANNUAL" description="ANNUAL_YIELD"/>
			<value enum="ATISSUE" description="YIELD_AT_ISSUE"/>
			<value enum="AVGMATURITY" description="YIELD_TO_AVERAGE_MATURITY"/>
			<value enum="BOOK" description="BOOK_YIELD"/>
			<value enum="CALL" description="YIELD_TO_NEXT_CALL"/>
			<value enum="CHANGE" description="YIELD_CHANGE_SINCE_CLOSE"/>
			<value enum="CLOSE" description="CLOSING_YIELD"/>
			<value enum="COMPOUND" description="COMPOUND_YIELD"/>
			<value enum="CURRENT" description="CURRENT_YIELD"/>
			<value enum="GROSS" description="TRUE_GROSS_YIELD"/>
			<value enum="GOVTEQUIV" description="GOVERNMENT_EQUIVALENT_YIELD"/>
			<value enum="INFLATION" description="YIELD_WITH_INFLATION_ASSUMPTION"/>
			<value enum="INVERSEFLOATER" description="INVERSE_FLOATER_BOND_YIELD"/>
			<value enum="LASTCLOSE" description="MOST_RECENT_CLOSING_YIELD"/>
			<value enum="LASTMONTH" description="CLOSING_YIELD_MOST_RECENT_MONTH"/>
			<value enum="LASTQUARTER" description="CLOSING_YIELD_MOST_RECENT_QUARTER"/>
			<value enum="LASTYEAR" description="CLOSING_YIELD_MOST_RECENT_YEAR"/>
			<value enum="LONGAVGLIFE" description="YIELD_TO_LONGEST_AVERAGE_LIFE"/>
			<value enum="MARK" description="MARK_TO_MARKET_YIELD"/>
			<value enum="MATURITY" description="YIELD_TO_MATURITY"/>
			<value enum="NEXTREFUND" description="YIELD_TO_NEXT_REFUND"/>
			<value enum="OPENAVG" description="OPEN_AVERAGE_YIELD"/>
			<value enum="PUT" description="YIELD_TO_NEXT_PUT"/>
			<value enum="PREVCLOSE" description="PREVIOUS_CLOSE_YIELD"/>
			<value enum="PROCEEDS" description="PROCEEDS_YIELD"/>
			<value enum="SEMIANNUAL" description="SEMI_ANNUAL_YIELD"/>
			<value enum="SHORTAVGLIFE" description="YIELD_TO_SHORTEST_AVERAGE_LIFE"/>
			<value enum="SIMPLE" description="SIMPLE_YIELD"/>
			<value enum="TAXEQUIV" description="TAX_EQUIVALENT_YIELD"/>
			<value enum="TENDER" description="YIELD_TO_TENDER_DATE"/>
			<value enum="TRUE" description="TRUE_YIELD"/>
			<value enum="VALUE1_32" description="YIELD_VALUE_OF_1_32"/>
			<value enum="WORST" description="YIELD_TO_WORST"/>
		</field>
		<field number="236" name="Yield" type="PERCENTAGE"/>
		<field number="237" name="TotalTakedown" type="AMT"/>
		<field number="238" name="Concession" type="AMT"/>
		<field number="239" name="RepoCollateralSecurityType" type="INT"/>
		<field number="240" name="RedemptionDate" type="LOCALMKTDATE"/>
		<field number="241" name="UnderlyingCouponPaymentDate" type="LOCALMKTDATE"/>
		<field number="242" name="UnderlyingIssueDate" type="LOCALMKTDATE"/>
		<field number="243" name="UnderlyingRepoCollateralSecurityType" type="INT"/>
		<field number="244" name="UnderlyingRepurchaseTerm" type="INT"/>
		<field number="245" name="UnderlyingRepurchaseRate" type="PERCENTAGE"/>
		<field number="246" name="UnderlyingFactor" type="FLOAT"/>
		<field number="247" name="UnderlyingRedemptionDate" type="LOCALMKTDATE"/>
		<field number="248" name="LegCouponPaymentDate" type="LOCALMKTDATE"/>
		<field number="249" name="LegIssueDate" type="LOCALMKTDATE"/>
		<field number="250" name="LegRepoCollateralSecurityType" type="INT"/>
		<field number="251" name="LegRepurchaseTerm" type="INT"/>
		<field number="252" name="LegRepurchaseRate" type="PERCENTAGE"/>
		<field number="253" name="LegFactor" type="FLOAT"/>
		<field number="254" name="LegRedemptionDate" type="LOCALMKTDATE"/>
		<field number="255" name="CreditRating" type="STRING"/>
		<field number="256" name="UnderlyingCreditRating" type="STRING"/>
		<field number="257" name="LegCreditRating" type="STRING"/>
		<field number="263" name="SubscriptionRequestType" type="CHAR">
			<value enum="0" description="SNAPSHOT"/>
			<value enum="1" description="SNAPSHOT_PLUS_UPDATES"/>
			<value enum="2" description="DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST"/>
		</field>
		<field number="305" name="UnderlyingSecurityIDSource" type="STRING"/>
		<field number="306" name="UnderlyingIssuer" type="STRING"/>
		<field number="307" name="UnderlyingSecurityDesc" type="STRING"/>
		<field number="308" name="UnderlyingSecurityExchange" type="EXCHANGE"/>
		<field number="309" name="UnderlyingSecurityID" type="STRING"/>
		<field number="310" name="UnderlyingSecurityType" type="STRING"/>
		<field number="311" name="UnderlyingSymbol" type="STRING"/>
		<field number="312" name="UnderlyingSymbolSfx" type="STRING"/>
		<field number="313" name="UnderlyingMaturityMonthYear" type="MONTHYEAR"/>
		<field number="316" name="UnderlyingStrikePrice" type="PRICE"/>
		<field number="317" name="UnderlyingOptAttribute" type="CHAR"/>
		<field number="318" name="UnderlyingCurrency" type="CURRENCY"/>
		<field number="325" name="UnsolicitedIndicator" type="BOOLEAN"/>
		<field number="336" name="TradingSessionID" type="STRING"/>
		<field number="347" name="MessageEncoding" type="STRING">
			<value enum="ISO-2022-JP" description="ISO_2022_JP"/>
			<value enum="EUC-JP" description="EUC_JP"/>
			<value enum="SHIFT_JIS" description="SHIFT_JIS"/>
			<value enum="UTF-8" description="UTF_8"/>
		</field>
		<field number="348" name="EncodedIssuerLen" type="LENGTH"/>
		<field number="349" name="EncodedIssuer" type="DATA"/>
		<field number="350" name="EncodedSecurityDescLen" type="LENGTH"/>
		<field number="351" name="EncodedSecurityDesc" type="DATA"/>
		<field number="354" name="EncodedTextLen" type="LENGTH"/>
		<field number="355" name="EncodedText" type="DATA"/>
		<field number="362" name="EncodedUnderlyingIssuerLen" type="LENGTH"/>
		<field number="363" name="EncodedUnderlyingIssuer" type="DATA"/>
		<field number="364" name="EncodedUnderlyingSecurityDescLen" type="LENGTH"/>
		<field number="365" name="EncodedUnderlyingSecurityDesc" type="DATA"/>
		<field number="369" name="LastMsgSeqNumProcessed" type="SEQNUM"/>
		<field number="371" name="RefTagID" type="INT"/>
		<field number="372" name="RefMsgType" type="STRING"/>
		<field number="373" name="SessionRejectReason" type="INT">
			<value enum="0" description="INVALID_TAG_NUMBER"/>
			<value enum="1" description="REQUIRED_TAG_MISSING"/>
			<value enum="2" description="TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE"/>
			<value enum="3" description="UNDEFINED_TAG"/>
			<value enum="4" description="TAG_SPECIFIED_WITHOUT_A_VALUE"/>
			<value enum="5" description="VALUE_IS_INCORRECT"/>
			<value enum="6" description="INCORRECT_DATA_FORMAT_FOR_VALUE"/>
			<value enum="7" description="DECRYPTION_PROBLEM"/>
			<value enum="8" description="SIGNATURE_PROBLEM"/>
			<value enum="9" description="COMPID_PROBLEM"/>
			<value enum="10" description="SENDINGTIME_ACCURACY_PROBLEM"/>
			<value enum="11" description="INVALID_MSGTYPE"/>
			<value enum="12" description="XML_VALIDATION_ERROR"/>
			<value enum="13" description="TAG_APPEARS_MORE_THAN_ONCE"/>
			<value enum="14" description="TAG_SPECIFIED_OUT_OF_REQUIRED_ORDER"/>
			<value enum="15" description="REPEATING_GROUP_FIELDS_OUT_OF_ORDER"/>
			<value enum="16" description="INCORRECT_NUMINGROUP_COUNT_FOR_REPEATING_GROUP"/>
			<value enum="17" description="NON_DATA_VALUE_INCLUDES_FIELD_DELIMITER"/>
			<value enum="99" description="OTHER"/>
		</field>
		<field number="376" name="ComplianceID" type="STRING"/>
		<field number="377" name="SolicitedFlag" type="BOOLEAN"/>
		<field number="378" name="ExecRestatementReason" type="INT">
			<value enum="0" description="GT_CORPORATE_ACTION"/>
			<value enum="1" description="GT_RENEWAL_RESTATEMENT"/>
			<value enum="2" description="VERBAL_CHANGE"/>
			<value enum="3" description="REPRICING_OF_ORDER"/>
			<value enum="4" description="BROKER_OPTION"/>
			<value enum="5" description="PARTIAL_DECLINE_OF_ORDERQTY"/>
			<value enum="6" description="CANCEL_ON_TRADING_HALT"/>
			<value enum="7" description="CANCEL_ON_SYSTEM_FAILURE"/>
			<value enum="8" description="MARKET_OPTION"/>
			<value enum="9" description="CANCELED_NOT_BEST"/>
		</field>
		<field number="379" name="BusinessRejectRefID" type="STRING"/>
		<field number="380" name="BusinessRejectReason" type="INT">
			<value enum="0" description="OTHER"/>
			<value enum="1" description="UNKOWN_ID"/>
			<value enum="2" description="UNKNOWN_SECURITY"/>
			<value enum="3" description="UNSUPPORTED_MESSAGE_TYPE"/>
			<value enum="4" description="APPLICATION_NOT_AVAILABLE"/>
			<value enum="5" description="CONDITIONALLY_REQUIRED_FIELD_MISSING"/>
			<value enum="6" description="NOT_AUTHORIZED"/>
			<value enum="7" description="DELIVERTO_FIRM_NOT_AVAILABLE_AT_THIS_TIME"/>
		</field>
		<field number="381" name="GrossTradeAmt" type="AMT"/>
		<field number="383" name="MaxMessageSize" type="LENGTH"/>
		<field number="384" name="NoMsgTypes" type="NUMINGROUP"/>
		<field number="385" name="MsgDirection" type="CHAR">
			<value enum="S" description="SEND"/>
			<value enum="R" description="RECEIVE"/>
		</field>
		<field number="423" name="PriceType" type="INT">
			<value enum="1" description="PERCENTAGE"/>
			<value enum="2" description="PER_UNIT"/>
			<value enum="3" description="FIXED_AMOUNT"/>
			<value enum="4" description="DISCOUNT"/>
			<value enum="5" description="PREMIUM"/>
			<value enum="6" description="SPREAD"/>
			<value enum="7" description="TED_PRICE"/>
			<value enum="8" description="TED_YIELD"/>
			<value enum="9" description="YIELD"/>
		</field>
		<field number="435" name="UnderlyingCouponRate" type="PERCENTAGE"/>
		<field number="436" name="UnderlyingContractMultiplier" type="FLOAT"/>
		<field number="442" name="MultiLegReportingType" type="CHAR">
			<value enum="1" description="SINGLE_SECURITY"/>
			<value enum="2" description="INDIVIDUAL_LEG_OF_A_MULTI_LEG_SECURITY"/>
			<value enum="3" description="MULTI_LEG_SECURITY"/>
		</field>
		<field number="447" name="PartyIDSource" type="CHAR">
			<value enum="B" description="BIC"/>
			<value enum="C" description="GENERALLY_ACCEPTED_MARKET_PARTICIPANT_IDENTIFIER"/>
			<value enum="D" description="PROPRIETARY_CUSTOM_CODE"/>
			<value enum="E" description="ISO_COUNTRY_CODE"/>
			<value enum="F" description="SETTLEMENT_ENTITY_LOCATION"/>
			<value enum="G" description="MIC"/>
			<value enum="H" description="CSD_PARTICIPANT_MEMBER_CODE"/>
			<value enum="1" description="KOREAN_INVESTOR_ID"/>
			<value enum="2" description="TAIWANESE_QUALIFIED_FOREIGN_INVESTOR_ID_QFII_FID"/>
			<value enum="3" description="TAIWANESE_TRADING_ACCOUNT"/>
			<value enum="4" description="MALAYSIAN_CENTRAL_DEPOSITORY_NUMBER"/>
			<value enum="5" description="CHINESE_B_SHARE"/>
			<value enum="6" description="UK_NATIONAL_INSURANCE_OR_PENSION_NUMBER"/>
			<value enum="7" description="US_SOCIAL_SECURITY_NUMBER"/>
			<value enum="8" description="US_EMPLOYER_IDENTIFICATION_NUMBER"/>
			<value enum="9" description="AUSTRALIAN_BUSINESS_NUMBER"/>
			<value enum="A" description="AUSTRALIAN_TAX_FILE_NUMBER"/>
			<value enum="I" description="DIRECTED_BROKER"/>
		</field>
		<field number="448" name="PartyID" type="STRING"/>
		<field number="452" name="PartyRole" type="INT">
			<value enum="1" description="EXECUTING_FIRM"/>
			<value enum="2" description="BROKER_OF_CREDIT"/>
			<value enum="3" description="CLIENT_ID"/>
			<value enum="4" description="CLEARING_FIRM"/>
			<value enum="5" description="INVESTOR_ID"/>
			<value enum="6" description="INTRODUCING_FIRM"/>
			<value enum="7" description="ENTERING_FIRM"/>
			<value enum="8" description="LOCATE_LENDING_FIRM"/>
			<value enum="9" description="FUND_MANAGER_CLIENT_ID"/>
			<value enum="10" description="SETTLEMENT_LOCATION"/>
			<value enum="11" description="ORDER_ORIGINATION_TRADER"/>
			<value enum="12" description="EXECUTING_TRADER"/>
			<value enum="13" description="ORDER_ORIGINATION_FIRM"/>
			<value enum="14" description="GIVEUP_CLEARING_FIRM"/>
			<value enum="15" description="CORRESPONDANT_CLEARING_FIRM"/>
			<value enum="16" description="EXECUTING_SYSTEM"/>
			<value enum="17" description="CONTRA_FIRM"/>
			<value enum="18" description="CONTRA_CLEARING_FIRM"/>
			<value enum="19" description="SPONSORING_FIRM"/>
			<value enum="20" description="UNDERLYING_CONTRA_FIRM"/>
			<value enum="21" description="CLEARING_ORGANIZATION"/>
			<value enum="22" description="EXCHANGE"/>
			<value enum="24" description="CUSTOMER_ACCOUNT"/>
			<value enum="25" description="CORRESPONDENT_CLEARING_ORGANIZATION"/>
			<value enum="26" description="CORRESPONDENT_BROKER"/>
			<value enum="27" description="BUYER_SELLER"/>
			<value enum="28" description="CUSTODIAN"/>
			<value enum="29" description="INTERMEDIARY"/>
			<value enum="30" description="AGENT"/>
			<value enum="31" description="SUB_CUSTODIAN"/>
			<value enum="32" description="BENEFICIARY"/>
			<value enum="33" description="INTERESTED_PARTY"/>
			<value enum="34" description="REGULATORY_BODY"/>
			<value enum="35" description="LIQUIDITY_PROVIDER"/>
			<value enum="36" description="ENTERING_TRADER"/>
			<value enum="37" description="CONTRA_TRADER"/>
			<value enum="38" description="POSITION_ACCOUNT"/>
		</field>
		<field number="453" name="NoPartyIDs" type="NUMINGROUP"/>
		<field number="454" name="NoSecurityAltID" type="NUMINGROUP"/>
		<field number="455" name="SecurityAltID" type="STRING"/>
		<field number="456" name="SecurityAltIDSource" type="STRING"/>
		<field number="457" name="NoUnderlyingSecurityAltID" type="NUMINGROUP"/>
		<field number="458" name="UnderlyingSecurityAltID" type="STRING"/>
		<field number="459" name="UnderlyingSecurityAltIDSource" type="STRING"/>
		<field number="460" name="Product" type="INT">
			<value enum="1" description="AGENCY"/>
			<value enum="2" description="COMMODITY"/>
			<value enum="3" description="CORPORATE"/>
			<value enum="4" description="CURRENCY"/>
			<value enum="5" description="EQUITY"/>
			<value enum="6" description="GOVERNMENT"/>
			<value enum="7" description="INDEX"/>
			<value enum="8" description="LOAN"/>
			<value enum="9" description="MONEYMARKET"/>
			<value enum="10" description="MORTGAGE"/>
			<value enum="11" description="MUNICIPAL"/>
			<value enum="12" description="OTHER"/>
			<value enum="13" description="FINANCING"/>
		</field>
		<field number="461" name="CFICode" type="STRING"/>
		<field number="462" name="UnderlyingProduct" type="INT"/>
		<field number="463" name="UnderlyingCFICode" type="STRING"/>
		<field number="464" name="TestMessageIndicator" type="BOOLEAN"/>
		<field number="467" name="IndividualAllocID" type="STRING"/>
		<field number="468" name="RoundingDirection" type="CHAR">
			<value enum="0" description="ROUND_TO_NEAREST"/>
			<value enum="1" description="ROUND_DOWN"/>
			<value enum="2" description="ROUND_UP"/>
		</field>
		<field number="469" name="RoundingModulus" type="FLOAT"/>
		<field number="470" name="CountryOfIssue" type="COUNTRY"/>
		<field number="471" name="StateOrProvinceOfIssue" type="STRING"/>
		<field number="472" name="LocaleOfIssue" type="STRING"/>
		<field number="479" name="CommCurrency" type="CURRENCY"/>
		<field number="483" name="TransBkdTime" type="UTCTIMESTAMP"/>
		<field number="487" name="TradeReportTransType" type="INT">
			<value enum="0" description="NEW"/>
			<value enum="1" description="CANCEL"/>
			<value enum="2" description="REPLACE"/>
			<value enum="3" description="RELEASE"/>
			<value enum="4" description="REVERSE"/>
		</field>
		<field number="497" name="FundRenewWaiv" type="CHAR">
			<value enum="Y" description="YES"/>
			<value enum="N" description="NO"/>
		</field>
		<field number="516" name="OrderPercent" type="PERCENTAGE"/>
		<field number="518" name="NoContAmts" type="NUMINGROUP"/>
		<field number="519" name="ContAmtType" type="INT">
			<value enum="1" description="COMMISSION_AMOUNT"/>
			<value enum="2" description="COMMISSION_PERCENT"/>
			<value enum="3" description="INITIAL_CHARGE_AMOUNT"/>
			<value enum="4" description="INITIAL_CHARGE_PERCENT"/>
			<value enum="5" description="DISCOUNT_AMOUNT"/>
			<value enum="6" description="DISCOUNT_PERCENT"/>
			<value enum="7" description="DILUTION_LEVY_AMOUNT"/>
			<value enum="8" description="DILUTION_LEVY_PERCENT"/>
			<value enum="9" description="EXIT_CHARGE_AMOUNT"/>
		</field>
		<field number="520" name="ContAmtValue" type="FLOAT"/>
		<field number="521" name="ContAmtCurr" type="CURRENCY"/>
		<field number="523" name="PartySubID" type="STRING"/>
		<field number="524" name="NestedPartyID" type="STRING"/>
		<field number="525" name="NestedPartyIDSource" type="CHAR"/>
		<field number="526" name="SecondaryClOrdID" type="STRING"/>
		<field number="527" name="SecondaryExecID" type="STRING"/>
		<field number="528" name="OrderCapacity" type="CHAR">
			<value enum="A" description="AGENCY"/>
			<value enum="G" description="PROPRIETARY"/>
			<value enum="I" description="INDIVIDUAL"/>
			<value enum="P" description="PRINCIPAL"/>
			<value enum="R" description="RISKLESS_PRINCIPAL"/>
			<value enum="W" description="AGENT_FOR_OTHER_MEMBER"/>
		</field>
		<field number="529" name="OrderRestrictions" type="MULTIPLEVALUESTRING">
			<value enum="1" description="PROGRAM_TRADE"/>
			<value enum="2" description="INDEX_ARBITRAGE"/>
			<value enum="3" description="NON_INDEX_ARBITRAGE"/>
			<value enum="4" description="COMPETING_MARKET_MAKER"/>
			<value enum="5" description="ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_SECURITY"/>
			<value enum="6" description="ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_UNDERLYING_SECURITY_OF_A_DERIVATIVE_SECURITY"/>
			<value enum="7" description="FOREIGN_ENTITY"/>
			<value enum="8" description="EXTERNAL_MARKET_PARTICIPANT"/>
			<value enum="9" description="EXTERNAL_INTER_CONNECTED_MARKET_LINKAGE"/>
			<value enum="A" description="RISKLESS_ARBITRAGE"/>
		</field>
		<field number="538" name="NestedPartyRole" type="INT"/>
		<field number="539" name="NoNestedPartyIDs" type="NUMINGROUP"/>
		<field number="541" name="MaturityDate" type="LOCALMKTDATE"/>
		<field number="542" name="UnderlyingMaturityDate" type="LOCALMKTDATE"/>
		<field number="543" name="InstrRegistry" type="STRING"/>
		<field number="545" name="NestedPartySubID" type="STRING"/>
		<field number="552" name="NoSides" type="NUMINGROUP">
			<value enum="1" description="ONE_SIDE"/>
			<value enum="2" description="BOTH_SIDES"/>
		</field>
		<field number="553" name="Username" type="STRING"/>
		<field number="554" name="Password" type="STRING"/>
		<field number="555" name="NoLegs" type="NUMINGROUP"/>
		<field number="556" name="LegCurrency" type="CURRENCY"/>
		<field number="564" name="LegPositionEffect" type="CHAR"/>
		<field number="565" name="LegCoveredOrUncovered" type="INT"/>
		<field number="566" name="LegPrice" type="PRICE"/>
		<field number="568" name="TradeRequestID" type="STRING"/>
		<field number="570" name="PreviouslyReported" type="BOOLEAN"/>
		<field number="571" name="TradeReportID" type="STRING"/>
		<field number="572" name="TradeReportRefID" type="STRING"/>
		<field number="573" name="MatchStatus" type="CHAR">
			<value enum="0" description="COMPARED_MATCHED_OR_AFFIRMED"/>
			<value enum="1" description="UNCOMPARED_UNMATCHED_OR_UNAFFIRMED"/>
			<value enum="2" description="ADVISORY_OR_ALERT"/>
		</field>
		<field number="574" name="MatchType" type="STRING"/>
		<field number="575" name="OddLot" type="BOOLEAN"/>
		<field number="576" name="NoClearingInstructions" type="INT"/>
		<field number="577" name="ClearingInstruction" type="INT">
			<value enum="0" description="PROCESS_NORMALLY"/>
			<value enum="1" description="EXCLUDE_FROM_ALL_NETTING"/>
			<value enum="2" description="BILATERAL_NETTING_ONLY"/>
			<value enum="3" description="EX_CLEARING"/>
			<value enum="4" description="SPECIAL_TRADE"/>
			<value enum="5" description="MULTILATERAL_NETTING"/>
			<value enum="6" description="CLEAR_AGAINST_CENTRAL_COUNTERPARTY"/>
			<value enum="7" description="EXCLUDE_FROM_CENTRAL_COUNTERPARTY"/>
			<value enum="8" description="MANUAL_MODE"/>
			<value enum="9" description="AUTOMATIC_POSTING_MODE"/>
		</field>
		<field number="578" name="TradeInputSource" type="STRING"/>
		<field number="579" name="TradeInputDevice" type="STRING"/>
		<field number="581" name="AccountType" type="INT">
			<value enum="1" description="ACCOUNT_IS_CARRIED_ON_CUSTOMER_SIDE_OF_BOOKS"/>
			<value enum="2" description="ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS"/>
			<value enum="3" description="HOUSE_TRADER"/>
			<value enum="4" description="FLOOR_TRADER"/>
			<value enum="6" description="ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS_AND_IS_CROSS_MARGINED"/>
			<value enum="7" description="ACCOUNT_IS_HOUSE_TRADER_AND_IS_CROSS_MARGINED"/>
			<value enum="8" description="JOINT_BACKOFFICE_ACCOUNT"/>
		</field>
		<field number="582" name="CustOrderCapacity" type="INT">
			<value enum="1" description="MEMBER_TRADING_FOR_THEIR_OWN_ACCOUNT"/>
			<value enum="2" description="CLEARING_FIRM_TRADING_FOR_ITS_PROPRIETARY_ACCOUNT"/>
			<value enum="3" description="MEMBER_TRADING_FOR_ANOTHER_MEMBER"/>
			<value enum="4" description="ALL_OTHER"/>
		</field>
		<field number="587" name="LegSettlType" type="CHAR"/>
		<field number="588" name="LegSettlDate" type="LOCALMKTDATE"/>
		<field number="591" name="PreallocMethod" type="CHAR">
			<value enum="0" description="PRO_RATA"/>
			<value enum="1" description="DO_NOT_PRO_RATA"/>
		</field>
		<field number="592" name="UnderlyingCountryOfIssue" type="COUNTRY"/>
		<field number="593" name="UnderlyingStateOrProvinceOfIssue" type="STRING"/>
		<field number="594" name="UnderlyingLocaleOfIssue" type="STRING"/>
		<field number="595" name="UnderlyingInstrRegistry" type="STRING"/>
		<field number="596" name="LegCountryOfIssue" type="COUNTRY"/>
		<field number="597" name="LegStateOrProvinceOfIssue" type="STRING"/>
		<field number="598" name="LegLocaleOfIssue" type="STRING"/>
		<field number="599" name="LegInstrRegistry" type="STRING"/>
		<field number="600" name="LegSymbol" type="STRING"/>
		<field number="601" name="LegSymbolSfx" type="STRING"/>
		<field number="602" name="LegSecurityID" type="STRING"/>
		<field number="603" name="LegSecurityIDSource" type="STRING"/>
		<field number="604" name="NoLegSecurityAltID" type="STRING"/>
		<field number="605" name="LegSecurityAltID" type="STRING"/>
		<field number="606" name="LegSecurityAltIDSource" type="STRING"/>
		<field number="607" name="LegProduct" type="INT"/>
		<field number="608" name="LegCFICode" type="STRING"/>
		<field number="609" name="LegSecurityType" type="STRING"/>
		<field number="610" name="LegMaturityMonthYear" type="MONTHYEAR"/>
		<field number="611" name="LegMaturityDate" type="LOCALMKTDATE"/>
		<field number="612" name="LegStrikePrice" type="PRICE"/>
		<field number="613" name="LegOptAttribute" type="CHAR"/>
		<field number="614" name="LegContractMultiplier" type="FLOAT"/>
		<field number="615" name="LegCouponRate" type="PERCENTAGE"/>
		<field number="616" name="LegSecurityExchange" type="EXCHANGE"/>
		<field number="617" name="LegIssuer" type="STRING"/>
		<field number="618" name="EncodedLegIssuerLen" type="LENGTH"/>
		<field number="619" name="EncodedLegIssuer" type="DATA"/>
		<field number="620" name="LegSecurityDesc" type="STRING"/>
		<field number="621" name="EncodedLegSecurityDescLen" type="LENGTH"/>
		<field number="622" name="EncodedLegSecurityDesc" type="DATA"/>
		<field number="623" name="LegRatioQty" type="FLOAT"/>
		<field number="624" name="LegSide" type="CHAR"/>
		<field number="625" name="TradingSessionSubID" type="STRING"/>
		<field number="627" name="NoHops" type="NUMINGROUP"/>
		<field number="628" name="HopCompID" type="STRING"/>
		<field number="629" name="HopSendingTime" type="UTCTIMESTAMP"/>
		<field number="630" name="HopRefID" type="SEQNUM"/>
		<field number="635" name="ClearingFeeIndicator" type="STRING">
			<value enum="B" description="CBOE_MEMBER"/>
			<value enum="C" description="NON_MEMBER_AND_CUSTOMER"/>
			<value enum="E" description="EQUITY_MEMBER_AND_CLEARING_MEMBER"/>
			<value enum="F" description="FULL_AND_ASSOCIATE_MEMBER_TRADING_FOR_OWN_ACCOUNT_AND_AS_FLOOR_BROKERS"/>
			<value enum="H" description="FIRMS_106H_AND_106J"/>
			<value enum="I" description="GIM_IDEM_AND_COM_MEMBERSHIP_INTEREST_HOLDERS"/>
			<value enum="L" description="LESSEE_AND_106F_EMPLOYEES"/>
			<value enum="M" description="ALL_OTHER_OWNERSHIP_TYPES"/>
		</field>
		<field number="637" name="LegLastPx" type="PRICE"/>
		<field number="654" name="LegRefID" type="STRING"/>
		<field number="660" name="AcctIDSource" type="INT">
			<value enum="1" description="BIC"/>
			<value enum="2" description="SID_CODE"/>
			<value enum="3" description="TFM"/>
			<value enum="4" description="OMGEO"/>
			<value enum="5" description="DTCC_CODE"/>
		</field>
		<field number="661" name="AllocAcctIDSource" type="INT"/>
		<field number="662" name="BenchmarkPrice" type="PRICE"/>
		<field number="663" name="BenchmarkPriceType" type="INT"/>
		<field number="667" name="ContractSettlMonth" type="MONTHYEAR"/>
		<field number="669" name="LastParPx" type="PRICE"/>
		<field number="683" name="NoLegStipulations" type="NUMINGROUP"/>
		<field number="687" name="LegQty" type="QTY"/>
		<field number="688" name="LegStipulationType" type="STRING"/>
		<field number="689" name="LegStipulationValue" type="STRING"/>
		<field number="690" name="LegSwapType" type="INT">
			<value enum="1" description="PAR_FOR_PAR"/>
			<value enum="2" description="MODIFIED_DURATION"/>
			<value enum="4" description="RISK"/>
			<value enum="5" description="PROCEEDS"/>
		</field>
		<field number="691" name="Pool" type="STRING"/>
		<field number="696" name="YieldRedemptionDate" type="LOCALMKTDATE"/>
		<field number="697" name="YieldRedemptionPrice" type="PRICE"/>
		<field number="698" name="YieldRedemptionPriceType" type="INT"/>
		<field number="699" name="BenchmarkSecurityID" type="STRING"/>
		<field number="701" name="YieldCalcDate" type="LOCALMKTDATE"/>
		<field number="707" name="PosAmtType" type="STRING">
			<value enum="FMTM" description="FINAL_MARK_TO_MARKET_AMOUNT"/>
			<value enum="IMTM" description="INCREMENTAL_MARK_TO_MARKET_AMOUNT"/>
			<value enum="TVAR" description="TRADE_VARIATION_AMOUNT"/>
			<value enum="SMTM" description="START_OF_DAY_MARK_TO_MARKET_AMOUNT"/>
			<value enum="PREM" description="PREMIUM_AMOUNT"/>
			<value enum="CRES" description="CASH_RESIDUAL_AMOUNT"/>
			<value enum="CASH" description="CASH_AMOUNT"/>
			<value enum="VADJ" description="VALUE_ADJUSTED_AMOUNT"/>
		</field>
		<field number="708" name="PosAmt" type="AMT"/>
		<field number="711" name="NoUnderlyings" type="NUMINGROUP"/>
		<field number="715" name="ClearingBusinessDate" type="LOCALMKTDATE"/>
		<field number="725" name="ResponseTransportType" type="INT">
			<value enum="0" description="INBAND"/>
			<value enum="1" description="OUT_OF_BAND"/>
		</field>
		<field number="726" name="ResponseDestination" type="STRING"/>
		<field number="736" name="AllocSettlCurrency" type="CURRENCY"/>
		<field number="738" name="InterestAtMaturity" type="AMT"/>
		<field number="739" name="LegDatedDate" type="LOCALMKTDATE"/>
		<field number="740" name="LegPool" type="STRING"/>
		<field number="748" name="TotNumTradeReports" type="INT"/>
		<field number="751" name="TradeReportRejectReason" type="INT">
			<value enum="0" description="SUCCESSFUL"/>
			<value enum="1" description="INVALID_PARTY_INFORMATION"/>
			<value enum="2" description="UNKNOWN_INSTRUMENT"/>
			<value enum="3" description="UNAUTHORIZED_TO_REPORT_TRADES"/>
			<value enum="4" description="INVALID_TRADE_TYPE"/>
		</field>
		<field number="752" name="SideMultiLegReportingType" type="INT">
			<value enum="1" description="SINGLE_SECURITY"/>
			<value enum="2" description="INDIVIDUAL_LEG_OF_A_MULTI_LEG_SECURITY"/>
			<value enum="3" description="MULTI_LEG_SECURITY"/>
		</field>
		<field number="753" name="NoPosAmt" type="NUMINGROUP"/>
		<field number="756" name="NoNested2PartyIDs" type="NUMINGROUP"/>
		<field number="757" name="Nested2PartyID" type="STRING"/>
		<field number="758" name="Nested2PartyIDSource" type="CHAR"/>
		<field number="759" name="Nested2PartyRole" type="INT"/>
		<field number="760" name="Nested2PartySubID" type="STRING"/>
		<field number="761" name="BenchmarkSecurityIDSource" type="STRING"/>
		<field number="762" name="SecuritySubType" type="STRING"/>
		<field number="763" name="UnderlyingSecuritySubType" type="STRING"/>
		<field number="764" name="LegSecuritySubType" type="STRING"/>
		<field number="768" name="NoTrdRegTimestamps" type="NUMINGROUP"/>
		<field number="769" name="TrdRegTimestamp" type="UTCTIMESTAMP"/>
		<field number="770" name="TrdRegTimestampType" type="INT">
			<value enum="1" description="EXECUTION_TIME"/>
			<value enum="2" description="TIME_IN"/>
			<value enum="3" description="TIME_OUT"/>
			<value enum="4" description="BROKER_RECEIPT"/>
			<value enum="5" description="BROKER_EXECUTION"/>
		</field>
		<field number="771" name="TrdRegTimestampOrigin" type="STRING"/>
		<field number="788" name="TerminationType" type="INT">
			<value enum="1" description="OVERNIGHT"/>
			<value enum="2" description="TERM"/>
			<value enum="3" description="FLEXIBLE"/>
			<value enum="4" description="OPEN"/>
		</field>
		<field number="789" name="NextExpectedMsgSeqNum" type="SEQNUM"/>
		<field number="797" name="CopyMsgIndicator" type="BOOLEAN"/>
		<field number="802" name="NoPartySubIDs" type="NUMINGROUP"/>
		<field number="803" name="PartySubIDType" type="INT"/>
		<field number="804" name="NoNestedPartySubIDs" type="NUMINGROUP"/>
		<field number="805" name="NestedPartySubIDType" type="INT"/>
		<field number="806" name="NoNested2PartySubIDs" type="NUMINGROUP"/>
		<field number="807" name="Nested2PartySubIDType" type="INT"/>
		<field number="810" name="UnderlyingPx" type="PRICE"/>
		<field number="818" name="SecondaryTradeReportID" type="STRING"/>
		<field number="819" name="AvgPxIndicator" type="INT">
			<value enum="0" description="NO_AVERAGE_PRICING"/>
			<value enum="1" description="TRADE_IS_PART_OF_AN_AVERAGE_PRICE_GROUP_IDENTIFIED_BY_THE_TRADELINKID"/>
			<value enum="2" description="LAST_TRADE_IN_THE_AVERAGE_PRICE_GROUP_IDENTIFIED_BY_THE_TRADELINKID"/>
		</field>
		<field number="820" name="TradeLinkID" type="STRING"/>
		<field number="821" name="OrderInputDevice" type="STRING"/>
		<field number="822" name="UnderlyingTradingSessionID" type="STRING"/>
		<field number="823" name="UnderlyingTradingSessionSubID" type="STRING"/>
		<field number="824" name="TradeLegRefID" type="STRING"/>
		<field number="825" name="ExchangeRule" type="STRING"/>
		<field number="826" name="TradeAllocIndicator" type="INT">
			<value enum="0" description="ALLOCATION_NOT_REQUIRED"/>
			<value enum="1" description="ALLOCATION_REQUIRED"/>
			<value enum="2" description="USE_ALLOCATION_PROVIDED_WITH_THE_TRADE"/>
		</field>
		<field number="828" name="TrdType" type="INT">
			<value enum="0" description="REGULAR_TRADE"/>
			<value enum="1" description="BLOCK_TRADE"/>
			<value enum="2" description="EFP"/>
			<value enum="3" description="TRANSFER"/>
			<value enum="4" description="LATE_TRADE"/>
			<value enum="5" description="T_TRADE"/>
			<value enum="6" description="WEIGHTED_AVERAGE_PRICE_TRADE"/>
			<value enum="7" description="BUNCHED_TRADE"/>
			<value enum="8" description="LATE_BUNCHED_TRADE"/>
			<value enum="9" description="PRIOR_REFERENCE_PRICE_TRADE"/>
		</field>
		<field number="829" name="TrdSubType" type="INT"/>
		<field number="830" name="TransferReason" type="STRING"/>
		<field number="852" name="PublishTrdIndicator" type="BOOLEAN"/>
		<field number="853" name="ShortSaleReason" type="INT">
			<value enum="0" description="DEALER_SOLD_SHORT"/>
			<value enum="1" description="DEALER_SOLD_SHORT_EXEMPT"/>
			<value enum="2" description="SELLING_CUSTOMER_SOLD_SHORT"/>
			<value enum="3" description="SELLING_CUSTOMER_SOLD_SHORT_EXEMPT"/>
			<value enum="4" description="QUALIFED_SERVICE_REPRESENTATIVE_OR_AUTOMATIC_GIVEUP_CONTRA_SIDE_SOLD_SHORT"/>
			<value enum="5" description="QSR_OR_AGU_CONTRA_SIDE_SOLD_SHORT_EXEMPT"/>
		</field>
		<field number="854" name="QtyType" type="INT">
			<value enum="0" description="UNITS"/>
			<value enum="1" description="CONTRACTS"/>
		</field>
		<field number="855" name="SecondaryTrdType" type="INT"/>
		<field number="856" name="TradeReportType" type="INT">
			<value enum="0" description="SUBMIT"/>
			<value enum="1" description="ALLEGED"/>
			<value enum="2" description="ACCEPT"/>
			<value enum="3" description="DECLINE"/>
			<value enum="4" description="ADDENDUM"/>
			<value enum="5" description="NO_WAS"/>
			<value enum="6" description="TRADE_REPORT_CANCEL"/>
			<value enum="7" description="LOCKED_IN_TRADE_BREAK"/>
		</field>
		<field number="864" name="NoEvents" type="NUMINGROUP"/>
		<field number="865" name="EventType" type="INT">
			<value enum="1" description="PUT"/>
			<value enum="2" description="CALL"/>
			<value enum="3" description="TENDER"/>
			<value enum="4" description="SINKING_FUND_CALL"/>
		</field>
		<field number="866" name="EventDate" type="LOCALMKTDATE"/>
		<field number="867" name="EventPx" type="PRICE"/>
		<field number="868" name="EventText" type="STRING"/>
		<field number="873" name="DatedDate" type="LOCALMKTDATE"/>
		<field number="874" name="InterestAccrualDate" type="LOCALMKTDATE"/>
		<field number="875" name="CPProgram" type="INT"/>
		<field number="876" name="CPRegType" type="STRING"/>
		<field number="877" name="UnderlyingCPProgram" type="STRING"/>
		<field number="878" name="UnderlyingCPRegType" type="STRING"/>
		<field number="879" name="UnderlyingQty" type="QTY"/>
		<field number="880" name="TrdMatchID" type="STRING"/>
		<field number="881" name="SecondaryTradeReportRefID" type="STRING"/>
		<field number="882" name="UnderlyingDirtyPrice" type="PRICE"/>
		<field number="883" name="UnderlyingEndPrice" type="PRICE"/>
		<field number="884" name="UnderlyingStartValue" type="AMT"/>
		<field number="885" name="UnderlyingCurrentValue" type="AMT"/>
		<field number="886" name="UnderlyingEndValue" type="AMT"/>
		<field number="887" name="NoUnderlyingStips" type="NUMINGROUP"/>
		<field number="888" name="UnderlyingStipType" type="STRING"/>
		<field number="889" name="UnderlyingStipValue" type="STRING"/>
		<field number="891" name="MiscFeeBasis" type="INT">
			<value enum="0" description="ABSOLUTE"/>
			<value enum="1" description="PER_UNIT"/>
			<value enum="2" description="PERCENTAGE"/>
		</field>
		<field number="898" name="MarginRatio" type="PERCENTAGE"/>
		<field number="912" name="LastRptRequested" type="BOOLEAN"/>
		<field number="913" name="AgreementDesc" type="STRING"/>
		<field number="914" name="AgreementID" type="STRING"/>
		<field number="915" name="AgreementDate" type="LOCALMKTDATE"/>
		<field number="916" name="StartDate" type="LOCALMKTDATE"/>
		<field number="917" name="EndDate" type="LOCALMKTDATE"/>
		<field number="918" name="AgreementCurrency" type="CURRENCY"/>
		<field number="919" name="DeliveryType" type="INT">
			<value enum="0" description="VERSUS_PAYMENT"/>
			<value enum="1" description="FREE"/>
			<value enum="2" description="TRI_PARTY"/>
			<value enum="3" description="HOLD_IN_CUSTODY"/>
		</field>
		<field number="920" name="EndAccruedInterestAmt" type="AMT"/>
		<field number="921" name="StartCash" type="AMT"/>
		<field number="922" name="EndCash" type="AMT"/>
		<field number="939" name="TrdRptStatus" type="INT">
			<value enum="0" description="ACCEPTED"/>
			<value enum="1" description="REJECTED"/>
		</field>
		<field number="941" name="UnderlyingStrikeCurrency" type="CURRENCY"/>
		<field number="942" name="LegStrikeCurrency" type="CURRENCY"/>
		<field number="943" name="TimeBracket" type="STRING"/>
		<field number="947" name="StrikeCurrency" type="CURRENCY"/>
		<field number="955" name="LegContractSettlMonth" type="MONTHYEAR"/>
		<field number="956" name="LegInterestAccrualDate" type="LOCALMKTDATE"/>
		<!-- Custom Triana Fields -->
		<field number="5049" name="Deal Link Reference" type="STRING"/>
		<field number="5227" name="FutSettDate2" type="LOCALMKTDATE"/>
		<field number="5233" name="OrderQuantity1" type="QTY"/>
		<field number="5234" name="OrderQuantity2" type="QTY"/>
		<field number="5235" name="SpotRate" type="PRICE"/>
		<field number="5454" name="MarketZone" type="STRING"/>
		<field number="5495" name="State" type="STRING"/>
		<field number="5720" name="EndPaymentDate" type="LOCALMKTDATE"/>
		<field number="5970" name="LegCalculatedCcyLastQty" type="QTY"/>
		<field number="5971" name="CalculatedCcyLastQty" type="QTY"/>
		<field number="6160" name="LastPx2" type="PRICE"/>
		<field number="6267" name="NearFwdPoints" type="PRICE"/>
		<field number="6268" name="FarForwardPoints" type="PRICE"/>
		<field number="7420" name="TradingStyle" type="STRING"/>
		<field number="9019" name="FXOptionStyle" type="STRING"/>
		<field number="9072" name="CallOrPut" type="STRING"/>
		<field number="9124" name="ExpiryTime" type="STRING"/>
		<field number="9127" name="OptionDeliveryType" type="STRING"/>
		<field number="9130" name="BarrierStyle" type="STRING"/>
		<field number="9131" name="BarrierLevel" type="STRING"/>
		<field number="9132" name="BarrierDirection" type="STRING"/>
		<field number="9135" name="BarrierRebate" type="STRING"/>
		<field number="9136" name="OptionProductType" type="STRING"/>
		<field number="9137" name="BarrierLevel2" type="STRING"/>
		<field number="9138" name="BarrierRebate2" type="STRING"/>
		<field number="9139" name="BarrierRebateCurrency" type="STRING"/>
		<field number="9148" name="BarrierRebateCurrency2" type="STRING"/>
		<field number="9187" name="TouchType" type="CHAR"/>
		<field number="9189" name="ExerciseEndDate" type="LOCALMKTDATE"/>
		<field number="9476" name="TransactionCostAmt" type="QTY"/>
		<field number="9477" name="TransactionCostCurrency" type="CURRENCY"/>
		<field number="9518" name="CompetingQuote" type="STRING"/>
		<field number="9520" name="CompetingQuoteLeg2" type="STRING"/>
		<field number="9579" name="NoNettedTrades" type="NUMINGROUP"/>
		<field number="9580" name="ParentID" type="STRING"/>
		<field number="9581" name="OrderId" type="STRING"/>
		<field number="9582" name="ChildID" type="STRING"/>
		<field number="10200" name="UniqueSwapIdentifier" type="STRING"/>
		<field number="10201" name="UniqueSwapIdentifierFar" type="STRING"/>
		<field number="10202" name="SwapID" type="STRING"/>
		<field number="10203" name="ExecutionTime" type="STRING"/>
		<field number="10204" name="ExecutingPlatform" type="STRING"/>
		<field number="10205" name="ExecutionVenueType" type="STRING"/>
		<field number="10206" name="NoTradeRepositories" type="INT"/>
		<field number="10207" name="TradeRepositoryName" type="STRING"/>
</fields>
</fix>
', GETUTCDATE())
GO