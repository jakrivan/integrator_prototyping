

UPDATE [dbo].[TableChangeLog] 
SET TableName = 'VenueMMSettings'
WHERE TableName = 'HotspotMMSettings'

exec sp_rename '[HotspotMMSettings]', 'VenueMMSettings'
GO

ALTER TABLE [dbo].[VenueMMSettings] ADD CounterpartyId [tinyint] NULL
GO


ALTER TRIGGER [dbo].[LastUpdatedTrigger_HotspotMMSettings] ON [dbo].[VenueMMSettings]
AFTER INSERT, UPDATE
AS

BEGIN

--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[VenueMMSettings] Set [LastUpdatedUtc] = GETUTCDATE() from [dbo].[VenueMMSettings] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[SymbolId] = myAlias.[SymbolId] AND triggerInsertedTable.[CounterpartyId] = myAlias.[CounterpartyId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueMMSettings'

END
GO

exec sp_rename '[LastUpdatedTrigger_HotspotMMSettings]', 'LastUpdatedTrigger_VenueMMSettings'
GO

exec sp_rename '[LastUpdatedTrigger_LmaxCrossSettings]', 'LastUpdatedTrigger_VenueCrossSettings'
GO


UPDATE [dbo].[VenueMMSettings] SET CounterpartyId = (SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTF')
GO

ALTER TABLE [VenueMMSettings] ALTER COLUMN [CounterpartyId] [tinyint] NOT NULL
GO

DROP INDEX [IX_HotspotMMSettingsSymbolId] ON [dbo].[VenueMMSettings] WITH ( ONLINE = OFF )
GO

CREATE UNIQUE CLUSTERED INDEX [IX_VenueMMSettingsSymbolPerCounterparty] ON [dbo].[VenueMMSettings]
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
) ON [PRIMARY]
GO


ALTER TABLE [dbo].[VenueMMSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueMMSettings_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO

ALTER TABLE [dbo].[VenueMMSettings] CHECK CONSTRAINT [FK_VenueMMSettings_Counterparty]
GO


INSERT INTO 
	[dbo].[VenueMMSettings]
           ([SymbolId]
		   ,[CounterpartyId]
           ,[SizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[Enabled]
           ,[LastResetRequestedUtc])
     SELECT 
		[Id]
	  ,(SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FA1')
      ,50000
	  ,0.1
	  ,0.25
	  ,280
	  ,0
	  ,NULL
  FROM [dbo].[Symbol]
  WHERE [ShortName] IN
	(
		SELECT 
			pair.FxpCode
		FROM [FXtickDB].[dbo].[CounterpartySymbolSettings] enabledSyms
		INNER JOIN [FXtickDB].[dbo].[FxPair] pair on enabledSyms.SymbolId = pair.FxPairId
		WHERE 
			[TradingTargetTypeId] = (SELECT [Id] FROM [FXtickDB].[dbo].[TradingTargetType] WHERE Code = 'FXall')
			AND Supported = 1
	)
GO

exec sp_rename '[GetUpdateHotspotMMSystemSettings_SP]', 'GetUpdateVenueMMSystemSettings_SP'
GO

ALTER PROCEDURE [dbo].[GetUpdateVenueMMSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueMMSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice]
			,[SizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[Enabled]
			,[LastResetRequestedUtc]
			,[LastUpdatedUtc]
		FROM 
			[dbo].[VenueMMSettings] sett
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			AND (@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name
	END
END
GO


exec sp_rename '[HotMMSettingsDisableAll_SP]', 'VenueMMSettingsDisableAll_SP'
GO
exec sp_rename '[HotMMSettingsDisableEnableOne_SP]', 'VenueMMSettingsDisableEnableOne_SP'
GO
exec sp_rename '[HotMMSettingsReset_SP]', 'VenueMMSettingsReset_SP'
GO
exec sp_rename '[HotMMSettingsUpdateSingle_SP]', 'VenueMMSettingsUpdateSingle_SP'
GO


ALTER PROCEDURE [dbo].[VenueMMSettingsDisableAll_SP]
(
	@CounterpartyCode [char](3)
)
AS
BEGIN

	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	UPDATE
		[dbo].[VenueMMSettings]
	SET
		[Enabled] = 0
	WHERE
		[CounterpartyId] = @CounterpartyId
END
GO


ALTER PROCEDURE [dbo].[VenueMMSettingsDisableEnableOne_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled Bit
)
AS
BEGIN

	DECLARE @SymbolId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	UPDATE
		[dbo].[VenueMMSettings]
	SET
		[Enabled] = @Enabled
	WHERE
		[SymbolId] = @SymbolId
		AND [CounterpartyId] = @CounterpartyId
END
GO


ALTER PROCEDURE [dbo].[VenueMMSettingsReset_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3)
)
AS
BEGIN

	DECLARE @SymbolId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	UPDATE
		[dbo].[VenueMMSettings]
	SET
		[LastResetRequestedUtc] = GETUTCDATE()
	WHERE
		[SymbolId] = @SymbolId
		AND [CounterpartyId] = @CounterpartyId
		AND [Enabled] = 0
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsUpdateSingle_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@LastUpdateTimeUtc DATETIME2,
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit]
)
AS
BEGIN

	DECLARE @SymbolId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	UPDATE
		[dbo].[VenueMMSettings]
	SET
		[SizeToTrade] = @SizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds,
		[Enabled] = @Enabled
	WHERE
		[SymbolId] = @SymbolId
		AND [CounterpartyId] = @CounterpartyId
		AND [LastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO