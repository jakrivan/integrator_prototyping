
print 'Starting:'
print GETUTCDATE()
GO

ALTER TABLE [dbo].[DealExternalExecuted] DROP CONSTRAINT [FK_DealExternalExecuted_OrderExternal]
GO

ALTER TABLE [dbo].[DealExternalRejected] DROP CONSTRAINT [FK_RejectedDealExternal_OrderExternal]
GO

ALTER TABLE [dbo].[ExecutionReportMessage] DROP CONSTRAINT [FK_ExecutionReportMessage_OrderExternal]
GO

ALTER TABLE [dbo].[OrderExternalCancelRequest] DROP CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternal]
GO

ALTER TABLE [dbo].[OrderExternalCancelResult] DROP CONSTRAINT [FK_OrderExternalCancelResult_OrderExternal]
GO

print 'FKs to OrderExternal dropped:'
print GETUTCDATE()
GO


/****** Object:  Index [PK_OrderExternal]    Script Date: 30. 10. 2013 10:56:07 ******/
DROP INDEX [PK_OrderExternal] ON [dbo].[OrderExternal] WITH ( ONLINE = OFF )
GO

/****** Object:  Index [IX_OrderExternalOrderSentUtc]    Script Date: 30. 10. 2013 10:56:33 ******/
DROP INDEX [IX_OrderExternalOrderSentUtc] ON [dbo].[OrderExternal]
GO

/****** Object:  Index [IX_OrderExternal_ClientOrderId]    Script Date: 30. 10. 2013 11:05:03 ******/
DROP INDEX [IX_OrderExternal_ClientOrderId] ON [dbo].[OrderExternal]
GO

/****** Object:  Index [IX_OrderExternalCancelRequest_OrderId]    Script Date: 30. 10. 2013 10:48:30 ******/
CREATE CLUSTERED INDEX [IX_OrderExternal_IntegratorSentExternalOrderUtc] ON [dbo].[OrderExternal]
(
	[IntegratorSentExternalOrderUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


print 'OrderExternal reindexed (clustered index):'
print GETUTCDATE()
GO

--nonclustered index
/****** Object:  Index [IX_OrderExternal_OrderId]    Script Date: 30. 10. 2013 11:02:37 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_OrderExternal_OrderId] ON [dbo].[OrderExternal]
(
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

print 'OrderExternal reindexed (nonclustered index):'
print GETUTCDATE()
GO

ALTER TABLE [dbo].[DealExternalExecuted]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecuted_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO

ALTER TABLE [dbo].[DealExternalExecuted] CHECK CONSTRAINT [FK_DealExternalExecuted_OrderExternal]
GO

ALTER TABLE [dbo].[DealExternalRejected]  WITH CHECK ADD  CONSTRAINT [FK_RejectedDealExternal_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO

ALTER TABLE [dbo].[DealExternalRejected] CHECK CONSTRAINT [FK_RejectedDealExternal_OrderExternal]
GO

ALTER TABLE [dbo].[ExecutionReportMessage]  WITH CHECK ADD  CONSTRAINT [FK_ExecutionReportMessage_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO

ALTER TABLE [dbo].[ExecutionReportMessage] CHECK CONSTRAINT [FK_ExecutionReportMessage_OrderExternal]
GO

ALTER TABLE [dbo].[OrderExternalCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO

ALTER TABLE [dbo].[OrderExternalCancelRequest] CHECK CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternal]
GO

ALTER TABLE [dbo].[OrderExternalCancelResult]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelResult_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO

ALTER TABLE [dbo].[OrderExternalCancelResult] CHECK CONSTRAINT [FK_OrderExternalCancelResult_OrderExternal]
GO

print 'FKs to OrderExternal reenabled:'
print GETUTCDATE()
GO

/****** Object:  Index [IX_ReportReceivedUtc_SymbolId_CounterpartyId]    Script Date: 30. 10. 2013 10:59:19 ******/
DROP INDEX [IX_ReportReceivedUtc_SymbolId_CounterpartyId] ON [dbo].[DealExternalExecuted] WITH ( ONLINE = OFF )
GO

/****** Object:  Index [IX_DealExternalExecuted_OrderId]    Script Date: 30. 10. 2013 10:59:41 ******/
CREATE CLUSTERED INDEX [IX_DealExternalExecuted_OrderId] ON [dbo].[DealExternalExecuted]
(
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


print 'DealExternalExecuted reindexed:'
print GETUTCDATE()
GO


/****** Object:  Index [IX_RejectedDealExternalUtc_SymbolId_CounterpartyId]    Script Date: 30. 10. 2013 11:00:49 ******/
DROP INDEX [IX_RejectedDealExternalUtc_SymbolId_CounterpartyId] ON [dbo].[DealExternalRejected] WITH ( ONLINE = OFF )
GO

/****** Object:  Index [IX_DealExternalRejected_OrderId]    Script Date: 30. 10. 2013 10:59:41 ******/
CREATE CLUSTERED INDEX [IX_DealExternalRejected_OrderId] ON [dbo].[DealExternalRejected]
(
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


print 'DealExternalRejected reindexed:'
print GETUTCDATE()
GO


/****** Object:  Index [IX_ExecutionReportMessage]    Script Date: 30. 10. 2013 11:08:07 ******/
DROP INDEX [IX_ExecutionReportMessage] ON [dbo].[ExecutionReportMessage] WITH ( ONLINE = OFF )
GO

/****** Object:  Index [IX_ExecutionReportMessage_OrderId]    Script Date: 30. 10. 2013 11:06:58 ******/
CREATE CLUSTERED INDEX [IX_ExecutionReportMessage_OrderId] ON [dbo].[ExecutionReportMessage]
(
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

print 'ExecutionReportMessage reindexed:'
print GETUTCDATE()
GO


--nonclustered indexes

/****** Object:  Index [IX_DealExternalExecuted_IntegratorReceivedExecutionReportUtc]    Script Date: 30. 10. 2013 11:02:37 ******/
CREATE NONCLUSTERED INDEX [IX_DealExternalExecuted_IntegratorReceivedExecutionReportUtc] ON [dbo].[DealExternalExecuted]
(
	[IntegratorReceivedExecutionReportUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/****** Object:  Index [IX_DealExternalRejected_IntegratorReceivedExecutionReportUtc]    Script Date: 30. 10. 2013 11:02:37 ******/
CREATE NONCLUSTERED INDEX [IX_DealExternalRejected_IntegratorReceivedExecutionReportUtc] ON [dbo].[DealExternalRejected]
(
	[IntegratorReceivedExecutionReportUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_ExecutionReportMessage_IntegratorReceivedExecutionReportUtc]    Script Date: 30. 10. 2013 11:02:37 ******/
CREATE NONCLUSTERED INDEX [IX_ExecutionReportMessage_IntegratorReceivedExecutionReportUtc] ON [dbo].[ExecutionReportMessage]
(
	[IntegratorReceivedExecutionReportUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_OrderExternalCancelRequest_IntegratorSentCancelRequestUtc]    Script Date: 30. 10. 2013 11:02:37 ******/
CREATE NONCLUSTERED INDEX [IX_OrderExternalCancelRequest_IntegratorSentCancelRequestUtc] ON [dbo].[OrderExternalCancelRequest]
(
	[IntegratorSentCancelRequestUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_OrderExternalCancelResult_CounterpartySentResultUtc]    Script Date: 30. 10. 2013 11:02:37 ******/
CREATE NONCLUSTERED INDEX [IX_OrderExternalCancelResult_CounterpartySentResultUtc] ON [dbo].[OrderExternalCancelResult]
(
	[CounterpartySentResultUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


print 'DONE'
print GETUTCDATE()
GO
