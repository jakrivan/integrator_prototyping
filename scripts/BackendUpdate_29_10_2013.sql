


/****** Object:  StoredProcedure [dbo].[UpdateSettingsXml_SP]    Script Date: 29. 10. 2013 15:26:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[UpdateSettingsXml_SP] (
	@SettingName NVARCHAR(100),
	@SettingsXml Xml
	)
AS
BEGIN
	UPDATE 
	[dbo].[Settings] SET 
		[SettingsXml] = @SettingsXml, 
		[LastUpdated] = GETUTCDATE()
	WHERE
		[Name] = @SettingName
END

GO
GRANT EXECUTE ON [dbo].[UpdateSettingsXml_SP] TO [IntegratorServiceAccount] AS [dbo]
GO



--Schema update


DROP TABLE [dbo].[Settings]
GO



DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.RiskManagement.dll.Schema]
GO

/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.RiskManagement.dll.Schema]    Script Date: 1. 10. 2013 11:11:22 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.RiskManagement.dll.Schema] AS 
N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RiskManagementSettings" nillable="true" type="RiskManagementSettings" />
  <xs:complexType name="RiskManagementSettings">
    <xs:sequence>
      <xs:element name="ConfigLastUpdated"  nillable="true" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAndPosition" type="PriceAndPositionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersSubmissionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrderMaximumAllowedSizeSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedBaseAbsSizeInUsd" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="PriceAndPositionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumExternalNOPInUSD" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceDeviationCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceHistoryForOnlinePriceDeviationCheck" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="OnlinePriceDeviationCheckMaxPriceAge_Minutes" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="Symbols" type="ArrayOfSymbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbol">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="Symbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="Symbol">
    <xs:attribute name="ShortName" type="xs:string" />
    <xs:attribute name="MidPrice" type="xs:decimal" />
    <xs:attribute name="NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
  </xs:complexType>
</xs:schema>'
GO

CREATE TABLE [dbo].[Settings](
	[Name] [nvarchar](100) NOT NULL,
	[SettingsXml] [xml](CONTENT [dbo].[Kreslik.Integrator.RiskManagement.dll.Schema]) NOT NULL,
	[LastUpdated] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


INSERT INTO [dbo].[Settings]
           ([Name]
           ,[SettingsXml]
		   ,[LastUpdated])
     VALUES
           (N'Kreslik.Integrator.RiskManagement.dll'
           ,
N'<RiskManagementSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ConfigLastUpdated>2013-10-01-0846-UTC</ConfigLastUpdated>
  <ExternalOrdersSubmissionRate>
    <RiskCheckAllowed>true</RiskCheckAllowed>
    <MaximumAllowedInstancesPerTimeInterval>50</MaximumAllowedInstancesPerTimeInterval>
    <TimeIntervalToCheck_Seconds>5</TimeIntervalToCheck_Seconds>
  </ExternalOrdersSubmissionRate>
  <ExternalOrdersRejectionRate>
    <RiskCheckAllowed>true</RiskCheckAllowed>
    <MaximumAllowedInstancesPerTimeInterval>100</MaximumAllowedInstancesPerTimeInterval>
    <TimeIntervalToCheck_Seconds>40</TimeIntervalToCheck_Seconds>
  </ExternalOrdersRejectionRate>
  <ExternalOrderMaximumAllowedSize>
    <RiskCheckAllowed>true</RiskCheckAllowed>
    <MaximumAllowedBaseAbsSizeInUsd>2500000</MaximumAllowedBaseAbsSizeInUsd>
  </ExternalOrderMaximumAllowedSize>
  <PriceAndPosition>
    <ExternalNOPRiskCheckAllowed>true</ExternalNOPRiskCheckAllowed>
    <MaximumExternalNOPInUSD>10000000</MaximumExternalNOPInUSD>
    <MaximumAllowedNOPCalculationTime_Milliseconds>2</MaximumAllowedNOPCalculationTime_Milliseconds>
    <PriceDeviationCheckAllowed>true</PriceDeviationCheckAllowed>
    <PriceHistoryForOnlinePriceDeviationCheck>200</PriceHistoryForOnlinePriceDeviationCheck>
    <OnlinePriceDeviationCheckMaxPriceAge_Minutes>15</OnlinePriceDeviationCheckMaxPriceAge_Minutes>
    <ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed>true</ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed>
    <Symbols>
      <Symbol ShortName="AUDCAD" MidPrice="1.00168" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="AUDDKK" MidPrice="5.17698" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="AUDHKD" MidPrice="7.25922" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="AUDCHF" MidPrice="0.855915" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="AUDJPY" MidPrice="93.1185" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="AUDNOK" MidPrice="5.648565" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="AUDNZD" MidPrice="1.156105" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="AUDSEK" MidPrice="6.0703" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="AUDSGD" MidPrice="1.184835" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="AUDUSD" MidPrice="0.958625" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="CADDKK" MidPrice="5.16825" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="CADCHF" MidPrice="0.854485" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="CADJPY" MidPrice="92.96249" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="CADNOK" MidPrice="5.63915" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="CADNZD" MidPrice="1.154295" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="CADSEK" MidPrice="6.0602" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="DKKJPY" MidPrice="17.9867" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="DKKNOK" MidPrice="1.091135" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="DKKSEK" MidPrice="1.172505" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURAUD" MidPrice="1.44079" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURCAD" MidPrice="1.44323" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURCNH" MidPrice="8.2849" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURCZK" MidPrice="25.767" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURDKK" MidPrice="7.458895" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURGBP" MidPrice="0.85187" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURHKD" MidPrice="10.48485" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURHUF" MidPrice="293.1135" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURCHF" MidPrice="1.233195" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURJPY" MidPrice="134.165" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURMXN" MidPrice="17.94408" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURNOK" MidPrice="8.13869" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURNZD" MidPrice="1.66576" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURPLN" MidPrice="4.189235" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURRUB" MidPrice="43.3967" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURSEK" MidPrice="8.745695" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURSKK" MidPrice="30.1258" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURTRY" MidPrice="2.746685" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURUSD" MidPrice="1.381175" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="EURZAR" MidPrice="13.57395" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="GBPAUD" MidPrice="1.69133" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="GBPCAD" MidPrice="1.6942" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="GBPCZK" MidPrice="30.70182" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="GBPDKK" MidPrice="8.755899" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="GBPHUF" MidPrice="355.3187" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="GBPCHF" MidPrice="1.44764" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="GBPJPY" MidPrice="157.499" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="GBPNOK" MidPrice="9.553755" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="GBPNZD" MidPrice="1.955445" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="GBPPLN" MidPrice="5.0486" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="GBPSEK" MidPrice="10.26682" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="GBPUSD" MidPrice="1.62135" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="HKDJPY" MidPrice="12.52831" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="CHFDKK" MidPrice="6.0485" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="CHFJPY" MidPrice="108.7945" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="CHFNOK" MidPrice="6.599495" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="CHFSEK" MidPrice="7.091885" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="MXNJPY" MidPrice="7.47665" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="NOKJPY" MidPrice="16.485" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="NOKSEK" MidPrice="1.074675" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="NZDCAD" MidPrice="0.866405" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="NZDDKK" MidPrice="4.47785" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="NZDCHF" MidPrice="0.74031" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="NZDJPY" MidPrice="80.5415" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="NZDNOK" MidPrice="4.88585" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="NZDSEK" MidPrice="5.25035" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="NZDSGD" MidPrice="1.02485" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="NZDUSD" MidPrice="0.829135" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="SGDJPY" MidPrice="78.5895" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDCAD" MidPrice="1.04491" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDCNH" MidPrice="6.1172" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDCZK" MidPrice="18.6555" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDDKK" MidPrice="5.4004" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDHKD" MidPrice="7.753455" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDHUF" MidPrice="212.215" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDCHF" MidPrice="0.89285" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDILS" MidPrice="3.527635" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDJPY" MidPrice="97.1385" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDMXN" MidPrice="12.99205" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDNOK" MidPrice="5.89253" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDPLN" MidPrice="3.033" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDRUB" MidPrice="32.4515" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDSEK" MidPrice="6.332355" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDSGD" MidPrice="1.23596" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDSKK" MidPrice="22.2791" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDTRY" MidPrice="1.98799" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="USDZAR" MidPrice="9.82785" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
      <Symbol ShortName="ZARJPY" MidPrice="9.884035" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
    </Symbols>
  </PriceAndPosition>
</RiskManagementSettings>'
,
GETUTCDATE())
GO