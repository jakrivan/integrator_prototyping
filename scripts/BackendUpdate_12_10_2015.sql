BEGIN TRANSACTION AAA




ALTER TABLE [dbo].[DealExternalExecuted] ADD SettlementTimeUtc [datetime] NULL
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] ADD SettlementTimeUtc [datetime] NULL
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] ADD SettlementTimeUtc [datetime] NULL
GO

-- manuall recalculation from code

ALTER TABLE [dbo].[DealExternalExecuted] ALTER COLUMN [SettlementTimeUtc] [datetime] NOT NULL
GO


TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
ALTER TABLE [dbo].[DealExternalExecutedUnsettled] ADD SettlementTimeUtc [datetime] NOT NULL
GO

DROP INDEX [IX_DealExternalExecutedUnsettled_CounterpartySymbol] ON [dbo].[DealExternalExecutedUnsettled] WITH ( ONLINE = OFF )
GO

CREATE CLUSTERED INDEX [IX_DealExternalExecutedUnsettled_Settlement] ON [dbo].[DealExternalExecutedUnsettled]
(
	[SettlementTimeUtc] ASC
)ON [Integrator_DailyData]
GO

ALTER PROCEDURE [dbo].[ForceRefreshAllUnsettledDeals_SP]
AS
BEGIN

	DECLARE @temp_unsettledDeals TABLE
	(
		[CounterpartyId] [tinyint],
		[SymbolId] [tinyint],
		[SettlementDateLocalMkt] [date],
		[SettlementTimeUtc] [DateTime],
		[SizeBasePol] [decimal](18, 2),
		[SizeTermPol] [decimal](18, 4),
		[DealsCount] [int]
	)

	INSERT INTO
		@temp_unsettledDeals
		(
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SettlementTimeUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		)
	SELECT
		[CounterpartyId],
		[SymbolId],
		[ValueDate],
		[SettlementTimeUtc],
		SUM([SizeBasePol]),
		SUM([SizeTermPol]),
		--count arbitrary column
		COUNT([CounterpartyId])
	FROM
		(
		SELECT 
			deal.[CounterpartyId]
			,deal.[SymbolId]
			,deal.[ValueDateCounterpartySuppliedLocMktDate] AS [ValueDate]
			,deal.[SettlementTimeUtc]
			,deal.[AmountBasePolExecuted] AS [SizeBasePol]
			,-deal.[AmountBasePolExecuted]*deal.[Price] AS [SizeTermPol]
		FROM 
			[dbo].[DealExternalExecuted] deal
		WHERE
			IntegratorReceivedExecutionReportUtc > DATEADD(DAY, -10, GETUTCDATE())
			AND
			[SettlementTimeUtc] > GetUtcDate()

		UNION ALL

		SELECT 
			[CounterpartyId]
			,[SymbolId]
			,[ValueDate]
			,[SettlementTimeUtc]
			,[IntegratorExecutedAmountBasePol] AS [SizeBasePol]
			,-[IntegratorExecutedAmountBasePol]*[CounterpartyRequestedPrice] AS [SizeTermPol]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]
		WHERE
			[IntegratorExecutedAmountBasePol] > 0

		UNION ALL

		SELECT 
			[CounterpartyId]
			,[SymbolId]
			,[ValueDate]
			,[SettlementTimeUtc]
			,[IntegratorExecutedAmountBasePol] AS [SizeBasePol]
			,-[IntegratorExecutedAmountBasePol]*[CounterpartyRequestedPrice] AS [SizeTermPol]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Archive] deal
		WHERE
			[IntegratorReceivedExternalOrderUtc] > DATEADD(DAY, -10, GETUTCDATE())
			AND
			[IntegratorExecutedAmountBasePol] > 0
			AND
			[SettlementTimeUtc] > GetUtcDate()
		) allUnsettledDeals
	GROUP BY
		[SettlementTimeUtc],
		[ValueDate],
		[CounterpartyId],
		[SymbolId]

		
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
	
		INSERT INTO
			[dbo].[DealExternalExecutedUnsettled]
			(
				[CounterpartyId],
				[SymbolId],
				[SettlementDateLocalMkt],
				[SettlementTimeUtc],
				[SizeBasePol],
				[SizeTermPol],
				[DealsCount]
			)
		SELECT
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SettlementTimeUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		FROM
			@temp_unsettledDeals
	COMMIT	
END
GO

exec [dbo].[ForceRefreshAllUnsettledDeals_SP]
GO



ALTER PROCEDURE [dbo].[Daily_PurgeAndRollupUnsettledDeals_SP]
AS
BEGIN

	DECLARE @temp_unsettledDeals TABLE
	(
		[CounterpartyId] [tinyint],
		[SymbolId] [tinyint],
		[SettlementDateLocalMkt] [date],
		[SettlementTimeUtc] [DateTime],
		[SizeBasePol] [decimal](18, 2),
		[SizeTermPol] [decimal](18, 4),
		[DealsCount] [int]
	)

	INSERT INTO
		@temp_unsettledDeals
		(
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SettlementTimeUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		)
	SELECT 
		[CounterpartyId]
		,[SymbolId]
		,[SettlementDateLocalMkt]
		,[SettlementTimeUtc]
		,SUM([SizeBasePol]) AS [NopBasePol]
		,SUM([SizeTermPol]) AS [NopTermPol]
		,SUM([DealsCount]) AS [DealsCount]
	FROM
		[dbo].[DealExternalExecutedUnsettled] unsettled				
	WHERE
		[SettlementTimeUtc] > GetUtcDate()
	GROUP BY
		[SettlementTimeUtc],
		[SettlementDateLocalMkt],
		[CounterpartyId],
		[SymbolId]
		
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
	
		INSERT INTO
			[dbo].[DealExternalExecutedUnsettled]
			(
				[CounterpartyId],
				[SymbolId],
				[SettlementDateLocalMkt],
				[SettlementTimeUtc],
				[SizeBasePol],
				[SizeTermPol],
				[DealsCount]
			)
		SELECT
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SettlementTimeUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		FROM
			@temp_unsettledDeals
	COMMIT	
END
GO

ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 2),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@ValueDateCounterpartySuppliedRecalcedToUtc DATETIME,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL,
	@SingleFillSystemId int = NULL,
	@MultipleFillsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL,
	@CounterpartyClientId varchar(20) = NULL)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @MultipleFillsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleFillsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external deal for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@SingleFillSystemId, -1)
				,@AmountBasePolExecuted
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc)
		END
		
		INSERT INTO 
		[dbo].[DealExternalExecuted]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolExecuted]
			   ,[AmountTermPolExecutedInUsd]
			   ,[Price]
			   ,[CounterpartyTransactionId]
			   ,[CounterpartySuppliedExecutionTimeStampUtc]
			   ,[ValueDateCounterpartySuppliedLocMktDate]
			   ,[SettlementTimeUtc]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[InternalTransactionIdentity]
			   ,[FlowSideId]
			   ,[CounterpartyClientId])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolExecuted
			   ,@AmountTermPolExecutedInUsd
			   ,@Price
			   ,@CounterpartyTransactionId
			   ,@CounterpartyExecutionReportTimeStampUtc
			   ,@ValueDateCounterpartySuppliedLocMktDate
			   ,@ValueDateCounterpartySuppliedRecalcedToUtc
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@CounterpartySentExecutionReportUtc
			   ,@InternalDealId
			   ,@FlowSideId
			   ,@CounterpartyClientId)
			   
		INSERT INTO [dbo].[DealExternalExecutedUnsettled]
           ([CounterpartyId]
           ,[SymbolId]
           ,[SettlementDateLocalMkt]
		   ,[SettlementTimeUtc]
           ,[SizeBasePol]
           ,[SizeTermPol]
		   ,[DealsCount])
		VALUES
           (@CounterpartyId
           ,@SymbolId
           ,@ValueDateCounterpartySuppliedLocMktDate
		   ,@ValueDateCounterpartySuppliedRecalcedToUtc
           ,@AmountBasePolExecuted
           ,-@AmountBasePolExecuted*@Price
		   ,1)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[InsertNewOrderExternalIncomingRejectable_SP]( 
    @ExternalOrderReceived [datetime2](7),
	@ExternalOrderSentUtc [datetime2](7),
	@IntegratorSentExecutionReportUtc [datetime2](7),
	@CounterpartyClientOrderId [varchar](32),
	@CounterpartyClientId [varchar](20),
	@TradingSystemId int = NULL,
	@SourceIntegratorPriceIdentity [varchar](20) = NULL,
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@RequestedFillMinimumBaseAbs decimal(18,2) = NULL,
	@IntegratorExecId [varchar](20) = NULL,
	@CounterpartyExecId [varchar](32) = NULL,
	@ValueDate [date] = NULL,
	@SettlementTimeUtc [datetime] = NULL,
	@OrderReceivedToExecutionReportSentLatency [time](7),
	@IntegratorExecutedAmountBaseAbs [decimal](18, 2) = NULL,
	@IntegratorRejectedAmountBaseAbs [decimal](18, 2) = NULL,
	@RejectionReason [varchar](32) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WITH(NOLOCK) WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WITH(NOLOCK) WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WITH(NOLOCK) WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @RejectionReason IS NOT NULL
	BEGIN
		SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
		IF @@ROWCOUNT <> 1
		BEGIN
			INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
			SET @RejectionReasonId = @@IDENTITY
		END
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @IntegratorExecutedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,COALESCE(@IntegratorExecId, @CounterpartyExecId)
				,@ExternalOrderReceived)
		END
		ELSE IF @IntegratorRejectedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,ISNULL(@RejectionReason, '<NONE>')
				,COALESCE(@IntegratorExecId, @CounterpartyExecId)
				,@RejectionDirectionId
				,@ExternalOrderReceived)
		END

		INSERT INTO 
			[dbo].[OrderExternalIncomingRejectable_Daily]
			   ([IntegratorReceivedExternalOrderUtc]
			   ,[CounterpartySentExternalOrderUtc]
			   ,[IntegratorSentExecutionReportUtc]
			   ,[CounterpartyClientOrderId]
			   ,[CounterpartyClientId]
			   ,[TradingSystemId]
			   ,[SourceIntegratorPriceIdentity]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorDealDirectionId]
			   ,[CounterpartyRequestedPrice]
			   ,[CounterpartyRequestedAmountBasePol]
			   ,[CounterpartyRequestedFillMinimumBaseAbs]
			   ,[IntegratorExecutionId]
			   ,[CounterpartyExecutionId]
			   ,[ValueDate]
			   ,[SettlementTimeUtc]
			   ,[OrderReceivedToExecutionReportSentLatency ]
			   ,[IntegratorExecutedAmountBasePol]
			   ,[IntegratorRejectedAmountBasePol]
			   ,[IntegratorRejectionReasonId])
		 VALUES
			   (@ExternalOrderReceived
			   ,@ExternalOrderSentUtc
			   ,@IntegratorSentExecutionReportUtc
			   ,@CounterpartyClientOrderId
			   ,@CounterpartyClientId
			   ,@TradingSystemId
			   ,@SourceIntegratorPriceIdentity
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@DealDirectionId
			   ,@RequestedPrice
			   ,CASE WHEN @DealDirectionId = 1 THEN -@RequestedAmountBaseAbs ELSE @RequestedAmountBaseAbs END
			   ,@RequestedFillMinimumBaseAbs
			   ,@IntegratorExecId
			   ,@CounterpartyExecId
			   ,@ValueDate
			   ,@SettlementTimeUtc
			   ,@OrderReceivedToExecutionReportSentLatency 
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
		       ,@RejectionReasonId)
			   
		IF @ValueDate IS NOT NULL AND @IntegratorExecutedAmountBaseAbs != 0
		BEGIN
			INSERT INTO [dbo].[DealExternalExecutedUnsettled]
			   ([CounterpartyId]
			   ,[SymbolId]
			   ,[SettlementDateLocalMkt]
			   ,[SettlementTimeUtc]
			   ,[SizeBasePol]
			   ,[SizeTermPol]
			   ,[DealsCount])
			VALUES
			   (@CounterpartyId
			   ,@SymbolId
			   ,@ValueDate
			   ,@SettlementTimeUtc
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN @IntegratorExecutedAmountBaseAbs ELSE -@IntegratorExecutedAmountBaseAbs END * @RequestedPrice
			   ,1)
		END
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH	   
END
GO

ALTER PROCEDURE [dbo].[Daily_ArchiveRejectableOrders_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[OrderExternalIncomingRejectable_Archive]
			([IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[CounterpartyRequestedFillMinimumBaseAbs],
			[IntegratorExecutionId],
			[CounterpartyExecutionId],
			[ValueDate],
			[SettlementTimeUtc],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[GlobalRecordId])
		SELECT
			[IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[CounterpartyRequestedFillMinimumBaseAbs],
			[IntegratorExecutionId],
			[CounterpartyExecutionId],
			[ValueDate],
			[SettlementTimeUtc],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[GlobalRecordId]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]
		
		--remember last identity
		DECLARE @LastId INT
		SELECT @LastId = IDENT_CURRENT ('OrderExternalIncomingRejectable_Daily')
		SET @LastId = @LastId + 1
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[OrderExternalIncomingRejectable_Daily]
		
		--and continue in identity
		DBCC CHECKIDENT ('OrderExternalIncomingRejectable_Daily', RESEED, @LastId)
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO


ALTER PROCEDURE [dbo].[UpdateSettlementStatisticsPerCreditLine_SP]
(
	@Force BIT = 0
)
AS
BEGIN

	UPDATE 
		[dbo].[TableChangeLog] 
	SET 
		[LastUpdatedUtc] = GETUTCDATE() 
	WHERE 
		[TableName] = 'SettlementStatistics' 
		AND 
		(@Force = 1 OR [LastUpdatedUtc] < DATEADD(SECOND, -2, GETUTCDATE()))

	IF @@ROWCOUNT > 0
	BEGIN

				
		DECLARE @temp_final_result TABLE
		(
			[PrimeBrokerName] [varchar](10),
			[CreditLineName] [varchar](255),
			[UnsettledNopUsdTotal] [decimal](18, 2),
			[DealsCountTotal] [int],
			[MaxUnsettledNopUsdTotal] [decimal](18, 2),
			[MaxUnsettledNopUsdTotalToTurnGFO] [decimal](18, 2),
			[MaxUnsettledNopUsdPerValueDate] [decimal](18, 2),
			[MaxUnsettledNopUsdPerValueDateToTurnGFO] [decimal](18, 2),
			T1 [date],
			T2 [date],
			T3 [date],
			T4 [date],
			T5 [date],
			T6 [date],
			T7 [date],
			T8 [date],
			UnsettledNopUsd_T1 [decimal](18, 2),
			UnsettledNopUsd_T2 [decimal](18, 2),
			UnsettledNopUsd_T3 [decimal](18, 2),
			UnsettledNopUsd_T4 [decimal](18, 2),
			UnsettledNopUsd_T5 [decimal](18, 2),
			UnsettledNopUsd_T6 [decimal](18, 2),
			UnsettledNopUsd_T7 [decimal](18, 2),
			UnsettledNopUsd_T8 [decimal](18, 2),
			[DealsCount_T1] [int],
			[DealsCount_T2] [int],
			[DealsCount_T3] [int],
			[DealsCount_T4] [int],
			[DealsCount_T5] [int],
			[DealsCount_T6] [int],
			[DealsCount_T7] [int],
			[DealsCount_T8] [int],
			[HighestUnsettledNopInSingleT] [decimal](18, 2),
			[CreditLineId] [tinyint]
		)
		
		
		DECLARE @temp_NOPs_pol TABLE
		(
			CreditLineId tinyint,
			BaseCurrencyId smallint,
			QuoteCurrencyId smallint,
			NopBasePol decimal(18,2),
			NopTermPol decimal(18,4),
			[DealsCount] int,
			[ValueDate] Date
		)
		
		DECLARE @temp_intermediate_result TABLE
		(
			CreditLineId tinyint,
			[UnsettledNopUsd] [decimal](18, 2),
			[DealsCount] [int],
			[ValueDate] Date
		)

		INSERT INTO
			@temp_NOPs_pol
			(
				CreditLineId,
				BaseCurrencyId,
				QuoteCurrencyId,
				NopBasePol,
				NopTermPol,
				DealsCount,
				ValueDate
			)
		SELECT 
			[CreditLineId]
			,MAX(sym.BaseCurrencyId) AS BaseCurrencyId
			,MAX(sym.QuoteCurrencyId) AS QuoteCurrencyId
			,SUM([SizeBasePol]) AS [NopBasePol]
			,SUM([SizeTermPol]) AS [NopTermPol]
			,SUM(DealsCount) AS [DealsCount]
			,SettlementDateLocalMkt AS ValueDate
			--,COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate]) AS [SettlementDate]
		FROM
			[dbo].[DealExternalExecutedUnsettled] unsettled
			INNER JOIN [dbo].[PrimeBrokerCreditLines_CptMapping] creditLineMap ON unsettled.[CounterpartyId] = creditLineMap.[CounterpartyId]
			INNER JOIN Symbol sym ON sym.ID = unsettled.[SymbolId]				
		WHERE
			[SettlementTimeUtc] > GetUtcDate()
		GROUP BY
			[CreditLineId],
			[SymbolId],
			[SettlementDateLocalMkt]

		
		INSERT INTO
			@temp_intermediate_result
			(
				[CreditLineId],
				[UnsettledNopUsd],
				[DealsCount],
				[ValueDate]
			)
			SELECT
				CreditLineId,
				SUM(ABS(NopPolInUSD))/2 AS NopAbsInUSD,
				SUM(DealsCount) AS DealsCount,
				ValueDate
			FROM
				(
				SELECT
					CreditLineId,
					nopsUnaggreggated.CurrencyId,
					SUM(NopPol * ccy.LastKnownUsdConversionMultiplier) AS NopPolInUSD,
					SUM(DealsCount) AS DealsCount,
					ValueDate
				FROM
					(
					SELECT
						CreditLineId,
						BaseCurrencyId AS CurrencyId,
						NopBasePol AS NopPol,
						DealsCount AS DealsCount,
						ValueDate
					FROM
						@temp_NOPs_pol basePos

					UNION ALL

					SELECT
						CreditLineId,
						QuoteCurrencyId AS CurrencyId,
						NopTermPol AS NopPol,
						0 AS DealsCount,
						ValueDate
					FROM
						@temp_NOPs_pol basePos
					) nopsUnaggreggated
					INNER JOIN Currency ccy ON ccy.CurrencyID = nopsUnaggreggated.CurrencyId
				GROUP BY
					CreditLineId,
					nopsUnaggreggated.CurrencyId,
					ValueDate
				)nopsAggreggated
			GROUP BY
				CreditLineId,
				ValueDate

			DECLARE @temp_ValueDates TABLE
			(
				ValueDate Date,
				ValueDateId int
			)

			INSERT INTO @temp_ValueDates(ValueDateId, ValueDate)
			SELECT
				ROW_NUMBER() OVER (ORDER BY ValueDate),
				ValueDate
			FROM
				@temp_intermediate_result
			WHERE
				ValueDate IS NOT NULL
			GROUP BY
				ValueDate


			
			;WITH TmpStats AS
			(
				SELECT 
					CreditLineId,
					UnsettledNopUsd,
					DealsCount,
					res.ValueDate,
					'Nop_T' + CAST(ValueDateId AS VARCHAR) AS NopsValueDateTag,
					'Deals_T' + CAST(ValueDateId AS VARCHAR) AS DealsValueDateTag
			FROM
				@temp_intermediate_result res
				FULL OUTER JOIN @temp_ValueDates valDates ON res.ValueDate = valDates.ValueDate
			)

			 INSERT INTO @temp_final_result
			(
				[PrimeBrokerName]
				,[CreditLineName]
				,[CreditLineId]
				,[MaxUnsettledNopUsdTotal]
				,[MaxUnsettledNopUsdTotalToTurnGFO]
				,[MaxUnsettledNopUsdPerValueDate]
				,[MaxUnsettledNopUsdPerValueDateToTurnGFO]
				,[UnsettledNopUsdTotal]
				,[DealsCountTotal]
				,[T1]
				,[T2]
				,[T3]
				,[T4]
				,[T5]
				,[T6]
				,[T7]
				,[T8]
				,[UnsettledNopUsd_T1]
				,[UnsettledNopUsd_T2]
				,[UnsettledNopUsd_T3]
				,[UnsettledNopUsd_T4]
				,[UnsettledNopUsd_T5]
				,[UnsettledNopUsd_T6]
				,[UnsettledNopUsd_T7]
				,[UnsettledNopUsd_T8]
				,[DealsCount_T1]
				,[DealsCount_T2]
				,[DealsCount_T3]
				,[DealsCount_T4]
				,[DealsCount_T5]
				,[DealsCount_T6]
				,[DealsCount_T7]
				,[DealsCount_T8]
				,[HighestUnsettledNopInSingleT]
			)

			SELECT
				primeBroker.PrimeBrokerName,
				creditLine.[CreditLineName],
				creditLine.[CreditLineId],
				creditLine.[MaxUnsettledNopUsdTotal],
				creditLine.[MaxUnsettledNopUsdTotalToTurnGFO],
				creditLine.[MaxUnsettledNopUsdPerValueDate],
				creditLine.[MaxUnsettledNopUsdPerValueDateToTurnGFO],
				UnsettledNopUsdTotal,
				DealsCountTotal,
				T1, T2, T3, T4, T5, T6, T7, T8,
				Nop_T1, Nop_T2, Nop_T3, Nop_T4, Nop_T5, Nop_T6, Nop_T7, Nop_T8, 
				Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8,
				(SELECT MAX(x) FROM (VALUES(Nop_T1), (Nop_T2), (Nop_T3), (Nop_T4), (Nop_T5), (Nop_T6), (Nop_T7), (Nop_T8)) AS VALUE(x)) AS [HighestUnsettledNopInSingleT]
			FROM

				(
					select T1, T2, T3, T4, T5, T6, T7, T8
					from
					(
						select ValueDate, 'T' + CAST(ValueDateId AS VARCHAR) AS ValueDateTag
						FROM @temp_ValueDates
					) d
					PIVOT
					(
						MAX(ValueDate)
						for ValueDateTag IN (T1, T2, T3, T4, T5, T6, T7, T8)
					) pivDates
				)ValDates

				INNER JOIN [dbo].[PrimeBrokerCreditLine] creditLine ON 1=1
				INNER JOIN [dbo].[PrimeBroker] primeBroker ON creditLine.[PrimeBrokerId] = primeBroker.[PrimeBrokerId]

				LEFT JOIN

				(
				SELECT 
					CreditLineId,
					MAX(Nop_T1) AS Nop_T1, MAX(Nop_T2) AS Nop_T2, MAX(Nop_T3) AS Nop_T3, MAX(Nop_T4) AS Nop_T4, MAX(Nop_T5) AS Nop_T5, MAX(Nop_T6) AS Nop_T6, MAX(Nop_T7) AS Nop_T7, MAX(Nop_T8) AS Nop_T8
				FROM
					TmpStats
					PIVOT 
					(
						MAX(UnsettledNopUsd) FOR NopsValueDateTag IN (Nop_T1, Nop_T2, Nop_T3, Nop_T4, Nop_T5, Nop_T6, Nop_T7, Nop_T8)
					)pivNops
				GROUP BY
					CreditLineId
				)NopsTmp ON creditLine.CreditLineId = NopsTmp.CreditLineId

				LEFT JOIN

				(
				SELECT 
					CreditLineId,
					MAX(Deals_T1) AS Deals_T1, MAX(Deals_T2) AS Deals_T2, MAX(Deals_T3) AS Deals_T3, MAX(Deals_T4) AS Deals_T4, MAX(Deals_T5) AS Deals_T5, MAX(Deals_T6) AS Deals_T6, MAX(Deals_T7) AS Deals_T7, MAX(Deals_T8) AS Deals_T8
				FROM
					TmpStats
					PIVOT 
					(
						MAX(DealsCount) FOR DealsValueDateTag IN (Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8)
					)piv
				GROUP BY
					CreditLineId
				)DealsTmp

				ON NopsTmp.CreditLineId = DealsTmp.CreditLineId

				LEFT JOIN

				(
				SELECT
					CreditLineId,
					SUM(UnsettledNopUsd) AS UnsettledNopUsdTotal,
					SUM(DealsCount) AS DealsCountTotal
				FROM
					TmpStats
				GROUP BY
					CreditLineId
				) totalSum

				ON totalSum.CreditLineId = DealsTmp.CreditLineId

			ORDER BY
				primeBroker.PrimeBrokerName,
				creditLine.[CreditLineName]
			
			
		BEGIN TRANSACTION
			TRUNCATE TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine]
		
			INSERT INTO
				[dbo].[PersistedSettlementStatisticsPerCreditLine]
				(
					[PrimeBrokerName]
					,[CreditLineName]
					,[CreditLineId]
					,[MaxUnsettledNopUsdTotal]
					,[MaxUnsettledNopUsdTotalToTurnGFO]
					,[MaxUnsettledNopUsdPerValueDate]
					,[MaxUnsettledNopUsdPerValueDateToTurnGFO]
					,[UnsettledNopUsdTotal]
					,[DealsCountTotal]
					,[T1]
					,[T2]
					,[T3]
					,[T4]
					,[T5]
					,[T6]
					,[T7]
					,[T8]
					,[UnsettledNopUsd_T1]
					,[UnsettledNopUsd_T2]
					,[UnsettledNopUsd_T3]
					,[UnsettledNopUsd_T4]
					,[UnsettledNopUsd_T5]
					,[UnsettledNopUsd_T6]
					,[UnsettledNopUsd_T7]
					,[UnsettledNopUsd_T8]
					,[DealsCount_T1]
					,[DealsCount_T2]
					,[DealsCount_T3]
					,[DealsCount_T4]
					,[DealsCount_T5]
					,[DealsCount_T6]
					,[DealsCount_T7]
					,[DealsCount_T8]
					,[HighestUnsettledNopInSingleT]
				)
			SELECT
				[PrimeBrokerName]
				,[CreditLineName]
				,[CreditLineId]
				,[MaxUnsettledNopUsdTotal]
				,[MaxUnsettledNopUsdTotalToTurnGFO]
				,[MaxUnsettledNopUsdPerValueDate]
				,[MaxUnsettledNopUsdPerValueDateToTurnGFO]
				,[UnsettledNopUsdTotal]
				,[DealsCountTotal]
				,[T1]
				,[T2]
				,[T3]
				,[T4]
				,[T5]
				,[T6]
				,[T7]
				,[T8]
				,[UnsettledNopUsd_T1]
				,[UnsettledNopUsd_T2]
				,[UnsettledNopUsd_T3]
				,[UnsettledNopUsd_T4]
				,[UnsettledNopUsd_T5]
				,[UnsettledNopUsd_T6]
				,[UnsettledNopUsd_T7]
				,[UnsettledNopUsd_T8]
				,[DealsCount_T1]
				,[DealsCount_T2]
				,[DealsCount_T3]
				,[DealsCount_T4]
				,[DealsCount_T5]
				,[DealsCount_T6]
				,[DealsCount_T7]
				,[DealsCount_T8]
				,[HighestUnsettledNopInSingleT]
			FROM
				@temp_final_result
		COMMIT	
		
		DECLARE @TransmissionEnabled BIT
		DECLARE @GoFlatOnlyOn BIT
		DECLARE @TradingOn BIT
		DECLARE @msg VARCHAR(MAX)
		
		--Total NOP
		IF EXISTS(SELECT * FROM @temp_final_result WHERE [MaxUnsettledNopUsdTotalToTurnGFO] > 0 AND [UnsettledNopUsdTotal] > [MaxUnsettledNopUsdTotalToTurnGFO])
		BEGIN
		
			SET @TransmissionEnabled = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()
			SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]
			SET @TradingOn = @TransmissionEnabled & ~@GoFlatOnlyOn
			
			IF @TradingOn = 1
			BEGIN
			
				exec [dbo].[FlipSystemsTradingControl_SP] @GoFlatOnly = 1
			
				SET @msg = 'Credit Line(s) total unsettled NOP limit is approaching and trading is enabled! Increase total unsettled NOP limits or keep GFO to get rid of this message.'

				SELECT
					@msg = @msg + ' ' + [CreditLineName] + '(' + [PrimeBrokerName] + ') Total Unsettled NOP: ' + CAST([UnsettledNopUsdTotal] AS VARCHAR) + 'USD (MAX: ' + CAST([MaxUnsettledNopUsdTotal] AS VARCHAR) + '). '
				FROM 
					@temp_final_result
				WHERE 
					[MaxUnsettledNopUsdTotalToTurnGFO] > 0 AND [UnsettledNopUsdTotal] > [MaxUnsettledNopUsdTotalToTurnGFO]

					
				INSERT INTO [dbo].[SystemsTradingControlSchedule]
				   ([IsEnabled]
				   ,[IsDaily]
				   ,[IsWeekly]
				   ,[ScheduleTimeZoneSpecific]
				   ,[TimeZoneCode]
				   ,[ActionId]
				   ,[ScheduleDescription]
				   ,[IsImmediatelyOnce])
			 VALUES
				   (1
					,0
				   ,0
				   ,GETUTCDATE()
				   ,'UTC'
				   ,(SELECT [ActionId] FROM [dbo].[SystemsTradingControlScheduleAction] WHERE [ActionName] = 'LogFatal')
				   ,@msg
				   ,1)	
			END
		END
		
		--Per single value date NOP
		IF EXISTS
			(SELECT * FROM @temp_final_result WHERE MaxUnsettledNopUsdPerValueDateToTurnGFO > 0 AND HighestUnsettledNopInSingleT > MaxUnsettledNopUsdPerValueDateToTurnGFO)
		BEGIN
		
			SET @TransmissionEnabled = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()
			SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]
			SET @TradingOn = @TransmissionEnabled & ~@GoFlatOnlyOn
			
			IF @TradingOn = 1
			BEGIN
			
				exec [dbo].[FlipSystemsTradingControl_SP] @GoFlatOnly = 1
			
				SET @msg = 'Credit Line(s) daily unsettled NOP limit is approaching and trading is enabled! Increase daily unsettled NOP limits or keep GFO to get rid of this message.'

				SELECT
					@msg = @msg + ' ' + [CreditLineName] + '(' + [PrimeBrokerName] + ') Unsettled NOP (' + CAST([ValueDate] AS VARCHAR) + '): ' + CAST([UnsettledNopUsd] AS VARCHAR) + 'USD (MAX per single value date: ' + CAST([MaxUnsettledNopUsdPerValueDate] AS VARCHAR) + '). '
				FROM 
					@temp_intermediate_result perDateResult
					INNER JOIN [dbo].[PrimeBrokerCreditLine] creditLine ON perDateResult.CreditLineId = creditLine.CreditLineId
					INNER JOIN [dbo].[PrimeBroker] primeBroker ON creditLine.[PrimeBrokerId] = primeBroker.[PrimeBrokerId]
				WHERE 
					[MaxUnsettledNopUsdPerValueDateToTurnGFO] > 0 AND [UnsettledNopUsd] > [MaxUnsettledNopUsdPerValueDateToTurnGFO]


					
				INSERT INTO [dbo].[SystemsTradingControlSchedule]
				   ([IsEnabled]
				   ,[IsDaily]
				   ,[IsWeekly]
				   ,[ScheduleTimeZoneSpecific]
				   ,[TimeZoneCode]
				   ,[ActionId]
				   ,[ScheduleDescription]
				   ,[IsImmediatelyOnce])
			 VALUES
				   (1
					,0
				   ,0
				   ,GETUTCDATE()
				   ,'UTC'
				   ,(SELECT [ActionId] FROM [dbo].[SystemsTradingControlScheduleAction] WHERE [ActionName] = 'LogFatal')
				   ,@msg
				   ,1)	
			END
		END
		
		
		
	END
END 
GO

----------------------------- ˇˇˇˇ  below is the NOP calc code  ˇˇˇˇ ------------------------

DROP PROCEDURE [dbo].[GetNopsPolPerCounterpartyAndCurrency_SP]

CREATE PROCEDURE [dbo].[GetUnsettledDealsPerCreditLineSettlementAndCurrency_SP]
AS
BEGIN
	SELECT 
		ctp.[CounterpartyCode] AS Counterparty,
		sym.[Name] AS Symbol,
		SettlementDateLocalMkt AS SettlementDateLocal,
		SettlementTimeUtc,
		SUM(unsettled.SizeBasePol) AS SizeBasePol,
		SUM(unsettled.SizeTermPol) AS SizeTermPol
	FROM
		[dbo].[DealExternalExecutedUnsettled] unsettled
		INNER JOIN [dbo].[Counterparty] ctp ON unsettled.[CounterpartyId] = ctp.[CounterpartyId]
		INNER JOIN Symbol sym ON sym.ID = unsettled.[SymbolId]				
	WHERE
		[SettlementTimeUtc] >= GETUTCDATE()
	GROUP BY
		[SettlementTimeUtc],
		[SettlementDateLocalMkt],
		ctp.[CounterpartyCode],
		sym.[Name]
END
GO

GRANT EXECUTE ON [dbo].[GetUnsettledDealsPerCreditLineSettlementAndCurrency_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

ALTER PROCEDURE [dbo].[GetCreditLineCptMap_SP]
AS
BEGIN
	SELECT 
		creditLine.CreditLineId
		,creditLine.CreditLineName
		,cpt.CounterpartyCode AS Counterparty
	FROM 
		[dbo].[PrimeBrokerCreditLines_CptMapping] map
		INNER JOIN [dbo].[PrimeBrokerCreditLine] creditLine ON map.CreditLineId = creditLine.CreditLineId
		INNER JOIN [dbo].[Counterparty] cpt ON map.[CounterpartyId] = cpt.[CounterpartyId]
	ORDER BY
		creditLine.CreditLineName ASC,
		cpt.CounterpartyCode ASC
END
GO

GRANT EXECUTE ON [dbo].[GetCreditLineCptMap_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

exec sp_rename '[dbo].[PrimeBrokerCreditLine].[MaxUnsettledNopUsdTotalToTurnGFO]', 'MaxUnsettledNopUsdTotalToLockDestination'
exec sp_rename '[dbo].[PrimeBrokerCreditLine].[MaxUnsettledNopUsdPerValueDateToTurnGFO]', 'MaxUnsettledNopUsdPerValueDateToLockDestination'

exec sp_rename '[dbo].[PersistedSettlementStatisticsPerCreditLine].[MaxUnsettledNopUsdTotalToTurnGFO]', 'MaxUnsettledNopUsdTotalToLockDestination'
exec sp_rename '[dbo].[PersistedSettlementStatisticsPerCreditLine].[MaxUnsettledNopUsdPerValueDateToTurnGFO]', 'MaxUnsettledNopUsdPerValueDateToLockDestination'
GO

CREATE PROCEDURE [dbo].[GetCreditLineSettings_SP]
AS
BEGIN
	SELECT 
		creditLine.CreditLineId
		,creditLine.CreditLineName
		,creditLine.MaxUnsettledNopUsdTotal
		,creditLine.MaxUnsettledNopUsdTotalToLockDestination
		,creditLine.MaxUnsettledNopUsdPerValueDate
		,creditLine.MaxUnsettledNopUsdPerValueDateToLockDestination
	FROM 
		[dbo].[PrimeBrokerCreditLine] creditLine
	ORDER BY
		creditLine.CreditLineId ASC
END
GO

GRANT EXECUTE ON [dbo].[GetCreditLineSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

INSERT INTO [dbo].[TableChangeLog] ([TableName], [LastUpdatedUtc]) VALUES ('PrimeBrokerCreditLine', '1/1/1900')
GO

CREATE TRIGGER [dbo].[PrimeBrokerCreditLine_Updated] ON [dbo].[PrimeBrokerCreditLine] AFTER INSERT, UPDATE
AS
BEGIN
	
	UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'PrimeBrokerCreditLine'
END
GO

CREATE PROCEDURE [dbo].[GetIsCreditLineSettingsChange_SP]
(
	@IsChangeFromUtc DATETIME
)
AS
BEGIN
	SELECT CAST(CASE WHEN [LastUpdatedUtc] > @IsChangeFromUtc THEN 1 ELSE 0 END AS BIT) AS IsChange FROM [dbo].[TableChangeLog] WHERE [TableName] = 'PrimeBrokerCreditLine'
END
GO

GRANT EXECUTE ON [dbo].[GetIsCreditLineSettingsChange_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

ALTER PROCEDURE [dbo].[PrimeBrokerCreditLine_UpdateLimit_SP]
(
	@CreditLineId tinyint,
	@NewUnsettledLimitUsdTotal decimal(18,2),
	@NewUnsettledLimitUsdTotalToLockDestiantion decimal(18,2),
	@NewUnsettledLimitUsdPerValueDate decimal(18,2),
	@NewUnsettledLimitUsdPerValueDateToLockDestiantion decimal(18,2)
)
AS
BEGIN
	UPDATE 
		[dbo].[PrimeBrokerCreditLine]
	SET
		[MaxUnsettledNopUsdTotal] = @NewUnsettledLimitUsdTotal
		,[MaxUnsettledNopUsdTotalToLockDestination] = @NewUnsettledLimitUsdTotalToLockDestiantion
		,[MaxUnsettledNopUsdPerValueDate] = @NewUnsettledLimitUsdPerValueDate
		,[MaxUnsettledNopUsdPerValueDateToLockDestination] = @NewUnsettledLimitUsdPerValueDateToLockDestiantion
	WHERE
		[CreditLineId] = @CreditLineId
END
GO

CREATE TABLE [dbo].[NopCheckingZone](
	[NopCheckingZoneId] [tinyint] IDENTITY(1,1) NOT NULL,
	[NopCheckingZoneName] [varchar](20) NOT NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [NopCheckingZone_Id] ON [dbo].[NopCheckingZone]
(
	[NopCheckingZoneId] ASC
) ON [PRIMARY]
GO

CREATE UNIQUE INDEX [NopCheckingZone_Name] ON [dbo].[NopCheckingZone]
(
	[NopCheckingZoneName] ASC
) ON [PRIMARY]
GO

INSERT INTO [dbo].[NopCheckingZone] ([NopCheckingZoneName])
SELECT name FROM
(VALUES ('NoCheckingZone'), ('SimpleCheckingZone'), ('LockedZone'), ('OverTheMaxZone')) AS Names(name)
GO


CREATE TABLE [dbo].[PrimeBrokerCreditLineNopCheckingZones](
	[CreditLineId] [tinyint] NOT NULL,
	[SettlementDate] [date] NULL,
	[NopCheckingZoneId] [tinyint] NOT NULL,
) ON [Integrator_DailyData]
GO

CREATE UNIQUE CLUSTERED INDEX [PrimeBrokerCreditLineNopCheckingZones_CreditLineAndDate] ON [dbo].[PrimeBrokerCreditLineNopCheckingZones]
(
	[CreditLineId] ASC,
	[SettlementDate] ASC
) ON [Integrator_DailyData]
GO

ALTER TABLE [dbo].[PrimeBrokerCreditLineNopCheckingZones]  WITH CHECK ADD  CONSTRAINT [FK_PrimeBrokerCreditLineNopCheckingZones_CreditLineId] FOREIGN KEY([CreditLineId])
REFERENCES [dbo].[PrimeBrokerCreditLine] ([CreditLineId])
GO
ALTER TABLE [dbo].[PrimeBrokerCreditLineNopCheckingZones] CHECK CONSTRAINT [FK_PrimeBrokerCreditLineNopCheckingZones_CreditLineId]
GO


ALTER TABLE [dbo].[PrimeBrokerCreditLineNopCheckingZones]  WITH CHECK ADD  CONSTRAINT [FK_PrimeBrokerCreditLineNopCheckingZones_NopCheckingZoneId] FOREIGN KEY([NopCheckingZoneId])
REFERENCES [dbo].[NopCheckingZone] ([NopCheckingZoneId])
GO
ALTER TABLE [dbo].[PrimeBrokerCreditLineNopCheckingZones] CHECK CONSTRAINT [FK_PrimeBrokerCreditLineNopCheckingZones_NopCheckingZoneId]
GO

CREATE PROCEDURE [dbo].[UpdatePrimeBrokerCreditLineNopCheckingZones_SP]
(
	@CreditLineId [tinyint],
	@SettlementDate [date],
	@NopCheckingZoneName [varchar](20)
)
AS
BEGIN

	DECLARE @NopCheckingZoneId INT

	SELECT @NopCheckingZoneId = [NopCheckingZoneId] FROM [dbo].[NopCheckingZone] WHERE [NopCheckingZoneName] = @NopCheckingZoneName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'NopCheckingZone not found in DB: %s', 16, 2, @NopCheckingZoneName) WITH SETERROR
	END
	
	--WARNING - this doesn't solve concurrency as it expects to be executed serializable once a while
	-- errors are acceptable thought (otherwise we'd need transaction + table locking)
	
	UPDATE
		[dbo].[PrimeBrokerCreditLineNopCheckingZones]
	SET
		[NopCheckingZoneId] = @NopCheckingZoneId
	WHERE
		[CreditLineId] = @CreditLineId
		AND (
			[SettlementDate] = @SettlementDate 
			OR 
			([SettlementDate] IS NULL AND @SettlementDate IS NULL)
			)
			
	IF @@rowcount = 0
	BEGIN
		INSERT INTO [dbo].[PrimeBrokerCreditLineNopCheckingZones] 
			([CreditLineId],
			[SettlementDate],
			[NopCheckingZoneId])
		VALUES
			(@CreditLineId,
			@SettlementDate,
			@NopCheckingZoneId)
	END
END
GO

GRANT EXECUTE ON [dbo].[UpdatePrimeBrokerCreditLineNopCheckingZones_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

ALTER PROCEDURE [dbo].[Daily_PurgeAndRollupUnsettledDeals_SP]
AS
BEGIN

	DECLARE @temp_unsettledDeals TABLE
	(
		[CounterpartyId] [tinyint],
		[SymbolId] [tinyint],
		[SettlementDateLocalMkt] [date],
		[SettlementTimeUtc] [DateTime],
		[SizeBasePol] [decimal](18, 2),
		[SizeTermPol] [decimal](18, 4),
		[DealsCount] [int]
	)

	INSERT INTO
		@temp_unsettledDeals
		(
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SettlementTimeUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		)
	SELECT 
		[CounterpartyId]
		,[SymbolId]
		,[SettlementDateLocalMkt]
		,[SettlementTimeUtc]
		,SUM([SizeBasePol]) AS [NopBasePol]
		,SUM([SizeTermPol]) AS [NopTermPol]
		,SUM([DealsCount]) AS [DealsCount]
	FROM
		[dbo].[DealExternalExecutedUnsettled] unsettled				
	WHERE
		[SettlementTimeUtc] > GetUtcDate()
	GROUP BY
		[SettlementTimeUtc],
		[SettlementDateLocalMkt],
		[CounterpartyId],
		[SymbolId]
		
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
	
		INSERT INTO
			[dbo].[DealExternalExecutedUnsettled]
			(
				[CounterpartyId],
				[SymbolId],
				[SettlementDateLocalMkt],
				[SettlementTimeUtc],
				[SizeBasePol],
				[SizeTermPol],
				[DealsCount]
			)
		SELECT
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SettlementTimeUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		FROM
			@temp_unsettledDeals
	COMMIT	

	--And purge old settlement date zone infos
	DELETE FROM [dbo].[PrimeBrokerCreditLineNopCheckingZones] WHERE [SettlementDate] < (SELECT MIN([SettlementDate]) FROM [dbo].[LastCurrentlyUnsettledSettlementDate])
END
GO


DROP TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine]
GO

CREATE TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine](
	[PrimeBrokerName] [varchar](10) NOT NULL,
	[CreditLineName] [varchar](255) NOT NULL,
	[CreditLineId] [tinyint] NOT NULL,
	[UnsettledNopUsdTotal] [decimal](18, 2) NULL,
	[HighestUnsettledNopInSingleT] [decimal](18, 2) NULL,
	[DealsCountTotal] [int] NULL,
	[Zone_Total] [varchar](20) NULL,
	[MaxUnsettledNopUsdTotal] [decimal](18, 2) NOT NULL,
	[MaxUnsettledNopUsdTotalToLockDestination] [decimal](18, 2) NOT NULL,
	[MaxUnsettledNopUsdPerValueDate] [decimal](18, 2) NOT NULL,
	[MaxUnsettledNopUsdPerValueDateToLockDestination] [decimal](18, 2) NOT NULL,
	[T1] [date] NULL,
	[T2] [date] NULL,
	[T3] [date] NULL,
	[T4] [date] NULL,
	[T5] [date] NULL,
	[T6] [date] NULL,
	[T7] [date] NULL,
	[T8] [date] NULL,
	[UnsettledNopUsd_T1] [decimal](18, 2) NULL,
	[UnsettledNopUsd_T2] [decimal](18, 2) NULL,
	[UnsettledNopUsd_T3] [decimal](18, 2) NULL,
	[UnsettledNopUsd_T4] [decimal](18, 2) NULL,
	[UnsettledNopUsd_T5] [decimal](18, 2) NULL,
	[UnsettledNopUsd_T6] [decimal](18, 2) NULL,
	[UnsettledNopUsd_T7] [decimal](18, 2) NULL,
	[UnsettledNopUsd_T8] [decimal](18, 2) NULL,
	[DealsCount_T1] [int] NULL,
	[DealsCount_T2] [int] NULL,
	[DealsCount_T3] [int] NULL,
	[DealsCount_T4] [int] NULL,
	[DealsCount_T5] [int] NULL,
	[DealsCount_T6] [int] NULL,
	[DealsCount_T7] [int] NULL,
	[DealsCount_T8] [int] NULL,
	[Zone_T1] [varchar](20) NULL,
	[Zone_T2] [varchar](20) NULL,
	[Zone_T3] [varchar](20) NULL,
	[Zone_T4] [varchar](20) NULL,
	[Zone_T5] [varchar](20) NULL,
	[Zone_T6] [varchar](20) NULL,
	[Zone_T7] [varchar](20) NULL,
	[Zone_T8] [varchar](20) NULL
) ON [Integrator_DailyData]
GO

GRANT ALTER ON [dbo].[PersistedSettlementStatisticsPerCreditLine] TO [IntegratorServiceAccount] AS [dbo]
GRANT SELECT ON [dbo].[PersistedSettlementStatisticsPerCreditLine] TO [IntegratorServiceAccount] AS [dbo]
GRANT ALTER ON [dbo].[PersistedSettlementStatisticsPerCreditLine] TO [IntegratorKillSwitchUser] AS [dbo]
GRANT SELECT ON [dbo].[PersistedSettlementStatisticsPerCreditLine] TO [IntegratorKillSwitchUser] AS [dbo]
GO


ALTER PROCEDURE [dbo].[GetSettlementStatisticsPerCreditLine_Cached_SP]
(
	@Force BIT = 0
)
AS
BEGIN

	exec UpdateSettlementStatisticsPerCreditLine_SP @Force=@Force
	
	SELECT 
		[PrimeBrokerName]
		,[CreditLineName]
		,[CreditLineId]
		,[UnsettledNopUsdTotal]
		,[HighestUnsettledNopInSingleT]
		,[DealsCountTotal]
		,[Zone_Total]
		,[MaxUnsettledNopUsdTotal]
		,[MaxUnsettledNopUsdTotalToLockDestination]
		,[MaxUnsettledNopUsdPerValueDate]
		,[MaxUnsettledNopUsdPerValueDateToLockDestination]
		,[T1]
		,[T2]
		,[T3]
		,[T4]
		,[T5]
		,[T6]
		,[T7]
		,[T8]
		,[UnsettledNopUsd_T1]
		,[UnsettledNopUsd_T2]
		,[UnsettledNopUsd_T3]
		,[UnsettledNopUsd_T4]
		,[UnsettledNopUsd_T5]
		,[UnsettledNopUsd_T6]
		,[UnsettledNopUsd_T7]
		,[UnsettledNopUsd_T8]
		,[DealsCount_T1]
		,[DealsCount_T2]
		,[DealsCount_T3]
		,[DealsCount_T4]
		,[DealsCount_T5]
		,[DealsCount_T6]
		,[DealsCount_T7]
		,[DealsCount_T8]	
		,[Zone_T1]
		,[Zone_T2]
		,[Zone_T3]
		,[Zone_T4]
		,[Zone_T5]
		,[Zone_T6]
		,[Zone_T7]
		,[Zone_T8]
	FROM 
		[dbo].[PersistedSettlementStatisticsPerCreditLine]
	ORDER BY
		[PrimeBrokerName] ASC
		,[CreditLineName] ASC
END
GO



ALTER PROCEDURE [dbo].[UpdateSettlementStatisticsPerCreditLine_SP]
(
	@Force BIT = 0
)
AS
BEGIN

	UPDATE 
		[dbo].[TableChangeLog] 
	SET 
		[LastUpdatedUtc] = GETUTCDATE() 
	WHERE 
		[TableName] = 'SettlementStatistics' 
		AND 
		(@Force = 1 OR [LastUpdatedUtc] < DATEADD(SECOND, -2, GETUTCDATE()))

	IF @@ROWCOUNT > 0
	BEGIN
	
		DECLARE @temp_final_result TABLE
		(
			[PrimeBrokerName] [varchar](10),
			[CreditLineName] [varchar](255),
			[CreditLineId] [tinyint],
			[UnsettledNopUsdTotal] [decimal](18, 2),
			[HighestUnsettledNopInSingleT] [decimal](18, 2),
			[DealsCountTotal] [int],
			[Zone_Total] [varchar](20),
			[MaxUnsettledNopUsdTotal] [decimal](18, 2),
			[MaxUnsettledNopUsdTotalToLockDestination] [decimal](18, 2),
			[MaxUnsettledNopUsdPerValueDate] [decimal](18, 2),
			[MaxUnsettledNopUsdPerValueDateToLockDestination] [decimal](18, 2),
			[T1] [date],
			[T2] [date],
			[T3] [date],
			[T4] [date],
			[T5] [date],
			[T6] [date],
			[T7] [date],
			[T8] [date],
			[UnsettledNopUsd_T1] [decimal](18, 2),
			[UnsettledNopUsd_T2] [decimal](18, 2),
			[UnsettledNopUsd_T3] [decimal](18, 2),
			[UnsettledNopUsd_T4] [decimal](18, 2),
			[UnsettledNopUsd_T5] [decimal](18, 2),
			[UnsettledNopUsd_T6] [decimal](18, 2),
			[UnsettledNopUsd_T7] [decimal](18, 2),
			[UnsettledNopUsd_T8] [decimal](18, 2),
			[DealsCount_T1] [int],
			[DealsCount_T2] [int],
			[DealsCount_T3] [int],
			[DealsCount_T4] [int],
			[DealsCount_T5] [int],
			[DealsCount_T6] [int],
			[DealsCount_T7] [int],
			[DealsCount_T8] [int],
			[Zone_T1] [varchar](20),
			[Zone_T2] [varchar](20),
			[Zone_T3] [varchar](20),
			[Zone_T4] [varchar](20),
			[Zone_T5] [varchar](20),
			[Zone_T6] [varchar](20),
			[Zone_T7] [varchar](20),
			[Zone_T8] [varchar](20)
		)
		
		
		DECLARE @temp_NOPs_pol TABLE
		(
			CreditLineId tinyint,
			BaseCurrencyId smallint,
			QuoteCurrencyId smallint,
			NopBasePol decimal(18,2),
			NopTermPol decimal(18,4),
			[DealsCount] int,
			[ValueDate] Date
		)
		
		DECLARE @temp_intermediate_result TABLE
		(
			CreditLineId tinyint,
			[UnsettledNopUsd] [decimal](18, 2),
			[DealsCount] [int],
			[ValueDate] Date
		)

		INSERT INTO
			@temp_NOPs_pol
			(
				CreditLineId,
				BaseCurrencyId,
				QuoteCurrencyId,
				NopBasePol,
				NopTermPol,
				DealsCount,
				ValueDate
			)
		SELECT 
			[CreditLineId]
			,MAX(sym.BaseCurrencyId) AS BaseCurrencyId
			,MAX(sym.QuoteCurrencyId) AS QuoteCurrencyId
			,SUM([SizeBasePol]) AS [NopBasePol]
			,SUM([SizeTermPol]) AS [NopTermPol]
			,SUM(DealsCount) AS [DealsCount]
			,SettlementDateLocalMkt AS ValueDate
			--,COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate]) AS [SettlementDate]
		FROM
			[dbo].[DealExternalExecutedUnsettled] unsettled
			INNER JOIN [dbo].[PrimeBrokerCreditLines_CptMapping] creditLineMap ON unsettled.[CounterpartyId] = creditLineMap.[CounterpartyId]
			INNER JOIN Symbol sym ON sym.ID = unsettled.[SymbolId]				
		WHERE
			[SettlementTimeUtc] > GetUtcDate()
		GROUP BY
			[CreditLineId],
			[SymbolId],
			[SettlementDateLocalMkt]

		
		INSERT INTO
			@temp_intermediate_result
			(
				[CreditLineId],
				[UnsettledNopUsd],
				[DealsCount],
				[ValueDate]
			)
			SELECT
				CreditLineId,
				SUM(ABS(NopPolInUSD))/2 AS NopAbsInUSD,
				SUM(DealsCount) AS DealsCount,
				ValueDate
			FROM
				(
				SELECT
					CreditLineId,
					nopsUnaggreggated.CurrencyId,
					SUM(NopPol * ccy.LastKnownUsdConversionMultiplier) AS NopPolInUSD,
					SUM(DealsCount) AS DealsCount,
					ValueDate
				FROM
					(
					SELECT
						CreditLineId,
						BaseCurrencyId AS CurrencyId,
						NopBasePol AS NopPol,
						DealsCount AS DealsCount,
						ValueDate
					FROM
						@temp_NOPs_pol basePos

					UNION ALL

					SELECT
						CreditLineId,
						QuoteCurrencyId AS CurrencyId,
						NopTermPol AS NopPol,
						0 AS DealsCount,
						ValueDate
					FROM
						@temp_NOPs_pol basePos
					) nopsUnaggreggated
					INNER JOIN Currency ccy ON ccy.CurrencyID = nopsUnaggreggated.CurrencyId
				GROUP BY
					CreditLineId,
					nopsUnaggreggated.CurrencyId,
					ValueDate
				)nopsAggreggated
			GROUP BY
				CreditLineId,
				ValueDate

			DECLARE @temp_ValueDates TABLE
			(
				ValueDate Date,
				ValueDateId int
			)

			INSERT INTO @temp_ValueDates(ValueDateId, ValueDate)
			SELECT
				ROW_NUMBER() OVER (ORDER BY ValueDate),
				ValueDate
			FROM
				@temp_intermediate_result
			WHERE
				ValueDate IS NOT NULL
			GROUP BY
				ValueDate


			
			;WITH TmpStats AS
			(
				SELECT 
					CreditLineId,
					UnsettledNopUsd,
					DealsCount,
					res.ValueDate,
					'Nop_T' + CAST(ValueDateId AS VARCHAR) AS NopsValueDateTag,
					'Deals_T' + CAST(ValueDateId AS VARCHAR) AS DealsValueDateTag
			FROM
				@temp_intermediate_result res
				FULL OUTER JOIN @temp_ValueDates valDates ON res.ValueDate = valDates.ValueDate
			)

			 INSERT INTO @temp_final_result
			(
				[PrimeBrokerName]
				,[CreditLineName]
				,[CreditLineId]
				,[UnsettledNopUsdTotal]
				,[HighestUnsettledNopInSingleT]
				,[DealsCountTotal]
				,[Zone_Total]
				,[MaxUnsettledNopUsdTotal]
				,[MaxUnsettledNopUsdTotalToLockDestination]
				,[MaxUnsettledNopUsdPerValueDate]
				,[MaxUnsettledNopUsdPerValueDateToLockDestination]
				,[T1]
				,[T2]
				,[T3]
				,[T4]
				,[T5]
				,[T6]
				,[T7]
				,[T8]
				,[UnsettledNopUsd_T1]
				,[UnsettledNopUsd_T2]
				,[UnsettledNopUsd_T3]
				,[UnsettledNopUsd_T4]
				,[UnsettledNopUsd_T5]
				,[UnsettledNopUsd_T6]
				,[UnsettledNopUsd_T7]
				,[UnsettledNopUsd_T8]
				,[DealsCount_T1]
				,[DealsCount_T2]
				,[DealsCount_T3]
				,[DealsCount_T4]
				,[DealsCount_T5]
				,[DealsCount_T6]
				,[DealsCount_T7]
				,[DealsCount_T8]
				,[Zone_T1]
				,[Zone_T2]
				,[Zone_T3]
				,[Zone_T4]
				,[Zone_T5]
				,[Zone_T6]
				,[Zone_T7]
				,[Zone_T8]
			)

			SELECT
				primeBroker.PrimeBrokerName,
				creditLine.[CreditLineName],
				creditLine.[CreditLineId],
				UnsettledNopUsdTotal,
				(SELECT MAX(x) FROM (VALUES(Nop_T1), (Nop_T2), (Nop_T3), (Nop_T4), (Nop_T5), (Nop_T6), (Nop_T7), (Nop_T8)) AS VALUE(x)) AS [HighestUnsettledNopInSingleT],
				DealsCountTotal,
				Zone_Total,
				creditLine.[MaxUnsettledNopUsdTotal],
				creditLine.[MaxUnsettledNopUsdTotalToLockDestination],
				creditLine.[MaxUnsettledNopUsdPerValueDate],
				creditLine.[MaxUnsettledNopUsdPerValueDateToLockDestination],
				T1, T2, T3, T4, T5, T6, T7, T8,
				Nop_T1, Nop_T2, Nop_T3, Nop_T4, Nop_T5, Nop_T6, Nop_T7, Nop_T8, 
				Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8,
				Zone_T1, Zone_T2, Zone_T3, Zone_T4, Zone_T5, Zone_T6, Zone_T7, Zone_T8
			FROM

				(
					select T1, T2, T3, T4, T5, T6, T7, T8
					from
					(
						select ValueDate, 'T' + CAST(ValueDateId AS VARCHAR) AS ValueDateTag
						FROM @temp_ValueDates
					) d
					PIVOT
					(
						MAX(ValueDate)
						for ValueDateTag IN (T1, T2, T3, T4, T5, T6, T7, T8)
					) pivDates
				)ValDates

				INNER JOIN [dbo].[PrimeBrokerCreditLine] creditLine ON 1=1
				INNER JOIN [dbo].[PrimeBroker] primeBroker ON creditLine.[PrimeBrokerId] = primeBroker.[PrimeBrokerId]

				LEFT JOIN

				(
				SELECT 
					CreditLineId,
					MAX(Nop_T1) AS Nop_T1, MAX(Nop_T2) AS Nop_T2, MAX(Nop_T3) AS Nop_T3, MAX(Nop_T4) AS Nop_T4, MAX(Nop_T5) AS Nop_T5, MAX(Nop_T6) AS Nop_T6, MAX(Nop_T7) AS Nop_T7, MAX(Nop_T8) AS Nop_T8
				FROM
					TmpStats
					PIVOT 
					(
						MAX(UnsettledNopUsd) FOR NopsValueDateTag IN (Nop_T1, Nop_T2, Nop_T3, Nop_T4, Nop_T5, Nop_T6, Nop_T7, Nop_T8)
					)pivNops
				GROUP BY
					CreditLineId
				)NopsTmp ON creditLine.CreditLineId = NopsTmp.CreditLineId

				LEFT JOIN

				(
				SELECT 
					CreditLineId,
					MAX(Deals_T1) AS Deals_T1, MAX(Deals_T2) AS Deals_T2, MAX(Deals_T3) AS Deals_T3, MAX(Deals_T4) AS Deals_T4, MAX(Deals_T5) AS Deals_T5, MAX(Deals_T6) AS Deals_T6, MAX(Deals_T7) AS Deals_T7, MAX(Deals_T8) AS Deals_T8
				FROM
					TmpStats
					PIVOT 
					(
						MAX(DealsCount) FOR DealsValueDateTag IN (Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8)
					)piv
				GROUP BY
					CreditLineId
				)DealsTmp

				ON NopsTmp.CreditLineId = DealsTmp.CreditLineId

				LEFT JOIN

				(
				SELECT
					CreditLineId,
					SUM(UnsettledNopUsd) AS UnsettledNopUsdTotal,
					SUM(DealsCount) AS DealsCountTotal
				FROM
					TmpStats
				GROUP BY
					CreditLineId
				) totalSum

				ON totalSum.CreditLineId = DealsTmp.CreditLineId
				
				LEFT JOIN
				
				(
				SELECT
					[CreditLineId],
					Zone_Total, Zone_T1, Zone_T2, Zone_T3, Zone_T4, Zone_T5, Zone_T6, Zone_T7, Zone_T8
				FROM
					(
					SELECT 
						[CreditLineId]
						,'Zone_T' + COALESCE(CAST(valDate.ValueDateId AS varchar), 'otal') AS ZoneValueDateTag
						,zoneName.[NopCheckingZoneName] AS ZoneName
					FROM 
						[dbo].[PrimeBrokerCreditLineNopCheckingZones] zone
						INNER JOIN [dbo].[NopCheckingZone] zoneName ON zone.[NopCheckingZoneId] = zoneName.[NopCheckingZoneId]
						LEFT JOIN @temp_ValueDates valDate ON zone.[SettlementDate] = valDate.ValueDate
					--be carefull not to join old settlement dates that are not in @temp_ValueDates
					WHERE
						zone.[SettlementDate] IS NULL AND valDate.ValueDate IS NULL
						OR
						zone.[SettlementDate] IS NOT NULL AND valDate.ValueDate IS NOT NULL
					) ZoneStats
					PIVOT
					(
						MAX(ZoneStats.ZoneName) FOR ZoneValueDateTag IN (Zone_Total, Zone_T1, Zone_T2, Zone_T3, Zone_T4, Zone_T5, Zone_T6, Zone_T7, Zone_T8)
					) piv
				) zoneInfo

				ON totalSum.CreditLineId = zoneInfo.CreditLineId

			ORDER BY
				primeBroker.PrimeBrokerName,
				creditLine.[CreditLineName]
			
		BEGIN TRANSACTION
			TRUNCATE TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine]
		
			INSERT INTO
				[dbo].[PersistedSettlementStatisticsPerCreditLine]
				(
					[PrimeBrokerName]
					,[CreditLineName]
					,[CreditLineId]
					,[UnsettledNopUsdTotal]
					,[HighestUnsettledNopInSingleT]
					,[DealsCountTotal]
					,[Zone_Total]
					,[MaxUnsettledNopUsdTotal]
					,[MaxUnsettledNopUsdTotalToLockDestination]
					,[MaxUnsettledNopUsdPerValueDate]
					,[MaxUnsettledNopUsdPerValueDateToLockDestination]
					,[T1]
					,[T2]
					,[T3]
					,[T4]
					,[T5]
					,[T6]
					,[T7]
					,[T8]
					,[UnsettledNopUsd_T1]
					,[UnsettledNopUsd_T2]
					,[UnsettledNopUsd_T3]
					,[UnsettledNopUsd_T4]
					,[UnsettledNopUsd_T5]
					,[UnsettledNopUsd_T6]
					,[UnsettledNopUsd_T7]
					,[UnsettledNopUsd_T8]
					,[DealsCount_T1]
					,[DealsCount_T2]
					,[DealsCount_T3]
					,[DealsCount_T4]
					,[DealsCount_T5]
					,[DealsCount_T6]
					,[DealsCount_T7]
					,[DealsCount_T8]
					,[Zone_T1]
					,[Zone_T2]
					,[Zone_T3]
					,[Zone_T4]
					,[Zone_T5]
					,[Zone_T6]
					,[Zone_T7]
					,[Zone_T8]
				)
			SELECT
				[PrimeBrokerName]
				,[CreditLineName]
				,[CreditLineId]
				,[UnsettledNopUsdTotal]
				,[HighestUnsettledNopInSingleT]
				,[DealsCountTotal]
				,[Zone_Total]
				,[MaxUnsettledNopUsdTotal]
				,[MaxUnsettledNopUsdTotalToLockDestination]
				,[MaxUnsettledNopUsdPerValueDate]
				,[MaxUnsettledNopUsdPerValueDateToLockDestination]
				,[T1]
				,[T2]
				,[T3]
				,[T4]
				,[T5]
				,[T6]
				,[T7]
				,[T8]
				,[UnsettledNopUsd_T1]
				,[UnsettledNopUsd_T2]
				,[UnsettledNopUsd_T3]
				,[UnsettledNopUsd_T4]
				,[UnsettledNopUsd_T5]
				,[UnsettledNopUsd_T6]
				,[UnsettledNopUsd_T7]
				,[UnsettledNopUsd_T8]
				,[DealsCount_T1]
				,[DealsCount_T2]
				,[DealsCount_T3]
				,[DealsCount_T4]
				,[DealsCount_T5]
				,[DealsCount_T6]
				,[DealsCount_T7]
				,[DealsCount_T8]
				,[Zone_T1]
				,[Zone_T2]
				,[Zone_T3]
				,[Zone_T4]
				,[Zone_T5]
				,[Zone_T6]
				,[Zone_T7]
				,[Zone_T8]
			FROM
				@temp_final_result
		COMMIT	

	END
END 
GO

---- After 2015-10-26 --------

DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd]
GO


CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd] AS N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RiskManagementSettings" nillable="true" type="RiskManagementSettings" />
  <xs:complexType name="RiskManagementSettings">
    <xs:sequence>
      <xs:element name="ConfigLastUpdated"  nillable="true" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="DisallowTradingOnInstances" type="ArrayOfString" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsRate" type="FatalErrorsRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAndPosition" type="PriceAndPositionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Instance" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersSubmissionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FatalErrorsRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrderMaximumAllowedSizeSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedBaseAbsSizeInUsd" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="PriceAndPositionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalUnsettledNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumExternalNOPInUSD" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceDeviationCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="OnlinePriceDeviationCheckMaxPriceAge_Minutes" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="SymbolTrustworthyCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumSymbolUntrustworthyPeriod_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="DefaultNewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="Symbols" type="ArrayOfSymbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbol">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="Symbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="Symbol">
    <xs:attribute name="ShortName" type="xs:string" />
    <xs:attribute name="NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
  </xs:complexType>
</xs:schema>'
GO

-- Manually - add <ExternalUnsettledNOPRiskCheckAllowed>true</ExternalUnsettledNOPRiskCheckAllowed>

CREATE TABLE [dbo].[PersistedSettlementStatisticsPerSymbol](

	[SymbolName] char(7) NOT NULL,
	[T1] [date] NULL,
	[T2] [date] NULL,
	[T3] [date] NULL,
	[T4] [date] NULL,
	[T5] [date] NULL,
	[T6] [date] NULL,
	[T7] [date] NULL,
	[T8] [date] NULL,
	[UnsettledNopNominal_T1] [decimal](18, 2) NULL,
	[UnsettledNopNominal_T2] [decimal](18, 2) NULL,
	[UnsettledNopNominal_T3] [decimal](18, 2) NULL,
	[UnsettledNopNominal_T4] [decimal](18, 2) NULL,
	[UnsettledNopNominal_T5] [decimal](18, 2) NULL,
	[UnsettledNopNominal_T6] [decimal](18, 2) NULL,
	[UnsettledNopNominal_T7] [decimal](18, 2) NULL,
	[UnsettledNopNominal_T8] [decimal](18, 2) NULL,
	[DealsCount_T1] [int] NULL,
	[DealsCount_T2] [int] NULL,
	[DealsCount_T3] [int] NULL,
	[DealsCount_T4] [int] NULL,
	[DealsCount_T5] [int] NULL,
	[DealsCount_T6] [int] NULL,
	[DealsCount_T7] [int] NULL,
	[DealsCount_T8] [int] NULL
) ON [Integrator_DailyData]
GO

GRANT ALTER ON [dbo].[PersistedSettlementStatisticsPerSymbol] TO [IntegratorServiceAccount] AS [dbo]
GRANT SELECT ON [dbo].[PersistedSettlementStatisticsPerSymbol] TO [IntegratorServiceAccount] AS [dbo]
GRANT ALTER ON [dbo].[PersistedSettlementStatisticsPerSymbol] TO [IntegratorKillSwitchUser] AS [dbo]
GRANT SELECT ON [dbo].[PersistedSettlementStatisticsPerSymbol] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[UpdateSettlementStatisticsPerSymbol_Internal_SP]
AS
BEGIN

	DECLARE @temp_finalSymbol_result TABLE
	(
		[SymbolName] char(7),
		[T1] [date],
		[T2] [date],
		[T3] [date],
		[T4] [date],
		[T5] [date],
		[T6] [date],
		[T7] [date],
		[T8] [date],
		[UnsettledNopNominal_T1] [decimal](18, 2),
		[UnsettledNopNominal_T2] [decimal](18, 2),
		[UnsettledNopNominal_T3] [decimal](18, 2),
		[UnsettledNopNominal_T4] [decimal](18, 2),
		[UnsettledNopNominal_T5] [decimal](18, 2),
		[UnsettledNopNominal_T6] [decimal](18, 2),
		[UnsettledNopNominal_T7] [decimal](18, 2),
		[UnsettledNopNominal_T8] [decimal](18, 2),
		[DealsCount_T1] [int],
		[DealsCount_T2] [int],
		[DealsCount_T3] [int],
		[DealsCount_T4] [int],
		[DealsCount_T5] [int],
		[DealsCount_T6] [int],
		[DealsCount_T7] [int],
		[DealsCount_T8] [int]
	)

	DECLARE @temp_symbol_result TABLE
	(
		SymbolId tinyint,
		[UnsettledNopNominal] [decimal](18, 2),
		[DealsCount] [int],
		[ValueDate] Date
	)

	INSERT INTO
	@temp_symbol_result
	(
		SymbolId,
		[UnsettledNopNominal],
		DealsCount,
		ValueDate
	)
	SELECT
		SymbolId,
		SUM(SizeBasePol) AS [UnsettledNopNominal],
		SUM(DealsCount),
		SettlementDateLocalMkt AS ValueDate
	FROM
		[dbo].[DealExternalExecutedUnsettled] unsettled
	WHERE
		[SettlementTimeUtc] > GetUtcDate()
	GROUP BY
		SettlementDateLocalMkt,
		SymbolId

	DECLARE @temp_ValueDates TABLE
	(
		ValueDate Date,
		ValueDateId int
	)

	INSERT INTO @temp_ValueDates(ValueDateId, ValueDate)
	SELECT
		ROW_NUMBER() OVER (ORDER BY ValueDate),
		ValueDate
	FROM
		@temp_symbol_result
	WHERE
		ValueDate IS NOT NULL
	GROUP BY
		ValueDate


	;WITH TmpStats AS
	(
		SELECT 
			SymbolId,
			[UnsettledNopNominal],
			DealsCount,
			res.ValueDate,
			'Nop_T' + CAST(ValueDateId AS VARCHAR) AS NopsValueDateTag,
			'Deals_T' + CAST(ValueDateId AS VARCHAR) AS DealsValueDateTag
	FROM
		@temp_symbol_result res
		FULL OUTER JOIN @temp_ValueDates valDates ON res.ValueDate = valDates.ValueDate
	)

		INSERT INTO @temp_finalSymbol_result
		(
		SymbolName,
		[T1],
		[T2],
		[T3],
		[T4],
		[T5],
		[T6],
		[T7],
		[T8],
		[UnsettledNopNominal_T1],
		[UnsettledNopNominal_T2],
		[UnsettledNopNominal_T3],
		[UnsettledNopNominal_T4],
		[UnsettledNopNominal_T5],
		[UnsettledNopNominal_T6],
		[UnsettledNopNominal_T7],
		[UnsettledNopNominal_T8],
		[DealsCount_T1],
		[DealsCount_T2],
		[DealsCount_T3],
		[DealsCount_T4],
		[DealsCount_T5],
		[DealsCount_T6],
		[DealsCount_T7],
		[DealsCount_T8]
		)
		SELECT
		SymbolName,
		[T1],
		[T2],
		[T3],
		[T4],
		[T5],
		[T6],
		[T7],
		[T8],
		[Nop_T1],
		[Nop_T2],
		[Nop_T3],
		[Nop_T4],
		[Nop_T5],
		[Nop_T6],
		[Nop_T7],
		[Nop_T8],
		[Deals_T1],
		[Deals_T2],
		[Deals_T3],
		[Deals_T4],
		[Deals_T5],
		[Deals_T6],
		[Deals_T7],
		[Deals_T8]
	FROM
					
		(
			SELECT
				SymbolId,
				MAX(sym.Name) AS SymbolName
			FROM
				@temp_symbol_result res
				INNER JOIN [dbo].[Symbol_Internal] sym ON res.SymbolId = sym.Id
			GROUP BY
				SymbolId
		) executedSymbols

		INNER JOIN

		(
			select T1, T2, T3, T4, T5, T6, T7, T8
			from
			(
				select ValueDate, 'T' + CAST(ValueDateId AS VARCHAR) AS ValueDateTag
				FROM @temp_ValueDates
			) d
			PIVOT
			(
				MAX(ValueDate)
				for ValueDateTag IN (T1, T2, T3, T4, T5, T6, T7, T8)
			) pivDates
		)ValDates ON 1=1
					
		LEFT JOIN

		(
		SELECT 
			SymbolId,
			MAX(Nop_T1) AS Nop_T1, MAX(Nop_T2) AS Nop_T2, MAX(Nop_T3) AS Nop_T3, MAX(Nop_T4) AS Nop_T4, MAX(Nop_T5) AS Nop_T5, MAX(Nop_T6) AS Nop_T6, MAX(Nop_T7) AS Nop_T7, MAX(Nop_T8) AS Nop_T8
		FROM
			TmpStats
			PIVOT 
			(
				MAX(UnsettledNopNominal) FOR NopsValueDateTag IN (Nop_T1, Nop_T2, Nop_T3, Nop_T4, Nop_T5, Nop_T6, Nop_T7, Nop_T8)
			)pivNops
		GROUP BY
			SymbolId
		)NopsTmp ON executedSymbols.SymbolId = NopsTmp.SymbolId

		LEFT JOIN

		(
		SELECT 
			SymbolId,
			MAX(Deals_T1) AS Deals_T1, MAX(Deals_T2) AS Deals_T2, MAX(Deals_T3) AS Deals_T3, MAX(Deals_T4) AS Deals_T4, MAX(Deals_T5) AS Deals_T5, MAX(Deals_T6) AS Deals_T6, MAX(Deals_T7) AS Deals_T7, MAX(Deals_T8) AS Deals_T8
		FROM
			TmpStats
			PIVOT 
			(
				MAX(DealsCount) FOR DealsValueDateTag IN (Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8)
			)piv
		GROUP BY
			SymbolId
		)DealsTmp

		ON NopsTmp.SymbolId = DealsTmp.SymbolId

		ORDER BY
			SymbolName
		
			
		BEGIN TRANSACTION
			TRUNCATE TABLE [dbo].[PersistedSettlementStatisticsPerSymbol]
		
			INSERT INTO
				[dbo].[PersistedSettlementStatisticsPerSymbol]
				(
					[SymbolName],
					[T1],
					[T2],
					[T3],
					[T4],
					[T5],
					[T6],
					[T7],
					[T8],
					[UnsettledNopNominal_T1],
					[UnsettledNopNominal_T2],
					[UnsettledNopNominal_T3],
					[UnsettledNopNominal_T4],
					[UnsettledNopNominal_T5],
					[UnsettledNopNominal_T6],
					[UnsettledNopNominal_T7],
					[UnsettledNopNominal_T8],
					[DealsCount_T1],
					[DealsCount_T2],
					[DealsCount_T3],
					[DealsCount_T4],
					[DealsCount_T5],
					[DealsCount_T6],
					[DealsCount_T7],
					[DealsCount_T8]
				)
			SELECT
				[SymbolName],
				[T1],
				[T2],
				[T3],
				[T4],
				[T5],
				[T6],
				[T7],
				[T8],
				[UnsettledNopNominal_T1],
				[UnsettledNopNominal_T2],
				[UnsettledNopNominal_T3],
				[UnsettledNopNominal_T4],
				[UnsettledNopNominal_T5],
				[UnsettledNopNominal_T6],
				[UnsettledNopNominal_T7],
				[UnsettledNopNominal_T8],
				[DealsCount_T1],
				[DealsCount_T2],
				[DealsCount_T3],
				[DealsCount_T4],
				[DealsCount_T5],
				[DealsCount_T6],
				[DealsCount_T7],
				[DealsCount_T8]
			FROM
				@temp_finalSymbol_result
		COMMIT
END 
GO

GRANT EXECUTE ON [dbo].[UpdateSettlementStatisticsPerSymbol_Internal_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[UpdateSettlementStatisticsPerSymbol_Internal_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROCEDURE [dbo].[UpdateSettlementStatisticsPerCreditLine_SP]
(
	@Force BIT = 0
)
AS
BEGIN

	UPDATE 
		[dbo].[TableChangeLog] 
	SET 
		[LastUpdatedUtc] = GETUTCDATE() 
	WHERE 
		[TableName] = 'SettlementStatistics' 
		AND 
		(@Force = 1 OR [LastUpdatedUtc] < DATEADD(SECOND, -2, GETUTCDATE()))

	IF @@ROWCOUNT > 0
	BEGIN
	
		DECLARE @temp_final_result TABLE
		(
			[PrimeBrokerName] [varchar](10),
			[CreditLineName] [varchar](255),
			[CreditLineId] [tinyint],
			[UnsettledNopUsdTotal] [decimal](18, 2),
			[HighestUnsettledNopInSingleT] [decimal](18, 2),
			[DealsCountTotal] [int],
			[Zone_Total] [varchar](20),
			[MaxUnsettledNopUsdTotal] [decimal](18, 2),
			[MaxUnsettledNopUsdTotalToLockDestination] [decimal](18, 2),
			[MaxUnsettledNopUsdPerValueDate] [decimal](18, 2),
			[MaxUnsettledNopUsdPerValueDateToLockDestination] [decimal](18, 2),
			[T1] [date],
			[T2] [date],
			[T3] [date],
			[T4] [date],
			[T5] [date],
			[T6] [date],
			[T7] [date],
			[T8] [date],
			[UnsettledNopUsd_T1] [decimal](18, 2),
			[UnsettledNopUsd_T2] [decimal](18, 2),
			[UnsettledNopUsd_T3] [decimal](18, 2),
			[UnsettledNopUsd_T4] [decimal](18, 2),
			[UnsettledNopUsd_T5] [decimal](18, 2),
			[UnsettledNopUsd_T6] [decimal](18, 2),
			[UnsettledNopUsd_T7] [decimal](18, 2),
			[UnsettledNopUsd_T8] [decimal](18, 2),
			[DealsCount_T1] [int],
			[DealsCount_T2] [int],
			[DealsCount_T3] [int],
			[DealsCount_T4] [int],
			[DealsCount_T5] [int],
			[DealsCount_T6] [int],
			[DealsCount_T7] [int],
			[DealsCount_T8] [int],
			[Zone_T1] [varchar](20),
			[Zone_T2] [varchar](20),
			[Zone_T3] [varchar](20),
			[Zone_T4] [varchar](20),
			[Zone_T5] [varchar](20),
			[Zone_T6] [varchar](20),
			[Zone_T7] [varchar](20),
			[Zone_T8] [varchar](20)
		)
		
		
		DECLARE @temp_NOPs_pol TABLE
		(
			CreditLineId tinyint,
			BaseCurrencyId smallint,
			QuoteCurrencyId smallint,
			NopBasePol decimal(18,2),
			NopTermPol decimal(18,4),
			[DealsCount] int,
			[ValueDate] Date
		)
		
		DECLARE @temp_intermediate_result TABLE
		(
			CreditLineId tinyint,
			[UnsettledNopUsd] [decimal](18, 2),
			[DealsCount] [int],
			[ValueDate] Date
		)

		INSERT INTO
			@temp_NOPs_pol
			(
				CreditLineId,
				BaseCurrencyId,
				QuoteCurrencyId,
				NopBasePol,
				NopTermPol,
				DealsCount,
				ValueDate
			)
		SELECT 
			[CreditLineId]
			,MAX(sym.BaseCurrencyId) AS BaseCurrencyId
			,MAX(sym.QuoteCurrencyId) AS QuoteCurrencyId
			,SUM([SizeBasePol]) AS [NopBasePol]
			,SUM([SizeTermPol]) AS [NopTermPol]
			,SUM(DealsCount) AS [DealsCount]
			,SettlementDateLocalMkt AS ValueDate
			--,COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate]) AS [SettlementDate]
		FROM
			[dbo].[DealExternalExecutedUnsettled] unsettled
			INNER JOIN [dbo].[PrimeBrokerCreditLines_CptMapping] creditLineMap ON unsettled.[CounterpartyId] = creditLineMap.[CounterpartyId]
			INNER JOIN Symbol sym ON sym.ID = unsettled.[SymbolId]				
		WHERE
			[SettlementTimeUtc] > GetUtcDate()
		GROUP BY
			[CreditLineId],
			[SymbolId],
			[SettlementDateLocalMkt]

		
		INSERT INTO
			@temp_intermediate_result
			(
				[CreditLineId],
				[UnsettledNopUsd],
				[DealsCount],
				[ValueDate]
			)
			SELECT
				CreditLineId,
				SUM(ABS(NopPolInUSD))/2 AS NopAbsInUSD,
				SUM(DealsCount) AS DealsCount,
				ValueDate
			FROM
				(
				SELECT
					CreditLineId,
					nopsUnaggreggated.CurrencyId,
					SUM(NopPol * ccy.LastKnownUsdConversionMultiplier) AS NopPolInUSD,
					SUM(DealsCount) AS DealsCount,
					ValueDate
				FROM
					(
					SELECT
						CreditLineId,
						BaseCurrencyId AS CurrencyId,
						NopBasePol AS NopPol,
						DealsCount AS DealsCount,
						ValueDate
					FROM
						@temp_NOPs_pol basePos

					UNION ALL

					SELECT
						CreditLineId,
						QuoteCurrencyId AS CurrencyId,
						NopTermPol AS NopPol,
						0 AS DealsCount,
						ValueDate
					FROM
						@temp_NOPs_pol basePos
					) nopsUnaggreggated
					INNER JOIN Currency ccy ON ccy.CurrencyID = nopsUnaggreggated.CurrencyId
				GROUP BY
					CreditLineId,
					nopsUnaggreggated.CurrencyId,
					ValueDate
				)nopsAggreggated
			GROUP BY
				CreditLineId,
				ValueDate

			DECLARE @temp_ValueDates TABLE
			(
				ValueDate Date,
				ValueDateId int
			)

			INSERT INTO @temp_ValueDates(ValueDateId, ValueDate)
			SELECT
				ROW_NUMBER() OVER (ORDER BY ValueDate),
				ValueDate
			FROM
				@temp_intermediate_result
			WHERE
				ValueDate IS NOT NULL
			GROUP BY
				ValueDate


			
			;WITH TmpStats AS
			(
				SELECT 
					CreditLineId,
					UnsettledNopUsd,
					DealsCount,
					res.ValueDate,
					'Nop_T' + CAST(ValueDateId AS VARCHAR) AS NopsValueDateTag,
					'Deals_T' + CAST(ValueDateId AS VARCHAR) AS DealsValueDateTag
			FROM
				@temp_intermediate_result res
				FULL OUTER JOIN @temp_ValueDates valDates ON res.ValueDate = valDates.ValueDate
			)

			 INSERT INTO @temp_final_result
			(
				[PrimeBrokerName]
				,[CreditLineName]
				,[CreditLineId]
				,[UnsettledNopUsdTotal]
				,[HighestUnsettledNopInSingleT]
				,[DealsCountTotal]
				,[Zone_Total]
				,[MaxUnsettledNopUsdTotal]
				,[MaxUnsettledNopUsdTotalToLockDestination]
				,[MaxUnsettledNopUsdPerValueDate]
				,[MaxUnsettledNopUsdPerValueDateToLockDestination]
				,[T1]
				,[T2]
				,[T3]
				,[T4]
				,[T5]
				,[T6]
				,[T7]
				,[T8]
				,[UnsettledNopUsd_T1]
				,[UnsettledNopUsd_T2]
				,[UnsettledNopUsd_T3]
				,[UnsettledNopUsd_T4]
				,[UnsettledNopUsd_T5]
				,[UnsettledNopUsd_T6]
				,[UnsettledNopUsd_T7]
				,[UnsettledNopUsd_T8]
				,[DealsCount_T1]
				,[DealsCount_T2]
				,[DealsCount_T3]
				,[DealsCount_T4]
				,[DealsCount_T5]
				,[DealsCount_T6]
				,[DealsCount_T7]
				,[DealsCount_T8]
				,[Zone_T1]
				,[Zone_T2]
				,[Zone_T3]
				,[Zone_T4]
				,[Zone_T5]
				,[Zone_T6]
				,[Zone_T7]
				,[Zone_T8]
			)

			SELECT
				primeBroker.PrimeBrokerName,
				creditLine.[CreditLineName],
				creditLine.[CreditLineId],
				UnsettledNopUsdTotal,
				(SELECT MAX(x) FROM (VALUES(Nop_T1), (Nop_T2), (Nop_T3), (Nop_T4), (Nop_T5), (Nop_T6), (Nop_T7), (Nop_T8)) AS VALUE(x)) AS [HighestUnsettledNopInSingleT],
				DealsCountTotal,
				Zone_Total,
				creditLine.[MaxUnsettledNopUsdTotal],
				creditLine.[MaxUnsettledNopUsdTotalToLockDestination],
				creditLine.[MaxUnsettledNopUsdPerValueDate],
				creditLine.[MaxUnsettledNopUsdPerValueDateToLockDestination],
				T1, T2, T3, T4, T5, T6, T7, T8,
				Nop_T1, Nop_T2, Nop_T3, Nop_T4, Nop_T5, Nop_T6, Nop_T7, Nop_T8, 
				Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8,
				Zone_T1, Zone_T2, Zone_T3, Zone_T4, Zone_T5, Zone_T6, Zone_T7, Zone_T8
			FROM

				(
					select T1, T2, T3, T4, T5, T6, T7, T8
					from
					(
						select ValueDate, 'T' + CAST(ValueDateId AS VARCHAR) AS ValueDateTag
						FROM @temp_ValueDates
					) d
					PIVOT
					(
						MAX(ValueDate)
						for ValueDateTag IN (T1, T2, T3, T4, T5, T6, T7, T8)
					) pivDates
				)ValDates

				INNER JOIN [dbo].[PrimeBrokerCreditLine] creditLine ON 1=1
				INNER JOIN [dbo].[PrimeBroker] primeBroker ON creditLine.[PrimeBrokerId] = primeBroker.[PrimeBrokerId]

				LEFT JOIN

				(
				SELECT 
					CreditLineId,
					MAX(Nop_T1) AS Nop_T1, MAX(Nop_T2) AS Nop_T2, MAX(Nop_T3) AS Nop_T3, MAX(Nop_T4) AS Nop_T4, MAX(Nop_T5) AS Nop_T5, MAX(Nop_T6) AS Nop_T6, MAX(Nop_T7) AS Nop_T7, MAX(Nop_T8) AS Nop_T8
				FROM
					TmpStats
					PIVOT 
					(
						MAX(UnsettledNopUsd) FOR NopsValueDateTag IN (Nop_T1, Nop_T2, Nop_T3, Nop_T4, Nop_T5, Nop_T6, Nop_T7, Nop_T8)
					)pivNops
				GROUP BY
					CreditLineId
				)NopsTmp ON creditLine.CreditLineId = NopsTmp.CreditLineId

				LEFT JOIN

				(
				SELECT 
					CreditLineId,
					MAX(Deals_T1) AS Deals_T1, MAX(Deals_T2) AS Deals_T2, MAX(Deals_T3) AS Deals_T3, MAX(Deals_T4) AS Deals_T4, MAX(Deals_T5) AS Deals_T5, MAX(Deals_T6) AS Deals_T6, MAX(Deals_T7) AS Deals_T7, MAX(Deals_T8) AS Deals_T8
				FROM
					TmpStats
					PIVOT 
					(
						MAX(DealsCount) FOR DealsValueDateTag IN (Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8)
					)piv
				GROUP BY
					CreditLineId
				)DealsTmp

				ON NopsTmp.CreditLineId = DealsTmp.CreditLineId

				LEFT JOIN

				(
				SELECT
					CreditLineId,
					SUM(UnsettledNopUsd) AS UnsettledNopUsdTotal,
					SUM(DealsCount) AS DealsCountTotal
				FROM
					TmpStats
				GROUP BY
					CreditLineId
				) totalSum

				ON totalSum.CreditLineId = DealsTmp.CreditLineId
				
				LEFT JOIN
				
				(
				SELECT
					[CreditLineId],
					Zone_Total, Zone_T1, Zone_T2, Zone_T3, Zone_T4, Zone_T5, Zone_T6, Zone_T7, Zone_T8
				FROM
					(
					SELECT 
						[CreditLineId]
						,'Zone_T' + COALESCE(CAST(valDate.ValueDateId AS varchar), 'otal') AS ZoneValueDateTag
						,zoneName.[NopCheckingZoneName] AS ZoneName
					FROM 
						[dbo].[PrimeBrokerCreditLineNopCheckingZones] zone
						INNER JOIN [dbo].[NopCheckingZone] zoneName ON zone.[NopCheckingZoneId] = zoneName.[NopCheckingZoneId]
						LEFT JOIN @temp_ValueDates valDate ON zone.[SettlementDate] = valDate.ValueDate
					--be carefull not to join old settlement dates that are not in @temp_ValueDates
					WHERE
						zone.[SettlementDate] IS NULL AND valDate.ValueDate IS NULL
						OR
						zone.[SettlementDate] IS NOT NULL AND valDate.ValueDate IS NOT NULL
					) ZoneStats
					PIVOT
					(
						MAX(ZoneStats.ZoneName) FOR ZoneValueDateTag IN (Zone_Total, Zone_T1, Zone_T2, Zone_T3, Zone_T4, Zone_T5, Zone_T6, Zone_T7, Zone_T8)
					) piv
				) zoneInfo

				ON totalSum.CreditLineId = zoneInfo.CreditLineId

			ORDER BY
				primeBroker.PrimeBrokerName,
				creditLine.[CreditLineName]
			
		BEGIN TRANSACTION
			TRUNCATE TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine]
		
			INSERT INTO
				[dbo].[PersistedSettlementStatisticsPerCreditLine]
				(
					[PrimeBrokerName]
					,[CreditLineName]
					,[CreditLineId]
					,[UnsettledNopUsdTotal]
					,[HighestUnsettledNopInSingleT]
					,[DealsCountTotal]
					,[Zone_Total]
					,[MaxUnsettledNopUsdTotal]
					,[MaxUnsettledNopUsdTotalToLockDestination]
					,[MaxUnsettledNopUsdPerValueDate]
					,[MaxUnsettledNopUsdPerValueDateToLockDestination]
					,[T1]
					,[T2]
					,[T3]
					,[T4]
					,[T5]
					,[T6]
					,[T7]
					,[T8]
					,[UnsettledNopUsd_T1]
					,[UnsettledNopUsd_T2]
					,[UnsettledNopUsd_T3]
					,[UnsettledNopUsd_T4]
					,[UnsettledNopUsd_T5]
					,[UnsettledNopUsd_T6]
					,[UnsettledNopUsd_T7]
					,[UnsettledNopUsd_T8]
					,[DealsCount_T1]
					,[DealsCount_T2]
					,[DealsCount_T3]
					,[DealsCount_T4]
					,[DealsCount_T5]
					,[DealsCount_T6]
					,[DealsCount_T7]
					,[DealsCount_T8]
					,[Zone_T1]
					,[Zone_T2]
					,[Zone_T3]
					,[Zone_T4]
					,[Zone_T5]
					,[Zone_T6]
					,[Zone_T7]
					,[Zone_T8]
				)
			SELECT
				[PrimeBrokerName]
				,[CreditLineName]
				,[CreditLineId]
				,[UnsettledNopUsdTotal]
				,[HighestUnsettledNopInSingleT]
				,[DealsCountTotal]
				,[Zone_Total]
				,[MaxUnsettledNopUsdTotal]
				,[MaxUnsettledNopUsdTotalToLockDestination]
				,[MaxUnsettledNopUsdPerValueDate]
				,[MaxUnsettledNopUsdPerValueDateToLockDestination]
				,[T1]
				,[T2]
				,[T3]
				,[T4]
				,[T5]
				,[T6]
				,[T7]
				,[T8]
				,[UnsettledNopUsd_T1]
				,[UnsettledNopUsd_T2]
				,[UnsettledNopUsd_T3]
				,[UnsettledNopUsd_T4]
				,[UnsettledNopUsd_T5]
				,[UnsettledNopUsd_T6]
				,[UnsettledNopUsd_T7]
				,[UnsettledNopUsd_T8]
				,[DealsCount_T1]
				,[DealsCount_T2]
				,[DealsCount_T3]
				,[DealsCount_T4]
				,[DealsCount_T5]
				,[DealsCount_T6]
				,[DealsCount_T7]
				,[DealsCount_T8]
				,[Zone_T1]
				,[Zone_T2]
				,[Zone_T3]
				,[Zone_T4]
				,[Zone_T5]
				,[Zone_T6]
				,[Zone_T7]
				,[Zone_T8]
			FROM
				@temp_final_result
		COMMIT	

		exec [dbo].[UpdateSettlementStatisticsPerSymbol_Internal_SP]
	END
END 
GO


CREATE PROCEDURE [dbo].[GetSettlementStatisticsPerSymbol_Cached_SP]
AS
BEGIN

	SELECT 
		[SymbolName]
		,[T1]
		,[T2]
		,[T3]
		,[T4]
		,[T5]
		,[T6]
		,[T7]
		,[T8]
		,[UnsettledNopNominal_T1]
		,[UnsettledNopNominal_T2]
		,[UnsettledNopNominal_T3]
		,[UnsettledNopNominal_T4]
		,[UnsettledNopNominal_T5]
		,[UnsettledNopNominal_T6]
		,[UnsettledNopNominal_T7]
		,[UnsettledNopNominal_T8]
		,[DealsCount_T1]
		,[DealsCount_T2]
		,[DealsCount_T3]
		,[DealsCount_T4]
		,[DealsCount_T5]
		,[DealsCount_T6]
		,[DealsCount_T7]
		,[DealsCount_T8]
	FROM
		[dbo].[PersistedSettlementStatisticsPerSymbol]
	ORDER BY
		[SymbolName] ASC
END
GO

GRANT EXECUTE ON [dbo].[GetSettlementStatisticsPerSymbol_Cached_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

------------------------------
----------------- SettlementTimeCentralBankUtc ----------------------------------------
------------------------------


exec sp_rename '[dbo].[DealExternalExecuted].[SettlementTimeUtc]', 'SettlementTimeCounterpartyUtc'
exec sp_rename '[dbo].[OrderExternalIncomingRejectable_Daily].[SettlementTimeUtc]', 'SettlementTimeCounterpartyUtc'
exec sp_rename '[dbo].[OrderExternalIncomingRejectable_Archive].[SettlementTimeUtc]', 'SettlementTimeCounterpartyUtc'
exec sp_rename '[dbo].[DealExternalExecutedUnsettled].[SettlementTimeUtc]', 'SettlementTimeCounterpartyUtc'
GO
ALTER TABLE [dbo].[DealExternalExecuted] ADD SettlementTimeCentralBankUtc [datetime] NULL
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] ADD SettlementTimeCentralBankUtc [datetime] NULL
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] ADD SettlementTimeCentralBankUtc [datetime] NULL
ALTER TABLE [dbo].[DealExternalExecutedUnsettled] ADD SettlementTimeCentralBankUtc [datetime] NULL
GO

UPDATE [dbo].[DealExternalExecuted]
SET [SettlementTimeCentralBankUtc] = [SettlementTimeCounterpartyUtc]

UPDATE [dbo].[OrderExternalIncomingRejectable_Daily]
SET [SettlementTimeCentralBankUtc] = [SettlementTimeCounterpartyUtc]

UPDATE [dbo].[OrderExternalIncomingRejectable_Archive]
SET [SettlementTimeCentralBankUtc] = [SettlementTimeCounterpartyUtc]

UPDATE [dbo].[DealExternalExecutedUnsettled]
SET [SettlementTimeCentralBankUtc] = [SettlementTimeCounterpartyUtc]

ALTER TABLE [dbo].[DealExternalExecuted] ALTER COLUMN [SettlementTimeCentralBankUtc] [datetime] NOT NULL
GO
ALTER TABLE [dbo].[DealExternalExecutedUnsettled] ALTER COLUMN [SettlementTimeCentralBankUtc] [datetime] NOT NULL
GO

CREATE NONCLUSTERED INDEX [IX_DealExternalExecutedUnsettled_SettlementCentralBank] ON [dbo].[DealExternalExecutedUnsettled]
(
	[SettlementTimeCentralBankUtc] ASC
)ON [Integrator_DailyData]
GO

ALTER PROCEDURE [dbo].[ForceRefreshAllUnsettledDeals_SP]
AS
BEGIN

	DECLARE @temp_unsettledDeals TABLE
	(
		[CounterpartyId] [tinyint],
		[SymbolId] [tinyint],
		[SettlementDateLocalMkt] [date],
		[SettlementTimeCounterpartyUtc] [DateTime],
		[SettlementTimeCentralBankUtc] [DateTime],
		[SizeBasePol] [decimal](18, 2),
		[SizeTermPol] [decimal](18, 4),
		[DealsCount] [int]
	)

	INSERT INTO
		@temp_unsettledDeals
		(
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SettlementTimeCounterpartyUtc],
			[SettlementTimeCentralBankUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		)
	SELECT
		[CounterpartyId],
		[SymbolId],
		[ValueDate],
		[SettlementTimeCounterpartyUtc],
		[SettlementTimeCentralBankUtc],
		SUM([SizeBasePol]),
		SUM([SizeTermPol]),
		--count arbitrary column
		COUNT([CounterpartyId])
	FROM
		(
		SELECT 
			deal.[CounterpartyId]
			,deal.[SymbolId]
			,deal.[ValueDateCounterpartySuppliedLocMktDate] AS [ValueDate]
			,deal.[SettlementTimeCounterpartyUtc]
			,deal.[SettlementTimeCentralBankUtc]
			,deal.[AmountBasePolExecuted] AS [SizeBasePol]
			,-deal.[AmountBasePolExecuted]*deal.[Price] AS [SizeTermPol]
		FROM 
			[dbo].[DealExternalExecuted] deal
		WHERE
			IntegratorReceivedExecutionReportUtc > DATEADD(DAY, -10, GETUTCDATE())
			AND
			([SettlementTimeCounterpartyUtc] > GetUtcDate() OR [SettlementTimeCentralBankUtc] > GetUtcDate())

		UNION ALL

		SELECT 
			[CounterpartyId]
			,[SymbolId]
			,[ValueDate]
			,[SettlementTimeCounterpartyUtc]
			,[SettlementTimeCentralBankUtc]
			,[IntegratorExecutedAmountBasePol] AS [SizeBasePol]
			,-[IntegratorExecutedAmountBasePol]*[CounterpartyRequestedPrice] AS [SizeTermPol]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]
		WHERE
			[IntegratorExecutedAmountBasePol] > 0

		UNION ALL

		SELECT 
			[CounterpartyId]
			,[SymbolId]
			,[ValueDate]
			,[SettlementTimeCounterpartyUtc]
			,[SettlementTimeCentralBankUtc]
			,[IntegratorExecutedAmountBasePol] AS [SizeBasePol]
			,-[IntegratorExecutedAmountBasePol]*[CounterpartyRequestedPrice] AS [SizeTermPol]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Archive] deal
		WHERE
			[IntegratorReceivedExternalOrderUtc] > DATEADD(DAY, -10, GETUTCDATE())
			AND
			[IntegratorExecutedAmountBasePol] > 0
			AND
			([SettlementTimeCounterpartyUtc] > GetUtcDate() OR [SettlementTimeCentralBankUtc] > GetUtcDate())
		) allUnsettledDeals
	GROUP BY
		[SettlementTimeCounterpartyUtc],
		[SettlementTimeCentralBankUtc],
		[ValueDate],
		[CounterpartyId],
		[SymbolId]

		
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
	
		INSERT INTO
			[dbo].[DealExternalExecutedUnsettled]
			(
				[CounterpartyId],
				[SymbolId],
				[SettlementDateLocalMkt],
				[SettlementTimeCounterpartyUtc],
				[SettlementTimeCentralBankUtc],
				[SizeBasePol],
				[SizeTermPol],
				[DealsCount]
			)
		SELECT
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SettlementTimeCounterpartyUtc],
			[SettlementTimeCentralBankUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		FROM
			@temp_unsettledDeals
	COMMIT	
END
GO

ALTER PROCEDURE [dbo].[GetUnsettledDealsPerCreditLineSettlementAndCurrency_SP]
AS
BEGIN
	SELECT 
		ctp.[CounterpartyCode] AS Counterparty,
		sym.[Name] AS Symbol,
		SettlementDateLocalMkt AS SettlementDateLocal,
		SettlementTimeCounterpartyUtc,
		SUM(unsettled.SizeBasePol) AS SizeBasePol,
		SUM(unsettled.SizeTermPol) AS SizeTermPol
	FROM
		[dbo].[DealExternalExecutedUnsettled] unsettled
		INNER JOIN [dbo].[Counterparty] ctp ON unsettled.[CounterpartyId] = ctp.[CounterpartyId]
		INNER JOIN Symbol sym ON sym.ID = unsettled.[SymbolId]				
	WHERE
		[SettlementTimeCounterpartyUtc] >= GETUTCDATE()
	GROUP BY
		[SettlementTimeCounterpartyUtc],
		[SettlementDateLocalMkt],
		ctp.[CounterpartyCode],
		sym.[Name]
END
GO

ALTER PROCEDURE [dbo].[Daily_PurgeAndRollupUnsettledDeals_SP]
AS
BEGIN

	DECLARE @temp_unsettledDeals TABLE
	(
		[CounterpartyId] [tinyint],
		[SymbolId] [tinyint],
		[SettlementDateLocalMkt] [date],
		[SettlementTimeCounterpartyUtc] [DateTime],
		[SettlementTimeCentralBankUtc] [DateTime],
		[SizeBasePol] [decimal](18, 2),
		[SizeTermPol] [decimal](18, 4),
		[DealsCount] [int]
	)

	INSERT INTO
		@temp_unsettledDeals
		(
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SettlementTimeCounterpartyUtc],
			[SettlementTimeCentralBankUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		)
	SELECT 
		[CounterpartyId]
		,[SymbolId]
		,[SettlementDateLocalMkt]
		,[SettlementTimeCounterpartyUtc]
		,[SettlementTimeCentralBankUtc]
		,SUM([SizeBasePol]) AS [NopBasePol]
		,SUM([SizeTermPol]) AS [NopTermPol]
		,SUM([DealsCount]) AS [DealsCount]
	FROM
		[dbo].[DealExternalExecutedUnsettled] unsettled				
	WHERE
		[SettlementTimeCounterpartyUtc] > GetUtcDate() OR [SettlementTimeCentralBankUtc] > GetUtcDate()
	GROUP BY
		[SettlementTimeCounterpartyUtc],
		[SettlementTimeCentralBankUtc],
		[SettlementDateLocalMkt],
		[CounterpartyId],
		[SymbolId]
		
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
	
		INSERT INTO
			[dbo].[DealExternalExecutedUnsettled]
			(
				[CounterpartyId],
				[SymbolId],
				[SettlementDateLocalMkt],
				[SettlementTimeCounterpartyUtc],
				[SettlementTimeCentralBankUtc],
				[SizeBasePol],
				[SizeTermPol],
				[DealsCount]
			)
		SELECT
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SettlementTimeCounterpartyUtc],
			[SettlementTimeCentralBankUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		FROM
			@temp_unsettledDeals
	COMMIT	
END
GO

ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 2),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@ValueDateCounterpartySuppliedRecalcedToCounterpartyUtc DATETIME,
	@ValueDateCounterpartySuppliedRecalcedToCentralBankUtc DATETIME,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL,
	@SingleFillSystemId int = NULL,
	@MultipleFillsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL,
	@CounterpartyClientId varchar(20) = NULL)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @MultipleFillsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleFillsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external deal for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@SingleFillSystemId, -1)
				,@AmountBasePolExecuted
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc)
		END
		
		INSERT INTO 
		[dbo].[DealExternalExecuted]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolExecuted]
			   ,[AmountTermPolExecutedInUsd]
			   ,[Price]
			   ,[CounterpartyTransactionId]
			   ,[CounterpartySuppliedExecutionTimeStampUtc]
			   ,[ValueDateCounterpartySuppliedLocMktDate]
			   ,[SettlementTimeCounterpartyUtc]
			   ,[SettlementTimeCentralBankUtc]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[InternalTransactionIdentity]
			   ,[FlowSideId]
			   ,[CounterpartyClientId])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolExecuted
			   ,@AmountTermPolExecutedInUsd
			   ,@Price
			   ,@CounterpartyTransactionId
			   ,@CounterpartyExecutionReportTimeStampUtc
			   ,@ValueDateCounterpartySuppliedLocMktDate
			   ,@ValueDateCounterpartySuppliedRecalcedToCounterpartyUtc
			   ,@ValueDateCounterpartySuppliedRecalcedToCentralBankUtc
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@CounterpartySentExecutionReportUtc
			   ,@InternalDealId
			   ,@FlowSideId
			   ,@CounterpartyClientId)
			   
		INSERT INTO [dbo].[DealExternalExecutedUnsettled]
           ([CounterpartyId]
           ,[SymbolId]
           ,[SettlementDateLocalMkt]
		   ,[SettlementTimeCounterpartyUtc]
		   ,[SettlementTimeCentralBankUtc]
           ,[SizeBasePol]
           ,[SizeTermPol]
		   ,[DealsCount])
		VALUES
           (@CounterpartyId
           ,@SymbolId
           ,@ValueDateCounterpartySuppliedLocMktDate
		   ,@ValueDateCounterpartySuppliedRecalcedToCounterpartyUtc
		   ,@ValueDateCounterpartySuppliedRecalcedToCentralBankUtc
           ,@AmountBasePolExecuted
           ,-@AmountBasePolExecuted*@Price
		   ,1)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[InsertNewOrderExternalIncomingRejectable_SP]( 
    @ExternalOrderReceived [datetime2](7),
	@ExternalOrderSentUtc [datetime2](7),
	@IntegratorSentExecutionReportUtc [datetime2](7),
	@CounterpartyClientOrderId [varchar](32),
	@CounterpartyClientId [varchar](20),
	@TradingSystemId int = NULL,
	@SourceIntegratorPriceIdentity [varchar](20) = NULL,
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@RequestedFillMinimumBaseAbs decimal(18,2) = NULL,
	@IntegratorExecId [varchar](20) = NULL,
	@CounterpartyExecId [varchar](32) = NULL,
	@ValueDate [date] = NULL,
	@SettlementTimeCounterpartyUtc [datetime] = NULL,
	@SettlementTimeCentralBankUtc [datetime] = NULL,
	@OrderReceivedToExecutionReportSentLatency [time](7),
	@IntegratorExecutedAmountBaseAbs [decimal](18, 2) = NULL,
	@IntegratorRejectedAmountBaseAbs [decimal](18, 2) = NULL,
	@RejectionReason [varchar](32) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WITH(NOLOCK) WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WITH(NOLOCK) WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WITH(NOLOCK) WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @RejectionReason IS NOT NULL
	BEGIN
		SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
		IF @@ROWCOUNT <> 1
		BEGIN
			INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
			SET @RejectionReasonId = @@IDENTITY
		END
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @IntegratorExecutedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,COALESCE(@IntegratorExecId, @CounterpartyExecId)
				,@ExternalOrderReceived)
		END
		ELSE IF @IntegratorRejectedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,ISNULL(@RejectionReason, '<NONE>')
				,COALESCE(@IntegratorExecId, @CounterpartyExecId)
				,@RejectionDirectionId
				,@ExternalOrderReceived)
		END

		INSERT INTO 
			[dbo].[OrderExternalIncomingRejectable_Daily]
			   ([IntegratorReceivedExternalOrderUtc]
			   ,[CounterpartySentExternalOrderUtc]
			   ,[IntegratorSentExecutionReportUtc]
			   ,[CounterpartyClientOrderId]
			   ,[CounterpartyClientId]
			   ,[TradingSystemId]
			   ,[SourceIntegratorPriceIdentity]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorDealDirectionId]
			   ,[CounterpartyRequestedPrice]
			   ,[CounterpartyRequestedAmountBasePol]
			   ,[CounterpartyRequestedFillMinimumBaseAbs]
			   ,[IntegratorExecutionId]
			   ,[CounterpartyExecutionId]
			   ,[ValueDate]
			   ,[SettlementTimeCounterpartyUtc]
			   ,[SettlementTimeCentralBankUtc]
			   ,[OrderReceivedToExecutionReportSentLatency ]
			   ,[IntegratorExecutedAmountBasePol]
			   ,[IntegratorRejectedAmountBasePol]
			   ,[IntegratorRejectionReasonId])
		 VALUES
			   (@ExternalOrderReceived
			   ,@ExternalOrderSentUtc
			   ,@IntegratorSentExecutionReportUtc
			   ,@CounterpartyClientOrderId
			   ,@CounterpartyClientId
			   ,@TradingSystemId
			   ,@SourceIntegratorPriceIdentity
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@DealDirectionId
			   ,@RequestedPrice
			   ,CASE WHEN @DealDirectionId = 1 THEN -@RequestedAmountBaseAbs ELSE @RequestedAmountBaseAbs END
			   ,@RequestedFillMinimumBaseAbs
			   ,@IntegratorExecId
			   ,@CounterpartyExecId
			   ,@ValueDate
			   ,@SettlementTimeCounterpartyUtc
			   ,@SettlementTimeCentralBankUtc
			   ,@OrderReceivedToExecutionReportSentLatency 
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
		       ,@RejectionReasonId)
			   
		IF @ValueDate IS NOT NULL AND @IntegratorExecutedAmountBaseAbs != 0
		BEGIN
			INSERT INTO [dbo].[DealExternalExecutedUnsettled]
			   ([CounterpartyId]
			   ,[SymbolId]
			   ,[SettlementDateLocalMkt]
			   ,[SettlementTimeCounterpartyUtc]
			   ,[SettlementTimeCentralBankUtc]
			   ,[SizeBasePol]
			   ,[SizeTermPol]
			   ,[DealsCount])
			VALUES
			   (@CounterpartyId
			   ,@SymbolId
			   ,@ValueDate
			   ,@SettlementTimeCounterpartyUtc
			   ,@SettlementTimeCentralBankUtc
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN @IntegratorExecutedAmountBaseAbs ELSE -@IntegratorExecutedAmountBaseAbs END * @RequestedPrice
			   ,1)
		END
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH	   
END
GO

ALTER PROCEDURE [dbo].[Daily_ArchiveRejectableOrders_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[OrderExternalIncomingRejectable_Archive]
			([IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[CounterpartyRequestedFillMinimumBaseAbs],
			[IntegratorExecutionId],
			[CounterpartyExecutionId],
			[ValueDate],
			[SettlementTimeCounterpartyUtc],
			[SettlementTimeCentralBankUtc],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[GlobalRecordId])
		SELECT
			[IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[CounterpartyRequestedFillMinimumBaseAbs],
			[IntegratorExecutionId],
			[CounterpartyExecutionId],
			[ValueDate],
			[SettlementTimeCounterpartyUtc],
			[SettlementTimeCentralBankUtc],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[GlobalRecordId]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]
		
		--remember last identity
		DECLARE @LastId INT
		SELECT @LastId = IDENT_CURRENT ('OrderExternalIncomingRejectable_Daily')
		SET @LastId = @LastId + 1
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[OrderExternalIncomingRejectable_Daily]
		
		--and continue in identity
		DBCC CHECKIDENT ('OrderExternalIncomingRejectable_Daily', RESEED, @LastId)
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO


ALTER PROCEDURE [dbo].[UpdateSettlementStatisticsPerCreditLine_SP]
(
	@Force BIT = 0
)
AS
BEGIN

	UPDATE 
		[dbo].[TableChangeLog] 
	SET 
		[LastUpdatedUtc] = GETUTCDATE() 
	WHERE 
		[TableName] = 'SettlementStatistics' 
		AND 
		(@Force = 1 OR [LastUpdatedUtc] < DATEADD(SECOND, -2, GETUTCDATE()))

	IF @@ROWCOUNT > 0
	BEGIN
	
		DECLARE @temp_final_result TABLE
		(
			[PrimeBrokerName] [varchar](10),
			[CreditLineName] [varchar](255),
			[CreditLineId] [tinyint],
			[UnsettledNopUsdTotal] [decimal](18, 2),
			[HighestUnsettledNopInSingleT] [decimal](18, 2),
			[DealsCountTotal] [int],
			[Zone_Total] [varchar](20),
			[MaxUnsettledNopUsdTotal] [decimal](18, 2),
			[MaxUnsettledNopUsdTotalToLockDestination] [decimal](18, 2),
			[MaxUnsettledNopUsdPerValueDate] [decimal](18, 2),
			[MaxUnsettledNopUsdPerValueDateToLockDestination] [decimal](18, 2),
			[T1] [date],
			[T2] [date],
			[T3] [date],
			[T4] [date],
			[T5] [date],
			[T6] [date],
			[T7] [date],
			[T8] [date],
			[UnsettledNopUsd_T1] [decimal](18, 2),
			[UnsettledNopUsd_T2] [decimal](18, 2),
			[UnsettledNopUsd_T3] [decimal](18, 2),
			[UnsettledNopUsd_T4] [decimal](18, 2),
			[UnsettledNopUsd_T5] [decimal](18, 2),
			[UnsettledNopUsd_T6] [decimal](18, 2),
			[UnsettledNopUsd_T7] [decimal](18, 2),
			[UnsettledNopUsd_T8] [decimal](18, 2),
			[DealsCount_T1] [int],
			[DealsCount_T2] [int],
			[DealsCount_T3] [int],
			[DealsCount_T4] [int],
			[DealsCount_T5] [int],
			[DealsCount_T6] [int],
			[DealsCount_T7] [int],
			[DealsCount_T8] [int],
			[Zone_T1] [varchar](20),
			[Zone_T2] [varchar](20),
			[Zone_T3] [varchar](20),
			[Zone_T4] [varchar](20),
			[Zone_T5] [varchar](20),
			[Zone_T6] [varchar](20),
			[Zone_T7] [varchar](20),
			[Zone_T8] [varchar](20)
		)
		
		
		DECLARE @temp_NOPs_pol TABLE
		(
			CreditLineId tinyint,
			BaseCurrencyId smallint,
			QuoteCurrencyId smallint,
			NopBasePol decimal(18,2),
			NopTermPol decimal(18,4),
			[DealsCount] int,
			[ValueDate] Date
		)
		
		DECLARE @temp_intermediate_result TABLE
		(
			CreditLineId tinyint,
			[UnsettledNopUsd] [decimal](18, 2),
			[DealsCount] [int],
			[ValueDate] Date
		)

		INSERT INTO
			@temp_NOPs_pol
			(
				CreditLineId,
				BaseCurrencyId,
				QuoteCurrencyId,
				NopBasePol,
				NopTermPol,
				DealsCount,
				ValueDate
			)
		SELECT 
			[CreditLineId]
			,MAX(sym.BaseCurrencyId) AS BaseCurrencyId
			,MAX(sym.QuoteCurrencyId) AS QuoteCurrencyId
			,SUM([SizeBasePol]) AS [NopBasePol]
			,SUM([SizeTermPol]) AS [NopTermPol]
			,SUM(DealsCount) AS [DealsCount]
			,SettlementDateLocalMkt AS ValueDate
			--,COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate]) AS [SettlementDate]
		FROM
			[dbo].[DealExternalExecutedUnsettled] unsettled
			INNER JOIN [dbo].[PrimeBrokerCreditLines_CptMapping] creditLineMap ON unsettled.[CounterpartyId] = creditLineMap.[CounterpartyId]
			INNER JOIN Symbol sym ON sym.ID = unsettled.[SymbolId]				
		WHERE
			[SettlementTimeCounterpartyUtc] > GetUtcDate()
		GROUP BY
			[CreditLineId],
			[SymbolId],
			[SettlementDateLocalMkt]

		
		INSERT INTO
			@temp_intermediate_result
			(
				[CreditLineId],
				[UnsettledNopUsd],
				[DealsCount],
				[ValueDate]
			)
			SELECT
				CreditLineId,
				SUM(ABS(NopPolInUSD))/2 AS NopAbsInUSD,
				SUM(DealsCount) AS DealsCount,
				ValueDate
			FROM
				(
				SELECT
					CreditLineId,
					nopsUnaggreggated.CurrencyId,
					SUM(NopPol * ccy.LastKnownUsdConversionMultiplier) AS NopPolInUSD,
					SUM(DealsCount) AS DealsCount,
					ValueDate
				FROM
					(
					SELECT
						CreditLineId,
						BaseCurrencyId AS CurrencyId,
						NopBasePol AS NopPol,
						DealsCount AS DealsCount,
						ValueDate
					FROM
						@temp_NOPs_pol basePos

					UNION ALL

					SELECT
						CreditLineId,
						QuoteCurrencyId AS CurrencyId,
						NopTermPol AS NopPol,
						0 AS DealsCount,
						ValueDate
					FROM
						@temp_NOPs_pol basePos
					) nopsUnaggreggated
					INNER JOIN Currency ccy ON ccy.CurrencyID = nopsUnaggreggated.CurrencyId
				GROUP BY
					CreditLineId,
					nopsUnaggreggated.CurrencyId,
					ValueDate
				)nopsAggreggated
			GROUP BY
				CreditLineId,
				ValueDate

			DECLARE @temp_ValueDates TABLE
			(
				ValueDate Date,
				ValueDateId int
			)

			INSERT INTO @temp_ValueDates(ValueDateId, ValueDate)
			SELECT
				ROW_NUMBER() OVER (ORDER BY ValueDate),
				ValueDate
			FROM
				@temp_intermediate_result
			WHERE
				ValueDate IS NOT NULL
			GROUP BY
				ValueDate


			
			;WITH TmpStats AS
			(
				SELECT 
					CreditLineId,
					UnsettledNopUsd,
					DealsCount,
					res.ValueDate,
					'Nop_T' + CAST(ValueDateId AS VARCHAR) AS NopsValueDateTag,
					'Deals_T' + CAST(ValueDateId AS VARCHAR) AS DealsValueDateTag
			FROM
				@temp_intermediate_result res
				FULL OUTER JOIN @temp_ValueDates valDates ON res.ValueDate = valDates.ValueDate
			)

			 INSERT INTO @temp_final_result
			(
				[PrimeBrokerName]
				,[CreditLineName]
				,[CreditLineId]
				,[UnsettledNopUsdTotal]
				,[HighestUnsettledNopInSingleT]
				,[DealsCountTotal]
				,[Zone_Total]
				,[MaxUnsettledNopUsdTotal]
				,[MaxUnsettledNopUsdTotalToLockDestination]
				,[MaxUnsettledNopUsdPerValueDate]
				,[MaxUnsettledNopUsdPerValueDateToLockDestination]
				,[T1]
				,[T2]
				,[T3]
				,[T4]
				,[T5]
				,[T6]
				,[T7]
				,[T8]
				,[UnsettledNopUsd_T1]
				,[UnsettledNopUsd_T2]
				,[UnsettledNopUsd_T3]
				,[UnsettledNopUsd_T4]
				,[UnsettledNopUsd_T5]
				,[UnsettledNopUsd_T6]
				,[UnsettledNopUsd_T7]
				,[UnsettledNopUsd_T8]
				,[DealsCount_T1]
				,[DealsCount_T2]
				,[DealsCount_T3]
				,[DealsCount_T4]
				,[DealsCount_T5]
				,[DealsCount_T6]
				,[DealsCount_T7]
				,[DealsCount_T8]
				,[Zone_T1]
				,[Zone_T2]
				,[Zone_T3]
				,[Zone_T4]
				,[Zone_T5]
				,[Zone_T6]
				,[Zone_T7]
				,[Zone_T8]
			)

			SELECT
				primeBroker.PrimeBrokerName,
				creditLine.[CreditLineName],
				creditLine.[CreditLineId],
				UnsettledNopUsdTotal,
				(SELECT MAX(x) FROM (VALUES(Nop_T1), (Nop_T2), (Nop_T3), (Nop_T4), (Nop_T5), (Nop_T6), (Nop_T7), (Nop_T8)) AS VALUE(x)) AS [HighestUnsettledNopInSingleT],
				DealsCountTotal,
				Zone_Total,
				creditLine.[MaxUnsettledNopUsdTotal],
				creditLine.[MaxUnsettledNopUsdTotalToLockDestination],
				creditLine.[MaxUnsettledNopUsdPerValueDate],
				creditLine.[MaxUnsettledNopUsdPerValueDateToLockDestination],
				T1, T2, T3, T4, T5, T6, T7, T8,
				Nop_T1, Nop_T2, Nop_T3, Nop_T4, Nop_T5, Nop_T6, Nop_T7, Nop_T8, 
				Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8,
				Zone_T1, Zone_T2, Zone_T3, Zone_T4, Zone_T5, Zone_T6, Zone_T7, Zone_T8
			FROM

				(
					select T1, T2, T3, T4, T5, T6, T7, T8
					from
					(
						select ValueDate, 'T' + CAST(ValueDateId AS VARCHAR) AS ValueDateTag
						FROM @temp_ValueDates
					) d
					PIVOT
					(
						MAX(ValueDate)
						for ValueDateTag IN (T1, T2, T3, T4, T5, T6, T7, T8)
					) pivDates
				)ValDates

				INNER JOIN [dbo].[PrimeBrokerCreditLine] creditLine ON 1=1
				INNER JOIN [dbo].[PrimeBroker] primeBroker ON creditLine.[PrimeBrokerId] = primeBroker.[PrimeBrokerId]

				LEFT JOIN

				(
				SELECT 
					CreditLineId,
					MAX(Nop_T1) AS Nop_T1, MAX(Nop_T2) AS Nop_T2, MAX(Nop_T3) AS Nop_T3, MAX(Nop_T4) AS Nop_T4, MAX(Nop_T5) AS Nop_T5, MAX(Nop_T6) AS Nop_T6, MAX(Nop_T7) AS Nop_T7, MAX(Nop_T8) AS Nop_T8
				FROM
					TmpStats
					PIVOT 
					(
						MAX(UnsettledNopUsd) FOR NopsValueDateTag IN (Nop_T1, Nop_T2, Nop_T3, Nop_T4, Nop_T5, Nop_T6, Nop_T7, Nop_T8)
					)pivNops
				GROUP BY
					CreditLineId
				)NopsTmp ON creditLine.CreditLineId = NopsTmp.CreditLineId

				LEFT JOIN

				(
				SELECT 
					CreditLineId,
					MAX(Deals_T1) AS Deals_T1, MAX(Deals_T2) AS Deals_T2, MAX(Deals_T3) AS Deals_T3, MAX(Deals_T4) AS Deals_T4, MAX(Deals_T5) AS Deals_T5, MAX(Deals_T6) AS Deals_T6, MAX(Deals_T7) AS Deals_T7, MAX(Deals_T8) AS Deals_T8
				FROM
					TmpStats
					PIVOT 
					(
						MAX(DealsCount) FOR DealsValueDateTag IN (Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8)
					)piv
				GROUP BY
					CreditLineId
				)DealsTmp

				ON NopsTmp.CreditLineId = DealsTmp.CreditLineId

				LEFT JOIN

				(
				SELECT
					CreditLineId,
					SUM(UnsettledNopUsd) AS UnsettledNopUsdTotal,
					SUM(DealsCount) AS DealsCountTotal
				FROM
					TmpStats
				GROUP BY
					CreditLineId
				) totalSum

				ON totalSum.CreditLineId = DealsTmp.CreditLineId
				
				LEFT JOIN
				
				(
				SELECT
					[CreditLineId],
					Zone_Total, Zone_T1, Zone_T2, Zone_T3, Zone_T4, Zone_T5, Zone_T6, Zone_T7, Zone_T8
				FROM
					(
					SELECT 
						[CreditLineId]
						,'Zone_T' + COALESCE(CAST(valDate.ValueDateId AS varchar), 'otal') AS ZoneValueDateTag
						,zoneName.[NopCheckingZoneName] AS ZoneName
					FROM 
						[dbo].[PrimeBrokerCreditLineNopCheckingZones] zone
						INNER JOIN [dbo].[NopCheckingZone] zoneName ON zone.[NopCheckingZoneId] = zoneName.[NopCheckingZoneId]
						LEFT JOIN @temp_ValueDates valDate ON zone.[SettlementDate] = valDate.ValueDate
					--be carefull not to join old settlement dates that are not in @temp_ValueDates
					WHERE
						zone.[SettlementDate] IS NULL AND valDate.ValueDate IS NULL
						OR
						zone.[SettlementDate] IS NOT NULL AND valDate.ValueDate IS NOT NULL
					) ZoneStats
					PIVOT
					(
						MAX(ZoneStats.ZoneName) FOR ZoneValueDateTag IN (Zone_Total, Zone_T1, Zone_T2, Zone_T3, Zone_T4, Zone_T5, Zone_T6, Zone_T7, Zone_T8)
					) piv
				) zoneInfo

				ON totalSum.CreditLineId = zoneInfo.CreditLineId

			ORDER BY
				primeBroker.PrimeBrokerName,
				creditLine.[CreditLineName]
			
		BEGIN TRANSACTION
			TRUNCATE TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine]
		
			INSERT INTO
				[dbo].[PersistedSettlementStatisticsPerCreditLine]
				(
					[PrimeBrokerName]
					,[CreditLineName]
					,[CreditLineId]
					,[UnsettledNopUsdTotal]
					,[HighestUnsettledNopInSingleT]
					,[DealsCountTotal]
					,[Zone_Total]
					,[MaxUnsettledNopUsdTotal]
					,[MaxUnsettledNopUsdTotalToLockDestination]
					,[MaxUnsettledNopUsdPerValueDate]
					,[MaxUnsettledNopUsdPerValueDateToLockDestination]
					,[T1]
					,[T2]
					,[T3]
					,[T4]
					,[T5]
					,[T6]
					,[T7]
					,[T8]
					,[UnsettledNopUsd_T1]
					,[UnsettledNopUsd_T2]
					,[UnsettledNopUsd_T3]
					,[UnsettledNopUsd_T4]
					,[UnsettledNopUsd_T5]
					,[UnsettledNopUsd_T6]
					,[UnsettledNopUsd_T7]
					,[UnsettledNopUsd_T8]
					,[DealsCount_T1]
					,[DealsCount_T2]
					,[DealsCount_T3]
					,[DealsCount_T4]
					,[DealsCount_T5]
					,[DealsCount_T6]
					,[DealsCount_T7]
					,[DealsCount_T8]
					,[Zone_T1]
					,[Zone_T2]
					,[Zone_T3]
					,[Zone_T4]
					,[Zone_T5]
					,[Zone_T6]
					,[Zone_T7]
					,[Zone_T8]
				)
			SELECT
				[PrimeBrokerName]
				,[CreditLineName]
				,[CreditLineId]
				,[UnsettledNopUsdTotal]
				,[HighestUnsettledNopInSingleT]
				,[DealsCountTotal]
				,[Zone_Total]
				,[MaxUnsettledNopUsdTotal]
				,[MaxUnsettledNopUsdTotalToLockDestination]
				,[MaxUnsettledNopUsdPerValueDate]
				,[MaxUnsettledNopUsdPerValueDateToLockDestination]
				,[T1]
				,[T2]
				,[T3]
				,[T4]
				,[T5]
				,[T6]
				,[T7]
				,[T8]
				,[UnsettledNopUsd_T1]
				,[UnsettledNopUsd_T2]
				,[UnsettledNopUsd_T3]
				,[UnsettledNopUsd_T4]
				,[UnsettledNopUsd_T5]
				,[UnsettledNopUsd_T6]
				,[UnsettledNopUsd_T7]
				,[UnsettledNopUsd_T8]
				,[DealsCount_T1]
				,[DealsCount_T2]
				,[DealsCount_T3]
				,[DealsCount_T4]
				,[DealsCount_T5]
				,[DealsCount_T6]
				,[DealsCount_T7]
				,[DealsCount_T8]
				,[Zone_T1]
				,[Zone_T2]
				,[Zone_T3]
				,[Zone_T4]
				,[Zone_T5]
				,[Zone_T6]
				,[Zone_T7]
				,[Zone_T8]
			FROM
				@temp_final_result
		COMMIT	
		
		exec [dbo].[UpdateSettlementStatisticsPerSymbol_Internal_SP]
	END
END 
GO

------------------------------
----------------- Nop Pol Term ----------------------------------------
------------------------------

DROP TABLE [dbo].[PersistedSettlementStatisticsPerSymbol] 
GO


CREATE TABLE [dbo].[PersistedSettlementStatisticsPerSymbol](

	[SymbolName] char(7) NOT NULL,
	[T1] [date] NULL,
	[T2] [date] NULL,
	[T3] [date] NULL,
	[T4] [date] NULL,
	[T5] [date] NULL,
	[T6] [date] NULL,
	[T7] [date] NULL,
	[T8] [date] NULL,
	[UnsettledNopNominalBase_T1] [decimal](18, 2) NULL,
	[UnsettledNopNominalBase_T2] [decimal](18, 2) NULL,
	[UnsettledNopNominalBase_T3] [decimal](18, 2) NULL,
	[UnsettledNopNominalBase_T4] [decimal](18, 2) NULL,
	[UnsettledNopNominalBase_T5] [decimal](18, 2) NULL,
	[UnsettledNopNominalBase_T6] [decimal](18, 2) NULL,
	[UnsettledNopNominalBase_T7] [decimal](18, 2) NULL,
	[UnsettledNopNominalBase_T8] [decimal](18, 2) NULL,
	[UnsettledNopNominalTerm_T1] [decimal](18, 2) NULL,
	[UnsettledNopNominalTerm_T2] [decimal](18, 2) NULL,
	[UnsettledNopNominalTerm_T3] [decimal](18, 2) NULL,
	[UnsettledNopNominalTerm_T4] [decimal](18, 2) NULL,
	[UnsettledNopNominalTerm_T5] [decimal](18, 2) NULL,
	[UnsettledNopNominalTerm_T6] [decimal](18, 2) NULL,
	[UnsettledNopNominalTerm_T7] [decimal](18, 2) NULL,
	[UnsettledNopNominalTerm_T8] [decimal](18, 2) NULL,
	[DealsCount_T1] [int] NULL,
	[DealsCount_T2] [int] NULL,
	[DealsCount_T3] [int] NULL,
	[DealsCount_T4] [int] NULL,
	[DealsCount_T5] [int] NULL,
	[DealsCount_T6] [int] NULL,
	[DealsCount_T7] [int] NULL,
	[DealsCount_T8] [int] NULL
) ON [Integrator_DailyData]
GO

GRANT ALTER ON [dbo].[PersistedSettlementStatisticsPerSymbol] TO [IntegratorServiceAccount] AS [dbo]
GRANT SELECT ON [dbo].[PersistedSettlementStatisticsPerSymbol] TO [IntegratorServiceAccount] AS [dbo]
GRANT ALTER ON [dbo].[PersistedSettlementStatisticsPerSymbol] TO [IntegratorKillSwitchUser] AS [dbo]
GRANT SELECT ON [dbo].[PersistedSettlementStatisticsPerSymbol] TO [IntegratorKillSwitchUser] AS [dbo]
GO


ALTER PROCEDURE [dbo].[UpdateSettlementStatisticsPerSymbol_Internal_SP]
AS
BEGIN

	DECLARE @temp_finalSymbol_result TABLE
	(
		[SymbolName] char(7),
		[T1] [date],
		[T2] [date],
		[T3] [date],
		[T4] [date],
		[T5] [date],
		[T6] [date],
		[T7] [date],
		[T8] [date],
		[UnsettledNopNominalBase_T1] [decimal](18, 2),
		[UnsettledNopNominalBase_T2] [decimal](18, 2),
		[UnsettledNopNominalBase_T3] [decimal](18, 2),
		[UnsettledNopNominalBase_T4] [decimal](18, 2),
		[UnsettledNopNominalBase_T5] [decimal](18, 2),
		[UnsettledNopNominalBase_T6] [decimal](18, 2),
		[UnsettledNopNominalBase_T7] [decimal](18, 2),
		[UnsettledNopNominalBase_T8] [decimal](18, 2),
		[UnsettledNopNominalTerm_T1] [decimal](18, 2),
		[UnsettledNopNominalTerm_T2] [decimal](18, 2),
		[UnsettledNopNominalTerm_T3] [decimal](18, 2),
		[UnsettledNopNominalTerm_T4] [decimal](18, 2),
		[UnsettledNopNominalTerm_T5] [decimal](18, 2),
		[UnsettledNopNominalTerm_T6] [decimal](18, 2),
		[UnsettledNopNominalTerm_T7] [decimal](18, 2),
		[UnsettledNopNominalTerm_T8] [decimal](18, 2),
		[DealsCount_T1] [int],
		[DealsCount_T2] [int],
		[DealsCount_T3] [int],
		[DealsCount_T4] [int],
		[DealsCount_T5] [int],
		[DealsCount_T6] [int],
		[DealsCount_T7] [int],
		[DealsCount_T8] [int]
	)

	DECLARE @temp_symbol_result TABLE
	(
		SymbolId tinyint,
		[UnsettledNopNominalBase] [decimal](18, 2),
		[UnsettledNopNominalTerm] [decimal](18, 2),
		[DealsCount] [int],
		[ValueDate] Date
	)

	INSERT INTO
	@temp_symbol_result
	(
		SymbolId,
		[UnsettledNopNominalBase],
		[UnsettledNopNominalTerm],
		DealsCount,
		ValueDate
	)
	SELECT
		SymbolId,
		SUM(SizeBasePol) AS [UnsettledNopNominalBase],
		SUM(SizeTermPol) AS [UnsettledNopNominalTerm],
		SUM(DealsCount),
		SettlementDateLocalMkt AS ValueDate
	FROM
		[dbo].[DealExternalExecutedUnsettled] unsettled
	WHERE
		[SettlementTimeCentralBankUtc] > GetUtcDate()
	GROUP BY
		SettlementDateLocalMkt,
		SymbolId

	DECLARE @temp_ValueDates TABLE
	(
		ValueDate Date,
		ValueDateId int
	)

	INSERT INTO @temp_ValueDates(ValueDateId, ValueDate)
	SELECT
		ROW_NUMBER() OVER (ORDER BY ValueDate),
		ValueDate
	FROM
		@temp_symbol_result
	WHERE
		ValueDate IS NOT NULL
	GROUP BY
		ValueDate


	;WITH TmpStats AS
	(
		SELECT 
			SymbolId,
			[UnsettledNopNominalBase],
			[UnsettledNopNominalTerm],
			DealsCount,
			res.ValueDate,
			'NopBase_T' + CAST(ValueDateId AS VARCHAR) AS NopsBaseValueDateTag,
			'NopTerm_T' + CAST(ValueDateId AS VARCHAR) AS NopsTermValueDateTag,
			'Deals_T' + CAST(ValueDateId AS VARCHAR) AS DealsValueDateTag
	FROM
		@temp_symbol_result res
		FULL OUTER JOIN @temp_ValueDates valDates ON res.ValueDate = valDates.ValueDate
	)

		INSERT INTO @temp_finalSymbol_result
		(
		SymbolName,
		[T1],
		[T2],
		[T3],
		[T4],
		[T5],
		[T6],
		[T7],
		[T8],
		[UnsettledNopNominalBase_T1],
		[UnsettledNopNominalBase_T2],
		[UnsettledNopNominalBase_T3],
		[UnsettledNopNominalBase_T4],
		[UnsettledNopNominalBase_T5],
		[UnsettledNopNominalBase_T6],
		[UnsettledNopNominalBase_T7],
		[UnsettledNopNominalBase_T8],
		[UnsettledNopNominalTerm_T1],
		[UnsettledNopNominalTerm_T2],
		[UnsettledNopNominalTerm_T3],
		[UnsettledNopNominalTerm_T4],
		[UnsettledNopNominalTerm_T5],
		[UnsettledNopNominalTerm_T6],
		[UnsettledNopNominalTerm_T7],
		[UnsettledNopNominalTerm_T8],
		[DealsCount_T1],
		[DealsCount_T2],
		[DealsCount_T3],
		[DealsCount_T4],
		[DealsCount_T5],
		[DealsCount_T6],
		[DealsCount_T7],
		[DealsCount_T8]
		)
		SELECT
		SymbolName,
		[T1],
		[T2],
		[T3],
		[T4],
		[T5],
		[T6],
		[T7],
		[T8],
		[NopBase_T1],
		[NopBase_T2],
		[NopBase_T3],
		[NopBase_T4],
		[NopBase_T5],
		[NopBase_T6],
		[NopBase_T7],
		[NopBase_T8],
		[NopTerm_T1],
		[NopTerm_T2],
		[NopTerm_T3],
		[NopTerm_T4],
		[NopTerm_T5],
		[NopTerm_T6],
		[NopTerm_T7],
		[NopTerm_T8],
		[Deals_T1],
		[Deals_T2],
		[Deals_T3],
		[Deals_T4],
		[Deals_T5],
		[Deals_T6],
		[Deals_T7],
		[Deals_T8]
	FROM
					
		(
			SELECT
				SymbolId,
				MAX(sym.Name) AS SymbolName
			FROM
				@temp_symbol_result res
				INNER JOIN [dbo].[Symbol_Internal] sym ON res.SymbolId = sym.Id
			GROUP BY
				SymbolId
		) executedSymbols

		INNER JOIN

		(
			select T1, T2, T3, T4, T5, T6, T7, T8
			from
			(
				select ValueDate, 'T' + CAST(ValueDateId AS VARCHAR) AS ValueDateTag
				FROM @temp_ValueDates
			) d
			PIVOT
			(
				MAX(ValueDate)
				for ValueDateTag IN (T1, T2, T3, T4, T5, T6, T7, T8)
			) pivDates
		)ValDates ON 1=1
					
		LEFT JOIN

		(
		SELECT 
			SymbolId,
			MAX(NopBase_T1) AS NopBase_T1, MAX(NopBase_T2) AS NopBase_T2, MAX(NopBase_T3) AS NopBase_T3, MAX(NopBase_T4) AS NopBase_T4, MAX(NopBase_T5) AS NopBase_T5, MAX(NopBase_T6) AS NopBase_T6, MAX(NopBase_T7) AS NopBase_T7, MAX(NopBase_T8) AS NopBase_T8
		FROM
			TmpStats
			PIVOT 
			(
				MAX(UnsettledNopNominalBase) FOR NopsBaseValueDateTag IN (NopBase_T1, NopBase_T2, NopBase_T3, NopBase_T4, NopBase_T5, NopBase_T6, NopBase_T7, NopBase_T8)
			)pivNops
		GROUP BY
			SymbolId
		)NopsBaseTmp ON executedSymbols.SymbolId = NopsBaseTmp.SymbolId

		LEFT JOIN

		(
		SELECT 
			SymbolId,
			MAX(NopTerm_T1) AS NopTerm_T1, MAX(NopTerm_T2) AS NopTerm_T2, MAX(NopTerm_T3) AS NopTerm_T3, MAX(NopTerm_T4) AS NopTerm_T4, MAX(NopTerm_T5) AS NopTerm_T5, MAX(NopTerm_T6) AS NopTerm_T6, MAX(NopTerm_T7) AS NopTerm_T7, MAX(NopTerm_T8) AS NopTerm_T8
		FROM
			TmpStats
			PIVOT 
			(
				MAX(UnsettledNopNominalTerm) FOR NopsTermValueDateTag IN (NopTerm_T1, NopTerm_T2, NopTerm_T3, NopTerm_T4, NopTerm_T5, NopTerm_T6, NopTerm_T7, NopTerm_T8)
			)pivNops
		GROUP BY
			SymbolId
		)NopsTermTmp ON executedSymbols.SymbolId = NopsTermTmp.SymbolId

		LEFT JOIN

		(
		SELECT 
			SymbolId,
			MAX(Deals_T1) AS Deals_T1, MAX(Deals_T2) AS Deals_T2, MAX(Deals_T3) AS Deals_T3, MAX(Deals_T4) AS Deals_T4, MAX(Deals_T5) AS Deals_T5, MAX(Deals_T6) AS Deals_T6, MAX(Deals_T7) AS Deals_T7, MAX(Deals_T8) AS Deals_T8
		FROM
			TmpStats
			PIVOT 
			(
				MAX(DealsCount) FOR DealsValueDateTag IN (Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8)
			)piv
		GROUP BY
			SymbolId
		)DealsTmp

		ON NopsBaseTmp.SymbolId = DealsTmp.SymbolId

		ORDER BY
			SymbolName
		
			
		BEGIN TRANSACTION
			TRUNCATE TABLE [dbo].[PersistedSettlementStatisticsPerSymbol]
		
			INSERT INTO
				[dbo].[PersistedSettlementStatisticsPerSymbol]
				(
					[SymbolName],
					[T1],
					[T2],
					[T3],
					[T4],
					[T5],
					[T6],
					[T7],
					[T8],
					[UnsettledNopNominalBase_T1],
					[UnsettledNopNominalBase_T2],
					[UnsettledNopNominalBase_T3],
					[UnsettledNopNominalBase_T4],
					[UnsettledNopNominalBase_T5],
					[UnsettledNopNominalBase_T6],
					[UnsettledNopNominalBase_T7],
					[UnsettledNopNominalBase_T8],
					[UnsettledNopNominalTerm_T1],
					[UnsettledNopNominalTerm_T2],
					[UnsettledNopNominalTerm_T3],
					[UnsettledNopNominalTerm_T4],
					[UnsettledNopNominalTerm_T5],
					[UnsettledNopNominalTerm_T6],
					[UnsettledNopNominalTerm_T7],
					[UnsettledNopNominalTerm_T8],
					[DealsCount_T1],
					[DealsCount_T2],
					[DealsCount_T3],
					[DealsCount_T4],
					[DealsCount_T5],
					[DealsCount_T6],
					[DealsCount_T7],
					[DealsCount_T8]
				)
			SELECT
				[SymbolName],
				[T1],
				[T2],
				[T3],
				[T4],
				[T5],
				[T6],
				[T7],
				[T8],
				[UnsettledNopNominalBase_T1],
				[UnsettledNopNominalBase_T2],
				[UnsettledNopNominalBase_T3],
				[UnsettledNopNominalBase_T4],
				[UnsettledNopNominalBase_T5],
				[UnsettledNopNominalBase_T6],
				[UnsettledNopNominalBase_T7],
				[UnsettledNopNominalBase_T8],
				[UnsettledNopNominalTerm_T1],
				[UnsettledNopNominalTerm_T2],
				[UnsettledNopNominalTerm_T3],
				[UnsettledNopNominalTerm_T4],
				[UnsettledNopNominalTerm_T5],
				[UnsettledNopNominalTerm_T6],
				[UnsettledNopNominalTerm_T7],
				[UnsettledNopNominalTerm_T8],
				[DealsCount_T1],
				[DealsCount_T2],
				[DealsCount_T3],
				[DealsCount_T4],
				[DealsCount_T5],
				[DealsCount_T6],
				[DealsCount_T7],
				[DealsCount_T8]
			FROM
				@temp_finalSymbol_result
		COMMIT
END 
GO

ALTER PROCEDURE [dbo].[GetSettlementStatisticsPerSymbol_Cached_SP]
AS
BEGIN

	SELECT 
		[SymbolName]
		,[T1]
		,[T2]
		,[T3]
		,[T4]
		,[T5]
		,[T6]
		,[T7]
		,[T8]
		,[UnsettledNopNominalBase_T1]
		,[UnsettledNopNominalBase_T2]
		,[UnsettledNopNominalBase_T3]
		,[UnsettledNopNominalBase_T4]
		,[UnsettledNopNominalBase_T5]
		,[UnsettledNopNominalBase_T6]
		,[UnsettledNopNominalBase_T7]
		,[UnsettledNopNominalBase_T8]
		,[UnsettledNopNominalTerm_T1]
		,[UnsettledNopNominalTerm_T2]
		,[UnsettledNopNominalTerm_T3]
		,[UnsettledNopNominalTerm_T4]
		,[UnsettledNopNominalTerm_T5]
		,[UnsettledNopNominalTerm_T6]
		,[UnsettledNopNominalTerm_T7]
		,[UnsettledNopNominalTerm_T8]
		,[DealsCount_T1]
		,[DealsCount_T2]
		,[DealsCount_T3]
		,[DealsCount_T4]
		,[DealsCount_T5]
		,[DealsCount_T6]
		,[DealsCount_T7]
		,[DealsCount_T8]
	FROM
		[dbo].[PersistedSettlementStatisticsPerSymbol]
	ORDER BY
		[SymbolName] ASC
END
GO


----------------------------------
-- Active state to system view ---
----------------------------------

ALTER PROCEDURE [dbo].[GetVenueCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()
	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]
	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumDiscountBasisPointsToTrade] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumDiscountToTradeDecimal],
		sett.[MinimumTimeBetweenSingleCounterpartySignals_seconds],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[MinimumFillSize],
		action.[Enabled],
		@TradingOn & action.[Enabled] AS Active,
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueGliderSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()
	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]
	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[MinimumBPGrossGain],
		sett.[MinimumBPGrossGain] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossGainDecimal],
		sett.[PreferLmaxDuringHedging],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumIntegratorGlidePriceLife_milliseconds],
		sett.[GlidingCutoffSpan_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[ReferenceLmaxBookOnly],
		glidingCounterparty.CounterpartyCode AS GlidingCounterparty,
		sett.[BidEnabled],
		sett.[AskEnabled],
		sett.[MinimumBPGrossGainIfZeroGlideFill],
		sett.[MinimumBPGrossGainIfZeroGlideFill] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossGainIfZeroGlideFillDecimal],
		sett.[PreferLLDestinationCheckToLmax],
		action.[Enabled],
		@TradingOn & action.[Enabled] AS Active,
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		INNER JOIN [dbo].[Counterparty] glidingCounterparty ON sett.[GlidingCounterpartyId] = glidingCounterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueMMSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()
	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]
	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[BidEnabled],
		sett.[AskEnabled],
		action.[Enabled],
		@TradingOn & action.[Enabled] AS Active,
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()
	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]
	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FixedSpreadBasisPoints],
		sett.[FixedSpreadBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [FixedSpreadDecimal],
		sett.[StopLossNetInUsd],
		spreadType.[VenueQuotingSettingsSpreadTypeName] AS SpreadType,
		sett.[MinimumDynamicSpreadBp],
		sett.[MinimumDynamicSpreadBp] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumDynamicSpreadDecimal],
		sett.[DynamicMarkupDecimal],
		sett.[EnableSkew],
		sett.[SkewDecimal],
		sett.[InactivityTimeoutSec],
		sett.[LiquidationSpreadBp],
		sett.[LiquidationSpreadBp] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [LiquidationSpreadDecimal],
		sett.[LmaxTickerLookbackSec],
		sett.[MinimumLmaxTickerVolumeUsd],
		sett.[MaximumLmaxTickerVolumeUsd],
		sett.[MinimumLmaxTickerAvgDealSizeUsd],
		sett.[MaximumLmaxTickerAvgDealSizeUsd],
		integratorStats.[CurrentLmaxTickerVolumeUsd],
		integratorStats.[CurrentLmaxTickerAvgDealSizeUsd],
		integratorStats.[CalculatedFromFullInterval],
		integratorStats.[InactivityRestrictionOn],
		integratorStats.[LTBandRestrictionOn],
		integratorStats.[CurrentSpreadDecimal],
		integratorStats.[CurrentLmaxTickerPaidVolumeRatio],
		action.[Enabled],
		@TradingOn & action.[Enabled] AS Active,
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		INNER JOIN [dbo].[VenueQuotingSettingsSpreadType] spreadType ON sett.[SpreadTypeId] = spreadType.[VenueQuotingSettingsSpreadTypeId]
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
		LEFT JOIN [dbo].[VenueQuotingIntegratorStats] integratorStats ON integratorStats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueSmartCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()
	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]
	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumDiscountBasisPointsToTrade] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumDiscountToTradeDecimal],
		sett.[MinimumTimeBetweenSingleCounterpartySignals_seconds],
		sett.[MinimumFillSize],
		sett.[CoverDistanceGrossBasisPoints],
		sett.[CoverDistanceGrossBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [CoverDistanceGrossDecimal],	
		sett.[CoverTimeout_milliseconds],
		sett.[MinimumTrueGainGrossBasisPoints],
		sett.[MinimumTrueGainGrossBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumTrueGainGrossDecimal],	
		action.[Enabled],
		@TradingOn & action.[Enabled] AS Active,
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueSmartCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueStreamSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()
	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]
	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[PreferLLDestinationCheckToLmax],
		sett.[MinimumBPGrossToAccept],
		sett.[MinimumBPGrossToAccept] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossToAcceptDecimal],
		sett.[PreferLmaxDuringHedging],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[ReferenceLmaxBookOnly],
		sett.[BidEnabled],
		sett.[AskEnabled],
		action.[Enabled],
		@TradingOn & action.[Enabled] AS Active,
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

-------------------------------------
---- NOP Risk Check -----------------
-------------------------------------

TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
GO

ALTER TABLE [dbo].[DealExternalExecutedUnsettled] ADD TradeDateUtc [DATE] NOT NULL
GO



ALTER PROCEDURE [dbo].[ForceRefreshAllUnsettledDeals_SP]
AS
BEGIN

	DECLARE @temp_unsettledDeals TABLE
	(
		[CounterpartyId] [tinyint],
		[SymbolId] [tinyint],
		[SettlementDateLocalMkt] [date],
		[TradeDateUtc] [date],
		[SettlementTimeCounterpartyUtc] [DateTime],
		[SettlementTimeCentralBankUtc] [DateTime],
		[SizeBasePol] [decimal](18, 2),
		[SizeTermPol] [decimal](18, 4),
		[DealsCount] [int]
	)

	INSERT INTO
		@temp_unsettledDeals
		(
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[TradeDateUtc],
			[SettlementTimeCounterpartyUtc],
			[SettlementTimeCentralBankUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		)
	SELECT
		[CounterpartyId],
		[SymbolId],
		[ValueDate],
		[TradeDateUtc],
		[SettlementTimeCounterpartyUtc],
		[SettlementTimeCentralBankUtc],
		SUM([SizeBasePol]),
		SUM([SizeTermPol]),
		--count arbitrary column
		COUNT([CounterpartyId])
	FROM
		(
		SELECT 
			deal.[CounterpartyId]
			,deal.[SymbolId]
			,deal.[ValueDateCounterpartySuppliedLocMktDate] AS [ValueDate]
			,CAST (deal.IntegratorReceivedExecutionReportUtc AS DATE) AS [TradeDateUtc]
			,deal.[SettlementTimeCounterpartyUtc]
			,deal.[SettlementTimeCentralBankUtc]
			,deal.[AmountBasePolExecuted] AS [SizeBasePol]
			,-deal.[AmountBasePolExecuted]*deal.[Price] AS [SizeTermPol]
		FROM 
			[dbo].[DealExternalExecuted] deal
		WHERE
			IntegratorReceivedExecutionReportUtc > DATEADD(DAY, -10, GETUTCDATE())
			AND
			([SettlementTimeCounterpartyUtc] > GetUtcDate() OR [SettlementTimeCentralBankUtc] > GetUtcDate())

		UNION ALL

		SELECT 
			[CounterpartyId]
			,[SymbolId]
			,[ValueDate]
			,CAST (IntegratorReceivedExternalOrderUtc AS DATE) AS [TradeDateUtc]
			,[SettlementTimeCounterpartyUtc]
			,[SettlementTimeCentralBankUtc]
			,[IntegratorExecutedAmountBasePol] AS [SizeBasePol]
			,-[IntegratorExecutedAmountBasePol]*[CounterpartyRequestedPrice] AS [SizeTermPol]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]
		WHERE
			[IntegratorExecutedAmountBasePol] != 0

		UNION ALL

		SELECT 
			[CounterpartyId]
			,[SymbolId]
			,[ValueDate]
			,CAST (IntegratorReceivedExternalOrderUtc AS DATE) AS [TradeDateUtc]
			,[SettlementTimeCounterpartyUtc]
			,[SettlementTimeCentralBankUtc]
			,[IntegratorExecutedAmountBasePol] AS [SizeBasePol]
			,-[IntegratorExecutedAmountBasePol]*[CounterpartyRequestedPrice] AS [SizeTermPol]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Archive] deal
		WHERE
			[IntegratorReceivedExternalOrderUtc] > DATEADD(DAY, -10, GETUTCDATE())
			AND
			[IntegratorExecutedAmountBasePol] != 0
			AND
			([SettlementTimeCounterpartyUtc] > GetUtcDate() OR [SettlementTimeCentralBankUtc] > GetUtcDate())
		) allUnsettledDeals
	GROUP BY
		[SettlementTimeCounterpartyUtc],
		[SettlementTimeCentralBankUtc],
		[ValueDate],
		[TradeDateUtc],
		[CounterpartyId],
		[SymbolId]

		
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
	
		INSERT INTO
			[dbo].[DealExternalExecutedUnsettled]
			(
				[CounterpartyId],
				[SymbolId],
				[SettlementDateLocalMkt],
				[TradeDateUtc],
				[SettlementTimeCounterpartyUtc],
				[SettlementTimeCentralBankUtc],
				[SizeBasePol],
				[SizeTermPol],
				[DealsCount]
			)
		SELECT
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[TradeDateUtc],
			[SettlementTimeCounterpartyUtc],
			[SettlementTimeCentralBankUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		FROM
			@temp_unsettledDeals
	COMMIT	
END
GO

exec [dbo].[ForceRefreshAllUnsettledDeals_SP]
GO

ALTER PROCEDURE [dbo].[Daily_PurgeAndRollupUnsettledDeals_SP]
AS
BEGIN

	DECLARE @temp_unsettledDeals TABLE
	(
		[CounterpartyId] [tinyint],
		[SymbolId] [tinyint],
		[SettlementDateLocalMkt] [date],
		[TradeDateUtc] [date],
		[SettlementTimeCounterpartyUtc] [DateTime],
		[SettlementTimeCentralBankUtc] [DateTime],
		[SizeBasePol] [decimal](18, 2),
		[SizeTermPol] [decimal](18, 4),
		[DealsCount] [int]
	)

	INSERT INTO
		@temp_unsettledDeals
		(
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[TradeDateUtc],
			[SettlementTimeCounterpartyUtc],
			[SettlementTimeCentralBankUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		)
	SELECT 
		[CounterpartyId]
		,[SymbolId]
		,[SettlementDateLocalMkt]
		,[TradeDateUtc]
		,[SettlementTimeCounterpartyUtc]
		,[SettlementTimeCentralBankUtc]
		,SUM([SizeBasePol]) AS [NopBasePol]
		,SUM([SizeTermPol]) AS [NopTermPol]
		,SUM([DealsCount]) AS [DealsCount]
	FROM
		[dbo].[DealExternalExecutedUnsettled] unsettled				
	WHERE
		[SettlementTimeCounterpartyUtc] > GetUtcDate() OR [SettlementTimeCentralBankUtc] > GetUtcDate()
	GROUP BY
		[SettlementTimeCounterpartyUtc],
		[SettlementTimeCentralBankUtc],
		[SettlementDateLocalMkt],
		[TradeDateUtc],
		[CounterpartyId],
		[SymbolId]
		
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
	
		INSERT INTO
			[dbo].[DealExternalExecutedUnsettled]
			(
				[CounterpartyId],
				[SymbolId],
				[SettlementDateLocalMkt],
				[TradeDateUtc],
				[SettlementTimeCounterpartyUtc],
				[SettlementTimeCentralBankUtc],
				[SizeBasePol],
				[SizeTermPol],
				[DealsCount]
			)
		SELECT
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[TradeDateUtc],
			[SettlementTimeCounterpartyUtc],
			[SettlementTimeCentralBankUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		FROM
			@temp_unsettledDeals
	COMMIT	
END
GO

ALTER PROCEDURE [dbo].[GetUnsettledDealsPerCreditLineSettlementAndCurrency_SP]
AS
BEGIN
	SELECT 
		ctp.[CounterpartyCode] AS Counterparty,
		sym.[Name] AS Symbol,
		SettlementDateLocalMkt AS SettlementDateLocal,
		[TradeDateUtc],
		SettlementTimeCounterpartyUtc,
		SUM(unsettled.SizeBasePol) AS SizeBasePol,
		SUM(unsettled.SizeTermPol) AS SizeTermPol
	FROM
		[dbo].[DealExternalExecutedUnsettled] unsettled
		INNER JOIN [dbo].[Counterparty] ctp ON unsettled.[CounterpartyId] = ctp.[CounterpartyId]
		INNER JOIN Symbol sym ON sym.ID = unsettled.[SymbolId]				
	WHERE
		[SettlementTimeCounterpartyUtc] >= GETUTCDATE()
	GROUP BY
		[SettlementTimeCounterpartyUtc],
		[SettlementDateLocalMkt],
		[TradeDateUtc],
		ctp.[CounterpartyCode],
		sym.[Name]
END
GO

ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 2),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@ValueDateCounterpartySuppliedRecalcedToCounterpartyUtc DATETIME,
	@ValueDateCounterpartySuppliedRecalcedToCentralBankUtc DATETIME,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL,
	@SingleFillSystemId int = NULL,
	@MultipleFillsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL,
	@CounterpartyClientId varchar(20) = NULL)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @MultipleFillsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleFillsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external deal for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@SingleFillSystemId, -1)
				,@AmountBasePolExecuted
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc)
		END
		
		INSERT INTO 
		[dbo].[DealExternalExecuted]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolExecuted]
			   ,[AmountTermPolExecutedInUsd]
			   ,[Price]
			   ,[CounterpartyTransactionId]
			   ,[CounterpartySuppliedExecutionTimeStampUtc]
			   ,[ValueDateCounterpartySuppliedLocMktDate]
			   ,[SettlementTimeCounterpartyUtc]
			   ,[SettlementTimeCentralBankUtc]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[InternalTransactionIdentity]
			   ,[FlowSideId]
			   ,[CounterpartyClientId])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolExecuted
			   ,@AmountTermPolExecutedInUsd
			   ,@Price
			   ,@CounterpartyTransactionId
			   ,@CounterpartyExecutionReportTimeStampUtc
			   ,@ValueDateCounterpartySuppliedLocMktDate
			   ,@ValueDateCounterpartySuppliedRecalcedToCounterpartyUtc
			   ,@ValueDateCounterpartySuppliedRecalcedToCentralBankUtc
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@CounterpartySentExecutionReportUtc
			   ,@InternalDealId
			   ,@FlowSideId
			   ,@CounterpartyClientId)
			   
		INSERT INTO [dbo].[DealExternalExecutedUnsettled]
           ([CounterpartyId]
           ,[SymbolId]
           ,[SettlementDateLocalMkt]
		   ,[TradeDateUtc]
		   ,[SettlementTimeCounterpartyUtc]
		   ,[SettlementTimeCentralBankUtc]
           ,[SizeBasePol]
           ,[SizeTermPol]
		   ,[DealsCount])
		VALUES
           (@CounterpartyId
           ,@SymbolId
           ,@ValueDateCounterpartySuppliedLocMktDate
		   ,CAST (@ExecutionReportReceivedUtc AS DATE)
		   ,@ValueDateCounterpartySuppliedRecalcedToCounterpartyUtc
		   ,@ValueDateCounterpartySuppliedRecalcedToCentralBankUtc
           ,@AmountBasePolExecuted
           ,-@AmountBasePolExecuted*@Price
		   ,1)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO


ALTER PROCEDURE [dbo].[InsertNewOrderExternalIncomingRejectable_SP]( 
    @ExternalOrderReceived [datetime2](7),
	@ExternalOrderSentUtc [datetime2](7),
	@IntegratorSentExecutionReportUtc [datetime2](7),
	@CounterpartyClientOrderId [varchar](32),
	@CounterpartyClientId [varchar](20),
	@TradingSystemId int = NULL,
	@SourceIntegratorPriceIdentity [varchar](20) = NULL,
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@RequestedFillMinimumBaseAbs decimal(18,2) = NULL,
	@IntegratorExecId [varchar](20) = NULL,
	@CounterpartyExecId [varchar](32) = NULL,
	@ValueDate [date] = NULL,
	@SettlementTimeCounterpartyUtc [datetime] = NULL,
	@SettlementTimeCentralBankUtc [datetime] = NULL,
	@OrderReceivedToExecutionReportSentLatency [time](7),
	@IntegratorExecutedAmountBaseAbs [decimal](18, 2) = NULL,
	@IntegratorRejectedAmountBaseAbs [decimal](18, 2) = NULL,
	@RejectionReason [varchar](32) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WITH(NOLOCK) WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WITH(NOLOCK) WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WITH(NOLOCK) WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @RejectionReason IS NOT NULL
	BEGIN
		SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
		IF @@ROWCOUNT <> 1
		BEGIN
			INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
			SET @RejectionReasonId = @@IDENTITY
		END
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @IntegratorExecutedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,COALESCE(@IntegratorExecId, @CounterpartyExecId)
				,@ExternalOrderReceived)
		END
		ELSE IF @IntegratorRejectedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,ISNULL(@RejectionReason, '<NONE>')
				,COALESCE(@IntegratorExecId, @CounterpartyExecId)
				,@RejectionDirectionId
				,@ExternalOrderReceived)
		END

		INSERT INTO 
			[dbo].[OrderExternalIncomingRejectable_Daily]
			   ([IntegratorReceivedExternalOrderUtc]
			   ,[CounterpartySentExternalOrderUtc]
			   ,[IntegratorSentExecutionReportUtc]
			   ,[CounterpartyClientOrderId]
			   ,[CounterpartyClientId]
			   ,[TradingSystemId]
			   ,[SourceIntegratorPriceIdentity]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorDealDirectionId]
			   ,[CounterpartyRequestedPrice]
			   ,[CounterpartyRequestedAmountBasePol]
			   ,[CounterpartyRequestedFillMinimumBaseAbs]
			   ,[IntegratorExecutionId]
			   ,[CounterpartyExecutionId]
			   ,[ValueDate]
			   ,[SettlementTimeCounterpartyUtc]
			   ,[SettlementTimeCentralBankUtc]
			   ,[OrderReceivedToExecutionReportSentLatency ]
			   ,[IntegratorExecutedAmountBasePol]
			   ,[IntegratorRejectedAmountBasePol]
			   ,[IntegratorRejectionReasonId])
		 VALUES
			   (@ExternalOrderReceived
			   ,@ExternalOrderSentUtc
			   ,@IntegratorSentExecutionReportUtc
			   ,@CounterpartyClientOrderId
			   ,@CounterpartyClientId
			   ,@TradingSystemId
			   ,@SourceIntegratorPriceIdentity
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@DealDirectionId
			   ,@RequestedPrice
			   ,CASE WHEN @DealDirectionId = 1 THEN -@RequestedAmountBaseAbs ELSE @RequestedAmountBaseAbs END
			   ,@RequestedFillMinimumBaseAbs
			   ,@IntegratorExecId
			   ,@CounterpartyExecId
			   ,@ValueDate
			   ,@SettlementTimeCounterpartyUtc
			   ,@SettlementTimeCentralBankUtc
			   ,@OrderReceivedToExecutionReportSentLatency 
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
		       ,@RejectionReasonId)
			   
		IF @ValueDate IS NOT NULL AND @IntegratorExecutedAmountBaseAbs != 0
		BEGIN
			INSERT INTO [dbo].[DealExternalExecutedUnsettled]
			   ([CounterpartyId]
			   ,[SymbolId]
			   ,[SettlementDateLocalMkt]
			   ,[TradeDateUtc]
			   ,[SettlementTimeCounterpartyUtc]
			   ,[SettlementTimeCentralBankUtc]
			   ,[SizeBasePol]
			   ,[SizeTermPol]
			   ,[DealsCount])
			VALUES
			   (@CounterpartyId
			   ,@SymbolId
			   ,@ValueDate
			   ,CAST (@ExternalOrderReceived AS DATE)
			   ,@SettlementTimeCounterpartyUtc
			   ,@SettlementTimeCentralBankUtc
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN @IntegratorExecutedAmountBaseAbs ELSE -@IntegratorExecutedAmountBaseAbs END * @RequestedPrice
			   ,1)
		END
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH	   
END
GO

--- Allow the PrimeBrokerCreditLine to have also NULL CreditLineId values indicating setting for entire PB ---

ALTER TABLE [dbo].[PrimeBrokerCreditLine] DROP CONSTRAINT [UQ_CreditLineName]
GO

ALTER TABLE [dbo].[PrimeBrokerCreditLineNopCheckingZones] DROP CONSTRAINT [FK_PrimeBrokerCreditLineNopCheckingZones_CreditLineId]
GO

ALTER TABLE [dbo].[PrimeBrokerCreditLines_CptMapping] DROP CONSTRAINT [FK_PrimeBrokerCreditLines_CptMapping_CreditLineId]
GO

DROP INDEX [PK_PrimeBrokerCreditLines] ON [dbo].[PrimeBrokerCreditLine] WITH ( ONLINE = OFF )
GO

ALTER TABLE [dbo].[PrimeBrokerCreditLine] ADD [CreditLineId_2] [tinyint] NULL
GO
UPDATE [dbo].[PrimeBrokerCreditLine] SET [CreditLineId_2] = [CreditLineId]
GO
ALTER TABLE [dbo].[PrimeBrokerCreditLine] DROP COLUMN [CreditLineId]
GO
exec sp_rename '[dbo].[PrimeBrokerCreditLine].[CreditLineId_2]', 'CreditLineId'
GO

ALTER TABLE [dbo].[PrimeBrokerCreditLine] ALTER COLUMN [CreditLineName] [varchar](255) NULL
ALTER TABLE [dbo].[PrimeBrokerCreditLine] ALTER COLUMN [PrimeBrokerId] [tinyint] NULL
GO


CREATE CLUSTERED INDEX [PK_PrimeBrokerCreditLines] ON [dbo].[PrimeBrokerCreditLine]([CreditLineId])
GO
CREATE UNIQUE INDEX [UQ_CreditLineId] ON [dbo].[PrimeBrokerCreditLine]([CreditLineId]) WHERE [CreditLineId] IS NOT NULL
GO

--ALTER TABLE [dbo].[PrimeBrokerCreditLineNopCheckingZones]  WITH CHECK ADD  CONSTRAINT [FK_PrimeBrokerCreditLineNopCheckingZones_CreditLineId] FOREIGN KEY([CreditLineId])
--REFERENCES [dbo].[PrimeBrokerCreditLine] ([CreditLineId])
--GO

--ALTER TABLE [dbo].[PrimeBrokerCreditLineNopCheckingZones] CHECK CONSTRAINT [FK_PrimeBrokerCreditLineNopCheckingZones_CreditLineId]
--GO

--ALTER TABLE [dbo].[PrimeBrokerCreditLines_CptMapping]  WITH CHECK ADD  CONSTRAINT [FK_PrimeBrokerCreditLines_CptMapping_CreditLineId] FOREIGN KEY([CreditLineId])
--REFERENCES [dbo].[PrimeBrokerCreditLine] ([CreditLineId])
--GO

--ALTER TABLE [dbo].[PrimeBrokerCreditLines_CptMapping] CHECK CONSTRAINT [FK_PrimeBrokerCreditLines_CptMapping_CreditLineId]
--GO

CREATE UNIQUE INDEX [UQ_CreditLineName] ON [dbo].[PrimeBrokerCreditLine]([CreditLineName]) WHERE [CreditLineName] IS NOT NULL;
CREATE UNIQUE INDEX [UQ_CreditPbPerNull] ON [dbo].[PrimeBrokerCreditLine]([PrimeBrokerId]) WHERE [CreditLineId] IS NULL;

ALTER TABLE [dbo].[PrimeBrokerCreditLine]
ADD CONSTRAINT CheckPrimeBrokerNotNullIfCreditLineNotNull
CHECK ([PrimeBrokerId] IS NOT NULL OR [CreditLineId] IS NULL)

GO


ALTER PROCEDURE [dbo].[GetCreditLineCptMap_SP]
AS
BEGIN
	SELECT 
		creditLine.CreditLineId
		,creditLine.CreditLineName
		,cpt.CounterpartyCode AS Counterparty
	FROM 
		[dbo].[PrimeBrokerCreditLines_CptMapping] map
		INNER JOIN [dbo].[PrimeBrokerCreditLine] creditLine ON map.CreditLineId = creditLine.CreditLineId
		INNER JOIN [dbo].[Counterparty] cpt ON map.[CounterpartyId] = cpt.[CounterpartyId]
	WHERE
		creditLine.CreditLineId IS NOT NULL
	ORDER BY
		creditLine.CreditLineName ASC,
		cpt.CounterpartyCode ASC
END
GO

CREATE PROCEDURE [dbo].[GetPrimeBrokerCptMap_SP]
AS
BEGIN
	SELECT 
		creditLine.PrimeBrokerId
		,pb.PrimeBrokerName
		,cpt.CounterpartyCode AS Counterparty
	FROM 
		[dbo].[PrimeBrokerCreditLines_CptMapping] map
		INNER JOIN [dbo].[PrimeBrokerCreditLine] creditLine ON map.CreditLineId = creditLine.CreditLineId
		INNER JOIN [dbo].[PrimeBroker] pb ON creditLine.[PrimeBrokerId] = pb.[PrimeBrokerId]
		INNER JOIN [dbo].[Counterparty] cpt ON map.[CounterpartyId] = cpt.[CounterpartyId]
	ORDER BY
		creditLine.CreditLineName ASC,
		cpt.CounterpartyCode ASC
END
GO

GRANT EXECUTE ON [dbo].[GetPrimeBrokerCptMap_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetPrimeBrokerCptMap_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROCEDURE [dbo].[GetCreditLineSettings_SP]
AS
BEGIN
	SELECT 
		creditLine.PrimeBrokerId
		,creditLine.CreditLineId
		,creditLine.CreditLineName
		,creditLine.MaxUnsettledNopUsdTotal
		,creditLine.MaxUnsettledNopUsdTotalToLockDestination
		,creditLine.MaxUnsettledNopUsdPerValueDate
		,creditLine.MaxUnsettledNopUsdPerValueDateToLockDestination
	FROM 
		[dbo].[PrimeBrokerCreditLine] creditLine
	ORDER BY
		creditLine.CreditLineId ASC
END
GO

CREATE PROCEDURE [dbo].[PrimeBrokerCreditLine_UpdatePBLimit_SP]
(
	@PrimeBrokerId tinyint = NULL,
	@NewUnsettledLimitUsdTotal decimal(18,2),
	@NewUnsettledLimitUsdTotalToLockDestiantion decimal(18,2)
)
AS
BEGIN
	UPDATE 
		[dbo].[PrimeBrokerCreditLine]
	SET
		[MaxUnsettledNopUsdTotal] = @NewUnsettledLimitUsdTotal
		,[MaxUnsettledNopUsdTotalToLockDestination] = @NewUnsettledLimitUsdTotalToLockDestiantion
	WHERE
		([PrimeBrokerId] = @PrimeBrokerId AND [CreditLineId] IS NULL)
		OR 
		([PrimeBrokerId] IS NULL AND @PrimeBrokerId IS NULL)
		AND [CreditLineId] IS NULL
END
GO

GRANT EXECUTE ON [dbo].[PrimeBrokerCreditLine_UpdatePBLimit_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

INSERT INTO [dbo].[PrimeBrokerCreditLine]
           ([PrimeBrokerId]
           ,[CreditLineName]
           ,[MaxUnsettledNopUsdTotal]
           ,[MaxUnsettledNopUsdTotalToLockDestination]
           ,[MaxUnsettledNopUsdPerValueDate]
           ,[MaxUnsettledNopUsdPerValueDateToLockDestination]
           ,[CreditLineId])
     VALUES
           (NULL
           ,NULL
           ,600000000
           ,598000000
           ,0
           ,0
           ,NULL)
GO

CREATE TABLE [dbo].[PrimeBrokerNopCheckingZones](
	[PrimeBrokerId] [tinyint] NULL,
	[NopCheckingZoneId] [tinyint] NOT NULL
) ON [Integrator_DailyData]

GO

ALTER TABLE [dbo].[PrimeBrokerNopCheckingZones]  WITH CHECK ADD  CONSTRAINT [FK_PrimeBrokerNopCheckingZones_NopCheckingZoneId] FOREIGN KEY([NopCheckingZoneId])
REFERENCES [dbo].[NopCheckingZone] ([NopCheckingZoneId])
GO
ALTER TABLE [dbo].[PrimeBrokerNopCheckingZones] CHECK CONSTRAINT [FK_PrimeBrokerNopCheckingZones_NopCheckingZoneId]
GO

ALTER TABLE [dbo].[PrimeBrokerNopCheckingZones]  WITH CHECK ADD  CONSTRAINT [FK_PrimeBrokerNopCheckingZones_PrimeBrokerId] FOREIGN KEY([PrimeBrokerId])
REFERENCES [dbo].[PrimeBroker] ([PrimeBrokerId])
GO
ALTER TABLE [dbo].[PrimeBrokerNopCheckingZones] CHECK CONSTRAINT [FK_PrimeBrokerNopCheckingZones_PrimeBrokerId]
GO

CREATE PROCEDURE [dbo].[UpdatePrimeBrokerNopCheckingZones_SP]
(
	@PrimeBrokerId [tinyint],
	@NopCheckingZoneName [varchar](20)
)
AS
BEGIN

	DECLARE @NopCheckingZoneId INT

	SELECT @NopCheckingZoneId = [NopCheckingZoneId] FROM [dbo].[NopCheckingZone] WHERE [NopCheckingZoneName] = @NopCheckingZoneName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'NopCheckingZone not found in DB: %s', 16, 2, @NopCheckingZoneName) WITH SETERROR
	END
	
	--WARNING - this doesn't solve concurrency as it expects to be executed serializable once a while
	-- errors are acceptable thought (otherwise we'd need transaction + table locking)
	
	UPDATE
		[dbo].[PrimeBrokerNopCheckingZones]
	SET
		[NopCheckingZoneId] = @NopCheckingZoneId
	WHERE
		[PrimeBrokerId] = @PrimeBrokerId
		OR 
		([PrimeBrokerId] IS NULL AND @PrimeBrokerId IS NULL)
			
	IF @@rowcount = 0
	BEGIN
		INSERT INTO [dbo].[PrimeBrokerNopCheckingZones] 
			([PrimeBrokerId],
			[NopCheckingZoneId])
		VALUES
			(@PrimeBrokerId,
			@NopCheckingZoneId)
	END
END
GO

GRANT EXECUTE ON [dbo].[UpdatePrimeBrokerNopCheckingZones_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


ALTER PROCEDURE [dbo].[Daily_PurgeAndRollupUnsettledDeals_SP]
AS
BEGIN

	DECLARE @temp_unsettledDeals TABLE
	(
		[CounterpartyId] [tinyint],
		[SymbolId] [tinyint],
		[SettlementDateLocalMkt] [date],
		[TradeDateUtc] [date],
		[SettlementTimeCounterpartyUtc] [DateTime],
		[SettlementTimeCentralBankUtc] [DateTime],
		[SizeBasePol] [decimal](18, 2),
		[SizeTermPol] [decimal](18, 4),
		[DealsCount] [int]
	)

	INSERT INTO
		@temp_unsettledDeals
		(
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[TradeDateUtc],
			[SettlementTimeCounterpartyUtc],
			[SettlementTimeCentralBankUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		)
	SELECT 
		[CounterpartyId]
		,[SymbolId]
		,[SettlementDateLocalMkt]
		,[TradeDateUtc]
		,[SettlementTimeCounterpartyUtc]
		,[SettlementTimeCentralBankUtc]
		,SUM([SizeBasePol]) AS [NopBasePol]
		,SUM([SizeTermPol]) AS [NopTermPol]
		,SUM([DealsCount]) AS [DealsCount]
	FROM
		[dbo].[DealExternalExecutedUnsettled] unsettled				
	WHERE
		[SettlementTimeCounterpartyUtc] > GetUtcDate() OR [SettlementTimeCentralBankUtc] > GetUtcDate()
	GROUP BY
		[SettlementTimeCounterpartyUtc],
		[SettlementTimeCentralBankUtc],
		[SettlementDateLocalMkt],
		[TradeDateUtc],
		[CounterpartyId],
		[SymbolId]
		
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
	
		INSERT INTO
			[dbo].[DealExternalExecutedUnsettled]
			(
				[CounterpartyId],
				[SymbolId],
				[SettlementDateLocalMkt],
				[TradeDateUtc],
				[SettlementTimeCounterpartyUtc],
				[SettlementTimeCentralBankUtc],
				[SizeBasePol],
				[SizeTermPol],
				[DealsCount]
			)
		SELECT
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[TradeDateUtc],
			[SettlementTimeCounterpartyUtc],
			[SettlementTimeCentralBankUtc],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		FROM
			@temp_unsettledDeals
	COMMIT	
	
		--And purge old settlement date zone infos
	DELETE FROM [dbo].[PrimeBrokerCreditLineNopCheckingZones] WHERE [SettlementDate] < (SELECT MIN([SettlementDate]) FROM [dbo].[LastCurrentlyUnsettledSettlementDate])
	TRUNCATE TABLE [dbo].[PrimeBrokerNopCheckingZones]
END
GO


CREATE TABLE [dbo].[PersistedSettlementStatisticsPerPrimeBroker](
	[PrimeBrokerName] [varchar](10) NULL,
	[PrimeBrokerId] [tinyint] NULL,
	[NopAbsUsdCurrentTradeDate] decimal(18,2) NULL,
	[DealsCountCurrentTradeDate] [int] NULL,
	[MaxUnsettledNopUsdTotal] [decimal](18, 2) NOT NULL,
	[MaxUnsettledNopUsdTotalToLockDestination] [decimal](18, 2) NOT NULL,
	[CheckingZone] [varchar](20) NULL
) ON [Integrator_DailyData]
GO

GRANT ALTER ON [dbo].[PersistedSettlementStatisticsPerPrimeBroker] TO [IntegratorServiceAccount] AS [dbo]
GRANT SELECT ON [dbo].[PersistedSettlementStatisticsPerPrimeBroker] TO [IntegratorServiceAccount] AS [dbo]
GRANT ALTER ON [dbo].[PersistedSettlementStatisticsPerPrimeBroker] TO [IntegratorKillSwitchUser] AS [dbo]
GRANT SELECT ON [dbo].[PersistedSettlementStatisticsPerPrimeBroker] TO [IntegratorKillSwitchUser] AS [dbo]



CREATE PROCEDURE [dbo].[UpdateNopStatsPerPb_Internal_SP]
AS
BEGIN

	DECLARE @temp_result TABLE
	(
		[PrimeBrokerName] [varchar](10),
		[PrimeBrokerId] [tinyint],
		[NopAbsUsdCurrentTradeDate] decimal(18,2),
		[DealsCountCurrentTradeDate] [int],
		[MaxUnsettledNopUsdTotal] [decimal](18, 2),
		[MaxUnsettledNopUsdTotalToLockDestination] [decimal](18, 2),
		[CheckingZone] [varchar](20)
	)


	DECLARE @temp_NOPs_pol TABLE
	(
		BaseCurrencyId tinyint,
		QuoteCurrencyId tinyint,
		PbId tinyint,
		DealsCount int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	DECLARE @aggreggatedUsd_NOPs_pol TABLE
	(
		PbId tinyint,
		DealsCount int,
		NopAbsUsd decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(BaseCurrencyId, QuoteCurrencyId, PbId, DealsCount, NopBasePol, NopTermPol)
	SELECT
		MAX(sym.BaseCurrencyId) AS BaseCurrencyId
		,MAX(sym.QuoteCurrencyId) AS QuoteCurrencyId
		,pb.PrimeBrokerId,
		Count(pb.PrimeBrokerId) AS DealsCount,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.Price) AS NopTermPol
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
		INNER JOIN [dbo].[Symbol] sym ON sym.ID = deal.[SymbolId]	
		INNER JOIN [dbo].[PrimeBrokerCreditLines_CptMapping] map ON deal.CounterpartyId = map.CounterpartyId
		INNER JOIN [dbo].[PrimeBrokerCreditLine] pb ON map.CreditLineId = pb.CreditLineId
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = CAST(GETUTCDATE() AS DATE)
	GROUP BY
		deal.SymbolId,
		pb.PrimeBrokerId


	INSERT INTO @aggreggatedUsd_NOPs_pol(PbId, NopAbsUsd, DealsCount)
	SELECT
			PbId,
			CASE WHEN SUM(CASE WHEN NopPolInUSD > 0 THEN NopPolInUSD ELSE 0 END) > SUM(CASE WHEN NopPolInUSD > 0 THEN 0 ELSE -NopPolInUSD END) 
				THEN SUM(CASE WHEN NopPolInUSD > 0 THEN NopPolInUSD ELSE 0 END) 
				ELSE SUM(CASE WHEN NopPolInUSD > 0 THEN 0 ELSE -NopPolInUSD END)
			END AS NopAbsInUSD,
			SUM(DealsCount) AS DealsCount
		FROM
			(
			SELECT
				PbId,
				nopsUnaggreggated.CurrencyId,
				SUM(NopPol * ccy.LastKnownUsdConversionMultiplier) AS NopPolInUSD,
				SUM(DealsCount) AS DealsCount
			FROM
				(
				SELECT
					PbId,
					BaseCurrencyId AS CurrencyId,
					NopBasePol AS NopPol,
					DealsCount AS DealsCount
				FROM
					@temp_NOPs_pol basePos

				UNION ALL

				SELECT
					PbId,
					QuoteCurrencyId AS CurrencyId,
					NopTermPol AS NopPol,
					0 AS DealsCount
				FROM
					@temp_NOPs_pol basePos
				) nopsUnaggreggated
				INNER JOIN Currency ccy ON ccy.CurrencyID = nopsUnaggreggated.CurrencyId
			GROUP BY
				PbId,
				nopsUnaggreggated.CurrencyId
			)nopsAggreggated
		GROUP BY
			PbId


	INSERT INTO @temp_result
	(
		[PrimeBrokerId]
		,[PrimeBrokerName]
		,[NopAbsUsdCurrentTradeDate]
		,[DealsCountCurrentTradeDate]
		,[MaxUnsettledNopUsdTotal]
		,[MaxUnsettledNopUsdTotalToLockDestination]
		,[CheckingZone]
	)
	SELECT
		pb.PrimeBrokerId,
		PrimeBrokerName,
		NopAbsUsd,
		DealsCount,
		limits.MaxUnsettledNopUsdTotal,
		limits.MaxUnsettledNopUsdTotalToLockDestination,
		zoneName.[NopCheckingZoneName]
	FROM
		[dbo].[PrimeBroker] pb
		INNER JOIN [dbo].[PrimeBrokerCreditLine] limits ON pb.PrimeBrokerId = limits.PrimeBrokerId AND limits.CreditLineId IS NULL
		LEFT JOIN @aggreggatedUsd_NOPs_pol nopsFinal ON pb.PrimeBrokerId = nopsFinal.pbId
		LEFT JOIN [dbo].[PrimeBrokerNopCheckingZones] zone ON pb.PrimeBrokerId = zone.PrimeBrokerId
		LEFT JOIN [dbo].[NopCheckingZone] zoneName ON zone.[NopCheckingZoneId] = zoneName.[NopCheckingZoneId]

	UNION ALL

	SELECT
		NULL AS PrimeBrokerId,
		NULL AS PrimeBrokerName,
		SUM(NopAbsUsd) AS NopAbsUsd,
		SUM(DealsCount) AS DealsCount,
		limits2.MaxUnsettledNopUsdTotal,
		limits2.MaxUnsettledNopUsdTotalToLockDestination,
		zoneName.[NopCheckingZoneName]
	FROM
		@aggreggatedUsd_NOPs_pol nopsFinalAggreggated
		INNER JOIN [dbo].[PrimeBrokerCreditLine] limits2 ON limits2.PrimeBrokerId IS NULL AND limits2.CreditLineId IS NULL
		LEFT JOIN [dbo].[PrimeBrokerNopCheckingZones] zone ON zone.PrimeBrokerId IS NULL
		LEFT JOIN [dbo].[NopCheckingZone] zoneName ON zone.[NopCheckingZoneId] = zoneName.[NopCheckingZoneId]
	GROUP BY
		limits2.MaxUnsettledNopUsdTotal,
		limits2.MaxUnsettledNopUsdTotalToLockDestination,
		zoneName.[NopCheckingZoneName]
		
	
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedSettlementStatisticsPerPrimeBroker]
		
		INSERT INTO [dbo].[PersistedSettlementStatisticsPerPrimeBroker]
		(
			[PrimeBrokerId]
			,[PrimeBrokerName]
			,[NopAbsUsdCurrentTradeDate]
			,[DealsCountCurrentTradeDate]
			,[MaxUnsettledNopUsdTotal]
			,[MaxUnsettledNopUsdTotalToLockDestination]
			,[CheckingZone]
		)
		SELECT
			[PrimeBrokerId]
			,[PrimeBrokerName]
			,[NopAbsUsdCurrentTradeDate]
			,[DealsCountCurrentTradeDate]
			,[MaxUnsettledNopUsdTotal]
			,[MaxUnsettledNopUsdTotalToLockDestination]
			,[CheckingZone]
		FROM 
			@temp_result

	COMMIT
END
GO

CREATE PROCEDURE [dbo].[GetNopStatsPerPb_Cached_SP]
(
	@Force BIT = 0
)
AS
BEGIN

	IF @Force = 1
	BEGIN
		exec [dbo].[UpdateNopStatsPerPb_Internal_SP]
	END

	SELECT 
		[PrimeBrokerName]
		,[PrimeBrokerId]
		,[NopAbsUsdCurrentTradeDate]
		,[DealsCountCurrentTradeDate]
		,[MaxUnsettledNopUsdTotal]
		,[MaxUnsettledNopUsdTotalToLockDestination]
		,[CheckingZone]
	FROM 
		[dbo].[PersistedSettlementStatisticsPerPrimeBroker]
	ORDER BY
		CASE WHEN [PrimeBrokerId] IS NULL THEN 0 ELSE 1 END DESC,
		[PrimeBrokerName] ASC
END
GO

GRANT EXECUTE ON [dbo].[GetNopStatsPerPb_Cached_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROCEDURE [dbo].[UpdateSettlementStatisticsPerCreditLine_SP]
(
	@Force BIT = 0
)
AS
BEGIN

	UPDATE 
		[dbo].[TableChangeLog] 
	SET 
		[LastUpdatedUtc] = GETUTCDATE() 
	WHERE 
		[TableName] = 'SettlementStatistics' 
		AND 
		(@Force = 1 OR [LastUpdatedUtc] < DATEADD(SECOND, -2, GETUTCDATE()))

	IF @@ROWCOUNT > 0
	BEGIN
	
		DECLARE @temp_final_result TABLE
		(
			[PrimeBrokerName] [varchar](10),
			[CreditLineName] [varchar](255),
			[CreditLineId] [tinyint],
			[UnsettledNopUsdTotal] [decimal](18, 2),
			[HighestUnsettledNopInSingleT] [decimal](18, 2),
			[DealsCountTotal] [int],
			[Zone_Total] [varchar](20),
			[MaxUnsettledNopUsdTotal] [decimal](18, 2),
			[MaxUnsettledNopUsdTotalToLockDestination] [decimal](18, 2),
			[MaxUnsettledNopUsdPerValueDate] [decimal](18, 2),
			[MaxUnsettledNopUsdPerValueDateToLockDestination] [decimal](18, 2),
			[T1] [date],
			[T2] [date],
			[T3] [date],
			[T4] [date],
			[T5] [date],
			[T6] [date],
			[T7] [date],
			[T8] [date],
			[UnsettledNopUsd_T1] [decimal](18, 2),
			[UnsettledNopUsd_T2] [decimal](18, 2),
			[UnsettledNopUsd_T3] [decimal](18, 2),
			[UnsettledNopUsd_T4] [decimal](18, 2),
			[UnsettledNopUsd_T5] [decimal](18, 2),
			[UnsettledNopUsd_T6] [decimal](18, 2),
			[UnsettledNopUsd_T7] [decimal](18, 2),
			[UnsettledNopUsd_T8] [decimal](18, 2),
			[DealsCount_T1] [int],
			[DealsCount_T2] [int],
			[DealsCount_T3] [int],
			[DealsCount_T4] [int],
			[DealsCount_T5] [int],
			[DealsCount_T6] [int],
			[DealsCount_T7] [int],
			[DealsCount_T8] [int],
			[Zone_T1] [varchar](20),
			[Zone_T2] [varchar](20),
			[Zone_T3] [varchar](20),
			[Zone_T4] [varchar](20),
			[Zone_T5] [varchar](20),
			[Zone_T6] [varchar](20),
			[Zone_T7] [varchar](20),
			[Zone_T8] [varchar](20)
		)
		
		
		DECLARE @temp_NOPs_pol TABLE
		(
			CreditLineId tinyint,
			BaseCurrencyId smallint,
			QuoteCurrencyId smallint,
			NopBasePol decimal(18,2),
			NopTermPol decimal(18,4),
			[DealsCount] int,
			[ValueDate] Date
		)
		
		DECLARE @temp_intermediate_result TABLE
		(
			CreditLineId tinyint,
			[UnsettledNopUsd] [decimal](18, 2),
			[DealsCount] [int],
			[ValueDate] Date
		)

		INSERT INTO
			@temp_NOPs_pol
			(
				CreditLineId,
				BaseCurrencyId,
				QuoteCurrencyId,
				NopBasePol,
				NopTermPol,
				DealsCount,
				ValueDate
			)
		SELECT 
			[CreditLineId]
			,MAX(sym.BaseCurrencyId) AS BaseCurrencyId
			,MAX(sym.QuoteCurrencyId) AS QuoteCurrencyId
			,SUM([SizeBasePol]) AS [NopBasePol]
			,SUM([SizeTermPol]) AS [NopTermPol]
			,SUM(DealsCount) AS [DealsCount]
			,SettlementDateLocalMkt AS ValueDate
			--,COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate]) AS [SettlementDate]
		FROM
			[dbo].[DealExternalExecutedUnsettled] unsettled
			INNER JOIN [dbo].[PrimeBrokerCreditLines_CptMapping] creditLineMap ON unsettled.[CounterpartyId] = creditLineMap.[CounterpartyId]
			INNER JOIN Symbol sym ON sym.ID = unsettled.[SymbolId]				
		WHERE
			[SettlementTimeCounterpartyUtc] > GetUtcDate()
		GROUP BY
			[CreditLineId],
			[SymbolId],
			[SettlementDateLocalMkt]

		
		INSERT INTO
			@temp_intermediate_result
			(
				[CreditLineId],
				[UnsettledNopUsd],
				[DealsCount],
				[ValueDate]
			)
			SELECT
				CreditLineId,
				CASE WHEN SUM(CASE WHEN NopPolInUSD > 0 THEN NopPolInUSD ELSE 0 END) > SUM(CASE WHEN NopPolInUSD > 0 THEN 0 ELSE -NopPolInUSD END) 
					THEN SUM(CASE WHEN NopPolInUSD > 0 THEN NopPolInUSD ELSE 0 END) 
					ELSE SUM(CASE WHEN NopPolInUSD > 0 THEN 0 ELSE -NopPolInUSD END)
				END AS NopAbsInUSD,
				SUM(DealsCount) AS DealsCount,
				ValueDate
			FROM
				(
				SELECT
					CreditLineId,
					nopsUnaggreggated.CurrencyId,
					SUM(NopPol * ccy.LastKnownUsdConversionMultiplier) AS NopPolInUSD,
					SUM(DealsCount) AS DealsCount,
					ValueDate
				FROM
					(
					SELECT
						CreditLineId,
						BaseCurrencyId AS CurrencyId,
						NopBasePol AS NopPol,
						DealsCount AS DealsCount,
						ValueDate
					FROM
						@temp_NOPs_pol basePos

					UNION ALL

					SELECT
						CreditLineId,
						QuoteCurrencyId AS CurrencyId,
						NopTermPol AS NopPol,
						0 AS DealsCount,
						ValueDate
					FROM
						@temp_NOPs_pol basePos
					) nopsUnaggreggated
					INNER JOIN Currency ccy ON ccy.CurrencyID = nopsUnaggreggated.CurrencyId
				GROUP BY
					CreditLineId,
					nopsUnaggreggated.CurrencyId,
					ValueDate
				)nopsAggreggated
			GROUP BY
				CreditLineId,
				ValueDate

			DECLARE @temp_ValueDates TABLE
			(
				ValueDate Date,
				ValueDateId int
			)

			INSERT INTO @temp_ValueDates(ValueDateId, ValueDate)
			SELECT
				ROW_NUMBER() OVER (ORDER BY ValueDate),
				ValueDate
			FROM
				@temp_intermediate_result
			WHERE
				ValueDate IS NOT NULL
			GROUP BY
				ValueDate


			
			;WITH TmpStats AS
			(
				SELECT 
					CreditLineId,
					UnsettledNopUsd,
					DealsCount,
					res.ValueDate,
					'Nop_T' + CAST(ValueDateId AS VARCHAR) AS NopsValueDateTag,
					'Deals_T' + CAST(ValueDateId AS VARCHAR) AS DealsValueDateTag
			FROM
				@temp_intermediate_result res
				FULL OUTER JOIN @temp_ValueDates valDates ON res.ValueDate = valDates.ValueDate
			)

			 INSERT INTO @temp_final_result
			(
				[PrimeBrokerName]
				,[CreditLineName]
				,[CreditLineId]
				,[UnsettledNopUsdTotal]
				,[HighestUnsettledNopInSingleT]
				,[DealsCountTotal]
				,[Zone_Total]
				,[MaxUnsettledNopUsdTotal]
				,[MaxUnsettledNopUsdTotalToLockDestination]
				,[MaxUnsettledNopUsdPerValueDate]
				,[MaxUnsettledNopUsdPerValueDateToLockDestination]
				,[T1]
				,[T2]
				,[T3]
				,[T4]
				,[T5]
				,[T6]
				,[T7]
				,[T8]
				,[UnsettledNopUsd_T1]
				,[UnsettledNopUsd_T2]
				,[UnsettledNopUsd_T3]
				,[UnsettledNopUsd_T4]
				,[UnsettledNopUsd_T5]
				,[UnsettledNopUsd_T6]
				,[UnsettledNopUsd_T7]
				,[UnsettledNopUsd_T8]
				,[DealsCount_T1]
				,[DealsCount_T2]
				,[DealsCount_T3]
				,[DealsCount_T4]
				,[DealsCount_T5]
				,[DealsCount_T6]
				,[DealsCount_T7]
				,[DealsCount_T8]
				,[Zone_T1]
				,[Zone_T2]
				,[Zone_T3]
				,[Zone_T4]
				,[Zone_T5]
				,[Zone_T6]
				,[Zone_T7]
				,[Zone_T8]
			)

			SELECT
				primeBroker.PrimeBrokerName,
				creditLine.[CreditLineName],
				creditLine.[CreditLineId],
				UnsettledNopUsdTotal,
				(SELECT MAX(x) FROM (VALUES(Nop_T1), (Nop_T2), (Nop_T3), (Nop_T4), (Nop_T5), (Nop_T6), (Nop_T7), (Nop_T8)) AS VALUE(x)) AS [HighestUnsettledNopInSingleT],
				DealsCountTotal,
				Zone_Total,
				creditLine.[MaxUnsettledNopUsdTotal],
				creditLine.[MaxUnsettledNopUsdTotalToLockDestination],
				creditLine.[MaxUnsettledNopUsdPerValueDate],
				creditLine.[MaxUnsettledNopUsdPerValueDateToLockDestination],
				T1, T2, T3, T4, T5, T6, T7, T8,
				Nop_T1, Nop_T2, Nop_T3, Nop_T4, Nop_T5, Nop_T6, Nop_T7, Nop_T8, 
				Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8,
				Zone_T1, Zone_T2, Zone_T3, Zone_T4, Zone_T5, Zone_T6, Zone_T7, Zone_T8
			FROM

				(
					select T1, T2, T3, T4, T5, T6, T7, T8
					from
					(
						select ValueDate, 'T' + CAST(ValueDateId AS VARCHAR) AS ValueDateTag
						FROM @temp_ValueDates
					) d
					PIVOT
					(
						MAX(ValueDate)
						for ValueDateTag IN (T1, T2, T3, T4, T5, T6, T7, T8)
					) pivDates
				)ValDates

				INNER JOIN [dbo].[PrimeBrokerCreditLine] creditLine ON creditLine.CreditLineId IS NOT NULL
				INNER JOIN [dbo].[PrimeBroker] primeBroker ON creditLine.[PrimeBrokerId] = primeBroker.[PrimeBrokerId]

				LEFT JOIN

				(
				SELECT 
					CreditLineId,
					MAX(Nop_T1) AS Nop_T1, MAX(Nop_T2) AS Nop_T2, MAX(Nop_T3) AS Nop_T3, MAX(Nop_T4) AS Nop_T4, MAX(Nop_T5) AS Nop_T5, MAX(Nop_T6) AS Nop_T6, MAX(Nop_T7) AS Nop_T7, MAX(Nop_T8) AS Nop_T8
				FROM
					TmpStats
					PIVOT 
					(
						MAX(UnsettledNopUsd) FOR NopsValueDateTag IN (Nop_T1, Nop_T2, Nop_T3, Nop_T4, Nop_T5, Nop_T6, Nop_T7, Nop_T8)
					)pivNops
				GROUP BY
					CreditLineId
				)NopsTmp ON creditLine.CreditLineId = NopsTmp.CreditLineId

				LEFT JOIN

				(
				SELECT 
					CreditLineId,
					MAX(Deals_T1) AS Deals_T1, MAX(Deals_T2) AS Deals_T2, MAX(Deals_T3) AS Deals_T3, MAX(Deals_T4) AS Deals_T4, MAX(Deals_T5) AS Deals_T5, MAX(Deals_T6) AS Deals_T6, MAX(Deals_T7) AS Deals_T7, MAX(Deals_T8) AS Deals_T8
				FROM
					TmpStats
					PIVOT 
					(
						MAX(DealsCount) FOR DealsValueDateTag IN (Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8)
					)piv
				GROUP BY
					CreditLineId
				)DealsTmp

				ON NopsTmp.CreditLineId = DealsTmp.CreditLineId

				LEFT JOIN

				(
				SELECT
					CreditLineId,
					SUM(UnsettledNopUsd) AS UnsettledNopUsdTotal,
					SUM(DealsCount) AS DealsCountTotal
				FROM
					TmpStats
				GROUP BY
					CreditLineId
				) totalSum

				ON totalSum.CreditLineId = DealsTmp.CreditLineId
				
				LEFT JOIN
				
				(
				SELECT
					[CreditLineId],
					Zone_Total, Zone_T1, Zone_T2, Zone_T3, Zone_T4, Zone_T5, Zone_T6, Zone_T7, Zone_T8
				FROM
					(
					SELECT 
						[CreditLineId]
						,'Zone_T' + COALESCE(CAST(valDate.ValueDateId AS varchar), 'otal') AS ZoneValueDateTag
						,zoneName.[NopCheckingZoneName] AS ZoneName
					FROM 
						[dbo].[PrimeBrokerCreditLineNopCheckingZones] zone
						INNER JOIN [dbo].[NopCheckingZone] zoneName ON zone.[NopCheckingZoneId] = zoneName.[NopCheckingZoneId]
						LEFT JOIN @temp_ValueDates valDate ON zone.[SettlementDate] = valDate.ValueDate
					--be carefull not to join old settlement dates that are not in @temp_ValueDates
					WHERE
						zone.[SettlementDate] IS NULL AND valDate.ValueDate IS NULL
						OR
						zone.[SettlementDate] IS NOT NULL AND valDate.ValueDate IS NOT NULL
					) ZoneStats
					PIVOT
					(
						MAX(ZoneStats.ZoneName) FOR ZoneValueDateTag IN (Zone_Total, Zone_T1, Zone_T2, Zone_T3, Zone_T4, Zone_T5, Zone_T6, Zone_T7, Zone_T8)
					) piv
				) zoneInfo

				ON totalSum.CreditLineId = zoneInfo.CreditLineId

			ORDER BY
				primeBroker.PrimeBrokerName,
				creditLine.[CreditLineName]
			
		BEGIN TRANSACTION
			TRUNCATE TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine]
		
			INSERT INTO
				[dbo].[PersistedSettlementStatisticsPerCreditLine]
				(
					[PrimeBrokerName]
					,[CreditLineName]
					,[CreditLineId]
					,[UnsettledNopUsdTotal]
					,[HighestUnsettledNopInSingleT]
					,[DealsCountTotal]
					,[Zone_Total]
					,[MaxUnsettledNopUsdTotal]
					,[MaxUnsettledNopUsdTotalToLockDestination]
					,[MaxUnsettledNopUsdPerValueDate]
					,[MaxUnsettledNopUsdPerValueDateToLockDestination]
					,[T1]
					,[T2]
					,[T3]
					,[T4]
					,[T5]
					,[T6]
					,[T7]
					,[T8]
					,[UnsettledNopUsd_T1]
					,[UnsettledNopUsd_T2]
					,[UnsettledNopUsd_T3]
					,[UnsettledNopUsd_T4]
					,[UnsettledNopUsd_T5]
					,[UnsettledNopUsd_T6]
					,[UnsettledNopUsd_T7]
					,[UnsettledNopUsd_T8]
					,[DealsCount_T1]
					,[DealsCount_T2]
					,[DealsCount_T3]
					,[DealsCount_T4]
					,[DealsCount_T5]
					,[DealsCount_T6]
					,[DealsCount_T7]
					,[DealsCount_T8]
					,[Zone_T1]
					,[Zone_T2]
					,[Zone_T3]
					,[Zone_T4]
					,[Zone_T5]
					,[Zone_T6]
					,[Zone_T7]
					,[Zone_T8]
				)
			SELECT
				[PrimeBrokerName]
				,[CreditLineName]
				,[CreditLineId]
				,[UnsettledNopUsdTotal]
				,[HighestUnsettledNopInSingleT]
				,[DealsCountTotal]
				,[Zone_Total]
				,[MaxUnsettledNopUsdTotal]
				,[MaxUnsettledNopUsdTotalToLockDestination]
				,[MaxUnsettledNopUsdPerValueDate]
				,[MaxUnsettledNopUsdPerValueDateToLockDestination]
				,[T1]
				,[T2]
				,[T3]
				,[T4]
				,[T5]
				,[T6]
				,[T7]
				,[T8]
				,[UnsettledNopUsd_T1]
				,[UnsettledNopUsd_T2]
				,[UnsettledNopUsd_T3]
				,[UnsettledNopUsd_T4]
				,[UnsettledNopUsd_T5]
				,[UnsettledNopUsd_T6]
				,[UnsettledNopUsd_T7]
				,[UnsettledNopUsd_T8]
				,[DealsCount_T1]
				,[DealsCount_T2]
				,[DealsCount_T3]
				,[DealsCount_T4]
				,[DealsCount_T5]
				,[DealsCount_T6]
				,[DealsCount_T7]
				,[DealsCount_T8]
				,[Zone_T1]
				,[Zone_T2]
				,[Zone_T3]
				,[Zone_T4]
				,[Zone_T5]
				,[Zone_T6]
				,[Zone_T7]
				,[Zone_T8]
			FROM
				@temp_final_result
		COMMIT	
		
		exec [dbo].[UpdateSettlementStatisticsPerSymbol_Internal_SP]
		exec [dbo].[UpdateNopStatsPerPb_Internal_SP]
	END
END 
GO

ALTER PROCEDURE [dbo].[GetNopAbsTotal_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	SELECT 
		[NopAbsUsdCurrentTradeDate] 
	FROM 
		[dbo].[PersistedSettlementStatisticsPerPrimeBroker] 
	WHERE 
		[PrimeBrokerId] IS NULL

END
GO


-- Wrong conditions for deals/rejections in [OrderExternalIncomingRejectable_Daily]
-----------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[StreamingSessionStatsToday_SP]
(
	@CounterpartyCode [char](3)
)
AS
BEGIN
	SELECT 
		SUM(CASE WHEN [IntegratorExecutedAmountBasePol] = 0 THEN 1 ELSE 0 END) AS RejectionsNum,
		SUM(CASE WHEN [IntegratorExecutedAmountBasePol] != 0 THEN 1 ELSE 0 END) AS DealsNum
	FROM
		[dbo].[OrderExternalIncomingRejectable_Daily] ord
		INNER JOIN [dbo].[Counterparty] ctp on ord.CounterpartyId = ctp.CounterpartyId
	WHERE
		ctp.[CounterpartyCode] = @CounterpartyCode
END
GO

ALTER PROCEDURE [dbo].[GetStatisticsPerCounterparty_Daily_SP] (
	@Day DATE
	)
AS
BEGIN

	DECLARE @KGTRejectionDirectionId BIT
	SELECT @KGTRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'
	DECLARE @CtpRejectionDirectionId BIT
	SELECT @CtpRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'CtpRejectd'
	DECLARE @MinDate DATE = GETUTCDATE()	
		
	DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		KGTRejectionsNum int,
		LastKgtRejectionUtc datetime2(7),
		CtpRejectionsNum int,
		LastCptRejectionUtc datetime2(7),
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int
	)

	INSERT INTO @temp_rejections(CounterpartyId, KGTRejectionsNum, LastKgtRejectionUtc, CtpRejectionsNum, LastCptRejectionUtc, IntegratorLatencyNanoseconds, IntegratorLatencyCases)
	SELECT
		[CounterpartyId],
		SUM(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN 1 ELSE 0 END) AS KGTRejections,
		MAX(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN [IntegratorReceivedExecutionReportUtc] ELSE @MinDate END) AS LastKgtRejectionUtc,
		SUM(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN 1 ELSE 0 END) AS CtpRejections,
		MAX(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN [IntegratorReceivedExecutionReportUtc] ELSE @MinDate END) AS LastCptRejectionUtc,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
	FROM
		[dbo].[DealExternalRejected_DailyView] ctpRej WITH(NOEXPAND, NOLOCK)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]

	UNION ALL

	SELECT
			[CounterpartyId],
			COUNT([CounterpartyId]) AS KGTRejections,
			MAX([IntegratorReceivedExternalOrderUtc]) AS LastKgtRejectionUtc,
			NULL AS CtpRejections,
			NULL AS LastCptRejectionUtc,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily] kgtRej WITH(NOLOCK)
		WHERE
			[IntegratorExecutedAmountBasePol] = 0
			AND [DatePartIntegratorReceivedOrderUtc] = @Day
			--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
		GROUP BY
			[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		CommissionPBBase decimal(18,2),
		CommissionCptBase decimal(18,2),
		DealsNum int,
		LastDealUtc datetime2(7),
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int,
		CounterpartyLatencyNanoseconds bigint,
		CounterpartyLatencyCases int
	)

	INSERT INTO 
		@temp_NOPs_pol
		(
		SymbolId, 
		CounterpartyId, 
		NopBasePol, 
		NopTermPol, 
		VolumeBaseAbs, 
		CommissionPBBase,
		CommissionCptBase,
		DealsNum, 
		LastDealUtc,
		IntegratorLatencyNanoseconds, 
		IntegratorLatencyCases, 
		CounterpartyLatencyNanoseconds, 
		CounterpartyLatencyCases
		)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		SUM(ABS([AmountBasePolExecuted]) * PbCommissionPpm / 1000000) AS CommissionPBBase,
		SUM(ABS([AmountBasePolExecuted]) * CounterpartyCommissionPpm / 1000000) AS CommissionCptBase,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum,
		MAX(deal.[IntegratorReceivedExecutionReportUtc]) AS LastDealUtc,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases,
		SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
		COUNT(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyCases
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
		INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
		--INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		deal.SymbolId,
		deal.CounterpartyId
		

	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.CommissionPbInUsd AS CommissionPbInUsd,
		DealsStatistics.CommissionCptInUsd AS CommissionCptInUsd,
		DealsStatistics.CommissionPbInUsd + DealsStatistics.CommissionCptInUsd  AS CommissionSumInUsd,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.LastDealUtc AS LastDealUtc,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		CASE WHEN COALESCE(RejectionsStatistics.KGTRejectionsNum,0) = 0 THEN NULL ELSE RejectionsStatistics.KGTRejectionsNum END AS KGTRejectionsNum,
		RejectionsStatistics.LastKgtRejectionUtc AS RejectionsStatistics,
		CASE WHEN COALESCE(RejectionsStatistics.CtpRejectionsNum,0) = 0 THEN NULL ELSE RejectionsStatistics.CtpRejectionsNum END AS CtpRejectionsNum,
		RejectionsStatistics.LastCptRejectionUtc AS LastCptRejectionUtc,
		CASE WHEN COALESCE(RejectionsStatistics.KGTRejectionsNum,0) = 0
		THEN
			 NULL
		ELSE
			RejectionsStatistics.KGTRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.KGTRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0))
		END AS KGTRejectionsRate,
		CASE WHEN COALESCE(RejectionsStatistics.CtpRejectionsNum,0) = 0
		THEN
			NULL
		ELSE
			RejectionsStatistics.CtpRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.CtpRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0))
		END AS CtpRejectionsRate,
		--this calculates average of both numbers if both are non-null; or average of the nonull case if just one is not null (as 2*A / 2*B = A/B); or NULL if both are NULL (avoids divide by zero exception)
		(COALESCE(RejectionsStatistics.IntegratorLatencyNanoseconds, DealsStatistics.IntegratorLatencyNanoseconds) + COALESCE(DealsStatistics.IntegratorLatencyNanoseconds, RejectionsStatistics.IntegratorLatencyNanoseconds))
		/
		(COALESCE(RejectionsStatistics.IntegratorLatencyCases, DealsStatistics.IntegratorLatencyCases) + COALESCE(DealsStatistics.IntegratorLatencyCases, RejectionsStatistics.IntegratorLatencyCases))
		AS AvgIntegratorLatencyNanoseconds,
		DealsStatistics.CounterpartyLatencyNanoseconds / DealsStatistics.CounterpartyLatencyCases AS AvgCounterpartyLatencyNanoseconds
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			-- from SHORTs
			SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs,
			--SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(CommissionPbInUsd) AS CommissionPbInUsd,
			SUM(CommissionCptInUsd) AS CommissionCptInUsd,
			SUM(DealsNum) AS DealsNum,
			MAX(LastDealUtc) AS LastDealUtc,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
			SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
			SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(CommissionPbInUsd) AS CommissionPbInUsd,
				SUM(CommissionCptInUsd) AS CommissionCptInUsd,
				SUM(DealsNum) AS DealsNum,
				MAX(LastDealUtc) AS LastDealUtc,
				SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
				SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
				SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
				SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.CommissionPBBase * ccy1.LastKnownUsdConversionMultiplier AS CommissionPbInUsd,
					basePos.CommissionCptBase * ccy1.LastKnownUsdConversionMultiplier AS CommissionCptInUsd,
					basePos.DealsNum AS DealsNum,
					basePos.LastDealUtc AS LastDealUtc,
					basePos.IntegratorLatencyNanoseconds AS IntegratorLatencyNanoseconds,
					basePos.IntegratorLatencyCases AS IntegratorLatencyCases,
					basePos.CounterpartyLatencyNanoseconds AS CounterpartyLatencyNanoseconds,
					basePos.CounterpartyLatencyCases AS CounterpartyLatencyCases
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd,
					0 AS CommissionPbInUsd,
					0 AS CommissionCptInUsd,
					0 AS DealsNum,
					NULL AS LastDealUtc,
					NULL AS IntegratorLatencyNanoseconds,
					NULL AS IntegratorLatencyCases,
					NULL AS CounterpartyLatencyNanoseconds,
					NULL AS CounterpartyLatencyCases
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode
END
GO


DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RiskManagementSettings" nillable="true" type="RiskManagementSettings" />
  <xs:complexType name="RiskManagementSettings">
    <xs:sequence>
      <xs:element name="ConfigLastUpdated"  nillable="true" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="DisallowTradingOnInstances" type="ArrayOfString" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsRate" type="FatalErrorsRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAndPosition" type="PriceAndPositionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Instance" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersSubmissionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FatalErrorsRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrderMaximumAllowedSizeSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedBaseAbsSizeInUsd" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="PriceAndPositionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalUnsettledNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceDeviationCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="OnlinePriceDeviationCheckMaxPriceAge_Minutes" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="SymbolTrustworthyCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumSymbolUntrustworthyPeriod_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="DefaultNewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="Symbols" type="ArrayOfSymbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbol">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="Symbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="Symbol">
    <xs:attribute name="ShortName" type="xs:string" />
    <xs:attribute name="NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
  </xs:complexType>
</xs:schema>'
GO

--manually update risk mgmt settings - remove MaximumExternalNOPInUSD and MaximumAllowedNOPCalculationTime_Milliseconds


DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_DBK" type="FIXChannel_DBKSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_H4T" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_H4M" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM2" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM3" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LX1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LGA" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LGC" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC1" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC2" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_PX1" type="FIXChannel_PXMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS1" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS2" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTAstream" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3stream" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FL1" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
      <xs:element minOccurs="1" maxOccurs="1" name="STP" type="STPSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_DBKSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DealConfirmationSupported" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="MaxOrdersPerSecond" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="L01LayersNum" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IocApplyTtl" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ApplyMaxHoldTime" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxHoldTime" type="FXCMMaxHoldTime" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_PXMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MaxPerSymbolPricesPerSecondSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultMaxAllowedPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultMaxAllowedIntervalToCheck_Milliseconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialSymbolRateSettings" type="ArrayOfSymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolRateSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="SymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolRateSetting">
    <xs:attribute name="Name" type="xs:string" />
    <xs:attribute name="MaxAllowedPerTimeInterval" type="xs:int" use="required" />
    <xs:attribute name="MaxAllowedIntervalToCheck_Milliseconds" type="xs:int" use="required" />
  </xs:complexType>
  <xs:complexType name="RejectionRateDisconnectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumOrdersToApplyCheck" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAllowedRejectionRatePercent" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="StreamingChannelSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="MaxTotalPricesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="MaxTotalPricesIntervalToCheck_Milliseconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="MaxPerSymbolPricesPerSecondSetting" type="MaxPerSymbolPricesPerSecondSetting" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExpectedPricingDepth" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="RejectionRateDisconnectSettings" type="RejectionRateDisconnectSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXSTPChannel_CTIPBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNongenuineInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialRejectionsHandling" type="ArrayOfSpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SpecialRejection" type="SpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="TriggerPhrase" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="FXCMMaxHoldTime">
    <xs:restriction base="xs:string">
      <xs:enumeration value="UpTo1ms" />
      <xs:enumeration value="UpTo30ms" />
      <xs:enumeration value="UpTo100ms" />
      <xs:enumeration value="UpTo500ms" />
      <xs:enumeration value="UpTo3000ms" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="MarketSessionIntoSeparateProcess" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="STPDestinations" type="ArrayOfSTPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSTPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="STPDestination" nillable="true" type="STPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Name" type="STPCounterparty" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedTicketTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="ServedCounterparties" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="STPCounterparty">
    <xs:restriction base="xs:string">
      <xs:enumeration value="CTIPB" />
      <xs:enumeration value="Traiana" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

Change:
<MaxTotalPricesPerSecond>200</MaxTotalPricesPerSecond>
to:
<MaxTotalPricesPerTimeInterval>200</MaxTotalPricesPerTimeInterval>
<MaxTotalPricesIntervalToCheck_Milliseconds>1000</MaxTotalPricesIntervalToCheck_Milliseconds>

Change:
<DefaultMaxAllowedPerSecondRate>30</DefaultMaxAllowedPerSecondRate>
to:
<DefaultMaxAllowedPerTimeInterval>30</DefaultMaxAllowedPerTimeInterval>
<DefaultMaxAllowedIntervalToCheck_Milliseconds>1000</DefaultMaxAllowedIntervalToCheck_Milliseconds>

Change:
MaxAllowedPerSecondRate="30"
to:
MaxAllowedPerTimeInterval="30" MaxAllowedIntervalToCheck_Milliseconds="1000"


--Manually update Settings
<FIXChannel_FL1>
    <!-- <MaxTotalPricesPerTimeInterval>1000</MaxTotalPricesPerTimeInterval>
	<MaxTotalPricesIntervalToCheck_Milliseconds>1000</MaxTotalPricesIntervalToCheck_Milliseconds> -->
	<MaxPerSymbolPricesPerSecondSetting>
      <DefaultMaxAllowedPerTimeInterval>3</DefaultMaxAllowedPerTimeInterval>
	  <DefaultMaxAllowedTimeIntervalToCheck_Milliseconds>30</DefaultMaxAllowedTimeIntervalToCheck_Milliseconds>
    </MaxPerSymbolPricesPerSecondSetting>
    <ExpectedPricingDepth>2</ExpectedPricingDepth>
    <RejectionRateDisconnectSettings>
      <RiskCheckAllowed>true</RiskCheckAllowed>
      <MinimumOrdersToApplyCheck>100</MinimumOrdersToApplyCheck>
      <MaxAllowedRejectionRatePercent>99</MaxAllowedRejectionRatePercent>
    </RejectionRateDisconnectSettings>
  </FIXChannel_FL1>


--- one time fix to CentralBankSettlementUtc ---
-------------------------------------------------


UPDATE
	d
SET
	d.[SettlementTimeCentralBankUtc] = DATEADD(hour, 18, CAST(CAST([SettlementTimeCentralBankUtc] AS DATE) AS DATETIME))
FROM 
	[dbo].[DealExternalExecuted] d
	INNER JOIN [Symbol] s on d.SymbolId = s.Id
WHERE
	(s.BaseCurrencyId = (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'NZD')
	OR
	s.QuoteCurrencyId = (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'NZD'))
	AND
	[IntegratorReceivedExecutionReportUtc] > '2015-10-20'
	
UPDATE
	d
SET
	d.[SettlementTimeCentralBankUtc] = DATEADD(hour, 18, CAST(CAST([SettlementTimeCentralBankUtc] AS DATE) AS DATETIME))
FROM 
	[dbo].[OrderExternalIncomingRejectable_Daily] d
	INNER JOIN [Symbol] s on d.SymbolId = s.Id
WHERE
	(s.BaseCurrencyId = (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'NZD')
	OR
	s.QuoteCurrencyId = (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'NZD'))
	AND
	[IntegratorReceivedExternalOrderUtc] > '2015-10-20'
	
UPDATE
	d
SET
	d.[SettlementTimeCentralBankUtc] = DATEADD(hour, 18, CAST(CAST([SettlementTimeCentralBankUtc] AS DATE) AS DATETIME))
FROM 
	[dbo].[OrderExternalIncomingRejectable_Archive] d
	INNER JOIN [Symbol] s on d.SymbolId = s.Id
WHERE
	(s.BaseCurrencyId = (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'NZD')
	OR
	s.QuoteCurrencyId = (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'NZD'))
	AND
	[IntegratorReceivedExternalOrderUtc] > '2015-10-20'

------------------------------
----------------- FL1 Below ----------------------------------------
------------------------------

UPDATE [dbo].[Counterparty] SET Active = 1 WHERE [CounterpartyCode] = 'FL1'

--TODO credit line settings

INSERT INTO [dbo].[StreamingSettingsPerCounterparty]
           ([CounterpartyId]
           ,[MaxLLTimeEnforcedByDestination_milliseconds]
           ,[LLTransportReserve_milliseconds])
     VALUES
           ((SELECT [CounterpartyId] FROM [dbo].Counterparty WHERE CounterpartyCode = 'FL1')
           ,400
           ,80)
GO

INSERT INTO [dbo].[PrimeBrokerCreditLine]
           ([PrimeBrokerId]
           ,[CreditLineName]
           ,[MaxUnsettledNopUsdTotal]
           ,[MaxUnsettledNopUsdTotalToLockDestination]
           ,[MaxUnsettledNopUsdPerValueDate]
           ,[MaxUnsettledNopUsdPerValueDateToLockDestination]
           ,[CreditLineId])
     VALUES
           (1
           ,'FXall New York LL maker [...]'
           ,99900000
           ,98000000
           ,1000000000
           ,998000000
           ,16)
GO

INSERT INTO [dbo].[PrimeBrokerCreditLines_CptMapping]
           ([CounterpartyId]
           ,[CreditLineId])
     VALUES
           ((SELECT [CounterpartyId] FROM [dbo].Counterparty WHERE CounterpartyCode = 'FL1')
           ,16)
GO

--MANUALLY!!
-- SET for FS1_ORD_Fluent
UseDataDictionary=N


INSERT INTO [dbo].[TradingSystemType_Internal] ([CounterpartyId], [SystemName], [Enabled]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FL1'), 'Stream', 1)
GO

--quotes
[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGTINV-PRC-TEST

[SESSION]
UseDataDictionary=N
#DataDictionary=FIX42_FCM.xml
BeginString=FIX.4.4
TargetCompID=FXALL
#5pm NYK
StartTime=17:28:00
EndTime=16:59:00
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=18282
SocketConnectHost=75.98.57.176

--trades
[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


SenderCompID=KGTINV-TRD-TEST

[SESSION]
UseDataDictionary=N
#DataDictionary=FIX42_FCM.xml
RelaxValidationForMessageTypes=8, BN
BeginString=FIX.4.4
TargetCompID=FXALL
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30


SocketConnectPort=18283
SocketConnectHost=75.98.57.176

-- Fluent trades
[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
ResetOnLogout=Y
#no messages replying
PersistMessages=N

SenderCompID=kgt
SenderSubID=kgt_FXALL_MAKER

SSLEnable=N

[SESSION]
UseDataDictionary=N
#DataDictionary=FIXT11_FCM.xml
#TransportDataDictionary=FIXT11_FCM.xml
#BypassParsing=Y
#UseDataDictionary=N
RelaxValidationForMessageTypes=D,8,BN
BeginString=FIXT.1.1
DefaultApplVerID=FIX.5.0SP2
TargetCompID=FluentStreamTrades
#5pm NYK
StartTime=17:02:05
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=30463
SocketConnectHost=38.76.0.115

--ROLLBACK TRANSACTION AAA
--GO
--COMMIT TRANSACTION AAA
--GO