

print 'Starting:'
print GETUTCDATE()
GO

CREATE NONCLUSTERED INDEX [IX_IntegratorPriceIdentity] ON [FXtickDB].[dbo].[MarketData]
(
	[IntegratorPriceIdentity] ASC
)
GO

print 'Index created:'
print GETUTCDATE()
GO

--recreate the data
UPDATE
    [Integrator].[dbo].[OrderExternal]
SET
	[Integrator].[dbo].[OrderExternal].[SourceIntegratorPriceTimeUtc] = MD.[IntegratorReceivedTimeUtc]
FROM 
	[Integrator].[dbo].[OrderExternal] ordr
	INNER JOIN [Integrator].[dbo].[Symbol] symbol on symbol.Id = ordr.symbolid
	INNER JOIN [FXtickDB].[dbo].[FxPair] fxpair ON symbol.shortname = fxpair.[FxpCode]
	INNER JOIN [FXtickDB].[dbo].[MarketData] MD ON MD.FXPairId = fxpair.fxpairId AND MD.[IntegratorPriceIdentity] = ordr.[SourceIntegratorPriceIdentity]

print 'Data recreated:'
print GETUTCDATE()
GO


DROP INDEX [IX_IntegratorPriceIdentity] ON [FXtickDB].[dbo].[MarketData]
GO

print 'Index dropped:'
print GETUTCDATE()
GO

print 'DONE'
GO