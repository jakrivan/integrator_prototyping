USE [FXtickDB]
GO
/****** Object:  User [IntegratorKillSwitchUser]    Script Date: 10. 6. 2015 15:42:48 ******/
CREATE USER [IntegratorKillSwitchUser] FOR LOGIN [IntegratorKillSwitchUser] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [IntegratorServiceAccount]    Script Date: 10. 6. 2015 15:42:48 ******/
CREATE USER [IntegratorServiceAccount] FOR LOGIN [IntegratorServiceAccount] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  StoredProcedure [db_datareader].[GetAllFiltredQuotesNOTUSED]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Krivanek Jan
-- Create date: June 08 2009
-- Description:	Returns all sorted Quotes specified by parameters
-- =============================================
CREATE PROCEDURE [db_datareader].[GetAllFiltredQuotesNOTUSED] 
	@DataProviderList varchar(MAX) = NULL,
	@PairList varchar(MAX) = NULL,
	@StartTickTime numeric(17,0),
	@EndTickTime numeric(17,0)
AS
BEGIN

	DECLARE @QuoteIdList TABLE 
		(TickId int,
		QuoteTimeStamp	NUMERIC(17, 0),
		QuotePrice REAL,
		QuoteSize INT,
		QuoteSymbol CHAR(6),
		IsBid BIT,
		Provider CHAR(3),
		OneSidedQuote BIT
		)

	DECLARE @DataProviderListTbl TABLE 
		(DataProviderId TINYINT,
		 ProviderCode CHAR(3))

	DECLARE @PairListTbl TABLE 
		(FxPairId TINYINT,
		 PairCode CHAR(6))


	IF @DataProviderList IS NULL
	BEGIN
		INSERT INTO @DataProviderListTbl
			(DataProviderId, ProviderCode)
				SELECT DataProviderId, DprCode
				FROM DataProvider
	END
	ELSE
	BEGIN
		INSERT INTO @DataProviderListTbl
			(DataProviderId, ProviderCode)
				SELECT DataProviderId, DprCode
				FROM DataProvider
				INNER JOIN db_datareader.Split_StringTokenList(@DataProviderList, ',') ON DprCode = Token
	END

	IF @PairList IS NULL
	BEGIN
		INSERT INTO @PairListTbl
			(FxPairId, PairCode)
				SELECT FxPairId, FxpCode
				FROM FxPair	
	END
	ELSE
	BEGIN
		INSERT INTO @PairListTbl
			(FxPairId, PairCode)
				SELECT FxPairId, FxpCode
				FROM FxPair	
				INNER JOIN db_datareader.Split_StringTokenList(@PairList, ',') ON FxpCode = Token
	END
	


	INSERT INTO @QuoteIdList
		(TickID, 
		QuoteTimeStamp,
		QuotePrice,
		QuoteSize, 
		QuoteSymbol,
		IsBid,
		Provider,
		OneSidedQuote)
		(SELECT TickID,
				COALESCE(BidTimeStamp,TickTime) AS QuoteTimeStamp,
				Bid AS QuotePrice,
				BidSize AS QuoteSize,
				PairCode AS QuoteSymbol, 
				1 AS IsBid,
				ProviderCode,
				EndOfContinuousData
		FROM Tick with (NOLOCK)
			INNER JOIN @DataProviderListTbl ON Tick.DataProviderID = [@DataProviderListTbl].DataProviderID
			INNER JOIN @PairListTbl ON Tick.FXPairID = [@PairListTbl].FxPairId
		WHERE
			(TickTime between @StartTickTime and @EndTickTime)

		UNION
		SELECT TickID,
				COALESCE(OfferTimeStamp, TickTime) AS QuoteTimeStamp,
				Ask AS QuotePrice,
				AskSize AS QuoteSize,
				PairCode AS QuoteSymbol,
				0 AS IsBid,
				ProviderCode,
				EndOfContinuousData
		FROM Tick with (NOLOCK)
			INNER JOIN @DataProviderListTbl ON Tick.DataProviderID = [@DataProviderListTbl].DataProviderID
			INNER JOIN @PairListTbl ON Tick.FXPairID = [@PairListTbl].FxPairId
		WHERE
			(TickTime BETWEEN @StartTickTime AND @EndTickTime)
		)


	SELECT TickID, QuoteTimeStamp, QuotePrice, QuoteSize, QuoteSymbol, IsBid, Provider, OneSidedQuote
		FROM @QuoteIdList 
		ORDER BY 
			QuoteTimeStamp,
			IsBid,
			CASE WHEN IsBid = 1 THEN -QuotePrice ELSE QuotePrice END
			ASC

END

GO
GRANT EXECUTE ON [db_datareader].[GetAllFiltredQuotesNOTUSED] TO [SqlMoSPExecuterAccess] AS [db_datareader]
GO
/****** Object:  StoredProcedure [db_datareader].[GetQuotesForSymbolAndProvider]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Krivanek Jan
-- Create date: June 08 2009
-- Description:	Returns all sorted Quotes specified by parameters
-- MODIFIED 2010/Feb/22 by Michal Kreslik
-- =============================================
CREATE PROCEDURE [db_datareader].[GetQuotesForSymbolAndProvider] 
	@DataProviderCode CHAR(3),
	@PairCode CHAR(6),
	@StartTickTime numeric(17,0),
	@EndTickTime numeric(17,0),
	@MinSize int,
	@FilterOutDiscountQuotes BIT,
	@LoadSizeInfo BIT

AS
BEGIN

	DECLARE @QuoteIdList TABLE 
			(TickId bigint,
			QuoteTimeStamp	NUMERIC(17, 0),
			QuotePrice REAL,
			QuoteSize INT,
			IsBid BIT
--,			OneSidedQuote BIT
			)

	DECLARE @DataProviderId tinyint
	DECLARE @FxPairId tinyint
	
	SELECT @DataProviderId = DataProviderId
	FROM DataProvider
	WHERE DprCode = @DataProviderCode

	SELECT @FxPairId = FxPairId
	FROM FxPair
	WHERE FxpCode = @PairCode

	INSERT INTO @QuoteIdList
		(TickID, 
		QuoteTimeStamp,
		QuotePrice,
		QuoteSize, 
		IsBid
--,		OneSidedQuote
		)
		(SELECT TickID,
--				COALESCE(BidTimeStamp,TickTime) AS QuoteTimeStamp,
				COALESCE(TickTime,BidTimeStamp) AS QuoteTimeStamp,
				Bid AS QuotePrice,
				BidSize AS QuoteSize,
				1 AS IsBid
--,
--				EndOfContinuousData
		FROM Tick with (NOLOCK)
		WHERE
			DataProviderID = @DataProviderId AND
			FXPairID = @FxPairId AND
			(TickTime BETWEEN @StartTickTime AND @EndTickTime) AND
			(BadTick is null or BadTick = 0) AND
			1 = case
					when (@FilterOutDiscountQuotes = 1 AND Bid <= Ask) then 1
					when @FilterOutDiscountQuotes = 0 then 1
				end

		UNION
		SELECT TickID,
--				COALESCE(OfferTimeStamp, TickTime) AS QuoteTimeStamp,
				COALESCE(TickTime,OfferTimeStamp) AS QuoteTimeStamp,
				Ask AS QuotePrice,
				AskSize AS QuoteSize,
				0 AS IsBid
--,
--				EndOfContinuousData
		FROM Tick with (NOLOCK)
		WHERE
			DataProviderID = @DataProviderId AND
			FXPairID = @FxPairId AND
			(TickTime BETWEEN @StartTickTime AND @EndTickTime) AND
			(BadTick is null or BadTick = 0) AND
			1 = case
					when (@FilterOutDiscountQuotes = 1 AND Bid <= Ask) then 1
					when @FilterOutDiscountQuotes = 0 then 1
				end
		)

	SELECT
			QuoteTimeStamp,
			QuotePrice,

			case when @LoadSizeInfo = 1
				then QuoteSize
				else NULL
			end
			AS QuoteSize,

			IsBid --, OneSidedQuote

		FROM @QuoteIdList
		WHERE QuoteSize >= @MinSize 
		ORDER BY 
			QuoteTimeStamp,
			IsBid,
			CASE WHEN IsBid = 1 THEN -QuotePrice ELSE QuotePrice END
			ASC

option(recompile)

END
GO
GRANT EXECUTE ON [db_datareader].[GetQuotesForSymbolAndProvider] TO [SqlMoSPExecuterAccess] AS [db_datareader]
GO
/****** Object:  StoredProcedure [dbo].[AddNewFXPair]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddNewFXPair] 

@Base char(3),
@Quote char(3),
@Multiplier smallint,
@ExamplePrice real

AS
BEGIN
SET NOCOUNT ON;

declare @BaseID smallint;
declare @QuoteID smallint;

set @BaseID = (select CurrencyID from Currency where CurrencyAlphaCode = @Base);
set @QuoteID = (select CurrencyID from Currency where CurrencyAlphaCode = @Quote);

insert into FXPair (FxpCode, BaseCurrencyID, QuoteCurrencyID, Multiplier, ExamplePrice)
 values (@Base + @Quote, @BaseID, @QuoteID, @Multiplier, @ExamplePrice);

select * from FXPair where BaseCurrencyID = @BaseID and QuoteCurrencyID = @QuoteID;

END

GO
/****** Object:  StoredProcedure [dbo].[CalculateEodUsdConversions_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CalculateEodUsdConversions_SP]
(
	--Both nulls - will calc only for the last day
	@StartDateInclusive DATE = NULL,
	@EndDateInclusive DATE = NULL
)
AS
BEGIN

	DECLARE @USDId SMALLINT
	SELECT @USDId = [CurrencyID] FROM [dbo].[Currency] WITH (NOLOCK) WHERE [CurrencyAlphaCode] = 'USD'

	IF @StartDateInclusive IS NULL
	BEGIN
		SELECT @StartDateInclusive = MAX(Date) FROM [dbo].[EODRate]
	END

	IF @EndDateInclusive IS NULL
	BEGIN
		SELECT @EndDateInclusive = MAX(Date) FROM [dbo].[EODRate]
	END
	
	IF @StartDateInclusive IS NULL OR @EndDateInclusive IS NULL
	BEGIN
		RETURN;
	END
	
	--First get rid of any overlapping records (e.g. dummy current day records)
	DELETE FROM [dbo].[EODUsdConversions] WHERE [Date] >= @StartDateInclusive AND [Date] <= @EndDateInclusive


	;WITH mycte AS
		(
			SELECT CAST(@StartDateInclusive AS DATETIME) DateValue
			UNION ALL
			SELECT  DateValue + 1
			FROM    mycte   
			WHERE   DateValue + 1 <= @EndDateInclusive
		)
		
	INSERT INTO 
		[dbo].[EODUsdConversions]
		   ([CurrencyId]
		   ,[Date]
		   ,[BidPrice]
		   ,[AskPrice])
		SELECT
			pair.BaseCurrencyId
			,[Date]
			,[BidPrice] AS BidPrice
			,[AskPrice] AS AskPrice
		FROM 
			[dbo].[EODRate] rate
			INNER JOIN [dbo].[FXPair] pair ON rate.FXPairId = pair.FXPairId
		WHERE	
			pair.QuoteCurrencyId = @USDId
			AND (@StartDateInclusive IS NULL OR rate.Date >= @StartDateInclusive)
			AND (@EndDateInclusive IS NULL OR rate.Date <= @EndDateInclusive)
			AND [BidPrice] IS NOT NULL AND [AskPrice] IS NOT NULL
		UNION ALL
		SELECT 
			pair.QuoteCurrencyId
			,[Date]
			,1/[AskPrice] AS BidPrice
			,1/[BidPrice] AS AskPrice
		FROM 
			[dbo].[EODRate] rate
			INNER JOIN [dbo].[FXPair] pair ON rate.FXPairId = pair.FXPairId
		WHERE	
			pair.BaseCurrencyId = @USDId
			AND (@StartDateInclusive IS NULL OR rate.Date >= @StartDateInclusive)
			AND (@EndDateInclusive IS NULL OR rate.Date <= @EndDateInclusive)
			AND [BidPrice] IS NOT NULL AND [AskPrice] IS NOT NULL
		UNION ALL
		SELECT
			@USDId, 
			CAST(DateValue AS DATE),
			1 AS BidPrice,
			1 AS AskPrice
		FROM
			mycte OPTION (MAXRECURSION 0)
			
			
		DECLARE @Today DATE = CAST(GETUTCDATE() AS DATE)
			
		-- insert dummy records for current day
		IF DATEADD(Day,1,@EndDateInclusive) = @Today AND @StartDateInclusive = @EndDateInclusive
		BEGIN
		
			INSERT INTO 
			[dbo].[EODUsdConversions]
			   ([CurrencyId]
			   ,[Date]
			   ,[BidPrice]
			   ,[AskPrice])
			SELECT
				pair.BaseCurrencyId
				,@Today
				,[BidPrice] AS BidPrice
				,[AskPrice] AS AskPrice
			FROM 
				[dbo].[EODRate] rate
				INNER JOIN [dbo].[FXPair] pair ON rate.FXPairId = pair.FXPairId
			WHERE	
				pair.QuoteCurrencyId = @USDId
				AND rate.Date = @EndDateInclusive
				AND [BidPrice] IS NOT NULL AND [AskPrice] IS NOT NULL
			UNION ALL
			SELECT 
				pair.QuoteCurrencyId
				,@Today
				,1/[AskPrice] AS BidPrice
				,1/[BidPrice] AS AskPrice
			FROM 
				[dbo].[EODRate] rate
				INNER JOIN [dbo].[FXPair] pair ON rate.FXPairId = pair.FXPairId
			WHERE	
				pair.BaseCurrencyId = @USDId
				AND rate.Date = @EndDateInclusive
				AND [BidPrice] IS NOT NULL AND [AskPrice] IS NOT NULL
			UNION ALL
			SELECT
				@USDId, 
				@Today,
				1 AS BidPrice,
				1 AS AskPrice
			
		END

END

GO
/****** Object:  StoredProcedure [dbo].[CalculateSingleDayEodRates_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CalculateSingleDayEodRates_SP]
(
	@Day DATE = NULL,
	@DeleteExisting BIT = 1,
	@InsertSameDataForFollowingDay BIT = 1
)
AS
BEGIN
	
	IF @Day IS NULL
	BEGIN
		SET @Day=DATEADD(day, -1, GETUTCDATE())
	END
	
	IF @DeleteExisting = 1
	BEGIN
		DELETE FROM [dbo].[EODRate] WHERE [Date] = @Day
	END
	
	
	DECLARE @RolloverTimeUtc DATETIME
	DECLARE @RolloverTimeEt DATETIME = DATEADD(hour, 17, CAST(@day AS DATETIME))
	exec [dev03].[DataCollection_Testing].[dbo].sp_executesql N'SELECT @output = [DataCollection_Testing].[dbo].[ConvertFromEtToUtc](@input)',
	N'@input datetime, @output datetime OUTPUT',
	@input=@RolloverTimeEt,
	@output=@RolloverTimeUtc OUTPUT
	DECLARE @CutoffTimeUtc DATETIME = DATEADD(hour, -1, @RolloverTimeUtc)
	
	
	INSERT INTO [dbo].[EODRate]
	   ([FXPairId]
	   ,[Date]
	   ,[BidPrice]
	   ,[AskPrice]
	   ,[IsBidPriceAuthentic]
	   ,[IsAskPriceAuthentic])
	SELECT
		CurrentEodRates.FxPairId,
		@Day,
		COALESCE(CurrentEodRates.BidToB, PreviousEodRates.BidPrice) AS BidPrice,
		COALESCE(CurrentEodRates.AskToB, PreviousEodRates.AskPrice) AS AskPrice,
		CASE WHEN CurrentEodRates.BidToB IS NULL THEN 0 ELSE 1 END AS IsBidPriceAuthentic,
		CASE WHEN CurrentEodRates.AskToB IS NULL THEN 0 ELSE 1 END AS IsAskPriceAuthentic
	FROM
		(
		SELECT 
			pair.[FxPairId],
			EodRates.BidToB,
			EodRates.AskToB
		FROM 
			[dbo].[FxPair] pair
			LEFT JOIN
			(
				SELECT
					FxPairId,
					--ask
					MAX(CASE WHEN SideId = 1 THEN LastSidedPrice ELSE NULL END) AS AskToB,
					MIN(CASE WHEN SideId = 0 THEN LastSidedPrice ELSE NULL END) AS BidToB
				FROM
				(
					SELECT 
						FxPairId,
						CounterpartyId,
						SideId,
						-- last price in current millisecond interval = ToB in current millisecond
						LAST_VALUE(md.Price) 
							OVER(
								PARTITION BY FxPairId, CounterpartyId, SideId
								ORDER BY md.integratorReceivedTimeUtc ASC 
								ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) AS LastSidedPrice
					FROM
						[dbo].[MarketData] md with (nolock) 
					WHERE
						FXPairId IN (SELECT fxpairid from [dbo].[fxpair] WITH(NOLOCK))
						AND IntegratorReceivedTimeUtc BETWEEN @CutoffTimeUtc AND @RolloverTimeUtc
						AND RecordTypeId IN (0, 3)
				) tmp1
				GROUP BY FxPairId
			) EodRates
			ON EodRates.FxPairId = pair.[FxPairId]
		)CurrentEodRates
		LEFT JOIN [dbo].[EODRate] PreviousEodRates 
			ON PreviousEodRates.[Date] = DATEADD(Day, -1, @Day) 
			AND CurrentEodRates.FxPairId = PreviousEodRates.FxPairId
			
		IF @InsertSameDataForFollowingDay = 1
		BEGIN
			
			IF @DeleteExisting = 1
			BEGIN
				DELETE FROM [dbo].[EODRate] WHERE [Date] = DATEADD(day, 1, @Day)
			END
			
			INSERT INTO [dbo].[EODRate]
			   ([FXPairId]
			   ,[Date]
			   ,[BidPrice]
			   ,[AskPrice]
			   ,[IsBidPriceAuthentic]
			   ,[IsAskPriceAuthentic])
			SELECT
				[FXPairId]
			   ,DATEADD(day, 1, @Day)
			   ,[BidPrice]
			   ,[AskPrice]
			   ,[IsBidPriceAuthentic]
			   ,[IsAskPriceAuthentic]
			FROM
			   [dbo].[EODRate]
			WHERE
				[date] = @Day
			
		END
		
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteAllDuplicateTicks]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteAllDuplicateTicks] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

delete from Tick where UniqueTick = 0;

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllFXPairs]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllFXPairs] 
AS
BEGIN
	SET NOCOUNT ON;

SELECT 
	[FxPairId], [FxpCode]
FROM
	[dbo].[FXPair]

END




GO
GRANT EXECUTE ON [dbo].[GetAllFXPairs] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetAvgSpreadByHour]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAvgSpreadByHour] 
@FxPairId tinyint,
@DataProviderId tinyint,
@MinimumSize int,
@StartTickTime numeric(17,0),
@EndTickTime numeric(17,0)
AS
BEGIN
SET NOCOUNT ON;

-- replace with the dbo.GetLastBid procedure:
declare @LastBid float;

set @LastBid =
(select top 1 Bid
from Tick with (NOLOCK)
where DataProviderId = @DataProviderId and FxPairId = @FxPairId
and TickTime = (select max(TickTime) from Tick with (NOLOCK) where DataProviderId = @DataProviderId and FxPairId = @FxPairId));

declare @Multiplier int;
set @Multiplier = (select Multiplier from FxPair where FxPairId = @FxPairId);

----------------------------------------------
select
TickHourEST,
AvgSpreadDecimal * @Multiplier 'AvgSpread (pips)',
AvgSpreadDecimal 'AvgSpread (decimal)',
AvgSpreadPpm 'AvgSpread (ppm)',
AvgSpreadPpm * @LastBid / 1000000 'AvgSpread (based on last bid)',
TickCount
from
(
select TickHour 'TickHourEST', avg(SpreadPpm) 'AvgSpreadPpm', avg(SpreadDecimal) 'AvgSpreadDecimal', count(*) 'TickCount'
from

(select 

substring(cast(TickTime as char(17)),9,2) 'TickHour',
1000000*((ask-bid)/((ask+bid)/2)) 'SpreadPpm', (ask-bid) 'SpreadDecimal'

from Tick with (NOLOCK)
where
(DataProviderId = @DataProviderId)
and FxPairId = @FxPairId
and BidSize >= @MinimumSize and AskSize >= @MinimumSize
and (TickTime between @StartTickTime and @EndTickTime)
) Ticks

group by TickHour
) temp

order by TickHourEST

END
GO
/****** Object:  StoredProcedure [dbo].[GetAvgSpreadByHourGMTBidTime]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAvgSpreadByHourGMTBidTime] 
@FxPairId tinyint,
@DataProviderId tinyint,
@MinimumSize int,
@StartTickTime numeric(17,0),
@EndTickTime numeric(17,0)
AS
BEGIN
SET NOCOUNT ON;

-- replace with the dbo.GetLastBid procedure:
declare @LastBid float;

set @LastBid =
(select top 1 Bid
from Tick with (NOLOCK)
where DataProviderId = @DataProviderId and FxPairId = @FxPairId
and TickTime = (select max(TickTime) from Tick with (NOLOCK) where DataProviderId = @DataProviderId and FxPairId = @FxPairId));

declare @Multiplier int;
set @Multiplier = (select Multiplier from FxPair where FxPairId = @FxPairId);

----------------------------------------------
select
TickHourGMT,
AvgSpreadDecimal * @Multiplier 'AvgSpread (pips)',
AvgSpreadDecimal 'AvgSpread (decimal)',
AvgSpreadPpm 'AvgSpread (ppm)',
AvgSpreadPpm * @LastBid / 1000000 'AvgSpread (based on last bid)',
TickCount
from
(
select TickHour 'TickHourGMT', avg(SpreadPpm) 'AvgSpreadPpm', avg(SpreadDecimal) 'AvgSpreadDecimal', count(*) 'TickCount'
from

(select 

substring(cast(BidTimeStamp as char(17)),9,2) 'TickHour',
1000000*((ask-bid)/((ask+bid)/2)) 'SpreadPpm', (ask-bid) 'SpreadDecimal'

from Tick with (NOLOCK)
where
(DataProviderId = @DataProviderId)
and FxPairId = @FxPairId
and BidSize >= @MinimumSize and AskSize >= @MinimumSize
and (TickTime between @StartTickTime and @EndTickTime)
) Ticks

group by TickHour
) temp

order by TickHourGMT

END
GO
/****** Object:  StoredProcedure [dbo].[GetBankPoolMarketData_DEBUG_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetBankPoolMarketData_DEBUG_SP]
	@FxPairNoSlash char(6),
	@StartDateTime DateTime2,
	@EndDateTime DateTime2,
	@SortBySourceTimeStamps bit
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

declare @FxPairId tinyint = (select f.FxPairId from fxpair f where f.fxpcode=@FxPairNoSlash);

DECLARE @StreamsToIgnore TABLE (LiqProviderStreamName char(3));

-------------------------------------------
INSERT INTO @StreamsToIgnore VALUES ('BNP')
INSERT INTO @StreamsToIgnore VALUES ('BOA')
INSERT INTO @StreamsToIgnore VALUES ('BRX')
INSERT INTO @StreamsToIgnore VALUES ('CRS')
INSERT INTO @StreamsToIgnore VALUES ('CTI')
INSERT INTO @StreamsToIgnore VALUES ('CZB')
INSERT INTO @StreamsToIgnore VALUES ('DBK')
INSERT INTO @StreamsToIgnore VALUES ('GLS')
INSERT INTO @StreamsToIgnore VALUES ('HSB')
INSERT INTO @StreamsToIgnore VALUES ('JPM')
INSERT INTO @StreamsToIgnore VALUES ('MGS')
INSERT INTO @StreamsToIgnore VALUES ('NOM')
INSERT INTO @StreamsToIgnore VALUES ('RBS')
INSERT INTO @StreamsToIgnore VALUES ('SOC')
INSERT INTO @StreamsToIgnore VALUES ('UBS')

INSERT INTO @StreamsToIgnore VALUES ('HTA')
--INSERT INTO @StreamsToIgnore VALUES ('HTF')
INSERT INTO @StreamsToIgnore VALUES ('FA1')
INSERT INTO @StreamsToIgnore VALUES ('LM1')
-------------------------------------------

IF @SortBySourceTimeStamps=0

	BEGIN

		select 

		md.CounterpartySentTimeUtc,
		md.IntegratorReceivedTimeUtc,
		side.PriceSideName'PriceSide',
		md.Price,
		lp.LiqProviderStreamName'LpStream',
		md.Size

		from 

		MarketData md with(nolock)
		left join LiqProviderStream lp on md.CounterpartyId=lp.LiqProviderStreamId
		left join PriceSide side on md.SideId=side.PriceSideId

		where 

		md.FXPairId = @FxPairId
		and md.IntegratorReceivedTimeUtc between @StartDateTime and @EndDateTime
		and md.CounterpartyId not in (select LiqProviderStreamId from LiqProviderStream lps where lps.LiqProviderStreamName in (select LiqProviderStreamName from @StreamsToIgnore))

		order by IntegratorReceivedTimeUtc asc

	END

ELSE

	BEGIN

		select 

		md.CounterpartySentTimeUtc,
		md.IntegratorReceivedTimeUtc,
		side.PriceSideName'PriceSide',
		md.Price,
		lp.LiqProviderStreamName'LpStream',
		md.Size

		from 

		MarketData md with(nolock)
		left join LiqProviderStream lp on md.CounterpartyId=lp.LiqProviderStreamId
		left join PriceSide side on md.SideId=side.PriceSideId

		where 

		md.FXPairId = @FxPairId
		and md.IntegratorReceivedTimeUtc between @StartDateTime and @EndDateTime
		and md.CounterpartyId not in (select LiqProviderStreamId from LiqProviderStream lps where lps.LiqProviderStreamName in (select LiqProviderStreamName from @StreamsToIgnore))

		order by CounterpartySentTimeUtc asc

	END

END
GO
/****** Object:  StoredProcedure [dbo].[GetBankPoolMarketData_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetBankPoolMarketData_SP]
	@FxPairNoSlash char(6),
	@StartDateTime DateTime2,
	@EndDateTime DateTime2,
	@SortBySourceTimeStamps bit
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

declare @FxPairId tinyint = (select f.FxPairId from fxpair f where f.fxpcode=@FxPairNoSlash);

DECLARE @StreamsToIgnore TABLE (LiqProviderStreamName char(3));

-------------------------------------------
INSERT INTO @StreamsToIgnore VALUES ('FA1')
INSERT INTO @StreamsToIgnore VALUES ('HTA')
INSERT INTO @StreamsToIgnore VALUES ('HTF')

INSERT INTO @StreamsToIgnore VALUES ('DBK')
INSERT INTO @StreamsToIgnore VALUES ('GLS')
INSERT INTO @StreamsToIgnore VALUES ('NOM')
INSERT INTO @StreamsToIgnore VALUES ('UBS')
-------------------------------------------

IF @SortBySourceTimeStamps=0

	BEGIN

		select 

		md.CounterpartySentTimeUtc,
		md.IntegratorReceivedTimeUtc,
		side.PriceSideName'PriceSide',
		md.Price,
		lp.LiqProviderStreamName'LpStream',
		md.Size

		from 

		MarketData md with(nolock)
		left join LiqProviderStream lp on md.CounterpartyId=lp.LiqProviderStreamId
		left join PriceSide side on md.SideId=side.PriceSideId

		where 

		md.FXPairId = @FxPairId
		and md.IntegratorReceivedTimeUtc between @StartDateTime and @EndDateTime
		and md.CounterpartyId not in (select LiqProviderStreamId from LiqProviderStream lps where lps.LiqProviderStreamName in (select LiqProviderStreamName from @StreamsToIgnore))

		order by IntegratorReceivedTimeUtc asc

	END

ELSE

	BEGIN

		select 

		md.CounterpartySentTimeUtc,
		md.IntegratorReceivedTimeUtc,
		side.PriceSideName'PriceSide',
		md.Price,
		lp.LiqProviderStreamName'LpStream',
		md.Size

		from 

		MarketData md with(nolock)
		left join LiqProviderStream lp on md.CounterpartyId=lp.LiqProviderStreamId
		left join PriceSide side on md.SideId=side.PriceSideId

		where 

		md.FXPairId = @FxPairId
		and md.IntegratorReceivedTimeUtc between @StartDateTime and @EndDateTime
		and md.CounterpartyId not in (select LiqProviderStreamId from LiqProviderStream lps where lps.LiqProviderStreamName in (select LiqProviderStreamName from @StreamsToIgnore))

		order by CounterpartySentTimeUtc asc

	END

END
GO
GRANT EXECUTE ON [dbo].[GetBankPoolMarketData_SP] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetBankPoolMarketDataSelective_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetBankPoolMarketDataSelective_SP]
	@FxPairNoSlash char(6),
	@StartDateTime DateTime2,
	@EndDateTime DateTime2,
	@SortBySourceTimeStamps bit,

@IncludeBNP bit,
@IncludeBOA bit,
@IncludeBRX bit,
@IncludeCRS bit,
@IncludeCTI bit,
@IncludeCZB bit,
@IncludeDBK bit,
@IncludeFA1 bit,
@IncludeFC1 bit,
@IncludeFC2 bit,
@IncludeGLS bit,
@IncludeHSB bit,
@IncludeHTA bit,
@IncludeHTF bit,
@IncludeHT3 bit,
@IncludeJPM bit,

@IncludeL01 bit,
@IncludeL02 bit,
@IncludeL03 bit,
@IncludeL04 bit,
@IncludeL05 bit,
@IncludeL06 bit,
@IncludeL07 bit,
@IncludeL08 bit,
@IncludeL09 bit,
@IncludeL10 bit,
@IncludeL11 bit,
@IncludeL12 bit,
@IncludeL13 bit,
@IncludeL14 bit,
@IncludeL15 bit,
@IncludeL16 bit,
@IncludeL17 bit,
@IncludeL18 bit,
@IncludeL19 bit,
@IncludeL20 bit,

@IncludeLM2 bit,
@IncludeLM3 bit,
@IncludeMGS bit,
@IncludeNOM bit,
@IncludeRBS bit,
@IncludeSOC bit,
@IncludeUBS bit

AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

declare @FxPairId tinyint = (select f.FxPairId from fxpair f with(nolock) where f.fxpcode=@FxPairNoSlash);

DECLARE @StreamsToInclude TABLE (LiqProviderStreamName char(3));

-------------------------------------------
IF @IncludeBNP=1 INSERT INTO @StreamsToInclude VALUES ('BNP')
IF @IncludeBOA=1 INSERT INTO @StreamsToInclude VALUES ('BOA')
IF @IncludeBRX=1 INSERT INTO @StreamsToInclude VALUES ('BRX')
IF @IncludeCRS=1 INSERT INTO @StreamsToInclude VALUES ('CRS')
IF @IncludeCTI=1 INSERT INTO @StreamsToInclude VALUES ('CTI')
IF @IncludeCZB=1 INSERT INTO @StreamsToInclude VALUES ('CZB')
IF @IncludeDBK=1 INSERT INTO @StreamsToInclude VALUES ('DBK')
IF @IncludeFA1=1 INSERT INTO @StreamsToInclude VALUES ('FA1')
IF @IncludeFC1=1 INSERT INTO @StreamsToInclude VALUES ('FC1')
IF @IncludeFC2=1 INSERT INTO @StreamsToInclude VALUES ('FC2')
IF @IncludeGLS=1 INSERT INTO @StreamsToInclude VALUES ('GLS')
IF @IncludeHSB=1 INSERT INTO @StreamsToInclude VALUES ('HSB')
IF @IncludeHTA=1 INSERT INTO @StreamsToInclude VALUES ('HTA')
IF @IncludeHTF=1 INSERT INTO @StreamsToInclude VALUES ('HTF')
IF @IncludeHT3=1 INSERT INTO @StreamsToInclude VALUES ('HT3')
IF @IncludeJPM=1 INSERT INTO @StreamsToInclude VALUES ('JPM')

IF @IncludeL01=1 INSERT INTO @StreamsToInclude VALUES ('L01')
IF @IncludeL02=1 INSERT INTO @StreamsToInclude VALUES ('L02')
IF @IncludeL03=1 INSERT INTO @StreamsToInclude VALUES ('L03')
IF @IncludeL04=1 INSERT INTO @StreamsToInclude VALUES ('L04')
IF @IncludeL05=1 INSERT INTO @StreamsToInclude VALUES ('L05')
IF @IncludeL06=1 INSERT INTO @StreamsToInclude VALUES ('L06')
IF @IncludeL07=1 INSERT INTO @StreamsToInclude VALUES ('L07')
IF @IncludeL08=1 INSERT INTO @StreamsToInclude VALUES ('L08')
IF @IncludeL09=1 INSERT INTO @StreamsToInclude VALUES ('L09')
IF @IncludeL10=1 INSERT INTO @StreamsToInclude VALUES ('L10')
IF @IncludeL11=1 INSERT INTO @StreamsToInclude VALUES ('L11')
IF @IncludeL12=1 INSERT INTO @StreamsToInclude VALUES ('L12')
IF @IncludeL13=1 INSERT INTO @StreamsToInclude VALUES ('L13')
IF @IncludeL14=1 INSERT INTO @StreamsToInclude VALUES ('L14')
IF @IncludeL15=1 INSERT INTO @StreamsToInclude VALUES ('L15')
IF @IncludeL16=1 INSERT INTO @StreamsToInclude VALUES ('L16')
IF @IncludeL17=1 INSERT INTO @StreamsToInclude VALUES ('L17')
IF @IncludeL18=1 INSERT INTO @StreamsToInclude VALUES ('L18')
IF @IncludeL19=1 INSERT INTO @StreamsToInclude VALUES ('L19')
IF @IncludeL20=1 INSERT INTO @StreamsToInclude VALUES ('L20')

IF @IncludeLM2=1 INSERT INTO @StreamsToInclude VALUES ('LM2')
IF @IncludeLM3=1 INSERT INTO @StreamsToInclude VALUES ('LM3')
IF @IncludeMGS=1 INSERT INTO @StreamsToInclude VALUES ('MGS')
IF @IncludeNOM=1 INSERT INTO @StreamsToInclude VALUES ('NOM')
IF @IncludeRBS=1 INSERT INTO @StreamsToInclude VALUES ('RBS')
IF @IncludeSOC=1 INSERT INTO @StreamsToInclude VALUES ('SOC')
IF @IncludeUBS=1 INSERT INTO @StreamsToInclude VALUES ('UBS')
-------------------------------------------

IF @SortBySourceTimeStamps=0

	BEGIN

		select 

		md.CounterpartySentTimeUtc,
		md.IntegratorReceivedTimeUtc,
		side.PriceSideName'PriceSide',
		md.Price,
		lp.LiqProviderStreamName'LpStream',
		md.Size,
		md.IntegratorPriceIdentity,
		md.RecordTypeId

		from 

		MarketData md with(nolock)
		left join LiqProviderStream lp  with(nolock) on md.CounterpartyId=lp.LiqProviderStreamId
		left join PriceSide side  with(nolock) on md.SideId=side.PriceSideId

		where 

		md.FXPairId = @FxPairId
		and md.IntegratorReceivedTimeUtc between @StartDateTime and @EndDateTime
		and md.CounterpartyId in (select LiqProviderStreamId from LiqProviderStream lps  with(nolock) where lps.LiqProviderStreamName in (select LiqProviderStreamName from @StreamsToInclude))

		order by IntegratorReceivedTimeUtc asc

	END

ELSE

	BEGIN

		select 

		md.CounterpartySentTimeUtc,
		md.IntegratorReceivedTimeUtc,
		side.PriceSideName'PriceSide',
		md.Price,
		lp.LiqProviderStreamName'LpStream',
		md.Size,
		md.IntegratorPriceIdentity,
		md.RecordTypeId

		from 

		MarketData md with(nolock)
		left join LiqProviderStream lp  with(nolock) on md.CounterpartyId=lp.LiqProviderStreamId
		left join PriceSide side  with(nolock) on md.SideId=side.PriceSideId

		where 

		md.FXPairId = @FxPairId
		and md.IntegratorReceivedTimeUtc between @StartDateTime and @EndDateTime
		and md.CounterpartyId in (select LiqProviderStreamId from LiqProviderStream lps  with(nolock) where lps.LiqProviderStreamName in (select LiqProviderStreamName from @StreamsToInclude))

		order by CounterpartySentTimeUtc asc

	END

END
GO
GRANT EXECUTE ON [dbo].[GetBankPoolMarketDataSelective_SP] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetBankPoolMarketDataWithWaitForNewData_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetBankPoolMarketDataWithWaitForNewData_SP]
	@FxPairNoSlash char(6),
	@StartDateTime DateTime2,
	@EndDateTime DateTime2,
	@SortBySourceTimeStamps bit
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

DECLARE @EndDateTimeWithReserve DateTime2;
-- add a reserve of 30 seconds to @EndDateTime, so that SQL server has enough time to write ALL the data between 
-- @StartDateTime and @EndDateTime
set @EndDateTimeWithReserve = DATEADD(second, 30, @EndDateTime);

DECLARE	@NewerDataExists bit;
set @NewerDataExists=0;

WHILE @NewerDataExists=0

	BEGIN
	EXEC	@NewerDataExists = [dbo].[GetIfNewerDataExistsForSymbol_SP]
			@DateTime2Value = @EndDateTimeWithReserve,
			@SymbolNoSlash = @FxPairNoSlash

		if @NewerDataExists=0 
			BEGIN
			-- waiting for the new data to become available in the database:
			WAITFOR DELAY '00:00:05' -- 5 seconds
			END

	END

-- the new data is now available:

EXEC	[dbo].[GetBankPoolMarketData_SP]
		@FxPairNoSlash = @FxPairNoSlash,
		@StartDateTime = @StartDateTime,
		@EndDateTime = @EndDateTime,
		@SortBySourceTimeStamps = @SortBySourceTimeStamps

END
GO
GRANT EXECUTE ON [dbo].[GetBankPoolMarketDataWithWaitForNewData_SP] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetBankPoolMarketDataWithWaitForNewDataSelective_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetBankPoolMarketDataWithWaitForNewDataSelective_SP]
	@FxPairNoSlash char(6),
	@StartDateTime DateTime2,
	@EndDateTime DateTime2,
	@SortBySourceTimeStamps bit,

@IncludeBNP bit,
@IncludeBOA bit,
@IncludeBRX bit,
@IncludeCRS bit,
@IncludeCTI bit,
@IncludeCZB bit,
@IncludeDBK bit,
@IncludeFA1 bit,
@IncludeFC1 bit,
@IncludeFC2 bit,
@IncludeGLS bit,
@IncludeHSB bit,
@IncludeHTA bit,
@IncludeHTF bit,
@IncludeHT3 bit,
@IncludeJPM bit,

@IncludeL01 bit,
@IncludeL02 bit,
@IncludeL03 bit,
@IncludeL04 bit,
@IncludeL05 bit,
@IncludeL06 bit,
@IncludeL07 bit,
@IncludeL08 bit,
@IncludeL09 bit,
@IncludeL10 bit,
@IncludeL11 bit,
@IncludeL12 bit,
@IncludeL13 bit,
@IncludeL14 bit,
@IncludeL15 bit,
@IncludeL16 bit,
@IncludeL17 bit,
@IncludeL18 bit,
@IncludeL19 bit,
@IncludeL20 bit,

@IncludeLM2 bit,
@IncludeLM3 bit,
@IncludeMGS bit,
@IncludeNOM bit,
@IncludeRBS bit,
@IncludeSOC bit,
@IncludeUBS bit


AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

DECLARE @EndDateTimeWithReserve DateTime2;
-- add a reserve of 30 seconds to @EndDateTime, so that SQL server has enough time to write ALL the data between 
-- @StartDateTime and @EndDateTime
set @EndDateTimeWithReserve = DATEADD(second, 30, @EndDateTime);

DECLARE	@NewerDataExists bit;
set @NewerDataExists=0;

WHILE @NewerDataExists=0

	BEGIN
	EXEC	@NewerDataExists = [dbo].[GetIfNewerDataExistsForSymbol_SP]
			@DateTime2Value = @EndDateTimeWithReserve,
			@SymbolNoSlash = @FxPairNoSlash

		if @NewerDataExists=0 
			BEGIN
			-- waiting for the new data to become available in the database:
			WAITFOR DELAY '00:00:05' -- 5 seconds
			END

	END

-- the new data is now available:

EXEC	[dbo].[GetBankPoolMarketDataSelective_SP]
		@FxPairNoSlash = @FxPairNoSlash,
		@StartDateTime = @StartDateTime,
		@EndDateTime = @EndDateTime,
		@SortBySourceTimeStamps = @SortBySourceTimeStamps,


@IncludeBNP =@IncludeBNP,
@IncludeBOA =@IncludeBOA,
@IncludeBRX =@IncludeBRX,
@IncludeCRS =@IncludeCRS,
@IncludeCTI =@IncludeCTI,
@IncludeCZB =@IncludeCZB,
@IncludeDBK =@IncludeDBK,
@IncludeFA1 =@IncludeFA1,
@IncludeFC1 =@IncludeFC1,
@IncludeFC2 =@IncludeFC2,
@IncludeGLS =@IncludeGLS,
@IncludeHSB =@IncludeHSB,
@IncludeHTA =@IncludeHTA,
@IncludeHTF =@IncludeHTF,
@IncludeHT3 =@IncludeHT3,
@IncludeJPM =@IncludeJPM,

@IncludeL01 =@IncludeL01,
@IncludeL02 =@IncludeL02,
@IncludeL03 =@IncludeL03,
@IncludeL04 =@IncludeL04,
@IncludeL05 =@IncludeL05,
@IncludeL06 =@IncludeL06,
@IncludeL07 =@IncludeL07,
@IncludeL08 =@IncludeL08,
@IncludeL09 =@IncludeL09,
@IncludeL10 =@IncludeL10,
@IncludeL11 =@IncludeL11,
@IncludeL12 =@IncludeL12,
@IncludeL13 =@IncludeL13,
@IncludeL14 =@IncludeL14,
@IncludeL15 =@IncludeL15,
@IncludeL16 =@IncludeL16,
@IncludeL17 =@IncludeL17,
@IncludeL18 =@IncludeL18,
@IncludeL19 =@IncludeL19,
@IncludeL20 =@IncludeL20,

@IncludeLM2 =@IncludeLM2,
@IncludeLM3 =@IncludeLM3,
@IncludeMGS =@IncludeMGS,
@IncludeNOM =@IncludeNOM,
@IncludeRBS =@IncludeRBS,
@IncludeSOC =@IncludeSOC,
@IncludeUBS =@IncludeUBS

END
GO
GRANT EXECUTE ON [dbo].[GetBankPoolMarketDataWithWaitForNewDataSelective_SP] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetBidAskTimeByProviderPairTime]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetBidAskTimeByProviderPairTime]
@DataProviderId tinyint,
@FxPairId tinyint,
@StartTickTime numeric(17,0),
@EndTickTime numeric(17,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select Bid, Ask, TickTime
	from Tick with (NOLOCK)
	where
		DataProviderId = @DataProviderId
		and FxPairId = @FxPairId
		and (TickTime between @StartTickTime and @EndTickTime)
	order by TickTime asc

option(recompile)
END
GO
/****** Object:  StoredProcedure [dbo].[GetCounterpartySymbolSettings_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--update SP accordingly
CREATE PROCEDURE [dbo].[GetCounterpartySymbolSettings_SP] 
AS
BEGIN

	SELECT 
		targetType.Code AS TradingTargetType
		,pair.FxpCode AS Symbol
		,settings.[Supported] AS Supported
		,settings.[MinimumPriceIncrement] AS MinimumPriceIncrement
		,settings.[OrderMinimumSize] AS OrderMinimumSize
		,settings.[OrderMinimumSizeIncrement] AS OrderMinimumSizeIncrement
	FROM 
		[dbo].[CounterpartySymbolSettings] settings
		INNER JOIN [dbo].[FxPair] pair ON pair.FxPairId = settings.[SymbolId]
		INNER JOIN [dbo].[TradingTargetType] targetType ON targetType.Id = settings.[TradingTargetTypeId]

END

GO
GRANT EXECUTE ON [dbo].[GetCounterpartySymbolSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetDataProviderCodeById]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetDataProviderCodeById]
@DataProviderId tinyint,
@DataProviderCode char(3) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select @DataProviderCode = 
		(select DprCode 
		from DataProvider 
		where DataProviderId = @DataProviderId)
END

GO
GRANT EXECUTE ON [dbo].[GetDataProviderCodeById] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetFastMatchDealsData_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetFastMatchDealsData_SP]
	@FxPairNoSlash char(6),
	@StartDateTimeUtc DateTime2,
	@EndDateTimeUtc DateTime2
AS

BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

declare @FastMatchName1 char(3) = 'FC1';
declare @FastMatchName2 char(3) = 'FC2';
--
declare @FxPairId tinyint = (select f.FxPairId from FxPair f with(nolock) where f.FxpCode=@FxPairNoSlash);
declare @CounterpartyId1 tinyint = (select c.LiqProviderStreamId from Counterparty c with(nolock) where c.LiqProviderStreamName=@FastMatchName1);
declare @CounterpartyId2 tinyint = (select c.LiqProviderStreamId from Counterparty c with(nolock) where c.LiqProviderStreamName=@FastMatchName2);
--

select

md.IntegratorReceivedTimeUtc,
md.Price,
md.Size,
c.LiqProviderStreamName'Pool'

from 

MarketData md with(nolock)
left join Counterparty c with(nolock) on md.CounterpartyId=c.LiqProviderStreamId

where

md.FXPairId=@FxPairId

and md.IntegratorReceivedTimeUtc between @StartDateTimeUtc and @EndDateTimeUtc
and md.CounterpartyId in (@CounterpartyId1, @CounterpartyId2)


and md.RecordTypeId = 6

order by md.IntegratorReceivedTimeUtc

END
GO
GRANT EXECUTE ON [dbo].[GetFastMatchDealsData_SP] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetFxPairId]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetFxPairId]
@BaseCurrencyAlpha char(3),
@QuoteCurrencyAlpha char(3),
@FxPairId tinyint output

AS
BEGIN
	SET NOCOUNT ON;

declare
@BaseCurrencyId smallint,
@QuoteCurrencyId smallint;

set @BaseCurrencyId = (select CurrencyId from Currency where CurrencyAlphaCode = @BaseCurrencyAlpha);
set @QuoteCurrencyId = (select CurrencyId from Currency where CurrencyAlphaCode = @QuoteCurrencyAlpha);

select @FxPairId = (select FxPairId from FxPair where BaseCurrencyId = @BaseCurrencyId and QuoteCurrencyId = @QuoteCurrencyId);

END

GO
/****** Object:  StoredProcedure [dbo].[GetHtaDealsData_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetHtaDealsData_SP]
	@FxPairNoSlash char(6),
	@StartDateTimeUtc DateTime2,
	@EndDateTimeUtc DateTime2
AS

BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

declare @HotspotAllStreamName char(3) = 'HTA';
--
declare @FxPairId tinyint = (select f.FxPairId from FxPair f with(nolock) where f.FxpCode=@FxPairNoSlash);
declare @CounterpartyId tinyint = (select c.LiqProviderStreamId from Counterparty c with(nolock) where c.LiqProviderStreamName=@HotspotAllStreamName);
--

select

md.IntegratorReceivedTimeUtc,
md.Price,
ts.TradeSide

from 

MarketData md with(nolock)
left join TradeSide ts with(nolock) on md.TradeSideId=ts.TradeSideId

where

md.FXPairId=@FxPairId
and md.IntegratorReceivedTimeUtc between @StartDateTimeUtc and @EndDateTimeUtc
and md.CounterpartyId = @CounterpartyId

and md.TradeSideId is not null
--andd md.RecordTypeId = 6 -- Venue Trade Data (slower than 'TradeSideId is not null')

order by md.IntegratorReceivedTimeUtc

END
GO
GRANT EXECUTE ON [dbo].[GetHtaDealsData_SP] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetIfNewerDataExistsForSymbol_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetIfNewerDataExistsForSymbol_SP]
@DateTime2Value DateTime2,
@SymbolNoSlash char(6)
AS
BEGIN
	SET NOCOUNT ON;


if exists
(select top 1 * from MarketData md with(nolock)
where md.FXPairId=(select FxPairId from FxPair where FxpCode=@SymbolNoSlash)
and md.IntegratorReceivedTimeUtc > @DateTime2Value)

return 1

else 

return 0

END

GO
/****** Object:  StoredProcedure [dbo].[GetKgtSoftPrices_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetKgtSoftPrices_SP] 
	@FxPairNoSlash char(6),
	@StartDateTimeUtc DateTime2,
	@EndDateTimeUtc DateTime2
AS
BEGIN
	SET NOCOUNT ON;

SELECT 

	--dbo.ConvertDateTime2ToTimeOfDayTicks(p.IntegratorSentTimeUtc)/10000.0'todMilliseconds',

	s.PriceSideName'PriceSide', -- NOT NULL
	c.LiqProviderStreamName'Counterparty', -- NOT NULL
	--p.RecordID, -- NOT NULL
	p.Price, -- NULL
	p.SizeBaseAbs, -- NULL
	p.TradingSystemId, -- NULL
	p.LayerOrderId, -- NOT NULL
	p.IntegratorSentTimeUtc, -- NOT NULL
	p.IntegratorPriceIdentity -- NULL

FROM

	FXtickDB.dbo.OutgoingSoftPrice p with(nolock) -- index: IX_PairCounterpartyTime
	join FXtickDB.dbo.PriceSide s with(nolock) on s.PriceSideId=p.SideId
	join FXtickDB.dbo.Counterparty c with(nolock) on c.LiqProviderStreamId=p.CounterpartyId

WHERE 

	p.FXPairId=
			(select f.FxPairId 
			from FXtickDB.dbo.FxPair f with(nolock) 
			where f.FxpCode=@FxPairNoSlash)

	and p.IntegratorSentTimeUtc between @StartDateTimeUtc and @EndDateTimeUtc

ORDER BY

	IntegratorSentTimeUtc

END

GO
GRANT EXECUTE ON [dbo].[GetKgtSoftPrices_SP] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetLastBankPricesForPair_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetLastBankPricesForPair_SP]
	@TopCount INT,
	@SymbolName CHAR(6)
AS
BEGIN
	DECLARE @SymbolId INT

	SELECT @SymbolId = [FxPairId] FROM [dbo].[FxPair] WHERE [FxpCode] = @SymbolName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @SymbolName) WITH SETERROR
	END

	SELECT TOP (@TopCount)
		[Price],
		IntegratorReceivedTimeUtc
	FROM 
		[dbo].[MarketData] WITH(NOLOCK)
	WHERE 
		[FXPairId] = @SymbolId
		AND [RecordTypeId] = 0
	ORDER BY 
		[IntegratorReceivedTimeUtc] DESC
END

GO
GRANT EXECUTE ON [dbo].[GetLastBankPricesForPair_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetLastBid]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLastBid]
@FxPairId tinyint,
@DataProviderId tinyint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select top 1 Bid
from Tick with (NOLOCK)
where DataProviderId = @DataProviderId and FxPairId = @FxPairId
and TickTime = (select max(TickTime) from Tick where DataProviderId = @DataProviderId and FxPairId = @FxPairId)

END

GO
/****** Object:  StoredProcedure [dbo].[GetLastLmaxPricesForPair_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetLastLmaxPricesForPair_SP]
	@TopCount INT,
	@SymbolName CHAR(6)
AS
BEGIN
	DECLARE @SymbolId INT

	SELECT @SymbolId = [FxPairId] FROM [dbo].[FxPair] WHERE [FxpCode] = @SymbolName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @SymbolName) WITH SETERROR
	END

	SELECT TOP (@TopCount)
		[Price],
		side.PriceSideName AS Side,
		[IntegratorReceivedTimeUtc]
	FROM 
		[dbo].[MarketData] md WITH(NOLOCK)
		INNER JOIN [dbo].[LiqProviderStream] lps WITH(NOLOCK) ON md.CounterpartyId = lps.LiqProviderStreamId
		INNER JOIN [dbo].LiquidityProvider lp WITH(NOLOCK) ON lps.LiqProviderId = lp.LiqProviderId
		INNER JOIN [dbo].[PriceSide] side WITH(NOLOCK) ON md.SideId = side.PriceSideId
	WHERE 
		[FXPairId] = @SymbolId
		AND lp.LiqProviderCode = 'LMX'
		AND md.RecordTypeId = 3
	ORDER BY 
		[IntegratorReceivedTimeUtc] DESC
END

GO
GRANT EXECUTE ON [dbo].[GetLastLmaxPricesForPair_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetLiqProviderStreamId]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLiqProviderStreamId] 
@LiqProviderStreamName nvarchar(50),
@LiqProviderStreamId tinyint output
AS
BEGIN
select @LiqProviderStreamId = (select LiqProviderStreamId from LiqProviderStream where LiqProviderStreamName = @LiqProviderStreamName)
END

GO
/****** Object:  StoredProcedure [dbo].[GetLiqProviderStreamsInfo]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLiqProviderStreamsInfo] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select LiqProviderStreamName, LiqProviderStreamId
from LiqProviderStream

END

GO
GRANT EXECUTE ON [dbo].[GetLiqProviderStreamsInfo] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetLiqProviderStreamsInfo] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetLmaxCrossStats_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLmaxCrossStats_SP]
@StartTickTimeEt numeric(17,0),
@EndTickTimeEt numeric(17,0),
@MinimumDiscountBp numeric(18,9)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @SymbolsToShow TABLE (FxpCode char(6));

INSERT INTO @SymbolsToShow VALUES ('AUDCAD')
INSERT INTO @SymbolsToShow VALUES ('AUDCHF')
INSERT INTO @SymbolsToShow VALUES ('AUDJPY')
INSERT INTO @SymbolsToShow VALUES ('AUDNZD')
INSERT INTO @SymbolsToShow VALUES ('AUDUSD')
INSERT INTO @SymbolsToShow VALUES ('CADJPY')
INSERT INTO @SymbolsToShow VALUES ('CHFJPY')
INSERT INTO @SymbolsToShow VALUES ('EURAUD')
INSERT INTO @SymbolsToShow VALUES ('EURCAD')
INSERT INTO @SymbolsToShow VALUES ('EURCHF')
INSERT INTO @SymbolsToShow VALUES ('EURGBP')
INSERT INTO @SymbolsToShow VALUES ('EURHUF')
INSERT INTO @SymbolsToShow VALUES ('EURJPY')
INSERT INTO @SymbolsToShow VALUES ('EURNOK')
INSERT INTO @SymbolsToShow VALUES ('EURNZD')
INSERT INTO @SymbolsToShow VALUES ('EURPLN')
INSERT INTO @SymbolsToShow VALUES ('EURSEK')
--INSERT INTO @SymbolsToShow VALUES ('EURTRY')
INSERT INTO @SymbolsToShow VALUES ('EURUSD')
INSERT INTO @SymbolsToShow VALUES ('GBPAUD')
INSERT INTO @SymbolsToShow VALUES ('GBPCAD')
INSERT INTO @SymbolsToShow VALUES ('GBPJPY')
INSERT INTO @SymbolsToShow VALUES ('GBPUSD')
INSERT INTO @SymbolsToShow VALUES ('NOKSEK')
INSERT INTO @SymbolsToShow VALUES ('NZDJPY')
INSERT INTO @SymbolsToShow VALUES ('NZDUSD')
INSERT INTO @SymbolsToShow VALUES ('USDCAD')
INSERT INTO @SymbolsToShow VALUES ('USDJPY')
INSERT INTO @SymbolsToShow VALUES ('USDMXN')
INSERT INTO @SymbolsToShow VALUES ('USDNOK')
INSERT INTO @SymbolsToShow VALUES ('USDSEK')
INSERT INTO @SymbolsToShow VALUES ('USDSGD')
INSERT INTO @SymbolsToShow VALUES ('USDTRY')
INSERT INTO @SymbolsToShow VALUES ('USDZAR')
INSERT INTO @SymbolsToShow VALUES ('USDCHF')


select f2.FxpCode'symbol',coalesce(round(res.discountBpSumNet,4),0)'discountBpSumNet' from FxPair f2
left join 

(select f.FxpCode,r.discountBpSumNet from

(select FXPairId,sum(((Bid-Ask)/Bid*1000000-11.5)/100)'discountBpSumNet' 
from Tick t with(nolock)
where t.DataProviderID =22
and t.FXPairID in (select FxPairId from FxPair)
and t.FXPairID in (select FXPairID from FxPair where Fxpair.FxpCode in (select FxpCode FROM @SymbolsToShow))
and (TickTime between @StartTickTimeEt and @EndTickTimeEt)
and t.Bid>t.Ask and ((Bid-Ask)/Bid*10000)>=@MinimumDiscountBp
and ((BidStreamProviderId=80 AND OfferStreamProviderId<>80 AND OfferTimeStamp>BidTimeStamp) OR (BidStreamProviderId<>80 AND OfferStreamProviderId=80 AND BidTimeStamp>OfferTimeStamp))
--((BidStreamProviderId=80 AND OfferStreamProviderId<>80 AND BidTimeStamp>OfferTimeStamp) OR (BidStreamProviderId<>80 AND OfferStreamProviderId=80 AND OfferTimeStamp>BidTimeStamp))
group by FxPairId) r

join FxPair f on r.FXPairID=f.FxPairId) res

on f2.fxpcode=res.FxpCode
where 
f2.FxPairId in 
	(SELECT SymbolId FROM CounterpartySymbolSettings css where css.TradingTargetTypeId =
		(select id from TradingTargetType ttt where ttt.Name='LMAX')
	and css.Supported=1) 

and f2.FxpCode in (select FxpCode FROM @SymbolsToShow)
order by f2.FxpCode
--order by discountBpSumNet desc

--option(recompile)
END
GO
GRANT EXECUTE ON [dbo].[GetLmaxCrossStats_SP] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetRaboPairsLastBids]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetRaboPairsLastBids]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @tempTable table
(
	FxPair char(6),
	Bid real
)

declare @minFxPairId int
declare @maxFxPairId int
declare @fxPairId int
declare @fxPair char(6)

declare @bid real

set @minFxPairId = (select min(FxPairId) from FxPair where IsRaboTradable = 1)
set @maxFxPairId = (select max(FxPairId) from FxPair where IsRaboTradable = 1)

set @fxPairId = @minFxPairId

while @fxPairId <= @maxFxPairId
begin
	
	if @fxPairId in (select FxPairId from FxPair where IsRaboTradable = 1)
	begin
		set @fxPair = (select FxpCode from FxPair where FxPairId = @fxPairId)

		----------------------------------------------------
		set @bid = (select top 1 Bid
			from Tick with (NOLOCK)
			where DataProviderId = 22 and FxPairId = @fxPairId
			and TickTime = (select max(TickTime)
				from Tick with (NOLOCK) where DataProviderId = 22 and FxPairId = @fxPairId))
	
		insert into @tempTable (FxPair, Bid) values (@fxPair, @bid)
		----------------------------------------------------
	end

	set @fxPairId = @fxPairId + 1
end

select * from @tempTable

END

GO
/****** Object:  StoredProcedure [dbo].[GetSpreadStats_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSpreadStats_SP]
	@FxPairNoSlash char(6),
	@StartDateTimeUtc DateTime2,
	@EndDateTimeUtc DateTime2,
	@CounterpartyCode char(3)
AS

BEGIN
SET NOCOUNT ON;

declare @StartTimeUtcInternal datetime2 = @StartDateTimeUtc;
declare @EndTimeUtcInternal datetime2 = @EndDateTimeUtc;
declare @FxPairIdInternal tinyint = (select f.FxPairId from FxPair f with(nolock) where f.FxpCode=@FxPairNoSlash);
declare @CounterpartyIdInternal tinyint = (select l.LiqProviderStreamId from LiqProviderStream l with(nolock) where l.LiqProviderStreamName=@CounterpartyCode)
----------------------------------------------------------------------------

declare @BidsSentTimes table(BidSentTimeUtc datetime2);
insert into @BidsSentTimes
select CounterpartySentTimeUtc from MarketData md with(nolock)
where md.FXPairId = @FxPairIdInternal
and md.IntegratorReceivedTimeUtc between @StartTimeUtcInternal and @EndTimeUtcInternal
and CounterpartyId=@CounterpartyIdInternal
and RecordTypeId=3
and SideId=0

--select * from @BidsSentTimes --------

declare @BidsSentTimeCounts table (BidSentTimeUtc datetime2, bidsSentAtThisTimeCount int);
insert into @BidsSentTimeCounts
select BidSentTimeUtc, count(*)'count' from @BidsSentTimes group by BidSentTimeUtc

--select * from @BidsSentTimeCounts --------

declare @AllBidsTable table(BidPrice decimal(38,10), BidSentTimeUtc datetime2);
insert into @AllBidsTable select 
Price,CounterpartySentTimeUtc
from MarketData md with(nolock)
where md.FXPairId = @FxPairIdInternal
and md.IntegratorReceivedTimeUtc between @StartTimeUtcInternal and @EndTimeUtcInternal
and CounterpartyId=@CounterpartyIdInternal
and RecordTypeId=3
and SideId=0

--select * from @AllBidsTable --------

declare @BidsWithUniqueSentTimeTable table (BidPrice decimal(38,10), BidSentTimeUtc datetime2);
insert into @BidsWithUniqueSentTimeTable
select b.BidPrice,b.BidSentTimeUtc from @AllBidsTable b join @BidsSentTimeCounts bc on b.BidSentTimeUtc=bc.BidSentTimeUtc where bidsSentAtThisTimeCount=1

--select * from @BidsWithUniqueSentTimeTable --------

-----------------------------------------------------------------

declare @AsksSentTimes table(AskSentTimeUtc datetime2);
insert into @AsksSentTimes
select CounterpartySentTimeUtc from MarketData md with(nolock)
where md.FXPairId = @FxPairIdInternal
and md.IntegratorReceivedTimeUtc between @StartTimeUtcInternal and @EndTimeUtcInternal
and CounterpartyId=@CounterpartyIdInternal
and RecordTypeId=3
and SideId=1

--select * from @AsksSentTimes --------

declare @AsksSentTimeCounts table (AskSentTimeUtc datetime2, AsksSentAtThisTimeCount int);
insert into @AsksSentTimeCounts
select AskSentTimeUtc, count(*)'count' from @AsksSentTimes group by AskSentTimeUtc

--select * from @AsksSentTimeCounts --------

declare @AllAsksTable table(AskPrice decimal(38,10), AskSentTimeUtc datetime2);
insert into @AllAsksTable select 
Price,CounterpartySentTimeUtc
from MarketData md with(nolock)
where md.FXPairId = @FxPairIdInternal
and md.IntegratorReceivedTimeUtc between @StartTimeUtcInternal and @EndTimeUtcInternal
and CounterpartyId=@CounterpartyIdInternal
and RecordTypeId=3
and SideId=1

--select * from @AllAsksTable --------

declare @AsksWithUniqueSentTimeTable table (AskPrice decimal(38,10), AskSentTimeUtc datetime2);
insert into @AsksWithUniqueSentTimeTable
select b.AskPrice,b.AskSentTimeUtc from @AllAsksTable b join @AsksSentTimeCounts bc on b.AskSentTimeUtc=bc.AskSentTimeUtc where AsksSentAtThisTimeCount=1

--select * from @AsksWithUniqueSentTimeTable --------

-------------------------------------------------------------

--select bq.BidSentTimeUtc'SentTimeUtc',bq.BidPrice,aq.AskPrice from @BidsWithUniqueSentTimeTable bq join @AsksWithUniqueSentTimeTable aq on bq.BidSentTimeUtc=aq.AskSentTimeUtc order by SentTimeUtc asc

declare @UnsortedSpreadsTable table (SpreadBp decimal(38,10));
insert into @UnsortedSpreadsTable
select 
(aq.AskPrice-bq.BidPrice)/bq.BidPrice*10000
from @BidsWithUniqueSentTimeTable bq join @AsksWithUniqueSentTimeTable aq on bq.BidSentTimeUtc=aq.AskSentTimeUtc

--select * from @UnsortedSpreadsTable

select
@FxPairNoSlash,
min(us.SpreadBp)'minSpreadBp',
avg(us.SpreadBp)'avgSpreadBp',
max(us.SpreadBp)'maxSpreadBp',
stdevp(us.SpreadBp)'stdevpSpreadBp',
count(*)'quotesCount'

from @UnsortedSpreadsTable us

END
GO
/****** Object:  StoredProcedure [dbo].[GetSpreadStatsAggregate_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSpreadStatsAggregate_SP]
	@StartDateTimeUtc DateTime2,
	@EndDateTimeUtc DateTime2,
	@CounterpartyCode char(3)
AS

BEGIN
SET NOCOUNT ON;

declare @StartDateTimeUtcInternal datetime2 = @StartDateTimeUtc;
declare @EndDateTimeUtcInternal datetime2 = @EndDateTimeUtc;
declare @CounterpartyCodeInternal char(3) = @CounterpartyCode;

------------------------

declare @SpreadStatsTable table (Symbol char(6), MinSpreadBp decimal(38,10), AvgSpreadBp decimal(38,10), MaxSpreadBp decimal(38,10), StdevpSpreadBp decimal(38,10), QuotesCount int);

declare @FxPairIdCurrent tinyint = 0;
declare @FxPairIdMax tinyint = (select max(f.FxPairId) from FxPair f with(nolock));

while @FxPairIdCurrent <= @FxPairIdMax

BEGIN

	IF (@FxPairIdCurrent in (select f.FxPairId from FxPair f with(nolock)))
		BEGIN

		declare @FxPairNoSlash char(6) = (select f.FxpCode from FxPair f with(nolock) where f.FxPairId=@FxPairIdCurrent);

		insert into @SpreadStatsTable

		EXEC	[dbo].[GetSpreadStats_SP]
				@FxPairNoSlash = @FxPairNoSlash,
				@StartDateTimeUtc = @StartDateTimeUtcInternal,
				@EndDateTimeUtc = @EndDateTimeUtcInternal,
				@CounterpartyCode = @CounterpartyCodeInternal;

		END

	set @FxPairIdCurrent = @FxPairIdCurrent + 1;

END

select * from @SpreadStatsTable sst
order by sst.Symbol

END
GO
/****** Object:  StoredProcedure [dbo].[GetSupportedCurrencies_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSupportedCurrencies_SP] 
AS
BEGIN
	SELECT [CurrencyAlphaCode] FROM [dbo].[Currency] WHERE [IsCitibankPbSupported] = 1
END

GO
GRANT EXECUTE ON [dbo].[GetSupportedCurrencies_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetSupportedCurrencies_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetSupportedSymbolsForCounterparty_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSupportedSymbolsForCounterparty_SP] 
(
	@TradingTargetCode [VARCHAR](16)
)
AS
BEGIN

	SELECT 
		pair.FxpCode AS Symbol
	FROM 
		[dbo].[CounterpartySymbolSettings] settings
		INNER JOIN [dbo].[FxPair] pair ON pair.FxPairId = settings.[SymbolId]
		INNER JOIN [dbo].[TradingTargetType] targetType ON targetType.Id = settings.[TradingTargetTypeId]
	WHERE
		targetType.Code = @TradingTargetCode
		AND settings.[Supported] = 1
	ORDER BY
		pair.FxpCode ASC
END

GO
GRANT EXECUTE ON [dbo].[GetSupportedSymbolsForCounterparty_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetTicksAndEocdByProviderPair]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetTicksAndEocdByProviderPair]
@DataProviderAlphaCode CHAR(3),
@FxPairAlpha6Code CHAR(6),
@StartTickTime numeric(17,0),
@EndTickTime numeric(17,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @DataProviderId tinyint
	DECLARE @FxPairId tinyint
	
	SELECT @DataProviderId = DataProviderId
	FROM DataProvider
	WHERE DprCode = @DataProviderAlphaCode

	SELECT @FxPairId = FxPairId
	FROM FxPair
	WHERE FxpCode = @FxPairAlpha6Code

    select Bid, Ask, TickTime, EndOfContinuousData
	from Tick with (NOLOCK)
	where
		DataProviderId = @DataProviderId
		and FxPairId = @FxPairId
		and (TickTime between @StartTickTime and @EndTickTime)
		and (BadTick is null or BadTick = 0)
		--and BidSize >= 1000000 and AskSize >= 1000000
	order by TickTime asc

option(recompile)
END
GO
GRANT EXECUTE ON [dbo].[GetTicksAndEocdByProviderPair] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetTicksByProviderAndPair]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetTicksByProviderAndPair]
@DataProviderId tinyint,
@FxPairId tinyint,
@StartTickTime numeric(17,0),
@EndTickTime numeric(17,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select Bid, Ask, TickTime
	from Tick with (NOLOCK)
	where
		DataProviderId = @DataProviderId
		and FxPairId = @FxPairId
		and (TickTime between @StartTickTime and @EndTickTime)
		and (BadTick is null or BadTick = 0)
		--and BidSize >= 1000000 and AskSize >= 1000000
	order by TickTime asc

option(recompile)
END
GO
GRANT EXECUTE ON [dbo].[GetTicksByProviderAndPair] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetTicksByProviderAndPairBySize]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetTicksByProviderAndPairBySize]
@DataProviderId tinyint,
@FxPairId tinyint,
@StartTickTime numeric(17,0),
@EndTickTime numeric(17,0),
@MinimumSize numeric(17,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select Bid, Ask, TickTime --Bid, Ask, TickTime!
	from Tick with (NOLOCK)
	where
		DataProviderId = @DataProviderId
		and FxPairId = @FxPairId
		and (TickTime between @StartTickTime and @EndTickTime)
		and (BadTick is null or BadTick = 0)
		and BidSize >= @MinimumSize and AskSize >= @MinimumSize
	order by TickTime asc

option(recompile)
END
GO
/****** Object:  StoredProcedure [dbo].[GetTicksCountByProviderPair]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetTicksCountByProviderPair]
@DataProviderAlphaCode CHAR(3),
@FxPairAlpha6Code CHAR(6),
@StartTickTime numeric(17,0),
@EndTickTime numeric(17,0),
@TickCount numeric(17,0) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @DataProviderId tinyint
	DECLARE @FxPairId tinyint
	
	SELECT @DataProviderId = DataProviderId
	FROM DataProvider
	WHERE DprCode = @DataProviderAlphaCode

	SELECT @FxPairId = FxPairId
	FROM FxPair
	WHERE FxpCode = @FxPairAlpha6Code

	select @TickCount =
    (select count(*)
	from Tick with (NOLOCK)
	where
		DataProviderId = @DataProviderId
		and FxPairId = @FxPairId
		and (TickTime between @StartTickTime and @EndTickTime)
		and (BadTick is null or BadTick = 0))

END
GO
/****** Object:  StoredProcedure [dbo].[GetTicksForDuplicateCalc]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetTicksForDuplicateCalc] 
AS
BEGIN
	SET NOCOUNT ON;

select t.[DataProviderID], t.[FXPairID], t.[TickTime], t.[SourceFileID], t.[TickID]

from
Tick t with (NOLOCK)
join

(select DataProviderID, FXPairID, min(TickTime) 'minTickTime', max(TickTime) 'maxTickTime'
from Tick with (NOLOCK)
where UniqueTick is null
group by DataProviderID, FXPairID) u

on t.DataProviderID = u. DataProviderID and t.FXPairID = u.FXPairID and t.TickTime between u.minTickTime and u.maxTickTime

order by t.[DataProviderID], t.[FXPairID], t.[TickTime], t.[SourceFileID], t.[TickID]

END








GO
/****** Object:  StoredProcedure [dbo].[GetTicksForDuplicateCalcCount]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTicksForDuplicateCalcCount] 
AS
BEGIN
	SET NOCOUNT ON;


select count(*)

from
Tick t with (NOLOCK)
join

(select DataProviderID, FXPairID, min(TickTime) 'minTickTime', max(TickTime) 'maxTickTime'
from Tick with (NOLOCK)
where UniqueTick is null
group by DataProviderID, FXPairID) u

on t.DataProviderID = u. DataProviderID and t.FXPairID = u.FXPairID and t.TickTime between u.minTickTime and u.maxTickTime



END




















GO
/****** Object:  StoredProcedure [dbo].[InsertFixQuote]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertFixQuote]
@DataProviderId tinyint,
@FxPairId tinyint,
@TickTime numeric(17,0),
@BidPrice real,
@AskPrice real,
@BidSize int,
@AskSize int,
@SourceFileId int,
@TradeSide bit

AS
BEGIN
SET NOCOUNT ON;

insert into Tick (
DataProviderId,
FxPairId ,
TickTime,
Bid,
Ask,
BidSize,
AskSize,
SourceFileId,
TradeSide)

values (
@DataProviderId,
@FxPairId,
@TickTime,
@BidPrice,
@AskPrice,
@BidSize,
@AskSize,
@SourceFileId,
@TradeSide)

END
GO
GRANT EXECUTE ON [dbo].[InsertFixQuote] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertQuote]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertQuote]
@DataProviderId tinyint,
@FxPairId tinyint,
@TickTime numeric(17,0),
@BidPrice real,
@AskPrice real,
@BidSize int,
@AskSize int,
@SourceFileId int,
@BidStreamProviderId tinyint,
@AskStreamProviderId tinyint,
@BidTimeStamp numeric(17,0),
@AskTimeStamp numeric(17,0)
AS
BEGIN
SET NOCOUNT ON;

insert into Tick (
DataProviderId,
FxPairId ,
TickTime,
Bid,
Ask,
BidSize,
AskSize,
SourceFileId,
BidStreamProviderId,
OfferStreamProviderId,
BidTimeStamp,
OfferTimeStamp)

values (
@DataProviderId,
@FxPairId,
@TickTime,
@BidPrice,
@AskPrice,
@BidSize,
@AskSize,
@SourceFileId,
@BidStreamProviderId,
@AskStreamProviderId,
@BidTimeStamp,
@AskTimeStamp)

END

GO
GRANT EXECUTE ON [dbo].[InsertQuote] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertSingleTradeDecayAnalysisRequest_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertSingleTradeDecayAnalysisRequest_SP]
(
	@TradeDecayAnalysisTypeName [varchar](16),
	@SourceEventGlobalRecordId [int],
	@AnalysisStartTimeUtc [datetime2](7),
	@IntegratorSymbolId [tinyint],
	@IntegratorCounterpartyId [tinyint],
	@ReferencePrice [decimal](18,9),
	@MatchingContraPriceSideId [bit]
)
AS
BEGIN
	INSERT INTO [dbo].[TradeDecayAnalysis_Pending]
           ([TradeDecayAnalysisTypeId]
           ,[SourceEventGlobalRecordId]
           ,[AnalysisStartTimeUtc]
           ,[FXPairId]
           ,[CounterpartyId]
           ,[ReferencePrice]
           ,[PriceSideId])
     VALUES
           ((SELECT [TradeDecayAnalysisTypeId] FROM [dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = @TradeDecayAnalysisTypeName)
           ,@SourceEventGlobalRecordId
           ,@AnalysisStartTimeUtc
           ,(SELECT [DCDBSymbolId] FROM [dbo].[_Internal_SymbolIdsMapping] WHERE [IntegratorDBSymbolId] = @IntegratorSymbolId)
           ,(SELECT [DCDBCounterpartyId] FROM [dbo].[_Internal_CounterpartyIdsMapping] WHERE [IntegratorDBCounterpartyId] = @IntegratorCounterpartyId)
           ,@ReferencePrice
           ,@MatchingContraPriceSideId)
END

GO
/****** Object:  StoredProcedure [dbo].[MarkUniqueAndDuplicateTicks]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MarkUniqueAndDuplicateTicks] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- mark unique ticks:
update Tick set UniqueTick = 1

from Tick t2 join

		(select t.DataProviderID 'FirstFileDataProviderID', t.FXPairID 'FirstFileFXPairID', t.TickTime 'FirstFileTickTime', min(SourceFileID) 'FirstSourceFile'

			from Tick t join

			(select DataProviderID 'MinMaxDataProviderID', FXPairID 'MinMaxFXPair', min(TickTime) 'MinTickTime', max(TickTime) 'MaxTickTime'
			from Tick
			where UniqueTick is null
			group by DataProviderID, FXPairID) MinMaxTimeWhereUniqueUknown

		on t.DataProviderID = MinMaxTimeWhereUniqueUknown.MinMaxDataProviderID
			and t.FXPairID = MinMaxTimeWhereUniqueUknown.MinMaxFXPair
			and t.TickTime between
				MinMaxTimeWhereUniqueUknown.MinTickTime
				and MinMaxTimeWhereUniqueUknown.MaxTickTime

		group by t.DataProviderID, t.FXPairID, t.TickTime) FirstSourceFileTable

	on t2.DataProviderID = FirstSourceFileTable.FirstFileDataProviderID
		and t2.FXPairID = FirstSourceFileTable.FirstFileFXPairID
		and t2.TickTime = FirstSourceFileTable.FirstFileTickTime
		and t2.SourceFileID = FirstSourceFileTable.FirstSourceFile
;

-- mark duplicate ticks:
update Tick set UniqueTick = 0

from Tick t2 join

		(select t.DataProviderID 'FirstFileDataProviderID', t.FXPairID 'FirstFileFXPairID', t.TickTime 'FirstFileTickTime', min(SourceFileID) 'FirstSourceFile'

			from Tick t join

			(select DataProviderID 'MinMaxDataProviderID', FXPairID 'MinMaxFXPair', min(TickTime) 'MinTickTime', max(TickTime) 'MaxTickTime'
			from Tick
			where UniqueTick is null
			group by DataProviderID, FXPairID) MinMaxTimeWhereUniqueUknown

		on t.DataProviderID = MinMaxTimeWhereUniqueUknown.MinMaxDataProviderID
			and t.FXPairID = MinMaxTimeWhereUniqueUknown.MinMaxFXPair
			and t.TickTime between
				MinMaxTimeWhereUniqueUknown.MinTickTime
				and MinMaxTimeWhereUniqueUknown.MaxTickTime

		group by t.DataProviderID, t.FXPairID, t.TickTime) FirstSourceFileTable

	on t2.DataProviderID = FirstSourceFileTable.FirstFileDataProviderID
		and t2.FXPairID = FirstSourceFileTable.FirstFileFXPairID
		and t2.TickTime = FirstSourceFileTable.FirstFileTickTime
		and t2.SourceFileID != FirstSourceFileTable.FirstSourceFile
;


END




GO
/****** Object:  StoredProcedure [dbo].[PerformSingleTradeDecayAnalysis_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PerformSingleTradeDecayAnalysis_SP]
(
	@TradeDecayAnalysisTypeId [tinyint],
	@SourceEventGlobalRecordId [int],
	@AnalysisStartTimeUtc [datetime2](7),
	@FXPairId [tinyint],
	@CounterpartyId [tinyint],
	@ReferencePrice [decimal](18,9),
	@PriceSideId [bit]
)
AS
BEGIN

	--@PriceSideId  meaning
	-- 0 is our sell - so our Ask (or sell on cpt Bid)
	-- 1 is our buy - so our Bid (or buy on cpt Ask)

	DECLARE @TradeDecayAnalysisId INT

	INSERT INTO [dbo].[TradeDecayAnalysis]
           ([TradeDecayAnalysisTypeId]
           ,[SourceEventGlobalRecordId]
           ,[AnalysisStartTimeUtc]
           ,[FXPairId]
           ,[CounterpartyId]
           ,[PriceSideId]
           ,[HasValidTickAfterIntervalEnd]
           ,[IsValidAndComplete])
     VALUES
           (@TradeDecayAnalysisTypeId,
			@SourceEventGlobalRecordId,
			@AnalysisStartTimeUtc,
			@FXPairId,
			@CounterpartyId,
			@PriceSideId,
			0,
			0)
	
	SET @TradeDecayAnalysisId = scope_identity();
	DECLARE @ReferenceCounterpartyId INT = (SELECT [LiqProviderStreamId] FROM [LiqProviderStream] WHERE [LiqProviderStreamName] = 'L01')
	-- round to next whole us and and 1ms for the first interval
	DECLARE @AnalysisFirstIntervalStartTimeUtc [datetime2](7) = DATEADD(ns, -DATEPART(ns, @AnalysisStartTimeUtc)%1000 + 1000 + 1000000, @AnalysisStartTimeUtc)
	DECLARE @AnalysisEndTimeUtc [datetime2](7) = DATEADD(minute, 5, @AnalysisStartTimeUtc)
	
	
	DECLARE @FirstBidTob [decimal](18,9)
	DECLARE @FirstAskTob [decimal](18,9)
	
	SET @FirstBidTob =
	(SELECT TOP 1
		md.Price AS LastToBPrice
	FROM
		[dbo].[MarketData] md with (nolock) 
	WHERE
		IntegratorReceivedTimeUtc BETWEEN DATEADD(second, -180, @AnalysisFirstIntervalStartTimeUtc) AND @AnalysisFirstIntervalStartTimeUtc
		AND fxpairid  = @FxPairId
		AND RecordTypeId IN (0, 3)
		AND CounterpartyId = @ReferenceCounterpartyId
		AND SideId = 0
	ORDER BY IntegratorReceivedTimeUtc DESC)
	
	SET @FirstAskTob =
	(SELECT TOP 1
		md.Price AS LastToBPrice
	FROM
		[dbo].[MarketData] md with (nolock) 
	WHERE
		IntegratorReceivedTimeUtc BETWEEN DATEADD(second, -180, @AnalysisFirstIntervalStartTimeUtc) AND @AnalysisFirstIntervalStartTimeUtc
		AND fxpairid  = @FxPairId
		AND RecordTypeId IN (0, 3)
		AND CounterpartyId = @ReferenceCounterpartyId
		AND SideId = 1
	ORDER BY IntegratorReceivedTimeUtc DESC)
	
	--fast track if there is no valid price before the interval
	IF @FirstBidTob IS NULL OR @FirstAskTob IS NULL
	BEGIN
		RETURN;
	END
	
	IF NOT EXISTS(
		SELECT 1 
		FROM 
			[dbo].[MarketData] md with (nolock) 
		WHERE
			IntegratorReceivedTimeUtc BETWEEN @AnalysisEndTimeUtc AND DATEADD(second, 180, @AnalysisEndTimeUtc)
			AND fxpairid  = @FxPairId
			AND RecordTypeId IN (0, 3)
			AND CounterpartyId = @ReferenceCounterpartyId
			AND SideId = @PriceSideId
		)
	BEGIN
		RETURN;
	END
	
	
	INSERT INTO [dbo].[TradeDecayAnalysisDataPoint]
           ([TradeDecayAnalysisId]
           ,[MillisecondDistanceFromAnalysisStart]
           ,[ReferenceMidPrice]
		   ,[ReferenceBidPrice]
		   ,[IsReferenceBidAuthenticValue]
		   ,[ReferenceAskPrice]
		   ,[IsReferenceAskAuthenticValue]
           ,[BpMtmToReferenceMidPrice]
		   ,[BpMtmToReferenceCrossPrice]
		   ,[BpMtmToReferenceJoinPrice]
		   )
	SELECT	
		@TradeDecayAnalysisId,
		MillisecondDistance,
		-- as we do not have ANY()
		AVG(LastSidedToB) AS MidToB,
		-- as we do not have ANY() - all 4 followings
		MAX(LastBid) AS Bid,
		MAX(IsLastBidAuthentic) AS IsBidAuthentic,
		MAX(LastAsk) AS Ask,
		MAX(IsLastAskAuthentic) AS IsAskAuthentic,
		CASE WHEN @PriceSideId = 0 THEN (@ReferencePrice - AVG(LastSidedToB)) ELSE (AVG(LastSidedToB) - @ReferencePrice) END / @ReferencePrice * 10000.0 AS MtMFromMid,
		CASE WHEN @PriceSideId = 0 THEN (@ReferencePrice - MAX(LastAsk)) ELSE (MAX(LastBid) - @ReferencePrice) END / @ReferencePrice * 10000.0 AS MtMFromCrossSpread,
		CASE WHEN @PriceSideId = 0 THEN (@ReferencePrice - MAX(LastBid)) ELSE (MAX(LastAsk) - @ReferencePrice) END / @ReferencePrice * 10000.0 AS MtMFromSameSide
	FROM	
		(

		SELECT
			MillisecondDistance,
			SideId,
			LastSidedToB,
			CASE WHEN SideId = 0 THEN LastSidedToB ELSE 0 END AS LastBid,
			CASE WHEN SideId = 0 THEN IsAuthenticValue ELSE 0 END AS IsLastBidAuthentic,
			CASE WHEN SideId = 1 THEN LastSidedToB ELSE 0 END AS LastAsk,
			CASE WHEN SideId = 1 THEN IsAuthenticValue ELSE 0 END AS IsLastAskAuthentic
		FROM
		(
			SELECT
				MillisecondDistance,
				SideId,
				--for lack of ANY()
				MAX(LastToBPrice) OVER (PARTITION BY NonNullToBsNum) AS LastSidedToB,
				CASE WHEN LastToBPrice IS NULL THEN 0 ELSE 1 END AS IsAuthenticValue
			FROM
				(
				SELECT
					nums.MsDistanceFromEventStart AS MillisecondDistance,
					nums.SideId AS SideId,
					LastToBPrice, -- as we do not have any - all are same
					-- this will count number of non-null rows - for nulls (no ToB in current millisecond) it will repeat previous val (so we can partition by this one level up)
					COUNT(LastToBPrice) OVER (ORDER BY nums.SideId, nums.MsDistanceFromEventStart ASC) as NonNullToBsNum
				FROM
				[dbo].[_Internal_TradeDecayIntervals] nums
				LEFT JOIN
				(
					SELECT
						0 AS BucketizedMillisecondsFromIntervalStart,
						0 AS SideId,
						@FirstBidTob AS LastToBPrice
					UNION ALL
					SELECT
						0 AS BucketizedMillisecondsFromIntervalStart,
						1 AS SideId,
						@FirstAskTob AS LastToBPrice	
					UNION ALL
					SELECT
						BucketizedMillisecondsFromIntervalStart,
						SideId,
						MAX(LastToBPrice) AS LastToBPrice -- as we do not have ANY() - all are same
					FROM
					(
						SELECT 
							-- millisecond difference from the beginig
							[dbo].[GetTradeDecayTimeIntervalBucket](DateDiff(mcs, @AnalysisStartTimeUtc, md.integratorReceivedTimeUtc)/1000) AS BucketizedMillisecondsFromIntervalStart,
							SideId,
							-- last price in current millisecond interval = ToB in current millisecond
							LAST_VALUE(md.Price) 
								OVER(
									PARTITION BY [dbo].[GetTradeDecayTimeIntervalBucket](DateDiff(mcs, @AnalysisStartTimeUtc, md.integratorReceivedTimeUtc)/1000), SideId
									ORDER BY md.integratorReceivedTimeUtc ASC 
									ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) AS LastToBPrice
						FROM
							[dbo].[MarketData] md with (nolock) 
						WHERE
							IntegratorReceivedTimeUtc BETWEEN @AnalysisFirstIntervalStartTimeUtc AND @AnalysisEndTimeUtc
							AND fxpairid  = @FxPairId
							AND RecordTypeId IN (0, 3)
							AND CounterpartyId = @ReferenceCounterpartyId
							--AND SideId = @PriceSideId
					) tmp
					GROUP BY BucketizedMillisecondsFromIntervalStart, SideId
				) tmp2 ON nums.MsDistanceFromEventStart = tmp2.BucketizedMillisecondsFromIntervalStart AND nums.SideId = tmp2.SideId
				) tmp3
			) tmp4
		)tmp5	
	GROUP BY MillisecondDistance
	ORDER BY MillisecondDistance ASC

	
	UPDATE [dbo].[TradeDecayAnalysis]
	SET 
      [HasValidTickAfterIntervalEnd] = 1
      ,[IsValidAndComplete] = 1
	WHERE 
		[TradeDecayAnalysisId] = @TradeDecayAnalysisId
	
END

GO
/****** Object:  StoredProcedure [dbo].[ProcessTradeDecayAnalysisQueue_SP]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessTradeDecayAnalysisQueue_SP]
(
	@FromTimeUtc [datetime2](7) = '1900-01-01',
	@ToTimeUtc [datetime2](7) = '3000-01-01',
	@BatchSize [int] = 5
)
AS
BEGIN

	DECLARE @TempPendingWork TABLE (
		[PendingTradeDecayAnalysisId] [int] NULL,
		[TradeDecayAnalysisTypeId] [tinyint] NOT NULL,
		[SourceEventGlobalRecordId] [int] NOT NULL,
		[AnalysisStartTimeUtc] [datetime2](7) NOT NULL,
		[FXPairId] [tinyint] NOT NULL,
		[CounterpartyId] [tinyint] NOT NULL,
		[ReferencePrice] [decimal](18,9) NOT NULL,
		[PriceSideId] [bit] NOT NULL
	)

BEGIN TRY
WHILE(1 = 1)
BEGIN
	print 'Starting to process next batch'
	print GETUTCDATE()

	DECLARE @CurrentUTCTime DATETIME2(7)
	SET @CurrentUTCTime = (SELECT GETUTCDATE())
	
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

	BEGIN TRANSACTION

	INSERT INTO @TempPendingWork (
		[PendingTradeDecayAnalysisId],
		[TradeDecayAnalysisTypeId],
		[SourceEventGlobalRecordId],
		[AnalysisStartTimeUtc],
		[FXPairId],
		[CounterpartyId],
		[ReferencePrice],
		[PriceSideId]
		)
	SELECT TOP (@BatchSize) 
		[PendingTradeDecayAnalysisId],
		[TradeDecayAnalysisTypeId],
		[SourceEventGlobalRecordId],
		[AnalysisStartTimeUtc],
		[FXPairId] [tinyint],
		[CounterpartyId],
		[ReferencePrice],
		[PriceSideId]
	FROM [dbo].[TradeDecayAnalysis_Pending] WITH (TABLOCKX)
	WHERE 
		AnalysisStartTimeUtc BETWEEN @FromTimeUtc AND @ToTimeUtc
		AND
			(
			AcquiredForCalculationTimeUtc IS NULL
			OR (@CurrentUTCTime > DATEADD(mi, 120, AcquiredForCalculationTimeUtc))
			)

	UPDATE wq
	SET AcquiredForCalculationTimeUtc = @CurrentUTCTime
	FROM @TempPendingWork TEMP
	INNER JOIN [dbo].[TradeDecayAnalysis_Pending] wq ON TEMP.[PendingTradeDecayAnalysisId] = wq.[PendingTradeDecayAnalysisId]

	COMMIT TRANSACTION
	
	IF(NOT EXISTS(SELECT * FROM @TempPendingWork))
		BREAK;
	
	
	DECLARE @PendingTradeDecayAnalysisId INT,
		@TradeDecayAnalysisTypeId [tinyint],
		@SourceEventGlobalRecordId [int],
		@AnalysisStartTimeUtc [datetime2](7),
		@FXPairId [tinyint],
		@CounterpartyId [tinyint],
		@ReferencePrice [decimal](18,9),
		@PriceSideId [bit]

	DECLARE itemsCursor CURSOR FOR
		SELECT TOP (@BatchSize)
			[PendingTradeDecayAnalysisId],
			[TradeDecayAnalysisTypeId],
			[SourceEventGlobalRecordId],
			[AnalysisStartTimeUtc],
			[FXPairId] [tinyint],
			[CounterpartyId],
			[ReferencePrice],
			[PriceSideId]
		FROM @TempPendingWork 
    
	OPEN itemsCursor
    FETCH NEXT FROM itemsCursor INTO
		@PendingTradeDecayAnalysisId,
		@TradeDecayAnalysisTypeId,
		@SourceEventGlobalRecordId,
		@AnalysisStartTimeUtc,
		@FXPairId,
		@CounterpartyId,
		@ReferencePrice,
		@PriceSideId
    WHILE @@FETCH_STATUS = 0
    BEGIN

		DELETE FROM [dbo].[TradeDecayAnalysis_Pending] WHERE PendingTradeDecayAnalysisId = @PendingTradeDecayAnalysisId
		
		--BEGIN TRY
			EXEC [dbo].[PerformSingleTradeDecayAnalysis_SP]
				@TradeDecayAnalysisTypeId,
				@SourceEventGlobalRecordId,
				@AnalysisStartTimeUtc,
				@FXPairId,
				@CounterpartyId,
				@ReferencePrice,
				@PriceSideId
				
			print 'Processed next analysis request'
			print GETUTCDATE()	
				
		--END TRY
		--BEGIN CATCH
			--CLOSE itemsCursor
			--DEALLOCATE itemsCursor
			--THROW;
		--END CATCH

		FETCH NEXT FROM itemsCursor INTO
			@PendingTradeDecayAnalysisId,
			@TradeDecayAnalysisTypeId,
			@SourceEventGlobalRecordId,
			@AnalysisStartTimeUtc,
			@FXPairId,
			@CounterpartyId,
			@ReferencePrice,
			@PriceSideId
    END
	CLOSE itemsCursor
    DEALLOCATE itemsCursor
	
	DELETE FROM @TempPendingWork
END
END TRY
BEGIN CATCH
	print 'failed to processrequest records';
	THROW;
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[ShowAvgSpreadsEFXandIB]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ShowAvgSpreadsEFXandIB] 
AS
BEGIN

select f.FxpCode, d.DataProviderName, round(sum(Ask-Bid) * f.Multiplier / count(*),3) 'AvgSpreadPips', count(*) 'NewPriceCount'

from Tick t with (NOLOCK) join DataProvider d on t.DataProviderID = d.DataProviderID join FXPair f on t.FXPairID = f.FXPairID

where t.DataProviderID in (12,13,16,18)

group by f.FxpCode, d.DataProviderName, f.Multiplier

order by f.FxpCode, d.DataProviderName, AvgSpreadPips

END

GO
/****** Object:  StoredProcedure [dbo].[ShowFXpairs]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ShowFXpairs] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT f.FXPairID, c1.CurrencyAlphaCode AS Base, c2.CurrencyAlphaCode AS Quote, f.Multiplier, f.ExamplePrice 'Example'
FROM         dbo.FXPair AS f INNER JOIN
                      dbo.Currency AS c1 ON f.BaseCurrencyID = c1.CurrencyID INNER JOIN
                      dbo.Currency AS c2 ON f.QuoteCurrencyID = c2.CurrencyID
ORDER BY c1.CurrencyAlphaCode
END




GO
/****** Object:  StoredProcedure [dbo].[ShowFXPairsProvidersAndTickCount]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ShowFXPairsProvidersAndTickCount] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select c1.CurrencyAlphaCode 'Base', c2.CurrencyAlphaCode 'Quote', d.DataProviderName 'Provider', t.Ticks

from

(select FXPairID, DataProviderID, count(*) 'Ticks'
from Tick with (NOLOCK)
group by FXPairID, DataProviderID) t

join FXPair f on t.FXPairID = f.FXPairID
join Currency c1 on f.BaseCurrencyID = c1.CurrencyID
join Currency c2 on f.QuoteCurrencyID = c2.CurrencyID

join DataProvider d on t.DataProviderID = d.DataProviderID
order by Base, Quote, Provider, Ticks
END

GO
/****** Object:  StoredProcedure [dbo].[ShowProviderPairTagComment]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ShowProviderPairTagComment]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select p.DataProviderName 'Provider', c1.CurrencyAlphaCode 'Base', c2.CurrencyAlphaCode 'Quote', s.FileImportTag 'Tag', s.Comment 'Comment'
from

(select DataProviderID, FXPairID, max(SourceFileID) 'Source' from Tick with (NOLOCK)
group by DataProviderID, FXPairID) d join SourceFile s on d.Source = s.SourceFileID
join DataProvider p on d.DataProviderID = p.DataProviderID
join FXPair f on d.FXPairID = f.FXPairID
join Currency c1 on f.BaseCurrencyID = c1.CurrencyID
join Currency c2 on f.QuoteCurrencyID = c2.CurrencyID

order by Provider, Base, Quote, Tag, Comment
END

GO
/****** Object:  StoredProcedure [dbo].[ShowProvidersFxPairsMinMaxTimeAndTickCount]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ShowProvidersFxPairsMinMaxTimeAndTickCount]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select d.DataProviderName 'Provider', c1.CurrencyAlphaCode 'Base', c2.CurrencyAlphaCode 'Quote', m.MinTickTime, m.MaxTickTime, m.Ticks
from

(select DataProviderID, FXPairID, [dbo].[GetFormattedTickTime](min(TickTime)) 'MinTickTime', [dbo].[GetFormattedTickTime](max(TickTime)) 'MaxTickTime', count(*) 'Ticks' from Tick with (NOLOCK)
group by DataProviderID, FXPairID) m

join DataProvider d on m.DataProviderID = d.DataProviderID


join FXPair f on m.FXPairID = f.FXPairID
join Currency c1 on f.BaseCurrencyID = c1.CurrencyID
join Currency c2 on f.QuoteCurrencyID = c2.CurrencyID

order by Provider, Base, Quote

END





GO
/****** Object:  StoredProcedure [dbo].[ShowSpreadStats]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ShowSpreadStats]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select t1.FXPairID, c1.CurrencyAlphaCode 'Base', c2.CurrencyAlphaCode 'Quote', t2.Bid, t2.Ask, Round(t2.Ask - t2.Bid, 4) 'Spread', s.Comment
from

(select FXPairID, min(TickID) 'MinTickID'
from Tick with (NOLOCK)
group by FXPairID) t1
join Tick t2 on t1.MinTickID = t2.TickID
join FXPair f on t1.FXPairID = f.FXPairID
join Currency c1 on f.BaseCurrencyID = c1.CurrencyID
join Currency c2 on f.QuoteCurrencyID = c2.CurrencyID
join SourceFile s on t2. SourceFileID = s.SourceFileID

order by Base, Quote
END

GO
/****** Object:  StoredProcedure [dbo].[ShrinkFXtickDB]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ShrinkFXtickDB] 
AS
BEGIN
	SET NOCOUNT ON;

	BACKUP LOG FXtickDB WITH TRUNCATE_ONLY

	DBCC SHRINKFILE('FXtickDB_log')
	-- WITH NO_INFOMSGS
END

GO
/****** Object:  StoredProcedure [dbo].[SourceFileTicks]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SourceFileTicks] 
	@SourceFileID int
AS
BEGIN

select

@SourceFileID SourceFileID,

(
select count(*)
from Tick with (NOLOCK)
where SourceFileID = @SourceFileID
AND UniqueTick = 1
)
'unique',

(
select count(*)
from Tick with (NOLOCK)
where SourceFileID = @SourceFileID
AND UniqueTick = 0
)
'duplicate',

(
select count(*)
from Tick with (NOLOCK)
where SourceFileID = @SourceFileID
AND UniqueTick is null
)
'null'


END


GO
/****** Object:  StoredProcedure [dbo].[UpdateUniqueDuplicateAndUnknownInSourceFiles]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateUniqueDuplicateAndUnknownInSourceFiles] 
AS
BEGIN
	SET NOCOUNT ON;

-- NO LONGER USED:

--update ResultTable
--set
--TicksUniqueInFile = SetUnique,
--TicksDuplicateInFile = SetDuplicate,
--TicksUnknownInFile = SetUnknown
--
--from
--(
--select
--s.SourceFileID,
--s.TicksUniqueInFile,
--s.TicksDuplicateInFile,
--s.TicksUnknownInFile,
--coalesce(u,0) 'SetUnique',
--coalesce(d,0) 'SetDuplicate',
--coalesce(w,0) 'SetUnknown'
--
--from
--
--(select * from SourceFile where TicksUnknownInFile > 0) s
--
---- unique ticks count for every SourceFileID:
--left join
--(select SourceFileID, count(*) 'u' from Tick where UniqueTick = 1 group by SourceFileID) UniqueTicks
--on s.SourceFileID = UniqueTicks.SourceFileID
--
---- duplicate ticks count for every SourceFileID:
--left join
--(select SourceFileID, count(*) 'd' from Tick where UniqueTick = 0 group by SourceFileID) DuplicateTicks
--on s.SourceFileID = DuplicateTicks.SourceFileID
--
---- unknown ticks count for every SourceFileID:
--left join
--(select SourceFileID, count(*) 'w' from Tick where UniqueTick is null group by SourceFileID) UnknownTicks
--on s.SourceFileID = UnknownTicks.SourceFileID
--) ResultTable

END
GO
/****** Object:  UserDefinedFunction [db_datareader].[Split_StringTokenList]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [db_datareader].[Split_StringTokenList] 
( 
@TokenList nvarchar(MAX), 
@Delimiter nvarchar(5) 
) 
RETURNS @ProcessedTokenList TABLE (Token nvarchar(max) ) 
AS 
BEGIN 
	DECLARE @SplitLength INT 

	WHILE LEN(@TokenList) > 0 
	BEGIN 
		SELECT @SplitLength = (CASE CHARINDEX(@Delimiter,@TokenList) WHEN 0 THEN 
								LEN(@TokenList) 
								ELSE CHARINDEX(@Delimiter,@TokenList) -1 
								END) 

		INSERT INTO @ProcessedTokenList 
		SELECT SUBSTRING(@TokenList,1,@SplitLength)

		SELECT @TokenList = (CASE (LEN(@TokenList) - @SplitLength) WHEN 0 THEN '' 
		ELSE RIGHT(@TokenList, LEN(@TokenList) - @SplitLength - 1) END) 
	END 

	RETURN 
END
GO
GRANT SELECT ON [db_datareader].[Split_StringTokenList] TO [SqlMoSPExecuterAccess] AS [db_datareader]
GO
/****** Object:  UserDefinedFunction [dbo].[ConvertDateTime2ToTimeOfDayTicks]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ConvertDateTime2ToTimeOfDayTicks] 
(
	@DateTime2Input DATETIME2
)
RETURNS bigint
AS
BEGIN
-- converts datetime2 (time of day only) to ticks (units of 100ns)

RETURN
CONVERT(bigint, DATEPART(HOUR, @DateTime2Input))*36000000000
+ CONVERT(bigint, DATEPART(MINUTE, @DateTime2Input))*600000000
+ CONVERT(bigint, DATEPART(SECOND, @DateTime2Input))*10000000
+ CONVERT(bigint, DATEPART(NANOSECOND, @DateTime2Input))/100

END
GO
/****** Object:  UserDefinedFunction [dbo].[ConvertDateTime2ToTimeOfYearMilliseconds]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ConvertDateTime2ToTimeOfYearMilliseconds] 
(
	@DateTime2Input DATETIME2
)
RETURNS bigint
AS
BEGIN
-- converts datetime2 (time of day only) to ticks (units of 100ns)

RETURN
CONVERT(bigint, DATEPART(DAYOFYEAR, @DateTime2Input))*86400000
+ CONVERT(bigint, DATEPART(HOUR, @DateTime2Input))*3600000
+ CONVERT(bigint, DATEPART(MINUTE, @DateTime2Input))*60000
+ CONVERT(bigint, DATEPART(SECOND, @DateTime2Input))*1000
+ CONVERT(bigint, DATEPART(MILLISECOND, @DateTime2Input))

END
GO
/****** Object:  UserDefinedFunction [dbo].[ConvertTickTimeToDateTime]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ConvertTickTimeToDateTime]
(
	@TickTime numeric(17)
)
RETURNS datetime2
AS
BEGIN
-- converts the tick time from the raw format (20070913235556000)
-- to datetime
-- http://technet.microsoft.com/en-us/library/hh213233.aspx


RETURN
DATETIME2FROMPARTS
(
cast(substring(cast(@TickTime as char(17)),1,4) as int), -- year
cast(substring(cast(@TickTime as char(17)),5,2) as int), -- month
cast(substring(cast(@TickTime as char(17)),7,2) as int), -- day
cast(substring(cast(@TickTime as char(17)),9,2) as int), -- hour
cast(substring(cast(@TickTime as char(17)),11,2) as int), -- minute
cast(substring(cast(@TickTime as char(17)),13,2) as int), -- sec
cast(substring(cast(@TickTime as char(17)),15,3) as int)*10000, -- ms, (ms need to be converted to units of 100ns, 1ms/100ns = 10000)
7 -- highest precision (100 ns)
)

END
GO
/****** Object:  UserDefinedFunction [dbo].[DataExists]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[DataExists]
(
	@DataProviderId tinyint,
	@FxPairId tinyint
)
RETURNS bit
AS
BEGIN
declare @DataExists bit;

if(
(
select count(*) from
(select top 1 * from Tick with (NOLOCK) where DataProviderId = @DataProviderId and FxPairId = @FxPairId) t
)
= 1)

set @DataExists = 1;
else
set @DataExists = 0;

return @DataExists;

END

GO
/****** Object:  UserDefinedFunction [dbo].[GetFormattedTickDateOnly]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetFormattedTickDateOnly] 
(
	@TickTime numeric(17)
)
RETURNS char(10)
AS
BEGIN
-- converts the tick time from numeric format (20070913235556000)
-- to date only format (2007/09/13)


RETURN substring(cast(@TickTime as char(17)),1,4)
+ '/' + substring(cast(@TickTime as char(17)),5,2)
+ '/' + substring(cast(@TickTime as char(17)),7,2)

END
GO
/****** Object:  UserDefinedFunction [dbo].[GetFormattedTickTime]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetFormattedTickTime] 
(
	@TickTime numeric(17)
)
RETURNS char(23)
AS
BEGIN
-- converts the tick time from numeric format (20070913235556000)
-- to date/time format (2007/09/13 23:55:56.000)


RETURN substring(cast(@TickTime as char(17)),1,4)
+ '/' + substring(cast(@TickTime as char(17)),5,2)
+ '/' + substring(cast(@TickTime as char(17)),7,2)
+ ' ' + substring(cast(@TickTime as char(17)),9,2)
+ ':' + substring(cast(@TickTime as char(17)),11,2)
+ ':' + substring(cast(@TickTime as char(17)),13,2)
+ '.' + substring(cast(@TickTime as char(17)),15,3)

END
GO
/****** Object:  UserDefinedFunction [dbo].[GetFormattedTickTimeOfDayOnly]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetFormattedTickTimeOfDayOnly] 
(
	@TickTime numeric(17)
)
RETURNS char(12)
AS
BEGIN
-- converts the tick time from numeric format (20070913235556000)
-- to time-of-day only format (23:55:56.000)


RETURN
substring(cast(@TickTime as char(17)),9,2)
+ ':' + substring(cast(@TickTime as char(17)),11,2)
+ ':' + substring(cast(@TickTime as char(17)),13,2)
+ '.' + substring(cast(@TickTime as char(17)),15,3)

END
GO
/****** Object:  UserDefinedFunction [dbo].[GetLastBidAskTable]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetLastBidAskTable] 
(

)
RETURNS 
@tempTable TABLE 
(
	FxPair char(6),
	LastBid real,
	LastAsk real,
	LastTickTime decimal(17)
)
AS
BEGIN

declare @minFxPairId int
declare @maxFxPairId int
declare @fxPairId int
declare @fxPair char(6)
declare @lastTickTime decimal(17)

declare @lastBid real
declare @lastAsk real

set @minFxPairId = (select min(FxPairId) from FxPair)
set @maxFxPairId = (select max(FxPairId) from FxPair)

set @fxPairId = @minFxPairId

while @fxPairId <= @maxFxPairId
begin
	
	if @fxPairId in (select FxPairId from FxPair/* where IsRaboTradable = 1*/)
	begin
		set @fxPair = (select FxpCode from FxPair where FxPairId = @fxPairId)

		----------------------------------------------------
		set @lastTickTime = (select max(TickTime) from Tick with (NOLOCK) where DataProviderId = 22 and FxPairId = @fxPairId)
		-- TODO: select the highest TickID among the lastTickTime ticks

		set @lastBid = (select top 1 Bid
			from Tick with (NOLOCK)
			where DataProviderId = 22 and FxPairId = @fxPairId and TickTime = @lastTickTime)

		set @lastAsk = (select top 1 Ask
			from Tick with (NOLOCK)
			where DataProviderId = 22 and FxPairId = @fxPairId and TickTime = @lastTickTime)

	
		insert into @tempTable (FxPair, LastBid, LastAsk, LastTickTime) values (@fxPair, @lastBid, @lastAsk, @lastTickTime)
		----------------------------------------------------
	end

	set @fxPairId = @fxPairId + 1
end


RETURN 
END

GO
/****** Object:  UserDefinedFunction [dbo].[GetLastBidsTable]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetLastBidsTable] 
(

)
RETURNS 
@tempTable TABLE 
(
	FxPair char(6),
	LastBid real,
	LastAsk real,
	LastTickTime decimal(17)
)
AS
BEGIN

declare @minFxPairId int
declare @maxFxPairId int
declare @fxPairId int
declare @fxPair char(6)
declare @lastTickTime decimal(17)

declare @lastBid real
declare @lastAsk real

set @minFxPairId = (select min(FxPairId) from FxPair)
set @maxFxPairId = (select max(FxPairId) from FxPair)

set @fxPairId = @minFxPairId

while @fxPairId <= @maxFxPairId
begin
	
	if @fxPairId in (select FxPairId from FxPair where IsRaboTradable = 1)
	begin
		set @fxPair = (select FxpCode from FxPair where FxPairId = @fxPairId)

		----------------------------------------------------
		set @lastTickTime = (select max(TickTime) from Tick with (NOLOCK) where DataProviderId = 22 and FxPairId = @fxPairId)
		-- TODO: select the highest TickID among the lastTickTime ticks

		set @lastBid = (select top 1 Bid
			from Tick with (NOLOCK)
			where DataProviderId = 22 and FxPairId = @fxPairId and TickTime = @lastTickTime)

		set @lastAsk = (select top 1 Ask
			from Tick with (NOLOCK)
			where DataProviderId = 22 and FxPairId = @fxPairId and TickTime = @lastTickTime)

	
		insert into @tempTable (FxPair, LastBid, LastAsk, LastTickTime) values (@fxPair, @lastBid, @lastAsk, @lastTickTime)
		----------------------------------------------------
	end

	set @fxPairId = @fxPairId + 1
end


RETURN 
END

GO
/****** Object:  UserDefinedFunction [dbo].[GetMsecCountForDays]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetMsecCountForDays] 
(
	@TickTime numeric(17)
)
RETURNS decimal(28)
AS
BEGIN
-- converts the tick time from numeric format (20070913235556000)
-- to date/time format (2007/09/13 23:55:56.000)


RETURN 
86400000 * cast(substring(cast(@TickTime as char(17)),7,2) as decimal)-- days
+ 3600000 * substring(cast(@TickTime as char(17)),9,2) -- hours
+ 60000 * substring(cast(@TickTime as char(17)),11,2) -- minutes
+ 1000 * substring(cast(@TickTime as char(17)),13,2) -- seconds
+ substring(cast(@TickTime as char(17)),15,3) -- ms


END
GO
/****** Object:  UserDefinedFunction [dbo].[GetRaboLastBidsTable]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetRaboLastBidsTable] 
(

)
RETURNS 
@tempTable TABLE 
(
	FxPair char(6),
	LastBid real,
	LastAsk real,
	LastTickTime decimal(17)
)
AS
BEGIN

declare @minFxPairId int
declare @maxFxPairId int
declare @fxPairId int
declare @fxPair char(6)
declare @lastTickTime decimal(17)

declare @lastBid real
declare @lastAsk real

set @minFxPairId = (select min(FxPairId) from FxPair where IsRaboTradable = 1)
set @maxFxPairId = (select max(FxPairId) from FxPair where IsRaboTradable = 1)

set @fxPairId = @minFxPairId

while @fxPairId <= @maxFxPairId
begin
	
	if @fxPairId in (select FxPairId from FxPair where IsRaboTradable = 1)
	begin
		set @fxPair = (select FxpCode from FxPair where FxPairId = @fxPairId)

		----------------------------------------------------
		set @lastTickTime = (select max(TickTime) from Tick with (NOLOCK) where DataProviderId = 22 and FxPairId = @fxPairId)
		-- TODO: select the highest TickID among the lastTickTime ticks

		set @lastBid = (select top 1 Bid
			from Tick with (NOLOCK)
			where DataProviderId = 22 and FxPairId = @fxPairId and TickTime = @lastTickTime)

		set @lastAsk = (select top 1 Ask
			from Tick with (NOLOCK)
			where DataProviderId = 22 and FxPairId = @fxPairId and TickTime = @lastTickTime)

	
		insert into @tempTable (FxPair, LastBid, LastAsk, LastTickTime) values (@fxPair, @lastBid, @lastAsk, @lastTickTime)
		----------------------------------------------------
	end

	set @fxPairId = @fxPairId + 1
end


RETURN 
END

GO
/****** Object:  UserDefinedFunction [dbo].[GetTradeDecayTimeIntervalBucket]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetTradeDecayTimeIntervalBucket](@msDuration AS INT) RETURNS INT
AS
BEGIN
    RETURN CASE 
		WHEN @msDuration >= 10000 THEN @msDuration - @msDuration%1000
		WHEN @msDuration >= 1000 THEN @msDuration - @msDuration%100
		WHEN @msDuration >= 300 THEN @msDuration - @msDuration%10
		ELSE @msDuration END
END

GO
/****** Object:  UserDefinedFunction [dbo].[GetWeekDayNameOfDate]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------
--- GetWeekDayNameOfDate
--- Returns Week Day Name in English
--- Takes @@DATEFIRST into consideration
--- You can edit udf for other languages
----------------------------------------
CREATE FUNCTION [dbo].[GetWeekDayNameOfDate]
(
  @Date datetime
)
RETURNS nvarchar(50)
BEGIN

DECLARE @DayName nvarchar(50)

SELECT
  @DayName =
  CASE (DATEPART(dw, @Date) + @@DATEFIRST) % 7
    WHEN 1 THEN 'Sun'
    WHEN 2 THEN 'Mon'
    WHEN 3 THEN 'Tue'
    WHEN 4 THEN 'Wed'
    WHEN 5 THEN 'Thu'
    WHEN 6 THEN 'Fri'
    WHEN 0 THEN 'Sat'
  END

RETURN @DayName

END


GO
/****** Object:  UserDefinedFunction [dbo].[GetWeekDayNameOfDayIdx]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------
--- GetWeekDayNameOfDayIdx
--- Returns Week Day Name in English
--- Takes @@DATEFIRST into consideration
--- You can edit udf for other languages
----------------------------------------
CREATE FUNCTION [dbo].[GetWeekDayNameOfDayIdx]
(
  @DayIdx int
)
RETURNS nvarchar(50)
BEGIN

DECLARE @DayName nvarchar(50)

SELECT
  @DayName =
  CASE (@DayIdx + @@DATEFIRST) % 7
    WHEN 1 THEN 'Sun'
    WHEN 2 THEN 'Mon'
    WHEN 3 THEN 'Tue'
    WHEN 4 THEN 'Wed'
    WHEN 5 THEN 'Thu'
    WHEN 6 THEN 'Fri'
    WHEN 0 THEN 'Sat'
  END

RETURN @DayName

END


GO
/****** Object:  UserDefinedFunction [dbo].[ShiftTickTimeWithinDay]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ShiftTickTimeWithinDay]
(
	@TickTime numeric(17),
	@ShiftInSeconds int
)
RETURNS numeric(17)
AS
BEGIN
-------------------------------
-- fast & simple version, doesn't take date roll into account!
-------------------------------
declare @hoursOrig int;
declare @minutesOrig int;
declare @secondsOrig int;

declare @sumSecondsOrig int;
declare @sumSecondsShifted int;

declare @hoursShifted int;
declare @minutesShifted int;
declare @secondsShifted int;

declare @convertedTickTime numeric(17);

set @hoursOrig = cast(substring(cast(@TickTime as char(17)),9,2) as int);
set @minutesOrig = cast(substring(cast(@TickTime as char(17)),11,2) as int);
set @secondsOrig = cast(substring(cast(@TickTime as char(17)),13,2) as int);

set @sumSecondsOrig = (@hoursOrig * 3600) + (@minutesOrig * 60) + @secondsOrig;
set @sumSecondsShifted = @sumSecondsOrig + @ShiftInSeconds;

declare @minutesAndSecondsPartShifted int;
set @minutesAndSecondsPartShifted = @sumSecondsShifted % 3600;
set @hoursShifted = (@sumSecondsShifted - @minutesAndSecondsPartShifted)/3600;

set @secondsShifted = @minutesAndSecondsPartShifted % 60;
set @minutesShifted = (@minutesAndSecondsPartShifted - @secondsShifted)/60;


declare @convertedTickTimeChars char(17);
set @convertedTickTimeChars  =
substring(cast(@TickTime as char(17)),1,4)
+ substring(cast(@TickTime as char(17)),5,2)
+ substring(cast(@TickTime as char(17)),7,2)
+ right('00' + convert(varchar, @hoursShifted),2)
+ right('00' + convert(varchar, @minutesShifted),2)
+ right('00' + convert(varchar, @secondsShifted),2)
+ substring(cast(@TickTime as char(17)),15,3);

set @convertedTickTime = convert(numeric(17), @convertedTickTimeChars);

return @convertedTickTime

END
GO
/****** Object:  Table [dbo].[_Internal_CounterpartyIdsMapping]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[_Internal_CounterpartyIdsMapping](
	[IntegratorDBCounterpartyId] [tinyint] NOT NULL,
	[DCDBCounterpartyId] [tinyint] NOT NULL,
	[CounterpartyCode] [char](3) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[_Internal_CurrencyIdsMapping]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[_Internal_CurrencyIdsMapping](
	[IntegratorDBCurrencyId] [smallint] NOT NULL,
	[DCDBCurrencyId] [smallint] NOT NULL,
	[CurrencyAlphaCode] [char](3) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[_Internal_CurrencyIdsMapping] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Table [dbo].[_Internal_SymbolIdsMapping]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[_Internal_SymbolIdsMapping](
	[IntegratorDBSymbolId] [tinyint] NOT NULL,
	[DCDBSymbolId] [tinyint] NOT NULL,
	[SymbolCodeNoSlash] [char](6) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[_Internal_SymbolIdsMapping] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Table [dbo].[_Internal_TradeDecayIntervals]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[_Internal_TradeDecayIntervals](
	[MsDistanceFromEventStart] [int] NOT NULL,
	[SideId] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CounterpartySymbolSettings]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CounterpartySymbolSettings](
	[TradingTargetTypeId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[Supported] [bit] NOT NULL,
	[OnePipDecimalValue_Legacy] [decimal](18, 11) NULL,
	[MinimumPriceIncrement] [decimal](18, 11) NULL,
	[OrderMinimumSize] [decimal](18, 6) NULL,
	[OrderMinimumSizeIncrement] [decimal](18, 6) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CounterpartySymbolSettings_old]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CounterpartySymbolSettings_old](
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[OnePipDecimalValue] [decimal](18, 9) NULL,
	[PriceGranularity] [decimal](18, 9) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Currency]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrencyID] [smallint] IDENTITY(1,1) NOT NULL,
	[CurrencyAlphaCode] [nvarchar](50) NULL,
	[CurrencyName] [nvarchar](250) NULL,
	[CurrencyISOCode] [smallint] NULL,
	[Valid] [bit] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[IsCitibankPbSupported] [bit] NOT NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[CurrencyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DataProvider]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DataProvider](
	[DataProviderId] [tinyint] IDENTITY(1,1) NOT NULL,
	[DprCode] [char](3) NOT NULL,
	[DataProviderName] [varchar](50) NULL,
	[DataSourceId] [int] NOT NULL,
 CONSTRAINT [PK_Broker] PRIMARY KEY CLUSTERED 
(
	[DataProviderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[DataProvider] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  Table [dbo].[DataSource]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DataSource](
	[DataSourceId] [int] IDENTITY(1,1) NOT NULL,
	[DataSourceName] [varchar](50) NOT NULL,
	[DsrcCode] [char](3) NOT NULL,
	[DataSourceUrl] [varchar](250) NOT NULL,
 CONSTRAINT [PK_DataSource] PRIMARY KEY CLUSTERED 
(
	[DataSourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Deal]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Deal](
	[DealId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Deal] PRIMARY KEY CLUSTERED 
(
	[DealId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EODRate]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EODRate](
	[FXPairId] [tinyint] NOT NULL,
	[Date] [date] NOT NULL,
	[BidPrice] [decimal](18, 9) NULL,
	[AskPrice] [decimal](18, 9) NULL,
	[IsBidPriceAuthentic] [bit] NOT NULL,
	[IsAskPriceAuthentic] [bit] NOT NULL,
	[IsMidPriceAuthentic]  AS (case when [IsBidPriceAuthentic]=(1) AND [IsAskPriceAuthentic]=(1) then (1) else (0) end),
	[MidPrice]  AS (([AskPrice]+[BidPrice])/(2))
) ON [PRIMARY]

GO
GRANT SELECT ON [dbo].[EODRate] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EODRate] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Table [dbo].[EODUsdConversions]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EODUsdConversions](
	[CurrencyId] [smallint] NOT NULL,
	[Date] [date] NOT NULL,
	[BidPrice] [decimal](18, 9) NOT NULL,
	[AskPrice] [decimal](18, 9) NOT NULL,
	[MidPrice]  AS (([AskPrice]+[BidPrice])/(2))
) ON [PRIMARY]

GO
GRANT SELECT ON [dbo].[EODUsdConversions] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EODUsdConversions] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Table [dbo].[FpiRing]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FpiRing](
	[FpiRingId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_FpiRing] PRIMARY KEY CLUSTERED 
(
	[FpiRingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FpiRingFxPair]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FpiRingFxPair](
	[FpiRingFxPairId] [int] IDENTITY(1,1) NOT NULL,
	[FpiRingId] [int] NOT NULL,
	[FxPairId] [tinyint] NOT NULL,
 CONSTRAINT [PK_FpiRingFxPair] PRIMARY KEY CLUSTERED 
(
	[FpiRingFxPairId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FxPair]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FxPair](
	[FxPairId] [tinyint] IDENTITY(1,1) NOT NULL,
	[FxpCode] [char](6) NOT NULL,
	[BaseCurrencyId] [smallint] NOT NULL,
	[QuoteCurrencyId] [smallint] NOT NULL,
	[Multiplier] [int] NULL,
	[ExamplePrice] [real] NULL,
	[IsRaboTradable] [bit] NULL,
	[Usage] [tinyint] NULL,
	[MinimumPriceGranularity] [decimal](18, 9) NULL,
	[Decimals]  AS (log10([Multiplier])) PERSISTED,
 CONSTRAINT [PK_FXPair] PRIMARY KEY CLUSTERED 
(
	[FxPairId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[FxPair] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  Table [dbo].[LiqProviderStream]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LiqProviderStream](
	[LiqProviderStreamId] [tinyint] IDENTITY(1,1) NOT NULL,
	[LiqProviderStreamName] [nvarchar](50) NOT NULL,
	[LiqProviderId] [tinyint] NOT NULL,
 CONSTRAINT [PK_LiqProviderStream] PRIMARY KEY CLUSTERED 
(
	[LiqProviderStreamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LiquidityProvider]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LiquidityProvider](
	[LiqProviderId] [tinyint] IDENTITY(1,1) NOT NULL,
	[LiqProviderName] [nvarchar](50) NOT NULL,
	[LiqProviderCode] [char](3) NOT NULL,
 CONSTRAINT [PK_LiquidityProvider] PRIMARY KEY CLUSTERED 
(
	[LiqProviderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MarketData]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarketData](
	[RecordID] [bigint] IDENTITY(1,1) NOT NULL,
	[FXPairId] [tinyint] NOT NULL,
	[SideId] [bit] NULL,
	[Price] [decimal](18, 9) NULL,
	[Size] [decimal](18, 0) NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradeSideId] [bit] NULL,
	[CounterpartySentTimeUtc] [datetime2](7) NULL,
	[IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
	[CounterpartyPriceIdentity] [nvarchar](max) NULL,
	[IntegratorPriceIdentity] [uniqueidentifier] NULL,
	[MinimumSize] [decimal](18, 0) NULL,
	[Granularity] [decimal](18, 0) NULL,
	[RecordTypeId] [tinyint] NOT NULL,
	[IsLastInContinuousData] [bit] NULL
) ON [MarketDataSymbolPartitionScheme_IndividualFileGroups]([FXPairId])

GO
GRANT INSERT ON [dbo].[MarketData] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[MarketData] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Table [dbo].[MarketDataRecordType]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarketDataRecordType](
	[RecordTypeId] [tinyint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MarketDataRecordType] PRIMARY KEY CLUSTERED 
(
	[RecordTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OutgoingSoftPrice]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OutgoingSoftPrice](
	[RecordID] [bigint] IDENTITY(1,1) NOT NULL,
	[FXPairId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SideId] [bit] NOT NULL,
	[Price] [decimal](18, 9) NULL,
	[SizeBaseAbs] [decimal](18, 2) NULL,
	[TradingSystemId] [int] NULL,
	[LayerOrderId] [tinyint] NOT NULL,
	[IntegratorSentTimeUtc] [datetime2](7) NOT NULL,
	[IntegratorPriceIdentity] [varchar](20) NULL
) ON [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups]([FXPairId])

GO
SET ANSI_PADDING OFF
GO
GRANT INSERT ON [dbo].[OutgoingSoftPrice] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[OutgoingSoftPrice] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Table [dbo].[PriceSide]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PriceSide](
	[PriceSideId] [bit] NOT NULL,
	[PriceSideName] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_PriceSide] PRIMARY KEY CLUSTERED 
(
	[PriceSideId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SourceFile]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SourceFile](
	[SourceFileID] [int] IDENTITY(4000,1) NOT NULL,
	[FileName] [nvarchar](max) NULL,
	[FileModifiedOn] [smalldatetime] NOT NULL,
	[FileSizeBytes] [bigint] NOT NULL,
	[TicksTotalInFile] [bigint] NULL,
	[TicksUniqueInFile] [bigint] NULL,
	[TicksDuplicateInFile] [bigint] NULL,
	[TicksUnknownInFile] [bigint] NULL,
	[FileDataTimeZoneID] [tinyint] NOT NULL,
	[FileImportTag] [nvarchar](max) NULL,
	[FileImportedOn] [smalldatetime] NOT NULL,
	[Comment] [nvarchar](max) NULL,
 CONSTRAINT [PK_SourceFile] PRIMARY KEY CLUSTERED 
(
	[SourceFileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tick]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tick](
	[TickID] [bigint] IDENTITY(2300000000,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON [TicksSymbolPartitionScheme_IndividualFileGroups]([FXPairID])

GO
GRANT INSERT ON [dbo].[Tick] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[Tick] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT INSERT ON [dbo].[Tick] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[Tick] TO [SqlMoSPExecuterAccess] AS [dbo]
GO
/****** Object:  Table [dbo].[TimeZone]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TimeZone](
	[TimeZoneID] [tinyint] IDENTITY(1,1) NOT NULL,
	[TznCode] [varchar](50) NULL,
	[TimeZoneName] [varchar](100) NULL,
	[DaylightSavingAdjusted] [bit] NOT NULL,
	[GMTOffsetSummer] [tinyint] NULL,
	[GMTOffestWinter] [tinyint] NULL,
	[SummerTimeStartsOn] [datetime] NULL,
	[WinterTimeStartsOn] [datetime] NULL,
 CONSTRAINT [PK_TimeZone] PRIMARY KEY CLUSTERED 
(
	[TimeZoneID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TradeDecayAnalysis]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradeDecayAnalysis](
	[TradeDecayAnalysisId] [int] IDENTITY(1,1) NOT NULL,
	[TradeDecayAnalysisTypeId] [tinyint] NOT NULL,
	[SourceEventGlobalRecordId] [int] NOT NULL,
	[AnalysisStartTimeUtc] [datetime2](7) NOT NULL,
	[FXPairId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[PriceSideId] [bit] NOT NULL,
	[HasValidTickAfterIntervalEnd] [bit] NOT NULL,
	[IsValidAndComplete] [bit] NOT NULL,
 CONSTRAINT [PK_TradeDecayAnalysis] PRIMARY KEY NONCLUSTERED 
(
	[TradeDecayAnalysisId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [TradeDecayFilegroup]
) ON [TradeDecayFilegroup]

GO
/****** Object:  Table [dbo].[TradeDecayAnalysis_Pending]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradeDecayAnalysis_Pending](
	[PendingTradeDecayAnalysisId] [int] IDENTITY(1,1) NOT NULL,
	[TradeDecayAnalysisTypeId] [tinyint] NOT NULL,
	[SourceEventGlobalRecordId] [int] NOT NULL,
	[AnalysisStartTimeUtc] [datetime2](7) NOT NULL,
	[FXPairId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[ReferencePrice] [decimal](18, 9) NOT NULL,
	[PriceSideId] [bit] NOT NULL,
	[AcquiredForCalculationTimeUtc] [datetime2](7) NULL
) ON [TradeDecayFilegroup]

GO
/****** Object:  Table [dbo].[TradeDecayAnalysisDataPoint]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradeDecayAnalysisDataPoint](
	[TradeDecayAnalysisId] [int] NOT NULL,
	[MillisecondDistanceFromAnalysisStart] [int] NOT NULL,
	[ReferenceMidPrice] [decimal](18, 9) NOT NULL,
	[BpMtmToReferenceMidPrice] [decimal](18, 9) NOT NULL,
	[ReferenceBidPrice] [decimal](18, 9) NOT NULL,
	[ReferenceAskPrice] [decimal](18, 9) NOT NULL,
	[BpMtmToReferenceCrossPrice] [decimal](18, 9) NOT NULL,
	[BpMtmToReferenceJoinPrice] [decimal](18, 9) NOT NULL,
	[IsReferenceBidAuthenticValue] [bit] NOT NULL,
	[IsReferenceAskAuthenticValue] [bit] NOT NULL,
	[IsReferenceMidAuthenticValue]  AS (case when [IsReferenceBidAuthenticValue]=(1) AND [IsReferenceAskAuthenticValue]=(1) then (1) else (0) end)
) ON [TradeDecayFilegroup]

GO
/****** Object:  Table [dbo].[TradeDecayAnalysisType]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TradeDecayAnalysisType](
	[TradeDecayAnalysisTypeId] [tinyint] IDENTITY(1,1) NOT NULL,
	[TradeDecayAnalysisTypeName] [varchar](16) NOT NULL,
 CONSTRAINT [PK_TradeDecayAnalysisType] PRIMARY KEY CLUSTERED 
(
	[TradeDecayAnalysisTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [TradeDecayFilegroup]
) ON [TradeDecayFilegroup]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TradeSide]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradeSide](
	[TradeSideId] [bit] NOT NULL,
	[TradeSide] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TradeSide] PRIMARY KEY CLUSTERED 
(
	[TradeSideId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TradingTargetType]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TradingTargetType](
	[Id] [tinyint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Code] [varchar](16) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[_dta_mv_0]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_dta_mv_0]
AS
SELECT        dbo.SourceFile.TicksUnknownInFile AS _col_1, dbo.Tick.UniqueTick AS _col_2, COUNT_BIG(*) AS _col_3
FROM            dbo.Tick WITH (NOLOCK) INNER JOIN
                         dbo.SourceFile ON dbo.Tick.SourceFileID = dbo.SourceFile.SourceFileID
GROUP BY dbo.SourceFile.TicksUnknownInFile, dbo.Tick.UniqueTick

GO
/****** Object:  View [dbo].[Counterparty]    Script Date: 10. 6. 2015 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Counterparty]
as
select * from LiqProviderStream
GO
/****** Object:  Index [IX__Internal_CounterpartyIdsMapping_IntegratorDbCpt]    Script Date: 10. 6. 2015 15:42:49 ******/
CREATE UNIQUE CLUSTERED INDEX [IX__Internal_CounterpartyIdsMapping_IntegratorDbCpt] ON [dbo].[_Internal_CounterpartyIdsMapping]
(
	[IntegratorDBCounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX__Internal_CurrencyIdsMapping_IntegratorDbCurrency]    Script Date: 10. 6. 2015 15:42:49 ******/
CREATE UNIQUE CLUSTERED INDEX [IX__Internal_CurrencyIdsMapping_IntegratorDbCurrency] ON [dbo].[_Internal_CurrencyIdsMapping]
(
	[IntegratorDBCurrencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX__Internal_SymbolIdsMapping_IntegratorDbSymbol]    Script Date: 10. 6. 2015 15:42:49 ******/
CREATE UNIQUE CLUSTERED INDEX [IX__Internal_SymbolIdsMapping_IntegratorDbSymbol] ON [dbo].[_Internal_SymbolIdsMapping]
(
	[IntegratorDBSymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX__Internal_TradeDecayIntervals2]    Script Date: 10. 6. 2015 15:42:49 ******/
CREATE UNIQUE CLUSTERED INDEX [IX__Internal_TradeDecayIntervals2] ON [dbo].[_Internal_TradeDecayIntervals]
(
	[SideId] ASC,
	[MsDistanceFromEventStart] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_EODRate_DatePair]    Script Date: 10. 6. 2015 15:42:49 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_EODRate_DatePair] ON [dbo].[EODRate]
(
	[Date] ASC,
	[FXPairId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_EODUsdConversions_DatePair]    Script Date: 10. 6. 2015 15:42:49 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_EODUsdConversions_DatePair] ON [dbo].[EODUsdConversions]
(
	[Date] ASC,
	[CurrencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_PairTime]    Script Date: 10. 6. 2015 15:42:49 ******/
CREATE CLUSTERED INDEX [IX_PairTime] ON [dbo].[MarketData]
(
	[FXPairId] ASC,
	[IntegratorReceivedTimeUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [MarketDataSymbolPartitionScheme_IndividualFileGroups]([FXPairId])
GO
/****** Object:  Index [IX_PairCounterpartyTime]    Script Date: 10. 6. 2015 15:42:49 ******/
CREATE CLUSTERED INDEX [IX_PairCounterpartyTime] ON [dbo].[OutgoingSoftPrice]
(
	[FXPairId] ASC,
	[IntegratorSentTimeUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups]([FXPairId])
GO
/****** Object:  Index [ProviderPairTime]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE CLUSTERED INDEX [ProviderPairTime] ON [dbo].[Tick]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [TicksSymbolPartitionScheme_IndividualFileGroups]([FXPairID])
GO
/****** Object:  Index [IX_TradeDecayAnalysis_CounterpartyTime]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE CLUSTERED INDEX [IX_TradeDecayAnalysis_CounterpartyTime] ON [dbo].[TradeDecayAnalysis]
(
	[CounterpartyId] ASC,
	[AnalysisStartTimeUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [TradeDecayFilegroup]
GO
/****** Object:  Index [IX_TradeDecayAnalysis_Pending_Time]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE CLUSTERED INDEX [IX_TradeDecayAnalysis_Pending_Time] ON [dbo].[TradeDecayAnalysis_Pending]
(
	[AnalysisStartTimeUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [TradeDecayFilegroup]
GO
/****** Object:  Index [IX_TradeDecayAnalysis_CounterpartyTime]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_TradeDecayAnalysis_CounterpartyTime] ON [dbo].[TradeDecayAnalysisDataPoint]
(
	[TradeDecayAnalysisId] ASC,
	[MillisecondDistanceFromAnalysisStart] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [TradeDecayFilegroup]
GO
/****** Object:  Index [PK_TradingTargetType]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE CLUSTERED INDEX [PK_TradingTargetType] ON [dbo].[TradingTargetType]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX__Internal_CounterpartyIdsMapping_DCDbCpt]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX__Internal_CounterpartyIdsMapping_DCDbCpt] ON [dbo].[_Internal_CounterpartyIdsMapping]
(
	[DCDBCounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX__Internal_CurrencyIdsMapping_DCDBCurrency]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX__Internal_CurrencyIdsMapping_DCDBCurrency] ON [dbo].[_Internal_CurrencyIdsMapping]
(
	[DCDBCurrencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX__Internal_SymbolIdsMapping_DCDbSymbol]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX__Internal_SymbolIdsMapping_DCDbSymbol] ON [dbo].[_Internal_SymbolIdsMapping]
(
	[DCDBSymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UniqueTradingTargetTypeAndSymbolCombination]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueTradingTargetTypeAndSymbolCombination] ON [dbo].[CounterpartySymbolSettings]
(
	[TradingTargetTypeId] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]
GO
/****** Object:  Index [UniqueCounterpartyAndSymbolCombination]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCounterpartyAndSymbolCombination] ON [dbo].[CounterpartySymbolSettings_old]
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueCurrencyAlphaCode]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyAlphaCode] ON [dbo].[Currency]
(
	[CurrencyAlphaCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UniqueCurrencyIsoCode]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyIsoCode] ON [dbo].[Currency]
(
	[CurrencyISOCode] ASC
)
WHERE ([CurrencyISOCode] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueCurrencyName]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyName] ON [dbo].[Currency]
(
	[CurrencyName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DataProviderName_Index]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [DataProviderName_Index] ON [dbo].[DataProvider]
(
	[DataProviderName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DprCode_Index]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [DprCode_Index] ON [dbo].[DataProvider]
(
	[DprCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DataSourceName]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [DataSourceName] ON [dbo].[DataSource]
(
	[DataSourceName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DscrCode]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [DscrCode] ON [dbo].[DataSource]
(
	[DsrcCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FpiRingFxPairUnique]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [FpiRingFxPairUnique] ON [dbo].[FpiRingFxPair]
(
	[FpiRingId] ASC,
	[FxPairId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [FxpCode_Index]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [FxpCode_Index] ON [dbo].[FxPair]
(
	[FxpCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]
GO
/****** Object:  Index [UniqueFXpair]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueFXpair] ON [dbo].[FxPair]
(
	[BaseCurrencyId] ASC,
	[QuoteCurrencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueLiqProviderStreamName]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueLiqProviderStreamName] ON [dbo].[LiqProviderStream]
(
	[LiqProviderStreamName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueLiqProviderCode]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueLiqProviderCode] ON [dbo].[LiquidityProvider]
(
	[LiqProviderCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueLiqProviderName]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueLiqProviderName] ON [dbo].[LiquidityProvider]
(
	[LiqProviderName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [SourceFile]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE NONCLUSTERED INDEX [SourceFile] ON [dbo].[Tick]
(
	[SourceFileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [TicksSymbolPartitionScheme_IndividualFileGroups]([FXPairID])
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TznCode_Index]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [TznCode_Index] ON [dbo].[TimeZone]
(
	[TznCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TradeDecayAnalysisTypeName]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TradeDecayAnalysisTypeName] ON [dbo].[TradeDecayAnalysisType]
(
	[TradeDecayAnalysisTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [TradeDecayFilegroup]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TradingTargetTypeCode]    Script Date: 10. 6. 2015 15:42:50 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TradingTargetTypeCode] ON [dbo].[TradingTargetType]
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CounterpartySymbolSettings]  WITH CHECK ADD  CONSTRAINT [FK_CounterpartySymbolSettings_FxPair] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[CounterpartySymbolSettings] CHECK CONSTRAINT [FK_CounterpartySymbolSettings_FxPair]
GO
ALTER TABLE [dbo].[CounterpartySymbolSettings]  WITH CHECK ADD  CONSTRAINT [FK_CounterpartySymbolSettings_TradingTargetType] FOREIGN KEY([TradingTargetTypeId])
REFERENCES [dbo].[TradingTargetType] ([Id])
GO
ALTER TABLE [dbo].[CounterpartySymbolSettings] CHECK CONSTRAINT [FK_CounterpartySymbolSettings_TradingTargetType]
GO
ALTER TABLE [dbo].[CounterpartySymbolSettings_old]  WITH CHECK ADD  CONSTRAINT [FK_CounterpartySymbolAvailability_FxPair] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[CounterpartySymbolSettings_old] CHECK CONSTRAINT [FK_CounterpartySymbolAvailability_FxPair]
GO
ALTER TABLE [dbo].[CounterpartySymbolSettings_old]  WITH CHECK ADD  CONSTRAINT [FK_CounterpartySymbolAvailability_LiquidityProvider] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[LiquidityProvider] ([LiqProviderId])
GO
ALTER TABLE [dbo].[CounterpartySymbolSettings_old] CHECK CONSTRAINT [FK_CounterpartySymbolAvailability_LiquidityProvider]
GO
ALTER TABLE [dbo].[DataProvider]  WITH CHECK ADD  CONSTRAINT [FK_DataProvider_DataSource] FOREIGN KEY([DataSourceId])
REFERENCES [dbo].[DataSource] ([DataSourceId])
GO
ALTER TABLE [dbo].[DataProvider] CHECK CONSTRAINT [FK_DataProvider_DataSource]
GO
ALTER TABLE [dbo].[EODRate]  WITH NOCHECK ADD  CONSTRAINT [FK_EODRate_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[EODRate] CHECK CONSTRAINT [FK_EODRate_FxPair]
GO
ALTER TABLE [dbo].[EODUsdConversions]  WITH NOCHECK ADD  CONSTRAINT [FK_EODRate_Currency] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currency] ([CurrencyID])
GO
ALTER TABLE [dbo].[EODUsdConversions] CHECK CONSTRAINT [FK_EODRate_Currency]
GO
ALTER TABLE [dbo].[FpiRingFxPair]  WITH CHECK ADD  CONSTRAINT [FK_FpiRingFxPair_FpiRing] FOREIGN KEY([FpiRingId])
REFERENCES [dbo].[FpiRing] ([FpiRingId])
GO
ALTER TABLE [dbo].[FpiRingFxPair] CHECK CONSTRAINT [FK_FpiRingFxPair_FpiRing]
GO
ALTER TABLE [dbo].[FxPair]  WITH CHECK ADD  CONSTRAINT [FK_FXPair_Currency] FOREIGN KEY([BaseCurrencyId])
REFERENCES [dbo].[Currency] ([CurrencyID])
GO
ALTER TABLE [dbo].[FxPair] CHECK CONSTRAINT [FK_FXPair_Currency]
GO
ALTER TABLE [dbo].[FxPair]  WITH CHECK ADD  CONSTRAINT [FK_FXPair_Currency1] FOREIGN KEY([QuoteCurrencyId])
REFERENCES [dbo].[Currency] ([CurrencyID])
GO
ALTER TABLE [dbo].[FxPair] CHECK CONSTRAINT [FK_FXPair_Currency1]
GO
ALTER TABLE [dbo].[LiqProviderStream]  WITH CHECK ADD  CONSTRAINT [FK_LiqProviderStream_LiquidityProvider] FOREIGN KEY([LiqProviderId])
REFERENCES [dbo].[LiquidityProvider] ([LiqProviderId])
GO
ALTER TABLE [dbo].[LiqProviderStream] CHECK CONSTRAINT [FK_LiqProviderStream_LiquidityProvider]
GO
ALTER TABLE [dbo].[MarketData]  WITH NOCHECK ADD  CONSTRAINT [FK_MarketData_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_FxPair]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_LiqProviderStream] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_LiqProviderStream]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_MarketDataRecordType] FOREIGN KEY([RecordTypeId])
REFERENCES [dbo].[MarketDataRecordType] ([RecordTypeId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_MarketDataRecordType]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_PriceSide] FOREIGN KEY([SideId])
REFERENCES [dbo].[PriceSide] ([PriceSideId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_PriceSide]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_TradeSide] FOREIGN KEY([TradeSideId])
REFERENCES [dbo].[TradeSide] ([TradeSideId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_TradeSide]
GO
ALTER TABLE [dbo].[OutgoingSoftPrice]  WITH NOCHECK ADD  CONSTRAINT [FK_OutgoingSoftPrice_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[OutgoingSoftPrice] CHECK CONSTRAINT [FK_OutgoingSoftPrice_FxPair]
GO
ALTER TABLE [dbo].[OutgoingSoftPrice]  WITH CHECK ADD  CONSTRAINT [FK_OutgoingSoftPrice_LiqProviderStream] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO
ALTER TABLE [dbo].[OutgoingSoftPrice] CHECK CONSTRAINT [FK_OutgoingSoftPrice_LiqProviderStream]
GO
ALTER TABLE [dbo].[OutgoingSoftPrice]  WITH CHECK ADD  CONSTRAINT [FK_OutgoingSoftPrice_PriceSide] FOREIGN KEY([SideId])
REFERENCES [dbo].[PriceSide] ([PriceSideId])
GO
ALTER TABLE [dbo].[OutgoingSoftPrice] CHECK CONSTRAINT [FK_OutgoingSoftPrice_PriceSide]
GO
ALTER TABLE [dbo].[SourceFile]  WITH CHECK ADD  CONSTRAINT [FK_SourceFile_TimeZone] FOREIGN KEY([FileDataTimeZoneID])
REFERENCES [dbo].[TimeZone] ([TimeZoneID])
GO
ALTER TABLE [dbo].[SourceFile] CHECK CONSTRAINT [FK_SourceFile_TimeZone]
GO
ALTER TABLE [dbo].[Tick]  WITH NOCHECK ADD  CONSTRAINT [FK_Tick_DataProvider] FOREIGN KEY([DataProviderID])
REFERENCES [dbo].[DataProvider] ([DataProviderId])
GO
ALTER TABLE [dbo].[Tick] CHECK CONSTRAINT [FK_Tick_DataProvider]
GO
ALTER TABLE [dbo].[Tick]  WITH NOCHECK ADD  CONSTRAINT [FK_Tick_FXPair] FOREIGN KEY([FXPairID])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[Tick] CHECK CONSTRAINT [FK_Tick_FXPair]
GO
ALTER TABLE [dbo].[Tick]  WITH NOCHECK ADD  CONSTRAINT [FK_Tick_LiqProviderStream] FOREIGN KEY([BidStreamProviderId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO
ALTER TABLE [dbo].[Tick] CHECK CONSTRAINT [FK_Tick_LiqProviderStream]
GO
ALTER TABLE [dbo].[Tick]  WITH NOCHECK ADD  CONSTRAINT [FK_Tick_LiqProviderStream1] FOREIGN KEY([OfferStreamProviderId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO
ALTER TABLE [dbo].[Tick] CHECK CONSTRAINT [FK_Tick_LiqProviderStream1]
GO
ALTER TABLE [dbo].[Tick]  WITH NOCHECK ADD  CONSTRAINT [FK_Tick_SourceFile] FOREIGN KEY([SourceFileID])
REFERENCES [dbo].[SourceFile] ([SourceFileID])
GO
ALTER TABLE [dbo].[Tick] CHECK CONSTRAINT [FK_Tick_SourceFile]
GO
ALTER TABLE [dbo].[TradeDecayAnalysis]  WITH NOCHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis] CHECK CONSTRAINT [FK_TradeDecayAnalysis_FxPair]
GO
ALTER TABLE [dbo].[TradeDecayAnalysis]  WITH CHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_LiqProviderStream] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis] CHECK CONSTRAINT [FK_TradeDecayAnalysis_LiqProviderStream]
GO
ALTER TABLE [dbo].[TradeDecayAnalysis]  WITH CHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_PriceSide] FOREIGN KEY([PriceSideId])
REFERENCES [dbo].[PriceSide] ([PriceSideId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis] CHECK CONSTRAINT [FK_TradeDecayAnalysis_PriceSide]
GO
ALTER TABLE [dbo].[TradeDecayAnalysis]  WITH NOCHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_Type] FOREIGN KEY([TradeDecayAnalysisTypeId])
REFERENCES [dbo].[TradeDecayAnalysisType] ([TradeDecayAnalysisTypeId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis] CHECK CONSTRAINT [FK_TradeDecayAnalysis_Type]
GO
ALTER TABLE [dbo].[TradeDecayAnalysis_Pending]  WITH NOCHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_Pending_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis_Pending] CHECK CONSTRAINT [FK_TradeDecayAnalysis_Pending_FxPair]
GO
ALTER TABLE [dbo].[TradeDecayAnalysis_Pending]  WITH CHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_Pending_LiqProviderStream] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis_Pending] CHECK CONSTRAINT [FK_TradeDecayAnalysis_Pending_LiqProviderStream]
GO
ALTER TABLE [dbo].[TradeDecayAnalysis_Pending]  WITH CHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_Pending_PriceSide] FOREIGN KEY([PriceSideId])
REFERENCES [dbo].[PriceSide] ([PriceSideId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis_Pending] CHECK CONSTRAINT [FK_TradeDecayAnalysis_Pending_PriceSide]
GO
ALTER TABLE [dbo].[TradeDecayAnalysis_Pending]  WITH NOCHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_Pending_Type] FOREIGN KEY([TradeDecayAnalysisTypeId])
REFERENCES [dbo].[TradeDecayAnalysisType] ([TradeDecayAnalysisTypeId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis_Pending] CHECK CONSTRAINT [FK_TradeDecayAnalysis_Pending_Type]
GO
ALTER TABLE [dbo].[FxPair]  WITH CHECK ADD  CONSTRAINT [CK_FXPair] CHECK  ((len([FxpCode])=(6)))
GO
ALTER TABLE [dbo].[FxPair] CHECK CONSTRAINT [CK_FXPair]
GO
ALTER TABLE [dbo].[FxPair]  WITH CHECK ADD  CONSTRAINT [CK_FXPair_1] CHECK  ((NOT [FxpCode] like '% %'))
GO
ALTER TABLE [dbo].[FxPair] CHECK CONSTRAINT [CK_FXPair_1]
GO
ALTER TABLE [dbo].[LiquidityProvider]  WITH CHECK ADD  CONSTRAINT [CK_LiquidityProvider] CHECK  ((len([LiqProviderCode])=(3)))
GO
ALTER TABLE [dbo].[LiquidityProvider] CHECK CONSTRAINT [CK_LiquidityProvider]
GO
ALTER TABLE [dbo].[LiquidityProvider]  WITH CHECK ADD  CONSTRAINT [CK_LiquidityProvider_1] CHECK  ((NOT [LiqProviderCode] like '% %'))
GO
ALTER TABLE [dbo].[LiquidityProvider] CHECK CONSTRAINT [CK_LiquidityProvider_1]
GO
EXEC sys.sp_addextendedproperty @name=N'ISO', @value=N'ISO 4217, http://www.iso.org/iso/en/prods-services/popstds/currencycodeslist.html' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FxpCode does not contain spaces' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FxPair', @level2type=N'CONSTRAINT',@level2name=N'CK_FXPair_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enforce fixed value length' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LiquidityProvider', @level2type=N'CONSTRAINT',@level2name=N'CK_LiquidityProvider'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ensure the value does not contain spaces' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LiquidityProvider', @level2type=N'CONSTRAINT',@level2name=N'CK_LiquidityProvider_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Tick"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 262
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SourceFile"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 250
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_dta_mv_0'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'_dta_mv_0'
GO


----------------------------------------------------
--
-- DATE INSERTIONS
---------------------------------------------------

USE [FXtickDB]
GO
SET IDENTITY_INSERT [dbo].[Currency] ON 

GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (4, N'ALL', N'Lek', 8, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (5, N'DZD', N'Algerian Dinar', 12, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (6, N'ARS', N'Argentine Peso', 32, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (7, N'AUD', N'Australian Dollar', 36, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (8, N'BSD', N'Bahamian Dollar', 44, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (9, N'BHD', N'Bahraini Dinar', 48, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (10, N'BDT', N'Taka', 50, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (11, N'AMD', N'Armenian Dram', 51, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (12, N'BBD', N'Barbados Dollar', 52, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (13, N'BMD', N'Bermudian Dollar', 60, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (14, N'BTN', N'Ngultrum', 64, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (15, N'BOB', N'Boliviano', 68, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (16, N'BWP', N'Pula', 72, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (17, N'BZD', N'Belize Dollar', 84, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (18, N'SBD', N'Solomon Islands Dollar', 90, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (19, N'BND', N'Brunei Dollar', 96, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (20, N'MMK', N'Kyat', 104, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (21, N'BIF', N'Burundi Franc', 108, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (22, N'KHR', N'Riel', 116, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (23, N'CAD', N'Canadian Dollar', 124, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (24, N'CVE', N'Cape Verde Escudo', 132, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (25, N'KYD', N'Cayman Islands Dollar', 136, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (26, N'LKR', N'Sri Lanka Rupee', 144, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (27, N'CLP', N'Chilean Peso', 152, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (28, N'CNY', N'Yuan Renminbi', 156, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (29, N'COP', N'Colombian Peso', 170, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (30, N'KMF', N'Comoro Franc', 174, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (31, N'CRC', N'Costa Rican Colon', 188, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (32, N'HRK', N'Croatian Kuna', 191, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (33, N'CUP', N'Cuban Peso', 192, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (34, N'CYP', N'Cyprus Pound', 196, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (35, N'CZK', N'Czech Koruna', 203, 1, CAST(0x000099E100A986CE AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (36, N'DKK', N'Danish Krone', 208, 1, CAST(0x0000A2E400A81740 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (37, N'DOP', N'Dominican Peso', 214, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (38, N'SVC', N'El Salvador Colon', 222, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (39, N'ETB', N'Ethiopian Birr', 230, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (40, N'ERN', N'Nakfa', 232, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (41, N'EEK', N'Kroon', 233, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (42, N'FKP', N'Falkland Islands Pound', 238, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (43, N'FJD', N'Fiji Dollar', 242, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (44, N'DJF', N'Djibouti Franc', 262, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (45, N'GMD', N'Dalasi', 270, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (46, N'GHC', N'Cedi', 288, 0, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (47, N'GIP', N'Gibraltar Pound', 292, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (48, N'GTQ', N'Quetzal', 320, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (49, N'GNF', N'Guinea Franc', 324, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (50, N'GYD', N'Guyana Dollar', 328, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (51, N'HTG', N'Gourde', 332, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (52, N'HNL', N'Lempira', 340, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (53, N'HKD', N'Hong Kong Dollar', 344, 1, CAST(0x0000A2E400A81740 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (54, N'HUF', N'Forint', 348, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (55, N'ISK', N'Iceland Krona', 352, 1, CAST(0x0000A2E400A81740 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (56, N'INR', N'Indian Rupee', 356, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (57, N'IDR', N'Rupiah', 360, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (58, N'IRR', N'Iranian Rial', 364, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (59, N'IQD', N'Iraqi Dinar', 368, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (60, N'ILS', N'New Israeli Sheqel', 376, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (61, N'JMD', N'Jamaican Dollar', 388, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (62, N'JPY', N'Yen', 392, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (63, N'KZT', N'Tenge', 398, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (64, N'JOD', N'Jordanian Dinar', 400, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (65, N'KES', N'Kenyan Shilling', 404, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (66, N'KPW', N'North Korean Won', 408, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (67, N'KRW', N'Won', 410, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (68, N'KWD', N'Kuwaiti Dinar', 414, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (69, N'KGS', N'Som', 417, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (70, N'LAK', N'Kip', 418, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (71, N'LBP', N'Lebanese Pound', 422, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (72, N'LSL', N'Loti', 426, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (73, N'LVL', N'Latvian Lats', 428, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (74, N'LRD', N'Liberian Dollar', 430, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (75, N'LYD', N'Libyan Dinar', 434, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (76, N'LTL', N'Lithuanian Litas', 440, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (77, N'MOP', N'Pataca', 446, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (78, N'MWK', N'Kwacha (Malawi)', 454, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (79, N'MYR', N'Malaysian Ringgit', 458, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (80, N'MVR', N'Rufiyaa', 462, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (81, N'MTL', N'Maltese Lira', 470, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (82, N'MRO', N'Ouguiya', 478, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (83, N'MUR', N'Mauritius Rupee', 480, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (84, N'MXN', N'Mexican Peso', 484, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (85, N'MNT', N'Tugrik', 496, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (86, N'MDL', N'Moldovan Leu', 498, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (87, N'MAD', N'Moroccan Dirham', 504, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (88, N'OMR', N'Rial Omani', 512, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (89, N'NAD', N'Namibian Dollar', 516, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (90, N'NPR', N'Nepalese Rupee', 524, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (91, N'ANG', N'Netherlands Antillian Guilder', 532, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (92, N'AWG', N'Aruban Guilder', 533, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (93, N'VUV', N'Vatu', 548, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (94, N'NZD', N'New Zealand Dollar', 554, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (95, N'NIO', N'Cordoba Oro', 558, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (96, N'NGN', N'Naira', 566, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (97, N'NOK', N'Norwegian Krone', 578, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (98, N'PKR', N'Pakistan Rupee', 586, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (99, N'PAB', N'Balboa', 590, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (100, N'PGK', N'Kina', 598, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (101, N'PYG', N'Guarani', 600, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (102, N'PEN', N'Nuevo Sol', 604, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (103, N'PHP', N'Philippine Peso', 608, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (104, N'GWP', N'Guinea-Bissau Peso', 624, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (105, N'QAR', N'Qatari Rial', 634, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (106, N'ROL', N'Old Leu', 642, 0, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (107, N'RUB', N'Russian Ruble', 643, 1, CAST(0x0000A2E400A81740 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (108, N'RWF', N'Rwanda Franc', 646, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (109, N'SHP', N'Saint Helena Pound', 654, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (110, N'STD', N'Dobra', 678, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (111, N'SAR', N'Saudi Riyal', 682, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (112, N'SCR', N'Seychelles Rupee', 690, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (113, N'SLL', N'Leone', 694, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (114, N'SGD', N'Singapore Dollar', 702, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (115, N'SKK', N'Slovak Koruna', 703, 0, CAST(0x0000A2E400A6B7B0 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (116, N'VND', N'Dong', 704, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (117, N'SIT', N'Tolar', 705, 0, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (118, N'SOS', N'Somali Shilling', 706, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (119, N'ZAR', N'Rand', 710, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (120, N'ZWD', N'Zimbabwe Dollar', 716, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (121, N'SDD', N'Sudanese Dinar', 736, 0, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (122, N'SZL', N'Lilangeni', 748, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (123, N'SEK', N'Swedish Krona', 752, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (124, N'CHF', N'Swiss Franc', 756, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (125, N'SYP', N'Syrian Pound', 760, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (126, N'THB', N'Baht', 764, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (127, N'TOP', N'Pa anga', 776, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (128, N'TTD', N'Trinidad and Tobago Dollar', 780, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (129, N'AED', N'UAE Dirham', 784, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (130, N'TND', N'Tunisian Dinar', 788, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (131, N'TMM', N'Manat', 795, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (132, N'UGX', N'Uganda Shilling', 800, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (133, N'MKD', N'Denar', 807, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (134, N'EGP', N'Egyptian Pound', 818, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (135, N'GBP', N'Pound Sterling', 826, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (136, N'TZS', N'Tanzanian Shilling', 834, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (137, N'USD', N'US Dollar', 840, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (138, N'UYU', N'Peso Uruguayo', 858, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (139, N'UZS', N'Uzbekistan Sum', 860, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (140, N'VEB', N'Bolivar', 862, 0, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (141, N'WST', N'Tala', 882, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (142, N'YER', N'Yemeni Rial', 886, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (143, N'CSD', N'Serbian Dinar (former)', 891, 0, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (144, N'ZMK', N'Kwacha (Zambia)', 894, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (145, N'TWD', N'New Taiwan Dollar', 901, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (146, N'MZN', N'Metical', 943, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (147, N'AZN', N'Azerbaijanian Manat', 944, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (148, N'RON', N'New Leu', 946, 1, CAST(0x000099E100A986CE AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (149, N'CHE', N'WIR Euro', 947, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (150, N'CHW', N'WIR Franc', 948, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (151, N'TRY', N'New Turkish Lira', 949, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (152, N'XAF', N'CFA Franc BEAC', 950, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (153, N'XCD', N'East Caribbean Dollar', 951, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (154, N'XOF', N'CFA Franc BCEAO', 952, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (155, N'XPF', N'CFP Franc', 953, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (156, N'XDR', N'SDR', 960, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (157, N'SRD', N'Surinam Dollar', 968, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (158, N'MGA', N'Malagascy Ariary', 969, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (159, N'COU', N'Unidad de Valor Real', 970, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (160, N'AFN', N'Afghani', 971, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (161, N'TJS', N'Somoni', 972, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (162, N'AOA', N'Kwanza', 973, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (163, N'BYR', N'Belarussian Ruble', 974, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (164, N'BGN', N'Bulgarian Lev', 975, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (165, N'CDF', N'Franc Congolais', 976, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (166, N'BAM', N'Convertible Marks', 977, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (167, N'EUR', N'Euro', 978, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (168, N'MXV', N'Mexican Unidad de Inversion (UID)', 979, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (169, N'UAH', N'Hryvnia', 980, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (170, N'GEL', N'Lari', 981, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (171, N'BOV', N'Mvdol', 984, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (172, N'PLN', N'Zloty', 985, 1, CAST(0x0000A2E400A81740 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (173, N'BRL', N'Brazilian Real', 986, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (174, N'CLF', N'Unidades de formento', 990, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (175, N'USN', N'US Dollar (Next day)', 997, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (176, N'USS', N'US Dollar (Same day)', 998, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (177, N'GHS', N'Ghana Cedi', 936, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (179, N'SDG', N'Sudanese Pound', 938, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (180, N'UYI', N'Uruguay Peso en Unidades Indexadas', 940, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (181, N'VEF', N'Bolivar Fuerte', 937, 1, CAST(0x000099E100A986CE AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (183, N'RSD', N'Serbian Dinar', 941, 1, CAST(0x000099E100DBF1F5 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (184, N'CNH', N'Offshore (Hong Kong) Chinese Renminbi Yuan', NULL, 1, CAST(0x0000A24900F9F060 AS DateTime), 1)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (185, N'XVN', N'VEN', NULL, 1, CAST(0x0000A3F400ECC423 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (186, N'FTS', N'UK 100', NULL, 1, CAST(0x0000A459013B12E8 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (187, N'DJI', N'Wall Street 30', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (188, N'SPX', N'US SPX 500', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (189, N'SPY', N'US SPX 500 (Mini)', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (190, N'NDX', N'US Tech 100', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (191, N'DAX', N'Germany 30', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (192, N'DXY', N'Germany 30 (Mini)', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (193, N'CAC', N'France 40', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (194, N'STX', N'Europe 50', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (195, N'NIK', N'Japan 225', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (196, N'AUS', N'Australia 200', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (197, N'XAU', N'Gold (Spot)', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (198, N'XAG', N'Silver (Spot)', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (199, N'WTI', N'US Crude (Spot)', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (200, N'BRE', N'UK Brent (Spot)', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (201, N'XPT', N'Platinum', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (202, N'XPD', N'Palladium', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES (203, N'GAS', N'US Natural Gas (Spot)', NULL, 1, CAST(0x0000A459013B12E9 AS DateTime), 0)
GO
SET IDENTITY_INSERT [dbo].[Currency] OFF
GO
SET ANSI_PADDING ON
GO
SET IDENTITY_INSERT [dbo].[FxPair] ON 

GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (3, N'EURUSD', 167, 137, 10000, 1.36185, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (4, N'GBPUSD', 135, 137, 10000, 2.05469, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (5, N'AUDJPY', 7, 62, 100, 97.996, 1, 1, CAST(0.001000000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (6, N'AUDUSD', 7, 137, 10000, 0.8205, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (7, N'CHFJPY', 124, 62, 100, 98.155, 1, 1, CAST(0.001000000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (8, N'EURAUD', 167, 7, 10000, 1.65749, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (9, N'EURCHF', 167, 124, 10000, 1.6472, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (10, N'EURGBP', 167, 135, 10000, 0.71245, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (11, N'EURJPY', 167, 62, 100, 162.519, 1, 1, CAST(0.001000000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (12, N'GBPCHF', 135, 124, 10000, 2.4415, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (13, N'GBPJPY', 135, 62, 100, 233.83, 1, 1, CAST(0.001000000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (14, N'NZDUSD', 94, 137, 10000, 0.7022, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (15, N'USDCAD', 137, 23, 10000, 1.0524, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (16, N'USDCHF', 137, 124, 10000, 1.2095, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (17, N'USDJPY', 137, 62, 100, 115.85, 1, 1, CAST(0.001000000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (22, N'NZDJPY', 94, 62, 100, 81.34, 1, 1, CAST(0.001000000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (43, N'CADJPY', 23, 62, 100, 110.13, 1, 1, CAST(0.001000000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (45, N'EURCAD', 167, 23, 10000, 1.4334, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (46, N'EURSEK', 167, 123, 1000, 9.3987, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (47, N'USDDKK', 137, 36, 10000, 5.4934, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (48, N'USDPLN', 137, 172, 10000, 2.8037, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (49, N'USDNOK', 137, 97, 10000, 5.8369, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (50, N'USDCZK', 137, 35, 1000, 20.326, 1, 2, CAST(0.000100000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (51, N'AUDNZD', 7, 94, 10000, 1.1689, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (56, N'EURNOK', 167, 97, 1000, 7.9465, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (57, N'AUDCAD', 7, 23, 10000, 0.8637, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (58, N'AUDCHF', 7, 124, 10000, 0.9928, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (59, N'CADCHF', 23, 124, 10000, 0.8748, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (60, N'EURCZK', 167, 35, 1000, 24.1751, 1, 2, CAST(0.000100000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (61, N'EURHUF', 167, 54, 100, 255.13, 1, 2, CAST(0.001000000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (62, N'EURNZD', 167, 94, 10000, 1.9408, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (63, N'EURPLN', 167, 172, 10000, 3.8184, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (65, N'GBPAUD', 135, 7, 10000, 2.4605, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (66, N'GBPCAD', 135, 23, 10000, 2.1245, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (67, N'USDSEK', 137, 123, 1000, 6.9022, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (68, N'USDMXN', 137, 84, 1000, 11.0239, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (70, N'EURDKK', 167, 36, 1000, 7.4535, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (72, N'USDHUF', 137, 54, 100, 172.62, 1, 2, CAST(0.001000000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (77, N'CADNZD', 23, 94, 10000, 1.1671, NULL, NULL, CAST(0.000001000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (78, N'GBPNZD', 135, 94, 10000, 2.7062, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (79, N'NZDCHF', 94, 124, 10000, 0.7498, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (80, N'AUDDKK', 7, 36, 10000, 5.1756, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (81, N'AUDNOK', 7, 97, 10000, 5.6519, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (82, N'EURTRY', 167, 151, 10000, 2.7286, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (83, N'EURZAR', 167, 119, 1000, 13.0225, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (84, N'NOKSEK', 97, 123, 10000, 1.1588, 1, 1, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (85, N'NZDCAD', 94, 23, 10000, 0.8569, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (86, N'USDHKD', 137, 53, 1000, 7.7515, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (87, N'USDSGD', 137, 114, 10000, 1.52355, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (88, N'USDTRY', 137, 151, 10000, 1.5502, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (89, N'EURMXN', 167, 84, 1000, 17.5139, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (90, N'USDZAR', 137, 119, 1000, 10.09667, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (91, N'AUDSEK', 7, 123, 10000, 5.9941, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (92, N'CADDKK', 23, 36, 10000, 5.3366, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (93, N'CADNOK', 23, 97, 10000, 5.8252, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (95, N'CADSEK', 23, 123, 10000, 6.1791, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (96, N'CHFDKK', 124, 36, 10000, 6.1015, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (97, N'CHFNOK', 124, 97, 1000, 6.6588, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (99, N'CHFSEK', 124, 123, 1000, 7.0631, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (100, N'DKKJPY', 36, 62, 1000, 17.806, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (101, N'DKKNOK', 36, 97, 10000, 1.0912, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (102, N'DKKSEK', 36, 123, 10000, 1.158, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (103, N'GBPDKK', 135, 36, 1000, 8.9256, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (104, N'GBPNOK', 135, 97, 1000, 9.8842, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (105, N'GBPSEK', 135, 123, 1000, 10.3336, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (106, N'HKDJPY', 53, 62, 1000, 12.54476, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (107, N'MXNJPY', 84, 62, 1000, 7.5011, NULL, NULL, CAST(0.000100000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (108, N'NOKJPY', 97, 62, 1000, 16.3136, NULL, NULL, CAST(0.000100000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (109, N'NZDDKK', 94, 36, 10000, 4.5736, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (110, N'NZDNOK', 94, 97, 10000, 4.9905, NULL, NULL, CAST(0.000100000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (111, N'NZDSEK', 94, 123, 10000, 5.2929, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (113, N'AUDSGD', 7, 114, 10000, 1.27044, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (117, N'ZARJPY', 119, 62, 1000, 11.384, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (128, N'NZDSGD', 94, 114, 10000, 1.02531, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (129, N'SGDJPY', 114, 62, 100, 64.629, 1, 2, CAST(0.001000000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (133, N'USDILS', 137, 60, 10000, 3.5037, 1, 2, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (142, N'AUDHKD', 7, 53, 1000, 7.25922, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (143, N'EURHKD', 167, 53, 1000, 10.48485, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (144, N'EURRUB', 167, 107, 1000, 43.3967, NULL, NULL, CAST(0.000100000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (145, N'GBPCZK', 135, 35, 1000, 30.70182, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (146, N'GBPHUF', 135, 54, 100, 355.3187, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (147, N'USDRUB', 137, 107, 1000, 32.4515, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (148, N'GBPPLN', 135, 172, 10000, 5.0486, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (149, N'EURCNH', 167, 184, 1000, 8.2849, NULL, NULL, CAST(0.000100000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (150, N'USDCNH', 137, 184, 10000, 6.1172, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (151, N'EURSGD', 167, 114, 10000, 1.72886, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (152, N'GBPHKD', 135, 53, 1000, 12.7303, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (153, N'GBPMXN', 135, 84, 1000, 21.79734, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (154, N'GBPSGD', 135, 114, 10000, 2.08222, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (155, N'GBPTRY', 135, 151, 10000, 3.63257, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (156, N'GBPZAR', 135, 119, 1000, 18.1361, NULL, NULL, CAST(0.000010000 AS Decimal(18, 9)))
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (157, N'CHFMXN', 124, 84, 1000, 14.9533, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (158, N'USDINR', 137, 56, 100, 61.175, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (159, N'AUDZAR', 7, 119, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (160, N'CADHKD', 23, 53, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (161, N'CADMXN', 23, 84, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (162, N'CADPLN', 23, 172, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (163, N'CADSGD', 23, 114, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (164, N'CADZAR', 23, 119, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (165, N'CZKJPY', 35, 62, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (166, N'DKKHKD', 36, 53, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (167, N'EURILS', 167, 60, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (168, N'GBPILS', 135, 60, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (169, N'HUFJPY', 54, 62, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (170, N'CHFCZK', 124, 35, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (171, N'CHFHKD', 124, 53, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (172, N'CHFHUF', 124, 54, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (173, N'CHFILS', 124, 60, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (174, N'CHFPLN', 124, 172, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (175, N'CHFSGD', 124, 114, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (176, N'CHFTRY', 124, 151, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (177, N'CHFZAR', 124, 119, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (178, N'NOKHKD', 97, 53, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (179, N'NZDHKD', 94, 53, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (180, N'NZDPLN', 94, 172, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (181, N'NZDZAR', 94, 119, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (182, N'PLNHUF', 172, 54, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (183, N'PLNJPY', 172, 62, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (184, N'SEKHKD', 123, 53, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (185, N'SEKJPY', 123, 62, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (186, N'SGDDKK', 114, 36, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (187, N'SGDHKD', 114, 53, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (188, N'SGDMXN', 114, 84, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (189, N'SGDNOK', 114, 97, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (190, N'SGDSEK', 114, 123, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (191, N'TRYJPY', 151, 62, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (192, N'USDGHS', 137, 177, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (193, N'USDKES', 137, 65, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (194, N'USDNGN', 137, 96, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (195, N'USDRON', 137, 148, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (196, N'USDUGX', 137, 132, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (197, N'USDZMK', 137, 144, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (198, N'ZARMXN', 119, 84, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (199, N'USDTHB', 137, 126, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (200, N'USDXVN', 137, 185, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (201, N'FTSGBP', 186, 135, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (202, N'DJIUSD', 187, 137, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (203, N'SPXUSD', 188, 137, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (204, N'SPYUSD', 189, 137, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (205, N'NDXUSD', 190, 137, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (206, N'DAXEUR', 191, 167, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (207, N'DXYEUR', 192, 167, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (208, N'CACEUR', 193, 167, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (209, N'STXEUR', 194, 167, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (210, N'NIKJPY', 195, 62, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (211, N'AUSAUD', 196, 7, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (212, N'XAUUSD', 197, 137, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (213, N'XAGUSD', 198, 137, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (214, N'XAUAUD', 197, 7, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (215, N'XAGAUD', 198, 7, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (216, N'WTIUSD', 199, 137, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (217, N'BREUSD', 200, 137, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (218, N'XPTUSD', 201, 137, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (219, N'XPDUSD', 202, 137, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (220, N'XAUEUR', 197, 167, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage], [MinimumPriceGranularity]) VALUES (221, N'GASUSD', 203, 137, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[FxPair] OFF
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (1, 62, N'BOA')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (2, 65, N'CTI')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (3, 7, N'RBS')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (4, 6, N'JPM')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (5, 68, N'GLS')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (6, 67, N'DBK')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (7, 13, N'UBS')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (8, 66, N'CZB')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (10, 71, N'MGS')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (12, 72, N'NOM')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (13, 64, N'CRS')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (14, 61, N'BNP')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (15, 63, N'BRX')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (17, 74, N'SOC')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (19, 69, N'HSB')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (20, 75, N'HTA')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (21, 76, N'HTF')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (22, 79, N'FA1')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (23, 80, N'L01')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (24, 81, N'LM2')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (25, 82, N'FC1')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (26, 83, N'FC2')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (27, 84, N'LM3')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (28, 85, N'HT3')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (29, 86, N'L02')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (30, 87, N'L03')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (31, 88, N'L04')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (32, 89, N'L05')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (33, 90, N'L06')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (34, 91, N'L07')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (35, 92, N'L08')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (36, 93, N'L09')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (37, 94, N'L10')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (38, 95, N'L11')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (39, 96, N'L12')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (40, 97, N'L13')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (41, 98, N'L14')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (42, 99, N'L15')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (43, 100, N'L16')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (44, 101, N'L17')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (45, 102, N'L18')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (46, 103, N'L19')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (47, 104, N'L20')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (48, 105, N'FS1')
GO
INSERT [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode]) VALUES (49, 106, N'FS2')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (4, 4, N'ALL')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (5, 5, N'DZD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (6, 6, N'ARS')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (7, 7, N'AUD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (8, 8, N'BSD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (9, 9, N'BHD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (10, 10, N'BDT')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (11, 11, N'AMD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (12, 12, N'BBD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (13, 13, N'BMD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (14, 14, N'BTN')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (15, 15, N'BOB')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (16, 16, N'BWP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (17, 17, N'BZD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (18, 18, N'SBD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (19, 19, N'BND')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (20, 20, N'MMK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (21, 21, N'BIF')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (22, 22, N'KHR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (23, 23, N'CAD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (24, 24, N'CVE')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (25, 25, N'KYD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (26, 26, N'LKR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (27, 27, N'CLP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (28, 28, N'CNY')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (29, 29, N'COP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (30, 30, N'KMF')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (31, 31, N'CRC')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (32, 32, N'HRK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (33, 33, N'CUP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (34, 34, N'CYP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (35, 35, N'CZK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (36, 36, N'DKK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (37, 37, N'DOP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (38, 38, N'SVC')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (39, 39, N'ETB')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (40, 40, N'ERN')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (41, 41, N'EEK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (42, 42, N'FKP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (43, 43, N'FJD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (44, 44, N'DJF')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (45, 45, N'GMD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (46, 46, N'GHC')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (47, 47, N'GIP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (48, 48, N'GTQ')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (49, 49, N'GNF')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (50, 50, N'GYD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (51, 51, N'HTG')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (52, 52, N'HNL')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (53, 53, N'HKD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (54, 54, N'HUF')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (55, 55, N'ISK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (56, 56, N'INR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (57, 57, N'IDR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (58, 58, N'IRR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (59, 59, N'IQD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (60, 60, N'ILS')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (61, 61, N'JMD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (62, 62, N'JPY')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (63, 63, N'KZT')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (64, 64, N'JOD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (65, 65, N'KES')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (66, 66, N'KPW')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (67, 67, N'KRW')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (68, 68, N'KWD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (69, 69, N'KGS')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (70, 70, N'LAK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (71, 71, N'LBP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (72, 72, N'LSL')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (73, 73, N'LVL')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (74, 74, N'LRD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (75, 75, N'LYD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (76, 76, N'LTL')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (77, 77, N'MOP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (78, 78, N'MWK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (79, 79, N'MYR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (80, 80, N'MVR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (81, 81, N'MTL')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (82, 82, N'MRO')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (83, 83, N'MUR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (84, 84, N'MXN')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (85, 85, N'MNT')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (86, 86, N'MDL')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (87, 87, N'MAD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (88, 88, N'OMR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (89, 89, N'NAD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (90, 90, N'NPR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (91, 91, N'ANG')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (92, 92, N'AWG')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (93, 93, N'VUV')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (94, 94, N'NZD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (95, 95, N'NIO')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (96, 96, N'NGN')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (97, 97, N'NOK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (98, 98, N'PKR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (99, 99, N'PAB')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (100, 100, N'PGK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (101, 101, N'PYG')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (102, 102, N'PEN')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (103, 103, N'PHP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (104, 104, N'GWP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (105, 105, N'QAR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (106, 106, N'ROL')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (107, 107, N'RUB')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (108, 108, N'RWF')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (109, 109, N'SHP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (110, 110, N'STD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (111, 111, N'SAR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (112, 112, N'SCR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (113, 113, N'SLL')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (114, 114, N'SGD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (115, 115, N'SKK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (116, 116, N'VND')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (117, 117, N'SIT')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (118, 118, N'SOS')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (119, 119, N'ZAR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (120, 120, N'ZWD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (121, 121, N'SDD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (122, 122, N'SZL')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (123, 123, N'SEK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (124, 124, N'CHF')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (125, 125, N'SYP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (126, 126, N'THB')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (127, 127, N'TOP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (128, 128, N'TTD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (129, 129, N'AED')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (130, 130, N'TND')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (131, 131, N'TMM')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (132, 132, N'UGX')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (133, 133, N'MKD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (134, 134, N'EGP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (135, 135, N'GBP')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (136, 136, N'TZS')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (137, 137, N'USD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (138, 138, N'UYU')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (139, 139, N'UZS')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (140, 140, N'VEB')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (141, 141, N'WST')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (142, 142, N'YER')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (143, 143, N'CSD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (144, 144, N'ZMK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (145, 145, N'TWD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (146, 146, N'MZN')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (147, 147, N'AZN')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (148, 148, N'RON')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (149, 149, N'CHE')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (150, 150, N'CHW')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (151, 151, N'TRY')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (152, 152, N'XAF')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (153, 153, N'XCD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (154, 154, N'XOF')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (155, 155, N'XPF')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (156, 156, N'XDR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (157, 157, N'SRD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (158, 158, N'MGA')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (159, 159, N'COU')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (160, 160, N'AFN')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (161, 161, N'TJS')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (162, 162, N'AOA')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (163, 163, N'BYR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (164, 164, N'BGN')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (165, 165, N'CDF')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (166, 166, N'BAM')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (167, 167, N'EUR')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (168, 168, N'MXV')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (169, 169, N'UAH')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (170, 170, N'GEL')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (171, 171, N'BOV')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (172, 172, N'PLN')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (173, 173, N'BRL')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (174, 174, N'CLF')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (175, 175, N'USN')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (176, 176, N'USS')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (177, 177, N'GHS')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (179, 179, N'SDG')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (180, 180, N'UYI')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (181, 181, N'VEF')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (183, 183, N'RSD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (184, 184, N'CNH')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (186, 185, N'XVN')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (187, 186, N'FTS')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (188, 187, N'DJI')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (189, 188, N'SPX')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (190, 189, N'SPY')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (191, 190, N'NDX')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (192, 191, N'DAX')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (193, 192, N'DXY')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (194, 193, N'CAC')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (195, 194, N'STX')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (196, 195, N'NIK')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (197, 196, N'AUS')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (198, 197, N'XAU')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (199, 198, N'XAG')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (200, 199, N'WTI')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (201, 200, N'BRE')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (202, 201, N'XPT')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (203, 202, N'XPD')
GO
INSERT [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode]) VALUES (204, 203, N'GAS')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (0, 57, N'AUDCAD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (1, 58, N'AUDCHF')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (2, 5, N'AUDJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (3, 51, N'AUDNZD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (4, 113, N'AUDSGD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (5, 6, N'AUDUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (6, 43, N'CADJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (7, 7, N'CHFJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (8, 8, N'EURAUD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (9, 45, N'EURCAD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (10, 9, N'EURCHF')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (11, 60, N'EURCZK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (12, 70, N'EURDKK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (13, 10, N'EURGBP')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (14, 61, N'EURHUF')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (15, 11, N'EURJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (16, 89, N'EURMXN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (17, 56, N'EURNOK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (18, 62, N'EURNZD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (19, 63, N'EURPLN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (20, 46, N'EURSEK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (21, 3, N'EURUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (22, 83, N'EURZAR')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (23, 65, N'GBPAUD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (24, 66, N'GBPCAD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (25, 12, N'GBPCHF')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (26, 13, N'GBPJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (27, 104, N'GBPNOK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (28, 78, N'GBPNZD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (29, 4, N'GBPUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (30, 106, N'HKDJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (31, 84, N'NOKSEK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (32, 22, N'NZDJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (33, 128, N'NZDSGD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (34, 14, N'NZDUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (35, 129, N'SGDJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (36, 15, N'USDCAD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (37, 16, N'USDCHF')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (38, 50, N'USDCZK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (39, 86, N'USDHKD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (40, 72, N'USDHUF')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (41, 133, N'USDILS')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (42, 17, N'USDJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (43, 68, N'USDMXN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (44, 49, N'USDNOK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (45, 48, N'USDPLN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (46, 67, N'USDSEK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (47, 87, N'USDSGD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (48, 88, N'USDTRY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (49, 90, N'USDZAR')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (50, 117, N'ZARJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (51, 47, N'USDDKK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (52, 59, N'CADCHF')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (55, 77, N'CADNZD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (56, 79, N'NZDCHF')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (57, 80, N'AUDDKK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (58, 81, N'AUDNOK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (59, 82, N'EURTRY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (60, 85, N'NZDCAD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (61, 91, N'AUDSEK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (62, 92, N'CADDKK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (63, 93, N'CADNOK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (64, 95, N'CADSEK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (65, 96, N'CHFDKK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (66, 97, N'CHFNOK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (67, 99, N'CHFSEK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (68, 100, N'DKKJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (69, 101, N'DKKNOK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (70, 102, N'DKKSEK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (71, 103, N'GBPDKK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (72, 105, N'GBPSEK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (73, 107, N'MXNJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (74, 108, N'NOKJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (75, 109, N'NZDDKK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (76, 110, N'NZDNOK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (77, 111, N'NZDSEK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (78, 142, N'AUDHKD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (79, 143, N'EURHKD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (80, 144, N'EURRUB')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (81, 145, N'GBPCZK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (82, 146, N'GBPHUF')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (83, 147, N'USDRUB')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (84, 148, N'GBPPLN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (85, 149, N'EURCNH')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (86, 150, N'USDCNH')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (87, 151, N'EURSGD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (88, 152, N'GBPHKD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (89, 153, N'GBPMXN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (90, 154, N'GBPSGD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (91, 155, N'GBPTRY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (92, 156, N'GBPZAR')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (93, 157, N'CHFMXN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (94, 158, N'USDINR')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (95, 159, N'AUDZAR')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (96, 160, N'CADHKD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (97, 161, N'CADMXN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (98, 162, N'CADPLN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (99, 163, N'CADSGD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (100, 164, N'CADZAR')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (101, 165, N'CZKJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (102, 166, N'DKKHKD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (103, 167, N'EURILS')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (104, 168, N'GBPILS')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (105, 169, N'HUFJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (106, 170, N'CHFCZK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (107, 171, N'CHFHKD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (108, 172, N'CHFHUF')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (109, 173, N'CHFILS')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (110, 174, N'CHFPLN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (111, 175, N'CHFSGD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (112, 176, N'CHFTRY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (113, 177, N'CHFZAR')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (114, 178, N'NOKHKD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (115, 179, N'NZDHKD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (116, 180, N'NZDPLN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (117, 181, N'NZDZAR')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (118, 182, N'PLNHUF')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (119, 183, N'PLNJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (120, 184, N'SEKHKD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (121, 185, N'SEKJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (122, 186, N'SGDDKK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (123, 187, N'SGDHKD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (124, 188, N'SGDMXN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (125, 189, N'SGDNOK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (126, 190, N'SGDSEK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (127, 191, N'TRYJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (128, 192, N'USDGHS')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (129, 193, N'USDKES')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (130, 194, N'USDNGN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (131, 195, N'USDRON')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (132, 196, N'USDUGX')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (133, 197, N'USDZMK')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (134, 198, N'ZARMXN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (135, 199, N'USDTHB')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (136, 200, N'USDXVN')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (137, 201, N'FTSGBP')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (138, 202, N'DJIUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (139, 203, N'SPXUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (140, 204, N'SPYUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (141, 205, N'NDXUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (142, 206, N'DAXEUR')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (143, 207, N'DXYEUR')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (144, 208, N'CACEUR')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (145, 209, N'STXEUR')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (146, 210, N'NIKJPY')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (147, 211, N'AUSAUD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (148, 212, N'XAUUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (149, 213, N'XAGUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (150, 214, N'XAUAUD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (151, 215, N'XAGAUD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (152, 216, N'WTIUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (153, 217, N'BREUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (154, 218, N'XPTUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (155, 219, N'XPDUSD')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (156, 220, N'XAUEUR')
GO
INSERT [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash]) VALUES (157, 221, N'GASUSD')
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (0, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (10, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (11, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (12, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (13, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (14, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (15, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (16, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (17, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (18, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (19, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (20, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (21, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (22, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (23, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (24, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (25, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (26, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (27, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (28, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (29, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (30, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (31, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (32, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (33, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (34, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (35, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (36, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (37, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (38, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (39, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (40, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (41, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (42, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (43, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (44, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (45, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (46, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (47, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (48, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (49, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (50, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (51, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (52, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (53, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (54, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (55, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (56, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (57, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (58, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (59, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (60, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (61, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (62, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (63, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (64, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (65, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (66, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (67, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (68, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (69, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (70, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (71, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (72, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (73, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (74, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (75, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (76, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (77, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (78, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (79, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (80, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (81, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (82, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (83, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (84, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (85, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (86, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (87, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (88, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (89, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (90, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (91, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (92, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (93, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (94, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (95, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (96, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (97, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (98, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (99, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (100, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (101, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (102, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (103, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (104, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (105, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (106, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (107, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (108, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (109, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (110, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (111, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (112, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (113, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (114, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (115, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (116, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (117, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (118, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (119, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (120, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (121, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (122, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (123, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (124, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (125, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (126, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (127, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (128, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (129, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (130, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (131, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (132, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (133, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (134, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (135, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (136, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (137, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (138, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (139, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (140, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (141, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (142, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (143, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (144, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (145, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (146, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (147, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (148, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (149, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (150, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (151, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (152, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (153, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (154, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (155, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (156, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (157, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (158, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (159, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (160, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (161, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (162, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (163, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (164, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (165, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (166, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (167, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (168, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (169, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (170, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (171, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (172, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (173, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (174, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (175, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (176, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (177, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (178, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (179, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (180, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (181, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (182, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (183, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (184, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (185, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (186, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (187, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (188, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (189, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (190, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (191, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (192, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (193, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (194, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (195, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (196, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (197, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (198, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (199, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (200, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (201, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (202, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (203, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (204, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (205, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (206, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (207, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (208, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (209, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (210, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (211, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (212, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (213, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (214, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (215, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (216, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (217, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (218, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (219, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (220, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (221, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (222, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (223, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (224, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (225, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (226, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (227, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (228, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (229, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (230, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (231, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (232, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (233, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (234, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (235, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (236, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (237, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (238, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (239, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (240, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (241, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (242, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (243, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (244, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (245, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (246, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (247, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (248, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (249, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (250, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (251, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (252, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (253, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (254, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (255, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (256, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (257, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (258, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (259, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (260, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (261, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (262, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (263, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (264, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (265, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (266, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (267, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (268, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (269, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (270, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (271, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (272, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (273, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (274, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (275, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (276, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (277, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (278, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (279, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (280, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (281, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (282, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (283, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (284, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (285, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (286, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (287, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (288, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (289, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (290, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (291, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (292, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (293, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (294, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (295, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (296, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (297, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (298, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (299, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (300, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (310, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (320, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (330, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (340, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (350, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (360, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (370, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (380, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (390, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (400, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (410, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (420, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (430, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (440, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (450, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (460, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (470, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (480, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (490, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (500, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (510, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (520, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (530, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (540, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (550, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (560, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (570, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (580, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (590, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (600, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (610, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (620, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (630, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (640, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (650, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (660, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (670, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (680, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (690, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (700, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (710, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (720, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (730, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (740, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (750, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (760, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (770, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (780, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (790, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (800, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (810, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (820, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (830, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (840, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (850, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (860, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (870, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (880, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (890, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (900, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (910, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (920, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (930, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (940, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (950, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (960, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (970, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (980, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (990, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1100, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1200, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1300, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1400, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1500, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1600, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1700, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1800, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1900, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2100, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2200, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2300, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2400, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2500, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2600, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2700, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2800, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2900, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3100, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3200, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3300, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3400, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3500, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3600, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3700, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3800, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3900, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4100, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4200, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4300, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4400, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4500, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4600, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4700, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4800, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4900, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5100, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5200, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5300, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5400, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5500, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5600, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5700, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5800, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5900, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6100, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6200, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6300, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6400, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6500, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6600, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6700, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6800, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6900, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7100, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7200, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7300, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7400, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7500, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7600, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7700, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7800, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7900, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8100, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8200, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8300, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8400, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8500, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8600, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8700, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8800, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8900, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9100, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9200, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9300, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9400, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9500, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9600, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9700, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9800, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9900, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (10000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (11000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (12000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (13000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (14000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (15000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (16000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (17000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (18000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (19000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (20000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (21000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (22000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (23000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (24000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (25000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (26000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (27000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (28000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (29000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (30000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (31000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (32000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (33000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (34000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (35000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (36000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (37000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (38000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (39000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (40000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (41000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (42000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (43000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (44000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (45000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (46000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (47000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (48000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (49000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (50000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (51000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (52000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (53000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (54000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (55000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (56000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (57000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (58000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (59000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (60000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (61000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (62000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (63000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (64000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (65000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (66000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (67000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (68000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (69000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (70000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (71000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (72000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (73000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (74000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (75000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (76000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (77000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (78000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (79000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (80000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (81000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (82000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (83000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (84000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (85000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (86000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (87000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (88000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (89000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (90000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (91000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (92000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (93000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (94000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (95000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (96000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (97000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (98000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (99000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (100000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (101000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (102000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (103000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (104000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (105000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (106000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (107000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (108000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (109000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (110000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (111000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (112000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (113000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (114000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (115000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (116000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (117000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (118000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (119000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (120000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (121000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (122000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (123000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (124000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (125000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (126000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (127000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (128000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (129000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (130000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (131000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (132000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (133000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (134000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (135000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (136000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (137000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (138000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (139000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (140000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (141000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (142000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (143000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (144000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (145000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (146000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (147000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (148000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (149000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (150000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (151000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (152000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (153000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (154000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (155000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (156000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (157000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (158000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (159000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (160000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (161000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (162000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (163000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (164000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (165000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (166000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (167000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (168000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (169000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (170000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (171000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (172000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (173000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (174000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (175000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (176000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (177000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (178000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (179000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (180000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (181000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (182000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (183000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (184000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (185000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (186000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (187000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (188000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (189000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (190000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (191000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (192000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (193000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (194000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (195000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (196000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (197000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (198000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (199000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (200000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (201000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (202000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (203000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (204000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (205000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (206000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (207000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (208000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (209000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (210000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (211000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (212000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (213000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (214000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (215000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (216000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (217000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (218000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (219000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (220000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (221000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (222000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (223000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (224000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (225000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (226000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (227000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (228000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (229000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (230000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (231000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (232000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (233000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (234000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (235000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (236000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (237000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (238000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (239000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (240000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (241000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (242000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (243000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (244000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (245000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (246000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (247000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (248000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (249000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (250000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (251000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (252000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (253000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (254000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (255000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (256000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (257000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (258000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (259000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (260000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (261000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (262000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (263000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (264000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (265000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (266000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (267000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (268000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (269000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (270000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (271000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (272000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (273000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (274000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (275000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (276000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (277000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (278000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (279000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (280000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (281000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (282000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (283000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (284000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (285000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (286000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (287000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (288000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (289000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (290000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (291000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (292000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (293000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (294000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (295000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (296000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (297000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (298000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (299000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (300000, 0)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (0, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (10, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (11, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (12, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (13, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (14, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (15, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (16, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (17, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (18, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (19, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (20, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (21, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (22, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (23, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (24, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (25, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (26, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (27, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (28, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (29, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (30, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (31, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (32, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (33, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (34, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (35, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (36, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (37, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (38, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (39, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (40, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (41, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (42, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (43, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (44, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (45, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (46, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (47, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (48, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (49, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (50, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (51, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (52, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (53, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (54, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (55, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (56, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (57, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (58, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (59, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (60, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (61, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (62, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (63, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (64, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (65, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (66, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (67, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (68, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (69, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (70, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (71, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (72, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (73, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (74, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (75, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (76, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (77, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (78, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (79, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (80, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (81, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (82, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (83, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (84, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (85, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (86, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (87, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (88, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (89, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (90, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (91, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (92, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (93, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (94, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (95, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (96, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (97, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (98, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (99, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (100, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (101, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (102, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (103, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (104, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (105, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (106, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (107, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (108, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (109, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (110, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (111, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (112, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (113, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (114, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (115, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (116, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (117, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (118, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (119, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (120, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (121, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (122, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (123, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (124, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (125, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (126, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (127, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (128, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (129, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (130, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (131, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (132, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (133, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (134, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (135, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (136, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (137, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (138, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (139, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (140, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (141, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (142, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (143, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (144, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (145, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (146, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (147, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (148, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (149, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (150, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (151, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (152, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (153, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (154, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (155, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (156, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (157, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (158, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (159, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (160, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (161, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (162, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (163, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (164, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (165, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (166, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (167, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (168, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (169, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (170, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (171, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (172, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (173, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (174, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (175, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (176, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (177, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (178, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (179, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (180, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (181, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (182, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (183, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (184, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (185, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (186, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (187, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (188, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (189, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (190, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (191, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (192, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (193, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (194, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (195, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (196, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (197, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (198, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (199, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (200, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (201, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (202, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (203, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (204, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (205, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (206, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (207, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (208, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (209, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (210, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (211, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (212, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (213, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (214, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (215, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (216, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (217, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (218, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (219, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (220, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (221, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (222, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (223, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (224, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (225, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (226, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (227, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (228, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (229, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (230, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (231, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (232, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (233, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (234, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (235, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (236, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (237, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (238, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (239, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (240, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (241, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (242, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (243, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (244, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (245, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (246, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (247, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (248, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (249, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (250, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (251, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (252, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (253, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (254, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (255, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (256, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (257, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (258, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (259, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (260, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (261, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (262, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (263, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (264, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (265, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (266, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (267, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (268, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (269, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (270, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (271, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (272, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (273, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (274, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (275, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (276, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (277, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (278, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (279, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (280, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (281, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (282, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (283, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (284, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (285, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (286, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (287, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (288, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (289, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (290, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (291, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (292, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (293, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (294, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (295, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (296, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (297, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (298, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (299, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (300, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (310, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (320, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (330, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (340, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (350, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (360, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (370, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (380, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (390, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (400, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (410, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (420, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (430, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (440, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (450, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (460, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (470, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (480, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (490, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (500, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (510, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (520, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (530, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (540, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (550, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (560, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (570, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (580, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (590, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (600, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (610, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (620, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (630, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (640, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (650, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (660, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (670, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (680, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (690, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (700, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (710, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (720, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (730, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (740, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (750, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (760, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (770, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (780, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (790, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (800, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (810, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (820, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (830, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (840, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (850, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (860, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (870, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (880, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (890, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (900, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (910, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (920, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (930, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (940, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (950, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (960, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (970, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (980, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (990, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1100, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1200, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1300, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1400, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1500, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1600, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1700, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1800, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (1900, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2100, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2200, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2300, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2400, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2500, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2600, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2700, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2800, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (2900, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3100, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3200, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3300, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3400, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3500, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3600, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3700, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3800, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (3900, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4100, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4200, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4300, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4400, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4500, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4600, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4700, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4800, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (4900, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5100, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5200, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5300, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5400, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5500, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5600, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5700, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5800, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (5900, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6100, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6200, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6300, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6400, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6500, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6600, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6700, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6800, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (6900, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7100, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7200, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7300, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7400, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7500, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7600, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7700, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7800, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (7900, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8100, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8200, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8300, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8400, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8500, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8600, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8700, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8800, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (8900, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9100, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9200, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9300, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9400, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9500, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9600, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9700, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9800, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (9900, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (10000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (11000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (12000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (13000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (14000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (15000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (16000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (17000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (18000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (19000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (20000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (21000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (22000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (23000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (24000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (25000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (26000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (27000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (28000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (29000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (30000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (31000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (32000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (33000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (34000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (35000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (36000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (37000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (38000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (39000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (40000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (41000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (42000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (43000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (44000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (45000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (46000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (47000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (48000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (49000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (50000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (51000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (52000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (53000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (54000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (55000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (56000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (57000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (58000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (59000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (60000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (61000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (62000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (63000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (64000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (65000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (66000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (67000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (68000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (69000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (70000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (71000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (72000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (73000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (74000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (75000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (76000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (77000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (78000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (79000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (80000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (81000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (82000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (83000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (84000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (85000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (86000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (87000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (88000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (89000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (90000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (91000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (92000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (93000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (94000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (95000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (96000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (97000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (98000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (99000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (100000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (101000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (102000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (103000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (104000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (105000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (106000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (107000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (108000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (109000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (110000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (111000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (112000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (113000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (114000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (115000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (116000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (117000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (118000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (119000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (120000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (121000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (122000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (123000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (124000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (125000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (126000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (127000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (128000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (129000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (130000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (131000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (132000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (133000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (134000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (135000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (136000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (137000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (138000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (139000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (140000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (141000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (142000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (143000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (144000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (145000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (146000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (147000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (148000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (149000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (150000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (151000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (152000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (153000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (154000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (155000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (156000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (157000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (158000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (159000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (160000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (161000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (162000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (163000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (164000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (165000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (166000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (167000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (168000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (169000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (170000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (171000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (172000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (173000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (174000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (175000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (176000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (177000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (178000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (179000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (180000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (181000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (182000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (183000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (184000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (185000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (186000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (187000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (188000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (189000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (190000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (191000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (192000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (193000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (194000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (195000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (196000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (197000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (198000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (199000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (200000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (201000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (202000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (203000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (204000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (205000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (206000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (207000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (208000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (209000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (210000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (211000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (212000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (213000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (214000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (215000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (216000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (217000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (218000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (219000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (220000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (221000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (222000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (223000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (224000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (225000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (226000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (227000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (228000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (229000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (230000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (231000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (232000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (233000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (234000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (235000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (236000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (237000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (238000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (239000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (240000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (241000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (242000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (243000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (244000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (245000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (246000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (247000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (248000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (249000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (250000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (251000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (252000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (253000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (254000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (255000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (256000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (257000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (258000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (259000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (260000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (261000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (262000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (263000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (264000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (265000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (266000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (267000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (268000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (269000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (270000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (271000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (272000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (273000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (274000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (275000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (276000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (277000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (278000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (279000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (280000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (281000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (282000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (283000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (284000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (285000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (286000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (287000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (288000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (289000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (290000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (291000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (292000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (293000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (294000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (295000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (296000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (297000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (298000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (299000, 1)
GO
INSERT [dbo].[_Internal_TradeDecayIntervals] ([MsDistanceFromEventStart], [SideId]) VALUES (300000, 1)
GO
INSERT [dbo].[PriceSide] ([PriceSideId], [PriceSideName]) VALUES (0, N'Bid')
GO
INSERT [dbo].[PriceSide] ([PriceSideId], [PriceSideName]) VALUES (1, N'Ask')
GO
INSERT [dbo].[TradeSide] ([TradeSideId], [TradeSide]) VALUES (0, N'SellGiven')
GO
INSERT [dbo].[TradeSide] ([TradeSideId], [TradeSide]) VALUES (1, N'BuyPaid')
GO
INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (0, N'BankPool', N'BankPool')
GO
INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (1, N'Hotspot', N'Hotspot')
GO
INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (2, N'FXall', N'FXall')
GO
INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (3, N'LMAX', N'LMAX')
GO
INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (4, N'FXCM', N'FXCM')
GO
INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (5, N'FXCMMM', N'FXCMMM')
GO
