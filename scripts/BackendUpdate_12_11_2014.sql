ALTER PROCEDURE [dbo].[GetTradingSystemsDailyStats_SP]
AS
BEGIN
	SELECT
		--BEWARE! There can be multiple symbols for same system id (this is especially true for system -1)
		systemStats.SymbolId AS SymbolId,
		systemStats.TradingSystemId AS TradingSystemId,
		COALESCE(systemStats.NopBasePol, 0) AS NopBasePol,
		COALESCE(systemStats.VolumeInUsd/1000000, 0) AS VolumeUsdM,
		COALESCE(systemStats.DealsNum, 0) as DealsNum,
		COALESCE(systemStats.RejectionsNum, 0) as RejectionsNum,
		COALESCE(systemStats.PnlTermPolInUsd/(systemStats.VolumeInUsd/1000000), 0) AS PnlGrossUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd, 0) AS PnlGrossUsd,
		COALESCE(systemStats.CommissionInUSD/(systemStats.VolumeInUsd/1000000), 0) AS CommissionsUsdPerMUsd,
		COALESCE(systemStats.CommissionInUSD, 0) AS CommissionsUsd,
		COALESCE((systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD)/(systemStats.VolumeInUsd/1000000), 0) AS PnlNetUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD, 0) AS PnlNetUsd
	FROM
		(
		SELECT
			Symbol.Id AS SymbolId,
			systemsStats.TradingSystemId AS TradingSystemId,
			systemsStats.NopBasePol AS NopBasePol,
			systemsStats.DealsNum as DealsNum,
			systemsStats.RejectionsNum as RejectionsNum,
			systemsStats.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd,
			--PnL = term position + yield of immediate close of outstanding base position
			--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
			(systemsStats.NopTermPol + (CASE WHEN systemsStats.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * systemsStats.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
			systemsStats.CommissionInCCY1 * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionInUSD
		FROM
			(
			SELECT
				COALESCE(systemsPositions.[TradingSystemId], systemRejections.[TradingSystemId]) AS [TradingSystemId],
				COALESCE(systemsPositions.[SymbolId], systemRejections.[SymbolId]) AS [SymbolId],
				NopBasePol,
				NopTermPol,
				VolumeInCcy1,
				CommissionInCCY1,
				DealsNum,
				RejectionsNum
			FROM	
				(
				--allways include stats fro unknown systems
				SELECT 
					-1 AS [TradingSystemId], 
					0 AS [SymbolId],
					NULL AS NopBasePol, 
					NULL AS NopTermPol, 
					NULL AS VolumeInCcy1, 
					NULL AS CommissionInCCY1, 
					NULL AS DealsNum
				UNION ALL
				SELECT 
					[TradingSystemId],
					[SymbolId],
					SUM([AmountBasePolExecuted]) AS NopBasePol,
					SUM(-[AmountBasePolExecuted]* [Price]) AS NopTermPol,
					SUM(ABS([AmountBasePolExecuted])) AS VolumeInCcy1,
					SUM(ABS([AmountBasePolExecuted]) * CommissionPricePerMillion / 1000000) AS CommissionInCCY1,
					COUNT([TradingSystemId]) AS DealsNum
				FROM
					[dbo].[TradingSystemDealsExecuted_Daily] deal
					INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
				GROUP BY
					[TradingSystemId], [SymbolId]
				) systemsPositions
				FULL OUTER JOIN
				(
					SELECT 
						[TradingSystemId],
						[SymbolId],
						COUNT([TradingSystemId]) AS RejectionsNum
					FROM
						[dbo].[TradingSystemDealsRejected_Daily] rejects
					GROUP BY
						[TradingSystemId], [SymbolId]
				)  systemRejections ON systemsPositions.TradingSystemId = systemRejections.TradingSystemId AND systemsPositions.[SymbolId] = systemRejections.[SymbolId]
			) systemsStats
			INNER JOIN [dbo].[Symbol] symbol ON systemsStats.SymbolId = symbol.Id
			INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
			INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
			) systemStats
END
GO


ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 2),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL,
	@SingleFillSystemId int = NULL,
	@MultipleFillsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @MultipleFillsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleFillsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external deal for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity])
			VALUES
				(ISNULL(@SingleFillSystemId, -1)
				,@AmountBasePolExecuted
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId)
		END
		
		
		INSERT INTO 
		[dbo].[DealExternalExecuted]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolExecuted]
			   ,[AmountTermPolExecutedInUsd]
			   ,[Price]
			   ,[CounterpartyTransactionId]
			   ,[CounterpartySuppliedExecutionTimeStampUtc]
			   ,[ValueDateCounterpartySuppliedLocMktDate]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[InternalTransactionIdentity]
			   ,[FlowSideId])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolExecuted
			   ,@AmountTermPolExecutedInUsd
			   ,@Price
			   ,@CounterpartyTransactionId
			   ,@CounterpartyExecutionReportTimeStampUtc
			   ,@ValueDateCounterpartySuppliedLocMktDate
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@CounterpartySentExecutionReportUtc
			   ,@InternalDealId
			   ,@FlowSideId)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 2),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@RejectedPrice decimal(18, 9),
	@SingleRejectionSystemId int = NULL,
	@MultipleRejectionsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END

	BEGIN TRY
	
		BEGIN TRANSACTION;
		
		IF @MultipleRejectionsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@RejectedPrice
				,@RejectionReason
				,@InternalOrderId
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleRejectionsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external rejection for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId])
			VALUES
				(ISNULL(@SingleRejectionSystemId, -1)
				,@AmountBasePolRejected
				,@SymbolId
				,@CounterpartyId
				,@RejectedPrice
				,@RejectionReason
				,@InternalOrderId)
		END

		INSERT INTO 
		[dbo].[DealExternalRejected]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[SymbolId]
			   ,[CounterpartyId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolRejected]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[RejectionReason]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[RejectedPrice])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@SymbolId
			   ,@CounterpartyId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolRejected
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@RejectionReason
			   ,@CounterpartySentExecutionReportUtc
			   ,@RejectedPrice)
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[InsertNewOrderExternal_SP]( 
	@ExternalOrderSentUtc [datetime2](7),
	@InternalOrderId [nvarchar](45),
	@InternalClientOrderId [nvarchar](32),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@QuoteReceivedToOrderSentInternalLatency [time](7),
	@FixMessageSentRaw nvarchar(1024),
	@OrderTypeName varchar(16),
	@OrderTimeInForceName varchar(16),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@SourceIntegratorPriceIdentity uniqueidentifier = NULL,
	@SourceIntegratorPriceTimeUtc [datetime2](7) = NULL,
	@PegTypeName varchar(16) = NULL,
	@PegDifferenceBasePol decimal(18,9) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @OrderTypeId TINYINT
	DECLARE @OrderTimeInForceId TINYINT
	DECLARE @PegTypeId BIT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	SELECT @OrderTypeId = [OrderTypeId] FROM [dbo].[OrderType] WHERE [OrderTypeName] = @OrderTypeName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderType not found in DB: %s', 16, 2, @OrderTypeName) WITH SETERROR
	END
	
	SELECT @OrderTimeInForceId = [OrderTimeInForceId] FROM [dbo].[OrderTimeInForce] WHERE [OrderTimeInForceName] = @OrderTimeInForceName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderTimeInForce not found in DB: %s', 16, 2, @OrderTimeInForceName) WITH SETERROR
	END
	
	IF @OrderTypeName = 'Pegged'
	BEGIN
		SELECT @PegTypeId = [PegTypeId] FROM [dbo].[PegType] WHERE [PegTypeName] = @PegTypeName
		
		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'PegType not found in DB: %s', 16, 2, @PegTypeName) WITH SETERROR
		END
		
		IF @PegDifferenceBasePol IS NULL
		BEGIN
			RAISERROR (N'PegDifferenceBasePol was not specified for pegged order - but it is compulsory', 16, 2) WITH SETERROR
		END
	END

	INSERT INTO 
		[dbo].[OrderExternal]
           ([IntegratorSentExternalOrderUtc]
           ,[IntegratorExternalOrderPrivateId]
		   ,[ExternalOrderLabelForCounterparty]
           ,[CounterpartyId]
           ,[SymbolId]
		   ,[DealDirectionId]
           ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[FixMessageSentRaw]
		   ,[OrderTypeId]
		   ,[OrderTimeInForceId]
		   ,[RequestedPrice]
		   ,[RequestedAmountBaseAbs]
		   ,[SourceIntegratorPriceIdentity]
		   ,[SourceIntegratorPriceTimeUtc]
		   ,[PegTypeId]
		   ,[PegDifferenceBasePol])
     VALUES
           (@ExternalOrderSentUtc
           ,@InternalOrderId
		   ,@InternalClientOrderId
           ,@CounterpartyId
           ,@SymbolId
		   ,@DealDirectionId
           ,@QuoteReceivedToOrderSentInternalLatency
		   ,@FixMessageSentRaw
		   ,@OrderTypeId
		   ,@OrderTimeInForceId
		   ,@RequestedPrice
		   ,@RequestedAmountBaseAbs
		   ,@SourceIntegratorPriceIdentity
		   ,@SourceIntegratorPriceTimeUtc
		   ,@PegTypeId
		   ,@PegDifferenceBasePol)
END
GO

ALTER PROCEDURE [dbo].[InsertOrderExternalCancelResult_SP]( 
	@IntegratorExternalOrderPrivateId [nvarchar](45),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
    @CounterpartySentResultUtc  DATETIME2(7),
    @CancelledAmountBasePol decimal(18,2),
    @ResultStatusName VARCHAR(16)
	)
AS
BEGIN

	DECLARE @ResultStatusId TINYINT

	SELECT @ResultStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ResultStatusName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ResultStatusName not found in DB: %s', 16, 2, @ResultStatusName) WITH SETERROR
	END


	INSERT INTO [dbo].[OrderExternalCancelResult]
           ([IntegratorExternalOrderPrivateId]
		   ,[IntegratorSentExternalOrderUtc]
           ,[CounterpartySentResultUtc]
           ,[CanceledAmountBasePol]
           ,[ResultStatusId])
     VALUES
           (@IntegratorExternalOrderPrivateId
		   ,@IntegratorSentExternalOrderUtc
           ,@CounterpartySentResultUtc
           ,@CancelledAmountBasePol
           ,@ResultStatusId)
END
GO


ALTER PROCEDURE [dbo].[InsertOrderExternalIgnored_SP]( 
	@ExternalOrderSentUtc [datetime2](7),
	@ExternalOrderIgnoreDetectedUtc [datetime2](7),
	@IntegratorExternalOrderPrivateId [nvarchar](45),
	@InternalClientOrderId [nvarchar](32),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4)
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END

	INSERT INTO 
	[dbo].[OrderExternalIgnored]
           ([IntegratorSentExternalOrderUtc]
           ,[IntegratorDetectedOrderIsIgnoredUtc]
           ,[IntegratorExternalOrderPrivateId]
		   ,[ExternalOrderLabelForCounterparty]
		   ,[RequestedAmountBaseAbs]
		   ,[RequestedPrice]
		   ,[CounterpartyId]
		   ,[SymbolId]
		   ,[DealDirectionId])
     VALUES
           (@ExternalOrderSentUtc
           ,@ExternalOrderIgnoreDetectedUtc
           ,@IntegratorExternalOrderPrivateId
		   ,@InternalClientOrderId
		   ,@RequestedAmountBaseAbs
		   ,@RequestedPrice
		   ,@CounterpartyId
		   ,@SymbolId
		   ,@DealDirectionId)
END
GO

ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	DECLARE @TempTradingSystemStatistics TABLE
	(
		[SymbolId] [tinyint] NOT NULL,
		[TradingSystemId] [int] NOT NULL,
		[NopBasePol] [decimal](18,2) NOT NULL,
		[VolumeUsdM] [decimal](18,6) NOT NULL,
		[DealsNum] [int]  NOT NULL,
		[RejectionsNum] [int]  NOT NULL,
		[PnlGrossUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlGrossUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsdPerMUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsd] [decimal](18,6) NOT NULL
	)

	INSERT INTO 
		@TempTradingSystemStatistics
		([SymbolId]
		,[TradingSystemId]
		,[NopBasePol]
		,[VolumeUsdM]
		,[DealsNum]
		,[RejectionsNum]
		,[PnlGrossUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsdPerMUsd]
		,[CommissionsUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlNetUsd])
    exec [dbo].[GetTradingSystemsDailyStats_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingSystemDaily]
			([TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[RejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd])
		SELECT
			[TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[RejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd]
		FROM
			@TempTradingSystemStatistics
	COMMIT
END
GO

--BEGIN TRANSACTION Install_12_11_001;
--GO

IF EXISTS(SELECT * FROM  sys.synonyms s WHERE s.name = 'DealExternalExecuted_DailyView')
BEGIN
	DROP SYNONYM DealExternalExecuted_DailyView
END
GO
DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)
DECLARE @Query NVARCHAR(MAX) = N'IF EXISTS(select * FROM sys.views where name = ''DealExternalExecutedDailyView_' + @TodayLiteral + ''')
BEGIN
	DROP VIEW [DealExternalExecutedDailyView_' + @TodayLiteral + ']
END
'
EXECUTE sp_executesql @Query
GO
--this index contains inlined amount data
DROP INDEX [IX_DatePart_Symbol] ON [dbo].[DealExternalExecuted]
GO

ALTER TABLE DealExternalExecuted ALTER COLUMN AmountBasePolExecuted decimal(18,2)
GO
EXECUTE [dbo].[CreateDailyDealsView_SP]
GO
--this index contains inlined amount data
CREATE NONCLUSTERED INDEX [IX_DatePart_Symbol] ON [dbo].[DealExternalExecuted]
(
	[DatePartIntegratorReceivedExecutionReportUtc] ASC,
	[SymbolId] ASC
)
INCLUDE ( 	[AmountBasePolExecuted],
	[Price],
	[AmountTermPolExecutedInUsd],
	[CounterpartyId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE DealExternalRejected ALTER COLUMN AmountBasePolRejected decimal(18,2)
GO

ALTER TABLE OrderExternalIgnored ALTER COLUMN RequestedAmountBaseAbs decimal(18,2)
GO

ALTER TABLE OrderExternalCancelResult ALTER COLUMN CanceledAmountBasePol decimal(18,2)
GO

ALTER TABLE PersistedStatisticsPerTradingSystemDaily ALTER COLUMN NopBasePol decimal(18,2)
GO


ALTER TABLE TradingSystemDealsExecuted_Daily ALTER COLUMN AmountBasePolExecuted decimal(18,2)
GO
ALTER TABLE TradingSystemDealsExecuted_Archive ALTER COLUMN AmountBasePolExecuted decimal(18,2)
GO
ALTER TABLE TradingSystemDealsRejected_Daily ALTER COLUMN AmountBasePolRejected decimal(18,2)
GO
ALTER TABLE TradingSystemDealsRejected_Archive ALTER COLUMN AmountBasePolRejected decimal(18,2)
GO

	
--ROLLBACK TRANSACTION Install_12_11_001;
--GO
--COMMIT TRANSACTION Install_12_11_001;
--GO


BEGIN TRANSACTION Install_18_11_001;
GO


UPDATE
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], 'SocketConnectPort1', '#SocketConnectPort1')
WHERE
	[Configuration] like '%SocketConnectPort1%'


UPDATE
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], '##SocketConnectPort1', '#SocketConnectPort1')
WHERE
	[Configuration] like '%##SocketConnectPort1%'

UPDATE
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], 'SocketConnectHost1', '#SocketConnectHost1')
WHERE
	[Configuration] like '%SocketConnectHost1%'

UPDATE
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], '##SocketConnectHost1', '#SocketConnectHost1')
WHERE
	[Configuration] like '%##SocketConnectHost1%'
	
	
	
UPDATE
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], 'SocketConnectPort2', '#SocketConnectPort2')
WHERE
	[Configuration] like '%SocketConnectPort2%'


UPDATE
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], '##SocketConnectPort2', '#SocketConnectPort2')
WHERE
	[Configuration] like '%##SocketConnectPort2%'


UPDATE
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], 'SocketConnectHost2', '#SocketConnectHost2')
WHERE
	[Configuration] like '%SocketConnectHost2%'

UPDATE
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], '##SocketConnectHost2', '#SocketConnectHost2')
WHERE
	[Configuration] like '%##SocketConnectHost2%'
	

--ROLLBACK TRANSACTION Install_18_11_001;
--GO
--COMMIT TRANSACTION Install_18_11_001;
--GO