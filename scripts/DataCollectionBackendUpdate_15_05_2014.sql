

-- updating the column type - enormous requirements on log file

--print 'Convert started:'
--print GETUTCDATE()
--GO

--sp_rename MarketData, MarketData_ColConvert
--GO

--ALTER TABLE dbo.MarketData_ColConvert ALTER COLUMN CounterpartyPriceIdentity VARCHAR(64);
--GO

--sp_rename MarketData_ColConvert, MarketData
--GO

--print 'Convert DONE:'
--print GETUTCDATE()
--




--log on share

--   sp_configure 'xp_cmdshell',1; 
--Go 
--RECONFIGURE WITH OVERRIDE; 
--Go
--use master

--EXEC XP_CMDSHELL 'net use W: \\dev03\sharedScratch s9F3cx7Gk /USER:kgtsql01\Administrator2'

--EXEC XP_CMDSHELL 'net use V: \\dev01\drop dW9g3s5C6b /USER:dev01\Administrator'

--ALTER DATABASE Integrator_Testing 
--ADD LOG FILE 
--(
--    NAME = remotelog,
--    FILENAME = 'V:\remotelog_testing.ldf',
--    SIZE = 5MB,
--    MAXSIZE = 100MB,
--    FILEGROWTH = 5MB
--)

--EXEC XP_CMDSHELL 'net use Q: /delete'



-- 1) Create Filegroups

print 'Started:'
print GETUTCDATE()
GO



Alter Database FXtickDB Add FileGroup MDFilegroup_AUDCAD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_AUDCAD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_AUDCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_AUDCAD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_AUDCHF
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_AUDCHF_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_AUDCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_AUDCHF
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_AUDDKK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_AUDDKK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_AUDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_AUDDKK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_AUDHKD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_AUDHKD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_AUDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_AUDHKD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_AUDJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_AUDJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_AUDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_AUDJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_AUDNOK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_AUDNOK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_AUDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_AUDNOK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_AUDNZD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_AUDNZD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_AUDNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_AUDNZD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_AUDSEK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_AUDSEK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_AUDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_AUDSEK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_AUDSGD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_AUDSGD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_AUDSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_AUDSGD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_AUDUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_AUDUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_AUDUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_AUDUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CADCHF
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CADCHF_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CADCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CADCHF
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CADDKK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CADDKK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CADDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CADDKK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CADJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CADJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CADJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CADJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CADNOK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CADNOK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CADNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CADNOK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CADNZD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CADNZD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CADNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CADNZD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CADSEK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CADSEK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CADSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CADSEK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CHFDKK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CHFDKK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CHFDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CHFDKK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CHFJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CHFJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CHFJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CHFJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CHFNOK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CHFNOK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CHFNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CHFNOK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CHFSEK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CHFSEK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CHFSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CHFSEK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_DKKJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_DKKJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_DKKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_DKKJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_DKKNOK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_DKKNOK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_DKKNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_DKKNOK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_DKKSEK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_DKKSEK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_DKKSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_DKKSEK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURAUD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURAUD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURAUD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURCAD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURCAD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURCAD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURCHF
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURCHF_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURCHF
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURCNH
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURCNH_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURCNH_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURCNH
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURCZK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURCZK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURCZK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURDKK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURDKK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURDKK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURGBP
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURGBP_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURGBP_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURGBP
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURHKD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURHKD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURHKD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURHUF
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURHUF_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURHUF
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURJPY_File01,
FileName='S:\SQLDATA\USERDB\FXtickDB\MDPartitions\MDFilegroup_EURJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURMXN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURMXN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURMXN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURNOK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURNOK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURNOK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURNZD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURNZD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURNZD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURPLN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURPLN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURPLN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURRUB
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURRUB_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURRUB_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURRUB
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURSEK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURSEK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURSEK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURTRY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURTRY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURTRY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURUSD_File01,
FileName='S:\SQLDATA\USERDB\FXtickDB\MDPartitions\MDFilegroup_EURUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURZAR
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURZAR_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURZAR
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPAUD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPAUD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPAUD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPCAD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPCAD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPCAD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPCHF
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPCHF_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPCHF
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPCZK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPCZK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPCZK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPDKK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPDKK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPDKK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPHUF
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPHUF_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPHUF
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPNOK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPNOK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPNOK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPNZD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPNZD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPNZD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPPLN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPPLN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPPLN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPSEK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPSEK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPSEK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_HKDJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_HKDJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_HKDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_HKDJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_MXNJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_MXNJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_MXNJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_MXNJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NOKJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NOKJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NOKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NOKJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NOKSEK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NOKSEK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NOKSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NOKSEK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NZDCAD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NZDCAD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NZDCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NZDCAD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NZDCHF
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NZDCHF_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NZDCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NZDCHF
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NZDDKK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NZDDKK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NZDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NZDDKK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NZDJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NZDJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NZDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NZDJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NZDNOK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NZDNOK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NZDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NZDNOK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NZDSEK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NZDSEK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NZDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NZDSEK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NZDSGD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NZDSGD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NZDSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NZDSGD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NZDUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NZDUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NZDUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NZDUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_SGDJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_SGDJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_SGDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_SGDJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDCAD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDCAD_File01,
FileName='S:\SQLDATA\USERDB\FXtickDB\MDPartitions\MDFilegroup_USDCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDCAD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDCHF
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDCHF_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDCHF
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDCNH
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDCNH_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDCNH_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDCNH
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDCZK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDCZK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDCZK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDDKK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDDKK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDDKK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDHKD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDHKD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDHKD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDHUF
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDHUF_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDHUF
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDILS
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDILS_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDILS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDILS
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDMXN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDMXN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDMXN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDNOK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDNOK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDNOK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDPLN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDPLN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDPLN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDRUB
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDRUB_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDRUB_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDRUB
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDSEK
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDSEK_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDSEK
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDSGD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDSGD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDSGD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDTRY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDTRY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDTRY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDZAR
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDZAR_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDZAR
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_ZARJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_ZARJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_ZARJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_ZARJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_EURSGD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_EURSGD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_EURSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_EURSGD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPHKD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPHKD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPHKD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPMXN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPMXN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPMXN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPSGD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPSGD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPSGD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPTRY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPTRY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPTRY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GBPZAR
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GBPZAR_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GBPZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GBPZAR
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CHFMXN
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CHFMXN_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CHFMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CHFMXN
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_USDINR
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_USDINR_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_USDINR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_USDINR
GO 

--Carefull Schema needs 1 more filegroup (for far right range)
Alter Database FXtickDB Add FileGroup MDFilegroup_FutureSymbols
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_FutureSymbols_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_FutureSymbols_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_FutureSymbols
GO 

print 'Files Added:'
print GETUTCDATE()
GO

CREATE PARTITION SCHEME MarketDataSymbolPartitionScheme_IndividualFileGroups
AS PARTITION MarketDataSymbolPartitioningFunction TO (MDFilegroup_EURUSD, MDFilegroup_GBPUSD, MDFilegroup_AUDJPY, MDFilegroup_AUDUSD, MDFilegroup_CHFJPY, MDFilegroup_EURAUD, MDFilegroup_EURCHF, MDFilegroup_EURGBP, MDFilegroup_EURJPY, MDFilegroup_GBPCHF, MDFilegroup_GBPJPY, MDFilegroup_NZDUSD, MDFilegroup_USDCAD, MDFilegroup_USDCHF, MDFilegroup_USDJPY, MDFilegroup_NZDJPY, MDFilegroup_CADJPY, MDFilegroup_EURCAD, MDFilegroup_EURSEK, MDFilegroup_USDDKK, MDFilegroup_USDPLN, MDFilegroup_USDNOK, MDFilegroup_USDCZK, MDFilegroup_AUDNZD, MDFilegroup_EURNOK, MDFilegroup_AUDCAD, MDFilegroup_AUDCHF, MDFilegroup_CADCHF, MDFilegroup_EURCZK, MDFilegroup_EURHUF, MDFilegroup_EURNZD, MDFilegroup_EURPLN, MDFilegroup_GBPAUD, MDFilegroup_GBPCAD, MDFilegroup_USDSEK, MDFilegroup_USDMXN, MDFilegroup_EURDKK, MDFilegroup_USDHUF, MDFilegroup_CADNZD, MDFilegroup_GBPNZD, MDFilegroup_NZDCHF, MDFilegroup_AUDDKK, MDFilegroup_AUDNOK, MDFilegroup_EURTRY, MDFilegroup_EURZAR, MDFilegroup_NOKSEK, MDFilegroup_NZDCAD, MDFilegroup_USDHKD, MDFilegroup_USDSGD, MDFilegroup_USDTRY, MDFilegroup_EURMXN, MDFilegroup_USDZAR, MDFilegroup_AUDSEK, MDFilegroup_CADDKK, MDFilegroup_CADNOK, MDFilegroup_CADSEK, MDFilegroup_CHFDKK, MDFilegroup_CHFNOK, MDFilegroup_CHFSEK, MDFilegroup_DKKJPY, MDFilegroup_DKKNOK, MDFilegroup_DKKSEK, MDFilegroup_GBPDKK, MDFilegroup_GBPNOK, MDFilegroup_GBPSEK, MDFilegroup_HKDJPY, MDFilegroup_MXNJPY, MDFilegroup_NOKJPY, MDFilegroup_NZDDKK, MDFilegroup_NZDNOK, MDFilegroup_NZDSEK, MDFilegroup_AUDSGD, MDFilegroup_ZARJPY, MDFilegroup_NZDSGD, MDFilegroup_SGDJPY, MDFilegroup_USDILS, MDFilegroup_AUDHKD, MDFilegroup_EURHKD, MDFilegroup_EURRUB, MDFilegroup_GBPCZK, MDFilegroup_GBPHUF, MDFilegroup_USDRUB, MDFilegroup_GBPPLN, MDFilegroup_EURCNH, MDFilegroup_USDCNH, MDFilegroup_EURSGD, MDFilegroup_GBPHKD, MDFilegroup_GBPMXN, MDFilegroup_GBPSGD, MDFilegroup_GBPTRY, MDFilegroup_GBPZAR, MDFilegroup_CHFMXN, MDFilegroup_USDINR, MDFilegroup_FutureSymbols);
GO

print 'Partition scheme created:'
print GETUTCDATE()
GO

sp_rename MarketData__, MarketData_Splitting
GO

CREATE CLUSTERED INDEX IX_PairTime ON MarketData_Splitting
(
	[FXPairId] ASC,
	[IntegratorReceivedTimeUtc] ASC
)
WITH (DROP_EXISTING = ON)
ON MarketDataSymbolPartitionScheme_IndividualFileGroups([FXPairId]);
GO

sp_rename MarketData_Splitting, MarketData
GO

print 'Split DONE:'
print GETUTCDATE()
GO

-- 4) Verify result:
--DECLARE @TableName sysname = 'MarketData';
--SELECT p.partition_number, fg.name, p.rows FROM sys.partitions p  INNER JOIN sys.allocation_units au  ON au.container_id = p.hobt_id  INNER JOIN sys.filegroups fg  ON fg.data_space_id = au.data_space_id WHERE p.object_id = OBJECT_ID(@TableName)


-- to release empty space:
--EXEC sp_spaceused @updateusage = N'TRUE';
--GO

--SELECT file_id, name
--FROM sys.database_files;
--GO
--DBCC SHRINKFILE (1, TRUNCATEONLY);
--if not help:
--DBCC SHRINKFILE (1);


--Check errors with
--DBCC CHECKFILEGROUP (1) WITH ALL_ERRORMSGS 
--Repair errors with:
--DBCC CHECKALLOC (FXtickDB, REPAIR_ALLOW_DATA_LOSS)

--shrink log file:
--CHECKPOINT;
--GO
--CHECKPOINT; -- run twice to ensure file wrap-around
--GO
--DBCC SHRINKFILE(Integrator_log, 200);
--GO