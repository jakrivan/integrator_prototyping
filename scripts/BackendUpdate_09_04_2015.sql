BEGIN TRANSACTION Install_17_02_001;
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueStreamSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueStreamSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MaximumSizeToTrade]
		    ,[BestPriceImprovementOffsetBasisPoints]
		    ,[MaximumDiscountBasisPointsToTrade] 
		    ,[KGTLLTime_milliseconds]
		    ,[PreferLLDestinationCheckToLmax]
		    ,[MinimumBPGrossToAccept]
		    ,[PreferLmaxDuringHedging]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,[MinimumBasisPointsFromKgtPriceToFastCancel]
			,[ReferenceLmaxBookOnly]
			,[BidEnabled]
			,[AskEnabled]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueStreamSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name
	END
END
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueStreamSystemSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


ALTER PROCEDURE [dbo].[GetUpdatedVenueCrossSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueCrossSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[MinimumFillSize]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueCrossSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name 
	END
END
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueCrossSystemSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueMMSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueMMSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice]
			,[MaximumSizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,[MinimumBasisPointsFromKgtPriceToFastCancel]
			,[BidEnabled]
			,[AskEnabled]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueMMSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name
	END
END
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueMMSystemSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueQuotingSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueQuotingSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty			
			,[MaximumPositionBaseAbs]
			,[FairPriceImprovementSpreadBasisPoints]
			,[StopLossNetInUsd]		
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueQuotingSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name 
	END
END
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueQuotingSystemSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[GetLastVenueMMSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MaximumSizeToTrade]
		,[BestPriceImprovementOffsetBasisPoints]
		,[MaximumDiscountBasisPointsToTrade]
		,[MaximumWaitTimeOnImprovementTick_milliseconds]
		,[MinimumIntegratorPriceLife_milliseconds]
		,[MinimumSizeToTrade]
		,[MinimumFillSize]
		,[MaximumSourceBookTiers]
		,[MinimumBasisPointsFromKgtPriceToFastCancel]
		,[BidEnabled]
		,[AskEnabled]
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END
GO
GRANT EXECUTE ON [dbo].[GetLastVenueMMSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[GetLastVenueCrossSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MinimumSizeToTrade]
      ,[MaximumSizeToTrade]
      ,[MinimumDiscountBasisPointsToTrade]
      ,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
      ,[MaximumWaitTimeOnImprovementTick_milliseconds]
      ,[MinimumFillSize]
	FROM 
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END
GO
GRANT EXECUTE ON [dbo].[GetLastVenueCrossSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[GetLastVenueQuotingSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MaximumPositionBaseAbs]
      ,[FairPriceImprovementSpreadBasisPoints]
      ,[StopLossNetInUsd]
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END
GO
GRANT EXECUTE ON [dbo].[GetLastVenueQuotingSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[GetLastVenueStreamSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MaximumSizeToTrade]
      ,[BestPriceImprovementOffsetBasisPoints]
      ,[MaximumDiscountBasisPointsToTrade]
      ,[KGTLLTime_milliseconds]
      ,[PreferLLDestinationCheckToLmax]
      ,[MinimumBPGrossToAccept]
      ,[PreferLmaxDuringHedging]
      ,[MinimumIntegratorPriceLife_milliseconds]
      ,[MinimumSizeToTrade]
      ,[MinimumFillSize]
      ,[MaximumSourceBookTiers]
      ,[MinimumBasisPointsFromKgtPriceToFastCancel]
      ,[ReferenceLmaxBookOnly]
      ,[BidEnabled]
      ,[AskEnabled]
	FROM 
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END
GO
GRANT EXECUTE ON [dbo].[GetLastVenueStreamSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
  
  
  
ALTER TABLE [dbo].[TradingSystemDealsExecuted_Daily] ADD [DealTimeUtc] [datetime2](7) NULL
ALTER TABLE [dbo].[TradingSystemDealsRejected_Daily] ADD [RejectionTimeUtc] [datetime2](7) NULL
GO
UPDATE [dbo].[TradingSystemDealsExecuted_Daily] SET [DealTimeUtc] = CAST(GETUTCDATE() AS DATE)
UPDATE [dbo].[TradingSystemDealsRejected_Daily] SET [RejectionTimeUtc] = CAST(GETUTCDATE() AS DATE)
GO
ALTER TABLE [dbo].[TradingSystemDealsExecuted_Daily] ALTER COLUMN [DealTimeUtc] [datetime2](7) NOT NULL
ALTER TABLE [dbo].[TradingSystemDealsRejected_Daily] ALTER COLUMN [RejectionTimeUtc] [datetime2](7) NOT NULL
GO 

ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 2),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL,
	@SingleFillSystemId int = NULL,
	@MultipleFillsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL,
	@CounterpartyClientId varchar(20) = NULL)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @MultipleFillsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleFillsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external deal for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@SingleFillSystemId, -1)
				,@AmountBasePolExecuted
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc)
		END
		
		
		INSERT INTO 
		[dbo].[DealExternalExecuted]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolExecuted]
			   ,[AmountTermPolExecutedInUsd]
			   ,[Price]
			   ,[CounterpartyTransactionId]
			   ,[CounterpartySuppliedExecutionTimeStampUtc]
			   ,[ValueDateCounterpartySuppliedLocMktDate]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[InternalTransactionIdentity]
			   ,[FlowSideId]
			   ,[CounterpartyClientId])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolExecuted
			   ,@AmountTermPolExecutedInUsd
			   ,@Price
			   ,@CounterpartyTransactionId
			   ,@CounterpartyExecutionReportTimeStampUtc
			   ,@ValueDateCounterpartySuppliedLocMktDate
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@CounterpartySentExecutionReportUtc
			   ,@InternalDealId
			   ,@FlowSideId
			   ,@CounterpartyClientId)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO  
  
ALTER PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 2),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@RejectedPrice decimal(18, 9),
	@SingleRejectionSystemId int = NULL,
	@MultipleRejectionsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL,
	@RejectionDirection varchar(16),
	@CounterpartyClientId varchar(20) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionDirectionId BIT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = @RejectionDirection
	
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'RejectionDirection not found in DB: %s', 16, 2, @RejectionDirection) WITH SETERROR
	END

	BEGIN TRY
	
		BEGIN TRANSACTION tr1;
		
		IF @MultipleRejectionsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@RejectedPrice
				,@RejectionReason
				,@InternalOrderId
				,@RejectionDirectionId
				,@ExecutionReportReceivedUtc
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleRejectionsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external rejection for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			VALUES
				(ISNULL(@SingleRejectionSystemId, -1)
				,@AmountBasePolRejected
				,@SymbolId
				,@CounterpartyId
				,@RejectedPrice
				,@RejectionReason
				,@InternalOrderId
				,@RejectionDirectionId
				,@ExecutionReportReceivedUtc)
		END

		INSERT INTO 
		[dbo].[DealExternalRejected]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[SymbolId]
			   ,[CounterpartyId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolRejected]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[RejectionReason]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[RejectedPrice]
			   ,[RejectionDirectionId]
			   ,[CounterpartyClientId])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@SymbolId
			   ,@CounterpartyId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolRejected
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@RejectionReason
			   ,@CounterpartySentExecutionReportUtc
			   ,@RejectedPrice
			   ,@RejectionDirectionId
			   ,@CounterpartyClientId)
		
		COMMIT TRANSACTION tr1;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION tr1;
		THROW
	END CATCH
END
GO  
  
  
ALTER PROCEDURE [dbo].[InsertNewOrderExternalIncomingRejectable_SP]( 
    @ExternalOrderReceived [datetime2](7),
	@ExternalOrderSentUtc [datetime2](7),
	@IntegratorSentExecutionReportUtc [datetime2](7),
	@CounterpartyClientOrderId [varchar](32),
	@CounterpartyClientId [varchar](20),
	@TradingSystemId int = NULL,
	@SourceIntegratorPriceIdentity [varchar](20) = NULL,
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@IntegratorExecId [varchar](20),
	@ValueDate [date] = NULL,
	@OrderReceivedToExecutionReportSentLatency [time](7),
	@IntegratorExecutedAmountBaseAbs [decimal](18, 2) = NULL,
	@IntegratorRejectedAmountBaseAbs [decimal](18, 2) = NULL,
	@RejectionReason [varchar](32) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WITH(NOLOCK) WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WITH(NOLOCK) WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WITH(NOLOCK) WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @RejectionReason IS NOT NULL
	BEGIN
		SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
		IF @@ROWCOUNT <> 1
		BEGIN
			INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
			SET @RejectionReasonId = @@IDENTITY
		END
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @IntegratorExecutedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,@IntegratorExecId
				,@ExternalOrderReceived)
		END
		
		IF @IntegratorRejectedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,ISNULL(@RejectionReason, '<NONE>')
				,@IntegratorExecId
				,@RejectionDirectionId
				,@ExternalOrderReceived)
		END

		INSERT INTO 
			[dbo].[OrderExternalIncomingRejectable_Daily]
			   ([IntegratorReceivedExternalOrderUtc]
			   ,[CounterpartySentExternalOrderUtc]
			   ,[IntegratorSentExecutionReportUtc]
			   ,[CounterpartyClientOrderId]
			   ,[CounterpartyClientId]
			   ,[TradingSystemId]
			   ,[SourceIntegratorPriceIdentity]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorDealDirectionId]
			   ,[CounterpartyRequestedPrice]
			   ,[CounterpartyRequestedAmountBasePol]
			   ,[IntegratorExecutionId]
			   ,[ValueDate]
			   ,[OrderReceivedToExecutionReportSentLatency ]
			   ,[IntegratorExecutedAmountBasePol]
			   ,[IntegratorRejectedAmountBasePol]
			   ,[IntegratorRejectionReasonId])
		 VALUES
			   (@ExternalOrderReceived
			   ,@ExternalOrderSentUtc
			   ,@IntegratorSentExecutionReportUtc
			   ,@CounterpartyClientOrderId
			   ,@CounterpartyClientId
			   ,@TradingSystemId
			   ,@SourceIntegratorPriceIdentity
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@DealDirectionId
			   ,@RequestedPrice
			   ,CASE WHEN @DealDirectionId = 1 THEN -@RequestedAmountBaseAbs ELSE @RequestedAmountBaseAbs END
			   ,@IntegratorExecId
			   ,@ValueDate
			   ,@OrderReceivedToExecutionReportSentLatency 
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
		       ,@RejectionReasonId)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH	   
		   	   
END
GO  
  
ALTER PROCEDURE [dbo].[RejectExecutedOrderExternalIncomingRejectable_SP]( 
	@IntegratorExecId  [varchar](20),
	@RejectionReason [varchar](20) = NULL
	)
AS
BEGIN
	DECLARE @OriginallyExecutedAmount decimal(18,2)
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT 
		@OriginallyExecutedAmount = [IntegratorExecutedAmountBasePol] 
	FROM 
		[dbo].[OrderExternalIncomingRejectable_Daily]
	WHERE 
		[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) AND [IntegratorExecutionId] = @IntegratorExecId

	IF @OriginallyExecutedAmount IS NOT NULL
	BEGIN
	
		IF @RejectionReason IS NOT NULL
		BEGIN
			SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
			IF @@ROWCOUNT <> 1
			BEGIN
				INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
				SET @RejectionReasonId = @@IDENTITY
			END
		END
	
		BEGIN TRY

		BEGIN TRANSACTION;
	
			UPDATE
				[dbo].[OrderExternalIncomingRejectable_Daily]
			SET
				[IntegratorRejectedAmountBasePol] += @OriginallyExecutedAmount,
				[IntegratorExecutedAmountBasePol] = 0,
				[IntegratorRejectionReasonId] = @RejectionReasonId
			WHERE 
				[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) 
				AND [IntegratorExecutionId] = @IntegratorExecId

				
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			SELECT
				[TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,ISNULL(@RejectionReason, '<NONE>')
				,[InternalTransactionIdentity]
				,@RejectionDirectionId
				,[DealTimeUtc]
			FROM
				[dbo].[TradingSystemDealsExecuted_Daily]
			WHERE
				[InternalTransactionIdentity] = @IntegratorExecId
					
			DELETE FROM
				[dbo].[TradingSystemDealsExecuted_Daily]
			WHERE
				[InternalTransactionIdentity] = @IntegratorExecId
			
		COMMIT TRANSACTION;
		
		SELECT 
			[IntegratorReceivedExternalOrderUtc]
			,[CounterpartySentExternalOrderUtc]
			,[CounterpartyClientOrderId]
			,[TradingSystemId]
			,[SourceIntegratorPriceIdentity]
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.[Name] as Symbol
			,direction.[Name] as DealDirection
			,[CounterpartyRequestedPrice]
			,@OriginallyExecutedAmount AS OriginallyExecutedAmountBasePol
			,[IntegratorExecutionId]
			,[ValueDate]
		FROM 
			[dbo].[OrderExternalIncomingRejectable_Daily] deals
			INNER JOIN [dbo].[Symbol] symbol ON deals.[SymbolId] = symbol.[Id]
			INNER JOIN [dbo].[Counterparty] counterparty ON deals.[CounterpartyId] = counterparty.[CounterpartyId]
			INNER JOIN [dbo].[DealDirection] direction ON deals.[IntegratorDealDirectionId] = direction.Id
		WHERE 
			[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) AND [IntegratorExecutionId] = @IntegratorExecId
		
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION;
			THROW
		END CATCH
		
	END
END
GO  
  
  
ALTER VIEW [dbo].[DealExternalExecutedTakersAndMakers_DailyView]
AS
SELECT
	[CounterpartyId]
	,[SymbolId]
	,[AmountBasePolExecuted]
	,[Price]
	,[IntegratorLatencyNanoseconds]
	,[CounterpartyLatencyNanoseconds]
	,[DatePartIntegratorReceivedExecutionReportUtc]
	,[IntegratorReceivedExecutionReportUtc]
FROM 
	[dbo].[DealExternalExecuted_DailyView] WITH(NOEXPAND, NOLOCK)

UNION ALL

SELECT 
	[CounterpartyId]
	,[SymbolId]
	,[IntegratorExecutedAmountBasePol] AS [AmountBasePolExecuted]
	,[CounterpartyRequestedPrice] AS [Price]
	,[IntegratorLatencyNanoseconds]
	,NULL
	,[DatePartIntegratorReceivedOrderUtc] AS [DatePartIntegratorReceivedExecutionReportUtc]
	,[IntegratorReceivedExternalOrderUtc]
FROM 
	[dbo].[OrderExternalIncomingRejectable_Daily] WITH(NOLOCK)
WHERE
	[IntegratorExecutedAmountBasePol] != 0
GO  
  
ALTER PROCEDURE [dbo].[GetStatisticsPerPair_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	SELECT
		Symbol.Name AS Symbol,
		positions.NopBasePol,
		positions.NopTermPol,
		positions.DealsNum as DealsNum,
		positions.LastDealUtc AS LastDealUtc,
		--PnL = term position + yield of immediate close of outstanding base position
		--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
		positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskMeanPrice ELSE Symbol.LastKnownBidMeanPrice END) * positions.NopBasePol AS PnlTermPol,
		(positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskMeanPrice ELSE Symbol.LastKnownBidMeanPrice END) * positions.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
		positions.VolumeInCcy1 AS VolumeInCcy1,
		positions.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd
	FROM
		(
		SELECT
			deal.SymbolId AS SymbolId,
			SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
			SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
			COUNT(deal.SymbolId) AS DealsNum,
			MAX(deal.IntegratorReceivedExecutionReportUtc) AS LastDealUtc,
			SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1
		FROM 
			DealExternalExecutedTakersAndMakers_DailyView deal
			
		WHERE
			[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		GROUP BY
			deal.SymbolId
		) positions
		INNER JOIN  [dbo].[Symbol] symbol ON positions.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
	ORDER BY
		symbol.Name
END
GO  
  
-----------------------------  

ALTER TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily] ADD LastDealUtc [datetime2](7) NULL
ALTER TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily] ADD LastCptRejectionUtc [datetime2](7) NULL
ALTER TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily] ADD LastKgtRejectionUtc [datetime2](7) NULL
GO
ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ADD LastDealUtc [datetime2](7) NULL
ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ADD LastCptRejectionUtc [datetime2](7) NULL
ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ADD LastKgtRejectionUtc [datetime2](7) NULL
GO
ALTER TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily] ADD LastDealUtc [datetime2](7) NULL
ALTER TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily] ADD LastCptRejectionUtc [datetime2](7) NULL
ALTER TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily] ADD LastKgtRejectionUtc [datetime2](7) NULL
GO

ALTER PROCEDURE [dbo].[GetStatisticsPerCounterparty_DailyCached_SP]
AS
BEGIN
	SELECT 
		[Counterparty]
		,[NopAbs]
		,[VolumeBaseAbsInUsd]
		,[VolumeShare]
		,[DealsNum]
		,[LastDealUtc]
		,[AvgDealSizeUsd]
		,[KGTRejectionsNum]
		,[LastKgtRejectionUtc]
		,[CtpRejectionsNum]
		,[LastCptRejectionUtc]
		,[KGTRejectionsRate]
		,[CtpRejectionsRate]
		,[AvgIntegratorLatencyNanoseconds]
		,[AvgCounterpartyLatencyNanoseconds]
	FROM 
		[dbo].[PersistedStatisticsPerCounterpartyDaily]
	ORDER BY
		[Counterparty]
END
GO 


ALTER PROCEDURE [dbo].[GetStatisticsPerCounterparty_Daily_SP] (
	@Day DATE
	)
AS
BEGIN

	DECLARE @KGTRejectionDirectionId BIT
	SELECT @KGTRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'
	DECLARE @CtpRejectionDirectionId BIT
	SELECT @CtpRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'CtpRejectd'
	DECLARE @MinDate DATE = GETUTCDATE()	
		
	DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		KGTRejectionsNum int,
		LastKgtRejectionUtc datetime2(7),
		CtpRejectionsNum int,
		LastCptRejectionUtc datetime2(7),
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int
	)

	INSERT INTO @temp_rejections(CounterpartyId, KGTRejectionsNum, LastKgtRejectionUtc, CtpRejectionsNum, LastCptRejectionUtc, IntegratorLatencyNanoseconds, IntegratorLatencyCases)
	SELECT
		[CounterpartyId],
		SUM(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN 1 ELSE 0 END) AS KGTRejections,
		MAX(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN [IntegratorReceivedExecutionReportUtc] ELSE @MinDate END) AS LastKgtRejectionUtc,
		SUM(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN 1 ELSE 0 END) AS CtpRejections,
		MAX(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN [IntegratorReceivedExecutionReportUtc] ELSE @MinDate END) AS LastCptRejectionUtc,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
	FROM
		[dbo].[DealExternalRejected_DailyView] ctpRej WITH(NOEXPAND, NOLOCK)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]

	UNION ALL

	SELECT
			[CounterpartyId],
			COUNT([CounterpartyId]) AS KGTRejections,
			MAX([IntegratorReceivedExternalOrderUtc]) AS LastKgtRejectionUtc,
			NULL AS CtpRejections,
			NULL AS LastCptRejectionUtc,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily] kgtRej WITH(NOLOCK)
		WHERE
			[IntegratorRejectedAmountBasePol] != 0
			AND [DatePartIntegratorReceivedOrderUtc] = @Day
			--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
		GROUP BY
			[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		DealsNum int,
		LastDealUtc datetime2(7),
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int,
		CounterpartyLatencyNanoseconds bigint,
		CounterpartyLatencyCases int
	)

	INSERT INTO 
		@temp_NOPs_pol
		(
		SymbolId, 
		CounterpartyId, 
		NopBasePol, 
		NopTermPol, 
		VolumeBaseAbs, 
		DealsNum, 
		LastDealUtc,
		IntegratorLatencyNanoseconds, 
		IntegratorLatencyCases, 
		CounterpartyLatencyNanoseconds, 
		CounterpartyLatencyCases
		)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum,
		MAX(deal.[IntegratorReceivedExecutionReportUtc]) AS LastDealUtc,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases,
		SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
		COUNT(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyCases
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
		--INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		deal.SymbolId,
		deal.CounterpartyId
		

	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.LastDealUtc AS LastDealUtc,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		CASE WHEN COALESCE(RejectionsStatistics.KGTRejectionsNum,0) = 0 THEN NULL ELSE RejectionsStatistics.KGTRejectionsNum END AS KGTRejectionsNum,
		RejectionsStatistics.LastKgtRejectionUtc AS RejectionsStatistics,
		CASE WHEN COALESCE(RejectionsStatistics.CtpRejectionsNum,0) = 0 THEN NULL ELSE RejectionsStatistics.CtpRejectionsNum END AS CtpRejectionsNum,
		RejectionsStatistics.LastCptRejectionUtc AS LastCptRejectionUtc,
		CASE WHEN COALESCE(RejectionsStatistics.KGTRejectionsNum,0) = 0
		THEN
			 NULL
		ELSE
			RejectionsStatistics.KGTRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.KGTRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0))
		END AS KGTRejectionsRate,
		CASE WHEN COALESCE(RejectionsStatistics.CtpRejectionsNum,0) = 0
		THEN
			NULL
		ELSE
			RejectionsStatistics.CtpRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.CtpRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0))
		END AS CtpRejectionsRate,
		--this calculates average of both numbers if both are non-null; or average of the nonull case if just one is not null (as 2*A / 2*B = A/B); or NULL if both are NULL (avoids divide by zero exception)
		(COALESCE(RejectionsStatistics.IntegratorLatencyNanoseconds, DealsStatistics.IntegratorLatencyNanoseconds) + COALESCE(DealsStatistics.IntegratorLatencyNanoseconds, RejectionsStatistics.IntegratorLatencyNanoseconds))
		/
		(COALESCE(RejectionsStatistics.IntegratorLatencyCases, DealsStatistics.IntegratorLatencyCases) + COALESCE(DealsStatistics.IntegratorLatencyCases, RejectionsStatistics.IntegratorLatencyCases))
		AS AvgIntegratorLatencyNanoseconds,
		DealsStatistics.CounterpartyLatencyNanoseconds / DealsStatistics.CounterpartyLatencyCases AS AvgCounterpartyLatencyNanoseconds
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			-- from SHORTs
			SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs,
			--SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(DealsNum) AS DealsNum,
			MAX(LastDealUtc) AS LastDealUtc,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
			SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
			SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(DealsNum) AS DealsNum,
				MAX(LastDealUtc) AS LastDealUtc,
				SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
				SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
				SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
				SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.DealsNum AS DealsNum,
					basePos.LastDealUtc AS LastDealUtc,
					basePos.IntegratorLatencyNanoseconds AS IntegratorLatencyNanoseconds,
					basePos.IntegratorLatencyCases AS IntegratorLatencyCases,
					basePos.CounterpartyLatencyNanoseconds AS CounterpartyLatencyNanoseconds,
					basePos.CounterpartyLatencyCases AS CounterpartyLatencyCases
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd,
					0 AS DealsNum,
					NULL AS LastDealUtc,
					NULL AS IntegratorLatencyNanoseconds,
					NULL AS IntegratorLatencyCases,
					NULL AS CounterpartyLatencyNanoseconds,
					NULL AS CounterpartyLatencyCases
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode
END
GO
  
ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
AS
BEGIN

	--create the temp table before transaction to speed up things
	
	CREATE TABLE #TempCounterpartyStatistics(
		[Counterparty] [varchar](3) NOT NULL,
		[NopAbs] decimal(18,2) NULL,
		[VolumeBaseAbsInUsd] decimal(18, 2) NULL,
		[VolumeShare] decimal(18, 4) NULL,
		[DealsNum] int NULL,
		[LastDealUtc] datetime2(7) NULL,
		[AvgDealSizeUsd] decimal(18,2) NULL,
		[KGTRejectionsNum] int NULL,
		[LastKgtRejectionUtc] datetime2(7) NULL,
		[CtpRejectionsNum] int NULL,
		[LastCptRejectionUtc] datetime2(7) NULL,
		[KGTRejectionsRate] decimal(18,4) NULL,
		[CtpRejectionsRate] decimal(18,4) NULL,
		[AvgIntegratorLatencyNanoseconds] bigint NULL,
		[AvgCounterpartyLatencyNanoseconds] bigint
	)

	DECLARE @Today DATE = GETUTCDATE()
	
	INSERT INTO 
		#TempCounterpartyStatistics
		(
		[Counterparty],
		[NopAbs],
		[VolumeBaseAbsInUsd],
		[VolumeShare],
		[DealsNum],
		[LastDealUtc],
		[AvgDealSizeUsd],
		[KGTRejectionsNum],
		[LastKgtRejectionUtc],
		[CtpRejectionsNum],
		[LastCptRejectionUtc],
		[KGTRejectionsRate],
		[CtpRejectionsRate],
		[AvgIntegratorLatencyNanoseconds],
		[AvgCounterpartyLatencyNanoseconds]
		)
	exec [dbo].[GetStatisticsPerCounterparty_Daily_SP] @Today

	BEGIN TRANSACTION
	   TRUNCATE TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily]

	   INSERT INTO 
			[dbo].[PersistedStatisticsPerCounterpartyDaily]
			(
			[Counterparty],
			[NopAbs],
			[VolumeBaseAbsInUsd],
			[VolumeShare],
			[DealsNum],
			[LastDealUtc],
			[AvgDealSizeUsd],
			[KGTRejectionsNum],
			[LastKgtRejectionUtc],
			[CtpRejectionsNum],
			[LastCptRejectionUtc],
			[KGTRejectionsRate],
			[CtpRejectionsRate],
			[AvgIntegratorLatencyNanoseconds],
			[AvgCounterpartyLatencyNanoseconds]
			)
		SELECT
			[Counterparty],
			[NopAbs],
			[VolumeBaseAbsInUsd],
			[VolumeShare],
			[DealsNum],
			[LastDealUtc],
			[AvgDealSizeUsd],
			[KGTRejectionsNum],
			[LastKgtRejectionUtc],
			[CtpRejectionsNum],
			[LastCptRejectionUtc],
			[KGTRejectionsRate],
			[CtpRejectionsRate],
			[AvgIntegratorLatencyNanoseconds],
			[AvgCounterpartyLatencyNanoseconds]
		FROM
			#TempCounterpartyStatistics

	COMMIT
END
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsDailyStats_SP]
AS
BEGIN

DECLARE @KGTRejectionDirectionId BIT
SELECT @KGTRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'
DECLARE @CtpRejectionDirectionId BIT
SELECT @CtpRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'CtpRejectd'
DECLARE @MinDate DATE = GETUTCDATE()

SELECT
		--BEWARE! There can be multiple symbols for same system id (this is especially true for system -1)
		systemStats.SymbolId AS SymbolId,
		systemStats.TradingSystemId AS TradingSystemId,
		COALESCE(systemStats.NopBasePol, 0) AS NopBasePol,
		COALESCE(systemStats.VolumeInUsd/1000000, 0) AS VolumeUsdM,
		COALESCE(systemStats.DealsNum, 0) as DealsNum,
		systemStats.LastDealUtc as LastDealUtc,
		COALESCE(systemStats.KGTRejectionsNum, 0) as KGTRejectionsNum,
		systemStats.LastKgtRejectionUtc as LastKgtRejectionUtc,
		COALESCE(systemStats.CtpRejectionsNum, 0) as CtpRejectionsNum,
		systemStats.LastCptRejectionUtc as LastCptRejectionUtc,
		COALESCE(systemStats.PnlTermPolInUsd/(systemStats.VolumeInUsd/1000000), 0) AS PnlGrossUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd, 0) AS PnlGrossUsd,
		COALESCE(systemStats.CommissionInUSD/(systemStats.VolumeInUsd/1000000), 0) AS CommissionsUsdPerMUsd,
		COALESCE(systemStats.CommissionInUSD, 0) AS CommissionsUsd,
		COALESCE((systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD)/(systemStats.VolumeInUsd/1000000), 0) AS PnlNetUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD, 0) AS PnlNetUsd
	FROM
		(
		SELECT
			Symbol.Id AS SymbolId,
			systemsStats.TradingSystemId AS TradingSystemId,
			systemsStats.NopBasePol AS NopBasePol,
			systemsStats.DealsNum as DealsNum,
			systemsStats.LastDealUtc as LastDealUtc,
			systemsStats.KGTRejectionsNum as KGTRejectionsNum,
			systemsStats.LastKgtRejectionUtc as LastKgtRejectionUtc,
			systemsStats.CtpRejectionsNum as CtpRejectionsNum,
			systemsStats.LastCptRejectionUtc as LastCptRejectionUtc,
			systemsStats.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd,
			--PnL = term position + yield of immediate close of outstanding base position
			--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
			(systemsStats.NopTermPol + (CASE WHEN systemsStats.NopBasePol < 0 THEN Symbol.LastKnownAskMeanPrice ELSE Symbol.LastKnownBidMeanPrice END) * systemsStats.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
			systemsStats.CommissionInCCY1 * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionInUSD
		FROM
			(
			SELECT
				COALESCE(systemsPositions.[TradingSystemId], systemRejections.[TradingSystemId]) AS [TradingSystemId],
				COALESCE(systemsPositions.[SymbolId], systemRejections.[SymbolId]) AS [SymbolId],
				NopBasePol,
				NopTermPol,
				VolumeInCcy1,
				CommissionInCCY1,
				DealsNum,
				LastDealUtc,
				KGTRejectionsNum,
				LastKgtRejectionUtc,
				CtpRejectionsNum,
				LastCptRejectionUtc
			FROM	
				(
				--allways include stats fro unknown systems
				SELECT 
					-1 AS [TradingSystemId], 
					0 AS [SymbolId],
					NULL AS NopBasePol, 
					NULL AS NopTermPol, 
					NULL AS VolumeInCcy1, 
					NULL AS CommissionInCCY1, 
					NULL AS DealsNum,
					NULL AS LastDealUtc
				UNION ALL
				SELECT 
					[TradingSystemId],
					[SymbolId],
					SUM([AmountBasePolExecuted]) AS NopBasePol,
					SUM(-[AmountBasePolExecuted]* [Price]) AS NopTermPol,
					SUM(ABS([AmountBasePolExecuted])) AS VolumeInCcy1,
					SUM(ABS([AmountBasePolExecuted]) * CommissionPricePerMillion / 1000000) AS CommissionInCCY1,
					COUNT([TradingSystemId]) AS DealsNum,
					MAX([DealTimeUtc]) AS LastDealUtc
				FROM
					[dbo].[TradingSystemDealsExecuted_Daily] deal
					INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
				GROUP BY
					[TradingSystemId], [SymbolId]
				) systemsPositions
				FULL OUTER JOIN
				(
					SELECT 
						[TradingSystemId],
						[SymbolId],
						--COUNT([TradingSystemId]) AS RejectionsNum
						SUM(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN 1 ELSE 0 END) AS KGTRejectionsNum, 
						MAX(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN [RejectionTimeUtc] ELSE @MinDate END) AS LastKgtRejectionUtc, 
						SUM(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN 1 ELSE 0 END) AS CtpRejectionsNum,
						MAX(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN [RejectionTimeUtc] ELSE @MinDate END) AS LastCptRejectionUtc 
					FROM
						[dbo].[TradingSystemDealsRejected_Daily] rejects
					GROUP BY
						 [TradingSystemId], [SymbolId]
				)  systemRejections ON systemsPositions.TradingSystemId = systemRejections.TradingSystemId AND systemsPositions.[SymbolId] = systemRejections.[SymbolId]
			) systemsStats
			INNER JOIN [dbo].[Symbol] symbol ON systemsStats.SymbolId = symbol.Id
			INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
			INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
			) systemStats
END
GO

ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	DECLARE @TempTradingSystemStatistics TABLE
	(
		[SymbolId] [tinyint] NOT NULL,
		[TradingSystemId] [int] NOT NULL,
		[NopBasePol] [decimal](18,2) NOT NULL,
		[VolumeUsdM] [decimal](18,6) NOT NULL,
		[DealsNum] [int]  NOT NULL,
		[LastDealUtc] [datetime2](7) NULL,
		[KGTRejectionsNum] [int]  NOT NULL,
		[LastKgtRejectionUtc] [datetime2](7) NULL,
		[CtpRejectionsNum] [int]  NOT NULL,
		[LastCptRejectionUtc] [datetime2](7) NULL,
		[PnlGrossUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlGrossUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsdPerMUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsd] [decimal](18,6) NOT NULL
	)

	INSERT INTO 
		@TempTradingSystemStatistics
		([SymbolId]
		,[TradingSystemId]
		,[NopBasePol]
		,[VolumeUsdM]
		,[DealsNum]
		,[LastDealUtc]
		,[KGTRejectionsNum]
		,[LastKgtRejectionUtc]
		,[CtpRejectionsNum]
		,[LastCptRejectionUtc]
		,[PnlGrossUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsdPerMUsd]
		,[CommissionsUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlNetUsd])
    exec [dbo].[GetTradingSystemsDailyStats_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingSystemDaily]
			([TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd])
		SELECT
			[TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd]
		FROM
			@TempTradingSystemStatistics
	COMMIT
END
GO


ALTER PROCEDURE [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP]
AS
BEGIN
	
	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()

	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]

	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn
	
	SELECT
		ROW_NUMBER() OVER(ORDER BY
			trgroup.[OrderingRank] ASC,
			[TradingSystemGroupName] ASC,
			counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] ASC,
			beginSymbol.Name ASC) AS Rank,
		trgroup.[TradingSystemGroupName] AS TradingSystemGroup,
		trgroup.[TradingSystemGroupId] As TradingSystemGroupId,
		counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] AS TradingSystemTypeName,
		counterparty.[CounterpartyCode] AS Counterparty,
		systemtype.[TradingSystemTypeId] AS TradingSystemTypeId,
		statsPerType.ConfiguredSystems,
		--statsPerType.EnabledSystems,
		--this should be called active - and upper stay enabled
		CASE WHEN @TradingOn = 1 THEN statsPerType.ActiveSystems ELSE 0 END AS EnabledSystems,
		statsPerType.HardBlockedSystems,
		statsPerType.VolumeUsdM,
		statsPerType.DealsNum,
		statsPerType.LastDealUtc,
		statsPerType.KGTRejectionsNum,
		statsPerType.LastKgtRejectionUtc,
		statsPerType.CtpRejectionsNum,
		statsPerType.LastCptRejectionUtc,
		statsPerType.PnlGrossUsdPerMUsd,
		statsPerType.CommissionsUsdPerMUsd,
		statsPerType.PnlNetUsdPerMUsd,
		statsPerType.PnlGrossUsd,
		statsPerType.CommissionsUsd,
		statsPerType.PnlNetUsd,
		allowedType.IsOrderTransmissionDisabled,
		allowedType.IsGoFlatOnlyOn,
		monitorPages.TradingSystemMonitoringPageId AS PageId,
		beginSymbol.Name AS BeginSymbol,
		endSymbol.Name AS EndSymbol,
		monitorPages.StripsCount
	FROM
	(
	SELECT 
		allowedType.TradingSystemGroupId AS TradingSystemGroupId,
		allowedType.TradingSystemTypeId AS TradingSystemTypeId,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		COALESCE(SUM(CASE WHEN action.[Enabled] = 1 AND action.[GoFlatOnly] = 0 AND action.[OrderTransmissionDisabled] = 0 THEN 1 ELSE 0 END), 0) AS ActiveSystems,
		COALESCE(SUM(CONVERT(int, action.[HardBlocked])), 0) AS HardBlockedSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		MAX(stats.[LastDealUtc]) AS LastDealUtc,
		SUM(stats.[KGTRejectionsNum]) AS KGTRejectionsNum,
		MAX(stats.[LastKgtRejectionUtc]) AS LastKgtRejectionUtc,
		SUM(stats.[CtpRejectionsNum]) AS CtpRejectionsNum,
		MAX(stats.[LastCptRejectionUtc]) AS LastCptRejectionUtc,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemTypeAllowedForGroup] allowedType_forJoinsOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = allowedType_forJoinsOnly.[TradingSystemTypeId] AND trsystem.TradingSystemGroupId = allowedType_forJoinsOnly.TradingSystemGroupId
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		--link in deleted systems if needed
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		-- but make sure that those are only the allowed ones
		LEFT JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON 
			allowedType.[TradingSystemTypeId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId])
			AND
			allowedType.[TradingSystemGroupId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemGroupId], trsystem_deleted.[TradingSystemGroupId])

	GROUP BY
		allowedType.TradingSystemGroupId, allowedType.TradingSystemTypeId
	)
	statsPerType
	INNER JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON allowedType.TradingSystemGroupId = statsPerType.TradingSystemGroupId AND allowedType.TradingSystemTypeId = statsPerType.TradingSystemTypeId
	INNER JOIN [dbo].[TradingSystemType] systemtype ON statsPerType.[TradingSystemTypeId] = systemtype.[TradingSystemTypeId]
	INNER JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	RIGHT JOIN TradingSystemGroup trgroup ON trgroup.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId
	LEFT JOIN TradingSystemMonitoringPage monitorPages ON monitorPages.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId AND monitorPages.[TradingSystemTypeId] = statsPerType.TradingSystemTypeId
	LEFT JOIN [dbo].[Symbol] beginSymbol ON monitorPages.BeginSymbolId = beginSymbol.Id
	LEFT JOIN [dbo].[Symbol] endSymbol ON monitorPages.EndSymbolId = endSymbol.Id
END
GO


ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	DECLARE @TempTradingGroupsStatistics TABLE
	(
		[Rank] [int] NOT NULL,
		[TradingSystemGroupName] [NVARCHAR](32) NOT NULL,
		[TradingSystemGroupId] [int]  NOT NULL,
		[TradingSystemTypeName] [nvarchar](12) NULL,
		[Counterparty] [char](3) NULL,
		[TradingSystemTypeId] [tinyint] NULL,
		[ConfiguredSystemsCount] int NULL,
		[EnabledSystemsCount] int NULL,
		[HardBlockedSystemsCount] int NULL,
		[VolumeUsdM] [decimal](18, 6) NULL,
		[DealsNum] [int] NULL,
		[LastDealUtc] [datetime2](7) NULL,
		[KGTRejectionsNum] [int] NULL,
		[LastKgtRejectionUtc] [datetime2](7) NULL,
		[CtpRejectionsNum] [int] NULL,
		[LastCptRejectionUtc] [datetime2](7) NULL,
		[PnlGrossUsdPerMUsd] [decimal](18, 6) NULL,
		[CommissionsUsdPerMUsd] [decimal](18, 6) NULL,
		[PnlNetUsdPerMUsd] [decimal](18, 6) NULL,
		[PnlGrossUsd] [decimal](18, 6) NULL,
		[CommissionsUsd] [decimal](18, 6) NULL,	
		[PnlNetUsd] [decimal](18, 6) NULL,
		[IsOrderTransmissionDisabled] [bit] NULL,
		[IsGoFlatOnlyOn] [bit] NULL,
		[PageId] [int] NULL,
		[BeginSymbol] [nchar](7) NULL,
		[EndSymbol] [nchar](7) NULL,
		[StripsCount] INT
	)

	INSERT INTO 
		@TempTradingGroupsStatistics
		([Rank]
		,[TradingSystemGroupName]
		,[TradingSystemGroupId]
		,[TradingSystemTypeName]
		,[Counterparty]
		,[TradingSystemTypeId]
		,[ConfiguredSystemsCount]
		,[EnabledSystemsCount]
		,[HardBlockedSystemsCount]
		,[VolumeUsdM]
		,[DealsNum]
		,[LastDealUtc]
		,[KGTRejectionsNum]
		,[LastKgtRejectionUtc]
		,[CtpRejectionsNum]
		,[LastCptRejectionUtc]
		,[PnlGrossUsdPerMUsd]
		,[CommissionsUsdPerMUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsd]
		,[PnlNetUsd]
		,[IsOrderTransmissionDisabled]
		,[IsGoFlatOnlyOn]
		,[PageId]
		,[BeginSymbol]
		,[EndSymbol]
		,[StripsCount])
    exec [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingGroupsDaily]
			([Rank]
			,[TradingSystemGroupName]
			,[TradingSystemGroupId]
			,[TradingSystemTypeName]
			,[Counterparty]
			,[TradingSystemTypeId]
			,[ConfiguredSystemsCount]
			,[EnabledSystemsCount]
			,[HardBlockedSystemsCount]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[CommissionsUsdPerMUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsd]
			,[PnlNetUsd]
			,[IsOrderTransmissionDisabled]
			,[IsGoFlatOnlyOn]
			,[PageId]
			,[BeginSymbol]
			,[EndSymbol]
			,[StripsCount])
		SELECT
			[Rank]
			,[TradingSystemGroupName]
			,[TradingSystemGroupId]
			,[TradingSystemTypeName]
			,[Counterparty]
			,[TradingSystemTypeId]
			,[ConfiguredSystemsCount]
			,[EnabledSystemsCount]
			,[HardBlockedSystemsCount]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[CommissionsUsdPerMUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsd]
			,[PnlNetUsd]
			,[IsOrderTransmissionDisabled]
			,[IsGoFlatOnlyOn]
			,[PageId]
			,[BeginSymbol]
			,[EndSymbol]
			,[StripsCount]
		FROM
			@TempTradingGroupsStatistics
	COMMIT
END
GO

---

ALTER PROCEDURE [dbo].[GetVenueCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumTimeBetweenSingleCounterpartySignals_seconds],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[MinimumFillSize],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueMMSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[BidEnabled],
		sett.[AskEnabled],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FairPriceImprovementSpreadBasisPoints],
		sett.[FairPriceImprovementSpreadBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [FairPriceImprovementSpreadDecimal],
		sett.[StopLossNetInUsd],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueStreamSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[PreferLLDestinationCheckToLmax],
		sett.[MinimumBPGrossToAccept],
		sett.[MinimumBPGrossToAccept] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossToAcceptDecimal],
		sett.[PreferLmaxDuringHedging],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[ReferenceLmaxBookOnly],
		sett.[BidEnabled],
		sett.[AskEnabled],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO


ALTER PROCEDURE [dbo].[GetTradingSystemsGroupOveralDailyStatsAndInfo_SP]
(
	@GroupId INT
)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]
	
	SELECT
		[TradingSystemGroupName] AS TradingSystemGroup
		,[TradingSystemGroupId] 
		,[TradingSystemTypeName]
		,[Counterparty]
		,[TradingSystemTypeId]
		,[ConfiguredSystemsCount] AS ConfiguredSystems
		,[EnabledSystemsCount] AS EnabledSystems
		,[HardBlockedSystemsCount] AS HardBlockedSystems
		,[VolumeUsdM]
		,[DealsNum]
		,[LastDealUtc]
		,[KGTRejectionsNum]
		,[LastKgtRejectionUtc]
		,[CtpRejectionsNum]
		,[LastCptRejectionUtc]
		,[PnlGrossUsdPerMUsd]
		,[CommissionsUsdPerMUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsd]
		,[PnlNetUsd]
		,[IsOrderTransmissionDisabled]
		,[IsGoFlatOnlyOn]
		,[PageId]
		,[BeginSymbol]
		,[EndSymbol]
		,[StripsCount]
	FROM 
		[dbo].[PersistedStatisticsPerTradingGroupsDaily]
	WHERE
		[TradingSystemGroupId] = @GroupId
	ORDER BY
		Rank ASC
END
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsOveralDailyStats_SP]
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()

	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]

	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn	
	
	SELECT 
		ISNULL(counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName], 'Unknown') AS SystemName,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		--COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		--this should be called active - and upper stay enabled
		CASE WHEN @TradingOn = 1 
			THEN 
			COALESCE(SUM(CASE WHEN action.[Enabled] = 1 AND action.[GoFlatOnly] = 0 AND action.[OrderTransmissionDisabled] = 0 THEN 1 ELSE 0 END), 0)
			ELSE 
			0 
			END AS EnabledSystems,
		COALESCE(SUM(CONVERT(int, action.[HardBlocked])), 0) AS HardBlockedSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		MAX(stats.[LastDealUtc]) AS LastDealUtc,
		SUM(stats.[KGTRejectionsNum]) AS KGTRejectionsNum,
		MAX(stats.[LastKgtRejectionUtc]) AS LastKgtRejectionUtc,
		SUM(stats.[CtpRejectionsNum]) AS CtpRejectionsNum,
		MAX(stats.[LastCptRejectionUtc]) AS LastCptRejectionUtc,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemType] systemtype_forJoinOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = systemtype_forJoinOnly.[TradingSystemTypeId]
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystemType] systemtype ON systemtype.[TradingSystemTypeId] = COALESCE(trsystem.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId], systemtype_forJoinOnly.[TradingSystemTypeId])
		LEFT JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	GROUP BY
		counterparty.[CounterpartyCode], systemtype.[SystemName]
	ORDER BY
		SystemName
END
GO
  
--ROLLBACK TRANSACTION Install_17_02_001;
--GO
--COMMIT TRANSACTION Install_17_02_001;
--GO















