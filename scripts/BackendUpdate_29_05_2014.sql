

-- rename column
EXEC sp_rename '[dbo].[HotspotMMSettings].[BestPriceImprovementOffset]', 'BestPriceImprovementOffsetBasisPoints', 'COLUMN'
GO

-- fix the values
UPDATE 
	hotmmsettings
SET 
	hotmmsettings.BestPriceImprovementOffsetBasisPoints = (hotmmsettings.BestPriceImprovementOffsetBasisPoints / sym.LastKnownMeanReferencePrice)*10000
FROM 
	[dbo].[HotspotMMSettings] hotmmsettings
	INNER JOIN [dbo].[Symbol] sym on hotmmsettings.SymbolId = sym.Id
GO

-- update the SPs
ALTER PROCEDURE [dbo].[GetUpdateHotspotMMSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'HotspotMMSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		SELECT 
			symbol.Name AS Symbol
			,symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice]
			,[SizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[Enabled]
			,[LastResetRequestedUtc]
			,[LastUpdatedUtc]
		FROM 
			[dbo].[HotspotMMSettings] sett
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		WHERE
			sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
		ORDER BY
			symbol.Name
	END
END
GO 



ALTER PROCEDURE [dbo].[HotMMSettingsUpdateSingle_SP]
(
	@Symbol CHAR(7),
	@LastUpdateTimeUtc DATETIME2,
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit]
)
AS
BEGIN

	DECLARE @SymbolId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	UPDATE
		[dbo].[HotspotMMSettings]
	SET
		[SizeToTrade] = @SizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds,
		[Enabled] = @Enabled
	WHERE
		[SymbolId] = @SymbolId
		AND [LastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO


CREATE TABLE [dbo].[SystemsTradingControl](
	[GoFlatOnly] [bit] NOT NULL
) ON [PRIMARY]
GO

INSERT INTO [dbo].[SystemsTradingControl]
           ([GoFlatOnly])
     VALUES
           (0)
GO


CREATE PROCEDURE [dbo].[FlipSystemsTradingControl_SP]
(
	@GoFlatOnly [bit]
)
AS
BEGIN
	UPDATE [dbo].[SystemsTradingControl]
	SET [GoFlatOnly] = @GoFlatOnly
END
GO

GRANT EXECUTE ON [dbo].[FlipSystemsTradingControl_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[FlipSystemsTradingControl_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetSystemsTradingControl_SP]
AS
BEGIN
	SELECT 
		[GoFlatOnly]
	FROM
		[dbo].[SystemsTradingControl]
END
GO

GRANT EXECUTE ON [dbo].[GetSystemsTradingControl_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetSystemsTradingControl_SP] TO [IntegratorServiceAccount] AS [dbo]
GO