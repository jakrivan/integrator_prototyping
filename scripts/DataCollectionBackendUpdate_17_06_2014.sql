
CREATE USER [IntegratorKillSwitchUser] FOR LOGIN [IntegratorKillSwitchUser] WITH DEFAULT_SCHEMA=[dbo]
GO
GRANT CONNECT TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetSupportedSymbolsForCounterparty_SP] 
(
	@TradingTargetCode [VARCHAR](16)
)
AS
BEGIN

	SELECT 
		pair.FxpCode AS Symbol
	FROM 
		[dbo].[CounterpartySymbolSettings] settings
		INNER JOIN [dbo].[FxPair] pair ON pair.FxPairId = settings.[SymbolId]
		INNER JOIN [dbo].[TradingTargetType] targetType ON targetType.Id = settings.[TradingTargetTypeId]
	WHERE
		targetType.Code = @TradingTargetCode
		AND settings.[Supported] = 1
	ORDER BY
		pair.FxpCode ASC
END
GO

GRANT EXECUTE ON [dbo].[GetSupportedSymbolsForCounterparty_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO



SET IDENTITY_INSERT [dbo].[LiqProviderStream] ON 
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (81, N'LM2', 22)
GO
SET IDENTITY_INSERT [dbo].[LiqProviderStream] OFF
GO


CREATE PROCEDURE [dbo].[GetSupportedCurrencies_SP] 
AS
BEGIN
	SELECT [CurrencyAlphaCode] FROM [dbo].[Currency] WHERE [IsCitibankPbSupported] = 1
END
GO

GRANT EXECUTE ON [dbo].[GetSupportedCurrencies_SP] TO [IntegratorServiceAccount] AS [dbo]
GO



UPDATE
	cs
SET
	[OrderMinimumSize] = 0.01,
	[OrderMinimumSizeIncrement] = 0.01
FROM
	[dbo].[CounterpartySymbolSettings] cs
	inner join [dbo].[TradingTargetType] tt on cs.[TradingTargetTypeId] = tt.id
WHERE
	tt.name = 'BankPool'