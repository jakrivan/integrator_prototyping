
-- temporarily set bank sizes and granularity to match the LMAX requirements
UPDATE
	symInfo
SET
	[OrderMinimumSize] = 1000,
	[OrderMinimumSizeIncrement] = 1000
FROM
	[dbo].[CounterpartySymbolSettings] symInfo
	INNER JOIN [dbo].[TradingTargetType] targetType ON symInfo.TradingTargetTypeId = targetType.Id
WHERE
	targetType.Code = 'BankPool'