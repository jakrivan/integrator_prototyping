
	
UPDATE [dbo].[Settings]
SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '<StartStopEmailSubject>Integrator service on {0} is {1}</StartStopEmailSubject>', '<StartStopEmailSubject>{1} {0} Integrator service</StartStopEmailSubject>')
WHERE
	[Name] = 'Kreslik.Integrator.BusinessLayer.dll'
	AND CAST([SettingsXml] AS NVARCHAR(MAX)) like '%<StartStopEmailSubject>Integrator service on {0} is {1}</StartStopEmailSubject>%'
	
	
DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd]
GO

CREATE TABLE [dbo].[SettlementDate](
	[SymbolId] [tinyint] NOT NULL,
	[SettlementDate] [date] NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_SettlementDate_SymbolId] ON [dbo].[SettlementDate]
(
	[SymbolId] ASC
)
GO

ALTER TABLE [dbo].[SettlementDate]  WITH NOCHECK ADD  CONSTRAINT [FK_SettlementDate_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[SettlementDate] CHECK CONSTRAINT [FK_SettlementDate_Symbol]
GO

INSERT INTO [dbo].[SettlementDate] ([SymbolId], [SettlementDate]) SELECT [Id], NULL FROM [dbo].[Symbol]
GO

CREATE PROCEDURE [dbo].[SettlementDatesClearAll_SP]
AS
BEGIN
	UPDATE
		[dbo].[SettlementDate]
	SET
		[SettlementDate] = NULL
END
GO

GRANT EXECUTE ON [dbo].[SettlementDatesClearAll_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE PROCEDURE [dbo].[SettlementDatesClearSelected_SP]
(
	@SymbolsCsvList varchar(MAX)
)
AS
BEGIN
	UPDATE
		settlDate
	SET
		settlDate.[SettlementDate] = NULL
	FROM
		[dbo].[csvlist_to_codes_tbl](@SymbolsCsvList) symbolNames
		INNER JOIN Symbol sym ON sym.ShortName = symbolNames.Code
		INNER JOIN [dbo].[SettlementDate] settlDate ON settlDate.SymbolId = sym.Id
END
GO

GRANT EXECUTE ON [dbo].[SettlementDatesClearSelected_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


CREATE FUNCTION [dbo].[csvlist_to_codesanddates_tbl] (@list varchar(MAX))
   RETURNS @tbl TABLE (code varchar(6) NOT NULL, dateValue date) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex('|', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1

	  IF @nextpos > @pos + 1
	  BEGIN

		  DECLARE @Code Varchar(6) = substring(@list, @pos + 1, @valuelen)
		  SELECT @pos = @nextpos

		  SELECT @nextpos = charindex('|', @list, @pos + 1)
		  SELECT @valuelen = CASE WHEN @nextpos > 0
								  THEN @nextpos
								  ELSE len(@list) + 1
							 END - @pos - 1

		  DECLARE @DateValue DATE = CONVERT(DATE, substring(@list, @pos + 1, @valuelen))
		  SELECT @pos = @nextpos

		  INSERT @tbl (code, dateValue)
			 VALUES (@Code, @DateValue)
	  END
	END
   RETURN
END
GO

CREATE PROCEDURE [dbo].[SettlementDatesUpdateSelected_SP]
(
	@SymbolsAndDatesList varchar(MAX)
)
AS
BEGIN	
	UPDATE
		settlDate
	SET
		settlDate.[SettlementDate] = dates.dateValue
	FROM
		[dbo].[csvlist_to_codesanddates_tbl](@SymbolsAndDatesList) dates
		INNER JOIN Symbol sym ON sym.ShortName = dates.code
		INNER JOIN [dbo].[SettlementDate] settlDate ON settlDate.SymbolId = sym.Id
END
GO

GRANT EXECUTE ON [dbo].[SettlementDatesUpdateSelected_SP] TO [IntegratorServiceAccount] AS [dbo]
GO






CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd] AS N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RiskManagementSettings" nillable="true" type="RiskManagementSettings" />
  <xs:complexType name="RiskManagementSettings">
    <xs:sequence>
      <xs:element name="ConfigLastUpdated"  nillable="true" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="DisallowTradingOnInstances" type="ArrayOfString" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsRate" type="FatalErrorsRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAndPosition" type="PriceAndPositionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Instance" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersSubmissionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FatalErrorsRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrderMaximumAllowedSizeSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedBaseAbsSizeInUsd" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="PriceAndPositionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumExternalNOPInUSD" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceDeviationCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="OnlinePriceDeviationCheckMaxPriceAge_Minutes" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="Symbols" type="ArrayOfSymbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbol">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="Symbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="Symbol">
    <xs:attribute name="ShortName" type="xs:string" />
    <xs:attribute name="NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
  </xs:complexType>
</xs:schema>'
GO


UPDATE [dbo].[Settings]
SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '</ExternalOrdersRejectionRate>', '</ExternalOrdersRejectionRate>
<FatalErrorsRate>
    <RiskCheckAllowed>true</RiskCheckAllowed>
    <MaximumAllowedInstancesPerTimeInterval>20</MaximumAllowedInstancesPerTimeInterval>
    <TimeIntervalToCheck_Seconds>600</TimeIntervalToCheck_Seconds>
  </FatalErrorsRate>')
WHERE
	[Name] = 'Kreslik.Integrator.RiskManagement.dll'
	AND CAST([SettingsXml] AS NVARCHAR(MAX)) like '%</ExternalOrdersRejectionRate>%'
	
	
	
CREATE TABLE [dbo].[Currency_Rates](
	[CurrencyID] [smallint] NOT NULL,
	[LastKnownUsdConversionMultiplier] [decimal](18, 9) NULL,
	[UsdConversionMultiplierUpdated] [datetime2](7) NULL,
 CONSTRAINT [PK_Currency_Rates] PRIMARY KEY CLUSTERED 
(
	[CurrencyID] ASC
)
) ON [PRIMARY]

ALTER TABLE [dbo].[Currency_Rates]  WITH NOCHECK ADD  CONSTRAINT [FK_Currency_Rates_Currency] FOREIGN KEY([CurrencyID])
REFERENCES [dbo].[Currency] ([CurrencyID])
GO

ALTER TABLE [dbo].[Currency_Rates] CHECK CONSTRAINT [FK_Currency_Rates_Currency]
GO

INSERT INTO [dbo].[Currency_Rates] (CurrencyID, LastKnownUsdConversionMultiplier, UsdConversionMultiplierUpdated)
SELECT CurrencyID, LastKnownUsdConversionMultiplier, UsdConversionMultiplierUpdated FROM [dbo].[Currency]


ALTER TABLE [dbo].[Currency] DROP COLUMN [LastKnownUsdConversionMultiplier]
ALTER TABLE [dbo].[Currency] DROP COLUMN [UsdConversionMultiplierUpdated]
GO

exec sp_rename '[dbo].[Currency]', 'Currency_Internal'
GO

CREATE VIEW [dbo].[Currency]
AS
SELECT
	c.[CurrencyID]


CREATE TABLE [dbo].[Symbol_Rates](
	[Id] [tinyint] NOT NULL,
	[LastKnownMeanReferencePrice] AS (LastKnownAskMeanPrice + LastKnownBidMeanPrice) / 2 PERSISTED,
	[LastKnownAskMeanPrice] [decimal](18, 9),
	[LastKnownBidMeanPrice] [decimal](18, 9),
	[AskMeanPriceUpdated] [datetime2](7) NULL,
	[BidMeanPriceUpdated] [datetime2](7) NULL
 CONSTRAINT [PK_Symbol_Rates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
) ON [PRIMARY]


ALTER TABLE [dbo].[Symbol_Rates]  WITH NOCHECK ADD  CONSTRAINT [FK_Symbol_Rates_Symbol] FOREIGN KEY([Id])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[Symbol_Rates] CHECK CONSTRAINT [FK_Symbol_Rates_Symbol]
GO


INSERT INTO [dbo].[Symbol_Rates] (Id, LastKnownAskMeanPrice, LastKnownBidMeanPrice, AskMeanPriceUpdated, BidMeanPriceUpdated)
SELECT [Id], [LastKnownAskToBMeanPrice], [LastKnownBidToBMeanPrice], [AskToBMeanPriceUpdated], [BidToBMeanPriceUpdated] FROM [dbo].[Symbol]


ALTER TABLE [dbo].[Symbol] DROP COLUMN [LastKnownMeanReferencePrice]
ALTER TABLE [dbo].[Symbol] DROP COLUMN [MeanReferencePriceUpdated]
ALTER TABLE [dbo].[Symbol] DROP COLUMN [LastKnownAskToBMeanPrice]
ALTER TABLE [dbo].[Symbol] DROP COLUMN [AskToBMeanPriceUpdated]
ALTER TABLE [dbo].[Symbol] DROP COLUMN [LastKnownBidToBMeanPrice]
ALTER TABLE [dbo].[Symbol] DROP COLUMN [BidToBMeanPriceUpdated]
GO


exec sp_rename '[dbo].[Symbol]', 'Symbol_Internal'
GO

CREATE VIEW [dbo].[Symbol]
AS
SELECT
	s.[Id]
	,[Name]
	,[ShortName]
	,[BaseCurrencyId]
	,[QuoteCurrencyId]
	,[IsRaboTradeable]
	,[Usage]
	,[LastKnownMeanReferencePrice]
	,CASE WHEN [AskMeanPriceUpdated] > [BidMeanPriceUpdated] THEN [AskMeanPriceUpdated] ELSE [BidMeanPriceUpdated] END AS [MeanReferencePriceUpdated]
	,[LastKnownAskMeanPrice]
	,[AskMeanPriceUpdated]
	,[LastKnownBidMeanPrice]
	,[BidMeanPriceUpdated]
FROM 
	[dbo].[Symbol_Internal] s WITH(NOLOCK)
	INNER JOIN [dbo].[Symbol_Rates] sr WITH(NOLOCK) ON s.Id = sr.Id
GO

DROP PROC [dbo].[UpdateSymbolReferencePrices_SP]
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsDailyStats_SP]
AS
BEGIN

DECLARE @KGTRejectionDirectionId BIT
SELECT @KGTRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'
DECLARE @CtpRejectionDirectionId BIT
SELECT @CtpRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'CtpRejectd'

SELECT
		--BEWARE! There can be multiple symbols for same system id (this is especially true for system -1)
		systemStats.SymbolId AS SymbolId,
		systemStats.TradingSystemId AS TradingSystemId,
		COALESCE(systemStats.NopBasePol, 0) AS NopBasePol,
		COALESCE(systemStats.VolumeInUsd/1000000, 0) AS VolumeUsdM,
		COALESCE(systemStats.DealsNum, 0) as DealsNum,
		COALESCE(systemStats.KGTRejectionsNum, 0) as KGTRejectionsNum,
		COALESCE(systemStats.CtpRejectionsNum, 0) as CtpRejectionsNum,
		COALESCE(systemStats.PnlTermPolInUsd/(systemStats.VolumeInUsd/1000000), 0) AS PnlGrossUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd, 0) AS PnlGrossUsd,
		COALESCE(systemStats.CommissionInUSD/(systemStats.VolumeInUsd/1000000), 0) AS CommissionsUsdPerMUsd,
		COALESCE(systemStats.CommissionInUSD, 0) AS CommissionsUsd,
		COALESCE((systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD)/(systemStats.VolumeInUsd/1000000), 0) AS PnlNetUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD, 0) AS PnlNetUsd
	FROM
		(
		SELECT
			Symbol.Id AS SymbolId,
			systemsStats.TradingSystemId AS TradingSystemId,
			systemsStats.NopBasePol AS NopBasePol,
			systemsStats.DealsNum as DealsNum,
			systemsStats.KGTRejectionsNum as KGTRejectionsNum,
			systemsStats.CtpRejectionsNum as CtpRejectionsNum,
			systemsStats.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd,
			--PnL = term position + yield of immediate close of outstanding base position
			--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
			(systemsStats.NopTermPol + (CASE WHEN systemsStats.NopBasePol < 0 THEN Symbol.LastKnownAskMeanPrice ELSE Symbol.LastKnownBidMeanPrice END) * systemsStats.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
			systemsStats.CommissionInCCY1 * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionInUSD
		FROM
			(
			SELECT
				COALESCE(systemsPositions.[TradingSystemId], systemRejections.[TradingSystemId]) AS [TradingSystemId],
				COALESCE(systemsPositions.[SymbolId], systemRejections.[SymbolId]) AS [SymbolId],
				NopBasePol,
				NopTermPol,
				VolumeInCcy1,
				CommissionInCCY1,
				DealsNum,
				KGTRejectionsNum,
				CtpRejectionsNum
			FROM	
				(
				--allways include stats fro unknown systems
				SELECT 
					-1 AS [TradingSystemId], 
					0 AS [SymbolId],
					NULL AS NopBasePol, 
					NULL AS NopTermPol, 
					NULL AS VolumeInCcy1, 
					NULL AS CommissionInCCY1, 
					NULL AS DealsNum
				UNION ALL
				SELECT 
					[TradingSystemId],
					[SymbolId],
					SUM([AmountBasePolExecuted]) AS NopBasePol,
					SUM(-[AmountBasePolExecuted]* [Price]) AS NopTermPol,
					SUM(ABS([AmountBasePolExecuted])) AS VolumeInCcy1,
					SUM(ABS([AmountBasePolExecuted]) * CommissionPricePerMillion / 1000000) AS CommissionInCCY1,
					COUNT([TradingSystemId]) AS DealsNum
				FROM
					[dbo].[TradingSystemDealsExecuted_Daily] deal
					INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
				GROUP BY
					[TradingSystemId], [SymbolId]
				) systemsPositions
				FULL OUTER JOIN
				(
					SELECT 
						[TradingSystemId],
						[SymbolId],
						--COUNT([TradingSystemId]) AS RejectionsNum
						SUM(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN 1 ELSE 0 END) AS KGTRejectionsNum, 
						SUM(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN 1 ELSE 0 END) AS CtpRejectionsNum
					FROM
						[dbo].[TradingSystemDealsRejected_Daily] rejects
					GROUP BY
						 [TradingSystemId], [SymbolId]
				)  systemRejections ON systemsPositions.TradingSystemId = systemRejections.TradingSystemId AND systemsPositions.[SymbolId] = systemRejections.[SymbolId]
			) systemsStats
			INNER JOIN [dbo].[Symbol] symbol ON systemsStats.SymbolId = symbol.Id
			INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
			INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
			) systemStats
END
GO

ALTER PROCEDURE [dbo].[GetStatisticsPerPair_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	SELECT
		Symbol.Name AS Symbol,
		positions.NopBasePol,
		positions.NopTermPol,
		positions.DealsNum as DealsNum,
		--PnL = term position + yield of immediate close of outstanding base position
		--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
		positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskMeanPrice ELSE Symbol.LastKnownBidMeanPrice END) * positions.NopBasePol AS PnlTermPol,
		(positions.NopTermPol + (CASE WHEN positions.NopBasePol < 0 THEN Symbol.LastKnownAskMeanPrice ELSE Symbol.LastKnownBidMeanPrice END) * positions.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
		positions.VolumeInCcy1 AS VolumeInCcy1,
		positions.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd
	FROM
		(
		SELECT
			deal.SymbolId AS SymbolId,
			SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
			SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
			COUNT(deal.SymbolId) AS DealsNum,
			SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1
		FROM 
			DealExternalExecutedTakersAndMakers_DailyView deal
			
		WHERE
			[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		GROUP BY
			deal.SymbolId
		) positions
		INNER JOIN  [dbo].[Symbol] symbol ON positions.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
	ORDER BY
		symbol.Name
END
GO


DROP PROCEDURE [dbo].[UpdateSymbolToBPrices_SP]
GO

CREATE PROCEDURE [dbo].[UpdateSymbolPrices_SP] (
	@AskPricesList VARCHAR(MAX),
	@BidPricesList VARCHAR(MAX)
	)
AS
BEGIN

	DECLARE @now DATETIME2 = GETUTCDATE()
	
	UPDATE
		sym
	SET
		sym.LastKnownAskMeanPrice = prices.price,
		sym.AskMeanPriceUpdated = @now
	FROM
		[dbo].[csvlist_to_codesandprices_tbl](@AskPricesList) prices
		INNER JOIN Symbol sym on sym.ShortName = prices.Code

	UPDATE
		sym
	SET
		sym.LastKnownBidMeanPrice = prices.price,
		sym.BidMeanPriceUpdated = @now
	FROM
		[dbo].[csvlist_to_codesandprices_tbl](@BidPricesList) prices
		INNER JOIN Symbol sym on sym.ShortName = prices.Code
		
	
	DECLARE @USDId SMALLINT
	SELECT @USDId = [CurrencyID] FROM [dbo].[Currency_Internal] WITH (NOLOCK) WHERE [CurrencyAlphaCode] = 'USD'
	
		
	UPDATE
		dirrectUsdConversion
	SET
		dirrectUsdConversion.LastKnownUsdConversionMultiplier = sym.[LastKnownMeanReferencePrice],
		dirrectUsdConversion.UsdConversionMultiplierUpdated = sym.MeanReferencePriceUpdated
	FROM
		[dbo].[Currency_Rates] dirrectUsdConversion
		INNER JOIN [dbo].[Symbol] sym ON sym.[QuoteCurrencyId] = @USDId AND sym.[BaseCurrencyId] = dirrectUsdConversion.[CurrencyID]
		
	UPDATE
		invertedUsdConversion
	SET
		invertedUsdConversion.LastKnownUsdConversionMultiplier = 1/sym.[LastKnownMeanReferencePrice],
		invertedUsdConversion.UsdConversionMultiplierUpdated = sym.MeanReferencePriceUpdated
	FROM
		[dbo].[Currency_Rates] invertedUsdConversion
		INNER JOIN [dbo].[Symbol] sym ON sym.[BaseCurrencyId] = @USDId AND sym.[QuoteCurrencyId] = invertedUsdConversion.[CurrencyID]

END
GO

GRANT EXECUTE ON [dbo].[UpdateSymbolPrices_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

DROP PROCEDURE [dbo].[UpdateUsdConversionMultipliers_SP]
GO


------------------------------------------------------------------------------
--       Yet to be updated in prod
------------------------------------------------------------------------------


UPDATE [dbo].[Settings]
SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '<Counterparty>FS1</Counterparty>', '')
WHERE
	[Name] = 'Kreslik.Integrator.SessionManagement.dll'
	AND CAST([SettingsXml] AS NVARCHAR(MAX)) like '%<Counterparty>FS1</Counterparty>%'
	
UPDATE [dbo].[Settings]
SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '<Counterparty>FC1</Counterparty>', '')
WHERE
	[Name] = 'Kreslik.Integrator.SessionManagement.dll'
	AND CAST([SettingsXml] AS NVARCHAR(MAX)) like '%<Counterparty>FC1</Counterparty>%'

UPDATE [dbo].[Settings]
SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '<Counterparty>FC2</Counterparty>', '')
WHERE
	[Name] = 'Kreslik.Integrator.SessionManagement.dll'
	AND CAST([SettingsXml] AS NVARCHAR(MAX)) like '%<Counterparty>FC2</Counterparty>%'

	
UPDATE [dbo].[Settings]
SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '<Counterparty>LM3</Counterparty>', '<Counterparty>LM3</Counterparty><Counterparty>FC1</Counterparty><Counterparty>FC2</Counterparty><Counterparty>FS1</Counterparty>')
WHERE
	[Name] = 'Kreslik.Integrator.SessionManagement.dll'
	AND CAST([SettingsXml] AS NVARCHAR(MAX)) like '%<Counterparty>LM3</Counterparty>%'


INSERT INTO [dbo].[DealExternalExecutedTicketType]
           ([DealExternalExecutedTicketTypeId]
           ,[DealExternalExecutedTicketTypeName])
     VALUES
           (8
           ,'DeletedByTraiana')
GO


DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_DBK" type="FIXChannel_DBKSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM2" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM3" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC1" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC2" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS1" type="FIXChannel_FCMMMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
      <xs:element minOccurs="1" maxOccurs="1" name="STP" type="STPSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_DBKSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DealConfirmationSupported" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumDealConfirmationDelay_Milliseconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IocApplyTtl" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ApplyMaxHoldTime" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxHoldTime" type="FXCMMaxHoldTime" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MaxPerSymbolPricesPerSecondSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultMaxAllowedPerSecondRate" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialSymbolRateSettings" type="ArrayOfSymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolRateSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="SymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolRateSetting">
    <xs:attribute name="Name" type="xs:string" />
    <xs:attribute name="MaxAllowedPerSecondRate" type="xs:int" use="required" />
  </xs:complexType>
  <xs:complexType name="RejectionRateDisconnectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumOrdersToApplyCheck" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAllowedRejectionRatePercent" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMMMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxOrderHoldTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxTotalPricesPerSecond" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxPerSymbolPricesPerSecondSetting" type="MaxPerSymbolPricesPerSecondSetting" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExpectedPricingDepth" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="RejectionRateDisconnectSettings" type="RejectionRateDisconnectSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXSTPChannel_CTIPBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNongenuineInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialRejectionsHandling" type="ArrayOfSpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SpecialRejection" type="SpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="TriggerPhrase" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="FXCMMaxHoldTime">
    <xs:restriction base="xs:string">
      <xs:enumeration value="UpTo1ms" />
      <xs:enumeration value="UpTo30ms" />
      <xs:enumeration value="UpTo100ms" />
      <xs:enumeration value="UpTo500ms" />
      <xs:enumeration value="UpTo3000ms" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="MarketSessionIntoSeparateProcess" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="STPDestinations" type="ArrayOfSTPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSTPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="STPDestination" nillable="true" type="STPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Name" type="STPCounterparty" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedTicketTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="ServedCounterparties" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="STPCounterparty">
    <xs:restriction base="xs:string">
      <xs:enumeration value="CTIPB" />
      <xs:enumeration value="Traiana" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO


--UPDATE [dbo].[Settings]
--SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '<MaxPricesPerSecond>200</MaxPricesPerSecond>', '<MaxTotalPricesPerSecond>200</MaxTotalPricesPerSecond><MaxPerSymbolPricesPerSecond>30</MaxPerSymbolPricesPerSecond>')
--WHERE
--	[Name] = 'Kreslik.Integrator.SessionManagement.dll'
--	AND CAST([SettingsXml] AS NVARCHAR(MAX)) like '%<MaxPricesPerSecond>200</MaxPricesPerSecond>%'
--


UPDATE [dbo].[Settings]
SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '<MaxOrderHoldTime_Milliseconds>300</MaxOrderHoldTime_Milliseconds>', '<MaxOrderHoldTime_Milliseconds>100</MaxOrderHoldTime_Milliseconds>')
WHERE
	CAST([SettingsXml] AS NVARCHAR(MAX)) like '%<MaxOrderHoldTime_Milliseconds>300</MaxOrderHoldTime_Milliseconds>%'
GO
	
UPDATE [dbo].[Settings]
SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '<MaxPricesPerSecond>200</MaxPricesPerSecond>', '<MaxTotalPricesPerSecond>200</MaxTotalPricesPerSecond>
<MaxPerSymbolPricesPerSecondSetting>
	<DefaultMaxAllowedPerSecondRate>15</DefaultMaxAllowedPerSecondRate>
	<SpecialSymbolRateSettings>
		<Symbol Name="AUD/CAD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="AUD/CHF" MaxAllowedPerSecondRate="30" />
		<Symbol Name="AUD/JPY" MaxAllowedPerSecondRate="30" />
		<Symbol Name="AUD/NOK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="AUD/NZD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="AUD/SEK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="AUD/USD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="CAD/CHF" MaxAllowedPerSecondRate="30" />
		<Symbol Name="CAD/JPY" MaxAllowedPerSecondRate="30" />
		<Symbol Name="CAD/NOK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="CAD/NZD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="CAD/SEK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="CHF/JPY" MaxAllowedPerSecondRate="30" />
		<Symbol Name="CHF/NOK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="CHF/SEK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="EUR/AUD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="EUR/CAD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="EUR/CHF" MaxAllowedPerSecondRate="30" />
		<Symbol Name="EUR/GBP" MaxAllowedPerSecondRate="30" />
		<Symbol Name="EUR/JPY" MaxAllowedPerSecondRate="30" />
		<Symbol Name="EUR/NOK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="EUR/NZD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="EUR/SEK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="EUR/USD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="GBP/AUD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="GBP/CAD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="GBP/CHF" MaxAllowedPerSecondRate="30" />
		<Symbol Name="GBP/JPY" MaxAllowedPerSecondRate="30" />
		<Symbol Name="GBP/NOK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="GBP/NZD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="GBP/SEK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="GBP/USD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="NOK/JPY" MaxAllowedPerSecondRate="30" />
		<Symbol Name="NOK/SEK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="NZD/CAD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="NZD/CHF" MaxAllowedPerSecondRate="30" />
		<Symbol Name="NZD/JPY" MaxAllowedPerSecondRate="30" />
		<Symbol Name="NZD/NOK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="NZD/SEK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="NZD/USD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="USD/CAD" MaxAllowedPerSecondRate="30" />
		<Symbol Name="USD/CHF" MaxAllowedPerSecondRate="30" />
		<Symbol Name="USD/JPY" MaxAllowedPerSecondRate="30" />
		<Symbol Name="USD/NOK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="USD/SEK" MaxAllowedPerSecondRate="30" />
		<Symbol Name="SEK/JPY" MaxAllowedPerSecondRate="30" />
	</SpecialSymbolRateSettings>
</MaxPerSymbolPricesPerSecondSetting>')
WHERE
	[Name] = 'Kreslik.Integrator.SessionManagement.dll'
	AND CAST([SettingsXml] AS NVARCHAR(MAX)) like '%<MaxPricesPerSecond>200</MaxPricesPerSecond>%'
GO
	
	

	
	
INSERT INTO [dbo].[PriceDelaySettings] ([CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks])
SELECT cpt.[CounterpartyId], [ExpectedDelayTicks], [MaxAllowedAdditionalDelayTicks] 
FROM [dbo].[PriceDelaySettings] sett
INNER JOIN [dbo].[Counterparty] cpt ON cpt.[CounterpartyCode] = 'FS1'
WHERE sett.[CounterpartyId] = (SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FC1')
GO

ALTER TABLE [dbo].[PriceDelaySettings] DROP COLUMN [MaxAllowedAdditionalDelay]
GO
ALTER TABLE [dbo].[PriceDelaySettings] ADD [MaxAllowedAdditionalDelay] AS (dateadd(nanosecond,[ExpectedDelayTicks]%10,dateadd(microsecond,[MaxAllowedAdditionalDelayTicks]/10,CONVERT([time](7),'00:00:00'))))
GO

INSERT INTO [dbo].[PriceDelayStats]
           ([CounterpartyId]
           ,[TotalReceivedPricesInLastMinute]
           ,[DelayedPricesInLastMinute]
           ,[TotalReceivedPricesInLastHour]
           ,[DelayedPricesInLastHour])
SELECT cpt.[CounterpartyId], 0, 0, 0, 0 
FROM [dbo].[Counterparty] cpt WHERE cpt.[CounterpartyCode] = 'FS1'
GO


ALTER PROCEDURE [dbo].[VenueStreamSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	DECLARE @MaxAllowedSystemsCount INT = 3
	DECLARE @FormedSystemsCount INT
	SELECT @FormedSystemsCount = COUNT([SymbolId])
	FROM [dbo].[VenueStreamSettings]
	WHERE [SymbolId] =  @SymbolId 

	IF @FormedSystemsCount >= @MaxAllowedSystemsCount
	BEGIN
		RAISERROR (N'Attempt to form more than %d systems for symbol %s - Disallowed', 16, 2, @MaxAllowedSystemsCount, @Symbol) WITH SETERROR
		RETURN
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Stream'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Stream TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueStreamSettings]
           ([SymbolId]
		   ,[CounterpartyId]
           ,[TradingSystemId]
           ,[SizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
		   ,[KGTLLTime_milliseconds]
		   ,[PreferLLDestinationCheckToLmax]
		   ,[MinimumBPGrossToAccept]
		   ,[PreferLmaxDuringHedging])
     VALUES
           (@SymbolId
		   ,@CounterpartyId
           ,@TradingSystemId
           ,@SizeToTrade 
           ,@BestPriceImprovementOffsetBasisPoints
           ,@MaximumDiscountBasisPointsToTrade
		   ,@KGTLLTime_milliseconds
		   ,@PreferLLDestinationCheckToLmax
		   ,@MinimumBPGrossToAccept
		   ,@PreferLmaxDuringHedging)
END
GO


INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[SettingsVersion]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           ('FC1_QUO_traiana'
           ,1
           ,'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=MDKGTNLLN1

SSLEnable=N

[SESSION]
BypassParsing=Y
UseDataDictionary=N
#DataDictionary=FIX42_FCM.xml
BeginString=FIX.4.2
TargetCompID=PROFMLD4
#5pm NYK
StartTime=17:30:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30
PoolBuffers=Y

SocketConnectPort=16957
SocketConnectHost=169.33.101.137'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[SettingsVersion]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           ('FC1_ORD_traiana'
           ,1
           ,'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=TRKGTNLLN1

SSLEnable=N

[SESSION]
BypassParsing=Y
UseDataDictionary=N
#DataDictionary=FIX42_FCM.xml
BeginString=FIX.4.2
TargetCompID=PROFMLD4
#5pm NYK
StartTime=17:30:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=2

SocketConnectPort=11286
SocketConnectHost=169.33.101.137'
           ,GETUTCDATE())
GO




INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[SettingsVersion]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           ('FC2_QUO_traiana'
           ,1
           ,'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=MDKGTNL1

SSLEnable=N

[SESSION]
BypassParsing=Y
UseDataDictionary=N
#DataDictionary=FIX42_FCM.xml
BeginString=FIX.4.2
TargetCompID=Fastmatch1
#5pm NYK
StartTime=17:30:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30
PoolBuffers=Y

SocketConnectPort=16151
SocketConnectHost=169.33.101.137'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[SettingsVersion]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           ('FC2_ORD_traiana'
           ,1
           ,'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=TRKGTNL1

SSLEnable=N

[SESSION]
BypassParsing=Y
UseDataDictionary=N
#DataDictionary=FIX42_FCM.xml
BeginString=FIX.4.2
TargetCompID=Fastmatch1
#5pm NYK
StartTime=17:30:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=2

SocketConnectPort=10751
SocketConnectHost=169.33.101.137'
           ,GETUTCDATE())
GO




DECLARE @FCQuoFixSettingId INT
DECLARE @FCOrdFixSettingId INT

SELECT
	@FCQuoFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'FC1_QUO_traiana'

SELECT
	@FCOrdFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'FC1_ORD_traiana'


INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [Id], @FCQuoFixSettingId  
FROM 
	[dbo].[IntegratorEnvironment]
WHERE 
	[IntegratorEnvironmentTypeId] = (SELECT [Id] FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = 'IntegratorService')	
	
INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [Id], @FCOrdFixSettingId  
FROM 
	[dbo].[IntegratorEnvironment]
WHERE 
	[IntegratorEnvironmentTypeId] = (SELECT [Id] FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = 'IntegratorService')	

	
	
SELECT
	@FCQuoFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'FC2_QUO_traiana'

SELECT
	@FCOrdFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'FC2_ORD_traiana'


INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [Id], @FCQuoFixSettingId  
FROM 
	[dbo].[IntegratorEnvironment]
WHERE 
	[IntegratorEnvironmentTypeId] = (SELECT [Id] FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = 'IntegratorService')	
	
INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [Id], @FCOrdFixSettingId  
FROM 
	[dbo].[IntegratorEnvironment]
WHERE 
	[IntegratorEnvironmentTypeId] = (SELECT [Id] FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = 'IntegratorService')	
	
	
	
---------------

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[SettingsVersion]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           ('FS1_QUO_traiana'
           ,1
           ,'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGTNLLNLL_LD4_PRICE

[SESSION]
DataDictionary=FIX42_FCM.xml
BeginString=FIX.4.2
TargetCompID=FASTMATCH_LD4_PRICE
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=11857
SocketConnectHost=169.33.101.137'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[SettingsVersion]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           ('FS1_ORD_traiana'
           ,1
           ,'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGTNLLNLL_LD4_TRADE

[SESSION]
DataDictionary=FIX42_FCM.xml
RelaxValidationForMessageTypes=D,Q
BeginString=FIX.4.2
TargetCompID=FASTMATCH_LD4_TRADE
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=2

SocketConnectPort=11858
SocketConnectHost=169.33.101.137'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[SettingsVersion]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           ('FS1_QUO_citi'
           ,1
           ,'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGTLNLL_LD4_PRICE

[SESSION]
DataDictionary=FIX42_FCM.xml
BeginString=FIX.4.2
TargetCompID=FASTMATCH_LD4_PRICE
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=11859
SocketConnectHost=169.33.101.137'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[SettingsVersion]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           ('FS1_ORD_citi'
           ,1
           ,'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGTLNLL_LD4_TRADE

[SESSION]
DataDictionary=FIX42_FCM.xml
BeginString=FIX.4.2
TargetCompID=FASTMATCH_LD4_TRADE
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=11860
SocketConnectHost=169.33.101.137'
           ,GETUTCDATE())
GO

DECLARE @FCQuoFixSettingId INT
DECLARE @FCOrdFixSettingId INT

SELECT
	@FCQuoFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'FS1_QUO_citi'

SELECT
	@FCOrdFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'FS1_ORD_citi'


INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [Id], @FCQuoFixSettingId  
FROM 
	[dbo].[IntegratorEnvironment]
WHERE 
	[IntegratorEnvironmentTypeId] = (SELECT [Id] FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = 'IntegratorService')	
	
INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [Id], @FCOrdFixSettingId  
FROM 
	[dbo].[IntegratorEnvironment]
WHERE 
	[IntegratorEnvironmentTypeId] = (SELECT [Id] FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = 'IntegratorService')	


SELECT
	@FCQuoFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'FS1_QUO_traiana'

SELECT
	@FCOrdFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'FS1_ORD_traiana'


INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [Id], @FCQuoFixSettingId  
FROM 
	[dbo].[IntegratorEnvironment]
WHERE 
	[IntegratorEnvironmentTypeId] = (SELECT [Id] FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = 'IntegratorService')	
	
INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [Id], @FCOrdFixSettingId  
FROM 
	[dbo].[IntegratorEnvironment]
WHERE 
	[IntegratorEnvironmentTypeId] = (SELECT [Id] FROM [dbo].[IntegratorEnvironmentType] WHERE [Name] = 'IntegratorService')	

	

---------------
	
	
	
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] ADD CounterpartyClientId VARCHAR(20) NULL
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] ADD CounterpartyClientId VARCHAR(20) NULL
GO

ALTER PROCEDURE [dbo].[Daily_ArchiveRejectableOrders_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[OrderExternalIncomingRejectable_Archive]
			([IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReason])
		SELECT
			[IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReason]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]	
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[OrderExternalIncomingRejectable_Daily]
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[InsertNewOrderExternalIncomingRejectable_SP]( 
    @ExternalOrderReceived [datetime2](7),
	@ExternalOrderSentUtc [datetime2](7),
	@IntegratorSentExecutionReportUtc [datetime2](7),
	@CounterpartyClientOrderId [varchar](32),
	@CounterpartyClientId [varchar](20),
	@TradingSystemId int = NULL,
	@SourceIntegratorPriceIdentity [varchar](20) = NULL,
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@IntegratorExecId [varchar](20),
	@ValueDate [date] = NULL,
	@OrderReceivedToExecutionReportSentLatency [time](7),
	@IntegratorExecutedAmountBaseAbs [decimal](18, 2) = NULL,
	@IntegratorRejectedAmountBaseAbs [decimal](18, 2) = NULL,
	@RejectionReason [varchar](32) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WITH(NOLOCK) WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WITH(NOLOCK) WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WITH(NOLOCK) WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @IntegratorExecutedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,@IntegratorExecId)
		END
		
		IF @IntegratorRejectedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,ISNULL(@RejectionReason, '<NONE>')
				,@IntegratorExecId
				,@RejectionDirectionId)
		END

		INSERT INTO 
			[dbo].[OrderExternalIncomingRejectable_Daily]
			   ([IntegratorReceivedExternalOrderUtc]
			   ,[CounterpartySentExternalOrderUtc]
			   ,[IntegratorSentExecutionReportUtc]
			   ,[CounterpartyClientOrderId]
			   ,[CounterpartyClientId]
			   ,[TradingSystemId]
			   ,[SourceIntegratorPriceIdentity]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorDealDirectionId]
			   ,[CounterpartyRequestedPrice]
			   ,[CounterpartyRequestedAmountBasePol]
			   ,[IntegratorExecutionId]
			   ,[ValueDate]
			   ,[OrderReceivedToExecutionReportSentLatency ]
			   ,[IntegratorExecutedAmountBasePol]
			   ,[IntegratorRejectedAmountBasePol]
			   ,[IntegratorRejectionReason])
		 VALUES
			   (@ExternalOrderReceived
			   ,@ExternalOrderSentUtc
			   ,@IntegratorSentExecutionReportUtc
			   ,@CounterpartyClientOrderId
			   ,@CounterpartyClientId
			   ,@TradingSystemId
			   ,@SourceIntegratorPriceIdentity
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@DealDirectionId
			   ,@RequestedPrice
			   ,CASE WHEN @DealDirectionId = 1 THEN -@RequestedAmountBaseAbs ELSE @RequestedAmountBaseAbs END
			   ,@IntegratorExecId
			   ,@ValueDate
			   ,@OrderReceivedToExecutionReportSentLatency 
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
		       ,@RejectionReason)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH	   
		   	   
END
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] DROP CONSTRAINT [FK_OrderExternalIncomingRejectable_Archive_TradingSystem]
GO
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] DROP CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_TradingSystem]
GO


------
-- use bypass parsing everywhere
-----
UPDATE 
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], 'BypassParsing=N', 'BypassParsing=Y')
WHERE
	[Configuration] LIKE '%BypassParsing=N%'
	AND [Name]  NOT LIKE 'FS%' AND [Name] LIKE '___[_]___'
GO

UPDATE 
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], '[SESSION]', '[SESSION]
#BypassParsing=Y')
WHERE
	[Configuration] NOT LIKE '%BypassParsing=Y%'
	AND [Name]  NOT LIKE 'FS%' AND [Name] LIKE '___[_]___'
GO

UPDATE 
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], '#BypassParsing=Y', 'BypassParsing=Y')
WHERE
	[Configuration] LIKE '%#BypassParsing=Y%'
	AND [Name]  NOT LIKE 'FS%' AND [Name] LIKE '___[_]___'
GO



------
-- do not use dictionaries everywhere
-----
UPDATE 
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], 'UseDataDictionary=Y', 'UseDataDictionary=N')
WHERE
	[Configuration] LIKE '%UseDataDictionary=Y%'
	AND [Name]  NOT LIKE 'FS%' AND [Name] LIKE '___[_]___'
GO

UPDATE 
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], 'BypassParsing=Y', 'BypassParsing=Y
#UseDataDictionary=N')
WHERE
	[Configuration] NOT LIKE '%UseDataDictionary=N%'
	AND [Name]  NOT LIKE 'FS%' AND [Name] LIKE '___[_]___'
GO

UPDATE 
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], '#UseDataDictionary=N', 'UseDataDictionary=N')
WHERE
	[Configuration] LIKE '%#UseDataDictionary=N%'
	AND [Name]  NOT LIKE 'FS%' AND [Name] LIKE '___[_]___'
GO

UPDATE 
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], 'DataDictionary=F', '#DataDictionary=F')
WHERE
	[Configuration] LIKE '%DataDictionary=F%' AND [Configuration] NOT LIKE '%#DataDictionary=F%' 
	AND [Name]  NOT LIKE 'FS%' AND [Name] LIKE '___[_]___'
GO




UPDATE 
	[dbo].[Settings_FIX]
SET
	[Name] = [Name] + '_discontinued'
WHERE
	[Name] LIKE 'BRX%' OR [Name] LIKE 'RBS%' OR [Name] LIKE 'UBS%'
GO

UPDATE [dbo].[Settings]
SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '</Counterparts>', '<CounterpartSetting>
          <ShortName>BRX</ShortName>
          <SettingAction>Remove</SettingAction>
        </CounterpartSetting><CounterpartSetting>
          <ShortName>RBS</ShortName>
          <SettingAction>Remove</SettingAction>
        </CounterpartSetting><CounterpartSetting>
          <ShortName>UBS</ShortName>
          <SettingAction>Remove</SettingAction>
        </CounterpartSetting></Counterparts>')
WHERE
	[Name] = 'Kreslik.Integrator.SessionManagement.dll'
	AND CAST([SettingsXml] AS NVARCHAR(MAX)) like '%</Counterparts>%'
	