
BEGIN TRANSACTION OrdersBackendSchemaUpdates2 WITH MARK N'External orders tables, stored procedures';

--
-- DealExternalExecuted table
--

-- Table
ALTER TABLE DealExternalExecuted  ADD IsPrimaryDealFromQuoted bit NULL
GO

-- Stored Procedure
ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolExecuted decimal(18, 0),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@IsPrimaryDealFromQuoted bit = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END

	INSERT INTO 
	[dbo].[DealExternalExecuted]
           ([IntegratorReceivedExecutionReportUtc]
           ,[CounterpartyId]
           ,[SymbolId]
           ,[IntegratorExternalOrderPrivateId]
           ,[DealDirectionId]
           ,[AmountBasePolExecuted]
           ,[Price]
           ,[CounterpartyTransactionId]
           ,[CounterpartySuppliedExecutionTimeStampUtc]
           ,[ValueDateCounterpartySuppliedLocMktDate]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[CounterpartySentExecutionReportUtc]
		   ,[IsPrimaryDealFromQuoted])
     VALUES
           (@ExecutionReportReceivedUtc
           ,@CounterpartyId
           ,@SymbolId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolExecuted
           ,@Price
           ,@CounterpartyTransactionId
           ,@CounterpartyExecutionReportTimeStampUtc
           ,@ValueDateCounterpartySuppliedLocMktDate
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@CounterpartySentExecutionReportUtc
		   ,@IsPrimaryDealFromQuoted)
END
GO

--
-- End of DealExternalExecuted table
--


--
-- DealExternalRejected table
--

-- Table
ALTER TABLE DealExternalRejected  ADD IsPrimaryRejectionFromQuoted bit NULL
GO

-- Stored Procedure
ALTER PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 0),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@IsPrimaryRejectionFromQuoted bit = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[DealExternalRejected]
           ([IntegratorReceivedExecutionReportUtc]
           ,[SymbolId]
           ,[CounterpartyId]
           ,[IntegratorExternalOrderPrivateId]
		   ,[DealDirectionId]
           ,[AmountBasePolRejected]
           ,[OrderSentToExecutionReportReceivedLatency]
           ,[RejectionReason]
		   ,[CounterpartySentExecutionReportUtc]
		   ,[IsPrimaryRejectionFromQuoted])
     VALUES
           (@ExecutionReportReceivedUtc
		   ,@SymbolId
           ,@CounterpartyId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolRejected
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@RejectionReason
		   ,@CounterpartySentExecutionReportUtc
		   ,@IsPrimaryRejectionFromQuoted)
END
GO

--
-- End of DealExternalRejected table
--

-- In case we would detect errors
-- IF *** ROLLBACK TRANSACTION OrdersBackendSchemaUpdates

COMMIT TRANSACTION OrdersBackendSchemaUpdates2;
GO

--
-- Data reconstruction
--

-- No data reconstruction is possible