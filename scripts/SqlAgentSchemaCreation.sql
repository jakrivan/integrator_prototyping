
/****** Object:  Operator [kgt support]    Script Date: 17. 1. 2014 17:55:16 ******/
EXEC msdb.dbo.sp_add_operator @name=N'kgt support', 
		@enabled=1, 
		@weekday_pager_start_time=90000, 
		@weekday_pager_end_time=180000, 
		@saturday_pager_start_time=90000, 
		@saturday_pager_end_time=180000, 
		@sunday_pager_start_time=90000, 
		@sunday_pager_end_time=180000, 
		@pager_days=0, 
		@email_address=N'support@kgtinv.com', 
		@category_name=N'[Uncategorized]'
GO


USE msdb ;
GO
EXEC dbo.sp_add_job
    @job_name = N'Daily Creation of Current Day Views',
	@enabled=1, 
	@notify_level_eventlog=2, 
	@notify_level_email=2, 
	@notify_level_netsend=0, 
	@notify_level_page=0, 
	@delete_level=0, 
	@notify_email_operator_name=N'kgt support';
GO
EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'Add OrderExternal Daily View',
    @subsystem = N'TSQL',
    @command = N'exec CreateDailyOrdersView_SP',
	@database_name = N'Integrator',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO
EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'Add DealExternalRejected Daily View',
    @subsystem = N'TSQL',
    @command = N'exec CreateDailyRejectionsView_SP',
	@database_name = N'Integrator',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO
EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'Add DealExternalExecuted Daily View',
    @subsystem = N'TSQL',
    @command = N'exec CreateDailyDealsView_SP',
	@database_name = N'Integrator',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO
EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'Check Steps Status',
    @subsystem = N'TSQL',
    @command = N'
	SELECT  step_name, message
	FROM    msdb.dbo.sysjobhistory
	WHERE   instance_id > COALESCE((SELECT MAX(instance_id) FROM msdb.dbo.sysjobhistory
									WHERE job_id = $(ESCAPE_SQUOTE(JOBID)) AND step_id = 0), 0)
			AND job_id = $(ESCAPE_SQUOTE(JOBID))
			AND run_status <> 1 -- success

	IF      @@ROWCOUNT <> 0
			RAISERROR(''One of steps failed'', 16, 1)
	',
    @retry_attempts = 0,
    @retry_interval = 0;
GO

EXEC dbo.sp_add_schedule
    @schedule_name = N'RunDailyAfterUtcMidnight',
	--Daily
    @freq_type = 4,
	--Every one day
	@freq_interval = 1,
	--HHMMSS
    @active_start_time = 000030 ;
GO
USE msdb ;
GO
EXEC sp_attach_schedule
   @job_name = N'Daily Creation of Current Day Views',
   @schedule_name = N'RunDailyAfterUtcMidnight';
GO
EXEC dbo.sp_add_jobserver
    @job_name = N'Daily Creation of Current Day Views';
GO




USE msdb ;
GO
EXEC dbo.sp_add_job
    @job_name = N'Ongoing Refresh of Persited Statistics',
	@enabled=1, 
	@notify_level_eventlog=2, 
	@notify_level_email=2, 
	@notify_level_netsend=0, 
	@notify_level_page=0, 
	@delete_level=0,  
	@notify_email_operator_name=N'kgt support';
GO
EXEC sp_add_jobstep
    @job_name = N'Ongoing Refresh of Persited Statistics',
    @step_name = N'Refresh Counterparty Statistics',
    @subsystem = N'TSQL',
    @command = N'exec [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]',
	@database_name = N'Integrator',
    @retry_attempts = 0,
    @retry_interval = 0;
GO

EXEC dbo.sp_add_schedule
    @schedule_name = N'RunEvery2Minutes',
	--Daily
    @freq_type = 4,
	--Every one day
	@freq_interval = 1,
	-- seconds
	@freq_subday_type = 2,
	-- every 120 seconds
	@freq_subday_interval= 120
	--no need for active start time and end time (and dates) => runs allways
GO
USE msdb ;
GO
EXEC sp_attach_schedule
   @job_name = N'Ongoing Refresh of Persited Statistics',
   @schedule_name = N'RunEvery2Minutes';
GO
EXEC dbo.sp_add_jobserver
    @job_name = N'Ongoing Refresh of Persited Statistics';
GO






