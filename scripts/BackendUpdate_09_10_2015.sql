BEGIN TRANSACTION AAA

INSERT INTO [dbo].[TradingSystemType_Internal] ([CounterpartyId], [SystemName], [Enabled])
   VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FA1'), 'SmartCross', 1)
INSERT INTO [dbo].[TradingSystemType_Internal] ([CounterpartyId], [SystemName], [Enabled])
   VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LM2'), 'SmartCross', 1)

ALTER TABLE [dbo].[VenueSmartCrossSettings] ADD MinimumTrueGainGrossBasisPoints [decimal](18,6) NULL
ALTER TABLE [dbo].[VenueSmartCrossSettings_Changes] ADD MinimumTrueGainGrossBasisPoints [decimal](18,6) NULL
GO
UPDATE [dbo].[VenueSmartCrossSettings] SET [MinimumTrueGainGrossBasisPoints] = [CoverDistanceGrossBasisPoints]
UPDATE [dbo].[VenueSmartCrossSettings_Changes] SET [MinimumTrueGainGrossBasisPoints] = [CoverDistanceGrossBasisPoints]
GO
ALTER TABLE [dbo].[VenueSmartCrossSettings] ALTER COLUMN [MinimumTrueGainGrossBasisPoints] [decimal](18,6) NOT NULL
ALTER TABLE [dbo].[VenueSmartCrossSettings_Changes] ALTER COLUMN [MinimumTrueGainGrossBasisPoints] [decimal](18,6) NOT NULL
GO

ALTER TRIGGER [dbo].[VenueSmartCrossSettings_DeletedOrUpdated] ON [dbo].[VenueSmartCrossSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueSmartCrossSettings_Changes]
	   ([SymbolId]
	   ,[MinimumSizeToTrade]
	   ,[MaximumSizeToTrade]
	   ,[MinimumDiscountBasisPointsToTrade]
	   ,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
	   ,[MinimumFillSize]
		,[CoverDistanceGrossBasisPoints]
		,[CoverTimeout_milliseconds]
		,[MinimumTrueGainGrossBasisPoints]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[MinimumSizeToTrade]
		,deleted.[MaximumSizeToTrade]
		,deleted.[MinimumDiscountBasisPointsToTrade]
		,deleted.[MinimumTimeBetweenSingleCounterpartySignals_seconds]
		,deleted.[MinimumFillSize]
		,deleted.[CoverDistanceGrossBasisPoints]
		,deleted.[CoverTimeout_milliseconds]
		,deleted.[MinimumTrueGainGrossBasisPoints]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueSmartCrossSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueCrossSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([MinimumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MinimumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MinimumDiscountBasisPointsToTrade] != deleted.[MinimumDiscountBasisPointsToTrade]))
		OR (UPDATE([MinimumTimeBetweenSingleCounterpartySignals_seconds]) AND (topUpdatedChange.[MinimumTimeBetweenSingleCounterpartySignals_seconds] IS NULL OR topUpdatedChange.[MinimumTimeBetweenSingleCounterpartySignals_seconds] != deleted.[MinimumTimeBetweenSingleCounterpartySignals_seconds]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([CoverDistanceGrossBasisPoints]) AND (topUpdatedChange.[CoverDistanceGrossBasisPoints] IS NULL OR topUpdatedChange.[CoverDistanceGrossBasisPoints] != deleted.[CoverDistanceGrossBasisPoints]))
		OR (UPDATE([CoverTimeout_milliseconds]) AND (topUpdatedChange.[CoverTimeout_milliseconds] IS NULL OR topUpdatedChange.[CoverTimeout_milliseconds] != deleted.[CoverTimeout_milliseconds]))
		OR (UPDATE([MinimumTrueGainGrossBasisPoints]) AND (topUpdatedChange.[MinimumTrueGainGrossBasisPoints] IS NULL OR topUpdatedChange.[MinimumTrueGainGrossBasisPoints] != deleted.[MinimumTrueGainGrossBasisPoints]))
END
GO

ALTER PROCEDURE [dbo].[GetLastVenueSmartCrossSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MinimumSizeToTrade]
		,[MaximumSizeToTrade]
		,[MinimumDiscountBasisPointsToTrade]
		,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
		,[MinimumFillSize]
		,[CoverDistanceGrossBasisPoints]
		,[CoverTimeout_milliseconds]
		,[MinimumTrueGainGrossBasisPoints]
	FROM 
		[dbo].[VenueSmartCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueSmartCrossSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueSmartCrossSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
			,[MinimumFillSize]
			,[CoverDistanceGrossBasisPoints]
			,[CoverTimeout_milliseconds]
			,[MinimumTrueGainGrossBasisPoints]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueSmartCrossSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name
	END
END
GO

ALTER PROCEDURE [dbo].[GetVenueSmartCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumDiscountBasisPointsToTrade] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumDiscountToTradeDecimal],
		sett.[MinimumTimeBetweenSingleCounterpartySignals_seconds],
		sett.[MinimumFillSize],
		sett.[CoverDistanceGrossBasisPoints],
		sett.[CoverDistanceGrossBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [CoverDistanceGrossDecimal],	
		sett.[CoverTimeout_milliseconds],
		sett.[MinimumTrueGainGrossBasisPoints],
		sett.[MinimumTrueGainGrossBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumTrueGainGrossDecimal],	
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueSmartCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[VenueSmartCrossSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@MinimumSizeToTrade [decimal](18, 0),
	@MaximumSizeToTrade [decimal](18, 0),
	@MinimumDiscountBasisPointsToTrade [decimal](18, 6),
	@MinimumTimeBetweenSingleCounterpartySignals_seconds [decimal](18, 9),
	@MinimumFillSize [decimal](18, 0),
	@CoverDistanceGrossBasisPoints [decimal](18, 6),
	@CoverTimeout_milliseconds [INT],
	@MinimumTrueGainGrossBasisPoints [decimal](18, 6),
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'SmartCross'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'SmartCross TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueSmartCrossSettings]
			([SymbolId]
			,[CounterpartyId]
			,[TradingSystemId]
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
			,[MinimumFillSize]
			,[CoverDistanceGrossBasisPoints]
			,[CoverTimeout_milliseconds]
			,[MinimumTrueGainGrossBasisPoints])
     VALUES
			(@SymbolId
			,@CounterpartyId
			,@TradingSystemId
			,@MinimumSizeToTrade
			,@MaximumSizeToTrade
			,@MinimumDiscountBasisPointsToTrade
			,@MinimumTimeBetweenSingleCounterpartySignals_seconds
			,@MinimumFillSize
			,@CoverDistanceGrossBasisPoints
			,@CoverTimeout_milliseconds
			,@MinimumTrueGainGrossBasisPoints)
END
GO

ALTER PROCEDURE [dbo].[VenueSmartCrossSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MinimumSizeToTrade [decimal](18, 0),
	@MaximumSizeToTrade [decimal](18, 0),
	@MinimumDiscountBasisPointsToTrade [decimal](18, 6),
	@MinimumTimeBetweenSingleCounterpartySignals_seconds [decimal](18, 9),
	@MinimumFillSize [decimal](18, 0),
	@CoverDistanceGrossBasisPoints [decimal](18, 6),
	@CoverTimeout_milliseconds [INT],
	@MinimumTrueGainGrossBasisPoints [decimal](18, 6)
)
AS
BEGIN
	UPDATE sett
	SET
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[MinimumDiscountBasisPointsToTrade] = @MinimumDiscountBasisPointsToTrade,
		[MinimumTimeBetweenSingleCounterpartySignals_seconds] = @MinimumTimeBetweenSingleCounterpartySignals_seconds,
		[MinimumFillSize] = @MinimumFillSize,
		[CoverDistanceGrossBasisPoints] = @CoverDistanceGrossBasisPoints,
		[CoverTimeout_milliseconds] = @CoverTimeout_milliseconds,
		[MinimumTrueGainGrossBasisPoints] = @MinimumTrueGainGrossBasisPoints
	FROM
		[dbo].[VenueSmartCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO


--ROLLBACK TRANSACTION AAA
--GO
--COMMIT TRANSACTION AAA
--GO