

/****** Object:  Table [dbo].[Counterparty]    Script Date: 26. 7. 2013 21:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Counterparty](
	[CounterpartyId] [tinyint] NOT NULL,
	[CounterpartyName] [nchar](50) NOT NULL,
	[CounterpartyCode] [char](3) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[DealExternalExecuted]    Script Date: 26. 7. 2013 21:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealExternalExecuted](
	[ExecutionReportReceivedUtc] [datetime2](7) NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[InternalOrderId] [nvarchar](45) NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[AmountBasePolExecuted] [decimal](18, 0) NOT NULL,
	[Price] [decimal](18, 9) NOT NULL,
	[CounterpartyTransactionId] [nvarchar](64) NOT NULL,
	[CounterpartyExecutionReportTimeStampUtc] [datetime2](7) NULL,
	[ValueDateCounterpartySuppliedLocMktDate] [date] NOT NULL,
	[OrderSentToExecutionReportReceivedLatency] [time](7) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DealExternalRejected]    Script Date: 26. 7. 2013 21:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealExternalRejected](
	[ExecutionReportReceivedUtc] [datetime2](7) NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[InternalOrderId] [nvarchar](45) NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[AmountBasePolRejected] [decimal](18, 0) NOT NULL,
	[OrderSentToExecutionReportReceivedLatency] [time](7) NOT NULL,
	[RejectionReason] [nvarchar](64) NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[ExecutionReportMessage]    Script Date: 26. 7. 2013 21:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExecutionReportMessage](
	[ExecutionReportReceivedUtc] [datetime2](7) NOT NULL,
	[ExecutionReportStatusId] [tinyint] NOT NULL,
	[InternalOrderId] [nvarchar](45) NOT NULL,
	[ExecutionReportCounterpartySentUtc] [datetime2](7) NOT NULL,
	[RawFIXMessage] [nvarchar](1024) NOT NULL,
	[ParsedFIXMessage] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExecutionReportStatus]    Script Date: 26. 7. 2013 21:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExecutionReportStatus](
	[Id] [tinyint] NOT NULL,
	[Name] [varchar](16) NOT NULL,
 CONSTRAINT [IX_ExecutionReportStatusName] UNIQUE NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[OrderExternal]    Script Date: 26. 7. 2013 21:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternal](
	[ExternalOrderSentUtc] [datetime2](7) NOT NULL,
	[InternalOrderId] [nvarchar](45) NOT NULL,
	[InternalClientOrderId] [nvarchar](32) NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[QuoteReceivedToOrderSentInternalLatency] [time](7) NOT NULL
) ON [PRIMARY]

GO



/****** Object:  Index [PK_Counterparty]    Script Date: 26. 7. 2013 21:02:42 ******/
CREATE UNIQUE CLUSTERED INDEX [PK_Counterparty] ON [dbo].[Counterparty]
(
	[CounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ReportReceivedUtc_SymbolId_CounterpartyId]    Script Date: 26. 7. 2013 21:02:42 ******/
CREATE CLUSTERED INDEX [IX_ReportReceivedUtc_SymbolId_CounterpartyId] ON [dbo].[DealExternalExecuted]
(
	[ExecutionReportReceivedUtc] ASC,
	[SymbolId] ASC,
	[CounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_RejectedDealExternalUtc_SymbolId_CounterpartyId]    Script Date: 26. 7. 2013 21:02:42 ******/
CREATE CLUSTERED INDEX [IX_RejectedDealExternalUtc_SymbolId_CounterpartyId] ON [dbo].[DealExternalRejected]
(
	[ExecutionReportReceivedUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/****** Object:  Index [IX_ExecutionReportMessage]    Script Date: 26. 7. 2013 21:02:42 ******/
CREATE CLUSTERED INDEX [IX_ExecutionReportMessage] ON [dbo].[ExecutionReportMessage]
(
	[ExecutionReportReceivedUtc] ASC,
	[InternalOrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PK_ExecutionReportStatus]    Script Date: 26. 7. 2013 21:02:42 ******/
CREATE UNIQUE CLUSTERED INDEX [PK_ExecutionReportStatus] ON [dbo].[ExecutionReportStatus]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PK_OrderExternal]    Script Date: 26. 7. 2013 21:02:42 ******/
CREATE UNIQUE CLUSTERED INDEX [PK_OrderExternal] ON [dbo].[OrderExternal]
(
	[InternalOrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO

/****** Object:  Index [IX_CounterpartyCode]    Script Date: 26. 7. 2013 21:02:42 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_CounterpartyCode] ON [dbo].[Counterparty]
(
	[CounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO

/****** Object:  Index [IX_OrderExternal_ClientOrderId]    Script Date: 26. 7. 2013 21:02:42 ******/
CREATE NONCLUSTERED INDEX [IX_OrderExternal_ClientOrderId] ON [dbo].[OrderExternal]
(
	[InternalClientOrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderExternalOrderSentUtc]    Script Date: 26. 7. 2013 21:02:42 ******/
CREATE NONCLUSTERED INDEX [IX_OrderExternalOrderSentUtc] ON [dbo].[OrderExternal]
(
	[ExternalOrderSentUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DealExternalExecuted]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecuted_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[DealExternalExecuted] CHECK CONSTRAINT [FK_DealExternalExecuted_Counterparty]
GO
ALTER TABLE [dbo].[DealExternalExecuted]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecuted_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[DealExternalExecuted] CHECK CONSTRAINT [FK_DealExternalExecuted_DealDirection]
GO
ALTER TABLE [dbo].[DealExternalExecuted]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecuted_OrderExternal] FOREIGN KEY([InternalOrderId])
REFERENCES [dbo].[OrderExternal] ([InternalOrderId])
GO
ALTER TABLE [dbo].[DealExternalExecuted] CHECK CONSTRAINT [FK_DealExternalExecuted_OrderExternal]
GO
ALTER TABLE [dbo].[DealExternalExecuted]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecuted_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO
ALTER TABLE [dbo].[DealExternalExecuted] CHECK CONSTRAINT [FK_DealExternalExecuted_Symbol]
GO
ALTER TABLE [dbo].[DealExternalRejected]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalRejected_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[DealExternalRejected] CHECK CONSTRAINT [FK_DealExternalRejected_DealDirection]
GO
ALTER TABLE [dbo].[DealExternalRejected]  WITH CHECK ADD  CONSTRAINT [FK_RejectedDealExternal_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[DealExternalRejected] CHECK CONSTRAINT [FK_RejectedDealExternal_Counterparty]
GO
ALTER TABLE [dbo].[DealExternalRejected]  WITH CHECK ADD  CONSTRAINT [FK_RejectedDealExternal_OrderExternal] FOREIGN KEY([InternalOrderId])
REFERENCES [dbo].[OrderExternal] ([InternalOrderId])
GO
ALTER TABLE [dbo].[DealExternalRejected] CHECK CONSTRAINT [FK_RejectedDealExternal_OrderExternal]
GO
ALTER TABLE [dbo].[DealExternalRejected]  WITH CHECK ADD  CONSTRAINT [FK_RejectedDealExternal_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO
ALTER TABLE [dbo].[DealExternalRejected] CHECK CONSTRAINT [FK_RejectedDealExternal_Symbol]
GO

ALTER TABLE [dbo].[ExecutionReportMessage]  WITH CHECK ADD  CONSTRAINT [FK_ExecutionReportMessage_ExecutionReportStatus] FOREIGN KEY([ExecutionReportStatusId])
REFERENCES [dbo].[ExecutionReportStatus] ([Id])
GO
ALTER TABLE [dbo].[ExecutionReportMessage] CHECK CONSTRAINT [FK_ExecutionReportMessage_ExecutionReportStatus]
GO
ALTER TABLE [dbo].[ExecutionReportMessage]  WITH CHECK ADD  CONSTRAINT [FK_ExecutionReportMessage_OrderExternal] FOREIGN KEY([InternalOrderId])
REFERENCES [dbo].[OrderExternal] ([InternalOrderId])
GO
ALTER TABLE [dbo].[ExecutionReportMessage] CHECK CONSTRAINT [FK_ExecutionReportMessage_OrderExternal]
GO


ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_Counterparty]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_DealDirection]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_Symbol]
GO





/****** Object:  StoredProcedure [dbo].[InsertDealExternalExecuted_SP]    Script Date: 26. 7. 2013 21:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolExecuted decimal(18, 0),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7)
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END

	INSERT INTO 
	[dbo].[DealExternalExecuted]
           ([ExecutionReportReceivedUtc]
           ,[CounterpartyId]
           ,[SymbolId]
           ,[InternalOrderId]
           ,[DealDirectionId]
           ,[AmountBasePolExecuted]
           ,[Price]
           ,[CounterpartyTransactionId]
           ,[CounterpartyExecutionReportTimeStampUtc]
           ,[ValueDateCounterpartySuppliedLocMktDate]
           ,[OrderSentToExecutionReportReceivedLatency])
     VALUES
           (@ExecutionReportReceivedUtc
           ,@CounterpartyId
           ,@SymbolId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolExecuted
           ,@Price
           ,@CounterpartyTransactionId
           ,@CounterpartyExecutionReportTimeStampUtc
           ,@ValueDateCounterpartySuppliedLocMktDate
           ,@OrderSentToExecutionReportReceivedLatency)
END


GO
GRANT EXECUTE ON [dbo].[InsertDealExternalExecuted_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertDealExternalRejected_SP]    Script Date: 26. 7. 2013 21:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 0),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@RejectionReason NVARCHAR(64)
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[DealExternalRejected]
           ([ExecutionReportReceivedUtc]
           ,[SymbolId]
           ,[CounterpartyId]
           ,[InternalOrderId]
		   ,[DealDirectionId]
           ,[AmountBasePolRejected]
           ,[OrderSentToExecutionReportReceivedLatency]
           ,[RejectionReason])
     VALUES
           (@ExecutionReportReceivedUtc
		   ,@SymbolId
           ,@CounterpartyId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolRejected
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@RejectionReason)
END


GO
GRANT EXECUTE ON [dbo].[InsertDealExternalRejected_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

/****** Object:  StoredProcedure [dbo].[InsertExecutionReportMessage_SP]    Script Date: 26. 7. 2013 21:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertExecutionReportMessage_SP]( 
	@ExecutionReportReceivedUtc [datetime2](7),
	@ExecutionReportStatus [varchar](16),
	@InternalOrderId [nvarchar](45),
	@ExecutionReportCounterpartySentUtc [datetime2](7),
	@RawFIXMessage [nvarchar](1024),
	@ParsedFIXMessage [nvarchar](MAX)
	)
AS
BEGIN

	DECLARE @ExecutionReportStatusId INT

	SELECT @ExecutionReportStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ExecutionReportStatus

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ExecutionReportStatus not found in DB: %s', 16, 2, @ExecutionReportStatus) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[ExecutionReportMessage]
           ([ExecutionReportReceivedUtc]
           ,[ExecutionReportStatusId]
           ,[InternalOrderId]
           ,[ExecutionReportCounterpartySentUtc]
           ,[RawFIXMessage]
           ,[ParsedFIXMessage])
     VALUES
           (
		   @ExecutionReportReceivedUtc
           ,@ExecutionReportStatusId
           ,@InternalOrderId
           ,@ExecutionReportCounterpartySentUtc
           ,@RawFIXMessage
           ,@ParsedFIXMessage
		   )
END


GO
GRANT EXECUTE ON [dbo].[InsertExecutionReportMessage_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertNewOrderExternal_SP]    Script Date: 26. 7. 2013 21:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertNewOrderExternal_SP]( 
	@ExternalOrderSentUtc [datetime2](7),
	@InternalOrderId [nvarchar](45),
	@InternalClientOrderId [nvarchar](32),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@QuoteReceivedToOrderSentInternalLatency [time](7)
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
		[dbo].[OrderExternal]
           ([ExternalOrderSentUtc]
           ,[InternalOrderId]
		   ,[InternalClientOrderId]
           ,[CounterpartyId]
           ,[SymbolId]
		   ,[DealDirectionId]
           ,[QuoteReceivedToOrderSentInternalLatency])
     VALUES
           (@ExternalOrderSentUtc
           ,@InternalOrderId
		   ,@InternalClientOrderId
           ,@CounterpartyId
           ,@SymbolId
		   ,@DealDirectionId
           ,@QuoteReceivedToOrderSentInternalLatency)
END


GO
GRANT EXECUTE ON [dbo].[InsertNewOrderExternal_SP] TO [IntegratorServiceAccount] AS [dbo]
GO




--Counterparty
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (1, N'Bank of America Merrill Lynch', N'BOA')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (2, N'Citibank', N'CTI')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (3, N'Royal Bank of Scotland', N'RBS')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (4, N'JPMorgan Chase', N'JPM')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (5, N'Goldman Sachs', N'GLS')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (6, N'Deutsche Bank', N'DBK')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (7, N'UBS', N'UBS')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (8, N'Commerzbank', N'CZB')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (9, N'Rabobank', N'RAB')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (10, N'Morgan Stanley', N'MGS')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (11, N'Natixis', N'NTX')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (12, N'Nomura', N'NOM')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (13, N'Credit Suisse', N'CRS')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (14, N'BNP Paribas', N'BNP')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (15, N'Barclays', N'BRX')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (16, N'CAPH', N'CPH')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (17, N'Societe Generale', N'SOC')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (18, N'Last Atlantis', N'LTA')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (19, N'HSBC', N'HSB')
GO

--Execution report status

INSERT INTO [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (0, N'NotSubmitted')
GO
INSERT INTO [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (1, N'Submitted')
GO
INSERT INTO [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (2, N'Filled')
GO
INSERT INTO [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (3, N'Rejected')
GO
INSERT INTO [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (4, N'InBrokenState')
GO