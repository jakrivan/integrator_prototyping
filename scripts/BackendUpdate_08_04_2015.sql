
ALTER PROCEDURE [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
AS
BEGIN

	DECLARE @CntOfAllowedEnabledSystemsPerSymbol INT = 3

	DECLARE @ErrorMsg VARCHAR(MAX) = ''
	DECLARE @IsError BIT = 0
	DECLARE @Tab CHAR = CHAR(9)
	DECLARE @CR CHAR = CHAR(13)
	DECLARE @LF CHAR = CHAR(10)

	;WITH systemCounts AS
	(
		SELECT 
			sym.[Name] AS Symbol, 
			cpt.[CounterpartyCode] AS Counterparty,
			streamSys.[TradingSystemId], 
			COUNT(sym.[Name]) OVER (PARTITION BY sym.[Name], cpt.[CounterpartyCode]) AS systemsForSymbolCnt
		FROM 
			[dbo].[VenueStreamSettings] streamSys
			INNER JOIN [dbo].[TradingSystemActions] tsa ON streamSys.[TradingSystemId] = tsa.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] sym on streamSys.[SymbolId] = sym.[Id]
			INNER JOIN [dbo].Counterparty cpt on streamSys.[CounterpartyId] = cpt.[CounterpartyId]
		WHERE 
			tsa.[Enabled] = 1
			--AND tsa.[GoFlatOnly] = 0
			--AND tsa.[OrderTransmissionDisabled] = 0
	)
	SELECT
		@IsError = 1,
		@ErrorMsg = @ErrorMsg + @CR + @LF + 'Symbol: ' + Symbol + ', Destination: ' + Counterparty + ', Group Name: ' + grp.[TradingSystemGroupName] + ',' + @Tab + ' SystemId: ' + CAST(systemCounts.TradingSystemId AS VARCHAR(10)) + ',' + @Tab + ' Total systems per this symbol: ' + CAST(systemsForSymbolCnt AS VARCHAR(4)) + ',' + @Tab + ' Total systems per this symbol allowed: ' + CAST(@CntOfAllowedEnabledSystemsPerSymbol AS VARCHAR(4))
	FROM
		systemCounts
		INNER JOIN [dbo].[TradingSystem] sys ON systemCounts.[TradingSystemId] = sys.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystemGroup] grp ON sys.[TradingSystemGroupId] = grp.[TradingSystemGroupId]
	WHERE
		systemsForSymbolCnt > @CntOfAllowedEnabledSystemsPerSymbol
	ORDER BY
		systemsForSymbolCnt DESC,
		grp.[TradingSystemGroupName] ASC,
		Counterparty ASC,
		sys.[TradingSystemId] ASC

	IF @IsError = 1
		RAISERROR ('Error - Action reverted - There would be too many enabled stream systmes per some symbols:%s', 16, 1, @ErrorMsg)

END
GO