BEGIN TRANSACTION AAA


INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (55, N'Hotspot', N'H4T', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (56, N'Hotspot', N'H4M', 1)
GO


INSERT INTO [dbo].[CommissionsSchedule_CptCommission] ([CounterpartyId], [CounterpartyCommissionPpm], [ValidFromUtc], [ValidToUtc]) VALUES (55, 0, '1900-01-01', '9999-12-31')
INSERT INTO [dbo].[CommissionsSchedule_CptCommission] ([CounterpartyId], [CounterpartyCommissionPpm], [ValidFromUtc], [ValidToUtc]) VALUES (56, 0, '1900-01-01', '9999-12-31')

INSERT INTO [dbo].[CommissionsSchedule_Mapping] ([CounterpartyId], [PrimeBrokerId], [ValidFromUtc], [ValidToUtc]) VALUES (55, 1, '2015-09-10', '9999-12-31')
INSERT INTO [dbo].[CommissionsSchedule_Mapping] ([CounterpartyId], [PrimeBrokerId], [ValidFromUtc], [ValidToUtc]) VALUES (56, 1, '2015-09-10', '9999-12-31')


INSERT INTO [dbo].[DelaySettings] 
	([CounterpartyId]
    ,[ExpectedDelayTicks]
      ,[MaxAllowedAdditionalDelayTicks]
      ,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
      ,[MinAdditionalDelayTicksToTurnGFO]
      ,[MinNegativeDelayTicksFromExpectedToTurnGFO])
SELECT
	55
	,[ExpectedDelayTicks]
	,[MaxAllowedAdditionalDelayTicks]
	,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
	,[MinAdditionalDelayTicksToTurnGFO]
	,[MinNegativeDelayTicksFromExpectedToTurnGFO]
FROM
	[dbo].[DelaySettings]
WHERE
	CounterpartyId = 48 -- FS1
	
INSERT INTO [dbo].[DelaySettings] 
	([CounterpartyId]
    ,[ExpectedDelayTicks]
      ,[MaxAllowedAdditionalDelayTicks]
      ,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
      ,[MinAdditionalDelayTicksToTurnGFO]
      ,[MinNegativeDelayTicksFromExpectedToTurnGFO])
SELECT
	56
	,[ExpectedDelayTicks]
	,[MaxAllowedAdditionalDelayTicks]
	,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
	,[MinAdditionalDelayTicksToTurnGFO]
	,[MinNegativeDelayTicksFromExpectedToTurnGFO]
FROM
	[dbo].[DelaySettings]
WHERE
	CounterpartyId = 48 -- FS1
	
INSERT INTO [dbo].[StreamingSettingsPerCounterparty]
   ([CounterpartyId]
   ,[MaxLLTimeEnforcedByDestination_milliseconds]
   ,[LLTransportReserve_milliseconds])
SELECT
	56
	,[MaxLLTimeEnforcedByDestination_milliseconds]
    ,[LLTransportReserve_milliseconds]
FROM
	[dbo].[StreamingSettingsPerCounterparty]
WHERE
	CounterpartyId = 20 -- HTA
GO
	
	
INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'H4M'), 'Stream')
INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'H4M'), 'Glider')	
	
INSERT INTO [dbo].[PriceDelayStats] ([CounterpartyId], [TotalReceivedPricesInLastMinute], [DelayedPricesInLastMinute], [TotalReceivedPricesInLastHour], [DelayedPricesInLastHour]) VALUES (55, 0, 0, 0, 0)
INSERT INTO [dbo].[PriceDelayStats] ([CounterpartyId], [TotalReceivedPricesInLastMinute], [DelayedPricesInLastMinute], [TotalReceivedPricesInLastHour], [DelayedPricesInLastHour]) VALUES (56, 0, 0, 0, 0)
GO


INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'H4T_ORD_Fluent', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
ResetOnLogout=Y
#no messages replying
PersistMessages=N

#HTF
SenderCompID=kgt
SenderSubID=kgt_HTF

SSLEnable=N

[SESSION]
#DataDictionary=none.xml
RelaxValidationForMessageTypes=8
BeginString=FIXT.1.1
DefaultApplVerID=FIX.5.0SP2
TargetCompID=FluentStreamTrades
#5pm NYK
StartTime=17:02:05
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30
BypassParsing=Y
UseDataDictionary=N

SocketConnectPort=30288
SocketConnectHost=38.76.0.115', GETUTCDATE())
GO

INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'H4M_ORD_Fluent', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
ResetOnLogout=Y
#no messages replying
PersistMessages=N

#HTF
SenderCompID=kgt
SenderSubID=kgt_HTF

SSLEnable=N

[SESSION]
#DataDictionary=none.xml
RelaxValidationForMessageTypes=8
BeginString=FIXT.1.1
DefaultApplVerID=FIX.5.0SP2
TargetCompID=FluentStreamTrades
#5pm NYK
StartTime=17:02:05
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30
BypassParsing=Y
UseDataDictionary=N

SocketConnectPort=30288
SocketConnectHost=38.76.0.115', GETUTCDATE())
GO



DECLARE @H4TOrdFixSettingId INT
DECLARE @H4MOrdFixSettingId INT

SELECT
	@H4TOrdFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'H4T_ORD_Fluent'

SELECT
	@H4MOrdFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'H4M_ORD_Fluent'


INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [IntegratorEnvironmentId], @H4TOrdFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]	
	
INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [IntegratorEnvironmentId], @H4MOrdFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]
	
	
DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.QuickItchN.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.QuickItchN.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="QuickItchNSettings" nillable="true" type="QuickItchNSettings" />
  <xs:complexType name="QuickItchNSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="HtaSessionSettings" type="SessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="HtfSessionSettings" type="SessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="Ht3SessionSettings" type="SessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="H4tSessionSettings" type="SessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="H4mSessionSettings" type="SessionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Host" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Port" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="HeartBeatInterval_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO



DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO

--new schema!!

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_DBK" type="FIXChannel_DBKSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_H4T" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_H4M" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM2" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM3" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LX1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LGA" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LGC" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC1" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC2" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_PX1" type="FIXChannel_PXMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS1" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS2" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTAstream" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3stream" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FL1" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
      <xs:element minOccurs="1" maxOccurs="1" name="STP" type="STPSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_DBKSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DealConfirmationSupported" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="MaxOrdersPerSecond" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="L01LayersNum" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IocApplyTtl" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ApplyMaxHoldTime" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxHoldTime" type="FXCMMaxHoldTime" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_PXMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MaxPerSymbolPricesPerSecondSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultMaxAllowedPerSecondRate" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialSymbolRateSettings" type="ArrayOfSymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolRateSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="SymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolRateSetting">
    <xs:attribute name="Name" type="xs:string" />
    <xs:attribute name="MaxAllowedPerSecondRate" type="xs:int" use="required" />
  </xs:complexType>
  <xs:complexType name="RejectionRateDisconnectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumOrdersToApplyCheck" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAllowedRejectionRatePercent" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="StreamingChannelSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxTotalPricesPerSecond" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="MaxPerSymbolPricesPerSecondSetting" type="MaxPerSymbolPricesPerSecondSetting" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExpectedPricingDepth" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="RejectionRateDisconnectSettings" type="RejectionRateDisconnectSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXSTPChannel_CTIPBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNongenuineInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialRejectionsHandling" type="ArrayOfSpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SpecialRejection" type="SpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="TriggerPhrase" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="FXCMMaxHoldTime">
    <xs:restriction base="xs:string">
      <xs:enumeration value="UpTo1ms" />
      <xs:enumeration value="UpTo30ms" />
      <xs:enumeration value="UpTo100ms" />
      <xs:enumeration value="UpTo500ms" />
      <xs:enumeration value="UpTo3000ms" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="MarketSessionIntoSeparateProcess" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="STPDestinations" type="ArrayOfSTPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSTPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="STPDestination" nillable="true" type="STPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Name" type="STPCounterparty" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedTicketTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="ServedCounterparties" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="STPCounterparty">
    <xs:restriction base="xs:string">
      <xs:enumeration value="CTIPB" />
      <xs:enumeration value="Traiana" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'



-- manually !!
--add (after </FIXChannel_HT3>): 
<FIXChannel_H4T>
	<SenderSubId>ldnkgtfix</SenderSubId>
    <Username>ldnkgtfix</Username>
    <Password>hotspot</Password>
    <DealConfirmationSupported>true</DealConfirmationSupported>
	<MaxOrdersPerSecond>95</MaxOrdersPerSecond>
</FIXChannel_H4T>
<FIXChannel_H4M>
	<SenderSubId>ldnkgtfix</SenderSubId>
    <Username>ldnkgtfix</Username>
    <Password>hotspot</Password>
    <DealConfirmationSupported>true</DealConfirmationSupported>
	<MaxOrdersPerSecond>95</MaxOrdersPerSecond>
</FIXChannel_H4M>


<CounterpartSetting>
	<ShortName>H4T</ShortName>
	<SettingAction>Add</SettingAction>
	<ShortName>H4M</ShortName>
	<SettingAction>Add</SettingAction>
</CounterpartSetting>


<CounterpartSetting>
          <ShortName>H4T</ShortName>
          <SettingAction>Add</SettingAction>
          <MarketSessionIntoSeparateProcess>true</MarketSessionIntoSeparateProcess>
</CounterpartSetting>

--also add H4T, H4M wherever is HTA

--itch
<H4tSessionSettings>
    <Host>195.3.211.5</Host>
    <Port>18177</Port>
    <Username>ldnkgtitch</Username>
    <Password>hotspot</Password>
    <HeartBeatInterval_Seconds>2</HeartBeatInterval_Seconds>
  </H4tSessionSettings>





---------------------------------
-- Quoting stats
-----------------------


ALTER TABLE [dbo].[VenueQuotingIntegratorStats] ADD [InactivityRestrictionOn] [bit] NULL
ALTER TABLE [dbo].[VenueQuotingIntegratorStats] ADD [LTBandRestrictionOn] [bit] NULL
ALTER TABLE [dbo].[VenueQuotingIntegratorStats] ADD [CurrentSpreadDecimal] [decimal](18,10) NULL
GO

--exec sp_rename '[dbo].[VenueQuotingIntegratorStatsUpdateSingle_SP]', '[VenueQuotingIntegratorStatsUpdateSingle_SP]'


ALTER PROCEDURE [dbo].[VenueQuotingIntegratorStatsUpdateSingle_SP]
(
	@TradingSystemId int,
	@CurrentLmaxTickerVolumeUsd [bigint] = NULL,
	@CurrentLmaxTickerAvgDealSizeUsd [bigint] = NULL,
	@CalculatedFromFullInterval [bit] = NULL,
	@InactivityRestrictionOn [bit] = NULL,
	@LTBandRestrictionOn [bit] = NULL,
	@CurrentSpreadDecimal [decimal](18,10) = NULL,
	@IsSpredNull [bit] = NULL
)
AS
BEGIN
	--WARNING - this doesn't solve concurrency as it expects to be executed serializable once every few seconds

	UPDATE
		[dbo].[VenueQuotingIntegratorStats]
	SET
		[CurrentLmaxTickerVolumeUsd] = ISNULL(@CurrentLmaxTickerVolumeUsd, [CurrentLmaxTickerVolumeUsd]),
		[CurrentLmaxTickerAvgDealSizeUsd] = ISNULL(@CurrentLmaxTickerAvgDealSizeUsd, [CurrentLmaxTickerAvgDealSizeUsd]),
		[CalculatedFromFullInterval] = ISNULL(@CalculatedFromFullInterval, [CalculatedFromFullInterval]),
		[InactivityRestrictionOn] = ISNULL(@InactivityRestrictionOn, [InactivityRestrictionOn]),
		[LTBandRestrictionOn] = ISNULL(@LTBandRestrictionOn, [LTBandRestrictionOn]),
		[CurrentSpreadDecimal] = CASE WHEN @IsSpredNull IS NULL THEN ISNULL(@CurrentSpreadDecimal, [CurrentSpreadDecimal]) ELSE NULL END
	WHERE
		[TradingSystemId] = @TradingSystemId
		
	IF @@rowcount = 0
	BEGIN
		INSERT INTO [dbo].[VenueQuotingIntegratorStats] 
			([TradingSystemId],
			[CurrentLmaxTickerVolumeUsd],
			[CurrentLmaxTickerAvgDealSizeUsd],
			[CalculatedFromFullInterval],
			[InactivityRestrictionOn],
			[LTBandRestrictionOn],
			[CurrentSpreadDecimal])
		VALUES
			(@TradingSystemId,
			@CurrentLmaxTickerVolumeUsd,
			@CurrentLmaxTickerAvgDealSizeUsd,
			@CalculatedFromFullInterval,
			@InactivityRestrictionOn,
			@LTBandRestrictionOn,
			CASE WHEN @IsSpredNull IS NULL THEN @CurrentSpreadDecimal ELSE @CurrentSpreadDecimal END)
	END
END
GO

CREATE PROCEDURE [dbo].[VenueQuotingIntegratorStatsClearSingle_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	DELETE FROM
		[dbo].[VenueQuotingIntegratorStats]
	WHERE
		[TradingSystemId] = @TradingSystemId
END
GO

GRANT EXECUTE ON [dbo].[VenueQuotingIntegratorStatsClearSingle_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

ALTER PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FixedSpreadBasisPoints],
		sett.[FixedSpreadBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [FixedSpreadDecimal],
		sett.[StopLossNetInUsd],
		spreadType.[VenueQuotingSettingsSpreadTypeName] AS SpreadType,
		sett.[MinimumDynamicSpreadBp],
		sett.[MinimumDynamicSpreadBp] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumDynamicSpreadDecimal],
		sett.[DynamicMarkupDecimal],
		sett.[EnableSkew],
		sett.[SkewDecimal],
		sett.[InactivityTimeoutSec],
		sett.[LiquidationSpreadBp],
		sett.[LiquidationSpreadBp] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [LiquidationSpreadDecimal],
		sett.[LmaxTickerLookbackSec],
		sett.[MinimumLmaxTickerVolumeUsd],
		sett.[MaximumLmaxTickerVolumeUsd],
		sett.[MinimumLmaxTickerAvgDealSizeUsd],
		sett.[MaximumLmaxTickerAvgDealSizeUsd],
		integratorStats.[CurrentLmaxTickerVolumeUsd],
		integratorStats.[CurrentLmaxTickerAvgDealSizeUsd],
		integratorStats.[CalculatedFromFullInterval],
		integratorStats.[InactivityRestrictionOn],
		integratorStats.[LTBandRestrictionOn],
		integratorStats.[CurrentSpreadDecimal],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		INNER JOIN [dbo].[VenueQuotingSettingsSpreadType] spreadType ON sett.[SpreadTypeId] = spreadType.[VenueQuotingSettingsSpreadTypeId]
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
		LEFT JOIN [dbo].[VenueQuotingIntegratorStats] integratorStats ON integratorStats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

--------------------------------------
-- Paid Vol Ratio
-------------------------------------

ALTER TABLE [dbo].[VenueQuotingIntegratorStats] ADD [CurrentLmaxTickerPaidVolumeRatio] [decimal](5,4) NULL
GO

ALTER PROCEDURE [dbo].[VenueQuotingIntegratorStatsUpdateSingle_SP]
(
	@TradingSystemId int,
	@CurrentLmaxTickerVolumeUsd [bigint] = NULL,
	@CurrentLmaxTickerAvgDealSizeUsd [bigint] = NULL,
	@CalculatedFromFullInterval [bit] = NULL,
	@InactivityRestrictionOn [bit] = NULL,
	@LTBandRestrictionOn [bit] = NULL,
	@CurrentSpreadDecimal [decimal](18,10) = NULL,
	@IsSpredNull [bit] = NULL,
	@CurrentLmaxTickerPaidVolumeRatio [decimal](5,4) = NULL
)
AS
BEGIN
	--WARNING - this doesn't solve concurrency as it expects to be executed serializable once every few seconds

	UPDATE
		[dbo].[VenueQuotingIntegratorStats]
	SET
		[CurrentLmaxTickerVolumeUsd] = ISNULL(@CurrentLmaxTickerVolumeUsd, [CurrentLmaxTickerVolumeUsd]),
		[CurrentLmaxTickerAvgDealSizeUsd] = ISNULL(@CurrentLmaxTickerAvgDealSizeUsd, [CurrentLmaxTickerAvgDealSizeUsd]),
		[CalculatedFromFullInterval] = ISNULL(@CalculatedFromFullInterval, [CalculatedFromFullInterval]),
		[InactivityRestrictionOn] = ISNULL(@InactivityRestrictionOn, [InactivityRestrictionOn]),
		[LTBandRestrictionOn] = ISNULL(@LTBandRestrictionOn, [LTBandRestrictionOn]),
		[CurrentSpreadDecimal] = CASE WHEN @IsSpredNull IS NULL THEN ISNULL(@CurrentSpreadDecimal, [CurrentSpreadDecimal]) ELSE NULL END,
		[CurrentLmaxTickerPaidVolumeRatio] = ISNULL(@CurrentLmaxTickerPaidVolumeRatio, [CurrentLmaxTickerPaidVolumeRatio])
	WHERE
		[TradingSystemId] = @TradingSystemId
		
	IF @@rowcount = 0
	BEGIN
		INSERT INTO [dbo].[VenueQuotingIntegratorStats] 
			([TradingSystemId],
			[CurrentLmaxTickerVolumeUsd],
			[CurrentLmaxTickerAvgDealSizeUsd],
			[CalculatedFromFullInterval],
			[InactivityRestrictionOn],
			[LTBandRestrictionOn],
			[CurrentSpreadDecimal],
			[CurrentLmaxTickerPaidVolumeRatio])
		VALUES
			(@TradingSystemId,
			@CurrentLmaxTickerVolumeUsd,
			@CurrentLmaxTickerAvgDealSizeUsd,
			@CalculatedFromFullInterval,
			@InactivityRestrictionOn,
			@LTBandRestrictionOn,
			CASE WHEN @IsSpredNull IS NULL THEN @CurrentSpreadDecimal ELSE @CurrentSpreadDecimal END,
			@CurrentLmaxTickerPaidVolumeRatio)
	END
END
GO

ALTER PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FixedSpreadBasisPoints],
		sett.[FixedSpreadBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [FixedSpreadDecimal],
		sett.[StopLossNetInUsd],
		spreadType.[VenueQuotingSettingsSpreadTypeName] AS SpreadType,
		sett.[MinimumDynamicSpreadBp],
		sett.[MinimumDynamicSpreadBp] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumDynamicSpreadDecimal],
		sett.[DynamicMarkupDecimal],
		sett.[EnableSkew],
		sett.[SkewDecimal],
		sett.[InactivityTimeoutSec],
		sett.[LiquidationSpreadBp],
		sett.[LiquidationSpreadBp] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [LiquidationSpreadDecimal],
		sett.[LmaxTickerLookbackSec],
		sett.[MinimumLmaxTickerVolumeUsd],
		sett.[MaximumLmaxTickerVolumeUsd],
		sett.[MinimumLmaxTickerAvgDealSizeUsd],
		sett.[MaximumLmaxTickerAvgDealSizeUsd],
		integratorStats.[CurrentLmaxTickerVolumeUsd],
		integratorStats.[CurrentLmaxTickerAvgDealSizeUsd],
		integratorStats.[CalculatedFromFullInterval],
		integratorStats.[InactivityRestrictionOn],
		integratorStats.[LTBandRestrictionOn],
		integratorStats.[CurrentSpreadDecimal],
		integratorStats.[CurrentLmaxTickerPaidVolumeRatio],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		INNER JOIN [dbo].[VenueQuotingSettingsSpreadType] spreadType ON sett.[SpreadTypeId] = spreadType.[VenueQuotingSettingsSpreadTypeId]
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
		LEFT JOIN [dbo].[VenueQuotingIntegratorStats] integratorStats ON integratorStats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

--ROLLBACK TRANSACTION AAA
--GO
--COMMIT TRANSACTION AAA
--GO
