BEGIN TRANSACTION AAA

ALTER TABLE [dbo].[VenueGliderSettings] ADD [GlidingCounterpartyId] [tinyint] NULL
ALTER TABLE [dbo].[VenueGliderSettings_Changes] ADD [GlidingCounterpartyId] [tinyint] NULL
GO

UPDATE [dbo].[VenueGliderSettings] SET 
	[GlidingCounterpartyId] = (SELECT [CounterpartyId] FROM [Counterparty] WHERE [CounterpartyCode] = 'L01')
UPDATE [dbo].[VenueGliderSettings_Changes] SET 
	[GlidingCounterpartyId] = (SELECT [CounterpartyId] FROM [Counterparty] WHERE [CounterpartyCode] = 'L01')
GO

ALTER TABLE [dbo].[VenueGliderSettings] ALTER COLUMN [GlidingCounterpartyId] [tinyint] NOT NULL
ALTER TABLE [dbo].[VenueGliderSettings_Changes] ALTER COLUMN [GlidingCounterpartyId] [tinyint] NOT NULL
GO


ALTER TABLE [dbo].[VenueGliderSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueGliderSettings_GlidingCounterparty] FOREIGN KEY([GlidingCounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueGliderSettings] CHECK CONSTRAINT [FK_VenueGliderSettings_GlidingCounterparty]
GO

ALTER TABLE [dbo].[VenueGliderSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueGliderSettingsChanges_GlidingCounterparty] FOREIGN KEY([GlidingCounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueGliderSettings_Changes] CHECK CONSTRAINT [FK_VenueGliderSettingsChanges_GlidingCounterparty]
GO



ALTER TRIGGER [dbo].[VenueGliderSettings_DeletedOrUpdated] ON [dbo].[VenueGliderSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueGliderSettings_Changes]
	   ([SymbolId]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[MaximumSizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade]
	   ,[KGTLLTime_milliseconds]
	   ,[MinimumBPGrossGain]
	   ,[PreferLmaxDuringHedging]
	   ,[MinimumIntegratorPriceLife_milliseconds]
	   ,[MinimumIntegratorGlidePriceLife_milliseconds]
	   ,[GlidingCutoffSpan_milliseconds]
	   ,[MinimumSizeToTrade]
	   ,[MinimumFillSize]
	   ,[MaximumSourceBookTiers]
	   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
	   ,[ReferenceLmaxBookOnly]
	   ,[BidEnabled]
	   ,[AskEnabled]
	   ,[MinimumBPGrossGainIfZeroGlideFill]
	   ,[PreferLLDestinationCheckToLmax]
	   ,[GlidingCounterpartyId]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[MaximumSizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[KGTLLTime_milliseconds]
		,deleted.[MinimumBPGrossGain]
		,deleted.[PreferLmaxDuringHedging]
		,deleted.[MinimumIntegratorPriceLife_milliseconds]
		,deleted.[MinimumIntegratorGlidePriceLife_milliseconds]
		,deleted.[GlidingCutoffSpan_milliseconds]
		,deleted.[MinimumSizeToTrade]
		,deleted.[MinimumFillSize]
		,deleted.[MaximumSourceBookTiers]
		,deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]
		,deleted.[ReferenceLmaxBookOnly]
		,deleted.[BidEnabled]
		,deleted.[AskEnabled]
		,deleted.[MinimumBPGrossGainIfZeroGlideFill]
	    ,deleted.[PreferLLDestinationCheckToLmax]
		,deleted.[GlidingCounterpartyId]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueGliderSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueGliderSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([KGTLLTime_milliseconds]) AND (topUpdatedChange.[KGTLLTime_milliseconds] IS NULL OR topUpdatedChange.[KGTLLTime_milliseconds] != deleted.[KGTLLTime_milliseconds]))
		OR (UPDATE([MinimumBPGrossGain]) AND (topUpdatedChange.[MinimumBPGrossGain] IS NULL OR topUpdatedChange.[MinimumBPGrossGain] != deleted.[MinimumBPGrossGain]))
		OR (UPDATE([PreferLmaxDuringHedging]) AND (topUpdatedChange.[PreferLmaxDuringHedging] IS NULL OR topUpdatedChange.[PreferLmaxDuringHedging] != deleted.[PreferLmaxDuringHedging]))
		OR (UPDATE([MinimumIntegratorPriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] != deleted.[MinimumIntegratorPriceLife_milliseconds]))
		OR (UPDATE([MinimumIntegratorGlidePriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorGlidePriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorGlidePriceLife_milliseconds] != deleted.[MinimumIntegratorGlidePriceLife_milliseconds]))
		OR (UPDATE([GlidingCutoffSpan_milliseconds]) AND (topUpdatedChange.[GlidingCutoffSpan_milliseconds] IS NULL OR topUpdatedChange.[GlidingCutoffSpan_milliseconds] != deleted.[GlidingCutoffSpan_milliseconds]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([MaximumSourceBookTiers]) AND (topUpdatedChange.[MaximumSourceBookTiers] IS NULL OR topUpdatedChange.[MaximumSourceBookTiers] != deleted.[MaximumSourceBookTiers]))
		OR (UPDATE([MinimumBasisPointsFromKgtPriceToFastCancel]) AND (topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] IS NULL OR topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] != deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]))
		OR (UPDATE([ReferenceLmaxBookOnly]) AND (topUpdatedChange.[ReferenceLmaxBookOnly] IS NULL OR topUpdatedChange.[ReferenceLmaxBookOnly] != deleted.[ReferenceLmaxBookOnly]))
		OR (UPDATE([BidEnabled]) AND (topUpdatedChange.[BidEnabled] IS NULL OR topUpdatedChange.[BidEnabled] != deleted.[BidEnabled]))
		OR (UPDATE([AskEnabled]) AND (topUpdatedChange.[AskEnabled] IS NULL OR topUpdatedChange.[AskEnabled] != deleted.[AskEnabled]))
		OR (UPDATE([MinimumBPGrossGainIfZeroGlideFill]) AND (topUpdatedChange.[MinimumBPGrossGainIfZeroGlideFill] IS NULL OR topUpdatedChange.[MinimumBPGrossGainIfZeroGlideFill] != deleted.[MinimumBPGrossGainIfZeroGlideFill]))
		OR (UPDATE([PreferLLDestinationCheckToLmax]) AND (topUpdatedChange.[PreferLLDestinationCheckToLmax] IS NULL OR topUpdatedChange.[PreferLLDestinationCheckToLmax] != deleted.[PreferLLDestinationCheckToLmax]))
		OR (UPDATE([GlidingCounterpartyId]) AND (topUpdatedChange.[GlidingCounterpartyId] IS NULL OR topUpdatedChange.[GlidingCounterpartyId] != deleted.[GlidingCounterpartyId]))
END
GO


ALTER PROCEDURE [dbo].[GetUpdatedVenueGliderSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueGliderSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MaximumSizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[KGTLLTime_milliseconds]
			,[MinimumBPGrossGain]
			,[MinimumBPGrossGainIfZeroGlideFill]
			,[PreferLmaxDuringHedging]
			,[PreferLLDestinationCheckToLmax]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumIntegratorGlidePriceLife_milliseconds]
			,[GlidingCutoffSpan_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,[MinimumBasisPointsFromKgtPriceToFastCancel]
			,[ReferenceLmaxBookOnly]
			,glidingCounterparty.CounterpartyCode AS GlidingCounterparty
			,[BidEnabled]
			,[AskEnabled]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueGliderSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
			INNER JOIN [dbo].[Counterparty] glidingCounterparty ON sett.[GlidingCounterpartyId] = glidingCounterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name
	END
END
GO



ALTER PROCEDURE [dbo].[GetUpdatedVenueGliderSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueGliderSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MaximumSizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[KGTLLTime_milliseconds]
			,[MinimumBPGrossGain]
			,[MinimumBPGrossGainIfZeroGlideFill]
			,[PreferLmaxDuringHedging]
			,[PreferLLDestinationCheckToLmax]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumIntegratorGlidePriceLife_milliseconds]
			,[GlidingCutoffSpan_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,[MinimumBasisPointsFromKgtPriceToFastCancel]
			,[ReferenceLmaxBookOnly]
			,glidingCounterparty.CounterpartyCode AS GlidingCounterparty
			,[BidEnabled]
			,[AskEnabled]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueGliderSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
			INNER JOIN [dbo].[Counterparty] glidingCounterparty ON sett.[GlidingCounterpartyId] = glidingCounterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name
	END
END
GO

ALTER PROCEDURE [dbo].[GetVenueGliderSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[MinimumBPGrossGain],
		sett.[MinimumBPGrossGain] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossGainDecimal],
		sett.[PreferLmaxDuringHedging],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumIntegratorGlidePriceLife_milliseconds],
		sett.[GlidingCutoffSpan_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[ReferenceLmaxBookOnly],
		glidingCounterparty.CounterpartyCode AS GlidingCounterparty,
		sett.[BidEnabled],
		sett.[AskEnabled],
		sett.[MinimumBPGrossGainIfZeroGlideFill],
		sett.[MinimumBPGrossGainIfZeroGlideFill] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossGainIfZeroGlideFillDecimal],
		sett.[PreferLLDestinationCheckToLmax],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		INNER JOIN [dbo].[Counterparty] glidingCounterparty ON sett.[GlidingCounterpartyId] = glidingCounterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[VenueGliderSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@BidEnabled [bit],
	@AskEnabled [bit],
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@MinimumBPGrossGain  [decimal](18,6),
	@MinimumBPGrossGainIfZeroGlideFill [decimal](18,6),
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumIntegratorGlidePriceLife_milliseconds [int],
	@GlidingCutoffSpan_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel  [decimal](18,6),
	@ReferenceLmaxBookOnly [bit],
	@PreferLLDestinationCheckToLmax [bit],
	@GlidingCounterpartyCode [char](3),
	@PreferLmaxDuringHedging [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @GlidingCounterpartyId INT
	SELECT @GlidingCounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @GlidingCounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'GlidingCounterpartyCode not found in DB: %s', 16, 2, @GlidingCounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Glider'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Stream TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	
	BEGIN TRANSACTION
	BEGIN TRY
		
		INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

		SELECT @TradingSystemId = scope_identity();
		
		--make sure that default switches values are correct
		INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
		SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
		FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
		WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
		
		INSERT INTO [dbo].[VenueGliderSettings]
			   ([SymbolId]
			   ,[CounterpartyId]
			   ,[TradingSystemId]
			   ,[MaximumSizeToTrade]
			   ,[BestPriceImprovementOffsetBasisPoints]
			   ,[MaximumDiscountBasisPointsToTrade]
			   ,[KGTLLTime_milliseconds]
			   ,[MinimumBPGrossGain]
			   ,[MinimumBPGrossGainIfZeroGlideFill]
			   ,[PreferLmaxDuringHedging]
			   ,[PreferLLDestinationCheckToLmax]
			   ,[MinimumIntegratorPriceLife_milliseconds]
			   ,[MinimumIntegratorGlidePriceLife_milliseconds]
			   ,[GlidingCutoffSpan_milliseconds]
			   ,[MinimumSizeToTrade]
			   ,[MinimumFillSize]
			   ,[MaximumSourceBookTiers]
			   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
			   ,[ReferenceLmaxBookOnly]
			   ,[GlidingCounterpartyId]
			   ,[BidEnabled]
			   ,[AskEnabled])
		 VALUES
			   (@SymbolId
			   ,@CounterpartyId
			   ,@TradingSystemId
			   ,@MaximumSizeToTrade 
			   ,@BestPriceImprovementOffsetBasisPoints
			   ,@MaximumDiscountBasisPointsToTrade
			   ,@KGTLLTime_milliseconds
			   ,@MinimumBPGrossGain
			   ,@MinimumBPGrossGainIfZeroGlideFill
			   ,@PreferLmaxDuringHedging
			   ,@PreferLLDestinationCheckToLmax
			   ,@MinimumIntegratorPriceLife_milliseconds
			   ,@MinimumIntegratorGlidePriceLife_milliseconds
			   ,@GlidingCutoffSpan_milliseconds
			   ,@MinimumSizeToTrade
			   ,@MinimumFillSize
			   ,@MaximumSourceBookTiers
			   ,@MinimumBasisPointsFromKgtPriceToFastCancel
			   ,@ReferenceLmaxBookOnly
			   ,@GlidingCounterpartyId
			   ,@BidEnabled
			   ,@AskEnabled)
		
	
		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[VenueGliderSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@MinimumBPGrossGain  [decimal](18,6),
	@MinimumBPGrossGainIfZeroGlideFill [decimal](18,6),
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumIntegratorGlidePriceLife_milliseconds [int],
	@GlidingCutoffSpan_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel [decimal](18,6),
	@ReferenceLmaxBookOnly [bit],
	@PreferLLDestinationCheckToLmax [bit],
	@PreferLmaxDuringHedging [bit],
	@GlidingCounterpartyCode [char](3),
	@BidEnabled [bit],
	@AskEnabled [bit]
)
AS
BEGIN

	DECLARE @GlidingCounterpartyId INT
	SELECT @GlidingCounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @GlidingCounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'GlidingCounterpartyCode not found in DB: %s', 16, 2, @GlidingCounterpartyCode) WITH SETERROR
	END

	UPDATE sett
	SET
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[KGTLLTime_milliseconds] = @KGTLLTime_milliseconds,
		[MinimumBPGrossGain] = @MinimumBPGrossGain,
		[MinimumBPGrossGainIfZeroGlideFill] = @MinimumBPGrossGainIfZeroGlideFill,
		[PreferLmaxDuringHedging] = @PreferLmaxDuringHedging,
		[PreferLLDestinationCheckToLmax] = @PreferLLDestinationCheckToLmax,
		[MinimumIntegratorPriceLife_milliseconds] = @MinimumIntegratorPriceLife_milliseconds,
		[MinimumIntegratorGlidePriceLife_milliseconds] = @MinimumIntegratorGlidePriceLife_milliseconds,
		[GlidingCutoffSpan_milliseconds] = @GlidingCutoffSpan_milliseconds,
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MinimumFillSize] = @MinimumFillSize,
		[MaximumSourceBookTiers] = @MaximumSourceBookTiers,
		[MinimumBasisPointsFromKgtPriceToFastCancel] = @MinimumBasisPointsFromKgtPriceToFastCancel,
		[ReferenceLmaxBookOnly] = @ReferenceLmaxBookOnly,
		[GlidingCounterpartyId] = @GlidingCounterpartyId,
		[BidEnabled] = @BidEnabled,
		[AskEnabled] = @AskEnabled
	FROM
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

ALTER PROCEDURE [dbo].[GetLastVenueGliderSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MaximumSizeToTrade]
      ,[BestPriceImprovementOffsetBasisPoints]
      ,[MaximumDiscountBasisPointsToTrade]
      ,[KGTLLTime_milliseconds]
      ,[MinimumBPGrossGain]
	  ,[MinimumBPGrossGainIfZeroGlideFill]
	  ,[PreferLmaxDuringHedging]
	  ,[PreferLLDestinationCheckToLmax]
      ,[MinimumIntegratorPriceLife_milliseconds]
	  ,[MinimumIntegratorGlidePriceLife_milliseconds]
	  ,[GlidingCutoffSpan_milliseconds]
      ,[MinimumSizeToTrade]
      ,[MinimumFillSize]
      ,[MaximumSourceBookTiers]
      ,[MinimumBasisPointsFromKgtPriceToFastCancel]
      ,[ReferenceLmaxBookOnly]
	  ,glidingCounterparty.CounterpartyCode AS GlidingCounterparty
      ,[BidEnabled]
      ,[AskEnabled]
	FROM 
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[Counterparty] glidingCounterparty ON sett.[GlidingCounterpartyId] = glidingCounterparty.CounterpartyId
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END
GO


INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LX1'), 'Quoting')
GO

exec sp_rename '[dbo].[VenueQuotingSystemSettingsUpdateSingle_SP]', 'VenueQuotingSettingsUpdateSingle_SP'
GO

INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FC1'), 'Quoting')
GO

ALTER TABLE [dbo].[VenueQuotingSettings] DROP CONSTRAINT [UNQ__VenueQuotingSettings__SymbolId]
GO

ALTER TABLE [dbo].[VenueQuotingSettings] ADD  CONSTRAINT [UNQ__VenueQuotingSettings__SymbolIdPerCptId] UNIQUE NONCLUSTERED 
(
	[CounterpartyId] ASC,
	[SymbolId] ASC
) ON [PRIMARY]
GO

ALTER PROCEDURE [dbo].[VenueQuotingSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MaximumPositionBaseAbs [decimal](18,0),
	@FairPriceImprovementSpreadBasisPoints [decimal](18,6),
	@StopLossNetInUsd [bigint],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Quoting'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Quoting TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;

		INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)
		SELECT @TradingSystemId = scope_identity();

		--make sure that default switches values are correct
		INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
		SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
		FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
		WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
		INSERT INTO [dbo].[VenueQuotingSettings]
			   ([SymbolId]
			   ,[MaximumPositionBaseAbs]
			   ,[FairPriceImprovementSpreadBasisPoints]
			   ,[StopLossNetInUsd]   
			   ,[CounterpartyId]
			   ,[TradingSystemId])
		 VALUES
			   (@SymbolId
			   ,@MaximumPositionBaseAbs
			   ,@FairPriceImprovementSpreadBasisPoints
			   ,@StopLossNetInUsd
			   ,@CounterpartyId
			   ,@TradingSystemId)

		COMMIT TRANSACTION;

	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

--ROLLBACK TRANSACTION AAA
--GO
--COMMIT TRANSACTION AAA
--GO