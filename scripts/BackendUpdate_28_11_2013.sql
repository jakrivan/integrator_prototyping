

ALTER PROCEDURE [dbo].[GetNopsPolSidedPerPair_SP] (
	@Day DATE
	)
AS
BEGIN
	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
		--followings make more sense after extracting multipliers behind brackets
		SUM(deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) AS PnlTermPol,
		SUM((deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) * termCurrency.LastKnownUsdConversionMultiplier) AS PnlTermPolInUsd
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]
END