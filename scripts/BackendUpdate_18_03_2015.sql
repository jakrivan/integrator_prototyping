
INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTA'), 'Stream')


UPDATE
	[dbo].[IntegratorRejectionReason]
SET
	[IntegratorRejectionReason] = 'DealMissed'
WHERE
	[IntegratorRejectionReason] = 'SystemBusinessDecision'
	
	
ALTER TABLE [dbo].[DealExternalRejected] ADD [RejectionDirectionId] [bit] NULL
GO

UPDATE [dbo].[DealExternalRejected] SET [RejectionDirectionId] = 0
GO

ALTER TABLE [dbo].[DealExternalRejected] ALTER COLUMN [RejectionDirectionId] [bit] NOT NULL
GO



ALTER PROCEDURE [dbo].[CreateDailyRejectionsView_SP] 
AS
BEGIN

	DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)
	DECLARE @YesterdayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), DATEADD(DAY, -1, GETUTCDATE()), 101)

	DECLARE @Query NVARCHAR(MAX) = N'
	CREATE VIEW [DealExternalRejectedDailyView_' + @TodayLiteral + ']
	WITH SCHEMABINDING 
	AS 
	SELECT 
		   reject.[IntegratorReceivedExecutionReportUtc]
		   ,reject.[IntegratorExternalOrderPrivateId]
		  ,reject.[CounterpartyId]
		  ,reject.[DatePartIntegratorReceivedExecutionReportUtc]
		  ,CAST(
			(
				(datepart(HOUR, reject.QuoteReceivedToOrderSentInternalLatency)*60 + datepart(MINUTE, reject.QuoteReceivedToOrderSentInternalLatency))*60 
				+ datepart(SECOND, reject.QuoteReceivedToOrderSentInternalLatency)
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, reject.QuoteReceivedToOrderSentInternalLatency) 
		   AS IntegratorLatencyNanoseconds,
		   reject.[RejectionDirectionId]
	FROM 
		[dbo].[DealExternalRejected] reject
	WHERE
		DatePartIntegratorReceivedExecutionReportUtc = CONVERT(DATE,''' + @TodayLiteral + ''', 101)'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE UNIQUE CLUSTERED INDEX [IX_ReceivedUtc_OrderId] ON [dbo].[DealExternalRejectedDailyView_' + @TodayLiteral + ']
	(
		[IntegratorReceivedExecutionReportUtc] ASC,
		[IntegratorExternalOrderPrivateId] ASC
	)ON [Integrator_Testing_DailyData]'


	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalRejectedDailyView_' + @TodayLiteral + ']
	(
		[DatePartIntegratorReceivedExecutionReportUtc] ASC,
		[CounterpartyId] ASC
	)
	INCLUDE
	(
		[IntegratorLatencyNanoseconds]
	)
	ON [Integrator_Testing_DailyData]'


	EXECUTE sp_executesql @Query

	IF EXISTS(SELECT * FROM  sys.synonyms s WHERE s.name = 'DealExternalRejected_DailyView')
	BEGIN
		DROP SYNONYM DealExternalRejected_DailyView
	END

	SET @Query = N'
	CREATE SYNONYM DealExternalRejected_DailyView FOR [DealExternalRejectedDailyView_' + @TodayLiteral + ']'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	IF EXISTS(select * FROM sys.views where name = ''DealExternalRejectedDailyView_' + @YesterdayLiteral + ''')
	BEGIN
		DROP VIEW [DealExternalRejectedDailyView_' + @YesterdayLiteral + ']
	END
	'

	EXECUTE sp_executesql @Query

END
GO


DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)
DECLARE @Query NVARCHAR(MAX) = N'
	IF EXISTS(select * FROM sys.views where name = ''DealExternalRejectedDailyView_' + @TodayLiteral + ''')
	BEGIN
		DROP VIEW [DealExternalRejectedDailyView_' + @TodayLiteral + ']
	END
	'
EXECUTE sp_executesql @Query
GO
EXECUTE [dbo].[CreateDailyRejectionsView_SP]
GO


ALTER PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 2),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@RejectedPrice decimal(18, 9),
	@SingleRejectionSystemId int = NULL,
	@MultipleRejectionsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL,
	@RejectionDirection varchar(16)
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionDirectionId BIT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = @RejectionDirection
	
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'RejectionDirection not found in DB: %s', 16, 2, @RejectionDirection) WITH SETERROR
	END

	BEGIN TRY
	
		BEGIN TRANSACTION tr1;
		
		IF @MultipleRejectionsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@RejectedPrice
				,@RejectionReason
				,@InternalOrderId
				,@RejectionDirectionId
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleRejectionsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external rejection for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId])
			VALUES
				(ISNULL(@SingleRejectionSystemId, -1)
				,@AmountBasePolRejected
				,@SymbolId
				,@CounterpartyId
				,@RejectedPrice
				,@RejectionReason
				,@InternalOrderId
				,@RejectionDirectionId)
		END

		INSERT INTO 
		[dbo].[DealExternalRejected]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[SymbolId]
			   ,[CounterpartyId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolRejected]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[RejectionReason]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[RejectedPrice]
			   ,[RejectionDirectionId])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@SymbolId
			   ,@CounterpartyId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolRejected
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@RejectionReason
			   ,@CounterpartySentExecutionReportUtc
			   ,@RejectedPrice
			   ,@RejectionDirectionId)
		
		COMMIT TRANSACTION tr1;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION tr1;
		THROW
	END CATCH
END
GO


ALTER PROCEDURE [dbo].[GetStatisticsPerCounterparty_Daily_SP] (
	@Day DATE
	)
AS
BEGIN

	DECLARE @KGTRejectionDirectionId BIT
	SELECT @KGTRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'
	DECLARE @CtpRejectionDirectionId BIT
	SELECT @CtpRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'CtpRejectd'
		
	DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		KGTRejectionsNum int,
		CtpRejectionsNum int,
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int
	)

	INSERT INTO @temp_rejections(CounterpartyId, KGTRejectionsNum, CtpRejectionsNum, IntegratorLatencyNanoseconds, IntegratorLatencyCases)
	SELECT
		[CounterpartyId],
		SUM(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN 1 ELSE 0 END) AS KGTRejections, 
		SUM(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN 1 ELSE 0 END) AS CtpRejections,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
	FROM
		[dbo].[DealExternalRejected_DailyView] ctpRej WITH(NOEXPAND, NOLOCK)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]

	UNION ALL

	SELECT
			[CounterpartyId],
			COUNT([CounterpartyId]) AS KGTRejections,
			NULL AS CtpRejections,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily] kgtRej WITH(NOLOCK)
		WHERE
			[IntegratorRejectedAmountBasePol] != 0
			AND [DatePartIntegratorReceivedOrderUtc] = @Day
			--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
		GROUP BY
			[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		DealsNum int,
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int,
		CounterpartyLatencyNanoseconds bigint,
		CounterpartyLatencyCases int
	)

	INSERT INTO 
		@temp_NOPs_pol
		(
		SymbolId, 
		CounterpartyId, 
		NopBasePol, 
		NopTermPol, 
		VolumeBaseAbs, 
		DealsNum, 
		IntegratorLatencyNanoseconds, 
		IntegratorLatencyCases, 
		CounterpartyLatencyNanoseconds, 
		CounterpartyLatencyCases
		)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases,
		SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
		COUNT(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyCases
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
		--INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		deal.SymbolId,
		deal.CounterpartyId
		

	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		RejectionsStatistics.KGTRejectionsNum AS KGTRejectionsNum,
		RejectionsStatistics.CtpRejectionsNum AS CtpRejectionsNum,
		RejectionsStatistics.KGTRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.KGTRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0)) AS KGTRejectionsRate,
		RejectionsStatistics.CtpRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.CtpRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0)) AS CtpRejectionsRate,
		--this calculates average of both numbers if both are non-null; or average of the nonull case if just one is not null (as 2*A / 2*B = A/B); or NULL if both are NULL (avoids divide by zero exception)
		(COALESCE(RejectionsStatistics.IntegratorLatencyNanoseconds, DealsStatistics.IntegratorLatencyNanoseconds) + COALESCE(DealsStatistics.IntegratorLatencyNanoseconds, RejectionsStatistics.IntegratorLatencyNanoseconds))
		/
		(COALESCE(RejectionsStatistics.IntegratorLatencyCases, DealsStatistics.IntegratorLatencyCases) + COALESCE(DealsStatistics.IntegratorLatencyCases, RejectionsStatistics.IntegratorLatencyCases))
		AS AvgIntegratorLatencyNanoseconds,
		DealsStatistics.CounterpartyLatencyNanoseconds / DealsStatistics.CounterpartyLatencyCases AS AvgCounterpartyLatencyNanoseconds
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			-- from SHORTs
			SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs,
			--SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(DealsNum) AS DealsNum,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
			SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
			SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(DealsNum) AS DealsNum,
				SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
				SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
				SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
				SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.DealsNum AS DealsNum,
					basePos.IntegratorLatencyNanoseconds AS IntegratorLatencyNanoseconds,
					basePos.IntegratorLatencyCases AS IntegratorLatencyCases,
					basePos.CounterpartyLatencyNanoseconds AS CounterpartyLatencyNanoseconds,
					basePos.CounterpartyLatencyCases AS CounterpartyLatencyCases
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd,
					NULL AS IntegratorLatencyNanoseconds,
					NULL AS IntegratorLatencyCases,
					NULL AS CounterpartyLatencyNanoseconds,
					NULL AS CounterpartyLatencyCases
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode
END
GO

ALTER PROCEDURE [dbo].[GetStatisticsPerCounterparty_Daily_SP] (
	@Day DATE
	)
AS
BEGIN

	DECLARE @KGTRejectionDirectionId BIT
	SELECT @KGTRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'
	DECLARE @CtpRejectionDirectionId BIT
	SELECT @CtpRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'CtpRejectd'
		
	DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		KGTRejectionsNum int,
		CtpRejectionsNum int,
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int
	)

	INSERT INTO @temp_rejections(CounterpartyId, KGTRejectionsNum, CtpRejectionsNum, IntegratorLatencyNanoseconds, IntegratorLatencyCases)
	SELECT
		[CounterpartyId],
		SUM(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN 1 ELSE 0 END) AS KGTRejections, 
		SUM(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN 1 ELSE 0 END) AS CtpRejections,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
	FROM
		[dbo].[DealExternalRejected_DailyView] ctpRej WITH(NOEXPAND, NOLOCK)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]

	UNION ALL

	SELECT
			[CounterpartyId],
			COUNT([CounterpartyId]) AS KGTRejections,
			NULL AS CtpRejections,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily] kgtRej WITH(NOLOCK)
		WHERE
			[IntegratorRejectedAmountBasePol] != 0
			AND [DatePartIntegratorReceivedOrderUtc] = @Day
			--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
		GROUP BY
			[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		DealsNum int,
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int,
		CounterpartyLatencyNanoseconds bigint,
		CounterpartyLatencyCases int
	)

	INSERT INTO 
		@temp_NOPs_pol
		(
		SymbolId, 
		CounterpartyId, 
		NopBasePol, 
		NopTermPol, 
		VolumeBaseAbs, 
		DealsNum, 
		IntegratorLatencyNanoseconds, 
		IntegratorLatencyCases, 
		CounterpartyLatencyNanoseconds, 
		CounterpartyLatencyCases
		)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases,
		SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
		COUNT(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyCases
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
		--INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		deal.SymbolId,
		deal.CounterpartyId
		

	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		CASE WHEN COALESCE(RejectionsStatistics.KGTRejectionsNum,0) = 0 THEN NULL ELSE RejectionsStatistics.KGTRejectionsNum END AS KGTRejectionsNum,
		CASE WHEN COALESCE(RejectionsStatistics.CtpRejectionsNum,0) = 0 THEN NULL ELSE RejectionsStatistics.CtpRejectionsNum END AS CtpRejectionsNum,
		CASE WHEN COALESCE(RejectionsStatistics.KGTRejectionsNum,0) = 0
		THEN
			 NULL
		ELSE
			RejectionsStatistics.KGTRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.KGTRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0))
		END AS KGTRejectionsRate,
		CASE WHEN COALESCE(RejectionsStatistics.CtpRejectionsNum,0) = 0
		THEN
			NULL
		ELSE
			RejectionsStatistics.CtpRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.CtpRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0))
		END AS CtpRejectionsRate,
		--this calculates average of both numbers if both are non-null; or average of the nonull case if just one is not null (as 2*A / 2*B = A/B); or NULL if both are NULL (avoids divide by zero exception)
		(COALESCE(RejectionsStatistics.IntegratorLatencyNanoseconds, DealsStatistics.IntegratorLatencyNanoseconds) + COALESCE(DealsStatistics.IntegratorLatencyNanoseconds, RejectionsStatistics.IntegratorLatencyNanoseconds))
		/
		(COALESCE(RejectionsStatistics.IntegratorLatencyCases, DealsStatistics.IntegratorLatencyCases) + COALESCE(DealsStatistics.IntegratorLatencyCases, RejectionsStatistics.IntegratorLatencyCases))
		AS AvgIntegratorLatencyNanoseconds,
		DealsStatistics.CounterpartyLatencyNanoseconds / DealsStatistics.CounterpartyLatencyCases AS AvgCounterpartyLatencyNanoseconds
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			-- from SHORTs
			SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs,
			--SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(DealsNum) AS DealsNum,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
			SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
			SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(DealsNum) AS DealsNum,
				SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
				SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
				SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
				SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.DealsNum AS DealsNum,
					basePos.IntegratorLatencyNanoseconds AS IntegratorLatencyNanoseconds,
					basePos.IntegratorLatencyCases AS IntegratorLatencyCases,
					basePos.CounterpartyLatencyNanoseconds AS CounterpartyLatencyNanoseconds,
					basePos.CounterpartyLatencyCases AS CounterpartyLatencyCases
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd,
					NULL AS IntegratorLatencyNanoseconds,
					NULL AS IntegratorLatencyCases,
					NULL AS CounterpartyLatencyNanoseconds,
					NULL AS CounterpartyLatencyCases
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode
END
GO


----------------------------------- FS2 --------------------------------------------------

INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (49, N'FXCM', N'FS2', 1)
GO

INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FS2'), 8.25)
GO

INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FS2'), 'Stream')


INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'FS2_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGTLNL2L_LD4_PRICE

[SESSION]
DataDictionary=FIX42_FCM.xml
BeginString=FIX.4.2
TargetCompID=FASTMATCH2_LD4_PRICE
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=11864
SocketConnectHost=169.33.101.137', GETUTCDATE())
GO

INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'FS2_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGTLN2LL_LD4_TRADE

[SESSION]
DataDictionary=FIX42_FCM.xml
BeginString=FIX.4.2
TargetCompID=FASTMATCH2_LD4_TRADE
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=11863
SocketConnectHost=169.33.101.137', GETUTCDATE())
GO


DECLARE @F1OrdFixSettingId INT
DECLARE @F1QuoFixSettingId INT

SELECT
	@F1OrdFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'FS2_ORD'
	
SELECT
	@F1QuoFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'FS2_QUO'


INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [IntegratorEnvironmentId], @F1OrdFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]

INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [IntegratorEnvironmentId], @F1QuoFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]
GO

DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_DBK" type="FIXChannel_DBKSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM2" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM3" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC1" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC2" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS1" type="FIXChannel_FCMMMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS2" type="FIXChannel_FCMMMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
      <xs:element minOccurs="1" maxOccurs="1" name="STP" type="STPSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_DBKSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DealConfirmationSupported" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumDealConfirmationDelay_Milliseconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IocApplyTtl" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ApplyMaxHoldTime" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxHoldTime" type="FXCMMaxHoldTime" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MaxPerSymbolPricesPerSecondSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultMaxAllowedPerSecondRate" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialSymbolRateSettings" type="ArrayOfSymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolRateSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="SymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolRateSetting">
    <xs:attribute name="Name" type="xs:string" />
    <xs:attribute name="MaxAllowedPerSecondRate" type="xs:int" use="required" />
  </xs:complexType>
  <xs:complexType name="RejectionRateDisconnectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumOrdersToApplyCheck" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAllowedRejectionRatePercent" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMMMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxOrderHoldTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxTotalPricesPerSecond" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="MaxPerSymbolPricesPerSecondSetting" type="MaxPerSymbolPricesPerSecondSetting" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExpectedPricingDepth" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="RejectionRateDisconnectSettings" type="RejectionRateDisconnectSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXSTPChannel_CTIPBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNongenuineInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialRejectionsHandling" type="ArrayOfSpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SpecialRejection" type="SpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="TriggerPhrase" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="FXCMMaxHoldTime">
    <xs:restriction base="xs:string">
      <xs:enumeration value="UpTo1ms" />
      <xs:enumeration value="UpTo30ms" />
      <xs:enumeration value="UpTo100ms" />
      <xs:enumeration value="UpTo500ms" />
      <xs:enumeration value="UpTo3000ms" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="MarketSessionIntoSeparateProcess" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="STPDestinations" type="ArrayOfSTPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSTPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="STPDestination" nillable="true" type="STPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Name" type="STPCounterparty" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedTicketTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="ServedCounterparties" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="STPCounterparty">
    <xs:restriction base="xs:string">
      <xs:enumeration value="CTIPB" />
      <xs:enumeration value="Traiana" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'

--Manually!: Add

--<FIXChannel_FS2>
  --  <MaxOrderHoldTime_Milliseconds>95</MaxOrderHoldTime_Milliseconds>
  --  <MaxTotalPricesPerSecond>2950</MaxTotalPricesPerSecond>
  --  <MaxPerSymbolPricesPerSecondSetting>
  --    <DefaultMaxAllowedPerSecondRate>90</DefaultMaxAllowedPerSecondRate>
  --    <SpecialSymbolRateSettings>
  --      <!--<Symbol Name="AUD/CAD" MaxAllowedPerSecondRate="30" />-->
  --    </SpecialSymbolRateSettings>
  --  </MaxPerSymbolPricesPerSecondSetting>
  --  <ExpectedPricingDepth>3</ExpectedPricingDepth>
  --  <RejectionRateDisconnectSettings>
  --    <RiskCheckAllowed>true</RiskCheckAllowed>
  --    <MinimumOrdersToApplyCheck>50</MinimumOrdersToApplyCheck>
  --    <MaxAllowedRejectionRatePercent>50</MaxAllowedRejectionRatePercent>
  --  </RejectionRateDisconnectSettings>
  --</FIXChannel_FS
  
  -- +
  
  -- Add FS2 everywhere where is FS1
  
  
  -- LMAX checkbox
  
  
ALTER TABLE [dbo].[VenueStreamSettings] ADD ReferenceLmaxBookOnly [bit] NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ADD ReferenceLmaxBookOnly [bit] NULL
GO
UPDATE [dbo].[VenueStreamSettings] SET ReferenceLmaxBookOnly = 0
UPDATE [dbo].[VenueStreamSettings_Changes] SET ReferenceLmaxBookOnly = 0
GO
ALTER TABLE [dbo].[VenueStreamSettings] ALTER COLUMN ReferenceLmaxBookOnly [bit] NOT NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ALTER COLUMN ReferenceLmaxBookOnly [bit] NOT NULL
GO

ALTER TRIGGER [dbo].[VenueStreamSettings_DeletedOrUpdated] ON [dbo].[VenueStreamSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueStreamSettings_Changes]
	   ([SymbolId]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[MaximumSizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade] 
	   ,[KGTLLTime_milliseconds]
	   ,[PreferLLDestinationCheckToLmax]
	   ,[MinimumBPGrossToAccept]
	   ,[PreferLmaxDuringHedging]   
	   ,[MinimumIntegratorPriceLife_milliseconds]
	   ,[MinimumSizeToTrade]
	   ,[MinimumFillSize]
	   ,[MaximumSourceBookTiers]
	   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
	   ,[ReferenceLmaxBookOnly]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[MaximumSizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[KGTLLTime_milliseconds]
	    ,deleted.[PreferLLDestinationCheckToLmax]
	    ,deleted.[MinimumBPGrossToAccept]
	    ,deleted.[PreferLmaxDuringHedging]
		,deleted.[MinimumIntegratorPriceLife_milliseconds]
		,deleted.[MinimumSizeToTrade]
	    ,deleted.[MinimumFillSize]
		,deleted.[MaximumSourceBookTiers]
		,deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]
		,deleted.[ReferenceLmaxBookOnly]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueStreamSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueStreamSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([KGTLLTime_milliseconds]) AND (topUpdatedChange.[KGTLLTime_milliseconds] IS NULL OR topUpdatedChange.[KGTLLTime_milliseconds] != deleted.[KGTLLTime_milliseconds]))
		OR (UPDATE([PreferLLDestinationCheckToLmax]) AND (topUpdatedChange.[PreferLLDestinationCheckToLmax] IS NULL OR topUpdatedChange.[PreferLLDestinationCheckToLmax] != deleted.[PreferLLDestinationCheckToLmax]))
		OR (UPDATE([MinimumBPGrossToAccept]) AND (topUpdatedChange.[MinimumBPGrossToAccept] IS NULL OR topUpdatedChange.[MinimumBPGrossToAccept] != deleted.[MinimumBPGrossToAccept]))
		OR (UPDATE([PreferLmaxDuringHedging]) AND (topUpdatedChange.[PreferLmaxDuringHedging] IS NULL OR topUpdatedChange.[PreferLmaxDuringHedging] != deleted.[PreferLmaxDuringHedging]))
		OR (UPDATE([MinimumIntegratorPriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] != deleted.[MinimumIntegratorPriceLife_milliseconds]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([MaximumSourceBookTiers]) AND (topUpdatedChange.[MaximumSourceBookTiers] IS NULL OR topUpdatedChange.[MaximumSourceBookTiers] != deleted.[MaximumSourceBookTiers]))
		OR (UPDATE([MinimumBasisPointsFromKgtPriceToFastCancel]) AND (topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] IS NULL OR topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] != deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]))
		OR (UPDATE([ReferenceLmaxBookOnly]) AND (topUpdatedChange.[ReferenceLmaxBookOnly] IS NULL OR topUpdatedChange.[ReferenceLmaxBookOnly] != deleted.[ReferenceLmaxBookOnly]))
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueStreamSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueStreamSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MaximumSizeToTrade]
		    ,[BestPriceImprovementOffsetBasisPoints]
		    ,[MaximumDiscountBasisPointsToTrade] 
		    ,[KGTLLTime_milliseconds]
		    ,[PreferLLDestinationCheckToLmax]
		    ,[MinimumBPGrossToAccept]
		    ,[PreferLmaxDuringHedging]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,[MinimumBasisPointsFromKgtPriceToFastCancel]
			,[ReferenceLmaxBookOnly]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueStreamSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name
	END
END
GO

ALTER PROCEDURE [dbo].[GetVenueStreamSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[PreferLLDestinationCheckToLmax],
		sett.[MinimumBPGrossToAccept],
		sett.[MinimumBPGrossToAccept] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossToAcceptDecimal],
		sett.[PreferLmaxDuringHedging],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[ReferenceLmaxBookOnly],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[VenueStreamSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel  [decimal](18,6),
	@ReferenceLmaxBookOnly [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Stream'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Stream TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	
	BEGIN TRANSACTION
	BEGIN TRY
		
		INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

		SELECT @TradingSystemId = scope_identity();
		
		--make sure that default switches values are correct
		INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
		SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
		FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
		WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
		
		INSERT INTO [dbo].[VenueStreamSettings]
			   ([SymbolId]
			   ,[CounterpartyId]
			   ,[TradingSystemId]
			   ,[MaximumSizeToTrade]
			   ,[BestPriceImprovementOffsetBasisPoints]
			   ,[MaximumDiscountBasisPointsToTrade]
			   ,[KGTLLTime_milliseconds]
			   ,[PreferLLDestinationCheckToLmax]
			   ,[MinimumBPGrossToAccept]
			   ,[PreferLmaxDuringHedging]
			   ,[MinimumIntegratorPriceLife_milliseconds]
			   ,[MinimumSizeToTrade]
			   ,[MinimumFillSize]
			   ,[MaximumSourceBookTiers]
			   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
			   ,[ReferenceLmaxBookOnly])
		 VALUES
			   (@SymbolId
			   ,@CounterpartyId
			   ,@TradingSystemId
			   ,@MaximumSizeToTrade 
			   ,@BestPriceImprovementOffsetBasisPoints
			   ,@MaximumDiscountBasisPointsToTrade
			   ,@KGTLLTime_milliseconds
			   ,@PreferLLDestinationCheckToLmax
			   ,@MinimumBPGrossToAccept
			   ,@PreferLmaxDuringHedging
			   ,@MinimumIntegratorPriceLife_milliseconds
			   ,@MinimumSizeToTrade
			   ,@MinimumFillSize
			   ,@MaximumSourceBookTiers
			   ,@MinimumBasisPointsFromKgtPriceToFastCancel
			   ,@ReferenceLmaxBookOnly)
		
	
		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[VenueStreamSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel [decimal](18,6),
	@ReferenceLmaxBookOnly [bit]
)
AS
BEGIN
	UPDATE sett
	SET
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[KGTLLTime_milliseconds] = @KGTLLTime_milliseconds,
		[PreferLLDestinationCheckToLmax] = @PreferLLDestinationCheckToLmax,
		[MinimumBPGrossToAccept] = @MinimumBPGrossToAccept,
		[PreferLmaxDuringHedging] = @PreferLmaxDuringHedging,
		[MinimumIntegratorPriceLife_milliseconds] = @MinimumIntegratorPriceLife_milliseconds,
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MinimumFillSize] = @MinimumFillSize,
		[MaximumSourceBookTiers] = @MaximumSourceBookTiers,
		[MinimumBasisPointsFromKgtPriceToFastCancel] = @MinimumBasisPointsFromKgtPriceToFastCancel,
		[ReferenceLmaxBookOnly] = @ReferenceLmaxBookOnly
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

--------------------- CptClient id ----------------------


ALTER TABLE [dbo].[DealExternalRejected] ADD CounterpartyClientId varchar(20) NULL
GO

ALTER PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 2),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@RejectedPrice decimal(18, 9),
	@SingleRejectionSystemId int = NULL,
	@MultipleRejectionsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL,
	@RejectionDirection varchar(16),
	@CounterpartyClientId varchar(20) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionDirectionId BIT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = @RejectionDirection
	
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'RejectionDirection not found in DB: %s', 16, 2, @RejectionDirection) WITH SETERROR
	END

	BEGIN TRY
	
		BEGIN TRANSACTION tr1;
		
		IF @MultipleRejectionsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@RejectedPrice
				,@RejectionReason
				,@InternalOrderId
				,@RejectionDirectionId
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleRejectionsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external rejection for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId])
			VALUES
				(ISNULL(@SingleRejectionSystemId, -1)
				,@AmountBasePolRejected
				,@SymbolId
				,@CounterpartyId
				,@RejectedPrice
				,@RejectionReason
				,@InternalOrderId
				,@RejectionDirectionId)
		END

		INSERT INTO 
		[dbo].[DealExternalRejected]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[SymbolId]
			   ,[CounterpartyId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolRejected]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[RejectionReason]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[RejectedPrice]
			   ,[RejectionDirectionId]
			   ,[CounterpartyClientId])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@SymbolId
			   ,@CounterpartyId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolRejected
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@RejectionReason
			   ,@CounterpartySentExecutionReportUtc
			   ,@RejectedPrice
			   ,@RejectionDirectionId
			   ,@CounterpartyClientId)
		
		COMMIT TRANSACTION tr1;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION tr1;
		THROW
	END CATCH
END
GO


--------------------- bid and ask checkboxes ----------------------
 
ALTER TABLE [dbo].[VenueStreamSettings] ADD BidEnabled [bit] NULL
ALTER TABLE [dbo].[VenueStreamSettings] ADD AskEnabled [bit] NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ADD BidEnabled [bit] NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ADD AskEnabled [bit] NULL
ALTER TABLE [dbo].[VenueMMSettings] ADD BidEnabled [bit] NULL
ALTER TABLE [dbo].[VenueMMSettings] ADD AskEnabled [bit] NULL
ALTER TABLE [dbo].[VenueMMSettings_Changes] ADD BidEnabled [bit] NULL
ALTER TABLE [dbo].[VenueMMSettings_Changes] ADD AskEnabled [bit] NULL
GO
UPDATE [dbo].[VenueStreamSettings] SET BidEnabled = 1
UPDATE [dbo].[VenueStreamSettings] SET AskEnabled = 1
UPDATE [dbo].[VenueStreamSettings_Changes] SET BidEnabled = 1
UPDATE [dbo].[VenueStreamSettings_Changes] SET AskEnabled = 1
UPDATE [dbo].[VenueMMSettings] SET BidEnabled = 1
UPDATE [dbo].[VenueMMSettings] SET AskEnabled = 1
UPDATE [dbo].[VenueMMSettings_Changes] SET BidEnabled = 1
UPDATE [dbo].[VenueMMSettings_Changes] SET AskEnabled = 1
GO
ALTER TABLE [dbo].[VenueStreamSettings] ALTER COLUMN BidEnabled [bit] NOT NULL
ALTER TABLE [dbo].[VenueStreamSettings] ALTER COLUMN AskEnabled [bit] NOT NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ALTER COLUMN BidEnabled [bit] NOT NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ALTER COLUMN AskEnabled [bit] NOT NULL
ALTER TABLE [dbo].[VenueMMSettings] ALTER COLUMN BidEnabled [bit] NOT NULL
ALTER TABLE [dbo].[VenueMMSettings] ALTER COLUMN AskEnabled [bit] NOT NULL
ALTER TABLE [dbo].[VenueMMSettings_Changes] ALTER COLUMN BidEnabled [bit] NOT NULL
ALTER TABLE [dbo].[VenueMMSettings_Changes] ALTER COLUMN AskEnabled [bit] NOT NULL
GO

ALTER TRIGGER [dbo].[VenueStreamSettings_DeletedOrUpdated] ON [dbo].[VenueStreamSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueStreamSettings_Changes]
	   ([SymbolId]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[MaximumSizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade] 
	   ,[KGTLLTime_milliseconds]
	   ,[PreferLLDestinationCheckToLmax]
	   ,[MinimumBPGrossToAccept]
	   ,[PreferLmaxDuringHedging]   
	   ,[MinimumIntegratorPriceLife_milliseconds]
	   ,[MinimumSizeToTrade]
	   ,[MinimumFillSize]
	   ,[MaximumSourceBookTiers]
	   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
	   ,[ReferenceLmaxBookOnly]
	   ,[BidEnabled]
	   ,[AskEnabled]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[MaximumSizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[KGTLLTime_milliseconds]
	    ,deleted.[PreferLLDestinationCheckToLmax]
	    ,deleted.[MinimumBPGrossToAccept]
	    ,deleted.[PreferLmaxDuringHedging]
		,deleted.[MinimumIntegratorPriceLife_milliseconds]
		,deleted.[MinimumSizeToTrade]
	    ,deleted.[MinimumFillSize]
		,deleted.[MaximumSourceBookTiers]
		,deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]
		,deleted.[ReferenceLmaxBookOnly]
		,deleted.[BidEnabled]
		,deleted.[AskEnabled]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueStreamSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueStreamSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([KGTLLTime_milliseconds]) AND (topUpdatedChange.[KGTLLTime_milliseconds] IS NULL OR topUpdatedChange.[KGTLLTime_milliseconds] != deleted.[KGTLLTime_milliseconds]))
		OR (UPDATE([PreferLLDestinationCheckToLmax]) AND (topUpdatedChange.[PreferLLDestinationCheckToLmax] IS NULL OR topUpdatedChange.[PreferLLDestinationCheckToLmax] != deleted.[PreferLLDestinationCheckToLmax]))
		OR (UPDATE([MinimumBPGrossToAccept]) AND (topUpdatedChange.[MinimumBPGrossToAccept] IS NULL OR topUpdatedChange.[MinimumBPGrossToAccept] != deleted.[MinimumBPGrossToAccept]))
		OR (UPDATE([PreferLmaxDuringHedging]) AND (topUpdatedChange.[PreferLmaxDuringHedging] IS NULL OR topUpdatedChange.[PreferLmaxDuringHedging] != deleted.[PreferLmaxDuringHedging]))
		OR (UPDATE([MinimumIntegratorPriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] != deleted.[MinimumIntegratorPriceLife_milliseconds]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([MaximumSourceBookTiers]) AND (topUpdatedChange.[MaximumSourceBookTiers] IS NULL OR topUpdatedChange.[MaximumSourceBookTiers] != deleted.[MaximumSourceBookTiers]))
		OR (UPDATE([MinimumBasisPointsFromKgtPriceToFastCancel]) AND (topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] IS NULL OR topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] != deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]))
		OR (UPDATE([ReferenceLmaxBookOnly]) AND (topUpdatedChange.[ReferenceLmaxBookOnly] IS NULL OR topUpdatedChange.[ReferenceLmaxBookOnly] != deleted.[ReferenceLmaxBookOnly]))
		OR (UPDATE([BidEnabled]) AND (topUpdatedChange.[BidEnabled] IS NULL OR topUpdatedChange.[BidEnabled] != deleted.[BidEnabled]))
		OR (UPDATE([AskEnabled]) AND (topUpdatedChange.[AskEnabled] IS NULL OR topUpdatedChange.[AskEnabled] != deleted.[AskEnabled]))
END
GO

ALTER TRIGGER [dbo].[VenueMMSettings_DeletedOrUpdated] ON [dbo].[VenueMMSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueMMSettings_Changes]
	   ([SymbolId]
	   ,[MaximumSizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade]
	   ,[MaximumWaitTimeOnImprovementTick_milliseconds]
	   ,[MinimumIntegratorPriceLife_milliseconds]
	   ,[MinimumSizeToTrade]
	   ,[MinimumFillSize]
	   ,[MaximumSourceBookTiers]
	   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
	   ,[BidEnabled]
	   ,[AskEnabled]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[MaximumSizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]
		,deleted.[MinimumIntegratorPriceLife_milliseconds]
		,deleted.[MinimumSizeToTrade]
	    ,deleted.[MinimumFillSize]
		,deleted.[MaximumSourceBookTiers]
		,deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]
		,deleted.[BidEnabled]
		,deleted.[AskEnabled]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueMMSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueMMSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([MaximumWaitTimeOnImprovementTick_milliseconds]) AND (topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] IS NULL OR topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] != deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]))
		OR (UPDATE([MinimumIntegratorPriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] != deleted.[MinimumIntegratorPriceLife_milliseconds]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([MaximumSourceBookTiers]) AND (topUpdatedChange.[MaximumSourceBookTiers] IS NULL OR topUpdatedChange.[MaximumSourceBookTiers] != deleted.[MaximumSourceBookTiers]))
		OR (UPDATE([MinimumBasisPointsFromKgtPriceToFastCancel]) AND (topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] IS NULL OR topUpdatedChange.[MinimumBasisPointsFromKgtPriceToFastCancel] != deleted.[MinimumBasisPointsFromKgtPriceToFastCancel]))
		OR (UPDATE([BidEnabled]) AND (topUpdatedChange.[BidEnabled] IS NULL OR topUpdatedChange.[BidEnabled] != deleted.[BidEnabled]))
		OR (UPDATE([AskEnabled]) AND (topUpdatedChange.[AskEnabled] IS NULL OR topUpdatedChange.[AskEnabled] != deleted.[AskEnabled]))
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueStreamSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueStreamSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MaximumSizeToTrade]
		    ,[BestPriceImprovementOffsetBasisPoints]
		    ,[MaximumDiscountBasisPointsToTrade] 
		    ,[KGTLLTime_milliseconds]
		    ,[PreferLLDestinationCheckToLmax]
		    ,[MinimumBPGrossToAccept]
		    ,[PreferLmaxDuringHedging]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,[MinimumBasisPointsFromKgtPriceToFastCancel]
			,[ReferenceLmaxBookOnly]
			,[BidEnabled]
			,[AskEnabled]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueStreamSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name
	END
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueMMSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueMMSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice]
			,[MaximumSizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,[MinimumBasisPointsFromKgtPriceToFastCancel]
			,[BidEnabled]
			,[AskEnabled]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueMMSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name
	END
END
GO

ALTER PROCEDURE [dbo].[GetVenueStreamSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[PreferLLDestinationCheckToLmax],
		sett.[MinimumBPGrossToAccept],
		sett.[MinimumBPGrossToAccept] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossToAcceptDecimal],
		sett.[PreferLmaxDuringHedging],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[ReferenceLmaxBookOnly],
		sett.[BidEnabled],
		sett.[AskEnabled],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueMMSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		sett.[MinimumBasisPointsFromKgtPriceToFastCancel],
		sett.[BidEnabled],
		sett.[AskEnabled],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[VenueStreamSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@BidEnabled [bit],
	@AskEnabled [bit],
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel  [decimal](18,6),
	@ReferenceLmaxBookOnly [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Stream'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Stream TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	
	BEGIN TRANSACTION
	BEGIN TRY
		
		INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

		SELECT @TradingSystemId = scope_identity();
		
		--make sure that default switches values are correct
		INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
		SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
		FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
		WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
		
		INSERT INTO [dbo].[VenueStreamSettings]
			   ([SymbolId]
			   ,[CounterpartyId]
			   ,[TradingSystemId]
			   ,[MaximumSizeToTrade]
			   ,[BestPriceImprovementOffsetBasisPoints]
			   ,[MaximumDiscountBasisPointsToTrade]
			   ,[KGTLLTime_milliseconds]
			   ,[PreferLLDestinationCheckToLmax]
			   ,[MinimumBPGrossToAccept]
			   ,[PreferLmaxDuringHedging]
			   ,[MinimumIntegratorPriceLife_milliseconds]
			   ,[MinimumSizeToTrade]
			   ,[MinimumFillSize]
			   ,[MaximumSourceBookTiers]
			   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
			   ,[ReferenceLmaxBookOnly]
			   ,[BidEnabled]
			   ,[AskEnabled])
		 VALUES
			   (@SymbolId
			   ,@CounterpartyId
			   ,@TradingSystemId
			   ,@MaximumSizeToTrade 
			   ,@BestPriceImprovementOffsetBasisPoints
			   ,@MaximumDiscountBasisPointsToTrade
			   ,@KGTLLTime_milliseconds
			   ,@PreferLLDestinationCheckToLmax
			   ,@MinimumBPGrossToAccept
			   ,@PreferLmaxDuringHedging
			   ,@MinimumIntegratorPriceLife_milliseconds
			   ,@MinimumSizeToTrade
			   ,@MinimumFillSize
			   ,@MaximumSourceBookTiers
			   ,@MinimumBasisPointsFromKgtPriceToFastCancel
			   ,@ReferenceLmaxBookOnly
			   ,@BidEnabled
			   ,@AskEnabled)
		
	
		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,	
	@MinimumBasisPointsFromKgtPriceToFastCancel  [decimal](18,6),
	@BidEnabled [bit],
	@AskEnabled [bit],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'MM'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'MM TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueMMSettings]
           ([SymbolId]
           ,[MaximumSizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
		   ,[MinimumIntegratorPriceLife_milliseconds]
		   ,[MinimumSizeToTrade]
		   ,[MinimumFillSize]
		   ,[MaximumSourceBookTiers]
		   ,[MinimumBasisPointsFromKgtPriceToFastCancel]
		   ,[BidEnabled]
		   ,[AskEnabled]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MaximumSizeToTrade 
           ,@BestPriceImprovementOffsetBasisPoints
           ,@MaximumDiscountBasisPointsToTrade
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
		   ,@MinimumIntegratorPriceLife_milliseconds
		   ,@MinimumSizeToTrade
		   ,@MinimumFillSize
		   ,@MaximumSourceBookTiers
		   ,@MinimumBasisPointsFromKgtPriceToFastCancel
		   ,@BidEnabled
		   ,@AskEnabled
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

ALTER PROCEDURE [dbo].[VenueStreamSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel [decimal](18,6),
	@ReferenceLmaxBookOnly [bit],
	@BidEnabled [bit],
	@AskEnabled [bit]
)
AS
BEGIN
	UPDATE sett
	SET
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[KGTLLTime_milliseconds] = @KGTLLTime_milliseconds,
		[PreferLLDestinationCheckToLmax] = @PreferLLDestinationCheckToLmax,
		[MinimumBPGrossToAccept] = @MinimumBPGrossToAccept,
		[PreferLmaxDuringHedging] = @PreferLmaxDuringHedging,
		[MinimumIntegratorPriceLife_milliseconds] = @MinimumIntegratorPriceLife_milliseconds,
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MinimumFillSize] = @MinimumFillSize,
		[MaximumSourceBookTiers] = @MaximumSourceBookTiers,
		[MinimumBasisPointsFromKgtPriceToFastCancel] = @MinimumBasisPointsFromKgtPriceToFastCancel,
		[ReferenceLmaxBookOnly] = @ReferenceLmaxBookOnly,
		[BidEnabled] = @BidEnabled,
		[AskEnabled] = @AskEnabled
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel [decimal](18,6),
	@BidEnabled [bit],
	@AskEnabled [bit]
)
AS
BEGIN
	UPDATE sett
	SET
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds,
		[MinimumIntegratorPriceLife_milliseconds] = @MinimumIntegratorPriceLife_milliseconds,
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MinimumFillSize] = @MinimumFillSize,
		[MaximumSourceBookTiers] = @MaximumSourceBookTiers,
		[MinimumBasisPointsFromKgtPriceToFastCancel] = @MinimumBasisPointsFromKgtPriceToFastCancel,
		[BidEnabled] = @BidEnabled,
		[AskEnabled] = @AskEnabled
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO