

Alter Database FXtickDB Add FileGroup TradeDecayFilegroup
GO
Alter Database FXtickDB Add File
(
Name=TradeDecayFilegroup_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TradeDecay\TradeDecayFilegroup_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TradeDecayFilegroup
GO

BEGIN TRANSACTION Install_18_02_001;
GO

CREATE TABLE [dbo].[TradeDecayAnalysisType](
	[TradeDecayAnalysisTypeId] [tinyint] IDENTITY(1,1) NOT NULL,
	[TradeDecayAnalysisTypeName] [varchar](16) NOT NULL
)
ON [TradeDecayFilegroup]
GO

ALTER TABLE [dbo].[TradeDecayAnalysisType] ADD  CONSTRAINT [PK_TradeDecayAnalysisType] PRIMARY KEY CLUSTERED 
(
	[TradeDecayAnalysisTypeId] ASC
) ON [TradeDecayFilegroup]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_TradeDecayAnalysisTypeName] ON [dbo].[TradeDecayAnalysisType]
(
	[TradeDecayAnalysisTypeName] ASC
) ON [TradeDecayFilegroup]
GO

INSERT INTO [dbo].[TradeDecayAnalysisType]([TradeDecayAnalysisTypeName]) VALUES('CptDeal')
INSERT INTO [dbo].[TradeDecayAnalysisType]([TradeDecayAnalysisTypeName]) VALUES('CptRejection')
INSERT INTO [dbo].[TradeDecayAnalysisType]([TradeDecayAnalysisTypeName]) VALUES('KgtDeal')
INSERT INTO [dbo].[TradeDecayAnalysisType]([TradeDecayAnalysisTypeName]) VALUES('KgtRejection')
GO

CREATE TABLE [dbo].[TradeDecayAnalysis](
	[TradeDecayAnalysisId] [int] IDENTITY(1,1) NOT NULL,
	[TradeDecayAnalysisTypeId] [tinyint] NOT NULL,
	[SourceEventRecordId] [int] NOT NULL,
	[AnalysisStartTimeUtc] [datetime2](7) NOT NULL,
	[FXPairId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[PriceSideId] [bit] NOT NULL,
	[HasValidTickAfterIntervalEnd] [bit] NOT NULL,
	[IsValidAndComplete] [bit] NOT NULL
)
ON [TradeDecayFilegroup]
GO

ALTER TABLE [dbo].[TradeDecayAnalysis] ADD  CONSTRAINT [PK_TradeDecayAnalysis] PRIMARY KEY NONCLUSTERED 
(
	[TradeDecayAnalysisId] ASC
) ON [TradeDecayFilegroup]
GO

CREATE CLUSTERED INDEX [IX_TradeDecayAnalysis_CounterpartyTime] ON [dbo].[TradeDecayAnalysis]
(
	[CounterpartyId] ASC,
	[AnalysisStartTimeUtc] ASC
) ON [TradeDecayFilegroup]
GO

ALTER TABLE [dbo].[TradeDecayAnalysis]  WITH NOCHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_Type] FOREIGN KEY([TradeDecayAnalysisTypeId])
REFERENCES [dbo].[TradeDecayAnalysisType] ([TradeDecayAnalysisTypeId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis] CHECK CONSTRAINT [FK_TradeDecayAnalysis_Type]
GO

ALTER TABLE [dbo].[TradeDecayAnalysis]  WITH NOCHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis] CHECK CONSTRAINT [FK_TradeDecayAnalysis_FxPair]
GO

ALTER TABLE [dbo].[TradeDecayAnalysis]  WITH CHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_LiqProviderStream] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis] CHECK CONSTRAINT [FK_TradeDecayAnalysis_LiqProviderStream]
GO

ALTER TABLE [dbo].[TradeDecayAnalysis]  WITH CHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_PriceSide] FOREIGN KEY([PriceSideId])
REFERENCES [dbo].[PriceSide] ([PriceSideId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis] CHECK CONSTRAINT [FK_TradeDecayAnalysis_PriceSide]
GO


CREATE TABLE [dbo].[TradeDecayAnalysisDataPoint](
	[TradeDecayAnalysisId] [int] NOT NULL,
	[MillisecondDistanceFromAnalysisStart] [int] NOT NULL,
	[MidToB] [decimal](18, 9) NOT NULL,
	[BpDifferenceFromReferencePrice] [decimal](18, 9) NOT NULL,
	[IsAuthenticValue] [bit] NOT NULL
)
ON [TradeDecayFilegroup]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_TradeDecayAnalysis_CounterpartyTime] ON [dbo].[TradeDecayAnalysisDataPoint]
(
	[TradeDecayAnalysisId] ASC,
	[MillisecondDistanceFromAnalysisStart] ASC
) ON [TradeDecayFilegroup]
GO



CREATE TABLE [dbo].[TradeDecayAnalysis_Pending](
	[PendingTradeDecayAnalysisId] [int] IDENTITY(1,1) NOT NULL,
	[TradeDecayAnalysisTypeId] [tinyint] NOT NULL,
	[SourceEventRecordId] [int] NOT NULL,
	[AnalysisStartTimeUtc] [datetime2](7) NOT NULL,
	[FXPairId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[ReferencePrice] [decimal](18,9) NOT NULL,
	[PriceSideId] [bit] NOT NULL,
	[AcquiredForCalculationTimeUtc] [datetime2](7) NULL
)
ON [TradeDecayFilegroup]
GO

CREATE CLUSTERED INDEX [IX_TradeDecayAnalysis_Pending_Time] ON [dbo].[TradeDecayAnalysis_Pending]
(
	[AnalysisStartTimeUtc] ASC
) ON [TradeDecayFilegroup]
GO


ALTER TABLE [dbo].[TradeDecayAnalysis_Pending]  WITH NOCHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_Pending_Type] FOREIGN KEY([TradeDecayAnalysisTypeId])
REFERENCES [dbo].[TradeDecayAnalysisType] ([TradeDecayAnalysisTypeId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis_Pending] CHECK CONSTRAINT [FK_TradeDecayAnalysis_Pending_Type]
GO

ALTER TABLE [dbo].[TradeDecayAnalysis_Pending]  WITH NOCHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_Pending_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis_Pending] CHECK CONSTRAINT [FK_TradeDecayAnalysis_Pending_FxPair]
GO

ALTER TABLE [dbo].[TradeDecayAnalysis_Pending]  WITH CHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_Pending_LiqProviderStream] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis_Pending] CHECK CONSTRAINT [FK_TradeDecayAnalysis_Pending_LiqProviderStream]
GO

ALTER TABLE [dbo].[TradeDecayAnalysis_Pending]  WITH CHECK ADD  CONSTRAINT [FK_TradeDecayAnalysis_Pending_PriceSide] FOREIGN KEY([PriceSideId])
REFERENCES [dbo].[PriceSide] ([PriceSideId])
GO
ALTER TABLE [dbo].[TradeDecayAnalysis_Pending] CHECK CONSTRAINT [FK_TradeDecayAnalysis_Pending_PriceSide]
GO
	
CREATE TABLE [dbo].[_Internal_CounterpartyIdsMapping](
	[IntegratorDBCounterpartyId] [tinyint] NOT NULL,
	[DCDBCounterpartyId] [tinyint] NOT NULL,
	[CounterpartyCode] [char](3) NOT NULL
)
ON [PRIMARY]
GO	
	
CREATE UNIQUE CLUSTERED INDEX [IX__Internal_CounterpartyIdsMapping_IntegratorDbCpt] ON [dbo].[_Internal_CounterpartyIdsMapping]
(
	[IntegratorDBCounterpartyId] ASC
) ON [PRIMARY]
GO

CREATE UNIQUE INDEX [IX__Internal_CounterpartyIdsMapping_DCDbCpt] ON [dbo].[_Internal_CounterpartyIdsMapping]
(
	[DCDBCounterpartyId] ASC
) ON [PRIMARY]
GO
	
INSERT INTO [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode])
SELECT 
	integratorCpt.[CounterpartyId] AS IntegratorDBCounterpartyId,
	dcCpt.[LiqProviderStreamId] AS DCDBCounterpartyId,
    dcCpt.[LiqProviderStreamName] AS CounterpartyCode
FROM
	[dbo].[LiqProviderStream] dcCpt
	INNER JOIN [Integrator].[dbo].[Counterparty] integratorCpt ON dcCpt.LiqProviderStreamName = integratorCpt.CounterpartyCode
	
	
CREATE TABLE [dbo].[_Internal_SymbolIdsMapping](
	[IntegratorDBSymbolId] [tinyint] NOT NULL,
	[DCDBSymbolId] [tinyint] NOT NULL,
	[SymbolCodeNoSlash] [char](6) NOT NULL
)
ON [PRIMARY]
GO	
	
CREATE UNIQUE CLUSTERED INDEX [IX__Internal_SymbolIdsMapping_IntegratorDbSymbol] ON [dbo].[_Internal_SymbolIdsMapping]
(
	[IntegratorDBSymbolId] ASC
) ON [PRIMARY]
GO

CREATE UNIQUE INDEX [IX__Internal_SymbolIdsMapping_DCDbSymbol] ON [dbo].[_Internal_SymbolIdsMapping]
(
	[DCDBSymbolId] ASC
) ON [PRIMARY]
GO
	
INSERT INTO [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash])
SELECT
	integratorSymbol.[Id] AS IntegratorDBSymbolId,
	dcSymbol.[FxPairId] AS DCDBSymbolId,
    dcSymbol.[FxpCode] AS SymbolCodeNoSlash
FROM 
	[dbo].[FxPair] dcSymbol
	INNER JOIN [Integrator].[dbo].[Symbol_Internal] integratorSymbol ON dcSymbol.[FxpCode] = integratorSymbol.[ShortName]
	
CREATE TABLE [dbo].[_Internal_TradeDecayIntervals](
	[MsDistanceFromEventStart] [int] NOT NULL
)
ON [PRIMARY]
GO	
	
CREATE UNIQUE CLUSTERED INDEX [IX__Internal_TradeDecayIntervals] ON [dbo].[_Internal_TradeDecayIntervals]
(
	[MsDistanceFromEventStart] ASC
) ON [PRIMARY]
GO	
	
DECLARE @i INT = 0
WHILE @i <= 300000
BEGIN
	INSERT INTO [dbo]._Internal_TradeDecayIntervals (MsDistanceFromEventStart) VALUES (@i)
	SET @i = CASE 
		WHEN @i >= 10000 THEN @i + 1000
		WHEN @i >= 1000 THEN @i + 100
		WHEN @i >= 300 THEN @i + 10
		ELSE @i + 1 END
END
GO	
	
CREATE PROCEDURE [dbo].[InsertSingleTradeDecayAnalysisRequest_SP]
(
	@TradeDecayAnalysisTypeName [varchar](16),
	@SourceEventRecordId [int],
	@AnalysisStartTimeUtc [datetime2](7),
	@IntegratorSymbolId [tinyint],
	@IntegratorCounterpartyId [tinyint],
	@ReferencePrice [decimal](18,9),
	@MatchingContraPriceSideId [bit]
)
AS
BEGIN
	INSERT INTO [dbo].[TradeDecayAnalysis_Pending]
           ([TradeDecayAnalysisTypeId]
           ,[SourceEventRecordId]
           ,[AnalysisStartTimeUtc]
           ,[FXPairId]
           ,[CounterpartyId]
           ,[ReferencePrice]
           ,[PriceSideId])
     VALUES
           ((SELECT [TradeDecayAnalysisTypeId] FROM [dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = @TradeDecayAnalysisTypeName)
           ,@SourceEventRecordId
           ,@AnalysisStartTimeUtc
           ,(SELECT [DCDBSymbolId] FROM [dbo].[_Internal_SymbolIdsMapping] WHERE [IntegratorDBSymbolId] = @IntegratorSymbolId)
           ,(SELECT [DCDBCounterpartyId] FROM [dbo].[_Internal_CounterpartyIdsMapping] WHERE [IntegratorDBCounterpartyId] = @IntegratorCounterpartyId)
           ,@ReferencePrice
           ,@MatchingContraPriceSideId)
END
GO

CREATE FUNCTION [dbo].[GetTradeDecayTimeIntervalBucket](@msDuration AS INT) RETURNS INT
AS
BEGIN
    RETURN CASE 
		WHEN @msDuration >= 10000 THEN @msDuration - @msDuration%1000
		WHEN @msDuration >= 1000 THEN @msDuration - @msDuration%100
		WHEN @msDuration >= 300 THEN @msDuration - @msDuration%10
		ELSE @msDuration END
END
GO
	
CREATE PROCEDURE [dbo].[PerformSingleTradeDecayAnalysis_SP]
(
	@TradeDecayAnalysisTypeId [tinyint],
	@SourceEventRecordId [int],
	@AnalysisStartTimeUtc [datetime2](7),
	@FXPairId [tinyint],
	@CounterpartyId [tinyint],
	@ReferencePrice [decimal](18,9),
	@PriceSideId [bit]
)
AS
BEGIN
	DECLARE @TradeDecayAnalysisId INT

	INSERT INTO [dbo].[TradeDecayAnalysis]
           ([TradeDecayAnalysisTypeId]
           ,[SourceEventRecordId]
           ,[AnalysisStartTimeUtc]
           ,[FXPairId]
           ,[CounterpartyId]
           ,[PriceSideId]
           ,[HasValidTickAfterIntervalEnd]
           ,[IsValidAndComplete])
     VALUES
           (@TradeDecayAnalysisTypeId,
			@SourceEventRecordId,
			@AnalysisStartTimeUtc,
			@FXPairId,
			@CounterpartyId,
			@PriceSideId,
			0,
			0)
	
	SET @TradeDecayAnalysisId = scope_identity();
	DECLARE @ReferenceCounterpartyId INT = (SELECT [LiqProviderStreamId] FROM [LiqProviderStream] WHERE [LiqProviderStreamName] = 'L01')
	-- round to next whole us and and 1ms for the first interval
	DECLARE @AnalysisFirstIntervalStartTimeUtc [datetime2](7) = DATEADD(ns, -DATEPART(ns, @AnalysisStartTimeUtc)%1000 + 1000 + 1000000, @AnalysisStartTimeUtc)
	DECLARE @AnalysisEndTimeUtc [datetime2](7) = DATEADD(minute, 5, @AnalysisStartTimeUtc)
	
	
	DECLARE @FirstBidTob [decimal](18,9)
	DECLARE @FirstAskTob [decimal](18,9)
	
	SET @FirstBidTob =
	(SELECT TOP 1
		md.Price AS LastToBPrice
	FROM
		[dbo].[MarketData] md with (nolock) 
		INNER JOIN [dbo].[LiqProviderStream] lpB with (nolock) on md.[CounterpartyId] = lpB.LiqProviderStreamId
	WHERE
		IntegratorReceivedTimeUtc BETWEEN DATEADD(second, -30, @AnalysisFirstIntervalStartTimeUtc) AND @AnalysisFirstIntervalStartTimeUtc
		AND fxpairid  = @FxPairId
		AND RecordTypeId IN (0, 3)
		AND CounterpartyId = @ReferenceCounterpartyId
		AND SideId = 0
	ORDER BY IntegratorReceivedTimeUtc DESC)
	
	SET @FirstAskTob =
	(SELECT TOP 1
		md.Price AS LastToBPrice
	FROM
		[dbo].[MarketData] md with (nolock) 
		INNER JOIN [dbo].[LiqProviderStream] lpB with (nolock) on md.[CounterpartyId] = lpB.LiqProviderStreamId
	WHERE
		IntegratorReceivedTimeUtc BETWEEN DATEADD(second, -30, @AnalysisFirstIntervalStartTimeUtc) AND @AnalysisFirstIntervalStartTimeUtc
		AND fxpairid  = @FxPairId
		AND RecordTypeId IN (0, 3)
		AND CounterpartyId = @ReferenceCounterpartyId
		AND SideId = 1
	ORDER BY IntegratorReceivedTimeUtc DESC)
	
	--fast track if there is no valid price before the interval
	IF @FirstBidTob IS NULL AND @FirstAskTob IS NULL
	BEGIN
		RETURN;
	END
	
	IF NOT EXISTS(
		SELECT 1 
		FROM 
			[dbo].[MarketData] md with (nolock) 
			INNER JOIN [dbo].[LiqProviderStream] lpB with (nolock) on md.[CounterpartyId] = lpB.LiqProviderStreamId
		WHERE
			IntegratorReceivedTimeUtc BETWEEN @AnalysisEndTimeUtc AND DATEADD(second, 30, @AnalysisEndTimeUtc)
			AND fxpairid  = @FxPairId
			AND RecordTypeId IN (0, 3)
			AND CounterpartyId = @ReferenceCounterpartyId
			AND SideId = @PriceSideId
		)
	BEGIN
		RETURN;
	END
	
	
	INSERT INTO [dbo].[TradeDecayAnalysisDataPoint]
           ([TradeDecayAnalysisId]
           ,[MillisecondDistanceFromAnalysisStart]
           ,[MidToB]
           ,[BpDifferenceFromReferencePrice]
		   ,[IsAuthenticValue])
	SELECT	
		@TradeDecayAnalysisId,
		MillisecondDistance,
		-- as we do not have ANY()
		AVG(LastSidedToB) AS MidToB,
		CASE WHEN @PriceSideId = 0 THEN (AVG(LastSidedToB) - @ReferencePrice) ELSE (@ReferencePrice - AVG(LastSidedToB)) END / @ReferencePrice * 10000.0,
		-- both must be 1 for this to be 1
		MIN(IsAuthenticValue) AS IsAuthenticValue
	FROM	
		(
		SELECT
			MillisecondDistance,
			SideId,
			--for lack of ANY()
			MAX(LastToBPrice) OVER (PARTITION BY NonNullToBsNum) AS LastSidedToB,
			CASE WHEN LastToBPrice IS NULL THEN 0 ELSE 1 END AS IsAuthenticValue
		FROM
			(
			SELECT
				nums.MsDistanceFromEventStart AS MillisecondDistance,
				nums.SideId AS SideId,
				LastToBPrice, -- as we do not have any - all are same
				-- this will count number of non-null rows - for nulls (no ToB in current millisecond) it will repeat previous val (so we can partition by this one level up)
				COUNT(LastToBPrice) OVER (ORDER BY nums.SideId, nums.MsDistanceFromEventStart ASC) as NonNullToBsNum
			FROM
			[dbo].[_Internal_TradeDecayIntervals] nums
			LEFT JOIN
			(
				SELECT
					0 AS BucketizedMillisecondsFromIntervalStart,
					0 AS SideId,
					@FirstBidTob AS LastToBPrice
				UNION ALL
				SELECT
					0 AS BucketizedMillisecondsFromIntervalStart,
					1 AS SideId,
					@FirstAskTob AS LastToBPrice	
				UNION ALL
				SELECT
					BucketizedMillisecondsFromIntervalStart,
					SideId,
					MAX(LastToBPrice) AS LastToBPrice -- as we do not have ANY() - all are same
				FROM
				(
					SELECT 
						-- millisecond difference from the beginig
						[dbo].[GetTradeDecayTimeIntervalBucket](DateDiff(mcs, @AnalysisStartTimeUtc, md.integratorReceivedTimeUtc)/1000) AS BucketizedMillisecondsFromIntervalStart,
						SideId,
						-- last price in current millisecond interval = ToB in current millisecond
						LAST_VALUE(md.Price) 
							OVER(
								PARTITION BY [dbo].[GetTradeDecayTimeIntervalBucket](DateDiff(mcs, @AnalysisStartTimeUtc, md.integratorReceivedTimeUtc)/1000), SideId
								ORDER BY md.integratorReceivedTimeUtc ASC 
								ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) AS LastToBPrice
					FROM
						[dbo].[MarketData] md with (nolock) 
						INNER JOIN [dbo].[LiqProviderStream] lpB with (nolock) on md.[CounterpartyId] = lpB.LiqProviderStreamId
					WHERE
						IntegratorReceivedTimeUtc BETWEEN @AnalysisFirstIntervalStartTimeUtc AND @AnalysisEndTimeUtc
						AND fxpairid  = @FxPairId
						AND RecordTypeId IN (0, 3)
						AND CounterpartyId = @ReferenceCounterpartyId
						--AND SideId = @PriceSideId
				) tmp
				GROUP BY BucketizedMillisecondsFromIntervalStart, SideId
			) tmp2 ON nums.MsDistanceFromEventStart = tmp2.BucketizedMillisecondsFromIntervalStart AND nums.SideId = tmp2.SideId
		) tmp3
	) tmp4
	GROUP BY MillisecondDistance
	ORDER BY MillisecondDistance ASC

	
	UPDATE [dbo].[TradeDecayAnalysis]
	SET 
      [HasValidTickAfterIntervalEnd] = 1
      ,[IsValidAndComplete] = 1
	WHERE 
		[TradeDecayAnalysisId] = @TradeDecayAnalysisId
	
END
GO

CREATE PROCEDURE [dbo].[ProcessTradeDecayAnalysisQueue_SP]
(
	@FromTimeUtc [datetime2](7) = '1900-01-01',
	@ToTimeUtc [datetime2](7) = '3000-01-01',
	@BatchSize [int] = 5
)
AS
BEGIN

	DECLARE @TempPendingWork TABLE (
		[PendingTradeDecayAnalysisId] [int] NULL,
		[TradeDecayAnalysisTypeId] [tinyint] NOT NULL,
		[SourceEventRecordId] [int] NOT NULL,
		[AnalysisStartTimeUtc] [datetime2](7) NOT NULL,
		[FXPairId] [tinyint] NOT NULL,
		[CounterpartyId] [tinyint] NOT NULL,
		[ReferencePrice] [decimal](18,9) NOT NULL,
		[PriceSideId] [bit] NOT NULL
	)

BEGIN TRY
WHILE(1 = 1)
BEGIN
	print 'Starting to process next batch'
	print GETUTCDATE()

	DECLARE @CurrentUTCTime DATETIME2(7)
	SET @CurrentUTCTime = (SELECT GETUTCDATE())
	
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

	BEGIN TRANSACTION

	INSERT INTO @TempPendingWork (
		[PendingTradeDecayAnalysisId],
		[TradeDecayAnalysisTypeId],
		[SourceEventRecordId],
		[AnalysisStartTimeUtc],
		[FXPairId],
		[CounterpartyId],
		[ReferencePrice],
		[PriceSideId]
		)
	SELECT TOP (@BatchSize) 
		[PendingTradeDecayAnalysisId],
		[TradeDecayAnalysisTypeId],
		[SourceEventRecordId],
		[AnalysisStartTimeUtc],
		[FXPairId] [tinyint],
		[CounterpartyId],
		[ReferencePrice],
		[PriceSideId]
	FROM [dbo].[TradeDecayAnalysis_Pending] WITH (TABLOCKX)
	WHERE 
		AnalysisStartTimeUtc BETWEEN @FromTimeUtc AND @ToTimeUtc
		AND
			(
			AcquiredForCalculationTimeUtc IS NULL
			OR (@CurrentUTCTime > DATEADD(mi, 120, AcquiredForCalculationTimeUtc))
			)

	UPDATE wq
	SET AcquiredForCalculationTimeUtc = @CurrentUTCTime
	FROM @TempPendingWork TEMP
	INNER JOIN [dbo].[TradeDecayAnalysis_Pending] wq ON TEMP.[PendingTradeDecayAnalysisId] = wq.[PendingTradeDecayAnalysisId]

	COMMIT TRANSACTION
	
	IF(NOT EXISTS(SELECT * FROM @TempPendingWork))
		BREAK;
	
	
	DECLARE @PendingTradeDecayAnalysisId INT,
		@TradeDecayAnalysisTypeId [tinyint],
		@SourceEventRecordId [int],
		@AnalysisStartTimeUtc [datetime2](7),
		@FXPairId [tinyint],
		@CounterpartyId [tinyint],
		@ReferencePrice [decimal](18,9),
		@PriceSideId [bit]

	DECLARE itemsCursor CURSOR FOR
		SELECT TOP (@BatchSize)
			[PendingTradeDecayAnalysisId],
			[TradeDecayAnalysisTypeId],
			[SourceEventRecordId],
			[AnalysisStartTimeUtc],
			[FXPairId] [tinyint],
			[CounterpartyId],
			[ReferencePrice],
			[PriceSideId]
		FROM @TempPendingWork 
    
	OPEN itemsCursor
    FETCH NEXT FROM itemsCursor INTO
		@PendingTradeDecayAnalysisId,
		@TradeDecayAnalysisTypeId,
		@SourceEventRecordId,
		@AnalysisStartTimeUtc,
		@FXPairId,
		@CounterpartyId,
		@ReferencePrice,
		@PriceSideId
    WHILE @@FETCH_STATUS = 0
    BEGIN

		DELETE FROM [dbo].[TradeDecayAnalysis_Pending] WHERE PendingTradeDecayAnalysisId = @PendingTradeDecayAnalysisId
		
		--BEGIN TRY
			EXEC [dbo].[PerformSingleTradeDecayAnalysis_SP]
				@TradeDecayAnalysisTypeId,
				@SourceEventRecordId,
				@AnalysisStartTimeUtc,
				@FXPairId,
				@CounterpartyId,
				@ReferencePrice,
				@PriceSideId
				
			print 'Processed next analysis request'
			print GETUTCDATE()	
				
		--END TRY
		--BEGIN CATCH
			--CLOSE itemsCursor
			--DEALLOCATE itemsCursor
			--THROW;
		--END CATCH

		FETCH NEXT FROM itemsCursor INTO
			@PendingTradeDecayAnalysisId,
			@TradeDecayAnalysisTypeId,
			@SourceEventRecordId,
			@AnalysisStartTimeUtc,
			@FXPairId,
			@CounterpartyId,
			@ReferencePrice,
			@PriceSideId
    END
	CLOSE itemsCursor
    DEALLOCATE itemsCursor
	
	DELETE FROM @TempPendingWork
END
END TRY
BEGIN CATCH
	print 'failed to processrequest records';
	THROW;
END CATCH
END
GO

--ROLLBACK TRANSACTION Install_18_02_001;
--GO
--COMMIT TRANSACTION Install_18_02_001;
--GO

select 'ALTER TABLE [dbo].[' + TABLE_NAME + '] ALTER COLUMN [' + COLUMN_NAME + '] ' + DATA_TYPE + '(' + CASE WHEN CHARACTER_MAXIMUM_LENGTH = -1 THEN 'MAX' ELSE CAST(CHARACTER_MAXIMUM_LENGTH AS VARCHAR(10)) END + ') COLLATE SQL_Latin1_General_CP1_CI_AS'
from FXtickDB.INFORMATION_SCHEMA.COLUMNS where CHARACTER_SET_NAME is not null AND COLLATION_NAME not like 'SQL_Latin1_General_CP1_CI_AS';

DROP INDEX [DataProviderName_Index] ON [dbo].[DataProvider]
GO

DROP INDEX [UniqueCurrencyAlphaCode] ON [dbo].[Currency]
GO

DROP INDEX [UniqueCurrencyName] ON [dbo].[Currency]
GO

DROP INDEX [TznCode_Index] ON [dbo].[TimeZone]
GO

ALTER TABLE [dbo].[TimeZone] ALTER COLUMN [TimeZoneName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS
ALTER TABLE [dbo].[DataProvider] ALTER COLUMN [DataProviderName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS
ALTER TABLE [dbo].[Currency] ALTER COLUMN [CurrencyAlphaCode] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS
ALTER TABLE [dbo].[Currency] ALTER COLUMN [CurrencyName] nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS
ALTER TABLE [dbo].[SourceFile] ALTER COLUMN [FileName] nvarchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
ALTER TABLE [dbo].[SourceFile] ALTER COLUMN [FileImportTag] nvarchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
ALTER TABLE [dbo].[SourceFile] ALTER COLUMN [Comment] nvarchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
ALTER TABLE [dbo].[TimeZone] ALTER COLUMN [TznCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS
GO

CREATE UNIQUE NONCLUSTERED INDEX [DataProviderName_Index] ON [dbo].[DataProvider]
(
	[DataProviderName] ASC
)ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyAlphaCode] ON [dbo].[Currency]
(
	[CurrencyAlphaCode] ASC
)ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyName] ON [dbo].[Currency]
(
	[CurrencyName] ASC
)ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [TznCode_Index] ON [dbo].[TimeZone]
(
	[TznCode] ASC
)ON [PRIMARY]
GO


