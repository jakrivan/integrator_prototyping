

-- SPs using the materialized views on SSD
--  What is added compared to previous SPs - selection from alias (instead of table) with the WITH(NOEXPAND) hint

ALTER PROCEDURE [dbo].[GetStatisticsPerCounterparty_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		RejectionsNum int,
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int
	)

	INSERT INTO @temp_rejections(CounterpartyId, RejectionsNum, IntegratorLatencyNanoseconds, IntegratorLatencyCases)
	SELECT
		[CounterpartyId],
		COUNT([CounterpartyId]) AS NumberOfRejections,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
	FROM
		DealExternalRejected_DailyView rej WITH(NOEXPAND)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		DealsNum int,
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int,
		CounterpartyLatencyNanoseconds bigint,
		CounterpartyLatencyCases int
	)

	INSERT INTO 
		@temp_NOPs_pol
		(
		SymbolId, 
		CounterpartyId, 
		NopBasePol, 
		NopTermPol, 
		VolumeBaseAbs, 
		DealsNum, 
		IntegratorLatencyNanoseconds, 
		IntegratorLatencyCases, 
		CounterpartyLatencyNanoseconds, 
		CounterpartyLatencyCases
		)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases,
		SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
		COUNT(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyCases
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
		--INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		deal.SymbolId,
		deal.CounterpartyId
		

	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		RejectionsStatistics.RejectionsNum AS RejectionsNum,
		RejectionsStatistics.RejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.RejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0)) AS RejectionsRate,
		--this calculates average of both numbers if both are non-null; or average of the nonull case if just one is not null (as 2*A / 2*B = A/B); or NULL if both are NULL (avoids divide by zero exception)
		(COALESCE(RejectionsStatistics.IntegratorLatencyNanoseconds, DealsStatistics.IntegratorLatencyNanoseconds) + COALESCE(DealsStatistics.IntegratorLatencyNanoseconds, RejectionsStatistics.IntegratorLatencyNanoseconds))
		/
		(COALESCE(RejectionsStatistics.IntegratorLatencyCases, DealsStatistics.IntegratorLatencyCases) + COALESCE(DealsStatistics.IntegratorLatencyCases, RejectionsStatistics.IntegratorLatencyCases))
		AS AvgIntegratorLatencyNanoseconds,
		DealsStatistics.CounterpartyLatencyNanoseconds / DealsStatistics.CounterpartyLatencyCases AS AvgCounterpartyLatencyNanoseconds
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			-- from SHORTs
			SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs,
			--SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(DealsNum) AS DealsNum,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
			SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
			SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(DealsNum) AS DealsNum,
				SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
				SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
				SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
				SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.DealsNum AS DealsNum,
					basePos.IntegratorLatencyNanoseconds AS IntegratorLatencyNanoseconds,
					basePos.IntegratorLatencyCases AS IntegratorLatencyCases,
					basePos.CounterpartyLatencyNanoseconds AS CounterpartyLatencyNanoseconds,
					basePos.CounterpartyLatencyCases AS CounterpartyLatencyCases
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd,
					NULL AS IntegratorLatencyNanoseconds,
					NULL AS IntegratorLatencyCases,
					NULL AS CounterpartyLatencyNanoseconds,
					NULL AS CounterpartyLatencyCases
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode
END
GO


ALTER PROCEDURE [dbo].[GetNopAbsTotal_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.Price) AS NopTermPol
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		deal.SymbolId 


	SELECT
		-- From SHORTs
		SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
	FROM
		(
		SELECT
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
		GROUP BY
			CCYPositions.CCY
		) AS CtpCCYPositions

END
GO


ALTER PROCEDURE [dbo].[GetNopAbsTotal_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		symbol.[Id] AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.Price) AS NopTermPol
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Id]


	SELECT
		-- From SHORTs
		SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
	FROM
		(
		SELECT
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
		GROUP BY
			CCYPositions.CCY
		) AS CtpCCYPositions

END
GO



DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="DiagnosticsDashboardSettings" nillable="true" type="DiagnosticsDashboardSettings" />
  <xs:complexType name="DiagnosticsDashboardSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RefreshTimeout_Miliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="FirstSpreadAgeLimit_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="SecondSpreadAgeLimit_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ThirdSpreadAgeLimit_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="CutOffSpreadAgeLimit_Hours" type="xs:int" />
	  <xs:element minOccurs="1" maxOccurs="1" name="SessionActionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="EnabledSymbols" type="ArrayOfString" />
      <xs:element minOccurs="1" maxOccurs="1" name="EnabledCounterparties" type="ArrayOfString1" />
      <xs:element minOccurs="1" maxOccurs="1" name="EnabledCounterpartyInfoRowTypes" type="ArrayOfCounterpartyInfoRowType" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString1">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartyInfoRowType">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartyInfoRowType" type="CounterpartyInfoRowType" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="CounterpartyInfoRowType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Active" />
      <xs:enumeration value="MarketData" />
      <xs:enumeration value="OrderFlow" />
      <xs:enumeration value="SessionsAndDealsDividerRow" />
      <xs:enumeration value="Deals" />
      <xs:enumeration value="FirstDailyInfoRow" />
      <xs:enumeration value="Rejections" />
      <xs:enumeration value="RejectionRate" />
      <xs:enumeration value="AvgDeal" />
      <xs:enumeration value="Volume" />
      <xs:enumeration value="VolumeShare" />
      <xs:enumeration value="KgtAvgLat" />
      <xs:enumeration value="CounterpartyAvgLat" />
      <xs:enumeration value="LastDailyInfoRow" />
      <xs:enumeration value="SpreadRank" />
      <xs:enumeration value="DealsAndSymbolsDividerRow" />
      <xs:enumeration value="MaximumItems" />
    </xs:restriction>
  </xs:simpleType>
</xs:schema>'
GO

--Manualy update DiagDashboard settings, Add:

--<SessionActionTimeout_Seconds>5</SessionActionTimeout_Seconds>

--<EnabledCounterpartyInfoRowTypes>
--	<CounterpartyInfoRowType>Active</CounterpartyInfoRowType>
--	<CounterpartyInfoRowType>MarketData</CounterpartyInfoRowType>
--	<CounterpartyInfoRowType>OrderFlow</CounterpartyInfoRowType>
--	<CounterpartyInfoRowType>SessionsAndDealsDividerRow</CounterpartyInfoRowType>
--	<CounterpartyInfoRowType>Deals</CounterpartyInfoRowType>
--	<CounterpartyInfoRowType>Rejections</CounterpartyInfoRowType>
--	<CounterpartyInfoRowType>RejectionRate</CounterpartyInfoRowType>
--	<CounterpartyInfoRowType>AvgDeal</CounterpartyInfoRowType>
--	<CounterpartyInfoRowType>Volume</CounterpartyInfoRowType>
--	<CounterpartyInfoRowType>VolumeShare</CounterpartyInfoRowType>
--	<CounterpartyInfoRowType>KgtAvgLat</CounterpartyInfoRowType>
--	<CounterpartyInfoRowType>CounterpartyAvgLat</CounterpartyInfoRowType>
--	<CounterpartyInfoRowType>SpreadRank</CounterpartyInfoRowType>
--	<CounterpartyInfoRowType>DealsAndSymbolsDividerRow</CounterpartyInfoRowType>
--</EnabledCounterpartyInfoRowTypes>


DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.Common.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.Common.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="CommonSettings" nillable="true" type="CommonSettings" />
  <xs:complexType name="CommonSettings">
    <xs:sequence>
	  <xs:element minOccurs="1" maxOccurs="1" name="AttemptToUseMeinbergClocks" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="Logging" type="LoggingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="Automailer" type="AutomailerSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="LoggingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="AvailableDiskSpaceThresholdToStopMDLogging_GB" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailMinDelaysLevelsBetweenEmails" type="ArrayOfInt" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfInt">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="unbounded" name="Delay_Seconds" nillable="true" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="AutomailerSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SmtpServer" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SmtpPort" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="BypassCertificationValidation" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="SmtpUsername" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SmtpPassword" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DirectoryForLocalEmails" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SendEmailsOnlyToLocalFolder" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

--Update settings manually
--<AttemptToUseMeinbergClocks>true</AttemptToUseMeinbergClocks>

--<FatalErrorsEmailMinDelaysLevelsBetweenEmails>
--  <Delay_Seconds>2</Delay_Seconds>
--  <Delay_Seconds>30</Delay_Seconds>
--  <Delay_Seconds>60</Delay_Seconds>
--  <Delay_Seconds>300</Delay_Seconds>
--  <Delay_Seconds>600</Delay_Seconds>
--</FatalErrorsEmailMinDelaysLevelsBetweenEmails>


UPDATE [dbo].[ExecutionReportStatus]
   SET [Name] = 'Cancelled'  WHERE [Name] = 'Canceled'
GO

ALTER PROCEDURE [dbo].[InsertOrderExternalCancelResult_SP]( 
	@IntegratorExternalOrderPrivateId [nvarchar](45),
    @CounterpartySentResultUtc  DATETIME2(7),
    @CancelledAmountBasePol decimal(18,0),
    @ResultStatusName VARCHAR(16)
	)
AS
BEGIN

	DECLARE @ResultStatusId TINYINT

	SELECT @ResultStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ResultStatusName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ResultStatusName not found in DB: %s', 16, 2, @ResultStatusName) WITH SETERROR
	END


	INSERT INTO [dbo].[OrderExternalCancelResult]
           ([IntegratorExternalOrderPrivateId]
           ,[CounterpartySentResultUtc]
           ,[CanceledAmountBasePol]
           ,[ResultStatusId])
     VALUES
           (@IntegratorExternalOrderPrivateId
           ,@CounterpartySentResultUtc
           ,@CancelledAmountBasePol
           ,@ResultStatusId)
END
GO