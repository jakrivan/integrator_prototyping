USE master 
SELECT name, physical_name 
FROM sys.master_files 
WHERE database_id = DB_ID('FXtickDB'); 


USE master
GO
ALTER DATABASE FXtickDB
SET OFFLINE WITH ROLLBACK IMMEDIATE
GO


ALTER DATABASE FXtickDB 
MODIFY FILE ( NAME = FXtickDB, FILENAME = 
"T:\SQLDATA\USERDB\FXtickDB\FXtickDB.mdf") 
GO 


ALTER DATABASE FXtickDB 
SET online 
GO 