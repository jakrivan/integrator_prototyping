


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--                                            START SCHEMA CREATION
--
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="BusinessLayerSettings" nillable="true" type="BusinessLayerSettings" />
  <xs:complexType name="BusinessLayerSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MarketableClientOrdersMatchingStrategy" type="MarketableClientOrdersMatchingStrategy" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="ConsecutiveRejectionsStopStrategyBehavior" type="ConsecutiveRejectionsStopStrategySettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartWithSessionsStopped" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="AutoKillInternalOrdersOnOrdersTransmissionDisabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailSubject" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailBody" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderCounterpartyContacts" type="ArrayOfIgnoredOrderCounterpartyContact" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailMinDelayBetweenEmails_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="MarketableClientOrdersMatchingStrategy">
    <xs:restriction base="xs:string">
      <xs:enumeration value="ImmediateMatching" />
      <xs:enumeration value="NextFreshPriceMatching" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ConsecutiveRejectionsStopStrategySettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="StrategyEnabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="NumerOfRejectionsToTriggerStrategy" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAgeRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceBpRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultCounterpartyRank" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRanks" type="ArrayOfCounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartyRank">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartyRank" nillable="true" type="CounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartyRank">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="Rank" type="xs:decimal" />
  </xs:complexType>
  <xs:complexType name="ArrayOfIgnoredOrderCounterpartyContact">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="IgnoredOrderCounterpartyContact" nillable="true" type="IgnoredOrderCounterpartyContact" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="IgnoredOrderCounterpartyContact">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="EmailContacts" type="xs:string" />
  </xs:complexType>
</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.Common.dll.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="CommonSettings" nillable="true" type="CommonSettings" />
  <xs:complexType name="CommonSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Logging" type="LoggingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="Automailer" type="AutomailerSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="LoggingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="AvailableDiskSpaceThresholdToStopMDLogging_GB" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="AutomailerSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SmtpServer" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SmtpPort" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="BypassCertificationValidation" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="SmtpUsername" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SmtpPassword" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DirectoryForLocalEmails" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SendEmailsOnlyToLocalFolder" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DAL.dll.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="DALSettings" nillable="true" type="DALSettings" />
  <xs:complexType name="DALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ConnectionString" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="KillSwitchPollIntervalSeconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="KillSwitchWatchDogIntervalSeconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="IntegratorSwitchName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="CommandDistributorPollIntervalSeconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="DataCollection" type="DataCollectionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="DataCollectionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="DataCollectionBackendConnectionString" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DataProviderId" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="SourceFileId" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="EnableToBDataCollection" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="EnableMarketDataCollection" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxBcpBatchSize" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxInMemoryColectionSize" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxIntervalBetweenUploads_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxUploadTimeAfterWhichSerializationStarts_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxProcessingTimeAfterShutdownSignalled_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="WaitIntervalBetweenBcpRetries_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="WaitIntervalBeforeShutdownStartsSerialization_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>

</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="DiagnosticsDashboardSettings" nillable="true" type="DiagnosticsDashboardSettings" />
  <xs:complexType name="DiagnosticsDashboardSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RefreshTimeout_Miliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="FirstSpreadAgeLimit_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="SecondSpreadAgeLimit_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ThirdSpreadAgeLimit_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="CutOffSpreadAgeLimit_Hours" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="EnabledSymbols" type="ArrayOfString" />
      <xs:element minOccurs="1" maxOccurs="1" name="EnabledCounterparties" type="ArrayOfString1" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString1">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.IntegratorClientHost.exe.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="IntegratorClientHostSettings" nillable="true" type="IntegratorClientHostSettings" />
  <xs:complexType name="IntegratorClientHostSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="DefaultIntegratorClientCofigurationIdentity" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IntegratorClientConfigurations" type="ArrayOfIntegratorClientConfiguration" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="ClientInterfaceType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Console" />
      <xs:enumeration value="Form" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfIntegratorClientConfiguration">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="IntegratorClientConfiguration" nillable="true" type="IntegratorClientConfiguration" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="IntegratorClientConfiguration">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ConfigurationIdentity" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="ClientInterfaceType" type="ClientInterfaceType" />
      <xs:element minOccurs="0" maxOccurs="1" name="ClientFriendlyName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="ClientTypeName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="AssemblyNameWithoutExtension" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="AssemblyFullPath" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="DefaultConfigurationParameter" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.IntegratorWatchDogSvc.exe.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="WatchdogSettings" nillable="true" type="WatchdogSettings" />
  <xs:complexType name="WatchdogSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IntegratorServiceDiagnosticsEndpoints" type="ArrayOfIntegratorServiceDiagnosticsEndpoint" />
      <xs:element minOccurs="1" maxOccurs="1" name="IntegratorNotRunnigSendFirstEmailAfter_Minutes" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="IntegratorNotRunnigResendEmailAfter_Hours" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="EmailRecipients" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfIntegratorServiceDiagnosticsEndpoint">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="unbounded" name="IntegratorServiceDiagnosticsEndpoint" nillable="true" type="IntegratorServiceDiagnosticsEndpoint" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="IntegratorServiceDiagnosticsEndpoint">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Endpoint" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Port" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBus.dll.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="MessageBusSettings" nillable="true" type="MessageBusSettings" />
  <xs:simpleType name="PriceStreamTransferMode">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Reliable" />
      <xs:enumeration value="FastAndUnreliable" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="MessageBusSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IntegratorServiceHost" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IntegratorServicePort" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="IntegratorServicePipeName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedDisconnectedInterval_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="DisconnectedSessionReconnectInterval_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="SessionHealthCheckInterval_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceStreamTransferMode" type="PriceStreamTransferMode" />
      <xs:element minOccurs="0" maxOccurs="1" name="PerSymbolPriceThrottlingInterval_Milliseconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="ForceConnectRemotingClientOnContractVersionMismatch" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.QuickItchN.dll.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="QuickItchNSettings" nillable="true" type="QuickItchNSettings" />
  <xs:complexType name="QuickItchNSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="HtaSessionSettings" type="SessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="HtfSessionSettings" type="SessionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Host" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Port" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="HeartBeatInterval_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO


CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RiskManagementSettings" nillable="true" type="RiskManagementSettings" />
  <xs:complexType name="RiskManagementSettings">
    <xs:sequence>
      <xs:element name="ConfigLastUpdated"  nillable="true" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAndPosition" type="PriceAndPositionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersSubmissionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrderMaximumAllowedSizeSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedBaseAbsSizeInUsd" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="PriceAndPositionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumExternalNOPInUSD" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceDeviationCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="OnlinePriceDeviationCheckMaxPriceAge_Minutes" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="Symbols" type="ArrayOfSymbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbol">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="Symbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="Symbol">
    <xs:attribute name="ShortName" type="xs:string" />
    <xs:attribute name="NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
  </xs:complexType>
</xs:schema>'
GO


CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--                                            END SCHEMA CREATION
--
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------


--CREATE TABLE [dbo].[IntegratorRole](
--	[RoleId] int NOT NULL,
--	[RoleName] VARCHAR(32) NOT NULL,
--	[RoleDescription] NVARCHAR(512) NULL
--) ON [PRIMARY]
--GO

--INSERT INTO [dbo].[IntegratorRole] (RoleId, RoleName, RoleDescription) VALUES(1, 'UAT_Primary')

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings_New](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[FriendlyName] [nvarchar](200) NULL,
	[SettingsVersion] INT NOT NULL DEFAULT(1),
	[SettingsXml] [xml] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[IsOnlineMonitored] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

--ALTER TABLE [dbo].[Settings]  WITH CHECK ADD  CONSTRAINT [FK_Settings_IntegratorRole] FOREIGN KEY([IntegratorRoleId])
--REFERENCES [dbo].[IntegratorRole] ([RoleId])
--GO
--ALTER TABLE [dbo].[Settings] CHECK CONSTRAINT [FK_Settings_IntegratorRole]
--GO

CREATE TRIGGER SchemaXsdCheckTrigger ON [dbo].[Settings_New] AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @Type NVARCHAR(100)
	DECLARE @SettingsXml [xml]
	
	DECLARE settingsCursor CURSOR FOR
		SELECT Name, SettingsXml FROM inserted
    
	OPEN settingsCursor
    FETCH NEXT FROM settingsCursor INTO @Type, @SettingsXml
    WHILE @@FETCH_STATUS = 0
    BEGIN

		IF(@Type = N'Kreslik.Integrator.BusinessLayer.dll')
		BEGIN
			DECLARE @x1 XML([dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd]) 
			SET @x1 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.Common.dll')
		BEGIN
			DECLARE @x2 XML([dbo].[Kreslik.Integrator.Common.dll.xsd]) 
			SET @x2 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL.dll')
		BEGIN
			DECLARE @x3 XML([dbo].[Kreslik.Integrator.DAL.dll.xsd]) 
			SET @x3 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DiagnosticsDashboard.exe')
		BEGIN
			DECLARE @x4 XML([dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd]) 
			SET @x4 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.IntegratorClientHost.exe')
		BEGIN
			DECLARE @x5 XML([dbo].[Kreslik.Integrator.IntegratorClientHost.exe.xsd]) 
			SET @x5 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.IntegratorWatchDogSvc.exe')
		BEGIN
			DECLARE @x6 XML([dbo].[Kreslik.Integrator.IntegratorWatchDogSvc.exe.xsd]) 
			SET @x6 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBus.dll')
		BEGIN
			DECLARE @x7 XML([dbo].[Kreslik.Integrator.MessageBus.dll.xsd]) 
			SET @x7 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.QuickItchN.dll')
		BEGIN
			DECLARE @x8 XML([dbo].[Kreslik.Integrator.QuickItchN.dll.xsd]) 
			SET @x8 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.RiskManagement.dll')
		BEGIN
			DECLARE @x9 XML([dbo].[Kreslik.Integrator.RiskManagement.dll.xsd]) 
			SET @x9 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.SessionManagement.dll')
		BEGIN
			DECLARE @x10 XML([dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]) 
			SET @x10 = @SettingsXml
		END

		ELSE
		BEGIN
			RAISERROR (N'Setting with name: %s is unknown to DB', 16, 2, @Type ) WITH SETERROR
			ROLLBACK TRAN
		END

		FETCH NEXT FROM settingsCursor INTO @Type, @SettingsXml
    END
	CLOSE settingsCursor
    DEALLOCATE settingsCursor
END
GO


INSERT INTO [dbo].[Settings_New](Name, SettingsXml, LastUpdated, IsOnlineMonitored)
SELECT Name, SettingsXml, LastUpdated, 1 FROM [dbo].[Settings]



INSERT INTO [dbo].[Settings_New]
           ([Name]
		   ,[FriendlyName]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.Common.dll'
			,N'Integrator common (sends fatal emails)'
			,N'<CommonSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Logging>
    <AvailableDiskSpaceThresholdToStopMDLogging_GB>15</AvailableDiskSpaceThresholdToStopMDLogging_GB>
  </Logging>
  <Automailer>
    <SmtpServer>173.194.70.108</SmtpServer>
    <SmtpPort>587</SmtpPort>
    <BypassCertificationValidation>true</BypassCertificationValidation>
    <SmtpUsername>automailer@kgtinv.com</SmtpUsername>
    <SmtpPassword>Np9G3bBd</SmtpPassword>
    <DirectoryForLocalEmails>LocalEmails</DirectoryForLocalEmails>
    <SendEmailsOnlyToLocalFolder>false</SendEmailsOnlyToLocalFolder>
  </Automailer>
</CommonSettings>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings_New]
           ([Name]
		   ,[FriendlyName]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.Common.dll'
			,N'Clients common (does not send fatal emails)'
			,N'<CommonSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Logging>
    <AvailableDiskSpaceThresholdToStopMDLogging_GB>15</AvailableDiskSpaceThresholdToStopMDLogging_GB>
  </Logging>
  <Automailer>
    <SmtpServer>173.194.70.108</SmtpServer>
    <SmtpPort>587</SmtpPort>
    <BypassCertificationValidation>true</BypassCertificationValidation>
    <SmtpUsername>automailer@kgtinv.com</SmtpUsername>
    <SmtpPassword>Np9G3bBd</SmtpPassword>
    <DirectoryForLocalEmails>LocalEmails</DirectoryForLocalEmails>
    <SendEmailsOnlyToLocalFolder>true</SendEmailsOnlyToLocalFolder>
  </Automailer>
</CommonSettings>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings_New]
           ([Name]
		   ,[FriendlyName]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.DAL.dll'
			,N'DAL trading instance (DC for ToB and also MD)'
			,N'<DALSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ConnectionString>Data Source=192.168.75.66;Initial Catalog=Integrator;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString>
  <KillSwitchPollIntervalSeconds>3</KillSwitchPollIntervalSeconds>
  <KillSwitchWatchDogIntervalSeconds>40</KillSwitchWatchDogIntervalSeconds>
  <IntegratorSwitchName>KGTIntegratorKillSwitch</IntegratorSwitchName>
  <CommandDistributorPollIntervalSeconds>3</CommandDistributorPollIntervalSeconds>
  <DataCollection>
    <DataCollectionBackendConnectionString>Data Source=192.168.75.66;Initial Catalog=FXtickDB;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</DataCollectionBackendConnectionString>
    <DataProviderId>22</DataProviderId>
    <SourceFileId>932696</SourceFileId>
    <EnableToBDataCollection>true</EnableToBDataCollection>
    <EnableMarketDataCollection>true</EnableMarketDataCollection>
    <MaxBcpBatchSize>10000</MaxBcpBatchSize>
    <MaxInMemoryColectionSize>1000000</MaxInMemoryColectionSize>
    <MaxIntervalBetweenUploads_Seconds>10</MaxIntervalBetweenUploads_Seconds>
    <MaxUploadTimeAfterWhichSerializationStarts_Seconds>5</MaxUploadTimeAfterWhichSerializationStarts_Seconds>
    <MaxProcessingTimeAfterShutdownSignalled_Seconds>30</MaxProcessingTimeAfterShutdownSignalled_Seconds>
    <WaitIntervalBetweenBcpRetries_Seconds>2</WaitIntervalBetweenBcpRetries_Seconds>
    <WaitIntervalBeforeShutdownStartsSerialization_Seconds>4</WaitIntervalBeforeShutdownStartsSerialization_Seconds>
  </DataCollection>
</DALSettings>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings_New]
           ([Name]
		   ,[FriendlyName]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.DAL.dll'
			,N'DAL NON-trading instance (DC for ToB only)'
			,N'<DALSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ConnectionString>Data Source=192.168.75.66;Initial Catalog=Integrator;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString>
  <KillSwitchPollIntervalSeconds>3</KillSwitchPollIntervalSeconds>
  <KillSwitchWatchDogIntervalSeconds>40</KillSwitchWatchDogIntervalSeconds>
  <IntegratorSwitchName>KGTIntegratorKillSwitch</IntegratorSwitchName>
  <CommandDistributorPollIntervalSeconds>3</CommandDistributorPollIntervalSeconds>
  <DataCollection>
    <DataCollectionBackendConnectionString>Data Source=192.168.75.66;Initial Catalog=FXtickDB;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</DataCollectionBackendConnectionString>
    <DataProviderId>22</DataProviderId>
    <SourceFileId>932696</SourceFileId>
    <EnableToBDataCollection>true</EnableToBDataCollection>
    <EnableMarketDataCollection>false</EnableMarketDataCollection>
    <MaxBcpBatchSize>10000</MaxBcpBatchSize>
    <MaxInMemoryColectionSize>1000000</MaxInMemoryColectionSize>
    <MaxIntervalBetweenUploads_Seconds>10</MaxIntervalBetweenUploads_Seconds>
    <MaxUploadTimeAfterWhichSerializationStarts_Seconds>5</MaxUploadTimeAfterWhichSerializationStarts_Seconds>
    <MaxProcessingTimeAfterShutdownSignalled_Seconds>30</MaxProcessingTimeAfterShutdownSignalled_Seconds>
    <WaitIntervalBetweenBcpRetries_Seconds>2</WaitIntervalBetweenBcpRetries_Seconds>
    <WaitIntervalBeforeShutdownStartsSerialization_Seconds>4</WaitIntervalBeforeShutdownStartsSerialization_Seconds>
  </DataCollection>
</DALSettings>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings_New]
           ([Name]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.DiagnosticsDashboard.exe'
			,N'<DiagnosticsDashboardSettings xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <RefreshTimeout_Miliseconds>500</RefreshTimeout_Miliseconds>
  <FirstSpreadAgeLimit_Seconds>10</FirstSpreadAgeLimit_Seconds>
  <SecondSpreadAgeLimit_Seconds>60</SecondSpreadAgeLimit_Seconds>
  <ThirdSpreadAgeLimit_Seconds>1200</ThirdSpreadAgeLimit_Seconds>
  <CutOffSpreadAgeLimit_Hours>24</CutOffSpreadAgeLimit_Hours>
  <EnabledSymbols>
    <Symbol>AUD/CAD</Symbol>
    <Symbol>AUD/CHF</Symbol>
    <Symbol>AUD/DKK</Symbol>
    <Symbol>AUD/HKD</Symbol>
    <Symbol>AUD/JPY</Symbol>
    <Symbol>AUD/NOK</Symbol>
    <Symbol>AUD/NZD</Symbol>
    <Symbol>AUD/SEK</Symbol>
    <Symbol>AUD/SGD</Symbol>
    <Symbol>AUD/USD</Symbol>
    <Symbol>CAD/CHF</Symbol>
    <Symbol>CAD/JPY</Symbol>
    <Symbol>CAD/NOK</Symbol>
    <Symbol>CAD/NZD</Symbol>
    <Symbol>CAD/SEK</Symbol>
    <Symbol>CHF/DKK</Symbol>
    <Symbol>CHF/JPY</Symbol>
    <Symbol>CHF/NOK</Symbol>
    <Symbol>CHF/SEK</Symbol>
    <Symbol>DKK/JPY</Symbol>
    <Symbol>DKK/NOK</Symbol>
    <Symbol>DKK/SEK</Symbol>
    <Symbol>EUR/AUD</Symbol>
    <Symbol>EUR/CAD</Symbol>
    <Symbol>EUR/CHF</Symbol>
    <Symbol>EUR/CNH</Symbol>
    <Symbol>EUR/CZK</Symbol>
    <Symbol>EUR/DKK</Symbol>
    <Symbol>EUR/GBP</Symbol>
    <Symbol>EUR/HKD</Symbol>
    <Symbol>EUR/HUF</Symbol>
    <Symbol>EUR/JPY</Symbol>
    <Symbol>EUR/MXN</Symbol>
    <Symbol>EUR/NOK</Symbol>
    <Symbol>EUR/NZD</Symbol>
    <Symbol>EUR/PLN</Symbol>
    <Symbol>EUR/RUB</Symbol>
    <Symbol>EUR/SEK</Symbol>
    <Symbol>EUR/SKK</Symbol>
    <Symbol>EUR/TRY</Symbol>
    <Symbol>EUR/USD</Symbol>
    <Symbol>EUR/ZAR</Symbol>
    <Symbol>GBP/AUD</Symbol>
    <Symbol>GBP/CAD</Symbol>
    <Symbol>GBP/CHF</Symbol>
    <Symbol>GBP/CZK</Symbol>
    <Symbol>GBP/DKK</Symbol>
    <Symbol>GBP/HUF</Symbol>
    <Symbol>GBP/JPY</Symbol>
    <Symbol>GBP/NOK</Symbol>
    <Symbol>GBP/NZD</Symbol>
    <Symbol>GBP/PLN</Symbol>
    <Symbol>GBP/SEK</Symbol>
    <Symbol>GBP/USD</Symbol>
    <Symbol>HKD/JPY</Symbol>
    <Symbol>MXN/JPY</Symbol>
    <Symbol>NOK/JPY</Symbol>
    <Symbol>NOK/SEK</Symbol>
    <Symbol>NZD/CAD</Symbol>
    <Symbol>NZD/CHF</Symbol>
    <Symbol>NZD/DKK</Symbol>
    <Symbol>NZD/JPY</Symbol>
    <Symbol>NZD/NOK</Symbol>
    <Symbol>NZD/SEK</Symbol>
    <Symbol>NZD/SGD</Symbol>
    <Symbol>NZD/USD</Symbol>
    <Symbol>SGD/JPY</Symbol>
    <Symbol>USD/CAD</Symbol>
    <Symbol>USD/CHF</Symbol>
    <Symbol>USD/CNH</Symbol>
    <Symbol>USD/CZK</Symbol>
    <Symbol>USD/DKK</Symbol>
    <Symbol>USD/HKD</Symbol>
    <Symbol>USD/HUF</Symbol>
    <Symbol>USD/ILS</Symbol>
    <Symbol>USD/JPY</Symbol>
    <Symbol>USD/MXN</Symbol>
    <Symbol>USD/NOK</Symbol>
    <Symbol>USD/PLN</Symbol>
    <Symbol>USD/RUB</Symbol>
    <Symbol>USD/SEK</Symbol>
    <Symbol>USD/SGD</Symbol>
    <Symbol>USD/SKK</Symbol>
    <Symbol>USD/TRY</Symbol>
    <Symbol>USD/ZAR</Symbol>
    <Symbol>ZAR/JPY</Symbol>
  </EnabledSymbols>
  <EnabledCounterparties>
    <Counterparty>CRS</Counterparty>
    <Counterparty>UBS</Counterparty>
    <Counterparty>DBK</Counterparty>
    <Counterparty>CTI</Counterparty>
    <Counterparty>BOA</Counterparty>
    <Counterparty>MGS</Counterparty>
    <Counterparty>RBS</Counterparty>
    <Counterparty>HSB</Counterparty>
    <Counterparty>JPM</Counterparty>
    <Counterparty>GLS</Counterparty>
    <Counterparty>BNP</Counterparty>
    <Counterparty>NOM</Counterparty>
    <Counterparty>CZB</Counterparty>
    <Counterparty>BRX</Counterparty>
    <Counterparty>SOC</Counterparty>
    <Counterparty>HTA</Counterparty>
    <Counterparty>HTF</Counterparty>
  </EnabledCounterparties>
</DiagnosticsDashboardSettings>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings_New]
           ([Name]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.IntegratorClientHost.exe'
			,N'<IntegratorClientHostSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <DefaultIntegratorClientCofigurationIdentity>ClientConfig001</DefaultIntegratorClientCofigurationIdentity>
  <IntegratorClientConfigurations>
    <IntegratorClientConfiguration>
      <ConfigurationIdentity>ClientConfig001</ConfigurationIdentity>
      <ClientInterfaceType>Console</ClientInterfaceType>
      <!-- Defaults to MachineName + PID -->
      <ClientFriendlyName>DummyTestingClientA</ClientFriendlyName>
      <!-- NullGUIClient -->
      <ClientTypeName>Kreslik.Integrator.BusinesLayerDriver.DummyIntegratorClient2</ClientTypeName>
      <!-- This is optional. It is ignored if AssemblyFullPath is used, otherwise only the currently executing assembly is probed -->
      <AssemblyNameWithoutExtension>Kreslik.Integrator.BusinesLayerDriver</AssemblyNameWithoutExtension>
      <!-- This is optional, Standard assembly probing will be used if not specified -->
      <!-- <AssemblyFullPath>E:\foo\bar.dll</AssemblyFullPath> -->
      <!-- This is optional, Will be used only if commandline parameter will not be specified -->
      <DefaultConfigurationParameter>FooBar</DefaultConfigurationParameter>
    </IntegratorClientConfiguration>
  </IntegratorClientConfigurations>
  
</IntegratorClientHostSettings>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings_New]
           ([Name]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.IntegratorWatchDogSvc.exe'
			,N'<WatchdogSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <IntegratorServiceDiagnosticsEndpoints>
    <IntegratorServiceDiagnosticsEndpoint>
      <Endpoint>192.168.75.67</Endpoint>
      <Port>9000</Port>
    </IntegratorServiceDiagnosticsEndpoint>
    <IntegratorServiceDiagnosticsEndpoint>
      <Endpoint>192.168.75.68</Endpoint>
      <Port>9000</Port>
    </IntegratorServiceDiagnosticsEndpoint>
    <IntegratorServiceDiagnosticsEndpoint>
      <Endpoint>192.168.75.69</Endpoint>
      <Port>9000</Port>
    </IntegratorServiceDiagnosticsEndpoint>
  </IntegratorServiceDiagnosticsEndpoints>
  <IntegratorNotRunnigSendFirstEmailAfter_Minutes>1</IntegratorNotRunnigSendFirstEmailAfter_Minutes>
  <IntegratorNotRunnigResendEmailAfter_Hours>3</IntegratorNotRunnigResendEmailAfter_Hours>
  <EmailRecipients>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</EmailRecipients>
</WatchdogSettings>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings_New]
           ([Name]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.MessageBus.dll'
			,N'<MessageBusSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <IntegratorServiceHost>localhost</IntegratorServiceHost>
  <IntegratorServicePort>9000</IntegratorServicePort>
  <IntegratorServicePipeName>IntegratorService</IntegratorServicePipeName>
  <MaximumAllowedDisconnectedInterval_Seconds>120</MaximumAllowedDisconnectedInterval_Seconds>
  <DisconnectedSessionReconnectInterval_Seconds>5</DisconnectedSessionReconnectInterval_Seconds>
  <SessionHealthCheckInterval_Seconds>10</SessionHealthCheckInterval_Seconds>
  <!--<PriceStreamTransferMode>FastAndUnreliable</PriceStreamTransferMode>-->
  <PriceStreamTransferMode>Reliable</PriceStreamTransferMode>
  <PerSymbolPriceThrottlingInterval_Milliseconds>300</PerSymbolPriceThrottlingInterval_Milliseconds>
  <ForceConnectRemotingClientOnContractVersionMismatch>false</ForceConnectRemotingClientOnContractVersionMismatch>
</MessageBusSettings>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings_New]
           ([Name]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.QuickItchN.dll'
			,N'<QuickItchNSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <HtaSessionSettings>
    <Host>209.191.250.210</Host>
	  <Port>9012</Port>
	  <Username>XXXXXXXX</Username>
	  <Password>XXXXXXXX</Password>
	  <HeartBeatInterval_Seconds>14</HeartBeatInterval_Seconds>
  </HtaSessionSettings>
  <HtfSessionSettings>
	  <Host>209.191.250.210</Host>
	  <Port>9012</Port>
	  <Username>XXXXXXXX</Username>
	  <Password>XXXXXXXX</Password>
	  <HeartBeatInterval_Seconds>14</HeartBeatInterval_Seconds>
  </HtfSessionSettings>
</QuickItchNSettings>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings_New]
           ([Name]
		   ,[FriendlyName]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.SessionManagement.dll'
			,N'Trading Instance (sessions for trading)'
			,N'<SessionsSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <FIXChannel_CRS>
    <ClientId>XXXXXXXX</ClientId>
    <Account>XXXXXXXX</Account>
  </FIXChannel_CRS>
  <FIXChannel_UBS>
    <Username>XXXXXXXX</Username>
    <Password>XXXXXXXX</Password>
    <PartyId>XXXXXXXX</PartyId>
  </FIXChannel_UBS>
  <FIXChannel_CTI>
    <Password>XXXXXXXX</Password>
    <Account>XXXXXXXX</Account>
  </FIXChannel_CTI>
  <FIXChannel_MGS>
    <OnBehalfOfCompId>XXXXXXXX</OnBehalfOfCompId>
    <Account>XXXXXXXX</Account>
  </FIXChannel_MGS>
  <FIXChannel_RBS>
    <Account>XXXXXXXX</Account>
  </FIXChannel_RBS>
  <FIXChannel_JPM>
    <Password>XXXXXXXX</Password>
    <PasswordLength>XXXXXXXX</PasswordLength>
    <Account>XXXXXXXX</Account>
  </FIXChannel_JPM>
  <FIXChannel_BNP>
    <Password>XXXXXXXX</Password>
    <Account>XXXXXXXX</Account>
  </FIXChannel_BNP>
  <FIXChannel_CZB>
    <Account>XXXXXXXX</Account>
  </FIXChannel_CZB>
  <FIXChannel_SOC>
    <OnBehalfOfCompID>XXXXXXXX</OnBehalfOfCompID>
  </FIXChannel_SOC>
  <FIXChannel_NOM>
    <OnBehalfOfCompID>XXXXXXXX</OnBehalfOfCompID>
    <SenderSubId>XXXXXXXX</SenderSubId>
    <Account>XXXXXXXX</Account>
  </FIXChannel_NOM>
  <FIXChannel_HTA>
    <SenderSubId>XXXXXXXX</SenderSubId>
    <Username>XXXXXXXX</Username>
    <Password>XXXXXXXX</Password>
  </FIXChannel_HTA>
  <FIXChannel_HTF>
    <SenderSubId>XXXXXXXX</SenderSubId>
    <Username>XXXXXXXX</Username>
    <Password>dKUK1VGC</Password>
  </FIXChannel_HTF>
  <UnexpectedMessagesTreating>
    <EnableThreasholdingOfUnexpectedMessages>true</EnableThreasholdingOfUnexpectedMessages>
    <MaximumAllowedInstancesPerTimeInterval>30</MaximumAllowedInstancesPerTimeInterval>
    <TimeIntervalToCheck_Minutes>30</TimeIntervalToCheck_Minutes>
  </UnexpectedMessagesTreating>
  <MarketDataSession>
    <MaxAutoResubscribeCount>4</MaxAutoResubscribeCount>
    <RetryIntervalsSequence_Seconds>1</RetryIntervalsSequence_Seconds>
    <RetryIntervalsSequence_Seconds>10</RetryIntervalsSequence_Seconds>
    <RetryIntervalsSequence_Seconds>60</RetryIntervalsSequence_Seconds>
    <RetryIntervalsSequence_Seconds>1800</RetryIntervalsSequence_Seconds>
    <UnconfirmedSubscriptionTimeout_Seconds>180</UnconfirmedSubscriptionTimeout_Seconds>
    <PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog>
  </MarketDataSession>
  <OrderFlowSession>
    <UnconfirmedOrderTimeout_Seconds>10</UnconfirmedOrderTimeout_Seconds>
    <PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog>
  </OrderFlowSession>
  
  
  <SubscriptionSettingsRaw>
    <ResubscribeOnReconnect>true</ResubscribeOnReconnect>
    <GlobalSubscriptionSettings>
      <Symbols>
        <SymbolSetting>
          <ShortName>ALL</ShortName>
          <Size>500000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
        <SymbolSetting>
          <ShortName>NOKSEK</ShortName>
          <Size>2000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
      </Symbols>
    </GlobalSubscriptionSettings>
    <CounterpartsSubscriptionSettings>
      <Counterparts>

       <CounterpartSetting>
          <ShortName>CRS</ShortName>
          <SettingAction>Add</SettingAction>
       </CounterpartSetting>
       
       <CounterpartSetting>
          <ShortName>MGS</ShortName>
          <SettingAction>Add</SettingAction>
          <Symbols>

            <SymbolSetting>
              <ShortName>ALL</ShortName>
              <Size>1000000</Size>
              <SettingAction>Add</SettingAction>
            </SymbolSetting>

		        <SymbolSetting>
		          <ShortName>NOKSEK</ShortName>
		          <Size>5000000</Size>
		          <SettingAction>Add</SettingAction>
		        </SymbolSetting>

		        <SymbolSetting>
		          <ShortName>ZARJPY</ShortName>
		          <Size>5000000</Size>
		          <SettingAction>Add</SettingAction>
		        </SymbolSetting>

          </Symbols>
       </CounterpartSetting>
 
       <CounterpartSetting>
          <ShortName>CTI</ShortName>
          <SettingAction>Add</SettingAction>
       </CounterpartSetting>
       
       <CounterpartSetting>
          <ShortName>UBS</ShortName>
          <SettingAction>Add</SettingAction>
       </CounterpartSetting>

       <CounterpartSetting>
          <ShortName>GLS</ShortName>
          <SettingAction>Add</SettingAction>
       </CounterpartSetting>
       
       <CounterpartSetting>
          <ShortName>HTA</ShortName>
          <SettingAction>Add</SettingAction>
       </CounterpartSetting>
       
      </Counterparts>
    </CounterpartsSubscriptionSettings>
  </SubscriptionSettingsRaw>
  
</SessionsSettings>'
			,GETUTCDATE()
			,0)
GO

INSERT INTO [dbo].[Settings_New]
           ([Name]
		   ,[FriendlyName]
           ,[SettingsXml]
		   ,[LastUpdated]
		   ,[IsOnlineMonitored])
     VALUES
			(N'Kreslik.Integrator.SessionManagement.dll'
			,N'NONTrading Instance (sessions for DC only)'
			,N'<SessionsSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <FIXChannel_CRS>
    <ClientId>XXXXXXXX</ClientId>
    <Account>XXXXXXXX</Account>
  </FIXChannel_CRS>
  <FIXChannel_UBS>
    <Username>XXXXXXXX</Username>
    <Password>XXXXXXXX</Password>
    <PartyId>XXXXXXXX</PartyId>
  </FIXChannel_UBS>
  <FIXChannel_CTI>
    <Password>XXXXXXXX</Password>
    <Account>XXXXXXXX</Account>
  </FIXChannel_CTI>
  <FIXChannel_MGS>
    <OnBehalfOfCompId>XXXXXXXX</OnBehalfOfCompId>
    <Account>XXXXXXXX</Account>
  </FIXChannel_MGS>
  <FIXChannel_RBS>
    <Account>XXXXXXXX</Account>
  </FIXChannel_RBS>
  <FIXChannel_JPM>
    <Password>XXXXXXXX</Password>
    <PasswordLength>XXXXXXXX</PasswordLength>
    <Account>XXXXXXXX</Account>
  </FIXChannel_JPM>
  <FIXChannel_BNP>
    <Password>XXXXXXXX</Password>
    <Account>XXXXXXXX</Account>
  </FIXChannel_BNP>
  <FIXChannel_CZB>
    <Account>XXXXXXXX</Account>
  </FIXChannel_CZB>
  <FIXChannel_SOC>
    <OnBehalfOfCompID>XXXXXXXX</OnBehalfOfCompID>
  </FIXChannel_SOC>
  <FIXChannel_NOM>
    <OnBehalfOfCompID>XXXXXXXX</OnBehalfOfCompID>
    <SenderSubId>XXXXXXXX</SenderSubId>
    <Account>XXXXXXXX</Account>
  </FIXChannel_NOM>
  <FIXChannel_HTA>
    <SenderSubId>XXXXXXXX</SenderSubId>
    <Username>XXXXXXXX</Username>
    <Password>XXXXXXXX</Password>
  </FIXChannel_HTA>
  <FIXChannel_HTF>
    <SenderSubId>XXXXXXXX</SenderSubId>
    <Username>XXXXXXXX</Username>
    <Password>dKUK1VGC</Password>
  </FIXChannel_HTF>
  <UnexpectedMessagesTreating>
    <EnableThreasholdingOfUnexpectedMessages>true</EnableThreasholdingOfUnexpectedMessages>
    <MaximumAllowedInstancesPerTimeInterval>30</MaximumAllowedInstancesPerTimeInterval>
    <TimeIntervalToCheck_Minutes>30</TimeIntervalToCheck_Minutes>
  </UnexpectedMessagesTreating>
  <MarketDataSession>
    <MaxAutoResubscribeCount>4</MaxAutoResubscribeCount>
    <RetryIntervalsSequence_Seconds>1</RetryIntervalsSequence_Seconds>
    <RetryIntervalsSequence_Seconds>10</RetryIntervalsSequence_Seconds>
    <RetryIntervalsSequence_Seconds>60</RetryIntervalsSequence_Seconds>
    <RetryIntervalsSequence_Seconds>1800</RetryIntervalsSequence_Seconds>
    <UnconfirmedSubscriptionTimeout_Seconds>180</UnconfirmedSubscriptionTimeout_Seconds>
    <PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog>
  </MarketDataSession>
  <OrderFlowSession>
    <UnconfirmedOrderTimeout_Seconds>10</UnconfirmedOrderTimeout_Seconds>
    <PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog>
  </OrderFlowSession>
  
  
  <SubscriptionSettingsRaw>
    <ResubscribeOnReconnect>true</ResubscribeOnReconnect>
    <GlobalSubscriptionSettings>
      <Symbols>
        <SymbolSetting>
          <ShortName>ALL</ShortName>
          <Size>500000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
        <SymbolSetting>
          <ShortName>NOKSEK</ShortName>
          <Size>2000000</Size>
          <SettingAction>Add</SettingAction>
        </SymbolSetting>
      </Symbols>
    </GlobalSubscriptionSettings>
    <CounterpartsSubscriptionSettings>
      <Counterparts>

       <CounterpartSetting>
          <ShortName>BNP</ShortName>
          <SettingAction>Add</SettingAction>
        </CounterpartSetting>
        
       <CounterpartSetting>
          <ShortName>BOA</ShortName>
          <SettingAction>Add</SettingAction>
       </CounterpartSetting>
       
              <CounterpartSetting>
          <ShortName>BRX</ShortName>
          <SettingAction>Add</SettingAction>
       </CounterpartSetting>
       
       <CounterpartSetting>
          <ShortName>CZB</ShortName>
          <SettingAction>Add</SettingAction>
          <Symbols>
          
		        <SymbolSetting>
		          <ShortName>USDILS</ShortName>
		          <SettingAction>Remove</SettingAction>
		        </SymbolSetting>
		        
		        <SymbolSetting>
		          <ShortName>USDSKK</ShortName>
		          <SettingAction>Remove</SettingAction>
		        </SymbolSetting>
		        <SymbolSetting>
		          <ShortName>EURSKK</ShortName>
		          <SettingAction>Remove</SettingAction>
		        </SymbolSetting>
		        
		        <SymbolSetting>
		          <ShortName>CADDKK</ShortName>
		          <SettingAction>Remove</SettingAction>
		        </SymbolSetting>
		        <SymbolSetting>
		          <ShortName>EURCNH</ShortName>
		          <SettingAction>Remove</SettingAction>
		        </SymbolSetting>
		        <SymbolSetting>
		          <ShortName>GBPCZK</ShortName>
		          <SettingAction>Remove</SettingAction>
		        </SymbolSetting>
		        <SymbolSetting>
		          <ShortName>NZDNOK</ShortName>
		          <SettingAction>Remove</SettingAction>
		        </SymbolSetting>
		        <SymbolSetting>
		          <ShortName>USDCNH</ShortName>
		          <SettingAction>Remove</SettingAction>
		        </SymbolSetting>
		        
          </Symbols>
       </CounterpartSetting>
       
       <CounterpartSetting>
          <ShortName>HSB</ShortName>
          <SettingAction>Add</SettingAction>
          <SenderSubId>KGT</SenderSubId>
       </CounterpartSetting>
           
       <CounterpartSetting>
          <ShortName>DBK</ShortName>
          <SettingAction>Add</SettingAction>
       </CounterpartSetting>  
      
       

       <CounterpartSetting>
          <ShortName>NOM</ShortName>
          <SettingAction>Add</SettingAction>
          <Symbols>
          
		        <SymbolSetting>
		          <ShortName>USDILS</ShortName>
		          <SettingAction>Remove</SettingAction>
		        </SymbolSetting>
		        
          </Symbols>
       </CounterpartSetting>
	   
	   <CounterpartSetting>
          <ShortName>RBS</ShortName>
          <SettingAction>Add</SettingAction>
       </CounterpartSetting>
       
       <CounterpartSetting>
          <ShortName>SOC</ShortName>
          <SettingAction>Add</SettingAction>
          <Symbols>
          
            <SymbolSetting>
              <ShortName>ALL</ShortName>
              <Size>1000000</Size>
              <SettingAction>Add</SettingAction>
            </SymbolSetting>
            
		        <SymbolSetting>
		          <ShortName>NOKSEK</ShortName>
		          <Size>2000000</Size>
		          <SettingAction>Add</SettingAction>
		        </SymbolSetting>
		        
          </Symbols>
       </CounterpartSetting>
       
       <CounterpartSetting>
          <ShortName>JPM</ShortName>
          <SettingAction>Add</SettingAction>
       </CounterpartSetting>
       
       <CounterpartSetting>
          <ShortName>HTF</ShortName>
          <SettingAction>Add</SettingAction>
       </CounterpartSetting>

      </Counterparts>
    </CounterpartsSubscriptionSettings>
  </SubscriptionSettingsRaw>
  
</SessionsSettings>'
			,GETUTCDATE()
			,0)
GO

DROP TABLE Settings
GO
DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.XsdSchemas]
GO
sp_rename Settings_New, Settings
GO

/****** Object:  Index [Settings_Id]    Script Date: 12. 12. 2013 18:28:54 ******/
CREATE UNIQUE CLUSTERED INDEX [Settings_Id] ON [dbo].[Settings]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


CREATE TABLE [dbo].[IntegratorEnvironment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
) ON [PRIMARY]
GO

/****** Object:  Index [IntegratorEnvironment_Name]    Script Date: 12. 12. 2013 19:02:51 ******/
CREATE UNIQUE CLUSTERED INDEX [IntegratorEnvironment_Name] ON [dbo].[IntegratorEnvironment]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE UNIQUE INDEX [IntegratorEnvironment_Id] ON [dbo].[IntegratorEnvironment]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE TABLE [dbo].[IntegratorEnvironmentToSettingsMap](
	[IntegratorEnvironmentId] [int] NOT NULL,
	[SettingsId] [int] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsMap]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironmentToSettingsMap_Environment] FOREIGN KEY([IntegratorEnvironmentId])
REFERENCES [dbo].[IntegratorEnvironment] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsMap] CHECK CONSTRAINT [FK_IntegratorEnvironmentToSettingsMap_Environment]
GO

ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsMap]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironmentToSettingsMap_Settings] FOREIGN KEY([SettingsId])
REFERENCES [dbo].[Settings] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsMap] CHECK CONSTRAINT [FK_IntegratorEnvironmentToSettingsMap_Settings]
GO



CREATE PROCEDURE [dbo].[GetEnvironmentExists_SP]( 
	@EnvironmentName [nvarchar](100)
	)
AS
BEGIN
	SELECT 
		[Name] 
	FROM 
		[dbo].[IntegratorEnvironment]
	WHERE
		[Name] = @EnvironmentName
END
GO

GRANT EXECUTE ON [dbo].[GetEnvironmentExists_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


ALTER PROCEDURE [dbo].[GetSettingsXml_SP] 
	@EnvironmentName NVARCHAR(100),
	@SettingName NVARCHAR(100),
	@UpdatedAfter Datetime
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		sett.SettingsXml
	FROM
		[dbo].[IntegratorEnvironment] env 
		INNER JOIN [dbo].[IntegratorEnvironmentToSettingsMap] map ON env.Id = map.IntegratorEnvironmentId
		INNER JOIN [dbo].[Settings] sett ON sett.id = map.SettingsId
	WHERE
		env.Name = @EnvironmentName
		AND sett.Name = @SettingName
		AND sett.[LastUpdated] > @UpdatedAfter
END
GO

ALTER PROCEDURE [dbo].[GenerateUpdateSettingsScripts_SP] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 'UPDATE [dbo].[Settings] SET [SettingsXml] = ''' + 
	CONVERT(VARCHAR(MAX), [SettingsXml]) +
	''', [LastUpdated] = GETUTCDATE() WHERE [Name] = ''' + 
	CONVERT(VARCHAR(MAX), [Name]) + ''' AND [Id] = ' + CONVERT(VARCHAR(5), [Id])
	FROM [dbo].[Settings]
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings_FIX](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[SettingsVersion] INT NOT NULL DEFAULT(1),
	[Configuration] [nvarchar](MAX) NOT NULL,
	[LastUpdated] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Index [Settings_Id]    Script Date: 12. 12. 2013 18:28:54 ******/
CREATE UNIQUE CLUSTERED INDEX [SettingsFIX_Id] ON [dbo].[Settings_FIX]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap](
	[IntegratorEnvironmentId] [int] NOT NULL,
	[SettingsFixId] [int] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironmentToSettingsFixMap_Environment] FOREIGN KEY([IntegratorEnvironmentId])
REFERENCES [dbo].[IntegratorEnvironment] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap] CHECK CONSTRAINT [FK_IntegratorEnvironmentToSettingsFixMap_Environment]
GO

ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironmentToSettingsFixMap_SettingsFix] FOREIGN KEY([SettingsFixId])
REFERENCES [dbo].[Settings_FIX] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap] CHECK CONSTRAINT [FK_IntegratorEnvironmentToSettingsFixMap_SettingsFix]
GO

CREATE PROCEDURE [dbo].[GetFixSettingsCfg_SP] 
	@EnvironmentName NVARCHAR(100),
	@SettingName NVARCHAR(100)
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		cfg.Configuration
	FROM
		[dbo].[IntegratorEnvironment] env 
		INNER JOIN [dbo].[IntegratorEnvironmentToSettingsFixMap] map ON env.Id = map.IntegratorEnvironmentId
		INNER JOIN [dbo].[Settings_FIX] cfg ON cfg.id = map.SettingsFixId
	WHERE
		env.Name = @EnvironmentName
		AND cfg.Name = @SettingName
END
GO

GRANT EXECUTE ON [dbo].[GetFixSettingsCfg_SP] TO [IntegratorServiceAccount] AS [dbo]
GO





INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'BNP_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


#SSLEnable=Y
#SSLCertificate=BNPClientCertificate_UAT.pfx
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N

SenderCompID=KGTPrice

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_BNP.xml
BeginString=FIX.4.4
TargetCompID=BNPP
StartTime=00:00:00
EndTime=23:59:59
HeartBtInt=30


# stunnel port was 20004 
SocketConnectPort=23581
SocketConnectHost=159.95.18.138'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'BNP_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y


#SSLEnable=Y
#SSLCertificate=BNPClientCertificate_UAT.pfx
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N

SenderCompID=KGTOrder

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_BNP.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=BNPP
StartTime=00:00:00
EndTime=23:59:59
HeartBtInt=30


# stunnel port was 20004 
SocketConnectPort=23582
SocketConnectHost=159.95.18.138'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'BOA_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT-QTS


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX43_BOA.xml
BeginString=FIX.4.3
TargetCompID=BAML-FIX-FX
#5pm NYK
StartTime=17:06:00
EndTime=17:00:00
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=6371
SocketConnectHost=165.34.236.241'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'BOA_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT-ORD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX43_BOA.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.3
TargetCompID=BAML-FIX-FX
#5pm NYK
StartTime=17:06:00
EndTime=17:00:00
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=6372
SocketConnectHost=165.34.236.241'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'BRX_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=60
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT-PRICES


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_BRX.xml
BeginString=FIX.4.2
TargetCompID=BARX-PRICES
#5pm NYK
StartTime=17:00:30
EndTime=16:59:30
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - direct connect:
SocketConnectPort=54417
SocketConnectHost=141.228.140.73

#failover
SocketConnectPort1=54417
SocketConnectHost1=141.228.80.73'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'BRX_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=60
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT-TRADES


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_BRX.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=BARX-TRADES
#5pm NYK
StartTime=17:00:30
EndTime=16:59:30
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - direct connect:
SocketConnectPort=54418
SocketConnectHost=141.228.140.73

#failover
SocketConnectPort1=54418
SocketConnectHost1=141.228.80.73'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'CRS_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=CSFX-KGT-QUO


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_CRS.xml
BeginString=FIX.4.2
TargetCompID=EFX_KGT
#Resets 06:05 - 06:10 Asian/Singapore time
#StartTime=06:10:10
#EndTime=06:04:50
#TimeZone=Singapore Standard Time
#There was a change to CRS session schedule (20:45UTC)
StartTime=20:47:00
EndTime=20:44:00

HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=10341
SocketConnectHost=169.33.48.50'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'CRS_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=CSFX-KGT-ORD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_CRS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=EFX_KGT
#Resets 06:05 - 06:10 Asian/Singapore time
#StartTime=06:10:10
#EndTime=06:04:50
#TimeZone=Singapore Standard Time
#There was a change to CRS session schedule (20:45UTC)
StartTime=20:47:00
EndTime=20:44:00

HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=10341
SocketConnectHost=169.33.48.50
'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'CTI_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

#SSLEnable=Y
#SSLCertificate=
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N


SenderCompID=KGT-QUOTE
TargetSubID=FXSpot

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_CTI.xml
BeginString=FIX.4.4
TargetCompID=CITIFX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 20001
SocketConnectPort=30243
SocketConnectHost=192.193.51.183'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'CTI_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

#SSLEnable=Y
#SSLCertificate=
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N


SenderCompID=KGT-TRADE
TargetSubID=FXSpot

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_CTI.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=CITIFX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 20001
SocketConnectPort=30243
SocketConnectHost=192.193.51.183'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'CZB_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


#SSLEnable=Y
#SSLCertificate=
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N


SenderCompID=KGTINVMD

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_CZB.xml
BeginString=FIX.4.4
TargetCompID=COBAFXMD
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 20003
SocketConnectPort=19001
SocketConnectHost=195.42.239.71

#failover
SocketConnectPort1=19001
SocketConnectHost1=160.82.118.143'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'CZB_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y


#SSLEnable=Y
#SSLCertificate=
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N


SenderCompID=KGTINVORD

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_CZB.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=COBAFXORD
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 20003
SocketConnectPort=19001
SocketConnectHost=195.42.239.71

#failover
SocketConnectPort1=19001
SocketConnectHost1=160.82.118.143'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'DBK_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


SSLEnable=N
#SSLCertificate=KGTI.FIX_nopass.pfx
#SSLServerName=KGTI.FIX
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N


SenderCompID=KGTI.FIX

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_DBK.xml
BeginString=FIX.4.4
TargetCompID=ABFX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=8002
SocketConnectHost=160.82.118.241'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'DBK_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y


SSLEnable=N
#SSLCertificate=DBKClientCertificate2_UAT.pfx
#SSLServerName=UAT.KGTI.FIX2
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N

#SenderCompID=KGTI_P.FIX
SenderCompID=KGTI1_P.FIX

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_DBK.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=ABFX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnel port was 20002
SocketConnectPort=8002
SocketConnectHost=160.82.118.241'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'GLS_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGTINVEST


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_GLS.xml
BeginString=FIX.4.4
TargetCompID=GSFXPRICES
#5pm NYK
StartTime=17:05:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=12010
SocketConnectHost=75.96.80.214'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'GLS_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGTINVEST


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_GLS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=GSFXTRADES
#5pm NYK
StartTime=17:05:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=12010
SocketConnectHost=75.96.80.214'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'HSB_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

#SSLEnable=Y
#SSLCertificate=HSBClientCertificate_UAT.pfx
#SSLServerName=fix.hsbc.com
#SSLProtocols=
#SSLCACertificate=HSBCACertificate_UAT.pfx
#SSLValidateCertificates=Y
#SSLValidateCertificatesOID=N


SenderCompID=KGTQLSSTR

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_HSB.xml
BeginString=FIX.4.4
TargetCompID=HSBCUKFIX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnel port was 19997
SocketConnectPort=47018
SocketConnectHost=208.224.251.4

#failover 1
SocketConnectPort1=47018
SocketConnectHost1=208.224.251.5

#failover 2
SocketConnectPort2=47018
SocketConnectHost2=208.224.251.7'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'HSB_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

#SSLEnable=Y
#SSLCertificate=HSBClientCertificate_UAT.pfx
#SSLServerName=fix.hsbc.com
#SSLProtocols=
#SSLCACertificate=HSBCACertificate_UAT.pfx
#SSLValidateCertificates=Y
#SSLValidateCertificatesOID=N

SenderCompID=KGTQLSTRD

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_HSB.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=HSBCUKFIX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 19997
SocketConnectPort=47018
SocketConnectHost=208.224.251.4

#failover 1
SocketConnectPort1=47018
SocketConnectHost1=208.224.251.5

#failover 2
SocketConnectPort2=47018
SocketConnectHost2=208.224.251.7
'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'HTA_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y
#Hotspot does not want reply of any messages - so no point of storing them
PersistMessages=N

SenderCompID=KGT2

SSLEnable=N
#SSLCertificate=UBSClientCertificate_UAT.pfx
#SSLServerName=fxfixb2bpte1.ibb.ubstest.com
#SSLProtocols=
#SSLCACertificate=UBSCACertificate_UAT.pfx
#SSLValidateCertificates=Y
#SSLValidateCertificatesOID=N



[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_HOT.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
SessionQualifier=HTA
TargetCompID=HSFX-FIX-BRIDGE
#5pm NYK
StartTime=17:02:05
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=8004
SocketConnectHost=209.191.250.180'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'HTF_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y
#Hotspot does not want reply of any messages - so no point of storing them
PersistMessages=N

SenderCompID=KGT

SSLEnable=N
#SSLCertificate=UBSClientCertificate_UAT.pfx
#SSLServerName=fxfixb2bpte1.ibb.ubstest.com
#SSLProtocols=
#SSLCACertificate=UBSCACertificate_UAT.pfx
#SSLValidateCertificates=Y
#SSLValidateCertificatesOID=N



[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_HOT.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
SessionQualifier=HTF
TargetCompID=HSFX-FIX-BRIDGE
#5pm NYK
StartTime=17:02:05
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=8004
SocketConnectHost=209.191.250.180'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'JPM_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=60
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT


SSLEnable=Y
SSLCertificate=JPMClientAndCACertificate_PRD.pfx
#SSLServerName=GCCG - Integration
SSLProtocols=Ssl3
SSLCACertificate=JPMClientAndCACertificate_PRD.pfx
SSLValidateCertificates=N
SSLValidateCertificatesOID=N


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_JPM.xml
BeginString=FIX.4.2
TargetCompID=JPM-MD
StartTime=00:00:00
EndTime=23:59:59
HeartBtInt=30

# stunnel port was 20006
SocketConnectPort=8061
SocketConnectHost=169.116.253.26
#SocketConnectPort=29006
#SocketConnectHost=127.0.0.1'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'JPM_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=60
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT


SSLEnable=Y
SSLCertificate=JPMClientAndCACertificate_PRD.pfx
#SSLServerName=GCCG - Integration
SSLProtocols=Ssl3
SSLCACertificate=JPMClientAndCACertificate_PRD.pfx
SSLValidateCertificates=N
SSLValidateCertificatesOID=N


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_JPM.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=JPM-OM
StartTime=00:00:00
EndTime=23:59:59
HeartBtInt=30

# stunnel port was 20006
SocketConnectPort=8062
SocketConnectHost=169.116.253.26'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'MGS_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT_RATE


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_MGS.xml
BeginString=FIX.4.2
TargetCompID=MSFX
OnBehalfOfCompID=KGT
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=16638
SocketConnectHost=109.74.25.209'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'MGS_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT_ORD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_MGS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=MSFX
OnBehalfOfCompID=KGT
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=16638
SocketConnectHost=109.74.25.209'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'NOM_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT_PRICE


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_NOM.xml
BeginString=FIX.4.4
TargetCompID=NOMURAFX
#5pm NYK
StartTime=17:16:00
EndTime=16:59:00
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - direct connect:
SocketConnectPort=40128
SocketConnectHost=10.113.168.7

#failover
SocketConnectPort1=40128
SocketConnectHost1=10.81.168.7'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'NOM_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT_TRADE


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_NOM.xml
BeginString=FIX.4.4
TargetCompID=NOMURAFX
#5pm NYK
StartTime=17:16:00
EndTime=16:59:00
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - direct connect:
SocketConnectPort=40128
SocketConnectHost=10.113.168.7

#failover
SocketConnectPort1=40128
SocketConnectHost1=10.81.168.7'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'RBS_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


#SSLEnable=Y
#SSLCertificate=RBSClientCertificate_UAT.pfx
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N



SenderCompID=kgt.api_MD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_RBS.xml
BeginString=FIX.4.4
TargetCompID=RBS_FX
#5:01pm NYK
StartTime=17:01:59
EndTime=16:59:59
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnel connection port was 20000
SocketConnectPort=4321
SocketConnectHost=147.114.61.254'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'RBS_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y


#SSLEnable=Y
#SSLCertificate=RBSClientCertificate_UAT.pfx
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N



SenderCompID=kgt.api_ORD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_RBS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=RBS_FX
#5:01pm NYK
StartTime=17:01:59
EndTime=16:59:59
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnel connection port was 20000
SocketConnectPort=4322
SocketConnectHost=147.114.61.254'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'SOC_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT-MKD

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_SOC.xml
BeginString=FIX.4.2
TargetCompID=SOCGEN-MKD
#5pm NYK
StartTime=17:05:10
EndTime=16:59:30
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=9066
SocketConnectHost=184.34.185.129

#failover
SocketConnectPort1=9066
SocketConnectHost1=184.42.12.17'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'SOC_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_SOC.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=SOCGEN
#5pm NYK
StartTime=17:05:10
EndTime=16:59:30
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=9067
SocketConnectHost=184.34.185.129

#failover
SocketConnectPort1=9067
SocketConnectHost1=184.42.12.17'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'UBS_QUO'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT_MKD

SSLEnable=Y
SSLCertificate=UBSClientCertificate_PRD.pfx
#SSLServerName=fxfixb2bpte1.ibb.ubstest.com
#SSLServerName=btobxprd0462
#SSLProtocols=
SSLCACertificate=UBSCACertificate_PRD.pfx
SSLValidateCertificates=N
SSLValidateCertificatesOID=N



[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX43_UBS.xml
BeginString=FIX.4.3
TargetCompID=UBSFX2B_PRD
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnelport was 19999
SocketConnectPort=2500
SocketConnectHost=139.149.131.5

# failover
SocketConnectPort1=2500
SocketConnectHost1=139.149.11.181'
           ,GETUTCDATE())
GO

INSERT INTO [dbo].[Settings_FIX]
           ([Name]
           ,[Configuration]
           ,[LastUpdated])
     VALUES
           (N'UBS_ORD'
           ,N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT_ORD

SSLEnable=Y
SSLCertificate=UBSClientCertificate_PRD.pfx
#SSLServerName=fxfixb2bpte1.ibb.ubstest.com
#SSLServerName=btobxprd0462
#SSLProtocols=
SSLCACertificate=UBSCACertificate_PRD.pfx
SSLValidateCertificates=N
SSLValidateCertificatesOID=N



[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX43_UBS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.3
TargetCompID=UBSFX2B_PRD
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnelport was 19999
SocketConnectPort=2500
SocketConnectHost=139.149.131.5

# failover
SocketConnectPort1=2500
SocketConnectHost1=139.149.11.181'
           ,GETUTCDATE())
GO



CREATE TABLE [dbo].[CertificateFile](
	[FileName] [nvarchar](100) NOT NULL,
	[FileContent] [varbinary](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Index [CertificateFile_FileName]    Script Date: 13. 12. 2013 16:06:25 ******/
CREATE UNIQUE CLUSTERED INDEX [CertificateFile_FileName] ON [dbo].[CertificateFile]
(
	[FileName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE PROCEDURE [dbo].[GetCertificateFile_SP] 
	@FileName NVARCHAR(100)
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		FileContent
	FROM
		[dbo].[CertificateFile]
	WHERE
		FileName = @FileName
END
GO

GRANT EXECUTE ON [dbo].[GetCertificateFile_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


INSERT INTO [dbo].[CertificateFile]
           ([FileName], [FileContent])
     SELECT N'JPMClientAndCACertificate_PRD.pfx', BulkColumn FROM OPENROWSET(BULK
     N'C:\Data\Temp\CertificatesToInsert\JPMClientAndCACertificate_PRD.pfx', SINGLE_BLOB) AS Document
GO

INSERT INTO [dbo].[CertificateFile]
           ([FileName], [FileContent])
     SELECT N'UBSCACertificate_PRD.pfx', BulkColumn FROM OPENROWSET(BULK
     N'C:\Data\Temp\CertificatesToInsert\UBSCACertificate_PRD.pfx', SINGLE_BLOB) AS Document
GO

INSERT INTO [dbo].[CertificateFile]
           ([FileName], [FileContent])
     SELECT N'UBSClientCertificate_PRD.pfx', BulkColumn FROM OPENROWSET(BULK
     N'C:\Data\Temp\CertificatesToInsert\UBSClientCertificate_PRD.pfx', SINGLE_BLOB) AS Document
GO

INSERT INTO [dbo].[IntegratorEnvironment]
           ([Name])
     VALUES
           (N'Production_TradingInstance')
GO

INSERT INTO [dbo].[IntegratorEnvironment]
           ([Name])
     VALUES
           (N'Production_NonTradingInstance')
GO

INSERT INTO [dbo].[IntegratorEnvironment]
           ([Name])
     VALUES
           (N'Production_Clients')
GO

INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
     SELECT 1, [Id] FROM [dbo].[Settings_FIX]
GO

INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
     SELECT 2, [Id] FROM [dbo].[Settings_FIX]
GO

-- all but clients common and nontrading DAL and Sessions
INSERT INTO [dbo].[IntegratorEnvironmentToSettingsMap]
           ([IntegratorEnvironmentId]
           ,[SettingsId])
     SELECT 1, [Id] FROM [dbo].[Settings] WHERE Id NOT IN (4,6,13)
GO

-- all but clients common and trading DAL and Sessions
INSERT INTO [dbo].[IntegratorEnvironmentToSettingsMap]
           ([IntegratorEnvironmentId]
           ,[SettingsId])
     SELECT 2, [Id] FROM [dbo].[Settings] WHERE Id NOT IN (4,5,12)
GO

-- clients common and nontrading DAL
INSERT INTO [dbo].[IntegratorEnvironmentToSettingsMap]
           ([IntegratorEnvironmentId]
           ,[SettingsId])
     VALUES(3, 4)
GO

INSERT INTO [dbo].[IntegratorEnvironmentToSettingsMap]
           ([IntegratorEnvironmentId]
           ,[SettingsId])
     VALUES(3, 6)
GO