

EXEC sp_rename 'dbo.TradeDecayAnalysis_Pending.SourceEventRecordId', 'SourceEventGlobalRecordId', 'COLUMN';
EXEC sp_rename 'dbo.TradeDecayAnalysis.SourceEventRecordId', 'SourceEventGlobalRecordId', 'COLUMN';
GO
ALTER PROCEDURE [dbo].[InsertSingleTradeDecayAnalysisRequest_SP]
(
	@TradeDecayAnalysisTypeName [varchar](16),
	@SourceEventGlobalRecordId [int],
	@AnalysisStartTimeUtc [datetime2](7),
	@IntegratorSymbolId [tinyint],
	@IntegratorCounterpartyId [tinyint],
	@ReferencePrice [decimal](18,9),
	@MatchingContraPriceSideId [bit]
)
AS
BEGIN
	INSERT INTO [dbo].[TradeDecayAnalysis_Pending]
           ([TradeDecayAnalysisTypeId]
           ,[SourceEventGlobalRecordId]
           ,[AnalysisStartTimeUtc]
           ,[FXPairId]
           ,[CounterpartyId]
           ,[ReferencePrice]
           ,[PriceSideId])
     VALUES
           ((SELECT [TradeDecayAnalysisTypeId] FROM [dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = @TradeDecayAnalysisTypeName)
           ,@SourceEventGlobalRecordId
           ,@AnalysisStartTimeUtc
           ,(SELECT [DCDBSymbolId] FROM [dbo].[_Internal_SymbolIdsMapping] WHERE [IntegratorDBSymbolId] = @IntegratorSymbolId)
           ,(SELECT [DCDBCounterpartyId] FROM [dbo].[_Internal_CounterpartyIdsMapping] WHERE [IntegratorDBCounterpartyId] = @IntegratorCounterpartyId)
           ,@ReferencePrice
           ,@MatchingContraPriceSideId)
END
GO

ALTER PROCEDURE [dbo].[ProcessTradeDecayAnalysisQueue_SP]
(
	@FromTimeUtc [datetime2](7) = '1900-01-01',
	@ToTimeUtc [datetime2](7) = '3000-01-01',
	@BatchSize [int] = 5
)
AS
BEGIN

	DECLARE @TempPendingWork TABLE (
		[PendingTradeDecayAnalysisId] [int] NULL,
		[TradeDecayAnalysisTypeId] [tinyint] NOT NULL,
		[SourceEventGlobalRecordId] [int] NOT NULL,
		[AnalysisStartTimeUtc] [datetime2](7) NOT NULL,
		[FXPairId] [tinyint] NOT NULL,
		[CounterpartyId] [tinyint] NOT NULL,
		[ReferencePrice] [decimal](18,9) NOT NULL,
		[PriceSideId] [bit] NOT NULL
	)

BEGIN TRY
WHILE(1 = 1)
BEGIN
	print 'Starting to process next batch'
	print GETUTCDATE()

	DECLARE @CurrentUTCTime DATETIME2(7)
	SET @CurrentUTCTime = (SELECT GETUTCDATE())
	
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

	BEGIN TRANSACTION

	INSERT INTO @TempPendingWork (
		[PendingTradeDecayAnalysisId],
		[TradeDecayAnalysisTypeId],
		[SourceEventGlobalRecordId],
		[AnalysisStartTimeUtc],
		[FXPairId],
		[CounterpartyId],
		[ReferencePrice],
		[PriceSideId]
		)
	SELECT TOP (@BatchSize) 
		[PendingTradeDecayAnalysisId],
		[TradeDecayAnalysisTypeId],
		[SourceEventGlobalRecordId],
		[AnalysisStartTimeUtc],
		[FXPairId] [tinyint],
		[CounterpartyId],
		[ReferencePrice],
		[PriceSideId]
	FROM [dbo].[TradeDecayAnalysis_Pending] WITH (TABLOCKX)
	WHERE 
		AnalysisStartTimeUtc BETWEEN @FromTimeUtc AND @ToTimeUtc
		AND
			(
			AcquiredForCalculationTimeUtc IS NULL
			OR (@CurrentUTCTime > DATEADD(mi, 120, AcquiredForCalculationTimeUtc))
			)

	UPDATE wq
	SET AcquiredForCalculationTimeUtc = @CurrentUTCTime
	FROM @TempPendingWork TEMP
	INNER JOIN [dbo].[TradeDecayAnalysis_Pending] wq ON TEMP.[PendingTradeDecayAnalysisId] = wq.[PendingTradeDecayAnalysisId]

	COMMIT TRANSACTION
	
	IF(NOT EXISTS(SELECT * FROM @TempPendingWork))
		BREAK;
	
	
	DECLARE @PendingTradeDecayAnalysisId INT,
		@TradeDecayAnalysisTypeId [tinyint],
		@SourceEventGlobalRecordId [int],
		@AnalysisStartTimeUtc [datetime2](7),
		@FXPairId [tinyint],
		@CounterpartyId [tinyint],
		@ReferencePrice [decimal](18,9),
		@PriceSideId [bit]

	DECLARE itemsCursor CURSOR FOR
		SELECT TOP (@BatchSize)
			[PendingTradeDecayAnalysisId],
			[TradeDecayAnalysisTypeId],
			[SourceEventGlobalRecordId],
			[AnalysisStartTimeUtc],
			[FXPairId] [tinyint],
			[CounterpartyId],
			[ReferencePrice],
			[PriceSideId]
		FROM @TempPendingWork 
    
	OPEN itemsCursor
    FETCH NEXT FROM itemsCursor INTO
		@PendingTradeDecayAnalysisId,
		@TradeDecayAnalysisTypeId,
		@SourceEventGlobalRecordId,
		@AnalysisStartTimeUtc,
		@FXPairId,
		@CounterpartyId,
		@ReferencePrice,
		@PriceSideId
    WHILE @@FETCH_STATUS = 0
    BEGIN

		DELETE FROM [dbo].[TradeDecayAnalysis_Pending] WHERE PendingTradeDecayAnalysisId = @PendingTradeDecayAnalysisId
		
		--BEGIN TRY
			EXEC [dbo].[PerformSingleTradeDecayAnalysis_SP]
				@TradeDecayAnalysisTypeId,
				@SourceEventGlobalRecordId,
				@AnalysisStartTimeUtc,
				@FXPairId,
				@CounterpartyId,
				@ReferencePrice,
				@PriceSideId
				
			print 'Processed next analysis request'
			print GETUTCDATE()	
				
		--END TRY
		--BEGIN CATCH
			--CLOSE itemsCursor
			--DEALLOCATE itemsCursor
			--THROW;
		--END CATCH

		FETCH NEXT FROM itemsCursor INTO
			@PendingTradeDecayAnalysisId,
			@TradeDecayAnalysisTypeId,
			@SourceEventGlobalRecordId,
			@AnalysisStartTimeUtc,
			@FXPairId,
			@CounterpartyId,
			@ReferencePrice,
			@PriceSideId
    END
	CLOSE itemsCursor
    DEALLOCATE itemsCursor
	
	DELETE FROM @TempPendingWork
END
END TRY
BEGIN CATCH
	print 'failed to processrequest records';
	THROW;
END CATCH
END
GO

TRUNCATE TABLE [dbo].[TradeDecayAnalysisDataPoint]
TRUNCATE TABLE [dbo].[TradeDecayAnalysis]
GO

EXEC sp_rename 'dbo.TradeDecayAnalysisDataPoint.BpDifferenceFromReferencePrice', 'BpMtmToReferenceMidPrice', 'COLUMN';
EXEC sp_rename 'dbo.TradeDecayAnalysisDataPoint.MidToB', 'ReferenceMidPrice', 'COLUMN';


ALTER TABLE [dbo].[TradeDecayAnalysisDataPoint] ADD ReferenceBidPrice decimal(18,9) NOT NULL
ALTER TABLE [dbo].[TradeDecayAnalysisDataPoint] ADD ReferenceAskPrice decimal(18,9) NOT NULL
ALTER TABLE [dbo].[TradeDecayAnalysisDataPoint] ADD BpMtmToReferenceCrossPrice decimal(18,9) NOT NULL
ALTER TABLE [dbo].[TradeDecayAnalysisDataPoint] ADD BpMtmToReferenceJoinPrice decimal(18,9) NOT NULL

ALTER TABLE [dbo].[TradeDecayAnalysisDataPoint] ADD IsReferenceBidAuthenticValue bit NOT NULL
ALTER TABLE [dbo].[TradeDecayAnalysisDataPoint] ADD IsReferenceAskAuthenticValue decimal(18,9) NOT NULL
GO
ALTER TABLE [dbo].[TradeDecayAnalysisDataPoint] ADD IsReferenceMidAuthenticValue AS (CASE WHEN IsReferenceBidAuthenticValue = 1 AND IsReferenceAskAuthenticValue = 1 THEN 1 ELSE 0 END)

ALTER TABLE [dbo].[TradeDecayAnalysisDataPoint] DROP COLUMN IsAuthenticValue
GO

ALTER PROCEDURE [dbo].[PerformSingleTradeDecayAnalysis_SP]
(
	@TradeDecayAnalysisTypeId [tinyint],
	@SourceEventGlobalRecordId [int],
	@AnalysisStartTimeUtc [datetime2](7),
	@FXPairId [tinyint],
	@CounterpartyId [tinyint],
	@ReferencePrice [decimal](18,9),
	@PriceSideId [bit]
)
AS
BEGIN

	--@PriceSideId  meaning
	-- 0 is our sell - so our Ask (or sell on cpt Bid)
	-- 1 is our buy - so our Bid (or buy on cpt Ask)

	DECLARE @TradeDecayAnalysisId INT

	INSERT INTO [dbo].[TradeDecayAnalysis]
           ([TradeDecayAnalysisTypeId]
           ,[SourceEventGlobalRecordId]
           ,[AnalysisStartTimeUtc]
           ,[FXPairId]
           ,[CounterpartyId]
           ,[PriceSideId]
           ,[HasValidTickAfterIntervalEnd]
           ,[IsValidAndComplete])
     VALUES
           (@TradeDecayAnalysisTypeId,
			@SourceEventGlobalRecordId,
			@AnalysisStartTimeUtc,
			@FXPairId,
			@CounterpartyId,
			@PriceSideId,
			0,
			0)
	
	SET @TradeDecayAnalysisId = scope_identity();
	DECLARE @ReferenceCounterpartyId INT = (SELECT [LiqProviderStreamId] FROM [LiqProviderStream] WHERE [LiqProviderStreamName] = 'L01')
	-- round to next whole us and and 1ms for the first interval
	DECLARE @AnalysisFirstIntervalStartTimeUtc [datetime2](7) = DATEADD(ns, -DATEPART(ns, @AnalysisStartTimeUtc)%1000 + 1000 + 1000000, @AnalysisStartTimeUtc)
	DECLARE @AnalysisEndTimeUtc [datetime2](7) = DATEADD(minute, 5, @AnalysisStartTimeUtc)
	
	
	DECLARE @FirstBidTob [decimal](18,9)
	DECLARE @FirstAskTob [decimal](18,9)
	
	SET @FirstBidTob =
	(SELECT TOP 1
		md.Price AS LastToBPrice
	FROM
		[dbo].[MarketData] md with (nolock) 
		INNER JOIN [dbo].[LiqProviderStream] lpB with (nolock) on md.[CounterpartyId] = lpB.LiqProviderStreamId
	WHERE
		IntegratorReceivedTimeUtc BETWEEN DATEADD(second, -30, @AnalysisFirstIntervalStartTimeUtc) AND @AnalysisFirstIntervalStartTimeUtc
		AND fxpairid  = @FxPairId
		AND RecordTypeId IN (0, 3)
		AND CounterpartyId = @ReferenceCounterpartyId
		AND SideId = 0
	ORDER BY IntegratorReceivedTimeUtc DESC)
	
	SET @FirstAskTob =
	(SELECT TOP 1
		md.Price AS LastToBPrice
	FROM
		[dbo].[MarketData] md with (nolock) 
		INNER JOIN [dbo].[LiqProviderStream] lpB with (nolock) on md.[CounterpartyId] = lpB.LiqProviderStreamId
	WHERE
		IntegratorReceivedTimeUtc BETWEEN DATEADD(second, -30, @AnalysisFirstIntervalStartTimeUtc) AND @AnalysisFirstIntervalStartTimeUtc
		AND fxpairid  = @FxPairId
		AND RecordTypeId IN (0, 3)
		AND CounterpartyId = @ReferenceCounterpartyId
		AND SideId = 1
	ORDER BY IntegratorReceivedTimeUtc DESC)
	
	--fast track if there is no valid price before the interval
	IF @FirstBidTob IS NULL AND @FirstAskTob IS NULL
	BEGIN
		RETURN;
	END
	
	IF NOT EXISTS(
		SELECT 1 
		FROM 
			[dbo].[MarketData] md with (nolock) 
			INNER JOIN [dbo].[LiqProviderStream] lpB with (nolock) on md.[CounterpartyId] = lpB.LiqProviderStreamId
		WHERE
			IntegratorReceivedTimeUtc BETWEEN @AnalysisEndTimeUtc AND DATEADD(second, 30, @AnalysisEndTimeUtc)
			AND fxpairid  = @FxPairId
			AND RecordTypeId IN (0, 3)
			AND CounterpartyId = @ReferenceCounterpartyId
			AND SideId = @PriceSideId
		)
	BEGIN
		RETURN;
	END
	
	
	INSERT INTO [dbo].[TradeDecayAnalysisDataPoint]
           ([TradeDecayAnalysisId]
           ,[MillisecondDistanceFromAnalysisStart]
           ,[ReferenceMidPrice]
		   ,[ReferenceBidPrice]
		   ,[IsReferenceBidAuthenticValue]
		   ,[ReferenceAskPrice]
		   ,[IsReferenceAskAuthenticValue]
           ,[BpMtmToReferenceMidPrice]
		   ,[BpMtmToReferenceCrossPrice]
		   ,[BpMtmToReferenceJoinPrice]
		   )
	SELECT	
		@TradeDecayAnalysisId,
		MillisecondDistance,
		-- as we do not have ANY()
		AVG(LastSidedToB) AS MidToB,
		-- as we do not have ANY() - all 4 followings
		MAX(LastBid) AS Bid,
		MAX(IsLastBidAuthentic) AS IsBidAuthentic,
		MAX(LastAsk) AS Ask,
		MAX(IsLastAskAuthentic) AS IsAskAuthentic,
		CASE WHEN @PriceSideId = 0 THEN (@ReferencePrice - AVG(LastSidedToB)) ELSE (AVG(LastSidedToB) - @ReferencePrice) END / @ReferencePrice * 10000.0 AS MtMFromMid,
		CASE WHEN @PriceSideId = 0 THEN (@ReferencePrice - MAX(LastAsk)) ELSE (MAX(LastBid) - @ReferencePrice) END / @ReferencePrice * 10000.0 AS MtMFromCrossSpread,
		CASE WHEN @PriceSideId = 0 THEN (@ReferencePrice - MAX(LastBid)) ELSE (MAX(LastAsk) - @ReferencePrice) END / @ReferencePrice * 10000.0 AS MtMFromSameSide
	FROM	
		(

		SELECT
			MillisecondDistance,
			SideId,
			LastSidedToB,
			CASE WHEN SideId = 0 THEN LastSidedToB ELSE 0 END AS LastBid,
			CASE WHEN SideId = 0 THEN IsAuthenticValue ELSE 0 END AS IsLastBidAuthentic,
			CASE WHEN SideId = 1 THEN LastSidedToB ELSE 0 END AS LastAsk,
			CASE WHEN SideId = 1 THEN IsAuthenticValue ELSE 0 END AS IsLastAskAuthentic
		FROM
		(
			SELECT
				MillisecondDistance,
				SideId,
				--for lack of ANY()
				MAX(LastToBPrice) OVER (PARTITION BY NonNullToBsNum) AS LastSidedToB,
				CASE WHEN LastToBPrice IS NULL THEN 0 ELSE 1 END AS IsAuthenticValue
			FROM
				(
				SELECT
					nums.MsDistanceFromEventStart AS MillisecondDistance,
					nums.SideId AS SideId,
					LastToBPrice, -- as we do not have any - all are same
					-- this will count number of non-null rows - for nulls (no ToB in current millisecond) it will repeat previous val (so we can partition by this one level up)
					COUNT(LastToBPrice) OVER (ORDER BY nums.SideId, nums.MsDistanceFromEventStart ASC) as NonNullToBsNum
				FROM
				[dbo].[_Internal_TradeDecayIntervals] nums
				LEFT JOIN
				(
					SELECT
						0 AS BucketizedMillisecondsFromIntervalStart,
						0 AS SideId,
						@FirstBidTob AS LastToBPrice
					UNION ALL
					SELECT
						0 AS BucketizedMillisecondsFromIntervalStart,
						1 AS SideId,
						@FirstAskTob AS LastToBPrice	
					UNION ALL
					SELECT
						BucketizedMillisecondsFromIntervalStart,
						SideId,
						MAX(LastToBPrice) AS LastToBPrice -- as we do not have ANY() - all are same
					FROM
					(
						SELECT 
							-- millisecond difference from the beginig
							[dbo].[GetTradeDecayTimeIntervalBucket](DateDiff(mcs, @AnalysisStartTimeUtc, md.integratorReceivedTimeUtc)/1000) AS BucketizedMillisecondsFromIntervalStart,
							SideId,
							-- last price in current millisecond interval = ToB in current millisecond
							LAST_VALUE(md.Price) 
								OVER(
									PARTITION BY [dbo].[GetTradeDecayTimeIntervalBucket](DateDiff(mcs, @AnalysisStartTimeUtc, md.integratorReceivedTimeUtc)/1000), SideId
									ORDER BY md.integratorReceivedTimeUtc ASC 
									ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) AS LastToBPrice
						FROM
							[dbo].[MarketData] md with (nolock) 
							INNER JOIN [dbo].[LiqProviderStream] lpB with (nolock) on md.[CounterpartyId] = lpB.LiqProviderStreamId
						WHERE
							IntegratorReceivedTimeUtc BETWEEN @AnalysisFirstIntervalStartTimeUtc AND @AnalysisEndTimeUtc
							AND fxpairid  = @FxPairId
							AND RecordTypeId IN (0, 3)
							AND CounterpartyId = @ReferenceCounterpartyId
							--AND SideId = @PriceSideId
					) tmp
					GROUP BY BucketizedMillisecondsFromIntervalStart, SideId
				) tmp2 ON nums.MsDistanceFromEventStart = tmp2.BucketizedMillisecondsFromIntervalStart AND nums.SideId = tmp2.SideId
				) tmp3
			) tmp4
		)tmp5	
	GROUP BY MillisecondDistance
	ORDER BY MillisecondDistance ASC

	
	UPDATE [dbo].[TradeDecayAnalysis]
	SET 
      [HasValidTickAfterIntervalEnd] = 1
      ,[IsValidAndComplete] = 1
	WHERE 
		[TradeDecayAnalysisId] = @TradeDecayAnalysisId
	
END
GO