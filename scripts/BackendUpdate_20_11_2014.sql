
--Replace here with refactored ctp id (DBK, HSB, MGS, RBS, SOC, UBS)
DECLARE @CTPID CHAR(3) = 'DBK'
--
DECLARE @CTPSEARCHPATTERN CHAR(7) = @CTPID + '[_]%'

UPDATE
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], '[SESSION]', '[SESSION]
BypassParsing=Y
UseDataDictionary=N')
WHERE
	[Name] like @CTPSEARCHPATTERN
	AND [Configuration] NOT like '%BypassParsing%'
	
	
UPDATE
	[dbo].[Settings_FIX]
SET
	[Configuration] = REPLACE([Configuration], 'DataDictionary=F', '#DataDictionary=F')
WHERE
	[Name] like @CTPSEARCHPATTERN
	AND [Configuration] NOT like '%#DataDictionary=F%'