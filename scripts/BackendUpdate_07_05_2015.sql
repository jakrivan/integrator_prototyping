DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_DBK" type="FIXChannel_DBKSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM2" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM3" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC1" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC2" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS1" type="FIXChannel_FCMMMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS2" type="FIXChannel_FCMMMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
      <xs:element minOccurs="1" maxOccurs="1" name="STP" type="STPSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_DBKSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DealConfirmationSupported" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumDealConfirmationDelay_Milliseconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="L01LayersNum" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IocApplyTtl" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ApplyMaxHoldTime" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxHoldTime" type="FXCMMaxHoldTime" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MaxPerSymbolPricesPerSecondSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultMaxAllowedPerSecondRate" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialSymbolRateSettings" type="ArrayOfSymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolRateSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="SymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolRateSetting">
    <xs:attribute name="Name" type="xs:string" />
    <xs:attribute name="MaxAllowedPerSecondRate" type="xs:int" use="required" />
  </xs:complexType>
  <xs:complexType name="RejectionRateDisconnectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumOrdersToApplyCheck" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAllowedRejectionRatePercent" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMMMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxOrderHoldTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxTotalPricesPerSecond" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="MaxPerSymbolPricesPerSecondSetting" type="MaxPerSymbolPricesPerSecondSetting" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExpectedPricingDepth" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="RejectionRateDisconnectSettings" type="RejectionRateDisconnectSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXSTPChannel_CTIPBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNongenuineInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialRejectionsHandling" type="ArrayOfSpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SpecialRejection" type="SpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="TriggerPhrase" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="FXCMMaxHoldTime">
    <xs:restriction base="xs:string">
      <xs:enumeration value="UpTo1ms" />
      <xs:enumeration value="UpTo30ms" />
      <xs:enumeration value="UpTo100ms" />
      <xs:enumeration value="UpTo500ms" />
      <xs:enumeration value="UpTo3000ms" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="MarketSessionIntoSeparateProcess" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="STPDestinations" type="ArrayOfSTPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSTPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="STPDestination" nillable="true" type="STPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Name" type="STPCounterparty" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedTicketTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="ServedCounterparties" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="STPCounterparty">
    <xs:restriction base="xs:string">
      <xs:enumeration value="CTIPB" />
      <xs:enumeration value="Traiana" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

UPDATE [dbo].[Settings]
SET [SettingsXml] = REPLACE(CAST([SettingsXml] AS NVARCHAR(MAX)), '<FIXChannel_LM1>', '<FIXChannel_LM1><L01LayersNum>4</L01LayersNum>')
WHERE
	[Name] = 'Kreslik.Integrator.SessionManagement.dll'
	AND CAST([SettingsXml] AS NVARCHAR(MAX)) like '%<FIXChannel_LM1>%'
	
	
	
CREATE TABLE [dbo].[PrimeBroker](
	[PrimeBrokerId] [tinyint] NOT NULL,
	[PrimeBrokerName] [varchar](10) NOT NULL
) ON [PRIMARY]
GO	

CREATE UNIQUE CLUSTERED INDEX [PK_PrimeBroker] ON [dbo].[PrimeBroker]
(
	[PrimeBrokerId] ASC
) ON [PRIMARY]
GO

INSERT INTO [dbo].[PrimeBroker] ([PrimeBrokerId], [PrimeBrokerName]) VALUES (1, 'SaxoBank')
INSERT INTO [dbo].[PrimeBroker] ([PrimeBrokerId], [PrimeBrokerName]) VALUES (2, 'CitiBank')
INSERT INTO [dbo].[PrimeBroker] ([PrimeBrokerId], [PrimeBrokerName]) VALUES (3, 'RaboBank')
GO

ALTER TABLE [dbo].[CommissionsPerCounterparty] DROP COLUMN [CommissionPricePerMillion]
GO

EXEC sp_rename '[dbo].[CommissionsPerCounterparty].[DestinationCommissionPpm]', 'CounterpartyCommissionPpm', 'COLUMN'
EXEC sp_rename '[dbo].[CommissionsPerCounterparty].[SaxoPbCommissionPpm]', 'PbCommissionPpm', 'COLUMN'
GO

ALTER TABLE [dbo].[CommissionsPerCounterparty] ADD [CommissionPricePerMillion] AS ([CounterpartyCommissionPpm]+[PbCommissionPpm])
ALTER TABLE [dbo].[CommissionsPerCounterparty] ADD [PrimeBrokerId] [tinyint] NULL
GO

UPDATE [dbo].[CommissionsPerCounterparty] SET PrimeBrokerId = 1

ALTER TABLE [dbo].[CommissionsPerCounterparty] ALTER COLUMN [PrimeBrokerId] [tinyint] NOT NULL
GO

ALTER TABLE [dbo].[CommissionsPerCounterparty]  WITH CHECK ADD  CONSTRAINT [FK_CommissionsPerCounterparty_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[CommissionsPerCounterparty] CHECK CONSTRAINT [FK_CommissionsPerCounterparty_Counterparty]
GO

ALTER TABLE [dbo].[CommissionsPerCounterparty]  WITH CHECK ADD  CONSTRAINT [FK_CommissionsPerCounterparty_PrimeBroker] FOREIGN KEY([PrimeBrokerId])
REFERENCES [dbo].[PrimeBroker] ([PrimeBrokerId])
GO
ALTER TABLE [dbo].[CommissionsPerCounterparty] CHECK CONSTRAINT [FK_CommissionsPerCounterparty_PrimeBroker]
GO


CREATE TABLE [dbo].[CommissionsSchedule_PBCommission](
	[PrimeBrokerId] [tinyint] NOT NULL,
	[PbCommissionPpm] [decimal](18, 6) NOT NULL,
	[ValidFromUtc] [date] NOT NULL,
	[ValidToUtc] [date] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CommissionsSchedule_PBCommission]  WITH CHECK ADD  CONSTRAINT [FK_CommissionsSchedule_PrimeBroker] FOREIGN KEY([PrimeBrokerId])
REFERENCES [dbo].[PrimeBroker] ([PrimeBrokerId])
GO
ALTER TABLE [dbo].[CommissionsSchedule_PBCommission] CHECK CONSTRAINT [FK_CommissionsSchedule_PrimeBroker]
GO

ALTER TABLE CommissionsSchedule_PBCommission
ADD CONSTRAINT CK_CommissionsSchedule_PBCommissionRangeValid
CHECK (ValidFromUtc <= ValidToUtc)
GO

CREATE CLUSTERED INDEX [IX_PBCommision_PbStartDate] ON [dbo].[CommissionsSchedule_PBCommission]
(
	[PrimeBrokerId] ASC,
	[ValidFromUtc] ASC
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[CommissionsSchedule_CptCommission](
	[CounterpartyId] [tinyint] NOT NULL,
	[CounterpartyCommissionPpm] [decimal](18, 6) NOT NULL,
	[ValidFromUtc] [date] NOT NULL,
	[ValidToUtc] [date] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CommissionsSchedule_CptCommission]  WITH CHECK ADD  CONSTRAINT [FK_CommissionsSchedule_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[CommissionsSchedule_CptCommission] CHECK CONSTRAINT [FK_CommissionsSchedule_Counterparty]
GO

ALTER TABLE CommissionsSchedule_CptCommission
ADD CONSTRAINT CK_CommissionsSchedule_CptCommissionRangeValid
CHECK (ValidFromUtc <= ValidToUtc)
GO

CREATE CLUSTERED INDEX [IX_CptCommision_CptStartDate] ON [dbo].[CommissionsSchedule_CptCommission]
(
	[CounterpartyId] ASC,
	[ValidFromUtc] ASC
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[CommissionsSchedule_Mapping](
	[CounterpartyId] [tinyint] NOT NULL,
	[PrimeBrokerId] [tinyint] NOT NULL,
	[ValidFromUtc] [date] NOT NULL,
	[ValidToUtc] [date] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CommissionsSchedule_Mapping]  WITH CHECK ADD  CONSTRAINT [FK_CommissionsScheduleMapping_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[CommissionsSchedule_Mapping] CHECK CONSTRAINT [FK_CommissionsScheduleMapping_Counterparty]
GO

ALTER TABLE CommissionsSchedule_Mapping
ADD CONSTRAINT CK_CommissionsSchedule_MappingRangeValid
CHECK (ValidFromUtc <= ValidToUtc)
GO

CREATE CLUSTERED INDEX [IX_CommisionMapping_CptStartDate] ON [dbo].[CommissionsSchedule_Mapping]
(
	[CounterpartyId] ASC,
	[ValidFromUtc] ASC
) ON [PRIMARY]
GO


INSERT INTO [dbo].[CommissionsSchedule_PBCommission] 
	([PrimeBrokerId], [PbCommissionPpm], [ValidFromUtc], [ValidToUtc])
SELECT
	DISTINCT [PrimeBrokerId], [PbCommissionPpm], '1900-01-01', '9999-12-31'
FROM
	[dbo].[CommissionsPerCounterparty]
	
	
INSERT INTO [dbo].[CommissionsSchedule_CptCommission] 
	([CounterpartyId], [CounterpartyCommissionPpm], [ValidFromUtc], [ValidToUtc])
SELECT
	[CounterpartyId], [CounterpartyCommissionPpm], '1900-01-01', '9999-12-31'
FROM
	[dbo].[CommissionsPerCounterparty]
	
	
INSERT INTO [dbo].[CommissionsSchedule_Mapping] 
	([PrimeBrokerId], [CounterpartyId], [ValidFromUtc], [ValidToUtc])
SELECT
	[PrimeBrokerId], [CounterpartyId], '1900-01-01', '9999-12-31'
FROM
	[dbo].[CommissionsPerCounterparty]



CREATE PROCEDURE [dbo].[NewCommissionScheduleHandling_SP]
AS
BEGIN
	
	--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
	SET NOCOUNT ON
	
	IF EXISTS(
		SELECT 1

		FROM
			(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY [PrimeBrokerId], [ValidFromUtc]) AS Schedule1RowId,
				[PrimeBrokerId]
				,[ValidFromUtc]
				--,[ValidToUtc]
			FROM 
				[dbo].[CommissionsSchedule_PBCommission]
			) schedule1

			INNER JOIN

			(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY [PrimeBrokerId], [ValidFromUtc]) AS Schedule2RowId,
				[PrimeBrokerId]
				,[ValidFromUtc]
				,[ValidToUtc]
			FROM 
				[dbo].[CommissionsSchedule_PBCommission]
			) schedule2
			ON schedule1.PrimeBrokerId = schedule2.PrimeBrokerId AND schedule1.Schedule1RowId != schedule2.Schedule2RowId

		WHERE
			schedule1.[ValidFromUtc] >= schedule2.[ValidFromUtc] AND schedule1.[ValidFromUtc] <= schedule2.[ValidToUtc]
		)
	BEGIN
		RAISERROR (N'Multiple currently valid records for single Counterparty', 16, 2 ) WITH SETERROR
		ROLLBACK TRAN
	END
	
	IF EXISTS(
		SELECT 1

		FROM
			(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY [CounterpartyId], [ValidFromUtc]) AS Schedule1RowId,
				[CounterpartyId]
				,[ValidFromUtc]
				--,[ValidToUtc]
			FROM 
				[dbo].[CommissionsSchedule_CptCommission]
			) schedule1

			INNER JOIN

			(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY [CounterpartyId], [ValidFromUtc]) AS Schedule2RowId,
				[CounterpartyId]
				,[ValidFromUtc]
				,[ValidToUtc]
			FROM 
				[dbo].[CommissionsSchedule_CptCommission]
			) schedule2
			ON schedule1.CounterpartyId = schedule2.CounterpartyId AND schedule1.Schedule1RowId != schedule2.Schedule2RowId

		WHERE
			schedule1.[ValidFromUtc] >= schedule2.[ValidFromUtc] AND schedule1.[ValidFromUtc] <= schedule2.[ValidToUtc]
		)
	BEGIN
		RAISERROR (N'Multiple currently valid records for single Counterparty', 16, 2 ) WITH SETERROR
		ROLLBACK TRAN
	END
	
	IF EXISTS(
		SELECT 1

		FROM
			(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY [CounterpartyId], [PrimeBrokerId], [ValidFromUtc]) AS Schedule1RowId,
				[PrimeBrokerId],
				[CounterpartyId]
				,[ValidFromUtc]
				--,[ValidToUtc]
			FROM 
				[dbo].[CommissionsSchedule_Mapping]
			) schedule1

			INNER JOIN

			(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY [CounterpartyId], [PrimeBrokerId], [ValidFromUtc]) AS Schedule2RowId,
				[PrimeBrokerId],
				[CounterpartyId]
				,[ValidFromUtc]
				,[ValidToUtc]
			FROM 
				[dbo].[CommissionsSchedule_Mapping]
			) schedule2
			ON 
				schedule1.CounterpartyId = schedule2.CounterpartyId 
				AND schedule1.Schedule1RowId != schedule2.Schedule2RowId

		WHERE
			schedule1.[ValidFromUtc] >= schedule2.[ValidFromUtc] AND schedule1.[ValidFromUtc] <= schedule2.[ValidToUtc]
		)
	BEGIN
		RAISERROR (N'Multiple currently valid records for single Counterparty', 16, 2 ) WITH SETERROR
		ROLLBACK TRAN
	END
	
	DECLARE @TodayUtc DATE = GETUTCDATE()

	
	TRUNCATE TABLE [dbo].[CommissionsPerCounterparty] 
	
	INSERT INTO [dbo].[CommissionsPerCounterparty] 
		([PrimeBrokerId], [CounterpartyId], [CounterpartyCommissionPpm], [PbCommissionPpm])
	SELECT 
		map.[PrimeBrokerId],
		map.[CounterpartyId],
		cpt.[CounterpartyCommissionPpm],
		pb.[PbCommissionPpm]
	FROM 
		[dbo].[CommissionsSchedule_Mapping] map
		INNER JOIN [dbo].[CommissionsSchedule_CptCommission] cpt ON map.[CounterpartyId] = cpt.CounterpartyId
		INNER JOIN [dbo].[CommissionsSchedule_PBCommission] pb ON map.[PrimeBrokerId] = pb.[PrimeBrokerId]
	WHERE
		map.[ValidFromUtc] <= @TodayUtc AND map.[ValidToUtc] >= @TodayUtc
		AND cpt.[ValidFromUtc] <= @TodayUtc AND cpt.[ValidToUtc] >= @TodayUtc
		AND pb.[ValidFromUtc] <= @TodayUtc AND pb.[ValidToUtc] >= @TodayUtc
END
GO

CREATE TRIGGER [dbo].[NewCommissionScheduleInserted_Map] ON [dbo].[CommissionsSchedule_Mapping] AFTER INSERT, UPDATE
AS
BEGIN
	--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
	SET NOCOUNT ON
	exec [dbo].[NewCommissionScheduleHandling_SP]
END
GO

CREATE TRIGGER [dbo].[NewCommissionScheduleInserted_Pb] ON [dbo].[CommissionsSchedule_PBCommission] AFTER INSERT, UPDATE
AS
BEGIN
	--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
	SET NOCOUNT ON
	exec [dbo].[NewCommissionScheduleHandling_SP]
END
GO

CREATE TRIGGER [dbo].[NewCommissionScheduleInserted_Cpt] ON [dbo].[CommissionsSchedule_CptCommission] AFTER INSERT, UPDATE
AS
BEGIN
	--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
	SET NOCOUNT ON
	exec [dbo].[NewCommissionScheduleHandling_SP]
END
GO


CREATE FUNCTION [dbo].[GetPbId](@counterpartyId tinyint, @date [date])
RETURNS [tinyint]
AS 
BEGIN
	DECLARE @pbId [tinyint]
	
	SELECT 
		@pbId = map.[PrimeBrokerId]
	FROM 
		[dbo].[CommissionsSchedule_Mapping] map
	WHERE
		map.[ValidFromUtc] <= @date AND map.[ValidToUtc] >= @date
		AND map.[CounterpartyId] = @counterpartyId

	RETURN @pbId
END
GO

ALTER TABLE [dbo].[DealExternalExecuted] ADD [PrimeBrokerId] AS [dbo].[GetPbId]([CounterpartyId], CONVERT([date],[IntegratorReceivedExecutionReportUtc]))
ALTER TABLE [dbo].[DealExternalRejected] ADD [PrimeBrokerId] AS [dbo].[GetPbId]([CounterpartyId], CONVERT([date],[IntegratorReceivedExecutionReportUtc]))

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] ADD [PrimeBrokerId] AS [dbo].[GetPbId]([CounterpartyId], CONVERT([date],[IntegratorReceivedExternalOrderUtc]))
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] ADD [PrimeBrokerId] AS [dbo].[GetPbId]([CounterpartyId], CONVERT([date],[IntegratorReceivedExternalOrderUtc]))
GO
	
	
ALTER TABLE [dbo].[DealExternalExecuted] ADD [CounterpartyClientIdPostTrade] varchar(20) NULL
ALTER TABLE [dbo].[DealExternalRejected] ADD [CounterpartyClientIdPostTrade] varchar(20) NULL
GO

--Commissions--

ALTER PROCEDURE [dbo].[GetTradingSystemsDailyStats_SP]
AS
BEGIN

DECLARE @KGTRejectionDirectionId BIT
SELECT @KGTRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'
DECLARE @CtpRejectionDirectionId BIT
SELECT @CtpRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'CtpRejectd'
DECLARE @MinDate DATE = GETUTCDATE()

SELECT
		--BEWARE! There can be multiple symbols for same system id (this is especially true for system -1)
		systemStats.SymbolId AS SymbolId,
		systemStats.TradingSystemId AS TradingSystemId,
		COALESCE(systemStats.NopBasePol, 0) AS NopBasePol,
		COALESCE(systemStats.VolumeInUsd/1000000, 0) AS VolumeUsdM,
		COALESCE(systemStats.DealsNum, 0) as DealsNum,
		systemStats.LastDealUtc as LastDealUtc,
		COALESCE(systemStats.KGTRejectionsNum, 0) as KGTRejectionsNum,
		systemStats.LastKgtRejectionUtc as LastKgtRejectionUtc,
		COALESCE(systemStats.CtpRejectionsNum, 0) as CtpRejectionsNum,
		systemStats.LastCptRejectionUtc as LastCptRejectionUtc,
		COALESCE(systemStats.PnlTermPolInUsd/(systemStats.VolumeInUsd/1000000), 0) AS PnlGrossUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd, 0) AS PnlGrossUsd,
		COALESCE(systemStats.CommissionPBInUSD/(systemStats.VolumeInUsd/1000000), 0) AS CommissionsPbUsdPerMUsd,
		COALESCE(systemStats.CommissionPBInUSD, 0) AS CommissionsPbUsd,
		COALESCE(systemStats.CommissionCptInUSD/(systemStats.VolumeInUsd/1000000), 0) AS CommissionsCptUsdPerMUsd,
		COALESCE(systemStats.CommissionCptInUSD, 0) AS CommissionsCptUsd,
		COALESCE(systemStats.CommissionSUMInUSD/(systemStats.VolumeInUsd/1000000), 0) AS CommissionsSumUsdPerMUsd,
		COALESCE(systemStats.CommissionSUMInUSD, 0) AS CommissionsSumUsd,
		COALESCE((systemStats.PnlTermPolInUsd-systemStats.CommissionSUMInUSD)/(systemStats.VolumeInUsd/1000000), 0) AS PnlNetUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd-systemStats.CommissionSUMInUSD, 0) AS PnlNetUsd
	FROM
		(
		SELECT
			Symbol.Id AS SymbolId,
			systemsStats.TradingSystemId AS TradingSystemId,
			systemsStats.NopBasePol AS NopBasePol,
			systemsStats.DealsNum as DealsNum,
			systemsStats.LastDealUtc as LastDealUtc,
			systemsStats.KGTRejectionsNum as KGTRejectionsNum,
			systemsStats.LastKgtRejectionUtc as LastKgtRejectionUtc,
			systemsStats.CtpRejectionsNum as CtpRejectionsNum,
			systemsStats.LastCptRejectionUtc as LastCptRejectionUtc,
			systemsStats.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd,
			--PnL = term position + yield of immediate close of outstanding base position
			--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
			(systemsStats.NopTermPol + (CASE WHEN systemsStats.NopBasePol < 0 THEN Symbol.LastKnownAskMeanPrice ELSE Symbol.LastKnownBidMeanPrice END) * systemsStats.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
			systemsStats.CommissionPBInCCY1 * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionPBInUSD,
			systemsStats.CommissionCptInCCY1 * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionCptInUSD,
			(systemsStats.CommissionPBInCCY1 + systemsStats.CommissionCptInCCY1) * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionSUMInUSD
		FROM
			(
			SELECT
				COALESCE(systemsPositions.[TradingSystemId], systemRejections.[TradingSystemId]) AS [TradingSystemId],
				COALESCE(systemsPositions.[SymbolId], systemRejections.[SymbolId]) AS [SymbolId],
				NopBasePol,
				NopTermPol,
				VolumeInCcy1,
				CommissionPBInCCY1,
				CommissionCptInCCY1,
				DealsNum,
				LastDealUtc,
				KGTRejectionsNum,
				LastKgtRejectionUtc,
				CtpRejectionsNum,
				LastCptRejectionUtc
			FROM	
				(
				--allways include stats fro unknown systems
				SELECT 
					-1 AS [TradingSystemId], 
					0 AS [SymbolId],
					NULL AS NopBasePol, 
					NULL AS NopTermPol, 
					NULL AS VolumeInCcy1, 
					NULL AS CommissionPBInCCY1, 
					NULL AS CommissionCptInCCY1, 
					NULL AS DealsNum,
					NULL AS LastDealUtc
				UNION ALL
				SELECT 
					[TradingSystemId],
					[SymbolId],
					SUM([AmountBasePolExecuted]) AS NopBasePol,
					SUM(-[AmountBasePolExecuted]* [Price]) AS NopTermPol,
					SUM(ABS([AmountBasePolExecuted])) AS VolumeInCcy1,
					SUM(ABS([AmountBasePolExecuted]) * PbCommissionPpm / 1000000) AS CommissionPBInCCY1,
					SUM(ABS([AmountBasePolExecuted]) * CounterpartyCommissionPpm / 1000000) AS CommissionCptInCCY1,
					COUNT([TradingSystemId]) AS DealsNum,
					MAX([DealTimeUtc]) AS LastDealUtc
				FROM
					[dbo].[TradingSystemDealsExecuted_Daily] deal
					INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
				GROUP BY
					[TradingSystemId], [SymbolId]
				) systemsPositions
				FULL OUTER JOIN
				(
					SELECT 
						[TradingSystemId],
						[SymbolId],
						--COUNT([TradingSystemId]) AS RejectionsNum
						SUM(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN 1 ELSE 0 END) AS KGTRejectionsNum, 
						MAX(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN [RejectionTimeUtc] ELSE @MinDate END) AS LastKgtRejectionUtc, 
						SUM(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN 1 ELSE 0 END) AS CtpRejectionsNum,
						MAX(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN [RejectionTimeUtc] ELSE @MinDate END) AS LastCptRejectionUtc 
					FROM
						[dbo].[TradingSystemDealsRejected_Daily] rejects
					GROUP BY
						 [TradingSystemId], [SymbolId]
				)  systemRejections ON systemsPositions.TradingSystemId = systemRejections.TradingSystemId AND systemsPositions.[SymbolId] = systemRejections.[SymbolId]
			) systemsStats
			INNER JOIN [dbo].[Symbol] symbol ON systemsStats.SymbolId = symbol.Id
			INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
			INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
			) systemStats
END
GO

ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ADD [CommissionsPbUsd] [decimal](18,2) NULL
ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ADD [CommissionsPbUsdPerMUsd] [decimal](18,2) NULL
ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ADD [CommissionsCptUsd] [decimal](18,2) NULL
ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ADD [CommissionsCptUsdPerMUsd] [decimal](18,2) NULL
GO

ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	DECLARE @TempTradingSystemStatistics TABLE
	(
		[SymbolId] [tinyint] NOT NULL,
		[TradingSystemId] [int] NOT NULL,
		[NopBasePol] [decimal](18,2) NOT NULL,
		[VolumeUsdM] [decimal](18,6) NOT NULL,
		[DealsNum] [int]  NOT NULL,
		[LastDealUtc] [datetime2](7) NULL,
		[KGTRejectionsNum] [int]  NOT NULL,
		[LastKgtRejectionUtc] [datetime2](7) NULL,
		[CtpRejectionsNum] [int]  NOT NULL,
		[LastCptRejectionUtc] [datetime2](7) NULL,
		[PnlGrossUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlGrossUsd] [decimal](18,6) NOT NULL,
		[CommissionsPbUsdPerMUsd] [decimal](18,2) NOT NULL,
		[CommissionsPbUsd] [decimal](18,2) NOT NULL,
		[CommissionsCptUsdPerMUsd] [decimal](18,2) NOT NULL,
		[CommissionsCptUsd] [decimal](18,2) NOT NULL,
		[CommissionsUsdPerMUsd] [decimal](18,2) NOT NULL,
		[CommissionsUsd] [decimal](18,2) NOT NULL,
		[PnlNetUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsd] [decimal](18,6) NOT NULL
	)

	INSERT INTO 
		@TempTradingSystemStatistics
		([SymbolId]
		,[TradingSystemId]
		,[NopBasePol]
		,[VolumeUsdM]
		,[DealsNum]
		,[LastDealUtc]
		,[KGTRejectionsNum]
		,[LastKgtRejectionUtc]
		,[CtpRejectionsNum]
		,[LastCptRejectionUtc]
		,[PnlGrossUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsPbUsdPerMUsd]
		,[CommissionsPbUsd]
		,[CommissionsCptUsdPerMUsd]
		,[CommissionsCptUsd]
		,[CommissionsUsdPerMUsd]
		,[CommissionsUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlNetUsd])
    exec [dbo].[GetTradingSystemsDailyStats_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingSystemDaily]
			([TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsPbUsdPerMUsd]
			,[CommissionsPbUsd]
			,[CommissionsCptUsdPerMUsd]
			,[CommissionsCptUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd])
		SELECT
			[TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsPbUsdPerMUsd]
			,[CommissionsPbUsd]
			,[CommissionsCptUsdPerMUsd]
			,[CommissionsCptUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd]
		FROM
			@TempTradingSystemStatistics
	COMMIT
END
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsOveralDailyStats_SP]
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()

	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]

	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn	
	
	SELECT 
		ISNULL(counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName], 'Unknown') AS SystemName,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		--COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		--this should be called active - and upper stay enabled
		CASE WHEN @TradingOn = 1 
			THEN 
			COALESCE(SUM(CASE WHEN action.[Enabled] = 1 AND action.[GoFlatOnly] = 0 AND action.[OrderTransmissionDisabled] = 0 THEN 1 ELSE 0 END), 0)
			ELSE 
			0 
			END AS EnabledSystems,
		COALESCE(SUM(CONVERT(int, action.[HardBlocked])), 0) AS HardBlockedSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		MAX(stats.[LastDealUtc]) AS LastDealUtc,
		SUM(stats.[KGTRejectionsNum]) AS KGTRejectionsNum,
		MAX(stats.[LastKgtRejectionUtc]) AS LastKgtRejectionUtc,
		SUM(stats.[CtpRejectionsNum]) AS CtpRejectionsNum,
		MAX(stats.[LastCptRejectionUtc]) AS LastCptRejectionUtc,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsPbUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsPbUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsCptUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsCptUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsPbUsd]) AS CommissionsPbUsd,
		SUM(stats.[CommissionsCptUsd]) AS CommissionsCptUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemType] systemtype_forJoinOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = systemtype_forJoinOnly.[TradingSystemTypeId]
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystemType] systemtype ON systemtype.[TradingSystemTypeId] = COALESCE(trsystem.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId], systemtype_forJoinOnly.[TradingSystemTypeId])
		LEFT JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	GROUP BY
		counterparty.[CounterpartyCode], systemtype.[SystemName]
	ORDER BY
		SystemName
END
GO

CREATE PROCEDURE [dbo].[TradingSystemsGroupResetMulti_SP]
(
	@SystemsGroupId [int],
	@SystemTypeId [int] = NULL
)
AS
BEGIN
	UPDATE 
		action
	SET
		[LastResetRequestedUtc] = GETUTCDATE(),
		[HardBlocked] = 0
	FROM
		[dbo].[TradingSystemActions] action
		INNER JOIN [dbo].[TradingSystem]  trsystem ON action.[TradingSystemId] = trsystem.[TradingSystemId]
	WHERE 
		trsystem.[TradingSystemGroupId] = @SystemsGroupId
		AND (@SystemTypeId IS NULL OR trsystem.[TradingSystemTypeId] = @SystemTypeId)
		AND [Enabled] = 0
END
GO
GRANT EXECUTE ON [dbo].[TradingSystemsGroupResetMulti_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


ALTER PROCEDURE [dbo].[GetStatisticsPerCounterparty_Daily_SP] (
	@Day DATE
	)
AS
BEGIN

	DECLARE @KGTRejectionDirectionId BIT
	SELECT @KGTRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'
	DECLARE @CtpRejectionDirectionId BIT
	SELECT @CtpRejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'CtpRejectd'
	DECLARE @MinDate DATE = GETUTCDATE()	
		
	DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		KGTRejectionsNum int,
		LastKgtRejectionUtc datetime2(7),
		CtpRejectionsNum int,
		LastCptRejectionUtc datetime2(7),
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int
	)

	INSERT INTO @temp_rejections(CounterpartyId, KGTRejectionsNum, LastKgtRejectionUtc, CtpRejectionsNum, LastCptRejectionUtc, IntegratorLatencyNanoseconds, IntegratorLatencyCases)
	SELECT
		[CounterpartyId],
		SUM(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN 1 ELSE 0 END) AS KGTRejections,
		MAX(CASE WHEN [RejectionDirectionId] = @KGTRejectionDirectionId THEN [IntegratorReceivedExecutionReportUtc] ELSE @MinDate END) AS LastKgtRejectionUtc,
		SUM(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN 1 ELSE 0 END) AS CtpRejections,
		MAX(CASE WHEN [RejectionDirectionId] = @CtpRejectionDirectionId THEN [IntegratorReceivedExecutionReportUtc] ELSE @MinDate END) AS LastCptRejectionUtc,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
	FROM
		[dbo].[DealExternalRejected_DailyView] ctpRej WITH(NOEXPAND, NOLOCK)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]

	UNION ALL

	SELECT
			[CounterpartyId],
			COUNT([CounterpartyId]) AS KGTRejections,
			MAX([IntegratorReceivedExternalOrderUtc]) AS LastKgtRejectionUtc,
			NULL AS CtpRejections,
			NULL AS LastCptRejectionUtc,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily] kgtRej WITH(NOLOCK)
		WHERE
			[IntegratorRejectedAmountBasePol] != 0
			AND [DatePartIntegratorReceivedOrderUtc] = @Day
			--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
		GROUP BY
			[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		CommissionPBBase decimal(18,2),
		CommissionCptBase decimal(18,2),
		DealsNum int,
		LastDealUtc datetime2(7),
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int,
		CounterpartyLatencyNanoseconds bigint,
		CounterpartyLatencyCases int
	)

	INSERT INTO 
		@temp_NOPs_pol
		(
		SymbolId, 
		CounterpartyId, 
		NopBasePol, 
		NopTermPol, 
		VolumeBaseAbs, 
		CommissionPBBase,
		CommissionCptBase,
		DealsNum, 
		LastDealUtc,
		IntegratorLatencyNanoseconds, 
		IntegratorLatencyCases, 
		CounterpartyLatencyNanoseconds, 
		CounterpartyLatencyCases
		)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		SUM(ABS([AmountBasePolExecuted]) * PbCommissionPpm / 1000000) AS CommissionPBBase,
		SUM(ABS([AmountBasePolExecuted]) * CounterpartyCommissionPpm / 1000000) AS CommissionCptBase,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum,
		MAX(deal.[IntegratorReceivedExecutionReportUtc]) AS LastDealUtc,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases,
		SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
		COUNT(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyCases
	FROM 
		DealExternalExecutedTakersAndMakers_DailyView deal
		INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
		--INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		deal.SymbolId,
		deal.CounterpartyId
		

	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.CommissionPbInUsd AS CommissionPbInUsd,
		DealsStatistics.CommissionCptInUsd AS CommissionCptInUsd,
		DealsStatistics.CommissionPbInUsd + DealsStatistics.CommissionCptInUsd  AS CommissionSumInUsd,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.LastDealUtc AS LastDealUtc,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		CASE WHEN COALESCE(RejectionsStatistics.KGTRejectionsNum,0) = 0 THEN NULL ELSE RejectionsStatistics.KGTRejectionsNum END AS KGTRejectionsNum,
		RejectionsStatistics.LastKgtRejectionUtc AS RejectionsStatistics,
		CASE WHEN COALESCE(RejectionsStatistics.CtpRejectionsNum,0) = 0 THEN NULL ELSE RejectionsStatistics.CtpRejectionsNum END AS CtpRejectionsNum,
		RejectionsStatistics.LastCptRejectionUtc AS LastCptRejectionUtc,
		CASE WHEN COALESCE(RejectionsStatistics.KGTRejectionsNum,0) = 0
		THEN
			 NULL
		ELSE
			RejectionsStatistics.KGTRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.KGTRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0))
		END AS KGTRejectionsRate,
		CASE WHEN COALESCE(RejectionsStatistics.CtpRejectionsNum,0) = 0
		THEN
			NULL
		ELSE
			RejectionsStatistics.CtpRejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.CtpRejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0))
		END AS CtpRejectionsRate,
		--this calculates average of both numbers if both are non-null; or average of the nonull case if just one is not null (as 2*A / 2*B = A/B); or NULL if both are NULL (avoids divide by zero exception)
		(COALESCE(RejectionsStatistics.IntegratorLatencyNanoseconds, DealsStatistics.IntegratorLatencyNanoseconds) + COALESCE(DealsStatistics.IntegratorLatencyNanoseconds, RejectionsStatistics.IntegratorLatencyNanoseconds))
		/
		(COALESCE(RejectionsStatistics.IntegratorLatencyCases, DealsStatistics.IntegratorLatencyCases) + COALESCE(DealsStatistics.IntegratorLatencyCases, RejectionsStatistics.IntegratorLatencyCases))
		AS AvgIntegratorLatencyNanoseconds,
		DealsStatistics.CounterpartyLatencyNanoseconds / DealsStatistics.CounterpartyLatencyCases AS AvgCounterpartyLatencyNanoseconds
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			-- from SHORTs
			SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs,
			--SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(CommissionPbInUsd) AS CommissionPbInUsd,
			SUM(CommissionCptInUsd) AS CommissionCptInUsd,
			SUM(DealsNum) AS DealsNum,
			MAX(LastDealUtc) AS LastDealUtc,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
			SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
			SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(CommissionPbInUsd) AS CommissionPbInUsd,
				SUM(CommissionCptInUsd) AS CommissionCptInUsd,
				SUM(DealsNum) AS DealsNum,
				MAX(LastDealUtc) AS LastDealUtc,
				SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
				SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
				SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
				SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.CommissionPBBase * ccy1.LastKnownUsdConversionMultiplier AS CommissionPbInUsd,
					basePos.CommissionCptBase * ccy1.LastKnownUsdConversionMultiplier AS CommissionCptInUsd,
					basePos.DealsNum AS DealsNum,
					basePos.LastDealUtc AS LastDealUtc,
					basePos.IntegratorLatencyNanoseconds AS IntegratorLatencyNanoseconds,
					basePos.IntegratorLatencyCases AS IntegratorLatencyCases,
					basePos.CounterpartyLatencyNanoseconds AS CounterpartyLatencyNanoseconds,
					basePos.CounterpartyLatencyCases AS CounterpartyLatencyCases
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd,
					0 AS CommissionPbInUsd,
					0 AS CommissionCptInUsd,
					0 AS DealsNum,
					NULL AS LastDealUtc,
					NULL AS IntegratorLatencyNanoseconds,
					NULL AS IntegratorLatencyCases,
					NULL AS CounterpartyLatencyNanoseconds,
					NULL AS CounterpartyLatencyCases
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode
END
GO


ALTER TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily] ADD [CommissionsPbUsd] [decimal](18,2) NULL
ALTER TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily] ADD [CommissionsCptUsd] [decimal](18,2) NULL
ALTER TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily] ADD [CommissionsSumUsd] [decimal](18,2) NULL
GO


ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
AS
BEGIN

	--create the temp table before transaction to speed up things
	
	CREATE TABLE #TempCounterpartyStatistics(
		[Counterparty] [varchar](3) NOT NULL,
		[NopAbs] decimal(18,2) NULL,
		[VolumeBaseAbsInUsd] decimal(18, 2) NULL,
		[VolumeShare] decimal(18, 4) NULL,
		[CommissionsPbUsd] decimal(18, 2) NULL,
		[CommissionsCptUsd] decimal(18, 2) NULL,
		[CommissionsSumUsd] decimal(18, 2) NULL,
		[DealsNum] int NULL,
		[LastDealUtc] datetime2(7) NULL,
		[AvgDealSizeUsd] decimal(18,2) NULL,
		[KGTRejectionsNum] int NULL,
		[LastKgtRejectionUtc] datetime2(7) NULL,
		[CtpRejectionsNum] int NULL,
		[LastCptRejectionUtc] datetime2(7) NULL,
		[KGTRejectionsRate] decimal(18,4) NULL,
		[CtpRejectionsRate] decimal(18,4) NULL,
		[AvgIntegratorLatencyNanoseconds] bigint NULL,
		[AvgCounterpartyLatencyNanoseconds] bigint
	)

	DECLARE @Today DATE = GETUTCDATE()
	
	INSERT INTO 
		#TempCounterpartyStatistics
		(
		[Counterparty],
		[NopAbs],
		[VolumeBaseAbsInUsd],
		[VolumeShare],
		[CommissionsPbUsd],
		[CommissionsCptUsd],
		[CommissionsSumUsd],
		[DealsNum],
		[LastDealUtc],
		[AvgDealSizeUsd],
		[KGTRejectionsNum],
		[LastKgtRejectionUtc],
		[CtpRejectionsNum],
		[LastCptRejectionUtc],
		[KGTRejectionsRate],
		[CtpRejectionsRate],
		[AvgIntegratorLatencyNanoseconds],
		[AvgCounterpartyLatencyNanoseconds]
		)
	exec [dbo].[GetStatisticsPerCounterparty_Daily_SP] @Today

	BEGIN TRANSACTION
	   TRUNCATE TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily]

	   INSERT INTO 
			[dbo].[PersistedStatisticsPerCounterpartyDaily]
			(
			[Counterparty],
			[NopAbs],
			[VolumeBaseAbsInUsd],
			[VolumeShare],
			[CommissionsPbUsd],
			[CommissionsCptUsd],
			[CommissionsSumUsd],
			[DealsNum],
			[LastDealUtc],
			[AvgDealSizeUsd],
			[KGTRejectionsNum],
			[LastKgtRejectionUtc],
			[CtpRejectionsNum],
			[LastCptRejectionUtc],
			[KGTRejectionsRate],
			[CtpRejectionsRate],
			[AvgIntegratorLatencyNanoseconds],
			[AvgCounterpartyLatencyNanoseconds]
			)
		SELECT
			[Counterparty],
			[NopAbs],
			[VolumeBaseAbsInUsd],
			[VolumeShare],
			[CommissionsPbUsd],
			[CommissionsCptUsd],
			[CommissionsSumUsd],
			[DealsNum],
			[LastDealUtc],
			[AvgDealSizeUsd],
			[KGTRejectionsNum],
			[LastKgtRejectionUtc],
			[CtpRejectionsNum],
			[LastCptRejectionUtc],
			[KGTRejectionsRate],
			[CtpRejectionsRate],
			[AvgIntegratorLatencyNanoseconds],
			[AvgCounterpartyLatencyNanoseconds]
		FROM
			#TempCounterpartyStatistics

	COMMIT
END
GO

ALTER PROCEDURE [dbo].[GetStatisticsPerCounterparty_DailyCached_SP]
AS
BEGIN
	SELECT 
		[Counterparty]
		,[NopAbs]
		,[VolumeBaseAbsInUsd]
		,[VolumeShare]
		,[CommissionsPbUsd]
		,[CommissionsCptUsd]
		,[CommissionsSumUsd]
		,[DealsNum]
		,[LastDealUtc]
		,[AvgDealSizeUsd]
		,[KGTRejectionsNum]
		,[LastKgtRejectionUtc]
		,[CtpRejectionsNum]
		,[LastCptRejectionUtc]
		,[KGTRejectionsRate]
		,[CtpRejectionsRate]
		,[AvgIntegratorLatencyNanoseconds]
		,[AvgCounterpartyLatencyNanoseconds]
	FROM 
		[dbo].[PersistedStatisticsPerCounterpartyDaily]
	ORDER BY
		[Counterparty]
END
GO

CREATE PROCEDURE [dbo].[UpdateEodUsdRates_SP]
AS
BEGIN

	UPDATE 
		eodConversions
	SET
		eodConversions.[BidPrice] = rates.[LastKnownUsdConversionMultiplier]
		,eodConversions.[AskPrice] = rates.[LastKnownUsdConversionMultiplier]
	FROM
		[FXTickDB].[dbo].[EODUsdConversions] eodConversions
		INNER JOIN [FXTickDB].[dbo].[_Internal_CurrencyIdsMapping] map ON eodConversions.[CurrencyID] = map.[DCDBCurrencyId] 
		INNER JOIN [dbo].[Currency_Rates] rates ON rates.[CurrencyID] = map.[IntegratorDBCurrencyId]
	WHERE
		[Date] = CAST(GETUTCDATE() AS DATE)
		AND rates.[LastKnownUsdConversionMultiplier] IS NOT NULL

END
GO
GRANT EXECUTE ON [dbo].[UpdateEodUsdRates_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE PROCEDURE [dbo].[UpdateEodRates_SP]
AS
BEGIN
	UPDATE 
		eodConversions
	SET
		eodConversions.[BidPrice] = rates.[LastKnownBidMeanPrice]
		,eodConversions.[AskPrice] = rates.[LastKnownAskMeanPrice]
	FROM
		[FXTickDB].[dbo].[EODRate] eodConversions
		INNER JOIN [DataCollection_Testing].[dbo].[_Internal_SymbolIdsMapping] map ON eodConversions.[FXPairId] = map.DCDBSymbolId
		INNER JOIN [Integrator_Testing].[dbo].[Symbol_Rates] rates ON rates.[Id] = map.[IntegratorDBSymbolId]
	WHERE
		[Date] = CAST(GETUTCDATE() AS DATE)
END
GO
GRANT EXECUTE ON [dbo].[UpdateEodRates_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


ALTER PROCEDURE [dbo].[UpdateSymbolPrices_SP] (
	@AskPricesList VARCHAR(MAX),
	@BidPricesList VARCHAR(MAX)
	)
AS
BEGIN

	DECLARE @now DATETIME2 = GETUTCDATE()
	
	UPDATE
		sym
	SET
		sym.LastKnownAskMeanPrice = prices.price,
		sym.AskMeanPriceUpdated = @now
	FROM
		[dbo].[csvlist_to_codesandprices_tbl](@AskPricesList) prices
		INNER JOIN Symbol sym on sym.ShortName = prices.Code

	UPDATE
		sym
	SET
		sym.LastKnownBidMeanPrice = prices.price,
		sym.BidMeanPriceUpdated = @now
	FROM
		[dbo].[csvlist_to_codesandprices_tbl](@BidPricesList) prices
		INNER JOIN Symbol sym on sym.ShortName = prices.Code
		
	
	DECLARE @USDId SMALLINT
	SELECT @USDId = [CurrencyID] FROM [dbo].[Currency_Internal] WITH (NOLOCK) WHERE [CurrencyAlphaCode] = 'USD'
	
		
	UPDATE
		dirrectUsdConversion
	SET
		dirrectUsdConversion.LastKnownUsdConversionMultiplier = sym.[LastKnownMeanReferencePrice],
		dirrectUsdConversion.UsdConversionMultiplierUpdated = sym.MeanReferencePriceUpdated
	FROM
		[dbo].[Currency_Rates] dirrectUsdConversion
		INNER JOIN [dbo].[Symbol] sym ON sym.[QuoteCurrencyId] = @USDId AND sym.[BaseCurrencyId] = dirrectUsdConversion.[CurrencyID]
		
	UPDATE
		invertedUsdConversion
	SET
		invertedUsdConversion.LastKnownUsdConversionMultiplier = 1/sym.[LastKnownMeanReferencePrice],
		invertedUsdConversion.UsdConversionMultiplierUpdated = sym.MeanReferencePriceUpdated
	FROM
		[dbo].[Currency_Rates] invertedUsdConversion
		INNER JOIN [dbo].[Symbol] sym ON sym.[BaseCurrencyId] = @USDId AND sym.[QuoteCurrencyId] = invertedUsdConversion.[CurrencyID]
		
		
	exec [dbo].[UpdateEodUsdRates_SP]
	exec [dbo].[UpdateEodRates_SP]
	

END
GO

CREATE TABLE [dbo].[SystemsTradingControlScheduleAction](
	[ActionId] tinyint NOT NULL,
	[ActionName] varchar(24) NOT NULL
) ON [PRIMARY]

CREATE UNIQUE NONCLUSTERED INDEX [SystemsTradingControlScheduleAction_Name] ON [dbo].[SystemsTradingControlScheduleAction]
(
	[ActionName] ASC
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [SystemsTradingControlScheduleAction_Id] ON [dbo].[SystemsTradingControlScheduleAction]
(
	[ActionId] ASC
) ON [PRIMARY]
GO

INSERT INTO [dbo].[SystemsTradingControlScheduleAction]([ActionId], [ActionName]) VALUES (1, 'ActivateGoFlatOnly')
INSERT INTO [dbo].[SystemsTradingControlScheduleAction]([ActionId], [ActionName]) VALUES (2, 'ActivateKillSwitch')
INSERT INTO [dbo].[SystemsTradingControlScheduleAction]([ActionId], [ActionName]) VALUES (3, 'RestartIntegrator')
INSERT INTO [dbo].[SystemsTradingControlScheduleAction]([ActionId], [ActionName]) VALUES (4, 'NotifyClients')
GO


ALTER TABLE [dbo].[SystemsTradingControlSchedule] ADD [TimeZoneCode] [char](3) NULL
ALTER TABLE [dbo].[SystemsTradingControlSchedule] ADD [ActionId] [char](3) NULL
ALTER TABLE [dbo].[SystemsTradingControlSchedule] DROP COLUMN [TurnOnGoFlatOnly]
exec sp_rename '[dbo].[SystemsTradingControlSchedule].[ScheduleTimeUtc]', 'ScheduleTimeZoneSpecific', 'COLUMN'
GO

UPDATE
	[dbo].[SystemsTradingControlSchedule]
SET
	[TimeZoneCode] = 'UTC',
	[ActionId] = 1

ALTER TABLE [dbo].[SystemsTradingControlSchedule] ALTER COLUMN [TimeZoneCode] [char](3) NOT NULL
ALTER TABLE [dbo].[SystemsTradingControlSchedule] ALTER COLUMN [ActionId] [char](3) NOT NULL
GO

ALTER PROCEDURE [dbo].[SystemsTradingControlScheduleInsertSingle_SP]
(
	@Enable Bit,
	@IsDaily Bit,
	@IsWeekly Bit,
	@ScheduleTimeZoneSpecific datetime2(7),
	@TimeZoneCode char(3),
	@ActionName varchar(24),
	@ScheduleDescription NVARCHAR(1024),
	@ScheduleEntryId int OUTPUT
)
AS
BEGIN
	INSERT INTO [dbo].[SystemsTradingControlSchedule]
           ([IsEnabled]
		   ,[IsDaily]
           ,[IsWeekly]
           ,[ScheduleTimeZoneSpecific]
		   ,[TimeZoneCode]
           ,[ActionId]
           ,[ScheduleDescription])
     VALUES
           (@Enable
			,@IsDaily
           ,@IsWeekly
           ,@ScheduleTimeZoneSpecific
		   ,@TimeZoneCode
           ,(SELECT [ActionId] FROM [dbo].[SystemsTradingControlScheduleAction] WHERE [ActionName] = @ActionName)
           ,@ScheduleDescription)
	
	SELECT @ScheduleEntryId = scope_identity();
END
GO

ALTER PROCEDURE [dbo].[SystemsTradingControlScheduleUpdateSingle_SP]
(
	@ScheduleEntryId int,
	@Enable Bit,
	@IsDaily Bit,
	@IsWeekly Bit,
	@ScheduleTimeZoneSpecific datetime2(7),
	@TimeZoneCode char(3),
	@ActionName varchar(24),
	@ScheduleDescription NVARCHAR(1024)
)
AS
BEGIN
	UPDATE 
		[dbo].[SystemsTradingControlSchedule]
	SET
		[IsEnabled] = @Enable
		,[IsDaily] = @IsDaily
		,[IsWeekly] = @IsWeekly
		,[ScheduleTimeZoneSpecific] = @ScheduleTimeZoneSpecific
		,[TimeZoneCode] = @TimeZoneCode
		,[ActionId] = (SELECT [ActionId] FROM [dbo].[SystemsTradingControlScheduleAction] WHERE [ActionName] = @ActionName)
		,[ScheduleDescription] = @ScheduleDescription
	WHERE
		[ScheduleEntryId] = @ScheduleEntryId
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedSystemsTradingControlSchedule_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@IsDailyRecords Bit = NULL,
	@IsWeeklyRecords Bit = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'SystemsTradingControlSchedule'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		SELECT
			[ScheduleEntryId]
			,[IsEnabled]
			,[IsDaily]
			,[IsWeekly]
			,[ScheduleTimeZoneSpecific]
			,[TimeZoneCode]
			,[ActionName]
			,[ScheduleDescription]
			,[LastUpdatedUtc]
		FROM 
			[dbo].[SystemsTradingControlSchedule] schedule
			INNER JOIN [dbo].[SystemsTradingControlScheduleAction] action on schedule.ActionId = action.ActionId
		WHERE
			(@IsDailyRecords IS NULL OR [IsDaily] = @IsDailyRecords)
			AND (@IsWeeklyRecords IS NULL OR [IsWeekly] = @IsWeeklyRecords)
		ORDER BY
			[IsDaily],
			[IsWeekly],
			[ScheduleTimeZoneSpecific]
	END
END
GO


ALTER PROCEDURE [dbo].[Daily_DeleteOldSchedules_SP]
AS
BEGIN
	DELETE FROM
		[dbo].[SystemsTradingControlSchedule]
	WHERE
		IsDaily = 0 AND IsWeekly = 0 AND ScheduleTimeZoneSpecific < GETUTCDATE()
END
GO


CREATE PROCEDURE [dbo].[Daily_ClearPriceDelayStats_SP]
AS
BEGIN
	UPDATE 
		[dbo].[PriceDelayStats]
	SET
		[TotalReceivedPricesInLastMinute] = 0,
		[DelayedPricesInLastMinute] = 0,
		[TotalReceivedPricesInLastHour] = 0,
		[DelayedPricesInLastHour] = 0
END
GO
GRANT EXECUTE ON [dbo].[Daily_ClearPriceDelayStats_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

USE msdb;
GO

EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'ClearPriceDelayStats',
    @subsystem = N'TSQL',
    @command = N'exec Daily_ClearPriceDelayStats_SP',
	@database_name = N'Integrator',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO

--MANUALY reaorder the steps - there is no other documented way than doing this through SSMS
