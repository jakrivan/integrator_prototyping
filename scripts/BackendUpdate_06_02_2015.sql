BEGIN TRANSACTION Install_06_02_001;
GO

ALTER TABLE [dbo].[VenueMMSettings]         ADD MinimumIntegratorPriceLife_milliseconds [int] NULL
ALTER TABLE [dbo].[VenueMMSettings_Changes] ADD MinimumIntegratorPriceLife_milliseconds [int] NULL
ALTER TABLE [dbo].[VenueStreamSettings]         ADD MinimumIntegratorPriceLife_milliseconds [int] NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ADD MinimumIntegratorPriceLife_milliseconds [int] NULL
GO

UPDATE [dbo].[VenueMMSettings] SET MinimumIntegratorPriceLife_milliseconds = 0
UPDATE [dbo].[VenueMMSettings_Changes] SET MinimumIntegratorPriceLife_milliseconds = 0
UPDATE [dbo].[VenueStreamSettings] SET MinimumIntegratorPriceLife_milliseconds = 0
UPDATE [dbo].[VenueStreamSettings_Changes] SET MinimumIntegratorPriceLife_milliseconds = 0

ALTER TABLE [dbo].[VenueMMSettings]         ALTER COLUMN MinimumIntegratorPriceLife_milliseconds [int] NOT NULL
ALTER TABLE [dbo].[VenueMMSettings_Changes] ALTER COLUMN MinimumIntegratorPriceLife_milliseconds [int] NOT NULL
ALTER TABLE [dbo].[VenueStreamSettings]         ALTER COLUMN MinimumIntegratorPriceLife_milliseconds [int] NOT NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ALTER COLUMN MinimumIntegratorPriceLife_milliseconds [int] NOT NULL
GO


ALTER TRIGGER [dbo].[VenueMMSettings_DeletedOrUpdated] ON [dbo].[VenueMMSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueMMSettings_Changes]
	   ([SymbolId]
	   ,[SizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade]
	   ,[MaximumWaitTimeOnImprovementTick_milliseconds]
	   ,[MinimumIntegratorPriceLife_milliseconds]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[SizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]
		,deleted.[MinimumIntegratorPriceLife_milliseconds]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueMMSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueMMSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([SizeToTrade]) AND (topUpdatedChange.[SizeToTrade] IS NULL OR topUpdatedChange.[SizeToTrade] != deleted.[SizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([MaximumWaitTimeOnImprovementTick_milliseconds]) AND (topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] IS NULL OR topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] != deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]))
		OR (UPDATE([MinimumIntegratorPriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] != deleted.[MinimumIntegratorPriceLife_milliseconds]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
END
GO

ALTER TRIGGER [dbo].[VenueStreamSettings_DeletedOrUpdated] ON [dbo].[VenueStreamSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueStreamSettings_Changes]
	   ([SymbolId]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[SizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade] 
	   ,[KGTLLTime_milliseconds]
	   ,[PreferLLDestinationCheckToLmax]
	   ,[MinimumBPGrossToAccept]
	   ,[PreferLmaxDuringHedging]   
	   ,[MinimumIntegratorPriceLife_milliseconds]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[SizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[KGTLLTime_milliseconds]
	    ,deleted.[PreferLLDestinationCheckToLmax]
	    ,deleted.[MinimumBPGrossToAccept]
	    ,deleted.[PreferLmaxDuringHedging]
		,deleted.[MinimumIntegratorPriceLife_milliseconds]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueStreamSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueStreamSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
		OR (UPDATE([SizeToTrade]) AND (topUpdatedChange.[SizeToTrade] IS NULL OR topUpdatedChange.[SizeToTrade] != deleted.[SizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([KGTLLTime_milliseconds]) AND (topUpdatedChange.[KGTLLTime_milliseconds] IS NULL OR topUpdatedChange.[KGTLLTime_milliseconds] != deleted.[KGTLLTime_milliseconds]))
		OR (UPDATE([PreferLLDestinationCheckToLmax]) AND (topUpdatedChange.[PreferLLDestinationCheckToLmax] IS NULL OR topUpdatedChange.[PreferLLDestinationCheckToLmax] != deleted.[PreferLLDestinationCheckToLmax]))
		OR (UPDATE([MinimumBPGrossToAccept]) AND (topUpdatedChange.[MinimumBPGrossToAccept] IS NULL OR topUpdatedChange.[MinimumBPGrossToAccept] != deleted.[MinimumBPGrossToAccept]))
		OR (UPDATE([PreferLmaxDuringHedging]) AND (topUpdatedChange.[PreferLmaxDuringHedging] IS NULL OR topUpdatedChange.[PreferLmaxDuringHedging] != deleted.[PreferLmaxDuringHedging]))
		OR (UPDATE([MinimumIntegratorPriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] != deleted.[MinimumIntegratorPriceLife_milliseconds]))
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueMMSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueMMSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice]
			,[SizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[MinimumIntegratorPriceLife_milliseconds]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueMMSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name
	END
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueStreamSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueStreamSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[SizeToTrade]
		    ,[BestPriceImprovementOffsetBasisPoints]
		    ,[MaximumDiscountBasisPointsToTrade] 
		    ,[KGTLLTime_milliseconds]
		    ,[PreferLLDestinationCheckToLmax]
		    ,[MinimumBPGrossToAccept]
		    ,[PreferLmaxDuringHedging]
			,[MinimumIntegratorPriceLife_milliseconds]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueStreamSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name
	END
END
GO


ALTER PROCEDURE [dbo].[GetVenueMMSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[SizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueStreamSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[SizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[PreferLLDestinationCheckToLmax],
		sett.[MinimumBPGrossToAccept],
		sett.[MinimumBPGrossToAccept] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossToAcceptDecimal],
		sett.[PreferLmaxDuringHedging],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'MM'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'MM TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueMMSettings]
           ([SymbolId]
           ,[SizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
		   ,[MinimumIntegratorPriceLife_milliseconds]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@SizeToTrade 
           ,@BestPriceImprovementOffsetBasisPoints
           ,@MaximumDiscountBasisPointsToTrade
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
		   ,@MinimumIntegratorPriceLife_milliseconds
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

ALTER PROCEDURE [dbo].[VenueStreamSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	DECLARE @MaxAllowedSystemsCount INT = 3
	DECLARE @FormedSystemsCount INT
	SELECT @FormedSystemsCount = COUNT([SymbolId])
	FROM [dbo].[VenueStreamSettings]
	WHERE [SymbolId] =  @SymbolId 

	IF @FormedSystemsCount >= @MaxAllowedSystemsCount
	BEGIN
		RAISERROR (N'Attempt to form more than %d systems for symbol %s - Disallowed', 16, 2, @MaxAllowedSystemsCount, @Symbol) WITH SETERROR
		RETURN
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Stream'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Stream TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueStreamSettings]
           ([SymbolId]
		   ,[CounterpartyId]
           ,[TradingSystemId]
           ,[SizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
		   ,[KGTLLTime_milliseconds]
		   ,[PreferLLDestinationCheckToLmax]
		   ,[MinimumBPGrossToAccept]
		   ,[PreferLmaxDuringHedging]
		   ,[MinimumIntegratorPriceLife_milliseconds])
     VALUES
           (@SymbolId
		   ,@CounterpartyId
           ,@TradingSystemId
           ,@SizeToTrade 
           ,@BestPriceImprovementOffsetBasisPoints
           ,@MaximumDiscountBasisPointsToTrade
		   ,@KGTLLTime_milliseconds
		   ,@PreferLLDestinationCheckToLmax
		   ,@MinimumBPGrossToAccept
		   ,@PreferLmaxDuringHedging
		   ,@MinimumIntegratorPriceLife_milliseconds)
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@MinimumIntegratorPriceLife_milliseconds [int]
)
AS
BEGIN
	UPDATE sett
	SET
		[SizeToTrade] = @SizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds,
		[MinimumIntegratorPriceLife_milliseconds] = @MinimumIntegratorPriceLife_milliseconds
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

ALTER PROCEDURE [dbo].[VenueStreamSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit],
	@MinimumIntegratorPriceLife_milliseconds [int]
)
AS
BEGIN
	UPDATE sett
	SET
		[SizeToTrade] = @SizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[KGTLLTime_milliseconds] = @KGTLLTime_milliseconds,
		[PreferLLDestinationCheckToLmax] = @PreferLLDestinationCheckToLmax,
		[MinimumBPGrossToAccept] = @MinimumBPGrossToAccept,
		[PreferLmaxDuringHedging] = @PreferLmaxDuringHedging,
		[MinimumIntegratorPriceLife_milliseconds] = @MinimumIntegratorPriceLife_milliseconds
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

--ROLLBACK TRANSACTION Install_06_02_001;
--GO
--COMMIT TRANSACTION Install_06_02_001;
--GO


CREATE TABLE [dbo].[IntegratorRejectionReason](
	[IntegratorRejectionReasonId] [tinyint] IDENTITY(1,1) NOT NULL,
	[IntegratorRejectionReason] [varchar](32) NOT NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_IntegratorRejectionReason_Id] ON [dbo].[IntegratorRejectionReason]
(
	[IntegratorRejectionReasonId] ASC
)
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_IntegratorRejectionReason_Reason] ON [dbo].[IntegratorRejectionReason]
(
	[IntegratorRejectionReason] ASC
)
GO

INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES ('InternalFailure')
INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES ('KgtTooLate')
INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES ('KgtTooLateOTDelivered')
INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES ('SystemBusinessDecision')
INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES ('SystemFailedToProcess')
INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES ('SystemUnavailableSizeToTrade')
INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES ('RiskMgmtDisallow')
INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES ('SettlDateUnavailable')
INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES ('IncomingOrderMismatch')
INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES ('CounterpartyRejected')
GO

TRUNCATE TABLE [dbo].[OrderExternalIncomingRejectable_Daily]
TRUNCATE TABLE [dbo].[OrderExternalIncomingRejectable_Archive]
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] DROP COLUMN [IntegratorRejectionReason]
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] DROP COLUMN [IntegratorRejectionReason]
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] ADD [IntegratorRejectionReasonId] [tinyint] NULL
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] ADD [IntegratorRejectionReasonId] [tinyint] NULL
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_IntegratorRejectionReason] FOREIGN KEY([IntegratorRejectionReasonId])
REFERENCES [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReasonId])
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] CHECK CONSTRAINT [FK_OrderExternalIncomingRejectable_Daily_IntegratorRejectionReason]
GO


ALTER PROCEDURE [dbo].[Daily_ArchiveRejectableOrders_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[OrderExternalIncomingRejectable_Archive]
			([IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId])
		SELECT
			[IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]	
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[OrderExternalIncomingRejectable_Daily]
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[InsertNewOrderExternalIncomingRejectable_SP]( 
    @ExternalOrderReceived [datetime2](7),
	@ExternalOrderSentUtc [datetime2](7),
	@IntegratorSentExecutionReportUtc [datetime2](7),
	@CounterpartyClientOrderId [varchar](32),
	@CounterpartyClientId [varchar](20),
	@TradingSystemId int = NULL,
	@SourceIntegratorPriceIdentity [varchar](20) = NULL,
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@IntegratorExecId [varchar](20),
	@ValueDate [date] = NULL,
	@OrderReceivedToExecutionReportSentLatency [time](7),
	@IntegratorExecutedAmountBaseAbs [decimal](18, 2) = NULL,
	@IntegratorRejectedAmountBaseAbs [decimal](18, 2) = NULL,
	@RejectionReason [varchar](32) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WITH(NOLOCK) WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WITH(NOLOCK) WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WITH(NOLOCK) WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @RejectionReason IS NOT NULL
	BEGIN
		SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
		IF @@ROWCOUNT <> 1
		BEGIN
			INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
			SET @RejectionReasonId = @@IDENTITY
		END
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @IntegratorExecutedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,@IntegratorExecId)
		END
		
		IF @IntegratorRejectedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,ISNULL(@RejectionReason, '<NONE>')
				,@IntegratorExecId
				,@RejectionDirectionId)
		END

		INSERT INTO 
			[dbo].[OrderExternalIncomingRejectable_Daily]
			   ([IntegratorReceivedExternalOrderUtc]
			   ,[CounterpartySentExternalOrderUtc]
			   ,[IntegratorSentExecutionReportUtc]
			   ,[CounterpartyClientOrderId]
			   ,[CounterpartyClientId]
			   ,[TradingSystemId]
			   ,[SourceIntegratorPriceIdentity]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorDealDirectionId]
			   ,[CounterpartyRequestedPrice]
			   ,[CounterpartyRequestedAmountBasePol]
			   ,[IntegratorExecutionId]
			   ,[ValueDate]
			   ,[OrderReceivedToExecutionReportSentLatency ]
			   ,[IntegratorExecutedAmountBasePol]
			   ,[IntegratorRejectedAmountBasePol]
			   ,[IntegratorRejectionReasonId])
		 VALUES
			   (@ExternalOrderReceived
			   ,@ExternalOrderSentUtc
			   ,@IntegratorSentExecutionReportUtc
			   ,@CounterpartyClientOrderId
			   ,@CounterpartyClientId
			   ,@TradingSystemId
			   ,@SourceIntegratorPriceIdentity
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@DealDirectionId
			   ,@RequestedPrice
			   ,CASE WHEN @DealDirectionId = 1 THEN -@RequestedAmountBaseAbs ELSE @RequestedAmountBaseAbs END
			   ,@IntegratorExecId
			   ,@ValueDate
			   ,@OrderReceivedToExecutionReportSentLatency 
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
		       ,@RejectionReasonId)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH	   
		   	   
END
GO


ALTER PROCEDURE [dbo].[RejectExecutedOrderExternalIncomingRejectable_SP]( 
	@IntegratorExecId  [varchar](20),
	@RejectionReason [varchar](20) = NULL
	)
AS
BEGIN
	DECLARE @OriginallyExecutedAmount decimal(18,2)
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT 
		@OriginallyExecutedAmount = [IntegratorExecutedAmountBasePol] 
	FROM 
		[dbo].[OrderExternalIncomingRejectable_Daily]
	WHERE 
		[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) AND [IntegratorExecutionId] = @IntegratorExecId

	IF @OriginallyExecutedAmount IS NOT NULL
	BEGIN
	
		IF @RejectionReason IS NOT NULL
		BEGIN
			SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
			IF @@ROWCOUNT <> 1
			BEGIN
				INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
				SET @RejectionReasonId = @@IDENTITY
			END
		END
	
		BEGIN TRY

		BEGIN TRANSACTION;
	
			UPDATE
				[dbo].[OrderExternalIncomingRejectable_Daily]
			SET
				[IntegratorRejectedAmountBasePol] += @OriginallyExecutedAmount,
				[IntegratorExecutedAmountBasePol] = 0,
				[IntegratorRejectionReasonId] = @RejectionReasonId
			WHERE 
				[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) 
				AND [IntegratorExecutionId] = @IntegratorExecId

				
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId])
			SELECT
				[TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,ISNULL(@RejectionReason, '<NONE>')
				,[InternalTransactionIdentity]
				,@RejectionDirectionId
			FROM
				[dbo].[TradingSystemDealsExecuted_Daily]
			WHERE
				[InternalTransactionIdentity] = @IntegratorExecId
					
			DELETE FROM
				[dbo].[TradingSystemDealsExecuted_Daily]
			WHERE
				[InternalTransactionIdentity] = @IntegratorExecId
			
		COMMIT TRANSACTION;
		
		SELECT 
			[IntegratorReceivedExternalOrderUtc]
			,[CounterpartySentExternalOrderUtc]
			,[CounterpartyClientOrderId]
			,[TradingSystemId]
			,[SourceIntegratorPriceIdentity]
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.[Name] as Symbol
			,direction.[Name] as DealDirection
			,[CounterpartyRequestedPrice]
			,@OriginallyExecutedAmount AS OriginallyExecutedAmountBasePol
			,[IntegratorExecutionId]
			,[ValueDate]
		FROM 
			[dbo].[OrderExternalIncomingRejectable_Daily] deals
			INNER JOIN [dbo].[Symbol] symbol ON deals.[SymbolId] = symbol.[Id]
			INNER JOIN [dbo].[Counterparty] counterparty ON deals.[CounterpartyId] = counterparty.[CounterpartyId]
			INNER JOIN [dbo].[DealDirection] direction ON deals.[IntegratorDealDirectionId] = direction.Id
		WHERE 
			[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) AND [IntegratorExecutionId] = @IntegratorExecId
		
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION;
			THROW
		END CATCH
		
	END
END
GO

