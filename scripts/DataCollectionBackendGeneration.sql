/****** Object:  User [IntegratorServiceAccount]    Script Date: 28. 2. 2014 11:05:05 ******/
CREATE USER [IntegratorServiceAccount] FOR LOGIN [IntegratorServiceAccount] WITH DEFAULT_SCHEMA=[dbo]
GO
GRANT CONNECT TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  PartitionFunction [MarketDataSymbolPartitioningFunction]    Script Date: 28. 2. 2014 11:05:05 ******/
CREATE PARTITION FUNCTION [MarketDataSymbolPartitioningFunction](tinyint) AS RANGE LEFT FOR VALUES (0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x16, 0x2B, 0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x32, 0x33, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F, 0x41, 0x42, 0x43, 0x44, 0x46, 0x48, 0x4D, 0x4E, 0x4F, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x5B, 0x5C, 0x5D, 0x5F, 0x60, 0x61, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F, 0x71, 0x75, 0x80, 0x81, 0x85, 0x8E, 0x8F, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9A, 0x9B, 0x9C)
GO
/****** Object:  PartitionScheme [MarketDataSymbolPartitionScheme]    Script Date: 28. 2. 2014 11:05:05 ******/
CREATE PARTITION SCHEME [MarketDataSymbolPartitionScheme] AS PARTITION [MarketDataSymbolPartitioningFunction] TO ([PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY])
GO
/****** Object:  StoredProcedure [dbo].[GetAllFXPairs]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllFXPairs] 
AS
BEGIN
	SET NOCOUNT ON;

SELECT 
	[FxPairId], [FxpCode]
FROM
	[dbo].[FXPair]

END

GO
GRANT EXECUTE ON [dbo].[GetAllFXPairs] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetCounterpartySymbolSettings_SP]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCounterpartySymbolSettings_SP] 
AS
BEGIN

	SELECT 
		targetType.Code AS TradingTargetType
		,pair.FxpCode AS Symbol
		,settings.[Supported] AS Supported
		,settings.[MinimumPriceGranularity] AS MinimumPriceGranularity
	FROM 
		[dbo].[CounterpartySymbolSettings] settings
		INNER JOIN [dbo].[FxPair] pair ON pair.FxPairId = settings.[SymbolId]
		INNER JOIN [dbo].[TradingTargetType] targetType ON targetType.Id = settings.[TradingTargetTypeId]

END

GO
GRANT EXECUTE ON [dbo].[GetCounterpartySymbolSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetLastBankPricesForPair_SP]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetLastBankPricesForPair_SP]
	@TopCount INT,
	@SymbolName CHAR(6)
AS
BEGIN
	DECLARE @SymbolId INT

	SELECT @SymbolId = [FxPairId] FROM [dbo].[FxPair] WHERE [FxpCode] = @SymbolName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @SymbolName) WITH SETERROR
	END

	SELECT TOP (@TopCount)
		[Price],
		[IntegratorReceivedTimeUtc]
	FROM 
		[dbo].[MarketData] WITH(NOLOCK)
	WHERE 
		[FXPairId] = @SymbolId
		AND [RecordTypeId] = 0
	ORDER BY 
		[IntegratorReceivedTimeUtc] DESC
END

GO
GRANT EXECUTE ON [dbo].[GetLastBankPricesForPair_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetLiqProviderStreamsInfo]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLiqProviderStreamsInfo] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select LiqProviderStreamName, LiqProviderStreamId
from LiqProviderStream

END

GO
GRANT EXECUTE ON [dbo].[GetLiqProviderStreamsInfo] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Table [dbo].[CounterpartySymbolSettings]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CounterpartySymbolSettings](
	[TradingTargetTypeId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[Supported] [bit] NOT NULL,
	[OnePipDecimalValue] [decimal](18, 11) NULL,
	[MinimumPriceGranularity] [decimal](18, 11) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Currency]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrencyID] [smallint] IDENTITY(1,1) NOT NULL,
	[CurrencyAlphaCode] [nvarchar](50) NOT NULL,
	[CurrencyName] [nvarchar](250) NOT NULL,
	[CurrencyISOCode] [smallint] NULL,
	[Valid] [bit] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[CurrencyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DataProvider]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DataProvider](
	[DataProviderId] [tinyint] IDENTITY(1,1) NOT NULL,
	[DprCode] [char](3) NOT NULL,
	[DataProviderName] [varchar](50) NOT NULL,
	[DataSourceId] [int] NOT NULL,
 CONSTRAINT [PK_Broker] PRIMARY KEY CLUSTERED 
(
	[DataProviderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DataSource]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DataSource](
	[DataSourceId] [int] IDENTITY(1,1) NOT NULL,
	[DataSourceName] [varchar](50) NOT NULL,
	[DsrcCode] [char](3) NOT NULL,
	[DataSourceUrl] [varchar](250) NOT NULL,
 CONSTRAINT [PK_DataSource] PRIMARY KEY CLUSTERED 
(
	[DataSourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FxPair]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FxPair](
	[FxPairId] [tinyint] IDENTITY(1,1) NOT NULL,
	[FxpCode] [char](6) NOT NULL,
	[BaseCurrencyId] [smallint] NOT NULL,
	[QuoteCurrencyId] [smallint] NOT NULL,
	[Multiplier] [int] NOT NULL,
	[Decimals]  AS (log10([Multiplier])) PERSISTED,
	[ExamplePrice] [real] NOT NULL,
	[IsRaboTradable] [bit] NULL,
	[Usage] [tinyint] NULL,
 CONSTRAINT [PK_FXPair] PRIMARY KEY CLUSTERED 
(
	[FxPairId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LiqProviderStream]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LiqProviderStream](
	[LiqProviderStreamId] [tinyint] IDENTITY(1,1) NOT NULL,
	[LiqProviderStreamName] [nvarchar](50) NOT NULL,
	[LiqProviderId] [tinyint] NOT NULL,
 CONSTRAINT [PK_LiqProviderStream] PRIMARY KEY CLUSTERED 
(
	[LiqProviderStreamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LiquidityProvider]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LiquidityProvider](
	[LiqProviderId] [tinyint] IDENTITY(1,1) NOT NULL,
	[LiqProviderName] [nvarchar](50) NOT NULL,
	[LiqProviderCode] [char](3) NOT NULL,
 CONSTRAINT [PK_LiquidityProvider] PRIMARY KEY CLUSTERED 
(
	[LiqProviderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MarketData]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarketData](
	[RecordID] [bigint] IDENTITY(1,1) NOT NULL,
	[FXPairId] [tinyint] NOT NULL,
	[SideId] [bit] NULL,
	[Price] [decimal](18, 9) NULL,
	[Size] [decimal](18, 0) NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[TradeSideId] [bit] NULL,
	[CounterpartySentTimeUtc] [datetime2] NULL,
	[IntegratorReceivedTimeUtc] [datetime2] NOT NULL,
	[CounterpartyPriceIdentity] [nvarchar](max) NULL,
	[IntegratorPriceIdentity] [uniqueidentifier] NULL,
	[MinimumSize] [decimal](18, 0) NULL,
	[Granularity] [decimal](18, 0) NULL,
	[RecordTypeId] [tinyint] NOT NULL,
	[IsLastInContinuousData] [bit] NULL
) ON [MarketDataSymbolPartitionScheme]([FXPairId])

GO
GRANT INSERT ON [dbo].[MarketData] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[MarketData] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Table [dbo].[MarketDataRecordType]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarketDataRecordType](
	[RecordTypeId] [tinyint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MarketDataRecordType] PRIMARY KEY CLUSTERED 
(
	[RecordTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PriceSide]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PriceSide](
	[PriceSideId] [bit] NOT NULL,
	[PriceSideName] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_PriceSide] PRIMARY KEY CLUSTERED 
(
	[PriceSideId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SourceFile]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SourceFile](
	[SourceFileID] [int] IDENTITY(4000,1) NOT NULL,
	[FileName] [nvarchar](max) NOT NULL,
	[FileModifiedOn] [smalldatetime] NOT NULL,
	[FileSizeBytes] [bigint] NOT NULL,
	[TicksTotalInFile] [bigint] NULL,
	[TicksUniqueInFile] [bigint] NULL,
	[TicksDuplicateInFile] [bigint] NULL,
	[TicksUnknownInFile] [bigint] NULL,
	[FileDataTimeZoneID] [tinyint] NOT NULL,
	[FileImportTag] [nvarchar](max) NULL,
	[FileImportedOn] [smalldatetime] NOT NULL,
	[Comment] [nvarchar](max) NULL,
 CONSTRAINT [PK_SourceFile] PRIMARY KEY CLUSTERED 
(
	[SourceFileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tick]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tick](
	[TickID] [bigint] IDENTITY(2300000000,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON [PRIMARY]

GO
GRANT INSERT ON [dbo].[Tick] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[Tick] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Table [dbo].[TimeZone]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TimeZone](
	[TimeZoneID] [tinyint] IDENTITY(1,1) NOT NULL,
	[TznCode] [varchar](50) NOT NULL,
	[TimeZoneName] [varchar](100) NOT NULL,
	[DaylightSavingAdjusted] [bit] NOT NULL,
	[GMTOffsetSummer] [tinyint] NULL,
	[GMTOffestWinter] [tinyint] NULL,
	[SummerTimeStartsOn] [datetime] NULL,
	[WinterTimeStartsOn] [datetime] NULL,
 CONSTRAINT [PK_TimeZone] PRIMARY KEY CLUSTERED 
(
	[TimeZoneID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TradeSide]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradeSide](
	[TradeSideId] [bit] NOT NULL,
	[TradeSide] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TradeSide] PRIMARY KEY CLUSTERED 
(
	[TradeSideId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TradingTargetType]    Script Date: 28. 2. 2014 11:05:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TradingTargetType](
	[Id] [tinyint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Code] [varchar](16) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_PairTime]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE CLUSTERED INDEX [IX_PairTime] ON [dbo].[MarketData]
(
	[FXPairId] ASC,
	[IntegratorReceivedTimeUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [MarketDataSymbolPartitionScheme]([FXPairId])
GO
/****** Object:  Index [ProviderPairTime]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE CLUSTERED INDEX [ProviderPairTime] ON [dbo].[Tick]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PK_TradingTargetType]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE CLUSTERED INDEX [PK_TradingTargetType] ON [dbo].[TradingTargetType]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UniqueTradingTargetTypeAndSymbolCombination]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueTradingTargetTypeAndSymbolCombination] ON [dbo].[CounterpartySymbolSettings]
(
	[TradingTargetTypeId] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueCurrencyAlphaCode]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyAlphaCode] ON [dbo].[Currency]
(
	[CurrencyAlphaCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UniqueCurrencyIsoCode]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyIsoCode] ON [dbo].[Currency]
(
	[CurrencyISOCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueCurrencyName]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyName] ON [dbo].[Currency]
(
	[CurrencyName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DataProviderName_Index]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [DataProviderName_Index] ON [dbo].[DataProvider]
(
	[DataProviderName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DprCode_Index]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [DprCode_Index] ON [dbo].[DataProvider]
(
	[DprCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DataSourceName]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [DataSourceName] ON [dbo].[DataSource]
(
	[DataSourceName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DscrCode]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [DscrCode] ON [dbo].[DataSource]
(
	[DsrcCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [FxpCode_Index]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [FxpCode_Index] ON [dbo].[FxPair]
(
	[FxpCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]
GO
/****** Object:  Index [UniqueFXpair]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueFXpair] ON [dbo].[FxPair]
(
	[BaseCurrencyId] ASC,
	[QuoteCurrencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]
GO
/****** Object:  Index [UniqueLiqProviderStreamName]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueLiqProviderStreamName] ON [dbo].[LiqProviderStream]
(
	[LiqProviderStreamName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueLiqProviderCode]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueLiqProviderCode] ON [dbo].[LiquidityProvider]
(
	[LiqProviderCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueLiqProviderName]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueLiqProviderName] ON [dbo].[LiquidityProvider]
(
	[LiqProviderName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [SourceFile]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE NONCLUSTERED INDEX [SourceFile] ON [dbo].[Tick]
(
	[SourceFileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TznCode_Index]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [TznCode_Index] ON [dbo].[TimeZone]
(
	[TznCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TradingTargetTypeCode]    Script Date: 28. 2. 2014 11:05:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TradingTargetTypeCode] ON [dbo].[TradingTargetType]
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CounterpartySymbolSettings]  WITH CHECK ADD  CONSTRAINT [FK_CounterpartySymbolSettings_FxPair] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[CounterpartySymbolSettings] CHECK CONSTRAINT [FK_CounterpartySymbolSettings_FxPair]
GO
ALTER TABLE [dbo].[CounterpartySymbolSettings]  WITH CHECK ADD  CONSTRAINT [FK_CounterpartySymbolSettings_TradingTargetType] FOREIGN KEY([TradingTargetTypeId])
REFERENCES [dbo].[TradingTargetType] ([Id])
GO
ALTER TABLE [dbo].[CounterpartySymbolSettings] CHECK CONSTRAINT [FK_CounterpartySymbolSettings_TradingTargetType]
GO
ALTER TABLE [dbo].[DataProvider]  WITH CHECK ADD  CONSTRAINT [FK_DataProvider_DataSource] FOREIGN KEY([DataSourceId])
REFERENCES [dbo].[DataSource] ([DataSourceId])
GO
ALTER TABLE [dbo].[DataProvider] CHECK CONSTRAINT [FK_DataProvider_DataSource]
GO
ALTER TABLE [dbo].[FxPair]  WITH CHECK ADD  CONSTRAINT [FK_FXPair_Currency] FOREIGN KEY([BaseCurrencyId])
REFERENCES [dbo].[Currency] ([CurrencyID])
GO
ALTER TABLE [dbo].[FxPair] CHECK CONSTRAINT [FK_FXPair_Currency]
GO
ALTER TABLE [dbo].[FxPair]  WITH CHECK ADD  CONSTRAINT [FK_FXPair_Currency1] FOREIGN KEY([QuoteCurrencyId])
REFERENCES [dbo].[Currency] ([CurrencyID])
GO
ALTER TABLE [dbo].[FxPair] CHECK CONSTRAINT [FK_FXPair_Currency1]
GO
ALTER TABLE [dbo].[LiqProviderStream]  WITH CHECK ADD  CONSTRAINT [FK_LiqProviderStream_LiquidityProvider] FOREIGN KEY([LiqProviderId])
REFERENCES [dbo].[LiquidityProvider] ([LiqProviderId])
GO
ALTER TABLE [dbo].[LiqProviderStream] CHECK CONSTRAINT [FK_LiqProviderStream_LiquidityProvider]
GO
ALTER TABLE [dbo].[MarketData]  WITH NOCHECK ADD  CONSTRAINT [FK_MarketData_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_FxPair]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_LiqProviderStream] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_LiqProviderStream]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_MarketDataRecordType] FOREIGN KEY([RecordTypeId])
REFERENCES [dbo].[MarketDataRecordType] ([RecordTypeId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_MarketDataRecordType]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_PriceSide] FOREIGN KEY([SideId])
REFERENCES [dbo].[PriceSide] ([PriceSideId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_PriceSide]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_TradeSide] FOREIGN KEY([TradeSideId])
REFERENCES [dbo].[TradeSide] ([TradeSideId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_TradeSide]
GO
ALTER TABLE [dbo].[SourceFile]  WITH CHECK ADD  CONSTRAINT [FK_SourceFile_TimeZone] FOREIGN KEY([FileDataTimeZoneID])
REFERENCES [dbo].[TimeZone] ([TimeZoneID])
GO
ALTER TABLE [dbo].[SourceFile] CHECK CONSTRAINT [FK_SourceFile_TimeZone]
GO
ALTER TABLE [dbo].[Tick]  WITH NOCHECK ADD  CONSTRAINT [FK_Tick_DataProvider] FOREIGN KEY([DataProviderID])
REFERENCES [dbo].[DataProvider] ([DataProviderId])
GO
ALTER TABLE [dbo].[Tick] CHECK CONSTRAINT [FK_Tick_DataProvider]
GO
ALTER TABLE [dbo].[Tick]  WITH NOCHECK ADD  CONSTRAINT [FK_Tick_FXPair] FOREIGN KEY([FXPairID])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[Tick] CHECK CONSTRAINT [FK_Tick_FXPair]
GO
ALTER TABLE [dbo].[Tick]  WITH NOCHECK ADD  CONSTRAINT [FK_Tick_LiqProviderStream] FOREIGN KEY([BidStreamProviderId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO
ALTER TABLE [dbo].[Tick] CHECK CONSTRAINT [FK_Tick_LiqProviderStream]
GO
ALTER TABLE [dbo].[Tick]  WITH NOCHECK ADD  CONSTRAINT [FK_Tick_LiqProviderStream1] FOREIGN KEY([OfferStreamProviderId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO
ALTER TABLE [dbo].[Tick] CHECK CONSTRAINT [FK_Tick_LiqProviderStream1]
GO
ALTER TABLE [dbo].[Tick]  WITH NOCHECK ADD  CONSTRAINT [FK_Tick_SourceFile] FOREIGN KEY([SourceFileID])
REFERENCES [dbo].[SourceFile] ([SourceFileID])
GO
ALTER TABLE [dbo].[Tick] CHECK CONSTRAINT [FK_Tick_SourceFile]
GO
ALTER TABLE [dbo].[FxPair]  WITH CHECK ADD  CONSTRAINT [CK_FXPair] CHECK  ((len([FxpCode])=(6)))
GO
ALTER TABLE [dbo].[FxPair] CHECK CONSTRAINT [CK_FXPair]
GO
ALTER TABLE [dbo].[FxPair]  WITH CHECK ADD  CONSTRAINT [CK_FXPair_1] CHECK  ((NOT [FxpCode] like '% %'))
GO
ALTER TABLE [dbo].[FxPair] CHECK CONSTRAINT [CK_FXPair_1]
GO
ALTER TABLE [dbo].[LiquidityProvider]  WITH CHECK ADD  CONSTRAINT [CK_LiquidityProvider] CHECK  ((len([LiqProviderCode])=(3)))
GO
ALTER TABLE [dbo].[LiquidityProvider] CHECK CONSTRAINT [CK_LiquidityProvider]
GO
ALTER TABLE [dbo].[LiquidityProvider]  WITH CHECK ADD  CONSTRAINT [CK_LiquidityProvider_1] CHECK  ((NOT [LiqProviderCode] like '% %'))
GO
ALTER TABLE [dbo].[LiquidityProvider] CHECK CONSTRAINT [CK_LiquidityProvider_1]
GO
EXEC sys.sp_addextendedproperty @name=N'ISO', @value=N'ISO 4217, http://www.iso.org/iso/en/prods-services/popstds/currencycodeslist.html' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FxpCode does not contain spaces' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FxPair', @level2type=N'CONSTRAINT',@level2name=N'CK_FXPair_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enforce fixed value length' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LiquidityProvider', @level2type=N'CONSTRAINT',@level2name=N'CK_LiquidityProvider'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ensure the value does not contain spaces' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LiquidityProvider', @level2type=N'CONSTRAINT',@level2name=N'CK_LiquidityProvider_1'
GO


--- DATA

SET IDENTITY_INSERT [dbo].[DataSource] ON 

GO
INSERT [dbo].[DataSource] ([DataSourceId], [DataSourceName], [DsrcCode], [DataSourceUrl]) VALUES (1, N'EFX', N'EFX', N'http://www.efxgroup.com')
GO
INSERT [dbo].[DataSource] ([DataSourceId], [DataSourceName], [DsrcCode], [DataSourceUrl]) VALUES (2, N'Interactive Brokers', N'IBR', N'http://www.interactivebrokers.com')
GO
INSERT [dbo].[DataSource] ([DataSourceId], [DataSourceName], [DsrcCode], [DataSourceUrl]) VALUES (4, N'Bloomberg', N'BLO', N'http://www.bloomberg.com')
GO
INSERT [dbo].[DataSource] ([DataSourceId], [DataSourceName], [DsrcCode], [DataSourceUrl]) VALUES (5, N'EBS', N'EBS', N'http://www.icap.com')
GO
INSERT [dbo].[DataSource] ([DataSourceId], [DataSourceName], [DsrcCode], [DataSourceUrl]) VALUES (6, N'HotSpot International', N'HSI', N'http://www.hotspotfx.com/')
GO
INSERT [dbo].[DataSource] ([DataSourceId], [DataSourceName], [DsrcCode], [DataSourceUrl]) VALUES (7, N'Gain Capital group', N'GCG', N'http://ratedata.gaincapital.com')
GO
INSERT [dbo].[DataSource] ([DataSourceId], [DataSourceName], [DsrcCode], [DataSourceUrl]) VALUES (9, N'Integral', N'INT', N'http://www.integral.com')
GO
INSERT [dbo].[DataSource] ([DataSourceId], [DataSourceName], [DsrcCode], [DataSourceUrl]) VALUES (12, N'TestData', N'TST', N'test')
GO
INSERT [dbo].[DataSource] ([DataSourceId], [DataSourceName], [DsrcCode], [DataSourceUrl]) VALUES (13, N'SolidFX', N'SFX', N'http://www.solid-trading.com/')
GO
INSERT [dbo].[DataSource] ([DataSourceId], [DataSourceName], [DsrcCode], [DataSourceUrl]) VALUES (14, N'Reuters', N'RTR', N'http://reuters.com')
GO
INSERT [dbo].[DataSource] ([DataSourceId], [DataSourceName], [DsrcCode], [DataSourceUrl]) VALUES (15, N'KGT Integrator', N'ING', N'http://www.kgtinv.com')
GO
SET IDENTITY_INSERT [dbo].[DataSource] OFF
GO
SET IDENTITY_INSERT [dbo].[DataProvider] ON 

GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (7, N'BLM', N'Bloomberg from Marek', 4)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (10, N'EBM', N'EBS from Marek', 5)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (11, N'HSM', N'HotSpot International from Marek', 6)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (12, N'IB1', N'Interactive Brokers (Brno)', 2)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (13, N'IB2', N'Interactive Brokers (Busnago)', 2)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (16, N'EF1', N'EFX (Brno)', 1)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (18, N'EF2', N'EFX (Busnago)', 1)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (20, N'EFK', N'EFX (Backbone Brno)', 1)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (22, N'IN1', N'Integral main stream', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (23, N'TS1', N'Test data Integral Citrix', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (24, N'TS2', N'Test data Integral from Suren', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (25, N'INO', N'Official data from Integral', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (26, N'INC', N'Integral Citrix Sep30 test', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (27, N'IN4', N'Integral Server4 Sep30 test', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (28, N'IN5', N'Integral Server5 Sep30 test', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (29, N'EFT', N'EFX Sep30 test', 1)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (30, N'C02', N'ID30 - Citrix one pair only', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (31, N'C03', N'ID31 - Server04 23 pairs PollingInt10', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (33, N'TS3', N'Test direct insertion', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (34, N'SF1', N'SolidFX data', 13)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (35, N'HS1', N'Hotspot Fix BookFeed UAT', 6)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (39, N'HS2', N'Hotspot FIX BookFeed LiveView', 6)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (40, N'IN2', N'Integral backup 1', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (42, N'IN3', N'Integral backup 2', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (43, N'IN6', N'Integral backup 3', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (44, N'IN7', N'Integral Citrix', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (45, N'RT1', N'Reuters Live Data', 14)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (46, N'IG1', N'KGT Integrator Stream 01', 15)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (47, N'INB', N'Integral backup when switched to KGT Integrator', 9)
GO
INSERT [dbo].[DataProvider] ([DataProviderId], [DprCode], [DataProviderName], [DataSourceId]) VALUES (48, N'IDC', N'KGT Integrator Data Collection', 15)
GO
SET IDENTITY_INSERT [dbo].[DataProvider] OFF
GO
SET IDENTITY_INSERT [dbo].[Currency] ON 

GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (4, N'ALL', N'Lek', 8, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (5, N'DZD', N'Algerian Dinar', 12, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (6, N'ARS', N'Argentine Peso', 32, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (7, N'AUD', N'Australian Dollar', 36, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (8, N'BSD', N'Bahamian Dollar', 44, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (9, N'BHD', N'Bahraini Dinar', 48, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (10, N'BDT', N'Taka', 50, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (11, N'AMD', N'Armenian Dram', 51, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (12, N'BBD', N'Barbados Dollar', 52, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (13, N'BMD', N'Bermudian Dollar', 60, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (14, N'BTN', N'Ngultrum', 64, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (15, N'BOB', N'Boliviano', 68, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (16, N'BWP', N'Pula', 72, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (17, N'BZD', N'Belize Dollar', 84, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (18, N'SBD', N'Solomon Islands Dollar', 90, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (19, N'BND', N'Brunei Dollar', 96, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (20, N'MMK', N'Kyat', 104, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (21, N'BIF', N'Burundi Franc', 108, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (22, N'KHR', N'Riel', 116, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (23, N'CAD', N'Canadian Dollar', 124, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (24, N'CVE', N'Cape Verde Escudo', 132, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (25, N'KYD', N'Cayman Islands Dollar', 136, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (26, N'LKR', N'Sri Lanka Rupee', 144, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (27, N'CLP', N'Chilean Peso', 152, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (28, N'CNY', N'Yuan Renminbi', 156, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (29, N'COP', N'Colombian Peso', 170, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (30, N'KMF', N'Comoro Franc', 174, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (31, N'CRC', N'Costa Rican Colon', 188, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (32, N'HRK', N'Croatian Kuna', 191, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (33, N'CUP', N'Cuban Peso', 192, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (34, N'CYP', N'Cyprus Pound', 196, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (35, N'CZK', N'Czech Koruna', 203, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (36, N'DKK', N'Danish Krone', 208, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (37, N'DOP', N'Dominican Peso', 214, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (38, N'SVC', N'El Salvador Colon', 222, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (39, N'ETB', N'Ethiopian Birr', 230, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (40, N'ERN', N'Nakfa', 232, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (41, N'EEK', N'Kroon', 233, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (42, N'FKP', N'Falkland Islands Pound', 238, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (43, N'FJD', N'Fiji Dollar', 242, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (44, N'DJF', N'Djibouti Franc', 262, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (45, N'GMD', N'Dalasi', 270, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (46, N'GHC', N'Cedi', 288, 0, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (47, N'GIP', N'Gibraltar Pound', 292, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (48, N'GTQ', N'Quetzal', 320, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (49, N'GNF', N'Guinea Franc', 324, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (50, N'GYD', N'Guyana Dollar', 328, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (51, N'HTG', N'Gourde', 332, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (52, N'HNL', N'Lempira', 340, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (53, N'HKD', N'Hong Kong Dollar', 344, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (54, N'HUF', N'Forint', 348, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (55, N'ISK', N'Iceland Krona', 352, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (56, N'INR', N'Indian Rupee', 356, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (57, N'IDR', N'Rupiah', 360, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (58, N'IRR', N'Iranian Rial', 364, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (59, N'IQD', N'Iraqi Dinar', 368, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (60, N'ILS', N'New Israeli Sheqel', 376, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (61, N'JMD', N'Jamaican Dollar', 388, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (62, N'JPY', N'Yen', 392, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (63, N'KZT', N'Tenge', 398, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (64, N'JOD', N'Jordanian Dinar', 400, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (65, N'KES', N'Kenyan Shilling', 404, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (66, N'KPW', N'North Korean Won', 408, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (67, N'KRW', N'Won', 410, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (68, N'KWD', N'Kuwaiti Dinar', 414, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (69, N'KGS', N'Som', 417, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (70, N'LAK', N'Kip', 418, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (71, N'LBP', N'Lebanese Pound', 422, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (72, N'LSL', N'Loti', 426, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (73, N'LVL', N'Latvian Lats', 428, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (74, N'LRD', N'Liberian Dollar', 430, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (75, N'LYD', N'Libyan Dinar', 434, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (76, N'LTL', N'Lithuanian Litas', 440, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (77, N'MOP', N'Pataca', 446, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (78, N'MWK', N'Kwacha (Malawi)', 454, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (79, N'MYR', N'Malaysian Ringgit', 458, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (80, N'MVR', N'Rufiyaa', 462, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (81, N'MTL', N'Maltese Lira', 470, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (82, N'MRO', N'Ouguiya', 478, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (83, N'MUR', N'Mauritius Rupee', 480, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (84, N'MXN', N'Mexican Peso', 484, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (85, N'MNT', N'Tugrik', 496, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (86, N'MDL', N'Moldovan Leu', 498, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (87, N'MAD', N'Moroccan Dirham', 504, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (88, N'OMR', N'Rial Omani', 512, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (89, N'NAD', N'Namibian Dollar', 516, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (90, N'NPR', N'Nepalese Rupee', 524, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (91, N'ANG', N'Netherlands Antillian Guilder', 532, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (92, N'AWG', N'Aruban Guilder', 533, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (93, N'VUV', N'Vatu', 548, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (94, N'NZD', N'New Zealand Dollar', 554, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (95, N'NIO', N'Cordoba Oro', 558, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (96, N'NGN', N'Naira', 566, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (97, N'NOK', N'Norwegian Krone', 578, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (98, N'PKR', N'Pakistan Rupee', 586, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (99, N'PAB', N'Balboa', 590, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (100, N'PGK', N'Kina', 598, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (101, N'PYG', N'Guarani', 600, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (102, N'PEN', N'Nuevo Sol', 604, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (103, N'PHP', N'Philippine Peso', 608, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (104, N'GWP', N'Guinea-Bissau Peso', 624, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (105, N'QAR', N'Qatari Rial', 634, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (106, N'ROL', N'Old Leu', 642, 0, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (107, N'RUB', N'Russian Ruble', 643, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (108, N'RWF', N'Rwanda Franc', 646, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (109, N'SHP', N'Saint Helena Pound', 654, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (110, N'STD', N'Dobra', 678, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (111, N'SAR', N'Saudi Riyal', 682, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (112, N'SCR', N'Seychelles Rupee', 690, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (113, N'SLL', N'Leone', 694, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (114, N'SGD', N'Singapore Dollar', 702, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (115, N'SKK', N'Slovak Koruna', 703, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (116, N'VND', N'Dong', 704, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (117, N'SIT', N'Tolar', 705, 0, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (118, N'SOS', N'Somali Shilling', 706, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (119, N'ZAR', N'Rand', 710, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (120, N'ZWD', N'Zimbabwe Dollar', 716, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (121, N'SDD', N'Sudanese Dinar', 736, 0, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (122, N'SZL', N'Lilangeni', 748, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (123, N'SEK', N'Swedish Krona', 752, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (124, N'CHF', N'Swiss Franc', 756, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (125, N'SYP', N'Syrian Pound', 760, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (126, N'THB', N'Baht', 764, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (127, N'TOP', N'Pa anga', 776, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (128, N'TTD', N'Trinidad and Tobago Dollar', 780, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (129, N'AED', N'UAE Dirham', 784, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (130, N'TND', N'Tunisian Dinar', 788, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (131, N'TMM', N'Manat', 795, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (132, N'UGX', N'Uganda Shilling', 800, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (133, N'MKD', N'Denar', 807, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (134, N'EGP', N'Egyptian Pound', 818, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (135, N'GBP', N'Pound Sterling', 826, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (136, N'TZS', N'Tanzanian Shilling', 834, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (137, N'USD', N'US Dollar', 840, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (138, N'UYU', N'Peso Uruguayo', 858, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (139, N'UZS', N'Uzbekistan Sum', 860, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (140, N'VEB', N'Bolivar', 862, 0, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (141, N'WST', N'Tala', 882, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (142, N'YER', N'Yemeni Rial', 886, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (143, N'CSD', N'Serbian Dinar (former)', 891, 0, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (144, N'ZMK', N'Kwacha (Zambia)', 894, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (145, N'TWD', N'New Taiwan Dollar', 901, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (146, N'MZN', N'Metical', 943, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (147, N'AZN', N'Azerbaijanian Manat', 944, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (148, N'RON', N'New Leu', 946, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (149, N'CHE', N'WIR Euro', 947, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (150, N'CHW', N'WIR Franc', 948, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (151, N'TRY', N'New Turkish Lira', 949, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (152, N'XAF', N'CFA Franc BEAC', 950, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (153, N'XCD', N'East Caribbean Dollar', 951, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (154, N'XOF', N'CFA Franc BCEAO', 952, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (155, N'XPF', N'CFP Franc', 953, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (156, N'XDR', N'SDR', 960, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (157, N'SRD', N'Surinam Dollar', 968, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (158, N'MGA', N'Malagascy Ariary', 969, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (159, N'COU', N'Unidad de Valor Real', 970, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (160, N'AFN', N'Afghani', 971, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (161, N'TJS', N'Somoni', 972, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (162, N'AOA', N'Kwanza', 973, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (163, N'BYR', N'Belarussian Ruble', 974, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (164, N'BGN', N'Bulgarian Lev', 975, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (165, N'CDF', N'Franc Congolais', 976, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (166, N'BAM', N'Convertible Marks', 977, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (167, N'EUR', N'Euro', 978, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (168, N'MXV', N'Mexican Unidad de Inversion (UID)', 979, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (169, N'UAH', N'Hryvnia', 980, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (170, N'GEL', N'Lari', 981, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (171, N'BOV', N'Mvdol', 984, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (172, N'PLN', N'Zloty', 985, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (173, N'BRL', N'Brazilian Real', 986, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (174, N'CLF', N'Unidades de formento', 990, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (175, N'USN', N'US Dollar (Next day)', 997, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (176, N'USS', N'US Dollar (Same day)', 998, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (177, N'GHS', N'Ghana Cedi', 936, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (179, N'SDG', N'Sudanese Pound', 938, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (180, N'UYI', N'Uruguay Peso en Unidades Indexadas', 940, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (181, N'VEF', N'Bolivar Fuerte', 937, 1, CAST(0x000099E100A986CE AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (183, N'RSD', N'Serbian Dinar', 941, 1, CAST(0x000099E100DBF1F5 AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES (184, N'CNH', N'Offshore (Hong Kong) Chinese Renminbi Yuan', NULL, 1, CAST(0x0000A24900F9F060 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Currency] OFF
GO
SET ANSI_PADDING ON
GO
SET IDENTITY_INSERT [dbo].[FxPair] ON 

GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (3, N'EURUSD', 167, 137, 10000, 1.36185, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (4, N'GBPUSD', 135, 137, 10000, 2.05469, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (5, N'AUDJPY', 7, 62, 100, 97.996, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (6, N'AUDUSD', 7, 137, 10000, 0.8205, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (7, N'CHFJPY', 124, 62, 100, 98.155, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (8, N'EURAUD', 167, 7, 10000, 1.65749, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (9, N'EURCHF', 167, 124, 10000, 1.6472, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (10, N'EURGBP', 167, 135, 10000, 0.71245, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (11, N'EURJPY', 167, 62, 100, 162.519, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (12, N'GBPCHF', 135, 124, 10000, 2.4415, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (13, N'GBPJPY', 135, 62, 100, 233.83, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (14, N'NZDUSD', 94, 137, 10000, 0.7022, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (15, N'USDCAD', 137, 23, 10000, 1.0524, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (16, N'USDCHF', 137, 124, 10000, 1.2095, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (17, N'USDJPY', 137, 62, 100, 115.85, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (22, N'NZDJPY', 94, 62, 100, 81.34, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (43, N'CADJPY', 23, 62, 100, 110.13, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (45, N'EURCAD', 167, 23, 10000, 1.4334, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (46, N'EURSEK', 167, 123, 10000, 9.3987, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (47, N'USDDKK', 137, 36, 10000, 5.4934, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (48, N'USDPLN', 137, 172, 10000, 2.8037, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (49, N'USDNOK', 137, 97, 10000, 5.8369, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (50, N'USDCZK', 137, 35, 10000, 20.326, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (51, N'AUDNZD', 7, 94, 10000, 1.1689, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (56, N'EURNOK', 167, 97, 10000, 7.9465, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (57, N'AUDCAD', 7, 23, 10000, 0.8637, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (58, N'AUDCHF', 7, 124, 10000, 0.9928, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (59, N'CADCHF', 23, 124, 10000, 0.8748, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (60, N'EURCZK', 167, 35, 10000, 24.1751, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (61, N'EURHUF', 167, 54, 100, 255.13, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (62, N'EURNZD', 167, 94, 10000, 1.9408, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (63, N'EURPLN', 167, 172, 10000, 3.8184, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (65, N'GBPAUD', 135, 7, 10000, 2.4605, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (66, N'GBPCAD', 135, 23, 10000, 2.1245, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (67, N'USDSEK', 137, 123, 10000, 6.9022, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (68, N'USDMXN', 137, 84, 10000, 11.0239, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (70, N'EURDKK', 167, 36, 10000, 7.4535, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (72, N'USDHUF', 137, 54, 100, 172.62, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (77, N'CADNZD', 23, 94, 10000, 1.1671, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (78, N'GBPNZD', 135, 94, 10000, 2.7062, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (79, N'NZDCHF', 94, 124, 10000, 0.7498, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (80, N'AUDDKK', 7, 36, 10000, 5.1756, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (81, N'AUDNOK', 7, 97, 10000, 5.6519, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (82, N'EURTRY', 167, 151, 10000, 2.7286, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (83, N'EURZAR', 167, 119, 10000, 13.0225, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (84, N'NOKSEK', 97, 123, 10000, 1.1588, 1, 1)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (85, N'NZDCAD', 94, 23, 10000, 0.8569, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (86, N'USDHKD', 137, 53, 10000, 7.7515, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (87, N'USDSGD', 137, 114, 10000, 1.52355, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (88, N'USDTRY', 137, 151, 10000, 1.5502, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (89, N'EURMXN', 167, 84, 10000, 17.5139, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (90, N'USDZAR', 137, 119, 10000, 10.09667, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (91, N'AUDSEK', 7, 123, 10000, 5.9941, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (92, N'CADDKK', 23, 36, 10000, 5.3366, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (93, N'CADNOK', 23, 97, 10000, 5.8252, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (95, N'CADSEK', 23, 123, 10000, 6.1791, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (96, N'CHFDKK', 124, 36, 10000, 6.1015, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (97, N'CHFNOK', 124, 97, 10000, 6.6588, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (99, N'CHFSEK', 124, 123, 10000, 7.0631, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (100, N'DKKJPY', 36, 62, 10000, 17.806, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (101, N'DKKNOK', 36, 97, 10000, 1.0912, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (102, N'DKKSEK', 36, 123, 10000, 1.158, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (103, N'GBPDKK', 135, 36, 10000, 8.9256, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (104, N'GBPNOK', 135, 97, 10000, 9.8842, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (105, N'GBPSEK', 135, 123, 10000, 10.3336, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (106, N'HKDJPY', 53, 62, 10000, 11.9482, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (107, N'MXNJPY', 84, 62, 10000, 7.5011, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (108, N'NOKJPY', 97, 62, 100, 16.3136, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (109, N'NZDDKK', 94, 36, 10000, 4.5736, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (110, N'NZDNOK', 94, 97, 10000, 4.9905, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (111, N'NZDSEK', 94, 123, 10000, 5.2929, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (113, N'AUDSGD', 7, 114, 10000, 1.27044, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (117, N'ZARJPY', 119, 62, 10000, 11.384, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (128, N'NZDSGD', 94, 114, 10000, 1.02531, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (129, N'SGDJPY', 114, 62, 100, 64.629, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (133, N'USDILS', 137, 60, 10000, 3.5037, 1, 2)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (142, N'AUDHKD', 7, 53, 10000, 7.25922, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (143, N'EURHKD', 167, 53, 10000, 10.48485, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (144, N'EURRUB', 167, 107, 1000, 43.3967, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (145, N'GBPCZK', 135, 35, 10000, 30.8005, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (146, N'GBPHUF', 135, 54, 1000, 356.4313, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (147, N'USDRUB', 137, 107, 1000, 32.4515, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (148, N'GBPPLN', 135, 172, 10000, 5.0486, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (149, N'EURCNH', 167, 184, 10000, 8.2849, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (150, N'USDCNH', 137, 184, 10000, 6.1172, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (151, N'EURSGD', 167, 114, 10000, 1.72886, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (152, N'GBPHKD', 135, 53, 1000, 12.7303, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (153, N'GBPMXN', 135, 84, 1000, 21.79734, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (154, N'GBPSGD', 135, 114, 10000, 2.08222, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (155, N'GBPTRY', 135, 151, 10000, 3.63257, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (156, N'GBPZAR', 135, 119, 1000, 18.1361, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[FxPair] OFF
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (0, N'BankPool', N'BankPool')
GO
INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (1, N'Hotspot', N'Hotspot')
GO
INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (2, N'FXall', N'FXall')
GO
INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (3, N'LMAX', N'LMAX')
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 57, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 58, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 80, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 142, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 5, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 81, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 51, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 91, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 113, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 6, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 59, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 92, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 43, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 93, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 77, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 95, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 96, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 7, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 97, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 99, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 100, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 101, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 102, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 8, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 45, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 9, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 149, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 60, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 70, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 10, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 143, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 61, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 11, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 89, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 56, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 62, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 63, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 144, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00250000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 46, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 151, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 82, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 3, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 83, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 65, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 66, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 12, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 145, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 103, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 152, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 146, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 13, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 153, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 104, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 78, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 148, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 105, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 154, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 155, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 4, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 156, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 106, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 107, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 108, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 84, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 85, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 79, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 109, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 22, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 110, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 111, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 128, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 14, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 129, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 15, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 16, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 150, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 50, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 47, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 86, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 72, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 133, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 17, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 68, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 49, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 48, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 147, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00250000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 67, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 87, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 88, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 90, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (1, 117, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.01000000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 57, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 58, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 80, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 142, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 5, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 81, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 51, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 91, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 113, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 6, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 59, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 92, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 43, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 93, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 77, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00000100000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 95, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 96, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 7, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 97, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 99, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 100, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 101, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 102, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 8, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 45, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 9, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 149, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 60, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 70, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 10, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 143, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 61, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 11, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 89, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 56, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 62, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 63, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 144, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 46, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 151, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 82, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 3, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 83, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 65, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 66, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 12, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 145, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 103, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 152, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 146, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 13, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 153, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 104, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 78, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 148, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 105, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 154, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 155, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 4, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 156, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 106, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 107, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 108, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 84, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 85, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 79, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 109, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 22, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 110, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 111, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 128, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 14, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 129, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 15, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 16, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 150, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 50, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 47, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 86, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 72, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 133, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 17, 1, CAST(0.01000000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 68, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 49, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 48, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 147, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 67, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 87, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 88, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 90, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (0, 117, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 57, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 58, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 80, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 142, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 5, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 81, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 51, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 91, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 113, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 6, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 59, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 92, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 43, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 93, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 77, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 95, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 96, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 7, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 97, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 99, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 100, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 101, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 102, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 8, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 45, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 9, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 149, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 60, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 70, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 10, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 143, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 61, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 11, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 89, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 56, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 62, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 63, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 144, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 46, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 151, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 82, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 3, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 83, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 65, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 66, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 12, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 145, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 103, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 152, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 146, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 13, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 153, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 104, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 78, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 148, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 105, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 154, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 155, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 4, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 156, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 106, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 107, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 108, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 84, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 85, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 79, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 109, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 22, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 110, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 111, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 128, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 14, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 129, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 15, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 16, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 150, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 50, 1, CAST(0.00010000000 AS Decimal(18, 11)), CAST(0.00010000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 47, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 86, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 72, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 133, 0, NULL, NULL)
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 17, 1, CAST(0.00100000000 AS Decimal(18, 11)), CAST(0.00100000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 68, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 49, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 48, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 147, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 67, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 87, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 88, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 90, 1, CAST(0.00001000000 AS Decimal(18, 11)), CAST(0.00001000000 AS Decimal(18, 11)))
GO
INSERT [dbo].[CounterpartySymbolSettings] ([TradingTargetTypeId], [SymbolId], [Supported], [OnePipDecimalValue], [MinimumPriceGranularity]) VALUES (3, 117, 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[LiquidityProvider] ON 

GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (1, N'Bank of America Merrill Lynch', N'BOA')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (2, N'Citibank', N'CTI')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (3, N'Royal Bank of Scotland', N'RBS')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (4, N'JPMorgan Chase', N'JPM')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (5, N'Goldman Sachs', N'GLS')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (6, N'Deutsche Bank', N'DBK')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (7, N'UBS', N'UBS')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (8, N'Commerzbank', N'CZB')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (9, N'Rabobank', N'RAB')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (10, N'Morgan Stanley', N'MGS')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (11, N'Natixis', N'NTX')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (12, N'Nomura', N'NOM')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (13, N'Credit Suisse', N'CRS')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (14, N'BNP Paribas', N'BNP')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (15, N'Barclays', N'BRX')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (16, N'CAPH', N'CPH')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (17, N'Societe Generale', N'SOC')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (18, N'Last Atlantis', N'LTA')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (19, N'HSBC', N'HSB')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (20, N'Hotspot', N'HOT')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (21, N'FXall', N'FXA')
GO
INSERT [dbo].[LiquidityProvider] ([LiqProviderId], [LiqProviderName], [LiqProviderCode]) VALUES (22, N'LMAX', N'LMX')
GO
SET IDENTITY_INSERT [dbo].[LiquidityProvider] OFF
GO
SET IDENTITY_INSERT [dbo].[LiqProviderStream] ON 

GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (3, N'CITIB', 2)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (4, N'BOAN', 1)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (5, N'GSFX1', 5)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (6, N'JPM', 4)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (7, N'RBS', 3)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (8, N'DB5', 6)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (9, N'UBSC', 7)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (10, N'DKIB', 8)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (11, N'BOAN1', 1)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (12, N'RABO', 9)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (13, N'UBS', 7)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (14, N'MSFXE', 10)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (16, N'MSFXB', 10)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (17, N'DB', 6)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (18, N'MSFXA', 10)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (19, N'MSFXG', 10)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (20, N'RBSA', 3)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (21, N'NTFX', 11)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (22, N'DKIBB', 8)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (24, N'RBSC', 3)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (25, N'NOMU', 12)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (26, N'CRSU', 13)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (27, N'BNPP', 14)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (28, N'BARXA', 15)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (29, N'BNPP1', 14)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (30, N'BNPP2', 14)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (31, N'BNPP3', 14)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (32, N'RBSG', 3)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (33, N'CAPH', 16)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (34, N'MSFXD', 10)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (35, N'MSFXF', 10)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (36, N'BNPP6', 14)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (37, N'BNPP7', 14)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (38, N'JPMPB', 4)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (39, N'JPMPY9', 4)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (40, N'SG', 17)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (41, N'LTAT', 18)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (42, N'RBSB', 3)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (43, N'CRSUA', 13)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (44, N'DB9', 6)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (45, N'HSBCD', 19)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (46, N'JPMB', 4)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (47, N'BOANH', 1)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (48, N'BOANF', 1)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (49, N'CZBK', 8)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (50, N'RBS1', 3)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (51, N'HSBC', 19)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (52, N'MSFX', 10)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (53, N'BARX', 15)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (54, N'NOMU1', 12)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (55, N'NOMU2', 12)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (57, N'CITI', 2)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (58, N'GSFX', 5)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (59, N'RBS2', 3)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (60, N'BARX1', 15)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (61, N'BNP', 14)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (62, N'BOA', 1)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (63, N'BRX', 15)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (64, N'CRS', 13)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (65, N'CTI', 2)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (66, N'CZB', 8)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (67, N'DBK', 6)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (68, N'GLS', 5)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (69, N'HSB', 19)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (71, N'MGS', 10)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (72, N'NOM', 12)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (74, N'SOC', 17)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (75, N'HTA', 20)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (76, N'HTF', 20)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (77, N'FA1', 21)
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (78, N'LM1', 22)
GO
SET IDENTITY_INSERT [dbo].[LiqProviderStream] OFF
GO
INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (0, N'Bank Quote Data')
GO
INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (1, N'Single Quote Cancel')
GO
INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (2, N'StreamProvider Stream Cancellation')
GO
INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (3, N'Venue New Order')
GO
INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (4, N'Venue Order Change')
GO
INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (5, N'Venue Order Cancel')
GO
INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (6, N'Venue Trade Data')
GO
INSERT [dbo].[PriceSide] ([PriceSideId], [PriceSideName]) VALUES (0, N'Bid')
GO
INSERT [dbo].[PriceSide] ([PriceSideId], [PriceSideName]) VALUES (1, N'Ask')
GO
SET IDENTITY_INSERT [dbo].[TimeZone] ON 

GO
INSERT [dbo].[TimeZone] ([TimeZoneID], [TznCode], [TimeZoneName], [DaylightSavingAdjusted], [GMTOffsetSummer], [GMTOffestWinter], [SummerTimeStartsOn], [WinterTimeStartsOn]) VALUES (1, N'ET', N'Eastern Time (USA)', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TimeZone] ([TimeZoneID], [TznCode], [TimeZoneName], [DaylightSavingAdjusted], [GMTOffsetSummer], [GMTOffestWinter], [SummerTimeStartsOn], [WinterTimeStartsOn]) VALUES (2, N'GMT', N'Greenwich Mean Time', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TimeZone] ([TimeZoneID], [TznCode], [TimeZoneName], [DaylightSavingAdjusted], [GMTOffsetSummer], [GMTOffestWinter], [SummerTimeStartsOn], [WinterTimeStartsOn]) VALUES (5, N'CET', N'Central European Time', 1, 1, 2, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TimeZone] OFF
GO
INSERT [dbo].[TradeSide] ([TradeSideId], [TradeSide]) VALUES (0, N'SellGiven')
GO
INSERT [dbo].[TradeSide] ([TradeSideId], [TradeSide]) VALUES (1, N'BuyPaid')
GO



SET IDENTITY_INSERT [dbo].[SourceFile] ON 
GO

  INSERT INTO [dbo].[SourceFile]
           ([SourceFileID]
		   ,[FileName]
           ,[FileModifiedOn]
           ,[FileSizeBytes]
           ,[TicksTotalInFile]
           ,[TicksUniqueInFile]
           ,[TicksDuplicateInFile]
           ,[TicksUnknownInFile]
           ,[FileDataTimeZoneID]
           ,[FileImportTag]
           ,[FileImportedOn]
           ,[Comment])
     VALUES
           (932696,
		   'OnlineInsert'
           ,'1900-01-01 00:00:00'
           ,0
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,1
           ,NULL
           ,'1900-01-01 00:00:00'
           ,NULL)
GO

SET IDENTITY_INSERT [dbo].[SourceFile] OFF 
GO