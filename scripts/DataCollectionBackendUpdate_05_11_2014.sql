BEGIN TRANSACTION Install_05_11_001;
GO

UPDATE
	[dbo].[LiqProviderStream]
SET
	[LiqProviderStreamName] = 'L01'
WHERE
	[LiqProviderStreamName] = 'LM1'
	
	
SET IDENTITY_INSERT [dbo].[LiqProviderStream] ON 
GO

INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (86, N'L02', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (87, N'L03', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (88, N'L04', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (89, N'L05', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (90, N'L06', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (91, N'L07', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (92, N'L08', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (93, N'L09', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (94, N'L10', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (95, N'L11', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (96, N'L12', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (97, N'L13', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (98, N'L14', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (99, N'L15', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (100, N'L16', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (101, N'L17', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (102, N'L18', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (103, N'L19', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (104, N'L20', 22)

GO
SET IDENTITY_INSERT [dbo].[LiqProviderStream] OFF
GO


--ROLLBACK TRANSACTION Install_05_11_001;
--GO
--COMMIT TRANSACTION Install_05_11_001;
--GO