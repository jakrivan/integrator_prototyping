--Remove FKs first
ALTER TABLE [dbo].[ExecutedExternalDeals] DROP CONSTRAINT [FK_ExecutedExternalDeals_Symbol]
GO

ALTER TABLE [dbo].[OrderExternal] DROP CONSTRAINT [FK_OrderExternal_Symbol]
GO

ALTER TABLE [dbo].[DealExternalRejected] DROP CONSTRAINT [FK_RejectedDealExternal_Symbol]
GO

ALTER TABLE [dbo].[DealExternalExecuted] DROP CONSTRAINT [FK_DealExternalExecuted_Symbol]
GO


--Add new symbols

INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [IsRaboTradeable], [Usage], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(93, N'CHF/MXN', 'CHFMXN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'CHF'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'MXN'), 1000, 1, 1, 14.9533, GETUTCDATE())
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [IsRaboTradeable], [Usage], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(94, N'USD/INR', 'USDINR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'USD'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'INR'), 100, 1, 1, 61.1750, GETUTCDATE())
GO




--alter xsd schemas and xml settings
--risk mgmt, session mgmt, diag dashboard
-- MANUALY:

	   --This way select settings that needs adjustments
	--SELECT 'UPDATE [dbo].[Settings] SET [SettingsXml] = ''' + 
	--CONVERT(VARCHAR(MAX), [SettingsXml]) +
	--''', [LastUpdated] = GETUTCDATE() WHERE [Name] = ''' + 
	--CONVERT(VARCHAR(MAX), [Name]) + ''' AND [Id] = ' + CONVERT(VARCHAR(5), [Id])
	--FROM [dbo].[Settings]
	  --where CAST([SettingsXml] AS NVARCHAR(MAX)) like '%SKK%'


		--This is for RiskMgmt updates
		--	<Symbol ShortName="CHFMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15"/>
		--	<Symbol ShortName="USDINR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="15"/>


--Add FKs
ALTER TABLE [dbo].[ExecutedExternalDeals]  WITH NOCHECK ADD  CONSTRAINT [FK_ExecutedExternalDeals_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[OrderExternal]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderExternal_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[DealExternalRejected]  WITH NOCHECK ADD  CONSTRAINT [FK_RejectedDealExternal_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[DealExternalExecuted]  WITH NOCHECK ADD  CONSTRAINT [FK_DealExternalExecuted_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO