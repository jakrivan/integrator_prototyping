BEGIN TRANSACTION Install_05_03_001;
GO

DROP INDEX [UniqueCurrencyIsoCode] ON [dbo].[Currency_Internal]
GO

INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('FTS', 'UK 100', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('DJI', 'Wall Street 30', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('SPX', 'US SPX 500', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('SPY', 'US SPX 500 (Mini)', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('NDX', 'US Tech 100', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('DAX', 'Germany 30', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('DXY', 'Germany 30 (Mini)', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('CAC', 'France 40', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('STX', 'Europe 50', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('NIK', 'Japan 225', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('AUS', 'Australia 200', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('XAU', 'Gold (Spot)', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('XAG', 'Silver (Spot)', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('WTI', 'US Crude (Spot)', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('BRE', 'UK Brent (Spot)', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('XPT', 'Platinum', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('XPD', 'Palladium', NULL, 1, GETUTCDATE())
INSERT INTO [dbo].[Currency_Internal] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES ('GAS', 'US Natural Gas (Spot)', NULL, 1, GETUTCDATE())
GO

INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (137, 'FTS/GBP', 'FTSGBP', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'FTS'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'GBP'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (138, 'DJI/USD', 'DJIUSD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'DJI'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (139, 'SPX/USD', 'SPXUSD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'SPX'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (140, 'SPY/USD', 'SPYUSD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'SPY'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (141, 'NDX/USD', 'NDXUSD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'NDX'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (142, 'DAX/EUR', 'DAXEUR', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'DAX'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'EUR'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (143, 'DXY/EUR', 'DXYEUR', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'DXY'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'EUR'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (144, 'CAC/EUR', 'CACEUR', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'CAC'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'EUR'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (145, 'STX/EUR', 'STXEUR', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'STX'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'EUR'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (146, 'NIK/JPY', 'NIKJPY', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'NIK'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'JPY'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (147, 'AUS/AUD', 'AUSAUD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'AUS'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'AUD'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (148, 'XAU/USD', 'XAUUSD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'XAU'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (149, 'XAG/USD', 'XAGUSD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'XAG'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (150, 'XAU/AUD', 'XAUAUD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'XAU'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'AUD'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (151, 'XAG/AUD', 'XAGAUD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'XAG'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'AUD'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (152, 'WTI/USD', 'WTIUSD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'WTI'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (153, 'BRE/USD', 'BREUSD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'BRE'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (154, 'XPT/USD', 'XPTUSD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'XPT'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (155, 'XPD/USD', 'XPDUSD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'XPD'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (156, 'XAU/EUR', 'XAUEUR', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'XAU'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'EUR'))
INSERT INTO [dbo].[Symbol_Internal] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES (157, 'GAS/USD', 'GASUSD', (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'GAS'), (SELECT [CurrencyID] FROM [dbo].[Currency_Internal] WHERE [CurrencyAlphaCode] = 'USD'))
GO

INSERT INTO
	[dbo].[Symbol_Rates] ([Id])
SELECT
	[Id]
FROM
	[dbo].[Symbol_Internal]
WHERE
	[Id] NOT IN (SELECT [Id] FROM [dbo].[Symbol_Rates])
	
INSERT INTO
	[dbo].[SettlementDate] ([SymbolId])
SELECT
	[Id]
FROM
	[dbo].[Symbol_Internal]
WHERE
	[Id] NOT IN (SELECT [Id] FROM [dbo].[SettlementDate])
	
INSERT INTO
	[dbo].[Symbol_TrustedSpreads] ([SymbolId])
SELECT
	[Id]
FROM
	[dbo].[Symbol_Internal]
WHERE
	[Id] NOT IN (SELECT [SymbolId] FROM [dbo].[Symbol_TrustedSpreads])	
	

DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd] AS N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RiskManagementSettings" nillable="true" type="RiskManagementSettings" />
  <xs:complexType name="RiskManagementSettings">
    <xs:sequence>
      <xs:element name="ConfigLastUpdated"  nillable="true" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="DisallowTradingOnInstances" type="ArrayOfString" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsRate" type="FatalErrorsRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAndPosition" type="PriceAndPositionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Instance" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersSubmissionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FatalErrorsRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrderMaximumAllowedSizeSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedBaseAbsSizeInUsd" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="PriceAndPositionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumExternalNOPInUSD" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceDeviationCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="OnlinePriceDeviationCheckMaxPriceAge_Minutes" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="SymbolTrustworthyCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumSymbolUntrustworthyPeriod_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="DefaultNewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="Symbols" type="ArrayOfSymbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbol">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="Symbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="Symbol">
    <xs:attribute name="ShortName" type="xs:string" />
    <xs:attribute name="NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
  </xs:complexType>
</xs:schema>'
GO	
	

--alter xsd schemas and xml settings
--risk mgmt, session mgmt, diag dashboard
-- MANUALY:
		--This is for RiskMgmt updates
		--<DefaultNewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent>0.5</DefaultNewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent>
	

--session settings:

--<SymbolSetting>
--	<ShortName>FTSGBP</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>DJIUSD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>SPXUSD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>SPYUSD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>NDXUSD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>DAXEUR</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>DXYEUR</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>CACEUR</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>STXEUR</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>NIKJPY</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>AUSAUD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>XAUUSD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>XAGUSD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>XAUAUD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>XAGAUD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>WTIUSD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>BREUSD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>XPTUSD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>XPDUSD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>XAUEUR</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--	<ShortName>GASUSD</ShortName>
--	<SettingAction>Remove</SettingAction>
--</SymbolSetting>
	
	
	
	

--<SymbolSetting>
--    <ShortName>FTSGBP</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>DJIUSD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>SPXUSD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>SPYUSD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>NDXUSD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>DAXEUR</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>DXYEUR</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CACEUR</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>STXEUR</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>NIKJPY</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>AUSAUD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>XAUUSD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>XAGUSD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>XAUAUD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>XAGAUD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>WTIUSD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>BREUSD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>XPTUSD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>XPDUSD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>XAUEUR</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>GASUSD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>


CREATE PROCEDURE VenueStreamSystemsCheckAllowedCount_SP
AS
BEGIN

	DECLARE @CntOfAllowedEnabledSystemsPerSymbol INT = 1

	DECLARE @ErrorMsg VARCHAR(MAX) = ''
	DECLARE @IsError BIT = 0
	DECLARE @Tab CHAR = CHAR(9)
	DECLARE @CR CHAR = CHAR(13)
	DECLARE @LF CHAR = CHAR(10)

	;WITH systemCounts AS
	(
		SELECT 
			sym.[Name] AS Symbol, 
			streamSys.[TradingSystemId], 
			COUNT(sym.[Name]) OVER (PARTITION BY sym.[Name]) AS systemsForSymbolCnt
		FROM 
			[dbo].[VenueStreamSettings] streamSys
			INNER JOIN [dbo].[TradingSystemActions] tsa ON streamSys.[TradingSystemId] = tsa.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] sym on streamSys.[SymbolId] = sym.[Id]
		WHERE 
			tsa.[Enabled] = 1
			--AND tsa.[GoFlatOnly] = 0
			--AND tsa.[OrderTransmissionDisabled] = 0
	)
	SELECT
		@IsError = 1,
		@ErrorMsg = @ErrorMsg + @CR + @LF + 'Symbol: ' + Symbol + ', Group Name: ' + grp.[TradingSystemGroupName] + ',' + @Tab + ' SystemId: ' + CAST(systemCounts.TradingSystemId AS VARCHAR(10)) + ',' + @Tab + ' Total systems per this symbol: ' + CAST(systemsForSymbolCnt AS VARCHAR(4)) + ',' + @Tab + ' Total systems per this symbol allowed: ' + CAST(@CntOfAllowedEnabledSystemsPerSymbol AS VARCHAR(4))
	FROM
		systemCounts
		INNER JOIN [dbo].[TradingSystem] sys ON systemCounts.[TradingSystemId] = sys.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystemGroup] grp ON sys.[TradingSystemGroupId] = grp.[TradingSystemGroupId]
	WHERE
		systemsForSymbolCnt > @CntOfAllowedEnabledSystemsPerSymbol
	ORDER BY
		systemsForSymbolCnt DESC,
		grp.[TradingSystemGroupName] ASC,
		sys.[TradingSystemId] ASC

	IF @IsError = 1
		RAISERROR ('Error - Action reverted - There would be too many enabled stream systmes per some symbols:%s', 16, 1, @ErrorMsg)

END
GO

ALTER PROCEDURE [dbo].[EnableExternalOrdersTransmission_SP] 
	@SwitchName NVARCHAR(64),
	@ChangedBy NVARCHAR(256),
	@ChangedFrom NVARCHAR(256)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SwitchId INT 
	SELECT @SwitchId = [Id] FROM [dbo].[ExternalOrdersTransmissionSwitch] WHERE [SwitchName] = @SwitchName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Switch not found in DB: %s', @SwitchName, 16, 2) WITH SETERROR
	END

	BEGIN TRANSACTION
	BEGIN TRY

		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		INSERT INTO [dbo].[ExternalOrdersTransmissionSwitchHistory]
			   ([ExternalOrdersTransmissionSwitchId]
			   ,[TransmissionEnabled]
			   ,[Changed]
			   ,[ChangedBy]
			   ,[ChangedFrom])
		 VALUES
			   (@SwitchId
			   ,1
			   ,GETUTCDATE()
			   ,@ChangedBy
			   ,@ChangedFrom)

		UPDATE [dbo].[ExternalOrdersTransmissionSwitch]
		SET [TransmissionEnabled] = 1
		WHERE [Id] = @SwitchId

		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
	
END
GO

ALTER PROCEDURE [dbo].[FlipSystemsTradingControl_SP]
(
	@GoFlatOnly [bit]
)
AS
BEGIN

	IF @GoFlatOnly = 1
	BEGIN
		UPDATE [dbo].[SystemsTradingControl]
		SET [GoFlatOnly] = @GoFlatOnly
	END
	ELSE
	BEGIN
		BEGIN TRANSACTION
		BEGIN TRY
		
			UPDATE [dbo].[SystemsTradingControl]
			SET [GoFlatOnly] = @GoFlatOnly
			
			exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
			
			COMMIT TRANSACTION
		
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION;
			THROW;
		END CATCH
		
	END
	
	exec [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]
END
GO

DROP TRIGGER [dbo].[SystemsTradingControl_Flipped] 
GO

ALTER PROCEDURE [dbo].[TradingSystemsGroupUpdateTransmissionControl_SP]
(
	@OrderTransmissionDisabled [bit],
	@SystemsGroupId [int],
	@SystemTypeId [int] = NULL
)
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
	
		UPDATE 
			[dbo].[TradingSystemTypeAllowedForGroup]
		SET
			[IsOrderTransmissionDisabled] = @OrderTransmissionDisabled
		WHERE 
			TradingSystemGroupId = @SystemsGroupId
			AND (@SystemTypeId IS NULL OR TradingSystemTypeId = @SystemTypeId)
	
		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END
GO


ALTER PROCEDURE [dbo].[TradingSystemsGroupUpdateGoFlatOnly_SP]
(
	@GoFlatOnlyOn [bit],
	@SystemsGroupId [int],
	@SystemTypeId [int] = NULL
)
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
	
		UPDATE 
			[dbo].[TradingSystemTypeAllowedForGroup]
		SET
			[IsGoFlatOnlyOn] = @GoFlatOnlyOn
		WHERE 
			TradingSystemGroupId = @SystemsGroupId
			AND (@SystemTypeId IS NULL OR TradingSystemTypeId = @SystemTypeId)
	
		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[TradingSystemsGroupDisableEnableMulti_SP]
(
	@Enable [bit],
	@SystemsGroupId [int],
	@SystemTypeId [int] = NULL
)
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
	
		UPDATE 
			action
		SET
			[Enabled] = @Enable
		FROM
			[dbo].[TradingSystemActions] action
			INNER JOIN [dbo].[TradingSystem]  trsystem ON action.[TradingSystemId] = trsystem.[TradingSystemId]
		WHERE 
			trsystem.[TradingSystemGroupId] = @SystemsGroupId
			AND (@SystemTypeId IS NULL OR trsystem.[TradingSystemTypeId] = @SystemTypeId)
			AND [HardBlocked] = 0
	
		IF @Enable = 1
			exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[VenueSystemDisableEnableOne_SP]
(
	@TradingSystemId int,
	@Enable Bit
)
AS
BEGIN

	BEGIN TRANSACTION
	BEGIN TRY
	
		UPDATE
			[dbo].[TradingSystemActions]
		SET
			[Enabled] = @Enable
		WHERE
			[TradingSystemId] = @TradingSystemId
			AND [HardBlocked] = 0
	
		IF @Enable = 1
			exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[VenueStreamSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@MinimumBasisPointsFromKgtPriceToFastCancel  [decimal](18,6),
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Stream'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Stream TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	
	BEGIN TRANSACTION
	BEGIN TRY
		
		INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

		SELECT @TradingSystemId = scope_identity();
		
		--make sure that default switches values are correct
		INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
		SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
		FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
		WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
		
		INSERT INTO [dbo].[VenueStreamSettings]
			   ([SymbolId]
			   ,[CounterpartyId]
			   ,[TradingSystemId]
			   ,[MaximumSizeToTrade]
			   ,[BestPriceImprovementOffsetBasisPoints]
			   ,[MaximumDiscountBasisPointsToTrade]
			   ,[KGTLLTime_milliseconds]
			   ,[PreferLLDestinationCheckToLmax]
			   ,[MinimumBPGrossToAccept]
			   ,[PreferLmaxDuringHedging]
			   ,[MinimumIntegratorPriceLife_milliseconds]
			   ,[MinimumSizeToTrade]
			   ,[MinimumFillSize]
			   ,[MaximumSourceBookTiers]
			   ,[MinimumBasisPointsFromKgtPriceToFastCancel])
		 VALUES
			   (@SymbolId
			   ,@CounterpartyId
			   ,@TradingSystemId
			   ,@MaximumSizeToTrade 
			   ,@BestPriceImprovementOffsetBasisPoints
			   ,@MaximumDiscountBasisPointsToTrade
			   ,@KGTLLTime_milliseconds
			   ,@PreferLLDestinationCheckToLmax
			   ,@MinimumBPGrossToAccept
			   ,@PreferLmaxDuringHedging
			   ,@MinimumIntegratorPriceLife_milliseconds
			   ,@MinimumSizeToTrade
			   ,@MinimumFillSize
			   ,@MaximumSourceBookTiers
			   ,@MinimumBasisPointsFromKgtPriceToFastCancel)
		
	
		exec [dbo].[VenueStreamSystemsCheckAllowedCount_SP]
	
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW;
	END CATCH
END
GO
	
	
--ROLLBACK TRANSACTION Install_05_03_001;
--GO
--COMMIT TRANSACTION Install_05_03_001;
--GO
