


ALTER PROC [dbo].[InsertCommandItem_SP] (
	@CommandText NVARCHAR(MAX)
	)
AS
BEGIN
	BEGIN TRANSACTION
	   DECLARE @CommandId int;

		INSERT INTO [dbo].[Command]
			   ([CommandText]
			   ,[CreationTime])
		 VALUES
			   (@CommandText
			   ,GETUTCDATE())

	   SELECT @CommandId = scope_identity();

	   INSERT INTO [dbo].[NewCommand]
           ([CommandId])
		VALUES
           (@CommandId)
	COMMIT
END
GO


ALTER PROC [dbo].[GetCommandItems_SP] (
	@MaxCount INT
	)
AS
BEGIN
	DECLARE @TempCommandIds TABLE (
		Id INT
		,CommandText NVARCHAR(MAX)
		)

    DECLARE @CurrentUTCTime DATETIME

	SET @CurrentUTCTime = (SELECT GETUTCDATE())

	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

	BEGIN TRANSACTION

	INSERT INTO @TempCommandIds (
		Id
		,CommandText
		)
	SELECT TOP (@MaxCount) 
		Id, CommandText
	FROM 
		[dbo].[Command] cmd WITH (TABLOCKX)
		INNER JOIN [dbo].[NewCommand] newcmd WITH (TABLOCKX) ON cmd.id = newcmd.CommandId

	DELETE 
		newcmd
	FROM 
		[dbo].[NewCommand] newcmd
		INNER JOIN @TempCommandIds tmpcmd ON newcmd.CommandId = tmpcmd.Id

	COMMIT TRANSACTION

	INSERT INTO CommandStatus (
		CommandId
		,Status
		,Time
		)
	SELECT
		Id
		,N'Acquired'
		,@CurrentUTCTime
	FROM @TempCommandIds

	SELECT 
		Id
		,CommandText
	FROM @TempCommandIds
END
GO