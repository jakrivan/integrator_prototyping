
DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd]
GO

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd] AS N'<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="BusinessLayerSettings" nillable="true" type="BusinessLayerSettings" />
  <xs:complexType name="BusinessLayerSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MarketableClientOrdersMatchingStrategy" type="MarketableClientOrdersMatchingStrategy" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="ConsecutiveRejectionsStopStrategyBehavior" type="ConsecutiveRejectionsStopStrategySettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartWithSessionsStopped" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="AutoKillInternalOrdersOnOrdersTransmissionDisabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailSubject" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailBody" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderCounterpartyContacts" type="ArrayOfIgnoredOrderCounterpartyContact" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailMinDelayBetweenEmails_Minutes" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="PreventMatchingToSameCounterpartyAfterOrderRejectBehavior" type="PreventMatchingToSameCounterpartyAfterOrderRejectSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="MarketableClientOrdersMatchingStrategy">
    <xs:restriction base="xs:string">
      <xs:enumeration value="ImmediateMatching" />
      <xs:enumeration value="NextFreshPriceMatching" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ConsecutiveRejectionsStopStrategySettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="StrategyEnabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="NumerOfRejectionsToTriggerStrategy" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAgeRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceBpRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultCounterpartyRank" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRanks" type="ArrayOfCounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartyRank">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartyRank" nillable="true" type="CounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartyRank">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="Rank" type="xs:decimal" />
  </xs:complexType>
  <xs:complexType name="ArrayOfIgnoredOrderCounterpartyContact">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="IgnoredOrderCounterpartyContact" nillable="true" type="IgnoredOrderCounterpartyContact" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="IgnoredOrderCounterpartyContact">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="EmailContacts" type="xs:string" />
  </xs:complexType>
  <xs:complexType name="PreventMatchingToSameCounterpartyAfterOrderRejectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="StrategyEnabled" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartyCodes" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO



UPDATE [dbo].[Settings] SET [SettingsXml] = N'<BusinessLayerSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	<MarketableClientOrdersMatchingStrategy>ImmediateMatching</MarketableClientOrdersMatchingStrategy>
	<ConsecutiveRejectionsStopStrategyBehavior>
		<StrategyEnabled>true</StrategyEnabled>
		<NumerOfRejectionsToTriggerStrategy>3</NumerOfRejectionsToTriggerStrategy>
		<CounterpartyRankWeightMultiplier>20</CounterpartyRankWeightMultiplier>
		<PriceAgeRankWeightMultiplier>1</PriceAgeRankWeightMultiplier>
		<PriceBpRankWeightMultiplier>20</PriceBpRankWeightMultiplier>
		<DefaultCounterpartyRank>10</DefaultCounterpartyRank>
		<CounterpartyRanks>
			<CounterpartyRank CounterpartyCode="UBS" Rank="5"/>
			<CounterpartyRank CounterpartyCode="MGS" Rank="5.2"/>
			<CounterpartyRank CounterpartyCode="RBS" Rank="5.5"/>
			<CounterpartyRank CounterpartyCode="CTI" Rank="5.8"/>
			<CounterpartyRank CounterpartyCode="CRS" Rank="6.1"/>
			<CounterpartyRank CounterpartyCode="BOA" Rank="6.8"/>
			<CounterpartyRank CounterpartyCode="CZB" Rank="7.1"/>
			<CounterpartyRank CounterpartyCode="GLS" Rank="7.4"/>
			<CounterpartyRank CounterpartyCode="JPM" Rank="7.7"/>
			<CounterpartyRank CounterpartyCode="BNP" Rank="8.9"/>
			<CounterpartyRank CounterpartyCode="BRX" Rank="9.5"/>
			<CounterpartyRank CounterpartyCode="SOC" Rank="9.5"/>
			<CounterpartyRank CounterpartyCode="HSB" Rank="10"/>
			<CounterpartyRank CounterpartyCode="NOM" Rank="10"/>
		</CounterpartyRanks>
	</ConsecutiveRejectionsStopStrategyBehavior>
	<StartWithSessionsStopped>false</StartWithSessionsStopped>
	<AutoKillInternalOrdersOnOrdersTransmissionDisabled>true</AutoKillInternalOrdersOnOrdersTransmissionDisabled>
	<StartStopEmailSubject>Integrator service on {0} is {1}</StartStopEmailSubject>
	<StartStopEmailBody/>
	<StartStopEmailTo>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</StartStopEmailTo>
	<StartStopEmailCc/>
	<IgnoredOrderCounterpartyContacts>
		<IgnoredOrderCounterpartyContact CounterpartyCode="CRS" EmailContacts="list.efx-support@credit-suisse.com;ivan.dexeus@credit-suisse.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="UBS" EmailContacts="fxehelp@ubs.com;gianandrea.grassi@ubs.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="DBK" EmailContacts="gm-ecommerce.implementations@db.com;thomas-m.geist@db.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="CTI" EmailContacts="fifx.support@citi.com;kenneth.aina@citi.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="BOA" EmailContacts="efx.csg@baml.com;kenneth.aina@citi.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="MGS" EmailContacts="fxacctmgt@morganstanley.com;guy.hopkins@morganstanley.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="RBS" EmailContacts="GBMEMFXTradeTimeouts@rbs.com;Martin.Pilcher@rbs.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="HSB" EmailContacts="fix.support@hsbcib.com;olivier.werenne@hsbcib.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="JPM" EmailContacts="JPMorgan_FXEcom_Urgent@jpmorgan.com;jon.price@jpmorgan.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="GLS" EmailContacts="ficc-fx-edealing-integration@gs.com;tom.robinson@gs.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="BNP" EmailContacts="siamak.nouri@uk.bnpparibas.com;steve.earl@uk.bnpparibas.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="NOM" EmailContacts="FI-ECommerceTechFXSupport@nomura.com;nicola.dilena@nomura.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="CZB" EmailContacts="esupport@commerzbank.com;Roland.White@commerzbank.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="BRX" EmailContacts="BARXsupport@barclays.com;conor.daly@barclays.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="SOC" EmailContacts="FX-support@sgcib.com;leon.brown@sgcib.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="HTA" EmailContacts="HOTFX.PRODSUPPORT@knight.com;HOTFX.LIQUIDITY@knight.com;pgertler@kcg.com"/>
		<IgnoredOrderCounterpartyContact CounterpartyCode="HTF" EmailContacts="HOTFX.PRODSUPPORT@knight.com;HOTFX.LIQUIDITY@knight.com;pgertler@kcg.com"/>
	</IgnoredOrderCounterpartyContacts>
	<IgnoredOrderEmailCc>michal.kreslik@kgtinv.com;marek.fogiel@kgtinv.com;jan.krivanek@kgtinv.com</IgnoredOrderEmailCc>
	<FatalErrorsEmailTo>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</FatalErrorsEmailTo>
	<FatalErrorsEmailCc/>
	<FatalErrorsEmailMinDelayBetweenEmails_Minutes>10</FatalErrorsEmailMinDelayBetweenEmails_Minutes>
	<PreventMatchingToSameCounterpartyAfterOrderRejectBehavior>
		<StrategyEnabled>true</StrategyEnabled>
		<CounterpartyCodes>
			<Counterparty>GLS</Counterparty>
		</CounterpartyCodes>
	</PreventMatchingToSameCounterpartyAfterOrderRejectBehavior>
</BusinessLayerSettings>'
, [LastUpdated] = GETUTCDATE() WHERE [Name] = 'Kreslik.Integrator.BusinessLayer.dll' AND [Id] = 4
GO