

CREATE PROCEDURE [dbo].[GetLastLmaxPricesForPair_SP]
	@TopCount INT,
	@SymbolName CHAR(6)
AS
BEGIN
	DECLARE @SymbolId INT

	SELECT @SymbolId = [FxPairId] FROM [dbo].[FxPair] WHERE [FxpCode] = @SymbolName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @SymbolName) WITH SETERROR
	END

	SELECT TOP (@TopCount)
		[Price],
		side.PriceSideName AS Side,
		[IntegratorReceivedTimeUtc]
	FROM 
		[dbo].[MarketData] md WITH(NOLOCK)
		INNER JOIN [dbo].[LiqProviderStream] lps WITH(NOLOCK) ON md.CounterpartyId = lps.LiqProviderStreamId
		INNER JOIN [dbo].LiquidityProvider lp WITH(NOLOCK) ON lps.LiqProviderId = lp.LiqProviderId
		INNER JOIN [dbo].[PriceSide] side WITH(NOLOCK) ON md.SideId = side.PriceSideId
	WHERE 
		[FXPairId] = @SymbolId
		AND lp.LiqProviderCode = 'LMX'
		AND md.RecordTypeId = 3
	ORDER BY 
		[IntegratorReceivedTimeUtc] DESC
END
GO

GRANT EXECUTE ON [dbo].[GetLastLmaxPricesForPair_SP] TO [IntegratorServiceAccount] AS [dbo]
GO