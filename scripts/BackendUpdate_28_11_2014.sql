BEGIN TRANSACTION Install_28_11_001;
GO


-- facilitate NULL iso codes (currentl max one paer table)
DROP INDEX [UniqueCurrencyIsoCode] ON [dbo].[Currency]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyIsoCode] ON [dbo].[Currency]
(
	[CurrencyISOCode] ASC
) 
WHERE [CurrencyISOCode] IS NOT NULL
ON [PRIMARY]
GO

-- Add new Currency
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES
           ('XVN', 'VEN', NULL, 1, GetUtcDate())
GO


--need to be dropped so that the underlying column can be changed
ALTER TABLE [dbo].[Symbol] DROP COLUMN [Decimals]
ALTER TABLE [dbo].[Symbol] DROP COLUMN [Multiplier]
GO


--Remove FKs first
ALTER TABLE [dbo].[ExecutedExternalDeals] DROP CONSTRAINT [FK_ExecutedExternalDeals_Symbol]
GO

ALTER TABLE [dbo].[OrderExternal] DROP CONSTRAINT [FK_OrderExternal_Symbol]
GO

ALTER TABLE [dbo].[DealExternalRejected] DROP CONSTRAINT [FK_RejectedDealExternal_Symbol]
GO

ALTER TABLE [dbo].[DealExternalExecuted] DROP CONSTRAINT [FK_DealExternalExecuted_Symbol]
GO


--Add new symbols

INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(95, N'AUD/ZAR', 'AUDZAR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('AUD/ZAR', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('AUD/ZAR', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(96, N'CAD/HKD', 'CADHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/HKD', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(97, N'CAD/MXN', 'CADMXN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/MXN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/MXN', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(98, N'CAD/PLN', 'CADPLN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/PLN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/PLN', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(99, N'CAD/SGD', 'CADSGD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/SGD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/SGD', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(100, N'CAD/ZAR', 'CADZAR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/ZAR', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CAD/ZAR', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(101, N'CZK/JPY', 'CZKJPY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CZK/JPY', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CZK/JPY', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(102, N'DKK/HKD', 'DKKHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('DKK/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('DKK/HKD', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(103, N'EUR/ILS', 'EURILS', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('EUR/ILS', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('EUR/ILS', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(104, N'GBP/ILS', 'GBPILS', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('GBP/ILS', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('GBP/ILS', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(105, N'HUF/JPY', 'HUFJPY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('HUF/JPY', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('HUF/JPY', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(106, N'CHF/CZK', 'CHFCZK', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/CZK', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/CZK', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(107, N'CHF/HKD', 'CHFHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/HKD', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(108, N'CHF/HUF', 'CHFHUF', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/HUF', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/HUF', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(109, N'CHF/ILS', 'CHFILS', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/ILS', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/ILS', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(110, N'CHF/PLN', 'CHFPLN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/PLN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/PLN', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(111, N'CHF/SGD', 'CHFSGD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/SGD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/SGD', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(112, N'CHF/TRY', 'CHFTRY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/TRY', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/TRY', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(113, N'CHF/ZAR', 'CHFZAR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/ZAR', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('CHF/ZAR', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(114, N'NOK/HKD', 'NOKHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NOK/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NOK/HKD', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(115, N'NZD/HKD', 'NZDHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NZD/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NZD/HKD', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(116, N'NZD/PLN', 'NZDPLN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NZD/PLN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NZD/PLN', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(117, N'NZD/ZAR', 'NZDZAR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NZD/ZAR', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('NZD/ZAR', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(118, N'PLN/HUF', 'PLNHUF', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('PLN/HUF', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('PLN/HUF', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(119, N'PLN/JPY', 'PLNJPY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('PLN/JPY', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('PLN/JPY', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(120, N'SEK/HKD', 'SEKHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SEK/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SEK/HKD', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(121, N'SEK/JPY', 'SEKJPY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SEK/JPY', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SEK/JPY', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(122, N'SGD/DKK', 'SGDDKK', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/DKK', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/DKK', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(123, N'SGD/HKD', 'SGDHKD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/HKD', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/HKD', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(124, N'SGD/MXN', 'SGDMXN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/MXN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/MXN', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(125, N'SGD/NOK', 'SGDNOK', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/NOK', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/NOK', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(126, N'SGD/SEK', 'SGDSEK', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/SEK', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('SGD/SEK', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(127, N'TRY/JPY', 'TRYJPY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('TRY/JPY', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('TRY/JPY', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(128, N'USD/GHS', 'USDGHS', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/GHS', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/GHS', 5, 3)), 3.1925, GetUtcDate())
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(129, N'USD/KES', 'USDKES', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/KES', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/KES', 5, 3)), 90.3000, GetUtcDate())
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(130, N'USD/NGN', 'USDNGN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/NGN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/NGN', 5, 3)), 182.73, GetUtcDate())
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(131, N'USD/RON', 'USDRON', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/RON', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/RON', 5, 3)), 182.73, GetUtcDate())
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(132, N'USD/UGX', 'USDUGX', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/UGX', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/UGX', 5, 3)), 2775.00, GetUtcDate())
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(133, N'USD/ZMK', 'USDZMK', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/ZMK', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/ZMK', 5, 3)), 5189.50, GetUtcDate())
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId]) VALUES(134, N'ZAR/MXN', 'ZARMXN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('ZAR/MXN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('ZAR/MXN', 5, 3)))
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(135, N'USD/THB', 'USDTHB', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/THB', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/THB', 5, 3)), 32.87, GetUtcDate())
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [LastKnownMeanReferencePrice], [MeanReferencePriceUpdated]) VALUES(136, N'USD/XVN', 'USDXVN', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/XVN', 1, 3)), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = substring('USD/XVN', 5, 3)), 10.24, GetUtcDate())
GO


--alter xsd schemas and xml settings
--risk mgmt, session mgmt, diag dashboard
-- MANUALY:

		--This is for RiskMgmt updates
		--<Symbol ShortName="AUDZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CADHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CADMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CADPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CADSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CADZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CZKJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="DKKHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="EURILS" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="GBPILS" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="HUFJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CHFCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CHFHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CHFHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CHFILS" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CHFPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CHFSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CHFTRY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="CHFZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="NOKHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="NZDHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="NZDPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="NZDZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="PLNHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="PLNJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="SEKHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="SEKJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="SGDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="SGDHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="SGDMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="SGDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="SGDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="TRYJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="USDGHS" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="USDKES" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="USDNGN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="USDRON" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="USDUGX" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="USDZMK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="ZARMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="USDTHB" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		--<Symbol ShortName="USDXVN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5"/>
		
--session settings:

	--<SymbolSetting>
	--    <ShortName>AUDZAR</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CADHKD</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CADMXN</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CADPLN</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CADSGD</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CADZAR</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CZKJPY</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>DKKHKD</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>EURILS</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>GBPILS</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>HUFJPY</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CHFCZK</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CHFHKD</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CHFHUF</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CHFILS</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CHFPLN</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CHFSGD</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CHFTRY</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>CHFZAR</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>NOKHKD</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>NZDHKD</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>NZDPLN</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>NZDZAR</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>PLNHUF</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>PLNJPY</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>SEKHKD</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>SEKJPY</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>SGDDKK</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>SGDHKD</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>SGDMXN</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>SGDNOK</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>SGDSEK</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>TRYJPY</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>USDGHS</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>USDKES</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>USDNGN</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>USDRON</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>USDUGX</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>USDZMK</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>ZARMXN</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>USDTHB</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>
	--<SymbolSetting>
	--    <ShortName>USDXVN</ShortName>
	--    <SettingAction>Remove</SettingAction>
	--</SymbolSetting>


-- FXCM - FC1 and FC2 settings:

--<SymbolSetting>
--    <ShortName>AUDZAR</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CADHKD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CADMXN</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CADPLN</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CADSGD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CADZAR</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CZKJPY</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>DKKHKD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>EURILS</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>GBPILS</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>HUFJPY</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CHFCZK</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CHFHKD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CHFHUF</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CHFILS</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CHFPLN</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CHFSGD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CHFTRY</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>CHFZAR</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>NOKHKD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>NZDHKD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>NZDPLN</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>NZDZAR</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>PLNHUF</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>PLNJPY</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>SEKHKD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>SEKJPY</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>SGDDKK</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>SGDHKD</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>SGDMXN</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>SGDNOK</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>SGDSEK</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>TRYJPY</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>USDGHS</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>USDKES</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>USDNGN</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>USDRON</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>USDUGX</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>USDZMK</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
--<SymbolSetting>
--    <ShortName>ZARMXN</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>


-- Hotspot (HTF, HTA, HT3) settings:

--<SymbolSetting>
--    <ShortName>USDTHB</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>


--LMAX professional (LM1, LM3) settings:

--<SymbolSetting>
--    <ShortName>USDXVN</ShortName>
--    <Size>500000</Size>
--    <SettingAction>Add</SettingAction>
--</SymbolSetting>
	
	

--Add FKs
ALTER TABLE [dbo].[ExecutedExternalDeals]  WITH NOCHECK ADD  CONSTRAINT [FK_ExecutedExternalDeals_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[OrderExternal]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderExternal_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[DealExternalRejected]  WITH NOCHECK ADD  CONSTRAINT [FK_RejectedDealExternal_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[DealExternalExecuted]  WITH NOCHECK ADD  CONSTRAINT [FK_DealExternalExecuted_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (48, N'FXCM', N'FS1', 1)
GO

INSERT INTO [dbo].[CommissionsPerCounterparty] ([CounterpartyId], [CommissionPricePerMillion]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FS1'), 6.25)
GO

--ROLLBACK TRANSACTION Install_28_11_001;
--GO
--COMMIT TRANSACTION Install_28_11_001;
--GO