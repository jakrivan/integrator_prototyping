BEGIN TRANSACTION Install_10_02_001;
GO

DISABLE TRIGGER [dbo].[VenueMMSettings_DeletedOrUpdated] ON [dbo].[VenueMMSettings]
GO
DISABLE TRIGGER [dbo].[VenueStreamSettings_DeletedOrUpdated] ON [dbo].[VenueStreamSettings]
GO
DISABLE TRIGGER [dbo].[VenueCrossSettings_DeletedOrUpdated] ON [dbo].[VenueCrossSettings]
GO

ALTER TABLE [dbo].[VenueMMSettings]         ADD MinimumSizeToTrade [decimal] NULL
ALTER TABLE [dbo].[VenueMMSettings]         ADD MinimumFillSize [decimal] NULL
ALTER TABLE [dbo].[VenueMMSettings]         ADD MaximumSourceBookTiers [int] NULL
ALTER TABLE [dbo].[VenueMMSettings_Changes] ADD MinimumSizeToTrade [decimal] NULL
ALTER TABLE [dbo].[VenueMMSettings_Changes] ADD MinimumFillSize [decimal] NULL
ALTER TABLE [dbo].[VenueMMSettings_Changes] ADD MaximumSourceBookTiers [int] NULL
ALTER TABLE [dbo].[VenueStreamSettings]         ADD MinimumSizeToTrade [decimal] NULL
ALTER TABLE [dbo].[VenueStreamSettings]         ADD MinimumFillSize [decimal] NULL
ALTER TABLE [dbo].[VenueStreamSettings]         ADD MaximumSourceBookTiers [int] NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ADD MinimumSizeToTrade [decimal] NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ADD MinimumFillSize [decimal] NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ADD MaximumSourceBookTiers [int] NULL
ALTER TABLE [dbo].[VenueCrossSettings]         ADD MinimumFillSize [decimal] NULL
ALTER TABLE [dbo].[VenueCrossSettings_Changes] ADD MinimumFillSize [decimal] NULL
EXEC sp_rename '[dbo].[VenueMMSettings].[SizeToTrade]', 'MaximumSizeToTrade', 'COLUMN';
EXEC sp_rename '[dbo].[VenueMMSettings_Changes].[SizeToTrade]', 'MaximumSizeToTrade', 'COLUMN';
EXEC sp_rename '[dbo].[VenueStreamSettings].[SizeToTrade]', 'MaximumSizeToTrade', 'COLUMN';
EXEC sp_rename '[dbo].[VenueStreamSettings_Changes].[SizeToTrade]', 'MaximumSizeToTrade', 'COLUMN';
GO

UPDATE [dbo].[VenueMMSettings] SET MinimumSizeToTrade = 100000
UPDATE [dbo].[VenueMMSettings_Changes] SET MinimumSizeToTrade = 100000
UPDATE [dbo].[VenueStreamSettings] SET MinimumSizeToTrade = 100000
UPDATE [dbo].[VenueStreamSettings_Changes] SET MinimumSizeToTrade = 100000
UPDATE [dbo].[VenueMMSettings] SET MinimumFillSize = 5000
UPDATE [dbo].[VenueMMSettings_Changes] SET MinimumFillSize = 5000
UPDATE [dbo].[VenueStreamSettings] SET MinimumFillSize = 5000
UPDATE [dbo].[VenueStreamSettings_Changes] SET MinimumFillSize = 5000
UPDATE [dbo].[VenueMMSettings] SET MaximumSourceBookTiers = 1
UPDATE [dbo].[VenueMMSettings_Changes] SET MaximumSourceBookTiers = 1
UPDATE [dbo].[VenueStreamSettings] SET MaximumSourceBookTiers = 1
UPDATE [dbo].[VenueStreamSettings_Changes] SET MaximumSourceBookTiers = 1
UPDATE [dbo].[VenueCrossSettings] SET MinimumFillSize = 5000
UPDATE [dbo].[VenueCrossSettings_Changes] SET MinimumFillSize = 5000

--LM3 has minfill size 10000, it can currently be only in cross system
UPDATE sett
SET MinimumFillSize = 10000
FROM 
	[dbo].[VenueCrossSettings] sett
	INNER JOIN [dbo].[Counterparty] cpt ON cpt.CounterpartyId = sett.CounterpartyId
WHERE 
	cpt.CounterpartyCode = 'LM3'
	
UPDATE sett
SET MinimumFillSize = 10000
FROM 
	[dbo].[VenueCrossSettings_Changes] sett
	INNER JOIN [dbo].[Counterparty] cpt ON cpt.CounterpartyId = sett.CounterpartyId
WHERE 
	cpt.CounterpartyCode = 'LM3'
	

UPDATE sett
SET MinimumSizeToTrade = 10000
FROM 
	[dbo].[VenueMMSettings] sett
	INNER JOIN [dbo].[Counterparty] cpt ON cpt.CounterpartyId = sett.CounterpartyId
WHERE 
	cpt.CounterpartyCode = 'FA1' OR cpt.CounterpartyCode = 'LM2'
	
UPDATE sett
SET MinimumSizeToTrade = 10000
FROM 
	[dbo].[VenueMMSettings_Changes] sett
	INNER JOIN [dbo].[Counterparty] cpt ON cpt.CounterpartyId = sett.CounterpartyId
WHERE 
	cpt.CounterpartyCode = 'FA1' OR cpt.CounterpartyCode = 'LM2'
	
UPDATE sett
SET MinimumSizeToTrade = 1000000
FROM 
	[dbo].[VenueMMSettings] sett
	INNER JOIN [dbo].[Counterparty] cpt ON cpt.CounterpartyId = sett.CounterpartyId
WHERE 
	cpt.CounterpartyCode = 'FC2'
	
UPDATE sett
SET MinimumSizeToTrade = 1000000
FROM 
	[dbo].[VenueMMSettings_Changes] sett
	INNER JOIN [dbo].[Counterparty] cpt ON cpt.CounterpartyId = sett.CounterpartyId
WHERE 
	cpt.CounterpartyCode = 'FC2'

ALTER TABLE [dbo].[VenueMMSettings]         ALTER COLUMN MinimumSizeToTrade [decimal] NOT NULL
ALTER TABLE [dbo].[VenueMMSettings_Changes] ALTER COLUMN MinimumSizeToTrade [decimal] NOT NULL
ALTER TABLE [dbo].[VenueStreamSettings]         ALTER COLUMN MinimumSizeToTrade [decimal] NOT NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ALTER COLUMN MinimumSizeToTrade [decimal] NOT NULL
ALTER TABLE [dbo].[VenueMMSettings]         ALTER COLUMN MinimumFillSize [decimal] NOT NULL
ALTER TABLE [dbo].[VenueMMSettings_Changes] ALTER COLUMN MinimumFillSize [decimal] NOT NULL
ALTER TABLE [dbo].[VenueStreamSettings]         ALTER COLUMN MinimumFillSize [decimal] NOT NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ALTER COLUMN MinimumFillSize [decimal] NOT NULL
ALTER TABLE [dbo].[VenueMMSettings]         ALTER COLUMN MaximumSourceBookTiers [int] NOT NULL
ALTER TABLE [dbo].[VenueMMSettings_Changes] ALTER COLUMN MaximumSourceBookTiers [int] NOT NULL
ALTER TABLE [dbo].[VenueStreamSettings]         ALTER COLUMN MaximumSourceBookTiers [int] NOT NULL
ALTER TABLE [dbo].[VenueStreamSettings_Changes] ALTER COLUMN MaximumSourceBookTiers [int] NOT NULL
ALTER TABLE [dbo].[VenueCrossSettings]         ALTER COLUMN MinimumFillSize [decimal] NOT NULL
ALTER TABLE [dbo].[VenueCrossSettings_Changes] ALTER COLUMN MinimumFillSize [decimal] NOT NULL
GO


ALTER TRIGGER [dbo].[VenueMMSettings_DeletedOrUpdated] ON [dbo].[VenueMMSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueMMSettings_Changes]
	   ([SymbolId]
	   ,[MaximumSizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade]
	   ,[MaximumWaitTimeOnImprovementTick_milliseconds]
	   ,[MinimumIntegratorPriceLife_milliseconds]
	   ,[MinimumSizeToTrade]
	   ,[MinimumFillSize]
	   ,[MaximumSourceBookTiers]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[MaximumSizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]
		,deleted.[MinimumIntegratorPriceLife_milliseconds]
		,deleted.[MinimumSizeToTrade]
	    ,deleted.[MinimumFillSize]
		,deleted.[MaximumSourceBookTiers]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueMMSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueMMSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([MaximumWaitTimeOnImprovementTick_milliseconds]) AND (topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] IS NULL OR topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] != deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]))
		OR (UPDATE([MinimumIntegratorPriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] != deleted.[MinimumIntegratorPriceLife_milliseconds]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([MaximumSourceBookTiers]) AND (topUpdatedChange.[MaximumSourceBookTiers] IS NULL OR topUpdatedChange.[MaximumSourceBookTiers] != deleted.[MaximumSourceBookTiers]))
END
GO

ALTER TRIGGER [dbo].[VenueStreamSettings_DeletedOrUpdated] ON [dbo].[VenueStreamSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueStreamSettings_Changes]
	   ([SymbolId]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[MaximumSizeToTrade]
	   ,[BestPriceImprovementOffsetBasisPoints]
	   ,[MaximumDiscountBasisPointsToTrade] 
	   ,[KGTLLTime_milliseconds]
	   ,[PreferLLDestinationCheckToLmax]
	   ,[MinimumBPGrossToAccept]
	   ,[PreferLmaxDuringHedging]   
	   ,[MinimumIntegratorPriceLife_milliseconds]
	   ,[MinimumSizeToTrade]
	   ,[MinimumFillSize]
	   ,[MaximumSourceBookTiers]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[MaximumSizeToTrade]
		,deleted.[BestPriceImprovementOffsetBasisPoints]
		,deleted.[MaximumDiscountBasisPointsToTrade]
		,deleted.[KGTLLTime_milliseconds]
	    ,deleted.[PreferLLDestinationCheckToLmax]
	    ,deleted.[MinimumBPGrossToAccept]
	    ,deleted.[PreferLmaxDuringHedging]
		,deleted.[MinimumIntegratorPriceLife_milliseconds]
		,deleted.[MinimumSizeToTrade]
	    ,deleted.[MinimumFillSize]
		,deleted.[MaximumSourceBookTiers]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueStreamSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueStreamSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([CounterpartyId]) AND (topUpdatedChange.[CounterpartyId] IS NULL OR topUpdatedChange.[CounterpartyId] != deleted.[CounterpartyId]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([BestPriceImprovementOffsetBasisPoints]) AND (topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] IS NULL OR topUpdatedChange.[BestPriceImprovementOffsetBasisPoints] != deleted.[BestPriceImprovementOffsetBasisPoints]))
		OR (UPDATE([MaximumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MaximumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MaximumDiscountBasisPointsToTrade] != deleted.[MaximumDiscountBasisPointsToTrade]))
		OR (UPDATE([KGTLLTime_milliseconds]) AND (topUpdatedChange.[KGTLLTime_milliseconds] IS NULL OR topUpdatedChange.[KGTLLTime_milliseconds] != deleted.[KGTLLTime_milliseconds]))
		OR (UPDATE([PreferLLDestinationCheckToLmax]) AND (topUpdatedChange.[PreferLLDestinationCheckToLmax] IS NULL OR topUpdatedChange.[PreferLLDestinationCheckToLmax] != deleted.[PreferLLDestinationCheckToLmax]))
		OR (UPDATE([MinimumBPGrossToAccept]) AND (topUpdatedChange.[MinimumBPGrossToAccept] IS NULL OR topUpdatedChange.[MinimumBPGrossToAccept] != deleted.[MinimumBPGrossToAccept]))
		OR (UPDATE([PreferLmaxDuringHedging]) AND (topUpdatedChange.[PreferLmaxDuringHedging] IS NULL OR topUpdatedChange.[PreferLmaxDuringHedging] != deleted.[PreferLmaxDuringHedging]))
		OR (UPDATE([MinimumIntegratorPriceLife_milliseconds]) AND (topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] IS NULL OR topUpdatedChange.[MinimumIntegratorPriceLife_milliseconds] != deleted.[MinimumIntegratorPriceLife_milliseconds]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([MaximumSourceBookTiers]) AND (topUpdatedChange.[MaximumSourceBookTiers] IS NULL OR topUpdatedChange.[MaximumSourceBookTiers] != deleted.[MaximumSourceBookTiers]))
END
GO

ALTER TRIGGER [dbo].[VenueCrossSettings_DeletedOrUpdated] ON [dbo].[VenueCrossSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueCrossSettings_Changes]
	   ([SymbolId]
	   ,[MinimumSizeToTrade]
	   ,[MaximumSizeToTrade]
	   ,[MinimumDiscountBasisPointsToTrade]
	   ,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
	   ,[MaximumWaitTimeOnImprovementTick_milliseconds]
	   ,[MinimumFillSize]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[MinimumSizeToTrade]
		,deleted.[MaximumSizeToTrade]
		,deleted.[MinimumDiscountBasisPointsToTrade]
		,deleted.[MinimumTimeBetweenSingleCounterpartySignals_seconds]
		,deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]
		,deleted.[MinimumFillSize]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueCrossSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueCrossSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([MinimumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MinimumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MinimumDiscountBasisPointsToTrade] != deleted.[MinimumDiscountBasisPointsToTrade]))
		OR (UPDATE([MinimumTimeBetweenSingleCounterpartySignals_seconds]) AND (topUpdatedChange.[MinimumTimeBetweenSingleCounterpartySignals_seconds] IS NULL OR topUpdatedChange.[MinimumTimeBetweenSingleCounterpartySignals_seconds] != deleted.[MinimumTimeBetweenSingleCounterpartySignals_seconds]))
		OR (UPDATE([MaximumWaitTimeOnImprovementTick_milliseconds]) AND (topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] IS NULL OR topUpdatedChange.[MaximumWaitTimeOnImprovementTick_milliseconds] != deleted.[MaximumWaitTimeOnImprovementTick_milliseconds]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
END
GO

ENABLE TRIGGER [dbo].[VenueMMSettings_DeletedOrUpdated] ON [dbo].[VenueMMSettings]
GO
ENABLE TRIGGER [dbo].[VenueStreamSettings_DeletedOrUpdated] ON [dbo].[VenueStreamSettings]
GO
ENABLE TRIGGER [dbo].[VenueCrossSettings_DeletedOrUpdated] ON [dbo].[VenueCrossSettings]
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueMMSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueMMSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice]
			,[MaximumSizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueMMSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name
	END
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueStreamSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueStreamSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MaximumSizeToTrade]
		    ,[BestPriceImprovementOffsetBasisPoints]
		    ,[MaximumDiscountBasisPointsToTrade] 
		    ,[KGTLLTime_milliseconds]
		    ,[PreferLLDestinationCheckToLmax]
		    ,[MinimumBPGrossToAccept]
		    ,[PreferLmaxDuringHedging]
			,[MinimumIntegratorPriceLife_milliseconds]
			,[MinimumSizeToTrade]
			,[MinimumFillSize]
			,[MaximumSourceBookTiers]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueStreamSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name
	END
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueCrossSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueCrossSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[MinimumFillSize]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueCrossSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name 
	END
END
GO


ALTER PROCEDURE [dbo].[GetVenueMMSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueStreamSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumSizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[BestPriceImprovementOffsetBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [BestPriceImprovementOffsetDecimal],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[KGTLLTime_milliseconds],
		sett.[PreferLLDestinationCheckToLmax],
		sett.[MinimumBPGrossToAccept],
		sett.[MinimumBPGrossToAccept] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossToAcceptDecimal],
		sett.[PreferLmaxDuringHedging],
		sett.[MinimumIntegratorPriceLife_milliseconds],
		sett.[MinimumSizeToTrade],
		sett.[MinimumFillSize],
		sett.[MaximumSourceBookTiers],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[KGTRejectionsNum],
		stats.[CtpRejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumTimeBetweenSingleCounterpartySignals_seconds],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[MinimumFillSize],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,	
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'MM'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'MM TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueMMSettings]
           ([SymbolId]
           ,[MaximumSizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
		   ,[MinimumIntegratorPriceLife_milliseconds]
		   ,[MinimumSizeToTrade]
		   ,[MinimumFillSize]
		   ,[MaximumSourceBookTiers]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MaximumSizeToTrade 
           ,@BestPriceImprovementOffsetBasisPoints
           ,@MaximumDiscountBasisPointsToTrade
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
		   ,@MinimumIntegratorPriceLife_milliseconds
		   ,@MinimumSizeToTrade
		   ,@MinimumFillSize
		   ,@MaximumSourceBookTiers
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

ALTER PROCEDURE [dbo].[VenueStreamSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int,
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	DECLARE @MaxAllowedSystemsCount INT = 1
	DECLARE @FormedSystemsCount INT
	SELECT @FormedSystemsCount = COUNT([SymbolId])
	FROM [dbo].[VenueStreamSettings]
	WHERE [SymbolId] =  @SymbolId 

	IF @FormedSystemsCount >= @MaxAllowedSystemsCount
	BEGIN
		RAISERROR (N'Attempt to form more than %d systems for symbol %s - Disallowed', 16, 2, @MaxAllowedSystemsCount, @Symbol) WITH SETERROR
		RETURN
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Stream'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Stream TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueStreamSettings]
           ([SymbolId]
		   ,[CounterpartyId]
           ,[TradingSystemId]
           ,[MaximumSizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
		   ,[KGTLLTime_milliseconds]
		   ,[PreferLLDestinationCheckToLmax]
		   ,[MinimumBPGrossToAccept]
		   ,[PreferLmaxDuringHedging]
		   ,[MinimumIntegratorPriceLife_milliseconds]
		   ,[MinimumSizeToTrade]
		   ,[MinimumFillSize]
		   ,[MaximumSourceBookTiers])
     VALUES
           (@SymbolId
		   ,@CounterpartyId
           ,@TradingSystemId
           ,@MaximumSizeToTrade 
           ,@BestPriceImprovementOffsetBasisPoints
           ,@MaximumDiscountBasisPointsToTrade
		   ,@KGTLLTime_milliseconds
		   ,@PreferLLDestinationCheckToLmax
		   ,@MinimumBPGrossToAccept
		   ,@PreferLmaxDuringHedging
		   ,@MinimumIntegratorPriceLife_milliseconds
		   ,@MinimumSizeToTrade
		   ,@MinimumFillSize
		   ,@MaximumSourceBookTiers)
END
GO

ALTER PROCEDURE [dbo].[VenueCrossSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenSingleCounterpartySignals_seconds [decimal](18,9),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@MinimumFillSize [decimal](18,0),
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Cross'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Cross TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueCrossSettings]
           ([SymbolId]
           ,[MinimumSizeToTrade]
           ,[MaximumSizeToTrade]
           ,[MinimumDiscountBasisPointsToTrade]
           ,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
		   ,[MinimumFillSize]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MinimumSizeToTrade
           ,@MaximumSizeToTrade
           ,@MinimumDiscountBasisPointsToTrade
           ,@MinimumTimeBetweenSingleCounterpartySignals_seconds
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
		   ,@MinimumFillSize
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int
)
AS
BEGIN
	UPDATE sett
	SET
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds,
		[MinimumIntegratorPriceLife_milliseconds] = @MinimumIntegratorPriceLife_milliseconds,
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MinimumFillSize] = @MinimumFillSize,
		[MaximumSourceBookTiers] = @MaximumSourceBookTiers
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

ALTER PROCEDURE [dbo].[VenueStreamSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumSizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@KGTLLTime_milliseconds [int],
	@PreferLLDestinationCheckToLmax [bit],
	@MinimumBPGrossToAccept  [decimal](18,6),
	@PreferLmaxDuringHedging [bit],
	@MinimumIntegratorPriceLife_milliseconds [int],
	@MinimumSizeToTrade [decimal](18,0),
	@MinimumFillSize [decimal](18,0),
	@MaximumSourceBookTiers int
)
AS
BEGIN
	UPDATE sett
	SET
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[KGTLLTime_milliseconds] = @KGTLLTime_milliseconds,
		[PreferLLDestinationCheckToLmax] = @PreferLLDestinationCheckToLmax,
		[MinimumBPGrossToAccept] = @MinimumBPGrossToAccept,
		[PreferLmaxDuringHedging] = @PreferLmaxDuringHedging,
		[MinimumIntegratorPriceLife_milliseconds] = @MinimumIntegratorPriceLife_milliseconds,
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MinimumFillSize] = @MinimumFillSize,
		[MaximumSourceBookTiers] = @MaximumSourceBookTiers
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

ALTER PROCEDURE [dbo].[VenueCrossSystemSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenSingleCounterpartySignals_seconds [decimal](18,9),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@MinimumFillSize [decimal](18,0)
)
AS
BEGIN
	UPDATE sett
	SET
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[MinimumDiscountBasisPointsToTrade] = @MinimumDiscountBasisPointsToTrade,
		[MinimumTimeBetweenSingleCounterpartySignals_seconds] = @MinimumTimeBetweenSingleCounterpartySignals_seconds,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds,
		[MinimumFillSize] = @MinimumFillSize
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

--ROLLBACK TRANSACTION Install_10_02_001;
--GO
--COMMIT TRANSACTION Install_10_02_001;
--GO