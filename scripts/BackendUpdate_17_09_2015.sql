BEGIN TRANSACTION AAA

CREATE TABLE [dbo].[VenueLLCrossSettings](
	[SymbolId] [tinyint] NOT NULL,
	[MinimumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MaximumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MinimumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[MinimumTimeBetweenSingleCounterpartySignals_seconds] [decimal](18, 9) NULL,
	[MinimumFillSize] [decimal](18, 0) NOT NULL,
	[MinimumGrossGainBasisPointsToCover] [decimal](18, 6) NOT NULL,
	[CoverAttemptsCutoffSpan_milliseconds] [INT] NOT NULL, --5
	[SoftPriceTtl_milliseconds] [INT] NOT NULL, --500
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
 CONSTRAINT [UNQ__VenueLLCrossSettings__TradingSystemId] UNIQUE NONCLUSTERED 
(
	[TradingSystemId] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VenueLLCrossSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueLLCrossSettings_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueLLCrossSettings] CHECK CONSTRAINT [FK_VenueLLCrossSettings_Symbol]
GO

ALTER TABLE [dbo].[VenueLLCrossSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueLLCrossSettings_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueLLCrossSettings] CHECK CONSTRAINT [FK_VenueLLCrossSettings_Counterparty]
GO


CREATE TABLE [dbo].[VenueLLCrossSettings_Changes](
	[SymbolId] [tinyint] NOT NULL,
	[MinimumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MaximumSizeToTrade] [decimal](18, 0) NOT NULL,
	[MinimumDiscountBasisPointsToTrade] [decimal](18, 6) NOT NULL,
	[MinimumTimeBetweenSingleCounterpartySignals_seconds] [decimal](18, 9) NULL,
	[MinimumFillSize] [decimal](18, 0) NOT NULL,
	[MinimumGrossGainBasisPointsToCover] [decimal](18, 6) NOT NULL,
	[CoverAttemptsCutoffSpan_milliseconds] [INT] NOT NULL, --5
	[SoftPriceTtl_milliseconds] [INT] NOT NULL, --500
	[CounterpartyId] [tinyint] NOT NULL,
	[TradingSystemId] [int] NOT NULL,
	[ValidFromUtc] [datetime2](7) NOT NULL,
	[ValidToUtc] [datetime2](7) NOT NULL,
	[Deleted] [bit] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[VenueLLCrossSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueLLCrossSettings_Changes_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol_Internal] ([Id])
GO
ALTER TABLE [dbo].[VenueLLCrossSettings_Changes] CHECK CONSTRAINT [FK_VenueLLCrossSettings_Changes_Symbol]
GO

ALTER TABLE [dbo].[VenueLLCrossSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueLLCrossSettings_Changes_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[VenueLLCrossSettings_Changes] CHECK CONSTRAINT [FK_VenueLLCrossSettings_Changes_Counterparty]
GO

INSERT INTO [dbo].[TableChangeLog] ([TableName], [LastUpdatedUtc]) VALUES ('VenueLLCrossSettings', '1/1/1900')
GO


CREATE TRIGGER [dbo].[LastDeletedTrigger_VenueLLCrossSettings] ON [dbo].[VenueLLCrossSettings]
AFTER DELETE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON
UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueLLCrossSettings'

END
GO


CREATE TRIGGER [dbo].[LastUpdatedTrigger_VenueLLCrossSettings] ON [dbo].[VenueLLCrossSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueLLCrossSettings'

END
GO


CREATE TRIGGER [dbo].[VenueLLCrossSettings_DeletedOrUpdated] ON [dbo].[VenueLLCrossSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueLLCrossSettings_Changes]
	   ([SymbolId]
	   ,[MinimumSizeToTrade]
	   ,[MaximumSizeToTrade]
	   ,[MinimumDiscountBasisPointsToTrade]
	   ,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
	   ,[MinimumFillSize]
		,[MinimumGrossGainBasisPointsToCover]
		,[CoverAttemptsCutoffSpan_milliseconds]
		,[SoftPriceTtl_milliseconds]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[MinimumSizeToTrade]
		,deleted.[MaximumSizeToTrade]
		,deleted.[MinimumDiscountBasisPointsToTrade]
		,deleted.[MinimumTimeBetweenSingleCounterpartySignals_seconds]
		,deleted.[MinimumFillSize]
		,deleted.[MinimumGrossGainBasisPointsToCover]
		,deleted.[CoverAttemptsCutoffSpan_milliseconds]
		,deleted.[SoftPriceTtl_milliseconds]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueLLCrossSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueCrossSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([MinimumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MinimumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MinimumDiscountBasisPointsToTrade] != deleted.[MinimumDiscountBasisPointsToTrade]))
		OR (UPDATE([MinimumTimeBetweenSingleCounterpartySignals_seconds]) AND (topUpdatedChange.[MinimumTimeBetweenSingleCounterpartySignals_seconds] IS NULL OR topUpdatedChange.[MinimumTimeBetweenSingleCounterpartySignals_seconds] != deleted.[MinimumTimeBetweenSingleCounterpartySignals_seconds]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([MinimumGrossGainBasisPointsToCover]) AND (topUpdatedChange.[MinimumGrossGainBasisPointsToCover] IS NULL OR topUpdatedChange.[MinimumGrossGainBasisPointsToCover] != deleted.[MinimumGrossGainBasisPointsToCover]))
		OR (UPDATE([CoverAttemptsCutoffSpan_milliseconds]) AND (topUpdatedChange.[CoverAttemptsCutoffSpan_milliseconds] IS NULL OR topUpdatedChange.[CoverAttemptsCutoffSpan_milliseconds] != deleted.[CoverAttemptsCutoffSpan_milliseconds]))
		OR (UPDATE([SoftPriceTtl_milliseconds]) AND (topUpdatedChange.[SoftPriceTtl_milliseconds] IS NULL OR topUpdatedChange.[SoftPriceTtl_milliseconds] != deleted.[SoftPriceTtl_milliseconds]))
END
GO

INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTA'), 'LLCross')
GO



CREATE PROCEDURE [dbo].[GetUpdatedVenueLLCrossSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueLLCrossSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
			,[MinimumFillSize]
			,[MinimumGrossGainBasisPointsToCover]
			,[CoverAttemptsCutoffSpan_milliseconds]
			,[SoftPriceTtl_milliseconds]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueLLCrossSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name
	END
END
GO

GRANT EXECUTE ON [dbo].[GetUpdatedVenueLLCrossSystemSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetUpdatedVenueLLCrossSystemSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetVenueLLCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumDiscountBasisPointsToTrade] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumDiscountToTradeDecimal],
		sett.[MinimumTimeBetweenSingleCounterpartySignals_seconds],
		sett.[MinimumFillSize],
		sett.[MinimumGrossGainBasisPointsToCover],
		sett.[MinimumGrossGainBasisPointsToCover] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumGrossGainToCoverDecimal],
		sett.[CoverAttemptsCutoffSpan_milliseconds],
		sett.[SoftPriceTtl_milliseconds],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueLLCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

GRANT EXECUTE ON [dbo].[GetVenueLLCrossSystemSettingsAndStats_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROCEDURE [dbo].[GetVenueCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumDiscountBasisPointsToTrade] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumDiscountToTradeDecimal],
		sett.[MinimumTimeBetweenSingleCounterpartySignals_seconds],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		sett.[MinimumFillSize],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

CREATE PROCEDURE [dbo].[VenueLLCrossSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@MinimumSizeToTrade [decimal](18, 0),
	@MaximumSizeToTrade [decimal](18, 0),
	@MinimumDiscountBasisPointsToTrade [decimal](18, 6),
	@MinimumTimeBetweenSingleCounterpartySignals_seconds [decimal](18, 9),
	@MinimumFillSize [decimal](18, 0),
	@MinimumGrossGainBasisPointsToCover [decimal](18, 6),
	@CoverAttemptsCutoffSpan_milliseconds [INT],
	@SoftPriceTtl_milliseconds [INT],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'LLCross'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Stream TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueLLCrossSettings]
			([SymbolId]
			,[CounterpartyId]
			,[TradingSystemId]
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
			,[MinimumFillSize]
			,[MinimumGrossGainBasisPointsToCover]
			,[CoverAttemptsCutoffSpan_milliseconds]
			,[SoftPriceTtl_milliseconds])
     VALUES
			(@SymbolId
			,@CounterpartyId
			,@TradingSystemId
			,@MinimumSizeToTrade
			,@MaximumSizeToTrade
			,@MinimumDiscountBasisPointsToTrade
			,@MinimumTimeBetweenSingleCounterpartySignals_seconds
			,@MinimumFillSize
			,@MinimumGrossGainBasisPointsToCover
			,@CoverAttemptsCutoffSpan_milliseconds
			,@SoftPriceTtl_milliseconds)
END
GO

GRANT EXECUTE ON [dbo].[VenueLLCrossSettingsInsertSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[VenueLLCrossSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MinimumSizeToTrade [decimal](18, 0),
	@MaximumSizeToTrade [decimal](18, 0),
	@MinimumDiscountBasisPointsToTrade [decimal](18, 6),
	@MinimumTimeBetweenSingleCounterpartySignals_seconds [decimal](18, 9),
	@MinimumFillSize [decimal](18, 0),
	@MinimumGrossGainBasisPointsToCover [decimal](18, 6),
	@CoverAttemptsCutoffSpan_milliseconds [INT],
	@SoftPriceTtl_milliseconds [INT]
)
AS
BEGIN
	UPDATE sett
	SET
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[MinimumDiscountBasisPointsToTrade] = @MinimumDiscountBasisPointsToTrade,
		[MinimumTimeBetweenSingleCounterpartySignals_seconds] = @MinimumTimeBetweenSingleCounterpartySignals_seconds,
		[MinimumFillSize] = @MinimumFillSize,
		[MinimumGrossGainBasisPointsToCover] = @MinimumGrossGainBasisPointsToCover,
		[CoverAttemptsCutoffSpan_milliseconds] = @CoverAttemptsCutoffSpan_milliseconds,
		[SoftPriceTtl_milliseconds] = @SoftPriceTtl_milliseconds
	FROM
		[dbo].[VenueLLCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

GRANT EXECUTE ON [dbo].[VenueLLCrossSettingsUpdateSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetLastVenueLLCrossSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MinimumSizeToTrade]
		,[MaximumSizeToTrade]
		,[MinimumDiscountBasisPointsToTrade]
		,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
		,[MinimumFillSize]
		,[MinimumGrossGainBasisPointsToCover]
		,[CoverAttemptsCutoffSpan_milliseconds]
		,[SoftPriceTtl_milliseconds]
	FROM 
		[dbo].[VenueLLCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END
GO

GRANT EXECUTE ON [dbo].[GetLastVenueLLCrossSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROCEDURE [dbo].[VenueSystemDeleteOne_SP]
(
	@TradingSystemId int
)
AS
BEGIN

	BEGIN TRANSACTION;
	
	DECLARE @DeletedCnt INT = 0;
	
	DELETE sett
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
		
	DELETE sett
	FROM
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueLLCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	IF @DeletedCnt > 0
	BEGIN
		DELETE FROM
			[dbo].[TradingSystemActions]
		WHERE 
			[TradingSystemId] = @TradingSystemId
			
		DELETE FROM 
			[dbo].[TradingSystem]
		WHERE 
			[TradingSystemId] = @TradingSystemId
	END
	
	
	COMMIT TRANSACTION;
END
GO

ALTER PROCEDURE [dbo].[VenueSystemDeleteMulti_SP]
(
	@TradingSystemId IdsTable READONLY
)
AS
BEGIN

	BEGIN TRANSACTION;
	
	DECLARE @DeletedCnt INT = 0;
	
	DELETE sett
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueLLCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
		

	DELETE FROM
		[dbo].[TradingSystemActions]
	WHERE 
		[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
		
	DELETE FROM 
		[dbo].[TradingSystem]
	WHERE 
		[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	COMMIT TRANSACTION;
END
GO


ALTER TABLE [dbo].[PriceDelayStats] ADD [LastUpdatedUtc] [Datetime] NULL
GO

ALTER PROCEDURE [dbo].[UpdatePriceDelayStats_SP] 
(
	@CsvStatsArg VARCHAR(MAX)
)
AS
BEGIN

	UPDATE
		stats
	SET
		stats.[TotalReceivedPricesInLastMinute] = statsArg.num1,
		stats.[DelayedPricesInLastMinute] = statsArg.num2,
		stats.[TotalReceivedPricesInLastHour] = statsArg.num3,
		stats.[DelayedPricesInLastHour] = statsArg.num4,
		stats.[LastUpdatedUtc] = GETUTCDATE()
	FROM
		[dbo].[csvlist_to_codesandintquadruple_tbl](@CsvStatsArg) statsArg
		INNER JOIN [dbo].[Counterparty] ctp on ctp.[CounterpartyCode] = statsArg.Code
		INNER JOIN [dbo].[PriceDelayStats] stats on ctp.[CounterpartyId] = stats.[CounterpartyId] 
END
GO










--ROLLBACK TRANSACTION AAA
--GO
--COMMIT TRANSACTION AAA
--GO
