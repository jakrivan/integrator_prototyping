

/****** Object:  StoredProcedure [dbo].[GetPolarizedNopPerPair_SP]    Script Date: 4. 7. 2013 7:46:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPolarizedNopPerPair_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN
	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[ExecutedAmountBasePol]) AS PolarizedNop
	FROM 
		[dbo].[ExecutedExternalDeals] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[ExecutionTimeStampUtc] BETWEEN @StartTime AND @EndTime
	GROUP BY
		symbol.[Name]

END
GO

GO
GRANT EXECUTE ON [dbo].[GetPolarizedNopPerPair_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO