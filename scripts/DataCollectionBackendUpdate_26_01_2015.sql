

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_AUDCAD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_AUDCAD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_AUDCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_AUDCAD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_AUDCHF
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_AUDCHF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_AUDCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_AUDCHF
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_AUDDKK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_AUDDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_AUDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_AUDDKK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_AUDHKD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_AUDHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_AUDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_AUDHKD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_AUDJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_AUDJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_AUDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_AUDJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_AUDNOK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_AUDNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_AUDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_AUDNOK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_AUDNZD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_AUDNZD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_AUDNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_AUDNZD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_AUDSEK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_AUDSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_AUDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_AUDSEK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_AUDSGD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_AUDSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_AUDSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_AUDSGD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_AUDUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_AUDUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_AUDUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_AUDUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CADCHF
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CADCHF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CADCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CADCHF
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CADDKK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CADDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CADDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CADDKK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CADJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CADJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CADJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CADJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CADNOK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CADNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CADNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CADNOK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CADNZD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CADNZD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CADNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CADNZD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CADSEK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CADSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CADSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CADSEK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CHFDKK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CHFDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CHFDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CHFDKK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CHFJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CHFJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CHFJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CHFJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CHFNOK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CHFNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CHFNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CHFNOK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CHFSEK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CHFSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CHFSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CHFSEK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_DKKJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_DKKJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_DKKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_DKKJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_DKKNOK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_DKKNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_DKKNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_DKKNOK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_DKKSEK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_DKKSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_DKKSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_DKKSEK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURAUD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURAUD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURAUD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURCAD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURCAD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURCAD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURCHF
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURCHF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURCHF
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURCNH
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURCNH_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURCNH_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURCNH
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURCZK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURCZK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURCZK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURDKK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURDKK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURGBP
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURGBP_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURGBP_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURGBP
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURHKD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURHKD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURHUF
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURHUF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURHUF
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURMXN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURMXN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURNOK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURNOK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURNZD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURNZD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURNZD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURPLN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURPLN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURPLN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURRUB
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURRUB_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURRUB_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURRUB
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURSEK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURSEK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURTRY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURTRY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURTRY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURZAR
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURZAR
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPAUD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPAUD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPAUD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPCAD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPCAD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPCAD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPCHF
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPCHF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPCHF
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPCZK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPCZK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPCZK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPDKK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPDKK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPHUF
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPHUF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPHUF
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPNOK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPNOK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPNZD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPNZD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPNZD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPNZD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPPLN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPPLN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPPLN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPSEK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPSEK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_HKDJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_HKDJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_HKDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_HKDJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_MXNJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_MXNJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_MXNJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_MXNJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NOKJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NOKJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NOKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NOKJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NOKSEK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NOKSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NOKSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NOKSEK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NZDCAD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NZDCAD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NZDCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NZDCAD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NZDCHF
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NZDCHF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NZDCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NZDCHF
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NZDDKK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NZDDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NZDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NZDDKK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NZDJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NZDJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NZDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NZDJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NZDNOK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NZDNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NZDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NZDNOK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NZDSEK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NZDSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NZDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NZDSEK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NZDSGD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NZDSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NZDSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NZDSGD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NZDUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NZDUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NZDUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NZDUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_SGDJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_SGDJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_SGDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_SGDJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDCAD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDCAD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDCAD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDCAD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDCHF
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDCHF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDCHF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDCHF
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDCNH
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDCNH_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDCNH_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDCNH
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDCZK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDCZK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDCZK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDDKK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDDKK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDHKD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDHKD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDHUF
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDHUF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDHUF
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDILS
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDILS_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDILS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDILS
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDMXN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDMXN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDNOK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDNOK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDPLN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDPLN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDPLN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDRUB
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDRUB_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDRUB_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDRUB
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDSEK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDSEK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDSGD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDSGD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDTRY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDTRY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDTRY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDZAR
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDZAR
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_ZARJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_ZARJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_ZARJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_ZARJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURSGD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURSGD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPHKD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPHKD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPMXN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPMXN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPSGD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPSGD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPTRY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPTRY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPTRY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPZAR
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPZAR
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CHFMXN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CHFMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CHFMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CHFMXN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDINR
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDINR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDINR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDINR
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_AUDZAR
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_AUDZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_AUDZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_AUDZAR
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CADHKD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CADHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CADHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CADHKD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CADMXN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CADMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CADMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CADMXN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CADPLN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CADPLN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CADPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CADPLN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CADSGD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CADSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CADSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CADSGD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CADZAR
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CADZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CADZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CADZAR
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CZKJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CZKJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CZKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CZKJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_DKKHKD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_DKKHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_DKKHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_DKKHKD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_EURILS
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_EURILS_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_EURILS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_EURILS
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GBPILS
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GBPILS_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GBPILS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GBPILS
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_HUFJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_HUFJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_HUFJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_HUFJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CHFCZK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CHFCZK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CHFCZK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CHFCZK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CHFHKD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CHFHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CHFHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CHFHKD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CHFHUF
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CHFHUF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CHFHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CHFHUF
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CHFILS
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CHFILS_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CHFILS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CHFILS
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CHFPLN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CHFPLN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CHFPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CHFPLN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CHFSGD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CHFSGD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CHFSGD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CHFSGD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CHFTRY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CHFTRY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CHFTRY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CHFTRY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CHFZAR
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CHFZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CHFZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CHFZAR
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NOKHKD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NOKHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NOKHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NOKHKD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NZDHKD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NZDHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NZDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NZDHKD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NZDPLN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NZDPLN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NZDPLN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NZDPLN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NZDZAR
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NZDZAR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NZDZAR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NZDZAR
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_PLNHUF
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_PLNHUF_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_PLNHUF_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_PLNHUF
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_PLNJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_PLNJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_PLNJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_PLNJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_SEKHKD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_SEKHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_SEKHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_SEKHKD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_SEKJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_SEKJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_SEKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_SEKJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_SGDDKK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_SGDDKK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_SGDDKK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_SGDDKK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_SGDHKD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_SGDHKD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_SGDHKD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_SGDHKD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_SGDMXN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_SGDMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_SGDMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_SGDMXN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_SGDNOK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_SGDNOK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_SGDNOK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_SGDNOK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_SGDSEK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_SGDSEK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_SGDSEK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_SGDSEK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_TRYJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_TRYJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_TRYJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_TRYJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDGHS
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDGHS_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDGHS_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDGHS
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDKES
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDKES_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDKES_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDKES
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDNGN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDNGN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDNGN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDNGN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDRON
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDRON_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDRON_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDRON
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDUGX
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDUGX_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDUGX_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDUGX
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDZMK
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDZMK_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDZMK_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDZMK
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_ZARMXN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_ZARMXN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_ZARMXN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_ZARMXN
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDTHB
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDTHB_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDTHB_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDTHB
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_USDXVN
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_USDXVN_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_USDXVN_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_USDXVN
GO


--Carefull Schema needs 1 more filegroup (for far right range)
Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_FutureSymbols
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_FutureSymbols_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_FutureSymbols_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_FutureSymbols
GO



CREATE PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] AS PARTITION [MarketDataSymbolPartitioningFunction] TO (
	[OutgoingPriceFilegroup_EURUSD],
	[OutgoingPriceFilegroup_GBPUSD],
	[OutgoingPriceFilegroup_AUDJPY],
	[OutgoingPriceFilegroup_AUDUSD],
	[OutgoingPriceFilegroup_CHFJPY],
	[OutgoingPriceFilegroup_EURAUD],
	[OutgoingPriceFilegroup_EURCHF],
	[OutgoingPriceFilegroup_EURGBP],
	[OutgoingPriceFilegroup_EURJPY],
	[OutgoingPriceFilegroup_GBPCHF],
	[OutgoingPriceFilegroup_GBPJPY],
	[OutgoingPriceFilegroup_NZDUSD],
	[OutgoingPriceFilegroup_USDCAD],
	[OutgoingPriceFilegroup_USDCHF],
	[OutgoingPriceFilegroup_USDJPY],
	[OutgoingPriceFilegroup_NZDJPY],
	[OutgoingPriceFilegroup_CADJPY],
	[OutgoingPriceFilegroup_EURCAD],
	[OutgoingPriceFilegroup_EURSEK],
	[OutgoingPriceFilegroup_USDDKK],
	[OutgoingPriceFilegroup_USDPLN],
	[OutgoingPriceFilegroup_USDNOK],
	[OutgoingPriceFilegroup_USDCZK],
	[OutgoingPriceFilegroup_AUDNZD],
	[OutgoingPriceFilegroup_EURNOK],
	[OutgoingPriceFilegroup_AUDCAD],
	[OutgoingPriceFilegroup_AUDCHF],
	[OutgoingPriceFilegroup_CADCHF],
	[OutgoingPriceFilegroup_EURCZK],
	[OutgoingPriceFilegroup_EURHUF],
	[OutgoingPriceFilegroup_EURNZD],
	[OutgoingPriceFilegroup_EURPLN],
	[OutgoingPriceFilegroup_GBPAUD],
	[OutgoingPriceFilegroup_GBPCAD],
	[OutgoingPriceFilegroup_USDSEK],
	[OutgoingPriceFilegroup_USDMXN],
	[OutgoingPriceFilegroup_EURDKK],
	[OutgoingPriceFilegroup_USDHUF],
	[OutgoingPriceFilegroup_CADNZD],
	[OutgoingPriceFilegroup_GBPNZD],
	[OutgoingPriceFilegroup_NZDCHF],
	[OutgoingPriceFilegroup_AUDDKK],
	[OutgoingPriceFilegroup_AUDNOK],
	[OutgoingPriceFilegroup_EURTRY],
	[OutgoingPriceFilegroup_EURZAR],
	[OutgoingPriceFilegroup_NOKSEK],
	[OutgoingPriceFilegroup_NZDCAD],
	[OutgoingPriceFilegroup_USDHKD],
	[OutgoingPriceFilegroup_USDSGD],
	[OutgoingPriceFilegroup_USDTRY],
	[OutgoingPriceFilegroup_EURMXN],
	[OutgoingPriceFilegroup_USDZAR],
	[OutgoingPriceFilegroup_AUDSEK],
	[OutgoingPriceFilegroup_CADDKK],
	[OutgoingPriceFilegroup_CADNOK],
	[OutgoingPriceFilegroup_CADSEK],
	[OutgoingPriceFilegroup_CHFDKK],
	[OutgoingPriceFilegroup_CHFNOK],
	[OutgoingPriceFilegroup_CHFSEK],
	[OutgoingPriceFilegroup_DKKJPY],
	[OutgoingPriceFilegroup_DKKNOK],
	[OutgoingPriceFilegroup_DKKSEK],
	[OutgoingPriceFilegroup_GBPDKK],
	[OutgoingPriceFilegroup_GBPNOK],
	[OutgoingPriceFilegroup_GBPSEK],
	[OutgoingPriceFilegroup_HKDJPY],
	[OutgoingPriceFilegroup_MXNJPY],
	[OutgoingPriceFilegroup_NOKJPY],
	[OutgoingPriceFilegroup_NZDDKK],
	[OutgoingPriceFilegroup_NZDNOK],
	[OutgoingPriceFilegroup_NZDSEK],
	[OutgoingPriceFilegroup_AUDSGD],
	[OutgoingPriceFilegroup_ZARJPY],
	[OutgoingPriceFilegroup_NZDSGD],
	[OutgoingPriceFilegroup_SGDJPY],
	[OutgoingPriceFilegroup_USDILS],
	[OutgoingPriceFilegroup_AUDHKD],
	[OutgoingPriceFilegroup_EURHKD],
	[OutgoingPriceFilegroup_EURRUB],
	[OutgoingPriceFilegroup_GBPCZK],
	[OutgoingPriceFilegroup_GBPHUF],
	[OutgoingPriceFilegroup_USDRUB],
	[OutgoingPriceFilegroup_GBPPLN],
	[OutgoingPriceFilegroup_EURCNH],
	[OutgoingPriceFilegroup_USDCNH],
	[OutgoingPriceFilegroup_EURSGD],
	[OutgoingPriceFilegroup_GBPHKD],
	[OutgoingPriceFilegroup_GBPMXN],
	[OutgoingPriceFilegroup_GBPSGD],
	[OutgoingPriceFilegroup_GBPTRY],
	[OutgoingPriceFilegroup_GBPZAR],
	[OutgoingPriceFilegroup_CHFMXN],
	[OutgoingPriceFilegroup_USDINR],
	[OutgoingPriceFilegroup_AUDZAR],
	[OutgoingPriceFilegroup_CADHKD],
	[OutgoingPriceFilegroup_CADMXN],
	[OutgoingPriceFilegroup_CADPLN],
	[OutgoingPriceFilegroup_CADSGD],
	[OutgoingPriceFilegroup_CADZAR],
	[OutgoingPriceFilegroup_CZKJPY],
	[OutgoingPriceFilegroup_DKKHKD],
	[OutgoingPriceFilegroup_EURILS],
	[OutgoingPriceFilegroup_GBPILS],
	[OutgoingPriceFilegroup_HUFJPY],
	[OutgoingPriceFilegroup_CHFCZK],
	[OutgoingPriceFilegroup_CHFHKD],
	[OutgoingPriceFilegroup_CHFHUF],
	[OutgoingPriceFilegroup_CHFILS],
	[OutgoingPriceFilegroup_CHFPLN],
	[OutgoingPriceFilegroup_CHFSGD],
	[OutgoingPriceFilegroup_CHFTRY],
	[OutgoingPriceFilegroup_CHFZAR],
	[OutgoingPriceFilegroup_NOKHKD],
	[OutgoingPriceFilegroup_NZDHKD],
	[OutgoingPriceFilegroup_NZDPLN],
	[OutgoingPriceFilegroup_NZDZAR],
	[OutgoingPriceFilegroup_PLNHUF],
	[OutgoingPriceFilegroup_PLNJPY],
	[OutgoingPriceFilegroup_SEKHKD],
	[OutgoingPriceFilegroup_SEKJPY],
	[OutgoingPriceFilegroup_SGDDKK],
	[OutgoingPriceFilegroup_SGDHKD],
	[OutgoingPriceFilegroup_SGDMXN],
	[OutgoingPriceFilegroup_SGDNOK],
	[OutgoingPriceFilegroup_SGDSEK],
	[OutgoingPriceFilegroup_TRYJPY],
	[OutgoingPriceFilegroup_USDGHS],
	[OutgoingPriceFilegroup_USDKES],
	[OutgoingPriceFilegroup_USDNGN],
	[OutgoingPriceFilegroup_USDRON],
	[OutgoingPriceFilegroup_USDUGX],
	[OutgoingPriceFilegroup_USDZMK],
	[OutgoingPriceFilegroup_ZARMXN],
	[OutgoingPriceFilegroup_USDTHB],
	[OutgoingPriceFilegroup_USDXVN],
	[OutgoingPriceFilegroup_FutureSymbols])
GO



--decided to partition only by Symbol because:
--  - partition must be on a single column. It can be computed column (from multiple), but must be persisted in that case. So we would be taking space
--  - there used to be 1k partitions limit for a long time, so large amount of partitions can cause some perf. downgrades
--  - today 15k of partitions is till less than smallint range and so trivial conversion of two tinnyint columns to one column can blow it
--  - Prices on single symbol during similar time range should be similar even on different venues.
--  - Easier to maintain (as other two tables has same partition function and same number of partitions)
CREATE TABLE [dbo].[OutgoingSoftPrice](
	[RecordID] [bigint] IDENTITY(1,1) NOT NULL,
	[FXPairId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SideId] [bit] NOT NULL,
	[Price] [decimal](18, 9) NULL,
	[SizeBaseAbs] [decimal](18, 2) NULL,
	--for future use
	--[MinSizeBaseAbs] [decimal](18, 2) NULL,
	--[SizeBaseAbsGranularity] [decimal](18, 2) NULL,
	[TradingSystemId] [int] NULL,
	[LayerOrderId] [tinyint] NOT NULL,
	[IntegratorSentTimeUtc] [datetime2](7) NOT NULL,
	[IntegratorPriceIdentity] [varchar](20) NULL
) 
ON OutgoingPriceSymbolPartitionScheme_IndividualFileGroups([FXPairId])
GO

ALTER TABLE [dbo].[OutgoingSoftPrice]  WITH NOCHECK ADD  CONSTRAINT [FK_OutgoingSoftPrice_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO

ALTER TABLE [dbo].[OutgoingSoftPrice] CHECK CONSTRAINT [FK_OutgoingSoftPrice_FxPair]
GO

ALTER TABLE [dbo].[OutgoingSoftPrice]  WITH CHECK ADD  CONSTRAINT [FK_OutgoingSoftPrice_LiqProviderStream] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO

ALTER TABLE [dbo].[OutgoingSoftPrice] CHECK CONSTRAINT [FK_OutgoingSoftPrice_LiqProviderStream]
GO

ALTER TABLE [dbo].[OutgoingSoftPrice]  WITH CHECK ADD  CONSTRAINT [FK_OutgoingSoftPrice_PriceSide] FOREIGN KEY([SideId])
REFERENCES [dbo].[PriceSide] ([PriceSideId])
GO

ALTER TABLE [dbo].[OutgoingSoftPrice] CHECK CONSTRAINT [FK_OutgoingSoftPrice_PriceSide]
GO

GRANT INSERT ON [dbo].[OutgoingSoftPrice] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[OutgoingSoftPrice] TO [IntegratorServiceAccount] AS [dbo]
GO


CREATE CLUSTERED INDEX [IX_PairCounterpartyTime] ON [dbo].[OutgoingSoftPrice]
(
	[FXPairId] ASC,
	[IntegratorSentTimeUtc] ASC
) 
ON OutgoingPriceSymbolPartitionScheme_IndividualFileGroups([FXPairId])
GO



