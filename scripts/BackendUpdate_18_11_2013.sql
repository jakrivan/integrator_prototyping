
CREATE PROCEDURE [dbo].[TryLoginRiskMgmtUser_SP] 
	@username NVARCHAR(256),
	@password NVARCHAR(256),
	@identity NVARCHAR(256)
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @t TABLE (Loged BIT, IsAdmin BIT, SiteName nvarchar(64))
	DECLARE @Loged BIT
	DECLARE @IsAdmin BIT
	DECLARE @SiteName NVARCHAR(64)
	DECLARE @SiteId INT
	SET @Loged = 0
	SET @IsAdmin = 0
	SET @SiteId = 1

	SELECT 
		@Loged = 1, @IsAdmin = usr.IsAdmin, @SiteName = s.Name, @SiteId = s.Id
	FROM 
		[dbo].[KillSwitchUser] usr
		INNER JOIN [dbo].[KillSwitchSite] s ON usr.SiteId = s.Id
	WHERE
		usr.UserName = @username AND usr.Password = @password


	INSERT INTO 
		[dbo].[KillSwitchLogon]
           ([UserName]
           ,[ActionTime]
           ,[Source]
           ,[Success]
		   ,[SiteId])
     VALUES
           (@username
           ,GETUTCDATE()
           ,@identity
           ,@Loged
		   ,@SiteId)

	INSERT INTO @t VALUES (@Loged,@IsAdmin,@SiteName)
	SELECT Loged, IsAdmin, SiteName FROM @t
END
GO

GRANT EXECUTE ON [dbo].[TryLoginRiskMgmtUser_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[GetDetailsForRiskMgmtUser_SP] 
	@username NVARCHAR(256)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @True BIT = 1

	SELECT 
		@True as Loged, usr.IsAdmin as IsAdmin, s.Name as SiteName
	FROM 
		[dbo].[KillSwitchUser] usr
		INNER JOIN [dbo].[KillSwitchSite] s ON usr.SiteId = s.Id
	WHERE
		usr.UserName = @username
END
GO

GRANT EXECUTE ON [dbo].[GetDetailsForRiskMgmtUser_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

DROP INDEX [IX_DatePart_Symbol] ON [dbo].[DealExternalExecuted]
GO

ALTER TABLE DealExternalExecuted ALTER COLUMN AmountTermPolExecutedInUsd decimal(18,2)
GO

SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO

/****** Object:  Index [IX_DatePart_Symbol]    Script Date: 15. 11. 2013 16:24:00 ******/
CREATE NONCLUSTERED INDEX [IX_DatePart_Symbol] ON [dbo].[DealExternalExecuted]
(
	[DatePartIntegratorReceivedExecutionReportUtc] ASC,
	[SymbolId] ASC
)
INCLUDE ( 	[AmountBasePolExecuted],
	[Price],
	[AmountTermPolExecutedInUsd],
	[CounterpartyId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO




ALTER TABLE [dbo].[Currency] ADD LastKnownUsdConversionMultiplier decimal(18,9) NULL
GO

ALTER TABLE [dbo].[Currency] ADD UsdConversionMultiplierUpdated datetime2 NULL
GO

CREATE PROC [dbo].[UpdateUsdConversionMultiplier_SP] (
	@CurrencyCode NCHAR(3),
	@ConversionMultiplier decimal(18,9)
	)
AS
BEGIN
	UPDATE [dbo].[Currency]
	SET
      [LastKnownUsdConversionMultiplier] = @ConversionMultiplier
      ,[UsdConversionMultiplierUpdated] = GETUTCDATE()
	WHERE 
		[CurrencyAlphaCode] = @CurrencyCode
END
GO

GRANT EXECUTE ON [dbo].[UpdateUsdConversionMultiplier_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


UPDATE 
	CCY
SET 
	CCY.[LastKnownUsdConversionMultiplier] = UsdMultipliers.UsdConversionMultiplier
	,CCY.[UsdConversionMultiplierUpdated] = UsdMultipliers.ConversionMultiplierLastUpdated
FROM
	[dbo].[Currency] CCY
	INNER JOIN
	(
		SELECT 
			[CurrencyID]
			,s.LastKnownMeanReferencePrice AS UsdConversionMultiplier
			,s.MeanReferencePriceUpdated AS ConversionMultiplierLastUpdated
		FROM [dbo].[Currency] c
		INNER JOIN Symbol s ON c.[CurrencyID] = s.BaseCurrencyId AND s.QuoteCurrencyId = 137

		UNION ALL

		SELECT 
			[CurrencyID]
			,1 / s.LastKnownMeanReferencePrice AS UsdConversionMultiplier
			,s.MeanReferencePriceUpdated AS ConversionMultiplierLastUpdated
		FROM [dbo].[Currency] c
		INNER JOIN Symbol s ON c.[CurrencyID] = s.QuoteCurrencyId AND s.BaseCurrencyId = 137
	) UsdMultipliers ON UsdMultipliers.[CurrencyID] = CCY.[CurrencyID]
GO

UPDATE 
	[dbo].[Currency]
SET 
	[LastKnownUsdConversionMultiplier] = 1,
	[UsdConversionMultiplierUpdated] = GetUtcDate()
WHERE
	[CurrencyAlphaCode] = 'USD'
GO



----------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[GetNopAbsTotal2_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		symbol.[Id] AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.Price) AS NopTermPol
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Id]


	SELECT
		SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
	FROM
		(
		SELECT
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
		GROUP BY
			CCYPositions.CCY
		) AS CtpCCYPositions

END
GO

GRANT EXECUTE ON [dbo].[GetNopAbsTotal2_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO




CREATE PROCEDURE [dbo].[GetNopsPolSidedPerCurrency2_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,0),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		symbol.[Id] AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Id]


	SELECT
		CCYPositions.CCY AS CCY,
		SUM(CCYPositions.NopPol) AS NopPol,
		SUM(CCYPositions.NopPolInUsd) AS NopPolInUsd
	FROM
		(
		SELECT 
			ccy1.CurrencyAlphaCode AS CCY,
			basePos.NopBasePol AS NopPol,
			basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
		FROM
			Currency ccy1
			INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
			INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

		UNION ALL

		SELECT 
			ccy2.CurrencyAlphaCode AS CCY,
			termPos.NopTermPol AS NopPol,
			termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
		FROM
			Currency ccy2
			INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
			INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
		) AS CCYPositions
	GROUP BY
		CCYPositions.CCY
	ORDER BY
		CCYPositions.CCY
END
GO

GRANT EXECUTE ON [dbo].[GetNopsPolSidedPerCurrency2_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[GetNopsAbsPerCounterparty2_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol)
	SELECT
		symbol.[Id] AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
	GROUP BY
		symbol.[Id],
		deal.CounterpartyId
		


	SELECT
		CtpCCYPositions.Counterparty AS Counterparty,
		SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs
		--SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
	FROM
		(
		SELECT
			ctp.CounterpartyCode AS Counterparty,
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			Counterparty ctp
			LEFT JOIN
			(
			SELECT 
				basePos.CounterpartyId,
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT 
				termPos.CounterpartyId,
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions ON ctp.CounterpartyId = CCYPositions.CounterpartyId
		WHERE
			ctp.Active = 1
		GROUP BY
			ctp.CounterpartyCode
			,CCYPositions.CCY
		) AS CtpCCYPositions
	GROUP BY
		CtpCCYPositions.Counterparty
	ORDER BY
		CtpCCYPositions.Counterparty
END
GO

GRANT EXECUTE ON [dbo].[GetNopsAbsPerCounterparty2_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO



-------------------------------------------------------------------------------


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings_New_NoSchema](
	[Name] [nvarchar](100) NOT NULL,
	[SettingsXml] [xml] NOT NULL,
	[LastUpdated] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

INSERT INTO [dbo].[Settings_New_NoSchema] (Name, SettingsXml, LastUpdated) SELECT Name, CONVERT(XML, SettingsXml), LastUpdated FROM [dbo].[Settings] WHERE [Name] = 'Kreslik.Integrator.RiskManagement.dll'
GO


DROP TABLE [dbo].[Settings]
GO

DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.XsdSchemas]
GO


/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.XsdSchemas]    Script Date: 09. 11. 2013 14:05:16 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.XsdSchemas] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RiskManagementSettings" nillable="true" type="RiskManagementSettings" />
  <xs:complexType name="RiskManagementSettings">
    <xs:sequence>
      <xs:element name="ConfigLastUpdated"  nillable="true" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAndPosition" type="PriceAndPositionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersSubmissionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrderMaximumAllowedSizeSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedBaseAbsSizeInUsd" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="PriceAndPositionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumExternalNOPInUSD" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceDeviationCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="OnlinePriceDeviationCheckMaxPriceAge_Minutes" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="Symbols" type="ArrayOfSymbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbol">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="Symbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="Symbol">
    <xs:attribute name="ShortName" type="xs:string" />
    <xs:attribute name="NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
  </xs:complexType>
</xs:schema>

<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="BusinessLayerSettings" nillable="true" type="BusinessLayerSettings" />
  <xs:complexType name="BusinessLayerSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MarketableClientOrdersMatchingStrategy" type="MarketableClientOrdersMatchingStrategy" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="ConsecutiveRejectionsStopStrategyBehavior" type="ConsecutiveRejectionsStopStrategySettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartWithSessionsStopped" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="AutoKillInternalOrdersOnOrdersTransmissionDisabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailSubject" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailBody" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderCounterpartyContacts" type="ArrayOfIgnoredOrderCounterpartyContact" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailMinDelayBetweenEmails_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="MarketableClientOrdersMatchingStrategy">
    <xs:restriction base="xs:string">
      <xs:enumeration value="ImmediateMatching" />
      <xs:enumeration value="NextFreshPriceMatching" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ConsecutiveRejectionsStopStrategySettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="StrategyEnabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="NumerOfRejectionsToTriggerStrategy" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAgeRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceBpRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultCounterpartyRank" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRanks" type="ArrayOfCounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartyRank">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartyRank" nillable="true" type="CounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartyRank">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="Rank" type="xs:decimal" />
  </xs:complexType>
  <xs:complexType name="ArrayOfIgnoredOrderCounterpartyContact">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="IgnoredOrderCounterpartyContact" nillable="true" type="IgnoredOrderCounterpartyContact" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="IgnoredOrderCounterpartyContact">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="EmailContacts" type="xs:string" />
  </xs:complexType>
</xs:schema>'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[Name] [nvarchar](100) NOT NULL,
	[SettingsXml] [xml](CONTENT [dbo].[Kreslik.Integrator.XsdSchemas]) NOT NULL,
	[LastUpdated] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


INSERT INTO [dbo].[Settings] (Name, SettingsXml, LastUpdated) SELECT Name, CONVERT(XML, SettingsXml), LastUpdated FROM [dbo].[Settings_New_NoSchema] WHERE [Name] = 'Kreslik.Integrator.RiskManagement.dll'
GO

DROP TABLE [dbo].[Settings_New_NoSchema]
GO

INSERT INTO [dbo].[Settings]
           ([Name]
           ,[SettingsXml]
		   ,[LastUpdated])
     VALUES
           (N'Kreslik.Integrator.BusinessLayer.dll'
           ,
N'<BusinessLayerSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <MarketableClientOrdersMatchingStrategy>ImmediateMatching</MarketableClientOrdersMatchingStrategy>
  <ConsecutiveRejectionsStopStrategyBehavior>
    <StrategyEnabled>true</StrategyEnabled>
    <NumerOfRejectionsToTriggerStrategy>1</NumerOfRejectionsToTriggerStrategy>
    <CounterpartyRankWeightMultiplier>20</CounterpartyRankWeightMultiplier>
    <PriceAgeRankWeightMultiplier>1</PriceAgeRankWeightMultiplier>
    <PriceBpRankWeightMultiplier>20</PriceBpRankWeightMultiplier>
    <DefaultCounterpartyRank>7</DefaultCounterpartyRank>
    <CounterpartyRanks>
      <CounterpartyRank CounterpartyCode="UBS" Rank="5" />
      <CounterpartyRank CounterpartyCode="MGS" Rank="5.2" />
      <CounterpartyRank CounterpartyCode="CTI" Rank="5.8" />
      <CounterpartyRank CounterpartyCode="CRS" Rank="6.1" />
      <CounterpartyRank CounterpartyCode="BOA" Rank="6.8" />
      <CounterpartyRank CounterpartyCode="CZB" Rank="7.1" />
      <CounterpartyRank CounterpartyCode="GLS" Rank="7.4" />
      <CounterpartyRank CounterpartyCode="JPM" Rank="7.7" />
      <CounterpartyRank CounterpartyCode="BNP" Rank="8.9" />
      <CounterpartyRank CounterpartyCode="RBS" Rank="9.4" />
      <CounterpartyRank CounterpartyCode="BRX" Rank="9.5" />
      <CounterpartyRank CounterpartyCode="SOC" Rank="9.6" />
      <CounterpartyRank CounterpartyCode="HSB" Rank="9.9" />
      <CounterpartyRank CounterpartyCode="NOM" Rank="10" />
    </CounterpartyRanks>
  </ConsecutiveRejectionsStopStrategyBehavior>
  <StartWithSessionsStopped>false</StartWithSessionsStopped>
  <AutoKillInternalOrdersOnOrdersTransmissionDisabled>true</AutoKillInternalOrdersOnOrdersTransmissionDisabled>
  <StartStopEmailSubject>Integrator service on {0} is {1}</StartStopEmailSubject>
  <StartStopEmailBody></StartStopEmailBody>
  <StartStopEmailTo>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</StartStopEmailTo>
  <StartStopEmailCc></StartStopEmailCc>
  <IgnoredOrderCounterpartyContacts>
    <IgnoredOrderCounterpartyContact CounterpartyCode="CRS" EmailContacts="list.efx-support@credit-suisse.com;ivan.dexeus@credit-suisse.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="UBS" EmailContacts="fxehelp@ubs.com;gianandrea.grassi@ubs.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="DBK" EmailContacts="gm-ecommerce.implementations@db.com;thomas-m.geist@db.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="CTI" EmailContacts="fifx.support@citi.com;kenneth.aina@citi.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="BOA" EmailContacts="efx.csg@baml.com;kenneth.aina@citi.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="MGS" EmailContacts="fxacctmgt@morganstanley.com;guy.hopkins@morganstanley.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="RBS" EmailContacts="GBMEMFXTradeTimeouts@rbs.com;Martin.Pilcher@rbs.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="HSB" EmailContacts="fix.support@hsbcib.com;olivier.werenne@hsbcib.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="JPM" EmailContacts="JPMorgan_FXEcom_Urgent@jpmorgan.com;jon.price@jpmorgan.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="GLS" EmailContacts="ficc-fx-edealing-integration@gs.com;tom.robinson@gs.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="BNP" EmailContacts="siamak.nouri@uk.bnpparibas.com;steve.earl@uk.bnpparibas.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="NOM" EmailContacts="FI-ECommerceTechFXSupport@nomura.com;nicola.dilena@nomura.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="CZB" EmailContacts="esupport@commerzbank.com;Roland.White@commerzbank.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="BRX" EmailContacts="BARXsupport@barclays.com;conor.daly@barclays.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="SOC" EmailContacts="FX-support@sgcib.com;leon.brown@sgcib.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="HTA" EmailContacts="HOTFX.PRODSUPPORT@knight.com;HOTFX.LIQUIDITY@knight.com;pgertler@kcg.com" />
    <IgnoredOrderCounterpartyContact CounterpartyCode="HTF" EmailContacts="HOTFX.PRODSUPPORT@knight.com;HOTFX.LIQUIDITY@knight.com;pgertler@kcg.com" />
  </IgnoredOrderCounterpartyContacts>
  <IgnoredOrderEmailCc>michal.kreslik@kgtinv.com;marek.fogiel@kgtinv.com;jan.krivanek@kgtinv.com</IgnoredOrderEmailCc>
  <FatalErrorsEmailTo>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</FatalErrorsEmailTo>
  <FatalErrorsEmailCc></FatalErrorsEmailCc>
  <FatalErrorsEmailMinDelayBetweenEmails_Minutes>10</FatalErrorsEmailMinDelayBetweenEmails_Minutes>
</BusinessLayerSettings>'
,
GETUTCDATE())
GO