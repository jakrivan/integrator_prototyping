//------------------------------------------------------------------------------
// <copyright file="CSSqlFunction.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

public partial class UserDefinedFunctions
{
    private static readonly TimeZoneInfo _easternTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
    private static readonly TimeZoneInfo _centralEuropeTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time");
    private static readonly TimeZoneInfo _nzTimeZone = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");

    //[Microsoft.SqlServer.Server.SqlFunction]
    //public static SqlString SqlFunction1()
    //{
    //    // Put your code here
    //    return new SqlString (string.Empty);
    //}

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDateTime ConvertFromUtcToEt(SqlDateTime utcTime)
    {
        if (utcTime.IsNull)
            return utcTime;
        return TimeZoneInfo.ConvertTimeFromUtc((DateTime) utcTime, _easternTimeZone);
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDateTime ConvertFromEtToUtc(SqlDateTime etTime)
    {
        if (etTime.IsNull)
            return etTime;
        return TimeZoneInfo.ConvertTimeToUtc((DateTime) etTime, _easternTimeZone);
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDateTime ConvertFromUtcToNzt(SqlDateTime utcTime)
    {
        if (utcTime.IsNull)
            return utcTime;
        return TimeZoneInfo.ConvertTimeFromUtc((DateTime) utcTime, _nzTimeZone);
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDateTime GetNztNow()
    {
        return ConvertFromUtcToNzt(DateTime.UtcNow);
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDateTime GetEtNow()
    {
        return ConvertFromUtcToEt(DateTime.UtcNow);
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDateTime ConvertFromUtcToCet(SqlDateTime utcTime)
    {
        if (utcTime.IsNull)
            return utcTime;
        return TimeZoneInfo.ConvertTimeFromUtc((DateTime) utcTime, _centralEuropeTimeZone);
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDateTime GetCetNow()
    {
        return ConvertFromUtcToCet(DateTime.UtcNow);
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDateTime ConvertFromCetToUtc(SqlDateTime cetTime)
    {
        if (cetTime.IsNull)
            return cetTime;
        return TimeZoneInfo.ConvertTimeToUtc((DateTime) cetTime, _centralEuropeTimeZone);
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDateTime ConvertFromCetToEt(SqlDateTime cetTime)
    {
        if (cetTime.IsNull)
            return cetTime;
        return ConvertFromUtcToEt(ConvertFromCetToUtc((DateTime) cetTime));
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDateTime ConvertFromEtToCet(SqlDateTime etTime)
    {
        if (etTime.IsNull)
            return etTime;
        return ConvertFromUtcToCet(ConvertFromEtToUtc(etTime));
    }
}
