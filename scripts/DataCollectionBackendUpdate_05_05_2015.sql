
sp_configure 'show advanced options', 1;
GO
RECONFIGURE;
GO
sp_configure 'clr enabled', 1;
GO
RECONFIGURE;
GO

CREATE ASSEMBLY CLRProcedures FROM 'C:\SQLDATA\Assemblies\BackendCLRProcedures.dll' WITH PERMISSION_SET = UNSAFE
GO

CREATE FUNCTION ConvertFromUtcToEt (@utcTime DATETIME)
RETURNS DATETIME
AS
EXTERNAL NAME CLRProcedures.UserDefinedFunctions.ConvertFromUtcToEt
GO

CREATE FUNCTION ConvertFromEtToUtc (@utcTime DATETIME)
RETURNS DATETIME
AS
EXTERNAL NAME CLRProcedures.UserDefinedFunctions.ConvertFromEtToUtc
GO

CREATE FUNCTION ConvertFromUtcToNzt (@etTime DATETIME)
RETURNS DATETIME
AS
EXTERNAL NAME CLRProcedures.UserDefinedFunctions.ConvertFromUtcToNzt
GO

CREATE FUNCTION ConvertFromUtcToCet (@utcTime DATETIME)
RETURNS DATETIME
AS
EXTERNAL NAME CLRProcedures.UserDefinedFunctions.ConvertFromUtcToCet
GO

CREATE FUNCTION ConvertFromCetToUtc (@cetTime DATETIME)
RETURNS DATETIME
AS
EXTERNAL NAME CLRProcedures.UserDefinedFunctions.ConvertFromCetToUtc
GO

CREATE FUNCTION ConvertFromCetToEt (@cetTime DATETIME)
RETURNS DATETIME
AS
EXTERNAL NAME CLRProcedures.UserDefinedFunctions.ConvertFromCetToEt
GO

CREATE FUNCTION ConvertFromEtToCet (@etTime DATETIME)
RETURNS DATETIME
AS
EXTERNAL NAME CLRProcedures.UserDefinedFunctions.ConvertFromEtToCet
GO

CREATE FUNCTION GetNztNow ()
RETURNS DATETIME
AS
EXTERNAL NAME CLRProcedures.UserDefinedFunctions.GetNztNow
GO

CREATE FUNCTION GetEtNow ()
RETURNS DATETIME
AS
EXTERNAL NAME CLRProcedures.UserDefinedFunctions.GetEtNow
GO

CREATE FUNCTION GetCetNow ()
RETURNS DATETIME
AS
EXTERNAL NAME CLRProcedures.UserDefinedFunctions.GetCetNow
GO


DECLARE @ConvertedETTime DATETIME
DECLARE @UtcTimeToConvert datetime = GETUTCDATE()
exec [dev03].[DataCollection_Testing].[dbo].sp_executesql N'SELECT @output = [DataCollection_Testing].[dbo].[ConvertFromUtcToEt](@input)',N'@input datetime, @output datetime OUTPUT',@input=@UtcTimeToConvert,@output=@ConvertedETTime OUTPUT
SELECT @ConvertedETTime



CREATE TABLE [dbo].[EODRate](
	[FXPairId] [tinyint] NOT NULL,
	[Date] [Date] NOT NULL,
	[BidPrice] [decimal](18, 9) NULL,
	[AskPrice] [decimal](18, 9) NULL,
	[MidPrice] AS ([AskPrice] + [BidPrice]) / 2,
	[IsBidPriceAuthentic] [bit] NOT NULL,
	[IsAskPriceAuthentic] [bit] NOT NULL,
	[IsMidPriceAuthentic]  AS (case when [IsBidPriceAuthentic]=(1) AND [IsAskPriceAuthentic]=(1) then (1) else (0) end)
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[EODRate]  WITH NOCHECK ADD  CONSTRAINT [FK_EODRate_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[EODRate] CHECK CONSTRAINT [FK_EODRate_FxPair]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_EODRate_DatePair] ON [dbo].[EODRate]
(
	[Date] ASC,
	[FXPairId] ASC
)
GO

CREATE PROCEDURE CalculateSingleDayEodRates_SP
(
	@Day DATE = NULL,
	@DeleteExisting BIT = 1,
	@InsertSameDataForFollowingDay BIT = 1
)
AS
BEGIN
	
	IF @Day IS NULL
	BEGIN
		SET @Day=DATEADD(day, -1, GETUTCDATE())
	END
	
	IF @DeleteExisting = 1
	BEGIN
		DELETE FROM [dbo].[EODRate] WHERE [Date] = @Day
	END
	
	
	DECLARE @RolloverTimeUtc DATETIME
	DECLARE @RolloverTimeEt DATETIME = DATEADD(hour, 17, CAST(@day AS DATETIME))
	exec [dev03].[DataCollection_Testing].[dbo].sp_executesql N'SELECT @output = [DataCollection_Testing].[dbo].[ConvertFromEtToUtc](@input)',
	N'@input datetime, @output datetime OUTPUT',
	@input=@RolloverTimeEt,
	@output=@RolloverTimeUtc OUTPUT
	DECLARE @CutoffTimeUtc DATETIME = DATEADD(hour, -1, @RolloverTimeUtc)
	
	
	INSERT INTO [dbo].[EODRate]
	   ([FXPairId]
	   ,[Date]
	   ,[BidPrice]
	   ,[AskPrice]
	   ,[IsBidPriceAuthentic]
	   ,[IsAskPriceAuthentic])
	SELECT
		CurrentEodRates.FxPairId,
		@Day,
		COALESCE(CurrentEodRates.BidToB, PreviousEodRates.BidPrice) AS BidPrice,
		COALESCE(CurrentEodRates.AskToB, PreviousEodRates.AskPrice) AS AskPrice,
		CASE WHEN CurrentEodRates.BidToB IS NULL THEN 0 ELSE 1 END AS IsBidPriceAuthentic,
		CASE WHEN CurrentEodRates.AskToB IS NULL THEN 0 ELSE 1 END AS IsAskPriceAuthentic
	FROM
		(
		SELECT 
			pair.[FxPairId],
			EodRates.BidToB,
			EodRates.AskToB
		FROM 
			[dbo].[FxPair] pair
			LEFT JOIN
			(
				SELECT
					FxPairId,
					--ask
					MAX(CASE WHEN SideId = 1 THEN LastSidedPrice ELSE NULL END) AS AskToB,
					MIN(CASE WHEN SideId = 0 THEN LastSidedPrice ELSE NULL END) AS BidToB
				FROM
				(
					SELECT 
						FxPairId,
						CounterpartyId,
						SideId,
						-- last price in current millisecond interval = ToB in current millisecond
						LAST_VALUE(md.Price) 
							OVER(
								PARTITION BY FxPairId, CounterpartyId, SideId
								ORDER BY md.integratorReceivedTimeUtc ASC 
								ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) AS LastSidedPrice
					FROM
						[dbo].[MarketData] md with (nolock) 
					WHERE
						FXPairId IN (SELECT fxpairid from [dbo].[fxpair] WITH(NOLOCK))
						AND IntegratorReceivedTimeUtc BETWEEN @CutoffTimeUtc AND @RolloverTimeUtc
						AND RecordTypeId IN (0, 3)
				) tmp1
				GROUP BY FxPairId
			) EodRates
			ON EodRates.FxPairId = pair.[FxPairId]
		)CurrentEodRates
		LEFT JOIN [dbo].[EODRate] PreviousEodRates 
			ON PreviousEodRates.[Date] = DATEADD(Day, -1, @Day) 
			AND CurrentEodRates.FxPairId = PreviousEodRates.FxPairId
			
		IF @InsertSameDataForFollowingDay = 1
		BEGIN
			
			IF @DeleteExisting = 1
			BEGIN
				DELETE FROM [dbo].[EODRate] WHERE [Date] = DATEADD(day, 1, @Day)
			END
			
			INSERT INTO [dbo].[EODRate]
			   ([FXPairId]
			   ,[Date]
			   ,[BidPrice]
			   ,[AskPrice]
			   ,[IsBidPriceAuthentic]
			   ,[IsAskPriceAuthentic])
			SELECT
				[FXPairId]
			   ,DATEADD(day, 1, @Day)
			   ,[BidPrice]
			   ,[AskPrice]
			   ,[IsBidPriceAuthentic]
			   ,[IsAskPriceAuthentic]
			FROM
			   [dbo].[EODRate]
			WHERE
				[date] = @Day
			
		END
		
END
GO


INSERT INTO [dbo].[EODRate]
	   ([FXPairId]
	   ,[Date]
	   ,[BidPrice]
	   ,[AskPrice]
	   ,[IsBidPriceAuthentic]
	   ,[IsAskPriceAuthentic])
SELECT
	FXPairID,
	DATEFROMPARTS(
		CAST(LastTickTime / 10000000000000 AS INT),
		CAST((LastTickTime - CAST(LastTickTime / 10000000000000 AS INT) * 10000000000000) / 100000000000 AS INT) ,
		CAST((LastTickTime  - CAST(LastTickTime / 100000000000   AS INT) * 100000000000 ) / 1000000000 AS INT)) AS Date,
	MAX(LastBid),
	MAX(LastAsk),
	1,
	1
FROM
(
	SELECT
		[FXPairID],
		LAST_VALUE([TickTime])
		OVER(
			PARTITION BY [FXPairID], [TickTime] - ([TickTime] % 1000000000)
			ORDER BY [TickTime]
			ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS LastTickTime,
		LAST_VALUE([Bid])
		OVER(
			PARTITION BY [FXPairID], [TickTime] - ([TickTime] % 1000000000)
			ORDER BY [TickTime]
			ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS LastBid,
		LAST_VALUE([Ask])
		OVER(
			PARTITION BY [FXPairID], [TickTime] - ([TickTime] % 1000000000)
			ORDER BY [TickTime]
			ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS LastAsk
	FROM 
		[dbo].[Tick] WITH(NOLOCK)
	WHERE
		[DataProviderID] = 22
		AND [FXPairID] IN (SELECT fxpairid from [dbo].[fxpair] WITH(NOLOCK))
		AND [TickTime] BETWEEN 20130701000000000 AND 20150508000000000
		AND CAST(([TickTime] % 1000000000) AS INT) / 10000000 < 17
)tmp1
GROUP BY [FXPairID],LastTickTime






declare @day DATE
declare cur CURSOR LOCAL for
		WITH mycte AS
		(
		  SELECT CAST('2013-10-02' AS DATETIME) DateValue
		  UNION ALL
		  SELECT  DateValue + 1
		  FROM    mycte   
		  WHERE   DateValue + 1 < '2015-05-06'
		)

		SELECT CAST(DateValue AS DATE)
		FROM    mycte
		OPTION (MAXRECURSION 0)
open cur

fetch next from cur into @day

while @@FETCH_STATUS = 0 BEGIN

	print @day
	exec CalculateSingleDayEodRates_SP @day

    fetch next from cur into @day
END

close cur
deallocate cur



USE msdb;
GO

EXEC sp_add_jobstep
    @job_name = N'Daily Creation of Current Day Views',
    @step_name = N'UpdateEoDRates',
    @subsystem = N'CMDEXEC',
    @command = N'osql -E -Q "exec CalculateSingleDayEodRates_SP"',
	@database_name = N'FXTickDB',
	--@database_user_name = N'sa',
	-- Go to the next step (on success and failure)
	@on_success_action = 3,
	@on_fail_action = 3,
    @retry_attempts = 0,
    @retry_interval = 0;
GO

--MANUALY reaorder the steps - there is no other documented way than doing this through SSMS
--MANUALY - map to sa
-- + alter authentication of linked server (see https://support.microsoft.com/en-us/kb/811031)


ALTER TABLE [dbo].[EODRate] DROP COLUMN [MidPrice]
ALTER TABLE [dbo].[EODRate] ADD [MidPrice] AS ([AskPrice] + [BidPrice]) / 2
GO

CREATE TABLE [dbo].[EODUsdConversions](
	[CurrencyId] [smallint] NOT NULL,
	[Date] [Date] NOT NULL,
	[BidPrice] [decimal](18, 9) NOT NULL,
	[AskPrice] [decimal](18, 9) NOT NULL,
	[MidPrice] AS ([AskPrice] + [BidPrice]) / 2
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[EODUsdConversions]  WITH NOCHECK ADD  CONSTRAINT [FK_EODRate_Currency] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currency] ([CurrencyID])
GO
ALTER TABLE [dbo].[EODUsdConversions] CHECK CONSTRAINT [FK_EODRate_Currency]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_EODUsdConversions_DatePair] ON [dbo].[EODUsdConversions]
(
	[Date] ASC,
	[CurrencyId] ASC
)
GO


CREATE PROCEDURE CalculateEodUsdConversions_SP
(
	--Both nulls - will calc only for the last day
	@StartDateInclusive DATE = NULL,
	@EndDateInclusive DATE = NULL
)
AS
BEGIN

	DECLARE @USDId SMALLINT
	SELECT @USDId = [CurrencyID] FROM [dbo].[Currency] WITH (NOLOCK) WHERE [CurrencyAlphaCode] = 'USD'

	IF @StartDateInclusive IS NULL
	BEGIN
		SELECT @StartDateInclusive = MAX(Date) FROM [dbo].[EODRate]
	END

	IF @EndDateInclusive IS NULL
	BEGIN
		SELECT @EndDateInclusive = MAX(Date) FROM [dbo].[EODRate]
	END
	
	IF @StartDateInclusive IS NULL OR @EndDateInclusive IS NULL
	BEGIN
		RETURN;
	END
	
	--First get rid of any overlapping records (e.g. dummy current day records)
	DELETE FROM [dbo].[EODUsdConversions] WHERE [Date] >= @StartDateInclusive AND [Date] <= @EndDateInclusive


	;WITH mycte AS
		(
			SELECT CAST(@StartDateInclusive AS DATETIME) DateValue
			UNION ALL
			SELECT  DateValue + 1
			FROM    mycte   
			WHERE   DateValue + 1 <= @EndDateInclusive
		)
		
	INSERT INTO 
		[dbo].[EODUsdConversions]
		   ([CurrencyId]
		   ,[Date]
		   ,[BidPrice]
		   ,[AskPrice])
		SELECT
			pair.BaseCurrencyId
			,[Date]
			,[BidPrice] AS BidPrice
			,[AskPrice] AS AskPrice
		FROM 
			[dbo].[EODRate] rate
			INNER JOIN [dbo].[FXPair] pair ON rate.FXPairId = pair.FXPairId
		WHERE	
			pair.QuoteCurrencyId = @USDId
			AND (@StartDateInclusive IS NULL OR rate.Date >= @StartDateInclusive)
			AND (@EndDateInclusive IS NULL OR rate.Date <= @EndDateInclusive)
			AND [BidPrice] IS NOT NULL AND [AskPrice] IS NOT NULL
		UNION ALL
		SELECT 
			pair.QuoteCurrencyId
			,[Date]
			,1/[AskPrice] AS BidPrice
			,1/[BidPrice] AS AskPrice
		FROM 
			[dbo].[EODRate] rate
			INNER JOIN [dbo].[FXPair] pair ON rate.FXPairId = pair.FXPairId
		WHERE	
			pair.BaseCurrencyId = @USDId
			AND (@StartDateInclusive IS NULL OR rate.Date >= @StartDateInclusive)
			AND (@EndDateInclusive IS NULL OR rate.Date <= @EndDateInclusive)
			AND [BidPrice] IS NOT NULL AND [AskPrice] IS NOT NULL
		UNION ALL
		SELECT
			@USDId, 
			CAST(DateValue AS DATE),
			1 AS BidPrice,
			1 AS AskPrice
		FROM
			mycte OPTION (MAXRECURSION 0)
			
			
		DECLARE @Today DATE = CAST(GETUTCDATE() AS DATE)
			
		-- insert dummy records for current day
		IF DATEADD(Day,1,@EndDateInclusive) = @Today AND @StartDateInclusive = @EndDateInclusive
		BEGIN
		
			INSERT INTO 
			[dbo].[EODUsdConversions]
			   ([CurrencyId]
			   ,[Date]
			   ,[BidPrice]
			   ,[AskPrice])
			SELECT
				pair.BaseCurrencyId
				,@Today
				,[BidPrice] AS BidPrice
				,[AskPrice] AS AskPrice
			FROM 
				[dbo].[EODRate] rate
				INNER JOIN [dbo].[FXPair] pair ON rate.FXPairId = pair.FXPairId
			WHERE	
				pair.QuoteCurrencyId = @USDId
				AND rate.Date = @EndDateInclusive
				AND [BidPrice] IS NOT NULL AND [AskPrice] IS NOT NULL
			UNION ALL
			SELECT 
				pair.QuoteCurrencyId
				,@Today
				,1/[AskPrice] AS BidPrice
				,1/[BidPrice] AS AskPrice
			FROM 
				[dbo].[EODRate] rate
				INNER JOIN [dbo].[FXPair] pair ON rate.FXPairId = pair.FXPairId
			WHERE	
				pair.BaseCurrencyId = @USDId
				AND rate.Date = @EndDateInclusive
				AND [BidPrice] IS NOT NULL AND [AskPrice] IS NOT NULL
			UNION ALL
			SELECT
				@USDId, 
				@Today,
				1 AS BidPrice,
				1 AS AskPrice
			
		END

END
GO

CREATE TABLE [dbo].[_Internal_CurrencyIdsMapping](
	[IntegratorDBCurrencyId] [smallint] NOT NULL,
	[DCDBCurrencyId] [smallint] NOT NULL,
	[CurrencyAlphaCode] [char](3) NOT NULL
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX__Internal_CurrencyIdsMapping_DCDBCurrency] ON [dbo].[_Internal_CurrencyIdsMapping]
(
	[DCDBCurrencyId] ASC
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IX__Internal_CurrencyIdsMapping_IntegratorDbCurrency] ON [dbo].[_Internal_CurrencyIdsMapping]
(
	[IntegratorDBCurrencyId] ASC
) ON [PRIMARY]
GO

INSERT INTO [dbo].[_Internal_CurrencyIdsMapping] ([IntegratorDBCurrencyId], [DCDBCurrencyId], [CurrencyAlphaCode])
SELECT
	integratorCurrency.[CurrencyID] AS IntegratorDBCurrencyId,
	dcCurrency.[CurrencyID] AS DCDBCurrencyId,
    dcCurrency.[CurrencyAlphaCode] AS CurrencyAlphaCode
FROM 
	[dbo].[Currency] dcCurrency
	INNER JOIN [Integrator].[dbo].[Currency_Internal] integratorCurrency ON dcCurrency.[CurrencyAlphaCode] = integratorCurrency.[CurrencyAlphaCode]


GRANT SELECT ON [dbo].[_Internal_CurrencyIdsMapping] TO [IntegratorServiceAccount] AS [dbo]
GO	
GRANT SELECT ON [dbo].[_Internal_SymbolIdsMapping] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[EODUsdConversions] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EODUsdConversions] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[EODRate] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EODRate] TO [IntegratorServiceAccount] AS [dbo]
GO
	
-- Controll query
-- Also with DealExternalRejected AND OrderExternalIncomingRejectable_Archive
SELECT
[DatePartIntegratorReceivedExecutionReportUtc],
[SymbolId],
usdConv.BidPrice
	FROM [Integrator].[dbo].[DealExternalExecuted] d
	LEFT JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] sym ON d.SymbolId = sym.IntegratorDBSymbolId
	LEFT JOIN [FXtickDB].[dbo].[FxPair] pair ON sym.DCDBSymbolId = pair.FxPairId
	LEFT JOIN [FXtickDB].[dbo].[EODUsdConversions] usdConv ON pair.QuoteCurrencyId = usdConv.CurrencyId
WHERE
	usdConv.BidPrice IS NULL
	
UPDATE
	sett
SET
	[OnePipDecimalValue_Legacy] = 0.0005,
	[MinimumPriceIncrement] = 0.0005,
	[Supported] = 1
FROM
	[dbo].[CounterpartySymbolSettings] sett
	INNER JOIN [dbo].[FxPair] pair on sett.[SymbolId] = pair.fxpairid
	INNER JOIN [dbo].[TradingTargetType] ttype on sett.[TradingTargetTypeId] = ttype.Id
WHERE
	fxpcode = 'USDILS'
	AND ttype.Name = 'LMAX'