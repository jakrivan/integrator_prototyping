/****** Object:  StoredProcedure [dbo].[GetRecentExternalOrdersTransmissionControlState_SP]    Script Date: 2. 7. 2013 8:33:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetExternalOrdersTransmissionSwitchState_SP] 
	@SwitchName NVARCHAR(64) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		[SwitchName],
		[TransmissionEnabled]
	FROM 
		[dbo].[ExternalOrdersTransmissionSwitch]
	WHERE 
		@SwitchName IS NULL OR [SwitchName] = @SwitchName
END

GO
GRANT EXECUTE ON [dbo].[GetExternalOrdersTransmissionSwitchState_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO