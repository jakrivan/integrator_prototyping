
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDealsStatistics_SP]( 
	@Day DATE
	)
AS
BEGIN

	SELECT
		SUM(ABS([AmountTermPolExecutedInUsd]))	AS TotalVolumeAbsInUsd
		,COUNT([AmountTermPolExecutedInUsd]) AS NumberOfDeals
		,CounterpartyCode AS Counterparty
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN Counterparty ctp ON deal.[CounterpartyId] = ctp.[CounterpartyId]
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		ctp.CounterpartyCode

END
GO

GRANT EXECUTE ON [dbo].[GetDealsStatistics_SP] TO [IntegratorServiceAccount] AS [dbo]
GO




ALTER TABLE [dbo].[DealExternalRejected] ADD DatePartIntegratorReceivedExecutionReportUtc AS CONVERT(DATE, [IntegratorReceivedExecutionReportUtc]) PERSISTED
GO

SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO

CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalRejected]
(
	[DatePartIntegratorReceivedExecutionReportUtc] ASC,
	[CounterpartyId] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetRejectionsStatistics_SP]( 
	@Day DATE
	)
AS
BEGIN

	SELECT
		COUNT([IntegratorReceivedExecutionReportUtc]) AS NumberOfRejections
		,CounterpartyCode AS Counterparty
	FROM
		[dbo].[DealExternalRejected] rej
		INNER JOIN Counterparty ctp ON rej.[CounterpartyId] = ctp.[CounterpartyId]
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		ctp.CounterpartyCode

END
GO

GRANT EXECUTE ON [dbo].[GetRejectionsStatistics_SP] TO [IntegratorServiceAccount] AS [dbo]
GO