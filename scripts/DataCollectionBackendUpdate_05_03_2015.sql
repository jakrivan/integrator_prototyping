

CREATE TABLE [dbo].[_Internal_TradeDecayIntervals2](
	[MsDistanceFromEventStart] [int] NOT NULL,
	[SideId] [bit] NOT NULL
) ON [PRIMARY]

GO

CREATE UNIQUE CLUSTERED INDEX [IX__Internal_TradeDecayIntervals2] ON [dbo].[_Internal_TradeDecayIntervals2]
(
	[SideId] ASC,
	[MsDistanceFromEventStart] ASC
) ON [PRIMARY]
GO


INSERT INTO [dbo].[_Internal_TradeDecayIntervals2] ([MsDistanceFromEventStart], [SideId])
SELECT [MsDistanceFromEventStart], 0 FROM [dbo].[_Internal_TradeDecayIntervals]
UNION ALL 
SELECT [MsDistanceFromEventStart], 1 FROM [dbo].[_Internal_TradeDecayIntervals]

DROP TABLE [dbo].[_Internal_TradeDecayIntervals]

exec sp_rename '[dbo].[_Internal_TradeDecayIntervals2]', '_Internal_TradeDecayIntervals'
GO

ALTER PROCEDURE [dbo].[PerformSingleTradeDecayAnalysis_SP]
(
	@TradeDecayAnalysisTypeId [tinyint],
	@SourceEventRecordId [int],
	@AnalysisStartTimeUtc [datetime2](7),
	@FXPairId [tinyint],
	@CounterpartyId [tinyint],
	@ReferencePrice [decimal](18,9),
	@PriceSideId [bit]
)
AS
BEGIN
	DECLARE @TradeDecayAnalysisId INT

	INSERT INTO [dbo].[TradeDecayAnalysis]
           ([TradeDecayAnalysisTypeId]
           ,[SourceEventRecordId]
           ,[AnalysisStartTimeUtc]
           ,[FXPairId]
           ,[CounterpartyId]
           ,[PriceSideId]
           ,[HasValidTickAfterIntervalEnd]
           ,[IsValidAndComplete])
     VALUES
           (@TradeDecayAnalysisTypeId,
			@SourceEventRecordId,
			@AnalysisStartTimeUtc,
			@FXPairId,
			@CounterpartyId,
			@PriceSideId,
			0,
			0)
	
	SET @TradeDecayAnalysisId = scope_identity();
	DECLARE @ReferenceCounterpartyId INT = (SELECT [LiqProviderStreamId] FROM [LiqProviderStream] WHERE [LiqProviderStreamName] = 'L01')
	-- round to next whole us and and 1ms for the first interval
	DECLARE @AnalysisFirstIntervalStartTimeUtc [datetime2](7) = DATEADD(ns, -DATEPART(ns, @AnalysisStartTimeUtc)%1000 + 1000 + 1000000, @AnalysisStartTimeUtc)
	DECLARE @AnalysisEndTimeUtc [datetime2](7) = DATEADD(minute, 5, @AnalysisStartTimeUtc)
	
	
	DECLARE @FirstBidTob [decimal](18,9)
	DECLARE @FirstAskTob [decimal](18,9)
	
	SET @FirstBidTob =
	(SELECT TOP 1
		md.Price AS LastToBPrice
	FROM
		[dbo].[MarketData] md with (nolock) 
		INNER JOIN [dbo].[LiqProviderStream] lpB with (nolock) on md.[CounterpartyId] = lpB.LiqProviderStreamId
	WHERE
		IntegratorReceivedTimeUtc BETWEEN DATEADD(second, -30, @AnalysisFirstIntervalStartTimeUtc) AND @AnalysisFirstIntervalStartTimeUtc
		AND fxpairid  = @FxPairId
		AND RecordTypeId IN (0, 3)
		AND CounterpartyId = @ReferenceCounterpartyId
		AND SideId = 0
	ORDER BY IntegratorReceivedTimeUtc DESC)
	
	SET @FirstAskTob =
	(SELECT TOP 1
		md.Price AS LastToBPrice
	FROM
		[dbo].[MarketData] md with (nolock) 
		INNER JOIN [dbo].[LiqProviderStream] lpB with (nolock) on md.[CounterpartyId] = lpB.LiqProviderStreamId
	WHERE
		IntegratorReceivedTimeUtc BETWEEN DATEADD(second, -30, @AnalysisFirstIntervalStartTimeUtc) AND @AnalysisFirstIntervalStartTimeUtc
		AND fxpairid  = @FxPairId
		AND RecordTypeId IN (0, 3)
		AND CounterpartyId = @ReferenceCounterpartyId
		AND SideId = 1
	ORDER BY IntegratorReceivedTimeUtc DESC)
	
	--fast track if there is no valid price before the interval
	IF @FirstBidTob IS NULL AND @FirstAskTob IS NULL
	BEGIN
		RETURN;
	END
	
	IF NOT EXISTS(
		SELECT 1 
		FROM 
			[dbo].[MarketData] md with (nolock) 
			INNER JOIN [dbo].[LiqProviderStream] lpB with (nolock) on md.[CounterpartyId] = lpB.LiqProviderStreamId
		WHERE
			IntegratorReceivedTimeUtc BETWEEN @AnalysisEndTimeUtc AND DATEADD(second, 30, @AnalysisEndTimeUtc)
			AND fxpairid  = @FxPairId
			AND RecordTypeId IN (0, 3)
			AND CounterpartyId = @ReferenceCounterpartyId
			AND SideId = @PriceSideId
		)
	BEGIN
		RETURN;
	END
	
	
	INSERT INTO [dbo].[TradeDecayAnalysisDataPoint]
           ([TradeDecayAnalysisId]
           ,[MillisecondDistanceFromAnalysisStart]
           ,[MidToB]
           ,[BpDifferenceFromReferencePrice]
		   ,[IsAuthenticValue])
	SELECT	
		@TradeDecayAnalysisId,
		MillisecondDistance,
		-- as we do not have ANY()
		AVG(LastSidedToB) AS MidToB,
		CASE WHEN @PriceSideId = 0 THEN (AVG(LastSidedToB) - @ReferencePrice) ELSE (@ReferencePrice - AVG(LastSidedToB)) END / @ReferencePrice * 10000.0,
		-- both must be 1 for this to be 1
		MIN(IsAuthenticValue) AS IsAuthenticValue
	FROM	
		(
		SELECT
			MillisecondDistance,
			SideId,
			--for lack of ANY()
			MAX(LastToBPrice) OVER (PARTITION BY NonNullToBsNum) AS LastSidedToB,
			CASE WHEN LastToBPrice IS NULL THEN 0 ELSE 1 END AS IsAuthenticValue
		FROM
			(
			SELECT
				nums.MsDistanceFromEventStart AS MillisecondDistance,
				nums.SideId AS SideId,
				LastToBPrice, -- as we do not have any - all are same
				-- this will count number of non-null rows - for nulls (no ToB in current millisecond) it will repeat previous val (so we can partition by this one level up)
				COUNT(LastToBPrice) OVER (ORDER BY nums.SideId, nums.MsDistanceFromEventStart ASC) as NonNullToBsNum
			FROM
			[dbo].[_Internal_TradeDecayIntervals] nums
			LEFT JOIN
			(
				SELECT
					0 AS BucketizedMillisecondsFromIntervalStart,
					0 AS SideId,
					@FirstBidTob AS LastToBPrice
				UNION ALL
				SELECT
					0 AS BucketizedMillisecondsFromIntervalStart,
					1 AS SideId,
					@FirstAskTob AS LastToBPrice	
				UNION ALL
				SELECT
					BucketizedMillisecondsFromIntervalStart,
					SideId,
					MAX(LastToBPrice) AS LastToBPrice -- as we do not have ANY() - all are same
				FROM
				(
					SELECT 
						-- millisecond difference from the beginig
						[dbo].[GetTradeDecayTimeIntervalBucket](DateDiff(mcs, @AnalysisStartTimeUtc, md.integratorReceivedTimeUtc)/1000) AS BucketizedMillisecondsFromIntervalStart,
						SideId,
						-- last price in current millisecond interval = ToB in current millisecond
						LAST_VALUE(md.Price) 
							OVER(
								PARTITION BY [dbo].[GetTradeDecayTimeIntervalBucket](DateDiff(mcs, @AnalysisStartTimeUtc, md.integratorReceivedTimeUtc)/1000), SideId
								ORDER BY md.integratorReceivedTimeUtc ASC 
								ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) AS LastToBPrice
					FROM
						[dbo].[MarketData] md with (nolock) 
						INNER JOIN [dbo].[LiqProviderStream] lpB with (nolock) on md.[CounterpartyId] = lpB.LiqProviderStreamId
					WHERE
						IntegratorReceivedTimeUtc BETWEEN @AnalysisFirstIntervalStartTimeUtc AND @AnalysisEndTimeUtc
						AND fxpairid  = @FxPairId
						AND RecordTypeId IN (0, 3)
						AND CounterpartyId = @ReferenceCounterpartyId
						--AND SideId = @PriceSideId
				) tmp
				GROUP BY BucketizedMillisecondsFromIntervalStart, SideId
			) tmp2 ON nums.MsDistanceFromEventStart = tmp2.BucketizedMillisecondsFromIntervalStart AND nums.SideId = tmp2.SideId
		) tmp3
	) tmp4
	GROUP BY MillisecondDistance
	ORDER BY MillisecondDistance ASC

	
	UPDATE [dbo].[TradeDecayAnalysis]
	SET 
      [HasValidTickAfterIntervalEnd] = 1
      ,[IsValidAndComplete] = 1
	WHERE 
		[TradeDecayAnalysisId] = @TradeDecayAnalysisId
	
END
GO



BEGIN TRANSACTION Install_05_03_001;
GO

INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('FTS', 'UK 100', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('DJI', 'Wall Street 30', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('SPX', 'US SPX 500', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('SPY', 'US SPX 500 (Mini)', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('NDX', 'US Tech 100', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('DAX', 'Germany 30', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('DXY', 'Germany 30 (Mini)', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('CAC', 'France 40', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('STX', 'Europe 50', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('NIK', 'Japan 225', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('AUS', 'Australia 200', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('XAU', 'Gold (Spot)', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('XAG', 'Silver (Spot)', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('WTI', 'US Crude (Spot)', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('BRE', 'UK Brent (Spot)', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('XPT', 'Platinum', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('XPD', 'Palladium', NULL, 1, GETUTCDATE(), 0)
INSERT INTO [dbo].[Currency] ([CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn], [IsCitibankPbSupported]) VALUES ('GAS', 'US Natural Gas (Spot)', NULL, 1, GETUTCDATE(), 0)
GO

INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('FTSGBP', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'FTS'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'GBP'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('DJIUSD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'DJI'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('SPXUSD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'SPX'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('SPYUSD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'SPY'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('NDXUSD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'NDX'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('DAXEUR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'DAX'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'EUR'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('DXYEUR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'DXY'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'EUR'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('CACEUR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'CAC'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'EUR'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('STXEUR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'STX'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'EUR'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('NIKJPY', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'NIK'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'JPY'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('AUSAUD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'AUS'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'AUD'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('XAUUSD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'XAU'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('XAGUSD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'XAG'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('XAUAUD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'XAU'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'AUD'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('XAGAUD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'XAG'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'AUD'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('WTIUSD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'WTI'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('BREUSD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'BRE'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('XPTUSD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'XPT'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('XPDUSD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'XPD'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'USD'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('XAUEUR', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'XAU'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'EUR'))
INSERT INTO [dbo].[FxPair] ([FxpCode], [BaseCurrencyId], [QuoteCurrencyId]) VALUES ('GASUSD', (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'GAS'), (SELECT [CurrencyID] FROM [dbo].[Currency] WHERE [CurrencyAlphaCode] = 'USD'))
GO

INSERT INTO [dbo].[_Internal_SymbolIdsMapping] ([IntegratorDBSymbolId], [DCDBSymbolId], [SymbolCodeNoSlash])
SELECT
	integratorSymbol.[Id] AS IntegratorDBSymbolId,
	dcSymbol.[FxPairId] AS DCDBSymbolId,
    dcSymbol.[FxpCode] AS SymbolCodeNoSlash
FROM 
	[dbo].[FxPair] dcSymbol
	INNER JOIN [Integrator].[dbo].[Symbol_Internal] integratorSymbol ON dcSymbol.[FxpCode] = integratorSymbol.[ShortName]
WHERE
	dcSymbol.[FxPairId] NOT IN (SELECT DCDBSymbolId FROM [dbo].[_Internal_SymbolIdsMapping])
	
	

--ROLLBACK TRANSACTION Install_05_03_001;
--GO
--COMMIT TRANSACTION Install_05_03_001;
--GO



print 'Started adding partition files:'
print GETUTCDATE()
GO

--Partitioning
--UAT: Q:\SQLData\Testing_canBeLost\DataCollection_Testing

Alter Database FXtickDB Add FileGroup MDFilegroup_FTSGBP
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_FTSGBP_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_FTSGBP_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_FTSGBP
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_DJIUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_DJIUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_DJIUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_DJIUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_SPXUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_SPXUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_SPXUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_SPXUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_SPYUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_SPYUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_SPYUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_SPYUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NDXUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NDXUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NDXUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NDXUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_DAXEUR
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_DAXEUR_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_DAXEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_DAXEUR
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_DXYEUR
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_DXYEUR_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_DXYEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_DXYEUR
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_CACEUR
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_CACEUR_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_CACEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_CACEUR
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_STXEUR
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_STXEUR_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_STXEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_STXEUR
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_NIKJPY
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_NIKJPY_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_NIKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_NIKJPY
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_AUSAUD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_AUSAUD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_AUSAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_AUSAUD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_XAUUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_XAUUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_XAUUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_XAUUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_XAGUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_XAGUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_XAGUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_XAGUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_XAUAUD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_XAUAUD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_XAUAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_XAUAUD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_XAGAUD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_XAGAUD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_XAGAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_XAGAUD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_WTIUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_WTIUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_WTIUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_WTIUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_BREUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_BREUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_BREUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_BREUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_XPTUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_XPTUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_XPTUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_XPTUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_XPDUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_XPDUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_XPDUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_XPDUSD
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_XAUEUR
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_XAUEUR_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_XAUEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_XAUEUR
GO 

Alter Database FXtickDB Add FileGroup MDFilegroup_GASUSD
GO
Alter Database FXtickDB Add File
(
Name=MDSplittedFilegroup_GASUSD_File01,
FileName='T:\SQLDATA\USERDB\FXTickDB\MDPartitions\MDFilegroup_GASUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup MDFilegroup_GASUSD
GO 

--
-- Ticks
--

Alter Database FXtickDB Add FileGroup TickFilegroup_FTSGBP
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_FTSGBP_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_FTSGBP_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_FTSGBP
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_DJIUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_DJIUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_DJIUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_DJIUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_SPXUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_SPXUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_SPXUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_SPXUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_SPYUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_SPYUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_SPYUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_SPYUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NDXUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NDXUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NDXUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NDXUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_DAXEUR
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_DAXEUR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_DAXEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_DAXEUR
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_DXYEUR
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_DXYEUR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_DXYEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_DXYEUR
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_CACEUR
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_CACEUR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_CACEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_CACEUR
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_STXEUR
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_STXEUR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_STXEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_STXEUR
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_NIKJPY
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_NIKJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_NIKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_NIKJPY
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_AUSAUD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_AUSAUD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_AUSAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_AUSAUD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_XAUUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_XAUUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_XAUUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_XAUUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_XAGUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_XAGUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_XAGUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_XAGUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_XAUAUD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_XAUAUD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_XAUAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_XAUAUD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_XAGAUD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_XAGAUD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_XAGAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_XAGAUD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_WTIUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_WTIUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_WTIUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_WTIUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_BREUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_BREUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_BREUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_BREUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_XPTUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_XPTUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_XPTUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_XPTUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_XPDUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_XPDUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_XPDUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_XPDUSD
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_XAUEUR
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_XAUEUR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_XAUEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_XAUEUR
GO 

Alter Database FXtickDB Add FileGroup TickFilegroup_GASUSD
GO
Alter Database FXtickDB Add File
(
Name=TickSplittedFilegroup_GASUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\TickPartitions\TickFilegroup_GASUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup TickFilegroup_GASUSD
GO 


--
-- Outgoing prices
--

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_FTSGBP
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_FTSGBP_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_FTSGBP_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_FTSGBP
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_DJIUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_DJIUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_DJIUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_DJIUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_SPXUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_SPXUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_SPXUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_SPXUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_SPYUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_SPYUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_SPYUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_SPYUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NDXUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NDXUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NDXUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NDXUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_DAXEUR
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_DAXEUR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_DAXEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_DAXEUR
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_DXYEUR
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_DXYEUR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_DXYEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_DXYEUR
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_CACEUR
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_CACEUR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_CACEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_CACEUR
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_STXEUR
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_STXEUR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_STXEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_STXEUR
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_NIKJPY
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_NIKJPY_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_NIKJPY_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_NIKJPY
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_AUSAUD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_AUSAUD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_AUSAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_AUSAUD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_XAUUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_XAUUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_XAUUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_XAUUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_XAGUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_XAGUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_XAGUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_XAGUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_XAUAUD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_XAUAUD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_XAUAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_XAUAUD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_XAGAUD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_XAGAUD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_XAGAUD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_XAGAUD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_WTIUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_WTIUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_WTIUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_WTIUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_BREUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_BREUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_BREUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_BREUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_XPTUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_XPTUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_XPTUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_XPTUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_XPDUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_XPDUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_XPDUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_XPDUSD
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_XAUEUR
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_XAUEUR_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_XAUEUR_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_XAUEUR
GO

Alter Database FXtickDB Add FileGroup OutgoingPriceFilegroup_GASUSD
GO
Alter Database FXtickDB Add File
(
Name=OutgoingPriceSplittedFilegroup_GASUSD_File01,
FileName='S:\SQLDATA\USERDB\FXTickDB\OutgoingPricePartitions\OutgoingPriceFilegroup_GASUSD_File01.ndf',
Size=1MB,
FileGrowth=10%
) To Filegroup OutgoingPriceFilegroup_GASUSD
GO



-- WARNING! verify that FTSGBP was added with id 201

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_FTSGBP]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_FTSGBP]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_FTSGBP]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (201);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_DJIUSD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_DJIUSD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_DJIUSD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (202);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_SPXUSD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_SPXUSD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_SPXUSD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (203);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_SPYUSD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_SPYUSD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_SPYUSD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (204);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_NDXUSD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_NDXUSD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_NDXUSD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (205);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_DAXEUR]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_DAXEUR]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_DAXEUR]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (206);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_DXYEUR]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_DXYEUR]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_DXYEUR]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (207);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_CACEUR]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_CACEUR]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_CACEUR]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (208);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_STXEUR]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_STXEUR]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_STXEUR]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (209);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_NIKJPY]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_NIKJPY]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_NIKJPY]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (210);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_AUSAUD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_AUSAUD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_AUSAUD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (211);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_XAUUSD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_XAUUSD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_XAUUSD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (212);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_XAGUSD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_XAGUSD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_XAGUSD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (213);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_XAUAUD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_XAUAUD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_XAUAUD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (214);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_XAGAUD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_XAGAUD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_XAGAUD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (215);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_WTIUSD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_WTIUSD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_WTIUSD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (216);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_BREUSD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_BREUSD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_BREUSD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (217);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_XPTUSD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_XPTUSD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_XPTUSD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (218);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_XPDUSD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_XPDUSD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_XPDUSD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (219);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_XAUEUR]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_XAUEUR]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_XAUEUR]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (220);
GO

ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_GASUSD]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_GASUSD]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_GASUSD]
GO
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (221);
GO




-- And mark the 'out of range' right partitions - so that future symbols doesn't get merget into existing partitions
ALTER PARTITION SCHEME [MarketDataSymbolPartitionScheme_IndividualFileGroups] NEXT USED [MDFilegroup_FutureSymbols]
ALTER PARTITION SCHEME [TicksSymbolPartitionScheme_IndividualFileGroups] NEXT USED [TickFilegroup_FutureSymbols]
ALTER PARTITION SCHEME [OutgoingPriceSymbolPartitionScheme_IndividualFileGroups] NEXT USED [OutgoingPriceFilegroup_FutureSymbols]
GO

print 'Partition scheme updated:'
print GETUTCDATE()
GO