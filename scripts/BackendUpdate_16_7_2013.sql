

BEGIN TRANSACTION
GO
ALTER TABLE dbo.ExecutedExternalDeals ADD
    Price decimal(18, 9) NULL
GO
COMMIT

/****** Object:  StoredProcedure [dbo].[InsertExecutedExternalDeal_SP]    Script Date: 16. 7. 2013 8:59:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[InsertExecutedExternalDeal_SP]( 
	@OrderId NVARCHAR(32),
	@ExecutionTimeStampUtc DATETIME,
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@ExecutedAmountBaseAbs DECIMAL(18,0),
	@ExecutedAmountBasePol DECIMAL(18,0),
	@Price decimal(18, 9)
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
		[dbo].[ExecutedExternalDeals]
           ([OrderId]
           ,[ExecutionTimeStampUtc]
           ,[SymbolId]
           ,[DealDirectionId]
           ,[ExecutedAmountBaseAbs]
           ,[ExecutedAmountBasePol]
           ,[Price])
     VALUES
           (@OrderId
           ,@ExecutionTimeStampUtc
           ,@SymbolId
           ,@DealDirectionId 
           ,@ExecutedAmountBaseAbs
           ,@ExecutedAmountBasePol
           ,@Price)
END