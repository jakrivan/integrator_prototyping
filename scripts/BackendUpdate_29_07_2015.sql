BEGIN TRANSACTION AAA


CREATE TABLE [dbo].[VenueQuotingSettingsSpreadType](
	[VenueQuotingSettingsSpreadTypeId] [bit] NOT NULL,
	[VenueQuotingSettingsSpreadTypeName] [varchar](8) NOT NULL,
 CONSTRAINT [IX_VenueQuotingSettingsSpreadTypeName] UNIQUE NONCLUSTERED 
(
	[VenueQuotingSettingsSpreadTypeName] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_VenueQuotingSettingsSpreadType] ON [dbo].[VenueQuotingSettingsSpreadType]
(
	[VenueQuotingSettingsSpreadTypeId] ASC
) ON [PRIMARY]
GO

INSERT INTO [dbo].[VenueQuotingSettingsSpreadType]([VenueQuotingSettingsSpreadTypeId], [VenueQuotingSettingsSpreadTypeName]) VALUES (0, 'Fixed')
INSERT INTO [dbo].[VenueQuotingSettingsSpreadType]([VenueQuotingSettingsSpreadTypeId], [VenueQuotingSettingsSpreadTypeName]) VALUES (1, 'Dynamic')
GO

DISABLE TRIGGER [dbo].[VenueQuotingSettings_DeletedOrUpdated] ON [dbo].[VenueQuotingSettings]
GO

sp_rename '[dbo].[VenueQuotingSettings].FairPriceImprovementSpreadBasisPoints', 'FixedSpreadBasisPoints'
ALTER TABLE [dbo].[VenueQuotingSettings] ADD [SpreadTypeId] [bit] NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ADD [MinimumDynamicSpreadBp] [decimal](18,6) NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ADD [DynamicMarkupDecimal] [decimal](18,10) NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ADD [EnableSkew] [bit] NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ADD [SkewDecimal] [decimal](18,10) NULL
GO

sp_rename '[dbo].[VenueQuotingSettings_Changes].FairPriceImprovementSpreadBasisPoints', 'FixedSpreadBasisPoints'
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ADD [SpreadTypeId] [bit] NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ADD [MinimumDynamicSpreadBp] [decimal](18,6) NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ADD [DynamicMarkupDecimal] [decimal](18,10) NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ADD [EnableSkew] [bit] NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ADD [SkewDecimal] [decimal](18,10) NULL
GO

UPDATE [dbo].[VenueQuotingSettings] SET 
	[SpreadTypeId] = 0,
	[MinimumDynamicSpreadBp] = 0,
	[DynamicMarkupDecimal] = 0,
	[EnableSkew] = 0,
	[SkewDecimal] = 0
	
UPDATE [dbo].[VenueQuotingSettings_Changes] SET 
	[SpreadTypeId] = 0,
	[MinimumDynamicSpreadBp] = 0,
	[DynamicMarkupDecimal] = 0,
	[EnableSkew] = 0,
	[SkewDecimal] = 0
GO

ALTER TABLE [dbo].[VenueQuotingSettings] ALTER COLUMN [SpreadTypeId] [bit] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ALTER COLUMN [MinimumDynamicSpreadBp] [decimal](18,6) NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ALTER COLUMN [DynamicMarkupDecimal] [decimal](18,10) NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ALTER COLUMN [EnableSkew] [bit] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings] ALTER COLUMN [SkewDecimal] [decimal](18,10) NOT NULL

ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ALTER COLUMN [SpreadTypeId] [bit] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ALTER COLUMN [MinimumDynamicSpreadBp] [decimal](18,6) NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ALTER COLUMN [DynamicMarkupDecimal] [decimal](18,10) NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ALTER COLUMN [EnableSkew] [bit] NOT NULL
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] ALTER COLUMN [SkewDecimal] [decimal](18,10) NOT NULL


ALTER TABLE [dbo].[VenueQuotingSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueQuotingSettings_SpreadType] FOREIGN KEY([SpreadTypeId])
REFERENCES [dbo].[VenueQuotingSettingsSpreadType] ([VenueQuotingSettingsSpreadTypeId])
GO
ALTER TABLE [dbo].[VenueQuotingSettings] CHECK CONSTRAINT [FK_VenueQuotingSettings_SpreadType]
GO

ALTER TABLE [dbo].[VenueQuotingSettings_Changes]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueQuotingSettings_Changes_SpreadType] FOREIGN KEY([SpreadTypeId])
REFERENCES [dbo].[VenueQuotingSettingsSpreadType] ([VenueQuotingSettingsSpreadTypeId])
GO
ALTER TABLE [dbo].[VenueQuotingSettings_Changes] CHECK CONSTRAINT [FK_VenueQuotingSettings_Changes_SpreadType]
GO


ALTER TRIGGER [dbo].[VenueQuotingSettings_DeletedOrUpdated] ON [dbo].[VenueQuotingSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueQuotingSettings_Changes]
	   ([SymbolId]
	   ,[CounterpartyId]
	   ,[TradingSystemId]  
	   ,[MaximumPositionBaseAbs]
	   ,[FixedSpreadBasisPoints]
	   ,[StopLossNetInUsd]
	   ,[SpreadTypeId]
	   ,[MinimumDynamicSpreadBp]
	   ,[DynamicMarkupDecimal]
	   ,[EnableSkew]
	   ,[SkewDecimal]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,deleted.[MaximumPositionBaseAbs]
		,deleted.[FixedSpreadBasisPoints]
		,deleted.[StopLossNetInUsd]
		,deleted.[SpreadTypeId]
	   ,deleted.[MinimumDynamicSpreadBp]
	   ,deleted.[DynamicMarkupDecimal]
	   ,deleted.[EnableSkew]
	   ,deleted.[SkewDecimal]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueQuotingSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueQuotingSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([MaximumPositionBaseAbs]) AND (topUpdatedChange.[MaximumPositionBaseAbs] IS NULL OR topUpdatedChange.[MaximumPositionBaseAbs] != deleted.[MaximumPositionBaseAbs]))
		OR (UPDATE([FixedSpreadBasisPoints]) AND (topUpdatedChange.[FixedSpreadBasisPoints] IS NULL OR topUpdatedChange.[FixedSpreadBasisPoints] != deleted.[FixedSpreadBasisPoints]))
		OR (UPDATE([StopLossNetInUsd]) AND (topUpdatedChange.[StopLossNetInUsd] IS NULL OR topUpdatedChange.[StopLossNetInUsd] != deleted.[StopLossNetInUsd]))	
		OR (UPDATE([SpreadTypeId]) AND (topUpdatedChange.[SpreadTypeId] IS NULL OR topUpdatedChange.[SpreadTypeId] != deleted.[SpreadTypeId]))
		OR (UPDATE([MinimumDynamicSpreadBp]) AND (topUpdatedChange.[MinimumDynamicSpreadBp] IS NULL OR topUpdatedChange.[MinimumDynamicSpreadBp] != deleted.[MinimumDynamicSpreadBp]))
		OR (UPDATE([DynamicMarkupDecimal]) AND (topUpdatedChange.[DynamicMarkupDecimal] IS NULL OR topUpdatedChange.[DynamicMarkupDecimal] != deleted.[DynamicMarkupDecimal]))
		OR (UPDATE([EnableSkew]) AND (topUpdatedChange.[EnableSkew] IS NULL OR topUpdatedChange.[EnableSkew] != deleted.[EnableSkew]))
		OR (UPDATE([SkewDecimal]) AND (topUpdatedChange.[SkewDecimal] IS NULL OR topUpdatedChange.[SkewDecimal] != deleted.[SkewDecimal]))
END
GO

ENABLE TRIGGER [dbo].[VenueQuotingSettings_DeletedOrUpdated] ON [dbo].[VenueQuotingSettings]
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueQuotingSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueQuotingSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty			
			,[MaximumPositionBaseAbs]
			,[FixedSpreadBasisPoints]
			,[StopLossNetInUsd]	
			,spreadType.[VenueQuotingSettingsSpreadTypeName] AS SpreadType
			,[MinimumDynamicSpreadBp]
			,[DynamicMarkupDecimal]
			,[EnableSkew]
			,[SkewDecimal]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueQuotingSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
			INNER JOIN [dbo].[VenueQuotingSettingsSpreadType] spreadType ON sett.[SpreadTypeId] = spreadType.[VenueQuotingSettingsSpreadTypeId]
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name 
	END
END
GO

ALTER PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		--ISNULL(symbol.LastKnownMeanReferencePrice, 0) AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FixedSpreadBasisPoints],
		sett.[FixedSpreadBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [FixedSpreadDecimal],
		sett.[StopLossNetInUsd],
		spreadType.[VenueQuotingSettingsSpreadTypeName] AS SpreadType,
		sett.[MinimumDynamicSpreadBp],
		sett.[MinimumDynamicSpreadBp] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumDynamicSpreadDecimal],
		sett.[DynamicMarkupDecimal],
		sett.[EnableSkew],
		sett.[SkewDecimal],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		INNER JOIN [dbo].[VenueQuotingSettingsSpreadType] spreadType ON sett.[SpreadTypeId] = spreadType.[VenueQuotingSettingsSpreadTypeId]
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[VenueQuotingSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MaximumPositionBaseAbs [decimal](18,0),
	@FixedSpreadBasisPoints [decimal](18,6),
	@StopLossNetInUsd [bigint],
	@SpreadType [varchar](8),
	@MinimumDynamicSpreadBp [decimal](18,6),
	@DynamicMarkupDecimal [decimal](18,10),
	@EnableSkew [bit],
	@SkewDecimal [decimal](18,10),
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @SpreadTypeId BIT
	SELECT @SpreadTypeId = [VenueQuotingSettingsSpreadTypeId] FROM [dbo].[VenueQuotingSettingsSpreadType] WHERE [VenueQuotingSettingsSpreadTypeName] = @SpreadType
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'SpreadType not found in DB: %s', 16, 2, @SpreadType) WITH SETERROR
	END

	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Quoting'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Quoting TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;

		INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)
		SELECT @TradingSystemId = scope_identity();

		--make sure that default switches values are correct
		INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
		SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
		FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
		WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
		INSERT INTO [dbo].[VenueQuotingSettings]
			([SymbolId]
			,[MaximumPositionBaseAbs]
			,[FixedSpreadBasisPoints]
			,[StopLossNetInUsd]   
			,[SpreadTypeId]
			,[MinimumDynamicSpreadBp]
			,[DynamicMarkupDecimal]
			,[EnableSkew]
			,[SkewDecimal]
			,[CounterpartyId]
			,[TradingSystemId])
		 VALUES
			(@SymbolId
			,@MaximumPositionBaseAbs
			,@FixedSpreadBasisPoints
			,@StopLossNetInUsd
			,@SpreadTypeId 
			,@MinimumDynamicSpreadBp
			,@DynamicMarkupDecimal
			,@EnableSkew
			,@SkewDecimal
			,@CounterpartyId
			,@TradingSystemId)

		COMMIT TRANSACTION;

	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[VenueQuotingSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumPositionBaseAbs [decimal](18,0),
	@FixedSpreadBasisPoints [decimal](18,6),
	@StopLossNetInUsd [bigint],
	@SpreadType [varchar](8),
	@MinimumDynamicSpreadBp [decimal](18,6),
	@DynamicMarkupDecimal [decimal](18,10),
	@EnableSkew [bit],
	@SkewDecimal [decimal](18,10)
)
AS
BEGIN
	DECLARE @SpreadTypeId BIT
	SELECT @SpreadTypeId = [VenueQuotingSettingsSpreadTypeId] FROM [dbo].[VenueQuotingSettingsSpreadType] WHERE [VenueQuotingSettingsSpreadTypeName] = @SpreadType
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'SpreadType not found in DB: %s', 16, 2, @SpreadType) WITH SETERROR
	END

	UPDATE sett
	SET
		[MaximumPositionBaseAbs] = @MaximumPositionBaseAbs,
		[FixedSpreadBasisPoints] = @FixedSpreadBasisPoints,
		[StopLossNetInUsd] = @StopLossNetInUsd,
		[SpreadTypeId] = @SpreadTypeId,
		[MinimumDynamicSpreadBp] = @MinimumDynamicSpreadBp,
		[DynamicMarkupDecimal] = @DynamicMarkupDecimal,
		[EnableSkew] = @EnableSkew,
		[SkewDecimal] = @SkewDecimal
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

ALTER PROCEDURE [dbo].[GetLastVenueQuotingSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MaximumPositionBaseAbs]
		,[FixedSpreadBasisPoints]
		,[StopLossNetInUsd]
		,spreadType.[VenueQuotingSettingsSpreadTypeName] AS SpreadType
		,[MinimumDynamicSpreadBp]
		,[DynamicMarkupDecimal]
		,[EnableSkew]
		,[SkewDecimal]
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[VenueQuotingSettingsSpreadType] spreadType ON sett.[SpreadTypeId] = spreadType.[VenueQuotingSettingsSpreadTypeId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END
GO



GRANT EXECUTE ON [dbo].[VenueSystemDisableEnableOne_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

--ROLLBACK TRANSACTION AAA
--GO
--COMMIT TRANSACTION AAA
--GO