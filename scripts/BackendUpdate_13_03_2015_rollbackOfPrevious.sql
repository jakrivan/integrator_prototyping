ALTER PROC [dbo].[GetSymbolReferencePrices_SP]
AS
BEGIN
	SELECT 
		[Name]
		,[LastKnownMeanReferencePrice]
		,[MeanReferencePriceUpdated]
	FROM 
		[dbo].[Symbol]
	WHERE
		[IsRaboTradeable] = 1
END
GO

ALTER PROCEDURE [dbo].[GetSymblTrustedRates_SP]
AS
BEGIN
	SELECT
		symbol.[Name] AS Symbol,
		spreads.[MaximumTrustedSpreadBp] AS MaxTrustedSpreadBp,
		spreads.[MaximumTrustedSpreadDecimal] AS MaxTrustedSpreadDecimal,
		spreads.[MinimumTrustedSpreadBp] AS MinTrustedSpreadBp,
		spreads.[MinimumTrustedSpreadDecimal] AS MinTrustedSpreadDecimal,
		spreads.[MaximumAllowedTickAge_milliseconds] AS MaximumAllowedTickAge_milliseconds
	FROM
		[dbo].[Symbol_TrustedSpreads] spreads
		INNER JOIN [dbo].[Symbol] symbol ON spreads.SymbolId = symbol.Id
	WHERE
		[IsRaboTradeable] = 1
END
GO

--manually RiskMgmgt
-- remove <DefaultNewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent>15</DefaultNewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent>

--add

--<Symbol ShortName="AUDCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="AUDCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="AUDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="AUDNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="AUDSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="AUDUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CADJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CHFJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURAUD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURGBP" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPAUD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="HKDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NOKSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NZDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NZDSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NZDUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="SGDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDILS" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDTRY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="ZARJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CADCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CADNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NZDCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="AUDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="AUDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURTRY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NZDCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="AUDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CADDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CADNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CADSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CHFDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CHFNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CHFSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="DKKJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="DKKNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="DKKSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="MXNJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NOKJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NZDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NZDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NZDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="AUDHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURRUB" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDRUB" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURCNH" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDCNH" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPTRY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CHFMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDINR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="AUDZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CADHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CADMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CADPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CADSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CADZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CZKJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="DKKHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="EURILS" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="GBPILS" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="HUFJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CHFCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CHFHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CHFHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CHFILS" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CHFPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CHFSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CHFTRY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="CHFZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NOKHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NZDHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NZDPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="NZDZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="PLNHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="PLNJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="SEKHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="SEKJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="SGDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="SGDHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="SGDMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="SGDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="SGDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="TRYJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDGHS" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDKES" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDNGN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDRON" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDUGX" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDZMK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="ZARMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDTHB" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
--<Symbol ShortName="USDXVN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="0.5" />
