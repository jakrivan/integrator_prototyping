

CREATE PROCEDURE [dbo].[Deduplicate_AggregatedExternalExecutedTicket_SP] 
AS
BEGIN
	CREATE TABLE #Temp_Deduped_AggregatedExternalExecutedTicket(
		[AggregatedExternalExecutedTicketCounterpartyId] [nvarchar](50) NOT NULL,
		[IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
		[CounterpartySentTimeUtc] [datetime2](7) NULL,
		[AggregatedSizeBaseAbs] [decimal](18, 2) NOT NULL,
		[AggregatedPrice] [decimal](18, 10) NOT NULL,
		[StpCounterpartyId] [tinyint] NOT NULL,
		[FixMessageRaw] [nvarchar](1024) NOT NULL,
		[FixMessageParsed] [nvarchar](max) NOT NULL
	) 

	INSERT INTO #Temp_Deduped_AggregatedExternalExecutedTicket
			   ([AggregatedExternalExecutedTicketCounterpartyId]
			   ,[IntegratorReceivedTimeUtc]
			   ,[CounterpartySentTimeUtc]
			   ,[AggregatedSizeBaseAbs]
			   ,[AggregatedPrice]
			   ,[StpCounterpartyId]
			   ,[FixMessageRaw]
			   ,[FixMessageParsed])
	SELECT DISTINCT
		[AggregatedExternalExecutedTicketCounterpartyId]
		  ,[IntegratorReceivedTimeUtc]
		  ,[CounterpartySentTimeUtc]
		  ,[AggregatedSizeBaseAbs]
		  ,[AggregatedPrice]
		  ,[StpCounterpartyId]
		  ,[FixMessageRaw]
		  ,[FixMessageParsed]
	FROM [dbo].[AggregatedExternalExecutedTicket]

	TRUNCATE TABLE [dbo].[AggregatedExternalExecutedTicket]

	INSERT INTO [dbo].[AggregatedExternalExecutedTicket]
			   ([AggregatedExternalExecutedTicketCounterpartyId]
			   ,[IntegratorReceivedTimeUtc]
			   ,[CounterpartySentTimeUtc]
			   ,[AggregatedSizeBaseAbs]
			   ,[AggregatedPrice]
			   ,[StpCounterpartyId]
			   ,[FixMessageRaw]
			   ,[FixMessageParsed])
	SELECT DISTINCT
		[AggregatedExternalExecutedTicketCounterpartyId]
		  ,[IntegratorReceivedTimeUtc]
		  ,[CounterpartySentTimeUtc]
		  ,[AggregatedSizeBaseAbs]
		  ,[AggregatedPrice]
		  ,[StpCounterpartyId]
		  ,[FixMessageRaw]
		  ,[FixMessageParsed]
	FROM #Temp_Deduped_AggregatedExternalExecutedTicket

	DROP TABLE #Temp_Deduped_AggregatedExternalExecutedTicket
END
GO

exec [dbo].[Deduplicate_AggregatedExternalExecutedTicket_SP] 
GO


ALTER FUNCTION [dbo].[csvlist_to_codes_tbl] (@list nvarchar(MAX))
   RETURNS @tbl TABLE (code varchar(50) NOT NULL) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1
      INSERT @tbl (code)
         VALUES (CONVERT(varchar(50), substring(@list, @pos + 1, @valuelen)))
      SELECT @pos = @nextpos
   END
   RETURN
END
GO