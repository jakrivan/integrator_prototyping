

ALTER TABLE [dbo].[DealExternalRejected] ADD RejectedPrice [decimal](18, 9) NULL
GO

ALTER PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 0),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@RejectedPrice decimal(18, 9)
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[DealExternalRejected]
           ([IntegratorReceivedExecutionReportUtc]
           ,[SymbolId]
           ,[CounterpartyId]
           ,[IntegratorExternalOrderPrivateId]
		   ,[DealDirectionId]
           ,[AmountBasePolRejected]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[IntegratorSentExternalOrderUtc]
           ,[RejectionReason]
		   ,[CounterpartySentExecutionReportUtc]
		   ,[RejectedPrice])
     VALUES
           (@ExecutionReportReceivedUtc
		   ,@SymbolId
           ,@CounterpartyId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolRejected
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@QuoteReceivedToOrderSentInternalLatency
		   ,@IntegratorSentExternalOrderUtc
		   ,@RejectionReason
		   ,@CounterpartySentExecutionReportUtc
		   ,@RejectedPrice)
END
GO

-------


ALTER TABLE [dbo].[OrderExternalIgnored] ADD ExternalOrderLabelForCounterparty [NVARCHAR](32) NULL
ALTER TABLE [dbo].[OrderExternalIgnored] ADD RequestedAmountBaseAbs [DECIMAL](18,0) NULL
ALTER TABLE [dbo].[OrderExternalIgnored] ADD RequestedPrice [DECIMAL](18,9) NULL
ALTER TABLE [dbo].[OrderExternalIgnored] ADD CounterpartyId TINYINT NULL
ALTER TABLE [dbo].[OrderExternalIgnored] ADD SymbolId TINYINT NULL
ALTER TABLE [dbo].[OrderExternalIgnored] ADD DealDirectionId BIT NULL
GO


UPDATE
	ignoredOrder
SET
	ignoredOrder.ExternalOrderLabelForCounterparty = ord.ExternalOrderLabelForCounterparty,
	ignoredOrder.RequestedAmountBaseAbs = ord.RequestedAmountBaseAbs,
	ignoredOrder.RequestedPrice = ord.RequestedPrice,
	ignoredOrder.CounterpartyId = ord.CounterpartyId,
	ignoredOrder.SymbolId = ord.SymbolId,
	ignoredOrder.DealDirectionId = ord.DealDirectionId
FROM
	[dbo].[OrderExternalIgnored] ignoredOrder
	INNER JOIN [dbo].[OrderExternal] ord ON ignoredOrder.IntegratorSentExternalOrderUtc = ord.IntegratorSentExternalOrderUtc AND ignoredOrder.IntegratorExternalOrderPrivateId = ord.IntegratorExternalOrderPrivateId
GO

UPDATE
	ignoredOrder
SET
	ignoredOrder.ExternalOrderLabelForCounterparty = ord.ExternalOrderLabelForCounterparty,
	ignoredOrder.RequestedAmountBaseAbs = ord.RequestedAmountBaseAbs,
	ignoredOrder.RequestedPrice = ord.RequestedPrice,
	ignoredOrder.CounterpartyId = ord.CounterpartyId,
	ignoredOrder.SymbolId = ord.SymbolId,
	ignoredOrder.DealDirectionId = ord.DealDirectionId
FROM
	[dbo].[OrderExternalIgnored] ignoredOrder
	INNER JOIN [dbo].[OrderExternal_Archive] ord ON ignoredOrder.IntegratorSentExternalOrderUtc = ord.IntegratorSentExternalOrderUtc AND ignoredOrder.IntegratorExternalOrderPrivateId = ord.IntegratorExternalOrderPrivateId
GO

UPDATE
	ignoredOrder
SET
	ignoredOrder.ExternalOrderLabelForCounterparty = ord.ExternalOrderLabelForCounterparty,
	ignoredOrder.RequestedAmountBaseAbs = ord.RequestedAmountBaseAbs,
	ignoredOrder.RequestedPrice = ord.RequestedPrice,
	ignoredOrder.CounterpartyId = ord.CounterpartyId,
	ignoredOrder.SymbolId = ord.SymbolId,
	ignoredOrder.DealDirectionId = ord.DealDirectionId
FROM
	[dbo].[OrderExternalIgnored] ignoredOrder
	INNER JOIN [dbo].[OrderExternal_Archive2] ord ON ignoredOrder.IntegratorSentExternalOrderUtc = ord.IntegratorSentExternalOrderUtc AND ignoredOrder.IntegratorExternalOrderPrivateId = ord.IntegratorExternalOrderPrivateId
GO

ALTER TABLE [dbo].[OrderExternalIgnored] ALTER COLUMN [ExternalOrderLabelForCounterparty] [NVARCHAR](32) NOT NULL
ALTER TABLE [dbo].[OrderExternalIgnored] ALTER COLUMN [RequestedAmountBaseAbs] [DECIMAL](18,0) NOT NULL
ALTER TABLE [dbo].[OrderExternalIgnored] ALTER COLUMN [RequestedPrice] [DECIMAL](18,9) NOT NULL
ALTER TABLE [dbo].[OrderExternalIgnored] ALTER COLUMN [CounterpartyId] TINYINT NOT NULL
ALTER TABLE [dbo].[OrderExternalIgnored] ALTER COLUMN [SymbolId] TINYINT NOT NULL
ALTER TABLE [dbo].[OrderExternalIgnored] ALTER COLUMN [DealDirectionId] BIT NOT NULL
GO


ALTER PROCEDURE [dbo].[GetIgnoredOrders_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN
	
	SELECT 
		ignoredOrder.[IntegratorSentExternalOrderUtc] AS IntegratorSentExternalOrderUtc
		,ignoredOrder.ExternalOrderLabelForCounterparty AS ExternalOrderLabelForCounterparty
		,ignoredOrder.RequestedAmountBaseAbs AS RequestedAmountBaseAbs
		,ignoredOrder.RequestedPrice AS RequestedPrice
		,ctp.CounterpartyCode AS CounterpartyCode
		,symbol.Name AS Symbol
		,direction.Name AS Direction
	FROM
		[dbo].[OrderExternalIgnored] ignoredOrder
		INNER JOIN [dbo].[Counterparty] ctp on ctp.CounterpartyId = ignoredOrder.CounterpartyId
		INNER JOIN [dbo].Symbol symbol on symbol.Id = ignoredOrder.SymbolId
		INNER JOIN [dbo].[DealDirection] direction on direction.Id = ignoredOrder.DealDirectionId
	WHERE 
		ignoredOrder.[IntegratorSentExternalOrderUtc] BETWEEN @StartTime AND @EndTime

END
GO

ALTER PROCEDURE [dbo].[InsertOrderExternalIgnored_SP]( 
	@ExternalOrderSentUtc [datetime2](7),
	@ExternalOrderIgnoreDetectedUtc [datetime2](7),
	@IntegratorExternalOrderPrivateId [nvarchar](45),
	@InternalClientOrderId [nvarchar](32),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,0),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4)
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END

	INSERT INTO 
	[dbo].[OrderExternalIgnored]
           ([IntegratorSentExternalOrderUtc]
           ,[IntegratorDetectedOrderIsIgnoredUtc]
           ,[IntegratorExternalOrderPrivateId]
		   ,[ExternalOrderLabelForCounterparty]
		   ,[RequestedAmountBaseAbs]
		   ,[RequestedPrice]
		   ,[CounterpartyId]
		   ,[SymbolId]
		   ,[DealDirectionId])
     VALUES
           (@ExternalOrderSentUtc
           ,@ExternalOrderIgnoreDetectedUtc
           ,@IntegratorExternalOrderPrivateId
		   ,@InternalClientOrderId
		   ,@RequestedAmountBaseAbs
		   ,@RequestedPrice
		   ,@CounterpartyId
		   ,@SymbolId
		   ,@DealDirectionId)
END
GO
	
	
	
IF EXISTS(SELECT * FROM  sys.synonyms s WHERE s.name = 'OrderExternal_DailyView')
BEGIN
	DROP SYNONYM OrderExternal_DailyView
END

DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)

DECLARE @Query NVARCHAR(MAX)
SET @Query = N'
IF EXISTS(select * FROM sys.views where name = ''OrderExternalDailyView_' + @TodayLiteral + ''')
BEGIN
	DROP VIEW [OrderExternalDailyView_' + @TodayLiteral + ']
END
'

EXECUTE sp_executesql @Query
GO

DROP PROCEDURE [dbo].[CreateDailyOrdersView_SP]
GO

--MANUALLY!!!
--Delete first step from daily job


CREATE PROCEDURE [dbo].[GetDbMappingsForSymbols_SP] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[Id] AS DbId, 
		[Name] AS EnumName
	FROM
		[dbo].[Symbol]
END
GO

GRANT EXECUTE ON [dbo].[GetDbMappingsForSymbols_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetDbMappingsForCounterparty_SP] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[CounterpartyId] AS DbId, 
		[CounterpartyCode] AS EnumName
	FROM
		[dbo].[Counterparty]
END
GO

GRANT EXECUTE ON [dbo].[GetDbMappingsForCounterparty_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetDbMappingsForOrderType_SP] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[OrderTypeId] AS DbId, 
		[OrderTypeName] AS EnumName
	FROM
		[dbo].[OrderType]
END
GO

GRANT EXECUTE ON [dbo].[GetDbMappingsForOrderType_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

CREATE PROCEDURE [dbo].[GetDbMappingsForOrderTimeInForce_SP] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[OrderTimeInForceId] AS DbId, 
		[OrderTimeInForceName] AS EnumName
	FROM
		[dbo].[OrderTimeInForce]
END
GO

GRANT EXECUTE ON [dbo].[GetDbMappingsForOrderTimeInForce_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


CREATE PROCEDURE [dbo].[GetDbMappingsForDealDirection_SP] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CAST([Id] AS TINYINT) AS DbId, 
		[Name] AS EnumName
	FROM
		[dbo].[DealDirection]
END
GO

GRANT EXECUTE ON [dbo].[GetDbMappingsForDealDirection_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


CREATE PROCEDURE [dbo].[GetDbMappingsForPegType_SP] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CAST([PegTypeId] AS TINYINT) AS DbId, 
		[PegTypeName] AS EnumName
	FROM
		[dbo].[PegType]
END
GO

GRANT EXECUTE ON [dbo].[GetDbMappingsForPegType_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

ALTER TABLE [dbo].[OrderType] ALTER COLUMN [OrderTypeName] VARCHAR(19)
GO

INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (4, N'MarketOnImprovement')
GO