/****** Object:  View [dbo].[DealExternalExecuted_View]    Script Date: 29. 7. 2013 16:10:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[DealExternalExecuted_View]
AS
SELECT
	[OrderId]
	,[ExecutionTimeStampUtc]
	,[SymbolId]
	,[DealDirectionId]
	,[ExecutedAmountBaseAbs]
	,[ExecutedAmountBasePol]
	,[Price]
FROM [dbo].[ExecutedExternalDeals]
WHERE [ExecutionTimeStampUtc] < '2013-07-29 14:30:00.000'

  UNION ALL

SELECT 
	[InternalOrderId] AS [OrderId]
	,[ExecutionReportReceivedUtc] AS [ExecutionTimeStampUtc]
	,[SymbolId]
	,[DealDirectionId]
	,ABS([AmountBasePolExecuted]) AS [ExecutedAmountBaseAbs]
	,[AmountBasePolExecuted] AS [ExecutedAmountBasePol]
	,[Price]
FROM [dbo].[DealExternalExecuted]
WHERE [ExecutionReportReceivedUtc] > '2013-07-29 14:30:00.000'

GO


/****** Object:  StoredProcedure [dbo].[GetPolarizedNopPerPair_SP]    Script Date: 30. 7. 2013 9:47:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetPolarizedNopPerPair_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN
	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[ExecutedAmountBasePol]) AS PolarizedNop
	FROM 
		[dbo].[DealExternalExecuted_View] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[ExecutionTimeStampUtc] BETWEEN @StartTime AND @EndTime
	GROUP BY
		symbol.[Name]

END