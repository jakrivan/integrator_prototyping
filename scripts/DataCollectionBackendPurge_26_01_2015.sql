
-----------
-- script for obtaining partition numbers
------------


--SELECT OBJECT_NAME(i.object_id) AS OBJECT_NAME,

--pair.[FxpCode],

--p.partition_number,



--right(fg.NAME, 6),

----CAST(VALUE AS INT) - p.partition_number As difference,

--fg.NAME AS FILEGROUP_NAME,

--ROWS,

--au.total_pages,

--CASE boundary_value_on_right

--WHEN 1 THEN 'Less than'

--ELSE 'Less or equal than' END AS 'Comparison',

--VALUE

--FROM sys.partitions p

--JOIN sys.indexes i ON p.object_id = i.object_id AND p.index_id = i.index_id

--JOIN sys.partition_schemes ps ON ps.data_space_id = i.data_space_id

--JOIN sys.partition_functions f ON f.function_id = ps.function_id

--LEFT JOIN sys.partition_range_values rv ON f.function_id = rv.function_id AND p.partition_number = rv.boundary_id

--JOIN sys.destination_data_spaces dds ON dds.partition_scheme_id = ps.data_space_id AND dds.destination_id = p.partition_number

--JOIN sys.filegroups fg ON dds.data_space_id = fg.data_space_id

--JOIN (

--SELECT container_id, SUM(total_pages) AS total_pages

--FROM sys.allocation_units

--GROUP BY container_id

--) AS au ON au.container_id = p.partition_id

--left join [dbo].[FxPair] pair on pair.[FxPairId] = CAST(VALUE AS INT)

--WHERE i.index_id < 2

--AND OBJECT_NAME(i.object_id) = 'MarketData'

--AND pair.[FxpCode] IN 

--('AUDDKK', 'AUDHKD', 'AUDNOK', 'AUDSEK', 'AUDSGD', 'CADDKK', 'CADHKD', 'CADNOK', 'CADSEK', 'CHFDKK', 'CHFHKD', 'CHFMXN', 'CHFNOK', 'CHFSEK', 'DKKHKD', 'DKKJPY', 'DKKNOK', 'DKKSEK', 'EURDKK', 'EURHKD', 'EURSGD', 'GBPCZK', 'GBPDKK', 'GBPHKD', 'GBPHUF', 'GBPMXN', 'GBPPLN', 'GBPSGD', 'GBPTRY', 'GBPZAR', 'HKDJPY', 'MXNJPY', 'NOKHKD', 'NOKJPY', 'NZDCHF', 'NZDDKK', 'NZDHKD', 'NZDNOK', 'NZDSEK', 'NZDSGD', 'SEKHKD', 'SGDDKK', 'SGDHKD', 'SGDJPY', 'USDDKK', 'ZARJPY')

--order by pair.[FxpCode] asc

----AND pair.[FxpCode] = right(fg.NAME, 6)

----AND (fg.name like '%AUDHKD%')




---------------------------------   AUDDKK   -------------------------------------------------------------

print 'Starting purge on AUDDKK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveAUDDKK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_AUDDKK
GO


CREATE CLUSTERED INDEX [IX_PairTime_AUDDKK] ON [dbo].[MarketData_ArchiveAUDDKK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_AUDDKK
GO


ALTER TABLE MarketData SWITCH PARTITION 42 TO MarketData_ArchiveAUDDKK
GO

truncate table MarketData_ArchiveAUDDKK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_AUDDKK_File01, 10)
GO

DROP TABLE MarketData_ArchiveAUDDKK
GO

-------

print 'Starting purge on AUDDKK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveAUDDKK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_AUDDKK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveAUDDKK] ON [dbo].[Tick_ArchiveAUDDKK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_AUDDKK
GO


ALTER TABLE Tick SWITCH PARTITION 42 TO Tick_ArchiveAUDDKK
GO

truncate table Tick_ArchiveAUDDKK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_AUDDKK_File01, 10)
GO

DROP TABLE Tick_ArchiveAUDDKK
GO

--------------------------------- END OF  AUDDKK   --------------------------------------------------------


---------------------------------   AUDHKD   -------------------------------------------------------------

print 'Starting purge on AUDHKD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveAUDHKD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_AUDHKD
GO


CREATE CLUSTERED INDEX [IX_PairTime_AUDHKD] ON [dbo].[MarketData_ArchiveAUDHKD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_AUDHKD
GO


ALTER TABLE MarketData SWITCH PARTITION 77 TO MarketData_ArchiveAUDHKD
GO

truncate table MarketData_ArchiveAUDHKD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_AUDHKD_File01, 10)
GO

DROP TABLE MarketData_ArchiveAUDHKD
GO

-------

print 'Starting purge on AUDHKD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveAUDHKD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_AUDHKD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveAUDHKD] ON [dbo].[Tick_ArchiveAUDHKD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_AUDHKD
GO


ALTER TABLE Tick SWITCH PARTITION 77 TO Tick_ArchiveAUDHKD
GO

truncate table Tick_ArchiveAUDHKD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_AUDHKD_File01, 10)
GO

DROP TABLE Tick_ArchiveAUDHKD
GO

--------------------------------- END OF  AUDHKD   --------------------------------------------------------


---------------------------------   AUDNOK   -------------------------------------------------------------

print 'Starting purge on AUDNOK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveAUDNOK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_AUDNOK
GO


CREATE CLUSTERED INDEX [IX_PairTime_AUDNOK] ON [dbo].[MarketData_ArchiveAUDNOK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_AUDNOK
GO


ALTER TABLE MarketData SWITCH PARTITION 43 TO MarketData_ArchiveAUDNOK
GO

truncate table MarketData_ArchiveAUDNOK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_AUDNOK_File01, 10)
GO

DROP TABLE MarketData_ArchiveAUDNOK
GO

-------

print 'Starting purge on AUDNOK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveAUDNOK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_AUDNOK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveAUDNOK] ON [dbo].[Tick_ArchiveAUDNOK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_AUDNOK
GO


ALTER TABLE Tick SWITCH PARTITION 43 TO Tick_ArchiveAUDNOK
GO

truncate table Tick_ArchiveAUDNOK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_AUDNOK_File01, 10)
GO

DROP TABLE Tick_ArchiveAUDNOK
GO

--------------------------------- END OF  AUDNOK   --------------------------------------------------------


---------------------------------   AUDSEK   -------------------------------------------------------------

print 'Starting purge on AUDSEK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveAUDSEK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_AUDSEK
GO


CREATE CLUSTERED INDEX [IX_PairTime_AUDSEK] ON [dbo].[MarketData_ArchiveAUDSEK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_AUDSEK
GO


ALTER TABLE MarketData SWITCH PARTITION 53 TO MarketData_ArchiveAUDSEK
GO

truncate table MarketData_ArchiveAUDSEK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_AUDSEK_File01, 10)
GO

DROP TABLE MarketData_ArchiveAUDSEK
GO

-------

print 'Starting purge on AUDSEK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveAUDSEK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_AUDSEK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveAUDSEK] ON [dbo].[Tick_ArchiveAUDSEK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_AUDSEK
GO


ALTER TABLE Tick SWITCH PARTITION 53 TO Tick_ArchiveAUDSEK
GO

truncate table Tick_ArchiveAUDSEK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_AUDSEK_File01, 10)
GO

DROP TABLE Tick_ArchiveAUDSEK
GO

--------------------------------- END OF  AUDSEK   --------------------------------------------------------


---------------------------------   AUDSGD   -------------------------------------------------------------

print 'Starting purge on AUDSGD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveAUDSGD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_AUDSGD
GO


CREATE CLUSTERED INDEX [IX_PairTime_AUDSGD] ON [dbo].[MarketData_ArchiveAUDSGD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_AUDSGD
GO


ALTER TABLE MarketData SWITCH PARTITION 72 TO MarketData_ArchiveAUDSGD
GO

truncate table MarketData_ArchiveAUDSGD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_AUDSGD_File01, 10)
GO

DROP TABLE MarketData_ArchiveAUDSGD
GO

-------

print 'Starting purge on AUDSGD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveAUDSGD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_AUDSGD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveAUDSGD] ON [dbo].[Tick_ArchiveAUDSGD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_AUDSGD
GO


ALTER TABLE Tick SWITCH PARTITION 72 TO Tick_ArchiveAUDSGD
GO

truncate table Tick_ArchiveAUDSGD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_AUDSGD_File01, 10)
GO

DROP TABLE Tick_ArchiveAUDSGD
GO

--------------------------------- END OF  AUDSGD   --------------------------------------------------------


---------------------------------   CADDKK   -------------------------------------------------------------

print 'Starting purge on CADDKK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveCADDKK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_CADDKK
GO


CREATE CLUSTERED INDEX [IX_PairTime_CADDKK] ON [dbo].[MarketData_ArchiveCADDKK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_CADDKK
GO


ALTER TABLE MarketData SWITCH PARTITION 54 TO MarketData_ArchiveCADDKK
GO

truncate table MarketData_ArchiveCADDKK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_CADDKK_File01, 10)
GO

DROP TABLE MarketData_ArchiveCADDKK
GO

-------

print 'Starting purge on CADDKK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveCADDKK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_CADDKK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveCADDKK] ON [dbo].[Tick_ArchiveCADDKK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_CADDKK
GO


ALTER TABLE Tick SWITCH PARTITION 54 TO Tick_ArchiveCADDKK
GO

truncate table Tick_ArchiveCADDKK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_CADDKK_File01, 10)
GO

DROP TABLE Tick_ArchiveCADDKK
GO

--------------------------------- END OF  CADDKK   --------------------------------------------------------


---------------------------------   CADHKD   -------------------------------------------------------------

print 'Starting purge on CADHKD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveCADHKD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_CADHKD
GO


CREATE CLUSTERED INDEX [IX_PairTime_CADHKD] ON [dbo].[MarketData_ArchiveCADHKD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_CADHKD
GO


ALTER TABLE MarketData SWITCH PARTITION 95 TO MarketData_ArchiveCADHKD
GO

truncate table MarketData_ArchiveCADHKD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_CADHKD_File01, 10)
GO

DROP TABLE MarketData_ArchiveCADHKD
GO

-------

print 'Starting purge on CADHKD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveCADHKD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_CADHKD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveCADHKD] ON [dbo].[Tick_ArchiveCADHKD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_CADHKD
GO


ALTER TABLE Tick SWITCH PARTITION 95 TO Tick_ArchiveCADHKD
GO

truncate table Tick_ArchiveCADHKD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_CADHKD_File01, 10)
GO

DROP TABLE Tick_ArchiveCADHKD
GO

--------------------------------- END OF  CADHKD   --------------------------------------------------------


---------------------------------   CADNOK   -------------------------------------------------------------

print 'Starting purge on CADNOK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveCADNOK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_CADNOK
GO


CREATE CLUSTERED INDEX [IX_PairTime_CADNOK] ON [dbo].[MarketData_ArchiveCADNOK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_CADNOK
GO


ALTER TABLE MarketData SWITCH PARTITION 55 TO MarketData_ArchiveCADNOK
GO

truncate table MarketData_ArchiveCADNOK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_CADNOK_File01, 10)
GO

DROP TABLE MarketData_ArchiveCADNOK
GO

-------

print 'Starting purge on CADNOK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveCADNOK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_CADNOK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveCADNOK] ON [dbo].[Tick_ArchiveCADNOK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_CADNOK
GO


ALTER TABLE Tick SWITCH PARTITION 55 TO Tick_ArchiveCADNOK
GO

truncate table Tick_ArchiveCADNOK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_CADNOK_File01, 10)
GO

DROP TABLE Tick_ArchiveCADNOK
GO

--------------------------------- END OF  CADNOK   --------------------------------------------------------


---------------------------------   CADSEK   -------------------------------------------------------------

print 'Starting purge on CADSEK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveCADSEK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_CADSEK
GO


CREATE CLUSTERED INDEX [IX_PairTime_CADSEK] ON [dbo].[MarketData_ArchiveCADSEK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_CADSEK
GO


ALTER TABLE MarketData SWITCH PARTITION 56 TO MarketData_ArchiveCADSEK
GO

truncate table MarketData_ArchiveCADSEK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_CADSEK_File01, 10)
GO

DROP TABLE MarketData_ArchiveCADSEK
GO

-------

print 'Starting purge on CADSEK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveCADSEK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_CADSEK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveCADSEK] ON [dbo].[Tick_ArchiveCADSEK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_CADSEK
GO


ALTER TABLE Tick SWITCH PARTITION 56 TO Tick_ArchiveCADSEK
GO

truncate table Tick_ArchiveCADSEK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_CADSEK_File01, 10)
GO

DROP TABLE Tick_ArchiveCADSEK
GO

--------------------------------- END OF  CADSEK   --------------------------------------------------------


---------------------------------   CHFDKK   -------------------------------------------------------------

print 'Starting purge on CHFDKK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveCHFDKK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_CHFDKK
GO


CREATE CLUSTERED INDEX [IX_PairTime_CHFDKK] ON [dbo].[MarketData_ArchiveCHFDKK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_CHFDKK
GO


ALTER TABLE MarketData SWITCH PARTITION 57 TO MarketData_ArchiveCHFDKK
GO

truncate table MarketData_ArchiveCHFDKK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_CHFDKK_File01, 10)
GO

DROP TABLE MarketData_ArchiveCHFDKK
GO

-------

print 'Starting purge on CHFDKK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveCHFDKK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_CHFDKK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveCHFDKK] ON [dbo].[Tick_ArchiveCHFDKK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_CHFDKK
GO


ALTER TABLE Tick SWITCH PARTITION 57 TO Tick_ArchiveCHFDKK
GO

truncate table Tick_ArchiveCHFDKK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_CHFDKK_File01, 10)
GO

DROP TABLE Tick_ArchiveCHFDKK
GO

--------------------------------- END OF  CHFDKK   --------------------------------------------------------


---------------------------------   CHFHKD   -------------------------------------------------------------

print 'Starting purge on CHFHKD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveCHFHKD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_CHFHKD
GO


CREATE CLUSTERED INDEX [IX_PairTime_CHFHKD] ON [dbo].[MarketData_ArchiveCHFHKD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_CHFHKD
GO


ALTER TABLE MarketData SWITCH PARTITION 106 TO MarketData_ArchiveCHFHKD
GO

truncate table MarketData_ArchiveCHFHKD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_CHFHKD_File01, 10)
GO

DROP TABLE MarketData_ArchiveCHFHKD
GO

-------

print 'Starting purge on CHFHKD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveCHFHKD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_CHFHKD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveCHFHKD] ON [dbo].[Tick_ArchiveCHFHKD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_CHFHKD
GO


ALTER TABLE Tick SWITCH PARTITION 106 TO Tick_ArchiveCHFHKD
GO

truncate table Tick_ArchiveCHFHKD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_CHFHKD_File01, 10)
GO

DROP TABLE Tick_ArchiveCHFHKD
GO

--------------------------------- END OF  CHFHKD   --------------------------------------------------------


---------------------------------   CHFMXN   -------------------------------------------------------------

print 'Starting purge on CHFMXN (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveCHFMXN](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_CHFMXN
GO


CREATE CLUSTERED INDEX [IX_PairTime_CHFMXN] ON [dbo].[MarketData_ArchiveCHFMXN]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_CHFMXN
GO


ALTER TABLE MarketData SWITCH PARTITION 92 TO MarketData_ArchiveCHFMXN
GO

truncate table MarketData_ArchiveCHFMXN
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_CHFMXN_File01, 10)
GO

DROP TABLE MarketData_ArchiveCHFMXN
GO

-------

print 'Starting purge on CHFMXN (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveCHFMXN](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_CHFMXN
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveCHFMXN] ON [dbo].[Tick_ArchiveCHFMXN]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_CHFMXN
GO


ALTER TABLE Tick SWITCH PARTITION 92 TO Tick_ArchiveCHFMXN
GO

truncate table Tick_ArchiveCHFMXN
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_CHFMXN_File01, 10)
GO

DROP TABLE Tick_ArchiveCHFMXN
GO

--------------------------------- END OF  CHFMXN   --------------------------------------------------------


---------------------------------   CHFNOK   -------------------------------------------------------------

print 'Starting purge on CHFNOK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveCHFNOK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_CHFNOK
GO


CREATE CLUSTERED INDEX [IX_PairTime_CHFNOK] ON [dbo].[MarketData_ArchiveCHFNOK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_CHFNOK
GO


ALTER TABLE MarketData SWITCH PARTITION 58 TO MarketData_ArchiveCHFNOK
GO

truncate table MarketData_ArchiveCHFNOK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_CHFNOK_File01, 10)
GO

DROP TABLE MarketData_ArchiveCHFNOK
GO

-------

print 'Starting purge on CHFNOK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveCHFNOK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_CHFNOK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveCHFNOK] ON [dbo].[Tick_ArchiveCHFNOK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_CHFNOK
GO


ALTER TABLE Tick SWITCH PARTITION 58 TO Tick_ArchiveCHFNOK
GO

truncate table Tick_ArchiveCHFNOK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_CHFNOK_File01, 10)
GO

DROP TABLE Tick_ArchiveCHFNOK
GO

--------------------------------- END OF  CHFNOK   --------------------------------------------------------


---------------------------------   CHFSEK   -------------------------------------------------------------

print 'Starting purge on CHFSEK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveCHFSEK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_CHFSEK
GO


CREATE CLUSTERED INDEX [IX_PairTime_CHFSEK] ON [dbo].[MarketData_ArchiveCHFSEK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_CHFSEK
GO


ALTER TABLE MarketData SWITCH PARTITION 59 TO MarketData_ArchiveCHFSEK
GO

truncate table MarketData_ArchiveCHFSEK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_CHFSEK_File01, 10)
GO

DROP TABLE MarketData_ArchiveCHFSEK
GO

-------

print 'Starting purge on CHFSEK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveCHFSEK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_CHFSEK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveCHFSEK] ON [dbo].[Tick_ArchiveCHFSEK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_CHFSEK
GO


ALTER TABLE Tick SWITCH PARTITION 59 TO Tick_ArchiveCHFSEK
GO

truncate table Tick_ArchiveCHFSEK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_CHFSEK_File01, 10)
GO

DROP TABLE Tick_ArchiveCHFSEK
GO

--------------------------------- END OF  CHFSEK   --------------------------------------------------------


---------------------------------   DKKHKD   -------------------------------------------------------------

print 'Starting purge on DKKHKD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveDKKHKD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_DKKHKD
GO


CREATE CLUSTERED INDEX [IX_PairTime_DKKHKD] ON [dbo].[MarketData_ArchiveDKKHKD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_DKKHKD
GO


ALTER TABLE MarketData SWITCH PARTITION 101 TO MarketData_ArchiveDKKHKD
GO

truncate table MarketData_ArchiveDKKHKD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_DKKHKD_File01, 10)
GO

DROP TABLE MarketData_ArchiveDKKHKD
GO

-------

print 'Starting purge on DKKHKD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveDKKHKD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_DKKHKD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveDKKHKD] ON [dbo].[Tick_ArchiveDKKHKD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_DKKHKD
GO


ALTER TABLE Tick SWITCH PARTITION 101 TO Tick_ArchiveDKKHKD
GO

truncate table Tick_ArchiveDKKHKD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_DKKHKD_File01, 10)
GO

DROP TABLE Tick_ArchiveDKKHKD
GO

--------------------------------- END OF  DKKHKD   --------------------------------------------------------


---------------------------------   DKKJPY   -------------------------------------------------------------

print 'Starting purge on DKKJPY (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveDKKJPY](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_DKKJPY
GO


CREATE CLUSTERED INDEX [IX_PairTime_DKKJPY] ON [dbo].[MarketData_ArchiveDKKJPY]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_DKKJPY
GO


ALTER TABLE MarketData SWITCH PARTITION 60 TO MarketData_ArchiveDKKJPY
GO

truncate table MarketData_ArchiveDKKJPY
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_DKKJPY_File01, 10)
GO

DROP TABLE MarketData_ArchiveDKKJPY
GO

-------

print 'Starting purge on DKKJPY (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveDKKJPY](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_DKKJPY
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveDKKJPY] ON [dbo].[Tick_ArchiveDKKJPY]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_DKKJPY
GO


ALTER TABLE Tick SWITCH PARTITION 60 TO Tick_ArchiveDKKJPY
GO

truncate table Tick_ArchiveDKKJPY
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_DKKJPY_File01, 10)
GO

DROP TABLE Tick_ArchiveDKKJPY
GO

--------------------------------- END OF  DKKJPY   --------------------------------------------------------


---------------------------------   DKKNOK   -------------------------------------------------------------

print 'Starting purge on DKKNOK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveDKKNOK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_DKKNOK
GO


CREATE CLUSTERED INDEX [IX_PairTime_DKKNOK] ON [dbo].[MarketData_ArchiveDKKNOK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_DKKNOK
GO


ALTER TABLE MarketData SWITCH PARTITION 61 TO MarketData_ArchiveDKKNOK
GO

truncate table MarketData_ArchiveDKKNOK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_DKKNOK_File01, 10)
GO

DROP TABLE MarketData_ArchiveDKKNOK
GO

-------

print 'Starting purge on DKKNOK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveDKKNOK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_DKKNOK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveDKKNOK] ON [dbo].[Tick_ArchiveDKKNOK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_DKKNOK
GO


ALTER TABLE Tick SWITCH PARTITION 61 TO Tick_ArchiveDKKNOK
GO

truncate table Tick_ArchiveDKKNOK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_DKKNOK_File01, 10)
GO

DROP TABLE Tick_ArchiveDKKNOK
GO

--------------------------------- END OF  DKKNOK   --------------------------------------------------------


---------------------------------   DKKSEK   -------------------------------------------------------------

print 'Starting purge on DKKSEK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveDKKSEK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_DKKSEK
GO


CREATE CLUSTERED INDEX [IX_PairTime_DKKSEK] ON [dbo].[MarketData_ArchiveDKKSEK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_DKKSEK
GO


ALTER TABLE MarketData SWITCH PARTITION 62 TO MarketData_ArchiveDKKSEK
GO

truncate table MarketData_ArchiveDKKSEK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_DKKSEK_File01, 10)
GO

DROP TABLE MarketData_ArchiveDKKSEK
GO

-------

print 'Starting purge on DKKSEK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveDKKSEK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_DKKSEK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveDKKSEK] ON [dbo].[Tick_ArchiveDKKSEK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_DKKSEK
GO


ALTER TABLE Tick SWITCH PARTITION 62 TO Tick_ArchiveDKKSEK
GO

truncate table Tick_ArchiveDKKSEK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_DKKSEK_File01, 10)
GO

DROP TABLE Tick_ArchiveDKKSEK
GO

--------------------------------- END OF  DKKSEK   --------------------------------------------------------


---------------------------------   EURDKK   -------------------------------------------------------------

print 'Starting purge on EURDKK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveEURDKK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_EURDKK
GO


CREATE CLUSTERED INDEX [IX_PairTime_EURDKK] ON [dbo].[MarketData_ArchiveEURDKK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_EURDKK
GO


ALTER TABLE MarketData SWITCH PARTITION 37 TO MarketData_ArchiveEURDKK
GO

truncate table MarketData_ArchiveEURDKK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_EURDKK_File01, 10)
GO

DROP TABLE MarketData_ArchiveEURDKK
GO

-------

print 'Starting purge on EURDKK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveEURDKK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_EURDKK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveEURDKK] ON [dbo].[Tick_ArchiveEURDKK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_EURDKK
GO


ALTER TABLE Tick SWITCH PARTITION 37 TO Tick_ArchiveEURDKK
GO

truncate table Tick_ArchiveEURDKK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_EURDKK_File01, 10)
GO

DROP TABLE Tick_ArchiveEURDKK
GO

--------------------------------- END OF  EURDKK   --------------------------------------------------------


---------------------------------   EURHKD   -------------------------------------------------------------

print 'Starting purge on EURHKD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveEURHKD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_EURHKD
GO


CREATE CLUSTERED INDEX [IX_PairTime_EURHKD] ON [dbo].[MarketData_ArchiveEURHKD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_EURHKD
GO


ALTER TABLE MarketData SWITCH PARTITION 78 TO MarketData_ArchiveEURHKD
GO

truncate table MarketData_ArchiveEURHKD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_EURHKD_File01, 10)
GO

DROP TABLE MarketData_ArchiveEURHKD
GO

-------

print 'Starting purge on EURHKD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveEURHKD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_EURHKD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveEURHKD] ON [dbo].[Tick_ArchiveEURHKD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_EURHKD
GO


ALTER TABLE Tick SWITCH PARTITION 78 TO Tick_ArchiveEURHKD
GO

truncate table Tick_ArchiveEURHKD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_EURHKD_File01, 10)
GO

DROP TABLE Tick_ArchiveEURHKD
GO

--------------------------------- END OF  EURHKD   --------------------------------------------------------


---------------------------------   EURSGD   -------------------------------------------------------------

print 'Starting purge on EURSGD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveEURSGD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_EURSGD
GO


CREATE CLUSTERED INDEX [IX_PairTime_EURSGD] ON [dbo].[MarketData_ArchiveEURSGD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_EURSGD
GO


ALTER TABLE MarketData SWITCH PARTITION 86 TO MarketData_ArchiveEURSGD
GO

truncate table MarketData_ArchiveEURSGD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_EURSGD_File01, 10)
GO

DROP TABLE MarketData_ArchiveEURSGD
GO

-------

print 'Starting purge on EURSGD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveEURSGD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_EURSGD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveEURSGD] ON [dbo].[Tick_ArchiveEURSGD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_EURSGD
GO


ALTER TABLE Tick SWITCH PARTITION 86 TO Tick_ArchiveEURSGD
GO

truncate table Tick_ArchiveEURSGD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_EURSGD_File01, 10)
GO

DROP TABLE Tick_ArchiveEURSGD
GO

--------------------------------- END OF  EURSGD   --------------------------------------------------------


---------------------------------   GBPCZK   -------------------------------------------------------------

print 'Starting purge on GBPCZK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveGBPCZK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_GBPCZK
GO


CREATE CLUSTERED INDEX [IX_PairTime_GBPCZK] ON [dbo].[MarketData_ArchiveGBPCZK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_GBPCZK
GO


ALTER TABLE MarketData SWITCH PARTITION 80 TO MarketData_ArchiveGBPCZK
GO

truncate table MarketData_ArchiveGBPCZK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_GBPCZK_File01, 10)
GO

DROP TABLE MarketData_ArchiveGBPCZK
GO

-------

print 'Starting purge on GBPCZK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveGBPCZK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_GBPCZK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveGBPCZK] ON [dbo].[Tick_ArchiveGBPCZK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_GBPCZK
GO


ALTER TABLE Tick SWITCH PARTITION 80 TO Tick_ArchiveGBPCZK
GO

truncate table Tick_ArchiveGBPCZK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_GBPCZK_File01, 10)
GO

DROP TABLE Tick_ArchiveGBPCZK
GO

--------------------------------- END OF  GBPCZK   --------------------------------------------------------


---------------------------------   GBPDKK   -------------------------------------------------------------

print 'Starting purge on GBPDKK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveGBPDKK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_GBPDKK
GO


CREATE CLUSTERED INDEX [IX_PairTime_GBPDKK] ON [dbo].[MarketData_ArchiveGBPDKK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_GBPDKK
GO


ALTER TABLE MarketData SWITCH PARTITION 63 TO MarketData_ArchiveGBPDKK
GO

truncate table MarketData_ArchiveGBPDKK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_GBPDKK_File01, 10)
GO

DROP TABLE MarketData_ArchiveGBPDKK
GO

-------

print 'Starting purge on GBPDKK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveGBPDKK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_GBPDKK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveGBPDKK] ON [dbo].[Tick_ArchiveGBPDKK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_GBPDKK
GO


ALTER TABLE Tick SWITCH PARTITION 63 TO Tick_ArchiveGBPDKK
GO

truncate table Tick_ArchiveGBPDKK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_GBPDKK_File01, 10)
GO

DROP TABLE Tick_ArchiveGBPDKK
GO

--------------------------------- END OF  GBPDKK   --------------------------------------------------------


---------------------------------   GBPHKD   -------------------------------------------------------------

print 'Starting purge on GBPHKD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveGBPHKD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_GBPHKD
GO


CREATE CLUSTERED INDEX [IX_PairTime_GBPHKD] ON [dbo].[MarketData_ArchiveGBPHKD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_GBPHKD
GO


ALTER TABLE MarketData SWITCH PARTITION 87 TO MarketData_ArchiveGBPHKD
GO

truncate table MarketData_ArchiveGBPHKD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_GBPHKD_File01, 10)
GO

DROP TABLE MarketData_ArchiveGBPHKD
GO

-------

print 'Starting purge on GBPHKD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveGBPHKD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_GBPHKD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveGBPHKD] ON [dbo].[Tick_ArchiveGBPHKD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_GBPHKD
GO


ALTER TABLE Tick SWITCH PARTITION 87 TO Tick_ArchiveGBPHKD
GO

truncate table Tick_ArchiveGBPHKD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_GBPHKD_File01, 10)
GO

DROP TABLE Tick_ArchiveGBPHKD
GO

--------------------------------- END OF  GBPHKD   --------------------------------------------------------


---------------------------------   GBPHUF   -------------------------------------------------------------

print 'Starting purge on GBPHUF (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveGBPHUF](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_GBPHUF
GO


CREATE CLUSTERED INDEX [IX_PairTime_GBPHUF] ON [dbo].[MarketData_ArchiveGBPHUF]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_GBPHUF
GO


ALTER TABLE MarketData SWITCH PARTITION 81 TO MarketData_ArchiveGBPHUF
GO

truncate table MarketData_ArchiveGBPHUF
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_GBPHUF_File01, 10)
GO

DROP TABLE MarketData_ArchiveGBPHUF
GO

-------

print 'Starting purge on GBPHUF (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveGBPHUF](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_GBPHUF
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveGBPHUF] ON [dbo].[Tick_ArchiveGBPHUF]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_GBPHUF
GO


ALTER TABLE Tick SWITCH PARTITION 81 TO Tick_ArchiveGBPHUF
GO

truncate table Tick_ArchiveGBPHUF
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_GBPHUF_File01, 10)
GO

DROP TABLE Tick_ArchiveGBPHUF
GO

--------------------------------- END OF  GBPHUF   --------------------------------------------------------


---------------------------------   GBPMXN   -------------------------------------------------------------

print 'Starting purge on GBPMXN (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveGBPMXN](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_GBPMXN
GO


CREATE CLUSTERED INDEX [IX_PairTime_GBPMXN] ON [dbo].[MarketData_ArchiveGBPMXN]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_GBPMXN
GO


ALTER TABLE MarketData SWITCH PARTITION 88 TO MarketData_ArchiveGBPMXN
GO

truncate table MarketData_ArchiveGBPMXN
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_GBPMXN_File01, 10)
GO

DROP TABLE MarketData_ArchiveGBPMXN
GO

-------

print 'Starting purge on GBPMXN (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveGBPMXN](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_GBPMXN
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveGBPMXN] ON [dbo].[Tick_ArchiveGBPMXN]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_GBPMXN
GO


ALTER TABLE Tick SWITCH PARTITION 88 TO Tick_ArchiveGBPMXN
GO

truncate table Tick_ArchiveGBPMXN
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_GBPMXN_File01, 10)
GO

DROP TABLE Tick_ArchiveGBPMXN
GO

--------------------------------- END OF  GBPMXN   --------------------------------------------------------


---------------------------------   GBPPLN   -------------------------------------------------------------

print 'Starting purge on GBPPLN (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveGBPPLN](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_GBPPLN
GO


CREATE CLUSTERED INDEX [IX_PairTime_GBPPLN] ON [dbo].[MarketData_ArchiveGBPPLN]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_GBPPLN
GO


ALTER TABLE MarketData SWITCH PARTITION 83 TO MarketData_ArchiveGBPPLN
GO

truncate table MarketData_ArchiveGBPPLN
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_GBPPLN_File01, 10)
GO

DROP TABLE MarketData_ArchiveGBPPLN
GO

-------

print 'Starting purge on GBPPLN (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveGBPPLN](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_GBPPLN
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveGBPPLN] ON [dbo].[Tick_ArchiveGBPPLN]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_GBPPLN
GO


ALTER TABLE Tick SWITCH PARTITION 83 TO Tick_ArchiveGBPPLN
GO

truncate table Tick_ArchiveGBPPLN
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_GBPPLN_File01, 10)
GO

DROP TABLE Tick_ArchiveGBPPLN
GO

--------------------------------- END OF  GBPPLN   --------------------------------------------------------


---------------------------------   GBPSGD   -------------------------------------------------------------

print 'Starting purge on GBPSGD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveGBPSGD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_GBPSGD
GO


CREATE CLUSTERED INDEX [IX_PairTime_GBPSGD] ON [dbo].[MarketData_ArchiveGBPSGD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_GBPSGD
GO


ALTER TABLE MarketData SWITCH PARTITION 89 TO MarketData_ArchiveGBPSGD
GO

truncate table MarketData_ArchiveGBPSGD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_GBPSGD_File01, 10)
GO

DROP TABLE MarketData_ArchiveGBPSGD
GO

-------

print 'Starting purge on GBPSGD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveGBPSGD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_GBPSGD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveGBPSGD] ON [dbo].[Tick_ArchiveGBPSGD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_GBPSGD
GO


ALTER TABLE Tick SWITCH PARTITION 89 TO Tick_ArchiveGBPSGD
GO

truncate table Tick_ArchiveGBPSGD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_GBPSGD_File01, 10)
GO

DROP TABLE Tick_ArchiveGBPSGD
GO

--------------------------------- END OF  GBPSGD   --------------------------------------------------------


---------------------------------   GBPTRY   -------------------------------------------------------------

print 'Starting purge on GBPTRY (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveGBPTRY](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_GBPTRY
GO


CREATE CLUSTERED INDEX [IX_PairTime_GBPTRY] ON [dbo].[MarketData_ArchiveGBPTRY]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_GBPTRY
GO


ALTER TABLE MarketData SWITCH PARTITION 90 TO MarketData_ArchiveGBPTRY
GO

truncate table MarketData_ArchiveGBPTRY
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_GBPTRY_File01, 10)
GO

DROP TABLE MarketData_ArchiveGBPTRY
GO

-------

print 'Starting purge on GBPTRY (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveGBPTRY](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_GBPTRY
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveGBPTRY] ON [dbo].[Tick_ArchiveGBPTRY]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_GBPTRY
GO


ALTER TABLE Tick SWITCH PARTITION 90 TO Tick_ArchiveGBPTRY
GO

truncate table Tick_ArchiveGBPTRY
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_GBPTRY_File01, 10)
GO

DROP TABLE Tick_ArchiveGBPTRY
GO

--------------------------------- END OF  GBPTRY   --------------------------------------------------------


---------------------------------   GBPZAR   -------------------------------------------------------------

print 'Starting purge on GBPZAR (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveGBPZAR](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_GBPZAR
GO


CREATE CLUSTERED INDEX [IX_PairTime_GBPZAR] ON [dbo].[MarketData_ArchiveGBPZAR]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_GBPZAR
GO


ALTER TABLE MarketData SWITCH PARTITION 91 TO MarketData_ArchiveGBPZAR
GO

truncate table MarketData_ArchiveGBPZAR
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_GBPZAR_File01, 10)
GO

DROP TABLE MarketData_ArchiveGBPZAR
GO

-------

print 'Starting purge on GBPZAR (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveGBPZAR](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_GBPZAR
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveGBPZAR] ON [dbo].[Tick_ArchiveGBPZAR]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_GBPZAR
GO


ALTER TABLE Tick SWITCH PARTITION 91 TO Tick_ArchiveGBPZAR
GO

truncate table Tick_ArchiveGBPZAR
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_GBPZAR_File01, 10)
GO

DROP TABLE Tick_ArchiveGBPZAR
GO

--------------------------------- END OF  GBPZAR   --------------------------------------------------------


---------------------------------   HKDJPY   -------------------------------------------------------------

print 'Starting purge on HKDJPY (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveHKDJPY](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_HKDJPY
GO


CREATE CLUSTERED INDEX [IX_PairTime_HKDJPY] ON [dbo].[MarketData_ArchiveHKDJPY]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_HKDJPY
GO


ALTER TABLE MarketData SWITCH PARTITION 66 TO MarketData_ArchiveHKDJPY
GO

truncate table MarketData_ArchiveHKDJPY
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_HKDJPY_File01, 10)
GO

DROP TABLE MarketData_ArchiveHKDJPY
GO

-------

print 'Starting purge on HKDJPY (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveHKDJPY](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_HKDJPY
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveHKDJPY] ON [dbo].[Tick_ArchiveHKDJPY]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_HKDJPY
GO


ALTER TABLE Tick SWITCH PARTITION 66 TO Tick_ArchiveHKDJPY
GO

truncate table Tick_ArchiveHKDJPY
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_HKDJPY_File01, 10)
GO

DROP TABLE Tick_ArchiveHKDJPY
GO

--------------------------------- END OF  HKDJPY   --------------------------------------------------------


---------------------------------   MXNJPY   -------------------------------------------------------------

print 'Starting purge on MXNJPY (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveMXNJPY](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_MXNJPY
GO


CREATE CLUSTERED INDEX [IX_PairTime_MXNJPY] ON [dbo].[MarketData_ArchiveMXNJPY]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_MXNJPY
GO


ALTER TABLE MarketData SWITCH PARTITION 67 TO MarketData_ArchiveMXNJPY
GO

truncate table MarketData_ArchiveMXNJPY
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_MXNJPY_File01, 10)
GO

DROP TABLE MarketData_ArchiveMXNJPY
GO

-------

print 'Starting purge on MXNJPY (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveMXNJPY](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_MXNJPY
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveMXNJPY] ON [dbo].[Tick_ArchiveMXNJPY]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_MXNJPY
GO


ALTER TABLE Tick SWITCH PARTITION 67 TO Tick_ArchiveMXNJPY
GO

truncate table Tick_ArchiveMXNJPY
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_MXNJPY_File01, 10)
GO

DROP TABLE Tick_ArchiveMXNJPY
GO

--------------------------------- END OF  MXNJPY   --------------------------------------------------------


---------------------------------   NOKHKD   -------------------------------------------------------------

print 'Starting purge on NOKHKD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveNOKHKD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_NOKHKD
GO


CREATE CLUSTERED INDEX [IX_PairTime_NOKHKD] ON [dbo].[MarketData_ArchiveNOKHKD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_NOKHKD
GO


ALTER TABLE MarketData SWITCH PARTITION 113 TO MarketData_ArchiveNOKHKD
GO

truncate table MarketData_ArchiveNOKHKD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_NOKHKD_File01, 10)
GO

DROP TABLE MarketData_ArchiveNOKHKD
GO

-------

print 'Starting purge on NOKHKD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveNOKHKD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_NOKHKD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveNOKHKD] ON [dbo].[Tick_ArchiveNOKHKD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_NOKHKD
GO


ALTER TABLE Tick SWITCH PARTITION 113 TO Tick_ArchiveNOKHKD
GO

truncate table Tick_ArchiveNOKHKD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_NOKHKD_File01, 10)
GO

DROP TABLE Tick_ArchiveNOKHKD
GO

--------------------------------- END OF  NOKHKD   --------------------------------------------------------


---------------------------------   NOKJPY   -------------------------------------------------------------

print 'Starting purge on NOKJPY (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveNOKJPY](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_NOKJPY
GO


CREATE CLUSTERED INDEX [IX_PairTime_NOKJPY] ON [dbo].[MarketData_ArchiveNOKJPY]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_NOKJPY
GO


ALTER TABLE MarketData SWITCH PARTITION 68 TO MarketData_ArchiveNOKJPY
GO

truncate table MarketData_ArchiveNOKJPY
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_NOKJPY_File01, 10)
GO

DROP TABLE MarketData_ArchiveNOKJPY
GO

-------

print 'Starting purge on NOKJPY (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveNOKJPY](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_NOKJPY
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveNOKJPY] ON [dbo].[Tick_ArchiveNOKJPY]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_NOKJPY
GO


ALTER TABLE Tick SWITCH PARTITION 68 TO Tick_ArchiveNOKJPY
GO

truncate table Tick_ArchiveNOKJPY
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_NOKJPY_File01, 10)
GO

DROP TABLE Tick_ArchiveNOKJPY
GO

--------------------------------- END OF  NOKJPY   --------------------------------------------------------


---------------------------------   NZDCHF   -------------------------------------------------------------

print 'Starting purge on NZDCHF (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveNZDCHF](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_NZDCHF
GO


CREATE CLUSTERED INDEX [IX_PairTime_NZDCHF] ON [dbo].[MarketData_ArchiveNZDCHF]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_NZDCHF
GO


ALTER TABLE MarketData SWITCH PARTITION 41 TO MarketData_ArchiveNZDCHF
GO

truncate table MarketData_ArchiveNZDCHF
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_NZDCHF_File01, 10)
GO

DROP TABLE MarketData_ArchiveNZDCHF
GO

-------

print 'Starting purge on NZDCHF (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveNZDCHF](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_NZDCHF
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveNZDCHF] ON [dbo].[Tick_ArchiveNZDCHF]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_NZDCHF
GO


ALTER TABLE Tick SWITCH PARTITION 41 TO Tick_ArchiveNZDCHF
GO

truncate table Tick_ArchiveNZDCHF
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_NZDCHF_File01, 10)
GO

DROP TABLE Tick_ArchiveNZDCHF
GO

--------------------------------- END OF  NZDCHF   --------------------------------------------------------


---------------------------------   NZDDKK   -------------------------------------------------------------

print 'Starting purge on NZDDKK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveNZDDKK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_NZDDKK
GO


CREATE CLUSTERED INDEX [IX_PairTime_NZDDKK] ON [dbo].[MarketData_ArchiveNZDDKK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_NZDDKK
GO


ALTER TABLE MarketData SWITCH PARTITION 69 TO MarketData_ArchiveNZDDKK
GO

truncate table MarketData_ArchiveNZDDKK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_NZDDKK_File01, 10)
GO

DROP TABLE MarketData_ArchiveNZDDKK
GO

-------

print 'Starting purge on NZDDKK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveNZDDKK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_NZDDKK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveNZDDKK] ON [dbo].[Tick_ArchiveNZDDKK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_NZDDKK
GO


ALTER TABLE Tick SWITCH PARTITION 69 TO Tick_ArchiveNZDDKK
GO

truncate table Tick_ArchiveNZDDKK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_NZDDKK_File01, 10)
GO

DROP TABLE Tick_ArchiveNZDDKK
GO

--------------------------------- END OF  NZDDKK   --------------------------------------------------------


---------------------------------   NZDHKD   -------------------------------------------------------------

print 'Starting purge on NZDHKD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveNZDHKD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_NZDHKD
GO


CREATE CLUSTERED INDEX [IX_PairTime_NZDHKD] ON [dbo].[MarketData_ArchiveNZDHKD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_NZDHKD
GO


ALTER TABLE MarketData SWITCH PARTITION 114 TO MarketData_ArchiveNZDHKD
GO

truncate table MarketData_ArchiveNZDHKD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_NZDHKD_File01, 10)
GO

DROP TABLE MarketData_ArchiveNZDHKD
GO

-------

print 'Starting purge on NZDHKD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveNZDHKD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_NZDHKD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveNZDHKD] ON [dbo].[Tick_ArchiveNZDHKD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_NZDHKD
GO


ALTER TABLE Tick SWITCH PARTITION 114 TO Tick_ArchiveNZDHKD
GO

truncate table Tick_ArchiveNZDHKD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_NZDHKD_File01, 10)
GO

DROP TABLE Tick_ArchiveNZDHKD
GO

--------------------------------- END OF  NZDHKD   --------------------------------------------------------


---------------------------------   NZDNOK   -------------------------------------------------------------

print 'Starting purge on NZDNOK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveNZDNOK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_NZDNOK
GO


CREATE CLUSTERED INDEX [IX_PairTime_NZDNOK] ON [dbo].[MarketData_ArchiveNZDNOK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_NZDNOK
GO


ALTER TABLE MarketData SWITCH PARTITION 70 TO MarketData_ArchiveNZDNOK
GO

truncate table MarketData_ArchiveNZDNOK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_NZDNOK_File01, 10)
GO

DROP TABLE MarketData_ArchiveNZDNOK
GO

-------

print 'Starting purge on NZDNOK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveNZDNOK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_NZDNOK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveNZDNOK] ON [dbo].[Tick_ArchiveNZDNOK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_NZDNOK
GO


ALTER TABLE Tick SWITCH PARTITION 70 TO Tick_ArchiveNZDNOK
GO

truncate table Tick_ArchiveNZDNOK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_NZDNOK_File01, 10)
GO

DROP TABLE Tick_ArchiveNZDNOK
GO

--------------------------------- END OF  NZDNOK   --------------------------------------------------------


---------------------------------   NZDSEK   -------------------------------------------------------------

print 'Starting purge on NZDSEK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveNZDSEK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_NZDSEK
GO


CREATE CLUSTERED INDEX [IX_PairTime_NZDSEK] ON [dbo].[MarketData_ArchiveNZDSEK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_NZDSEK
GO


ALTER TABLE MarketData SWITCH PARTITION 71 TO MarketData_ArchiveNZDSEK
GO

truncate table MarketData_ArchiveNZDSEK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_NZDSEK_File01, 10)
GO

DROP TABLE MarketData_ArchiveNZDSEK
GO

-------

print 'Starting purge on NZDSEK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveNZDSEK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_NZDSEK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveNZDSEK] ON [dbo].[Tick_ArchiveNZDSEK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_NZDSEK
GO


ALTER TABLE Tick SWITCH PARTITION 71 TO Tick_ArchiveNZDSEK
GO

truncate table Tick_ArchiveNZDSEK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_NZDSEK_File01, 10)
GO

DROP TABLE Tick_ArchiveNZDSEK
GO

--------------------------------- END OF  NZDSEK   --------------------------------------------------------


---------------------------------   NZDSGD   -------------------------------------------------------------

print 'Starting purge on NZDSGD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveNZDSGD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_NZDSGD
GO


CREATE CLUSTERED INDEX [IX_PairTime_NZDSGD] ON [dbo].[MarketData_ArchiveNZDSGD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_NZDSGD
GO


ALTER TABLE MarketData SWITCH PARTITION 74 TO MarketData_ArchiveNZDSGD
GO

truncate table MarketData_ArchiveNZDSGD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_NZDSGD_File01, 10)
GO

DROP TABLE MarketData_ArchiveNZDSGD
GO

-------

print 'Starting purge on NZDSGD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveNZDSGD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_NZDSGD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveNZDSGD] ON [dbo].[Tick_ArchiveNZDSGD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_NZDSGD
GO


ALTER TABLE Tick SWITCH PARTITION 74 TO Tick_ArchiveNZDSGD
GO

truncate table Tick_ArchiveNZDSGD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_NZDSGD_File01, 10)
GO

DROP TABLE Tick_ArchiveNZDSGD
GO

--------------------------------- END OF  NZDSGD   --------------------------------------------------------


---------------------------------   SEKHKD   -------------------------------------------------------------

print 'Starting purge on SEKHKD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveSEKHKD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_SEKHKD
GO


CREATE CLUSTERED INDEX [IX_PairTime_SEKHKD] ON [dbo].[MarketData_ArchiveSEKHKD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_SEKHKD
GO


ALTER TABLE MarketData SWITCH PARTITION 119 TO MarketData_ArchiveSEKHKD
GO

truncate table MarketData_ArchiveSEKHKD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_SEKHKD_File01, 10)
GO

DROP TABLE MarketData_ArchiveSEKHKD
GO

-------

print 'Starting purge on SEKHKD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveSEKHKD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_SEKHKD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveSEKHKD] ON [dbo].[Tick_ArchiveSEKHKD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_SEKHKD
GO


ALTER TABLE Tick SWITCH PARTITION 119 TO Tick_ArchiveSEKHKD
GO

truncate table Tick_ArchiveSEKHKD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_SEKHKD_File01, 10)
GO

DROP TABLE Tick_ArchiveSEKHKD
GO

--------------------------------- END OF  SEKHKD   --------------------------------------------------------


---------------------------------   SGDDKK   -------------------------------------------------------------

print 'Starting purge on SGDDKK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveSGDDKK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_SGDDKK
GO


CREATE CLUSTERED INDEX [IX_PairTime_SGDDKK] ON [dbo].[MarketData_ArchiveSGDDKK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_SGDDKK
GO


ALTER TABLE MarketData SWITCH PARTITION 121 TO MarketData_ArchiveSGDDKK
GO

truncate table MarketData_ArchiveSGDDKK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_SGDDKK_File01, 10)
GO

DROP TABLE MarketData_ArchiveSGDDKK
GO

-------

print 'Starting purge on SGDDKK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveSGDDKK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_SGDDKK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveSGDDKK] ON [dbo].[Tick_ArchiveSGDDKK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_SGDDKK
GO


ALTER TABLE Tick SWITCH PARTITION 121 TO Tick_ArchiveSGDDKK
GO

truncate table Tick_ArchiveSGDDKK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_SGDDKK_File01, 10)
GO

DROP TABLE Tick_ArchiveSGDDKK
GO

--------------------------------- END OF  SGDDKK   --------------------------------------------------------


---------------------------------   SGDHKD   -------------------------------------------------------------

print 'Starting purge on SGDHKD (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveSGDHKD](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_SGDHKD
GO


CREATE CLUSTERED INDEX [IX_PairTime_SGDHKD] ON [dbo].[MarketData_ArchiveSGDHKD]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_SGDHKD
GO


ALTER TABLE MarketData SWITCH PARTITION 122 TO MarketData_ArchiveSGDHKD
GO

truncate table MarketData_ArchiveSGDHKD
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_SGDHKD_File01, 10)
GO

DROP TABLE MarketData_ArchiveSGDHKD
GO

-------

print 'Starting purge on SGDHKD (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveSGDHKD](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_SGDHKD
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveSGDHKD] ON [dbo].[Tick_ArchiveSGDHKD]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_SGDHKD
GO


ALTER TABLE Tick SWITCH PARTITION 122 TO Tick_ArchiveSGDHKD
GO

truncate table Tick_ArchiveSGDHKD
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_SGDHKD_File01, 10)
GO

DROP TABLE Tick_ArchiveSGDHKD
GO

--------------------------------- END OF  SGDHKD   --------------------------------------------------------


---------------------------------   SGDJPY   -------------------------------------------------------------

print 'Starting purge on SGDJPY (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveSGDJPY](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_SGDJPY
GO


CREATE CLUSTERED INDEX [IX_PairTime_SGDJPY] ON [dbo].[MarketData_ArchiveSGDJPY]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_SGDJPY
GO


ALTER TABLE MarketData SWITCH PARTITION 75 TO MarketData_ArchiveSGDJPY
GO

truncate table MarketData_ArchiveSGDJPY
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_SGDJPY_File01, 10)
GO

DROP TABLE MarketData_ArchiveSGDJPY
GO

-------

print 'Starting purge on SGDJPY (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveSGDJPY](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_SGDJPY
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveSGDJPY] ON [dbo].[Tick_ArchiveSGDJPY]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_SGDJPY
GO


ALTER TABLE Tick SWITCH PARTITION 75 TO Tick_ArchiveSGDJPY
GO

truncate table Tick_ArchiveSGDJPY
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_SGDJPY_File01, 10)
GO

DROP TABLE Tick_ArchiveSGDJPY
GO

--------------------------------- END OF  SGDJPY   --------------------------------------------------------


---------------------------------   USDDKK   -------------------------------------------------------------

print 'Starting purge on USDDKK (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveUSDDKK](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_USDDKK
GO


CREATE CLUSTERED INDEX [IX_PairTime_USDDKK] ON [dbo].[MarketData_ArchiveUSDDKK]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_USDDKK
GO


ALTER TABLE MarketData SWITCH PARTITION 20 TO MarketData_ArchiveUSDDKK
GO

truncate table MarketData_ArchiveUSDDKK
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_USDDKK_File01, 10)
GO

DROP TABLE MarketData_ArchiveUSDDKK
GO

-------

print 'Starting purge on USDDKK (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveUSDDKK](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_USDDKK
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveUSDDKK] ON [dbo].[Tick_ArchiveUSDDKK]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_USDDKK
GO


ALTER TABLE Tick SWITCH PARTITION 20 TO Tick_ArchiveUSDDKK
GO

truncate table Tick_ArchiveUSDDKK
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_USDDKK_File01, 10)
GO

DROP TABLE Tick_ArchiveUSDDKK
GO

--------------------------------- END OF  USDDKK   --------------------------------------------------------


---------------------------------   ZARJPY   -------------------------------------------------------------

print 'Starting purge on ZARJPY (MD):'
print GETUTCDATE()

CREATE TABLE [dbo].[MarketData_ArchiveZARJPY](
                [RecordID] [bigint] IDENTITY(1,1) NOT NULL,
                [FXPairId] [tinyint] NOT NULL,
                [SideId] [bit] NULL,
                [Price] [decimal](18, 9) NULL,
                [Size] [decimal](18, 0) NULL,
                [CounterpartyId] [tinyint] NOT NULL,
                [TradeSideId] [bit] NULL,
                [CounterpartySentTimeUtc] [datetime2](7) NULL,
                [IntegratorReceivedTimeUtc] [datetime2](7) NOT NULL,
                [CounterpartyPriceIdentity] [nvarchar](max) NULL,
                [IntegratorPriceIdentity] [uniqueidentifier] NULL,
                [MinimumSize] [decimal](18, 0) NULL,
                [Granularity] [decimal](18, 0) NULL,
                [RecordTypeId] [tinyint] NOT NULL,
                [IsLastInContinuousData] [bit] NULL
) ON MDFilegroup_ZARJPY
GO


CREATE CLUSTERED INDEX [IX_PairTime_ZARJPY] ON [dbo].[MarketData_ArchiveZARJPY]
(
                [FXPairId] ASC,
                [IntegratorReceivedTimeUtc] ASC
)  ON MDFilegroup_ZARJPY
GO


ALTER TABLE MarketData SWITCH PARTITION 73 TO MarketData_ArchiveZARJPY
GO

truncate table MarketData_ArchiveZARJPY
GO

DBCC SHRINKFILE  (MDSplittedFilegroup_ZARJPY_File01, 10)
GO

DROP TABLE MarketData_ArchiveZARJPY
GO

-------

print 'Starting purge on ZARJPY (TickD):'
print GETUTCDATE()

CREATE TABLE [dbo].[Tick_ArchiveZARJPY](
	[TickID] [bigint] IDENTITY(1,1) NOT NULL,
	[DataProviderID] [tinyint] NOT NULL,
	[FXPairID] [tinyint] NOT NULL,
	[TickTime] [numeric](17, 0) NOT NULL,
	[Bid] [real] NOT NULL,
	[Ask] [real] NOT NULL,
	[BidSize] [int] NULL,
	[AskSize] [int] NULL,
	[UniqueTick] [bit] NULL,
	[SourceFileID] [int] NOT NULL,
	[BadTick] [bit] NULL,
	[EndOfContinuousData] [bit] NULL,
	[BidStreamProviderId] [tinyint] NULL,
	[OfferStreamProviderId] [tinyint] NULL,
	[BidTimeStamp] [numeric](17, 0) NULL,
	[OfferTimeStamp] [numeric](17, 0) NULL,
	[TradeSide] [bit] NULL
) ON TickFilegroup_ZARJPY
GO

CREATE CLUSTERED INDEX [ProviderPairTime_ArchiveZARJPY] ON [dbo].[Tick_ArchiveZARJPY]
(
	[DataProviderID] ASC,
	[FXPairID] ASC,
	[TickTime] ASC
) ON TickFilegroup_ZARJPY
GO


ALTER TABLE Tick SWITCH PARTITION 73 TO Tick_ArchiveZARJPY
GO

truncate table Tick_ArchiveZARJPY
GO

DBCC SHRINKFILE  (TickSplittedFilegroup_ZARJPY_File01, 10)
GO

DROP TABLE Tick_ArchiveZARJPY
GO

--------------------------------- END OF  ZARJPY   --------------------------------------------------------



