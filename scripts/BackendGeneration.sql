USE [master]
GO
/****** Object:  Database [Integrator]    Script Date: 17. 1. 2014 17:29:25 ******/
CREATE DATABASE [Integrator]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Integrator', FILENAME = N'S:\SQLDATA\USERDB\Integrator.mdf' , SIZE = 256917504KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB ), 
 FILEGROUP [Integrator_DailyData] 
( NAME = N'Integrator_DailyData_File01', FILENAME = N'C:\SQLDATA\USERDB\Integrator\Integrator_DailyData_File01.ndf' , SIZE = 5242880KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'Integrator_log', FILENAME = N'C:\SQLDATA\LOG\Integrator_log.ldf' , SIZE = 3164032KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Integrator] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Integrator].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Integrator] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Integrator] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Integrator] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Integrator] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Integrator] SET ARITHABORT OFF 
GO
ALTER DATABASE [Integrator] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Integrator] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Integrator] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Integrator] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Integrator] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Integrator] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Integrator] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Integrator] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Integrator] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Integrator] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Integrator] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Integrator] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Integrator] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Integrator] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Integrator] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Integrator] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Integrator] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Integrator] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Integrator] SET RECOVERY FULL 
GO
ALTER DATABASE [Integrator] SET  MULTI_USER 
GO
ALTER DATABASE [Integrator] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Integrator] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Integrator] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Integrator] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Integrator', N'ON'
GO

CREATE LOGIN [IntegratorServiceAccount] WITH PASSWORD=N'«Y {???W"i?J§	a?IôoÖr%L3?6«', DEFAULT_DATABASE=[Integrator], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO

CREATE LOGIN [IntegratorKillSwitchUser] WITH PASSWORD=N'ú,3>bç?éUömáuE?d?TrU?3vú]d&', DEFAULT_DATABASE=[Integrator], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=ON
GO


USE [Integrator]
GO
/****** Object:  User [IntegratorServiceAccount]    Script Date: 17. 1. 2014 17:29:34 ******/
CREATE USER [IntegratorServiceAccount] FOR LOGIN [IntegratorServiceAccount] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [IntegratorKillSwitchUser]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE USER [IntegratorKillSwitchUser] FOR LOGIN [IntegratorKillSwitchUser] WITH DEFAULT_SCHEMA=[dbo]
GO
GRANT CONNECT TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT CONNECT TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="BusinessLayerSettings" type="BusinessLayerSettings" nillable="true" /><xsd:complexType name="ArrayOfCounterpartyRank"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="CounterpartyRank" type="CounterpartyRank" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfIgnoredOrderCounterpartyContact"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IgnoredOrderCounterpartyContact" type="IgnoredOrderCounterpartyContact" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfString"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Counterparty" type="xsd:string" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="BusinessLayerSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="MarketableClientOrdersMatchingStrategy" type="MarketableClientOrdersMatchingStrategy" /><xsd:element name="ConsecutiveRejectionsStopStrategyBehavior" type="ConsecutiveRejectionsStopStrategySettings" maxOccurs="unbounded" /><xsd:element name="StartWithSessionsStopped" type="xsd:boolean" /><xsd:element name="AutoKillInternalOrdersOnOrdersTransmissionDisabled" type="xsd:boolean" /><xsd:element name="StartStopEmailSubject" type="xsd:string" /><xsd:element name="StartStopEmailBody" type="xsd:string" /><xsd:element name="StartStopEmailTo" type="xsd:string" /><xsd:element name="StartStopEmailCc" type="xsd:string" /><xsd:element name="IgnoredOrderCounterpartyContacts" type="ArrayOfIgnoredOrderCounterpartyContact" /><xsd:element name="IgnoredOrderEmailCc" type="xsd:string" /><xsd:element name="FatalErrorsEmailTo" type="xsd:string" /><xsd:element name="FatalErrorsEmailCc" type="xsd:string" /><xsd:element name="FatalErrorsEmailMinDelayBetweenEmails_Minutes" type="xsd:int" /><xsd:element name="PreventMatchingToSameCounterpartyAfterOrderRejectBehavior" type="PreventMatchingToSameCounterpartyAfterOrderRejectSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ConsecutiveRejectionsStopStrategySettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="StrategyEnabled" type="xsd:boolean" /><xsd:element name="NumerOfRejectionsToTriggerStrategy" type="xsd:int" /><xsd:element name="CounterpartyRankWeightMultiplier" type="xsd:decimal" /><xsd:element name="PriceAgeRankWeightMultiplier" type="xsd:decimal" /><xsd:element name="PriceBpRankWeightMultiplier" type="xsd:decimal" /><xsd:element name="DefaultCounterpartyRank" type="xsd:decimal" /><xsd:element name="CounterpartyRanks" type="ArrayOfCounterpartyRank" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="CounterpartyRank"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence /><xsd:attribute name="CounterpartyCode" type="xsd:string" /><xsd:attribute name="Rank" type="xsd:decimal" /></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="IgnoredOrderCounterpartyContact"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence /><xsd:attribute name="CounterpartyCode" type="xsd:string" /><xsd:attribute name="EmailContacts" type="xsd:string" /></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="PreventMatchingToSameCounterpartyAfterOrderRejectSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="StrategyEnabled" type="xsd:boolean" /><xsd:element name="CounterpartyCodes" type="ArrayOfString" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:simpleType name="MarketableClientOrdersMatchingStrategy"><xsd:restriction base="xsd:string"><xsd:enumeration value="ImmediateMatching" /><xsd:enumeration value="NextFreshPriceMatching" /></xsd:restriction></xsd:simpleType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.Common.dll.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.Common.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="CommonSettings" type="CommonSettings" nillable="true" /><xsd:complexType name="AutomailerSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="SmtpServer" type="xsd:string" /><xsd:element name="SmtpPort" type="xsd:int" /><xsd:element name="BypassCertificationValidation" type="xsd:boolean" /><xsd:element name="SmtpUsername" type="xsd:string" /><xsd:element name="SmtpPassword" type="xsd:string" /><xsd:element name="DirectoryForLocalEmails" type="xsd:string" /><xsd:element name="SendEmailsOnlyToLocalFolder" type="xsd:boolean" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="CommonSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Logging" type="LoggingSettings" /><xsd:element name="Automailer" type="AutomailerSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="LoggingSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="AvailableDiskSpaceThresholdToStopMDLogging_GB" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.DAL.dll.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DAL.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="DALSettings" type="DALSettings" nillable="true" /><xsd:complexType name="DALSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ConnectionString" type="xsd:string" /><xsd:element name="KillSwitchPollIntervalSeconds" type="xsd:int" /><xsd:element name="KillSwitchWatchDogIntervalSeconds" type="xsd:int" /><xsd:element name="IntegratorSwitchName" type="xsd:string" /><xsd:element name="CommandDistributorPollIntervalSeconds" type="xsd:int" /><xsd:element name="DataCollection" type="DataCollectionSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="DataCollectionSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="DataCollectionBackendConnectionString" type="xsd:string" /><xsd:element name="DataProviderId" type="xsd:int" /><xsd:element name="SourceFileId" type="xsd:int" /><xsd:element name="EnableToBDataCollection" type="xsd:boolean" /><xsd:element name="EnableMarketDataCollection" type="xsd:boolean" /><xsd:element name="MaxBcpBatchSize" type="xsd:int" /><xsd:element name="MaxInMemoryColectionSize" type="xsd:int" /><xsd:element name="MaxIntervalBetweenUploads_Seconds" type="xsd:int" /><xsd:element name="MaxUploadTimeAfterWhichSerializationStarts_Seconds" type="xsd:int" /><xsd:element name="MaxProcessingTimeAfterShutdownSignalled_Seconds" type="xsd:int" /><xsd:element name="WaitIntervalBetweenBcpRetries_Seconds" type="xsd:int" /><xsd:element name="WaitIntervalBeforeShutdownStartsSerialization_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.DALv2.dll.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DALv2.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="DALSettings" type="DALSettings" nillable="true" /><xsd:complexType name="DALSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="KillSwitchPollIntervalSeconds" type="xsd:int" /><xsd:element name="KillSwitchWatchDogIntervalSeconds" type="xsd:int" /><xsd:element name="IntegratorSwitchName" type="xsd:string" /><xsd:element name="CommandDistributorPollIntervalSeconds" type="xsd:int" /><xsd:element name="DataCollection" type="DataCollectionSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="DataCollectionSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="DataProviderId" type="xsd:int" /><xsd:element name="SourceFileId" type="xsd:int" /><xsd:element name="MaxBcpBatchSize" type="xsd:int" /><xsd:element name="MaxInMemoryColectionSize" type="xsd:int" /><xsd:element name="MaxIntervalBetweenUploads_Seconds" type="xsd:int" /><xsd:element name="MaxUploadTimeAfterWhichSerializationStarts_Seconds" type="xsd:int" /><xsd:element name="MaxProcessingTimeAfterShutdownSignalled_Seconds" type="xsd:int" /><xsd:element name="WaitIntervalBetweenBcpRetries_Seconds" type="xsd:int" /><xsd:element name="WaitIntervalBeforeShutdownStartsSerialization_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.DALv2_DataCollectionConnection.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DALv2_DataCollectionConnection.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="DALSettings_DataCollectionConnection" type="DALSettings_DataCollectionConnection" nillable="true" /><xsd:complexType name="DALSettings_DataCollectionConnection"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ConnectionString" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.DALv2_DataCollectionEnabling.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DALv2_DataCollectionEnabling.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="DALSettings_DataCollectionEnabling" type="DALSettings_DataCollectionEnabling" nillable="true" /><xsd:complexType name="DALSettings_DataCollectionEnabling"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="EnableToBDataCollection" type="xsd:boolean" /><xsd:element name="EnableMarketDataCollection" type="xsd:boolean" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.DALv2_IntegratorConnection.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DALv2_IntegratorConnection.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="DALSettings_IntegratorConnection" type="DALSettings_IntegratorConnection" nillable="true" /><xsd:complexType name="DALSettings_IntegratorConnection"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ConnectionString" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="DiagnosticsDashboardSettings" type="DiagnosticsDashboardSettings" nillable="true" /><xsd:complexType name="ArrayOfString"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Symbol" type="xsd:string" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfString1"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Counterparty" type="xsd:string" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="DiagnosticsDashboardSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="RefreshTimeout_Miliseconds" type="xsd:int" /><xsd:element name="FirstSpreadAgeLimit_Seconds" type="xsd:int" /><xsd:element name="SecondSpreadAgeLimit_Seconds" type="xsd:int" /><xsd:element name="ThirdSpreadAgeLimit_Seconds" type="xsd:int" /><xsd:element name="CutOffSpreadAgeLimit_Hours" type="xsd:int" /><xsd:element name="EnabledSymbols" type="ArrayOfString" /><xsd:element name="EnabledCounterparties" type="ArrayOfString1" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.IntegratorClientHost.exe.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.IntegratorClientHost.exe.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="IntegratorClientHostSettings" type="IntegratorClientHostSettings" nillable="true" /><xsd:complexType name="ArrayOfIntegratorClientConfiguration"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IntegratorClientConfiguration" type="IntegratorClientConfiguration" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="IntegratorClientConfiguration"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ConfigurationIdentity" type="xsd:string" /><xsd:element name="ClientInterfaceType" type="ClientInterfaceType" /><xsd:element name="ClientFriendlyName" type="xsd:string" minOccurs="0" /><xsd:element name="ClientTypeName" type="xsd:string" /><xsd:element name="AssemblyNameWithoutExtension" type="xsd:string" minOccurs="0" /><xsd:element name="AssemblyFullPath" type="xsd:string" minOccurs="0" /><xsd:element name="DefaultConfigurationParameter" type="xsd:string" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="IntegratorClientHostSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="DefaultIntegratorClientCofigurationIdentity" type="xsd:string" minOccurs="0" /><xsd:element name="IntegratorClientConfigurations" type="ArrayOfIntegratorClientConfiguration" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:simpleType name="ClientInterfaceType"><xsd:restriction base="xsd:string"><xsd:enumeration value="Console" /><xsd:enumeration value="Form" /></xsd:restriction></xsd:simpleType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.IntegratorWatchDogSvc.exe.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.IntegratorWatchDogSvc.exe.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="WatchdogSettings" type="WatchdogSettings" nillable="true" /><xsd:complexType name="ArrayOfIntegratorServiceDiagnosticsEndpoint"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IntegratorServiceDiagnosticsEndpoint" type="IntegratorServiceDiagnosticsEndpoint" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="IntegratorServiceDiagnosticsEndpoint"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Endpoint" type="xsd:string" /><xsd:element name="Port" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="WatchdogSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IntegratorServiceDiagnosticsEndpoints" type="ArrayOfIntegratorServiceDiagnosticsEndpoint" /><xsd:element name="IntegratorNotRunnigSendFirstEmailAfter_Minutes" type="xsd:int" /><xsd:element name="IntegratorNotRunnigResendEmailAfter_Hours" type="xsd:int" /><xsd:element name="EmailRecipients" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.MessageBus.dll.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBus.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="MessageBusSettings" type="MessageBusSettings" nillable="true" /><xsd:complexType name="MessageBusSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IntegratorServiceHost" type="xsd:string" /><xsd:element name="IntegratorServicePort" type="xsd:int" /><xsd:element name="IntegratorServicePipeName" type="xsd:string" /><xsd:element name="MaximumAllowedDisconnectedInterval_Seconds" type="xsd:int" /><xsd:element name="DisconnectedSessionReconnectInterval_Seconds" type="xsd:int" /><xsd:element name="SessionHealthCheckInterval_Seconds" type="xsd:int" /><xsd:element name="PriceStreamTransferMode" type="PriceStreamTransferMode" /><xsd:element name="PerSymbolPriceThrottlingInterval_Milliseconds" type="xsd:int" minOccurs="0" /><xsd:element name="ForceConnectRemotingClientOnContractVersionMismatch" type="xsd:boolean" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:simpleType name="PriceStreamTransferMode"><xsd:restriction base="xsd:string"><xsd:enumeration value="Reliable" /><xsd:enumeration value="FastAndUnreliable" /></xsd:restriction></xsd:simpleType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="MessageBusSettings_ClientConnectionInfo" type="MessageBusSettings_ClientConnectionInfo" nillable="true" /><xsd:complexType name="MessageBusSettings_ClientConnectionInfo"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IntegratorServicePipeName" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.MessageBusSettings_ConnectionKeeping.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_ConnectionKeeping.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="MessageBusSettings_ConnectionKeeping" type="MessageBusSettings_ConnectionKeeping" nillable="true" /><xsd:complexType name="MessageBusSettings_ConnectionKeeping"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="MaximumAllowedDisconnectedInterval_Seconds" type="xsd:int" /><xsd:element name="DisconnectedSessionReconnectInterval_Seconds" type="xsd:int" /><xsd:element name="SessionHealthCheckInterval_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="MessageBusSettings_ContractMismatchHandling" type="MessageBusSettings_ContractMismatchHandling" nillable="true" /><xsd:complexType name="MessageBusSettings_ContractMismatchHandling"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ForceConnectRemotingClientOnContractVersionMismatch" type="xsd:boolean" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="MessageBusSettings_IntegratorEndpointConnectionInfo" type="MessageBusSettings_IntegratorEndpointConnectionInfo" nillable="true" /><xsd:complexType name="MessageBusSettings_IntegratorEndpointConnectionInfo"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="IntegratorServiceHost" type="xsd:string" /><xsd:element name="IntegratorServicePort" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="MessageBusSettings_PriceTransferBehavior" type="MessageBusSettings_PriceTransferBehavior" nillable="true" /><xsd:complexType name="MessageBusSettings_PriceTransferBehavior"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="PriceStreamTransferMode" type="PriceStreamTransferMode" /><xsd:element name="PerSymbolPriceThrottlingInterval_Milliseconds" type="xsd:int" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:simpleType name="PriceStreamTransferMode"><xsd:restriction base="xsd:string"><xsd:enumeration value="Reliable" /><xsd:enumeration value="FastAndUnreliable" /></xsd:restriction></xsd:simpleType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.QuickItchN.dll.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.QuickItchN.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="QuickItchNSettings" type="QuickItchNSettings" nillable="true" /><xsd:complexType name="QuickItchNSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="HtaSessionSettings" type="SessionSettings" /><xsd:element name="HtfSessionSettings" type="SessionSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="SessionSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Host" type="xsd:string" /><xsd:element name="Port" type="xsd:int" /><xsd:element name="Username" type="xsd:string" /><xsd:element name="Password" type="xsd:string" /><xsd:element name="HeartBeatInterval_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.RiskManagement.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="RiskManagementSettings" type="RiskManagementSettings" nillable="true" /><xsd:complexType name="ArrayOfSymbol"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Symbol" type="Symbol" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ExternalOrderMaximumAllowedSizeSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="RiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumAllowedBaseAbsSizeInUsd" type="xsd:decimal" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ExternalOrdersRejectionRateSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="RiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumAllowedInstancesPerTimeInterval" type="xsd:int" /><xsd:element name="TimeIntervalToCheck_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ExternalOrdersSubmissionRateSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="RiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumAllowedInstancesPerTimeInterval" type="xsd:int" /><xsd:element name="TimeIntervalToCheck_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="PriceAndPositionSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ExternalNOPRiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumExternalNOPInUSD" type="xsd:decimal" /><xsd:element name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xsd:int" /><xsd:element name="PriceDeviationCheckAllowed" type="xsd:boolean" /><xsd:element name="OnlinePriceDeviationCheckMaxPriceAge_Minutes" type="xsd:int" /><xsd:element name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xsd:boolean" /><xsd:element name="Symbols" type="ArrayOfSymbol" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="RiskManagementSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ConfigLastUpdated" type="xsd:string" nillable="true" /><xsd:element name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" /><xsd:element name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" /><xsd:element name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" /><xsd:element name="PriceAndPosition" type="PriceAndPositionSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="Symbol"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence /><xsd:attribute name="ShortName" type="xsd:string" /><xsd:attribute name="NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xsd:decimal" /></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="SessionsSettings" type="SessionsSettings" nillable="true" /><xsd:complexType name="ArrayOfCounterpartSetting"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="CounterpartSetting" type="CounterpartSetting" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ArrayOfSymbolSetting"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="SymbolSetting" type="SymbolSetting" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="CounterpartSetting"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ShortName" type="xsd:string" /><xsd:element name="SettingAction" type="SettingAction" /><xsd:element name="Symbols" type="ArrayOfSymbolSetting" minOccurs="0" /><xsd:element name="SenderSubId" type="xsd:string" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="CounterpartsSubscription"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Counterparts" type="ArrayOfCounterpartSetting" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_BNPSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Password" type="xsd:string" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_CRSSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ClientId" type="xsd:string" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_CTISettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Password" type="xsd:string" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_CZBSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_HOTSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="SenderSubId" type="xsd:string" /><xsd:element name="Username" type="xsd:string" /><xsd:element name="Password" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_JPMSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Password" type="xsd:string" /><xsd:element name="PasswordLength" type="xsd:int" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_MGSSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="OnBehalfOfCompId" type="xsd:string" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_NOMSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="OnBehalfOfCompID" type="xsd:string" /><xsd:element name="SenderSubId" type="xsd:string" /><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_RBSSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Account" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_SOCSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="OnBehalfOfCompID" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="FIXChannel_UBSSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Username" type="xsd:string" /><xsd:element name="Password" type="xsd:string" /><xsd:element name="PartyId" type="xsd:string" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="GlobalSubscription"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Symbols" type="ArrayOfSymbolSetting" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="MarketDataSessionSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="MaxAutoResubscribeCount" type="xsd:int" /><xsd:element name="RetryIntervalsSequence_Seconds" type="xsd:int" maxOccurs="unbounded" /><xsd:element name="UnconfirmedSubscriptionTimeout_Seconds" type="xsd:int" /><xsd:element name="PutErrorsInLessImportantLog" type="xsd:boolean" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="OrderFlowSessionSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="UnconfirmedOrderTimeout_Seconds" type="xsd:int" /><xsd:element name="PutErrorsInLessImportantLog" type="xsd:boolean" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="SessionsSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="FIXChannel_CRS" type="FIXChannel_CRSSettings" /><xsd:element name="FIXChannel_UBS" type="FIXChannel_UBSSettings" /><xsd:element name="FIXChannel_CTI" type="FIXChannel_CTISettings" /><xsd:element name="FIXChannel_MGS" type="FIXChannel_MGSSettings" /><xsd:element name="FIXChannel_RBS" type="FIXChannel_RBSSettings" /><xsd:element name="FIXChannel_JPM" type="FIXChannel_JPMSettings" /><xsd:element name="FIXChannel_BNP" type="FIXChannel_BNPSettings" /><xsd:element name="FIXChannel_CZB" type="FIXChannel_CZBSettings" /><xsd:element name="FIXChannel_SOC" type="FIXChannel_SOCSettings" /><xsd:element name="FIXChannel_NOM" type="FIXChannel_NOMSettings" /><xsd:element name="FIXChannel_HTA" type="FIXChannel_HOTSettings" /><xsd:element name="FIXChannel_HTF" type="FIXChannel_HOTSettings" /><xsd:element name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" /><xsd:element name="MarketDataSession" type="MarketDataSessionSettings" /><xsd:element name="OrderFlowSession" type="OrderFlowSessionSettings" /><xsd:element name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="SubscriptionSettingsBag"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ResubscribeOnReconnect" type="xsd:boolean" /><xsd:element name="GlobalSubscriptionSettings" type="GlobalSubscription" minOccurs="0" /><xsd:element name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="SymbolSetting"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ShortName" type="xsd:string" /><xsd:element name="Size" type="xsd:decimal" minOccurs="0" /><xsd:element name="SettingAction" type="SettingAction" /><xsd:element name="SenderSubId" type="xsd:string" minOccurs="0" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="UnexpectedMessagesTreatingSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="EnableThreasholdingOfUnexpectedMessages" type="xsd:boolean" /><xsd:element name="MaximumAllowedInstancesPerTimeInterval" type="xsd:int" /><xsd:element name="TimeIntervalToCheck_Minutes" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:simpleType name="SettingAction"><xsd:restriction base="xsd:string"><xsd:enumeration value="Add" /><xsd:enumeration value="Remove" /></xsd:restriction></xsd:simpleType></xsd:schema>'
GO
/****** Object:  StoredProcedure [dbo].[CreateDailyDealsView_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CreateDailyDealsView_SP] 
AS
BEGIN

	DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)
	DECLARE @YesterdayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), DATEADD(DAY, -1, GETUTCDATE()), 101)

	DECLARE @Query NVARCHAR(MAX) = N'
	CREATE VIEW [DealExternalExecutedDailyView_' + @TodayLiteral + ']
	WITH SCHEMABINDING 
	AS 
	SELECT 
		deal.[IntegratorReceivedExecutionReportUtc]
		,deal.[CounterpartyId]
		,deal.[SymbolId]
		,deal.[IntegratorExternalOrderPrivateId]
		,deal.[AmountBasePolExecuted]
		,deal.[Price]
		,CAST(
			(
				(datepart(HOUR, ordr.QuoteReceivedToOrderSentInternalLatency)*60 + datepart(MINUTE, ordr.QuoteReceivedToOrderSentInternalLatency))*60 
				+ datepart(SECOND, ordr.QuoteReceivedToOrderSentInternalLatency)
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, ordr.QuoteReceivedToOrderSentInternalLatency) 
		   AS IntegratorLatencyNanoseconds
		,CAST(
			(
				(datepart(HOUR, deal.[OrderSentToExecutionReportReceivedLatency])*60 + datepart(MINUTE, deal.[OrderSentToExecutionReportReceivedLatency]))*60 
				+ datepart(SECOND, deal.[OrderSentToExecutionReportReceivedLatency])
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, deal.[OrderSentToExecutionReportReceivedLatency]) 
		   AS CounterpartyLatencyNanoseconds
		,deal.[AmountTermPolExecutedInUsd]
		,deal.[DatePartIntegratorReceivedExecutionReportUtc]
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN [dbo].[OrderExternal] ordr ON deal.[IntegratorExternalOrderPrivateId] = ordr.[IntegratorExternalOrderPrivateId]
	WHERE
		DatePartIntegratorReceivedExecutionReportUtc = CONVERT(DATE,''' + @TodayLiteral + ''', 101)'
		
		
	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE UNIQUE CLUSTERED INDEX [IX_ReceivedUtc_OrderId] ON [dbo].[DealExternalExecutedDailyView_' + @TodayLiteral + ']
	(
		[IntegratorReceivedExecutionReportUtc] ASC,
		[IntegratorExternalOrderPrivateId] ASC
	)ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalExecutedDailyView_' + @TodayLiteral + ']
	(
		[DatePartIntegratorReceivedExecutionReportUtc] ASC,
		[CounterpartyId] ASC,
		[SymbolId] ASC
	)
	INCLUDE 
	(
		[AmountBasePolExecuted],
		[Price],
		[AmountTermPolExecutedInUsd],
		[IntegratorLatencyNanoseconds],
		[CounterpartyLatencyNanoseconds])
	ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	IF EXISTS(SELECT * FROM  sys.synonyms s WHERE s.name = 'DealExternalExecuted_DailyView')
	BEGIN
		DROP SYNONYM DealExternalExecuted_DailyView
	END

	SET @Query = N'
	CREATE SYNONYM DealExternalExecuted_DailyView FOR [DealExternalExecutedDailyView_' + @TodayLiteral + ']'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	IF EXISTS(select * FROM sys.views where name = ''DealExternalExecutedDailyView_' + @YesterdayLiteral + ''')
	BEGIN
		DROP VIEW [DealExternalExecutedDailyView_' + @YesterdayLiteral + ']
	END
	'

	EXECUTE sp_executesql @Query

END

GO
ALTER AUTHORIZATION ON [dbo].[CreateDailyDealsView_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[CreateDailyOrdersView_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateDailyOrdersView_SP] 
AS
BEGIN

	DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)
	DECLARE @YesterdayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), DATEADD(DAY, -1, GETUTCDATE()), 101)

	DECLARE @Query NVARCHAR(MAX) = N'
	CREATE VIEW [OrderExternalDailyView_' + @TodayLiteral + ']
	WITH SCHEMABINDING 
	AS 
	SELECT 
		   [IntegratorSentExternalOrderUtc]
		  ,[DatePartIntegratorSentExternalOrderUtc]
		  ,[IntegratorExternalOrderPrivateId]
		  ,[CounterpartyId]
		  ,[QuoteReceivedToOrderSentInternalLatency]
	FROM [dbo].[OrderExternal]
	WHERE
		DatePartIntegratorSentExternalOrderUtc = CONVERT(DATE,''' + @TodayLiteral + ''', 101)'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE UNIQUE CLUSTERED INDEX [IX_OrderId] ON [dbo].[OrderExternalDailyView_' + @TodayLiteral + ']
	(
		[IntegratorExternalOrderPrivateId] ASC
	)ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

--	SET @Query = N'
--	CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[OrderExternalDailyView_' + @TodayLiteral + ']
--	(
--		[DatePartIntegratorSentExternalOrderUtc] ASC,
--		[CounterpartyId] ASC
--	)
--	INCLUDE ([QuoteReceivedToOrderSentInternalLatency]) 
--	ON [Integrator_DailyData]'


--	EXECUTE sp_executesql @Query

	IF EXISTS(SELECT * FROM  sys.synonyms s WHERE s.name = 'OrderExternal_DailyView')
	BEGIN
		DROP SYNONYM OrderExternal_DailyView
	END

	SET @Query = N'
	CREATE SYNONYM OrderExternal_DailyView FOR [OrderExternalDailyView_' + @TodayLiteral + ']'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	IF EXISTS(select * FROM sys.views where name = ''OrderExternalDailyView_' + @YesterdayLiteral + ''')
	BEGIN
		DROP VIEW [OrderExternalDailyView_' + @YesterdayLiteral + ']
	END
	'

	EXECUTE sp_executesql @Query

END

GO
ALTER AUTHORIZATION ON [dbo].[CreateDailyOrdersView_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[CreateDailyRejectionsView_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CreateDailyRejectionsView_SP] 
AS
BEGIN

	DECLARE @TodayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), GETUTCDATE(), 101)
	DECLARE @YesterdayLiteral NVARCHAR(10) = CONVERT(NVARCHAR(10), DATEADD(DAY, -1, GETUTCDATE()), 101)

	DECLARE @Query NVARCHAR(MAX) = N'
	CREATE VIEW [DealExternalRejectedDailyView_' + @TodayLiteral + ']
	WITH SCHEMABINDING 
	AS 
	SELECT 
		   reject.[IntegratorReceivedExecutionReportUtc]
		   ,reject.[IntegratorExternalOrderPrivateId]
		  ,reject.[CounterpartyId]
		  ,reject.[DatePartIntegratorReceivedExecutionReportUtc]
		  ,CAST(
			(
				(datepart(HOUR, ordr.QuoteReceivedToOrderSentInternalLatency)*60 + datepart(MINUTE, ordr.QuoteReceivedToOrderSentInternalLatency))*60 
				+ datepart(SECOND, ordr.QuoteReceivedToOrderSentInternalLatency)
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, ordr.QuoteReceivedToOrderSentInternalLatency) 
		   AS IntegratorLatencyNanoseconds
	FROM 
		[dbo].[DealExternalRejected] reject
		INNER JOIN [dbo].[OrderExternal] ordr ON reject.[IntegratorExternalOrderPrivateId] = ordr.[IntegratorExternalOrderPrivateId]
	WHERE
		DatePartIntegratorReceivedExecutionReportUtc = CONVERT(DATE,''' + @TodayLiteral + ''', 101)'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE UNIQUE CLUSTERED INDEX [IX_ReceivedUtc_OrderId] ON [dbo].[DealExternalRejectedDailyView_' + @TodayLiteral + ']
	(
		[IntegratorReceivedExecutionReportUtc] ASC,
		[IntegratorExternalOrderPrivateId] ASC
	)ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	SET @Query = N'
	CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalRejectedDailyView_' + @TodayLiteral + ']
	(
		[DatePartIntegratorReceivedExecutionReportUtc] ASC,
		[CounterpartyId] ASC
	)
	INCLUDE
	(
		[IntegratorLatencyNanoseconds]
	)
	ON [Integrator_DailyData]'


	EXECUTE sp_executesql @Query

	IF EXISTS(SELECT * FROM  sys.synonyms s WHERE s.name = 'DealExternalRejected_DailyView')
	BEGIN
		DROP SYNONYM DealExternalRejected_DailyView
	END

	SET @Query = N'
	CREATE SYNONYM DealExternalRejected_DailyView FOR [DealExternalRejectedDailyView_' + @TodayLiteral + ']'

	EXECUTE sp_executesql @Query

	SET @Query = N'
	IF EXISTS(select * FROM sys.views where name = ''DealExternalRejectedDailyView_' + @YesterdayLiteral + ''')
	BEGIN
		DROP VIEW [DealExternalRejectedDailyView_' + @YesterdayLiteral + ']
	END
	'

	EXECUTE sp_executesql @Query

END

GO
ALTER AUTHORIZATION ON [dbo].[CreateDailyRejectionsView_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[DisableExternalOrdersTransmission_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[DisableExternalOrdersTransmission_SP] 
	@SwitchName NVARCHAR(64),
	@ChangedBy NVARCHAR(256),
	@ChangedFrom NVARCHAR(256),
	@Reason NVARCHAR(MAX) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SwitchId INT 
	SELECT @SwitchId = [Id] FROM [dbo].[ExternalOrdersTransmissionSwitch] WHERE [SwitchName] = @SwitchName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Switch not found in DB: %s', @SwitchName, 16, 2) WITH SETERROR
	END

	SET @Reason = ISNULL(@Reason, 'Changed per request by ' + @ChangedBy)

	BEGIN TRANSACTION

	INSERT INTO [dbo].[ExternalOrdersTransmissionSwitchHistory]
           ([ExternalOrdersTransmissionSwitchId]
           ,[TransmissionEnabled]
           ,[Changed]
           ,[ChangedBy]
           ,[ChangedFrom]
		   ,[Reason])
     VALUES
           (@SwitchId
           ,0
           ,GETUTCDATE()
           ,@ChangedBy
		   ,@ChangedFrom
		   ,@Reason)

	UPDATE [dbo].[ExternalOrdersTransmissionSwitch]
	SET [TransmissionEnabled] = 0
	WHERE [Id] = @SwitchId

	COMMIT TRANSACTION
END

GO
ALTER AUTHORIZATION ON [dbo].[DisableExternalOrdersTransmission_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[DisableExternalOrdersTransmission_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[DisableExternalOrdersTransmission_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[EnableExternalOrdersTransmission_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EnableExternalOrdersTransmission_SP] 
	@SwitchName NVARCHAR(64),
	@ChangedBy NVARCHAR(256),
	@ChangedFrom NVARCHAR(256)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SwitchId INT 
	SELECT @SwitchId = [Id] FROM [dbo].[ExternalOrdersTransmissionSwitch] WHERE [SwitchName] = @SwitchName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Switch not found in DB: %s', @SwitchName, 16, 2) WITH SETERROR
	END

	BEGIN TRANSACTION

	INSERT INTO [dbo].[ExternalOrdersTransmissionSwitchHistory]
           ([ExternalOrdersTransmissionSwitchId]
           ,[TransmissionEnabled]
           ,[Changed]
           ,[ChangedBy]
           ,[ChangedFrom])
     VALUES
           (@SwitchId
           ,1
           ,GETUTCDATE()
           ,@ChangedBy
		   ,@ChangedFrom)

	UPDATE [dbo].[ExternalOrdersTransmissionSwitch]
	SET [TransmissionEnabled] = 1
	WHERE [Id] = @SwitchId

	COMMIT TRANSACTION
END

GO
ALTER AUTHORIZATION ON [dbo].[EnableExternalOrdersTransmission_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[EnableExternalOrdersTransmission_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GenerateUpdateSettingsScripts_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GenerateUpdateSettingsScripts_SP] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 'UPDATE [dbo].[Settings] SET [SettingsXml] = ''' + 
	CONVERT(VARCHAR(MAX), [SettingsXml]) +
	''', [LastUpdated] = GETUTCDATE() WHERE [Name] = ''' + 
	CONVERT(VARCHAR(MAX), [Name]) + ''' AND [Id] = ' + CONVERT(VARCHAR(5), [Id])
	FROM [dbo].[Settings]
END


GO
ALTER AUTHORIZATION ON [dbo].[GenerateUpdateSettingsScripts_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetCertificateFile_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCertificateFile_SP] 
	@FileName NVARCHAR(100)
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		FileContent
	FROM
		[dbo].[CertificateFile]
	WHERE
		FileName = @FileName
END

GO
ALTER AUTHORIZATION ON [dbo].[GetCertificateFile_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetCertificateFile_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetCommandItems_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[GetCommandItems_SP] (
	@MaxCount INT
	)
AS
BEGIN
	DECLARE @TempCommandIds TABLE (
		Id INT
		,CommandText NVARCHAR(MAX)
		)

    DECLARE @CurrentUTCTime DATETIME

	SET @CurrentUTCTime = (SELECT GETUTCDATE())

	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

	BEGIN TRANSACTION

	INSERT INTO @TempCommandIds (
		Id
		,CommandText
		)
	SELECT TOP (@MaxCount) 
		Id, CommandText
	FROM 
		[dbo].[Command] cmd WITH (TABLOCKX)
		INNER JOIN [dbo].[NewCommand] newcmd WITH (TABLOCKX) ON cmd.id = newcmd.CommandId

	DELETE 
		newcmd
	FROM 
		[dbo].[NewCommand] newcmd
		INNER JOIN @TempCommandIds tmpcmd ON newcmd.CommandId = tmpcmd.Id

	COMMIT TRANSACTION

	INSERT INTO CommandStatus (
		CommandId
		,Status
		,Time
		)
	SELECT
		Id
		,N'Acquired'
		,@CurrentUTCTime
	FROM @TempCommandIds

	SELECT 
		Id
		,CommandText
	FROM @TempCommandIds
END

GO
ALTER AUTHORIZATION ON [dbo].[GetCommandItems_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetCommandItems_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetCounterpartyMonitorSettingsList_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCounterpartyMonitorSettingsList_SP]
AS
BEGIN
	SELECT 
		[Id]
		,[FriendlyName]
	FROM 
		[dbo].[Settings]
	WHERE
		[Name] = 'Kreslik.Integrator.DiagnosticsDashboard.exe'
END

GO
ALTER AUTHORIZATION ON [dbo].[GetCounterpartyMonitorSettingsList_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetCounterpartyMonitorSettingsList_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetCounterpartyMonitorSettingsName_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCounterpartyMonitorSettingsName_SP] 
	@EnvironmentName NVARCHAR(100)
AS
BEGIN

	SELECT
		sett.FriendlyName
	FROM
		[dbo].[IntegratorEnvironment] envint 
		INNER JOIN [dbo].[IntegratorEnvironmentToSettingsMap] map ON envint.Id = map.IntegratorEnvironmentId
		INNER JOIN [dbo].[Settings] sett ON sett.id = map.SettingsId
	WHERE
		envint.Name = @EnvironmentName
		AND sett.Name = 'Kreslik.Integrator.DiagnosticsDashboard.exe'
END

GO
ALTER AUTHORIZATION ON [dbo].[GetCounterpartyMonitorSettingsName_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetCounterpartyMonitorSettingsName_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetDealsExecutedOnHotspot_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDealsExecutedOnHotspot_SP]
	@FxPairNoSlash char(6),
	@StartDateTime DateTime2,
	@EndDateTime DateTime2,
	@SortBySourceTimeStamps bit
AS
BEGIN
SET NOCOUNT ON; -- prevent extra result sets from interfering with SELECT statements

declare @SymbolId tinyint = (select Id from Symbol sym where sym.ShortName=@FxPairNoSlash);

IF @SortBySourceTimeStamps=0

	BEGIN
		select 

		dee.CounterpartySentExecutionReportUtc,
		dee.IntegratorReceivedExecutionReportUtc,
		dee.Price,
		dir.Name'DealDirection'

		from 

		DealExternalExecuted dee
		join DealDirection dir on dee.DealDirectionId=dir.Id

		where 

		dee.CounterpartyId in (20, 21)
		and dee.IntegratorReceivedExecutionReportUtc between @StartDateTime and @EndDateTime
		and dee.SymbolId=@SymbolId

		order by 

		dee.IntegratorReceivedExecutionReportUtc asc
	END 

	ELSE

	BEGIN
		select 

		dee.CounterpartySentExecutionReportUtc,
		dee.IntegratorReceivedExecutionReportUtc,
		dee.Price,
		dir.Name'DealDirection'

		from 

		DealExternalExecuted dee
		join DealDirection dir on dee.DealDirectionId=dir.Id

		where 

		dee.CounterpartyId in (20, 21)
		and dee.IntegratorReceivedExecutionReportUtc between @StartDateTime and @EndDateTime
		and dee.SymbolId=@SymbolId

		order by 

		dee.CounterpartySentExecutionReportUtc asc
	END 


END

GO
ALTER AUTHORIZATION ON [dbo].[GetDealsExecutedOnHotspot_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetDealsStatistics_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDealsStatistics_SP]( 
	@Day DATE
	)
AS
BEGIN

	SELECT
		SUM(ABS([AmountTermPolExecutedInUsd]))	AS TotalVolumeAbsInUsd
		,COUNT([AmountTermPolExecutedInUsd]) AS NumberOfDeals
		,CounterpartyCode AS Counterparty
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN Counterparty ctp ON deal.[CounterpartyId] = ctp.[CounterpartyId]
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		ctp.CounterpartyCode

END

GO
ALTER AUTHORIZATION ON [dbo].[GetDealsStatistics_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetDealsStatistics_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetDetailsForRiskMgmtUser_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetDetailsForRiskMgmtUser_SP] 
	@username NVARCHAR(256)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @True BIT = 1

	SELECT 
		@True as Loged, usr.IsAdmin as IsAdmin, s.Name as SiteName
	FROM 
		[dbo].[KillSwitchUser] usr
		INNER JOIN [dbo].[KillSwitchSite] s ON usr.SiteId = s.Id
	WHERE
		usr.UserName = @username
END

GO
ALTER AUTHORIZATION ON [dbo].[GetDetailsForRiskMgmtUser_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetDetailsForRiskMgmtUser_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetEnvironmentExists_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetEnvironmentExists_SP]( 
	@EnvironmentName [nvarchar](100)
	)
AS
BEGIN
	SELECT 
		[Name] 
	FROM 
		[dbo].[IntegratorEnvironment]
	WHERE
		[Name] = @EnvironmentName
END

GO
ALTER AUTHORIZATION ON [dbo].[GetEnvironmentExists_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetEnvironmentExists_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetEnvironmentSettingsSet_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetEnvironmentSettingsSet_SP] 
	@EnvironmentName NVARCHAR(100)
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		COALESCE(mandatorySettings.SettingsName, configuredSettings.SettingsName) AS SettingsName
		,CASE WHEN configuredSettings.SettingsName IS NULL THEN 0 ELSE 1 END AS IsConfigured
		,COUNT(configuredSettings.SettingsName) AS DistinctValuesCount
	FROM
		(
		SELECT
			mandSett.SettingsName AS SettingsName
		FROM
			[dbo].[IntegratorEnvironment] env 
			INNER JOIN [dbo].[IntegratorEnvironmentType] envtype ON env.IntegratorEnvironmentTypeId = envtype.Id
			INNER JOIN [dbo].[SettingsMandatoryInEnvironemntType] mandSett ON mandSett.IntegratorEnvironmentTypeId = envtype.Id
		WHERE
			env.Name = @EnvironmentName
		) mandatorySettings

		FULL OUTER JOIN

		(
		SELECT
			sett.Name AS SettingsName
		FROM
			[dbo].[IntegratorEnvironment] envint 
			INNER JOIN [dbo].[IntegratorEnvironmentToSettingsMap] map ON envint.Id = map.IntegratorEnvironmentId
			INNER JOIN [dbo].[Settings] sett ON sett.id = map.SettingsId
		WHERE
			envint.Name = @EnvironmentName
		) configuredSettings 
		
		ON mandatorySettings.SettingsName = configuredSettings.SettingsName

	GROUP BY
		mandatorySettings.SettingsName, configuredSettings.SettingsName
END

GO
ALTER AUTHORIZATION ON [dbo].[GetEnvironmentSettingsSet_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetEnvironmentSettingsSet_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetExternalOrdersTransmissionSwitchState_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetExternalOrdersTransmissionSwitchState_SP] 
	@SwitchName NVARCHAR(64) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		[SwitchName],
		[TransmissionEnabled]
	FROM 
		[dbo].[ExternalOrdersTransmissionSwitch]
	WHERE 
		@SwitchName IS NULL OR [SwitchName] = @SwitchName
END


GO
ALTER AUTHORIZATION ON [dbo].[GetExternalOrdersTransmissionSwitchState_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetExternalOrdersTransmissionSwitchState_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetFixSettingsCfg_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetFixSettingsCfg_SP] 
	@EnvironmentName NVARCHAR(100),
	@SettingName NVARCHAR(100)
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		cfg.Configuration
	FROM
		[dbo].[IntegratorEnvironment] env 
		INNER JOIN [dbo].[IntegratorEnvironmentToSettingsFixMap] map ON env.Id = map.IntegratorEnvironmentId
		INNER JOIN [dbo].[Settings_FIX] cfg ON cfg.id = map.SettingsFixId
	WHERE
		env.Name = @EnvironmentName
		AND cfg.Name = @SettingName
END

GO
ALTER AUTHORIZATION ON [dbo].[GetFixSettingsCfg_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetFixSettingsCfg_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetIgnoredOrders_OLD_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetIgnoredOrders_OLD_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN
	
	SELECT 
		ord.[IntegratorSentExternalOrderUtc] AS IntegratorSentExternalOrderUtc
		,ord.ExternalOrderLabelForCounterparty AS ExternalOrderLabelForCounterparty
		,ord.RequestedAmountBaseAbs AS RequestedAmountBaseAbs
		,ord.RequestedPrice AS RequestedPrice
		,ctp.CounterpartyCode AS CounterpartyCode
		,symbol.Name AS Symbol
		,direction.Name AS Direction
	FROM
		[dbo].[OrderExternal] ord
		LEFT JOIN [dbo].[DealExternalExecuted] deal on deal.[IntegratorExternalOrderPrivateId] = ord.[IntegratorExternalOrderPrivateId]
		LEFT JOIN [dbo].[DealExternalRejected] reject on reject.[IntegratorExternalOrderPrivateId] = ord.[IntegratorExternalOrderPrivateId]
		LEFT JOIN [dbo].[OrderExternalCancelResult] cancel on cancel.[IntegratorExternalOrderPrivateId] = ord.[IntegratorExternalOrderPrivateId]
		INNER JOIN [dbo].[Counterparty] ctp on ctp.CounterpartyId = ord.CounterpartyId
		INNER JOIN [dbo].Symbol symbol on symbol.Id = ord.SymbolId
		INNER JOIN [dbo].[DealDirection] direction on direction.Id = ord.DealDirectionId
	WHERE 
		ord.[IntegratorSentExternalOrderUtc] BETWEEN @StartTime AND @EndTime
		AND ord.[IntegratorSentExternalOrderUtc] < DATEADD(SECOND, -120, GETUTCDATE())
		AND deal.IntegratorExternalOrderPrivateId IS NULL AND reject.IntegratorExternalOrderPrivateId IS NULL AND cancel.[IntegratorExternalOrderPrivateId] IS NULL
		
	
	--
	-- Here goes version with Index hints, however resulting plan is even worse
	--
	
	--SELECT 
		--[IntegratorSentExternalOrderUtc]
	--FROM [dbo].[OrderExternal] ord
	--LEFT JOIN [dbo].[DealExternalExecuted] deal WITH(INDEX(IX_DealExternalExecuted_OrderId)) on deal.[IntegratorExternalOrderPrivateId] = ord.[IntegratorExternalOrderPrivateId]
	--LEFT JOIN [dbo].[DealExternalRejected] reject WITH(INDEX(IX_DealExternalRejected_OrderId)) on reject.[IntegratorExternalOrderPrivateId] = ord.[IntegratorExternalOrderPrivateId]
	--LEFT JOIN [dbo].[OrderExternalCancelResult] cancel WITH(INDEX(IX_OrderExternalCancelResult_OrderId)) on cancel.[IntegratorExternalOrderPrivateId] = ord.[IntegratorExternalOrderPrivateId]
	--WHERE ord.[IntegratorSentExternalOrderUtc] BETWEEN @StartTime AND @EndTime
	--AND deal.IntegratorExternalOrderPrivateId IS NULL AND reject.IntegratorExternalOrderPrivateId IS NULL AND cancel.[IntegratorExternalOrderPrivateId] IS NULL

END

GO
ALTER AUTHORIZATION ON [dbo].[GetIgnoredOrders_OLD_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetIgnoredOrders_OLD_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetIgnoredOrders_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetIgnoredOrders_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN
	
	SELECT 
		ord.[IntegratorSentExternalOrderUtc] AS IntegratorSentExternalOrderUtc
		,ord.ExternalOrderLabelForCounterparty AS ExternalOrderLabelForCounterparty
		,ord.RequestedAmountBaseAbs AS RequestedAmountBaseAbs
		,ord.RequestedPrice AS RequestedPrice
		,ctp.CounterpartyCode AS CounterpartyCode
		,symbol.Name AS Symbol
		,direction.Name AS Direction
	FROM
		[dbo].[OrderExternalIgnored] ignoredOrder
		INNER JOIN [dbo].[OrderExternal] ord ON ignoredOrder.IntegratorSentExternalOrderUtc = ord.IntegratorSentExternalOrderUtc AND ignoredOrder.IntegratorExternalOrderPrivateId = ord.IntegratorExternalOrderPrivateId
		INNER JOIN [dbo].[Counterparty] ctp on ctp.CounterpartyId = ord.CounterpartyId
		INNER JOIN [dbo].Symbol symbol on symbol.Id = ord.SymbolId
		INNER JOIN [dbo].[DealDirection] direction on direction.Id = ord.DealDirectionId
	WHERE 
		ignoredOrder.[IntegratorSentExternalOrderUtc] BETWEEN @StartTime AND @EndTime

END

GO
ALTER AUTHORIZATION ON [dbo].[GetIgnoredOrders_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetIgnoredOrders_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetNopAbsTotal_Daily_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetNopAbsTotal_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.Price) AS NopTermPol
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		deal.SymbolId 


	SELECT
		-- From SHORTs
		SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
	FROM
		(
		SELECT
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
		GROUP BY
			CCYPositions.CCY
		) AS CtpCCYPositions

END

GO
ALTER AUTHORIZATION ON [dbo].[GetNopAbsTotal_Daily_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetNopAbsTotal_Daily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetNopAbsTotal_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetNopAbsTotal_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		symbol.[Id] AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.Price) AS NopTermPol
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Id]


	SELECT
		-- From SHORTs
		SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
	FROM
		(
		SELECT
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
		GROUP BY
			CCYPositions.CCY
		) AS CtpCCYPositions

END

GO
ALTER AUTHORIZATION ON [dbo].[GetNopAbsTotal_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetNopAbsTotal_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetNopsAbsPerCounterparty_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetNopsAbsPerCounterparty_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
	GROUP BY
		deal.SymbolId,
		deal.CounterpartyId
		


	SELECT
		CtpCCYPositions.Counterparty AS Counterparty,
		-- from SHORTs
		SUM(CASE WHEN CtpCCYPositions.NopPolInUSD < 0 THEN -CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
		--SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs
	FROM
		(
		SELECT
			ctp.CounterpartyCode AS Counterparty,
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			Counterparty ctp
			LEFT JOIN
			(
			SELECT 
				basePos.CounterpartyId,
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT 
				termPos.CounterpartyId,
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions ON ctp.CounterpartyId = CCYPositions.CounterpartyId
		WHERE
			ctp.Active = 1
		GROUP BY
			ctp.CounterpartyCode
			,CCYPositions.CCY
		) AS CtpCCYPositions
	GROUP BY
		CtpCCYPositions.Counterparty
	ORDER BY
		CtpCCYPositions.Counterparty
END

GO
ALTER AUTHORIZATION ON [dbo].[GetNopsAbsPerCounterparty_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetNopsAbsPerCounterparty_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetNopsPolPerCounterpartyAndCurrency_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetNopsPolPerCounterpartyAndCurrency_SP] (
	@Day Date
	)
AS
BEGIN

	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		deal.CounterpartyId,
		deal.SymbolId
		
		

		SELECT
			ctp.CounterpartyCode AS Counterparty,
			CCYPositions.CCY AS CCY,
			SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD
		FROM
			(
			SELECT 
				basePos.CounterpartyId,
				ccy1.CurrencyAlphaCode AS CCY,
				basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy1
				INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
				INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

			UNION ALL

			SELECT 
				termPos.CounterpartyId,
				ccy2.CurrencyAlphaCode AS CCY,
				termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
			FROM
				Currency ccy2
				INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
				INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
			) AS CCYPositions
			INNER JOIN [dbo].[Counterparty] ctp ON ctp.CounterpartyId = CCYPositions.CounterpartyId
		GROUP BY
			ctp.CounterpartyCode
			,CCYPositions.CCY
		ORDER BY
			ctp.CounterpartyCode
			,CCYPositions.CCY

END

GO
ALTER AUTHORIZATION ON [dbo].[GetNopsPolPerCounterpartyAndCurrency_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetNopsPolPerCounterpartyAndCurrency_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetNopsPolSidedPerCurrency_Daily_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetNopsPolSidedPerCurrency_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,0),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		deal.SymbolId AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		deal.SymbolId


	SELECT
		CCYPositions.CCY AS CCY,
		SUM(CCYPositions.NopPol) AS NopPol,
		SUM(CCYPositions.NopPolInUsd) AS NopPolInUsd
	FROM
		(
		SELECT 
			ccy1.CurrencyAlphaCode AS CCY,
			basePos.NopBasePol AS NopPol,
			basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
		FROM
			Currency ccy1
			INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
			INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

		UNION ALL

		SELECT 
			ccy2.CurrencyAlphaCode AS CCY,
			termPos.NopTermPol AS NopPol,
			termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
		FROM
			Currency ccy2
			INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
			INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
		) AS CCYPositions
	GROUP BY
		CCYPositions.CCY
	ORDER BY
		CCYPositions.CCY
END

GO
ALTER AUTHORIZATION ON [dbo].[GetNopsPolSidedPerCurrency_Daily_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetNopsPolSidedPerCurrency_Daily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetNopsPolSidedPerCurrency_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[GetNopsPolSidedPerCurrency_SP] (
	@Day DATE
	)
AS
BEGIN
	
	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		NopBasePol decimal(18,0),
		NopTermPol decimal(18,2)
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, NopBasePol, NopTermPol)
	SELECT
		symbol.[Id] AS SymbolId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Id]


	SELECT
		CCYPositions.CCY AS CCY,
		SUM(CCYPositions.NopPol) AS NopPol,
		SUM(CCYPositions.NopPolInUsd) AS NopPolInUsd
	FROM
		(
		SELECT 
			ccy1.CurrencyAlphaCode AS CCY,
			basePos.NopBasePol AS NopPol,
			basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd
		FROM
			Currency ccy1
			INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
			INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

		UNION ALL

		SELECT 
			ccy2.CurrencyAlphaCode AS CCY,
			termPos.NopTermPol AS NopPol,
			termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd
		FROM
			Currency ccy2
			INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
			INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
		) AS CCYPositions
	GROUP BY
		CCYPositions.CCY
	ORDER BY
		CCYPositions.CCY
END

GO
ALTER AUTHORIZATION ON [dbo].[GetNopsPolSidedPerCurrency_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetNopsPolSidedPerCurrency_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetNopsPolSidedPerPair_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetNopsPolSidedPerPair_SP] (
	@Day DATE
	)
AS
BEGIN
	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
		--following two make more sense after extracting multipliers behind brackets
		SUM(deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) AS PnlTermPol,
		SUM((deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) * termCurrency.LastKnownUsdConversionMultiplier) AS PnlTermPolInUsd,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1,
		SUM(ABS(deal.[AmountBasePolExecuted])* deal.[Price] * termCurrency.LastKnownUsdConversionMultiplier) AS VolumeInUsd
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetNopsPolSidedPerPair_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetNopsPolSidedPerPair_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetOpenPositionBaseSizePolToday_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetOpenPositionBaseSizePolToday_SP] 
	@FxPairNoSlash char(6),
	@CounterpartyCode char(3),
	@OpenPositionBaseSizePolToday decimal(18,0) output
	
AS
BEGIN

SET NOCOUNT ON;

DECLARE @Today DATE = GETUTCDATE()

SELECT 
	@OpenPositionBaseSizePolToday = 
	COALESCE(SUM(AmountBasePolExecuted),0) -- will always return number, never NULL

FROM 
	DealExternalExecuted deal
	INNER JOIN Symbol sym ON deal.SymbolId = sym.Id
	INNER JOIN Counterparty ctp ON deal.CounterpartyId = ctp.CounterpartyId
WHERE
	DatePartIntegratorReceivedExecutionReportUtc = @Today
	AND sym.ShortName = @FxPairNoSlash
	AND ctp.CounterpartyCode = @CounterpartyCode

END
GO
ALTER AUTHORIZATION ON [dbo].[GetOpenPositionBaseSizePolToday_SP] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[GetPolarizedNopPerPair_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPolarizedNopPerPair_SP] (
	@StartTime DateTime,
	@EndTime DateTime
	)
AS
BEGIN
	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[ExecutedAmountBasePol]) AS PolarizedNop
	FROM 
		[dbo].[DealExternalExecuted_View] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[ExecutionTimeStampUtc] BETWEEN @StartTime AND @EndTime
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]

END
GO
ALTER AUTHORIZATION ON [dbo].[GetPolarizedNopPerPair_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetPolarizedNopPerPair_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetRecentExternalOrdersTransmissionControlState_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetRecentExternalOrdersTransmissionControlState_SP] 
	@SwitchName NVARCHAR(64),
	@ChangedBy NVARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SwitchId INT 
	SELECT @SwitchId = [Id] FROM [dbo].[ExternalOrdersTransmissionSwitch] WHERE [SwitchName] = @SwitchName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Switch not found in DB: %s', @SwitchName, 16, 2) WITH SETERROR
	END


	SELECT TOP 10
		[TransmissionEnabled]
		,[Changed]
		,[ChangedBy]
		,[ChangedFrom]
		,[Reason]
	FROM 
		[dbo].[ExternalOrdersTransmissionSwitchHistory]
	WHERE
		[ExternalOrdersTransmissionSwitchId] = @SwitchId
		AND	@ChangedBy IS NULL OR [ChangedBy] = @ChangedBy
	ORDER BY
		[Changed] DESC

END

GO
ALTER AUTHORIZATION ON [dbo].[GetRecentExternalOrdersTransmissionControlState_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetRecentExternalOrdersTransmissionControlState_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetRecentKillSwitchLoginActivity_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetRecentKillSwitchLoginActivity_SP] 
	@siteName NVARCHAR(64),
	@username  NVARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SiteId INT 
	SELECT @SiteId = [Id] FROM [dbo].[KillSwitchSite] WHERE [Name] = @siteName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Site not found in DB: %s', @siteName, 16, 2) WITH SETERROR
	END

	SELECT TOP 10 
		[UserName], 
		[ActionTime], 
		[Source], 
		[Success] 
	FROM 
		[dbo].[KillSwitchLogon] 
	WHERE 
		[SiteId] = @SiteId AND @username IS NULL OR [UserName] = @username 
	ORDER BY ActionTime DESC

END

GO
ALTER AUTHORIZATION ON [dbo].[GetRecentKillSwitchLoginActivity_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetRecentKillSwitchLoginActivity_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetRejectionsStatistics_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetRejectionsStatistics_SP]( 
	@Day DATE
	)
AS
BEGIN

	SELECT
		COUNT([IntegratorReceivedExecutionReportUtc]) AS NumberOfRejections
		,CounterpartyCode AS Counterparty
	FROM
		[dbo].[DealExternalRejected] rej
		INNER JOIN Counterparty ctp ON rej.[CounterpartyId] = ctp.[CounterpartyId]
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		ctp.CounterpartyCode

END

GO
ALTER AUTHORIZATION ON [dbo].[GetRejectionsStatistics_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetRejectionsStatistics_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetSettingsXml_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetSettingsXml_SP] 
	@EnvironmentName NVARCHAR(100),
	@SettingName NVARCHAR(100),
	@UpdatedAfter Datetime
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		sett.SettingsXml
	FROM
		[dbo].[IntegratorEnvironment] env 
		INNER JOIN [dbo].[IntegratorEnvironmentToSettingsMap] map ON env.Id = map.IntegratorEnvironmentId
		INNER JOIN [dbo].[Settings] sett ON sett.id = map.SettingsId
	WHERE
		env.Name = @EnvironmentName
		AND sett.Name = @SettingName
		AND sett.[LastUpdated] > @UpdatedAfter
END

GO
ALTER AUTHORIZATION ON [dbo].[GetSettingsXml_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetSettingsXml_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetSettingsXml2_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSettingsXml2_SP] 
	@SettingName NVARCHAR(100),
	@SettingFriendlyName NVARCHAR(200)
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT
		sett.SettingsXml
	FROM
		[dbo].[Settings] sett
	WHERE
		sett.Name = @SettingName
		AND sett.[FriendlyName] = @SettingFriendlyName
END

GO
ALTER AUTHORIZATION ON [dbo].[GetSettingsXml2_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetSettingsXml2_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetStatisticsPerCounterparty_Daily_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- SPs using the materialized views on SSD
--  What is added compared to previous SPs - selection from alias (instead of table) with the WITH(NOEXPAND) hint

CREATE PROCEDURE [dbo].[GetStatisticsPerCounterparty_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		RejectionsNum int,
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int
	)

	INSERT INTO @temp_rejections(CounterpartyId, RejectionsNum, IntegratorLatencyNanoseconds, IntegratorLatencyCases)
	SELECT
		[CounterpartyId],
		COUNT([CounterpartyId]) AS NumberOfRejections,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases
	FROM
		DealExternalRejected_DailyView rej WITH(NOEXPAND)
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		DealsNum int,
		IntegratorLatencyNanoseconds bigint,
		IntegratorLatencyCases int,
		CounterpartyLatencyNanoseconds bigint,
		CounterpartyLatencyCases int
	)

	INSERT INTO 
		@temp_NOPs_pol
		(
		SymbolId, 
		CounterpartyId, 
		NopBasePol, 
		NopTermPol, 
		VolumeBaseAbs, 
		DealsNum, 
		IntegratorLatencyNanoseconds, 
		IntegratorLatencyCases, 
		CounterpartyLatencyNanoseconds, 
		CounterpartyLatencyCases
		)
	SELECT
		deal.SymbolId AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum,
		SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
		COUNT(IntegratorLatencyNanoseconds) AS IntegratorLatencyCases,
		SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
		COUNT(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyCases
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
		--INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		deal.SymbolId,
		deal.CounterpartyId
		

	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		RejectionsStatistics.RejectionsNum AS RejectionsNum,
		RejectionsStatistics.RejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.RejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0)) AS RejectionsRate,
		--this calculates average of both numbers if both are non-null; or average of the nonull case if just one is not null (as 2*A / 2*B = A/B); or NULL if both are NULL (avoids divide by zero exception)
		(COALESCE(RejectionsStatistics.IntegratorLatencyNanoseconds, DealsStatistics.IntegratorLatencyNanoseconds) + COALESCE(DealsStatistics.IntegratorLatencyNanoseconds, RejectionsStatistics.IntegratorLatencyNanoseconds))
		/
		(COALESCE(RejectionsStatistics.IntegratorLatencyCases, DealsStatistics.IntegratorLatencyCases) + COALESCE(DealsStatistics.IntegratorLatencyCases, RejectionsStatistics.IntegratorLatencyCases))
		AS AvgIntegratorLatencyNanoseconds,
		DealsStatistics.CounterpartyLatencyNanoseconds / DealsStatistics.CounterpartyLatencyCases AS AvgCounterpartyLatencyNanoseconds
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			--SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(DealsNum) AS DealsNum,
			SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
			SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
			SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
			SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(DealsNum) AS DealsNum,
				SUM(IntegratorLatencyNanoseconds) AS IntegratorLatencyNanoseconds,
				SUM(IntegratorLatencyCases) AS IntegratorLatencyCases,
				SUM(CounterpartyLatencyNanoseconds) AS CounterpartyLatencyNanoseconds,
				SUM(CounterpartyLatencyCases) AS CounterpartyLatencyCases
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.DealsNum AS DealsNum,
					basePos.IntegratorLatencyNanoseconds AS IntegratorLatencyNanoseconds,
					basePos.IntegratorLatencyCases AS IntegratorLatencyCases,
					basePos.CounterpartyLatencyNanoseconds AS CounterpartyLatencyNanoseconds,
					basePos.CounterpartyLatencyCases AS CounterpartyLatencyCases
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd,
					NULL AS IntegratorLatencyNanoseconds,
					NULL AS IntegratorLatencyCases,
					NULL AS CounterpartyLatencyNanoseconds,
					NULL AS CounterpartyLatencyCases
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode
END

GO
ALTER AUTHORIZATION ON [dbo].[GetStatisticsPerCounterparty_Daily_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_Daily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_Daily_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetStatisticsPerCounterparty_DailyCached_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetStatisticsPerCounterparty_DailyCached_SP]
AS
BEGIN

	SELECT 
		[Counterparty]
		,[NopAbs]
		,[VolumeBaseAbsInUsd]
		,[VolumeShare]
		,[DealsNum]
		,[AvgDealSizeUsd]
		,[RejectionsNum]
		,[RejectionsRate]
		,[AvgIntegratorLatencyNanoseconds]
		,[AvgCounterpartyLatencyNanoseconds]
	FROM 
		[dbo].[PersistedStatisticsPerCounterpartyDaily]
	ORDER BY
		[Counterparty]

END

GO
ALTER AUTHORIZATION ON [dbo].[GetStatisticsPerCounterparty_DailyCached_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_DailyCached_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_DailyCached_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetStatisticsPerCounterparty_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Version with filtering (commented out) - turns out to be less effective that selecting everything
CREATE PROCEDURE [dbo].[GetStatisticsPerCounterparty_SP] (
	@Day DATE
	--,@CounterpartyCodesList VARCHAR(100) = NULL
	)
AS
BEGIN
	
	--DECLARE @temp_ids TABLE
	--(
	--	CounterpartyId int
	--)

	--IF @CounterpartyCodesList IS NOT NULL
	--BEGIN
	--	INSERT INTO @temp_ids(CounterpartyId)
	--	SELECT 
	--		CounterpartyId 
	--	FROM 
	--		dbo.Counterparty ctp 
	--		JOIN dbo.csvlist_to_codes_tbl(@CounterpartyCodesList) list ON ctp.CounterpartyCode = list.code
	--END
	
	DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		RejectionsNum int
	)

	INSERT INTO @temp_rejections(CounterpartyId, RejectionsNum)
	SELECT
		[CounterpartyId],
		COUNT([IntegratorReceivedExecutionReportUtc]) AS NumberOfRejections
	FROM
		[dbo].[DealExternalRejected] rej
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		DealsNum int
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol, VolumeBaseAbs, DealsNum)
	SELECT
		symbol.[Id] AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		symbol.[Id],
		deal.CounterpartyId
		


	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		RejectionsStatistics.RejectionsNum AS RejectionsNum,
		RejectionsStatistics.RejectionsNum * 1.0 / (COALESCE(RejectionsStatistics.RejectionsNum,0) + COALESCE(DealsStatistics.DealsNum,0)) AS RejectionsRate
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			--SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(DealsNum) AS DealsNum
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(DealsNum) AS DealsNum
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.DealsNum AS DealsNum
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode
END
GO
ALTER AUTHORIZATION ON [dbo].[GetStatisticsPerCounterparty_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerCounterparty_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetStatisticsPerPair_Daily_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetStatisticsPerPair_Daily_SP] (
	@Day DATE
	)
AS
BEGIN
	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
		COUNT(deal.SymbolId) AS DealsNum,
		--following two make more sense after extracting multipliers behind brackets
		SUM(deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) AS PnlTermPol,
		SUM((deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) * termCurrency.LastKnownUsdConversionMultiplier) AS PnlTermPolInUsd,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1,
		SUM(ABS(deal.[AmountBasePolExecuted])* baseCurrency.LastKnownUsdConversionMultiplier) AS VolumeInUsd
	FROM 
		DealExternalExecuted_DailyView deal WITH(NOEXPAND)
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]
END
GO
ALTER AUTHORIZATION ON [dbo].[GetStatisticsPerPair_Daily_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerPair_Daily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetStatisticsPerPair_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetStatisticsPerPair_SP] (
	@Day DATE
	)
AS
BEGIN
	
	SELECT
		symbol.[Name] AS Symbol,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted]* deal.[Price]) AS NopTermPol,
		--following two make more sense after extracting multipliers behind brackets
		SUM(deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) AS PnlTermPol,
		SUM((deal.[AmountBasePolExecuted] * symbol.LastKnownMeanReferencePrice - deal.[AmountBasePolExecuted] * deal.[Price]) * termCurrency.LastKnownUsdConversionMultiplier) AS PnlTermPolInUsd,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeInCcy1,
		SUM(ABS(deal.[AmountBasePolExecuted])* baseCurrency.LastKnownUsdConversionMultiplier) AS VolumeInUsd
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
		INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
		INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	GROUP BY
		symbol.[Name]
	ORDER BY
		symbol.[Name]
END

GO
ALTER AUTHORIZATION ON [dbo].[GetStatisticsPerPair_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerPair_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetStatisticsPerPair_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[GetSymbolReferencePrices_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GetSymbolReferencePrices_SP]
AS
BEGIN
	SELECT 
		[Name]
		,[LastKnownMeanReferencePrice]
		,[MeanReferencePriceUpdated]
	FROM 
		[dbo].[Symbol]
END

GRANT EXECUTE ON [dbo].[GetSymbolReferencePrices_SP] TO [IntegratorServiceAccount] AS [dbo]

GO
ALTER AUTHORIZATION ON [dbo].[GetSymbolReferencePrices_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetSymbolReferencePrices_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertCommandItem_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[InsertCommandItem_SP] (
	@CommandText NVARCHAR(MAX)
	)
AS
BEGIN
	BEGIN TRANSACTION
	   DECLARE @CommandId int;

		INSERT INTO [dbo].[Command]
			   ([CommandText]
			   ,[CreationTime])
		 VALUES
			   (@CommandText
			   ,GETUTCDATE())

	   SELECT @CommandId = scope_identity();

	   INSERT INTO [dbo].[NewCommand]
           ([CommandId])
		VALUES
           (@CommandId)
	COMMIT
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertCommandItem_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertCommandItem_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertDealExternalExecuted_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolExecuted decimal(18, 0),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END

	INSERT INTO 
	[dbo].[DealExternalExecuted]
           ([IntegratorReceivedExecutionReportUtc]
           ,[CounterpartyId]
           ,[SymbolId]
           ,[IntegratorExternalOrderPrivateId]
           ,[DealDirectionId]
           ,[AmountBasePolExecuted]
		   ,[AmountTermPolExecutedInUsd]
           ,[Price]
           ,[CounterpartyTransactionId]
           ,[CounterpartySuppliedExecutionTimeStampUtc]
           ,[ValueDateCounterpartySuppliedLocMktDate]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[CounterpartySentExecutionReportUtc])
     VALUES
           (@ExecutionReportReceivedUtc
           ,@CounterpartyId
           ,@SymbolId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolExecuted
		   ,@AmountTermPolExecutedInUsd
           ,@Price
           ,@CounterpartyTransactionId
           ,@CounterpartyExecutionReportTimeStampUtc
           ,@ValueDateCounterpartySuppliedLocMktDate
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@CounterpartySentExecutionReportUtc)
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertDealExternalExecuted_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertDealExternalExecuted_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertDealExternalRejected_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Stored Procedure
CREATE PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 0),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[DealExternalRejected]
           ([IntegratorReceivedExecutionReportUtc]
           ,[SymbolId]
           ,[CounterpartyId]
           ,[IntegratorExternalOrderPrivateId]
		   ,[DealDirectionId]
           ,[AmountBasePolRejected]
           ,[OrderSentToExecutionReportReceivedLatency]
           ,[RejectionReason]
		   ,[CounterpartySentExecutionReportUtc])
     VALUES
           (@ExecutionReportReceivedUtc
		   ,@SymbolId
           ,@CounterpartyId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolRejected
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@RejectionReason
		   ,@CounterpartySentExecutionReportUtc)
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertDealExternalRejected_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertDealExternalRejected_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertExecutedExternalDeal_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertExecutedExternalDeal_SP]( 
	@OrderId NVARCHAR(32),
	@ExecutionTimeStampUtc DATETIME,
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@ExecutedAmountBaseAbs DECIMAL(18,0),
	@ExecutedAmountBasePol DECIMAL(18,0),
	@Price decimal(18, 9)
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END


	INSERT INTO 
		[dbo].[ExecutedExternalDeals]
           ([OrderId]
           ,[ExecutionTimeStampUtc]
           ,[SymbolId]
           ,[DealDirectionId]
           ,[ExecutedAmountBaseAbs]
           ,[ExecutedAmountBasePol]
           ,[Price])
     VALUES
           (@OrderId
           ,@ExecutionTimeStampUtc
           ,@SymbolId
           ,@DealDirectionId 
           ,@ExecutedAmountBaseAbs
           ,@ExecutedAmountBasePol
           ,@Price)
END
GO
ALTER AUTHORIZATION ON [dbo].[InsertExecutedExternalDeal_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertExecutedExternalDeal_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertExecutionReportMessage_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Stored Procedure
CREATE PROCEDURE [dbo].[InsertExecutionReportMessage_SP]( 
	@ExecutionReportReceivedUtc [datetime2](7),
	@ExecutionReportStatus [varchar](16),
	@InternalOrderId [nvarchar](45),
	@ExecutionReportCounterpartySentUtc [datetime2](7),
	@RawFIXMessage [nvarchar](1024),
	@ParsedFIXMessage [nvarchar](MAX)
	)
AS
BEGIN

	DECLARE @ExecutionReportStatusId INT

	SELECT @ExecutionReportStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ExecutionReportStatus

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ExecutionReportStatus not found in DB: %s', 16, 2, @ExecutionReportStatus) WITH SETERROR
	END


	INSERT INTO 
	[dbo].[ExecutionReportMessage]
           ([IntegratorReceivedExecutionReportUtc]
           ,[ExecutionReportStatusId]
           ,[IntegratorExternalOrderPrivateId]
           ,[CounterpartySentExecutionReportUtc]
           ,[FixMessageReceivedRaw]
           ,[FixMessageReceivedParsed])
     VALUES
           (
		   @ExecutionReportReceivedUtc
           ,@ExecutionReportStatusId
           ,@InternalOrderId
           ,@ExecutionReportCounterpartySentUtc
           ,@RawFIXMessage
           ,@ParsedFIXMessage
		   )
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertExecutionReportMessage_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertExecutionReportMessage_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertNewOrderExternal_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Stored procedure
CREATE PROCEDURE [dbo].[InsertNewOrderExternal_SP]( 
	@ExternalOrderSentUtc [datetime2](7),
	@InternalOrderId [nvarchar](45),
	@InternalClientOrderId [nvarchar](32),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@QuoteReceivedToOrderSentInternalLatency [time](7),
	@FixMessageSentRaw nvarchar(1024),
	@FixMessageSentParsed nvarchar(MAX),
	@OrderTypeName varchar(16),
	@OrderTimeInForceName varchar(16),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,0),
	@SourceIntegratorPriceIdentity uniqueidentifier = NULL,
	@SourceIntegratorPriceTimeUtc [datetime2](7) = NULL,
	@PegTypeName varchar(16) = NULL,
	@PegDifferenceBasePol decimal(18,9) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @OrderTypeId TINYINT
	DECLARE @OrderTimeInForceId TINYINT
	DECLARE @PegTypeId BIT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	SELECT @OrderTypeId = [OrderTypeId] FROM [dbo].[OrderType] WHERE [OrderTypeName] = @OrderTypeName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderType not found in DB: %s', 16, 2, @OrderTypeName) WITH SETERROR
	END
	
	SELECT @OrderTimeInForceId = [OrderTimeInForceId] FROM [dbo].[OrderTimeInForce] WHERE [OrderTimeInForceName] = @OrderTimeInForceName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderTimeInForce not found in DB: %s', 16, 2, @OrderTimeInForceName) WITH SETERROR
	END
	
	IF @OrderTypeName = 'Pegged'
	BEGIN
		SELECT @PegTypeId = [PegTypeId] FROM [dbo].[PegType] WHERE [PegTypeName] = @PegTypeName
		
		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'PegType not found in DB: %s', 16, 2, @PegTypeName) WITH SETERROR
		END
		
		IF @PegDifferenceBasePol IS NULL
		BEGIN
			RAISERROR (N'PegDifferenceBasePol was not specified for pegged order - but it is compulsory', 16, 2) WITH SETERROR
		END
	END

	INSERT INTO 
		[dbo].[OrderExternal]
           ([IntegratorSentExternalOrderUtc]
           ,[IntegratorExternalOrderPrivateId]
		   ,[ExternalOrderLabelForCounterparty]
           ,[CounterpartyId]
           ,[SymbolId]
		   ,[DealDirectionId]
           ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[FixMessageSentRaw]
		   ,[FixMessageSentParsed]
		   ,[OrderTypeId]
		   ,[OrderTimeInForceId]
		   ,[RequestedPrice]
		   ,[RequestedAmountBaseAbs]
		   ,[SourceIntegratorPriceIdentity]
		   ,[SourceIntegratorPriceTimeUtc]
		   ,[PegTypeId]
		   ,[PegDifferenceBasePol])
     VALUES
           (@ExternalOrderSentUtc
           ,@InternalOrderId
		   ,@InternalClientOrderId
           ,@CounterpartyId
           ,@SymbolId
		   ,@DealDirectionId
           ,@QuoteReceivedToOrderSentInternalLatency
		   ,@FixMessageSentRaw
		   ,@FixMessageSentParsed
		   ,@OrderTypeId
		   ,@OrderTimeInForceId
		   ,@RequestedPrice
		   ,@RequestedAmountBaseAbs
		   ,@SourceIntegratorPriceIdentity
		   ,@SourceIntegratorPriceTimeUtc
		   ,@PegTypeId
		   ,@PegDifferenceBasePol)
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertNewOrderExternal_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertNewOrderExternal_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertOrderExternalCancelRequest_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Stored Procedure
CREATE PROCEDURE [dbo].[InsertOrderExternalCancelRequest_SP]( 
	@IntegratorExternalOrderPrivateId [nvarchar](45),
    @IntegratorSentCancelRequestUtc  DATETIME2(7),
    @OrderExternalCancelTypeName VARCHAR(16),
    @RawFIXMessage [nvarchar](1024),
	@ParsedFIXMessage [nvarchar](MAX)
	)
AS
BEGIN

	DECLARE @OrderExternalCancelTypeId TINYINT

	SELECT @OrderExternalCancelTypeId = [OrderExternalCancelTypeId] FROM [dbo].[OrderExternalCancelType] WHERE [OrderExternalCancelTypeName] = @OrderExternalCancelTypeName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderExternalCancelTypeName not found in DB: %s', 16, 2, @OrderExternalCancelTypeName) WITH SETERROR
	END


	INSERT INTO [dbo].[OrderExternalCancelRequest]
           ([IntegratorExternalOrderPrivateId]
           ,[IntegratorSentCancelRequestUtc]
           ,[OrderExternalCancelTypeId]
           ,[FixMessageSentRaw]
           ,[FixMessageSentParsed])
     VALUES
           (@IntegratorExternalOrderPrivateId
           ,@IntegratorSentCancelRequestUtc
           ,@OrderExternalCancelTypeId
           ,@RawFIXMessage
           ,@ParsedFIXMessage)
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertOrderExternalCancelRequest_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertOrderExternalCancelRequest_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertOrderExternalCancelResult_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Stored Procedure
CREATE PROCEDURE [dbo].[InsertOrderExternalCancelResult_SP]( 
	@IntegratorExternalOrderPrivateId [nvarchar](45),
    @CounterpartySentResultUtc  DATETIME2(7),
    @CanceledAmountBasePol decimal(18,0),
    @ResultStatusName VARCHAR(16)
	)
AS
BEGIN

	DECLARE @ResultStatusId TINYINT

	SELECT @ResultStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ResultStatusName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ResultStatusName not found in DB: %s', 16, 2, @ResultStatusName) WITH SETERROR
	END


	INSERT INTO [dbo].[OrderExternalCancelResult]
           ([IntegratorExternalOrderPrivateId]
           ,[CounterpartySentResultUtc]
           ,[CanceledAmountBasePol]
           ,[ResultStatusId])
     VALUES
           (@IntegratorExternalOrderPrivateId
           ,@CounterpartySentResultUtc
           ,@CanceledAmountBasePol
           ,@ResultStatusId)
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertOrderExternalCancelResult_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertOrderExternalCancelResult_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[InsertOrderExternalIgnored_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[InsertOrderExternalIgnored_SP]( 
	@ExternalOrderSentUtc [datetime2](7),
	@ExternalOrderIgnoreDetectedUtc [datetime2](7),
	@IntegratorExternalOrderPrivateId [nvarchar](45)
	)
AS
BEGIN
	INSERT INTO 
	[dbo].[OrderExternalIgnored]
           ([IntegratorSentExternalOrderUtc]
           ,[IntegratorDetectedOrderIsIgnoredUtc]
           ,[IntegratorExternalOrderPrivateId])
     VALUES
           (@ExternalOrderSentUtc
           ,@ExternalOrderIgnoreDetectedUtc
           ,@IntegratorExternalOrderPrivateId)
END

GO
ALTER AUTHORIZATION ON [dbo].[InsertOrderExternalIgnored_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[InsertOrderExternalIgnored_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[Test1]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Test1]
(
	@Day Date,
	@CounterpartyCodesList VARCHAR(100) = NULL
)
AS

BEGIN

	DECLARE @temp_ids TABLE
	(
		CounterpartyId int
	)

	IF @CounterpartyCodesList IS NOT NULL
	BEGIN
		INSERT INTO @temp_ids(CounterpartyId)
		SELECT 
			CounterpartyId 
		FROM 
			dbo.Counterparty ctp 
			JOIN dbo.csvlist_to_codes_tbl(@CounterpartyCodesList) list ON ctp.CounterpartyCode = list.code
	END

	DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		RejectionsNum int
	)

	INSERT INTO @temp_rejections(CounterpartyId, RejectionsNum)
	SELECT
		[CounterpartyId],
		COUNT([IntegratorReceivedExecutionReportUtc]) AS NumberOfRejections
	FROM
		[dbo].[DealExternalRejected] rej
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		DealsNum int
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol, VolumeBaseAbs, DealsNum)
	SELECT
		symbol.[Id] AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		symbol.[Id],
		deal.CounterpartyId
		


	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		RejectionsStatistics.RejectionsNum AS RejectionsNum,
		RejectionsStatistics.RejectionsNum * 1.0 / (RejectionsStatistics.RejectionsNum + DealsStatistics.DealsNum) AS RejectionsRate
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			--SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(DealsNum) AS DealsNum
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(DealsNum) AS DealsNum
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.DealsNum AS DealsNum
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

					--@temp_NOPs_pol basePos
					--INNER JOIN Symbol sym ON basePos.SymbolId = sym.Id
					--INNER JOIN Currency ccy1 ON ccy1.CurrencyID = sym.BaseCurrencyId

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode

END
GO
ALTER AUTHORIZATION ON [dbo].[Test1] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[Test2]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Test2]
(
	@Day Date--,
	--@CounterpartyCodesList VARCHAR(100) = NULL
)
AS

BEGIN

	DECLARE @temp_ids TABLE
	(
		CounterpartyId int
	)

	--IF @CounterpartyCodesList IS NOT NULL
	--BEGIN
	--	INSERT INTO @temp_ids(CounterpartyId)
	--	SELECT 
	--		CounterpartyId 
	--	FROM 
	--		dbo.Counterparty ctp 
	--		JOIN dbo.csvlist_to_codes_tbl(@CounterpartyCodesList) list ON ctp.CounterpartyCode = list.code
	--END

	DECLARE @temp_rejections TABLE
	(
		CounterpartyId int,
		RejectionsNum int
	)

	INSERT INTO @temp_rejections(CounterpartyId, RejectionsNum)
	SELECT
		[CounterpartyId],
		COUNT([IntegratorReceivedExecutionReportUtc]) AS NumberOfRejections
	FROM
		[dbo].[DealExternalRejected] rej
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		[CounterpartyId]


	DECLARE @temp_NOPs_pol TABLE
	(
		SymbolId int,
		CounterpartyId int,
		NopBasePol decimal(18,2),
		NopTermPol decimal(18,2),
		VolumeBaseAbs decimal(18,2),
		DealsNum int
	)

	INSERT INTO @temp_NOPs_pol(SymbolId, CounterpartyId, NopBasePol, NopTermPol, VolumeBaseAbs, DealsNum)
	SELECT
		symbol.[Id] AS SymbolId,
		deal.CounterpartyId,
		SUM(deal.[AmountBasePolExecuted]) AS NopBasePol,
		SUM(-deal.[AmountBasePolExecuted] * deal.[Price]) AS NopTermPol,
		SUM(ABS(deal.[AmountBasePolExecuted])) AS VolumeBaseAbs,
		COUNT(deal.[AmountBasePolExecuted]) AS DealsNum
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN  [dbo].[Symbol] symbol ON deal.SymbolId = symbol.Id
	WHERE
		[DatePartIntegratorReceivedExecutionReportUtc] = @Day
	-- Order of grouping is unimportant for the meaning of data (it'll be same)
	--  but this order is  enforcing usage of the index
		--AND (@CounterpartyCodesList IS NULL OR [CounterpartyId] IN (SELECT * FROM @temp_ids))
	GROUP BY
		symbol.[Id],
		deal.CounterpartyId
		


	SELECT
		ctp.CounterpartyCode AS Counterparty,
		DealsStatistics.NopAbs AS NopAbs,
		DealsStatistics.VolumeBaseAbsInUsd AS VolumeBaseAbsInUsd,
		DealsStatistics.VolumeShare AS VolumeShare,
		DealsStatistics.DealsNum AS DealsNum,
		DealsStatistics.VolumeBaseAbsInUsd / DealsStatistics.DealsNum AS AvgDealSizeUsd,
		RejectionsStatistics.RejectionsNum AS RejectionsNum,
		RejectionsStatistics.RejectionsNum * 1.0 / (RejectionsStatistics.RejectionsNum + DealsStatistics.DealsNum) AS RejectionsRate
	FROM
		Counterparty ctp
		LEFT JOIN
		(
		SELECT
			CtpCCYPositions.CounterpartyId,
			SUM(ABS(CtpCCYPositions.NopPolInUSD)/2) AS NopAbs,
			--SUM(CASE WHEN CtpCCYPositions.NopPolInUSD > 0 THEN CtpCCYPositions.NopPolInUSD  ELSE 0 END) AS NopAbs
			SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
			SUM(VolumeBaseAbsInUsd) / SUM(SUM(VolumeBaseAbsInUsd)) OVER () AS VolumeShare,
			SUM(DealsNum) AS DealsNum
		FROM
			(
			SELECT
				CCYPositions.CounterpartyId,
				CCYPositions.CCY AS CCY,
				SUM(CCYPositions.NopPolInUsd) AS NopPolInUSD,
				SUM(VolumeBaseAbsInUsd) AS VolumeBaseAbsInUsd,
				SUM(DealsNum) AS DealsNum
			FROM
				(
				SELECT 
					basePos.CounterpartyId,
					ccy1.CurrencyAlphaCode AS CCY,
					basePos.NopBasePol * ccy1.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					basePos.VolumeBaseAbs * ccy1.LastKnownUsdConversionMultiplier AS VolumeBaseAbsInUsd,
					basePos.DealsNum AS DealsNum
				FROM
					Currency ccy1
					INNER JOIN Symbol sym ON ccy1.CurrencyID = sym.BaseCurrencyId
					INNER JOIN @temp_NOPs_pol basePos ON basePos.SymbolId = sym.Id

					--@temp_NOPs_pol basePos
					--INNER JOIN Symbol sym ON basePos.SymbolId = sym.Id
					--INNER JOIN Currency ccy1 ON ccy1.CurrencyID = sym.BaseCurrencyId

				UNION ALL

				SELECT 
					termPos.CounterpartyId,
					ccy2.CurrencyAlphaCode AS CCY,
					termPos.NopTermPol * ccy2.LastKnownUsdConversionMultiplier AS NopPolInUsd,
					0 AS NopPolInUsd,
					0 AS VolumeBaseAbsInUsd
				FROM
					Currency ccy2
					INNER JOIN Symbol sym ON ccy2.CurrencyID = sym.QuoteCurrencyId
					INNER JOIN @temp_NOPs_pol termPos ON termPos.SymbolId = sym.Id
				) AS CCYPositions
			GROUP BY
				CounterpartyId
				,CCYPositions.CCY
			) AS CtpCCYPositions
		GROUP BY
			CounterpartyId
		) AS DealsStatistics ON ctp.CounterpartyId = DealsStatistics.CounterpartyId
		LEFT JOIN @temp_rejections RejectionsStatistics ON ctp.CounterpartyId = RejectionsStatistics.CounterpartyId
	WHERE
		ctp.Active = 1
		--AND (@CounterpartyCodesList IS NULL OR ctp.[CounterpartyId] IN (SELECT * FROM @temp_ids))
	ORDER BY
		ctp.CounterpartyCode

END
GO
ALTER AUTHORIZATION ON [dbo].[Test2] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[TryLoginKillSwitchUser_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TryLoginKillSwitchUser_SP] 
	@username NVARCHAR(256),
	@password NVARCHAR(256),
	@identity NVARCHAR(256),
	@siteName NVARCHAR(64)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SiteId INT 
	SELECT @SiteId = [Id] FROM [dbo].[KillSwitchSite] WHERE [Name] = @siteName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Site not found in DB: %s', @siteName, 16, 2) WITH SETERROR
	END

    DECLARE @t TABLE (Loged BIT, IsAdmin BIT)
	DECLARE @Loged BIT
	DECLARE @IsAdmin BIT
	SET @Loged = 0
	SET @IsAdmin = 0

	SELECT 
		@Loged = 1, @IsAdmin = usr.IsAdmin
	FROM 
		[dbo].[KillSwitchUser] usr
	WHERE
		usr.UserName = @username AND usr.Password = @password AND usr.SiteId = @SiteId


	INSERT INTO 
		[dbo].[KillSwitchLogon]
           ([UserName]
           ,[ActionTime]
           ,[Source]
           ,[Success]
		   ,[SiteId])
     VALUES
           (@username
           ,GETUTCDATE()
           ,@identity
           ,@Loged
		   ,@SiteId)

	INSERT INTO @t VALUES (@Loged,@IsAdmin)
	SELECT Loged, IsAdmin FROM @t
END

GO
ALTER AUTHORIZATION ON [dbo].[TryLoginKillSwitchUser_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TryLoginKillSwitchUser_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[TryLoginRiskMgmtUser_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TryLoginRiskMgmtUser_SP] 
	@username NVARCHAR(256),
	@password NVARCHAR(256),
	@identity NVARCHAR(256)
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @t TABLE (Loged BIT, IsAdmin BIT, SiteName nvarchar(64))
	DECLARE @Loged BIT
	DECLARE @IsAdmin BIT
	DECLARE @SiteName NVARCHAR(64)
	DECLARE @SiteId INT
	SET @Loged = 0
	SET @IsAdmin = 0
	SET @SiteId = 1

	SELECT 
		@Loged = 1, @IsAdmin = usr.IsAdmin, @SiteName = s.Name, @SiteId = s.Id
	FROM 
		[dbo].[KillSwitchUser] usr
		INNER JOIN [dbo].[KillSwitchSite] s ON usr.SiteId = s.Id
	WHERE
		usr.UserName = @username AND usr.Password = @password


	INSERT INTO 
		[dbo].[KillSwitchLogon]
           ([UserName]
           ,[ActionTime]
           ,[Source]
           ,[Success]
		   ,[SiteId])
     VALUES
           (@username
           ,GETUTCDATE()
           ,@identity
           ,@Loged
		   ,@SiteId)

	INSERT INTO @t VALUES (@Loged,@IsAdmin,@SiteName)
	SELECT Loged, IsAdmin, SiteName FROM @t
END

GO
ALTER AUTHORIZATION ON [dbo].[TryLoginRiskMgmtUser_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[TryLoginRiskMgmtUser_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateCommandItem_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[UpdateCommandItem_SP] (
	@CommandId int,
	@UpdateStatus NVARCHAR(32)
	)
AS
BEGIN
	INSERT INTO [dbo].[CommandStatus]
           ([CommandId]
           ,[Status]
           ,[Time])
     VALUES
           (@CommandId
           ,@UpdateStatus
           ,GETUTCDATE())
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdateCommandItem_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateCommandItem_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateCounterpartyMonitorSettings_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UpdateCounterpartyMonitorSettings_SP] 
	@EnvironmentName NVARCHAR(100),
	@SettingsFriendlyName NVARCHAR(200),
	@SettingsXml Xml
AS
BEGIN
	DECLARE @CurrentDiagDashboardSettingId INT
	DECLARE @CurrentDiagDashboardSettingFriendlyName NVARCHAR(200)
	DECLARE @EnvironmentId INT


	SELECT
		@CurrentDiagDashboardSettingId = sett.Id,
		@CurrentDiagDashboardSettingFriendlyName = sett.FriendlyName,
		@EnvironmentId = envint.Id
	FROM
		[dbo].[IntegratorEnvironment] envint 
		INNER JOIN [dbo].[IntegratorEnvironmentToSettingsMap] map ON envint.Id = map.IntegratorEnvironmentId
		INNER JOIN [dbo].[Settings] sett ON sett.id = map.SettingsId
	WHERE
		envint.Name = @EnvironmentName
		AND sett.Name = 'Kreslik.Integrator.DiagnosticsDashboard.exe'

	IF @CurrentDiagDashboardSettingFriendlyName = @SettingsFriendlyName
	BEGIN
		-- This is update to current Diag dashboar setting
		UPDATE 
		[dbo].[Settings] 
		SET 
			[SettingsXml] = @SettingsXml, 
			[LastUpdated] = GETUTCDATE()
		WHERE
			[Id] = @CurrentDiagDashboardSettingId

		--and we are done
	END
	ELSE
	BEGIN
		DECLARE @NewDiagDashboardSettingId INT = NULL

		SELECT
			@NewDiagDashboardSettingId = sett.Id
		FROM
			[dbo].[Settings] sett
		WHERE
			sett.Name = 'Kreslik.Integrator.DiagnosticsDashboard.exe'
			AND sett.FriendlyName = @SettingsFriendlyName

		IF @NewDiagDashboardSettingId IS NULL
		BEGIN
			-- This is creation of fresh new setting 
			
			INSERT INTO [dbo].[Settings]
				([Name]
				,[FriendlyName]
				,[SettingsVersion]
				,[SettingsXml]
				,[LastUpdated]
				,[IsOnlineMonitored])
			VALUES
				(N'Kreslik.Integrator.DiagnosticsDashboard.exe'
				,@SettingsFriendlyName
				,1
				,@SettingsXml
				,GETUTCDATE()
				,0
				)

			SELECT @NewDiagDashboardSettingId = SCOPE_IDENTITY()

		END
		ELSE
		BEGIN
			-- This is update of existing setting outside current environment
			
			UPDATE 
			[dbo].[Settings] 
			SET 
				[SettingsXml] = @SettingsXml, 
				[LastUpdated] = GETUTCDATE()
			WHERE
				[Id] = @NewDiagDashboardSettingId
		END

		-- Now we need to remap environment

		UPDATE 
			[dbo].[IntegratorEnvironmentToSettingsMap]
		SET 
			[SettingsId] = @NewDiagDashboardSettingId
		WHERE
			[IntegratorEnvironmentId] = @EnvironmentId
			AND [SettingsId] = @CurrentDiagDashboardSettingId
	END
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdateCounterpartyMonitorSettings_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateCounterpartyMonitorSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
AS
BEGIN

	--create the temp table before transaction to speed up things
	
	CREATE TABLE #TempCounterpartyStatistics(
		[Counterparty] [varchar](3) NOT NULL,
		[NopAbs] decimal(18,2) NULL,
		[VolumeBaseAbsInUsd] decimal(18, 2) NULL,
		[VolumeShare] decimal(18, 4) NULL,
		[DealsNum] int NULL,
		[AvgDealSizeUsd] decimal(18,2) NULL,
		[RejectionsNum] int NULL,
		[RejectionsRate] decimal(18,4) NULL,
		[AvgIntegratorLatencyNanoseconds] bigint NULL,
		[AvgCounterpartyLatencyNanoseconds] bigint
	)
	
	DECLARE @Today DATE = GETUTCDATE()
	
	INSERT INTO 
		#TempCounterpartyStatistics
		(
		[Counterparty],
		[NopAbs],
		[VolumeBaseAbsInUsd],
		[VolumeShare],
		[DealsNum],
		[AvgDealSizeUsd],
		[RejectionsNum],
		[RejectionsRate],
		[AvgIntegratorLatencyNanoseconds],
		[AvgCounterpartyLatencyNanoseconds]
		)
	exec [dbo].[GetStatisticsPerCounterparty_Daily_SP]  @Today

	BEGIN TRANSACTION
	   TRUNCATE TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily]

	   INSERT INTO 
			[dbo].[PersistedStatisticsPerCounterpartyDaily]
			(
			[Counterparty],
			[NopAbs],
			[VolumeBaseAbsInUsd],
			[VolumeShare],
			[DealsNum],
			[AvgDealSizeUsd],
			[RejectionsNum],
			[RejectionsRate],
			[AvgIntegratorLatencyNanoseconds],
			[AvgCounterpartyLatencyNanoseconds]
			)
		SELECT
			[Counterparty],
			[NopAbs],
			[VolumeBaseAbsInUsd],
			[VolumeShare],
			[DealsNum],
			[AvgDealSizeUsd],
			[RejectionsNum],
			[RejectionsRate],
			[AvgIntegratorLatencyNanoseconds],
			[AvgCounterpartyLatencyNanoseconds]
		FROM
			#TempCounterpartyStatistics

	COMMIT
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateSettingsXml_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[UpdateSettingsXml_SP] (
	@SettingName NVARCHAR(100),
	@SettingsXml Xml
	)
AS
BEGIN
	UPDATE 
	[dbo].[Settings] SET 
		[SettingsXml] = @SettingsXml, 
		[LastUpdated] = GETUTCDATE()
	WHERE
		[Name] = @SettingName
END


GO
ALTER AUTHORIZATION ON [dbo].[UpdateSettingsXml_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateSettingsXml_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateSymbolReferencePrice_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UpdateSymbolReferencePrice_SP] (
	@SymbolName NCHAR(7),
	@Price decimal(18,9)
	)
AS
BEGIN
	UPDATE [dbo].[Symbol]
	SET
      [LastKnownMeanReferencePrice] = @Price
      ,[MeanReferencePriceUpdated] = GETUTCDATE()
	WHERE 
		[Name] = @SymbolName
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdateSymbolReferencePrice_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateSymbolReferencePrice_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[UpdateUsdConversionMultiplier_SP]    Script Date: 17. 1. 2014 17:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UpdateUsdConversionMultiplier_SP] (
	@CurrencyCode NCHAR(3),
	@ConversionMultiplier decimal(18,9)
	)
AS
BEGIN
	UPDATE [dbo].[Currency]
	SET
      [LastKnownUsdConversionMultiplier] = @ConversionMultiplier
      ,[UsdConversionMultiplierUpdated] = GETUTCDATE()
	WHERE 
		[CurrencyAlphaCode] = @CurrencyCode
END

GO
ALTER AUTHORIZATION ON [dbo].[UpdateUsdConversionMultiplier_SP] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[UpdateUsdConversionMultiplier_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Synonym [dbo].[DealExternalExecuted_DailyView]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE SYNONYM [dbo].[DealExternalExecuted_DailyView] FOR [DealExternalExecutedDailyView_01/17/2014]
GO
ALTER AUTHORIZATION ON [dbo].[DealExternalExecuted_DailyView] TO  SCHEMA OWNER 
GO
/****** Object:  Synonym [dbo].[DealExternalRejected_DailyView]    Script Date: 17. 1. 2014 17:29:35 ******/
CREATE SYNONYM [dbo].[DealExternalRejected_DailyView] FOR [DealExternalRejectedDailyView_01/17/2014]
GO
ALTER AUTHORIZATION ON [dbo].[DealExternalRejected_DailyView] TO  SCHEMA OWNER 
GO
/****** Object:  Synonym [dbo].[OrderExternal_DailyView]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE SYNONYM [dbo].[OrderExternal_DailyView] FOR [OrderExternalDailyView_01/17/2014]
GO
ALTER AUTHORIZATION ON [dbo].[OrderExternal_DailyView] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[csvlist_to_codes_tbl]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
		CREATE FUNCTION [dbo].[csvlist_to_codes_tbl] (@list nvarchar(MAX))
   RETURNS @tbl TABLE (code varchar(3) NOT NULL) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1
      INSERT @tbl (code)
         VALUES (substring(@list, @pos + 1, @valuelen))
      SELECT @pos = @nextpos
   END
   RETURN
END

GO
ALTER AUTHORIZATION ON [dbo].[csvlist_to_codes_tbl] TO  SCHEMA OWNER 
GO
/****** Object:  UserDefinedFunction [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState] 
()
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultState BIT
	SET @ResultState = 0


	 SELECT 
	 @ResultState = 
	 CASE 
		WHEN SUM(CASE WHEN [TransmissionEnabled] = 0 THEN 1 ELSE 0 END) > 0 
		THEN 0 
		ELSE 1 
		END 
	FROM [dbo].[ExternalOrdersTransmissionSwitch]


	-- Return the result of the function
	RETURN @ResultState
END

GO
ALTER AUTHORIZATION ON [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  UserDefinedFunction [dbo].[GetExternalOrdersTransmissionSwitchState]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetExternalOrdersTransmissionSwitchState] 
(@SwitchName NVARCHAR(64))
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultState BIT
	SET @ResultState = 0


	SELECT TOP 1
		@ResultState = [TransmissionEnabled]
	FROM [dbo].[ExternalOrdersTransmissionSwitch]
	WHERE [SwitchName] = @SwitchName
	ORDER BY Id DESC

	-- Return the result of the function
	RETURN @ResultState
END

GO
ALTER AUTHORIZATION ON [dbo].[GetExternalOrdersTransmissionSwitchState] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetExternalOrdersTransmissionSwitchState] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  UserDefinedFunction [dbo].[GetIsKillSwitchUserAdmin]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetIsKillSwitchUserAdmin] 
(
	@UserName NVARCHAR(256)
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultState BIT
	SET @ResultState = 0


	SELECT 
		@ResultState = [IsAdmin]
	FROM 
		[dbo].[KillSwitchUser]
	WHERE
		UserName = @UserName

	-- Return the result of the function
	RETURN @ResultState
END

GO
ALTER AUTHORIZATION ON [dbo].[GetIsKillSwitchUserAdmin] TO  SCHEMA OWNER 
GO
GRANT EXECUTE ON [dbo].[GetIsKillSwitchUserAdmin] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  Table [dbo].[CertificateFile]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CertificateFile](
	[FileName] [nvarchar](100) NOT NULL,
	[FileContent] [varbinary](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[CertificateFile] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Command]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Command](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[CommandText] [nvarchar](max) NOT NULL,
	[CreationTime] [datetime] NOT NULL,
 CONSTRAINT [PK_Command] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Command] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[CommandStatus]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommandStatus](
	[CommandId] [int] NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[Time] [datetime] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[CommandStatus] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Counterparty]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Counterparty](
	[CounterpartyId] [tinyint] NOT NULL,
	[CounterpartyName] [nchar](50) NOT NULL,
	[CounterpartyCode] [char](3) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Counterparty] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrencyID] [smallint] IDENTITY(1,1) NOT NULL,
	[CurrencyAlphaCode] [nvarchar](50) NOT NULL,
	[CurrencyName] [nvarchar](250) NOT NULL,
	[CurrencyISOCode] [smallint] NULL,
	[Valid] [bit] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[LastKnownUsdConversionMultiplier] [decimal](18, 9) NULL,
	[UsdConversionMultiplierUpdated] [datetime2](7) NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[CurrencyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Currency] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[DealDirection]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealDirection](
	[Id] [bit] NOT NULL,
	[Name] [nvarchar](4) NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[DealDirection] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[DealExternalExecuted]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DealExternalExecuted](
	[IntegratorReceivedExecutionReportUtc] [datetime2](7) NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[AmountBasePolExecuted] [decimal](18, 0) NOT NULL,
	[Price] [decimal](18, 9) NOT NULL,
	[CounterpartyTransactionId] [nvarchar](64) NOT NULL,
	[CounterpartySuppliedExecutionTimeStampUtc] [datetime2](7) NULL,
	[ValueDateCounterpartySuppliedLocMktDate] [date] NOT NULL,
	[OrderSentToExecutionReportReceivedLatency] [time](7) NOT NULL,
	[CounterpartySentExecutionReportUtc] [datetime2](7) NULL,
	[AmountTermPolExecutedInUsd] [decimal](18, 2) NULL,
	[DatePartIntegratorReceivedExecutionReportUtc]  AS (CONVERT([date],[IntegratorReceivedExecutionReportUtc])) PERSISTED
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[DealExternalExecuted] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[DealExternalRejected]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DealExternalRejected](
	[IntegratorReceivedExecutionReportUtc] [datetime2](7) NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[AmountBasePolRejected] [decimal](18, 0) NOT NULL,
	[OrderSentToExecutionReportReceivedLatency] [time](7) NOT NULL,
	[RejectionReason] [nvarchar](64) NOT NULL,
	[CounterpartySentExecutionReportUtc] [datetime2](7) NULL,
	[DatePartIntegratorReceivedExecutionReportUtc]  AS (CONVERT([date],[IntegratorReceivedExecutionReportUtc])) PERSISTED
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[DealExternalRejected] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[ExecutedExternalDeals]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExecutedExternalDeals](
	[OrderId] [nvarchar](32) NOT NULL,
	[ExecutionTimeStampUtc] [datetime] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[ExecutedAmountBaseAbs] [decimal](18, 0) NOT NULL,
	[ExecutedAmountBasePol] [decimal](18, 0) NOT NULL,
	[Price] [decimal](18, 9) NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[ExecutedExternalDeals] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[ExecutionReportMessage]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExecutionReportMessage](
	[IntegratorReceivedExecutionReportUtc] [datetime2](7) NOT NULL,
	[ExecutionReportStatusId] [tinyint] NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[CounterpartySentExecutionReportUtc] [datetime2](7) NOT NULL,
	[FixMessageReceivedRaw] [nvarchar](1024) NOT NULL,
	[FixMessageReceivedParsed] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[ExecutionReportMessage] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[ExecutionReportStatus]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExecutionReportStatus](
	[Id] [tinyint] NOT NULL,
	[Name] [varchar](16) NOT NULL,
 CONSTRAINT [IX_ExecutionReportStatusName] UNIQUE NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[ExecutionReportStatus] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[ExternalOrdersTransmissionSwitch]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExternalOrdersTransmissionSwitch](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SwitchName] [nvarchar](64) NOT NULL,
	[TransmissionEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_ExternalOrdersTransmissionSwitch] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[ExternalOrdersTransmissionSwitch] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[ExternalOrdersTransmissionSwitchHistory]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExternalOrdersTransmissionSwitchHistory](
	[ExternalOrdersTransmissionSwitchId] [int] NOT NULL,
	[TransmissionEnabled] [bit] NOT NULL,
	[Changed] [datetime] NOT NULL,
	[ChangedBy] [nvarchar](256) NOT NULL,
	[ChangedFrom] [nvarchar](256) NOT NULL,
	[Reason] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[ExternalOrdersTransmissionSwitchHistory] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorEnvironment]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntegratorEnvironment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[IntegratorEnvironmentTypeId] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[IntegratorEnvironment] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorEnvironmentToSettingsFixMap]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap](
	[IntegratorEnvironmentId] [int] NOT NULL,
	[SettingsFixId] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[IntegratorEnvironmentToSettingsFixMap] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorEnvironmentToSettingsMap]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntegratorEnvironmentToSettingsMap](
	[IntegratorEnvironmentId] [int] NOT NULL,
	[SettingsId] [int] NOT NULL,
 CONSTRAINT [MappingIsUnique] UNIQUE NONCLUSTERED 
(
	[IntegratorEnvironmentId] ASC,
	[SettingsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[IntegratorEnvironmentToSettingsMap] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[IntegratorEnvironmentType]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntegratorEnvironmentType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](200) NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[IntegratorEnvironmentType] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[KillSwitchLogon]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KillSwitchLogon](
	[UserName] [nvarchar](256) NOT NULL,
	[ActionTime] [datetime] NOT NULL,
	[Source] [nvarchar](256) NOT NULL,
	[Success] [bit] NOT NULL,
	[SiteId] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[KillSwitchLogon] TO  SCHEMA OWNER 
GO
GRANT SELECT ON [dbo].[KillSwitchLogon] TO [IntegratorKillSwitchUser] AS [dbo]
GO
/****** Object:  Table [dbo].[KillSwitchSite]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KillSwitchSite](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_KillSwitchSite] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[KillSwitchSite] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[KillSwitchUser]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KillSwitchUser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[Password] [nvarchar](256) NOT NULL,
	[IsAdmin] [bit] NOT NULL,
	[SiteId] [int] NOT NULL,
 CONSTRAINT [PK_KillSwitchUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_KillSwitchUser] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[KillSwitchUser] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[NewCommand]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewCommand](
	[CommandId] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[NewCommand] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternal]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderExternal](
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[ExternalOrderLabelForCounterparty] [nvarchar](32) NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[DealDirectionId] [bit] NOT NULL,
	[QuoteReceivedToOrderSentInternalLatency] [time](7) NULL,
	[FixMessageSentRaw] [nvarchar](1024) NULL,
	[FixMessageSentParsed] [nvarchar](max) NULL,
	[SourceIntegratorPriceIdentity] [uniqueidentifier] NULL,
	[OrderTypeId] [tinyint] NULL,
	[OrderTimeInForceId] [tinyint] NULL,
	[RequestedPrice] [decimal](18, 9) NULL,
	[RequestedAmountBaseAbs] [decimal](18, 0) NULL,
	[SourceIntegratorPriceTimeUtc] [datetime2](7) NULL,
	[PegTypeId] [bit] NULL,
	[PegDifferenceBasePol] [decimal](18, 9) NULL,
	[DatePartIntegratorSentExternalOrderUtc]  AS (CONVERT([date],[IntegratorSentExternalOrderUtc])) PERSISTED
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[OrderExternal] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalCancelRequest]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalCancelRequest](
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NULL,
	[IntegratorSentCancelRequestUtc] [datetime2](7) NOT NULL,
	[OrderExternalCancelTypeId] [tinyint] NOT NULL,
	[FixMessageSentRaw] [nvarchar](1024) NOT NULL,
	[FixMessageSentParsed] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalCancelRequest] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalCancelResult]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalCancelResult](
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[CounterpartySentResultUtc] [datetime2](7) NOT NULL,
	[CanceledAmountBasePol] [decimal](18, 0) NOT NULL,
	[ResultStatusId] [tinyint] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalCancelResult] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalCancelType]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderExternalCancelType](
	[OrderExternalCancelTypeId] [tinyint] NOT NULL,
	[OrderExternalCancelTypeName] [varchar](16) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalCancelType] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderExternalIgnored]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalIgnored](
	[IntegratorSentExternalOrderUtc] [datetime2](7) NOT NULL,
	[IntegratorDetectedOrderIsIgnoredUtc] [datetime2](7) NOT NULL,
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalIgnored] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderTimeInForce]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderTimeInForce](
	[OrderTimeInForceId] [tinyint] NOT NULL,
	[OrderTimeInForceName] [varchar](16) NOT NULL,
 CONSTRAINT [IX_OrderTimeInForceName] UNIQUE NONCLUSTERED 
(
	[OrderTimeInForceName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[OrderTimeInForce] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[OrderType]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderType](
	[OrderTypeId] [tinyint] NOT NULL,
	[OrderTypeName] [varchar](16) NOT NULL,
 CONSTRAINT [IX_OrderTypeName] UNIQUE NONCLUSTERED 
(
	[OrderTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[OrderType] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[PegType]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PegType](
	[PegTypeId] [bit] NOT NULL,
	[PegTypeName] [varchar](16) NOT NULL,
 CONSTRAINT [IX_PegTypeName] UNIQUE NONCLUSTERED 
(
	[PegTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[PegType] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[PersistedStatisticsPerCounterpartyDaily]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersistedStatisticsPerCounterpartyDaily](
	[Counterparty] [varchar](3) NOT NULL,
	[NopAbs] [decimal](18, 2) NULL,
	[VolumeBaseAbsInUsd] [decimal](18, 2) NULL,
	[VolumeShare] [decimal](18, 4) NULL,
	[DealsNum] [int] NULL,
	[AvgDealSizeUsd] [decimal](18, 2) NULL,
	[RejectionsNum] [int] NULL,
	[RejectionsRate] [decimal](18, 4) NULL,
	[AvgIntegratorLatencyNanoseconds] [bigint] NULL,
	[AvgCounterpartyLatencyNanoseconds] [bigint] NULL
) ON [Integrator_DailyData]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[PersistedStatisticsPerCounterpartyDaily] TO  SCHEMA OWNER 
GO
GRANT ALTER ON [dbo].[PersistedStatisticsPerCounterpartyDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[PersistedStatisticsPerCounterpartyDaily] TO [IntegratorServiceAccount] AS [dbo]
GO
/****** Object:  Table [dbo].[Settings]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[FriendlyName] [nvarchar](200) NULL,
	[SettingsVersion] [int] NOT NULL,
	[SettingsXml] [xml] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[IsOnlineMonitored] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Settings] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Settings_FIX]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings_FIX](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[SettingsVersion] [int] NOT NULL,
	[Configuration] [nvarchar](max) NOT NULL,
	[LastUpdated] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Settings_FIX] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[SettingsMandatoryInEnvironemntType]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SettingsMandatoryInEnvironemntType](
	[IntegratorEnvironmentTypeId] [int] NOT NULL,
	[SettingsName] [nvarchar](100) NOT NULL,
 CONSTRAINT [MandatorySettingIsUnique] UNIQUE NONCLUSTERED 
(
	[IntegratorEnvironmentTypeId] ASC,
	[SettingsName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[SettingsMandatoryInEnvironemntType] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Symbol]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Symbol](
	[Id] [tinyint] NOT NULL,
	[Name] [nchar](7) NOT NULL,
	[ShortName] [nchar](6) NOT NULL,
	[BaseCurrencyId] [smallint] NOT NULL,
	[QuoteCurrencyId] [smallint] NOT NULL,
	[Multiplier] [int] NOT NULL,
	[Decimals]  AS (log10([Multiplier])) PERSISTED,
	[IsRaboTradeable] [bit] NULL,
	[Usage] [tinyint] NULL,
	[LastKnownMeanReferencePrice] [decimal](18, 9) NULL,
	[MeanReferencePriceUpdated] [datetime2](7) NULL,
 CONSTRAINT [PK_Symbol] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Symbol] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Test]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Test](
	[name] [nvarchar](10) NOT NULL,
	[Value] [bit] NOT NULL
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Test] TO  SCHEMA OWNER 
GO
/****** Object:  View [dbo].[DealExternalExecuted_View]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- View
CREATE VIEW [dbo].[DealExternalExecuted_View]
AS
SELECT
	[OrderId]
	,[ExecutionTimeStampUtc]
	,[SymbolId]
	,[DealDirectionId]
	,[ExecutedAmountBaseAbs]
	,[ExecutedAmountBasePol]
	,[Price]
FROM [dbo].[ExecutedExternalDeals]
WHERE [ExecutionTimeStampUtc] < '2013-07-29 14:30:00.000'

  UNION ALL

SELECT 
	[IntegratorExternalOrderPrivateId] AS [OrderId]
	,[IntegratorReceivedExecutionReportUtc] AS [ExecutionTimeStampUtc]
	,[SymbolId]
	,[DealDirectionId]
	,ABS([AmountBasePolExecuted]) AS [ExecutedAmountBaseAbs]
	,[AmountBasePolExecuted] AS [ExecutedAmountBasePol]
	,[Price]
FROM [dbo].[DealExternalExecuted]
WHERE [IntegratorReceivedExecutionReportUtc] > '2013-07-29 14:30:00.000'

GO
ALTER AUTHORIZATION ON [dbo].[DealExternalExecuted_View] TO  SCHEMA OWNER 
GO
/****** Object:  View [dbo].[DealExternalExecutedDailyView_01/17/2014]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE VIEW [dbo].[DealExternalExecutedDailyView_01/17/2014]
	WITH SCHEMABINDING 
	AS 
	SELECT 
		deal.[IntegratorReceivedExecutionReportUtc]
		,deal.[CounterpartyId]
		,deal.[SymbolId]
		,deal.[IntegratorExternalOrderPrivateId]
		,deal.[AmountBasePolExecuted]
		,deal.[Price]
		,CAST(
			(
				(datepart(HOUR, ordr.QuoteReceivedToOrderSentInternalLatency)*60 + datepart(MINUTE, ordr.QuoteReceivedToOrderSentInternalLatency))*60 
				+ datepart(SECOND, ordr.QuoteReceivedToOrderSentInternalLatency)
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, ordr.QuoteReceivedToOrderSentInternalLatency) 
		   AS IntegratorLatencyNanoseconds
		,CAST(
			(
				(datepart(HOUR, deal.[OrderSentToExecutionReportReceivedLatency])*60 + datepart(MINUTE, deal.[OrderSentToExecutionReportReceivedLatency]))*60 
				+ datepart(SECOND, deal.[OrderSentToExecutionReportReceivedLatency])
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, deal.[OrderSentToExecutionReportReceivedLatency]) 
		   AS CounterpartyLatencyNanoseconds
		,deal.[AmountTermPolExecutedInUsd]
		,deal.[DatePartIntegratorReceivedExecutionReportUtc]
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN [dbo].[OrderExternal] ordr ON deal.[IntegratorExternalOrderPrivateId] = ordr.[IntegratorExternalOrderPrivateId]
	WHERE
		DatePartIntegratorReceivedExecutionReportUtc = CONVERT(DATE,'01/17/2014', 101)
GO
ALTER AUTHORIZATION ON [dbo].[DealExternalExecutedDailyView_01/17/2014] TO  SCHEMA OWNER 
GO
/****** Object:  View [dbo].[DealExternalRejectedDailyView_01/17/2014]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE VIEW [dbo].[DealExternalRejectedDailyView_01/17/2014]
	WITH SCHEMABINDING 
	AS 
	SELECT 
		   reject.[IntegratorReceivedExecutionReportUtc]
		   ,reject.[IntegratorExternalOrderPrivateId]
		  ,reject.[CounterpartyId]
		  ,reject.[DatePartIntegratorReceivedExecutionReportUtc]
		  ,CAST(
			(
				(datepart(HOUR, ordr.QuoteReceivedToOrderSentInternalLatency)*60 + datepart(MINUTE, ordr.QuoteReceivedToOrderSentInternalLatency))*60 
				+ datepart(SECOND, ordr.QuoteReceivedToOrderSentInternalLatency)
			) 
			AS BIGINT
		   )
		   *1000000000 
		   + datepart(nanosecond, ordr.QuoteReceivedToOrderSentInternalLatency) 
		   AS IntegratorLatencyNanoseconds
	FROM 
		[dbo].[DealExternalRejected] reject
		INNER JOIN [dbo].[OrderExternal] ordr ON reject.[IntegratorExternalOrderPrivateId] = ordr.[IntegratorExternalOrderPrivateId]
	WHERE
		DatePartIntegratorReceivedExecutionReportUtc = CONVERT(DATE,'01/17/2014', 101)
GO
ALTER AUTHORIZATION ON [dbo].[DealExternalRejectedDailyView_01/17/2014] TO  SCHEMA OWNER 
GO
/****** Object:  View [dbo].[OrderExternalDailyView_01/17/2014]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE VIEW [dbo].[OrderExternalDailyView_01/17/2014]
	WITH SCHEMABINDING 
	AS 
	SELECT 
		   [IntegratorSentExternalOrderUtc]
		  ,[DatePartIntegratorSentExternalOrderUtc]
		  ,[IntegratorExternalOrderPrivateId]
		  ,[CounterpartyId]
		  ,[QuoteReceivedToOrderSentInternalLatency]
	FROM [dbo].[OrderExternal]
	WHERE
		DatePartIntegratorSentExternalOrderUtc = CONVERT(DATE,'01/17/2014', 101)
GO
ALTER AUTHORIZATION ON [dbo].[OrderExternalDailyView_01/17/2014] TO  SCHEMA OWNER 
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CertificateFile_FileName]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [CertificateFile_FileName] ON [dbo].[CertificateFile]
(
	[FileName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PK_Counterparty]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [PK_Counterparty] ON [dbo].[Counterparty]
(
	[CounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_DealExternalExecuted_OrderId]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE CLUSTERED INDEX [IX_DealExternalExecuted_OrderId] ON [dbo].[DealExternalExecuted]
(
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_DealExternalRejected_OrderId]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE CLUSTERED INDEX [IX_DealExternalRejected_OrderId] ON [dbo].[DealExternalRejected]
(
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ExecutionTimeStampUtc_SymbolId]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE CLUSTERED INDEX [IX_ExecutionTimeStampUtc_SymbolId] ON [dbo].[ExecutedExternalDeals]
(
	[ExecutionTimeStampUtc] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ExecutionReportMessage_OrderId]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE CLUSTERED INDEX [IX_ExecutionReportMessage_OrderId] ON [dbo].[ExecutionReportMessage]
(
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PK_ExecutionReportStatus]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [PK_ExecutionReportStatus] ON [dbo].[ExecutionReportStatus]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IntegratorEnvironment_Name]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [IntegratorEnvironment_Name] ON [dbo].[IntegratorEnvironment]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IntegratorEnvironmentType_Id]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [IntegratorEnvironmentType_Id] ON [dbo].[IntegratorEnvironmentType]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderExternal_IntegratorSentExternalOrderUtc]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE CLUSTERED INDEX [IX_OrderExternal_IntegratorSentExternalOrderUtc] ON [dbo].[OrderExternal]
(
	[IntegratorSentExternalOrderUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_OrderExternalCancelRequest_OrderId]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE CLUSTERED INDEX [IX_OrderExternalCancelRequest_OrderId] ON [dbo].[OrderExternalCancelRequest]
(
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_OrderExternalCancelResult_OrderId]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE CLUSTERED INDEX [IX_OrderExternalCancelResult_OrderId] ON [dbo].[OrderExternalCancelResult]
(
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderExternalCancelTypeId]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_OrderExternalCancelTypeId] ON [dbo].[OrderExternalCancelType]
(
	[OrderExternalCancelTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderExternalIgnored_IntegratorSentExternalOrderUtc]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE CLUSTERED INDEX [IX_OrderExternalIgnored_IntegratorSentExternalOrderUtc] ON [dbo].[OrderExternalIgnored]
(
	[IntegratorSentExternalOrderUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderTimeInForceId]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_OrderTimeInForceId] ON [dbo].[OrderTimeInForce]
(
	[OrderTimeInForceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderType]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_OrderType] ON [dbo].[OrderType]
(
	[OrderTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_PegType]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_PegType] ON [dbo].[PegType]
(
	[PegTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [Settings_Id]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [Settings_Id] ON [dbo].[Settings]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [SettingsFIX_Id]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [SettingsFIX_Id] ON [dbo].[Settings_FIX]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_ReceivedUtc_OrderId]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_ReceivedUtc_OrderId] ON [dbo].[DealExternalExecutedDailyView_01/17/2014]
(
	[IntegratorReceivedExecutionReportUtc] ASC,
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [Integrator_DailyData]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_ReceivedUtc_OrderId]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_ReceivedUtc_OrderId] ON [dbo].[DealExternalRejectedDailyView_01/17/2014]
(
	[IntegratorReceivedExecutionReportUtc] ASC,
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [Integrator_DailyData]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_OrderId]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_OrderId] ON [dbo].[OrderExternalDailyView_01/17/2014]
(
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [Integrator_DailyData]
GO
/****** Object:  Index [IX_CounterpartyCode]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_CounterpartyCode] ON [dbo].[Counterparty]
(
	[CounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueCurrencyAlphaCode]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyAlphaCode] ON [dbo].[Currency]
(
	[CurrencyAlphaCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UniqueCurrencyIsoCode]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyIsoCode] ON [dbo].[Currency]
(
	[CurrencyISOCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UniqueCurrencyName]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UniqueCurrencyName] ON [dbo].[Currency]
(
	[CurrencyName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DealDirection]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_DealDirection] ON [dbo].[DealDirection]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_DatePart_Symbol]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE NONCLUSTERED INDEX [IX_DatePart_Symbol] ON [dbo].[DealExternalExecuted]
(
	[DatePartIntegratorReceivedExecutionReportUtc] ASC,
	[SymbolId] ASC
)
INCLUDE ( 	[AmountBasePolExecuted],
	[Price],
	[AmountTermPolExecutedInUsd],
	[CounterpartyId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DealExternalExecuted_IntegratorReceivedExecutionReportUtc]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE NONCLUSTERED INDEX [IX_DealExternalExecuted_IntegratorReceivedExecutionReportUtc] ON [dbo].[DealExternalExecuted]
(
	[IntegratorReceivedExecutionReportUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_DatePart_Counterparty]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalRejected]
(
	[DatePartIntegratorReceivedExecutionReportUtc] ASC,
	[CounterpartyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DealExternalRejected_IntegratorReceivedExecutionReportUtc]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE NONCLUSTERED INDEX [IX_DealExternalRejected_IntegratorReceivedExecutionReportUtc] ON [dbo].[DealExternalRejected]
(
	[IntegratorReceivedExecutionReportUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ExecutionReportMessage_IntegratorReceivedExecutionReportUtc]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE NONCLUSTERED INDEX [IX_ExecutionReportMessage_IntegratorReceivedExecutionReportUtc] ON [dbo].[ExecutionReportMessage]
(
	[IntegratorReceivedExecutionReportUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IntegratorEnvironment_Id]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IntegratorEnvironment_Id] ON [dbo].[IntegratorEnvironment]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_KillSwitchLogon]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE NONCLUSTERED INDEX [IX_KillSwitchLogon] ON [dbo].[KillSwitchLogon]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_OrderExternal_OrderId]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_OrderExternal_OrderId] ON [dbo].[OrderExternal]
(
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderExternalCancelRequest_IntegratorSentCancelRequestUtc]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE NONCLUSTERED INDEX [IX_OrderExternalCancelRequest_IntegratorSentCancelRequestUtc] ON [dbo].[OrderExternalCancelRequest]
(
	[IntegratorSentCancelRequestUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderExternalCancelResult_CounterpartySentResultUtc]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE NONCLUSTERED INDEX [IX_OrderExternalCancelResult_CounterpartySentResultUtc] ON [dbo].[OrderExternalCancelResult]
(
	[CounterpartySentResultUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_OrderExternalCancelTypeName]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_OrderExternalCancelTypeName] ON [dbo].[OrderExternalCancelType]
(
	[OrderExternalCancelTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [settings_friendlyName]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE NONCLUSTERED INDEX [settings_friendlyName] ON [dbo].[Settings]
(
	[FriendlyName] ASC
)
WHERE ([FriendlyName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Symbol]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Symbol] ON [dbo].[Symbol]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_DatePart_Counterparty]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalExecutedDailyView_01/17/2014]
(
	[DatePartIntegratorReceivedExecutionReportUtc] ASC,
	[CounterpartyId] ASC,
	[SymbolId] ASC
)
INCLUDE ( 	[AmountBasePolExecuted],
	[Price],
	[AmountTermPolExecutedInUsd],
	[IntegratorLatencyNanoseconds],
	[CounterpartyLatencyNanoseconds]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [Integrator_DailyData]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_DatePart_Counterparty]    Script Date: 17. 1. 2014 17:29:36 ******/
CREATE NONCLUSTERED INDEX [IX_DatePart_Counterparty] ON [dbo].[DealExternalRejectedDailyView_01/17/2014]
(
	[DatePartIntegratorReceivedExecutionReportUtc] ASC,
	[CounterpartyId] ASC
)
INCLUDE ( 	[IntegratorLatencyNanoseconds]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [Integrator_DailyData]
GO
ALTER TABLE [dbo].[IntegratorEnvironment] ADD  DEFAULT ((1)) FOR [IntegratorEnvironmentTypeId]
GO
ALTER TABLE [dbo].[KillSwitchLogon] ADD  CONSTRAINT [DF_KillSwitchLogon_SiteId]  DEFAULT ((1)) FOR [SiteId]
GO
ALTER TABLE [dbo].[KillSwitchUser] ADD  CONSTRAINT [DF_KillSwitchUser_SiteId]  DEFAULT ((1)) FOR [SiteId]
GO
ALTER TABLE [dbo].[Settings] ADD  DEFAULT ((1)) FOR [SettingsVersion]
GO
ALTER TABLE [dbo].[Settings_FIX] ADD  DEFAULT ((1)) FOR [SettingsVersion]
GO
ALTER TABLE [dbo].[CommandStatus]  WITH CHECK ADD  CONSTRAINT [FK_CommandStatus_Command] FOREIGN KEY([CommandId])
REFERENCES [dbo].[Command] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CommandStatus] CHECK CONSTRAINT [FK_CommandStatus_Command]
GO
ALTER TABLE [dbo].[DealExternalExecuted]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecuted_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[DealExternalExecuted] CHECK CONSTRAINT [FK_DealExternalExecuted_Counterparty]
GO
ALTER TABLE [dbo].[DealExternalExecuted]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecuted_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[DealExternalExecuted] CHECK CONSTRAINT [FK_DealExternalExecuted_DealDirection]
GO
ALTER TABLE [dbo].[DealExternalExecuted]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecuted_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO
ALTER TABLE [dbo].[DealExternalExecuted] CHECK CONSTRAINT [FK_DealExternalExecuted_OrderExternal]
GO
ALTER TABLE [dbo].[DealExternalExecuted]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalExecuted_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO
ALTER TABLE [dbo].[DealExternalExecuted] CHECK CONSTRAINT [FK_DealExternalExecuted_Symbol]
GO
ALTER TABLE [dbo].[DealExternalRejected]  WITH CHECK ADD  CONSTRAINT [FK_DealExternalRejected_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[DealExternalRejected] CHECK CONSTRAINT [FK_DealExternalRejected_DealDirection]
GO
ALTER TABLE [dbo].[DealExternalRejected]  WITH CHECK ADD  CONSTRAINT [FK_RejectedDealExternal_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[DealExternalRejected] CHECK CONSTRAINT [FK_RejectedDealExternal_Counterparty]
GO
ALTER TABLE [dbo].[DealExternalRejected]  WITH CHECK ADD  CONSTRAINT [FK_RejectedDealExternal_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO
ALTER TABLE [dbo].[DealExternalRejected] CHECK CONSTRAINT [FK_RejectedDealExternal_OrderExternal]
GO
ALTER TABLE [dbo].[DealExternalRejected]  WITH CHECK ADD  CONSTRAINT [FK_RejectedDealExternal_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO
ALTER TABLE [dbo].[DealExternalRejected] CHECK CONSTRAINT [FK_RejectedDealExternal_Symbol]
GO
ALTER TABLE [dbo].[ExecutedExternalDeals]  WITH CHECK ADD  CONSTRAINT [FK_ExecutedExternalDeals_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[ExecutedExternalDeals] CHECK CONSTRAINT [FK_ExecutedExternalDeals_DealDirection]
GO
ALTER TABLE [dbo].[ExecutedExternalDeals]  WITH CHECK ADD  CONSTRAINT [FK_ExecutedExternalDeals_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO
ALTER TABLE [dbo].[ExecutedExternalDeals] CHECK CONSTRAINT [FK_ExecutedExternalDeals_Symbol]
GO
ALTER TABLE [dbo].[ExecutionReportMessage]  WITH CHECK ADD  CONSTRAINT [FK_ExecutionReportMessage_ExecutionReportStatus] FOREIGN KEY([ExecutionReportStatusId])
REFERENCES [dbo].[ExecutionReportStatus] ([Id])
GO
ALTER TABLE [dbo].[ExecutionReportMessage] CHECK CONSTRAINT [FK_ExecutionReportMessage_ExecutionReportStatus]
GO
ALTER TABLE [dbo].[ExecutionReportMessage]  WITH CHECK ADD  CONSTRAINT [FK_ExecutionReportMessage_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO
ALTER TABLE [dbo].[ExecutionReportMessage] CHECK CONSTRAINT [FK_ExecutionReportMessage_OrderExternal]
GO
ALTER TABLE [dbo].[ExternalOrdersTransmissionSwitchHistory]  WITH CHECK ADD  CONSTRAINT [FK_ExternalOrdersTransmissionSwitchHistory_ExternalOrdersTransmissionSwitch] FOREIGN KEY([ExternalOrdersTransmissionSwitchId])
REFERENCES [dbo].[ExternalOrdersTransmissionSwitch] ([Id])
GO
ALTER TABLE [dbo].[ExternalOrdersTransmissionSwitchHistory] CHECK CONSTRAINT [FK_ExternalOrdersTransmissionSwitchHistory_ExternalOrdersTransmissionSwitch]
GO
ALTER TABLE [dbo].[IntegratorEnvironment]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironment_IntegratorEnvironmentType] FOREIGN KEY([IntegratorEnvironmentTypeId])
REFERENCES [dbo].[IntegratorEnvironmentType] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironment] CHECK CONSTRAINT [FK_IntegratorEnvironment_IntegratorEnvironmentType]
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironmentToSettingsFixMap_Environment] FOREIGN KEY([IntegratorEnvironmentId])
REFERENCES [dbo].[IntegratorEnvironment] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap] CHECK CONSTRAINT [FK_IntegratorEnvironmentToSettingsFixMap_Environment]
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironmentToSettingsFixMap_SettingsFix] FOREIGN KEY([SettingsFixId])
REFERENCES [dbo].[Settings_FIX] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsFixMap] CHECK CONSTRAINT [FK_IntegratorEnvironmentToSettingsFixMap_SettingsFix]
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsMap]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironmentToSettingsMap_Environment] FOREIGN KEY([IntegratorEnvironmentId])
REFERENCES [dbo].[IntegratorEnvironment] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsMap] CHECK CONSTRAINT [FK_IntegratorEnvironmentToSettingsMap_Environment]
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsMap]  WITH CHECK ADD  CONSTRAINT [FK_IntegratorEnvironmentToSettingsMap_Settings] FOREIGN KEY([SettingsId])
REFERENCES [dbo].[Settings] ([Id])
GO
ALTER TABLE [dbo].[IntegratorEnvironmentToSettingsMap] CHECK CONSTRAINT [FK_IntegratorEnvironmentToSettingsMap_Settings]
GO
ALTER TABLE [dbo].[KillSwitchLogon]  WITH CHECK ADD  CONSTRAINT [FK_KillSwitchLogon_KillSwitchSite] FOREIGN KEY([SiteId])
REFERENCES [dbo].[KillSwitchSite] ([Id])
GO
ALTER TABLE [dbo].[KillSwitchLogon] CHECK CONSTRAINT [FK_KillSwitchLogon_KillSwitchSite]
GO
ALTER TABLE [dbo].[KillSwitchUser]  WITH CHECK ADD  CONSTRAINT [FK_KillSwitchUser_KillSwitchSite] FOREIGN KEY([SiteId])
REFERENCES [dbo].[KillSwitchSite] ([Id])
GO
ALTER TABLE [dbo].[KillSwitchUser] CHECK CONSTRAINT [FK_KillSwitchUser_KillSwitchSite]
GO
ALTER TABLE [dbo].[NewCommand]  WITH CHECK ADD  CONSTRAINT [FK_NewCommand_Command] FOREIGN KEY([CommandId])
REFERENCES [dbo].[Command] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NewCommand] CHECK CONSTRAINT [FK_NewCommand_Command]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_Counterparty]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_DealDirection] FOREIGN KEY([DealDirectionId])
REFERENCES [dbo].[DealDirection] ([Id])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_DealDirection]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_OrderTimeInForce] FOREIGN KEY([OrderTimeInForceId])
REFERENCES [dbo].[OrderTimeInForce] ([OrderTimeInForceId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_OrderTimeInForce]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_OrderType] FOREIGN KEY([OrderTypeId])
REFERENCES [dbo].[OrderType] ([OrderTypeId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_OrderType]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_PegType] FOREIGN KEY([PegTypeId])
REFERENCES [dbo].[PegType] ([PegTypeId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_PegType]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_Symbol]
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest] CHECK CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternal]
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternalCancelType] FOREIGN KEY([OrderExternalCancelTypeId])
REFERENCES [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId])
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest] CHECK CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternalCancelType]
GO
ALTER TABLE [dbo].[OrderExternalCancelResult]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelResult_ExecutionReportStatus] FOREIGN KEY([ResultStatusId])
REFERENCES [dbo].[ExecutionReportStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderExternalCancelResult] CHECK CONSTRAINT [FK_OrderExternalCancelResult_ExecutionReportStatus]
GO
ALTER TABLE [dbo].[OrderExternalCancelResult]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelResult_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO
ALTER TABLE [dbo].[OrderExternalCancelResult] CHECK CONSTRAINT [FK_OrderExternalCancelResult_OrderExternal]
GO
ALTER TABLE [dbo].[OrderExternalIgnored]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalIgnored_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO
ALTER TABLE [dbo].[OrderExternalIgnored] CHECK CONSTRAINT [FK_OrderExternalIgnored_OrderExternal]
GO
ALTER TABLE [dbo].[SettingsMandatoryInEnvironemntType]  WITH CHECK ADD  CONSTRAINT [FK_SettingsMandatoryInEnvironemntType_IntegratorEnvironmentType] FOREIGN KEY([IntegratorEnvironmentTypeId])
REFERENCES [dbo].[IntegratorEnvironmentType] ([Id])
GO
ALTER TABLE [dbo].[SettingsMandatoryInEnvironemntType] CHECK CONSTRAINT [FK_SettingsMandatoryInEnvironemntType_IntegratorEnvironmentType]
GO
ALTER TABLE [dbo].[Symbol]  WITH CHECK ADD  CONSTRAINT [FK_Symbol_Currency] FOREIGN KEY([BaseCurrencyId])
REFERENCES [dbo].[Currency] ([CurrencyID])
GO
ALTER TABLE [dbo].[Symbol] CHECK CONSTRAINT [FK_Symbol_Currency]
GO
ALTER TABLE [dbo].[Symbol]  WITH CHECK ADD  CONSTRAINT [FK_Symbol_Currency1] FOREIGN KEY([QuoteCurrencyId])
REFERENCES [dbo].[Currency] ([CurrencyID])
GO
ALTER TABLE [dbo].[Symbol] CHECK CONSTRAINT [FK_Symbol_Currency1]
GO
/****** Object:  Trigger [dbo].[DealExternalExecutedInserted]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[DealExternalExecutedInserted] ON [dbo].[DealExternalExecuted] AFTER INSERT
AS
BEGIN
	-- commit the internal insert transaction to prevent deadlocks
	COMMIT TRANSACTION
	exec [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION
END

GO
/****** Object:  Trigger [dbo].[DealExternalRejectedInserted]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[DealExternalRejectedInserted] ON [dbo].[DealExternalRejected] AFTER INSERT
AS
BEGIN
	-- commit the internal insert transaction to prevent deadlocks
	COMMIT TRANSACTION
	exec [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION
END

GO
/****** Object:  Trigger [dbo].[SchemaXsdCheckTrigger]    Script Date: 17. 1. 2014 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[SchemaXsdCheckTrigger] ON [dbo].[Settings] AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @Type NVARCHAR(100)
	DECLARE @SettingsXml [xml]
	DECLARE @Version [int]
	
	DECLARE settingsCursor CURSOR FOR
		SELECT Name, SettingsVersion, SettingsXml FROM inserted
    
	OPEN settingsCursor
    FETCH NEXT FROM settingsCursor INTO @Type, @Version, @SettingsXml
    WHILE @@FETCH_STATUS = 0
    BEGIN

		IF(@Type = N'Kreslik.Integrator.BusinessLayer.dll')
		BEGIN
			DECLARE @x1 XML([dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd]) 
			SET @x1 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.Common.dll')
		BEGIN
			DECLARE @x2 XML([dbo].[Kreslik.Integrator.Common.dll.xsd]) 
			SET @x2 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL.dll' AND @Version = 1)
		BEGIN
			DECLARE @x3 XML([dbo].[Kreslik.Integrator.DAL.dll.xsd]) 
			SET @x3 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL.dll' AND @Version = 2)
		BEGIN
			DECLARE @x31 XML([dbo].[Kreslik.Integrator.DALv2.dll.xsd]) 
			SET @x31 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL_IntegratorConnection')
		BEGIN
			DECLARE @x32 XML([dbo].[Kreslik.Integrator.DALv2_IntegratorConnection.xsd]) 
			SET @x32 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL_DataCollectionConnection')
		BEGIN
			DECLARE @x33 XML([dbo].[Kreslik.Integrator.DALv2_DataCollectionConnection.xsd]) 
			SET @x33 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DAL_DataCollectionEnabling')
		BEGIN
			DECLARE @x34 XML([dbo].[Kreslik.Integrator.DALv2_DataCollectionEnabling.xsd]) 
			SET @x34 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.DiagnosticsDashboard.exe')
		BEGIN
			DECLARE @x4 XML([dbo].[Kreslik.Integrator.DiagnosticsDashboard.exe.xsd]) 
			SET @x4 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.IntegratorClientHost.exe')
		BEGIN
			DECLARE @x5 XML([dbo].[Kreslik.Integrator.IntegratorClientHost.exe.xsd]) 
			SET @x5 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.IntegratorWatchDogSvc.exe')
		BEGIN
			DECLARE @x6 XML([dbo].[Kreslik.Integrator.IntegratorWatchDogSvc.exe.xsd]) 
			SET @x6 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBus.dll')
		BEGIN
			DECLARE @x7 XML([dbo].[Kreslik.Integrator.MessageBus.dll.xsd]) 
			SET @x7 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo')
		BEGIN
			DECLARE @x7_1 XML([dbo].[Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo.xsd]) 
			SET @x7_1 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo')
		BEGIN
			DECLARE @x7_2 XML([dbo].[Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo.xsd]) 
			SET @x7_2 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping')
		BEGIN
			DECLARE @x7_3 XML([dbo].[Kreslik.Integrator.MessageBusSettings_ConnectionKeeping.xsd]) 
			SET @x7_3 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior')
		BEGIN
			DECLARE @x7_4 XML([dbo].[Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior.xsd]) 
			SET @x7_4 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling')
		BEGIN
			DECLARE @x7_5 XML([dbo].[Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling.xsd]) 
			SET @x7_5 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.QuickItchN.dll')
		BEGIN
			DECLARE @x8 XML([dbo].[Kreslik.Integrator.QuickItchN.dll.xsd]) 
			SET @x8 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.RiskManagement.dll')
		BEGIN
			DECLARE @x9 XML([dbo].[Kreslik.Integrator.RiskManagement.dll.xsd]) 
			SET @x9 = @SettingsXml
		END
		ELSE IF(@Type = N'Kreslik.Integrator.SessionManagement.dll')
		BEGIN
			DECLARE @x10 XML([dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]) 
			SET @x10 = @SettingsXml
		END

		ELSE
		BEGIN
			RAISERROR (N'Setting with name: %s is unknown to DB', 16, 2, @Type ) WITH SETERROR
			ROLLBACK TRAN
		END

		FETCH NEXT FROM settingsCursor INTO @Type, @Version, @SettingsXml
    END
	CLOSE settingsCursor
    DEALLOCATE settingsCursor
END

GO
EXEC sys.sp_addextendedproperty @name=N'ISO', @value=N'ISO 4217, http://www.iso.org/iso/en/prods-services/popstds/currencycodeslist.html' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency'
GO
USE [master]
GO
ALTER DATABASE [Integrator] SET  READ_WRITE 
GO


---------------------------------------------------------------------------------------
----------------------------------- DATA ----------------------------------------------


SET IDENTITY_INSERT [dbo].[ExternalOrdersTransmissionSwitch] ON 

GO
INSERT [dbo].[ExternalOrdersTransmissionSwitch] ([Id], [SwitchName], [TransmissionEnabled]) VALUES (1, N'RaboKillSwitch', 1)
GO
INSERT [dbo].[ExternalOrdersTransmissionSwitch] ([Id], [SwitchName], [TransmissionEnabled]) VALUES (2, N'KGTManualKillSwitch', 1)
GO
INSERT [dbo].[ExternalOrdersTransmissionSwitch] ([Id], [SwitchName], [TransmissionEnabled]) VALUES (3, N'KGTIntegratorKillSwitch', 1)
GO
SET IDENTITY_INSERT [dbo].[ExternalOrdersTransmissionSwitch] OFF


SET IDENTITY_INSERT [dbo].[KillSwitchSite] ON 

GO
INSERT [dbo].[KillSwitchSite] ([Id], [Name]) VALUES (1, N'killsw.kgtinv.com')
GO
INSERT [dbo].[KillSwitchSite] ([Id], [Name]) VALUES (2, N'killsw-internal.kgtinv.com')
GO
SET IDENTITY_INSERT [dbo].[KillSwitchSite] OFF


SET IDENTITY_INSERT [dbo].[Settings] ON 

GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (1, N'Kreslik.Integrator.RiskManagement.dll', NULL, 1, N'<RiskManagementSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ConfigLastUpdated>2013-11-13-1011-UTC</ConfigLastUpdated><ExternalOrdersSubmissionRate><RiskCheckAllowed>true</RiskCheckAllowed><MaximumAllowedInstancesPerTimeInterval>1000</MaximumAllowedInstancesPerTimeInterval><TimeIntervalToCheck_Seconds>5</TimeIntervalToCheck_Seconds></ExternalOrdersSubmissionRate><ExternalOrdersRejectionRate><RiskCheckAllowed>true</RiskCheckAllowed><MaximumAllowedInstancesPerTimeInterval>2000</MaximumAllowedInstancesPerTimeInterval><TimeIntervalToCheck_Seconds>60</TimeIntervalToCheck_Seconds></ExternalOrdersRejectionRate><ExternalOrderMaximumAllowedSize><RiskCheckAllowed>true</RiskCheckAllowed><MaximumAllowedBaseAbsSizeInUsd>2500000</MaximumAllowedBaseAbsSizeInUsd></ExternalOrderMaximumAllowedSize><PriceAndPosition><ExternalNOPRiskCheckAllowed>true</ExternalNOPRiskCheckAllowed><MaximumExternalNOPInUSD>100000000</MaximumExternalNOPInUSD><MaximumAllowedNOPCalculationTime_Milliseconds>5</MaximumAllowedNOPCalculationTime_Milliseconds><PriceDeviationCheckAllowed>true</PriceDeviationCheckAllowed><OnlinePriceDeviationCheckMaxPriceAge_Minutes>480</OnlinePriceDeviationCheckMaxPriceAge_Minutes><ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed>true</ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed><Symbols><Symbol ShortName="AUDCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="AUDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="AUDHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="AUDCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="AUDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="AUDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="AUDNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="AUDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="AUDSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="AUDUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="CADDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="CADCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="CADJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="CADNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="CADNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="CADSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="DKKJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="DKKNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="DKKSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURAUD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURCNH" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURGBP" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURRUB" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURSKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURTRY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="EURZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="GBPAUD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="GBPCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="GBPCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="GBPDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="GBPHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="GBPCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="GBPJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="GBPNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="GBPNZD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="GBPPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="GBPSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="GBPUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="HKDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="CHFDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="CHFJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="CHFNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="CHFSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="MXNJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="NOKJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="NOKSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="NZDCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="NZDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="NZDCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="NZDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="NZDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="NZDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="NZDSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="NZDUSD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="SGDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDCAD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDCNH" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDCZK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDDKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDHKD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDHUF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDCHF" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDILS" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDMXN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDNOK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDPLN" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDRUB" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDSEK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDSGD" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDSKK" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDTRY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="USDZAR" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /><Symbol ShortName="ZARJPY" NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent="1" /></Symbols></PriceAndPosition></RiskManagementSettings>', CAST(0x0000A2770122949F AS DateTime), 1)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (2, N'Kreslik.Integrator.BusinessLayer.dll', NULL, 1, N'<BusinessLayerSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><MarketableClientOrdersMatchingStrategy>ImmediateMatching</MarketableClientOrdersMatchingStrategy><ConsecutiveRejectionsStopStrategyBehavior><StrategyEnabled>true</StrategyEnabled><NumerOfRejectionsToTriggerStrategy>2</NumerOfRejectionsToTriggerStrategy><CounterpartyRankWeightMultiplier>20</CounterpartyRankWeightMultiplier><PriceAgeRankWeightMultiplier>1</PriceAgeRankWeightMultiplier><PriceBpRankWeightMultiplier>20</PriceBpRankWeightMultiplier><DefaultCounterpartyRank>7</DefaultCounterpartyRank><CounterpartyRanks><CounterpartyRank CounterpartyCode="UBS" Rank="5" /><CounterpartyRank CounterpartyCode="MGS" Rank="5.2" /><CounterpartyRank CounterpartyCode="CRS" Rank="6.1" /><CounterpartyRank CounterpartyCode="BOA" Rank="6.8" /><CounterpartyRank CounterpartyCode="CZB" Rank="7.1" /><CounterpartyRank CounterpartyCode="JPM" Rank="7.7" /><CounterpartyRank CounterpartyCode="CTI" Rank="8.5" /><CounterpartyRank CounterpartyCode="GLS" Rank="8.8" /><CounterpartyRank CounterpartyCode="BNP" Rank="8.9" /><CounterpartyRank CounterpartyCode="RBS" Rank="9.4" /><CounterpartyRank CounterpartyCode="BRX" Rank="9.5" /><CounterpartyRank CounterpartyCode="SOC" Rank="9.6" /><CounterpartyRank CounterpartyCode="HSB" Rank="9.9" /><CounterpartyRank CounterpartyCode="NOM" Rank="10" /></CounterpartyRanks></ConsecutiveRejectionsStopStrategyBehavior><StartWithSessionsStopped>false</StartWithSessionsStopped><AutoKillInternalOrdersOnOrdersTransmissionDisabled>true</AutoKillInternalOrdersOnOrdersTransmissionDisabled><StartStopEmailSubject>Integrator service on {0} is {1}</StartStopEmailSubject><StartStopEmailBody /><StartStopEmailTo>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</StartStopEmailTo><StartStopEmailCc /><IgnoredOrderCounterpartyContacts><IgnoredOrderCounterpartyContact CounterpartyCode="CRS" EmailContacts="list.efx-support@credit-suisse.com;ivan.dexeus@credit-suisse.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="UBS" EmailContacts="fxehelp@ubs.com;gianandrea.grassi@ubs.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="DBK" EmailContacts="gm-ecommerce.implementations@db.com;thomas-m.geist@db.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="CTI" EmailContacts="fifx.support@citi.com;kenneth.aina@citi.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="BOA" EmailContacts="efx.csg@baml.com;kenneth.aina@citi.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="MGS" EmailContacts="fxacctmgt@morganstanley.com;guy.hopkins@morganstanley.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="RBS" EmailContacts="GBMEMFXTradeTimeouts@rbs.com;Martin.Pilcher@rbs.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="HSB" EmailContacts="fix.support@hsbcib.com;olivier.werenne@hsbcib.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="JPM" EmailContacts="JPMorgan_FXEcom_Urgent@jpmorgan.com;jon.price@jpmorgan.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="GLS" EmailContacts="ficc-fx-edealing-integration@gs.com;tom.robinson@gs.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="BNP" EmailContacts="fx.api.integration@uk.bnpparibas.com;siamak.nouri@uk.bnpparibas.com;steve.earl@uk.bnpparibas.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="NOM" EmailContacts="FI-ECommerceTechFXSupport@nomura.com;nicola.dilena@nomura.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="CZB" EmailContacts="esupport@commerzbank.com;Roland.White@commerzbank.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="BRX" EmailContacts="BARXsupport@barclays.com;conor.daly@barclays.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="SOC" EmailContacts="FX-support@sgcib.com;leon.brown@sgcib.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="HTA" EmailContacts="HOTFX.PRODSUPPORT@knight.com;HOTFX.LIQUIDITY@knight.com;pgertler@kcg.com" /><IgnoredOrderCounterpartyContact CounterpartyCode="HTF" EmailContacts="HOTFX.PRODSUPPORT@knight.com;HOTFX.LIQUIDITY@knight.com;pgertler@kcg.com" /></IgnoredOrderCounterpartyContacts><IgnoredOrderEmailCc>michal.kreslik@kgtinv.com;marek.fogiel@kgtinv.com;jan.krivanek@kgtinv.com</IgnoredOrderEmailCc><FatalErrorsEmailTo>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</FatalErrorsEmailTo><FatalErrorsEmailCc /><FatalErrorsEmailMinDelayBetweenEmails_Minutes>10</FatalErrorsEmailMinDelayBetweenEmails_Minutes><PreventMatchingToSameCounterpartyAfterOrderRejectBehavior><StrategyEnabled>true</StrategyEnabled><CounterpartyCodes><Counterparty>GLS</Counterparty></CounterpartyCodes></PreventMatchingToSameCounterpartyAfterOrderRejectBehavior></BusinessLayerSettings>', CAST(0x0000A2B20104EC95 AS DateTime), 1)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (3, N'Kreslik.Integrator.Common.dll', N'Integrator common (sends fatal emails)', 1, N'<CommonSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Logging><AvailableDiskSpaceThresholdToStopMDLogging_GB>15</AvailableDiskSpaceThresholdToStopMDLogging_GB></Logging><Automailer><SmtpServer>173.194.70.108</SmtpServer><SmtpPort>587</SmtpPort><BypassCertificationValidation>true</BypassCertificationValidation><SmtpUsername>automailer@kgtinv.com</SmtpUsername><SmtpPassword>Np9G3bBd</SmtpPassword><DirectoryForLocalEmails>LocalEmails</DirectoryForLocalEmails><SendEmailsOnlyToLocalFolder>false</SendEmailsOnlyToLocalFolder></Automailer></CommonSettings>', CAST(0x0000A29600D91B77 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (4, N'Kreslik.Integrator.Common.dll', N'Clients common (does not send fatal emails)', 1, N'<CommonSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Logging><AvailableDiskSpaceThresholdToStopMDLogging_GB>15</AvailableDiskSpaceThresholdToStopMDLogging_GB></Logging><Automailer><SmtpServer>173.194.70.108</SmtpServer><SmtpPort>587</SmtpPort><BypassCertificationValidation>true</BypassCertificationValidation><SmtpUsername>automailer@kgtinv.com</SmtpUsername><SmtpPassword>Np9G3bBd</SmtpPassword><DirectoryForLocalEmails>LocalEmails</DirectoryForLocalEmails><SendEmailsOnlyToLocalFolder>true</SendEmailsOnlyToLocalFolder></Automailer></CommonSettings>', CAST(0x0000A29600D91B89 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (5, N'Kreslik.Integrator.DAL.dll', N'DAL trading instance (DC for ToB and also MD)', 1, N'<DALSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ConnectionString>Data Source=192.168.75.66;Initial Catalog=Integrator;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString><KillSwitchPollIntervalSeconds>3</KillSwitchPollIntervalSeconds><KillSwitchWatchDogIntervalSeconds>40</KillSwitchWatchDogIntervalSeconds><IntegratorSwitchName>KGTIntegratorKillSwitch</IntegratorSwitchName><CommandDistributorPollIntervalSeconds>3</CommandDistributorPollIntervalSeconds><DataCollection><DataCollectionBackendConnectionString>Data Source=192.168.75.66;Initial Catalog=FXtickDB;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</DataCollectionBackendConnectionString><DataProviderId>22</DataProviderId><SourceFileId>932696</SourceFileId><EnableToBDataCollection>true</EnableToBDataCollection><EnableMarketDataCollection>true</EnableMarketDataCollection><MaxBcpBatchSize>10000</MaxBcpBatchSize><MaxInMemoryColectionSize>1000000</MaxInMemoryColectionSize><MaxIntervalBetweenUploads_Seconds>10</MaxIntervalBetweenUploads_Seconds><MaxUploadTimeAfterWhichSerializationStarts_Seconds>5</MaxUploadTimeAfterWhichSerializationStarts_Seconds><MaxProcessingTimeAfterShutdownSignalled_Seconds>30</MaxProcessingTimeAfterShutdownSignalled_Seconds><WaitIntervalBetweenBcpRetries_Seconds>2</WaitIntervalBetweenBcpRetries_Seconds><WaitIntervalBeforeShutdownStartsSerialization_Seconds>4</WaitIntervalBeforeShutdownStartsSerialization_Seconds></DataCollection></DALSettings>', CAST(0x0000A29600D91BA7 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (6, N'Kreslik.Integrator.DAL.dll', N'DAL NON-trading instance (DC for ToB only)', 1, N'<DALSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ConnectionString>Data Source=192.168.75.66;Initial Catalog=Integrator;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString><KillSwitchPollIntervalSeconds>3</KillSwitchPollIntervalSeconds><KillSwitchWatchDogIntervalSeconds>40</KillSwitchWatchDogIntervalSeconds><IntegratorSwitchName>KGTIntegratorKillSwitch</IntegratorSwitchName><CommandDistributorPollIntervalSeconds>3</CommandDistributorPollIntervalSeconds><DataCollection><DataCollectionBackendConnectionString>Data Source=192.168.75.66;Initial Catalog=FXtickDB;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</DataCollectionBackendConnectionString><DataProviderId>22</DataProviderId><SourceFileId>932696</SourceFileId><EnableToBDataCollection>true</EnableToBDataCollection><EnableMarketDataCollection>false</EnableMarketDataCollection><MaxBcpBatchSize>10000</MaxBcpBatchSize><MaxInMemoryColectionSize>1000000</MaxInMemoryColectionSize><MaxIntervalBetweenUploads_Seconds>10</MaxIntervalBetweenUploads_Seconds><MaxUploadTimeAfterWhichSerializationStarts_Seconds>5</MaxUploadTimeAfterWhichSerializationStarts_Seconds><MaxProcessingTimeAfterShutdownSignalled_Seconds>30</MaxProcessingTimeAfterShutdownSignalled_Seconds><WaitIntervalBetweenBcpRetries_Seconds>2</WaitIntervalBetweenBcpRetries_Seconds><WaitIntervalBeforeShutdownStartsSerialization_Seconds>4</WaitIntervalBeforeShutdownStartsSerialization_Seconds></DataCollection></DALSettings>', CAST(0x0000A29600D91BB7 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (7, N'Kreslik.Integrator.DiagnosticsDashboard.exe', NULL, 1, N'<DiagnosticsDashboardSettings xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><RefreshTimeout_Miliseconds>500</RefreshTimeout_Miliseconds><FirstSpreadAgeLimit_Seconds>10</FirstSpreadAgeLimit_Seconds><SecondSpreadAgeLimit_Seconds>60</SecondSpreadAgeLimit_Seconds><ThirdSpreadAgeLimit_Seconds>1200</ThirdSpreadAgeLimit_Seconds><CutOffSpreadAgeLimit_Hours>24</CutOffSpreadAgeLimit_Hours><EnabledSymbols><Symbol>AUD/CAD</Symbol><Symbol>AUD/CHF</Symbol><Symbol>AUD/DKK</Symbol><Symbol>AUD/HKD</Symbol><Symbol>AUD/JPY</Symbol><Symbol>AUD/NOK</Symbol><Symbol>AUD/NZD</Symbol><Symbol>AUD/SEK</Symbol><Symbol>AUD/SGD</Symbol><Symbol>AUD/USD</Symbol><Symbol>CAD/CHF</Symbol><Symbol>CAD/JPY</Symbol><Symbol>CAD/NOK</Symbol><Symbol>CAD/NZD</Symbol><Symbol>CAD/SEK</Symbol><Symbol>CHF/DKK</Symbol><Symbol>CHF/JPY</Symbol><Symbol>CHF/NOK</Symbol><Symbol>CHF/SEK</Symbol><Symbol>DKK/JPY</Symbol><Symbol>DKK/NOK</Symbol><Symbol>DKK/SEK</Symbol><Symbol>EUR/AUD</Symbol><Symbol>EUR/CAD</Symbol><Symbol>EUR/CHF</Symbol><Symbol>EUR/CNH</Symbol><Symbol>EUR/CZK</Symbol><Symbol>EUR/DKK</Symbol><Symbol>EUR/GBP</Symbol><Symbol>EUR/HKD</Symbol><Symbol>EUR/HUF</Symbol><Symbol>EUR/JPY</Symbol><Symbol>EUR/MXN</Symbol><Symbol>EUR/NOK</Symbol><Symbol>EUR/NZD</Symbol><Symbol>EUR/PLN</Symbol><Symbol>EUR/RUB</Symbol><Symbol>EUR/SEK</Symbol><Symbol>EUR/SKK</Symbol><Symbol>EUR/TRY</Symbol><Symbol>EUR/USD</Symbol><Symbol>EUR/ZAR</Symbol><Symbol>GBP/AUD</Symbol><Symbol>GBP/CAD</Symbol><Symbol>GBP/CHF</Symbol><Symbol>GBP/CZK</Symbol><Symbol>GBP/DKK</Symbol><Symbol>GBP/HUF</Symbol><Symbol>GBP/JPY</Symbol><Symbol>GBP/NOK</Symbol><Symbol>GBP/NZD</Symbol><Symbol>GBP/PLN</Symbol><Symbol>GBP/SEK</Symbol><Symbol>GBP/USD</Symbol><Symbol>HKD/JPY</Symbol><Symbol>MXN/JPY</Symbol><Symbol>NOK/JPY</Symbol><Symbol>NOK/SEK</Symbol><Symbol>NZD/CAD</Symbol><Symbol>NZD/CHF</Symbol><Symbol>NZD/DKK</Symbol><Symbol>NZD/JPY</Symbol><Symbol>NZD/NOK</Symbol><Symbol>NZD/SEK</Symbol><Symbol>NZD/SGD</Symbol><Symbol>NZD/USD</Symbol><Symbol>SGD/JPY</Symbol><Symbol>USD/CAD</Symbol><Symbol>USD/CHF</Symbol><Symbol>USD/CNH</Symbol><Symbol>USD/CZK</Symbol><Symbol>USD/DKK</Symbol><Symbol>USD/HKD</Symbol><Symbol>USD/HUF</Symbol><Symbol>USD/ILS</Symbol><Symbol>USD/JPY</Symbol><Symbol>USD/MXN</Symbol><Symbol>USD/NOK</Symbol><Symbol>USD/PLN</Symbol><Symbol>USD/RUB</Symbol><Symbol>USD/SEK</Symbol><Symbol>USD/SGD</Symbol><Symbol>USD/SKK</Symbol><Symbol>USD/TRY</Symbol><Symbol>USD/ZAR</Symbol><Symbol>ZAR/JPY</Symbol></EnabledSymbols><EnabledCounterparties><Counterparty>CRS</Counterparty><Counterparty>UBS</Counterparty><Counterparty>DBK</Counterparty><Counterparty>CTI</Counterparty><Counterparty>BOA</Counterparty><Counterparty>MGS</Counterparty><Counterparty>RBS</Counterparty><Counterparty>HSB</Counterparty><Counterparty>JPM</Counterparty><Counterparty>GLS</Counterparty><Counterparty>BNP</Counterparty><Counterparty>NOM</Counterparty><Counterparty>CZB</Counterparty><Counterparty>BRX</Counterparty><Counterparty>SOC</Counterparty><Counterparty>HTA</Counterparty><Counterparty>HTF</Counterparty></EnabledCounterparties></DiagnosticsDashboardSettings>', CAST(0x0000A29600D91BC9 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (8, N'Kreslik.Integrator.IntegratorClientHost.exe', NULL, 1, N'<IntegratorClientHostSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><DefaultIntegratorClientCofigurationIdentity>ClientConfigDummy2</DefaultIntegratorClientCofigurationIdentity><IntegratorClientConfigurations><IntegratorClientConfiguration><ConfigurationIdentity>ClientConfigDummy2</ConfigurationIdentity><ClientInterfaceType>Console</ClientInterfaceType><!-- Defaults to MachineName + PID --><ClientFriendlyName>DummyTestingClientA</ClientFriendlyName><!-- NullGUIClient --><ClientTypeName>Kreslik.Integrator.BusinesLayerDriver.DummyIntegratorClient2</ClientTypeName><!-- This is optional. It is ignored if AssemblyFullPath is used, otherwise only the currently executing assembly is probed --><AssemblyNameWithoutExtension>Kreslik.Integrator.BusinesLayerDriver</AssemblyNameWithoutExtension><!-- This is optional, Standard assembly probing will be used if not specified --><!-- <AssemblyFullPath>E:\foo\bar.dll</AssemblyFullPath> --><!-- This is optional, Will be used only if commandline parameter will not be specified --><DefaultConfigurationParameter>FooBar</DefaultConfigurationParameter></IntegratorClientConfiguration><IntegratorClientConfiguration><ConfigurationIdentity>ClientConfigHtaTest</ConfigurationIdentity><ClientInterfaceType>Console</ClientInterfaceType><!-- Defaults to MachineName + PID --><ClientFriendlyName>DummyTestingClientA</ClientFriendlyName><!-- NullGUIClient --><ClientTypeName>Kreslik.Integrator.BusinesLayerDriver.HotspotOrdersTest</ClientTypeName><!-- This is optional. It is ignored if AssemblyFullPath is used, otherwise only the currently executing assembly is probed --><AssemblyNameWithoutExtension>Kreslik.Integrator.BusinesLayerDriver</AssemblyNameWithoutExtension><!-- This is optional, Standard assembly probing will be used if not specified --><!-- <AssemblyFullPath>E:\foo\bar.dll</AssemblyFullPath> --><!-- This is optional, Will be used only if commandline parameter will not be specified --><DefaultConfigurationParameter>FooBar</DefaultConfigurationParameter></IntegratorClientConfiguration><IntegratorClientConfiguration><ConfigurationIdentity>ClientConfigNullGui</ConfigurationIdentity><ClientInterfaceType>Form</ClientInterfaceType><!-- Defaults to MachineName + PID --><ClientFriendlyName>DummyTestingClientA</ClientFriendlyName><!-- NullGUIClient --><ClientTypeName>Kreslik.Integrator.BusinesLayerDriver.NullGUIClient</ClientTypeName><!-- This is optional. It is ignored if AssemblyFullPath is used, otherwise only the currently executing assembly is probed --><AssemblyNameWithoutExtension>Kreslik.Integrator.BusinesLayerDriver</AssemblyNameWithoutExtension><!-- This is optional, Standard assembly probing will be used if not specified --><!-- <AssemblyFullPath>E:\foo\bar.dll</AssemblyFullPath> --><!-- This is optional, Will be used only if commandline parameter will not be specified --><DefaultConfigurationParameter>FooBar</DefaultConfigurationParameter></IntegratorClientConfiguration><IntegratorClientConfiguration><ConfigurationIdentity>ClientConfigComposites</ConfigurationIdentity><ClientInterfaceType>Console</ClientInterfaceType><!-- Defaults to MachineName + PID --><ClientFriendlyName>DummyTestingClientA</ClientFriendlyName><!-- NullGUIClient --><ClientTypeName>Kreslik.Integrator.BusinesLayerDriver.CompositeOrdersTest</ClientTypeName><!-- This is optional. It is ignored if AssemblyFullPath is used, otherwise only the currently executing assembly is probed --><AssemblyNameWithoutExtension>Kreslik.Integrator.BusinesLayerDriver</AssemblyNameWithoutExtension><!-- This is optional, Standard assembly probing will be used if not specified --><!-- <AssemblyFullPath>E:\foo\bar.dll</AssemblyFullPath> --><!-- This is optional, Will be used only if commandline parameter will not be specified --><DefaultConfigurationParameter>FooBar</DefaultConfigurationParameter></IntegratorClientConfiguration></IntegratorClientConfigurations></IntegratorClientHostSettings>', CAST(0x0000A2A7007F1AA7 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (9, N'Kreslik.Integrator.IntegratorWatchDogSvc.exe', NULL, 1, N'<WatchdogSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceDiagnosticsEndpoints><IntegratorServiceDiagnosticsEndpoint><Endpoint>192.168.75.67</Endpoint><Port>9000</Port></IntegratorServiceDiagnosticsEndpoint><IntegratorServiceDiagnosticsEndpoint><Endpoint>192.168.75.68</Endpoint><Port>9000</Port></IntegratorServiceDiagnosticsEndpoint><IntegratorServiceDiagnosticsEndpoint><Endpoint>192.168.75.69</Endpoint><Port>9000</Port></IntegratorServiceDiagnosticsEndpoint></IntegratorServiceDiagnosticsEndpoints><IntegratorNotRunnigSendFirstEmailAfter_Minutes>1</IntegratorNotRunnigSendFirstEmailAfter_Minutes><IntegratorNotRunnigResendEmailAfter_Hours>3</IntegratorNotRunnigResendEmailAfter_Hours><EmailRecipients>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</EmailRecipients></WatchdogSettings>', CAST(0x0000A29600D91BF8 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (10, N'Kreslik.Integrator.MessageBus.dll', NULL, 1, N'<MessageBusSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceHost>localhost</IntegratorServiceHost><IntegratorServicePort>9000</IntegratorServicePort><IntegratorServicePipeName>IntegratorService</IntegratorServicePipeName><MaximumAllowedDisconnectedInterval_Seconds>120</MaximumAllowedDisconnectedInterval_Seconds><DisconnectedSessionReconnectInterval_Seconds>5</DisconnectedSessionReconnectInterval_Seconds><SessionHealthCheckInterval_Seconds>10</SessionHealthCheckInterval_Seconds><!--<PriceStreamTransferMode>FastAndUnreliable</PriceStreamTransferMode>--><PriceStreamTransferMode>Reliable</PriceStreamTransferMode><PerSymbolPriceThrottlingInterval_Milliseconds>300</PerSymbolPriceThrottlingInterval_Milliseconds><ForceConnectRemotingClientOnContractVersionMismatch>false</ForceConnectRemotingClientOnContractVersionMismatch></MessageBusSettings>', CAST(0x0000A29600D91C08 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (11, N'Kreslik.Integrator.QuickItchN.dll', NULL, 1, N'<QuickItchNSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><HtaSessionSettings><Host>209.191.250.210</Host><Port>9012</Port><Username>KGT2</Username><Password>4nSAZQnp</Password><HeartBeatInterval_Seconds>14</HeartBeatInterval_Seconds></HtaSessionSettings><HtfSessionSettings><Host>209.191.250.210</Host><Port>9012</Port><Username>KGT</Username><Password>31x23UMR</Password><HeartBeatInterval_Seconds>14</HeartBeatInterval_Seconds></HtfSessionSettings></QuickItchNSettings>', CAST(0x0000A29600D91C19 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (12, N'Kreslik.Integrator.SessionManagement.dll', N'Trading Instance (sessions for trading)', 1, N'<SessionsSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><FIXChannel_CRS><ClientId>fix_kgt</ClientId><Account>RABOKGT</Account></FIXChannel_CRS><FIXChannel_UBS><Username>btobxprd0462</Username><Password>Vk+Mx#ZtZ18OCl=p7opjqvDv9pzwrU</Password><PartyId>KGTGURABO</PartyId></FIXChannel_UBS><FIXChannel_CTI><Password>citifxprd</Password><Account>105084161</Account></FIXChannel_CTI><FIXChannel_MGS><OnBehalfOfCompId>KGT</OnBehalfOfCompId><Account>GUKGTRABO</Account></FIXChannel_MGS><FIXChannel_RBS><Account>142991</Account></FIXChannel_RBS><FIXChannel_JPM><Password>Xie1ue2i</Password><PasswordLength>8</PasswordLength><Account>MDAPI KGT</Account></FIXChannel_JPM><FIXChannel_BNP><Password>kgt123</Password><Account>RABOUTRKGTFX</Account></FIXChannel_BNP><FIXChannel_CZB><Account>RABOKGTUTR</Account></FIXChannel_CZB><FIXChannel_SOC><OnBehalfOfCompID>KGT</OnBehalfOfCompID></FIXChannel_SOC><FIXChannel_NOM><OnBehalfOfCompID>KGT</OnBehalfOfCompID><SenderSubId>kgtuser</SenderSubId><Account>KGT</Account></FIXChannel_NOM><FIXChannel_HTA><SenderSubId>KGT2_1</SenderSubId><Username>KGT2_1</Username><Password>Nr7MjRLN</Password></FIXChannel_HTA><FIXChannel_HTF><SenderSubId>KGT_1</SenderSubId><Username>KGT_1</Username><Password>dKUK1VGC</Password></FIXChannel_HTF><UnexpectedMessagesTreating><EnableThreasholdingOfUnexpectedMessages>true</EnableThreasholdingOfUnexpectedMessages><MaximumAllowedInstancesPerTimeInterval>30</MaximumAllowedInstancesPerTimeInterval><TimeIntervalToCheck_Minutes>30</TimeIntervalToCheck_Minutes></UnexpectedMessagesTreating><MarketDataSession><MaxAutoResubscribeCount>4</MaxAutoResubscribeCount><RetryIntervalsSequence_Seconds>1</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>10</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>60</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>1800</RetryIntervalsSequence_Seconds><UnconfirmedSubscriptionTimeout_Seconds>180</UnconfirmedSubscriptionTimeout_Seconds><PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog></MarketDataSession><OrderFlowSession><UnconfirmedOrderTimeout_Seconds>10</UnconfirmedOrderTimeout_Seconds><PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog></OrderFlowSession><SubscriptionSettingsRaw><ResubscribeOnReconnect>true</ResubscribeOnReconnect><GlobalSubscriptionSettings><Symbols><SymbolSetting><ShortName>ALL</ShortName><Size>500000</Size><SettingAction>Add</SettingAction></SymbolSetting><SymbolSetting><ShortName>NOKSEK</ShortName><Size>2000000</Size><SettingAction>Add</SettingAction></SymbolSetting></Symbols></GlobalSubscriptionSettings><CounterpartsSubscriptionSettings><Counterparts><CounterpartSetting><ShortName>CRS</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>MGS</ShortName><SettingAction>Add</SettingAction><Symbols><SymbolSetting><ShortName>ALL</ShortName><Size>1000000</Size><SettingAction>Add</SettingAction></SymbolSetting><SymbolSetting><ShortName>NOKSEK</ShortName><Size>5000000</Size><SettingAction>Add</SettingAction></SymbolSetting><SymbolSetting><ShortName>ZARJPY</ShortName><Size>5000000</Size><SettingAction>Add</SettingAction></SymbolSetting></Symbols></CounterpartSetting><CounterpartSetting><ShortName>CTI</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>UBS</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>GLS</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>HTA</ShortName><SettingAction>Add</SettingAction></CounterpartSetting></Counterparts></CounterpartsSubscriptionSettings></SubscriptionSettingsRaw></SessionsSettings>', CAST(0x0000A29600D91C36 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (13, N'Kreslik.Integrator.SessionManagement.dll', N'NONTrading Instance (sessions for DC only)', 1, N'<SessionsSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><FIXChannel_CRS><ClientId>fix_kgt</ClientId><Account>RABOKGT</Account></FIXChannel_CRS><FIXChannel_UBS><Username>btobxprd0462</Username><Password>Vk+Mx#ZtZ18OCl=p7opjqvDv9pzwrU</Password><PartyId>KGTGURABO</PartyId></FIXChannel_UBS><FIXChannel_CTI><Password>citifxprd</Password><Account>105084161</Account></FIXChannel_CTI><FIXChannel_MGS><OnBehalfOfCompId>KGT</OnBehalfOfCompId><Account>GUKGTRABO</Account></FIXChannel_MGS><FIXChannel_RBS><Account>142991</Account></FIXChannel_RBS><FIXChannel_JPM><Password>Xie1ue2i</Password><PasswordLength>8</PasswordLength><Account>MDAPI KGT</Account></FIXChannel_JPM><FIXChannel_BNP><Password>kgt123</Password><Account>RABOUTRKGTFX</Account></FIXChannel_BNP><FIXChannel_CZB><Account>RABOKGTUTR</Account></FIXChannel_CZB><FIXChannel_SOC><OnBehalfOfCompID>KGT</OnBehalfOfCompID></FIXChannel_SOC><FIXChannel_NOM><OnBehalfOfCompID>KGT</OnBehalfOfCompID><SenderSubId>kgtuser</SenderSubId><Account>KGT</Account></FIXChannel_NOM><FIXChannel_HTA><SenderSubId>KGT2_1</SenderSubId><Username>KGT2_1</Username><Password>Nr7MjRLN</Password></FIXChannel_HTA><FIXChannel_HTF><SenderSubId>KGT_1</SenderSubId><Username>KGT_1</Username><Password>dKUK1VGC</Password></FIXChannel_HTF><UnexpectedMessagesTreating><EnableThreasholdingOfUnexpectedMessages>true</EnableThreasholdingOfUnexpectedMessages><MaximumAllowedInstancesPerTimeInterval>30</MaximumAllowedInstancesPerTimeInterval><TimeIntervalToCheck_Minutes>30</TimeIntervalToCheck_Minutes></UnexpectedMessagesTreating><MarketDataSession><MaxAutoResubscribeCount>4</MaxAutoResubscribeCount><RetryIntervalsSequence_Seconds>1</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>10</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>60</RetryIntervalsSequence_Seconds><RetryIntervalsSequence_Seconds>1800</RetryIntervalsSequence_Seconds><UnconfirmedSubscriptionTimeout_Seconds>180</UnconfirmedSubscriptionTimeout_Seconds><PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog></MarketDataSession><OrderFlowSession><UnconfirmedOrderTimeout_Seconds>10</UnconfirmedOrderTimeout_Seconds><PutErrorsInLessImportantLog>true</PutErrorsInLessImportantLog></OrderFlowSession><SubscriptionSettingsRaw><ResubscribeOnReconnect>true</ResubscribeOnReconnect><GlobalSubscriptionSettings><Symbols><SymbolSetting><ShortName>ALL</ShortName><Size>500000</Size><SettingAction>Add</SettingAction></SymbolSetting><SymbolSetting><ShortName>NOKSEK</ShortName><Size>2000000</Size><SettingAction>Add</SettingAction></SymbolSetting></Symbols></GlobalSubscriptionSettings><CounterpartsSubscriptionSettings><Counterparts><CounterpartSetting><ShortName>BNP</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>BOA</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>BRX</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>CZB</ShortName><SettingAction>Add</SettingAction><Symbols><SymbolSetting><ShortName>USDILS</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>USDSKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>EURSKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>CADDKK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>EURCNH</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>GBPCZK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>NZDNOK</ShortName><SettingAction>Remove</SettingAction></SymbolSetting><SymbolSetting><ShortName>USDCNH</ShortName><SettingAction>Remove</SettingAction></SymbolSetting></Symbols></CounterpartSetting><CounterpartSetting><ShortName>HSB</ShortName><SettingAction>Add</SettingAction><SenderSubId>KGT</SenderSubId></CounterpartSetting><CounterpartSetting><ShortName>DBK</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>NOM</ShortName><SettingAction>Add</SettingAction><Symbols><SymbolSetting><ShortName>USDILS</ShortName><SettingAction>Remove</SettingAction></SymbolSetting></Symbols></CounterpartSetting><CounterpartSetting><ShortName>RBS</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>SOC</ShortName><SettingAction>Add</SettingAction><Symbols><SymbolSetting><ShortName>ALL</ShortName><Size>1000000</Size><SettingAction>Add</SettingAction></SymbolSetting><SymbolSetting><ShortName>NOKSEK</ShortName><Size>2000000</Size><SettingAction>Add</SettingAction></SymbolSetting></Symbols></CounterpartSetting><CounterpartSetting><ShortName>JPM</ShortName><SettingAction>Add</SettingAction></CounterpartSetting><CounterpartSetting><ShortName>HTF</ShortName><SettingAction>Add</SettingAction></CounterpartSetting></Counterparts></CounterpartsSubscriptionSettings></SubscriptionSettingsRaw></SessionsSettings>', CAST(0x0000A29600D91C59 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (19, N'Kreslik.Integrator.MessageBus.dll', NULL, 1, N'<MessageBusSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceHost>37.46.6.119</IntegratorServiceHost><IntegratorServicePort>9000</IntegratorServicePort><IntegratorServicePipeName>IntegratorService</IntegratorServicePipeName><MaximumAllowedDisconnectedInterval_Seconds>120</MaximumAllowedDisconnectedInterval_Seconds><DisconnectedSessionReconnectInterval_Seconds>5</DisconnectedSessionReconnectInterval_Seconds><SessionHealthCheckInterval_Seconds>10</SessionHealthCheckInterval_Seconds><PriceStreamTransferMode>Reliable</PriceStreamTransferMode><ForceConnectRemotingClientOnContractVersionMismatch>true</ForceConnectRemotingClientOnContractVersionMismatch></MessageBusSettings>', CAST(0x0000A297014E3ECC AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (23, N'Kreslik.Integrator.IntegratorClientHost.exe', N'M_ Client (non-trading) - Charts feeder', 2, N'<IntegratorClientHostSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><DefaultIntegratorClientCofigurationIdentity>IntegratorToNeotickerDdeFeeder</DefaultIntegratorClientCofigurationIdentity><IntegratorClientConfigurations><IntegratorClientConfiguration><ConfigurationIdentity>IntegratorToNeotickerDdeFeeder</ConfigurationIdentity><ClientInterfaceType>Form</ClientInterfaceType><ClientFriendlyName>DdeFeeder</ClientFriendlyName><ClientTypeName>FxiToNeoDde.FxiToNeoDdeFeeder</ClientTypeName><AssemblyNameWithoutExtension>IntegratorToNeotickerDdeFeeder</AssemblyNameWithoutExtension></IntegratorClientConfiguration></IntegratorClientConfigurations></IntegratorClientHostSettings>', CAST(0x0000A298009B4600 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (24, N'Kreslik.Integrator.DAL.dll', N'Integrator DAL', 2, N'<DALSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><KillSwitchPollIntervalSeconds>3</KillSwitchPollIntervalSeconds><KillSwitchWatchDogIntervalSeconds>40</KillSwitchWatchDogIntervalSeconds><IntegratorSwitchName>KGTIntegratorKillSwitch</IntegratorSwitchName><CommandDistributorPollIntervalSeconds>3</CommandDistributorPollIntervalSeconds><DataCollection><DataProviderId>22</DataProviderId><SourceFileId>932696</SourceFileId><MaxBcpBatchSize>10000</MaxBcpBatchSize><MaxInMemoryColectionSize>1000000</MaxInMemoryColectionSize><MaxIntervalBetweenUploads_Seconds>10</MaxIntervalBetweenUploads_Seconds><MaxUploadTimeAfterWhichSerializationStarts_Seconds>5</MaxUploadTimeAfterWhichSerializationStarts_Seconds><MaxProcessingTimeAfterShutdownSignalled_Seconds>30</MaxProcessingTimeAfterShutdownSignalled_Seconds><WaitIntervalBetweenBcpRetries_Seconds>2</WaitIntervalBetweenBcpRetries_Seconds><WaitIntervalBeforeShutdownStartsSerialization_Seconds>4</WaitIntervalBeforeShutdownStartsSerialization_Seconds></DataCollection></DALSettings>', CAST(0x0000A2A70135EB40 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (25, N'Kreslik.Integrator.DAL_IntegratorConnection', N'Integrator DB connection (public)', 2, N'<DALSettings_IntegratorConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ConnectionString>Data Source=37.46.6.116;Initial Catalog=Integrator;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString></DALSettings_IntegratorConnection>', CAST(0x0000A2A70135EB52 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (26, N'Kreslik.Integrator.DAL_IntegratorConnection', N'Integrator DB connection (private)', 2, N'<DALSettings_IntegratorConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ConnectionString>Data Source=192.168.75.66;Initial Catalog=Integrator;User ID=IntegratorServiceAccount;Password=d2xm*OFc3</ConnectionString></DALSettings_IntegratorConnection>', CAST(0x0000A2A70135EB62 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (27, N'Kreslik.Integrator.DAL_DataCollectionConnection', N'Integrator DC DB connection (public)', 2, N'<DALSettings_DataCollectionConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ConnectionString>Data Source=37.46.6.116;Initial Catalog=FXtickDB;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</ConnectionString></DALSettings_DataCollectionConnection>', CAST(0x0000A2A70135EB71 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (28, N'Kreslik.Integrator.DAL_DataCollectionConnection', N'Integrator DC DB connection (private)', 2, N'<DALSettings_DataCollectionConnection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ConnectionString>Data Source=192.168.75.66;Initial Catalog=FXtickDB;User ID=IntegratorServiceAccount;Password=d2xm*OFc3;Connection Timeout=999</ConnectionString></DALSettings_DataCollectionConnection>', CAST(0x0000A2A70135EB81 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (29, N'Kreslik.Integrator.DAL_DataCollectionEnabling', N'Integrator DC enabling (enabled)', 2, N'<DALSettings_DataCollectionEnabling xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><EnableToBDataCollection>true</EnableToBDataCollection><EnableMarketDataCollection>true</EnableMarketDataCollection></DALSettings_DataCollectionEnabling>', CAST(0x0000A2A70135EB96 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (30, N'Kreslik.Integrator.DAL_DataCollectionEnabling', N'Integrator DC enabling (ToB collection disabled)', 2, N'<DALSettings_DataCollectionEnabling xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><EnableToBDataCollection>false</EnableToBDataCollection><EnableMarketDataCollection>true</EnableMarketDataCollection></DALSettings_DataCollectionEnabling>', CAST(0x0000A2A70135EBA6 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (31, N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo', N'Message Bus', 2, N'<MessageBusSettings_IntegratorEndpointConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceHost>localhost</IntegratorServiceHost><IntegratorServicePort>9000</IntegratorServicePort></MessageBusSettings_IntegratorEndpointConnectionInfo>', CAST(0x0000A2A70135EBB4 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (32, N'Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo', N'Default pipe name', 2, N'<MessageBusSettings_ClientConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServicePipeName>IntegratorService</IntegratorServicePipeName></MessageBusSettings_ClientConnectionInfo>', CAST(0x0000A2A70135EBC4 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (33, N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping', N'Reliable connection', 2, N'<MessageBusSettings_ConnectionKeeping xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><MaximumAllowedDisconnectedInterval_Seconds>120</MaximumAllowedDisconnectedInterval_Seconds><DisconnectedSessionReconnectInterval_Seconds>5</DisconnectedSessionReconnectInterval_Seconds><SessionHealthCheckInterval_Seconds>10</SessionHealthCheckInterval_Seconds></MessageBusSettings_ConnectionKeeping>', CAST(0x0000A2A70135EBD4 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (34, N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior', N'Reliable price transfer', 2, N'<MessageBusSettings_PriceTransferBehavior xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><PriceStreamTransferMode>Reliable</PriceStreamTransferMode><PerSymbolPriceThrottlingInterval_Milliseconds>300</PerSymbolPriceThrottlingInterval_Milliseconds></MessageBusSettings_PriceTransferBehavior>', CAST(0x0000A2A70135EBE3 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (35, N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling', N'Contract mismatch NOT allowed', 2, N'<MessageBusSettings_ContractMismatchHandling xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ForceConnectRemotingClientOnContractVersionMismatch>false</ForceConnectRemotingClientOnContractVersionMismatch></MessageBusSettings_ContractMismatchHandling>', CAST(0x0000A2A70135EBF2 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (36, N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo', N'Instance_A (trading) PrivateIP', 2, N'<MessageBusSettings_IntegratorEndpointConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceHost>192.168.75.69</IntegratorServiceHost><IntegratorServicePort>9000</IntegratorServicePort></MessageBusSettings_IntegratorEndpointConnectionInfo>', CAST(0x0000A2A70143F529 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (37, N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo', N'Instance_A (trading) PublicIP', 2, N'<MessageBusSettings_IntegratorEndpointConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceHost>37.46.6.119</IntegratorServiceHost><IntegratorServicePort>9000</IntegratorServicePort></MessageBusSettings_IntegratorEndpointConnectionInfo>', CAST(0x0000A2A70144223A AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (38, N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo', N'Message Bus - NontradingInstanceA - public IP', 2, N'<MessageBusSettings_IntegratorEndpointConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceHost>37.46.6.117</IntegratorServiceHost><IntegratorServicePort>9000</IntegratorServicePort></MessageBusSettings_IntegratorEndpointConnectionInfo>', CAST(0x0000A2A70148738A AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (39, N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo', N'Message Bus - NontradingInstanceA - private IP', 2, N'<MessageBusSettings_IntegratorEndpointConnectionInfo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IntegratorServiceHost>192.168.75.67</IntegratorServiceHost><IntegratorServicePort>9000</IntegratorServicePort></MessageBusSettings_IntegratorEndpointConnectionInfo>', CAST(0x0000A2A701488405 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (40, N'Kreslik.Integrator.IntegratorClientHost.exe', N'M_ Client (trading) - MarkupSystemHotspot02', 2, N'<IntegratorClientHostSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><DefaultIntegratorClientCofigurationIdentity>MarkupSystemHotspot02</DefaultIntegratorClientCofigurationIdentity><IntegratorClientConfigurations><IntegratorClientConfiguration><ConfigurationIdentity>MarkupSystemHotspot02</ConfigurationIdentity><ClientInterfaceType>Form</ClientInterfaceType><ClientFriendlyName>MarkupSystemHotspot02</ClientFriendlyName><ClientTypeName>Kreslik.MarkupSystemHotspot02.IntegratorHost</ClientTypeName><AssemblyNameWithoutExtension>MarkupSystemHotspot02</AssemblyNameWithoutExtension></IntegratorClientConfiguration></IntegratorClientConfigurations></IntegratorClientHostSettings>', CAST(0x0000A2A70155C430 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (42, N'Kreslik.Integrator.IntegratorClientHost.exe', N'M_ Client (trading) - MeanRev001', 2, N'<IntegratorClientHostSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><DefaultIntegratorClientCofigurationIdentity>MeanRev001</DefaultIntegratorClientCofigurationIdentity><IntegratorClientConfigurations><IntegratorClientConfiguration><ConfigurationIdentity>MeanRev001</ConfigurationIdentity><ClientInterfaceType>Form</ClientInterfaceType><ClientFriendlyName>MeanRevTS001</ClientFriendlyName><ClientTypeName>SnakeFxi.TsCore</ClientTypeName><AssemblyNameWithoutExtension>SnakeFxi</AssemblyNameWithoutExtension></IntegratorClientConfiguration></IntegratorClientConfigurations></IntegratorClientHostSettings>', CAST(0x0000A2A7015AB5D0 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (43, N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling', N'Contract mismatch IS allowed', 2, N'<MessageBusSettings_ContractMismatchHandling xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><ForceConnectRemotingClientOnContractVersionMismatch>true</ForceConnectRemotingClientOnContractVersionMismatch></MessageBusSettings_ContractMismatchHandling>', CAST(0x0000A2A7015AB5D0 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (45, N'Kreslik.Integrator.IntegratorClientHost.exe', N'M_ Client (non-trading) - Dashboard', 2, N'<IntegratorClientHostSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><DefaultIntegratorClientCofigurationIdentity>Dashboard</DefaultIntegratorClientCofigurationIdentity><IntegratorClientConfigurations><IntegratorClientConfiguration><ConfigurationIdentity>Dashboard</ConfigurationIdentity><ClientInterfaceType>Form</ClientInterfaceType><ClientFriendlyName>Dashboard01</ClientFriendlyName><ClientTypeName>FxiClusterDashboard.Dashboard</ClientTypeName><AssemblyNameWithoutExtension>Dashboard</AssemblyNameWithoutExtension></IntegratorClientConfiguration></IntegratorClientConfigurations></IntegratorClientHostSettings>', CAST(0x0000A2A70162ACE0 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (46, N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior', N'M_ Fast and unreliable price transfer', 2, N'<MessageBusSettings_PriceTransferBehavior xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><PriceStreamTransferMode>FastAndUnreliable</PriceStreamTransferMode><PerSymbolPriceThrottlingInterval_Milliseconds>500</PerSymbolPriceThrottlingInterval_Milliseconds></MessageBusSettings_PriceTransferBehavior>', CAST(0x0000A2A7016452C0 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (47, N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping', N'M_ Unreliable connection', 2, N'<MessageBusSettings_ConnectionKeeping xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><MaximumAllowedDisconnectedInterval_Seconds>99999</MaximumAllowedDisconnectedInterval_Seconds><DisconnectedSessionReconnectInterval_Seconds>5</DisconnectedSessionReconnectInterval_Seconds><SessionHealthCheckInterval_Seconds>10</SessionHealthCheckInterval_Seconds></MessageBusSettings_ConnectionKeeping>', CAST(0x0000A2A7016711E0 AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (50, N'Kreslik.Integrator.DiagnosticsDashboard.exe', N'Instance_A_Trading -- Active pairs only', 1, N'<DiagnosticsDashboardSettings xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><RefreshTimeout_Miliseconds>100</RefreshTimeout_Miliseconds><FirstSpreadAgeLimit_Seconds>10</FirstSpreadAgeLimit_Seconds><SecondSpreadAgeLimit_Seconds>60</SecondSpreadAgeLimit_Seconds><ThirdSpreadAgeLimit_Seconds>1200</ThirdSpreadAgeLimit_Seconds><CutOffSpreadAgeLimit_Hours>24</CutOffSpreadAgeLimit_Hours><EnabledSymbols><Symbol>AUD/USD</Symbol><Symbol>EUR/NOK</Symbol><Symbol>EUR/SEK</Symbol><Symbol>NZD/USD</Symbol><Symbol>USD/SEK</Symbol></EnabledSymbols><EnabledCounterparties><Counterparty>CRS</Counterparty><Counterparty>CTI</Counterparty><Counterparty>GLS</Counterparty><Counterparty>HTA</Counterparty><Counterparty>MGS</Counterparty><Counterparty>UBS</Counterparty></EnabledCounterparties></DiagnosticsDashboardSettings>', CAST(0x0000A2AE012D9E1E AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (51, N'Kreslik.Integrator.DiagnosticsDashboard.exe', N'Instance_G_NonTrading -- All non-trading c-parties', 1, N'<DiagnosticsDashboardSettings xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><RefreshTimeout_Miliseconds>500</RefreshTimeout_Miliseconds><FirstSpreadAgeLimit_Seconds>10</FirstSpreadAgeLimit_Seconds><SecondSpreadAgeLimit_Seconds>60</SecondSpreadAgeLimit_Seconds><ThirdSpreadAgeLimit_Seconds>1200</ThirdSpreadAgeLimit_Seconds><CutOffSpreadAgeLimit_Hours>24</CutOffSpreadAgeLimit_Hours><EnabledSymbols><Symbol>AUD/CAD</Symbol><Symbol>AUD/CHF</Symbol><Symbol>AUD/JPY</Symbol><Symbol>AUD/NZD</Symbol><Symbol>AUD/USD</Symbol><Symbol>CAD/JPY</Symbol><Symbol>CHF/JPY</Symbol><Symbol>EUR/AUD</Symbol><Symbol>EUR/CAD</Symbol><Symbol>EUR/CHF</Symbol><Symbol>EUR/GBP</Symbol><Symbol>EUR/JPY</Symbol><Symbol>EUR/NOK</Symbol><Symbol>EUR/SEK</Symbol><Symbol>EUR/USD</Symbol><Symbol>GBP/AUD</Symbol><Symbol>GBP/CAD</Symbol><Symbol>GBP/JPY</Symbol><Symbol>GBP/USD</Symbol><Symbol>NOK/SEK</Symbol><Symbol>NZD/USD</Symbol><Symbol>USD/CAD</Symbol><Symbol>USD/CHF</Symbol><Symbol>USD/JPY</Symbol><Symbol>USD/SEK</Symbol></EnabledSymbols><EnabledCounterparties><Counterparty>BNP</Counterparty><Counterparty>BOA</Counterparty><Counterparty>BRX</Counterparty><Counterparty>CZB</Counterparty><Counterparty>DBK</Counterparty><Counterparty>GLS</Counterparty><Counterparty>HSB</Counterparty><Counterparty>HTF</Counterparty><Counterparty>JPM</Counterparty><Counterparty>NOM</Counterparty><Counterparty>RBS</Counterparty><Counterparty>SOC</Counterparty></EnabledCounterparties></DiagnosticsDashboardSettings>', CAST(0x0000A2B400C0B5BC AS DateTime), 0)
GO
INSERT [dbo].[Settings] ([Id], [Name], [FriendlyName], [SettingsVersion], [SettingsXml], [LastUpdated], [IsOnlineMonitored]) VALUES (52, N'Kreslik.Integrator.IntegratorClientHost.exe', N'M_ Client (non-trading) - IntegratorOnlineView', 2, N'<IntegratorClientHostSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><DefaultIntegratorClientCofigurationIdentity>IntegratorOnlineView</DefaultIntegratorClientCofigurationIdentity><IntegratorClientConfigurations><IntegratorClientConfiguration><ConfigurationIdentity>IntegratorOnlineView</ConfigurationIdentity><ClientInterfaceType>Form</ClientInterfaceType><ClientFriendlyName>IntegratorOnlineView</ClientFriendlyName><ClientTypeName>Kreslik.IntegratorOnlineView.IntegratorHost</ClientTypeName><AssemblyNameWithoutExtension>IntegratorOnlineView</AssemblyNameWithoutExtension></IntegratorClientConfiguration></IntegratorClientConfigurations></IntegratorClientHostSettings>', CAST(0x0000A2B200FA9470 AS DateTime), 0)
GO
SET IDENTITY_INSERT [dbo].[Settings] OFF
GO
SET IDENTITY_INSERT [dbo].[IntegratorEnvironmentType] ON 

GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (1, N'IntegratorService', N'Environment for IntegratorService')
GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (2, N'IntegratorService_old', N'Environment for IntegratorService (previous version)')
GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (3, N'Client', N'Environment for Integrator Client started via IntegratorServiceHost')
GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (4, N'Dashboard', N'Environment for DiagnosticsDashboard (AKA Counterparty Monitor)')
GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (5, N'CommandsConsole', N'Environment for ConsoleRunner started solely for purposes of running commands')
GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (6, N'IntegratorWatchdog', N'Environment for IntegratorWatchdog windows service')
GO
INSERT [dbo].[IntegratorEnvironmentType] ([Id], [Name], [Description]) VALUES (7, N'Splitter', N'Environment for Splitter')
GO
SET IDENTITY_INSERT [dbo].[IntegratorEnvironmentType] OFF
GO
SET IDENTITY_INSERT [dbo].[IntegratorEnvironment] ON 

GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (11, N'PRO_Dashboard_localhost', NULL, 4)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (1, N'PRO_Instance_A_Trading', NULL, 1)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (2, N'PRO_Instance_G_NonTrading', NULL, 1)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (12, N'PRO_M_BktDashboard_Client_PrivateIP', NULL, 3)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (13, N'PRO_M_BktDashboard_Splitter_PrivateIP', NULL, 7)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (6, N'PRO_M_Charts_Client_PublicIP', NULL, 3)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (5, N'PRO_M_Charts_Splitter_PublicIP', NULL, 7)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (18, N'PRO_M_C-monitor_Instance_A_Trading_PrivateIP', NULL, 4)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (19, N'PRO_M_C-monitor_Instance_G_NonTrading_PrivateIP', NULL, 4)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (14, N'PRO_M_HotspotSystem02_Client_PrivateIP', NULL, 3)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (22, N'PRO_M_HotspotSystem02_Client_PublicIP', NULL, 3)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (15, N'PRO_M_HotspotSystem02_Splitter_PrivateIP', NULL, 7)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (20, N'PRO_M_HotspotSystem02_Splitter_PublicIP', NULL, 7)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (24, N'PRO_M_IntegratorOnlineView_Client_PrivateIP', NULL, 3)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (23, N'PRO_M_IntegratorOnlineView_Client_PublicIP', NULL, 3)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (16, N'PRO_M_MeanRev001_Client_PrivateIP', NULL, 3)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (17, N'PRO_M_MeanRev001_Splitter_PrivateIP', NULL, 7)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (26, N'PRO_M_Splitter_PrivateIP', NULL, 7)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (25, N'PRO_M_Splitter_PublicIP', NULL, 7)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (8, N'PRO_Splitter_localhost', NULL, 7)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (10, N'PRO_Splitter_subnet_NontradingInstance_A', NULL, 7)
GO
INSERT [dbo].[IntegratorEnvironment] ([Id], [Name], [Description], [IntegratorEnvironmentTypeId]) VALUES (9, N'PRO_Splitter_subnet_TradingInstance_A', NULL, 7)
GO
SET IDENTITY_INSERT [dbo].[IntegratorEnvironment] OFF
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 1)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 2)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 3)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 11)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 12)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 24)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 26)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 28)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 29)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (1, 35)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 1)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 2)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 3)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 11)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 13)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 24)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 26)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 28)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 30)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (2, 35)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (5, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (5, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (5, 37)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (5, 43)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (5, 46)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (5, 47)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (6, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (6, 23)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (6, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (6, 37)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (6, 43)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (6, 46)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (6, 47)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (11, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (11, 7)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (11, 26)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (11, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 36)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 43)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (12, 45)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (13, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (13, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (13, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (13, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (13, 36)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (13, 43)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (14, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (14, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (14, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (14, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (14, 35)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (14, 36)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (14, 40)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (15, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (15, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (15, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (15, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (15, 35)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (15, 36)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (16, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (16, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (16, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (16, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (16, 35)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (16, 36)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (16, 42)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (17, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (17, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (17, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (17, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (17, 35)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (17, 36)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (18, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (18, 26)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (18, 36)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (18, 50)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (19, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (19, 26)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (19, 39)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (19, 51)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (20, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (20, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (20, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (20, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (20, 35)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (20, 37)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (22, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (22, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (22, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (22, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (22, 35)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (22, 37)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (22, 40)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (23, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (23, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (23, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (23, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (23, 35)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (23, 37)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (23, 52)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (24, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (24, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (24, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (24, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (24, 35)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (24, 36)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (24, 52)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (25, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (25, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (25, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (25, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (25, 35)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (25, 37)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (26, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (26, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (26, 33)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (26, 34)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (26, 35)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsMap] ([IntegratorEnvironmentId], [SettingsId]) VALUES (26, 36)
GO
SET IDENTITY_INSERT [dbo].[Settings_FIX] ON 

GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (1, N'BNP_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


#SSLEnable=Y
#SSLCertificate=BNPClientCertificate_UAT.pfx
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N

SenderCompID=KGTPrice

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_BNP.xml
BeginString=FIX.4.4
TargetCompID=BNPP
StartTime=00:00:00
EndTime=23:59:59
HeartBtInt=30


# stunnel port was 20004 
SocketConnectPort=23581
SocketConnectHost=159.95.18.138', CAST(0x0000A29600DA19FC AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (2, N'BNP_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y


#SSLEnable=Y
#SSLCertificate=BNPClientCertificate_UAT.pfx
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N

SenderCompID=KGTOrder

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_BNP.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=BNPP
StartTime=00:00:00
EndTime=23:59:59
HeartBtInt=30


# stunnel port was 20004 
SocketConnectPort=23582
SocketConnectHost=159.95.18.138', CAST(0x0000A29600DA1A0D AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (3, N'BOA_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT-QTS


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX43_BOA.xml
BeginString=FIX.4.3
TargetCompID=BAML-FIX-FX
#5pm NYK
StartTime=17:06:00
EndTime=17:00:00
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=6371
SocketConnectHost=165.34.236.241', CAST(0x0000A29600DA1A1D AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (4, N'BOA_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT-ORD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX43_BOA.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.3
TargetCompID=BAML-FIX-FX
#5pm NYK
StartTime=17:06:00
EndTime=17:00:00
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=6372
SocketConnectHost=165.34.236.241', CAST(0x0000A29600DA1A2F AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (5, N'BRX_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=60
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT-PRICES


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_BRX.xml
BeginString=FIX.4.2
TargetCompID=BARX-PRICES
#5pm NYK
StartTime=17:00:30
EndTime=16:59:30
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - direct connect:
SocketConnectPort=54417
SocketConnectHost=141.228.140.73

#failover
SocketConnectPort1=54417
SocketConnectHost1=141.228.80.73', CAST(0x0000A29600DA1A41 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (6, N'BRX_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=60
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT-TRADES


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_BRX.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=BARX-TRADES
#5pm NYK
StartTime=17:00:30
EndTime=16:59:30
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - direct connect:
SocketConnectPort=54418
SocketConnectHost=141.228.140.73

#failover
SocketConnectPort1=54418
SocketConnectHost1=141.228.80.73', CAST(0x0000A29600DA1A50 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (7, N'CRS_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=CSFX-KGT-QUO


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_CRS.xml
BeginString=FIX.4.2
TargetCompID=EFX_KGT
#Resets 06:05 - 06:10 Asian/Singapore time
#StartTime=06:10:10
#EndTime=06:04:50
#TimeZone=Singapore Standard Time
#There was a change to CRS session schedule (20:45UTC)
StartTime=20:47:00
EndTime=20:44:00

HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=10341
SocketConnectHost=169.33.48.50', CAST(0x0000A29600DA1A61 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (8, N'CRS_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=CSFX-KGT-ORD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_CRS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=EFX_KGT
#Resets 06:05 - 06:10 Asian/Singapore time
#StartTime=06:10:10
#EndTime=06:04:50
#TimeZone=Singapore Standard Time
#There was a change to CRS session schedule (20:45UTC)
StartTime=20:47:00
EndTime=20:44:00

HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=10341
SocketConnectHost=169.33.48.50
', CAST(0x0000A29600DA1A71 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (9, N'CTI_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

#SSLEnable=Y
#SSLCertificate=
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N


SenderCompID=KGT-QUOTE
TargetSubID=FXSpot

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_CTI.xml
BeginString=FIX.4.4
TargetCompID=CITIFX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 20001
SocketConnectPort=30243
SocketConnectHost=192.193.51.183', CAST(0x0000A29600DA1A80 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (10, N'CTI_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

#SSLEnable=Y
#SSLCertificate=
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N


SenderCompID=KGT-TRADE
TargetSubID=FXSpot

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_CTI.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=CITIFX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 20001
SocketConnectPort=30243
SocketConnectHost=192.193.51.183', CAST(0x0000A29600DA1A91 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (11, N'CZB_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


#SSLEnable=Y
#SSLCertificate=
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N


SenderCompID=KGTINVMD

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_CZB.xml
BeginString=FIX.4.4
TargetCompID=COBAFXMD
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 20003
SocketConnectPort=19001
SocketConnectHost=195.42.239.71

#failover
SocketConnectPort1=19001
SocketConnectHost1=160.82.118.143', CAST(0x0000A29600DA1AA1 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (12, N'CZB_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y


#SSLEnable=Y
#SSLCertificate=
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N


SenderCompID=KGTINVORD

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_CZB.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=COBAFXORD
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 20003
SocketConnectPort=19001
SocketConnectHost=195.42.239.71

#failover
SocketConnectPort1=19001
SocketConnectHost1=160.82.118.143', CAST(0x0000A29600DA1AB1 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (13, N'DBK_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


SSLEnable=N
#SSLCertificate=KGTI.FIX_nopass.pfx
#SSLServerName=KGTI.FIX
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N


SenderCompID=KGTI.FIX

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_DBK.xml
BeginString=FIX.4.4
TargetCompID=ABFX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=8002
SocketConnectHost=160.82.118.241', CAST(0x0000A29600DA1AC3 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (14, N'DBK_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y


SSLEnable=N
#SSLCertificate=DBKClientCertificate2_UAT.pfx
#SSLServerName=UAT.KGTI.FIX2
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N

#SenderCompID=KGTI_P.FIX
SenderCompID=KGTI1_P.FIX

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_DBK.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=ABFX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnel port was 20002
SocketConnectPort=8002
SocketConnectHost=160.82.118.241', CAST(0x0000A29600DA1AD4 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (15, N'GLS_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGTINVEST


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_GLS.xml
BeginString=FIX.4.4
TargetCompID=GSFXPRICES
#5pm NYK
StartTime=17:05:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=12010
SocketConnectHost=75.96.80.214', CAST(0x0000A29600DA1AE3 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (16, N'GLS_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGTINVEST


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_GLS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=GSFXTRADES
#5pm NYK
StartTime=17:05:10
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=12010
SocketConnectHost=75.96.80.214', CAST(0x0000A29600DA1AF2 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (17, N'HSB_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

#SSLEnable=Y
#SSLCertificate=HSBClientCertificate_UAT.pfx
#SSLServerName=fix.hsbc.com
#SSLProtocols=
#SSLCACertificate=HSBCACertificate_UAT.pfx
#SSLValidateCertificates=Y
#SSLValidateCertificatesOID=N


SenderCompID=KGTQLSSTR

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_HSB.xml
BeginString=FIX.4.4
TargetCompID=HSBCUKFIX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnel port was 19997
SocketConnectPort=47018
SocketConnectHost=208.224.251.4

#failover 1
SocketConnectPort1=47018
SocketConnectHost1=208.224.251.5

#failover 2
SocketConnectPort2=47018
SocketConnectHost2=208.224.251.7', CAST(0x0000A29600DA1B03 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (18, N'HSB_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

#SSLEnable=Y
#SSLCertificate=HSBClientCertificate_UAT.pfx
#SSLServerName=fix.hsbc.com
#SSLProtocols=
#SSLCACertificate=HSBCACertificate_UAT.pfx
#SSLValidateCertificates=Y
#SSLValidateCertificatesOID=N

SenderCompID=KGTQLSTRD

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_HSB.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=HSBCUKFIX
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30


# stunnel port was 19997
SocketConnectPort=47018
SocketConnectHost=208.224.251.4

#failover 1
SocketConnectPort1=47018
SocketConnectHost1=208.224.251.5

#failover 2
SocketConnectPort2=47018
SocketConnectHost2=208.224.251.7
', CAST(0x0000A29600DA1B14 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (19, N'HTA_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y
#Hotspot does not want reply of any messages - so no point of storing them
PersistMessages=N

SenderCompID=KGT2

SSLEnable=N
#SSLCertificate=UBSClientCertificate_UAT.pfx
#SSLServerName=fxfixb2bpte1.ibb.ubstest.com
#SSLProtocols=
#SSLCACertificate=UBSCACertificate_UAT.pfx
#SSLValidateCertificates=Y
#SSLValidateCertificatesOID=N



[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_HOT.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
SessionQualifier=HTA
TargetCompID=HSFX-FIX-BRIDGE
#5pm NYK
StartTime=17:02:05
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=8004
SocketConnectHost=209.191.250.180', CAST(0x0000A29600DA1B25 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (20, N'HTF_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y
#Hotspot does not want reply of any messages - so no point of storing them
PersistMessages=N

SenderCompID=KGT

SSLEnable=N
#SSLCertificate=UBSClientCertificate_UAT.pfx
#SSLServerName=fxfixb2bpte1.ibb.ubstest.com
#SSLProtocols=
#SSLCACertificate=UBSCACertificate_UAT.pfx
#SSLValidateCertificates=Y
#SSLValidateCertificatesOID=N



[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_HOT.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
SessionQualifier=HTF
TargetCompID=HSFX-FIX-BRIDGE
#5pm NYK
StartTime=17:02:05
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=8004
SocketConnectHost=209.191.250.180', CAST(0x0000A29600DA1B34 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (21, N'JPM_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=60
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT


SSLEnable=Y
SSLCertificate=JPMClientAndCACertificate_PRD.pfx
#SSLServerName=GCCG - Integration
SSLProtocols=Ssl3
SSLCACertificate=JPMClientAndCACertificate_PRD.pfx
SSLValidateCertificates=N
SSLValidateCertificatesOID=N


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_JPM.xml
BeginString=FIX.4.2
TargetCompID=JPM-MD
StartTime=00:00:00
EndTime=23:59:59
HeartBtInt=30

# stunnel port was 20006
SocketConnectPort=8061
SocketConnectHost=169.116.253.26
#SocketConnectPort=29006
#SocketConnectHost=127.0.0.1', CAST(0x0000A29600DA1B44 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (22, N'JPM_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=60
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT


SSLEnable=Y
SSLCertificate=JPMClientAndCACertificate_PRD.pfx
#SSLServerName=GCCG - Integration
SSLProtocols=Ssl3
SSLCACertificate=JPMClientAndCACertificate_PRD.pfx
SSLValidateCertificates=N
SSLValidateCertificatesOID=N


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_JPM.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=JPM-OM
StartTime=00:00:00
EndTime=23:59:59
HeartBtInt=30

# stunnel port was 20006
SocketConnectPort=8062
SocketConnectHost=169.116.253.26', CAST(0x0000A29600DA1B53 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (23, N'MGS_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT_RATE


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_MGS.xml
BeginString=FIX.4.2
TargetCompID=MSFX
OnBehalfOfCompID=KGT
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=16638
SocketConnectHost=109.74.25.209', CAST(0x0000A29600DA1B63 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (24, N'MGS_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT_ORD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_MGS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=MSFX
OnBehalfOfCompID=KGT
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - dirrect connect:
SocketConnectPort=16638
SocketConnectHost=109.74.25.209', CAST(0x0000A29600DA1B71 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (25, N'NOM_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT_PRICE


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_NOM.xml
BeginString=FIX.4.4
TargetCompID=NOMURAFX
#5pm NYK
StartTime=17:16:00
EndTime=16:59:00
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - direct connect:
SocketConnectPort=40128
SocketConnectHost=10.113.168.7

#failover
SocketConnectPort1=40128
SocketConnectHost1=10.81.168.7', CAST(0x0000A29600DA1B80 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (26, N'NOM_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT_TRADE


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_NOM.xml
BeginString=FIX.4.4
TargetCompID=NOMURAFX
#5pm NYK
StartTime=17:16:00
EndTime=16:59:00
TimeZone=Eastern Standard Time
HeartBtInt=30

# no ssl - direct connect:
SocketConnectPort=40128
SocketConnectHost=10.113.168.7

#failover
SocketConnectPort1=40128
SocketConnectHost1=10.81.168.7', CAST(0x0000A29600DA1B90 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (27, N'RBS_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


#SSLEnable=Y
#SSLCertificate=RBSClientCertificate_UAT.pfx
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N



SenderCompID=kgt.api_MD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_RBS.xml
BeginString=FIX.4.4
TargetCompID=RBS_FX
#5:01pm NYK
StartTime=17:01:59
EndTime=16:59:59
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnel connection port was 20000
SocketConnectPort=4321
SocketConnectHost=147.114.61.254', CAST(0x0000A29600DA1BA8 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (28, N'RBS_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y


#SSLEnable=Y
#SSLCertificate=RBSClientCertificate_UAT.pfx
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
#SSLValidateCertificates=N



SenderCompID=kgt.api_ORD


[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX44_RBS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=RBS_FX
#5:01pm NYK
StartTime=17:01:59
EndTime=16:59:59
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnel connection port was 20000
SocketConnectPort=4322
SocketConnectHost=147.114.61.254', CAST(0x0000A29600DA1BB8 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (29, N'SOC_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT-MKD

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_SOC.xml
BeginString=FIX.4.2
TargetCompID=SOCGEN-MKD
#5pm NYK
StartTime=17:05:10
EndTime=16:59:30
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=9066
SocketConnectHost=184.34.185.129

#failover
SocketConnectPort1=9066
SocketConnectHost1=184.42.12.17', CAST(0x0000A29600DA1BC8 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (30, N'SOC_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT

[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX42_SOC.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.2
TargetCompID=SOCGEN
#5pm NYK
StartTime=17:05:10
EndTime=16:59:30
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=9067
SocketConnectHost=184.34.185.129

#failover
SocketConnectPort1=9067
SocketConnectHost1=184.42.12.17', CAST(0x0000A29600DA1BD8 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (31, N'UBS_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=KGT_MKD

SSLEnable=Y
SSLCertificate=UBSClientCertificate_PRD.pfx
#SSLServerName=fxfixb2bpte1.ibb.ubstest.com
#SSLServerName=btobxprd0462
#SSLProtocols=
SSLCACertificate=UBSCACertificate_PRD.pfx
SSLValidateCertificates=N
SSLValidateCertificatesOID=N



[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX43_UBS.xml
BeginString=FIX.4.3
TargetCompID=UBSFX2B_PRD
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnelport was 19999
SocketConnectPort=2500
SocketConnectHost=139.149.131.5

# failover
SocketConnectPort1=2500
SocketConnectHost1=139.149.11.181', CAST(0x0000A29600DA1BE8 AS DateTime))
GO
INSERT [dbo].[Settings_FIX] ([Id], [Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (32, N'UBS_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=KGT_ORD

SSLEnable=Y
SSLCertificate=UBSClientCertificate_PRD.pfx
#SSLServerName=fxfixb2bpte1.ibb.ubstest.com
#SSLServerName=btobxprd0462
#SSLProtocols=
SSLCACertificate=UBSCACertificate_PRD.pfx
SSLValidateCertificates=N
SSLValidateCertificatesOID=N



[SESSION]
DataDictionary=CounterpartSpecific/FIXdictionaries/FIX43_UBS.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.3
TargetCompID=UBSFX2B_PRD
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30

# stunnelport was 19999
SocketConnectPort=2500
SocketConnectHost=139.149.131.5

# failover
SocketConnectPort1=2500
SocketConnectHost1=139.149.11.181', CAST(0x0000A29600DA1BF8 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Settings_FIX] OFF
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 1)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 2)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 3)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 5)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 6)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 7)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 9)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 10)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 11)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 12)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 13)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 14)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 15)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 16)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 17)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 18)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 19)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 20)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 21)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 22)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 23)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 24)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 25)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 26)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 27)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 28)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 29)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 30)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (1, 32)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 1)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 2)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 3)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 4)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 5)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 6)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 7)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 8)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 9)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 10)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 11)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 12)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 13)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 14)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 15)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 16)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 17)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 18)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 19)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 20)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 21)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 22)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 23)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 24)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 25)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 26)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 27)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 28)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 29)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 30)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 31)
GO
INSERT [dbo].[IntegratorEnvironmentToSettingsFixMap] ([IntegratorEnvironmentId], [SettingsFixId]) VALUES (2, 32)
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (1, N'Kreslik.Integrator.BusinessLayer.dll')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (1, N'Kreslik.Integrator.Common.dll')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (1, N'Kreslik.Integrator.DAL.dll')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (1, N'Kreslik.Integrator.DAL_DataCollectionConnection')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (1, N'Kreslik.Integrator.DAL_DataCollectionEnabling')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (1, N'Kreslik.Integrator.DAL_IntegratorConnection')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (1, N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (1, N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (1, N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (1, N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (1, N'Kreslik.Integrator.QuickItchN.dll')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (1, N'Kreslik.Integrator.RiskManagement.dll')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (1, N'Kreslik.Integrator.SessionManagement.dll')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (3, N'Kreslik.Integrator.Common.dll')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (3, N'Kreslik.Integrator.IntegratorClientHost.exe')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (3, N'Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (3, N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (3, N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (3, N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (4, N'Kreslik.Integrator.Common.dll')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (4, N'Kreslik.Integrator.DAL_IntegratorConnection')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (4, N'Kreslik.Integrator.DiagnosticsDashboard.exe')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (4, N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (5, N'Kreslik.Integrator.Common.dll')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (5, N'Kreslik.Integrator.DAL.dll')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (5, N'Kreslik.Integrator.DAL_IntegratorConnection')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (5, N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (7, N'Kreslik.Integrator.Common.dll')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (7, N'Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (7, N'Kreslik.Integrator.MessageBusSettings_ConnectionKeeping')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (7, N'Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (7, N'Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo')
GO
INSERT [dbo].[SettingsMandatoryInEnvironemntType] ([IntegratorEnvironmentTypeId], [SettingsName]) VALUES (7, N'Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior')
GO
INSERT [dbo].[CertificateFile] ([FileName], [FileContent]) VALUES (N'JPMClientAndCACertificate_PRD.pfx', 0x3082042C020103308203F206092A864886F70D010701A08203E3048203DF308203DB308203D706092A864886F70D010706A08203C8308203C4020100308203BD06092A864886F70D010701301C060A2A864886F70D010C0106300E04088B5A9E99878F36F102020800808203907FB209490FF3F6099CC2FFA2776223386EE20C550794628F31EAEA4F93BF312D7EA9C074A37DF73F1A71A81ED436DF9AAE1949DB2E756282B6A6DE199445C64C4EFCC06C029953E3413D1B8ECA81C35EFC89EF588723B6093E5E6C9F4B3B63312F734D513A5B97048E94B7A02F4BF4141320BA3F11C4116E9E6B4A8AEA0EB6B86AADD285627547BEEDC0870599961D0D51C2A5DE18ED4DF09204834D40AB7689322F4559F8587A97455A8AFBA76C8492CA6F214D33433E3818DCAF8A7129D7850C2DE68B91D27C1077FBD1D477955087E42E85225EFD2E89C1ADBAAED9875AA0CEFCF3B2EC5678EAF24996F9236E5F51816B24AA1552012A3668FF45B35EBCB696DAF9F5344DECB983DDB7594520705289FFB1E3F6B1E87ABA40D29299334C79137D341CE4AA8FFED77707B85DF45A049638BB235B914AF6588BE12640E2C3BF2C529AC0B4C82520DE056CA656B85FF51C6A30397F2181AF078C8640BB7C50FEE7351AC6AFE017B5EF32F451DCAC4B0E2F5A5761F42040149E4D1AB9E7218396C987B4D90C0B1B00ACE6838768E70100C3319AE4E326764A28AF272BF10A39F47A36BB32C5E07868A210426D542EDD9EAD37E8DB385D79D5D0721C8EA4417EAD6044F34B919C6DA3240E6100355F8BD03939314AFD9AB366470CEBF3ECDCCD644087811A63378DD99C21DA715083A46DC36EF2A34D1C2D1039C734DAEB219CBE958A0256B6FE9124295BF9475E4CFEE84085DDDA2F00BE6CFF27FD1E81BAC691A32C9421E3838496571D543CE2170F33D75F2C29467C793E9126B319273FFB007FF630FF4796C4A6B4C4F21986CBA1A3DE47298387582C2372FD56F8D719459FB8A4161A8C6CAF1CD5A226BF305DE2F309655686D795CC5B9EF901752FFE747AFFF95741AFF109031BB1EDEB145A8C49624616C6C684442C2CBB866301E7145D5EA13861C241C995E1ECDB4100A16D5D98FF79751547151DCE7D082F753C10337A93EE63978CCD6430A49C2C4F07B3781EFA2054397D7987C2734190D03F4E80B0531F7A77A9146A3F9730CAB573415BB35AB09D262539C9D669C8D25428F322F055274CB642B0C1F92754EEE0AAE75418C58C7375CA8AB8E480FC7A8D401150E9971308304990E24E0854361BE7927F0D5F11050AE9FD94FF01599E59904481E90FBCD9C21DA89E333F3B2609755BCB1651844735A774474EDC5C195AE3F5DEC1943BFC3144BF034C7F89F1D68B3B8C12795D3F96364D056ADE906E20AB1F890A5A48F5C4FAB00C6C9E2AE8D15B4B2530313021300906052B0E03021A05000414546A499DBC53193EA2E8F08953F0939BED0226D004080D2DCE04882FF5B802020800)
GO
INSERT [dbo].[CertificateFile] ([FileName], [FileContent]) VALUES (N'UBSCACertificate_PRD.pfx', 0x3082049C0201033082046206092A864886F70D010701A08204530482044F3082044B3082044706092A864886F70D010706A0820438308204340201003082042D06092A864886F70D010701301C060A2A864886F70D010C0106300E0408DA420DBB645B85310202080080820400439F89A0144768D43A8F329CBC4FAFD32999F29A3C9386C0667DD4778DC09B76C2AE8CAA92F88715A118069B65C1F1BB2F4273868F221A4686FADD702CA690BA4D3EA3AA03375B04059E2B67EB9BACA98BC5D5C5690B20F284855D54BC1012915336246DA4D5509D9A9D187F3D10874AE65DF7707F9AF92951F0BA7A7726B8B9C66594EE6B64256F87D3AA0FE97BD20560F60EC2479B7F1885AF7CB89799F35E9866D88C17F02D6F440ACB899AC9A21548D8E984283082EA7C047A5F6EF2CF607D97F6C874A959E9B47F43F71E1A215E453BD7BFC210E853599F9793E7C7F203BC8C37C7E3DF762837A2C22A80F72D694BF56E4D2ACC536095F55BBE88F5C5E4E74B2BC14356FDE06FFE12E10469D8370D3DB1D8F9FD14379C5A7E1E9AAF852940AB72821A25322C056E2BFCBBD5B9395F7AAA9A8EE24660B3D3CC350924ADCF2FF415ECD6722B77B99C996613CAB7D7912BF6EE65FFDDD1A632F425AB8ADD92457BB0CEDBEF7621E8CD416DA294BF7454F0D3F51B5EA7FF1756B341887D6FA95A7C341F95FCCC4C829C0B76C5E4C116632A2F95D0613DAEBAE9545C78CEC502662197162885FB2776437EEFBD1A9C5C61A8A2A8CD01E326E3D1263F82832FAA3E0D02CEDC2B29650EFCBE5C8519FF6FAEAC1D8FB460235C0E72DF7F87654418071CF4D6A43506B662D19F9BEAB20F650D857904A6BB41B44063BAC1EBE0AC9CF3CA423407D7AB90469C1C2D98DD960AFCFF0ACBF346A86BBD45764B546BE59A55502B40256F785B9EC0B68BE6F38E35415743B791955B90A63D289AEBEA3510B19E9D72C715CA92650CE0A7DF4886BC6054246A2EAEEDE4A5CAC0368750B2A9826E88BCF1C9DFDE84947BBF639511F1B7CB1B762A2A651FC7A2366CBAC394CB37ADF87BBE4642574BF3894B630920B32A6E20248B0C6DB148C0958607332B9C43FF9877DE96FC97B092D592C59B575146E60A57078FA482F1C3531B4E2297A943FF4889537E03B264D0D6E87F3C8BEA86CDD4F0ABEE86A626BA505B6EF4CDE8AB73CC4CDEA85D21F8FB78FACCE03476D98031DBBB5B0F05E8F34A7FC84E8B6236082C86F109F75D3B98E5F4AAB28E93C5FDC72C3D6C69694FFEE259C4DE89B7FD9036E5EFDFC80FC92AB82DE5E45853DFA844CA126BACB62A6B0BC3FB51E3B2FCEA6E46AC21EE8668693658E612D4873ED3401CFA99B9C3959A2A6A8C9A9EBF10A4A6C980F4DC03B291BC545EE4C5C0B9946CA76A24F57957D60910F82B8FF9636DDADF7A68EE2DB0F24F29ECF608958B10F75AF79F4F56A6EAF873AF13FA269E00951C54A7CF7C3F0D773B676634C3BA0D2A4B4F05382C3022137D430128B3503D9A7EDEF4C34A3A41B1351F6646F44286680555475DF4CAC6E52EEF2703A7ABA2D8B13AA9609D0D3F3C696A4D942EBAD66C8D63D245038FDE9545054D414E30313021300906052B0E03021A05000414F3B9F567CAB53B733082C7B65711694F68D0DCE50408E474F619B83EE1C902020800)
GO
INSERT [dbo].[CertificateFile] ([FileName], [FileContent]) VALUES (N'UBSClientCertificate_PRD.pfx', 0x308206910201033082065706092A864886F70D010701A082064804820644308206403082033F06092A864886F70D010706A08203303082032C0201003082032506092A864886F70D010701301C060A2A864886F70D010C0106300E040861909ADC0B07EFD102020800808202F8B74614B5E4A440AEFF72BA88BD6E3C47F01805EBA5D6790DCA791F8DDE3255D2BBA5454D41E3A85EB559CDF0372AF4D19B75F8B99CD60F9A8C76CEDAD6E01A7A4A16C6A6B30A9F6A61F14722964B13C36273EAEC9D306035A2D06E17FBA8EFEFCE479AE44CBDF01C3D743EDB9F5AD2C83DF58E9F578C40F5142FCA88206F9596513E9BCB5016FA956C3AB25809E39A5354255C9E3A423F4C524BBF3D2EEFAAE97FEC1A851E6995175BEEACDCD8975031A849157E910248E19087DC4D9A99EBC004E437B1DCDC39333F1802B44417DB19A3A40DACBAB3F89042A3E71D743DE3AFD3B9A76327C71A5C7EEA3088170A04BC3D6CD9BF07379BFD4787D91B2442B9F2D3F4E3A230DED373820E5ADD68A1185D39B425BB461D3F3B8A6D053F9D4AABDC88F87B7EA0B11BCA7B90603FDBDC8F86C785965E2729D36444FA2A393E0F36314931B166C3C52364932100334A3E5E9EAF5E8A2D4FD52D821A9CC48EA02DE0ED6F72C9ADE6514A79A917BF85F805CF319D4D3BD95EEAF3E0DB8BC14305A8C514C15CAD3D9995C266E4C4DB5A54866566988169710E6C39A87A4C7F9E5CEC2BD7076D8F8A1B54BE6A2F32486118E2D0F452964886203325643D52BF54C47BB579797081B7A40D736F83CB54E738EB246EB336746AD550058EEF847AA33EDA81ADED047EB89029945049BB2E324B94ACB2DEDBAAC7E207DEC3131FEDD733EDD771281E6C35B4BF8BE8075F79897CF2BE98972A1D3BA591A78A0D19F7589A79819325EF19F01F46E57D45D555A15DC1E60EB30A18BC7A3F768272601FB0E01D73EED0D05C359BE48FD637B4598E1B6D1A616324EFA15A025F510E58A41FAEF89E3748125AE368D7919609F6E3E90A8CE0D7025699D4C401515766555DE3DCBD8B621A85F171993DDCDF03103B7F565171B223A5BF1D5AA57188C48D225498453AF9DC133B09B3458EECCD51240E072BAEEFE2E3E603396F14A1767BC7250A58DADFE21571D1B4C288F1BCDEF2FE308E55F6442F7056C627D8635F2B088A34E771770089FCEB2B65FF467B14B8B0E36962B334D4A0F2052EBE9A308202F906092A864886F70D010701A08202EA048202E6308202E2308202DE060B2A864886F70D010C0A0102A08202A6308202A2301C060A2A864886F70D010C0103300E0408F6E1B1B2160885400202080004820280E516A3D75F438FBEB765CCBE5BDE6438B9E78A7B5EA1902AFF4E80665C950F05A67B61ED686B31735CAA75615A48821435F33C7A76FE22B8DE9CCC17FC50BA47CE7BDC738E3209DAC8208343187B47264D31A95F442C1F371915A41B0AE9AE9407B8E86676024AC77909B68B7EBEEED2D82972C3C2F4D7DB2CC2686AC6D897F9C1E6A12E85D0EA3B7B1C21E720B89A508D1FAA1C5B06AC41548B22F5312AE45686E36EB3294ACB3EA16E1C55CDE1665D4C5554E250F554C0F42130FFE06EE35C3B405BAA2EE9ECF70B96219DE46A682A1A56195D7BBD3CCE6AE935848977E4102ADA7385CA3F2406C0E671D4278C197A25E19F7A900E8A8F9106469CE3DC917100CC416FCAA2094E8E462858FB58B259ACEBF7E811217929940120D3CB6DF304CD7C2396AE9527FE656301384DB0119C6EAFF15D01EB7DFCFE1D7AF09052ECF2131D593F38952EC4EE1D9A43A06B0A7E94F7729878BF94F1381F957A32C2648C629BE542633D37AB6E4A44CBE18492F3A71B1E3CCA077AD1B4C9535FF8898329172301A8B413005519C5618BEA77501A922F3135491A39AC4EBE1A912796AC6A660EB176054AFD7DDE50C85654E87009AF8AD081948E6346A9FE6883D6C5FB0346C6E5BFD57192F89431498BBC4D4A2E9EE46D1135730467B3F66FE1A90044A0F723AB6BB2626B7CB49F74DB3A9068E0D3D366A3DE85C4F4F26CCD7385F9D42B7BD2716885A5FB5FA8BF38B84F26CB6DEB9C056728EF0A044781B9C490AFB7823BC76FD4DCD0AF348714D96D18E13ACFBAFE12CEBC7380906EF69705AB6DD1F783D7CC92B7A5FE23C02C72A9C7581FE35BB4D2A80D6B43E8647DA24A8878045915921EA537A86D1360D9179C87C5F8E105A7FD317F35B1B7ED67EB05E470D4453125302306092A864886F70D01091531160414B64C7265DC75F852DEC5DC7CB505706579F7420730313021300906052B0E03021A05000414B412DF3AB83712868C4C437F6FFF17AAFF5193520408CBBEA9B3B2CB7DFC02020800)
GO
INSERT [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId], [OrderExternalCancelTypeName]) VALUES (0, N'Cancel')
GO
INSERT [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId], [OrderExternalCancelTypeName]) VALUES (2, N'CancelAll')
GO
INSERT [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId], [OrderExternalCancelTypeName]) VALUES (1, N'CancelAndReplace')
GO
INSERT [dbo].[PegType] ([PegTypeId], [PegTypeName]) VALUES (0, N'MarketPeg')
GO
INSERT [dbo].[PegType] ([PegTypeId], [PegTypeName]) VALUES (1, N'PrimaryPeg')
GO

--DealDirection
INSERT INTO [dbo].[DealDirection] ([Id], [Name]) VALUES (0, N'Buy')
GO
INSERT INTO [dbo].[DealDirection] ([Id], [Name]) VALUES (1, N'Sell')
GO

--Currency
SET IDENTITY_INSERT [dbo].[Currency] ON
GO

INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(4, N'ALL', N'Lek', 8, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(5, N'DZD', N'Algerian Dinar', 12, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(6, N'ARS', N'Argentine Peso', 32, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(7, N'AUD', N'Australian Dollar', 36, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(8, N'BSD', N'Bahamian Dollar', 44, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(9, N'BHD', N'Bahraini Dinar', 48, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(10, N'BDT', N'Taka', 50, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(11, N'AMD', N'Armenian Dram', 51, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(12, N'BBD', N'Barbados Dollar', 52, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(13, N'BMD', N'Bermudian Dollar', 60, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(14, N'BTN', N'Ngultrum', 64, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(15, N'BOB', N'Boliviano', 68, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(16, N'BWP', N'Pula', 72, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(17, N'BZD', N'Belize Dollar', 84, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(18, N'SBD', N'Solomon Islands Dollar', 90, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(19, N'BND', N'Brunei Dollar', 96, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(20, N'MMK', N'Kyat', 104, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(21, N'BIF', N'Burundi Franc', 108, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(22, N'KHR', N'Riel', 116, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(23, N'CAD', N'Canadian Dollar', 124, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(24, N'CVE', N'Cape Verde Escudo', 132, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(25, N'KYD', N'Cayman Islands Dollar', 136, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(26, N'LKR', N'Sri Lanka Rupee', 144, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(27, N'CLP', N'Chilean Peso', 152, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(28, N'CNY', N'Yuan Renminbi', 156, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(29, N'COP', N'Colombian Peso', 170, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(30, N'KMF', N'Comoro Franc', 174, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(31, N'CRC', N'Costa Rican Colon', 188, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(32, N'HRK', N'Croatian Kuna', 191, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(33, N'CUP', N'Cuban Peso', 192, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(34, N'CYP', N'Cyprus Pound', 196, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(35, N'CZK', N'Czech Koruna', 203, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(36, N'DKK', N'Danish Krone', 208, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(37, N'DOP', N'Dominican Peso', 214, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(38, N'SVC', N'El Salvador Colon', 222, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(39, N'ETB', N'Ethiopian Birr', 230, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(40, N'ERN', N'Nakfa', 232, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(41, N'EEK', N'Kroon', 233, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(42, N'FKP', N'Falkland Islands Pound', 238, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(43, N'FJD', N'Fiji Dollar', 242, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(44, N'DJF', N'Djibouti Franc', 262, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(45, N'GMD', N'Dalasi', 270, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(46, N'GHC', N'Cedi', 288, 0, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(47, N'GIP', N'Gibraltar Pound', 292, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(48, N'GTQ', N'Quetzal', 320, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(49, N'GNF', N'Guinea Franc', 324, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(50, N'GYD', N'Guyana Dollar', 328, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(51, N'HTG', N'Gourde', 332, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(52, N'HNL', N'Lempira', 340, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(53, N'HKD', N'Hong Kong Dollar', 344, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(54, N'HUF', N'Forint', 348, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(55, N'ISK', N'Iceland Krona', 352, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(56, N'INR', N'Indian Rupee', 356, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(57, N'IDR', N'Rupiah', 360, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(58, N'IRR', N'Iranian Rial', 364, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(59, N'IQD', N'Iraqi Dinar', 368, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(60, N'ILS', N'New Israeli Sheqel', 376, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(61, N'JMD', N'Jamaican Dollar', 388, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(62, N'JPY', N'Yen', 392, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(63, N'KZT', N'Tenge', 398, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(64, N'JOD', N'Jordanian Dinar', 400, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(65, N'KES', N'Kenyan Shilling', 404, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(66, N'KPW', N'North Korean Won', 408, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(67, N'KRW', N'Won', 410, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(68, N'KWD', N'Kuwaiti Dinar', 414, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(69, N'KGS', N'Som', 417, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(70, N'LAK', N'Kip', 418, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(71, N'LBP', N'Lebanese Pound', 422, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(72, N'LSL', N'Loti', 426, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(73, N'LVL', N'Latvian Lats', 428, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(74, N'LRD', N'Liberian Dollar', 430, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(75, N'LYD', N'Libyan Dinar', 434, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(76, N'LTL', N'Lithuanian Litas', 440, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(77, N'MOP', N'Pataca', 446, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(78, N'MWK', N'Kwacha (Malawi)', 454, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(79, N'MYR', N'Malaysian Ringgit', 458, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(80, N'MVR', N'Rufiyaa', 462, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(81, N'MTL', N'Maltese Lira', 470, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(82, N'MRO', N'Ouguiya', 478, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(83, N'MUR', N'Mauritius Rupee', 480, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(84, N'MXN', N'Mexican Peso', 484, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(85, N'MNT', N'Tugrik', 496, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(86, N'MDL', N'Moldovan Leu', 498, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(87, N'MAD', N'Moroccan Dirham', 504, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(88, N'OMR', N'Rial Omani', 512, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(89, N'NAD', N'Namibian Dollar', 516, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(90, N'NPR', N'Nepalese Rupee', 524, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(91, N'ANG', N'Netherlands Antillian Guilder', 532, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(92, N'AWG', N'Aruban Guilder', 533, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(93, N'VUV', N'Vatu', 548, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(94, N'NZD', N'New Zealand Dollar', 554, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(95, N'NIO', N'Cordoba Oro', 558, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(96, N'NGN', N'Naira', 566, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(97, N'NOK', N'Norwegian Krone', 578, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(98, N'PKR', N'Pakistan Rupee', 586, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(99, N'PAB', N'Balboa', 590, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(100, N'PGK', N'Kina', 598, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(101, N'PYG', N'Guarani', 600, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(102, N'PEN', N'Nuevo Sol', 604, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(103, N'PHP', N'Philippine Peso', 608, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(104, N'GWP', N'Guinea-Bissau Peso', 624, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(105, N'QAR', N'Qatari Rial', 634, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(106, N'ROL', N'Old Leu', 642, 0, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(107, N'RUB', N'Russian Ruble', 643, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(108, N'RWF', N'Rwanda Franc', 646, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(109, N'SHP', N'Saint Helena Pound', 654, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(110, N'STD', N'Dobra', 678, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(111, N'SAR', N'Saudi Riyal', 682, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(112, N'SCR', N'Seychelles Rupee', 690, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(113, N'SLL', N'Leone', 694, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(114, N'SGD', N'Singapore Dollar', 702, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(115, N'SKK', N'Slovak Koruna', 703, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(116, N'VND', N'Dong', 704, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(117, N'SIT', N'Tolar', 705, 0, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(118, N'SOS', N'Somali Shilling', 706, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(119, N'ZAR', N'Rand', 710, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(120, N'ZWD', N'Zimbabwe Dollar', 716, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(121, N'SDD', N'Sudanese Dinar', 736, 0, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(122, N'SZL', N'Lilangeni', 748, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(123, N'SEK', N'Swedish Krona', 752, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(124, N'CHF', N'Swiss Franc', 756, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(125, N'SYP', N'Syrian Pound', 760, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(126, N'THB', N'Baht', 764, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(127, N'TOP', N'Pa anga', 776, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(128, N'TTD', N'Trinidad and Tobago Dollar', 780, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(129, N'AED', N'UAE Dirham', 784, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(130, N'TND', N'Tunisian Dinar', 788, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(131, N'TMM', N'Manat', 795, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(132, N'UGX', N'Uganda Shilling', 800, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(133, N'MKD', N'Denar', 807, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(134, N'EGP', N'Egyptian Pound', 818, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(135, N'GBP', N'Pound Sterling', 826, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(136, N'TZS', N'Tanzanian Shilling', 834, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(137, N'USD', N'US Dollar', 840, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(138, N'UYU', N'Peso Uruguayo', 858, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(139, N'UZS', N'Uzbekistan Sum', 860, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(140, N'VEB', N'Bolivar', 862, 0, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(141, N'WST', N'Tala', 882, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(142, N'YER', N'Yemeni Rial', 886, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(143, N'CSD', N'Serbian Dinar (former)', 891, 0, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(144, N'ZMK', N'Kwacha (Zambia)', 894, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(145, N'TWD', N'New Taiwan Dollar', 901, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(146, N'MZN', N'Metical', 943, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(147, N'AZN', N'Azerbaijanian Manat', 944, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(148, N'RON', N'New Leu', 946, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(149, N'CHE', N'WIR Euro', 947, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(150, N'CHW', N'WIR Franc', 948, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(151, N'TRY', N'New Turkish Lira', 949, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(152, N'XAF', N'CFA Franc BEAC', 950, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(153, N'XCD', N'East Caribbean Dollar', 951, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(154, N'XOF', N'CFA Franc BCEAO', 952, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(155, N'XPF', N'CFP Franc', 953, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(156, N'XDR', N'SDR', 960, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(157, N'SRD', N'Surinam Dollar', 968, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(158, N'MGA', N'Malagascy Ariary', 969, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(159, N'COU', N'Unidad de Valor Real', 970, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(160, N'AFN', N'Afghani', 971, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(161, N'TJS', N'Somoni', 972, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(162, N'AOA', N'Kwanza', 973, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(163, N'BYR', N'Belarussian Ruble', 974, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(164, N'BGN', N'Bulgarian Lev', 975, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(165, N'CDF', N'Franc Congolais', 976, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(166, N'BAM', N'Convertible Marks', 977, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(167, N'EUR', N'Euro', 978, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(168, N'MXV', N'Mexican Unidad de Inversion (UID)', 979, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(169, N'UAH', N'Hryvnia', 980, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(170, N'GEL', N'Lari', 981, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(171, N'BOV', N'Mvdol', 984, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(172, N'PLN', N'Zloty', 985, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(173, N'BRL', N'Brazilian Real', 986, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(174, N'CLF', N'Unidades de formento', 990, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(175, N'USN', N'US Dollar (Next day)', 997, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(176, N'USS', N'US Dollar (Same day)', 998, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(177, N'GHS', N'Ghana Cedi', 936, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(179, N'SDG', N'Sudanese Pound', 938, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(180, N'UYI', N'Uruguay Peso en Unidades Indexadas', 940, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(181, N'VEF', N'Bolivar Fuerte', 937, 1, 2007-11-09 10:17:13.647)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(183, N'RSD', N'Serbian Dinar', 941, 1, 2007-11-09 13:20:47.750)
GO
INSERT INTO [dbo].[Currency] ([CurrencyID], [CurrencyAlphaCode], [CurrencyName], [CurrencyISOCode], [Valid], [UpdatedOn]) VALUES(184, N'CNH', N'Offshore (Hong Kong) Chinese Renminbi Yuan', NULL, 1, CAST(0x0000A24900F9F060 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Currency] OFF
GO

--Symbols
SET IDENTITY_INSERT [dbo].[Symbol] ON 
GO

INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(0, N'AUD/CAD', 'AUDCAD', 7, 23, 10000, 0.8637, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(1, N'AUD/CHF', 'AUDCHF', 7, 124, 10000, 0.9928, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(2, N'AUD/JPY', 'AUDJPY', 7, 62, 100, 97.996, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(3, N'AUD/NZD', 'AUDNZD', 7, 94, 10000, 1.1689, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(4, N'AUD/SGD', 'AUDSGD', 7, 114, 10000, 1.27044, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(5, N'AUD/USD', 'AUDUSD', 7, 137, 10000, 0.8205, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(6, N'CAD/JPY', 'CADJPY', 23, 62, 100, 110.13, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(7, N'CHF/JPY', 'CHFJPY', 124, 62, 100, 98.155, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(8, N'EUR/AUD', 'EURAUD', 167, 7, 10000, 1.65749, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(9, N'EUR/CAD', 'EURCAD', 167, 23, 10000, 1.4334, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(10, N'EUR/CHF', 'EURCHF', 167, 124, 10000, 1.6472, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(11, N'EUR/CZK', 'EURCZK', 167, 35, 10000, 24.1751, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(12, N'EUR/DKK', 'EURDKK', 167, 36, 10000, 7.4535, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(13, N'EUR/GBP', 'EURGBP', 167, 135, 10000, 0.71245, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(14, N'EUR/HUF', 'EURHUF', 167, 54, 100, 255.13, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(15, N'EUR/JPY', 'EURJPY', 167, 62, 100, 162.519, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(16, N'EUR/MXN', 'EURMXN', 167, 84, 10000, 17.5139, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(17, N'EUR/NOK', 'EURNOK', 167, 97, 10000, 7.9465, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(18, N'EUR/NZD', 'EURNZD', 167, 94, 10000, 1.9408, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(19, N'EUR/PLN', 'EURPLN', 167, 172, 10000, 3.8184, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(20, N'EUR/SEK', 'EURSEK', 167, 123, 10000, 9.3987, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(21, N'EUR/USD', 'EURUSD', 167, 137, 10000, 1.36185, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(22, N'EUR/ZAR', 'EURZAR', 167, 119, 10000, 13.0225, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(23, N'GBP/AUD', 'GBPAUD', 135, 7, 10000, 2.4605, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(24, N'GBP/CAD', 'GBPCAD', 135, 23, 10000, 2.1245, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(25, N'GBP/CHF', 'GBPCHF', 135, 124, 10000, 2.4415, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(26, N'GBP/JPY', 'GBPJPY', 135, 62, 100, 233.83, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(27, N'GBP/NOK', 'GBPNOK', 135, 97, 10000, 9.8842, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(28, N'GBP/NZD', 'GBPNZD', 135, 94, 10000, 2.7062, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(29, N'GBP/USD', 'GBPUSD', 135, 137, 10000, 2.05469, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(30, N'HKD/JPY', 'HKDJPY', 53, 62, 10000, 11.9482, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(31, N'NOK/SEK', 'NOKSEK', 97, 123, 10000, 1.1588, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(32, N'NZD/JPY', 'NZDJPY', 94, 62, 100, 81.34, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(33, N'NZD/SGD', 'NZDSGD', 94, 114, 10000, 1.02531, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(34, N'NZD/USD', 'NZDUSD', 94, 137, 10000, 0.7022, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(35, N'SGD/JPY', 'SGDJPY', 114, 62, 100, 64.629, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(36, N'USD/CAD', 'USDCAD', 137, 23, 10000, 1.0524, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(37, N'USD/CHF', 'USDCHF', 137, 124, 10000, 1.2095, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(38, N'USD/CZK', 'USDCZK', 137, 35, 10000, 20.326, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(39, N'USD/HKD', 'USDHKD', 137, 53, 10000, 7.7515, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(40, N'USD/HUF', 'USDHUF', 137, 54, 100, 172.62, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(41, N'USD/ILS', 'USDILS', 137, 60, 10000, 3.5037, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(42, N'USD/JPY', 'USDJPY', 137, 62, 100, 115.85, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(43, N'USD/MXN', 'USDMXN', 137, 84, 10000, 11.0239, 1, 1)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(44, N'USD/NOK', 'USDNOK', 137, 97, 10000, 5.8369, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(45, N'USD/PLN', 'USDPLN', 137, 172, 10000, 2.8037, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(46, N'USD/SEK', 'USDSEK', 137, 123, 10000, 6.9022, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(47, N'USD/SGD', 'USDSGD', 137, 114, 10000, 1.52355, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(48, N'USD/TRY', 'USDTRY', 137, 151, 10000, 1.5502, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(49, N'USD/ZAR', 'USDZAR', 137, 119, 10000, 10.09667, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(50, N'ZAR/JPY', 'ZARJPY', 119, 62, 10000, 11.384, 1, 2)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(51, N'USD/DKK', 'USDDKK', 137, 36, 10000, 5.4934, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(52, N'CAD/CHF', 'CADCHF', 23, 124, 10000, 0.8748, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(53, N'USD/SKK', 'USDSKK', 137, 115, 10000, 22.2791, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(54, N'EUR/SKK', 'EURSKK', 167, 115, 10000, 30.1258, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(55, N'CAD/NZD', 'CADNZD', 23, 94, 10000, 1.1671, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(56, N'NZD/CHF', 'NZDCHF', 94, 124, 10000, 0.7498, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(57, N'AUD/DKK', 'AUDDKK', 7, 36, 10000, 5.1756, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(58, N'AUD/NOK', 'AUDNOK', 7, 97, 10000, 5.6519, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(59, N'EUR/TRY', 'EURTRY', 167, 151, 10000, 2.7286, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(60, N'NZD/CAD', 'NZDCAD', 94, 23, 10000, 0.8569, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(61, N'AUD/SEK', 'AUDSEK', 7, 123, 10000, 5.9941, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(62, N'CAD/DKK', 'CADDKK', 23, 36, 10000, 5.3366, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(63, N'CAD/NOK', 'CADNOK', 23, 97, 10000, 5.8252, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(64, N'CAD/SEK', 'CADSEK', 23, 123, 10000, 6.1791, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(65, N'CHF/DKK', 'CHFDKK', 124, 36, 10000, 6.1015, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(66, N'CHF/NOK', 'CHFNOK', 124, 97, 10000, 6.6588, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(67, N'CHF/SEK', 'CHFSEK', 124, 123, 10000, 7.0631, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(68, N'DKK/JPY', 'DKKJPY', 36, 62, 10000, 17.806, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(69, N'DKK/NOK', 'DKKNOK', 36, 97, 10000, 1.0912, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(70, N'DKK/SEK', 'DKKSEK', 36, 123, 10000, 1.158, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(71, N'GBP/DKK', 'GBPDKK', 135, 36, 10000, 8.9256, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(72, N'GBP/SEK', 'GBPSEK', 135, 123, 10000, 10.3336, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(73, N'MXN/JPY', 'MXNJPY', 84, 62, 10000, 7.5011, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(74, N'NOK/JPY', 'NOKJPY', 97, 62, 100, 16.3136, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(75, N'NZD/DKK', 'NZDDKK', 94, 36, 10000, 4.5736, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(76, N'NZD/NOK', 'NZDNOK', 94, 97, 10000, 4.9905, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(77, N'NZD/SEK', 'NZDSEK', 94, 123, 10000, 5.2929, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(78, N'AUD/HKD', 'AUDHKD', 7, 53, 10000, 7.25922, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(79, N'EUR/HKD', 'EURHKD', 167, 53, 10000, 10.48485, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(80, N'EUR/RUB', 'EURRUB', 167, 107, 1000, 43.3967, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(81, N'GBP/CZK', 'GBPCZK', 135, 35, 10000, 30.8005, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(82, N'GBP/HUF', 'GBPHUF', 135, 54, 1000, 356.4313, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(83, N'USD/RUB', 'USDRUB', 137, 107, 1000, 32.4515, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(84, N'GBP/PLN', 'GBPPLN', 135, 172, 10000, 5.0486, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(85, N'EUR/CNH', 'EURCNH', 167, 184, 10000, 8.2849, NULL, NULL)
GO
INSERT INTO [dbo].[Symbol] ([Id], [Name], [ShortName], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradeable], [Usage]) VALUES(86, N'USD/CNH', 'USDCNH', 137, 184, 10000, 6.1172, NULL, NULL)
GO


SET IDENTITY_INSERT [dbo].[Symbol] OFF 
GO

--Counterparty
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (1, N'Bank of America Merrill Lynch', N'BOA')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (2, N'Citibank', N'CTI')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (3, N'Royal Bank of Scotland', N'RBS')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (4, N'JPMorgan Chase', N'JPM')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (5, N'Goldman Sachs', N'GLS')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (6, N'Deutsche Bank', N'DBK')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (7, N'UBS', N'UBS')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (8, N'Commerzbank', N'CZB')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (9, N'Rabobank', N'RAB')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (10, N'Morgan Stanley', N'MGS')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (11, N'Natixis', N'NTX')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (12, N'Nomura', N'NOM')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (13, N'Credit Suisse', N'CRS')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (14, N'BNP Paribas', N'BNP')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (15, N'Barclays', N'BRX')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (16, N'CAPH', N'CPH')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (17, N'Societe Generale', N'SOC')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (18, N'Last Atlantis', N'LTA')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (19, N'HSBC', N'HSB')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (20, N'Hotspot', N'HTA')
GO
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode]) VALUES (21, N'Hotspot', N'HTF')
GO

--Execution report status

INSERT INTO [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (0, N'NotSubmitted')
GO
INSERT INTO [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (1, N'Submitted')
GO
INSERT INTO [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (2, N'Filled')
GO
INSERT INTO [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (3, N'Rejected')
GO
INSERT INTO [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (4, N'InBrokenState')
GO

INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (5, N'PendingCancel')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (6, N'Canceled')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (7, N'CancelRejected')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (8, N'PartiallyFilled')
GO

INSERT [dbo].[OrderTimeInForce] ([OrderTimeInForceId], [OrderTimeInForceName]) VALUES (1, N'Day')
GO
INSERT [dbo].[OrderTimeInForce] ([OrderTimeInForceId], [OrderTimeInForceName]) VALUES (0, N'ImmediateOrKill')
GO

INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (1, N'Limit')
GO
INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (3, N'Market')
GO
INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (2, N'Pegged')
GO
INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (0, N'Quoted')
GO

INSERT [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId], [OrderExternalCancelTypeName]) VALUES (0, N'Cancel')
GO
INSERT [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId], [OrderExternalCancelTypeName]) VALUES (2, N'CancelAll')
GO
INSERT [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId], [OrderExternalCancelTypeName]) VALUES (1, N'CancelAndReplace')
GO

