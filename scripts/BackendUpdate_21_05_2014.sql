


INSERT INTO [dbo].[TableChangeLog] ([TableName], [LastUpdatedUtc]) VALUES ('HotspotMMSettings', '1/1/1900')

CREATE TABLE [dbo].[HotspotMMSettings](
	[SymbolId] [tinyint] NOT NULL,
	[SizeToTrade] [decimal](18,0) NOT NULL,
	[BestPriceImprovementOffset] [decimal](18,6) NOT NULL,
	[MaximumDiscountBasisPointsToTrade]  [decimal](18,6) NOT NULL,
	[MaximumWaitTimeOnImprovementTick_milliseconds] int NOT NULL,
	[Enabled] [bit] NOT NULL,
	[LastResetRequestedUtc] [datetime2] NULL,
	[LastUpdatedUtc] [datetime2] NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_HotspotMMSettingsSymbolId] ON [dbo].[HotspotMMSettings]
(
	[SymbolId] ASC
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[HotspotMMSettings]  WITH NOCHECK ADD  CONSTRAINT [FK_HotspotMMSettings_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO

ALTER TABLE [dbo].[HotspotMMSettings] CHECK CONSTRAINT [FK_HotspotMMSettings_Symbol]
GO

CREATE TRIGGER [dbo].[LastUpdatedTrigger_HotspotMMSettings] ON [dbo].[HotspotMMSettings]
AFTER INSERT, UPDATE
AS

BEGIN

--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[HotspotMMSettings] Set [LastUpdatedUtc] = GETUTCDATE() from [dbo].[HotspotMMSettings] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[SymbolId] = myAlias.[SymbolId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'HotspotMMSettings'

END
GO


INSERT INTO 
	[dbo].[HotspotMMSettings]
           ([SymbolId]
           ,[SizeToTrade]
           ,[BestPriceImprovementOffset]
           ,[MaximumDiscountBasisPointsToTrade]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[Enabled]
           ,[LastResetRequestedUtc])
     SELECT 
		[Id]
      ,50000
	  ,0.00001
	  ,0.25
	  ,280
	  ,0
	  ,NULL
  FROM [dbo].[Symbol]
GO


CREATE PROCEDURE [dbo].[GetUpdateHotspotMMSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'HotspotMMSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		SELECT 
			symbol.Name AS Symbol
			,[SizeToTrade]
			,[BestPriceImprovementOffset]
			,[MaximumDiscountBasisPointsToTrade]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,[Enabled]
			,[LastResetRequestedUtc]
			,[LastUpdatedUtc]
		FROM 
			[dbo].[HotspotMMSettings] sett
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		WHERE
			sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
	END
END
GO

GRANT EXECUTE ON [dbo].[GetUpdateHotspotMMSystemSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetUpdateHotspotMMSystemSettings_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO



CREATE PROCEDURE [dbo].[HotMMSettingsDisableAll_SP]
AS
BEGIN

	UPDATE
		[dbo].[HotspotMMSettings]
	SET
		[Enabled] = 0
END
GO

GRANT EXECUTE ON [dbo].[HotMMSettingsDisableAll_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[HotMMSettingsDisableEnableOne_SP]
(
	@Symbol CHAR(7),
	@Enabled Bit
)
AS
BEGIN

	DECLARE @SymbolId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	UPDATE
		[dbo].[HotspotMMSettings]
	SET
		[Enabled] = @Enabled
	WHERE
		[SymbolId] = @SymbolId
END
GO

GRANT EXECUTE ON [dbo].[HotMMSettingsDisableEnableOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[HotMMSettingsReset_SP]
(
	@Symbol CHAR(7)
)
AS
BEGIN

	DECLARE @SymbolId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	UPDATE
		[dbo].[HotspotMMSettings]
	SET
		[LastResetRequestedUtc] = GETUTCDATE()
	WHERE
		[SymbolId] = @SymbolId
		AND [Enabled] = 0
END
GO

GRANT EXECUTE ON [dbo].[HotMMSettingsReset_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[HotMMSettingsUpdateSingle_SP]
(
	@Symbol CHAR(7),
	@LastUpdateTimeUtc DATETIME2,
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffset [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit]
)
AS
BEGIN

	DECLARE @SymbolId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	UPDATE
		[dbo].[HotspotMMSettings]
	SET
		[SizeToTrade] = @SizeToTrade,
		[BestPriceImprovementOffset] = @BestPriceImprovementOffset,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds,
		[Enabled] = @Enabled
	WHERE
		[SymbolId] = @SymbolId
		AND [LastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

GRANT EXECUTE ON [dbo].[HotMMSettingsUpdateSingle_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


