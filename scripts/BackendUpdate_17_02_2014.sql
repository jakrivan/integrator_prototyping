

UPDATE [dbo].[Settings_FIX]
SET
	[Configuration] = '[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N


SSLEnable=Y
SSLCertificate=CZBClientCertificate_UAT.pfx
SSLCertificatePassword=L0nd0n2013
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N


SenderCompID=KGTMD

[SESSION]
DataDictionary=FIX44_CZB.xml
BeginString=FIX.4.4
TargetCompID=COBAFXMD
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30


SocketConnectPort=21101
SocketConnectHost=62.129.126.204'
WHERE 
	[Name] = 'CZB_QUO'
GO


UPDATE [dbo].[Settings_FIX]
SET
	[Configuration] = '[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y


SSLEnable=Y
SSLCertificate=CZBClientCertificate_UAT.pfx
SSLCertificatePassword=L0nd0n2013
#SSLServerName=
#SSLProtocols=
#SSLCACertificate=
SSLValidateCertificates=N


SenderCompID=KGTORD

[SESSION]
DataDictionary=FIX44_CZB.xml
RelaxValidationForMessageTypes=8
BeginString=FIX.4.4
TargetCompID=COBAFXORD
#5pm NYK
StartTime=17:05:01
StartDay=Sunday
EndTime=16:59:50
EndDay=Friday
TimeZone=Eastern Standard Time
HeartBtInt=30

SocketConnectPort=21101
SocketConnectHost=62.129.126.204'
WHERE 
	[Name] = 'CZB_ORD'
GO


INSERT INTO [dbo].[CertificateFile]
           ([FileName], [FileContent])
     SELECT N'CZBClientCertificate_UAT.pfx', BulkColumn FROM OPENROWSET(BULK
     N'C:\Data\Temp\CertificatesToInsert\KGT.pfx', SINGLE_BLOB) AS Document
GO
