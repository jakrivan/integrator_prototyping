



/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.XsdSchemas]    Script Date: 09. 11. 2013 14:05:16 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.XsdSchemas] AS 
N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RiskManagementSettings" nillable="true" type="RiskManagementSettings" />
  <xs:complexType name="RiskManagementSettings">
    <xs:sequence>
      <xs:element name="ConfigLastUpdated"  nillable="true" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAndPosition" type="PriceAndPositionSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersSubmissionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
  </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrderMaximumAllowedSizeSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedBaseAbsSizeInUsd" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="PriceAndPositionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalNOPRiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumExternalNOPInUSD" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceDeviationCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceHistoryForOnlinePriceDeviationCheck" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="OnlinePriceDeviationCheckMaxPriceAge_Minutes" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="Symbols" type="ArrayOfSymbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbol">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="Symbol" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="Symbol">
    <xs:attribute name="ShortName" type="xs:string" />
    <xs:attribute name="MidPrice" type="xs:decimal" />
    <xs:attribute name="NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent" type="xs:decimal" />
  </xs:complexType>
</xs:schema>

<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="BusinessLayerSettings" nillable="true" type="BusinessLayerSettings" />
  <xs:complexType name="BusinessLayerSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MarketableClientOrdersMatchingStrategy" type="MarketableClientOrdersMatchingStrategy" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="ConsecutiveRejectionsStopStrategyBehavior" type="ConsecutiveRejectionsStopStrategySettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartWithSessionsStopped" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="AutoKillInternalOrdersOnOrdersTransmissionDisabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailSubject" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailBody" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalErrorsEmailMinDelayBetweenEmails_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="MarketableClientOrdersMatchingStrategy">
    <xs:restriction base="xs:string">
      <xs:enumeration value="ImmediateMatching" />
      <xs:enumeration value="NextFreshPriceMatching" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ConsecutiveRejectionsStopStrategySettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="StrategyEnabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="NumerOfRejectionsToTriggerStrategy" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAgeRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceBpRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultCounterpartyRank" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRanks" type="ArrayOfCounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartyRank">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartyRank" nillable="true" type="CounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartyRank">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="Rank" type="xs:decimal" />
  </xs:complexType>
</xs:schema>'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings_New](
	[Name] [nvarchar](100) NOT NULL,
	[SettingsXml] [xml](CONTENT [dbo].[Kreslik.Integrator.XsdSchemas]) NOT NULL,
	[LastUpdated] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

INSERT INTO [dbo].[Settings_New] (Name, SettingsXml, LastUpdated) SELECT Name, CONVERT(XML, SettingsXml), LastUpdated FROM [dbo].[Settings]


DROP TABLE [dbo].[Settings]
GO

DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.RiskManagement.dll.Schema]
GO

EXEC sp_rename 'Settings_New', 'Settings';
GO




INSERT INTO [dbo].[Settings]
           ([Name]
           ,[SettingsXml]
		   ,[LastUpdated])
     VALUES
           (N'Kreslik.Integrator.BusinessLayer.dll'
           ,
N'<BusinessLayerSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <MarketableClientOrdersMatchingStrategy>ImmediateMatching</MarketableClientOrdersMatchingStrategy>
  <ConsecutiveRejectionsStopStrategyBehavior>
    <StrategyEnabled>true</StrategyEnabled>
    <NumerOfRejectionsToTriggerStrategy>3</NumerOfRejectionsToTriggerStrategy>
    <CounterpartyRankWeightMultiplier>20</CounterpartyRankWeightMultiplier>
    <PriceAgeRankWeightMultiplier>1</PriceAgeRankWeightMultiplier>
    <PriceBpRankWeightMultiplier>20</PriceBpRankWeightMultiplier>
    <DefaultCounterpartyRank>7</DefaultCounterpartyRank>
    <CounterpartyRanks>
      <CounterpartyRank CounterpartyCode="UBS" Rank="5" />
      <CounterpartyRank CounterpartyCode="MGS" Rank="5.2" />
      <CounterpartyRank CounterpartyCode="RBS" Rank="5.5" />
      <CounterpartyRank CounterpartyCode="CTI" Rank="5.8" />
      <CounterpartyRank CounterpartyCode="CRS" Rank="6.1" />
      <CounterpartyRank CounterpartyCode="BOA" Rank="6.8" />
      <CounterpartyRank CounterpartyCode="CZB" Rank="7.1" />
      <CounterpartyRank CounterpartyCode="GLS" Rank="7.4" />
      <CounterpartyRank CounterpartyCode="JPM" Rank="7.7" />
      <CounterpartyRank CounterpartyCode="BNP" Rank="8.9" />
      <CounterpartyRank CounterpartyCode="BRX" Rank="9.5" />
      <CounterpartyRank CounterpartyCode="SOC" Rank="9.5" />
      <CounterpartyRank CounterpartyCode="HSB" Rank="10" />
      <CounterpartyRank CounterpartyCode="NOM" Rank="10" />
    </CounterpartyRanks>
  </ConsecutiveRejectionsStopStrategyBehavior>
  <StartWithSessionsStopped>false</StartWithSessionsStopped>
  <AutoKillInternalOrdersOnOrdersTransmissionDisabled>true</AutoKillInternalOrdersOnOrdersTransmissionDisabled>
 
  <StartStopEmailSubject>Integrator service on {0} is {1}</StartStopEmailSubject>
  <StartStopEmailBody></StartStopEmailBody>
  <StartStopEmailTo>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</StartStopEmailTo>
  <StartStopEmailCc></StartStopEmailCc>
  <IgnoredOrderEmailTo>michal.kreslik@kgtinv.com;marek.fogiel@kgtinv.com</IgnoredOrderEmailTo>
  <IgnoredOrderEmailCc>jan.krivanek@kgtinv.com</IgnoredOrderEmailCc>
  <FatalErrorsEmailTo>michal.kreslik@kgtinv.com;jan.krivanek@kgtinv.com</FatalErrorsEmailTo>
  <FatalErrorsEmailCc></FatalErrorsEmailCc>
  <FatalErrorsEmailMinDelayBetweenEmails_Minutes>10</FatalErrorsEmailMinDelayBetweenEmails_Minutes>
</BusinessLayerSettings>'
,
GETUTCDATE())
GO