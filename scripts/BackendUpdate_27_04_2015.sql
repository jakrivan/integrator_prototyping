

	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[OrderExternalIncomingRejectable_Archive]
			([IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[RecordId])
		SELECT
			[IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[RecordId]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]	

		-- get ready for the next day
		TRUNCATE TABLE [dbo].[OrderExternalIncomingRejectable_Daily]

		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
GO



ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Daily] ADD GlobalRecordId AS [RecordId] * 4 + 2 PERSISTED
ALTER TABLE [dbo].[DealExternalExecuted] ADD GlobalRecordId AS [RecordId] * 4 + 0 PERSISTED
print 'DealExternalExecuted GlobalRecordId added:'
print GETUTCDATE()
ALTER TABLE [dbo].[DealExternalRejected] ADD GlobalRecordId AS [RecordId] * 4 + 1 PERSISTED
print 'DealExternalRejected GlobalRecordId added:'
print GETUTCDATE()
GO

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] ADD RecordIdTmp INT IDENTITY(1,1) NOT NULL
print 'OrderExternalIncomingRejectable_Archive GlobalRecordId added:'
print GETUTCDATE()
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] ADD GlobalRecordId INT NULL
GO

UPDATE [dbo].[OrderExternalIncomingRejectable_Archive] SET GlobalRecordId = RecordIdTmp * 4 + 2
GO
print 'OrderExternalIncomingRejectable_Archive RecordId inserted (as not identity):'
print GETUTCDATE()

DECLARE @LastId INT
SELECT @LastId = IDENT_CURRENT ('OrderExternalIncomingRejectable_Archive')
SET @LastId = @LastId + 1
DBCC CHECKIDENT ('OrderExternalIncomingRejectable_Daily', RESEED, @LastId)

ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] DROP COLUMN RecordIdTmp
ALTER TABLE [dbo].[OrderExternalIncomingRejectable_Archive] ALTER COLUMN GlobalRecordId INT NOT NULL
GO
print 'OrderExternalIncomingRejectable_Archive columns updated:'
print GETUTCDATE()
GO


ALTER PROCEDURE [dbo].[Daily_ArchiveRejectableOrders_SP]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO
			[dbo].[OrderExternalIncomingRejectable_Archive]
			([IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[RecordId])
		SELECT
			[IntegratorReceivedExternalOrderUtc],
			[CounterpartySentExternalOrderUtc],
			[IntegratorSentExecutionReportUtc],
			[CounterpartyClientOrderId],
			[CounterpartyClientId],
			[TradingSystemId],
			[SourceIntegratorPriceIdentity],
			[CounterpartyId],
			[SymbolId],
			[IntegratorDealDirectionId],
			[CounterpartyRequestedPrice],
			[CounterpartyRequestedAmountBasePol],
			[IntegratorExecutionId],
			[ValueDate],
			[OrderReceivedToExecutionReportSentLatency],
			[IntegratorExecutedAmountBasePol],
			[IntegratorRejectedAmountBasePol],
			[IntegratorRejectionReasonId],
			[RecordId]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]	
		
		--remember last identity
		DECLARE @LastId INT
		SELECT @LastId = IDENT_CURRENT ('OrderExternalIncomingRejectable_Daily')
		SET @LastId = @LastId + 1
		
		-- get ready for the next day
		TRUNCATE TABLE [dbo].[OrderExternalIncomingRejectable_Daily]
		
		--and continue in identity
		DBCC CHECKIDENT ('OrderExternalIncomingRejectable_Daily', RESEED, @LastId)
		
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO

ALTER PROCEDURE [dbo].[InsertTradeDecayAnalysisRequests_SP]
(
	@FromTimeUtc [datetime2](7) = NULL,
	@ToTimeUtc [datetime2](7) = NULL
)
AS
BEGIN

	IF @ToTimeUtc IS NULL
	BEGIN
		SET @ToTimeUtc = DATEADD(minute, -5, GETUTCDATE())
	END
	
	IF @FromTimeUtc IS NULL
	BEGIN
		SELECT @FromTimeUtc = [LastProcessedRecordUtc] FROM [dbo].[TradeDecay_LastProcessedRecord]
	END
	
	DECLARE @TypeCptDealId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'CptDeal')
	DECLARE @TypeCptRejectionId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'CptRejection')
	DECLARE @TypeKgtDealId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'KgtDeal')
	DECLARE @TypeKgtRejectionId [tinyint] = (SELECT [TradeDecayAnalysisTypeId] FROM [FXtickDB].[dbo].[TradeDecayAnalysisType] WHERE [TradeDecayAnalysisTypeName] = 'KgtRejection')
	
	
	DECLARE @TempRequestsToAdd TABLE (
		[TradeDecayAnalysisTypeId] [tinyint] NOT NULL,
		[SourceEventGlobalRecordId] [int] NOT NULL,
		[SourceEventIndexTimeUtc] [datetime2](7) NOT NULL,
		[AnalysisStartTimeUtc] [datetime2](7) NOT NULL,
		[FXPairId] [tinyint] NOT NULL,
		[CounterpartyId] [tinyint] NOT NULL,
		[ReferencePrice] [decimal](18,9) NOT NULL,
		[PriceSideId] [bit] NOT NULL
	)
	
	INSERT INTO @TempRequestsToAdd(
		[TradeDecayAnalysisTypeId],
		[SourceEventGlobalRecordId],
		[SourceEventIndexTimeUtc],
		[AnalysisStartTimeUtc],
		[FXPairId],
		[CounterpartyId],
		[ReferencePrice],
		[PriceSideId]
	)
	SELECT
		@TypeCptDealId,
		[GlobalRecordId],
		[IntegratorReceivedExecutionReportUtc],
		CASE WHEN [FlowSideId] = 0 OR ([FlowSideId] IS NULL AND cpt.CounterpartyName IN ('Hotspot', 'FXall', 'FXCM')) 
		THEN DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExecutionReportUtc])
		ELSE [IntegratorSentExternalOrderUtc] END,
		symbolIdMap.[DCDBSymbolId],
		cptIdMap.[DCDBCounterpartyId],
		[Price],
		CASE WHEN [DealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
	FROM 
		[dbo].[DealExternalExecuted] deal
		INNER JOIN [dbo].[Counterparty] cpt ON deal.CounterpartyId = cpt.CounterpartyId
		INNER JOIN [dbo].[PriceDelaySettings] delaySett ON deal.CounterpartyId = delaySett.CounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON deal.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON deal.SymbolId = symbolIdMap.IntegratorDBSymbolId
		--cannot use between due to inclusions
		WHERE [IntegratorReceivedExecutionReportUtc] > @FromTimeUtc AND [IntegratorReceivedExecutionReportUtc] < @ToTimeUtc
	UNION ALL
	SELECT
		@TypeCptRejectionId,
		[GlobalRecordId],
		[IntegratorReceivedExecutionReportUtc],
		CASE WHEN cpt.CounterpartyName IN ('Hotspot', 'FXall', 'FXCM') 
		THEN DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExecutionReportUtc]) 
		ELSE [IntegratorSentExternalOrderUtc] END,
		symbolIdMap.[DCDBSymbolId],
		cptIdMap.[DCDBCounterpartyId],
		[RejectedPrice],
		CASE WHEN [DealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
	FROM 
		[dbo].[DealExternalRejected] reject
		INNER JOIN [dbo].[Counterparty] cpt ON reject.CounterpartyId = cpt.CounterpartyId
		INNER JOIN [dbo].[PriceDelaySettings] delaySett ON reject.CounterpartyId = delaySett.CounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON reject.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON reject.SymbolId = symbolIdMap.IntegratorDBSymbolId
		--cannot use between due to inclusions
		WHERE [IntegratorReceivedExecutionReportUtc] > @FromTimeUtc AND [IntegratorReceivedExecutionReportUtc] < @ToTimeUtc
		AND [RejectedPrice] IS NOT NULL
	UNION ALL
	SELECT
		CASE WHEN [IntegratorRejectionReasonId] IS NULL THEN @TypeKgtDealId ELSE @TypeKgtRejectionId END,
		[GlobalRecordId],
		[IntegratorReceivedExternalOrderUtc],
		DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExternalOrderUtc]),
		symbolIdMap.[DCDBSymbolId],
		cptIdMap.[DCDBCounterpartyId],
		[CounterpartyRequestedPrice],
		CASE WHEN [IntegratorDealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
	FROM 
		[dbo].[OrderExternalIncomingRejectable_Daily] cptOrder
		INNER JOIN [dbo].[PriceDelaySettings] delaySett ON cptOrder.CounterpartyId = delaySett.CounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON cptOrder.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
		INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON cptOrder.SymbolId = symbolIdMap.IntegratorDBSymbolId
		--cannot use between due to inclusions
		WHERE [IntegratorReceivedExternalOrderUtc] > @FromTimeUtc AND [IntegratorReceivedExternalOrderUtc] < @ToTimeUtc
	
	IF DATEDIFF(day, @FromTimeUtc, GETUTCDATE()) != 0
	BEGIN
		INSERT INTO @TempRequestsToAdd(
			[TradeDecayAnalysisTypeId],
			[SourceEventGlobalRecordId],
			[SourceEventIndexTimeUtc],
			[AnalysisStartTimeUtc],
			[FXPairId],
			[CounterpartyId],
			[ReferencePrice],
			[PriceSideId]
		)
		SELECT
			CASE WHEN [IntegratorRejectionReasonId] IS NULL THEN @TypeKgtDealId ELSE @TypeKgtRejectionId END,
			[GlobalRecordId],
			[IntegratorReceivedExternalOrderUtc],
			DATEADD(NANOSECOND,-delaySett.[ExpectedDelayTicks]*(100), [IntegratorReceivedExternalOrderUtc]),
			symbolIdMap.[DCDBSymbolId],
			cptIdMap.[DCDBCounterpartyId],
			[CounterpartyRequestedPrice],
			CASE WHEN [IntegratorDealDirectionId] = 0 THEN 1 ELSE 0 END AS [PriceSideId]
		FROM 
			[dbo].[OrderExternalIncomingRejectable_Archive] cptOrder
			INNER JOIN [dbo].[PriceDelaySettings] delaySett ON cptOrder.CounterpartyId = delaySett.CounterpartyId
			INNER JOIN [FXtickDB].[dbo].[_Internal_CounterpartyIdsMapping] cptIdMap ON cptOrder.CounterpartyId = cptIdMap.IntegratorDBCounterpartyId
			INNER JOIN [FXtickDB].[dbo].[_Internal_SymbolIdsMapping] symbolIdMap ON cptOrder.SymbolId = symbolIdMap.IntegratorDBSymbolId
			--cannot use between due to inclusions
			WHERE [IntegratorReceivedExternalOrderUtc] > @FromTimeUtc AND [IntegratorReceivedExternalOrderUtc] < @ToTimeUtc
	END
	
	
	DECLARE  @LastRecordTimeUtc [datetime2](7)
	
	SELECT @LastRecordTimeUtc = MAX([SourceEventIndexTimeUtc]) FROM @TempRequestsToAdd
	
	IF @LastRecordTimeUtc IS NOT NULL
	BEGIN
		--try catch used so that execution flow is stopped after error
		BEGIN TRY
		
			INSERT INTO [FXtickDB].[dbo].[TradeDecayAnalysis_Pending]
				   ([TradeDecayAnalysisTypeId]
				   ,[SourceEventGlobalRecordId]
				   ,[AnalysisStartTimeUtc]
				   ,[FXPairId]
				   ,[CounterpartyId]
				   ,[ReferencePrice]
				   ,[PriceSideId])
			SELECT
				[TradeDecayAnalysisTypeId],
				[SourceEventGlobalRecordId],
				[AnalysisStartTimeUtc],
				[FXPairId],
				[CounterpartyId],
				[ReferencePrice],
				[PriceSideId]
			FROM
				@TempRequestsToAdd
				   
			UPDATE [dbo].[TradeDecay_LastProcessedRecord] SET [LastProcessedRecordUtc] = @LastRecordTimeUtc 

		END TRY
		BEGIN CATCH
			print 'error during storing';
			THROW;
		END CATCH
	END  
	ELSE
	BEGIN
		-- so that we do not search same intervals over and over if no trading
		UPDATE [dbo].[TradeDecay_LastProcessedRecord] SET [LastProcessedRecordUtc] = DATEADD(minute, -5, @ToTimeUtc)
	END
	
END
GO

UPDATE [dbo].[TradeDecay_LastProcessedRecord] SET LastProcessedRecordUtc = '2000-01-01'


ALTER TABLE [dbo].[CommissionsPerCounterparty] ADD [DestinationCommissionPpm] decimal(18,6) NULL
ALTER TABLE [dbo].[CommissionsPerCounterparty] ADD [SaxoPbCommissionPpm] decimal(18,6) NULL
GO
UPDATE [dbo].[CommissionsPerCounterparty] SET [DestinationCommissionPpm] = [CommissionPricePerMillion] - 3.25
UPDATE [dbo].[CommissionsPerCounterparty] SET [SaxoPbCommissionPpm] = 5.5
GO
ALTER TABLE [dbo].[CommissionsPerCounterparty] ALTER COLUMN [DestinationCommissionPpm] decimal(18,6) NOT NULL
ALTER TABLE [dbo].[CommissionsPerCounterparty] ALTER COLUMN [SaxoPbCommissionPpm] decimal(18,6) NOT NULL
GO
ALTER TABLE [dbo].[CommissionsPerCounterparty] DROP COLUMN [CommissionPricePerMillion]
GO
ALTER TABLE [dbo].[CommissionsPerCounterparty] ADD [CommissionPricePerMillion] AS [DestinationCommissionPpm] + [SaxoPbCommissionPpm]
GO