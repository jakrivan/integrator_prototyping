BEGIN TRANSACTION AAA

--LmaxMrgAct

INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (52, N'LMAX', N'LGA', 1)
INSERT INTO [dbo].[Counterparty] ([CounterpartyId], [CounterpartyName], [CounterpartyCode], [Active]) VALUES (53, N'LMAX', N'LGC', 1)
GO


INSERT INTO [dbo].[CommissionsSchedule_CptCommission] ([CounterpartyId], [CounterpartyCommissionPpm], [ValidFromUtc], [ValidToUtc]) VALUES (52, 5, '1900-01-01', '9999-12-31')
INSERT INTO [dbo].[CommissionsSchedule_CptCommission] ([CounterpartyId], [CounterpartyCommissionPpm], [ValidFromUtc], [ValidToUtc]) VALUES (53, 5, '1900-01-01', '9999-12-31')

INSERT INTO [dbo].[CommissionsSchedule_Mapping] ([CounterpartyId], [PrimeBrokerId], [ValidFromUtc], [ValidToUtc]) VALUES (52, 5, '2015-07-21', '9999-12-31')
INSERT INTO [dbo].[CommissionsSchedule_Mapping] ([CounterpartyId], [PrimeBrokerId], [ValidFromUtc], [ValidToUtc]) VALUES (53, 5, '2015-07-21', '9999-12-31')


INSERT INTO [dbo].[DelaySettings] 
	([CounterpartyId]
    ,[ExpectedDelayTicks]
      ,[MaxAllowedAdditionalDelayTicks]
      ,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
      ,[MinAdditionalDelayTicksToTurnGFO]
      ,[MinNegativeDelayTicksFromExpectedToTurnGFO])
SELECT
	52
	,[ExpectedDelayTicks]
	,[MaxAllowedAdditionalDelayTicks]
	,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
	,[MinAdditionalDelayTicksToTurnGFO]
	,[MinNegativeDelayTicksFromExpectedToTurnGFO]
FROM
	[dbo].[DelaySettings]
WHERE
	CounterpartyId = 23 -- l01
	
INSERT INTO [dbo].[DelaySettings] 
	([CounterpartyId]
    ,[ExpectedDelayTicks]
      ,[MaxAllowedAdditionalDelayTicks]
      ,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
      ,[MinAdditionalDelayTicksToTurnGFO]
      ,[MinNegativeDelayTicksFromExpectedToTurnGFO])
SELECT
	53
	,[ExpectedDelayTicks]
	,[MaxAllowedAdditionalDelayTicks]
	,[AllowGoFlatOnlyFlipOnTimeStampOutOfBounds]
	,[MinAdditionalDelayTicksToTurnGFO]
	,[MinNegativeDelayTicksFromExpectedToTurnGFO]
FROM
	[dbo].[DelaySettings]
WHERE
	CounterpartyId = 23 -- FA1
	
INSERT INTO [dbo].[PriceDelayStats] ([CounterpartyId], [TotalReceivedPricesInLastMinute], [DelayedPricesInLastMinute], [TotalReceivedPricesInLastHour], [DelayedPricesInLastHour]) VALUES (52, 0, 0, 0, 0)
INSERT INTO [dbo].[PriceDelayStats] ([CounterpartyId], [TotalReceivedPricesInLastMinute], [DelayedPricesInLastMinute], [TotalReceivedPricesInLastHour], [DelayedPricesInLastHour]) VALUES (53, 0, 0, 0, 0)
GO


INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'LGA_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=kgtinvfix

SSLEnable=N
#SSLValidateCertificates=N
#SSLValidateCertificatesOID=N

[SESSION]
#DataDictionary=FIX44_LMX.xml
UseDataDictionary=N
BeginString=FIX.4.4
TargetCompID=LMXBL
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30
BypassParsing=Y

#ssl
#SocketConnectPort=443
#no ssl
SocketConnectPort=50000
#SocketConnectHost=fix.lmaxtrader.com
SocketConnectHost=91.215.164.10', GETUTCDATE())
GO

INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'LGA_QUO', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# For market data there is no need of history even in case of unexpected aborts 
#  (banks also never resend historic quotes), so we do not want to persist and we want to rest on logon
ResetOnLogon=Y
PersistMessages=N

SenderCompID=kgtinvfix

SSLEnable=N
#SSLValidateCertificates=N
#SSLValidateCertificatesOID=N

[SESSION]
#DataDictionary=FIX44_LMX.xml
UseDataDictionary=N
BeginString=FIX.4.4
TargetCompID=LMXBLM
#5pm NYK
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30
BypassParsing=Y

#ssl
#SocketConnectPort=443
#no ssl
SocketConnectPort=50000
#SocketConnectHost=fix-md.lmaxtrader.com
SocketConnectHost=91.215.164.11', GETUTCDATE())
GO

INSERT [dbo].[Settings_FIX] ([Name], [SettingsVersion], [Configuration], [LastUpdated]) VALUES (N'LGC_ORD', 1, N'[DEFAULT]
FileStorePath=store
FileLogPath=log
ConnectionType=initiator
ReconnectInterval=30
# In case of successful session shutdown we do not want to persist anything (ResetOnLogout)
# However after unexpected abort we want to receive any potentially missed messages (ResetOnLogon)
#ResetOnLogon=Y
ResetOnLogout=Y

SenderCompID=kgtinv

SSLEnable=N
#SSLValidateCertificates=N
#SSLValidateCertificatesOID=N

[SESSION]
#DataDictionary=FIX44_LMX.xml
UseDataDictionary=N
BeginString=FIX.4.4
TargetCompID=LMXBL
StartTime=17:05:01
EndTime=16:59:50
TimeZone=Eastern Standard Time
HeartBtInt=30
BypassParsing=Y

#ssl
#SocketConnectPort=443
#no ssl
SocketConnectPort=50000
#SocketConnectHost=fix.lmaxtrader.com
SocketConnectHost=91.215.164.10', GETUTCDATE())
GO

DECLARE @LCAOrdFixSettingId INT
DECLARE @LGAOrdFixSettingId INT
DECLARE @LGAQuoFixSettingId INT

SELECT
	@LCAOrdFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'LGC_ORD'

SELECT
	@LGAOrdFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'LGA_ORD'
	
SELECT
	@LGAQuoFixSettingId = [Id]
FROM
	[dbo].[Settings_FIX]
WHERE
	[Name] = 'LGA_QUO'


INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [IntegratorEnvironmentId], @LCAOrdFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]	
	
INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])
SELECT 
	DISTINCT [IntegratorEnvironmentId], @LGAOrdFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]

INSERT INTO [dbo].[IntegratorEnvironmentToSettingsFixMap]
           ([IntegratorEnvironmentId]
           ,[SettingsFixId])

SELECT 
	DISTINCT [IntegratorEnvironmentId], @LGAQuoFixSettingId
FROM 
	[dbo].[IntegratorEnvironmentToSettingsFixMap]
GO


DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd]
GO

--new schema!!

CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.SessionManagement.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="SessionsSettings" nillable="true" type="SessionsSettings" />
  <xs:complexType name="SessionsSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CRS" type="FIXChannel_CRSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_DBK" type="FIXChannel_DBKSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_UBS" type="FIXChannel_UBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CTI" type="FIXChannel_CTISettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_MGS" type="FIXChannel_MGSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_RBS" type="FIXChannel_RBSSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_JPM" type="FIXChannel_JPMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_BNP" type="FIXChannel_BNPSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_CZB" type="FIXChannel_CZBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_SOC" type="FIXChannel_SOCSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_NOM" type="FIXChannel_NOMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTA" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTF" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3" type="FIXChannel_HOTSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FAL" type="FIXChannel_FALSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM1" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM2" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LM3" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LX1" type="FIXChannel_LMXSettings" />
	  <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LGA" type="FIXChannel_LMXSettings" />
	  <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_LGC" type="FIXChannel_LMXSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC1" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FC2" type="FIXChannel_FCMSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS1" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FS2" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HTAstream" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_HT3stream" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXChannel_FL1" type="StreamingChannelSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="FIXSTPChannel_CTIPB" type="FIXSTPChannel_CTIPBSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnexpectedMessagesTreating" type="UnexpectedMessagesTreatingSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="MarketDataSession" type="MarketDataSessionSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="OrderFlowSession" type="OrderFlowSessionSettings" />
      <xs:element minOccurs="0" maxOccurs="1" name="SubscriptionSettingsRaw" type="SubscriptionSettingsBag" />
      <xs:element minOccurs="1" maxOccurs="1" name="STP" type="STPSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CRSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ClientId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_DBKSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_UBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PartyId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CTISettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_MGSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_RBSSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_JPMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PasswordLength" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_BNPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_CZBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_SOCSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_NOMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="OnBehalfOfCompID" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_HOTSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="DealConfirmationSupported" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FALSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_LMXSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="L01LayersNum" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="Username" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXChannel_FCMSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="IocApplyTtl" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="IocTtlMs" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ApplyMaxHoldTime" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxHoldTime" type="FXCMMaxHoldTime" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MaxPerSymbolPricesPerSecondSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultMaxAllowedPerSecondRate" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialSymbolRateSettings" type="ArrayOfSymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolRateSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Symbol" nillable="true" type="SymbolRateSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolRateSetting">
    <xs:attribute name="Name" type="xs:string" />
    <xs:attribute name="MaxAllowedPerSecondRate" type="xs:int" use="required" />
  </xs:complexType>
  <xs:complexType name="RejectionRateDisconnectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="RiskCheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumOrdersToApplyCheck" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAllowedRejectionRatePercent" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="StreamingChannelSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxTotalPricesPerSecond" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="MaxPerSymbolPricesPerSecondSetting" type="MaxPerSymbolPricesPerSecondSetting" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExpectedPricingDepth" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="RejectionRateDisconnectSettings" type="RejectionRateDisconnectSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FIXSTPChannel_CTIPBSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Password" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="Account" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UnexpectedMessagesTreatingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="EnableThreasholdingOfUnexpectedMessages" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Minutes" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MarketDataSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MaxAutoResubscribeCount" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="RetryIntervalsSequence_Seconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedSubscriptionTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OrderFlowSessionSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedOrderTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="PutErrorsInLessImportantLog" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalOrdersRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="CheckAllowed" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedNongenuineInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="SpecialRejectionsHandling" type="ArrayOfSpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SpecialRejection" type="SpecialRejectionRateSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SpecialRejectionRateSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="TriggerPhrase" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="MaximumAllowedInstancesPerTimeInterval" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeIntervalToCheck_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SubscriptionSettingsBag">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ResubscribeOnReconnect" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="GlobalSubscriptionSettings" type="GlobalSubscription" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartsSubscriptionSettings" type="CounterpartsSubscription" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GlobalSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="SymbolSetting" nillable="true" type="SymbolSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SymbolSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="Size" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="FXCMMaxHoldTime">
    <xs:restriction base="xs:string">
      <xs:enumeration value="UpTo1ms" />
      <xs:enumeration value="UpTo30ms" />
      <xs:enumeration value="UpTo100ms" />
      <xs:enumeration value="UpTo500ms" />
      <xs:enumeration value="UpTo3000ms" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="SettingAction">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add" />
      <xs:enumeration value="Remove" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="CounterpartsSubscription">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="Counterparts" type="ArrayOfCounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartSetting" nillable="true" type="CounterpartSetting" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartSetting">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="ShortName" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="SettingAction" type="SettingAction" />
      <xs:element minOccurs="0" maxOccurs="1" name="Symbols" type="ArrayOfSymbolSetting" />
      <xs:element minOccurs="0" maxOccurs="1" name="SenderSubId" type="xs:string" />
      <xs:element minOccurs="0" maxOccurs="1" name="MarketSessionIntoSeparateProcess" type="xs:boolean" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="STPDestinations" type="ArrayOfSTPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfSTPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="STPDestination" nillable="true" type="STPDestinationSettings" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="STPDestinationSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="Name" type="STPCounterparty" />
      <xs:element minOccurs="1" maxOccurs="1" name="UnconfirmedTicketTimeout_Seconds" type="xs:int" />
      <xs:element minOccurs="0" maxOccurs="1" name="ServedCounterparties" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="STPCounterparty">
    <xs:restriction base="xs:string">
      <xs:enumeration value="CTIPB" />
      <xs:enumeration value="Traiana" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'


-- manually !!
--add (after </FIXChannel_LX1>): 
<FIXChannel_LGA>
	<Username>kgtinvfix</Username>
	<Password>D-s1Xc6wQ</Password>
</FIXChannel_LGA>
<FIXChannel_LGC>
	<Username>kgtinv</Username>
	<Password>sX9-cQ3gL</Password>
</FIXChannel_LGC>


<CounterpartSetting>
			  <ShortName>LGA</ShortName>
			  <SettingAction>Add</SettingAction>
			  <Symbols>
			  <SymbolSetting>
				  <ShortName>ALL</ShortName>
				  <SettingAction>Remove</SettingAction>
			  </SymbolSetting>
			  </Symbols>
		  </CounterpartSetting>
		  <CounterpartSetting>
			  <ShortName>LGC</ShortName>
			  <SettingAction>Add</SettingAction>
			  <Symbols>
				  <SymbolSetting>
					  <ShortName>ALL</ShortName>
					  <SettingAction>Remove</SettingAction>
				  </SymbolSetting>
			  </Symbols>
		  </CounterpartSetting>

--also add LGA/LCA wherever is LM1 or L01


--ROLLBACK TRANSACTION AAA
--GO
--COMMIT TRANSACTION AAA
--GO

--remove the constraint of 1 quoting system per symbol
ALTER TABLE [dbo].[VenueQuotingSettings] DROP CONSTRAINT [UNQ__VenueQuotingSettings__SymbolIdPerCptId]
GO

INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LGC'), 'Quoting')
GO
