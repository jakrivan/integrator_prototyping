
/****** Object:  Table [dbo].[OrderTimeInForce]    Script Date: 17. 10. 2013 17:41:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderTimeInForce](
	[OrderTimeInForceId] [tinyint] NOT NULL,
	[OrderTimeInForceName] [varchar](16) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderType]    Script Date: 17. 10. 2013 17:41:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderType](
	[OrderTypeId] [tinyint] NOT NULL,
	[OrderTypeName] [varchar](16) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[OrderExternalCancelRequest]    Script Date: 17. 10. 2013 18:26:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalCancelRequest](
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NULL,
	[IntegratorSentCancelRequestUtc] [datetime2](7) NOT NULL,
	[OrderExternalCancelTypeId] [tinyint] NOT NULL,
	[FixMessageSentRaw] [nvarchar](1024) NOT NULL,
	[FixMessageSentParsed] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderExternalCancelResult]    Script Date: 17. 10. 2013 18:26:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderExternalCancelResult](
	[IntegratorExternalOrderPrivateId] [nvarchar](45) NOT NULL,
	[CounterpartySentResultUtc] [datetime2](7) NOT NULL,
	[CanceledAmountBasePol] [decimal](18, 0) NOT NULL,
	[ResultStatusId] [tinyint] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderExternalCancelType]    Script Date: 17. 10. 2013 18:26:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderExternalCancelType](
	[OrderExternalCancelTypeId] [tinyint] NOT NULL,
	[OrderExternalCancelTypeName] [varchar](16) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO

/****** Object:  Index [IX_OrderTimeInForceId]    Script Date: 17. 10. 2013 17:41:51 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_OrderTimeInForceId] ON [dbo].[OrderTimeInForce]
(
	[OrderTimeInForceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderType]    Script Date: 17. 10. 2013 17:41:51 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_OrderType] ON [dbo].[OrderType]
(
	[OrderTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_OrderExternalCancelRequest_OrderId]    Script Date: 17. 10. 2013 18:26:37 ******/
CREATE CLUSTERED INDEX [IX_OrderExternalCancelRequest_OrderId] ON [dbo].[OrderExternalCancelRequest]
(
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_OrderExternalCancelResult_OrderId]    Script Date: 17. 10. 2013 18:26:37 ******/
CREATE CLUSTERED INDEX [IX_OrderExternalCancelResult_OrderId] ON [dbo].[OrderExternalCancelResult]
(
	[IntegratorExternalOrderPrivateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderExternalCancelTypeId]    Script Date: 17. 10. 2013 18:26:37 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_OrderExternalCancelTypeId] ON [dbo].[OrderExternalCancelType]
(
	[OrderExternalCancelTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (5, N'PendingCancel')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (6, N'Canceled')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (7, N'CancelRejected')
GO
INSERT [dbo].[ExecutionReportStatus] ([Id], [Name]) VALUES (8, N'PartiallyFilled')
GO
INSERT [dbo].[OrderTimeInForce] ([OrderTimeInForceId], [OrderTimeInForceName]) VALUES (1, N'Day')
GO
INSERT [dbo].[OrderTimeInForce] ([OrderTimeInForceId], [OrderTimeInForceName]) VALUES (0, N'ImmediateOrKill')
GO
INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (1, N'Limit')
GO
INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (3, N'Market')
GO
INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (2, N'Pegged')
GO
INSERT [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (0, N'Quoted')
GO

INSERT [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId], [OrderExternalCancelTypeName]) VALUES (0, N'Cancel')
GO
INSERT [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId], [OrderExternalCancelTypeName]) VALUES (2, N'CancelAll')
GO
INSERT [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId], [OrderExternalCancelTypeName]) VALUES (1, N'CancelAndReplace')
GO


/****** Object:  Index [IX_OrderTimeInForceName]    Script Date: 17. 10. 2013 17:41:51 ******/
ALTER TABLE [dbo].[OrderTimeInForce] ADD  CONSTRAINT [IX_OrderTimeInForceName] UNIQUE NONCLUSTERED 
(
	[OrderTimeInForceName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_OrderTypeName]    Script Date: 17. 10. 2013 17:41:51 ******/
ALTER TABLE [dbo].[OrderType] ADD  CONSTRAINT [IX_OrderTypeName] UNIQUE NONCLUSTERED 
(
	[OrderTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/****** Object:  Index [IX_OrderExternalCancelTypeName]    Script Date: 17. 10. 2013 18:26:38 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_OrderExternalCancelTypeName] ON [dbo].[OrderExternalCancelType]
(
	[OrderExternalCancelTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest] CHECK CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternal]
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternalCancelType] FOREIGN KEY([OrderExternalCancelTypeId])
REFERENCES [dbo].[OrderExternalCancelType] ([OrderExternalCancelTypeId])
GO
ALTER TABLE [dbo].[OrderExternalCancelRequest] CHECK CONSTRAINT [FK_OrderExternalCancelRequest_OrderExternalCancelType]
GO
ALTER TABLE [dbo].[OrderExternalCancelResult]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelResult_ExecutionReportStatus] FOREIGN KEY([ResultStatusId])
REFERENCES [dbo].[ExecutionReportStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderExternalCancelResult] CHECK CONSTRAINT [FK_OrderExternalCancelResult_ExecutionReportStatus]
GO
ALTER TABLE [dbo].[OrderExternalCancelResult]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternalCancelResult_OrderExternal] FOREIGN KEY([IntegratorExternalOrderPrivateId])
REFERENCES [dbo].[OrderExternal] ([IntegratorExternalOrderPrivateId])
GO
ALTER TABLE [dbo].[OrderExternalCancelResult] CHECK CONSTRAINT [FK_OrderExternalCancelResult_OrderExternal]
GO



/****** Object:  StoredProcedure [dbo].[InsertOrderExternalCancelRequest_SP]    Script Date: 17. 10. 2013 18:31:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Stored Procedure
CREATE PROCEDURE [dbo].[InsertOrderExternalCancelRequest_SP]( 
	@IntegratorExternalOrderPrivateId [nvarchar](45),
    @IntegratorSentCancelRequestUtc  DATETIME2(7),
    @OrderExternalCancelTypeName VARCHAR(16),
    @RawFIXMessage [nvarchar](1024),
	@ParsedFIXMessage [nvarchar](MAX)
	)
AS
BEGIN

	DECLARE @OrderExternalCancelTypeId TINYINT

	SELECT @OrderExternalCancelTypeId = [OrderExternalCancelTypeId] FROM [dbo].[OrderExternalCancelType] WHERE [OrderExternalCancelTypeName] = @OrderExternalCancelTypeName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderExternalCancelTypeName not found in DB: %s', 16, 2, @OrderExternalCancelTypeName) WITH SETERROR
	END


	INSERT INTO [dbo].[OrderExternalCancelRequest]
           ([IntegratorExternalOrderPrivateId]
           ,[IntegratorSentCancelRequestUtc]
           ,[OrderExternalCancelTypeId]
           ,[FixMessageSentRaw]
           ,[FixMessageSentParsed])
     VALUES
           (@IntegratorExternalOrderPrivateId
           ,@IntegratorSentCancelRequestUtc
           ,@OrderExternalCancelTypeId
           ,@RawFIXMessage
           ,@ParsedFIXMessage)
END
GO
GRANT EXECUTE ON [dbo].[InsertOrderExternalCancelRequest_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

/****** Object:  StoredProcedure [dbo].[InsertOrderExternalCancelResult_SP]    Script Date: 17. 10. 2013 18:31:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Stored Procedure
CREATE PROCEDURE [dbo].[InsertOrderExternalCancelResult_SP]( 
	@IntegratorExternalOrderPrivateId [nvarchar](45),
    @CounterpartySentResultUtc  DATETIME2(7),
    @CanceledAmountBasePol decimal(18,0),
    @ResultStatusName VARCHAR(16)
	)
AS
BEGIN

	DECLARE @ResultStatusId TINYINT

	SELECT @ResultStatusId = [Id] FROM [dbo].[ExecutionReportStatus] WHERE [Name] = @ResultStatusName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'ResultStatusName not found in DB: %s', 16, 2, @ResultStatusName) WITH SETERROR
	END


	INSERT INTO [dbo].[OrderExternalCancelResult]
           ([IntegratorExternalOrderPrivateId]
           ,[CounterpartySentResultUtc]
           ,[CanceledAmountBasePol]
           ,[ResultStatusId])
     VALUES
           (@IntegratorExternalOrderPrivateId
           ,@CounterpartySentResultUtc
           ,@CanceledAmountBasePol
           ,@ResultStatusId)
END
GO
GRANT EXECUTE ON [dbo].[InsertOrderExternalCancelResult_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

--
-- OrderExternal table
--

-- Table
ALTER TABLE [OrderExternal] ALTER COLUMN [QuoteReceivedToOrderSentInternalLatency] TIME(7) NULL
GO

ALTER TABLE [OrderExternal]  ADD OrderTypeId tinyint NULL
ALTER TABLE [OrderExternal]  ADD OrderTimeInForceId tinyint NULL
ALTER TABLE [OrderExternal]  ADD RequestedPrice decimal(18,9) NULL
ALTER TABLE [OrderExternal]  ADD RequestedAmountBaseAbs decimal(18,0) NULL
ALTER TABLE [OrderExternal]  ADD SourceIntegratorPriceTimeUtc datetime2(7) NULL
GO


ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_OrderTimeInForce] FOREIGN KEY([OrderTimeInForceId])
REFERENCES [dbo].[OrderTimeInForce] ([OrderTimeInForceId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_OrderTimeInForce]
GO
ALTER TABLE [dbo].[OrderExternal]  WITH CHECK ADD  CONSTRAINT [FK_OrderExternal_OrderType] FOREIGN KEY([OrderTypeId])
REFERENCES [dbo].[OrderType] ([OrderTypeId])
GO
ALTER TABLE [dbo].[OrderExternal] CHECK CONSTRAINT [FK_OrderExternal_OrderType]
GO


-- Stored procedure
ALTER PROCEDURE [dbo].[InsertNewOrderExternal_SP]( 
	@ExternalOrderSentUtc [datetime2](7),
	@InternalOrderId [nvarchar](45),
	@InternalClientOrderId [nvarchar](32),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@QuoteReceivedToOrderSentInternalLatency [time](7),
	@FixMessageSentRaw nvarchar(1024),
	@FixMessageSentParsed nvarchar(MAX),
	@OrderTypeName varchar(16),
	@OrderTimeInForceName varchar(16),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,0),
	@SourceIntegratorPriceIdentity uniqueidentifier = NULL,
	@SourceIntegratorPriceTimeUtc [datetime2](7) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @OrderTypeId TINYINT
	DECLARE @OrderTimeInForceId TINYINT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	SELECT @OrderTypeId = [OrderTypeId] FROM [dbo].[OrderType] WHERE [OrderTypeName] = @OrderTypeName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderType not found in DB: %s', 16, 2, @OrderTypeName) WITH SETERROR
	END
	
	SELECT @OrderTimeInForceId = [OrderTimeInForceId] FROM [dbo].[OrderTimeInForce] WHERE [OrderTimeInForceName] = @OrderTimeInForceName

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'OrderTimeInForce not found in DB: %s', 16, 2, @OrderTimeInForceName) WITH SETERROR
	END

	INSERT INTO 
		[dbo].[OrderExternal]
           ([IntegratorSentExternalOrderUtc]
           ,[IntegratorExternalOrderPrivateId]
		   ,[ExternalOrderLabelForCounterparty]
           ,[CounterpartyId]
           ,[SymbolId]
		   ,[DealDirectionId]
           ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[FixMessageSentRaw]
		   ,[FixMessageSentParsed]
		   ,[OrderTypeId]
		   ,[OrderTimeInForceId]
		   ,[RequestedPrice]
		   ,[RequestedAmountBaseAbs]
		   ,[SourceIntegratorPriceIdentity]
		   ,[SourceIntegratorPriceTimeUtc])
     VALUES
           (@ExternalOrderSentUtc
           ,@InternalOrderId
		   ,@InternalClientOrderId
           ,@CounterpartyId
           ,@SymbolId
		   ,@DealDirectionId
           ,@QuoteReceivedToOrderSentInternalLatency
		   ,@FixMessageSentRaw
		   ,@FixMessageSentParsed
		   ,@OrderTypeId
		   ,@OrderTimeInForceId
		   ,@RequestedPrice
		   ,@RequestedAmountBaseAbs
		   ,@SourceIntegratorPriceIdentity
		   ,@SourceIntegratorPriceTimeUtc)
END
GO

--
-- End of OrderExternal table
--

--
-- Data reconstruction
--


--TimeInForce
UPDATE
    [dbo].[OrderExternal]
SET
    [OrderTimeInForceId] = 0 -- IoC
	
	
--Price and Amount
UPDATE
    [dbo].[OrderExternal]
SET
	[dbo].[OrderExternal].[RequestedAmountBaseAbs] = ABS(rej.AmountBasePolRejected)
FROM 
	[dbo].[OrderExternal] ordr
	INNER JOIN [dbo].[DealExternalRejected] rej ON rej.IntegratorExternalOrderPrivateId = ordr.IntegratorExternalOrderPrivateId	
	
UPDATE
    [dbo].[OrderExternal]
SET
    [dbo].[OrderExternal].[RequestedPrice] = deal.Price
	,[dbo].[OrderExternal].[RequestedAmountBaseAbs] = ABS(deal.AmountBasePolExecuted)
FROM 
	[dbo].[OrderExternal] ordr
	INNER JOIN [dbo].[DealExternalExecuted] deal ON deal.IntegratorExternalOrderPrivateId = ordr.IntegratorExternalOrderPrivateId


--OrderType
UPDATE
    [dbo].[OrderExternal]
SET
    [OrderTypeId] = 0 -- Quoted

UPDATE
    [dbo].[OrderExternal]
SET
    [OrderTypeId] = 1 -- Limit
WHERE
	[CounterpartyId] IN (SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] IN ('JPM', 'DBK', 'HOT'))