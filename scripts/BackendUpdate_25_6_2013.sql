
/****** Object:  XmlSchemaCollection [dbo].[Kreslik.Integrator.RiskManagement.dll.Schema]    Script Date: 25. 6. 2013 14:05:16 ******/
CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.RiskManagement.dll.Schema] AS N'<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"><xsd:element name="RiskManagementSettings" type="RiskManagementSettings" nillable="true" /><xsd:complexType name="ArrayOfSymbol"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="Symbol" type="Symbol" minOccurs="0" maxOccurs="unbounded" nillable="true" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ExternalOrderMaximumAllowedSizeSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="RiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumAllowedBaseAbsSizeInUsd" type="xsd:decimal" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ExternalOrdersRejectionRateSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="RiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumAllowedInstancesPerTimeInterval" type="xsd:int" /><xsd:element name="TimeIntervalToCheck_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="ExternalOrdersSubmissionRateSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="RiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumAllowedInstancesPerTimeInterval" type="xsd:int" /><xsd:element name="TimeIntervalToCheck_Seconds" type="xsd:int" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="PriceAndPositionSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ExternalNOPRiskCheckAllowed" type="xsd:boolean" /><xsd:element name="MaximumExternalNOPInUSD" type="xsd:decimal" /><xsd:element name="MaximumAllowedNOPCalculationTime_Milliseconds" type="xsd:int" /><xsd:element name="PriceDeviationCheckAllowed" type="xsd:boolean" /><xsd:element name="ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed" type="xsd:boolean" /><xsd:element name="Symbols" type="ArrayOfSymbol" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="RiskManagementSettings"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence><xsd:element name="ExternalOrdersSubmissionRate" type="ExternalOrdersSubmissionRateSettings" /><xsd:element name="ExternalOrdersRejectionRate" type="ExternalOrdersRejectionRateSettings" /><xsd:element name="ExternalOrderMaximumAllowedSize" type="ExternalOrderMaximumAllowedSizeSettings" /><xsd:element name="PriceAndPosition" type="PriceAndPositionSettings" /></xsd:sequence></xsd:restriction></xsd:complexContent></xsd:complexType><xsd:complexType name="Symbol"><xsd:complexContent><xsd:restriction base="xsd:anyType"><xsd:sequence /><xsd:attribute name="ShortName" type="xsd:string" /><xsd:attribute name="MidPrice" type="xsd:decimal" /><xsd:attribute name="NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent" type="xsd:decimal" /></xsd:restriction></xsd:complexContent></xsd:complexType></xsd:schema>'
GO


/****** Object:  Table [dbo].[Settings]    Script Date: 25. 6. 2013 14:05:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[Name] [nvarchar](100) NOT NULL,
	[SettingsXml] [xml](CONTENT [dbo].[Kreslik.Integrator.RiskManagement.dll.Schema]) NOT NULL,
	[LastUpdated] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Index [IX_Settings]    Script Date: 25. 6. 2013 14:05:17 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_Settings] ON [dbo].[Settings]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/****** Object:  StoredProcedure [dbo].[GenerateUpdateSettingsScripts_SP]    Script Date: 25. 6. 2013 14:05:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GenerateUpdateSettingsScripts_SP] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 'UPDATE [dbo].[Settings] SET [SettingsXml] = ''' + 
	CONVERT(VARCHAR(MAX), [SettingsXml]) +
	''', [LastUpdated] = GETUTCDATE() WHERE [Name] = ''' + 
	CONVERT(VARCHAR(MAX), [Name]) + '''' 
	FROM [dbo].[Settings]
END


/****** Object:  StoredProcedure [dbo].[InsertCommandItem_SP]    Script Date: 28. 5. 2013 19:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSettingsXml_SP] 
	@SettingName NVARCHAR(100),
	@UpdatedAfter Datetime
AS
BEGIN
	--SET NOCOUNT ON;

	SELECT 
		[SettingsXml]
	FROM 
		[dbo].[Settings]
	WHERE
		[Name] = @SettingName
		AND [LastUpdated] > @UpdatedAfter
END

GO
GRANT EXECUTE ON [dbo].[GetSettingsXml_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


INSERT INTO [dbo].[Settings]
           ([Name]
           ,[SettingsXml]
		   ,[LastUpdated])
     VALUES
           (N'Kreslik.Integrator.RiskManagement.dll'
           ,
N'<RiskManagementSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ExternalOrdersSubmissionRate>
    <RiskCheckAllowed>true</RiskCheckAllowed>
    <MaximumAllowedInstancesPerTimeInterval>20</MaximumAllowedInstancesPerTimeInterval>
    <TimeIntervalToCheck_Seconds>5</TimeIntervalToCheck_Seconds>
  </ExternalOrdersSubmissionRate>
  <ExternalOrdersRejectionRate>
    <RiskCheckAllowed>true</RiskCheckAllowed>
    <MaximumAllowedInstancesPerTimeInterval>7</MaximumAllowedInstancesPerTimeInterval>
    <TimeIntervalToCheck_Seconds>25</TimeIntervalToCheck_Seconds>
  </ExternalOrdersRejectionRate>
  <ExternalOrderMaximumAllowedSize>
    <RiskCheckAllowed>true</RiskCheckAllowed>
    <MaximumAllowedBaseAbsSizeInUsd>2500000</MaximumAllowedBaseAbsSizeInUsd>
  </ExternalOrderMaximumAllowedSize>
  <PriceAndPosition>
    <ExternalNOPRiskCheckAllowed>true</ExternalNOPRiskCheckAllowed>
    <MaximumExternalNOPInUSD>10000000</MaximumExternalNOPInUSD>
    <MaximumAllowedNOPCalculationTime_Milliseconds>2</MaximumAllowedNOPCalculationTime_Milliseconds>
    <PriceDeviationCheckAllowed>true</PriceDeviationCheckAllowed>
    <ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed>true</ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed>
    <Symbols>
      <Symbol ShortName="AUDCAD" MidPrice="0.9755" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="AUDCHF" MidPrice="0.88539" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="AUDNZD" MidPrice="1.198" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="AUDJPY" MidPrice="90.36" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="AUDSGD" MidPrice="1.2004" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="AUDUSD" MidPrice="0.95952" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="CADJPY" MidPrice="92.62" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="CHFJPY" MidPrice="102.046" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURAUD" MidPrice="1.3866" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURCHF" MidPrice="1.22767" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURCAD" MidPrice="1.35272" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURDKK" MidPrice="7.45852" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURHUF" MidPrice="293.34" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURGBP" MidPrice="0.84881" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="SGDJPY" MidPrice="75.247" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="GBPUSD" MidPrice="1.5673" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURJPY" MidPrice="125.299" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="NZDUSD" MidPrice="0.80065" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="NZDSGD" MidPrice="1.00162" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURNOK" MidPrice="7.6705" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURNZD" MidPrice="1.6613" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDNOK" MidPrice="5.7674" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURPLN" MidPrice="4.2455" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURSEK" MidPrice="8.6661" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDTRY" MidPrice="1.86347" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDSGD" MidPrice="1.251" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURUSD" MidPrice="1.33051" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDJPY" MidPrice="94.172" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDPLN" MidPrice="3.1907" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDMXN" MidPrice="12.8137" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="GBPCHF" MidPrice="1.44627" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDILS" MidPrice="3.6073" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDHUF" MidPrice="220.458" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="GBPAUD" MidPrice="1.6334" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDSEK" MidPrice="6.5129" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="GBPCAD" MidPrice="1.5935" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDHKD" MidPrice="7.7643" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDCHF" MidPrice="0.92275" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDCAD" MidPrice="1.01655" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="NZDJPY" MidPrice="75.399" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="HKDJPY" MidPrice="12.1284" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="GBPJPY" MidPrice="147.596" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="GBPNZD" MidPrice="1.957" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDCZK" MidPrice="19.294" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="ZARJPY" MidPrice="9.51" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="USDZAR" MidPrice="9.895" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURCZK" MidPrice="25.67" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURZAR" MidPrice="13.1655" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="GBPNOK" MidPrice="9.0405" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="NOKSEK" MidPrice="1.1289" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="EURMXN" MidPrice="17.0456" NewOrderPriceMaximumDeviationFromMarketMidPriceInPercent="10" />
      <Symbol ShortName="DKKUSD" MidPrice="0.175" />
    </Symbols>
  </PriceAndPosition>
</RiskManagementSettings>'
,
GETUTCDATE())
GO