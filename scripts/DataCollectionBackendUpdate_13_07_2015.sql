
SET IDENTITY_INSERT [dbo].[LiqProviderStream] ON 
GO
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (107, N'LX1', 22)
INSERT [dbo].[LiqProviderStream] ([LiqProviderStreamId], [LiqProviderStreamName], [LiqProviderId]) VALUES (108, N'FL1', 21)
GO
SET IDENTITY_INSERT [dbo].[LiqProviderStream] OFF
GO


INSERT INTO [dbo].[_Internal_CounterpartyIdsMapping] ([IntegratorDBCounterpartyId], [DCDBCounterpartyId], [CounterpartyCode])
SELECT 
	integratorCpt.[CounterpartyId] AS IntegratorDBCounterpartyId,
	dcCpt.[LiqProviderStreamId] AS DCDBCounterpartyId,
    dcCpt.[LiqProviderStreamName] AS CounterpartyCode
FROM
	[dbo].[LiqProviderStream] dcCpt
	INNER JOIN [Integrator].[dbo].[Counterparty] integratorCpt ON dcCpt.LiqProviderStreamName = integratorCpt.CounterpartyCode
WHERE
	dcCpt.[LiqProviderStreamId] NOT IN (SELECT DCDBCounterpartyId FROM [dbo].[_Internal_CounterpartyIdsMapping])
	
INSERT INTO [dbo].[MarketDataRecordType]
           ([RecordTypeId]
           ,[Description])
     VALUES
           (7
           ,'Kgt Own Order On Venue')
GO

ALTER PROCEDURE [dbo].[PerformSingleTradeDecayAnalysis_SP]
(
	@TradeDecayAnalysisTypeId [tinyint],
	@SourceEventGlobalRecordId [int],
	@AnalysisStartTimeUtc [datetime2](7),
	@FXPairId [tinyint],
	@CounterpartyId [tinyint],
	@ReferencePrice [decimal](18,9),
	@PriceSideId [bit]
)
AS
BEGIN

	--@PriceSideId  meaning
	-- 0 is our sell - so our Ask (or sell on cpt Bid)
	-- 1 is our buy - so our Bid (or buy on cpt Ask)

	DECLARE @TradeDecayAnalysisId INT

	INSERT INTO [dbo].[TradeDecayAnalysis]
           ([TradeDecayAnalysisTypeId]
           ,[SourceEventGlobalRecordId]
           ,[AnalysisStartTimeUtc]
           ,[FXPairId]
           ,[CounterpartyId]
           ,[PriceSideId]
           ,[HasValidTickAfterIntervalEnd]
           ,[IsValidAndComplete])
     VALUES
           (@TradeDecayAnalysisTypeId,
			@SourceEventGlobalRecordId,
			@AnalysisStartTimeUtc,
			@FXPairId,
			@CounterpartyId,
			@PriceSideId,
			0,
			0)
	
	SET @TradeDecayAnalysisId = scope_identity();
	DECLARE @ReferenceCounterpartyId INT = (SELECT [LiqProviderStreamId] FROM [LiqProviderStream] WHERE [LiqProviderStreamName] = 'L01')
	-- round to next whole us and and 1ms for the first interval
	DECLARE @AnalysisFirstIntervalStartTimeUtc [datetime2](7) = DATEADD(ns, -DATEPART(ns, @AnalysisStartTimeUtc)%1000 + 1000 + 1000000, @AnalysisStartTimeUtc)
	DECLARE @AnalysisEndTimeUtc [datetime2](7) = DATEADD(minute, 5, @AnalysisStartTimeUtc)
	
	
	DECLARE @FirstBidTob [decimal](18,9)
	DECLARE @FirstAskTob [decimal](18,9)
	
	SET @FirstBidTob =
	(SELECT TOP 1
		md.Price AS LastToBPrice
	FROM
		[dbo].[MarketData] md with (nolock) 
	WHERE
		IntegratorReceivedTimeUtc BETWEEN DATEADD(second, -30, @AnalysisFirstIntervalStartTimeUtc) AND @AnalysisFirstIntervalStartTimeUtc
		AND fxpairid  = @FxPairId
		AND RecordTypeId IN (0, 3, 7)
		AND CounterpartyId = @ReferenceCounterpartyId
		AND SideId = 0
	ORDER BY IntegratorReceivedTimeUtc DESC)
	
	SET @FirstAskTob =
	(SELECT TOP 1
		md.Price AS LastToBPrice
	FROM
		[dbo].[MarketData] md with (nolock) 
	WHERE
		IntegratorReceivedTimeUtc BETWEEN DATEADD(second, -30, @AnalysisFirstIntervalStartTimeUtc) AND @AnalysisFirstIntervalStartTimeUtc
		AND fxpairid  = @FxPairId
		AND RecordTypeId IN (0, 3, 7)
		AND CounterpartyId = @ReferenceCounterpartyId
		AND SideId = 1
	ORDER BY IntegratorReceivedTimeUtc DESC)
	
	--fast track if there is no valid price before the interval
	IF @FirstBidTob IS NULL AND @FirstAskTob IS NULL
	BEGIN
		RETURN;
	END
	
	IF NOT EXISTS(
		SELECT 1 
		FROM 
			[dbo].[MarketData] md with (nolock) 
		WHERE
			IntegratorReceivedTimeUtc BETWEEN @AnalysisEndTimeUtc AND DATEADD(second, 30, @AnalysisEndTimeUtc)
			AND fxpairid  = @FxPairId
			AND RecordTypeId IN (0, 3, 7)
			AND CounterpartyId = @ReferenceCounterpartyId
			AND SideId = @PriceSideId
		)
	BEGIN
		RETURN;
	END
	
	
	INSERT INTO [dbo].[TradeDecayAnalysisDataPoint]
           ([TradeDecayAnalysisId]
           ,[MillisecondDistanceFromAnalysisStart]
           ,[ReferenceMidPrice]
		   ,[ReferenceBidPrice]
		   ,[IsReferenceBidAuthenticValue]
		   ,[ReferenceAskPrice]
		   ,[IsReferenceAskAuthenticValue]
           ,[BpMtmToReferenceMidPrice]
		   ,[BpMtmToReferenceCrossPrice]
		   ,[BpMtmToReferenceJoinPrice]
		   )
	SELECT	
		@TradeDecayAnalysisId,
		MillisecondDistance,
		-- as we do not have ANY()
		AVG(LastSidedToB) AS MidToB,
		-- as we do not have ANY() - all 4 followings
		MAX(LastBid) AS Bid,
		MAX(IsLastBidAuthentic) AS IsBidAuthentic,
		MAX(LastAsk) AS Ask,
		MAX(IsLastAskAuthentic) AS IsAskAuthentic,
		CASE WHEN @PriceSideId = 0 THEN (@ReferencePrice - AVG(LastSidedToB)) ELSE (AVG(LastSidedToB) - @ReferencePrice) END / @ReferencePrice * 10000.0 AS MtMFromMid,
		CASE WHEN @PriceSideId = 0 THEN (@ReferencePrice - MAX(LastAsk)) ELSE (MAX(LastBid) - @ReferencePrice) END / @ReferencePrice * 10000.0 AS MtMFromCrossSpread,
		CASE WHEN @PriceSideId = 0 THEN (@ReferencePrice - MAX(LastBid)) ELSE (MAX(LastAsk) - @ReferencePrice) END / @ReferencePrice * 10000.0 AS MtMFromSameSide
	FROM	
		(

		SELECT
			MillisecondDistance,
			SideId,
			LastSidedToB,
			CASE WHEN SideId = 0 THEN LastSidedToB ELSE 0 END AS LastBid,
			CASE WHEN SideId = 0 THEN IsAuthenticValue ELSE 0 END AS IsLastBidAuthentic,
			CASE WHEN SideId = 1 THEN LastSidedToB ELSE 0 END AS LastAsk,
			CASE WHEN SideId = 1 THEN IsAuthenticValue ELSE 0 END AS IsLastAskAuthentic
		FROM
		(
			SELECT
				MillisecondDistance,
				SideId,
				--for lack of ANY()
				MAX(LastToBPrice) OVER (PARTITION BY NonNullToBsNum) AS LastSidedToB,
				CASE WHEN LastToBPrice IS NULL THEN 0 ELSE 1 END AS IsAuthenticValue
			FROM
				(
				SELECT
					nums.MsDistanceFromEventStart AS MillisecondDistance,
					nums.SideId AS SideId,
					LastToBPrice, -- as we do not have any - all are same
					-- this will count number of non-null rows - for nulls (no ToB in current millisecond) it will repeat previous val (so we can partition by this one level up)
					COUNT(LastToBPrice) OVER (ORDER BY nums.SideId, nums.MsDistanceFromEventStart ASC) as NonNullToBsNum
				FROM
				[dbo].[_Internal_TradeDecayIntervals] nums
				LEFT JOIN
				(
					SELECT
						0 AS BucketizedMillisecondsFromIntervalStart,
						0 AS SideId,
						@FirstBidTob AS LastToBPrice
					UNION ALL
					SELECT
						0 AS BucketizedMillisecondsFromIntervalStart,
						1 AS SideId,
						@FirstAskTob AS LastToBPrice	
					UNION ALL
					SELECT
						BucketizedMillisecondsFromIntervalStart,
						SideId,
						MAX(LastToBPrice) AS LastToBPrice -- as we do not have ANY() - all are same
					FROM
					(
						SELECT 
							-- millisecond difference from the beginig
							[dbo].[GetTradeDecayTimeIntervalBucket](DateDiff(mcs, @AnalysisStartTimeUtc, md.integratorReceivedTimeUtc)/1000) AS BucketizedMillisecondsFromIntervalStart,
							SideId,
							-- last price in current millisecond interval = ToB in current millisecond
							LAST_VALUE(md.Price) 
								OVER(
									PARTITION BY [dbo].[GetTradeDecayTimeIntervalBucket](DateDiff(mcs, @AnalysisStartTimeUtc, md.integratorReceivedTimeUtc)/1000), SideId
									ORDER BY md.integratorReceivedTimeUtc ASC 
									ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) AS LastToBPrice
						FROM
							[dbo].[MarketData] md with (nolock) 
						WHERE
							IntegratorReceivedTimeUtc BETWEEN @AnalysisFirstIntervalStartTimeUtc AND @AnalysisEndTimeUtc
							AND fxpairid  = @FxPairId
							AND RecordTypeId IN (0, 3, 7)
							AND CounterpartyId = @ReferenceCounterpartyId
							--AND SideId = @PriceSideId
					) tmp
					GROUP BY BucketizedMillisecondsFromIntervalStart, SideId
				) tmp2 ON nums.MsDistanceFromEventStart = tmp2.BucketizedMillisecondsFromIntervalStart AND nums.SideId = tmp2.SideId
				) tmp3
			) tmp4
		)tmp5	
	GROUP BY MillisecondDistance
	ORDER BY MillisecondDistance ASC

	
	UPDATE [dbo].[TradeDecayAnalysis]
	SET 
      [HasValidTickAfterIntervalEnd] = 1
      ,[IsValidAndComplete] = 1
	WHERE 
		[TradeDecayAnalysisId] = @TradeDecayAnalysisId
	
END
GO