BEGIN TRANSACTION Install_03_10_001;
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP]
AS
BEGIN
	
	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()

	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]

	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn
	
	SELECT
		ROW_NUMBER() OVER(ORDER BY
			trgroup.[OrderingRank] ASC,
			[TradingSystemGroupName] ASC,
			counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] ASC,
			beginSymbol.Name ASC) AS Rank,
		trgroup.[TradingSystemGroupName] AS TradingSystemGroup,
		trgroup.[TradingSystemGroupId] As TradingSystemGroupId,
		counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] AS TradingSystemTypeName,
		counterparty.[CounterpartyCode] AS Counterparty,
		systemtype.[TradingSystemTypeId] AS TradingSystemTypeId,
		statsPerType.ConfiguredSystems,
		--statsPerType.EnabledSystems,
		--this should be called active - and upper stay enabled
		CASE WHEN @TradingOn = 1 THEN statsPerType.ActiveSystems ELSE 0 END AS EnabledSystems,
		statsPerType.VolumeUsdM,
		statsPerType.DealsNum,
		statsPerType.RejectionsNum,
		statsPerType.PnlGrossUsdPerMUsd,
		statsPerType.CommissionsUsdPerMUsd,
		statsPerType.PnlNetUsdPerMUsd,
		statsPerType.PnlGrossUsd,
		statsPerType.CommissionsUsd,
		statsPerType.PnlNetUsd,
		allowedType.IsOrderTransmissionDisabled,
		allowedType.IsGoFlatOnlyOn,
		monitorPages.TradingSystemMonitoringPageId AS PageId,
		beginSymbol.Name AS BeginSymbol,
		endSymbol.Name AS EndSymbol,
		monitorPages.StripsCount
	FROM
	(
	SELECT 
		allowedType.TradingSystemGroupId AS TradingSystemGroupId,
		allowedType.TradingSystemTypeId AS TradingSystemTypeId,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		COALESCE(SUM(CASE WHEN action.[Enabled] = 1 AND action.[GoFlatOnly] = 0 AND action.[OrderTransmissionDisabled] = 0 THEN 1 ELSE 0 END), 0) AS ActiveSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		SUM(stats.[RejectionsNum]) AS RejectionsNum,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemTypeAllowedForGroup] allowedType_forJoinsOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = allowedType_forJoinsOnly.[TradingSystemTypeId] AND trsystem.TradingSystemGroupId = allowedType_forJoinsOnly.TradingSystemGroupId
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		--link in deleted systems if needed
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		-- but make sure that those are only the allowed ones
		LEFT JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON 
			allowedType.[TradingSystemTypeId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId])
			AND
			allowedType.[TradingSystemGroupId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemGroupId], trsystem_deleted.[TradingSystemGroupId])

	GROUP BY
		allowedType.TradingSystemGroupId, allowedType.TradingSystemTypeId
	)
	statsPerType
	INNER JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON allowedType.TradingSystemGroupId = statsPerType.TradingSystemGroupId AND allowedType.TradingSystemTypeId = statsPerType.TradingSystemTypeId
	INNER JOIN [dbo].[TradingSystemType] systemtype ON statsPerType.[TradingSystemTypeId] = systemtype.[TradingSystemTypeId]
	INNER JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	RIGHT JOIN TradingSystemGroup trgroup ON trgroup.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId
	LEFT JOIN TradingSystemMonitoringPage monitorPages ON monitorPages.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId AND monitorPages.[TradingSystemTypeId] = statsPerType.TradingSystemTypeId
	LEFT JOIN [dbo].[Symbol] beginSymbol ON monitorPages.BeginSymbolId = beginSymbol.Id
	LEFT JOIN [dbo].[Symbol] endSymbol ON monitorPages.EndSymbolId = endSymbol.Id
END
GO




CREATE TRIGGER [dbo].[SystemsTradingControl_Flipped] ON [dbo].[SystemsTradingControl] AFTER INSERT, UPDATE
AS
BEGIN
	-- commit the internal insert transaction to prevent deadlocks
	COMMIT TRANSACTION
	exec [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]
	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION
END
GO

ALTER TRIGGER [dbo].[ExternalOrdersTransmissionSwitch_Changing] ON [dbo].[ExternalOrdersTransmissionSwitch]
--needed to be able to execute sp_send_dbmail
WITH EXECUTE AS 'dbo'
AFTER INSERT, UPDATE
AS

BEGIN
	--we need to store the Id before commiting, as afterwards the inserted table will be empty
	DECLARE @updatedSwitchId INT
	SELECT TOP 1 @updatedSwitchId = [Id] FROM inserted

	-- commit the internal insert transaction to prevent problems during inserting critical data
	-- and also to make sure that following functions has fresh data
	COMMIT TRANSACTION

	--Extremely important to perform operation in try - catch to make sure that error happens just once
	begin try
	
        DECLARE @subject NVARCHAR(255) = 'OrderTrans ' + CASE WHEN [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]() = 1 THEN 'ON (' ELSE 'OFF (' END

		SELECT 
			@subject = @subject + [SwitchFriendlyName] + ' ' + CASE WHEN [TransmissionEnabled] = 1 THEN 'ON)' ELSE 'OFF)' END
		FROM
			[dbo].[ExternalOrdersTransmissionSwitch]
		WHERE
			[Id] = @updatedSwitchId

		DECLARE @body NVARCHAR(MAX) = [dbo].[GetExternalOrdersTransmissionOverviewString](@updatedSwitchId)

		EXEC msdb.dbo.sp_send_dbmail
				@recipients = 'support@kgtinv.com', 
				@profile_name = 'SQL Mail Profile',
				@subject = @subject,
				@body = @body;
    end try
    begin catch
        --select 0;
    end catch;

	exec [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]
	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION

END



ALTER PROCEDURE [dbo].[GetTradingSystemsOveralDailyStats_SP]
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @TransmissionEnabled BIT = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()

	DECLARE @GoFlatOnlyOn BIT
	SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]

	DECLARE @TradingOn BIT = @TransmissionEnabled & ~@GoFlatOnlyOn	
	
	SELECT 
		ISNULL(counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName], 'Unknown') AS SystemName,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		--COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		--this should be called active - and upper stay enabled
		CASE WHEN @TradingOn = 1 
			THEN 
			COALESCE(SUM(CASE WHEN action.[Enabled] = 1 AND action.[GoFlatOnly] = 0 AND action.[OrderTransmissionDisabled] = 0 THEN 1 ELSE 0 END), 0)
			ELSE 
			0 
			END AS EnabledSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		SUM(stats.[RejectionsNum]) AS RejectionsNum,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemType] systemtype_forJoinOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = systemtype_forJoinOnly.[TradingSystemTypeId]
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystemType] systemtype ON systemtype.[TradingSystemTypeId] = COALESCE(trsystem.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId], systemtype_forJoinOnly.[TradingSystemTypeId])
		LEFT JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	GROUP BY
		counterparty.[CounterpartyCode], systemtype.[SystemName]
	ORDER BY
		SystemName
END
GO


--ROLLBACK TRANSACTION Install_03_10_001;
--GO
--COMMIT TRANSACTION Install_03_10_001;
--GO