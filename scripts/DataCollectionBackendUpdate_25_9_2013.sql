
/****** Object:  Table [dbo].[MarketData]    Script Date: 27. 9. 2013 15:02:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarketData](
	[IntegratorPriceIdentity] [uniqueidentifier] NULL,
	[IntegratorReceivedTimeUtc] [datetime2] NOT NULL,
	[CounterpartySentTimeUtc] [datetime2] NULL,
	[SideId] [bit] NULL,
	[FXPairId] [tinyint] NOT NULL,
	[Price] [decimal](18, 9) NULL,
	[Size] [decimal](18, 0) NULL,
	[MinimumSize] [decimal](18, 0) NULL,
	[Granularity] [decimal](18, 0) NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[RecordTypeId] [tinyint] NOT NULL,
	[TradeSideId] [bit] NULL,
	[IsLastInContinuousData] [bit] NULL,
	[CounterpartyPriceIdentity] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

GRANT INSERT ON [dbo].[MarketData] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT SELECT ON [dbo].[MarketData] TO [IntegratorServiceAccount] AS [dbo]
GO

GRANT EXECUTE ON [dbo].[GetAllFXPairs] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[GetLiqProviderStreamsInfo] TO [IntegratorServiceAccount] AS [dbo]
GO

/****** Object:  Table [dbo].[MarketDataRecordType]    Script Date: 27. 9. 2013 15:02:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarketDataRecordType](
	[RecordTypeId] [tinyint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MarketDataRecordType] PRIMARY KEY CLUSTERED 
(
	[RecordTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

GO
/****** Object:  Table [dbo].[PriceSide]    Script Date: 25. 9. 2013 12:58:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PriceSide](
	[PriceSideId] [bit] NOT NULL,
	[PriceSideName] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_PriceSide] PRIMARY KEY CLUSTERED 
(
	[PriceSideId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Index [IX_PairTime]    Script Date: 27. 9. 2013 15:02:43 ******/
CREATE CLUSTERED INDEX [IX_PairTime] ON [dbo].[MarketData]
(
	[FXPairId] ASC,
	[IntegratorReceivedTimeUtc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_IntegratorPriceIdentity]    Script Date: 27. 9. 2013 15:19:31 ******/
CREATE NONCLUSTERED INDEX [IX_IntegratorPriceIdentity] ON [dbo].[MarketData]
(
	[IntegratorPriceIdentity] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_FxPair]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_LiqProviderStream] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[LiqProviderStream] ([LiqProviderStreamId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_LiqProviderStream]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_MarketDataRecordType] FOREIGN KEY([RecordTypeId])
REFERENCES [dbo].[MarketDataRecordType] ([RecordTypeId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_MarketDataRecordType]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_PriceSide] FOREIGN KEY([SideId])
REFERENCES [dbo].[PriceSide] ([PriceSideId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_PriceSide]
GO
ALTER TABLE [dbo].[MarketData]  WITH CHECK ADD  CONSTRAINT [FK_MarketData_TradeSide] FOREIGN KEY([TradeSideId])
REFERENCES [dbo].[TradeSide] ([TradeSideId])
GO
ALTER TABLE [dbo].[MarketData] CHECK CONSTRAINT [FK_MarketData_TradeSide]
GO


INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (0, N'Bank Quote Data')
GO
INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (1, N'Single Quote Cancel')
GO
INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (2, N'StreamProvider Stream Cancellation')
GO
INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (3, N'Venue New Order')
GO
INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (4, N'Venue Order Change')
GO
INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (5, N'Venue Order Cancel')
GO
INSERT [dbo].[MarketDataRecordType] ([RecordTypeId], [Description]) VALUES (6, N'Venue Trade Data')
GO

GO
INSERT [dbo].[PriceSide] ([PriceSideId], [PriceSideName]) VALUES (0, N'Bid')
GO
INSERT [dbo].[PriceSide] ([PriceSideId], [PriceSideName]) VALUES (1, N'Ask')
GO