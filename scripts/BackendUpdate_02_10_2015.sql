SET XACT_ABORT ON 
BEGIN TRANSACTION AAA

INSERT INTO [dbo].[OrderType] ([OrderTypeId], [OrderTypeName]) VALUES (6, 'LimitTillTimeout')
GO

ALTER TABLE [dbo].[TradingSystemType] ADD [Enabled] BIT NULL
GO
UPDATE [dbo].[TradingSystemType] SET [Enabled] = 1
GO
ALTER TABLE [dbo].[TradingSystemType] ALTER COLUMN [Enabled] BIT NOT NULL
GO

UPDATE [dbo].[TradingSystemType] SET [Enabled] = 0
WHERE [SystemName] IN ('Cross', 'MM', 'LLCross')
GO

exec sp_rename '[dbo].[TradingSystemType]', 'TradingSystemType_Internal'
GO

CREATE VIEW [dbo].[TradingSystemType]
AS
SELECT
	[TradingSystemTypeId]
	,[CounterpartyId]
	,[SystemName]
FROM 
	[dbo].[TradingSystemType_Internal]
WHERE
	[Enabled] = 1
GO

ALTER TABLE [dbo].[TradingSystemType_Internal] ALTER COLUMN SystemName VARCHAR(16) NOT NULL
GO
ALTER TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily] ALTER COLUMN TradingSystemTypeName VARCHAR(20) NULL
GO

ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	DECLARE @TempTradingGroupsStatistics TABLE
	(
		[Rank] [int] NOT NULL,
		[TradingSystemGroupName] [NVARCHAR](32) NOT NULL,
		[TradingSystemGroupId] [int]  NOT NULL,
		[TradingSystemTypeName] [nvarchar](20) NULL,
		[Counterparty] [char](3) NULL,
		[TradingSystemTypeId] [tinyint] NULL,
		[ConfiguredSystemsCount] int NULL,
		[EnabledSystemsCount] int NULL,
		[HardBlockedSystemsCount] int NULL,
		[VolumeUsdM] [decimal](18, 6) NULL,
		[DealsNum] [int] NULL,
		[LastDealUtc] [datetime2](7) NULL,
		[KGTRejectionsNum] [int] NULL,
		[LastKgtRejectionUtc] [datetime2](7) NULL,
		[CtpRejectionsNum] [int] NULL,
		[LastCptRejectionUtc] [datetime2](7) NULL,
		[PnlGrossUsdPerMUsd] [decimal](18, 6) NULL,
		[CommissionsUsdPerMUsd] [decimal](18, 6) NULL,
		[PnlNetUsdPerMUsd] [decimal](18, 6) NULL,
		[PnlGrossUsd] [decimal](18, 6) NULL,
		[CommissionsUsd] [decimal](18, 6) NULL,	
		[PnlNetUsd] [decimal](18, 6) NULL,
		[IsOrderTransmissionDisabled] [bit] NULL,
		[IsGoFlatOnlyOn] [bit] NULL,
		[PageId] [int] NULL,
		[BeginSymbol] [nchar](7) NULL,
		[EndSymbol] [nchar](7) NULL,
		[StripsCount] INT
	)

	INSERT INTO 
		@TempTradingGroupsStatistics
		([Rank]
		,[TradingSystemGroupName]
		,[TradingSystemGroupId]
		,[TradingSystemTypeName]
		,[Counterparty]
		,[TradingSystemTypeId]
		,[ConfiguredSystemsCount]
		,[EnabledSystemsCount]
		,[HardBlockedSystemsCount]
		,[VolumeUsdM]
		,[DealsNum]
		,[LastDealUtc]
		,[KGTRejectionsNum]
		,[LastKgtRejectionUtc]
		,[CtpRejectionsNum]
		,[LastCptRejectionUtc]
		,[PnlGrossUsdPerMUsd]
		,[CommissionsUsdPerMUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsd]
		,[PnlNetUsd]
		,[IsOrderTransmissionDisabled]
		,[IsGoFlatOnlyOn]
		,[PageId]
		,[BeginSymbol]
		,[EndSymbol]
		,[StripsCount])
    exec [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingGroupsDaily]
			([Rank]
			,[TradingSystemGroupName]
			,[TradingSystemGroupId]
			,[TradingSystemTypeName]
			,[Counterparty]
			,[TradingSystemTypeId]
			,[ConfiguredSystemsCount]
			,[EnabledSystemsCount]
			,[HardBlockedSystemsCount]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[CommissionsUsdPerMUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsd]
			,[PnlNetUsd]
			,[IsOrderTransmissionDisabled]
			,[IsGoFlatOnlyOn]
			,[PageId]
			,[BeginSymbol]
			,[EndSymbol]
			,[StripsCount])
		SELECT
			[Rank]
			,[TradingSystemGroupName]
			,[TradingSystemGroupId]
			,[TradingSystemTypeName]
			,[Counterparty]
			,[TradingSystemTypeId]
			,[ConfiguredSystemsCount]
			,[EnabledSystemsCount]
			,[HardBlockedSystemsCount]
			,[VolumeUsdM]
			,[DealsNum]
			,[LastDealUtc]
			,[KGTRejectionsNum]
			,[LastKgtRejectionUtc]
			,[CtpRejectionsNum]
			,[LastCptRejectionUtc]
			,[PnlGrossUsdPerMUsd]
			,[CommissionsUsdPerMUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsd]
			,[PnlNetUsd]
			,[IsOrderTransmissionDisabled]
			,[IsGoFlatOnlyOn]
			,[PageId]
			,[BeginSymbol]
			,[EndSymbol]
			,[StripsCount]
		FROM
			@TempTradingGroupsStatistics
	COMMIT
END
GO

INSERT INTO [dbo].[TradingSystemType_Internal] ([CounterpartyId], [SystemName], [Enabled])
   VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'HTF'), 'SmartCross', 1)
GO

TRUNCATE TABLE [dbo].[VenueLLCrossSettings]
TRUNCATE TABLE [dbo].[VenueLLCrossSettings_Changes]
exec sp_rename '[dbo].[VenueLLCrossSettings]', 'VenueSmartCrossSettings'
exec sp_rename '[dbo].[VenueLLCrossSettings_Changes]', 'VenueSmartCrossSettings_Changes'
GO

exec sp_rename '[dbo].[VenueSmartCrossSettings].[MinimumGrossGainBasisPointsToCover]', 'CoverDistanceGrossBasisPoints'
exec sp_rename '[dbo].[VenueSmartCrossSettings].[CoverAttemptsCutoffSpan_milliseconds]', 'CoverTimeout_milliseconds'

exec sp_rename '[dbo].[VenueSmartCrossSettings_Changes].[MinimumGrossGainBasisPointsToCover]', 'CoverDistanceGrossBasisPoints'
exec sp_rename '[dbo].[VenueSmartCrossSettings_Changes].[CoverAttemptsCutoffSpan_milliseconds]', 'CoverTimeout_milliseconds'
GO

ALTER TABLE [dbo].[VenueSmartCrossSettings] DROP COLUMN SoftPriceTtl_milliseconds
ALTER TABLE [dbo].[VenueSmartCrossSettings_Changes] DROP COLUMN SoftPriceTtl_milliseconds
GO


exec sp_rename '[dbo].[LastDeletedTrigger_VenueLLCrossSettings]', 'LastDeletedTrigger_VenueSmartCrossSettings'
exec sp_rename '[dbo].[LastUpdatedTrigger_VenueLLCrossSettings]', 'LastUpdatedTrigger_VenueSmartCrossSettings'
exec sp_rename '[dbo].[VenueLLCrossSettings_DeletedOrUpdated]', 'VenueSmartCrossSettings_DeletedOrUpdated'
GO

UPDATE [dbo].[TableChangeLog] SET [TableName] = 'VenueSmartCrossSettings' WHERE [TableName] = 'VenueLLCrossSettings'
GO

ALTER TRIGGER [dbo].[LastDeletedTrigger_VenueSmartCrossSettings] ON [dbo].[VenueSmartCrossSettings]
AFTER DELETE
AS
BEGIN
	--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
	SET NOCOUNT ON
	UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueSmartCrossSettings'
END
GO

ALTER TRIGGER [dbo].[LastUpdatedTrigger_VenueSmartCrossSettings] ON [dbo].[VenueSmartCrossSettings]
AFTER INSERT, UPDATE
AS
BEGIN
	--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
	SET NOCOUNT ON

	Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
	triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

	UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueSmartCrossSettings'
END
GO

ALTER TRIGGER [dbo].[VenueSmartCrossSettings_DeletedOrUpdated] ON [dbo].[VenueSmartCrossSettings] AFTER DELETE, UPDATE
AS
BEGIN
	--Use select with nested query so that it implicitly handles multideletes (deleted is table - potentialy with multiple records)
	INSERT INTO 
		[dbo].[VenueSmartCrossSettings_Changes]
	   ([SymbolId]
	   ,[MinimumSizeToTrade]
	   ,[MaximumSizeToTrade]
	   ,[MinimumDiscountBasisPointsToTrade]
	   ,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
	   ,[MinimumFillSize]
		,[CoverDistanceGrossBasisPoints]
		,[CoverTimeout_milliseconds]
	   ,[CounterpartyId]
	   ,[TradingSystemId]
	   ,[ValidFromUtc]
	   ,[ValidToUtc]
	   ,[Deleted])
	SELECT 
		deleted.[SymbolId]
		,deleted.[MinimumSizeToTrade]
		,deleted.[MaximumSizeToTrade]
		,deleted.[MinimumDiscountBasisPointsToTrade]
		,deleted.[MinimumTimeBetweenSingleCounterpartySignals_seconds]
		,deleted.[MinimumFillSize]
		,deleted.[CoverDistanceGrossBasisPoints]
		,deleted.[CoverTimeout_milliseconds]
		,deleted.[CounterpartyId]
		,deleted.[TradingSystemId]
		,ISNULL(topUpdatedChange.[ValidToUtc], CAST('0001-01-01 00:00:00.0000000' as datetime2(7)))
		,GETUTCDATE()
		,CASE WHEN EXISTS(SELECT * FROM inserted) THEN 0 ELSE 1 END
	FROM 
		deleted
		LEFT JOIN [dbo].[VenueSmartCrossSettings_Changes] topUpdatedChange 
			ON deleted.[TradingSystemId] = topUpdatedChange.[TradingSystemId]
			AND topUpdatedChange.[ValidToUtc] = (SELECT MAX([ValidToUtc]) FROM [dbo].[VenueCrossSettings_Changes] WHERE [TradingSystemId] = deleted.[TradingSystemId])
	WHERE
		NOT EXISTS(SELECT * FROM inserted)
		--prevent insertion of a new record that is actually no change (either by no-effect update, or by update in different dimension - e.g. disable/enable)
		--ALSO - BEWARE of nested triggers and triggers on tables updating self - those can cause multirecords inserts - therefore use UPDATE() function (in connection with manual check - to prevent no change inserts)
		OR (UPDATE([SymbolId]) AND (topUpdatedChange.[SymbolId] IS NULL OR topUpdatedChange.[SymbolId] != deleted.[SymbolId]))
		OR (UPDATE([MinimumSizeToTrade]) AND (topUpdatedChange.[MinimumSizeToTrade] IS NULL OR topUpdatedChange.[MinimumSizeToTrade] != deleted.[MinimumSizeToTrade]))
		OR (UPDATE([MaximumSizeToTrade]) AND (topUpdatedChange.[MaximumSizeToTrade] IS NULL OR topUpdatedChange.[MaximumSizeToTrade] != deleted.[MaximumSizeToTrade]))
		OR (UPDATE([MinimumDiscountBasisPointsToTrade]) AND (topUpdatedChange.[MinimumDiscountBasisPointsToTrade] IS NULL OR topUpdatedChange.[MinimumDiscountBasisPointsToTrade] != deleted.[MinimumDiscountBasisPointsToTrade]))
		OR (UPDATE([MinimumTimeBetweenSingleCounterpartySignals_seconds]) AND (topUpdatedChange.[MinimumTimeBetweenSingleCounterpartySignals_seconds] IS NULL OR topUpdatedChange.[MinimumTimeBetweenSingleCounterpartySignals_seconds] != deleted.[MinimumTimeBetweenSingleCounterpartySignals_seconds]))
		OR (UPDATE([MinimumFillSize]) AND (topUpdatedChange.[MinimumFillSize] IS NULL OR topUpdatedChange.[MinimumFillSize] != deleted.[MinimumFillSize]))
		OR (UPDATE([CoverDistanceGrossBasisPoints]) AND (topUpdatedChange.[CoverDistanceGrossBasisPoints] IS NULL OR topUpdatedChange.[CoverDistanceGrossBasisPoints] != deleted.[CoverDistanceGrossBasisPoints]))
		OR (UPDATE([CoverTimeout_milliseconds]) AND (topUpdatedChange.[CoverTimeout_milliseconds] IS NULL OR topUpdatedChange.[CoverTimeout_milliseconds] != deleted.[CoverTimeout_milliseconds]))
END
GO

ALTER PROCEDURE [dbo].[VenueSystemDeleteMulti_SP]
(
	@TradingSystemId IdsTable READONLY
)
AS
BEGIN

	BEGIN TRANSACTION;
	
	DECLARE @DeletedCnt INT = 0;
	
	DELETE sett
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueSmartCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
		

	DELETE FROM
		[dbo].[TradingSystemActions]
	WHERE 
		[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
		
	DELETE FROM 
		[dbo].[TradingSystem]
	WHERE 
		[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	COMMIT TRANSACTION;
END
GO

ALTER PROCEDURE [dbo].[VenueSystemDeleteOne_SP]
(
	@TradingSystemId int
)
AS
BEGIN

	BEGIN TRANSACTION;
	
	DECLARE @DeletedCnt INT = 0;
	
	DELETE sett
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueStreamSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
		
	DELETE sett
	FROM
		[dbo].[VenueGliderSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueSmartCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	IF @DeletedCnt > 0
	BEGIN
		DELETE FROM
			[dbo].[TradingSystemActions]
		WHERE 
			[TradingSystemId] = @TradingSystemId
			
		DELETE FROM 
			[dbo].[TradingSystem]
		WHERE 
			[TradingSystemId] = @TradingSystemId
	END
	
	
	COMMIT TRANSACTION;
END
GO

exec sp_rename '[dbo].[GetLastVenueLLCrossSettings_SP]', 'GetLastVenueSmartCrossSettings_SP'
GO

ALTER PROCEDURE [dbo].[GetLastVenueSmartCrossSettings_SP]
AS
BEGIN
	SELECT TOP 1
		[MinimumSizeToTrade]
		,[MaximumSizeToTrade]
		,[MinimumDiscountBasisPointsToTrade]
		,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
		,[MinimumFillSize]
		,[CoverDistanceGrossBasisPoints]
		,[CoverTimeout_milliseconds]
	FROM 
		[dbo].[VenueSmartCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	ORDER BY
		action.SettingsLastUpdatedUtc DESC
END
GO

exec sp_rename '[dbo].[GetUpdatedVenueLLCrossSystemSettings_SP]', 'GetUpdatedVenueSmartCrossSystemSettings_SP'
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueSmartCrossSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@TradingSystemId [int] = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueSmartCrossSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,system.TradingSystemGroupId
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
			,[MinimumFillSize]
			,[CoverDistanceGrossBasisPoints]
			,[CoverTimeout_milliseconds]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueSmartCrossSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
			AND
			(@TradingSystemId IS NULL OR sett.[TradingSystemId] = @TradingSystemId)
		ORDER BY
			symbol.Name
	END
END
GO

exec sp_rename '[dbo].[GetVenueLLCrossSystemSettingsAndStats_SP]', 'GetVenueSmartCrossSystemSettingsAndStats_SP'
GO

ALTER PROCEDURE [dbo].[GetVenueSmartCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[LastDealUtc],
		stats.[KGTRejectionsNum],
		stats.[LastKgtRejectionUtc],
		stats.[CtpRejectionsNum],
		stats.[LastCptRejectionUtc],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumDiscountBasisPointsToTrade] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [MinimumDiscountToTradeDecimal],
		sett.[MinimumTimeBetweenSingleCounterpartySignals_seconds],
		sett.[MinimumFillSize],
		sett.[CoverDistanceGrossBasisPoints],
		sett.[CoverDistanceGrossBasisPoints] * symbol.LastKnownMeanReferencePrice / 10000.0 AS [CoverDistanceGrossDecimal],	
		sett.[CoverTimeout_milliseconds],
		action.[Enabled],
		action.[HardBlocked],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueSmartCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

exec sp_rename '[dbo].[VenueLLCrossSettingsInsertSingle_SP]', 'VenueSmartCrossSettingsInsertSingle_SP'
GO

ALTER PROCEDURE [dbo].[VenueSmartCrossSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@Enabled [bit],
	@MinimumSizeToTrade [decimal](18, 0),
	@MaximumSizeToTrade [decimal](18, 0),
	@MinimumDiscountBasisPointsToTrade [decimal](18, 6),
	@MinimumTimeBetweenSingleCounterpartySignals_seconds [decimal](18, 9),
	@MinimumFillSize [decimal](18, 0),
	@CoverDistanceGrossBasisPoints [decimal](18, 6),
	@CoverTimeout_milliseconds [INT],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'SmartCross'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'SmartCross TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueSmartCrossSettings]
			([SymbolId]
			,[CounterpartyId]
			,[TradingSystemId]
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenSingleCounterpartySignals_seconds]
			,[MinimumFillSize]
			,[CoverDistanceGrossBasisPoints]
			,[CoverTimeout_milliseconds])
     VALUES
			(@SymbolId
			,@CounterpartyId
			,@TradingSystemId
			,@MinimumSizeToTrade
			,@MaximumSizeToTrade
			,@MinimumDiscountBasisPointsToTrade
			,@MinimumTimeBetweenSingleCounterpartySignals_seconds
			,@MinimumFillSize
			,@CoverDistanceGrossBasisPoints
			,@CoverTimeout_milliseconds)
END
GO

exec sp_rename '[dbo].[VenueLLCrossSettingsUpdateSingle_SP]', 'VenueSmartCrossSettingsUpdateSingle_SP'
GO

ALTER PROCEDURE [dbo].[VenueSmartCrossSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MinimumSizeToTrade [decimal](18, 0),
	@MaximumSizeToTrade [decimal](18, 0),
	@MinimumDiscountBasisPointsToTrade [decimal](18, 6),
	@MinimumTimeBetweenSingleCounterpartySignals_seconds [decimal](18, 9),
	@MinimumFillSize [decimal](18, 0),
	@CoverDistanceGrossBasisPoints [decimal](18, 6),
	@CoverTimeout_milliseconds [INT]
)
AS
BEGIN
	UPDATE sett
	SET
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[MinimumDiscountBasisPointsToTrade] = @MinimumDiscountBasisPointsToTrade,
		[MinimumTimeBetweenSingleCounterpartySignals_seconds] = @MinimumTimeBetweenSingleCounterpartySignals_seconds,
		[MinimumFillSize] = @MinimumFillSize,
		[CoverDistanceGrossBasisPoints] = @CoverDistanceGrossBasisPoints,
		[CoverTimeout_milliseconds] = @CoverTimeout_milliseconds
	FROM
		[dbo].[VenueSmartCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

INSERT INTO [dbo].[TradingSystemType_Internal] ([CounterpartyId], [SystemName], [Enabled])
   VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'LX1'), 'MM', 1)
GO



------------
-- Credit lines improvements
-----------

CREATE PROCEDURE [dbo].[GetCreditLineCptMap_SP]
AS
BEGIN
	SELECT 
		creditLine.CreditLineName
		,cpt.CounterpartyCode AS Counterparty
	FROM 
		[dbo].[PrimeBrokerCreditLines_CptMapping] map
		INNER JOIN [dbo].[PrimeBrokerCreditLine] creditLine ON map.CreditLineId = creditLine.CreditLineId
		INNER JOIN [dbo].[Counterparty] cpt ON map.[CounterpartyId] = cpt.[CounterpartyId]
	ORDER BY
		creditLine.CreditLineName ASC,
		cpt.CounterpartyCode ASC
END
GO

GRANT EXECUTE ON [dbo].[GetCreditLineCptMap_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER TABLE [dbo].[DealExternalExecutedUnsettled] ADD DealsCount [int] NULL
GO
UPDATE [dbo].[DealExternalExecutedUnsettled] SET DealsCount = 1
GO
ALTER TABLE [dbo].[DealExternalExecutedUnsettled] ALTER COLUMN DealsCount [int] NOT NULL
GO

exec sp_rename '[dbo].[PrimeBrokerCreditLine].[MaxUnsettledNopUsd]', 'MaxUnsettledNopUsdTotal'
exec sp_rename '[dbo].[PrimeBrokerCreditLine].[MaxUnsettledNopUsdToTurnGFO]', 'MaxUnsettledNopUsdTotalToTurnGFO'
GO

ALTER TABLE [dbo].[PrimeBrokerCreditLine] ADD [MaxUnsettledNopUsdPerValueDate] [decimal](18, 2) NULL
ALTER TABLE [dbo].[PrimeBrokerCreditLine] ADD [MaxUnsettledNopUsdPerValueDateToTurnGFO] [decimal](18, 2) NULL
GO
UPDATE [dbo].[PrimeBrokerCreditLine] SET MaxUnsettledNopUsdPerValueDate = 0, MaxUnsettledNopUsdPerValueDateToTurnGFO = 0
GO
ALTER TABLE [dbo].[PrimeBrokerCreditLine] ALTER COLUMN MaxUnsettledNopUsdPerValueDate [decimal](18, 2) NOT NULL
ALTER TABLE [dbo].[PrimeBrokerCreditLine] ALTER COLUMN MaxUnsettledNopUsdPerValueDateToTurnGFO [decimal](18, 2) NOT NULL
GO

exec sp_rename '[dbo].[PersistedSettlementStatisticsPerCreditLine].[UnsettledNopUsd]', 'UnsettledNopUsdTotal'
exec sp_rename '[dbo].[PersistedSettlementStatisticsPerCreditLine].[MaxUnsettledNopUsd]', 'MaxUnsettledNopUsdTotal'
exec sp_rename '[dbo].[PersistedSettlementStatisticsPerCreditLine].[MaxUnsettledNopUsdToTurnGFO]', 'MaxUnsettledNopUsdTotalToTurnGFO'
GO

ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [DealsCountTotal] [int] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [MaxUnsettledNopUsdPerValueDate] [decimal](18, 2) NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [MaxUnsettledNopUsdPerValueDateToTurnGFO] [decimal](18, 2) NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [T1] [DATE] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [T2] [DATE] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [T3] [DATE] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [T4] [DATE] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [T5] [DATE] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [T6] [DATE] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [T7] [DATE] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [T8] [DATE] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [UnsettledNopUsd_T1] [decimal](18, 2) NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [UnsettledNopUsd_T2] [decimal](18, 2) NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [UnsettledNopUsd_T3] [decimal](18, 2) NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [UnsettledNopUsd_T4] [decimal](18, 2) NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [UnsettledNopUsd_T5] [decimal](18, 2) NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [UnsettledNopUsd_T6] [decimal](18, 2) NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [UnsettledNopUsd_T7] [decimal](18, 2) NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [UnsettledNopUsd_T8] [decimal](18, 2) NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [DealsCount_T1] [int] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [DealsCount_T2] [int] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [DealsCount_T3] [int] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [DealsCount_T4] [int] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [DealsCount_T5] [int] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [DealsCount_T6] [int] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [DealsCount_T7] [int] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [DealsCount_T8] [int] NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [HighestUnsettledNopInSingleT] [decimal](18, 2) NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ADD [CreditLineId] [tinyint] NULL
GO

UPDATE [dbo].[PersistedSettlementStatisticsPerCreditLine] 
SET 
	MaxUnsettledNopUsdPerValueDate = 0,
	MaxUnsettledNopUsdPerValueDateToTurnGFO =0,
	[CreditLineId] =0
GO
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ALTER COLUMN MaxUnsettledNopUsdPerValueDate [decimal](18, 2) NOT NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ALTER COLUMN MaxUnsettledNopUsdPerValueDateToTurnGFO [decimal](18, 2) NOT NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ALTER COLUMN UnsettledNopUsdTotal [decimal](18, 2) NULL
ALTER TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine] ALTER COLUMN [CreditLineId] [tinyint] NOT NULL
GO


ALTER PROCEDURE [GetSettlementStatisticsPerCreditLine_Cached_SP]
(
	@Force BIT = 0
)
AS
BEGIN

	exec UpdateSettlementStatisticsPerCreditLine_SP @Force=@Force
	
	SELECT 
		[PrimeBrokerName]
		,[CreditLineName]
		,[CreditLineId]
		,[MaxUnsettledNopUsdTotal]
		,[MaxUnsettledNopUsdTotalToTurnGFO]
		,[MaxUnsettledNopUsdPerValueDate]
		,[MaxUnsettledNopUsdPerValueDateToTurnGFO]
		,[UnsettledNopUsdTotal]
		,[DealsCountTotal]
		,[T1]
		,[T2]
		,[T3]
		,[T4]
		,[T5]
		,[T6]
		,[T7]
		,[T8]
		,[UnsettledNopUsd_T1]
		,[UnsettledNopUsd_T2]
		,[UnsettledNopUsd_T3]
		,[UnsettledNopUsd_T4]
		,[UnsettledNopUsd_T5]
		,[UnsettledNopUsd_T6]
		,[UnsettledNopUsd_T7]
		,[UnsettledNopUsd_T8]
		,[DealsCount_T1]
		,[DealsCount_T2]
		,[DealsCount_T3]
		,[DealsCount_T4]
		,[DealsCount_T5]
		,[DealsCount_T6]
		,[DealsCount_T7]
		,[DealsCount_T8]	
		,[HighestUnsettledNopInSingleT]
	FROM 
		[dbo].[PersistedSettlementStatisticsPerCreditLine]
	ORDER BY
		[PrimeBrokerName] ASC
		,[CreditLineName] ASC

END
GO



ALTER PROCEDURE [dbo].[PrimeBrokerCreditLine_UpdateLimit_SP]
(
	@CreditLineId tinyint,
	@NewUnsettledLimitUsdTotal decimal(18,2),
	@NewUnsettledLimitUsdPerValueDate decimal(18,2)
)
AS
BEGIN
	UPDATE 
		[dbo].[PrimeBrokerCreditLine]
	SET
		[MaxUnsettledNopUsdTotal] = @NewUnsettledLimitUsdTotal
		,[MaxUnsettledNopUsdTotalToTurnGFO] = CASE WHEN @NewUnsettledLimitUsdTotal - 2000000 > 0 THEN @NewUnsettledLimitUsdTotal - 2000000 ELSE 0 END
		,[MaxUnsettledNopUsdPerValueDate] = @NewUnsettledLimitUsdPerValueDate
		,[MaxUnsettledNopUsdPerValueDateToTurnGFO] = CASE WHEN @NewUnsettledLimitUsdPerValueDate - 2000000 > 0 THEN @NewUnsettledLimitUsdPerValueDate - 2000000 ELSE 0 END
	WHERE
		[CreditLineId] = @CreditLineId
END
GO


ALTER PROCEDURE [UpdateSettlementStatisticsPerCreditLine_SP]
(
	@Force BIT = 0
)
AS
BEGIN

	UPDATE 
		[dbo].[TableChangeLog] 
	SET 
		[LastUpdatedUtc] = GETUTCDATE() 
	WHERE 
		[TableName] = 'SettlementStatistics' 
		AND 
		(@Force = 1 OR [LastUpdatedUtc] < DATEADD(SECOND, -2, GETUTCDATE()))

	IF @@ROWCOUNT > 0
	BEGIN

				
		DECLARE @temp_final_result TABLE
		(
			[PrimeBrokerName] [varchar](10),
			[CreditLineName] [varchar](255),
			[UnsettledNopUsdTotal] [decimal](18, 2),
			[DealsCountTotal] [int],
			[MaxUnsettledNopUsdTotal] [decimal](18, 2),
			[MaxUnsettledNopUsdTotalToTurnGFO] [decimal](18, 2),
			[MaxUnsettledNopUsdPerValueDate] [decimal](18, 2),
			[MaxUnsettledNopUsdPerValueDateToTurnGFO] [decimal](18, 2),
			T1 [date],
			T2 [date],
			T3 [date],
			T4 [date],
			T5 [date],
			T6 [date],
			T7 [date],
			T8 [date],
			UnsettledNopUsd_T1 [decimal](18, 2),
			UnsettledNopUsd_T2 [decimal](18, 2),
			UnsettledNopUsd_T3 [decimal](18, 2),
			UnsettledNopUsd_T4 [decimal](18, 2),
			UnsettledNopUsd_T5 [decimal](18, 2),
			UnsettledNopUsd_T6 [decimal](18, 2),
			UnsettledNopUsd_T7 [decimal](18, 2),
			UnsettledNopUsd_T8 [decimal](18, 2),
			[DealsCount_T1] [int],
			[DealsCount_T2] [int],
			[DealsCount_T3] [int],
			[DealsCount_T4] [int],
			[DealsCount_T5] [int],
			[DealsCount_T6] [int],
			[DealsCount_T7] [int],
			[DealsCount_T8] [int],
			[HighestUnsettledNopInSingleT] [decimal](18, 2),
			[CreditLineId] [tinyint]
		)
		
		
		DECLARE @temp_NOPs_pol TABLE
		(
			CreditLineId tinyint,
			BaseCurrencyId smallint,
			QuoteCurrencyId smallint,
			NopBasePol decimal(18,2),
			NopTermPol decimal(18,4),
			[DealsCount] int,
			[ValueDate] Date
		)
		
		DECLARE @temp_intermediate_result TABLE
		(
			CreditLineId tinyint,
			[UnsettledNopUsd] [decimal](18, 2),
			[DealsCount] [int],
			[ValueDate] Date
		)

		INSERT INTO
			@temp_NOPs_pol
			(
				CreditLineId,
				BaseCurrencyId,
				QuoteCurrencyId,
				NopBasePol,
				NopTermPol,
				DealsCount,
				ValueDate
			)
		SELECT 
			[CreditLineId]
			,MAX(sym.BaseCurrencyId) AS BaseCurrencyId
			,MAX(sym.QuoteCurrencyId) AS QuoteCurrencyId
			,SUM([SizeBasePol]) AS [NopBasePol]
			,SUM([SizeTermPol]) AS [NopTermPol]
			,SUM(DealsCount) AS [DealsCount]
			,SettlementDateLocalMkt AS ValueDate
			--,COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate]) AS [SettlementDate]
		FROM
			[dbo].[DealExternalExecutedUnsettled] unsettled
			INNER JOIN [dbo].[PrimeBrokerCreditLines_CptMapping] creditLineMap ON unsettled.[CounterpartyId] = creditLineMap.[CounterpartyId]
			INNER JOIN Symbol sym ON sym.ID = unsettled.[SymbolId]
			LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateCurrencyMatch 
						ON settlDateCurrencyMatch.CurrencyID = sym.BaseCurrencyId OR settlDateCurrencyMatch.CurrencyID = sym.QuoteCurrencyId
			LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateOtherCurrencies 
						ON settlDateOtherCurrencies.CurrencyID IS NULL					
		WHERE
			[SettlementDateLocalMkt] >= COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate])
		GROUP BY
			[CreditLineId],
			[SymbolId],
			[SettlementDateLocalMkt]

		
		INSERT INTO
			@temp_intermediate_result
			(
				[CreditLineId],
				[UnsettledNopUsd],
				[DealsCount],
				[ValueDate]
			)
			SELECT
				CreditLineId,
				SUM(ABS(NopPolInUSD))/2 AS NopAbsInUSD,
				SUM(DealsCount) AS DealsCount,
				ValueDate
			FROM
				(
				SELECT
					CreditLineId,
					nopsUnaggreggated.CurrencyId,
					SUM(NopPol * ccy.LastKnownUsdConversionMultiplier) AS NopPolInUSD,
					SUM(DealsCount) AS DealsCount,
					ValueDate
				FROM
					(
					SELECT
						CreditLineId,
						BaseCurrencyId AS CurrencyId,
						NopBasePol AS NopPol,
						DealsCount AS DealsCount,
						ValueDate
					FROM
						@temp_NOPs_pol basePos

					UNION ALL

					SELECT
						CreditLineId,
						QuoteCurrencyId AS CurrencyId,
						NopTermPol AS NopPol,
						0 AS DealsCount,
						ValueDate
					FROM
						@temp_NOPs_pol basePos
					) nopsUnaggreggated
					INNER JOIN Currency ccy ON ccy.CurrencyID = nopsUnaggreggated.CurrencyId
				GROUP BY
					CreditLineId,
					nopsUnaggreggated.CurrencyId,
					ValueDate
				)nopsAggreggated
			GROUP BY
				CreditLineId,
				ValueDate

			DECLARE @temp_ValueDates TABLE
			(
				ValueDate Date,
				ValueDateId int
			)

			INSERT INTO @temp_ValueDates(ValueDateId, ValueDate)
			SELECT
				ROW_NUMBER() OVER (ORDER BY ValueDate),
				ValueDate
			FROM
				@temp_intermediate_result
			WHERE
				ValueDate IS NOT NULL
			GROUP BY
				ValueDate


			
			;WITH TmpStats AS
			(
				SELECT 
					CreditLineId,
					UnsettledNopUsd,
					DealsCount,
					res.ValueDate,
					'Nop_T' + CAST(ValueDateId AS VARCHAR) AS NopsValueDateTag,
					'Deals_T' + CAST(ValueDateId AS VARCHAR) AS DealsValueDateTag
			FROM
				@temp_intermediate_result res
				FULL OUTER JOIN @temp_ValueDates valDates ON res.ValueDate = valDates.ValueDate
			)

			 INSERT INTO @temp_final_result
			(
				[PrimeBrokerName]
				,[CreditLineName]
				,[CreditLineId]
				,[MaxUnsettledNopUsdTotal]
				,[MaxUnsettledNopUsdTotalToTurnGFO]
				,[MaxUnsettledNopUsdPerValueDate]
				,[MaxUnsettledNopUsdPerValueDateToTurnGFO]
				,[UnsettledNopUsdTotal]
				,[DealsCountTotal]
				,[T1]
				,[T2]
				,[T3]
				,[T4]
				,[T5]
				,[T6]
				,[T7]
				,[T8]
				,[UnsettledNopUsd_T1]
				,[UnsettledNopUsd_T2]
				,[UnsettledNopUsd_T3]
				,[UnsettledNopUsd_T4]
				,[UnsettledNopUsd_T5]
				,[UnsettledNopUsd_T6]
				,[UnsettledNopUsd_T7]
				,[UnsettledNopUsd_T8]
				,[DealsCount_T1]
				,[DealsCount_T2]
				,[DealsCount_T3]
				,[DealsCount_T4]
				,[DealsCount_T5]
				,[DealsCount_T6]
				,[DealsCount_T7]
				,[DealsCount_T8]
				,[HighestUnsettledNopInSingleT]
			)

			SELECT
				primeBroker.PrimeBrokerName,
				creditLine.[CreditLineName],
				creditLine.[CreditLineId],
				creditLine.[MaxUnsettledNopUsdTotal],
				creditLine.[MaxUnsettledNopUsdTotalToTurnGFO],
				creditLine.[MaxUnsettledNopUsdPerValueDate],
				creditLine.[MaxUnsettledNopUsdPerValueDateToTurnGFO],
				UnsettledNopUsdTotal,
				DealsCountTotal,
				T1, T2, T3, T4, T5, T6, T7, T8,
				Nop_T1, Nop_T2, Nop_T3, Nop_T4, Nop_T5, Nop_T6, Nop_T7, Nop_T8, 
				Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8,
				(SELECT MAX(x) FROM (VALUES(Nop_T1), (Nop_T2), (Nop_T3), (Nop_T4), (Nop_T5), (Nop_T6), (Nop_T7), (Nop_T8)) AS VALUE(x)) AS [HighestUnsettledNopInSingleT]
			FROM

				(
					select T1, T2, T3, T4, T5, T6, T7, T8
					from
					(
						select ValueDate, 'T' + CAST(ValueDateId AS VARCHAR) AS ValueDateTag
						FROM @temp_ValueDates
					) d
					PIVOT
					(
						MAX(ValueDate)
						for ValueDateTag IN (T1, T2, T3, T4, T5, T6, T7, T8)
					) pivDates
				)ValDates

				INNER JOIN [dbo].[PrimeBrokerCreditLine] creditLine ON 1=1
				INNER JOIN [dbo].[PrimeBroker] primeBroker ON creditLine.[PrimeBrokerId] = primeBroker.[PrimeBrokerId]

				LEFT JOIN

				(
				SELECT 
					CreditLineId,
					MAX(Nop_T1) AS Nop_T1, MAX(Nop_T2) AS Nop_T2, MAX(Nop_T3) AS Nop_T3, MAX(Nop_T4) AS Nop_T4, MAX(Nop_T5) AS Nop_T5, MAX(Nop_T6) AS Nop_T6, MAX(Nop_T7) AS Nop_T7, MAX(Nop_T8) AS Nop_T8
				FROM
					TmpStats
					PIVOT 
					(
						MAX(UnsettledNopUsd) FOR NopsValueDateTag IN (Nop_T1, Nop_T2, Nop_T3, Nop_T4, Nop_T5, Nop_T6, Nop_T7, Nop_T8)
					)pivNops
				GROUP BY
					CreditLineId
				)NopsTmp ON creditLine.CreditLineId = NopsTmp.CreditLineId

				LEFT JOIN

				(
				SELECT 
					CreditLineId,
					MAX(Deals_T1) AS Deals_T1, MAX(Deals_T2) AS Deals_T2, MAX(Deals_T3) AS Deals_T3, MAX(Deals_T4) AS Deals_T4, MAX(Deals_T5) AS Deals_T5, MAX(Deals_T6) AS Deals_T6, MAX(Deals_T7) AS Deals_T7, MAX(Deals_T8) AS Deals_T8
				FROM
					TmpStats
					PIVOT 
					(
						MAX(DealsCount) FOR DealsValueDateTag IN (Deals_T1, Deals_T2, Deals_T3, Deals_T4, Deals_T5, Deals_T6, Deals_T7, Deals_T8)
					)piv
				GROUP BY
					CreditLineId
				)DealsTmp

				ON NopsTmp.CreditLineId = DealsTmp.CreditLineId

				LEFT JOIN

				(
				SELECT
					CreditLineId,
					SUM(UnsettledNopUsd) AS UnsettledNopUsdTotal,
					SUM(DealsCount) AS DealsCountTotal
				FROM
					TmpStats
				GROUP BY
					CreditLineId
				) totalSum

				ON totalSum.CreditLineId = DealsTmp.CreditLineId

			ORDER BY
				primeBroker.PrimeBrokerName,
				creditLine.[CreditLineName]
			
			
		BEGIN TRANSACTION
			TRUNCATE TABLE [dbo].[PersistedSettlementStatisticsPerCreditLine]
		
			INSERT INTO
				[dbo].[PersistedSettlementStatisticsPerCreditLine]
				(
					[PrimeBrokerName]
					,[CreditLineName]
					,[CreditLineId]
					,[MaxUnsettledNopUsdTotal]
					,[MaxUnsettledNopUsdTotalToTurnGFO]
					,[MaxUnsettledNopUsdPerValueDate]
					,[MaxUnsettledNopUsdPerValueDateToTurnGFO]
					,[UnsettledNopUsdTotal]
					,[DealsCountTotal]
					,[T1]
					,[T2]
					,[T3]
					,[T4]
					,[T5]
					,[T6]
					,[T7]
					,[T8]
					,[UnsettledNopUsd_T1]
					,[UnsettledNopUsd_T2]
					,[UnsettledNopUsd_T3]
					,[UnsettledNopUsd_T4]
					,[UnsettledNopUsd_T5]
					,[UnsettledNopUsd_T6]
					,[UnsettledNopUsd_T7]
					,[UnsettledNopUsd_T8]
					,[DealsCount_T1]
					,[DealsCount_T2]
					,[DealsCount_T3]
					,[DealsCount_T4]
					,[DealsCount_T5]
					,[DealsCount_T6]
					,[DealsCount_T7]
					,[DealsCount_T8]
					,[HighestUnsettledNopInSingleT]
				)
			SELECT
				[PrimeBrokerName]
				,[CreditLineName]
				,[CreditLineId]
				,[MaxUnsettledNopUsdTotal]
				,[MaxUnsettledNopUsdTotalToTurnGFO]
				,[MaxUnsettledNopUsdPerValueDate]
				,[MaxUnsettledNopUsdPerValueDateToTurnGFO]
				,[UnsettledNopUsdTotal]
				,[DealsCountTotal]
				,[T1]
				,[T2]
				,[T3]
				,[T4]
				,[T5]
				,[T6]
				,[T7]
				,[T8]
				,[UnsettledNopUsd_T1]
				,[UnsettledNopUsd_T2]
				,[UnsettledNopUsd_T3]
				,[UnsettledNopUsd_T4]
				,[UnsettledNopUsd_T5]
				,[UnsettledNopUsd_T6]
				,[UnsettledNopUsd_T7]
				,[UnsettledNopUsd_T8]
				,[DealsCount_T1]
				,[DealsCount_T2]
				,[DealsCount_T3]
				,[DealsCount_T4]
				,[DealsCount_T5]
				,[DealsCount_T6]
				,[DealsCount_T7]
				,[DealsCount_T8]
				,[HighestUnsettledNopInSingleT]
			FROM
				@temp_final_result
		COMMIT	
		
		DECLARE @TransmissionEnabled BIT
		DECLARE @GoFlatOnlyOn BIT
		DECLARE @TradingOn BIT
		DECLARE @msg VARCHAR(MAX)
		
		--Total NOP
		IF EXISTS(SELECT * FROM @temp_final_result WHERE [MaxUnsettledNopUsdTotalToTurnGFO] > 0 AND [UnsettledNopUsdTotal] > [MaxUnsettledNopUsdTotalToTurnGFO])
		BEGIN
		
			SET @TransmissionEnabled = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()
			SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]
			SET @TradingOn = @TransmissionEnabled & ~@GoFlatOnlyOn
			
			IF @TradingOn = 1
			BEGIN
			
				exec [dbo].[FlipSystemsTradingControl_SP] @GoFlatOnly = 1
			
				SET @msg = 'Credit Line(s) total unsettled NOP limit is approaching and trading is enabled! Increase total unsettled NOP limits or keep GFO to get rid of this message.'

				SELECT
					@msg = @msg + ' ' + [CreditLineName] + '(' + [PrimeBrokerName] + ') Total Unsettled NOP: ' + CAST([UnsettledNopUsdTotal] AS VARCHAR) + 'USD (MAX: ' + CAST([MaxUnsettledNopUsdTotal] AS VARCHAR) + '). '
				FROM 
					@temp_final_result
				WHERE 
					[MaxUnsettledNopUsdTotalToTurnGFO] > 0 AND [UnsettledNopUsdTotal] > [MaxUnsettledNopUsdTotalToTurnGFO]

					
				INSERT INTO [dbo].[SystemsTradingControlSchedule]
				   ([IsEnabled]
				   ,[IsDaily]
				   ,[IsWeekly]
				   ,[ScheduleTimeZoneSpecific]
				   ,[TimeZoneCode]
				   ,[ActionId]
				   ,[ScheduleDescription]
				   ,[IsImmediatelyOnce])
			 VALUES
				   (1
					,0
				   ,0
				   ,GETUTCDATE()
				   ,'UTC'
				   ,(SELECT [ActionId] FROM [dbo].[SystemsTradingControlScheduleAction] WHERE [ActionName] = 'LogFatal')
				   ,@msg
				   ,1)	
			END
		END
		
		--Per single value date NOP
		IF EXISTS
			(SELECT * FROM @temp_final_result WHERE MaxUnsettledNopUsdPerValueDateToTurnGFO > 0 AND HighestUnsettledNopInSingleT > MaxUnsettledNopUsdPerValueDateToTurnGFO)
		BEGIN
		
			SET @TransmissionEnabled = [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()
			SELECT @GoFlatOnlyOn = [GoFlatOnly] FROM [dbo].[SystemsTradingControl]
			SET @TradingOn = @TransmissionEnabled & ~@GoFlatOnlyOn
			
			IF @TradingOn = 1
			BEGIN
			
				exec [dbo].[FlipSystemsTradingControl_SP] @GoFlatOnly = 1
			
				SET @msg = 'Credit Line(s) daily unsettled NOP limit is approaching and trading is enabled! Increase daily unsettled NOP limits or keep GFO to get rid of this message.'

				SELECT
					@msg = @msg + ' ' + [CreditLineName] + '(' + [PrimeBrokerName] + ') Unsettled NOP (' + CAST([ValueDate] AS VARCHAR) + '): ' + CAST([UnsettledNopUsd] AS VARCHAR) + 'USD (MAX per single value date: ' + CAST([MaxUnsettledNopUsdPerValueDate] AS VARCHAR) + '). '
				FROM 
					@temp_intermediate_result perDateResult
					INNER JOIN [dbo].[PrimeBrokerCreditLine] creditLine ON perDateResult.CreditLineId = creditLine.CreditLineId
					INNER JOIN [dbo].[PrimeBroker] primeBroker ON creditLine.[PrimeBrokerId] = primeBroker.[PrimeBrokerId]
				WHERE 
					[MaxUnsettledNopUsdPerValueDateToTurnGFO] > 0 AND [UnsettledNopUsd] > [MaxUnsettledNopUsdPerValueDateToTurnGFO]


					
				INSERT INTO [dbo].[SystemsTradingControlSchedule]
				   ([IsEnabled]
				   ,[IsDaily]
				   ,[IsWeekly]
				   ,[ScheduleTimeZoneSpecific]
				   ,[TimeZoneCode]
				   ,[ActionId]
				   ,[ScheduleDescription]
				   ,[IsImmediatelyOnce])
			 VALUES
				   (1
					,0
				   ,0
				   ,GETUTCDATE()
				   ,'UTC'
				   ,(SELECT [ActionId] FROM [dbo].[SystemsTradingControlScheduleAction] WHERE [ActionName] = 'LogFatal')
				   ,@msg
				   ,1)	
			END
		END
		
		
		
	END
END 
GO

ALTER PROCEDURE [Daily_PurgeAndRollupUnsettledDeals_SP]
AS
BEGIN

	DECLARE @temp_unsettledDeals TABLE
	(
		[CounterpartyId] [tinyint],
		[SymbolId] [tinyint],
		[SettlementDateLocalMkt] [date],
		[SizeBasePol] [decimal](18, 2),
		[SizeTermPol] [decimal](18, 4),
		[DealsCount] [int]
	)

	INSERT INTO
		@temp_unsettledDeals
		(
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		)
	SELECT 
		[CounterpartyId]
		,[SymbolId]
		,[SettlementDateLocalMkt]
		,SUM([SizeBasePol]) AS [NopBasePol]
		,SUM([SizeTermPol]) AS [NopTermPol]
		,SUM([DealsCount]) AS [DealsCount]
	FROM
		[dbo].[DealExternalExecutedUnsettled] unsettled
		INNER JOIN Symbol sym ON sym.ID = unsettled.[SymbolId]
		LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateCurrencyMatch 
					ON settlDateCurrencyMatch.CurrencyID = sym.BaseCurrencyId OR settlDateCurrencyMatch.CurrencyID = sym.QuoteCurrencyId
		LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateOtherCurrencies 
					ON settlDateOtherCurrencies.CurrencyID IS NULL					
	WHERE
		[SettlementDateLocalMkt] >= COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate])
	GROUP BY
		[SettlementDateLocalMkt],
		[CounterpartyId],
		[SymbolId]
		
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
	
		INSERT INTO
			[dbo].[DealExternalExecutedUnsettled]
			(
				[CounterpartyId],
				[SymbolId],
				[SettlementDateLocalMkt],
				[SizeBasePol],
				[SizeTermPol],
				[DealsCount]
			)
		SELECT
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		FROM
			@temp_unsettledDeals
	COMMIT	
END
GO


ALTER PROCEDURE [ForceRefreshAllUnsettledDeals_SP]
AS
BEGIN

	DECLARE @temp_unsettledDeals TABLE
	(
		[CounterpartyId] [tinyint],
		[SymbolId] [tinyint],
		[SettlementDateLocalMkt] [date],
		[SizeBasePol] [decimal](18, 2),
		[SizeTermPol] [decimal](18, 4),
		[DealsCount] [int]
	)

	INSERT INTO
		@temp_unsettledDeals
		(
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		)
	SELECT
		[CounterpartyId],
		[SymbolId],
		[ValueDate],
		SUM([SizeBasePol]),
		SUM([SizeTermPol]),
		--count arbitrary column
		COUNT([CounterpartyId])
	FROM
		(
		SELECT 
			deal.[CounterpartyId]
			,deal.[SymbolId]
			,deal.[ValueDateCounterpartySuppliedLocMktDate] AS [ValueDate]
			,deal.[AmountBasePolExecuted] AS [SizeBasePol]
			,-deal.[AmountBasePolExecuted]*deal.[Price] AS [SizeTermPol]
		FROM 
			[dbo].[DealExternalExecuted] deal
			INNER JOIN Symbol sym ON sym.ID = deal.[SymbolId]
			LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateCurrencyMatch 
						ON settlDateCurrencyMatch.CurrencyID = sym.BaseCurrencyId OR settlDateCurrencyMatch.CurrencyID = sym.QuoteCurrencyId
			LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateOtherCurrencies 
						ON settlDateOtherCurrencies.CurrencyID IS NULL
		WHERE
			IntegratorReceivedExecutionReportUtc > DATEADD(DAY, -10, GETUTCDATE())
			AND
			[ValueDateCounterpartySuppliedLocMktDate] >= COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate])

		UNION ALL

		SELECT 
			[CounterpartyId]
			,[SymbolId]
			,[ValueDate]
			,[IntegratorExecutedAmountBasePol] AS [SizeBasePol]
			,-[IntegratorExecutedAmountBasePol]*[CounterpartyRequestedPrice] AS [SizeTermPol]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Daily]
		WHERE
			[IntegratorExecutedAmountBasePol] > 0

		UNION ALL

		SELECT 
			[CounterpartyId]
			,[SymbolId]
			,[ValueDate]
			,[IntegratorExecutedAmountBasePol] AS [SizeBasePol]
			,-[IntegratorExecutedAmountBasePol]*[CounterpartyRequestedPrice] AS [SizeTermPol]
		FROM
			[dbo].[OrderExternalIncomingRejectable_Archive] deal
			INNER JOIN Symbol sym ON sym.ID = deal.[SymbolId]
			LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateCurrencyMatch 
						ON settlDateCurrencyMatch.CurrencyID = sym.BaseCurrencyId OR settlDateCurrencyMatch.CurrencyID = sym.QuoteCurrencyId
			LEFT JOIN [dbo].[LastCurrentlyUnsettledSettlementDate] settlDateOtherCurrencies 
						ON settlDateOtherCurrencies.CurrencyID IS NULL
		WHERE
			[IntegratorReceivedExternalOrderUtc] > DATEADD(DAY, -10, GETUTCDATE())
			AND
			[IntegratorExecutedAmountBasePol] > 0
			AND
			[ValueDate] >= COALESCE(settlDateCurrencyMatch.[SettlementDate], settlDateOtherCurrencies.[SettlementDate])
		) allUnsettledDeals
	GROUP BY
		[ValueDate],
		[CounterpartyId],
		[SymbolId]

		
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[DealExternalExecutedUnsettled]
	
		INSERT INTO
			[dbo].[DealExternalExecutedUnsettled]
			(
				[CounterpartyId],
				[SymbolId],
				[SettlementDateLocalMkt],
				[SizeBasePol],
				[SizeTermPol],
				[DealsCount]
			)
		SELECT
			[CounterpartyId],
			[SymbolId],
			[SettlementDateLocalMkt],
			[SizeBasePol],
			[SizeTermPol],
			[DealsCount]
		FROM
			@temp_unsettledDeals
	COMMIT	
END
GO



ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 2),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL,
	@SingleFillSystemId int = NULL,
	@MultipleFillsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL,
	@CounterpartyClientId varchar(20) = NULL)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @MultipleFillsSystemIdsAndAmountsBaseAbs IS NOT NULL
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			SELECT
				intpair.num1
				,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc
			FROM
			[dbo].[csvlist_to_intpairs_tbl] (@MultipleFillsSystemIdsAndAmountsBaseAbs) intpair
		END
		-- this is major case (one external deal for one internal order)
		ELSE
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@SingleFillSystemId, -1)
				,@AmountBasePolExecuted
				,@SymbolId
				,@CounterpartyId
				,@Price
				,@InternalDealId
				,@ExecutionReportReceivedUtc)
		END
		
		INSERT INTO 
		[dbo].[DealExternalExecuted]
			   ([IntegratorReceivedExecutionReportUtc]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorExternalOrderPrivateId]
			   ,[DealDirectionId]
			   ,[AmountBasePolExecuted]
			   ,[AmountTermPolExecutedInUsd]
			   ,[Price]
			   ,[CounterpartyTransactionId]
			   ,[CounterpartySuppliedExecutionTimeStampUtc]
			   ,[ValueDateCounterpartySuppliedLocMktDate]
			   ,[OrderSentToExecutionReportReceivedLatency]
			   ,[QuoteReceivedToOrderSentInternalLatency]
			   ,[IntegratorSentExternalOrderUtc]
			   ,[CounterpartySentExecutionReportUtc]
			   ,[InternalTransactionIdentity]
			   ,[FlowSideId]
			   ,[CounterpartyClientId])
		 VALUES
			   (@ExecutionReportReceivedUtc
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@InternalOrderId
			   ,@DealDirectionId
			   ,@AmountBasePolExecuted
			   ,@AmountTermPolExecutedInUsd
			   ,@Price
			   ,@CounterpartyTransactionId
			   ,@CounterpartyExecutionReportTimeStampUtc
			   ,@ValueDateCounterpartySuppliedLocMktDate
			   ,@OrderSentToExecutionReportReceivedLatency
			   ,@QuoteReceivedToOrderSentInternalLatency
			   ,@IntegratorSentExternalOrderUtc
			   ,@CounterpartySentExecutionReportUtc
			   ,@InternalDealId
			   ,@FlowSideId
			   ,@CounterpartyClientId)
			   
		INSERT INTO [dbo].[DealExternalExecutedUnsettled]
           ([CounterpartyId]
           ,[SymbolId]
           ,[SettlementDateLocalMkt]
           ,[SizeBasePol]
           ,[SizeTermPol]
		   ,[DealsCount])
		VALUES
           (@CounterpartyId
           ,@SymbolId
           ,@ValueDateCounterpartySuppliedLocMktDate
           ,@AmountBasePolExecuted
           ,-@AmountBasePolExecuted*@Price
		   ,1)
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH
END
GO


ALTER PROCEDURE [dbo].[InsertNewOrderExternalIncomingRejectable_SP]( 
    @ExternalOrderReceived [datetime2](7),
	@ExternalOrderSentUtc [datetime2](7),
	@IntegratorSentExecutionReportUtc [datetime2](7),
	@CounterpartyClientOrderId [varchar](32),
	@CounterpartyClientId [varchar](20),
	@TradingSystemId int = NULL,
	@SourceIntegratorPriceIdentity [varchar](20) = NULL,
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@RequestedPrice decimal(18,9),
	@RequestedAmountBaseAbs decimal(18,2),
	@RequestedFillMinimumBaseAbs decimal(18,2) = NULL,
	@IntegratorExecId [varchar](20) = NULL,
	@CounterpartyExecId [varchar](32) = NULL,
	@ValueDate [date] = NULL,
	@OrderReceivedToExecutionReportSentLatency [time](7),
	@IntegratorExecutedAmountBaseAbs [decimal](18, 2) = NULL,
	@IntegratorRejectedAmountBaseAbs [decimal](18, 2) = NULL,
	@RejectionReason [varchar](32) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WITH(NOLOCK) WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WITH(NOLOCK) WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WITH(NOLOCK) WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @RejectionReason IS NOT NULL
	BEGIN
		SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
		IF @@ROWCOUNT <> 1
		BEGIN
			INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
			SET @RejectionReasonId = @@IDENTITY
		END
	END
	
	BEGIN TRY
	
		BEGIN TRANSACTION;
	
		IF @IntegratorExecutedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsExecuted_Daily]
				([TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,[InternalTransactionIdentity]
				,[DealTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,COALESCE(@IntegratorExecId, @CounterpartyExecId)
				,@ExternalOrderReceived)
		END
		ELSE IF @IntegratorRejectedAmountBaseAbs > 0
		BEGIN
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			VALUES
				(ISNULL(@TradingSystemId, -1)
				,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
				,@SymbolId
				,@CounterpartyId
				,@RequestedPrice
				,ISNULL(@RejectionReason, '<NONE>')
				,COALESCE(@IntegratorExecId, @CounterpartyExecId)
				,@RejectionDirectionId
				,@ExternalOrderReceived)
		END

		INSERT INTO 
			[dbo].[OrderExternalIncomingRejectable_Daily]
			   ([IntegratorReceivedExternalOrderUtc]
			   ,[CounterpartySentExternalOrderUtc]
			   ,[IntegratorSentExecutionReportUtc]
			   ,[CounterpartyClientOrderId]
			   ,[CounterpartyClientId]
			   ,[TradingSystemId]
			   ,[SourceIntegratorPriceIdentity]
			   ,[CounterpartyId]
			   ,[SymbolId]
			   ,[IntegratorDealDirectionId]
			   ,[CounterpartyRequestedPrice]
			   ,[CounterpartyRequestedAmountBasePol]
			   ,[CounterpartyRequestedFillMinimumBaseAbs]
			   ,[IntegratorExecutionId]
			   ,[CounterpartyExecutionId]
			   ,[ValueDate]
			   ,[OrderReceivedToExecutionReportSentLatency ]
			   ,[IntegratorExecutedAmountBasePol]
			   ,[IntegratorRejectedAmountBasePol]
			   ,[IntegratorRejectionReasonId])
		 VALUES
			   (@ExternalOrderReceived
			   ,@ExternalOrderSentUtc
			   ,@IntegratorSentExecutionReportUtc
			   ,@CounterpartyClientOrderId
			   ,@CounterpartyClientId
			   ,@TradingSystemId
			   ,@SourceIntegratorPriceIdentity
			   ,@CounterpartyId
			   ,@SymbolId
			   ,@DealDirectionId
			   ,@RequestedPrice
			   ,CASE WHEN @DealDirectionId = 1 THEN -@RequestedAmountBaseAbs ELSE @RequestedAmountBaseAbs END
			   ,@RequestedFillMinimumBaseAbs
			   ,@IntegratorExecId
			   ,@CounterpartyExecId
			   ,@ValueDate
			   ,@OrderReceivedToExecutionReportSentLatency 
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorRejectedAmountBaseAbs ELSE @IntegratorRejectedAmountBaseAbs END
		       ,@RejectionReasonId)
			   
		IF @ValueDate IS NOT NULL AND @IntegratorExecutedAmountBaseAbs != 0
		BEGIN
			INSERT INTO [dbo].[DealExternalExecutedUnsettled]
			   ([CounterpartyId]
			   ,[SymbolId]
			   ,[SettlementDateLocalMkt]
			   ,[SizeBasePol]
			   ,[SizeTermPol]
			   ,[DealsCount])
			VALUES
			   (@CounterpartyId
			   ,@SymbolId
			   ,@ValueDate
			   ,CASE WHEN @DealDirectionId = 1 THEN -@IntegratorExecutedAmountBaseAbs ELSE @IntegratorExecutedAmountBaseAbs END
			   ,CASE WHEN @DealDirectionId = 1 THEN @IntegratorExecutedAmountBaseAbs ELSE -@IntegratorExecutedAmountBaseAbs END * @RequestedPrice
			   ,1)
		END
			   
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		THROW
	END CATCH	   
END
GO

ALTER PROCEDURE [dbo].[RejectExecutedOrderExternalIncomingRejectable_SP]( 
	@IntegratorExecId  [varchar](20) = NULL,
	@CounterpartyExecId  [varchar](20) = NULL,
	@RejectionReason [varchar](20) = NULL
	)
AS
BEGIN
	DECLARE @OriginallyExecutedAmount decimal(18,2)
	DECLARE @RejectionReasonId TINYINT
	DECLARE @RejectionDirectionId BIT
	SELECT @RejectionDirectionId = [RejectionDirectionId] FROM [dbo].[RejectionDirection] WHERE [RejectionDirectionDescription] = 'KGTRejectd'


	SELECT 
		@OriginallyExecutedAmount = [IntegratorExecutedAmountBasePol] 
	FROM 
		[dbo].[OrderExternalIncomingRejectable_Daily]
	WHERE 
		[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) 
		AND ([IntegratorExecutionId] = @IntegratorExecId OR [CounterpartyExecutionId] = @CounterpartyExecId)

	IF @OriginallyExecutedAmount IS NOT NULL
	BEGIN
	
		IF @RejectionReason IS NOT NULL
		BEGIN
			SELECT @RejectionReasonId = [IntegratorRejectionReasonId] FROM [dbo].[IntegratorRejectionReason] WITH(NOLOCK) WHERE [IntegratorRejectionReason] = @RejectionReason
			IF @@ROWCOUNT <> 1
			BEGIN
				INSERT INTO [dbo].[IntegratorRejectionReason] ([IntegratorRejectionReason]) VALUES (@RejectionReason)
				SET @RejectionReasonId = @@IDENTITY
			END
		END
	
		BEGIN TRY

		BEGIN TRANSACTION;
	
			UPDATE
				[dbo].[OrderExternalIncomingRejectable_Daily]
			SET
				[IntegratorRejectedAmountBasePol] += @OriginallyExecutedAmount,
				[IntegratorExecutedAmountBasePol] = 0,
				[IntegratorRejectionReasonId] = @RejectionReasonId
			WHERE 
				[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) 
				AND ([IntegratorExecutionId] = @IntegratorExecId OR [CounterpartyExecutionId] = @CounterpartyExecId)

				
			INSERT INTO
			[dbo].[TradingSystemDealsRejected_Daily]
				([TradingSystemId]
				,[AmountBasePolRejected]
				,[SymbolId]
				,[CounterpartyId]
				,[RejectedPrice]
				,[RejectionReason]
				,[IntegratorExternalOrderPrivateId]
				,[RejectionDirectionId]
				,[RejectionTimeUtc])
			SELECT
				[TradingSystemId]
				,[AmountBasePolExecuted]
				,[SymbolId]
				,[CounterpartyId]
				,[Price]
				,ISNULL(@RejectionReason, '<NONE>')
				,[InternalTransactionIdentity]
				,@RejectionDirectionId
				,[DealTimeUtc]
			FROM
				[dbo].[TradingSystemDealsExecuted_Daily]
			WHERE
				[InternalTransactionIdentity] = COALESCE(@IntegratorExecId, @CounterpartyExecId)
					
			DELETE FROM
				[dbo].[TradingSystemDealsExecuted_Daily]
			WHERE
				[InternalTransactionIdentity] = COALESCE(@IntegratorExecId, @CounterpartyExecId)
			
			--delete condition (inner join)
			DELETE TOP (1) unsettled FROM
				[dbo].[DealExternalExecutedUnsettled] unsettled
				INNER JOIN [dbo].[OrderExternalIncomingRejectable_Daily] deals
			ON
				unsettled.[CounterpartyId] = deals.[CounterpartyId] AND
				unsettled.[SymbolId] = deals.[SymbolId] AND
				unsettled.[SettlementDateLocalMkt] = deals.[ValueDate] AND
				unsettled.[SizeBasePol] = deals.[IntegratorExecutedAmountBasePol] AND
				unsettled.[SizeTermPol] = -deals.[IntegratorExecutedAmountBasePol]*deals.[CounterpartyRequestedPrice]
			WHERE
				[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) 
				AND ([IntegratorExecutionId] = @IntegratorExecId OR [CounterpartyExecutionId] = @CounterpartyExecId)

			
		COMMIT TRANSACTION;
		
		SELECT 
			[IntegratorReceivedExternalOrderUtc]
			,[CounterpartySentExternalOrderUtc]
			,[CounterpartyClientOrderId]
			,[TradingSystemId]
			,[SourceIntegratorPriceIdentity]
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.[Name] as Symbol
			,direction.[Name] as DealDirection
			,[CounterpartyRequestedPrice]
			,@OriginallyExecutedAmount AS OriginallyExecutedAmountBasePol
			,[IntegratorExecutionId]
			,[CounterpartyExecutionId]
			,[ValueDate]
		FROM 
			[dbo].[OrderExternalIncomingRejectable_Daily] deals
			INNER JOIN [dbo].[Symbol] symbol ON deals.[SymbolId] = symbol.[Id]
			INNER JOIN [dbo].[Counterparty] counterparty ON deals.[CounterpartyId] = counterparty.[CounterpartyId]
			INNER JOIN [dbo].[DealDirection] direction ON deals.[IntegratorDealDirectionId] = direction.Id
		WHERE 
			[IntegratorReceivedExternalOrderUtc] > CAST(GETUTCDATE() AS DATE) 
			AND ([IntegratorExecutionId] = @IntegratorExecId OR [CounterpartyExecutionId] = @CounterpartyExecId)
		
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION;
			THROW
		END CATCH
		
	END
END
GO


DROP XML SCHEMA COLLECTION  [dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd]
GO


CREATE XML SCHEMA COLLECTION [dbo].[Kreslik.Integrator.BusinessLayer.dll.xsd] AS N'
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="BusinessLayerSettings" nillable="true" type="BusinessLayerSettings" />
  <xs:complexType name="BusinessLayerSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="MarketableClientOrdersMatchingStrategy" type="MarketableClientOrdersMatchingStrategy" />
      <xs:element minOccurs="1" maxOccurs="unbounded" name="ConsecutiveRejectionsStopStrategyBehavior" type="ConsecutiveRejectionsStopStrategySettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartWithSessionsStopped" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="AutoKillInternalOrdersOnOrdersTransmissionDisabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailSubject" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailBody" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailTo" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="StartStopEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="FatalRejectedOrderTriggerPhrasesCsv" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderCounterpartyContacts" type="ArrayOfIgnoredOrderCounterpartyContact" />
      <xs:element minOccurs="1" maxOccurs="1" name="IgnoredOrderEmailCc" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="PreventMatchingToSameCounterpartyAfterOrderRejectBehavior" type="PreventMatchingToSameCounterpartyAfterOrderRejectSettings" />
      <xs:element minOccurs="1" maxOccurs="1" name="TimeGapsWatchingBehavior" type="TimeGapsWatchingSettings" /> 
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="MarketableClientOrdersMatchingStrategy">
    <xs:restriction base="xs:string">
      <xs:enumeration value="ImmediateMatching" />
      <xs:enumeration value="NextFreshPriceMatching" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ConsecutiveRejectionsStopStrategySettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="StrategyEnabled" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="NumerOfRejectionsToTriggerStrategy" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="DurationOfRejectionsToTriggerStrategy_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceAgeRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="PriceBpRankWeightMultiplier" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="DefaultCounterpartyRank" type="xs:decimal" />
      <xs:element minOccurs="1" maxOccurs="1" name="CounterpartyRanks" type="ArrayOfCounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfCounterpartyRank">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="CounterpartyRank" nillable="true" type="CounterpartyRank" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CounterpartyRank">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="Rank" type="xs:decimal" />
  </xs:complexType>
  <xs:complexType name="ArrayOfIgnoredOrderCounterpartyContact">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="IgnoredOrderCounterpartyContact" nillable="true" type="IgnoredOrderCounterpartyContact" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="IgnoredOrderCounterpartyContact">
    <xs:attribute name="CounterpartyCode" type="xs:string" />
    <xs:attribute name="EmailContacts" type="xs:string" />
  </xs:complexType>
  <xs:complexType name="PreventMatchingToSameCounterpartyAfterOrderRejectSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="StrategyEnabled" type="xs:boolean" />
      <xs:element minOccurs="0" maxOccurs="1" name="CounterpartyCodes" type="ArrayOfString" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ArrayOfString">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="Counterparty" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TimeGapsWatchingSettings">
    <xs:sequence>
      <xs:element minOccurs="1" maxOccurs="1" name="LogFatalsDuringOffBusinessHours" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumTimeGapToWatchFor_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="MinimumTimeGapToLogFatal_Milliseconds" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="EnableWatchingForGapsClusters" type="xs:boolean" />
      <xs:element minOccurs="1" maxOccurs="1" name="NumberOfGapsInClusterToLogFatal" type="xs:int" />
      <xs:element minOccurs="1" maxOccurs="1" name="ClusterDuration_Seconds" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>'
GO

-- manually:
Add 
<NumerOfRejectionsToTriggerStrategy>3</NumerOfRejectionsToTriggerStrategy>
<DurationOfRejectionsToTriggerStrategy_Milliseconds>320</DurationOfRejectionsToTriggerStrategy_Milliseconds>
--END TRY
--BEGIN CATCH
--	ROLLBACK TRANSACTION AAA;
--	THROW
--END CATCH
--ROLLBACK TRANSACTION AAA
--GO
--COMMIT TRANSACTION AAA
--GO
