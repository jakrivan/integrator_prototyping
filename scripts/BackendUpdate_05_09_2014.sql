


CREATE TABLE [dbo].[TradingSystemActions](
	[TradingSystemId] [int] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[CounterpartyId] [tinyint] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[GoFlatOnly] [bit] NOT NULL,
	[OrderTransmissionDisabled] [bit] NOT NULL,
	[SettingsLastUpdatedUtc] [datetime2](7) NULL,
	[LastResetRequestedUtc] [datetime2](7) NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX [IX_TradingSystemActions_TradingSystemId] ON [dbo].[TradingSystemActions]
(
	[TradingSystemId] ASC
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TradingSystemActions]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemActions_TradingSystemActions] FOREIGN KEY([TradingSystemId])
REFERENCES [dbo].[TradingSystemActions] ([TradingSystemId])
GO
ALTER TABLE [dbo].[TradingSystemActions] CHECK CONSTRAINT [FK_TradingSystemActions_TradingSystemActions]
GO

ALTER TABLE [dbo].[TradingSystemActions]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemActions_Symbol] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[Symbol] ([Id])
GO
ALTER TABLE [dbo].[TradingSystemActions] CHECK CONSTRAINT [FK_TradingSystemActions_Symbol]
GO

ALTER TABLE [dbo].[TradingSystemActions]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemActions_Counterparty] FOREIGN KEY([CounterpartyId])
REFERENCES [dbo].[Counterparty] ([CounterpartyId])
GO
ALTER TABLE [dbo].[TradingSystemActions] CHECK CONSTRAINT [FK_TradingSystemActions_Counterparty]
GO


INSERT INTO [dbo].[TradingSystemActions]
	([TradingSystemId],
	[SymbolId],
	[CounterpartyId],
	[Enabled],
	[GoFlatOnly],
	[OrderTransmissionDisabled],
	[SettingsLastUpdatedUtc],
	[LastResetRequestedUtc])
SELECT
	[TradingSystemId],
	[SymbolId],
	[CounterpartyId],
	[Enabled],
	0,
	0,
	[LastUpdatedUtc],
	[LastResetRequestedUtc]
FROM 
	[dbo].[VenueCrossSettings]
UNION ALL
SELECT
	[TradingSystemId],
	[SymbolId],
	[CounterpartyId],
	[Enabled],
	0,
	0,
	[LastUpdatedUtc],
	[LastResetRequestedUtc]
FROM 
	[dbo].[VenueMMSettings]
UNION ALL
SELECT
	[TradingSystemId],
	[SymbolId],
	[CounterpartyId],
	[Enabled],
	0,
	0,
	[LastUpdatedUtc],
	[LastResetRequestedUtc]
FROM 
	[dbo].[VenueQuotingSettings]	
GO	
	
--UPDATE SPs and Triggers

ALTER PROCEDURE [dbo].[GetUpdatedVenueCrossSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueCrossSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenTrades_seconds]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,action.[Enabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueCrossSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name 
	END
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueMMSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueMMSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice]
			,[SizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,action.[Enabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueMMSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name
	END
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueQuotingSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueQuotingSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty			
			,[MaximumPositionBaseAbs]
			,[FairPriceImprovementSpreadBasisPoints]
			,[StopLossNetInUsd]		
			,action.[Enabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueQuotingSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name 
	END
END
GO

ALTER PROCEDURE [dbo].[GetVenueCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumTimeBetweenTrades_seconds],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO


ALTER PROCEDURE [dbo].[GetVenueMMSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice],
		sett.[SizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FairPriceImprovementSpreadBasisPoints],
		sett.[StopLossNetInUsd],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER TRIGGER [dbo].[LastUpdatedTrigger_VenueCrossSettings] ON [dbo].[VenueCrossSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueCrossSettings'

END
GO

ALTER TRIGGER [dbo].[LastUpdatedTrigger_VenueMMSettings] ON [dbo].[VenueMMSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueMMSettings'

END
GO

ALTER TRIGGER [dbo].[LastUpdatedTrigger_VenueQuotingSettings] ON [dbo].[VenueQuotingSettings]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE [dbo].[TableChangeLog] SET [LastUpdatedUtc] = GETUTCDATE() WHERE [TableName] = 'VenueQuotingSettings'

END
GO	

ALTER PROCEDURE [dbo].[VenueCrossSettingsDeleteOne_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	DELETE sett
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
		
	IF @@ROWCOUNT > 0
	BEGIN
		DELETE FROM
			[dbo].[TradingSystemActions]
		WHERE 
			[TradingSystemId] = @TradingSystemId
			
		DELETE FROM 
			[dbo].[TradingSystem]
		WHERE 
			[TradingSystemId] = @TradingSystemId
	END
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsDeleteOne_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	DELETE sett
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
		
	IF @@ROWCOUNT > 0
	BEGIN
		DELETE FROM
			[dbo].[TradingSystemActions]
		WHERE 
			[TradingSystemId] = @TradingSystemId
			
		DELETE FROM 
			[dbo].[TradingSystem]
		WHERE 
			[TradingSystemId] = @TradingSystemId
	END
END
GO

ALTER PROCEDURE [dbo].[VenueQuotingSettingsDeleteOne_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	DELETE sett
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
		
	IF @@ROWCOUNT > 0
	BEGIN
		DELETE FROM
			[dbo].[TradingSystemActions]
		WHERE 
			[TradingSystemId] = @TradingSystemId
			
		DELETE FROM 
			[dbo].[TradingSystem]
		WHERE 
			[TradingSystemId] = @TradingSystemId
	END
END
GO

ALTER PROCEDURE [dbo].[VenueCrossSettingsInsertSingle_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenTrades_seconds [int],
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Cross'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Cross TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId]) VALUES (@SymbolId, @TradingSystemTypeId)

	SELECT @TradingSystemId = scope_identity();
	
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [Enabled],	[GoFlatOnly], [OrderTransmissionDisabled]) VALUES (@TradingSystemId, @Enabled, 0, 0)
	
	INSERT INTO [dbo].[VenueCrossSettings]
           ([SymbolId]
           ,[MinimumSizeToTrade]
           ,[MaximumSizeToTrade]
           ,[MinimumDiscountBasisPointsToTrade]
           ,[MinimumTimeBetweenTrades_seconds]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MinimumSizeToTrade
           ,@MaximumSizeToTrade
           ,@MinimumDiscountBasisPointsToTrade
           ,@MinimumTimeBetweenTrades_seconds
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsInsertSingle_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'MM'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'MM TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId]) VALUES (@SymbolId, @TradingSystemTypeId)

	SELECT @TradingSystemId = scope_identity();
	
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [Enabled],	[GoFlatOnly], [OrderTransmissionDisabled]) VALUES (@TradingSystemId, @Enabled, 0, 0)

	INSERT INTO [dbo].[VenueMMSettings]
           ([SymbolId]
           ,[SizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@SizeToTrade 
           ,@BestPriceImprovementOffsetBasisPoints
           ,@MaximumDiscountBasisPointsToTrade
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

ALTER PROCEDURE [dbo].[VenueQuotingSettingsInsertSingle_SP]
(
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MaximumPositionBaseAbs [decimal](18,0),
	@FairPriceImprovementSpreadBasisPoints [decimal](18,6),
	@StopLossNetInUsd [bigint],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Cross'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Cross TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId]) VALUES (@SymbolId, @TradingSystemTypeId)

	SELECT @TradingSystemId = scope_identity();
	
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [Enabled],	[GoFlatOnly], [OrderTransmissionDisabled]) VALUES (@TradingSystemId, @Enabled, 0, 0)

	INSERT INTO [dbo].[VenueQuotingSettings]
           ([SymbolId]
           ,[MaximumPositionBaseAbs]
           ,[FairPriceImprovementSpreadBasisPoints]
           ,[StopLossNetInUsd]   
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MaximumPositionBaseAbs
           ,@FairPriceImprovementSpreadBasisPoints
           ,@StopLossNetInUsd
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

DROP PROCEDURE [dbo].[VenueCrossSystemSettingsDisableAll_SP]
GO
DROP PROCEDURE [dbo].[VenueMMSettingsDisableAll_SP]
GO

CREATE TRIGGER [dbo].[LastUpdatedTrigger_TradingSystemActions] ON [dbo].[TradingSystemActions]
AFTER INSERT, UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

Update [dbo].[TradingSystemActions] Set [SettingsLastUpdatedUtc] = GETUTCDATE() from [dbo].[TradingSystemActions] myAlias , inserted triggerInsertedTable where 
triggerInsertedTable.[TradingSystemId] = myAlias.[TradingSystemId]

UPDATE 
	[dbo].[TableChangeLog] 
SET 
	[LastUpdatedUtc] = GETUTCDATE() 
WHERE 
	[TableName] IN
	(	
	SELECT
		'Venue' + tst.SystemName + 'Settings'
	FROM 
		inserted insertedAction
		INNER JOIN [dbo].[TradingSystem] ts ON insertedAction.TradingSystemId = ts.TradingSystemId
		INNER JOIN [dbo].[TradingSystemType] tst ON ts.TradingSystemTypeId = tst.TradingSystemTypeId
	)
END
GO

DROP PROCEDURE [dbo].[VenueCrossSystemSettingsDisableEnableOne_SP]
GO
DROP PROCEDURE [dbo].[VenueMMSettingsDisableEnableOne_SP]
GO
DROP PROCEDURE [dbo].[VenueQuotingSystemSettingsDisableEnableOne_SP]
GO

CREATE PROCEDURE [dbo].[VenueSystemDisableEnableOne_SP]
(
	@TradingSystemId int,
	@Enable Bit
)
AS
BEGIN
	UPDATE
		[dbo].[TradingSystemActions]
	SET
		[Enabled] = @Enable
	WHERE
		[TradingSystemId] = @TradingSystemId
END
GO

GRANT EXECUTE ON [dbo].[VenueSystemDisableEnableOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


DROP PROCEDURE [dbo].[VenueCrossSystemSettingsReset_SP]
GO
DROP PROCEDURE [dbo].[VenueMMSettingsReset_SP]
GO
DROP PROCEDURE [dbo].[VenueQuotingSystemSettingsReset_SP]
GO

CREATE PROCEDURE [dbo].[VenueSystemReset_SP]
(
	@TradingSystemId int
)
AS
BEGIN
	UPDATE
		[dbo].[TradingSystemActions]
	SET
		[LastResetRequestedUtc] = GETUTCDATE()
	WHERE
		[TradingSystemId] = @TradingSystemId
		AND [Enabled] = 0
END
GO

GRANT EXECUTE ON [dbo].[VenueSystemReset_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROCEDURE [dbo].[VenueCrossSystemSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenTrades_seconds [int],
	@MaximumWaitTimeOnImprovementTick_milliseconds [int]
)
AS
BEGIN
	UPDATE sett
	SET
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[MinimumDiscountBasisPointsToTrade] = @MinimumDiscountBasisPointsToTrade,
		[MinimumTimeBetweenTrades_seconds] = @MinimumTimeBetweenTrades_seconds,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int]
)
AS
BEGIN
	UPDATE sett
	SET
		[SizeToTrade] = @SizeToTrade,
		[BestPriceImprovementOffsetBasisPoints] = @BestPriceImprovementOffsetBasisPoints,
		[MaximumDiscountBasisPointsToTrade] = @MaximumDiscountBasisPointsToTrade,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

ALTER PROCEDURE [dbo].[VenueQuotingSystemSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MaximumPositionBaseAbs [decimal](18,0),
	@FairPriceImprovementSpreadBasisPoints [decimal](18,6),
	@StopLossNetInUsd [bigint]
)
AS
BEGIN
	UPDATE sett
	SET
		[MaximumPositionBaseAbs] = @MaximumPositionBaseAbs,
		[FairPriceImprovementSpreadBasisPoints] = @FairPriceImprovementSpreadBasisPoints,
		[StopLossNetInUsd] = @StopLossNetInUsd
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO

	
--Drop old columns	
	
ALTER TABLE [dbo].[VenueCrossSettings] DROP COLUMN [Enabled]
ALTER TABLE [dbo].[VenueCrossSettings] DROP COLUMN [LastUpdatedUtc]
ALTER TABLE [dbo].[VenueCrossSettings] DROP COLUMN [LastResetRequestedUtc]

ALTER TABLE [dbo].[VenueMMSettings] DROP COLUMN [Enabled]
ALTER TABLE [dbo].[VenueMMSettings] DROP COLUMN [LastUpdatedUtc]
ALTER TABLE [dbo].[VenueMMSettings] DROP COLUMN [LastResetRequestedUtc]

ALTER TABLE [dbo].[VenueQuotingSettings] DROP COLUMN [Enabled]
ALTER TABLE [dbo].[VenueQuotingSettings] DROP COLUMN [LastUpdatedUtc]
ALTER TABLE [dbo].[VenueQuotingSettings] DROP COLUMN [LastResetRequestedUtc]


-- Simplifying overall trading systems stats + adding missing groups
ALTER PROCEDURE [dbo].[GetTradingSystemsOveralDailyStats_SP]
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	SELECT 
		counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] AS SystemName,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) AS PnlGrossUsdPerMUsd,
		SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) AS CommissionsUsdPerMUsd,
		SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemType] systemtype
		INNER JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = systemtype.[TradingSystemTypeId]
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
	GROUP BY
		counterparty.[CounterpartyCode], systemtype.[SystemName]
	ORDER BY
		SystemName
END
GO

-- Adding Rejections info to the trading systems stats
ALTER PROCEDURE [dbo].[GetTradingSystemsDailyStats_SP]
AS
BEGIN
	SELECT
		--systemStats.Symbol AS Symbol,
		systemStats.SymbolId AS SymbolId,
		systemStats.TradingSystemId AS TradingSystemId,
		--systemStats.TradingSystemDescription AS TradingSystemDescription,
		COALESCE(systemStats.NopBasePol, 0) AS NopBasePol,
		COALESCE(systemStats.VolumeInUsd/1000000, 0) AS VolumeUsdM,
		COALESCE(systemStats.DealsNum, 0) as DealsNum,
		COALESCE(systemStats.RejectionsNum, 0) as RejectionsNum,
		COALESCE(systemStats.PnlTermPolInUsd/(systemStats.VolumeInUsd/1000000), 0) AS PnlGrossUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd, 0) AS PnlGrossUsd,
		COALESCE(systemStats.CommissionInUSD/(systemStats.VolumeInUsd/1000000), 0) AS CommissionsUsdPerMUsd,
		COALESCE(systemStats.CommissionInUSD, 0) AS CommissionsUsd,
		COALESCE((systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD)/(systemStats.VolumeInUsd/1000000), 0) AS PnlNetUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD, 0) AS PnlNetUsd
	FROM
		(
		SELECT
			--Symbol.Name AS Symbol,
			Symbol.Id AS SymbolId,
			tradingSystem.TradingSystemId AS TradingSystemId,
			--tradingSystem.Description AS TradingSystemDescription,
			systemsStats.NopBasePol AS NopBasePol,
			systemsStats.DealsNum as DealsNum,
			systemsStats.RejectionsNum as RejectionsNum,
			systemsStats.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd,
			--PnL = term position + yield of immediate close of outstanding base position
			--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
			(systemsStats.NopTermPol + (CASE WHEN systemsStats.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * systemsStats.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
			systemsStats.CommissionInCCY1 * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionInUSD
		FROM
			(
			SELECT
				COALESCE(systemsPositions.[TradingSystemId], systemRejections.[TradingSystemId]) AS [TradingSystemId],
				NopBasePol,
				NopTermPol,
				VolumeInCcy1,
				CommissionInCCY1,
				DealsNum,
				RejectionsNum
			FROM	
				(
				SELECT 
					[TradingSystemId],
					SUM([AmountBasePolExecuted]) AS NopBasePol,
					SUM(-[AmountBasePolExecuted]* [Price]) AS NopTermPol,
					SUM(ABS([AmountBasePolExecuted])) AS VolumeInCcy1,
					SUM(ABS([AmountBasePolExecuted]) * CommissionPricePerMillion / 1000000) AS CommissionInCCY1,
					COUNT([TradingSystemId]) AS DealsNum
				FROM
					[dbo].[TradingSystemDealsExecuted_Daily] deal
					INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
				GROUP BY
					[TradingSystemId]
				) systemsPositions
				FULL OUTER JOIN
				(
					SELECT 
						[TradingSystemId],
						COUNT([TradingSystemId]) AS RejectionsNum
					FROM
						[dbo].[TradingSystemDealsRejected_Daily] rejects
					GROUP BY
						[TradingSystemId]
				)  systemRejections ON systemsPositions.TradingSystemId = systemRejections.TradingSystemId
			) systemsStats
			INNER JOIN [dbo].[TradingSystem] tradingSystem ON systemsStats.TradingSystemId = tradingSystem.TradingSystemId
			INNER JOIN [dbo].[Symbol] symbol ON tradingSystem.SymbolId = symbol.Id
			INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
			INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
			) systemStats
END
GO

ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ADD RejectionsNum [int] NULL
GO
UPDATE [dbo].[PersistedStatisticsPerTradingSystemDaily] SET RejectionsNum = 0
GO
ALTER TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily] ALTER COLUMN [RejectionsNum] [int] NOT NULL
GO

ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	CREATE TABLE #TempTradingSystemStatistics
	(
		[SymbolId] [tinyint] NOT NULL,
		[TradingSystemId] [int] NOT NULL,
		[NopBasePol] [decimal](18,0) NOT NULL,
		[VolumeUsdM] [decimal](18,6) NOT NULL,
		[DealsNum] [int]  NOT NULL,
		[RejectionsNum] [int]  NOT NULL,
		[PnlGrossUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlGrossUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsdPerMUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsd] [decimal](18,6) NOT NULL
	)

	INSERT INTO 
		#TempTradingSystemStatistics
		([SymbolId]
		,[TradingSystemId]
		,[NopBasePol]
		,[VolumeUsdM]
		,[DealsNum]
		,[RejectionsNum]
		,[PnlGrossUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsdPerMUsd]
		,[CommissionsUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlNetUsd])
    exec [dbo].[GetTradingSystemsDailyStats_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingSystemDaily]
			([TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[RejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd])
		SELECT
			[TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[RejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd]
		FROM
			#TempTradingSystemStatistics
	COMMIT
END
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsOveralDailyStats_SP]
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	SELECT 
		counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] AS SystemName,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		SUM(stats.[RejectionsNum]) AS RejectionsNum,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemType] systemtype
		INNER JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = systemtype.[TradingSystemTypeId]
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
	GROUP BY
		counterparty.[CounterpartyCode], systemtype.[SystemName]
	ORDER BY
		SystemName
END
GO


ALTER PROCEDURE [dbo].[GetVenueCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[RejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumTimeBetweenTrades_seconds],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueMMSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[RejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice],
		sett.[SizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[RejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FairPriceImprovementSpreadBasisPoints],
		sett.[StopLossNetInUsd],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER TRIGGER [dbo].[DealExternalRejectedInserted] ON [dbo].[DealExternalRejected] AFTER INSERT
AS
BEGIN
	-- commit the internal insert transaction to prevent deadlocks
	COMMIT TRANSACTION
	exec [dbo].[UpdatePersistedStatisticsPerCounterpartyDaily]
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP]
	--trick the sql server that the transaction was running during the entire trigger
	BEGIN TRANSACTION
END
GO

-- Trading system groups

CREATE TABLE [dbo].[TradingSystemGroup](
	[TradingSystemGroupId] [int] IDENTITY(1,1) NOT NULL,
	[TradingSystemGroupName] [nvarchar](32) NOT NULL,
	[OrderingRank] [int] NULL,
	CONSTRAINT UniqueTradingSystemGroupName UNIQUE(TradingSystemGroupName) 
)
GO

CREATE UNIQUE CLUSTERED INDEX [IX_TradingSystemGroup_TradingSystemGroupId] ON [dbo].[TradingSystemGroup]
(
	[TradingSystemGroupId] ASC
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[TradingSystemGroup_Deleted](
	[TradingSystemGroupId] [int] NOT NULL,
	[TradingSystemGroupName] [nvarchar](32) NOT NULL,
	[DeletedUtc] [datetime2](7) NOT NULL
)
GO

CREATE TRIGGER [dbo].[TradingSystemGroup_ArchiveDeleted] ON [dbo].[TradingSystemGroup] AFTER DELETE
AS
BEGIN
	INSERT INTO
		[dbo].[TradingSystemGroup_Deleted]
			([TradingSystemGroupId],
			[TradingSystemGroupName],
			[DeletedUtc])
	SELECT
		[TradingSystemGroupId],
		[TradingSystemGroupName],
		GETUTCDATE()
	FROM
		deleted
END
GO

-- TODO: SPs for altering bits - those need to invoke changes in actions table

INSERT INTO [dbo].[TradingSystemGroup] ([TradingSystemGroupName]) VALUES (N'Initial Default Group', 0, 0)
GO

CREATE TABLE [dbo].[TradingSystemTypeAllowedForGroup](
	[TradingSystemTypeId] [tinyint] NOT NULL,
	[TradingSystemGroupId] [int] NOT NULL,
	[IsGoFlatOnlyOn] [bit] NOT NULL DEFAULT 0,
	[IsOrderTransmissionDisabled] [bit] NOT NULL DEFAULT 0,
	CONSTRAINT UniqueTradingSystemTypePerGroup UNIQUE(TradingSystemGroupId, TradingSystemTypeId)
)
GO

ALTER TABLE [dbo].[TradingSystemTypeAllowedForGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemTypeAllowedForGroup_TradingSystemType] FOREIGN KEY([TradingSystemTypeId])
REFERENCES [dbo].[TradingSystemType] ([TradingSystemTypeId])
GO
ALTER TABLE [dbo].[TradingSystemTypeAllowedForGroup] CHECK CONSTRAINT [FK_TradingSystemTypeAllowedForGroup_TradingSystemType]
GO

ALTER TABLE [dbo].[TradingSystemTypeAllowedForGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemTypeAllowedForGroup_TradingSystemGroup] FOREIGN KEY([TradingSystemGroupId])
REFERENCES [dbo].[TradingSystemGroup] ([TradingSystemGroupId])
GO
ALTER TABLE [dbo].[TradingSystemTypeAllowedForGroup] CHECK CONSTRAINT [FK_TradingSystemTypeAllowedForGroup_TradingSystemGroup]
GO

INSERT INTO [dbo].[TradingSystemTypeAllowedForGroup] ([TradingSystemTypeId], [TradingSystemGroupId]) SELECT [TradingSystemTypeId], 1 FROM [dbo].[TradingSystemType]
GO

ALTER TABLE [dbo].[TradingSystem] ADD TradingSystemGroupId [int] NULL
GO

UPDATE [dbo].[TradingSystem] SET [TradingSystemGroupId] = 1
GO

ALTER TABLE [dbo].[TradingSystem] ALTER COLUMN TradingSystemGroupId [int] NOT NULL
GO

ALTER TABLE [dbo].[TradingSystem]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystem_TradingSystemGroup] FOREIGN KEY([TradingSystemGroupId])
REFERENCES [dbo].[TradingSystemGroup] ([TradingSystemGroupId])
GO
ALTER TABLE [dbo].[TradingSystem] CHECK CONSTRAINT [FK_TradingSystem_TradingSystemGroup]
GO

--TODO: All insertions must specify group id or name

ALTER TABLE [dbo].[TradingSystem_Deleted] ADD TradingSystemGroupId [int] NULL
GO

UPDATE [dbo].[TradingSystem_Deleted] SET [TradingSystemGroupId] = -1
GO

ALTER TABLE [dbo].[TradingSystem_Deleted] ALTER COLUMN TradingSystemGroupId [int] NOT NULL
GO

ALTER TRIGGER [dbo].[TradingSystem_ArchiveDeleted] ON [dbo].[TradingSystem] AFTER DELETE
AS
BEGIN
	INSERT INTO
		[dbo].[TradingSystem_Deleted]
			([TradingSystemId],
			[TradingSystemTypeId],
			[TradingSystemGroupId],
			[SymbolId],
			[Description],
			[DeletedUtc])
	SELECT
		[TradingSystemId],
		[TradingSystemTypeId],
		[TradingSystemGroupId],
		[SymbolId],
		[Description],
		GETUTCDATE()
	FROM
		deleted
END
GO

ALTER TABLE [dbo].[TradingSystemMonitoringPage] ADD TradingSystemGroupId [int] NULL
GO

UPDATE [dbo].[TradingSystemMonitoringPage] SET [TradingSystemGroupId] = 1
GO

ALTER TABLE [dbo].[TradingSystemMonitoringPage] ALTER COLUMN TradingSystemGroupId [int] NOT NULL
GO

ALTER TABLE [dbo].[TradingSystemMonitoringPage]  WITH NOCHECK ADD  CONSTRAINT [FK_TradingSystemMonitoringPage_TradingSystemGroup] FOREIGN KEY([TradingSystemGroupId])
REFERENCES [dbo].[TradingSystemGroup] ([TradingSystemGroupId])
GO
ALTER TABLE [dbo].[TradingSystemMonitoringPage] CHECK CONSTRAINT [FK_TradingSystemMonitoringPage_TradingSystemGroup]
GO

--TODO: All insertions must specify group id or name


CREATE PROCEDURE [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_SP]
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	SELECT
		trgroup.[TradingSystemGroupName] AS TradingSystemGroup,
		trgroup.[TradingSystemGroupId] As TradingSystemGroupId,
		counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] AS TradingSystemTypeName,
		counterparty.[CounterpartyCode] AS Counterparty,
		systemtype.[TradingSystemTypeId] AS TradingSystemTypeId,
		statsPerType.ConfiguredSystems,
		statsPerType.EnabledSystems,
		statsPerType.VolumeUsdM,
		statsPerType.DealsNum,
		statsPerType.RejectionsNum,
		statsPerType.PnlGrossUsdPerMUsd,
		statsPerType.CommissionsUsdPerMUsd,
		statsPerType.PnlNetUsdPerMUsd,
		statsPerType.PnlGrossUsd,
		statsPerType.CommissionsUsd,
		statsPerType.PnlNetUsd,
		allowedType.IsOrderTransmissionDisabled,
		allowedType.IsGoFlatOnlyOn,
		monitorPages.TradingSystemMonitoringPageId AS PageId,
		beginSymbol.Name AS BeginSymbol,
		endSymbol.Name AS EndSymbol,
		monitorPages.StripsCount
	FROM
	(
	SELECT 
		allowedType.TradingSystemGroupId AS TradingSystemGroupId,
		allowedType.TradingSystemTypeId AS TradingSystemTypeId,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		SUM(stats.[RejectionsNum]) AS RejectionsNum,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemTypeAllowedForGroup] allowedType_forJoinsOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = allowedType_forJoinsOnly.[TradingSystemTypeId] AND trsystem.TradingSystemGroupId = allowedType_forJoinsOnly.TradingSystemGroupId
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		--link in deleted systems if needed
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		-- but make sure that those are only the allowed ones
		LEFT JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON 
			allowedType.[TradingSystemTypeId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId])
			AND
			allowedType.[TradingSystemGroupId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemGroupId], trsystem_deleted.[TradingSystemGroupId])

	GROUP BY
		allowedType.TradingSystemGroupId, allowedType.TradingSystemTypeId
	)
	statsPerType
	INNER JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON allowedType.TradingSystemGroupId = statsPerType.TradingSystemGroupId AND allowedType.TradingSystemTypeId = statsPerType.TradingSystemTypeId
	INNER JOIN [dbo].[TradingSystemType] systemtype ON statsPerType.[TradingSystemTypeId] = systemtype.[TradingSystemTypeId]
	INNER JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	RIGHT JOIN TradingSystemGroup trgroup ON trgroup.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId
	LEFT JOIN TradingSystemMonitoringPage monitorPages ON monitorPages.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId AND monitorPages.[TradingSystemTypeId] = statsPerType.TradingSystemTypeId
	LEFT JOIN [dbo].[Symbol] beginSymbol ON monitorPages.BeginSymbolId = beginSymbol.Id
	LEFT JOIN [dbo].[Symbol] endSymbol ON monitorPages.EndSymbolId = endSymbol.Id
	ORDER BY
		trgroup.[OrderingRank] ASC,
		TradingSystemGroup ASC,
		TradingSystemTypeName ASC,
		beginSymbol.Name ASC
END
GO

GRANT EXECUTE ON [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO



-- Deleted and unknown system statistics

ALTER PROCEDURE [dbo].[InsertDealExternalExecuted_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@InternalDealId [nvarchar](50),
	@AmountBasePolExecuted decimal(18, 0),
	@AmountTermPolExecutedInUsd decimal(18, 2),
	@Price decimal(18, 9),
	@CounterpartyTransactionId NVARCHAR(64),
	@CounterpartyExecutionReportTimeStampUtc DATETIME2(7),
	@ValueDateCounterpartySuppliedLocMktDate DATE,
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@FlowSide [varchar](16) = NULL,
	@SingleFillSystemId int = NULL,
	@MultipleFillsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT
	DECLARE @FlowSideId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END
	
	IF @FlowSide IS NOT NULL
	BEGIN
		SELECT @FlowSideId = [FlowSideId] FROM [dbo].[FlowSide] WHERE [FlowSideName] = @FlowSide

		IF @@ROWCOUNT <> 1
		BEGIN
			RAISERROR (N'FlowSide not found in DB: %s', 16, 2, @FlowSide) WITH SETERROR
		END
	END
	ELSE
	BEGIN
		SET @FlowSideId = NULL
	END
	
	IF @MultipleFillsSystemIdsAndAmountsBaseAbs IS NOT NULL
	BEGIN
		INSERT INTO
		[dbo].[TradingSystemDealsExecuted_Daily]
			([TradingSystemId]
			,[AmountBasePolExecuted]
			,[SymbolId]
			,[CounterpartyId]
			,[Price]
			,[InternalTransactionIdentity])
		SELECT
			intpair.num1
			,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
			,@SymbolId
			,@CounterpartyId
			,@Price
			,@InternalDealId
		FROM
		[dbo].[csvlist_to_intpairs_tbl] (@MultipleFillsSystemIdsAndAmountsBaseAbs) intpair
	END
	-- this is major case (one external deal for one internal order)
	ELSE
	BEGIN
		INSERT INTO
		[dbo].[TradingSystemDealsExecuted_Daily]
			([TradingSystemId]
			,[AmountBasePolExecuted]
			,[SymbolId]
			,[CounterpartyId]
			,[Price]
			,[InternalTransactionIdentity])
		VALUES
			(ISNULL(@SingleFillSystemId, -1)
			,@AmountBasePolExecuted
			,@SymbolId
			,@CounterpartyId
			,@Price
			,@InternalDealId)
	END
	
	
	INSERT INTO 
	[dbo].[DealExternalExecuted]
           ([IntegratorReceivedExecutionReportUtc]
           ,[CounterpartyId]
           ,[SymbolId]
           ,[IntegratorExternalOrderPrivateId]
           ,[DealDirectionId]
           ,[AmountBasePolExecuted]
		   ,[AmountTermPolExecutedInUsd]
           ,[Price]
           ,[CounterpartyTransactionId]
           ,[CounterpartySuppliedExecutionTimeStampUtc]
           ,[ValueDateCounterpartySuppliedLocMktDate]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[IntegratorSentExternalOrderUtc]
		   ,[CounterpartySentExecutionReportUtc]
		   ,[InternalTransactionIdentity]
		   ,[FlowSideId])
     VALUES
           (@ExecutionReportReceivedUtc
           ,@CounterpartyId
           ,@SymbolId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolExecuted
		   ,@AmountTermPolExecutedInUsd
           ,@Price
           ,@CounterpartyTransactionId
           ,@CounterpartyExecutionReportTimeStampUtc
           ,@ValueDateCounterpartySuppliedLocMktDate
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@QuoteReceivedToOrderSentInternalLatency
		   ,@IntegratorSentExternalOrderUtc
		   ,@CounterpartySentExecutionReportUtc
		   ,@InternalDealId
		   ,@FlowSideId)
END
GO

ALTER PROCEDURE [dbo].[InsertDealExternalRejected_SP]( 
	@ExecutionReportReceivedUtc DATETIME2(7),
	@CounterpartyCode [char](3),
	@Symbol NCHAR(7),
	@DealDirection NVARCHAR(4),
	@InternalOrderId [nvarchar](45),
	@AmountBasePolRejected decimal(18, 0),
	@OrderSentToExecutionReportReceivedLatency TIME(7),
	@QuoteReceivedToOrderSentInternalLatency TIME(7),
	@IntegratorSentExternalOrderUtc DATETIME2(7),
	@RejectionReason NVARCHAR(64),
	@CounterpartySentExecutionReportUtc datetime2 = NULL,
	@RejectedPrice decimal(18, 9),
	@SingleRejectionSystemId int = NULL,
	@MultipleRejectionsSystemIdsAndAmountsBaseAbs varchar(MAX) = NULL
	)
AS
BEGIN

	DECLARE @SymbolId INT
	DECLARE @CounterpartyId INT
	DECLARE @DealDirectionId INT

	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT @DealDirectionId = [Id] FROM [dbo].[DealDirection] WHERE [Name] = @DealDirection

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'DealDirection not found in DB: %s', 16, 2, @DealDirection) WITH SETERROR
	END

	IF @MultipleRejectionsSystemIdsAndAmountsBaseAbs IS NOT NULL
	BEGIN
		INSERT INTO
		[dbo].[TradingSystemDealsRejected_Daily]
			([TradingSystemId]
			,[AmountBasePolRejected]
			,[SymbolId]
			,[CounterpartyId]
			,[RejectedPrice]
			,[RejectionReason]
			,[IntegratorExternalOrderPrivateId])
		SELECT
			intpair.num1
			,CASE WHEN @DealDirectionId = 1 THEN -intpair.num2 ELSE intpair.num2 END
			,@SymbolId
			,@CounterpartyId
			,@RejectedPrice
			,@RejectionReason
			,@InternalOrderId
		FROM
		[dbo].[csvlist_to_intpairs_tbl] (@MultipleRejectionsSystemIdsAndAmountsBaseAbs) intpair
	END
	-- this is major case (one external rejection for one internal order)
	ELSE
	BEGIN
		INSERT INTO
		[dbo].[TradingSystemDealsRejected_Daily]
			([TradingSystemId]
			,[AmountBasePolRejected]
			,[SymbolId]
			,[CounterpartyId]
			,[RejectedPrice]
			,[RejectionReason]
			,[IntegratorExternalOrderPrivateId])
		VALUES
			(ISNULL(@SingleRejectionSystemId, -1)
			,@AmountBasePolRejected
			,@SymbolId
			,@CounterpartyId
			,@RejectedPrice
			,@RejectionReason
			,@InternalOrderId)
	END

	INSERT INTO 
	[dbo].[DealExternalRejected]
           ([IntegratorReceivedExecutionReportUtc]
           ,[SymbolId]
           ,[CounterpartyId]
           ,[IntegratorExternalOrderPrivateId]
		   ,[DealDirectionId]
           ,[AmountBasePolRejected]
           ,[OrderSentToExecutionReportReceivedLatency]
		   ,[QuoteReceivedToOrderSentInternalLatency]
		   ,[IntegratorSentExternalOrderUtc]
           ,[RejectionReason]
		   ,[CounterpartySentExecutionReportUtc]
		   ,[RejectedPrice])
     VALUES
           (@ExecutionReportReceivedUtc
		   ,@SymbolId
           ,@CounterpartyId
           ,@InternalOrderId
           ,@DealDirectionId
           ,@AmountBasePolRejected
           ,@OrderSentToExecutionReportReceivedLatency
		   ,@QuoteReceivedToOrderSentInternalLatency
		   ,@IntegratorSentExternalOrderUtc
		   ,@RejectionReason
		   ,@CounterpartySentExecutionReportUtc
		   ,@RejectedPrice)
END
GO


ALTER PROCEDURE [dbo].[GetTradingSystemsDailyStats_SP]
AS
BEGIN
	SELECT
		--BEWARE! There can be multiple symbols for same system id (this is especially true for system -1)
		systemStats.SymbolId AS SymbolId,
		systemStats.TradingSystemId AS TradingSystemId,
		COALESCE(systemStats.NopBasePol, 0) AS NopBasePol,
		COALESCE(systemStats.VolumeInUsd/1000000, 0) AS VolumeUsdM,
		COALESCE(systemStats.DealsNum, 0) as DealsNum,
		COALESCE(systemStats.RejectionsNum, 0) as RejectionsNum,
		COALESCE(systemStats.PnlTermPolInUsd/(systemStats.VolumeInUsd/1000000), 0) AS PnlGrossUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd, 0) AS PnlGrossUsd,
		COALESCE(systemStats.CommissionInUSD/(systemStats.VolumeInUsd/1000000), 0) AS CommissionsUsdPerMUsd,
		COALESCE(systemStats.CommissionInUSD, 0) AS CommissionsUsd,
		COALESCE((systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD)/(systemStats.VolumeInUsd/1000000), 0) AS PnlNetUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD, 0) AS PnlNetUsd
	FROM
		(
		SELECT
			Symbol.Id AS SymbolId,
			systemsStats.TradingSystemId AS TradingSystemId,
			systemsStats.NopBasePol AS NopBasePol,
			systemsStats.DealsNum as DealsNum,
			systemsStats.RejectionsNum as RejectionsNum,
			systemsStats.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd,
			--PnL = term position + yield of immediate close of outstanding base position
			--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
			(systemsStats.NopTermPol + (CASE WHEN systemsStats.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * systemsStats.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
			systemsStats.CommissionInCCY1 * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionInUSD
		FROM
			(
			SELECT
				COALESCE(systemsPositions.[TradingSystemId], systemRejections.[TradingSystemId]) AS [TradingSystemId],
				COALESCE(systemsPositions.[SymbolId], systemRejections.[SymbolId]) AS [SymbolId],
				NopBasePol,
				NopTermPol,
				VolumeInCcy1,
				CommissionInCCY1,
				DealsNum,
				RejectionsNum
			FROM	
				(
				SELECT 
					[TradingSystemId],
					[SymbolId],
					SUM([AmountBasePolExecuted]) AS NopBasePol,
					SUM(-[AmountBasePolExecuted]* [Price]) AS NopTermPol,
					SUM(ABS([AmountBasePolExecuted])) AS VolumeInCcy1,
					SUM(ABS([AmountBasePolExecuted]) * CommissionPricePerMillion / 1000000) AS CommissionInCCY1,
					COUNT([TradingSystemId]) AS DealsNum
				FROM
					[dbo].[TradingSystemDealsExecuted_Daily] deal
					INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
				GROUP BY
					[TradingSystemId], [SymbolId]
				) systemsPositions
				FULL OUTER JOIN
				(
					SELECT 
						[TradingSystemId],
						[SymbolId],
						COUNT([TradingSystemId]) AS RejectionsNum
					FROM
						[dbo].[TradingSystemDealsRejected_Daily] rejects
					GROUP BY
						[TradingSystemId], [SymbolId]
				)  systemRejections ON systemsPositions.TradingSystemId = systemRejections.TradingSystemId
			) systemsStats
			INNER JOIN [dbo].[Symbol] symbol ON systemsStats.SymbolId = symbol.Id
			INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
			INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
			) systemStats
END
GO
		
		
ALTER PROCEDURE [dbo].[GetTradingSystemsOveralDailyStats_SP]
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	SELECT 
		ISNULL(counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName], 'Unknown') AS SystemName,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		SUM(stats.[RejectionsNum]) AS RejectionsNum,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemType] systemtype_forJoinOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = systemtype_forJoinOnly.[TradingSystemTypeId]
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		LEFT JOIN [dbo].[TradingSystemType] systemtype ON systemtype.[TradingSystemTypeId] = COALESCE(trsystem.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId], systemtype_forJoinOnly.[TradingSystemTypeId])
		LEFT JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	GROUP BY
		counterparty.[CounterpartyCode], systemtype.[SystemName]
	ORDER BY
		SystemName
END
GO

CREATE PROCEDURE [dbo].[TradingSystemsGroupUpdateTransmissionControl_SP]
(
	@OrderTransmissionDisabled [bit],
	@SystemsGroupId [int],
	@SystemTypeId [int] = NULL
)
AS
BEGIN
	UPDATE 
		[dbo].[TradingSystemTypeAllowedForGroup]
	SET
		[IsOrderTransmissionDisabled] = @OrderTransmissionDisabled
	WHERE 
		TradingSystemGroupId = @SystemsGroupId
		AND (@SystemTypeId IS NULL OR TradingSystemTypeId = @SystemTypeId)
END
GO

GRANT EXECUTE ON [dbo].[TradingSystemsGroupUpdateTransmissionControl_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[TradingSystemsGroupUpdateGoFlatOnly_SP]
(
	@GoFlatOnlyOn [bit],
	@SystemsGroupId [int],
	@SystemTypeId [int] = NULL
)
AS
BEGIN
	UPDATE 
		[dbo].[TradingSystemTypeAllowedForGroup]
	SET
		[IsGoFlatOnlyOn] = @GoFlatOnlyOn
	WHERE 
		TradingSystemGroupId = @SystemsGroupId
		AND (@SystemTypeId IS NULL OR TradingSystemTypeId = @SystemTypeId)
END
GO

GRANT EXECUTE ON [dbo].[TradingSystemsGroupUpdateGoFlatOnly_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[TradingSystemsGroupDisableEnableMulti_SP]
(
	@Enable [bit],
	@SystemsGroupId [int],
	@SystemTypeId [int] = NULL
)
AS
BEGIN
	UPDATE 
		action
	SET
		[Enabled] = @Enable
	FROM
		[dbo].[TradingSystemActions] action
		INNER JOIN [dbo].[TradingSystem]  trsystem ON action.[TradingSystemId] = trsystem.[TradingSystemId]
	WHERE 
		trsystem.[TradingSystemGroupId] = @SystemsGroupId
		AND (@SystemTypeId IS NULL OR trsystem.[TradingSystemTypeId] = @SystemTypeId)
END
GO

GRANT EXECUTE ON [dbo].[TradingSystemsGroupDisableEnableMulti_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROCEDURE [dbo].[GetNumberOfConflicitngSymbolRangesForSystemType_SP]
(
	@TradingSystemTypeId INT,
	@TradingSystemsGroupId [int],
	@PageIdToExclude INT = NULL,
	@BeginSymbol VARCHAR(7),
	@EndSymbol VARCHAR(7)
)
AS
BEGIN

	DECLARE @BeginSymbolId INT
	DECLARE @EndSymbolId INT
	DECLARE @BeginSymbolRank INT
	DECLARE @EndSymbolRank INT

	SELECT @BeginSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @BeginSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @BeginSymbol) WITH SETERROR
	END

	SELECT @EndSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @EndSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @EndSymbol) WITH SETERROR
	END

	DECLARE @SymbolRanks TABLE (SymbolName varchar(7), SymbolId int, SymbolAlphaRank int)

	INSERT INTO @SymbolRanks(SymbolName, SymbolId, SymbolAlphaRank)
	SELECT
		name, 
		id, 
		RANK() OVER (order by name asc) AS AlphaRank 
	FROM
		[dbo].[Symbol]

	SELECT @BeginSymbolRank = SymbolAlphaRank FROM @SymbolRanks WHERE SymbolId = @BeginSymbolId
	SELECT @EndSymbolRank = SymbolAlphaRank FROM @SymbolRanks WHERE SymbolId = @EndSymbolId

	SELECT
		COUNT(*)
	FROM
		[dbo].[TradingSystemMonitoringPage] sysPage
		INNER JOIN @SymbolRanks BeginSymbolRanks on sysPage.BeginSymbolId = BeginSymbolRanks.SymbolId
		INNER JOIN @SymbolRanks EndSymbolRanks on sysPage.EndSymbolId = EndSymbolRanks.SymbolId
	WHERE
		[TradingSystemTypeId] = @TradingSystemTypeId
		AND
		[TradingSystemGroupId] = @TradingSystemsGroupId
		AND
		(@PageIdToExclude IS NULL OR [TradingSystemMonitoringPageId] != @PageIdToExclude)
		AND
		(
			--start symbol of existing range is inside new tested range
			(BeginSymbolRanks.SymbolAlphaRank >= @BeginSymbolRank AND BeginSymbolRanks.SymbolAlphaRank <= @EndSymbolRank)
			OR
			--end symbol of existing range is inside new tested range
			(EndSymbolRanks.SymbolAlphaRank >= @BeginSymbolRank AND EndSymbolRanks.SymbolAlphaRank <= @EndSymbolRank)
		)
END
GO


ALTER PROCEDURE [dbo].[InsertTradingSystemMonitoringPages_SP]
(
	@BeginSymbol NCHAR(7),
	@EndSymbol NCHAR(7),
	@StripsCount INT,
	@SystemGroupId INT,
	@SystemTypeId INT,
	@TradingSystemMonitoringPageId INT OUTPUT
)
AS
BEGIN

	DECLARE @BeginSymbolId INT
	DECLARE @EndSymbolId INT

	SELECT @BeginSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @BeginSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @BeginSymbol) WITH SETERROR
	END
	
	SELECT @EndSymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @EndSymbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @EndSymbol) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystemMonitoringPage]
           (TradingSystemGroupId
		   ,[TradingSystemTypeId]
           ,[BeginSymbolId]
           ,[EndSymbolId]
           ,[StripsCount])
     VALUES
           (@SystemGroupId,
		   @SystemTypeId,
           @BeginSymbolId,
           @EndSymbolId,
           @StripsCount)
	
	SELECT @TradingSystemMonitoringPageId = scope_identity()
END
GO

CREATE PROCEDURE [dbo].[GetTradingSystemGroupNames_SP]
AS
BEGIN
	SELECT 
		[TradingSystemGroupId]
		,[TradingSystemGroupName]
	FROM 
		[dbo].[TradingSystemGroup]
END
GO

GRANT EXECUTE ON [dbo].[GetTradingSystemGroupNames_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

ALTER PROCEDURE [dbo].[GetVenueCrossSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[RejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		sett.[MinimumSizeToTrade],
		sett.[MaximumSizeToTrade],
		sett.[MinimumDiscountBasisPointsToTrade],
		sett.[MinimumTimeBetweenTrades_seconds],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueMMSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[RejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice],
		sett.[SizeToTrade],
		sett.[BestPriceImprovementOffsetBasisPoints],
		sett.[MaximumDiscountBasisPointsToTrade],
		sett.[MaximumWaitTimeOnImprovementTick_milliseconds],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[GetVenueQuotingSystemSettingsAndStats_SP] (
	@CounterpartyCode [char](3),
	@TradingGroupId int = null,
	@BeginWithSymbol [nchar](7) = null,
	@EndWithSymbol [nchar](7) = null
	)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]

	DECLARE @CounterpartyId INT

	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	SELECT 
		system.[TradingSystemId],
		system.[Description] AS TradingSystemDescription,
		symbol.Name AS Symbol,
		stats.[NopBasePol],
		stats.[VolumeUsdM],
		stats.[DealsNum],
		stats.[RejectionsNum],
		stats.[PnlGrossUsdPerMUsd],
		stats.[PnlGrossUsd],
		stats.[CommissionsUsdPerMUsd],
		stats.[CommissionsUsd],
		stats.[PnlNetUsdPerMUsd],
		stats.[PnlNetUsd],
		symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice],
		sett.[MaximumPositionBaseAbs],
		sett.[FairPriceImprovementSpreadBasisPoints],
		sett.[StopLossNetInUsd],
		action.[Enabled],
		action.[LastResetRequestedUtc],
		action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
	FROM 
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
		INNER JOIN [dbo].[TradingSystem] system ON sett.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		LEFT JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = system.[TradingSystemId]
	WHERE
		counterparty.CounterpartyId = @CounterpartyId
		AND (@TradingGroupId IS NULL OR system.TradingSystemGroupId = @TradingGroupId)
		AND 
		(
		@BeginWithSymbol IS NULL 
		OR @EndWithSymbol IS NULL 
		OR symbol.[Name] BETWEEN @BeginWithSymbol AND @EndWithSymbol 
		OR symbol.[Name] BETWEEN @EndWithSymbol AND @BeginWithSymbol
		)
	ORDER BY
		symbol.Name ASC,
		system.[TradingSystemId] ASC
END
GO

ALTER PROCEDURE [dbo].[VenueCrossSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenTrades_seconds [int],
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Cross'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Cross TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueCrossSettings]
           ([SymbolId]
           ,[MinimumSizeToTrade]
           ,[MaximumSizeToTrade]
           ,[MinimumDiscountBasisPointsToTrade]
           ,[MinimumTimeBetweenTrades_seconds]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MinimumSizeToTrade
           ,@MaximumSizeToTrade
           ,@MinimumDiscountBasisPointsToTrade
           ,@MinimumTimeBetweenTrades_seconds
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

ALTER PROCEDURE [dbo].[VenueMMSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@SizeToTrade [decimal](18,0),
	@BestPriceImprovementOffsetBasisPoints [decimal](18,6),
	@MaximumDiscountBasisPointsToTrade [decimal](18,6),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'MM'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'MM TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueMMSettings]
           ([SymbolId]
           ,[SizeToTrade]
           ,[BestPriceImprovementOffsetBasisPoints]
           ,[MaximumDiscountBasisPointsToTrade]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@SizeToTrade 
           ,@BestPriceImprovementOffsetBasisPoints
           ,@MaximumDiscountBasisPointsToTrade
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

ALTER PROCEDURE [dbo].[VenueQuotingSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MaximumPositionBaseAbs [decimal](18,0),
	@FairPriceImprovementSpreadBasisPoints [decimal](18,6),
	@StopLossNetInUsd [bigint],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Cross'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Cross TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueQuotingSettings]
           ([SymbolId]
           ,[MaximumPositionBaseAbs]
           ,[FairPriceImprovementSpreadBasisPoints]
           ,[StopLossNetInUsd]   
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MaximumPositionBaseAbs
           ,@FairPriceImprovementSpreadBasisPoints
           ,@StopLossNetInUsd
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO


CREATE PROCEDURE [dbo].[GetActiveSystemsCountForPagePlan_SP]
(
	@PageId [int]
)
AS
BEGIN
	SELECT
		--COUNT(action.TradingSystemId) AS ConfiguredSystems,
		COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems
	FROM 
		[dbo].[TradingSystemMonitoringPage] page
		INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemGroupId] = page.[TradingSystemGroupId] AND system.[TradingSystemTypeId] = page.[TradingSystemTypeId]
		INNER JOIN [dbo].[TradingSystemActions] action ON action.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON action.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Symbol] beginSymbol ON page.[BeginSymbolId] = beginSymbol.Id
		INNER JOIN [dbo].[Symbol] endSymbol ON page.[EndSymbolId] = endSymbol.Id
	WHERE
		page.[TradingSystemMonitoringPageId] = @PageId
		AND	symbol.[Name] BETWEEN beginSymbol.[Name] AND endSymbol.[Name]
END
GO

GRANT EXECUTE ON [dbo].[GetActiveSystemsCountForPagePlan_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[VenueSystemDeleteOne_SP]
(
	@TradingSystemId int
)
AS
BEGIN

	BEGIN TRANSACTION;
	
	DECLARE @DeletedCnt INT = 0;
	
	DELETE sett
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[Enabled] = 0
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
		
	
	IF @DeletedCnt > 0
	BEGIN
		DELETE FROM
			[dbo].[TradingSystemActions]
		WHERE 
			[TradingSystemId] = @TradingSystemId
			
		DELETE FROM 
			[dbo].[TradingSystem]
		WHERE 
			[TradingSystemId] = @TradingSystemId
	END
	
	
	COMMIT TRANSACTION;
END
GO

GRANT EXECUTE ON [dbo].[VenueSystemDeleteOne_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

DROP PROCEDURE [dbo].[VenueCrossSettingsDeleteOne_SP]
DROP PROCEDURE [dbo].[VenueMMSettingsDeleteOne_SP]
DROP PROCEDURE [dbo].[VenueQuotingSettingsDeleteOne_SP]
GO

CREATE TYPE IdsTable AS TABLE
(
	Id int NOT NULL
)
GO


CREATE PROCEDURE [dbo].[VenueSystemDeleteMulti_SP]
(
	@TradingSystemId IdsTable READONLY
)
AS
BEGIN

	BEGIN TRANSACTION;
	
	DECLARE @DeletedCnt INT = 0;
	
	DELETE sett
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueMMSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
	
	DELETE sett
	FROM
		[dbo].[VenueQuotingSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	SELECT @DeletedCnt = @DeletedCnt + @@ROWCOUNT
		

	DELETE FROM
		[dbo].[TradingSystemActions]
	WHERE 
		[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
		
	DELETE FROM 
		[dbo].[TradingSystem]
	WHERE 
		[TradingSystemId] IN (SELECT Id FROM @TradingSystemId)
	
	COMMIT TRANSACTION;
END
GO

GRANT EXECUTE ON [dbo].[VenueSystemDeleteMulti_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
		
		
ALTER PROCEDURE [dbo].[DeleteTradingSystemMonitoringPages_SP]
(
	@TradingSystemMonitoringPageId INT
)
AS
BEGIN

	DECLARE @TradingSystemIdsToDelete IdsTable
	
	INSERT INTO @TradingSystemIdsToDelete (Id)
	
	SELECT
		system.[TradingSystemId]
	FROM 
		[dbo].[TradingSystemMonitoringPage] page
		INNER JOIN [dbo].[TradingSystem] system ON system.[TradingSystemGroupId] = page.[TradingSystemGroupId] AND system.[TradingSystemTypeId] = page.[TradingSystemTypeId]
		INNER JOIN [dbo].[TradingSystemActions] action ON action.[TradingSystemId] = system.[TradingSystemId]
		INNER JOIN [dbo].[Symbol] symbol ON action.[SymbolId] = symbol.Id
		INNER JOIN [dbo].[Symbol] beginSymbol ON page.[BeginSymbolId] = beginSymbol.Id
		INNER JOIN [dbo].[Symbol] endSymbol ON page.[EndSymbolId] = endSymbol.Id
	WHERE
		page.[TradingSystemMonitoringPageId] = @TradingSystemMonitoringPageId
		AND	symbol.[Name] BETWEEN beginSymbol.[Name] AND endSymbol.[Name]
		
	EXEC [dbo].[VenueSystemDeleteMulti_SP] @TradingSystemIdsToDelete

	DELETE FROM
		[dbo].[TradingSystemMonitoringPage]
	WHERE
		TradingSystemMonitoringPageId = @TradingSystemMonitoringPageId
END		
GO


CREATE PROCEDURE [dbo].[TradingSystemGroupDeleteSubtype_SP]
(
	@TradingSystemGroupId INT,
	@TradingSystemTypeId INT
)
AS
BEGIN

	DECLARE @TradingSystemIdsToDelete IdsTable
	
	INSERT INTO @TradingSystemIdsToDelete (Id)
	SELECT 
		[TradingSystemId]
	FROM
		[dbo].[TradingSystem]
	WHERE
		TradingSystemTypeId = @TradingSystemTypeId AND TradingSystemGroupId = @TradingSystemGroupId
		
	EXEC [dbo].[VenueSystemDeleteMulti_SP] @TradingSystemIdsToDelete
	
	DELETE FROM [dbo].[TradingSystemTypeAllowedForGroup]
    WHERE TradingSystemTypeId = @TradingSystemTypeId AND TradingSystemGroupId = @TradingSystemGroupId
	
	DELETE FROM
		[dbo].[TradingSystemMonitoringPage]
	WHERE
		TradingSystemTypeId = @TradingSystemTypeId AND TradingSystemGroupId = @TradingSystemGroupId
END
GO

GRANT EXECUTE ON [dbo].[TradingSystemGroupDeleteSubtype_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

CREATE PROCEDURE [dbo].[TradingSystemGroupDelete_SP]
(
	@TradingSystemGroupId INT
)
AS
BEGIN

	DECLARE @TradingSystemIdsToDelete IdsTable
	
	INSERT INTO @TradingSystemIdsToDelete (Id)
	SELECT 
		[TradingSystemId]
	FROM
		[dbo].[TradingSystem]
	WHERE
		TradingSystemGroupId = @TradingSystemGroupId
		
	EXEC [dbo].[VenueSystemDeleteMulti_SP] @TradingSystemIdsToDelete
	
	DELETE FROM [dbo].[TradingSystemTypeAllowedForGroup]
    WHERE TradingSystemGroupId = @TradingSystemGroupId
	
	DELETE FROM
		[dbo].[TradingSystemMonitoringPage]
	WHERE
		TradingSystemGroupId = @TradingSystemGroupId
	
	DELETE FROM [dbo].[TradingSystemGroup]
	WHERE TradingSystemGroupId = @TradingSystemGroupId

END
GO

GRANT EXECUTE ON [dbo].[TradingSystemGroupDelete_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[GetTradingSystemTypeNames_SP]
AS
BEGIN
	SELECT
		[TradingSystemTypeId]
		,ctp.[CounterpartyCode] + ' ' + [SystemName] AS TradingSystemTypeName
	FROM 
		[dbo].[TradingSystemType] system
		INNER JOIN [dbo].[Counterparty] ctp ON ctp.CounterpartyId = system.CounterpartyId
END
GO

GRANT EXECUTE ON [dbo].[GetTradingSystemTypeNames_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[TradingSystemGroupRename_SP]
(
	@TradingSystemGroupId INT,
	@NewName NVARCHAR(32)
)
AS
BEGIN
	UPDATE 
		[dbo].[TradingSystemGroup]
	SET
		[TradingSystemGroupName] = @NewName
	WHERE 
		TradingSystemGroupId = @TradingSystemGroupId 
END
GO

GRANT EXECUTE ON [dbo].[TradingSystemGroupRename_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[TradingSystemGroupAdd_SP]
(
	@NewName NVARCHAR(32)
)
AS
BEGIN

	INSERT INTO
		[dbo].[TradingSystemGroup] (TradingSystemGroupName)
	VALUES
		(@NewName)
END
GO

GRANT EXECUTE ON [dbo].[TradingSystemGroupAdd_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[TradingSystemGroupAddType_SP]
(
	@GroupId INT,
	@TypeId INT
)
AS
BEGIN

	INSERT INTO [dbo].[TradingSystemTypeAllowedForGroup]
           ([TradingSystemTypeId]
           ,[TradingSystemGroupId]
           ,[IsGoFlatOnlyOn]
           ,[IsOrderTransmissionDisabled])
     VALUES
           (@TypeId
           ,@GroupId
           ,0
           ,0)
END
GO

GRANT EXECUTE ON [dbo].[TradingSystemGroupAddType_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


DROP PROCEDURE [dbo].[GetTradingSystemMonitoringPages_SP]
GO



--preaggregating


CREATE TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily](
	[Rank] [int] NOT NULL,
	[TradingSystemGroupName] [NVARCHAR](32) NOT NULL,
	[TradingSystemGroupId] [int]  NOT NULL,
	[TradingSystemTypeName] [nvarchar](12) NULL,
	[Counterparty] [char](3) NULL,
	[TradingSystemTypeId] [int] NULL,
	[ConfiguredSystemsCount] int NULL,
	[EnabledSystemsCount] int NULL,
	[VolumeUsdM] [decimal](18, 6) NULL,
	[DealsNum] [int] NULL,
	[RejectionsNum] [int] NULL,
	[PnlGrossUsdPerMUsd] [decimal](18, 6) NULL,
	[CommissionsUsdPerMUsd] [decimal](18, 6) NULL,
	[PnlNetUsdPerMUsd] [decimal](18, 6) NULL,
	[PnlGrossUsd] [decimal](18, 6) NULL,
	[CommissionsUsd] [decimal](18, 6) NULL,	
	[PnlNetUsd] [decimal](18, 6) NULL,
	[IsOrderTransmissionDisabled] [bit] NULL,
	[IsGoFlatOnlyOn] [bit] NULL,
	[PageId] [int] NULL,
	[BeginSymbol] [nchar](7) NULL,
	[EndSymbol] [nchar](7) NULL,
	[StripsCount] INT
) ON [PRIMARY]

CREATE UNIQUE CLUSTERED INDEX [IX_PersistedStatisticsPerTradingGroupsDaily_GroupAndRank] ON [dbo].[PersistedStatisticsPerTradingGroupsDaily]
(
	[TradingSystemGroupId] ASC,
	[Rank] ASC
) ON [PRIMARY]
GO


exec sp_rename 'dbo.GetTradingSystemsGroupsOveralDailyStatsAndInfo_SP', 'GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP'
GO

ALTER PROCEDURE [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP]
AS
BEGIN
	SELECT
		ROW_NUMBER() OVER(ORDER BY
			trgroup.[OrderingRank] ASC,
			[TradingSystemGroupName] ASC,
			counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] ASC,
			beginSymbol.Name ASC) AS Rank,
		trgroup.[TradingSystemGroupName] AS TradingSystemGroup,
		trgroup.[TradingSystemGroupId] As TradingSystemGroupId,
		counterparty.[CounterpartyCode] + ' ' + systemtype.[SystemName] AS TradingSystemTypeName,
		counterparty.[CounterpartyCode] AS Counterparty,
		systemtype.[TradingSystemTypeId] AS TradingSystemTypeId,
		statsPerType.ConfiguredSystems,
		statsPerType.EnabledSystems,
		statsPerType.VolumeUsdM,
		statsPerType.DealsNum,
		statsPerType.RejectionsNum,
		statsPerType.PnlGrossUsdPerMUsd,
		statsPerType.CommissionsUsdPerMUsd,
		statsPerType.PnlNetUsdPerMUsd,
		statsPerType.PnlGrossUsd,
		statsPerType.CommissionsUsd,
		statsPerType.PnlNetUsd,
		allowedType.IsOrderTransmissionDisabled,
		allowedType.IsGoFlatOnlyOn,
		monitorPages.TradingSystemMonitoringPageId AS PageId,
		beginSymbol.Name AS BeginSymbol,
		endSymbol.Name AS EndSymbol,
		monitorPages.StripsCount
	FROM
	(
	SELECT 
		allowedType.TradingSystemGroupId AS TradingSystemGroupId,
		allowedType.TradingSystemTypeId AS TradingSystemTypeId,
		count(trsystem.[TradingSystemId]) AS ConfiguredSystems,
		COALESCE(SUM(CONVERT(int, action.[Enabled])), 0) AS EnabledSystems,
		SUM(stats.[VolumeUsdM]) AS VolumeUsdM,
		SUM(stats.[DealsNum]) AS DealsNum,
		SUM(stats.[RejectionsNum]) AS RejectionsNum,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlGrossUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlGrossUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[CommissionsUsd]) / SUM(stats.[VolumeUsdM]) END AS CommissionsUsdPerMUsd,
		CASE WHEN SUM(stats.[VolumeUsdM]) = 0 THEN 0 ELSE SUM(stats.[PnlNetUsd]) / SUM(stats.[VolumeUsdM]) END AS PnlNetUsdPerMUsd,
		SUM(stats.[PnlGrossUsd]) AS PnlGrossUsd,
		SUM(stats.[CommissionsUsd]) AS CommissionsUsd,
		SUM(stats.[PnlNetUsd]) AS PnlNetUsd
	FROM 
		[dbo].[TradingSystemTypeAllowedForGroup] allowedType_forJoinsOnly
		LEFT JOIN [dbo].[TradingSystem] trsystem ON trsystem.[TradingSystemTypeId] = allowedType_forJoinsOnly.[TradingSystemTypeId] AND trsystem.TradingSystemGroupId = allowedType_forJoinsOnly.TradingSystemGroupId
		LEFT JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
		FULL OUTER JOIN [dbo].[PersistedStatisticsPerTradingSystemDaily] stats ON stats.[TradingSystemId] = trsystem.[TradingSystemId]
		--link in deleted systems if needed
		LEFT JOIN [dbo].[TradingSystem_Deleted] trsystem_deleted ON trsystem.[TradingSystemId] IS NULL AND trsystem_deleted.[TradingSystemId] = stats.[TradingSystemId]
		-- but make sure that those are only the allowed ones
		LEFT JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON 
			allowedType.[TradingSystemTypeId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemTypeId], trsystem_deleted.[TradingSystemTypeId])
			AND
			allowedType.[TradingSystemGroupId] = COALESCE(allowedType_forJoinsOnly.[TradingSystemGroupId], trsystem_deleted.[TradingSystemGroupId])

	GROUP BY
		allowedType.TradingSystemGroupId, allowedType.TradingSystemTypeId
	)
	statsPerType
	INNER JOIN [dbo].[TradingSystemTypeAllowedForGroup] allowedType ON allowedType.TradingSystemGroupId = statsPerType.TradingSystemGroupId AND allowedType.TradingSystemTypeId = statsPerType.TradingSystemTypeId
	INNER JOIN [dbo].[TradingSystemType] systemtype ON statsPerType.[TradingSystemTypeId] = systemtype.[TradingSystemTypeId]
	INNER JOIN [dbo].[Counterparty] counterparty ON systemtype.[CounterpartyId] = counterparty.CounterpartyId
	RIGHT JOIN TradingSystemGroup trgroup ON trgroup.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId
	LEFT JOIN TradingSystemMonitoringPage monitorPages ON monitorPages.[TradingSystemGroupId] = statsPerType.TradingSystemGroupId AND monitorPages.[TradingSystemTypeId] = statsPerType.TradingSystemTypeId
	LEFT JOIN [dbo].[Symbol] beginSymbol ON monitorPages.BeginSymbolId = beginSymbol.Id
	LEFT JOIN [dbo].[Symbol] endSymbol ON monitorPages.EndSymbolId = endSymbol.Id
END
GO

CREATE PROCEDURE [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_SP]
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]
	
	SELECT
		[TradingSystemGroupName] AS TradingSystemGroup
		,[TradingSystemGroupId] 
		,[TradingSystemTypeName]
		,[Counterparty]
		,[TradingSystemTypeId]
		,[ConfiguredSystemsCount] AS ConfiguredSystems
		,[EnabledSystemsCount] AS EnabledSystems
		,[VolumeUsdM]
		,[DealsNum]
		,[RejectionsNum]
		,[PnlGrossUsdPerMUsd]
		,[CommissionsUsdPerMUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsd]
		,[PnlNetUsd]
		,[IsOrderTransmissionDisabled]
		,[IsGoFlatOnlyOn]
		,[PageId]
		,[BeginSymbol]
		,[EndSymbol]
		,[StripsCount]
	FROM 
		[dbo].[PersistedStatisticsPerTradingGroupsDaily]
	ORDER BY
		Rank ASC
END
GO


GRANT EXECUTE ON [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

GRANT EXECUTE ON [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	DECLARE @TempTradingSystemStatistics TABLE
	(
		[SymbolId] [tinyint] NOT NULL,
		[TradingSystemId] [int] NOT NULL,
		[NopBasePol] [decimal](18,0) NOT NULL,
		[VolumeUsdM] [decimal](18,6) NOT NULL,
		[DealsNum] [int]  NOT NULL,
		[RejectionsNum] [int]  NOT NULL,
		[PnlGrossUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlGrossUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsdPerMUsd] [decimal](18,6) NOT NULL,
		[CommissionsUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsdPerMUsd] [decimal](18,6) NOT NULL,
		[PnlNetUsd] [decimal](18,6) NOT NULL
	)

	INSERT INTO 
		@TempTradingSystemStatistics
		([SymbolId]
		,[TradingSystemId]
		,[NopBasePol]
		,[VolumeUsdM]
		,[DealsNum]
		,[RejectionsNum]
		,[PnlGrossUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsdPerMUsd]
		,[CommissionsUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlNetUsd])
    exec [dbo].[GetTradingSystemsDailyStats_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingSystemDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingSystemDaily]
			([TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[RejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd])
		SELECT
			[TradingSystemId]
			,[NopBasePol]
			,[VolumeUsdM]
			,[DealsNum]
			,[RejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsdPerMUsd]
			,[CommissionsUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlNetUsd]
		FROM
			@TempTradingSystemStatistics
	COMMIT
END
GO

CREATE PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]
AS
BEGIN
	--create the temp table before transaction to speed up things
	DECLARE @TempTradingGroupsStatistics TABLE
	(
		[Rank] [int] NOT NULL,
		[TradingSystemGroupName] [NVARCHAR](32) NOT NULL,
		[TradingSystemGroupId] [int]  NOT NULL,
		[TradingSystemTypeName] [nvarchar](12) NULL,
		[Counterparty] [char](3) NULL,
		[TradingSystemTypeId] [tinyint] NULL,
		[ConfiguredSystemsCount] int NULL,
		[EnabledSystemsCount] int NULL,
		[VolumeUsdM] [decimal](18, 6) NULL,
		[DealsNum] [int] NULL,
		[RejectionsNum] [int] NULL,
		[PnlGrossUsdPerMUsd] [decimal](18, 6) NULL,
		[CommissionsUsdPerMUsd] [decimal](18, 6) NULL,
		[PnlNetUsdPerMUsd] [decimal](18, 6) NULL,
		[PnlGrossUsd] [decimal](18, 6) NULL,
		[CommissionsUsd] [decimal](18, 6) NULL,	
		[PnlNetUsd] [decimal](18, 6) NULL,
		[IsOrderTransmissionDisabled] [bit] NULL,
		[IsGoFlatOnlyOn] [bit] NULL,
		[PageId] [int] NULL,
		[BeginSymbol] [nchar](7) NULL,
		[EndSymbol] [nchar](7) NULL,
		[StripsCount] INT
	)

	INSERT INTO 
		@TempTradingGroupsStatistics
		([Rank]
		,[TradingSystemGroupName]
		,[TradingSystemGroupId]
		,[TradingSystemTypeName]
		,[Counterparty]
		,[TradingSystemTypeId]
		,[ConfiguredSystemsCount]
		,[EnabledSystemsCount]
		,[VolumeUsdM]
		,[DealsNum]
		,[RejectionsNum]
		,[PnlGrossUsdPerMUsd]
		,[CommissionsUsdPerMUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsd]
		,[PnlNetUsd]
		,[IsOrderTransmissionDisabled]
		,[IsGoFlatOnlyOn]
		,[PageId]
		,[BeginSymbol]
		,[EndSymbol]
		,[StripsCount])
    exec [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_Internal_SP]

	--this needs to be in transaction in order not te see empty table from outside
	-- and because we have transaction we try to minimise time inside it - therefore temp table
	BEGIN TRANSACTION
		TRUNCATE TABLE [dbo].[PersistedStatisticsPerTradingGroupsDaily]

		INSERT INTO [dbo].[PersistedStatisticsPerTradingGroupsDaily]
			([Rank]
			,[TradingSystemGroupName]
			,[TradingSystemGroupId]
			,[TradingSystemTypeName]
			,[Counterparty]
			,[TradingSystemTypeId]
			,[ConfiguredSystemsCount]
			,[EnabledSystemsCount]
			,[VolumeUsdM]
			,[DealsNum]
			,[RejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[CommissionsUsdPerMUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsd]
			,[PnlNetUsd]
			,[IsOrderTransmissionDisabled]
			,[IsGoFlatOnlyOn]
			,[PageId]
			,[BeginSymbol]
			,[EndSymbol]
			,[StripsCount])
		SELECT
			[Rank]
			,[TradingSystemGroupName]
			,[TradingSystemGroupId]
			,[TradingSystemTypeName]
			,[Counterparty]
			,[TradingSystemTypeId]
			,[ConfiguredSystemsCount]
			,[EnabledSystemsCount]
			,[VolumeUsdM]
			,[DealsNum]
			,[RejectionsNum]
			,[PnlGrossUsdPerMUsd]
			,[CommissionsUsdPerMUsd]
			,[PnlNetUsdPerMUsd]
			,[PnlGrossUsd]
			,[CommissionsUsd]
			,[PnlNetUsd]
			,[IsOrderTransmissionDisabled]
			,[IsGoFlatOnlyOn]
			,[PageId]
			,[BeginSymbol]
			,[EndSymbol]
			,[StripsCount]
		FROM
			@TempTradingGroupsStatistics
	COMMIT
END
GO

ALTER PROCEDURE [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_SP]
AS
BEGIN
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP]
	exec [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP]
END
GO

GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerTradingGroupsDaily_SP] TO [IntegratorServiceAccount] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[UpdatePersistedStatisticsPerTradingSystemDaily_Internal_SP] TO [IntegratorServiceAccount] AS [dbo]
GO

GRANT ALTER ON [dbo].[PersistedStatisticsPerTradingGroupsDaily] TO [IntegratorServiceAccount] AS [dbo]
GRANT SELECT ON [dbo].[PersistedStatisticsPerTradingGroupsDaily] TO [IntegratorServiceAccount] AS [dbo]
GRANT ALTER ON [dbo].[PersistedStatisticsPerTradingGroupsDaily] TO [IntegratorKillSwitchUser] AS [dbo]
GRANT SELECT ON [dbo].[PersistedStatisticsPerTradingGroupsDaily] TO [IntegratorKillSwitchUser] AS [dbo]
GO

--disallow detail pages from actions if hidden

CREATE PROCEDURE [dbo].[TradingSystemPageExists_SP]
(
	@GroupId INT,
	@PageId INT
)
AS
BEGIN
	SELECT CONVERT(bit, 1) AS IsPresent
	FROM [dbo].[TradingSystemMonitoringPage]
	WHERE [TradingSystemMonitoringPageId] = @PageId AND [TradingSystemGroupId] = @GroupId
END
GO

GRANT EXECUTE ON [dbo].[TradingSystemPageExists_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

-- groups to separate pages

CREATE PROCEDURE [dbo].[GetTradingSystemsGroupOveralDailyStatsAndInfo_SP]
(
	@GroupId INT
)
AS
BEGIN
	-- make sure statistics are fresh
	exec [dbo].[UpdatePersistedStatisticsPerTradingSystemDailyIfNeeded_SP]
	
	SELECT
		[TradingSystemGroupName] AS TradingSystemGroup
		,[TradingSystemGroupId] 
		,[TradingSystemTypeName]
		,[Counterparty]
		,[TradingSystemTypeId]
		,[ConfiguredSystemsCount] AS ConfiguredSystems
		,[EnabledSystemsCount] AS EnabledSystems
		,[VolumeUsdM]
		,[DealsNum]
		,[RejectionsNum]
		,[PnlGrossUsdPerMUsd]
		,[CommissionsUsdPerMUsd]
		,[PnlNetUsdPerMUsd]
		,[PnlGrossUsd]
		,[CommissionsUsd]
		,[PnlNetUsd]
		,[IsOrderTransmissionDisabled]
		,[IsGoFlatOnlyOn]
		,[PageId]
		,[BeginSymbol]
		,[EndSymbol]
		,[StripsCount]
	FROM 
		[dbo].[PersistedStatisticsPerTradingGroupsDaily]
	WHERE
		[TradingSystemGroupId] = @GroupId
	ORDER BY
		Rank ASC
END
GO

GRANT EXECUTE ON [dbo].[GetTradingSystemsGroupOveralDailyStatsAndInfo_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO


CREATE PROCEDURE [dbo].[TradingSystemGroupHasActiveSystems_SP]
(
	@GroupId INT
)
AS
BEGIN
	SELECT 
		COUNT(system.[TradingSystemId])
	FROM 
		[dbo].[TradingSystem] system
		INNER JOIN [dbo].[TradingSystemActions] action ON action.TradingSystemId = system.TradingSystemId
	WHERE
		system.TradingSystemGroupId = @GroupId AND action.Enabled = 1
END
GO

GRANT EXECUTE ON [dbo].[TradingSystemGroupHasActiveSystems_SP] TO [IntegratorKillSwitchUser] AS [dbo]
GO

DROP PROCEDURE [dbo].[GetTradingSystemsGroupsOveralDailyStatsAndInfo_SP]
GO



CREATE TRIGGER [dbo].[TradingSystemTypeAllowedForGroup_SwitchChanged] ON [dbo].[TradingSystemTypeAllowedForGroup]
AFTER UPDATE
AS
BEGIN
--this is to prevent trigger from confusing SSMS 'Edit top 200' dialog
SET NOCOUNT ON

	UPDATE
		action
	SET
		action.GoFlatOnly = inserted.IsGoFlatOnlyOn,
		action.OrderTransmissionDisabled = inserted.IsOrderTransmissionDisabled
	FROM
		inserted
		INNER JOIN [dbo].[TradingSystem] trsystem ON inserted.TradingSystemGroupId = trsystem.TradingSystemGroupId AND inserted.TradingSystemTypeId = trsystem.TradingSystemTypeId
		INNER JOIN [dbo].[TradingSystemActions] action ON trsystem.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		UPDATE([IsGoFlatOnlyOn]) OR UPDATE([IsOrderTransmissionDisabled])
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueCrossSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueCrossSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,[MinimumSizeToTrade]
			,[MaximumSizeToTrade]
			,[MinimumDiscountBasisPointsToTrade]
			,[MinimumTimeBetweenTrades_seconds]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueCrossSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name 
	END
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueMMSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueMMSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty
			,symbol.LastKnownMeanReferencePrice AS [LastKnownMeanReferencePrice]
			,[SizeToTrade]
			,[BestPriceImprovementOffsetBasisPoints]
			,[MaximumDiscountBasisPointsToTrade]
			,[MaximumWaitTimeOnImprovementTick_milliseconds]
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueMMSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc 
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name
	END
END
GO

ALTER PROCEDURE [dbo].[GetUpdatedVenueQuotingSystemSettings_SP] (
	@LastUpdateTimeUtc DATETIME2,
	@CounterpartyCode [char](3) = NULL,
	@LastUpdateTimeServerSideUtc DATETIME2 OUTPUT
	)
AS
BEGIN
	SELECT 
		@LastUpdateTimeServerSideUtc = [LastUpdatedUtc]
	FROM 
		[dbo].[TableChangeLog]
	WHERE
		[TableName] = 'VenueQuotingSettings'
		AND [LastUpdatedUtc] > @LastUpdateTimeUtc


	IF @LastUpdateTimeServerSideUtc IS NOT NULL
	BEGIN	
	
		DECLARE @CounterpartyId INT
		
		IF @CounterpartyCode IS NOT NULL
		BEGIN
			SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode

			IF @@ROWCOUNT <> 1
			BEGIN
				RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
			END
		END
	
		SELECT 
			sett.[TradingSystemId]
			,symbol.Name AS Symbol
			,counterparty.CounterpartyCode AS Counterparty			
			,[MaximumPositionBaseAbs]
			,[FairPriceImprovementSpreadBasisPoints]
			,[StopLossNetInUsd]		
			,action.[Enabled]
			,action.[GoFlatOnly]
			,action.[OrderTransmissionDisabled]
			,action.[LastResetRequestedUtc]
			,action.[SettingsLastUpdatedUtc] AS LastUpdatedUtc
		FROM 
			[dbo].[VenueQuotingSettings] sett
			INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
			INNER JOIN [dbo].[Symbol] symbol ON sett.[SymbolId] = symbol.Id
			INNER JOIN [dbo].[Counterparty] counterparty ON sett.[CounterpartyId] = counterparty.CounterpartyId
		WHERE
			-- we need to be able to process also deletes
			--sett.[LastUpdatedUtc] > @LastUpdateTimeUtc
			(@CounterpartyId IS NULL OR counterparty.CounterpartyId = @CounterpartyId)
		ORDER BY
			symbol.Name 
	END
END
GO

--Allways include stats for unknown systems
ALTER PROCEDURE [dbo].[GetTradingSystemsDailyStats_SP]
AS
BEGIN
	SELECT
		--BEWARE! There can be multiple symbols for same system id (this is especially true for system -1)
		systemStats.SymbolId AS SymbolId,
		systemStats.TradingSystemId AS TradingSystemId,
		COALESCE(systemStats.NopBasePol, 0) AS NopBasePol,
		COALESCE(systemStats.VolumeInUsd/1000000, 0) AS VolumeUsdM,
		COALESCE(systemStats.DealsNum, 0) as DealsNum,
		COALESCE(systemStats.RejectionsNum, 0) as RejectionsNum,
		COALESCE(systemStats.PnlTermPolInUsd/(systemStats.VolumeInUsd/1000000), 0) AS PnlGrossUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd, 0) AS PnlGrossUsd,
		COALESCE(systemStats.CommissionInUSD/(systemStats.VolumeInUsd/1000000), 0) AS CommissionsUsdPerMUsd,
		COALESCE(systemStats.CommissionInUSD, 0) AS CommissionsUsd,
		COALESCE((systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD)/(systemStats.VolumeInUsd/1000000), 0) AS PnlNetUsdPerMUsd,
		COALESCE(systemStats.PnlTermPolInUsd-systemStats.CommissionInUSD, 0) AS PnlNetUsd
	FROM
		(
		SELECT
			Symbol.Id AS SymbolId,
			systemsStats.TradingSystemId AS TradingSystemId,
			systemsStats.NopBasePol AS NopBasePol,
			systemsStats.DealsNum as DealsNum,
			systemsStats.RejectionsNum as RejectionsNum,
			systemsStats.VolumeInCcy1 * baseCurrency.LastKnownUsdConversionMultiplier AS VolumeInUsd,
			--PnL = term position + yield of immediate close of outstanding base position
			--  also when closing negative outstanding base position, we opened position that can be closed buy buying - for Ask price, therefore for short we use ask price (and vice versa)
			(systemsStats.NopTermPol + (CASE WHEN systemsStats.NopBasePol < 0 THEN Symbol.LastKnownAskToBMeanPrice ELSE Symbol.LastKnownBidToBMeanPrice END) * systemsStats.NopBasePol) * termCurrency.LastKnownUsdConversionMultiplier AS PnlTermPolInUsd,
			systemsStats.CommissionInCCY1 * baseCurrency.LastKnownUsdConversionMultiplier AS CommissionInUSD
		FROM
			(
			SELECT
				COALESCE(systemsPositions.[TradingSystemId], systemRejections.[TradingSystemId]) AS [TradingSystemId],
				COALESCE(systemsPositions.[SymbolId], systemRejections.[SymbolId]) AS [SymbolId],
				NopBasePol,
				NopTermPol,
				VolumeInCcy1,
				CommissionInCCY1,
				DealsNum,
				RejectionsNum
			FROM	
				(
				--allways include stats fro unknown systems
				SELECT 
					-1 AS [TradingSystemId], 
					0 AS [SymbolId],
					NULL AS NopBasePol, 
					NULL AS NopTermPol, 
					NULL AS VolumeInCcy1, 
					NULL AS CommissionInCCY1, 
					NULL AS DealsNum
				UNION ALL
				SELECT 
					[TradingSystemId],
					[SymbolId],
					SUM([AmountBasePolExecuted]) AS NopBasePol,
					SUM(-[AmountBasePolExecuted]* [Price]) AS NopTermPol,
					SUM(ABS([AmountBasePolExecuted])) AS VolumeInCcy1,
					SUM(ABS([AmountBasePolExecuted]) * CommissionPricePerMillion / 1000000) AS CommissionInCCY1,
					COUNT([TradingSystemId]) AS DealsNum
				FROM
					[dbo].[TradingSystemDealsExecuted_Daily] deal
					INNER JOIN [dbo].[CommissionsPerCounterparty] commisions ON deal.CounterpartyId = commisions.CounterpartyId
				GROUP BY
					[TradingSystemId], [SymbolId]
				) systemsPositions
				FULL OUTER JOIN
				(
					SELECT 
						[TradingSystemId],
						[SymbolId],
						COUNT([TradingSystemId]) AS RejectionsNum
					FROM
						[dbo].[TradingSystemDealsRejected_Daily] rejects
					GROUP BY
						[TradingSystemId], [SymbolId]
				)  systemRejections ON systemsPositions.TradingSystemId = systemRejections.TradingSystemId
			) systemsStats
			INNER JOIN [dbo].[Symbol] symbol ON systemsStats.SymbolId = symbol.Id
			INNER JOIN [dbo].[Currency] termCurrency ON symbol.QuoteCurrencyId = termCurrency.CurrencyID
			INNER JOIN [dbo].[Currency] baseCurrency ON symbol.BaseCurrencyId = baseCurrency.CurrencyID
			) systemStats
END
GO

--NEW DEPLOYMENT:
--Adding FC1MM system
INSERT INTO [dbo].[TradingSystemType] ([CounterpartyId], [SystemName]) VALUES ((SELECT [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = 'FC1'), 'MM')

ALTER TABLE [dbo].[VenueCrossSettings] ALTER COLUMN MinimumTimeBetweenTrades_seconds [decimal](18,9)
GO

ALTER PROCEDURE [dbo].[VenueCrossSettingsInsertSingle_SP]
(
	@TradingGroupId int,
	@Symbol CHAR(7),
	@CounterpartyCode [char](3),
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenTrades_seconds [decimal](18,9),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int],
	@Enabled [bit],
	@TradingSystemId int OUTPUT	
)
AS
BEGIN

	DECLARE @SymbolId INT
	SELECT @SymbolId = [Id] FROM [dbo].[Symbol] WHERE [Name] = @Symbol

	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Symbol not found in DB: %s', 16, 2, @Symbol) WITH SETERROR
	END
	
	DECLARE @CounterpartyId INT
	SELECT @CounterpartyId = [CounterpartyId] FROM [dbo].[Counterparty] WHERE [CounterpartyCode] = @CounterpartyCode
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'CounterpartyCode not found in DB: %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END

	DECLARE @TradingSystemTypeId INT
	SELECT @TradingSystemTypeId = [TradingSystemTypeId] FROM [dbo].[TradingSystemType] WHERE [CounterpartyId] = @CounterpartyId AND [SystemName] = 'Cross'
	IF @@ROWCOUNT <> 1
	BEGIN
		RAISERROR (N'Cross TradingSystemType not found in DB for : %s', 16, 2, @CounterpartyCode) WITH SETERROR
	END
	
	INSERT INTO [dbo].[TradingSystem] ([SymbolId], [TradingSystemTypeId], [TradingSystemGroupId]) VALUES (@SymbolId, @TradingSystemTypeId, @TradingGroupId)

	SELECT @TradingSystemId = scope_identity();
	
	--make sure that default switches values are correct
	INSERT INTO [dbo].[TradingSystemActions] ([TradingSystemId], [SymbolId], [CounterpartyId], [Enabled], [GoFlatOnly], [OrderTransmissionDisabled]) 
	SELECT @TradingSystemId, @SymbolId, @CounterpartyId, @Enabled, switches.[IsGoFlatOnlyOn], switches.[IsOrderTransmissionDisabled]
	FROM [dbo].[TradingSystemTypeAllowedForGroup] switches
	WHERE switches.TradingSystemGroupId = @TradingGroupId AND switches.TradingSystemTypeId = @TradingSystemTypeId
	
	INSERT INTO [dbo].[VenueCrossSettings]
           ([SymbolId]
           ,[MinimumSizeToTrade]
           ,[MaximumSizeToTrade]
           ,[MinimumDiscountBasisPointsToTrade]
           ,[MinimumTimeBetweenTrades_seconds]
           ,[MaximumWaitTimeOnImprovementTick_milliseconds]
           ,[CounterpartyId]
           ,[TradingSystemId])
     VALUES
           (@SymbolId
           ,@MinimumSizeToTrade
           ,@MaximumSizeToTrade
           ,@MinimumDiscountBasisPointsToTrade
           ,@MinimumTimeBetweenTrades_seconds
           ,@MaximumWaitTimeOnImprovementTick_milliseconds
           ,@CounterpartyId
           ,@TradingSystemId)
END
GO

ALTER PROCEDURE [dbo].[VenueCrossSystemSettingsUpdateSingle_SP]
(
	@TradingSystemId int,
	@LastUpdateTimeUtc DATETIME2,
	@MinimumSizeToTrade [decimal](18,0),
	@MaximumSizeToTrade [decimal](18,0),
	@MinimumDiscountBasisPointsToTrade [decimal](18,6),
	@MinimumTimeBetweenTrades_seconds [decimal](18,9),
	@MaximumWaitTimeOnImprovementTick_milliseconds [int]
)
AS
BEGIN
	UPDATE sett
	SET
		[MinimumSizeToTrade] = @MinimumSizeToTrade,
		[MaximumSizeToTrade] = @MaximumSizeToTrade,
		[MinimumDiscountBasisPointsToTrade] = @MinimumDiscountBasisPointsToTrade,
		[MinimumTimeBetweenTrades_seconds] = @MinimumTimeBetweenTrades_seconds,
		[MaximumWaitTimeOnImprovementTick_milliseconds] = @MaximumWaitTimeOnImprovementTick_milliseconds
	FROM
		[dbo].[VenueCrossSettings] sett
		INNER JOIN [dbo].[TradingSystemActions] action ON sett.[TradingSystemId] = action.[TradingSystemId]
	WHERE
		sett.[TradingSystemId] = @TradingSystemId
		AND action.[SettingsLastUpdatedUtc] <= @LastUpdateTimeUtc
END
GO