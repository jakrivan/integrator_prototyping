
--Disable FKs first
ALTER TABLE [dbo].[Tick] DROP CONSTRAINT [FK_Tick_FXPair]
GO
ALTER TABLE [dbo].[MarketData] DROP CONSTRAINT [FK_MarketData_FxPair]
GO


--Add new symbols
SET IDENTITY_INSERT [dbo].[FxPair] ON 

INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (151, N'EURSGD', 167, 114, 10000, 1.72886, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (152, N'GBPHKD', 135, 53, 1000, 12.7303, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (153, N'GBPMXN', 135, 84, 1000, 21.79734, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (154, N'GBPSGD', 135, 114, 10000, 2.08222, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (155, N'GBPTRY', 135, 151, 10000, 3.63257, NULL, NULL)
GO
INSERT [dbo].[FxPair] ([FxPairId], [FxpCode], [BaseCurrencyId], [QuoteCurrencyId], [Multiplier], [ExamplePrice], [IsRaboTradable], [Usage]) VALUES (156, N'GBPZAR', 135, 119, 1000, 18.1361, NULL, NULL)
GO

SET IDENTITY_INSERT [dbo].[FxPair] OFF

--remove SKK functions
DELETE FROM [dbo].[FxPair]
      WHERE FxPairId = 73
GO
DELETE FROM [dbo].[FxPair]
      WHERE FxPairId = 74
GO

--unfortunately not possible
--ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction (tinyint)
--AS RANGE LEFT FOR VALUES (3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 22, 43, 45, 46, 47, 48, 49, 50, 51, 56, 57, 58, 59, 60, 61, 62, 63, 65, 66, 67, 68, 70, 72, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 95, 96, 97, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 113, 117, 128, 129, 133, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156)
--GO

--removing partitions for SKK
ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
	MERGE RANGE (73);

ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
	MERGE RANGE (74);
GO


--adding partitions for new symbols
ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme_AnotherFileGroup NEXT USED [MarketDataFileGroup]
GO
ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme NEXT USED [MarketDataFileGroup]
GO

ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction()
    SPLIT RANGE (151);
GO

ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme_AnotherFileGroup NEXT USED [MarketDataFileGroup]
GO
ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme NEXT USED [MarketDataFileGroup]
GO

ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction() 
    SPLIT RANGE (152);
GO

ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme_AnotherFileGroup NEXT USED [MarketDataFileGroup]
GO
ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme NEXT USED [MarketDataFileGroup]
GO

ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction() 
    SPLIT RANGE (153);
GO

ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme_AnotherFileGroup NEXT USED [MarketDataFileGroup]
GO
ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme NEXT USED [MarketDataFileGroup]
GO

ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction() 
    SPLIT RANGE (154);
GO

ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme_AnotherFileGroup NEXT USED [MarketDataFileGroup]
GO
ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme NEXT USED [MarketDataFileGroup]
GO

ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction() 
    SPLIT RANGE (155);
GO

ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme_AnotherFileGroup NEXT USED [MarketDataFileGroup]
GO
ALTER PARTITION SCHEME MarketDataSymbolPartitionScheme NEXT USED [MarketDataFileGroup]
GO

ALTER PARTITION FUNCTION MarketDataSymbolPartitioningFunction() 
    SPLIT RANGE (156);
GO


--Adding back the FKs
ALTER TABLE [dbo].[Tick]  WITH NOCHECK ADD  CONSTRAINT [FK_Tick_FXPair] FOREIGN KEY([FXPairID])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO

ALTER TABLE [dbo].[MarketData]  WITH NOCHECK ADD  CONSTRAINT [FK_MarketData_FxPair] FOREIGN KEY([FXPairId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO



--Trading Target type

CREATE TABLE [dbo].[TradingTargetType](
	[Id] [tinyint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Code] [varchar](16) NOT NULL
) ON [PRIMARY]

GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_TradingTargetTypeCode] ON [dbo].[TradingTargetType]
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [PK_TradingTargetType] ON [dbo].[TradingTargetType]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (0, N'BankPool', N'BankPool')
GO
INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (1, N'Hotspot', N'Hotspot')
GO
INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (2, N'FXall', N'FXall')
GO
INSERT [dbo].[TradingTargetType] ([Id], [Name], [Code]) VALUES (3, N'LMAX', N'LMAX')
GO


CREATE TABLE [dbo].[CounterpartySymbolSettings](
	[TradingTargetTypeId] [tinyint] NOT NULL,
	[SymbolId] [tinyint] NOT NULL,
	[Supported] [bit] NOT NULL,
	[OnePipDecimalValue] [decimal](18, 11) NULL,
	[MinimumPriceGranularity] [decimal](18, 11) NULL
) ON [PRIMARY]

GO


CREATE UNIQUE NONCLUSTERED INDEX [UniqueTradingTargetTypeAndSymbolCombination] ON [dbo].[CounterpartySymbolSettings]
(
	[TradingTargetTypeId] ASC,
	[SymbolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CounterpartySymbolSettings]  WITH CHECK ADD  CONSTRAINT [FK_CounterpartySymbolSettings_FxPair] FOREIGN KEY([SymbolId])
REFERENCES [dbo].[FxPair] ([FxPairId])
GO

ALTER TABLE [dbo].[CounterpartySymbolSettings] CHECK CONSTRAINT [FK_CounterpartySymbolSettings_FxPair]
GO

ALTER TABLE [dbo].[CounterpartySymbolSettings]  WITH CHECK ADD  CONSTRAINT [FK_CounterpartySymbolSettings_TradingTargetType] FOREIGN KEY([TradingTargetTypeId])
REFERENCES [dbo].[TradingTargetType] ([Id])
GO

ALTER TABLE [dbo].[CounterpartySymbolSettings] CHECK CONSTRAINT [FK_CounterpartySymbolSettings_TradingTargetType]
GO


CREATE PROCEDURE [dbo].[GetCounterpartySymbolSettings_SP] 
AS
BEGIN

	SELECT 
		targetType.Code AS TradingTargetType
		,pair.FxpCode AS Symbol
		,settings.[Supported] AS Supported
		,settings.[MinimumPriceGranularity] AS MinimumPriceGranularity
	FROM 
		[dbo].[CounterpartySymbolSettings] settings
		INNER JOIN [dbo].[FxPair] pair ON pair.FxPairId = settings.[SymbolId]
		INNER JOIN [dbo].[TradingTargetType] targetType ON targetType.Id = settings.[TradingTargetTypeId]

END
GO

GRANT EXECUTE ON [dbo].[GetCounterpartySymbolSettings_SP] TO [IntegratorServiceAccount] AS [dbo]
GO


--Values Insertion

--HOTSPOT
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDCAD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDCHF'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDDKK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDHKD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDJPY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDNOK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDNZD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDSEK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDSGD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDUSD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADCHF'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADDKK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADJPY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADNOK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADNZD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADSEK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFDKK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFJPY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFNOK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFSEK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'DKKJPY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'DKKNOK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'DKKSEK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURAUD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCAD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCHF'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCNH'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCZK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURDKK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURGBP'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURHKD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURHUF'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURJPY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURMXN'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURNOK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURNZD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURPLN'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.0025
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURRUB'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURSEK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURSGD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURTRY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURUSD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURZAR'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPAUD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPCAD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPCHF'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPCZK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPDKK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPHKD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPHUF'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPJPY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPMXN'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPNOK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPNZD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPPLN'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPSEK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPSGD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPTRY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPUSD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPZAR'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'HKDJPY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'MXNJPY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NOKJPY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NOKSEK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDCAD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDCHF'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDDKK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDJPY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDNOK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDSEK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDSGD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDUSD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'SGDJPY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCAD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCHF'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCNH'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCZK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDDKK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDHKD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDHUF'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDILS'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDJPY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDMXN'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDNOK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDPLN'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.0025
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDRUB'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDSEK'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDSGD'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDTRY'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDZAR'
 where Name = 'Hotspot'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.01
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'ZARJPY'
 where Name = 'Hotspot'
GO

--BankPool
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDCAD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDCHF'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDDKK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDHKD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDJPY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDNOK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDNZD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDSEK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDSGD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDUSD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADCHF'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADDKK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADJPY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADNOK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.000001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADNZD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADSEK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFDKK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFJPY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFNOK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFSEK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'DKKJPY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'DKKNOK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'DKKSEK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURAUD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCAD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCHF'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCNH'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCZK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURDKK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURGBP'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURHKD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURHUF'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURJPY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURMXN'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURNOK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURNZD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURPLN'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURRUB'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURSEK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURSGD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURTRY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURUSD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURZAR'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPAUD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPCAD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPCHF'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPCZK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPDKK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPHKD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPHUF'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPJPY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPMXN'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPNOK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPNZD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPPLN'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPSEK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPSGD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPTRY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPUSD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPZAR'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'HKDJPY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'MXNJPY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NOKJPY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NOKSEK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDCAD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDCHF'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDDKK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDJPY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDNOK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDSEK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDSGD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDUSD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'SGDJPY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCAD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCHF'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCNH'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCZK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDDKK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDHKD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDHUF'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDILS'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.01, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDJPY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDMXN'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDNOK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDPLN'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDRUB'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDSEK'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDSGD'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDTRY'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDZAR'
 where Name = 'BankPool'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'ZARJPY'
 where Name = 'BankPool'
GO


--LMAX
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDCAD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDCHF'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDDKK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDHKD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDJPY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDNOK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDNZD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDSEK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDSGD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'AUDUSD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADCHF'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADDKK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADJPY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADNOK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADNZD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CADSEK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFDKK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFJPY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFNOK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'CHFSEK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'DKKJPY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'DKKNOK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'DKKSEK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURAUD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCAD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCHF'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCNH'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURCZK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURDKK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURGBP'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURHKD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURHUF'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURJPY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURMXN'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURNOK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURNZD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURPLN'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURRUB'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURSEK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURSGD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURTRY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURUSD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'EURZAR'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPAUD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPCAD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPCHF'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPCZK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPDKK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPHKD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPHUF'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPJPY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPMXN'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPNOK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPNZD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPPLN'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPSEK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPSGD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPTRY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPUSD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'GBPZAR'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'HKDJPY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'MXNJPY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NOKJPY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NOKSEK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDCAD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDCHF'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDDKK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDJPY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDNOK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDSEK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDSGD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'NZDUSD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'SGDJPY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCAD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCHF'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCNH'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.0001, 0.0001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDCZK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDDKK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDHKD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDHUF'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDILS'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.001, 0.001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDJPY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDMXN'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDNOK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDPLN'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDRUB'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDSEK'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDSGD'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDTRY'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 1, 0.00001, 0.00001
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'USDZAR'
 where Name = 'LMAX'
GO
INSERT INTO [dbo].[CounterpartySymbolSettings]
           ([TradingTargetTypeId]
           ,[SymbolId]
           ,[Supported]
           ,[OnePipDecimalValue]
           ,[MinimumPriceGranularity])
SELECT TargetType.[Id], [FxPairId], 0, NULL, NULL
FROM 
 [dbo].[TradingTargetType] TargetType
 INNER JOIN [dbo].[FxPair] pair on pair.FxpCode = 'ZARJPY'
 where Name = 'LMAX'
GO

