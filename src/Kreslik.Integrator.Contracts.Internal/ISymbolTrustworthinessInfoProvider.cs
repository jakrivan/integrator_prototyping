﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts.Internal
{
    public interface ISymbolTrustworthinessInfoProvider
    {
        List<SymbolTrustworthinessInfo> SymbolTrustworthinessInfo { get; }
    }
    public enum SymbolTrustState
    {
        Trusted,
        Trusted_CheckTurnedOff,
        Untrusted_Initial,
        Untrusted_NotConfigured,
        Untrusted_PriceCancelled,
        Untrusted_PriceTooOld,
        Untrusted_SpreadOverMax,
        Untrusted_SpreadUnderMin,
        Untrusted_DeviationCheckNotInitialized,
        Untrusted_Rollover
    }

    [DataContract]
    public class SymbolTrustworthinessInfo
    {
        public void SetTrustState(bool isTrusted, SymbolTrustState trustState)
        {
            if (isTrusted != this.IsTrusted)
                this.LastTrustStateChange = HighResolutionDateTime.UtcNow;
            this.IsTrusted = isTrusted;
            this.LastTrustState = trustState;
        }
        [DataMember]
        public bool IsTrusted { get; private set; }
        [DataMember]
        public DateTime LastTrustStateChange { get; private set; }
        [DataMember]
        public SymbolTrustState LastTrustState { get; private set; }
        [DataMember]
        public decimal MaxTrustedSpreadBp { get; set; }
        [DataMember]
        public decimal MaxTrustedSpread { get; set; }
        
    }
}
