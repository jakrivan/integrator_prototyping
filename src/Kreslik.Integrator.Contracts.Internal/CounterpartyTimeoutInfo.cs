﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts.Internal
{
    public class CounterpartyTimeoutInfo
    {
        public CounterpartyTimeoutInfo(DateTime integratorReceivedTimeoutUtc, DateTime counterpartySentTimeoutUtc, string counterpartyClientOrderId, string counterpartyExecutionId)
        {
            this.IntegratorReceivedTimeoutUtc = integratorReceivedTimeoutUtc;
            this.CounterpartySentTimeoutUtc = counterpartySentTimeoutUtc;
            this.CounterpartyClientOrderId = counterpartyClientOrderId;
            this.CounterpartyExecutionId = counterpartyExecutionId;
        }

        public DateTime IntegratorReceivedTimeoutUtc { get; private set; }
        public DateTime CounterpartySentTimeoutUtc { get; private set; }
        public string CounterpartyClientOrderId { get; private set; }
        public string CounterpartyExecutionId { get; private set; }

    }
}
