﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts.Internal
{
    public struct StreamingPricesProducerIndex
    {
        public static StreamingPricesProducerIndex NullIndex = new StreamingPricesProducerIndex(-1, -1);

        public StreamingPricesProducerIndex(int layerId, int tradingSystemId)
            : this()
        {
            this.LayerId = layerId;
            this.IntegratedTradingSystemIdentification = tradingSystemId;
        }
        public int LayerId { get; private set; }
        public int IntegratedTradingSystemIdentification { get; private set; }

        public static bool operator !=(StreamingPricesProducerIndex a, StreamingPricesProducerIndex b)
        {
            return a.LayerId != b.LayerId;
        }

        public static bool operator ==(StreamingPricesProducerIndex a, StreamingPricesProducerIndex b)
        {
            return a.LayerId == b.LayerId;
        }
    }
}
