﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts.Internal
{
    public enum CancelType
    {
        Cancel,
        CancelAndReplace,
        CancelAll
    }

    public class ExternalCancelRequest
    {
        public ExternalCancelRequest(DateTime integratorSentTimeUtc, CancelType cancelType,
                                     string outgoingFixMessageRaw)
        {
            this.IntegratorSentTimeUtc = integratorSentTimeUtc;
            this.CancelType = cancelType;
            this.OutgoingFixMessageRaw = outgoingFixMessageRaw;
        }

        public DateTime IntegratorSentTimeUtc { get; private set; }
        public CancelType CancelType { get; private set; }
        public string OutgoingFixMessageRaw { get; private set; }
    }
}
