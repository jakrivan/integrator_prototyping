﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts.Internal
{
    public enum IntegratorOrderExternalStatus
    {
        NotSubmitted,
        Submitted,
        ConfirmedNew,
        Filled,
        Rejected,
        Cancelled,
        InBrokenState,
        PartiallyFilled,
        NonConfirmedDeal
    }

    public enum IntegratorOrderExternalChange
    {
        NotSubmitted,
        Submitted,   // currently not assigned anywhere - but this may come in handy once using async orders sending
        ConfirmedNew,
        Filled,
        Rejected,
        PendingCancel,
        Cancelled,
        CancelRejected,
        InBrokenState,
        PartiallyFilled,
        NonConfirmedDeal
    }
}
