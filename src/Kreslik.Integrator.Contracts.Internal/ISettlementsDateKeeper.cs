﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts.Internal
{
    public interface ISettlementDatesProvider
    {
        bool TryRequestSettlemtDatesInfo();
        event Action<IEnumerable<Tuple<Common.Symbol, DateTime>>> NewSettlemtDatesInfo;
    }

    public interface ISettlementDatesInfoPersistor
    {
        void UpdateSettlementInfo(IEnumerable<Tuple<Symbol, DateTime>> info);
        void ClearAllSettlemntInfo();
        void ClearSettlemntInfo(IEnumerable<Symbol> symbols);
    }

    public interface ISettlementDatesKeeper
    {
        event Action<Symbol, bool> InfoAvailabilityChanged;
        event Action AllInfoAvailabilityRemoved;
        DateTime? GetSettlementDate(Symbol symbol);
        bool HasSettlementDateInfo(Symbol symbol);
    }

    public class SettlementDatesKeeper : ISettlementDatesKeeper
    {
        private DateTime?[] _settlementDates = Enumerable.Repeat((DateTime?) null, Symbol.ValuesCount).ToArray();
        private DateTime?[] _oldSettlementDates = Enumerable.Repeat((DateTime?)null, Symbol.ValuesCount).ToArray();
        private List<ISettlementDatesProvider> _settlementDatesProviders = new List<ISettlementDatesProvider>();
        private ISettlementDatesInfoPersistor _settlementDatesInfoPersistor;
        private ILogger _logger;
        private SafeTimer _lazyTimer;

        public event Action<Symbol, bool> InfoAvailabilityChanged;
        public event Action AllInfoAvailabilityRemoved;

        public SettlementDatesKeeper(ISettlementDatesInfoPersistor settlementsDatesInfoPersistor, ILogger logger)
        {
            this._logger = logger;
            this._settlementDatesInfoPersistor = settlementsDatesInfoPersistor;

            ITradingHoursHelper tradingHoursHelper = TradingHoursHelper.Instance;

            tradingHoursHelper.MarketRolloverApproaching += TradingHoursHelperOnMarketRolloverApproaching;
            tradingHoursHelper.MarketRolloverDone += TradingHoursHelperOnMarketRolloverDone;
            tradingHoursHelper.MarketRollingOver += TradingHoursHelperOnMarketRollingOver;
            tradingHoursHelper.NZDRolloverApproaching += TradingHoursHelperOnNzdRolloverApproaching;
            tradingHoursHelper.NZDRolloverDone += TradingHoursHelperOnNzdRolloverDone;
            tradingHoursHelper.NZDRollingOver += TradingHoursHelperOnNzdRollingOver;

            _lazyTimer = new SafeTimer(RequestSettlemtDatesRefresh, TimeSpan.FromSeconds(20), TimeSpan.FromHours(2))
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1min,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
        }

        public void RegisterSettlementDatesProvider(ISettlementDatesProvider settlementDatesProvider)
        {
            this._settlementDatesProviders.Add(settlementDatesProvider);
            settlementDatesProvider.NewSettlemtDatesInfo += SettlementsDateProviderOnNewSettlemtDatesInfo;
        }

        private void SettlementsDateProviderOnNewSettlemtDatesInfo(IEnumerable<Tuple<Symbol, DateTime>> enumerable)
        {
            _lastUpdated = DateTime.UtcNow;
            this._logger.Log(LogLevel.Info, "SettlementsDateKeeper received refresh of settlement dates");

            List<Tuple<Symbol, DateTime>> settlInfos = enumerable.ToList();
            foreach (Tuple<Symbol, DateTime> tuple in settlInfos)
            {
                if (!_settlementDates[(int) tuple.Item1].HasValue && InfoAvailabilityChanged != null)
                {
                    _settlementDates[(int)tuple.Item1] = tuple.Item2;
                    InfoAvailabilityChanged(tuple.Item1, true);
                }
                else
                    _settlementDates[(int)tuple.Item1] = tuple.Item2;
                _oldSettlementDates[(int)tuple.Item1] = tuple.Item2;
            }

            this._settlementDatesInfoPersistor.UpdateSettlementInfo(settlInfos);
        }

        private void TradingHoursHelperOnNzdRolloverDone()
        {
            this.RequestSettlemtDatesRefresh();
        }

        private void TradingHoursHelperOnNzdRollingOver()
        {
            //not clearing the info earlier, just for case if deal comes in the meantime
            foreach (
                Symbol symbol in
                    Symbol.Values.Where(s => s.BaseCurrency == Currency.NZD || s.TermCurrency == Currency.NZD))
            {
                _oldSettlementDates[(int)symbol] = null;
            }
        }

        private void TradingHoursHelperOnNzdRolloverApproaching()
        {
            foreach (
                Symbol symbol in
                    Symbol.Values.Where(s => s.BaseCurrency == Currency.NZD || s.TermCurrency == Currency.NZD))
            {
                _oldSettlementDates[(int)symbol] = _settlementDates[(int)symbol];
                _settlementDates[(int)symbol] = null;
                if (InfoAvailabilityChanged != null)
                    InfoAvailabilityChanged(symbol, false);
            }

            this._settlementDatesInfoPersistor.ClearSettlemntInfo(
                Symbol.Values.Where(s => s.BaseCurrency == Currency.NZD || s.TermCurrency == Currency.NZD));
        }

        private void TradingHoursHelperOnMarketRolloverDone()
        {
            this.RequestSettlemtDatesRefresh();
        }

        private void TradingHoursHelperOnMarketRollingOver()
        {
            //not clearing the info earlier, just for case if deal comes in the meantime
            Array.Clear(_oldSettlementDates, 0, _oldSettlementDates.Length);
        }

        private void TradingHoursHelperOnMarketRolloverApproaching()
        {
            Array.Copy(_settlementDates, _oldSettlementDates, _settlementDates.Length);
            Array.Clear(_settlementDates, 0, _settlementDates.Length);

            if (AllInfoAvailabilityRemoved != null)
                AllInfoAvailabilityRemoved();

            this._settlementDatesInfoPersistor.ClearAllSettlemntInfo();
        }

        private bool _requestingRefresh = false;
        private void RequestSettlemtDatesRefresh()
        {
            if (_requestingRefresh)
            {
                _logger.Log(LogLevel.Info, "SettlementsDateKeeper needed to request settlemnt dates update, however other attempt is in progress - giving up");
                return;
            }
            _requestingRefresh = true;

            _logger.Log(LogLevel.Trace, "SettlementsDateKeeper requesting settlemnt dates update");

            //let's request the info allways in random order - if one session is busted so that we can still continue
            if (_settlementDatesProviders.OrderRandomly().Any(settlementDatesProvider => settlementDatesProvider.TryRequestSettlemtDatesInfo()))
            {
                _requestingRefresh = false;
                VerifySettlmentsInfoUpdated();
                return;
            }

            TaskEx.StartNew(() =>
            {
                int tries = 0;
                Random rand = new Random();
                do
                {
                    _logger.Log(LogLevel.Error,
                        "SettlementsDateKeeper wasn't able to request settlement dates refresh - will retry");
                    Thread.Sleep(TimeSpan.FromMinutes(++tries % 15));
                    if (_settlementDatesProviders.OrderRandomly().Any(settlementDatesProvider => settlementDatesProvider.TryRequestSettlemtDatesInfo()))
                    {
                        _requestingRefresh = false;
                        VerifySettlmentsInfoUpdated();
                        return;
                    }
                } while (true);
            });
        }

        private DateTime _lastUpdated;
        private void VerifySettlmentsInfoUpdated()
        {
            TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(10), () =>
            {
                if (DateTime.UtcNow - _lastUpdated > TimeSpan.FromMinutes(1))
                {
                    this._logger.Log(LogLevel.Error,
                        "SettlementsDateKeeper didn't refresh it's state despit it recently requested refresh. Requesting again");
                    this.RequestSettlemtDatesRefresh();
                }
            });
        }

        public DateTime? GetSettlementDate(Symbol symbol)
        {
            return this._settlementDates[(int) symbol] ?? this._oldSettlementDates[(int) symbol];
        }

        public bool HasSettlementDateInfo(Symbol symbol)
        {
            //intentionally not looking on aged value - as this check is only pre-send check
            return this._settlementDates[(int) symbol].HasValue;
        }
    }

    public class DummySettlmentDatesKeeper : ISettlementDatesKeeper
    {

        public event Action<Symbol, bool> InfoAvailabilityChanged;

        public event Action AllInfoAvailabilityRemoved;

        public DateTime? GetSettlementDate(Symbol symbol)
        {
            return DateTime.MinValue;
        }

        public bool HasSettlementDateInfo(Symbol symbol)
        {
            return true;
        }
    }
}
