﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts.Internal
{
    public class StreamingIdsGeneratorHelper
    {
        public StreamingIdsGeneratorHelper(Counterparty cpt)
        {
            _uniqueIdPrefix = ByteUtils.ConvertToBase(
                ((((DateTime.UtcNow.Year % 100) * 100 + DateTime.UtcNow.Month) * 100 + DateTime.UtcNow.Day) * 100 + DateTime.UtcNow.Hour) * 100 + DateTime.UtcNow.Minute,
                true) + ByteUtils.ConvertToBase((int)cpt, true);

        }

        private readonly string _uniqueIdPrefix;

        private void GetUniqueId(Common.Symbol s, DealDirection d, int layerId, byte[] buffer, int startIdx, ref int counter)
        {
            // [layer in base - 1 byte] [Side - 1 byte [B|S]] [Symbol in base - 2 bytes] [seq id in base] : [Date in base] [CtpId in base] 
            //                                Can be cutted if whole id is too long   \---------------------------/

            if (buffer.Length != StreamingPrice.MAX_INTEGRATOR_IDENTITY_LENGTH)
                throw new Exception();

            int bufferIdx = startIdx;
            buffer[bufferIdx++] = ByteUtils.ConvertToBaseByte(layerId, false);
            buffer[bufferIdx++] = _sideIds[(int)d];
            ByteUtils.ReverseFillBufferWithIntConvertedToBase(buffer, false, (int)s, ref bufferIdx);
            if (bufferIdx == 3)
                buffer[bufferIdx++] = (byte)'0';
            ByteUtils.ReverseFillBufferWithIntConvertedToBase(buffer, false, Interlocked.Increment(ref counter), ref bufferIdx);

            for (int prefixIdx = 0; prefixIdx < _uniqueIdPrefix.Length && bufferIdx + prefixIdx < buffer.Length; prefixIdx++)
            {
                buffer[bufferIdx + prefixIdx] = (byte)_uniqueIdPrefix[_uniqueIdPrefix.Length - prefixIdx - 1];
            }
        }

        private int _quotesNum = 0;
        public string GetQuoteEntryId(Common.Symbol s, PriceSide outgoingPriceSide, int layerId)
        {
            byte[] buffer = new byte[StreamingPrice.MAX_INTEGRATOR_IDENTITY_LENGTH];
            //not needed - but make sure that ExecIds and quote ids are offsetted same (1 char, then layer, then side etc.)
            buffer[0] = (byte)'Q';
            this.GetUniqueId(s, outgoingPriceSide.ToOutgoingDealDirection().ToOpositeDirection(), layerId, buffer, 1, ref this._quotesNum);

            return buffer.ConvertToAsciiStringTrimingNulls();
        }

        private int _execsNum = 0;
        private const byte _rejectId = (byte)'R';
        private const byte _acceptId = (byte)'A';
        private static readonly byte[] _sideIds = new[] { (byte)'B', (byte)'S' };
        public string GetExecId(Common.Symbol s, DealDirection d, int layerId, bool isReject)
        {
            byte[] buffer = new byte[StreamingPrice.MAX_INTEGRATOR_IDENTITY_LENGTH];
            buffer[0] = isReject ? _rejectId : _acceptId;
            this.GetUniqueId(s, d, layerId, buffer, 1, ref this._execsNum);

            return buffer.ConvertToAsciiStringTrimingNulls();
        }

        public static DealDirection? TryExtractIntegratorDirectionFromId(string integratorGeneratedIdentity)
        {
            if (string.IsNullOrEmpty(integratorGeneratedIdentity) || integratorGeneratedIdentity.Length <= 2)
                return null;

            DealDirection? integratorDealDirection = null;

            int directionId = Array.IndexOf(_sideIds, (byte)integratorGeneratedIdentity[2]);

            if (directionId >= 0 && directionId < _sideIds.Length)
            {
                integratorDealDirection = (DealDirection)directionId;
            }

            return integratorDealDirection;
        }

        public static int ExtractLayerId(string integratorGeneratedIdentity)
        {
            return ByteUtils.ConvertFromBaseByte((byte)integratorGeneratedIdentity[1]);
        }
    }
}
