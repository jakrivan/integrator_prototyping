﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts.Internal
{
    public enum IntegratedTradingSystemType
    {
        MM,
        Cross,
        SmartCross,
        Quoting,
        Stream,
        Glider
    }

    [DataContract]
    [KnownType(typeof(WrittableTradingSystemGroup))]
    public class TradingSystemGroup
    {
        public TradingSystemGroup(int tradingSystemGroupIdentity, string initialName)
        {
            this.TradingSystemGroupIdentity = tradingSystemGroupIdentity;
            this.LastKnownName = initialName;
        }

        [DataMember]
        public int TradingSystemGroupIdentity { get; private set; }

        [DataMember]
        public string LastKnownName { get; private set; }

        protected void UpdateNameInternal(string name)
        {
            this.LastKnownName = name;
        }

        public override string ToString()
        {
            return string.Format("TradingGroup [{0}]-[{1}]", TradingSystemGroupIdentity, LastKnownName);
        }
    }

    [DataContract]
    public class WrittableTradingSystemGroup : TradingSystemGroup
    {
        public WrittableTradingSystemGroup(int tradingSystemGroupIdentity, string initialName)
            : base(tradingSystemGroupIdentity, initialName)
        { }

        public void UpdateName(string name)
        {
            base.UpdateNameInternal(name);
        }
    }

    [DataContract]
    public class IntegratedTradingSystemIdentification
    {
        public IntegratedTradingSystemIdentification(
            int integratedTradingSystemIdentity,
            IntegratedTradingSystemType integratedTradingSystemType,
            Counterparty targetVenueCounterparty,
            TradingSystemGroup tradingSystemGroup)
        {
            this.IntegratedTradingSystemIdentity = integratedTradingSystemIdentity;
            this.IntegratedTradingSystemType = integratedTradingSystemType;
            this.TargetVenueCounterparty = targetVenueCounterparty;
            this.TradingSystemGroup = tradingSystemGroup;
        }

        [DataMember]
        public int IntegratedTradingSystemIdentity { get; private set; }
        [DataMember]
        public IntegratedTradingSystemType IntegratedTradingSystemType { get; private set; }

        public Counterparty TargetVenueCounterparty { get; private set; }

        [DataMember]
        public string TargetVenueCounterpartyName
        {
            get { return this.TargetVenueCounterparty.ToString(); }
            set { this.TargetVenueCounterparty = (Counterparty)value; }
        }

        [DataMember]
        public TradingSystemGroup TradingSystemGroup { get; private set; }

        public override string ToString()
        {
            return string.Format("{0} {1} IntegratedSystem [{2}], {3}", TargetVenueCounterparty, IntegratedTradingSystemType,
                                 IntegratedTradingSystemIdentity, TradingSystemGroup);
        }
    }
}
