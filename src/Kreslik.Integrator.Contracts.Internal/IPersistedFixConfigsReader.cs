﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts.Internal
{
    public interface IFluentCredentials
    {
        string UserName { get; }
        string Password { get; }
        string OnBehalfOfCompId { get; }
    }

    public interface IFluentRedirectionInfo
    {
        bool IsRedirectedViaFlunet(Counterparty cpt);
    }

    public interface IPersistedFixConfigsReader
    {
        TextReader ReadFixConfig();
        string ConfigName { get; }
        byte[] GetCertificateFile(string certificateName);
        XmlDocument GetXmlDictionary(string dictionaryName);
        bool IsQuotingSession { get; }
        bool IsRedirectedViaFluent { get; }

        IFluentCredentials FluentCredentials { get; }
    }
}
