﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts.Internal
{
    public class StreamingPrice : IIntegratorOrderInfo
    {
        public const int MAX_INTEGRATOR_IDENTITY_LENGTH = 20;

        public StreamingPrice()
        { }

        public void OverrideContent(Common.Symbol s, Counterparty counterparty, PriceSide side, decimal sizeBaseAbs,
            decimal minSizeBaseAbs, decimal price,
            StreamingPricesProducerIndex priceProducerIndex)
        {
            this.Symbol = s;
            this.Counterparty = counterparty;
            this.Side = side;
            this.SizeBaseAbs = sizeBaseAbs;
            this.MinSizeBaseAbs = minSizeBaseAbs;
            this.Price = price;
            this.PriceProducerIndex = priceProducerIndex;
        }

        protected void ClearContent()
        {
            this.Symbol = Symbol.NULL;
            this.Counterparty = Counterparty.NULL;
            this.Side = PriceSide.Bid;
            this.SizeBaseAbs = 0m;
            this.MinSizeBaseAbs = 0m;
            this.Price = 0m;
            this.PriceProducerIndex = StreamingPricesProducerIndex.NullIndex;

            this.IntegratorSentTimeUtc = DateTime.MinValue;
            this.IntegratorPriceIdentity = null;
        }

        public Common.Symbol Symbol { get; private set; }

        public Counterparty Counterparty { get; private set; }
        public PriceSide Side { get; private set; }

        public decimal SizeBaseAbs { get; private set; }
        public decimal MinSizeBaseAbs { get; private set; }
        public decimal Price { get; private set; }

        public StreamingPricesProducerIndex PriceProducerIndex { get; private set; }
        public int LayerId { get { return PriceProducerIndex.LayerId; } }


        //TODO: this object - once pooled - should contain also the buffer segment for quoteId 
        // and also buffer segment for the actual FIX message

        #region IIntegratorOrderInfo
        public string Identity
        {
            get { return string.IsNullOrEmpty(this.IntegratorPriceIdentity) ? "StreamingPrice" : this.IntegratorPriceIdentity; }
        }

        public DealDirection IntegratorDealDirection
        {
            //sides are for counterparties which will deal on them from their perspectives
            get { return Side.ToOppositeSide().ToOutgoingDealDirection(); }
        }

        public decimal SizeBaseAbsInitial
        {
            get { return this.SizeBaseAbs; }
        }

        public decimal RequestedPrice
        {
            get { return this.Price; }
        }

        public IntegratorOrderInfoType IntegratorOrderInfoType
        {
            get { return IntegratorOrderInfoType.ExternalOrder; }
        }
        public bool IsRiskRemovingOrder { get { return false; } }
        #endregion IIntegratorOrderInfo

        public DateTime IntegratorSentTimeUtc { get; set; }
        public string IntegratorPriceIdentity { get; set; }
    }

    public class StreamingPriceCancel
    {
        public StreamingPriceCancel(Common.Symbol s, Counterparty counterparty, PriceSide? side, StreamingPricesProducerIndex? priceProducerIndex)
        {
            this.Symbol = s;
            this.Counterparty = counterparty;
            this.Side = side;
            this.PriceProducerIndex = priceProducerIndex;
        }

        public StreamingPriceCancel(StreamingPrice priceToCancel)
        {
            this.Symbol = priceToCancel.Symbol;
            this.Counterparty = priceToCancel.Counterparty;
            this.Side = priceToCancel.Side;
            this.PriceProducerIndex = priceToCancel.PriceProducerIndex;
        }

        public Common.Symbol Symbol { get; private set; }
        public Counterparty Counterparty { get; private set; }
        public PriceSide? Side { get; private set; }
        public StreamingPricesProducerIndex? PriceProducerIndex { get; private set; }
        public DateTime IntegratorSentTimeUtc { get; private set; }

        public void SetSentPriceCancelInfo(DateTime integratorSentTimeUtc)
        {
            this.IntegratorSentTimeUtc = integratorSentTimeUtc;
        }
    }
}
