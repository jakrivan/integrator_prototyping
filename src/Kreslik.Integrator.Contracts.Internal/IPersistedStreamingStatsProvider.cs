﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts.Internal
{
    public interface IPersistedStreamingStatsProvider
    {
        void GetStreamingSessionStats(Counterparty counterparty, out int dealsNum, out int rejectionsNum);
    }
}
