﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts.Internal
{
    public class IntegratorDealDone: IEquatable<IntegratorDealDone>
    {
        public IntegratorDealDone(Counterparty counterparty, Symbol symbol, DateTime settlementDateLocal, DateTime tradeDateUtc, DateTime settlementTimeCounterpartyUtc,
            decimal sizeBasePolarized, decimal sizeTermPolarized)
        {
            this.Counterparty = counterparty;
            this.Symbol = symbol;
            this.SettlementDateLocal = settlementDateLocal;
            this.TradeDateUtc = tradeDateUtc;
            this.SettlementTimeCounterpartyUtc = settlementTimeCounterpartyUtc;
            this.SizeBasePolarized = AtomicSize.FromDecimal(sizeBasePolarized);
            this.SizeTermPolarized = AtomicSize.FromDecimal(sizeTermPolarized);
        }

        public Counterparty Counterparty { get; private set; }
        public Symbol Symbol { get; private set; }
        public DateTime SettlementDateLocal { get; private set; }
        public DateTime TradeDateUtc { get; private set; }
        public DateTime SettlementTimeCounterpartyUtc { get; private set; }
        public AtomicSize SizeBasePolarized { get; private set; }
        public AtomicSize SizeTermPolarized { get; private set; }
        //TradeTimeUtc - not needed - all removed on rollover

        //for removing purposes
        public void Invert()
        {
            this.SizeBasePolarized = -this.SizeBasePolarized;
            this.SizeTermPolarized = -this.SizeTermPolarized;
        }

        public bool Equals(IntegratorDealDone other)
        {
            return
                this.Counterparty == other.Counterparty &&
                this.Symbol == other.Symbol &&
                this.SettlementDateLocal == other.SettlementDateLocal &&
                this.SizeBasePolarized == other.SizeBasePolarized &&
                this.SizeTermPolarized == other.SizeTermPolarized;
        }

        public override string ToString()
        {
            return string.Format("IntegratorDealDone: {0} {1} Base: {2} Term: {3} (ValueDate: {4})",
                this.Counterparty, this.Symbol, this.SizeBasePolarized, this.SizeTermPolarized, this.SettlementDateLocal);
        }
    }
}
