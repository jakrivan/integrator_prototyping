﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts.Internal
{
    public enum IntegratorProcessState
    {
        Inactive,
        Starting,
        Running,
        ShuttingDown,
        Unknown
    }

    public static class ProcessTypeGuids
    {
        private static Guid[] _guids = new Guid[]
            {
                new Guid("7C0B4B28-8121-434F-A225-4C87A8010BF3"),
                new Guid("F7F898C1-6B25-456D-95CE-331BA4874C03"),
                new Guid("3A97F2B0-7026-458C-9E50-FF731070E0C6")
            };

        public static Guid GetGuidForProcessType(IntegratorProcessType integratorProcessType)
        {
            return _guids[(int) integratorProcessType];
        }
    }

    public class IntegratorProcessInfo
    {
        public IntegratorProcessInfo(IntegratorProcessType integratorProcessType, int processIdentifier, string privateIPAddress, string publicIPAddress, string hostname, int? tradingAPIPort, int? diagnosticsAPIPort, IntegratorProcessState integratorProcessState)
        {
            this.IntegratorProcessType = integratorProcessType;
            this.ProcessIdentifier = processIdentifier;
            this.PrivateIPAddress = privateIPAddress;
            this.PublicIPAddress = publicIPAddress;
            this.Hostname = hostname;
            this.TradingAPIPort = tradingAPIPort;
            this.DiagnosticsAPIPort = diagnosticsAPIPort;
            this._integratorProcessState = integratorProcessState;
        }

        public IntegratorProcessType IntegratorProcessType { get; private set; }
        public int ProcessIdentifier { get; private set; }
        public string PrivateIPAddress { get; private set; }
        public string PublicIPAddress { get; private set; }
        public string Hostname { get; private set; }
        public int? TradingAPIPort { get; private set; }
        public int? DiagnosticsAPIPort { get; private set; }

        private IntegratorProcessState _integratorProcessState;

        public IntegratorProcessState IntegratorProcessState
        {
            get { return this._integratorProcessState; }
            set
            {
                if (value != this._integratorProcessState && VerifyStateTransition(value))
                {
                    this._integratorProcessState = value;
                    IntegratorProcessStateChanged.SafeInvoke(LogFactory.Instance.GetLogger(null), value);
                }
            }
        }

        public event Action<IntegratorProcessState> IntegratorProcessStateChanged;

        private bool VerifyStateTransition(IntegratorProcessState newState)
        {
            if (
                newState == IntegratorProcessState.Starting
                ||
                (newState == IntegratorProcessState.Running &&
                 _integratorProcessState != IntegratorProcessState.Starting)
                )
            {
                LogFactory.Instance.GetLogger(null)
                          .Log(LogLevel.Fatal,
                               "Attempting to perform disallowed transition from [{0}] to [{1}]. Ignoring",
                               _integratorProcessState, newState);
                return false;
            }

            return true;
        }
    }
}
