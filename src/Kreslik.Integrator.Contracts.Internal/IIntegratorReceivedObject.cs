﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts.Internal
{
    public interface IIntegratorReceivedObject
    {
        DateTime IntegratorReceivedTimeUtc { get; }
        DateTime CounterpartySentTimeUtc { get; }
    }
}
