﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts.Internal
{
    public interface ITrustworthyWatcherSettingsProvider
    {
        IList<ITrustworthyWatcherSettings> GetSettings();

        ITrustworthyWatcherSettings GetEmptySettings(Symbol symbol);
    }

    public interface ITrustworthyWatcherSettings
    {
        Symbol Symbol { get; }
        bool AreSettingsComplete { get; }
        decimal MaxAllowedSpreadBp { get; }
        decimal MinAllowedSpreadBp { get; }
        AtomicDecimal MaxAllowedSpread { get; }
        AtomicDecimal MinAllowedSpreadInverted { get; }
        TimeSpan MaxAllowedTickAge { get; }
    }
}
