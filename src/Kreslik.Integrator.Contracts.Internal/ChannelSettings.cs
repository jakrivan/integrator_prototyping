﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts.Internal
{
    public static class FluentRedirectionSettings
    {
        public static bool IsRedirectionAllowed { get { return true; } }
    }

    public class FIXChannel_CRSSettings
    {
        public string ClientId { get; set; }
        public string Account { get; set; }
    }

    public class FIXChannel_DBKSettings
    {
        public string Account { get; set; }
    }

    public class FIXChannel_UBSSettings
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string PartyId { get; set; }
    }

    public class FIXChannel_CTISettings
    {
        public string Password { get; set; }
        public string Account { get; set; }
    }

    public class FIXChannel_MGSSettings
    {
        public string OnBehalfOfCompId { get; set; }
        public string Account { get; set; }
    }

    public class FIXChannel_RBSSettings
    {
        public string Account { get; set; }
    }

    public class FIXChannel_JPMSettings
    {
        public string Password { get; set; }
        public int PasswordLength { get; set; }
        public string Account { get; set; }
    }

    public class FIXChannel_BNPSettings
    {
        public string Password { get; set; }
        public string Account { get; set; }
    }

    public class FIXChannel_CZBSettings
    {
        public string Account { get; set; }
    }

    public class FIXChannel_SOCSettings
    {
        public string OnBehalfOfCompID { get; set; }
    }

    public class FIXChannel_NOMSettings
    {
        public string OnBehalfOfCompID { get; set; }
        public string SenderSubId { get; set; }
        public string Account { get; set; }
    }

    public class FIXChannel_HOTSettings
    {
        public string SenderSubId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool DealConfirmationSupported { get; set; }
        public int MaxOrdersPerSecond { get; set; }
    }

    public class FIXChannel_FALSettings
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Account { get; set; }
    }

    public class FIXChannel_LMXSettings
    {
        public int L01LayersNum { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class FIXSTPChannel_CTIPBSettings
    {
        public string Password { get; set; }
        public string Account { get; set; }
    }

    public class FIXChannel_FCMSettings
    {
        public bool IocApplyTtl { get; set; }
        public int IocTtlMs { get; set; }
        public bool ApplyMaxHoldTime { get; set; }
        public FXCMMaxHoldTime MaxHoldTime { get; set; }
    }

    public class FIXChannel_PXMSettings
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int IocTtlMs { get; set; }
    }

    public enum FXCMMaxHoldTime
    {
        UpTo1ms = 1,
        UpTo30ms = 2,
        UpTo100ms = 3,
        UpTo500ms = 4,
        UpTo3000ms = 5
    }


    public class RejectionRateDisconnectSettings
    {
        public bool RiskCheckAllowed { get; set; }

        public int MinimumOrdersToApplyCheck { get; set; }

        public decimal MaxAllowedRejectionRatePercent { get; set; }
    }

    public interface ISymbolRateSetting
    {
        Symbol Symbol { get; }
        int MaxAllowedPerTimeInterval { get; }
        TimeSpan MaxAllowedIntervalToCheck { get; }
    }

    public interface IMaxPerSymbolPricesPerSecondSetting
    {
        int DefaultMaxAllowedPerTimeInterval { get; }
        TimeSpan DefaultMaxAllowedIntervalToCheck { get; }
        IEnumerable<ISymbolRateSetting> SpecialSymbolRateSettings { get; }
    }

    public class MaxPerSymbolPricesPerSecondSetting : IMaxPerSymbolPricesPerSecondSetting
    {
        public int DefaultMaxAllowedPerTimeInterval { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public TimeSpan DefaultMaxAllowedIntervalToCheck { get; set; }

        [System.Xml.Serialization.XmlAttribute("DefaultMaxAllowedIntervalToCheck_Milliseconds")]
        public int DefaultMaxAllowedIntervalToCheckXml
        {
            get { return (int)DefaultMaxAllowedIntervalToCheck.TotalMilliseconds; }
            set { DefaultMaxAllowedIntervalToCheck = System.TimeSpan.FromMilliseconds(value); }
        }

        [XmlArray(ElementName = "SpecialSymbolRateSettings")]
        [XmlArrayItem(ElementName = "Symbol")]
        public List<SymbolRateSetting> SpecialSymbolRateSettingsXml { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public IEnumerable<ISymbolRateSetting> SpecialSymbolRateSettings { get { return this.SpecialSymbolRateSettingsXml; } }

        public class SymbolRateSetting : ISymbolRateSetting
        {
            [System.Xml.Serialization.XmlAttribute("Name")]
            public string SymbolCodeXml { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public Symbol Symbol { get { return (Symbol) this.SymbolCodeXml; } }

            [System.Xml.Serialization.XmlAttribute]
            public int MaxAllowedPerTimeInterval { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public TimeSpan MaxAllowedIntervalToCheck { get; set; }

            [System.Xml.Serialization.XmlAttribute("MaxAllowedIntervalToCheck_Milliseconds")]
            public int MaxAllowedIntervalToCheckXml
            {
                get { return (int)MaxAllowedIntervalToCheck.TotalMilliseconds; }
                set { MaxAllowedIntervalToCheck = System.TimeSpan.FromMilliseconds(value); }
            }
        }
    }

    public interface IStreamingChannelSettings
    {
        int MaxTotalPricesPerTimeInterval { get; }

        TimeSpan MaxTotalPricesIntervalToCheck { get; }

        IMaxPerSymbolPricesPerSecondSetting MaxPerSymbolPricesPerSecondSetting { get; }

        int ExpectedPricingDepth { get; }

        RejectionRateDisconnectSettings RejectionRateDisconnectSettings { get; }
    }

    public class StreamingChannelSettings : IStreamingChannelSettings
    {
        public int MaxTotalPricesPerTimeInterval { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public TimeSpan MaxTotalPricesIntervalToCheck { get; set; }

        [System.Xml.Serialization.XmlAttribute("MaxTotalPricesIntervalToCheck_Milliseconds")]
        public int MaxTotalPricesIntervalToCheckXml
        {
            get { return (int)MaxTotalPricesIntervalToCheck.TotalMilliseconds; }
            set { MaxTotalPricesIntervalToCheck = System.TimeSpan.FromMilliseconds(value); }
        }


        [System.Xml.Serialization.XmlElement("MaxPerSymbolPricesPerSecondSetting")]
        public MaxPerSymbolPricesPerSecondSetting MaxPerSymbolPricesPerSecondSettingXml { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public IMaxPerSymbolPricesPerSecondSetting MaxPerSymbolPricesPerSecondSetting
        {
            get { return this.MaxPerSymbolPricesPerSecondSettingXml; }
        }

        public int ExpectedPricingDepth { get; set; }

        public RejectionRateDisconnectSettings RejectionRateDisconnectSettings { get; set; }
    }

    public class UnexpectedMessagesTreatingSettings
    {
        public bool EnableThreasholdingOfUnexpectedMessages { get; set; }
        public int MaximumAllowedInstancesPerTimeInterval { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public System.TimeSpan TimeIntervalToCheck { get; set; }

        [System.Xml.Serialization.XmlElement("TimeIntervalToCheck_Minutes")]
        public int TimeIntervalToCheckXml
        {
            get { return (int)TimeIntervalToCheck.TotalMinutes; }
            set { TimeIntervalToCheck = System.TimeSpan.FromMinutes(value); }
        }
    }


    public class OrderFlowSessionSettings
    {
        [System.Xml.Serialization.XmlIgnore]
        public System.TimeSpan UnconfirmedOrderTimeout { get; set; }

        [System.Xml.Serialization.XmlElement("UnconfirmedOrderTimeout_Seconds")]
        public int UnconfirmedOrderTimeoutXml
        {
            get { return (int)UnconfirmedOrderTimeout.TotalSeconds; }
            set { UnconfirmedOrderTimeout = System.TimeSpan.FromSeconds(value); }
        }

        public bool PutErrorsInLessImportantLog { get; set; }

        public ExternalOrdersRejectionRateSettings ExternalOrdersRejectionRate { get; set; }

        public class ExternalOrdersRejectionRateSettings
        {
            public bool CheckAllowed { get; set; }

            public int MaximumAllowedInstancesPerTimeInterval { get; set; }

            public int MaximumAllowedNongenuineInstancesPerTimeInterval { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public TimeSpan TimeIntervalToCheck { get; set; }

            [System.Xml.Serialization.XmlElement("TimeIntervalToCheck_Seconds")]
            public int TimeIntervalToCheckXml
            {
                get { return (int)TimeIntervalToCheck.TotalSeconds; }
                set { TimeIntervalToCheck = System.TimeSpan.FromSeconds(value); }
            }

            [XmlArray(ElementName = "SpecialRejectionsHandling")]
            [XmlArrayItem(ElementName = "SpecialRejection")]
            public List<SpecialRejectionRateSettings> SpecialRejectionsHandling { get; set; }
        }

        public class SpecialRejectionRateSettings
        {
            public string TriggerPhrase { get; set; }

            public int MaximumAllowedInstancesPerTimeInterval { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public TimeSpan TimeIntervalToCheck { get; set; }

            [System.Xml.Serialization.XmlElement("TimeIntervalToCheck_Seconds")]
            public int TimeIntervalToCheckXml
            {
                get { return (int)TimeIntervalToCheck.TotalSeconds; }
                set { TimeIntervalToCheck = System.TimeSpan.FromSeconds(value); }
            }
        }
    }

    public class MarketDataSessionSettings
    {
        public int MaxAutoResubscribeCount { get; set; }
        [System.Xml.Serialization.XmlIgnore]
        public System.TimeSpan[] RetryIntervalsSequence { get; set; }

        [System.Xml.Serialization.XmlElement("RetryIntervalsSequence_Seconds")]
        public int[] RetryIntervalsSequenceXml
        {
            get { return RetryIntervalsSequence.Select(span => (int)span.TotalSeconds).ToArray(); }
            set
            {
                RetryIntervalsSequence =
                    value.Select(secondsCount => System.TimeSpan.FromSeconds(secondsCount)).ToArray();
            }
        }

        [System.Xml.Serialization.XmlIgnore]
        public System.TimeSpan UnconfirmedSubscriptionTimeout { get; set; }

        [System.Xml.Serialization.XmlElement("UnconfirmedSubscriptionTimeout_Seconds")]
        public int UnconfirmedSubscriptionTimeoutXml
        {
            get { return (int)UnconfirmedSubscriptionTimeout.TotalSeconds; }
            set { UnconfirmedSubscriptionTimeout = System.TimeSpan.FromSeconds(value); }
        }

        public bool PutErrorsInLessImportantLog { get; set; }
    }

    public class SubscriptionSettingsBag
    {
        public bool ResubscribeOnReconnect { get; set; }
        public SubscriptionSettingsBag.GlobalSubscription GlobalSubscriptionSettings { get; set; }
        public SubscriptionSettingsBag.CounterpartsSubscription CounterpartsSubscriptionSettings { get; set; }

        public enum SettingAction
        {
            Add,
            Remove
        }

        public class GlobalSubscription
        {
            public System.Collections.Generic.List<SubscriptionSettingsBag.SymbolSetting> Symbols { get; set; }
        }

        public class CounterpartsSubscription
        {
            public System.Collections.Generic.List<SubscriptionSettingsBag.CounterpartSetting> Counterparts { get; set; }
        }

        public class SymbolSetting
        {
            public string ShortName { get; set; }
            public decimal Size { get; set; }
            public SubscriptionSettingsBag.SettingAction SettingAction { get; set; }
            public string SenderSubId { get; set; }
        }

        public class CounterpartSetting
        {
            public string ShortName { get; set; }
            public SubscriptionSettingsBag.SettingAction SettingAction { get; set; }
            public System.Collections.Generic.List<SubscriptionSettingsBag.SymbolSetting> Symbols { get; set; }
            public string SenderSubId { get; set; }
            public bool MarketSessionIntoSeparateProcess { get; set; }
        }
    }

    //Not to be serialized at all
    public class SubscriptionSettingsDictionary
    {
        public List<Tuple<Symbol, decimal, string>> this[Counterparty counterparty]
        {
            get
            {
                if (EnabledCounterparts.Contains(counterparty))
                {
                    if (_counterpartSymbolSettings.ContainsKey(counterparty))
                    {
                        return _counterpartSymbolSettings[counterparty];
                    }
                    else
                    {
                        return _globalSymbolsSetting;
                    }
                }
                else
                {
                    return new List<Tuple<Symbol, decimal, string>>();
                }
            }
        }

        public List<Counterparty> EnabledCounterparts { get; private set; }

        public List<Counterparty> EnabledCounterpartsWithMDSessionInSeparateProcess { get; private set; }

        public bool ResubscribeOnReconnect { get; private set; }

        private List<Tuple<Symbol, decimal, string>> _globalSymbolsSetting;
        private Dictionary<Counterparty, List<Tuple<Symbol, decimal, string>>> _counterpartSymbolSettings;

        public SubscriptionSettingsDictionary(SubscriptionSettingsBag subscriptionSettingsBag)
        {
            this.ResubscribeOnReconnect = subscriptionSettingsBag.ResubscribeOnReconnect;

            _globalSymbolsSetting = new List<Tuple<Symbol, decimal, string>>();

            _globalSymbolsSetting = ProcessSymbolsSettings(subscriptionSettingsBag.GlobalSubscriptionSettings.Symbols, _globalSymbolsSetting, null);

            EnabledCounterparts = new List<Counterparty>();
            EnabledCounterpartsWithMDSessionInSeparateProcess = new List<Counterparty>();
            _counterpartSymbolSettings = new Dictionary<Counterparty, List<Tuple<Symbol, decimal, string>>>();

            foreach (
                SubscriptionSettingsBag.CounterpartSetting counterpartSetting in
                    subscriptionSettingsBag.CounterpartsSubscriptionSettings.Counterparts)
            {
                if (counterpartSetting.ShortName.Equals("ALL", StringComparison.OrdinalIgnoreCase))
                {
                    EnabledCounterparts.Clear();
                    EnabledCounterpartsWithMDSessionInSeparateProcess.Clear();

                    if (counterpartSetting.SettingAction ==
                        SubscriptionSettingsBag.SettingAction.Add)
                    {
                        EnabledCounterparts.AddRange(Counterparty.Values);
                        if (counterpartSetting.MarketSessionIntoSeparateProcess)
                            EnabledCounterpartsWithMDSessionInSeparateProcess.AddRange(Counterparty.Values);

                        if (counterpartSetting.Symbols != null)
                        {
                            _globalSymbolsSetting = ProcessSymbolsSettings(counterpartSetting.Symbols,
                                                                           _globalSymbolsSetting, counterpartSetting.SenderSubId);
                        }
                    }
                }
                else if (Counterparty.StringValues.Any(sv => sv.Equals(counterpartSetting.ShortName)))
                {
                    EnabledCounterparts.Remove((Counterparty)counterpartSetting.ShortName);
                    EnabledCounterpartsWithMDSessionInSeparateProcess.Remove((Counterparty)counterpartSetting.ShortName);

                    if (counterpartSetting.SettingAction ==
                        SubscriptionSettingsBag.SettingAction.Add)
                    {
                        EnabledCounterparts.Add((Counterparty)counterpartSetting.ShortName);
                        if (counterpartSetting.MarketSessionIntoSeparateProcess)
                            EnabledCounterpartsWithMDSessionInSeparateProcess.Add((Counterparty)counterpartSetting.ShortName);

                        if (counterpartSetting.Symbols != null)
                        {
                            List<Tuple<Symbol, decimal, string>> symbolsLocal;

                            if (string.IsNullOrEmpty(counterpartSetting.SenderSubId))
                            {
                                symbolsLocal = new List<Tuple<Symbol, decimal, string>>(_globalSymbolsSetting);
                            }
                            else
                            {
                                string senderSubIdLocal = counterpartSetting.SenderSubId;
                                symbolsLocal =
                                    new List<Tuple<Symbol, decimal, string>>(
                                        _globalSymbolsSetting.Select(
                                            tuple =>
                                            new Tuple<Symbol, decimal, string>(tuple.Item1, tuple.Item2,
                                                                               senderSubIdLocal)));
                            }

                            symbolsLocal = ProcessSymbolsSettings(counterpartSetting.Symbols, symbolsLocal, counterpartSetting.SenderSubId);
                            _counterpartSymbolSettings[(Counterparty)counterpartSetting.ShortName] = symbolsLocal;
                        }
                    }
                }
                else
                {
                    throw new Exception("Unknown counterpart string " + counterpartSetting.ShortName);
                }
            }
        }

        private static List<Tuple<Symbol, decimal, string>> ProcessSymbolsSettings(
        List<SubscriptionSettingsBag.SymbolSetting> symbolSettingsList, List<Tuple<Symbol, decimal, string>> symbols, string defaultSenderSubId)
        {
            foreach (SubscriptionSettingsBag.SymbolSetting symbolSettings in symbolSettingsList)
            {
                if (symbolSettings.ShortName.Equals("ALL", StringComparison.OrdinalIgnoreCase))
                {
                    symbols.RemoveAll(v => Symbol.Values.Contains(v.Item1));

                    if (symbolSettings.SettingAction == SubscriptionSettingsBag.SettingAction.Add)
                    {
                        symbols.AddRange(Symbol.Values.Select(s => new Tuple<Symbol, decimal, string>(s, symbolSettings.Size, symbolSettings.SenderSubId ?? defaultSenderSubId)));
                    }
                }
                else if (Symbol.Values.Any(s => s.ShortName.Equals(symbolSettings.ShortName)))
                {
                    symbols.RemoveAll(s => s.Item1 == ((Symbol)symbolSettings.ShortName));

                    if (symbolSettings.SettingAction == SubscriptionSettingsBag.SettingAction.Add)
                    {
                        symbols.Add(new Tuple<Symbol, decimal, string>((Symbol)symbolSettings.ShortName, symbolSettings.Size, symbolSettings.SenderSubId ?? defaultSenderSubId));
                    }
                }
                else
                {
                    throw new Exception("Unknown symbol string " + symbolSettings.ShortName);
                }
            }

            return symbols;
        }
    }
}