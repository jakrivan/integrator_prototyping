﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts.Internal
{
    public interface IActivityBandStats
    {
        decimal TotalVolumUsd { get; }
        decimal AvgDealSizeUsd { get; }
        decimal PaidVolumeRatio { get; }
        bool HadEnoughData { get; }
        bool Equals(IActivityBandStats activityBandStats);
    }

    public interface IQuotingStatsPersistor
    {
        IQuotingStatsPersistorProxy CreateQuotingStatsPersistorProxy(int tradingSystemId);
    }

    public interface IQuotingStatsPersistorProxy
    {
        void UpdateLTStats(IActivityBandStats activityBandStats, bool ltBandRestrictionOn);
        void UpdateInactivityRestrictionState(bool inactivityRestrictionOn);
        void UpdateLtBandRestrictionState(bool ltBandRestrictionOn);
        void UpdateCurrentSpread(decimal currentSpread);
        void ClearCurrentSpread();
        void SetShouldUploadStats(bool shouldUpload);
    }
}
