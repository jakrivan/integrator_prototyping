﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts.Internal
{
    public interface IIntegratorInstanceProcessUtils
    {
        //bool IsKnownPrivateIp(System.Net.IPAddress privateIpAddress);
        bool IsKnownPrivateIp(string privateIpAddress);
        List<IntegratorProcessInfo> GetIntegratorProcessDetails();

        List<IntegratorProcessInfo> TryRegisterIntegratorProcess(string privateIPAddress, out int? integratorProcessIdentifier);

        void UpdateIntegratorProcessState(IntegratorProcessInfo integratorProcessInfo, IntegratorProcessState newState);
        void HeartBeat(IntegratorProcessInfo integratorProcessInfo);
    }
}
