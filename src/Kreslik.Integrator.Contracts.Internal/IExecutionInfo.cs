﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts.Internal
{
    public interface IExecutionInfo
    {
        bool IsExecutionInfoPopulated { get; }
        decimal FilledAmountBaseAbs { get; }
        decimal UsedPrice { get; }
        string IntegratorTransactionId { get; }
        string ExecutionId { get; }
        DateTime SettlementDateLocal { get; }
        DateTime TransactionTime { get; }
    }
}
