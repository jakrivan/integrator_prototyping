﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts.Internal
{
    public interface IClientSystemOrderAllocations
    {
        int? SingleAllocationClientSystemId { get; }
        List<int> ClientSystemAllocationsIds { get; }
        List<decimal> ClientSystemAllocationAmountsBaseAbs { get; }
    }

    internal class WrittableClientSystemOrderAllocations : IClientSystemOrderAllocations
    {
        public int? SingleAllocationClientSystemId { get; private set; }
        private List<int> _clientSystemAllocationIds;
        private List<decimal> _clientSystemAllocationAmountsBaseAbs;


        public void AddClientSystemAllocation(IntegratedTradingSystemIdentification clientSystemId, decimal amountAllocatedBaseAbs)
        {
            if (_clientSystemAllocationIds == null)
                _clientSystemAllocationIds = new List<int>();
            _clientSystemAllocationIds.Add(GetIntegratedTradingSystemId(clientSystemId));

            if (_clientSystemAllocationAmountsBaseAbs == null)
                _clientSystemAllocationAmountsBaseAbs = new List<decimal>();
            _clientSystemAllocationAmountsBaseAbs.Add(amountAllocatedBaseAbs);
        }

        public void SetSingleFillId(IntegratedTradingSystemIdentification clientSystemId)
        {
            this.SingleAllocationClientSystemId = GetIntegratedTradingSystemId(clientSystemId);
        }

        private int GetIntegratedTradingSystemId(IntegratedTradingSystemIdentification clientSystemId)
        {
            return clientSystemId == null ? -1 : clientSystemId.IntegratedTradingSystemIdentity;
        }

        public List<int> ClientSystemAllocationsIds
        {
            get { return this._clientSystemAllocationIds; }
        }
        public List<decimal> ClientSystemAllocationAmountsBaseAbs
        {
            get { return this._clientSystemAllocationAmountsBaseAbs; }
        }
    }

    public abstract class AllocationInfo
    {
        private WrittableClientSystemOrderAllocations _writtableClientSystemAllocations;
        public IClientSystemOrderAllocations ClientSystemAllocations { get { return this._writtableClientSystemAllocations; } }
        public string CounterpartyClientId { get; set; }

        protected abstract decimal? TotalAllocationAmount { get; }

        public void AddClientSystemAllocation(IntegratedTradingSystemIdentification clientSystemId, decimal amountAllocatedBaseAbs)
        {
            if (_writtableClientSystemAllocations == null)
                _writtableClientSystemAllocations = new WrittableClientSystemOrderAllocations();
            else if (_writtableClientSystemAllocations.SingleAllocationClientSystemId != null)
                throw new Exception("Attempt to add multiple single client allocations for transaction " + this.ToString());

            if (amountAllocatedBaseAbs == this.TotalAllocationAmount)
            {
                _writtableClientSystemAllocations.SetSingleFillId(clientSystemId);
            }
            else
            {
                _writtableClientSystemAllocations.AddClientSystemAllocation(clientSystemId, amountAllocatedBaseAbs);
            }
        }
    }

    public class ExecutionInfo : AllocationInfo
    {
        public ExecutionInfo(decimal? filledAmount, decimal? usedPrice, string counterpartyTransactionId,
                             DateTime? counterpartyTransactionTime, DateTime? settlementDate, DealDirection integratorDealDirection)
        {
            this.FilledAmount = filledAmount;
            this.UsedPrice = usedPrice;
            this.CounterpartyTransactionId = counterpartyTransactionId;
            this.CounterpartyTransactionTime = counterpartyTransactionTime;
            this.SettlementDate = settlementDate;
            this.IntegratorDealDirection = integratorDealDirection;
        }

        public void UpdateIntegratorTransactionId(string transactionId)
        {
            this.IntegratorTransactionId = transactionId;
        }

        public void ResetCounterpartyTransactionId()
        {
            this.CounterpartyTransactionId = string.Empty;
        }

        public void ResetSettlementDate()
        {
            this.SettlementDate = SqlDateTime.MinValue.Value;
        }

        public void SetUtcSettlementTime(Symbol symbol, Counterparty counterparty)
        {
            this.SettlementTimeCounterpartyUtc = TradingHoursHelper.Instance.GetCounterpartyRolloverTimeInUtc(
                this.SettlementDate.Value, symbol, counterparty);
            this.SettlementTimeCentralBankUtc = TradingHoursHelper.Instance.GetCentralBankRolloverTimeInUtc(
                this.SettlementDate.Value, symbol);
        }

        public void SetFilledAmount(decimal filledAmount)
        {
            this.FilledAmount = filledAmount;
        }

        public void SetUsedPrice(decimal usedPrice)
        {
            this.UsedPrice = usedPrice;
        }

        public decimal? FilledAmount { get; private set; }

        public decimal FilledAmountBasePolarized
        {
            get
            {
                return IntegratorDealDirection == DealDirection.Buy
                    ? FilledAmount.Value
                    : -FilledAmount.Value;
            }
        }

        public decimal FilledAmountTermPolarized
        {
            get { return -FilledAmountBasePolarized*UsedPrice.Value; }
        }

        public decimal? UsedPrice { get; private set; }
        public string CounterpartyTransactionId { get; private set; }
        public string IntegratorTransactionId { get; private set; }
        public DealDirection IntegratorDealDirection { get; private set; }
        public DateTime? CounterpartyTransactionTime { get; private set; }
        public DateTime? SettlementDate { get; private set; }
        public DateTime SettlementTimeCounterpartyUtc { get; private set; }
        public DateTime SettlementTimeCentralBankUtc { get; private set; }
        public FlowSide? FlowSide { get; set; }

        public override string ToString()
        {
            return string.Format("ExecutionInfo: Filled: {0}, Price: {1}, CounterpartyTransactionId: {2}",
                                 FilledAmount, UsedPrice, CounterpartyTransactionId);
        }

        protected override decimal? TotalAllocationAmount
        {
            get { return this.FilledAmount; }
        }
    }

    public enum RejectionType
    {
        OrderReject,
        OrderCancel,
        CancelReplaceReject,
        InternalRejectTriggeredByReplace,
        DealRejectByIntegrator,
        DealRejectByCounterpartyForced
    }

    public enum RejectionDirection
    {
        CtpRejectd = 0,
        KGTRejectd = 1
    }

    public class RejectionInfo : AllocationInfo
    {
        public RejectionInfo(string rejectionReason, decimal? rejectedAmount, RejectionType rejectionType)
        {
            this.RejectionReason = rejectionReason;
            this.RejectedAmount = rejectedAmount;
            this.RejectionType = rejectionType;
        }

        public string RejectionReason { get; set; }
        public decimal? RejectedAmount { get; set; }
        public RejectionType RejectionType { get; private set; }

        public RejectionDirection RejectionDirection
        {
            get
            {
                return this.RejectionType == RejectionType.DealRejectByIntegrator
                    ? RejectionDirection.KGTRejectd
                    : RejectionDirection.CtpRejectd;
            }
        }

        public void SetRejectedAmount(decimal rejectedAmount)
        {
            if (!this.RejectedAmount.HasValue)
            {
                this.RejectedAmount = rejectedAmount;
            }
        }

        public void ForceSetRejectedAmount(decimal rejectedAmount)
        {
            this.RejectedAmount = rejectedAmount;
        }

        //CancelReplaceReject should be likely also counted as genuine reject,
        // despite we didn't want to count it as genuine reject not to repeat Hotspot high message rate scenario
        public bool IsGenuineReject
        {
            get
            {
                return this.RejectionType == RejectionType.OrderReject;
            }
        }

        public bool IsLLContractReject
        {
            get
            {
                return this.RejectionType == RejectionType.DealRejectByIntegrator ||
                       RejectionType == RejectionType.DealRejectByCounterpartyForced;
            }
        }

        public override string ToString()
        {
            return string.Format("RejectionInfo: Rejected: {0}, RejectionType: {1}, Reason: {2}",
                                 RejectedAmount, RejectionType, RejectionReason);
        }

        protected override decimal? TotalAllocationAmount
        {
            get { return this.RejectedAmount; }
        }
    }

    public class OrderChangeEventArgs
    {
        public OrderChangeEventArgs(IntegratorOrderExternalChange changeState)
        {
            this.ChangeState = changeState;
            this.ExecutionInfo = null;
            this.RejectionInfo = null;
        }

        public OrderChangeEventArgs(IntegratorOrderExternalChange changeState, RejectionInfo rejectionInfo)
        {
            this.ChangeState = changeState;
            this.ExecutionInfo = null;
            this.RejectionInfo = rejectionInfo;
        }

        public OrderChangeEventArgs(IntegratorOrderExternalChange changeState, ExecutionInfo executionInfo, RejectionInfo rejectionInfo)
        {
            this.ChangeState = changeState;
            this.ExecutionInfo = executionInfo;
            this.RejectionInfo = rejectionInfo;
        }

        public OrderChangeEventArgs(IntegratorOrderExternalChange changeState, ExecutionInfo executionInfo, RejectionInfo rejectionInfo, string counterpartyReason)
        {
            this.ChangeState = changeState;
            this.ExecutionInfo = executionInfo;
            this.RejectionInfo = rejectionInfo;
            this.CounterpartyReason = counterpartyReason;
        }

        public IntegratorOrderExternalChange ChangeState { get; private set; }
        public ExecutionInfo ExecutionInfo { get; set; }
        public RejectionInfo RejectionInfo { get; set; }
        public string CounterpartyReason { get; private set; }

        //might be needed in future for correct passing of CounterpartyTime etc. to clients 
        // (now, it's through order - and for multiple partiall fills info can get overriden in the meantime)
        //public OrderChangeInfo OrderChangeInfo { get; private set; }

        public override string ToString()
        {
            return string.Format("OrderChangeEventArgs: State - {0}, {1}, {2}", ChangeState,
                                 ExecutionInfo, RejectionInfo);
        }
    }

    public class OrderChangeInfo : IExecutionInfo, IIntegratorReceivedObject
    {
        public OrderChangeInfo(string identity, Symbol symbol, IntegratorOrderExternalChange orderStatus,
                               DateTime? counterpartySentTime, IPooledBufferSegment rawMessageHandle,
                               ExecutionInfo executionInfo, RejectionInfo rejectionInfo, DateTime integratorReceivedUtc)
            : this(identity, symbol, null, orderStatus, counterpartySentTime, rawMessageHandle,
                               executionInfo, rejectionInfo, (decimal?) null, integratorReceivedUtc)
        { }

        //TODO: to be discontnued
        public OrderChangeInfo(string identity, Symbol symbol, IntegratorOrderExternalChange orderStatus,
                               DateTime? counterpartySentTime, IPooledBufferSegment rawMessageHandle,
                               ExecutionInfo executionInfo, RejectionInfo rejectionInfo)
            : this(identity, symbol, orderStatus, counterpartySentTime, rawMessageHandle,
                               executionInfo, rejectionInfo, (decimal?)null)
        { }

        //TODO: to be discontnued
        public OrderChangeInfo(string identity, Symbol symbol, IntegratorOrderExternalChange orderStatus,
                               DateTime? counterpartySentTime, IPooledBufferSegment rawMessageHandle,
                               ExecutionInfo executionInfo, RejectionInfo rejectionInfo, decimal? initaialConfirmedAmount)
        {
            this.IntegratorReceivedTimeUtc = HighResolutionDateTime.UtcNow;
            this.Identity = identity;
            this.Symbol = symbol;
            this.CounterpartySentTime = counterpartySentTime;
            this.DangerousMessageHandle = rawMessageHandle;
            this.InitaialConfirmedAmount = initaialConfirmedAmount;
            this.ChangeEventArgs = new OrderChangeEventArgs(orderStatus, executionInfo, rejectionInfo);
        }

        public OrderChangeInfo(string identity, Symbol symbol, string remotePlatformOrderId, IntegratorOrderExternalChange orderStatus,
                               DateTime? counterpartySentTime, IPooledBufferSegment rawMessageHandle,
                               ExecutionInfo executionInfo, RejectionInfo rejectionInfo, decimal? initaialConfirmedAmount, DateTime integratorIntegratorReceivedUtc)
        {
            this.IntegratorReceivedTimeUtc = integratorIntegratorReceivedUtc;
            this.Identity = identity;
            this.Symbol = symbol;
            this.RemotePlatformOrderId = remotePlatformOrderId;
            this.CounterpartySentTime = counterpartySentTime;
            this.DangerousMessageHandle = rawMessageHandle;
            this.InitaialConfirmedAmount = initaialConfirmedAmount;
            this.ChangeEventArgs = new OrderChangeEventArgs(orderStatus, executionInfo, rejectionInfo);
        }

        public OrderChangeInfo(string identity, Symbol symbol, IntegratorOrderExternalChange orderStatus,
                               DateTime? counterpartySentTime, IPooledBufferSegment rawMessageHandle,
                               ExecutionInfo executionInfo, RejectionInfo rejectionInfo, string counterpartyReason)
        {
            this.IntegratorReceivedTimeUtc = HighResolutionDateTime.UtcNow;
            this.Identity = identity;
            this.Symbol = symbol;
            this.CounterpartySentTime = counterpartySentTime;
            this.DangerousMessageHandle = rawMessageHandle;
            this.InitaialConfirmedAmount = null;
            this.ChangeEventArgs = new OrderChangeEventArgs(orderStatus, executionInfo, rejectionInfo, counterpartyReason);
        }

        public string Identity { get; private set; }
        public string UniqueInternalIdentity { get; set; }
        public string RemotePlatformOrderId { get; private set; }
        public DateTime IntegratorReceivedTimeUtc { get; private set; }
        public DateTime? CounterpartySentTime { get; private set; }
        public DateTime CounterpartySentTimeUtc { get { return CounterpartySentTime.Value; } }
        public Symbol Symbol { get; private set; }
        public IPooledBufferSegment DangerousMessageHandle { get; private set; }

        public decimal? InitaialConfirmedAmount { get; private set; }
        public OrderChangeEventArgs ChangeEventArgs { get; protected set; }

        public void Validate(ILogger logger, Counterparty counterparty)
        {
            if (!CounterpartySentTime.HasValue)
            {
                logger.Log(LogLevel.Error, "CounterpartySentTime not specified in OrderChangeInfo [{0}]", this);
                CounterpartySentTime = DateTime.MinValue;
            }

            if (this.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.Rejected ||
                this.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.Cancelled)
            {
                if (this.ChangeEventArgs.RejectionInfo == null)
                {
                    logger.Log(LogLevel.Error, "OrderChangeInfo [{0}] is in Rejected state, but RejectionInfo was not specified", this);

                    this.ChangeEventArgs.RejectionInfo = new RejectionInfo(null, null, RejectionType.OrderReject);
                }


                if (string.IsNullOrEmpty(this.ChangeEventArgs.RejectionInfo.RejectionReason))
                {
                    //error out only for rejections (requested cancelles or cancel/replaces usually do not have reason)
                    if (this.ChangeEventArgs.ChangeState != IntegratorOrderExternalChange.Cancelled)
                        logger.Log(LogLevel.Error, "OrderChangeInfo [{0}] is in Rejected state, but reason was not specified", this);
                    this.ChangeEventArgs.RejectionInfo.RejectionReason = string.Empty;
                }
            }

            if (this.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.Filled ||
                this.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.PartiallyFilled ||
                this.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.NonConfirmedDeal /*||
                (this.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.Rejected && this.ChangeEventArgs.RejectionInfo.IsLLContractReject)*/
                )
            {
                if (this.ChangeEventArgs.ExecutionInfo == null)
                {
                    logger.Log(LogLevel.Error,
                               "OrderChangeInfo [{0}] is in (partially)Filled state, but ExecutionInfo was not specified",
                               this);
                    this.ChangeEventArgs.ExecutionInfo = new ExecutionInfo(null, null, null, null, null, DealDirection.Buy);
                }
            }

            //this can be populated also for rejects ()ECN reject case
            if (this.ChangeEventArgs.ExecutionInfo != null)
            {
                if (string.IsNullOrWhiteSpace(this.ChangeEventArgs.ExecutionInfo.CounterpartyTransactionId))
                {
                    logger.Log(LogLevel.Error,
                               "OrderChangeInfo [{0}] has an ExecutionInfo, but transactionId was not specified", this);
                    this.ChangeEventArgs.ExecutionInfo.ResetCounterpartyTransactionId();
                }

                if (!this.ChangeEventArgs.ExecutionInfo.SettlementDate.HasValue)
                {
                    logger.Log(LogLevel.Error,
                               "OrderChangeInfo [{0}] has an ExecutionInfo, but settlement date was not specied", this);
                    this.ChangeEventArgs.ExecutionInfo.ResetSettlementDate();
                }

                this.ChangeEventArgs.ExecutionInfo.SetUtcSettlementTime(this.Symbol, counterparty);
            }
        }

        public override string ToString()
        {
            return string.Format("OrderChangeInfo [{0}]: {1}", Identity, this.ChangeEventArgs);
        }

        #region IExecutionInfo

        public bool IsExecutionInfoPopulated
        {
            get { return this.ChangeEventArgs.ExecutionInfo != null; }
        }

        public decimal FilledAmountBaseAbs
        {
            get { return this.ChangeEventArgs.ExecutionInfo.FilledAmount.Value; }
        }

        public decimal UsedPrice
        {
            get { return this.ChangeEventArgs.ExecutionInfo.UsedPrice.Value; }
        }

        public string IntegratorTransactionId
        {
            get { return ChangeEventArgs.ExecutionInfo.IntegratorTransactionId; }
        }

        public string ExecutionId
        {
            get { return ChangeEventArgs.ExecutionInfo.CounterpartyTransactionId; }
        }

        public DateTime SettlementDateLocal
        {
            get
            {
                return this.ChangeEventArgs.ExecutionInfo.SettlementDate.HasValue
                    ? this.ChangeEventArgs.ExecutionInfo.SettlementDate.Value
                    : DateTime.UtcNow.AddDays(1);
            }
        }

        public DateTime TransactionTime
        {
            get
            {
                return this.ChangeEventArgs.ExecutionInfo.CounterpartyTransactionTime.HasValue
                    ? this.ChangeEventArgs.ExecutionInfo.CounterpartyTransactionTime.Value
                    : this.IntegratorReceivedTimeUtc;
            }
        }

        #endregion IExecutionInfo
    }
}
