﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts.Internal
{
    public interface IStreamingSettings
    {
        TimeSpan GetMaxLLTimeInIntegrator(Counterparty counterparty);
        TimeSpan GetMaxLLTimeOnDestiantion(Counterparty counterparty);
        TimeSpan GetExpectedSendingTime(Counterparty counterparty);
    }
}
