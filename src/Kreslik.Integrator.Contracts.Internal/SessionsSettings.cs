﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.Contracts.Internal.Properties;

namespace Kreslik.Integrator.Contracts.Internal
{
    public class SessionsSettings
    {
        public FIXChannel_CRSSettings FIXChannel_CRS { get; set; }
        public FIXChannel_DBKSettings FIXChannel_DBK { get; set; }
        public FIXChannel_UBSSettings FIXChannel_UBS { get; set; }
        public FIXChannel_CTISettings FIXChannel_CTI { get; set; }
        public FIXChannel_MGSSettings FIXChannel_MGS { get; set; }
        public FIXChannel_RBSSettings FIXChannel_RBS { get; set; }
        public FIXChannel_JPMSettings FIXChannel_JPM { get; set; }
        public FIXChannel_BNPSettings FIXChannel_BNP { get; set; }
        public FIXChannel_CZBSettings FIXChannel_CZB { get; set; }
        public FIXChannel_SOCSettings FIXChannel_SOC { get; set; }
        public FIXChannel_NOMSettings FIXChannel_NOM { get; set; }
        public FIXChannel_HOTSettings FIXChannel_HTA { get; set; }
        public FIXChannel_HOTSettings FIXChannel_HTF { get; set; }
        public FIXChannel_HOTSettings FIXChannel_HT3 { get; set; }
        public FIXChannel_HOTSettings FIXChannel_H4T { get; set; }
        public FIXChannel_HOTSettings FIXChannel_H4M { get; set; }
        public FIXChannel_FALSettings FIXChannel_FAL { get; set; }
        public FIXChannel_LMXSettings FIXChannel_LM1 { get; set; }
        public FIXChannel_LMXSettings FIXChannel_LM2 { get; set; }
        public FIXChannel_LMXSettings FIXChannel_LM3 { get; set; }
        public FIXChannel_LMXSettings FIXChannel_LX1 { get; set; }
        public FIXChannel_LMXSettings FIXChannel_LGA { get; set; }
        public FIXChannel_LMXSettings FIXChannel_LGC { get; set; }
        public FIXChannel_FCMSettings FIXChannel_FC1 { get; set; }
        public FIXChannel_FCMSettings FIXChannel_FC2 { get; set; }
        public FIXChannel_PXMSettings FIXChannel_PX1 { get; set; }
        public StreamingChannelSettings FIXChannel_FS1 { get; set; }
        public StreamingChannelSettings FIXChannel_FS2 { get; set; }
        public StreamingChannelSettings FIXChannel_HTAstream { get; set; }
        public StreamingChannelSettings FIXChannel_HT3stream { get; set; }
        public StreamingChannelSettings FIXChannel_FL1 { get; set; }
        public FIXSTPChannel_CTIPBSettings FIXSTPChannel_CTIPB { get; set; }
        public UnexpectedMessagesTreatingSettings UnexpectedMessagesTreating { get; set; }
        public MarketDataSessionSettings MarketDataSession { get; set; }
        public OrderFlowSessionSettings OrderFlowSession { get; set; }
        public SubscriptionSettingsBag SubscriptionSettingsRaw { get; set; }
        public STPSettings STP { get; set; }


        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_CRSSettings FIXChannel_CRSBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_CRS; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_DBKSettings FIXChannel_DBKBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_DBK; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_UBSSettings FIXChannel_UBSBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_UBS; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_CTISettings FIXChannel_CTIBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_CTI; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_MGSSettings FIXChannel_MGSBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_MGS; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_RBSSettings FIXChannel_RBSBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_RBS; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_JPMSettings FIXChannel_JPMBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_JPM; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_BNPSettings FIXChannel_BNPBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_BNP; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_CZBSettings FIXChannel_CZBBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_CZB; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_SOCSettings FIXChannel_SOCBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_SOC; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_NOMSettings FIXChannel_NOMBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_NOM; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_HOTSettings FIXChannel_HTABehavior
        {
            get { return SessionsSettingsBag.FIXChannel_HTA; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_HOTSettings FIXChannel_HTFBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_HTF; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_HOTSettings FIXChannel_HT3Behavior
        {
            get { return SessionsSettingsBag.FIXChannel_HT3; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_HOTSettings FIXChannel_H4TBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_H4T; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_HOTSettings FIXChannel_H4MBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_H4M; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_FALSettings FIXChannel_FALBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_FAL; }
        }
        
        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_LMXSettings FIXChannel_LM1Behavior
        {
            get { return SessionsSettingsBag.FIXChannel_LM1; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_LMXSettings FIXChannel_LM2Behavior
        {
            get { return SessionsSettingsBag.FIXChannel_LM2; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_LMXSettings FIXChannel_LM3Behavior
        {
            get { return SessionsSettingsBag.FIXChannel_LM3; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_LMXSettings FIXChannel_LX1Behavior
        {
            get { return SessionsSettingsBag.FIXChannel_LX1; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_LMXSettings FIXChannel_LGABehavior
        {
            get { return SessionsSettingsBag.FIXChannel_LGA; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_LMXSettings FIXChannel_LGCBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_LGC; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_FCMSettings FIXChannel_FC1Behavior
        {
            get { return SessionsSettingsBag.FIXChannel_FC1; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_FCMSettings FIXChannel_FC2Behavior
        {
            get { return SessionsSettingsBag.FIXChannel_FC2; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXChannel_PXMSettings FIXChannel_PX1Behavior
        {
            get { return SessionsSettingsBag.FIXChannel_PX1; }
        }

        public static StreamingChannelSettings FIXChannel_FS1Behavior
        {
            get { return SessionsSettingsBag.FIXChannel_FS1; }
        }

        public static StreamingChannelSettings FIXChannel_FL1Behavior
        {
            get { return SessionsSettingsBag.FIXChannel_FL1; }
        }

        public static StreamingChannelSettings FIXChannel_FS2Behavior
        {
            get { return SessionsSettingsBag.FIXChannel_FS2; }
        }

        public static StreamingChannelSettings FIXChannel_HTAstreamBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_HTAstream; }
        }

        public static StreamingChannelSettings FIXChannel_HT3streamBehavior
        {
            get { return SessionsSettingsBag.FIXChannel_HT3stream; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static FIXSTPChannel_CTIPBSettings FIXSTPChannel_CTIPBBehavior
        {
            get { return SessionsSettingsBag.FIXSTPChannel_CTIPB; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static UnexpectedMessagesTreatingSettings UnexpectedMessagesTreatingBehavior
        {
            get { return SessionsSettingsBag.UnexpectedMessagesTreating; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static MarketDataSessionSettings MarketDataSessionBehavior
        {
            get { return SessionsSettingsBag.MarketDataSession; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static OrderFlowSessionSettings OrderFlowSessionBehavior
        {
            get { return SessionsSettingsBag.OrderFlowSession; }
        }

        private static SubscriptionSettingsDictionary _subscriptionSettingsDictionary;

        [System.Xml.Serialization.XmlIgnore]
        public static SubscriptionSettingsDictionary SubscriptionSettings
        {
            get
            {
                if (_subscriptionSettingsDictionary == null)
                {
                    _subscriptionSettingsDictionary =
                        new SubscriptionSettingsDictionary(SessionsSettingsBag.SubscriptionSettingsRaw);
                }

                return _subscriptionSettingsDictionary;
            }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static STPSettings STPBehavior
        {
            get { return SessionsSettingsBag.STP; }
        }

        public static void RefreshSettings()
        {
            _sessionsSettingsBag = null;
            _subscriptionSettingsDictionary = null;
        }

        [System.Xml.Serialization.XmlIgnore]
        private static SessionsSettings SessionsSettingsBag
        {
            get
            {
                if (_sessionsSettingsBag == null)
                {
                    _sessionsSettingsBag = SettingsInitializator.Instance.ReadSettings<SessionsSettings>(
                        @"Kreslik.Integrator.SessionManagement.dll",
                        Resources.Kreslik_Integrator_SessionManagement_dll
                        );
                }

                return _sessionsSettingsBag;
            }
        }

        public static void OverrideSettingsFromAlternatePath(string alternatePath)
        {
            _sessionsSettingsBag = SettingsReader.DeserializeConfiguration<SessionsSettings>(
                        alternatePath,
                        Resources.Kreslik_Integrator_SessionManagement_dll
                        );
        }

        private static SessionsSettings _sessionsSettingsBag;


        public class STPSettings
        {

            [XmlArray(ElementName = "STPDestinations")]
            [XmlArrayItem(ElementName = "STPDestination")]
            public List<STPDestinationSettings> STPDestinations
            {
                get;
                set;
            }

            public class STPDestinationSettings
            {
                public STPCounterparty Name { get; set; }

                [System.Xml.Serialization.XmlIgnore]
                public System.TimeSpan UnconfirmedTicketTimeout { get; set; }

                [System.Xml.Serialization.XmlElement("UnconfirmedTicketTimeout_Seconds")]
                public int UnconfirmedOrderTimeoutXml
                {
                    get { return (int)UnconfirmedTicketTimeout.TotalSeconds; }
                    set { UnconfirmedTicketTimeout = System.TimeSpan.FromSeconds(value); }
                }

                [XmlArray(ElementName = "ServedCounterparties")]
                [XmlArrayItem(ElementName = "Counterparty")]
                public List<string> ServedCounterpartiesXml
                {
                    get;
                    set;
                }

                private List<Counterparty> _servedCounterparties;

                [System.Xml.Serialization.XmlIgnore]
                public List<Counterparty> ServedCounterparties
                {
                    get
                    {
                        if (_servedCounterparties == null)
                        {
                            _servedCounterparties = ServedCounterpartiesXml == null
                                                  ? new List<Counterparty>()
                                                  : ServedCounterpartiesXml.Select(ctpStr => (Counterparty)ctpStr).ToList();
                        }

                        return _servedCounterparties;
                    }

                    set
                    {
                        _servedCounterparties = value;
                        ServedCounterpartiesXml = value == null ? new List<string>() : value.Select(ctp => ctp.ToString()).ToList();
                    }
                }
            }
        }

    }
}