﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts.Internal
{
    public enum IntegratorOrderInfoType
    {
        ExternalOrder,
        InternalMktOrder,
        InternalLmtOrder,
        ExternalClientDealRequest
    }

    public static class IntegratorOrderInfoExtender
    {
        public static bool IsExternal(this IIntegratorOrderInfo orderInfo)
        {
            return orderInfo.IntegratorOrderInfoType == IntegratorOrderInfoType.ExternalClientDealRequest ||
                   orderInfo.IntegratorOrderInfoType == IntegratorOrderInfoType.ExternalOrder;
        }

        public static AtomicSize GetSizeBasePolarized(this IIntegratorOrderInfo orderInfo)
        {
            return AtomicSize.FromDecimal(orderInfo.IntegratorDealDirection == DealDirection.Buy
                ? orderInfo.SizeBaseAbsInitial
                : -orderInfo.SizeBaseAbsInitial);
        }

        public static AtomicSize GetSizeTermPolarized(this IIntegratorOrderInfo orderInfo, decimal? requestedPriceNullable)
        {
            decimal requestedPrice = requestedPriceNullable == null ? orderInfo.RequestedPrice : requestedPriceNullable.Value;
            return AtomicSize.FromDecimal(orderInfo.IntegratorDealDirection == DealDirection.Buy
                ? -orderInfo.SizeBaseAbsInitial * requestedPrice
                : orderInfo.SizeBaseAbsInitial * requestedPrice);
        }
    }

    public interface IIntegratorOrderInfo
    {
        string Identity { get; }
        Common.Symbol Symbol { get; }
        DealDirection IntegratorDealDirection { get; }
        Counterparty Counterparty { get; }
        decimal SizeBaseAbsInitial { get; }
        decimal RequestedPrice { get; }
        IntegratorOrderInfoType IntegratorOrderInfoType { get; }
        bool IsRiskRemovingOrder { get; }
    }

    public interface IClientDealRequest : IIntegratorOrderInfo
    {
        string CounterpartyDealIdentity { get; }
        decimal CounterpartyRequestedPrice { get; }
        decimal CounterpartyRequestedSizeBaseAbs { get; }
        decimal CounterpartyRequestedSizeBaseAbsFillMinimum { get; }
        decimal IntegratorFilledAmountBaseAbs { get; }
        new bool IsRiskRemovingOrder { get; set; }
        DateTime MaxProcessingCutoffTime { get; }
    }
}
