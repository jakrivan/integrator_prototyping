﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts.Internal
{
    public enum TradeReportTicketInfoType
    {
        SentToPB,
        AcceptedByPB,
        BookedByPB,
        RejectedByPB,
        Unexpected,
        SentToTraiana,
        AcceptedByTraiana,
        RejectedByTraiana,
        DeletedByTraiana
    }

    public class TradeReportTicketInfo
    {
        public TradeReportTicketInfo(string integratorDealId, DateTime integratorSentOrReceivedTimeUtc,
                                     DateTime? counterpartySentTime, TradeReportTicketInfoType tradeReportTicketInfoType,
                                     STPCounterparty stpCounterparty, string fixMessageRaw)
        {
            this.IntegratorDealId = integratorDealId;
            this.IntegratorSentOrReceivedTimeUtc = integratorSentOrReceivedTimeUtc;
            this.CounterpartySentTime = counterpartySentTime;
            this.TradeReportTicketInfoType = tradeReportTicketInfoType;
            this.StpCounterparty = stpCounterparty;
            this.FixMessageRaw = fixMessageRaw;
        }

        public string IntegratorDealId { get; private set; }
        public DateTime IntegratorSentOrReceivedTimeUtc { get; private set; }
        public DateTime? CounterpartySentTime { get; private set; }
        public TradeReportTicketInfoType TradeReportTicketInfoType { get; private set; }
        public STPCounterparty StpCounterparty { get; private set; }
        public string FixMessageRaw { get; private set; }
    }

    public class AggregatedTradeTicketInfo
    {
        public AggregatedTradeTicketInfo(string aggregatedTradeReportId, DateTime integratorReceivedTimeUtc, DateTime? counterpartySentTime, decimal aggregatedPrice, decimal aggregatedSizeBaseAbs,
                                         List<string> childTreadesIds, STPCounterparty stpCounterparty, string fixMessageRaw)
        {
            this.AggregatedTradeReportId = aggregatedTradeReportId;
            this.IntegratorReceivedTimeUtc = integratorReceivedTimeUtc;
            this.CounterpartySentTime = counterpartySentTime;
            this.AggregatedPrice = aggregatedPrice;
            this.AggregatedSizeBaseAbs = aggregatedSizeBaseAbs;
            this.ChildTreadesIds = childTreadesIds;
            this.StpCounterparty = stpCounterparty;
            this.FixMessageRaw = fixMessageRaw;
        }

        public string AggregatedTradeReportId { get; private set; }
        public DateTime IntegratorReceivedTimeUtc { get; private set; }
        public DateTime? CounterpartySentTime { get; private set; }
        public decimal AggregatedPrice { get; private set; }
        public decimal AggregatedSizeBaseAbs { get; private set; }
        public List<string> ChildTreadesIds { get; private set; }
        public STPCounterparty StpCounterparty { get; private set; }
        public string FixMessageRaw { get; private set; }
    }
}
