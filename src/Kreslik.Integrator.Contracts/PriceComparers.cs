﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts
{
    public interface IIdentityEqualityComparer<TObj, TIdentity>
    {
        bool IdentityEquals(TObj obj, TIdentity identity);
        bool ItemsEquals(TObj x, TObj y);
        bool ItemsAreIdentical(TObj x, TObj y);
        bool AreIdentical(TObj x, Guid identificator);
    }

    public class BidPriceComparer : IComparer<PriceObject>
    {
        static BidPriceComparer()
        {
            Default = new BidPriceComparer();
        }

        public static BidPriceComparer Default { get; private set; }

        public int Compare(PriceObject x, PriceObject y)
        {
            // The bigger the bid price the better
            int compareResult = decimal.Compare(x.Price, y.Price);

            if (compareResult == 0)
            {
                //the newer (= bigger) the time stamp, the better
                compareResult = DateTime.Compare(x.IntegratorReceivedTimeUtc, y.IntegratorReceivedTimeUtc);

                if (compareResult == 0)
                {
                    //the bigger the size the better
                    compareResult = decimal.Compare(x.SizeBaseAbsRemaining, y.SizeBaseAbsRemaining);
                }
            }

            return compareResult;
        }
    }

    public class AskPriceComparer : IComparer<PriceObject>
    {
        static AskPriceComparer()
        {
            Default = new AskPriceComparer();
        }

        public static AskPriceComparer Default { get; private set; }

        public int Compare(PriceObject x, PriceObject y)
        {
            // The smaller the ask price the better
            int compareResult = decimal.Compare(y.Price, x.Price);

            if (compareResult == 0)
            {
                //the newer (= bigger) the time stamp, the better
                compareResult = DateTime.Compare(x.IntegratorReceivedTimeUtc, y.IntegratorReceivedTimeUtc);

                if (compareResult == 0)
                {
                    //the bigger the size the better
                    compareResult = decimal.Compare(x.SizeBaseAbsRemaining, y.SizeBaseAbsRemaining);
                }
            }

            return compareResult;
        }
    }

    public class BidTopPriceComparer : IComparer<PriceObject>
    {
        static BidTopPriceComparer()
        {
            Default = new BidTopPriceComparer();
        }

        public static BidTopPriceComparer Default { get; private set; }

        public int Compare(PriceObject x, PriceObject y)
        {
            // The bigger the bid price the better
            return decimal.Compare(x.Price, y.Price);
        }
    }

    public class AskTopPriceComparer : IComparer<PriceObject>
    {
        static AskTopPriceComparer()
        {
            Default = new AskTopPriceComparer();
        }

        public static AskTopPriceComparer Default { get; private set; }

        public int Compare(PriceObject x, PriceObject y)
        {
            // The smaller the ask price the better
            return decimal.Compare(y.Price, x.Price);
        }
    }

    public class PriceCounterpartEqualityComparer : IIdentityEqualityComparer<PriceObject, Counterparty>
    {
        static PriceCounterpartEqualityComparer()
        {
            Default = new PriceCounterpartEqualityComparer();
        }

        public static PriceCounterpartEqualityComparer Default { get; private set; }

        public bool IdentityEquals(PriceObject obj, Counterparty identity)
        {
            //this will compare references so it's even faster then casting to ints
            return obj.Counterparty == identity;
        }


        public bool ItemsEquals(PriceObject x, PriceObject y)
        {
            //this will compare references so it's even faster then casting to ints
            return x.Counterparty == y.Counterparty;
        }


        public bool ItemsAreIdentical(PriceObject x, PriceObject y)
        {
            return x.IntegratorIdentity.Equals(y.IntegratorIdentity);
        }

        public bool AreIdentical(PriceObject x, Guid identificator)
        {
            return x.IntegratorIdentity.Equals(identificator);
        }
    }

    public class PriceEqualityComparer : IEqualityComparer<PriceObject>
    {
        static PriceEqualityComparer()
        {
            Default = new PriceEqualityComparer();
        }

        public static PriceEqualityComparer Default { get; private set; }

        public bool Equals(PriceObject x, PriceObject y)
        {
            return x.IntegratorIdentity.Equals(y.IntegratorIdentity);
        }

        public int GetHashCode(PriceObject obj)
        {
            return obj.IntegratorIdentity.GetHashCode();
        }
    }
}
