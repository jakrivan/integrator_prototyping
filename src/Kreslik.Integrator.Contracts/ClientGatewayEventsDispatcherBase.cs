﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public abstract class ClientGatewayEventsDispatcherBase : ITradingService
    {
        private ILogger _logger;
        private string _identity;
        private BlockingCollection<ITradingEvent> _tradingEvents = new BlockingCollection<ITradingEvent>(new ConcurrentQueue<ITradingEvent>());
        private bool _closed = false;
        private readonly bool _usePriceObjectsPooling;
        public bool IsSubscribedToAllPrices { get; set; }

        public ClientGatewayEventsDispatcherBase(ILogger logger, string identity, bool usePriceObjectsPooling)
        {
            this._logger = logger;
            this._identity = identity;
            this._usePriceObjectsPooling = usePriceObjectsPooling;
            ThreadPool.QueueUserWorkItem(ProcessTradingEvents);
        }

        public void OnPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            if (!this._closed)
            {
                if (!this.ThrottlePriceUpdateNeeded(priceUpdateEventArgs))
                {
                    if (this._usePriceObjectsPooling)
                    {
                        IPooledObject pooledObject = priceUpdateEventArgs as IPooledObject;
                        if (pooledObject != null)
                            pooledObject.Acquire();
                    }

                    this._tradingEvents.Add(priceUpdateEventArgs);
                }
            }
            else
            {
                this._logger.Log(LogLevel.Warn, "Attempting to call OnPriceUpdate on ClientProxy for already disconnected client ({0})", this._identity);
            }
        }

        public void OnPriceImproved(IBookTop<PriceObjectInternal> sender, PriceObjectInternal price, DateTime timestamp)
        {
            this.OnPriceUpdate(new SinglePriceChangeEventArgs(PriceUpdateType.PriceOnTopOfBookImproved, timestamp, price, sender.TradingTargetType));
        }

        public void OnPriceDeteriorated(IBookTop<PriceObjectInternal> sender, PriceObjectInternal price, DateTime timestamp)
        {
            PriceUpdateType priceUpdateType = PriceObjectInternal.IsNullPrice(price)
                                                  ? PriceUpdateType.PriceOnTopOfBookRemoved
                                                  : PriceUpdateType.PriceOnTopOfBookDeteriorated;

            this.OnPriceUpdate(new SinglePriceChangeEventArgs(priceUpdateType, timestamp, price, sender.TradingTargetType));
        }

        public void OnNewPriceArrived(IBookTop<PriceObjectInternal> sender, PriceObjectInternal price, DateTime timestamp)
        {
            this.OnPriceUpdate(price);
        }

        public void OnPriceReplaced(IBookTop<PriceObjectInternal> sender, PriceObjectInternal price, DateTime timestamp)
        {
            this.OnPriceUpdate(new SinglePriceChangeEventArgs(PriceUpdateType.PriceOnTopOfBookReplaced, timestamp, price, sender.TradingTargetType));
        }

        public void OnExistingPriceInvalidated(IBookTop<PriceObjectInternal> sender, PriceObjectInternal price, DateTime timestamp)
        {
            this.OnPriceUpdate(new SinglePriceChangeEventArgs(PriceUpdateType.PriceExistingInvalidated, timestamp, price, sender.TradingTargetType));
        }

        public void OnPricesRemoved(IBookTop<PriceObjectInternal> sender, Counterparty counterparty, DateTime timestamp)
        {
            this.OnPriceUpdate(new PriceUpdateEventArgs(PriceUpdateType.PricesRemoved, timestamp, sender.TradingTargetType, counterparty));
        }

        public void OnIntegratorUnicastInfo(IntegratorInfoObjectBase integratorInfoObject)
        {
            if (!this._closed)
            {
                this._tradingEvents.Add(new IntegratorUnicastEvent(integratorInfoObject));
            }
            else
            {
                this._logger.Log(LogLevel.Warn, "Attempting to call OnIntegratorUnicastInfo ({0}) on ClientProxy for already disconnected client ({1})", integratorInfoObject, this._identity);
            }
        }

        public void OnIntegratorBroadcastInfo(IntegratorBroadcastInfoBase integratorInfoObject)
        {
            if (!this._closed)
            {
                this.CheckEventsNum();
                this._tradingEvents.Add(new IntegratorBroadcastEvent(integratorInfoObject));
            }
            else
            {
                this._logger.Log(LogLevel.Warn, "Attempting to call OnIntegratorBroadcastInfo ({0}) on ClientProxy for already disconnected client ({1})", integratorInfoObject, this._identity);
            }
        }

        public void CloseLocalResourcesOnly(string reason)
        {
            if (!this._closed)
            {
                this._logger.Log(LogLevel.Warn, "Service proxy is closing. Reason: {0}", reason);
                this._closed = true;
                _tradingEvents.CompleteAdding();

                try
                {
                    DispatchGatewayClosed_CloseLocalResourcesOnly(reason);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e,
                                              "ClientGatewayEventsDispatcher {0} experienced unhandled exception",
                                              this._identity);
                }
            }
        }

        private DateTime _lastBloatingLoggedTimeUtc;
        private readonly TimeSpan _bloatingCheckInterval = TimeSpan.FromMinutes(1);
        private void CheckEventsNum()
        {
            int eventsCount = this._tradingEvents.Count;

            if (eventsCount > 100 && (DateTime.UtcNow - _lastBloatingLoggedTimeUtc) > _bloatingCheckInterval)
            {
                _lastBloatingLoggedTimeUtc = DateTime.UtcNow;
                this._logger.Log(eventsCount > 300 ? LogLevel.Error : LogLevel.Warn,
                                 "Dispatching queue for [{0}] has [{1}] items. Number should be low and mainly nonincreasing",
                                 this._identity, eventsCount);
            }
        }

        public void OnProxyClosingHandler(ITradingServiceEndpoint sender, string reason, bool isLocalOnlyCall)
        {
            this.CloseLocalResourcesOnly(reason);
        }

        protected virtual bool ThrottlePriceUpdateNeeded(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            return false;
        }

        protected abstract void DispatchPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs);
        protected abstract void DispatchUnicastInfo(IntegratorInfoObjectBase unicastInfo);
        protected abstract void DispatchBroadcastInfo(IntegratorBroadcastInfoBase broadcastInfo);
        protected abstract void DispatchGatewayClosed_CloseLocalResourcesOnly(string reason);
        public event Action<Exception> ExceptionDuringDispatchingOccured;

        private void ProcessExceptionDuringDispatching(Exception e)
        {
            this._logger.LogException(LogLevel.Fatal, e,
                                                      "ClientGatewayEventsDispatcher {0} experienced unhandled exception",
                                                      this._identity);

            if (ExceptionDuringDispatchingOccured != null)
            {
                try
                {
                    ExceptionDuringDispatchingOccured(e);
                }
                catch (Exception ex)
                {
                    this._logger.LogException(LogLevel.Fatal, ex,
                                                            "ClientGatewayEventsDispatcher experienced unhandled exception - during handling another unhandled exception");
                }
            }
        }

        private void ProcessTradingEvents(object unused)
        {
            foreach (ITradingEvent tradingEvent in _tradingEvents.GetConsumingEnumerable())
            {
                switch (tradingEvent.TradingEventType)
                {
                    case TradingEventType.PriceUpdate:
                        try
                        {
                            PriceUpdateEventArgs args = tradingEvent as PriceUpdateEventArgs;

                            DispatchPriceUpdate(args);

                            if (this._usePriceObjectsPooling)
                            {
                                IPooledObject pooledObject = args as IPooledObject;
                                if (pooledObject != null)
                                    pooledObject.Release();
                            }
                        }
                        catch (Exception e)
                        {
                            this.ProcessExceptionDuringDispatching(e);
                        }
                        break;
                    case TradingEventType.IntegratorUnicastInfo:
                        IntegratorUnicastEvent unicastInfo = tradingEvent as IntegratorUnicastEvent;
                        try
                        {
                            this._logger.Log(LogLevel.Debug, "OnIntegratorUnicastInfo ([{0}]) called for client [{1}]. Dispatching delay: [{2}]",
                                 unicastInfo.IntegratorUnicastInfo, this._identity, HighResolutionDateTime.UtcNow - unicastInfo.EventCreatedUtc);

                            DispatchUnicastInfo(unicastInfo.IntegratorUnicastInfo);
                        }
                        catch (Exception e)
                        {
                            this.ProcessExceptionDuringDispatching(e);
                        }
                        break;
                    case TradingEventType.IntegratorBroadcastInfo:
                        IntegratorBroadcastEvent broadcastInfo = tradingEvent as IntegratorBroadcastEvent;
                        try
                        {
                            this._logger.Log(LogLevel.Debug, "OnIntegratorBroadcastInfo ([{0}]) called for client [{1}]. Dispatching delay: [{2}]",
                                 broadcastInfo.IntegratorBroadcastInfo, this._identity, HighResolutionDateTime.UtcNow - broadcastInfo.EventCreatedUtc);

                            DispatchBroadcastInfo(broadcastInfo.IntegratorBroadcastInfo);
                        }
                        catch (Exception e)
                        {
                            this.ProcessExceptionDuringDispatching(e);
                        }
                        break;
                    default:
                        this._logger.Log(LogLevel.Fatal, "Unexpected type of event: {0}", tradingEvent.TradingEventType);
                        break;
                }

                if (_closed)
                {
                    this.ReleaseRestOfTheQueue();
                    return;
                }
            }
        }

        private void ReleaseRestOfTheQueue()
        {
            if(!this._usePriceObjectsPooling)
                return;

            try
            {
                foreach (ITradingEvent tradingEvent in _tradingEvents.GetConsumingEnumerable())
                {
                    IPooledObject pooledObject = tradingEvent as IPooledObject;
                    if (pooledObject != null)
                        pooledObject.Release();
                }
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e,
                    "Unexpected exception during emptying [{0}] queue - there is a possibility of leaks of PriceObjects",
                    this._identity);
            }
            
        }


    }
}
