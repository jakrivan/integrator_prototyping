﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts
{

    [Serializable]
    public class PriceObjectPoolException : Exception
    {
        public PriceObjectPoolException() { }
        public PriceObjectPoolException(string message) : base(message) { }
        public PriceObjectPoolException(string message, Exception inner) : base(message, inner) { }
    }

    public /*static*/ class PriceObjectPool
    {
        private static PriceObjectPool _instance;// = new PriceObjectPool(Counterparty.ValuesCount * 6);

        public static void InitializeInstance(int pooledPricesCount)
        {
            _instance = new PriceObjectPool(pooledPricesCount);
        }
        public static PriceObjectPool Instance { get { return _instance; } }

        public static Type PooledPriceType { get { return typeof (PooledPriceObjectInternal); } }

        private IExtendedObjectPool<PriceObjectInternal> _priceObjectsPool;
        private SafeTimer _perfCountersTimer;
        private IIntegratorPerformanceCounter _perfCounters = IntegratorPerformanceCounter.Instance;

        private PriceObjectPool(int pooledPricesCount)
        {
            var basicPool = new GCFreeObjectPool<PriceObjectInternal>(PooledPriceObjectInternal.CreateEmptyPooled,
                PriceObjectInternal.Invalidate,
                pooledPricesCount, "PriceObjectsPool");
            _priceObjectsPool = new ObjectPoolExtender<PriceObjectInternal>(basicPool, PriceObjectInternal.CreateEmpty,
                item => item is PooledPriceObjectInternal);
            this._perfCountersTimer = new SafeTimer(PerfCountersCallback, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2))
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1sec,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
            this.InitializeCountersState();
        }

        private void InitializeCountersState()
        {
            this._perfCounters.SetPriceObjectsPoolSize(this._priceObjectsPool.TotalPoolSize);
        }

        private void PerfCountersCallback()
        {
            this._perfCounters.SetUnpooledPriceObjectsInUseCount(this._priceObjectsPool.NumberOfUnpooledItemsInUse);
            this._perfCounters.SetTotalUnpooledPriceObjectsCreated(this._priceObjectsPool.TotalNumberOfUnpooledItemsCreated);
            this._perfCounters.SetPooledPriceObjectsInUseCount(this._priceObjectsPool.NumberOfPooledItemsInUse);
        }

        public void Return(PriceObjectInternal priceObject)
        {
            this._priceObjectsPool.Return(priceObject);
        }

        [DataContract]
        private sealed class PooledPriceObjectInternal : PriceObjectInternal
        {
            private PooledPriceObjectInternal() { }

            internal static PriceObjectInternal CreateEmptyPooled()
            {
                return new PooledPriceObjectInternal();
            }

            ~PooledPriceObjectInternal()
            {
                if (this.GetType() != typeof (PriceObjectInternal))
                    LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Pooled PriceObject leaked");
            }
        }

        ////////////
        // Object pool factories
        ////////////


        public int TryFetchBatchOfPriceObjects(PriceObjectInternal[] priceObjectsBatch)
        {
            int fetchedCnt = this._priceObjectsPool.TryFetchBatch(priceObjectsBatch, priceObjectsBatch.Length);
            for (int idx = 0; idx < fetchedCnt; idx++)
            {
                if (priceObjectsBatch[idx] == null)
                    break;

                priceObjectsBatch[idx].AcquireUnchecked();
            }

            return fetchedCnt;
        }

        public PriceObjectInternal GetPriceObject(AtomicDecimal priceConverted, decimal sizeBaseAbsInitial, PriceSide side,
            Symbol symbol, Counterparty counterparty, byte[] counterpartyIdentity,
            MarketDataRecordType marketDataRecordType, DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            if (counterpartyIdentity != null && counterpartyIdentity.Length > PriceObject.MAX_CTP_IDENTITY_LENGTH)
            {
                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Counterparty identity is too long - ignoring. [{0}]",
                    counterpartyIdentity.GetContentAsAsciiString());
                counterpartyIdentity = BufferSegmentUtils.EMPTY;
            }

            var p = this._priceObjectsPool.Fetch();
            p.OverrideContentAndAcquire(priceConverted, sizeBaseAbsInitial, null, null, side, symbol,
                counterparty, marketDataRecordType, null, counterpartyIdentity, integratorReceivedTime,
                counterpartySentTime);
            return p;
        }

        public static PriceObjectInternal OverridePrefetchedPriceObject(PriceObjectInternal priceObjet, AtomicDecimal priceConverted, decimal sizeBaseAbsInitial, PriceSide side,
            Symbol symbol, Counterparty counterparty, byte[] counterpartyIdentity,
            MarketDataRecordType marketDataRecordType, DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            if (counterpartyIdentity != null && counterpartyIdentity.Length > PriceObject.MAX_CTP_IDENTITY_LENGTH)
            {
                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Counterparty identity is too long - ignoring. [{0}]",
                    counterpartyIdentity.GetContentAsAsciiString());
                counterpartyIdentity = BufferSegmentUtils.EMPTY;
            }

            priceObjet.OverrideContent(priceConverted, sizeBaseAbsInitial, null, null, side, symbol,
                counterparty, marketDataRecordType, null, counterpartyIdentity, integratorReceivedTime,
                counterpartySentTime);
            return priceObjet;
        }

        public static PriceObjectInternal OverridePrefetchedHotspotPriceObject(PriceObjectInternal priceObjet, decimal price, decimal sizeBaseAbsInitial, decimal? sizeBaseAbsFillMinimum,
                                   decimal? sizeBaseAbsFillGranularity, PriceSide side, Symbol symbol,
                                   Counterparty counterparty,
                                   MarketDataRecordType marketDataRecordType, TradeSide? tradeSide,
                                   string counterpartyIdentity, DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            if (counterpartyIdentity != null && counterpartyIdentity.Length > PriceObject.MAX_CTP_IDENTITY_LENGTH)
            {
                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Counterparty identity is too long - ignoring. [{0}]",
                    counterpartyIdentity);
                counterpartyIdentity = string.Empty;
            }

            AtomicDecimal priceConverted = new AtomicDecimal(price);
            priceObjet.OverrideContent(priceConverted, sizeBaseAbsInitial, sizeBaseAbsFillMinimum, sizeBaseAbsFillGranularity, side, symbol,
                counterparty, marketDataRecordType, tradeSide, counterpartyIdentity.ToAsciiBuffer(), integratorReceivedTime, counterpartySentTime);
            return priceObjet;
        }

        //public PriceObject CreateNewHotspotOrderPriceObject(decimal price, decimal sizeBaseAbsInitial, decimal? sizeBaseAbsFillMinimum,
        //                           decimal? sizeBaseAbsFillGranularity, PriceSide side, Symbol symbol,
        //                           Counterparty counterparty,
        //                           MarketDataRecordType marketDataRecordType, TradeSide? tradeSide,
        //                           string counterpartyIdentity, DateTime integratorReceivedTime, DateTime counterpartySentTime)
        //{
        //    if (counterpartyIdentity.Length > PriceObject.MAX_CTP_IDENTITY_LENGTH)
        //        throw new PriceObjectPoolException("Counterparty identity is too long");

        //    PriceObject p = this._priceObjectsPool.Fetch();
        //    AtomicDecimal priceConverted = new AtomicDecimal(price);
        //    p.OverrideContentAndAcquire(priceConverted, sizeBaseAbsInitial, sizeBaseAbsFillMinimum, sizeBaseAbsFillGranularity, side, symbol,
        //        counterparty, marketDataRecordType, tradeSide, ParsedBufferSegment.CreateFromAsciiString(counterpartyIdentity), integratorReceivedTime, counterpartySentTime);
        //    return p;
        //}
    }

    [DataContract]
    [KnownType(typeof (PriceObjectInternal))]
    public class PriceObject : PriceUpdateEventArgs, IIntegratorReceivedObjectEx
    {
        public const int MAX_CTP_IDENTITY_LENGTH = 64;


        public static PriceObject GetNullBid(Symbol symbol)
        {
            return PriceObjectInternal.GetNullBidInternal(symbol, Counterparty.SOC, false);
        }

        public static PriceObject GetNullBid(Symbol symbol, Counterparty counterparty)
        {
            return PriceObjectInternal.GetNullBidInternal(symbol, counterparty, false);
        }

        public static PriceObject GetNullAsk(Symbol symbol)
        {
            return PriceObjectInternal.GetNullAskInternal(symbol, Counterparty.SOC, false);
        }

        public static PriceObject GetNullAsk(Symbol symbol, Counterparty counterparty)
        {
            return PriceObjectInternal.GetNullAskInternal(symbol, counterparty, false);
        }

        [DataMember]
        public decimal Price { get; private set; }


        //Reading this is not thread safe
        [DataMember]
        public decimal SizeBaseAbsRemaining { get; protected set; }


        [DataMember]
        public decimal SizeBaseAbsInitial { get; private set; }

        [DataMember]
        public decimal? SizeBaseAbsFillMinimum { get; private set; }

        [DataMember]
        public decimal? SizeBaseAbsFillGranularity { get; private set; }

        [DataMember]
        public PriceSide Side { get; private set; }

        public Symbol Symbol { get; private set; }

        [DataMember]
        //this member is needed so that we can (de)serialize Symbol while still using the same instance of this type
        private int SymbolSerialized
        {
            get { return (int)this.Symbol; }
            set { this.Symbol = (Symbol)value; }
        }

        [DataMember]
        public MarketDataRecordType MarketDataRecordType { get; private set; }

        //TODO: add!
        //[DataMember]
        //public string SubscriptionIdentity { get; private set; }

        [DataMember]
        public TradeSide? TradeSide { get; private set; }

        [DataMember]
        //Guid is a struct so this doesn't create GC garbage
        public Guid IntegratorIdentity { get; private set; }

        [DataMember]
        public byte[] CounterpartyIdentity { get; private set; }

        /// <summary>
        /// Returns CounterpartyIdentity As an Ascii string - allocates memory!
        /// </summary>
        public string CounterpartyIdentityAsAsciiString
        {
            get { return CounterpartyIdentity.ConvertToAsciiStringTrimingNulls(); }
        }

        [DataMember]
        public DateTime CounterpartySentTimeUtc { get; private set; }

        public DateTime IntegratorReceivedTimeUtc
        {
            get { return base.IntegratorEventTimeUtc; }
        }

        protected internal PriceObject() : base(PriceUpdateType.PriceNewArrived)
        {
            this.CounterpartyIdentity = new byte[MAX_CTP_IDENTITY_LENGTH];
        }

        protected void OverrideContentInternal(
            AtomicDecimal priceConverted, decimal sizeBaseAbsInitial, decimal? sizeBaseAbsFillMinimum,
            decimal? sizeBaseAbsFillGranularity, PriceSide side, Symbol symbol, Counterparty counterparty,
            MarketDataRecordType marketDataRecordType, TradeSide? tradeSide,
            byte[] counterpartyIdentity, DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            base.OverrideContent(integratorReceivedTime, counterparty.TradingTargetType, counterparty);

            this.Price = priceConverted.ToDecimal().Normalize();
            this.SizeBaseAbsRemaining = sizeBaseAbsInitial;
            this.SizeBaseAbsInitial = sizeBaseAbsInitial;
            this.SizeBaseAbsFillMinimum = sizeBaseAbsFillMinimum;
            this.SizeBaseAbsFillGranularity = sizeBaseAbsFillGranularity;
            this.Side = side;
            this.Symbol = symbol;
            this.Counterparty = counterparty;
            this.MarketDataRecordType = marketDataRecordType;
            this.TradeSide = tradeSide;
            counterpartyIdentity.CopyToClearingRest(this.CounterpartyIdentity);
            //Guid is a struct so this doesn't create GC garbage
            this.IntegratorIdentity = GuidHelper.NewSequentialGuid();
            this.CounterpartySentTimeUtc = counterpartySentTime;
            this.Counterparty = counterparty;
        }

        protected void DangerousTestingOnly__OverrideContentInternal(
            AtomicDecimal priceConverted, decimal sizeBaseAbsInitial, decimal? sizeBaseAbsFillMinimum,
            decimal? sizeBaseAbsFillGranularity, PriceSide side, Symbol symbol, Counterparty counterparty,
            MarketDataRecordType marketDataRecordType, TradeSide? tradeSide,
            byte[] counterpartyIdentity, Guid integratorIdentity, DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            base.OverrideContent(integratorReceivedTime, counterparty.TradingTargetType, counterparty);

            this.Price = priceConverted.ToDecimal().Normalize();
            this.SizeBaseAbsRemaining = sizeBaseAbsInitial;
            this.SizeBaseAbsInitial = sizeBaseAbsInitial;
            this.SizeBaseAbsFillMinimum = sizeBaseAbsFillMinimum;
            this.SizeBaseAbsFillGranularity = sizeBaseAbsFillGranularity;
            this.Side = side;
            this.Symbol = symbol;
            this.Counterparty = counterparty;
            this.MarketDataRecordType = marketDataRecordType;
            this.TradeSide = tradeSide;
            counterpartyIdentity.CopyToClearingRest(this.CounterpartyIdentity);
            this.IntegratorIdentity = integratorIdentity;
            this.CounterpartySentTimeUtc = counterpartySentTime;
            this.Counterparty = counterparty;
        }

        //TODO: remove after testing phase
        protected static void Invalidate(PriceObject priceObject)
        {
            priceObject.OverrideContent(DateTime.MinValue, TradingTargetType.BankPool, Counterparty.NULL);

            priceObject.Price = 0m;
            priceObject.SizeBaseAbsRemaining = 0m;
            priceObject.SizeBaseAbsInitial = 0m;
            priceObject.SizeBaseAbsFillMinimum = null;
            priceObject.SizeBaseAbsFillGranularity = null;
            priceObject.Symbol = Common.Symbol.NULL;
            priceObject.Counterparty = Counterparty.NULL;
            priceObject.TradeSide = null;
            //nullify first char
            priceObject.CounterpartyIdentity[0] = 0;
            priceObject.IntegratorIdentity = Guid.Empty;
            priceObject.CounterpartySentTimeUtc = DateTime.MinValue;
            priceObject.Counterparty = Counterparty.NULL;
        }
    }


    [DataContract/*(IsReference = true)*/]
    public class PriceObjectInternal : PriceObject, IPooledObject
    {
        private IPoolingHelper _poolingHelper;

        public static PriceObjectInternal GetNullBidInternal(Symbol symbol)
        {
            return GetNullBidInternal(symbol, Counterparty.SOC, true);
        }

        public static PriceObjectInternal GetNullBidInternal(Symbol symbol, Counterparty counterparty)
        {
            return GetNullBidInternal(symbol, counterparty, true);
        }

        internal static PriceObjectInternal GetNullBidInternal(Symbol symbol, Counterparty counterparty, bool acquire)
        {
            var p = new PriceObjectInternal();
            p.OverrideInternalContentInternal(AtomicDecimal.Zero, 0m, null, null, PriceSide.Bid, symbol, counterparty,
                MarketDataRecordType.BankQuoteData, null, null, DateTime.MinValue, DateTime.MinValue);
            if(acquire)
                p._poolingHelper.AcquireInitial();
            else
                p._poolingHelper.SetValid();
            return p;
        }

        public static PriceObjectInternal GetNullAskInternal(Symbol symbol)
        {
            return GetNullAskInternal(symbol, Counterparty.SOC, true);
        }

        public static PriceObjectInternal GetNullAskInternal(Symbol symbol, Counterparty counterparty)
        {
            return GetNullAskInternal(symbol, counterparty, true);
        }

        internal static PriceObjectInternal GetNullAskInternal(Symbol symbol, Counterparty counterparty, bool acquire)
        {
            var p = new PriceObjectInternal();
            p.OverrideInternalContentInternal(AtomicDecimal.MaxValue, 0m, null, null, PriceSide.Ask, symbol, counterparty,
                MarketDataRecordType.BankQuoteData, null, null, DateTime.MinValue, DateTime.MinValue);
            if (acquire)
                p._poolingHelper.AcquireInitial();
            else
                p._poolingHelper.SetValid();
            return p;
        }


        public static bool IsNullPrice(PriceObject price)
        {
            return
                price == null
                ||
                (
                    (price.Price == 0m || price.Price == decimal.MaxValue || price.Price == AtomicDecimal.MaxValueDecimal) 
                    &&
                    price.SizeBaseAbsInitial == 0m
                );
        }

        public static bool IsNullPrice(decimal price)
        {
            return price == 0m || price == decimal.MaxValue || price == AtomicDecimal.MaxValueDecimal;
        }

        public static PriceObjectInternal GetAllocatedTestingPriceObject(decimal price, decimal sizeBaseAbsInitial, PriceSide side,
            Symbol symbol,
            Counterparty counterparty, string counterpartyIdentity, MarketDataRecordType marketDataRecordType,
            DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            var po = new PriceObjectInternal();
            po.OverrideContent(new AtomicDecimal(price), sizeBaseAbsInitial, null, null, side, symbol,
                counterparty, marketDataRecordType, null, counterpartyIdentity.ToAsciiBuffer(), integratorReceivedTime,
                counterpartySentTime);
            return po;
        }

        //for pooling
        protected internal PriceObjectInternal()
        {
            this._poolingHelper = new PoolingHelper(this.ReleaseInternal);
        }

        internal static PriceObjectInternal CreateEmpty()
        {
            return new PriceObjectInternal();
        }


        //not thread safe
        public bool SubstractSizeBaseAbs(decimal sizeBaseAbsToSubstract)
        {
            if (sizeBaseAbsToSubstract < 0)
            {
                return false;
            }

            if (sizeBaseAbsToSubstract > this.SizeBaseAbsRemaining)
            {
                return false;
            }

            this.SizeBaseAbsRemaining -= sizeBaseAbsToSubstract;
            this.SizeBaseAbsRemainingToClaim -= sizeBaseAbsToSubstract;
            //lets cut down to two decimals
            this.SizeBaseAbsRemaining = Decimal.Floor(this.SizeBaseAbsRemaining * 100) / 100;

            return true;
        }

        //not thread safe
        public decimal ClaimSizeBaseAbs(decimal minimumSizeBaseAbsToClaim, decimal maximumSizeBaseAbsToClaim)
        {
            if (maximumSizeBaseAbsToClaim < 0)
            {
                return 0m;
            }

            if (minimumSizeBaseAbsToClaim > this.SizeBaseAbsRemainingToClaim)
            {
                return 0m;
            }

            decimal claimed;

            if (maximumSizeBaseAbsToClaim > this.SizeBaseAbsRemainingToClaim)
            {
                claimed = this.SizeBaseAbsRemainingToClaim;
                this.SizeBaseAbsRemainingToClaim = 0m;
            }
            else
            {
                claimed = maximumSizeBaseAbsToClaim;
                this.SizeBaseAbsRemainingToClaim -= maximumSizeBaseAbsToClaim;
            }

            return claimed;
        }

        //not thread safe
        public bool AddSizeBaseAbs(decimal sizeBaseAbsToAdd)
        {
            if (sizeBaseAbsToAdd < 0)
            {
                return false;
            }

            this.SizeBaseAbsRemaining += sizeBaseAbsToAdd;
            this.SizeBaseAbsRemainingToClaim += sizeBaseAbsToAdd;
            //lets cut down to two decimals
            this.SizeBaseAbsRemaining = Decimal.Floor(this.SizeBaseAbsRemaining * 100) / 100;

            return true;
        }

        public override string ToString()
        {
            return
                String.Format(
                    "Price [{6}, {7}]: {0}, Remaining Size: {1} (Initial: {2}), Side: {3}, Symbol: {4} Counterpart: {5}.",
                    Price, SizeBaseAbsRemaining, SizeBaseAbsInitial, Side, Symbol, Counterparty, this.IntegratorIdentity, this.CounterpartyIdentityAsAsciiString);
        }


        ////////////////////////////////////////////////////////////
        //Internal properties
        ////////////////////////////////////////////////////////////

        public decimal SizeBaseAbsRemainingToClaim { get; protected set; }

        public AtomicDecimal ConvertedPrice { get; private set; }


        ////////////////////////////////////////////////////////////
        //Pooling functions
        ////////////////////////////////////////////////////////////

        public void Acquire()
        {
            _poolingHelper.Acquire();
        }

        internal void AcquireUnchecked()
        {
            this._poolingHelper.AcquireInitial();
        }

        private void ReleaseInternal()
        {
            PriceObjectPool.Instance.Return(this);
        }

        public void Release()
        {
            _poolingHelper.Release();
        }

        public void MarkReleaseDisallowed()
        {
            this._poolingHelper.MarkReleaseDisallowed();
        }

        public void ClearReleaseDisallowed()
        {
            this._poolingHelper.ClearReleaseDisallowed();
        }

        internal void OverrideContentAndAcquire(
            AtomicDecimal priceConverted, decimal sizeBaseAbsInitial, decimal? sizeBaseAbsFillMinimum,
            decimal? sizeBaseAbsFillGranularity, PriceSide side, Symbol symbol, Counterparty counterparty,
            MarketDataRecordType marketDataRecordType, TradeSide? tradeSide,
            byte[] counterpartyIdentity, DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            this.OverrideInternalContentInternal(priceConverted, sizeBaseAbsInitial, sizeBaseAbsFillMinimum, sizeBaseAbsFillGranularity,
                side, symbol, counterparty, marketDataRecordType, tradeSide, counterpartyIdentity,
                integratorReceivedTime, counterpartySentTime);

            _poolingHelper.AcquireInitial();
        }

        public void OverrideContent(
            AtomicDecimal priceConverted, decimal sizeBaseAbsInitial, decimal? sizeBaseAbsFillMinimum,
            decimal? sizeBaseAbsFillGranularity, PriceSide side, Symbol symbol, Counterparty counterparty,
            MarketDataRecordType marketDataRecordType, TradeSide? tradeSide,
            byte[] counterpartyIdentity, DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            this.OverrideInternalContentInternal(priceConverted, sizeBaseAbsInitial, sizeBaseAbsFillMinimum, sizeBaseAbsFillGranularity,
                side, symbol, counterparty, marketDataRecordType, tradeSide, counterpartyIdentity,
                integratorReceivedTime, counterpartySentTime);

            _poolingHelper.SetValid();
        }

        private void OverrideInternalContentInternal(
            AtomicDecimal priceConverted, decimal sizeBaseAbsInitial, decimal? sizeBaseAbsFillMinimum,
            decimal? sizeBaseAbsFillGranularity, PriceSide side, Symbol symbol, Counterparty counterparty,
            MarketDataRecordType marketDataRecordType, TradeSide? tradeSide,
            byte[] counterpartyIdentity, DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            base.OverrideContentInternal(priceConverted, sizeBaseAbsInitial, sizeBaseAbsFillMinimum, sizeBaseAbsFillGranularity,
                side, symbol, counterparty, marketDataRecordType, tradeSide, counterpartyIdentity,
                integratorReceivedTime, counterpartySentTime);

            this.ConvertedPrice = priceConverted;
            this.SizeBaseAbsRemainingToClaim = sizeBaseAbsInitial;
        }

        //TODO: remove after testing phase
        public static void Invalidate(PriceObjectInternal priceObject)
        {
            PriceObject.Invalidate(priceObject);

            priceObject.ConvertedPrice = AtomicDecimal.Zero;
            priceObject.SizeBaseAbsRemainingToClaim = 0m;
        }


        protected void DangerousTestingOnly__OverrideContentInternalAndAcquire(
            AtomicDecimal priceConverted, decimal sizeBaseAbsInitial, decimal? sizeBaseAbsFillMinimum,
            decimal? sizeBaseAbsFillGranularity, PriceSide side, Symbol symbol, Counterparty counterparty,
            MarketDataRecordType marketDataRecordType, TradeSide? tradeSide,
            byte[] counterpartyIdentity, Guid integratorIdentity, DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            base.OverrideContentInternal(priceConverted, sizeBaseAbsInitial, sizeBaseAbsFillMinimum, sizeBaseAbsFillGranularity,
                side, symbol, counterparty, marketDataRecordType, tradeSide, counterpartyIdentity,
                integratorReceivedTime, counterpartySentTime);

            this.ConvertedPrice = priceConverted;
            this.SizeBaseAbsRemainingToClaim = sizeBaseAbsInitial;

            _poolingHelper.AcquireInitial();
        }
    }
}
