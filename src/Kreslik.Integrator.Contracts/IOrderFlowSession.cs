﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    public enum SubmissionResult
    {
        Success = 0,
        Failed_SessionNotRunning = 1,
        Failed_OrderTypeIncorrect = 2,
        Failed_RiskManagementDisallowed = 3,
        Failed_RiskManagementDisallowedCanBeRetryed = 10,
        Failed_InternalError = 4,
        Failed_MessagingLayerError = 5,
        Failed_TooHighFrequency = 6,
        Failed_NotSubscribed = 7,
        Failed_PricingForSenderDisallowed = 8,
        Failed_ProdeucerNotRegistered = 9
    }

    public enum CancelRequestResult
    {
        Success = 0,
        Failed_OrderNotKnown = 1,
        Failed_OrderAlreadyClosed = 2,
        Failed_MessagingLayerError = 3
    }

    public interface IOrderFlowSession : IFIXChannel
    {
        IIntegratorOrderExternal GetNewExternalOrder(OrderRequestInfo orderRequestInfo, out string errorMessage);
        IIntegratorOrderExternal GetNewExternalOrder(OrderRequestInfo orderRequestInfo, Counterparty counterparty,
            out string errorMessage);
        SubmissionResult SubmitOrder(IIntegratorOrderExternal newOrderExternal);
        IIntegratorOrderExternal SendOrder(OrderRequestInfo orderRequestInfo);
        event Action<IIntegratorOrderExternal, OrderChangeEventArgs> OnOrderChanged;
        event Action<IIntegratorOrderExternal> OrderSubmitted;
        event Action<IIntegratorOrderExternal, DateTime> OrderNotActive;
        IEnumerable<IIntegratorOrderExternal> ActiveOrderList { get; }
    }
}
