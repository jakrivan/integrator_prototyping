﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public interface IUnderlyingSessionState
    {
        event Action OnStopInitiated;
        event Action OnStarted;
        bool IsReadyAndOpen { get; }
        Counterparty Counterparty { get; }
    }

    public interface IFIXChannel : IUnderlyingSessionState
    {
        void Start();
        void Stop();
        void StopAsync(TimeSpan delay);
        event Action OnStopped;
        SessionState State { get; }
        bool Inactivated { get; }
    }
}
