﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts.Internal
{
    //[DataContract]
    public enum StpFlowSessionState
    {
        Disconnected,
        ConnectedUnsubscribed,
        ConnectedSubscribed
    }

    public interface IStpFlowSessionInfo
    {
        StpFlowSessionState StpFlowSessionState { get; }
        STPCounterparty StpCounterparty { get; }
    }

    [DataContract]
    public class StpFlowSessionInfo : IStpFlowSessionInfo
    {
        [DataMember]
        public StpFlowSessionState StpFlowSessionState { get; set; }
        [DataMember]
        public STPCounterparty StpCounterparty { get; set; }
    }

    [DataContract]
    public class SessionStatusInfo
    {
        [DataMember]
        public bool Configured { get; private set; }
        [DataMember]
        public bool Claimed { get; set; }
        [DataMember]
        public bool Inactivated { get; private set; }
        [DataMember]
        public SessionState SessionState { get; private set; }

        public SessionStatusInfo(bool configured, bool inactivated, SessionState sessionState)
        {
            this.Configured = configured;
            this.Inactivated = inactivated;
            this.SessionState = sessionState;
        }

        public void SetUnconfigured()
        {
            this.Configured = false;
            this.Inactivated = true;
            this.SessionState = SessionState.Idle;
        }

        public void SetInactivated()
        {
            this.Configured = true;
            this.Inactivated = true;
            this.SessionState = SessionState.Idle;
        }

        public void SetState(SessionState sessionState)
        {
            this.Configured = true;
            this.Inactivated = false;
            this.SessionState = sessionState;
        }
    }

    public interface IFIXSessionsDiagnostics
    {
        List<SessionStatusInfo> MarketDataSessionsStatusInfos { get; }
        List<SessionStatusInfo> OrderFlowSessionsStatusInfos { get; }
        //WCF cannot use interfaces as contracts
        List<StpFlowSessionInfo> StpFlowSessionInfos { get; }
    }
}
