﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public interface IImportantIntegratorInformation
    {
        string Subject { get; }
        string Body { get; }
        int GroupedEntriesCount { get; }
        bool IsBodyTrimmed { get; }
        DateTime InformationDistributedUtc { get; }

        LogLevel SeverityLevel { get; }

        IntegratorProcessType OriginatingProcessType { get; }
        string OriginatingInstanceName { get; }
        string OriginatingComputerName { get; }
    }

    [DataContract]
    public class ImportantIntegratorInformation : IntegratorInfoObjectBase, IImportantIntegratorInformation
    {
        private static int _MAX_BODY_LENGTH = 1000;

        public ImportantIntegratorInformation(string subject, string body, int entriesCount, LogLevel severityLevel)
        {
            this.InformationDistributedUtc = HighResolutionDateTime.UtcNow;
            this.Subject = subject;
            if (body.Length > _MAX_BODY_LENGTH)
            {
                this.Body = body.Substring(0, _MAX_BODY_LENGTH);
                this.IsBodyTrimmed = true;
            }
            else
            {
                this.Body = body;
            }
            this.GroupedEntriesCount = entriesCount;
            this.SeverityLevel = severityLevel;
            this.OriginatingProcessType = IntegratorStateInitializer.IntegratorProcessType;
            this.OriginatingInstanceName = SettingsInitializator.Instance.InstanceName;
            this.OriginatingComputerName = Environment.MachineName;
        }

        public ImportantIntegratorInformation(string subject, string body, int entriesCount)
            : this(subject, body, entriesCount, LogLevel.Fatal)
        { }

        [DataMember]
        public string Subject { get; private set; }
        [DataMember]
        public string Body { get; private set; }
        [DataMember]
        public int GroupedEntriesCount { get; private set; }
        [DataMember]
        public bool IsBodyTrimmed { get; private set; }
        [DataMember]
        public DateTime InformationDistributedUtc { get; private set; }
        [DataMember]
        public LogLevel SeverityLevel { get; private set; }

        [DataMember]
        public IntegratorProcessType OriginatingProcessType { get; private set; }

        [DataMember]
        public string OriginatingInstanceName { get; private set; }
        [DataMember]
        public string OriginatingComputerName { get; private set; }

        public override string ToString()
        {
            return
                string.Format(
                    "ImportantIntegratorInformation [{0:yyyy-MM-dd hh:mm:ss.fffffff}] from {1}-{2} on {3} (entries: {4}, trimmed: {5}).{6}Subject:{7}{6}Body:{6}{8}",
                    this.InformationDistributedUtc, this.OriginatingInstanceName, this.OriginatingProcessType,
                    this.OriginatingComputerName, this.GroupedEntriesCount, this.IsBodyTrimmed, Environment.NewLine,
                    this.Subject, this.Body);
        }
    }
}
