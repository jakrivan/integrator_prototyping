﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts
{
    public enum MarketDataRecordType: byte
    {
        BankQuoteData = 0,
        SingleQuoteCancel = 1,
        StreamCancellation = 2,
        VenueNewOrder = 3,
        VenueOrderChange = 4,
        VenueOrderCancel = 5,
        VenueTradeData = 6,
        KgtOwnOrderOnVenue = 7
    }
}
