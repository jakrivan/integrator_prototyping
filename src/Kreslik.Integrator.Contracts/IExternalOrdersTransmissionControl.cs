﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts
{
    public delegate void OrderTransmissionStatusChangedEventHandler(OrderTransmissionStatusChangedEventArgs orderTransmissionStatusChangedEventArgs);
    public delegate void TradingAllowedStatusChangedEventHandler(TradingAllowedStatusChangedEventArgs tradingAllowedStatusChangedEventArgs);

    public interface IExternalOrdersTransmissionState
    {
        bool IsTransmissionDisabled { get; }
        DateTime GoFlatOnlyLastEnabledTime { get; }
        bool IsGoFlatOnlyEnabled { get; }
        event Action<bool> GoFlatOnlyStateChanged;
        event Action<OrderTransmissionStatusChangedEventArgs> OrderTransmissionChanged;
    }

    public interface IExternalOrdersTransmissionControlScheduling
    {
        void ScheduledDisableTransmission();
        void ScheduledTurnOnGoFlatOnly();
    }

    public interface IExternalOrdersTransmissionControl : IExternalOrdersTransmissionState, IBroadcastInfosStore
    {
        void DisableTransmission(string reason);
        void TurnOnGoFlatOnly(string reason);

        bool AllowPropagationOfStateChangesToBackend { get; set; }
    }
}
