﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public delegate void NewBroadcastInfoEventHandler(IntegratorBroadcastInfoBase broadcastInfo);

    public class BroadcastInfosStoreBase : IBroadcastInfosStore
    {
        protected BroadcastInfosStoreBase(ILogger logger)
        {
            this._logger = logger;
        }

        private ILogger _logger;

        private ConcurrentDictionary<Guid, IntegratorBroadcastInfoBase> _existingInfosDic =
            new ConcurrentDictionary<Guid, IntegratorBroadcastInfoBase>();

        public event NewBroadcastInfoEventHandler NewBroadcastInfo;

        public IEnumerable<IntegratorBroadcastInfoBase> ExistingInfos
        {
            get { return _existingInfosDic.Values; }
        }

        protected void OnNewBroadcastInfo(IntegratorBroadcastInfoBase broadcastInfo)
        {
            if (broadcastInfo.CanBeCached)
            {
                _existingInfosDic[broadcastInfo.BroadcastInfoTypeIdentity] = broadcastInfo;
            }

            if (this.NewBroadcastInfo != null)
            {
                this.NewBroadcastInfo.SafeInvoke(this._logger, broadcastInfo);
            }
        }
    }
}
