﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public interface IIntegratorInstanceUtils
    {
        bool IsCounterpartySessionClaimed(Counterparty counterparty);
        event Action<Counterparty, bool> CounterpartyClaimStatusChanged;
        void ReleaseCounterpartySession(Counterparty counterparty);
        IntegratorRequestResult TryClaimCounterpartySession(Counterparty counterparty);
    }
}
