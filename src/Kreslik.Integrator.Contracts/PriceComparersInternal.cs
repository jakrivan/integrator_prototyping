﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts
{
    public class BidPriceComparerInternal : IComparer<PriceObjectInternal>
    {
        static BidPriceComparerInternal()
        {
            Default = new BidPriceComparerInternal();
        }

        public static BidPriceComparerInternal Default { get; private set; }

        public int Compare(PriceObjectInternal x, PriceObjectInternal y)
        {
            // The bigger the bid price the better
            int compareResult = AtomicDecimal.Compare(x.ConvertedPrice, y.ConvertedPrice);

            if (compareResult == 0)
            {
                //the newer (= bigger) the time stamp, the better
                compareResult = DateTime.Compare(x.IntegratorReceivedTimeUtc, y.IntegratorReceivedTimeUtc);

                if (compareResult == 0)
                {
                    //the bigger the size the better
                    compareResult = decimal.Compare(x.SizeBaseAbsRemaining, y.SizeBaseAbsRemaining);
                }
            }

            return compareResult;
        }
    }

    public class AskPriceComparerInternal : IComparer<PriceObjectInternal>
    {
        static AskPriceComparerInternal()
        {
            Default = new AskPriceComparerInternal();
        }

        public static AskPriceComparerInternal Default { get; private set; }

        public int Compare(PriceObjectInternal x, PriceObjectInternal y)
        {
            // The smaller the ask price the better
            int compareResult = AtomicDecimal.Compare(y.ConvertedPrice, x.ConvertedPrice);

            if (compareResult == 0)
            {
                //the newer (= bigger) the time stamp, the better
                compareResult = DateTime.Compare(x.IntegratorReceivedTimeUtc, y.IntegratorReceivedTimeUtc);

                if (compareResult == 0)
                {
                    //the bigger the size the better
                    compareResult = decimal.Compare(x.SizeBaseAbsRemaining, y.SizeBaseAbsRemaining);
                }
            }

            return compareResult;
        }
    }

    public class BidTopPriceComparerInternal : IComparer<PriceObjectInternal>, IComparer<decimal>
    {
        static BidTopPriceComparerInternal()
        {
            Default = new BidTopPriceComparerInternal();
        }

        public static BidTopPriceComparerInternal Default { get; private set; }

        public int Compare(PriceObjectInternal x, PriceObjectInternal y)
        {
            // The bigger the bid price the better
            return AtomicDecimal.Compare(x.ConvertedPrice, y.ConvertedPrice);
        }

        public int Compare(decimal x, decimal y)
        {
            // The bigger the bid price the better
            return decimal.Compare(x, y);
        }
    }

    public class AskTopPriceComparerInternal : IComparer<PriceObjectInternal>, IComparer<decimal>
    {
        static AskTopPriceComparerInternal()
        {
            Default = new AskTopPriceComparerInternal();
        }

        public static AskTopPriceComparerInternal Default { get; private set; }

        public int Compare(PriceObjectInternal x, PriceObjectInternal y)
        {
            // The smaller the ask price the better
            return AtomicDecimal.Compare(y.ConvertedPrice, x.ConvertedPrice);
        }

        public int Compare(decimal x, decimal y)
        {
            // The bigger the bid price the better
            return decimal.Compare(y, x);
        }
    }

    public class PriceCounterpartEqualityComparerInternal : IIdentityEqualityComparer<PriceObjectInternal, Counterparty>
    {
        static PriceCounterpartEqualityComparerInternal()
        {
            Default = new PriceCounterpartEqualityComparerInternal();
        }

        public static PriceCounterpartEqualityComparerInternal Default { get; private set; }

        public bool IdentityEquals(PriceObjectInternal obj, Counterparty identity)
        {
            //this will compare references so it's even faster then casting to ints
            return obj.Counterparty == identity;
        }


        public bool ItemsEquals(PriceObjectInternal x, PriceObjectInternal y)
        {
            //this will compare references so it's even faster then casting to ints
            return x.Counterparty == y.Counterparty;
        }


        public bool ItemsAreIdentical(PriceObjectInternal x, PriceObjectInternal y)
        {
            return x.IntegratorIdentity.Equals(y.IntegratorIdentity);
        }

        public bool AreIdentical(PriceObjectInternal x, Guid identificator)
        {
            return x.IntegratorIdentity.Equals(identificator);
        }
    }

    public class PriceEqualityComparerInternal : IEqualityComparer<PriceObjectInternal>
    {
        static PriceEqualityComparerInternal()
        {
            Default = new PriceEqualityComparerInternal();
        }

        public static PriceEqualityComparerInternal Default { get; private set; }

        public bool Equals(PriceObjectInternal x, PriceObjectInternal y)
        {
            return x.IntegratorIdentity.Equals(y.IntegratorIdentity);
        }

        public int GetHashCode(PriceObjectInternal obj)
        {
            return obj.IntegratorIdentity.GetHashCode();
        }
    }
}
