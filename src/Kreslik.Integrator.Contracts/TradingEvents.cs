﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public enum TradingEventType
    {
        PriceUpdate,
        IntegratorBroadcastInfo,
        IntegratorUnicastInfo
    }

    [DataContract/*(IsReference = true)*/]
    [KnownType(typeof(PriceUpdateEventArgs))]
    public abstract class ITradingEvent
#if POOLS_INSTRUMENTATION        
        : Kreslik.Integrator.LowLatencyUtils.TimedObjectBase
#endif
    {
        public TradingEventType TradingEventType { get; protected set; }
    }

    public abstract class IntegratorStampedEventBase: ITradingEvent
    {
        protected IntegratorStampedEventBase(TradingEventType tradingEventType)
        {
            this.TradingEventType = tradingEventType;
            this.EventCreatedUtc = HighResolutionDateTime.UtcNow;
        }

        public DateTime EventCreatedUtc { get; private set; }
    }

    public class IntegratorUnicastEvent : IntegratorStampedEventBase
    {
        public IntegratorUnicastEvent(IntegratorInfoObjectBase integratorInfoObject)
            : base(TradingEventType.IntegratorUnicastInfo)
        {
            this.IntegratorUnicastInfo = integratorInfoObject;
        }

        public IntegratorInfoObjectBase IntegratorUnicastInfo { get; private set; }
    }

    public class IntegratorBroadcastEvent : IntegratorStampedEventBase
    {
        public IntegratorBroadcastEvent(IntegratorBroadcastInfoBase integratorInfoObject)
            :base(TradingEventType.IntegratorBroadcastInfo)
        {
            this.IntegratorBroadcastInfo = integratorInfoObject;
        }

        public IntegratorBroadcastInfoBase IntegratorBroadcastInfo { get; private set; }
    }
}
