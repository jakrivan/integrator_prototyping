﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts
{
    [DataContract]
    public class VenueDealObject : PriceUpdateEventArgs
    {
        public VenueDealObject(Symbol symbol, Counterparty counterparty, TradeSide tradeSide, decimal price,
                               DateTime counterpartySuppliedTransactionTimeUtc, DateTime counterpartySentTimeUtc,
                               DateTime integratorReceivedTimeUtc)
            :base(PriceUpdateType.VenueDealUpdate, integratorReceivedTimeUtc, counterparty.TradingTargetType, counterparty)
        {
            this.Symbol = symbol;
            this.Counterparty = counterparty;
            this.TradeSide = tradeSide;
            this.Price = price;
            this.CounterpartySuppliedTransactionTimeUtc = counterpartySuppliedTransactionTimeUtc;
            this.CounterpartySentTimeUtc = counterpartySentTimeUtc;
            this.IntegratorReceivedTimeUtc = integratorReceivedTimeUtc;
        }

        public Symbol Symbol { get; private set; }

        [DataMember]
        //this member is needed so that we can (de)serialize Symbol while still using the same instance of this type
        private int SymbolSerialized
        {
            get { return (int)this.Symbol; }
            set { this.Symbol = (Symbol)value; }
        }

        [DataMember]
        public TradeSide TradeSide { get; private set; }

        [DataMember]
        public decimal Price { get; private set; }

        [DataMember]
        public DateTime CounterpartySuppliedTransactionTimeUtc { get; private set; }

        [DataMember]
        public DateTime CounterpartySentTimeUtc { get; private set; }

        [DataMember]
        public DateTime IntegratorReceivedTimeUtc { get; private set; }

        public override string ToString()
        {
            return
                string.Format(
                    "VenueDealObject: {0} from {1}, AggressorSide: {2}, Price: {3}, TransactionTime(UTC): {4:dd-HH:mm:ss.fff}, Sent(UTC): {5:dd-HH:mm:ss.fff}, Received(UTC): {6:dd-HH:mm:ss.fff}"
                    , this.Symbol, this.Counterparty, this.TradeSide, this.Price,
                    this.CounterpartySuppliedTransactionTimeUtc, this.CounterpartySentTimeUtc,
                    this.IntegratorReceivedTimeUtc);
        }
    }

    //TODO: Temporary - to be consolidated with upper class, but that would require breaking of contracts
    public class VenueDealObjectInternal
    {
        //public FxcmVenueDealObject(Symbol symbol, Counterparty counterparty, TradeSide? tradeSide, decimal price,
        //                        decimal dealSizeBaseAbs, DateTime? counterpartySuppliedTransactionTimeUtc,
        //                        DateTime counterpartySentTimeUtc, DateTime integratorReceivedTimeUtc, BufferSegmentBase counterpartyDealIdentity)
        //{
        //    this.Symbol = symbol;
        //    this.Counterparty = counterparty;
        //    this.TradeSide = tradeSide;
        //    this.Price = price;
        //    this.DealSizeBaseAbs = dealSizeBaseAbs;
        //    this.CounterpartySuppliedTransactionTimeUtc = counterpartySuppliedTransactionTimeUtc;
        //    this.CounterpartySentTimeUtc = counterpartySentTimeUtc;
        //    this.IntegratorReceivedTimeUtc = integratorReceivedTimeUtc;
        //    this.CounterpartyDealIdentity = new byte[PriceObject.MAX_CTP_IDENTITY_LENGTH];
        //    counterpartyDealIdentity.CopyToClearingRest(this.CounterpartyDealIdentity);
        //    this.IntegratorIdentity = GuidHelper.NewSequentialGuid();
        //}

        public void OverrideContent(Symbol symbol, Counterparty counterparty, TradeSide? tradeSide, decimal price,
                                decimal dealSizeBaseAbs, DateTime? counterpartySuppliedTransactionTimeUtc,
                                DateTime counterpartySentTimeUtc, DateTime integratorReceivedTimeUtc, byte[] counterpartyDealIdentity)
        {
            this.Symbol = symbol;
            this.Counterparty = counterparty;
            this.TradeSide = tradeSide;
            this.Price = price;
            this.DealSizeBaseAbs = dealSizeBaseAbs;
            this.CounterpartySuppliedTransactionTimeUtc = counterpartySuppliedTransactionTimeUtc;
            this.CounterpartySentTimeUtc = counterpartySentTimeUtc;
            this.IntegratorReceivedTimeUtc = integratorReceivedTimeUtc;
            counterpartyDealIdentity.CopyToClearingRest(this.CounterpartyDealIdentity);
            this.IntegratorIdentity = GuidHelper.NewSequentialGuid();
        }

        private VenueDealObjectInternal()
        {
            this.CounterpartyDealIdentity = new byte[PriceObject.MAX_CTP_IDENTITY_LENGTH];
        }

        //internal static readonly FxcmVenueDealObject EMPTY_QUOTE = new QuoteObject();

        public static VenueDealObjectInternal GetNewUninitializedObject()
        {
            return new VenueDealObjectInternal();
        }

        public Symbol Symbol { get; private set; }

        public Counterparty Counterparty { get; protected set; }

        public TradeSide? TradeSide { get; private set; }

        public PriceSide? Side { get { return null; } }

        public decimal Price { get; private set; }

        public decimal DealSizeBaseAbs { get; private set; }

        public DateTime? CounterpartySuppliedTransactionTimeUtc { get; private set; }

        public DateTime CounterpartySentTimeUtc { get; private set; }

        public DateTime IntegratorReceivedTimeUtc { get; private set; }

        public byte[] CounterpartyDealIdentity { get; private set; }

        public Guid IntegratorIdentity { get; private set; }

        public override string ToString()
        {
            return
                string.Format(
                    "FxcmVenueDealObject: {0} from {1}, AggressorSide: {2}, Price: {3}, Size: {7} TransactionTime(UTC): {4:dd-HH:mm:ss.fff}, Sent(UTC): {5:dd-HH:mm:ss.fff}, Received(UTC): {6:dd-HH:mm:ss.fff}"
                    , this.Symbol, this.Counterparty, this.TradeSide, this.Price,
                    this.CounterpartySuppliedTransactionTimeUtc, this.CounterpartySentTimeUtc,
                    this.IntegratorReceivedTimeUtc, this.DealSizeBaseAbs);
        }
    }

}
