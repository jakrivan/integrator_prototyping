﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts
{
    public enum TimeInForce
    {
        ImmediateOrKill,
        Day
    }

    /// <summary>
    /// Property bag for order request
    /// Property bag is read-only after the creation so that it cannot be changed after request submission
    /// </summary>
    public class OrderRequestInfo
    {
        private static readonly byte[] EmptyIdentity = new byte[0];

        private OrderRequestInfo() { }

        private OrderRequestInfo(decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side, PriceObject priceObject, bool isRiskRemovingOrder)
        {
            this.SizeBaseAbsInitial = sizeBaseAbsInitial;
            //Allow internal improvements
            this.RequestedPrice = priceObject.Price; //requestedPrice;
            this.Side = side;
            //this.PriceObject = priceObject;
            this.IntegratorPriceIdentity = priceObject.IntegratorIdentity;
            this.IntegratorPriceReceivedUtc = priceObject.IntegratorReceivedTimeUtc;
            if (priceObject.CounterpartyIdentity == null || priceObject.CounterpartyIdentity[0] == 0)
                this._counterpartyIdentity = EmptyIdentity;
            else
            {
                this._counterpartyIdentity = new byte[PriceObject.MAX_CTP_IDENTITY_LENGTH];
                priceObject.CounterpartyIdentity.CopyToClearingRest(this._counterpartyIdentity);
            }
            this.Symbol = priceObject.Symbol;
            this.OrderType = OrderType.Quoted;
            this.TimeInForce = TimeInForce.ImmediateOrKill;
            //this.SizeBaseAbsFillMinimum = sizeBaseAbsInitial;
            this.IsRiskRemovingOrder = isRiskRemovingOrder;
            InitializeFlowSideAtSubmitTime();
        }

        private OrderRequestInfo(decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side, Symbol symbol, TimeInForce timeInForce, decimal sizeBaseAbsFillMinimum, bool isRiskRemovingOrder)
        {
            this.SizeBaseAbsInitial = sizeBaseAbsInitial;
            this.RequestedPrice = requestedPrice;
            this.Side = side;
            this.Symbol = symbol;
            this.OrderType = OrderType.Limit;
            this.TimeInForce = timeInForce;
            this.SizeBaseAbsFillMinimum = sizeBaseAbsFillMinimum;
            this.IsRiskRemovingOrder = isRiskRemovingOrder;
            InitializeFlowSideAtSubmitTime();
        }

        private OrderRequestInfo(decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side, Symbol symbol, TimeInForce timeInForce, bool isRiskRemovingOrder)
            :this(sizeBaseAbsInitial, requestedPrice, side, symbol, timeInForce, sizeBaseAbsInitial, isRiskRemovingOrder)
        { }

        private OrderRequestInfo(decimal sizeBaseAbsInitial, PegType pegType, decimal pegDifferenceBasePolAlwaysAdded, decimal requestedPrice, DealDirection side, Symbol symbol, TimeInForce timeInForce, decimal sizeBaseAbsFillMinimum, bool isRiskRemovingOrder)
        {
            this.SizeBaseAbsInitial = sizeBaseAbsInitial;
            this.RequestedPrice = requestedPrice;
            this.Side = side;
            this.Symbol = symbol;
            this.OrderType = OrderType.Pegged;
            this.PegType = pegType;
            this.PegDifferenceBasePolAlwaysAdded = pegDifferenceBasePolAlwaysAdded;
            this.TimeInForce = timeInForce;
            this.SizeBaseAbsFillMinimum = sizeBaseAbsFillMinimum;
            this.IsRiskRemovingOrder = isRiskRemovingOrder;
            InitializeFlowSideAtSubmitTime();
        }

        public bool TryAdd(OrderRequestInfo secondOrderRequest)
        {
            //Both order requests need to satisfy the basic criteria, 
            // we shouldn't retest those as size might have been decremented during creation of those two
            //However we need to test that both given OrderRequests matches

            if (this.IntegratorPriceIdentity == secondOrderRequest.IntegratorPriceIdentity
                && this.Side == secondOrderRequest.Side
                && this.RequestedPrice == secondOrderRequest.RequestedPrice
                && this.IsRiskRemovingOrder == secondOrderRequest.IsRiskRemovingOrder)
            {
                this.SizeBaseAbsInitial += secondOrderRequest.SizeBaseAbsInitial;
                return true;
            }

            return false;
        }

        //public void SetRejectableDefaults(RejectableDealDefaults rejectableDealDefaults)
        //{
        //    //todo: check if it's day deal?

        //    this.RejectableDealDefaults = rejectableDealDefaults;
        //}

        [System.Runtime.CompilerServices.MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void InitializeFlowSideAtSubmitTime()
        {
            if (this.TimeInForce == TimeInForce.ImmediateOrKill)
            {
                this.FlowSideAtSubmitTime = FlowSide.LiquidityTaker;
            }
        }

        public decimal SizeBaseAbsInitial { get; private set; }
        public decimal RequestedPrice { get; private set; }
        public DealDirection Side { get; private set; }

        //until OrderRequestInfo is pooled it's quite complicated to track lifetime and prevent leaking of PriceObject
        //public PriceObject PriceObject { get; private set; }
        public Guid IntegratorPriceIdentity { get; private set; }
        public DateTime IntegratorPriceReceivedUtc { get; private set; }

        private byte[] _counterpartyIdentity;
        public string CounterpartyIdentityAsAsciiString
        {
            get { return this._counterpartyIdentity.ConvertToAsciiStringTrimingNulls(); }
        }

        public Symbol Symbol { get; private set; }
        public TimeInForce TimeInForce { get; private set; }
        public OrderType OrderType { get; private set; }
        public decimal SizeBaseAbsFillMinimum { get; private set; }
        public PegType PegType { get; private set; }
        public decimal PegDifferenceBasePolAlwaysAdded { get; private set; }
        //public RejectableDealDefaults RejectableDealDefaults { get; private set; }
        public FlowSide? FlowSideAtSubmitTime { get; set; }
        public bool IsRiskRemovingOrder { get; private set; }

        public override string ToString()
        {
            return string.Format("Request to {0} {1} {2} at {3} (created from PriceObject: {4}) - {5} {6}",
                Side, SizeBaseAbsInitial, Symbol, RequestedPrice, IntegratorPriceIdentity, OrderType, TimeInForce);
        }

        public static OrderRequestInfo INVALID_ORDER_REQUEST = new OrderRequestInfo();

        public static bool CanBeSatisfied(decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side, 
            PriceObject priceObject)
        {
            return
                //is side properly populated?
                priceObject != null && priceObject.Price > 0 && side == priceObject.Side.ToOutgoingDealDirection()
                &&
                //does the quote meet the pricing requirements
                (side == DealDirection.Buy
                    ? priceObject.Price <= requestedPrice
                    : priceObject.Price >= requestedPrice
                    )
                &&
                //does the quote meet the liquidity requirements
                priceObject.SizeBaseAbsRemaining >= sizeBaseAbsInitial;
        }

        public static OrderRequestInfo CreateQuoted(decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side,
                                              PriceObject priceObject, bool isRiskRemovingOrder)
        {
            if (OrderRequestInfo.CanBeSatisfied(sizeBaseAbsInitial, requestedPrice, side, priceObject))
            {
                return new OrderRequestInfo(sizeBaseAbsInitial, requestedPrice, side, priceObject, isRiskRemovingOrder);
            }
            else
            {
                return OrderRequestInfo.INVALID_ORDER_REQUEST;
            }
        }

        public static OrderRequestInfo CreateLimit(decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side, Symbol symbol, TimeInForce timeInForce, bool isRiskRemovingOrder)
        {
            return new OrderRequestInfo(sizeBaseAbsInitial, requestedPrice, side, symbol, timeInForce, isRiskRemovingOrder);
        }

        public static OrderRequestInfo CreateLimit(decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side, Symbol symbol, TimeInForce timeInForce, decimal sizeBaseAbsFillMinimum, bool isRiskRemovingOrder)
        {
            return new OrderRequestInfo(sizeBaseAbsInitial, requestedPrice, side, symbol, timeInForce, sizeBaseAbsFillMinimum, isRiskRemovingOrder);
        }

        public static OrderRequestInfo CreatePegged(decimal sizeBaseAbsInitial, PegType pegType,
                                                    decimal pegDifferenceBasePolAlwaysAdded, decimal requestedLimitPrice,
                                                    DealDirection side, Symbol symbol, TimeInForce timeInForce, bool isRiskRemovingOrder)
        {
            return new OrderRequestInfo(sizeBaseAbsInitial, pegType, pegDifferenceBasePolAlwaysAdded,
                                        requestedLimitPrice, side, symbol, timeInForce, sizeBaseAbsInitial, isRiskRemovingOrder);
        }

        public static OrderRequestInfo CreatePegged(decimal sizeBaseAbsInitial, PegType pegType,
                                                    decimal pegDifferenceBasePolAlwaysAdded, decimal requestedLimitPrice,
                                                    DealDirection side, Symbol symbol, TimeInForce timeInForce, decimal sizeBaseAbsFillMinimum, bool isRiskRemovingOrder)
        {
            return new OrderRequestInfo(sizeBaseAbsInitial, pegType, pegDifferenceBasePolAlwaysAdded,
                                        requestedLimitPrice, side, symbol, timeInForce, sizeBaseAbsFillMinimum, isRiskRemovingOrder);
        }
    }
}
