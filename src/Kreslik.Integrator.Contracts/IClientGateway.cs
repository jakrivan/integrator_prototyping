﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public struct DealInfo
    {
        public DealInfo(Symbol symbol, DealDirection requestedDealDirection, decimal sizeBaseAbs, decimal price)
            :this()
        {
            this.Symbol = symbol;
            this.DealDirection = requestedDealDirection;
            this.SizeBaseAbs = sizeBaseAbs;
            this.Price = price;
        }

        public Symbol Symbol { get; private set; }
        public decimal SizeBaseAbs { get; private set; }
        public decimal SizeBasePol { get { return DealDirection == DealDirection.Buy ? SizeBaseAbs : -SizeBaseAbs; } }
        public DealDirection DealDirection { get; private set; }
        public decimal Price { get; private set; }
        public decimal SizeTermAbs { get { return SizeBaseAbs*Price; } }
        public decimal SizeTermPol { get { return -SizeBasePol*Price; } }
    }

    public enum RoundingKind
    {
        /// <summary>
        /// 3.26 with granularity 0.05 rounded to 3.25
        /// -3.26 with granularity 0.05 rounded to -3.25
        /// 3.29 with granularity 0.05 rounded to 3.25
        /// -3.29 with granularity 0.05 rounded to -3.25
        /// </summary>
        AlwaysToZero,
        /// <summary>
        /// 3.26 with granularity 0.05 rounded to 3.30
        /// -3.26 with granularity 0.05 rounded to -3.30
        /// 3.29 with granularity 0.05 rounded to 3.30
        /// -3.29 with granularity 0.05 rounded to -3.30
        /// </summary>
        AlwaysAwayFromZero,
        /// <summary>
        /// 3.26 with granularity 0.05 rounded to 3.25
        /// -3.26 with granularity 0.05 rounded to -3.25
        /// 3.29 with granularity 0.05 rounded to 3.30
        /// -3.29 with granularity 0.05 rounded to -3.30
        /// </summary>
        ToNearest,
        /// <summary>
        /// 3.26 with granularity 0.05 rounded to 3.30
        /// -3.26 with granularity 0.05 rounded to -3.25
        /// 3.29 with granularity 0.05 rounded to 3.30
        /// -3.29 with granularity 0.05 rounded to -3.25
        /// </summary>
        ToCeiling,
        /// <summary>
        /// 3.26 with granularity 0.05 rounded to 3.25
        /// -3.26 with granularity 0.05 rounded to -3.30
        /// 3.29 with granularity 0.05 rounded to 3.25
        /// -3.29 with granularity 0.05 rounded to -3.30
        /// </summary>
        ToFloor
    }

    public class RoundingResult
    {
        public static RoundingResult CreateSuccessResult(decimal resultValue)
        {
            return new RoundingResult(true, null, resultValue);
        }

        public static RoundingResult CreateFailedResult(string errorMessage)
        {
            return new RoundingResult(false, errorMessage, 0);
        }

        private RoundingResult(bool roundingSucceeded, string errorMessage, decimal roundedValue)
        {
            this.RoundingSucceeded = roundingSucceeded;
            this.ErrorMessage = errorMessage;
            this._roundedValue = roundedValue;
        }

        public bool RoundingSucceeded { get; private set; }
        public string ErrorMessage { get; private set; }

        private decimal _roundedValue;
        public decimal RoundedValue
        {
            get
            {
                if (this.RoundingSucceeded)
                    return this._roundedValue;
                else
                    throw new ClientGatewayException("Attempt to use rounded result value after unsuccessful rounding!");
            }
        }
    }

    public interface ISymbolsInfo
    {
        decimal GetCommisionPricePerUnit(Counterparty counterparty);
        decimal GetCommisionPricePerMillion(Counterparty counterparty);

        bool IsSupported(Symbol symbol, TradingTargetType tradingTargetType);
        bool IsSupported(Currency currency);
        List<string> GetAscendingSortedStringListOfSupportedSymbols(TradingTargetType tradingTargetType);

        decimal? GetMinimumPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType);
        bool IsPriceWithinPriceIncrementLimit(decimal price, Symbol symbol, TradingTargetType tradingTargetType);
        RoundingResult TryRoundToSupportedPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType, RoundingKind roundingKind, decimal price);
        RoundingResult TryRoundToSupportedPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType, decimal price);

        decimal? GetOrderMinimumSize(Symbol symbol, TradingTargetType tradingTargetType);
        decimal? GetOrderMinimumSizeIncrement(Symbol symbol, TradingTargetType tradingTargetType);
        bool IsOrderSizeWithinSizeAndIncrementLimit(decimal orderSize, Symbol symbol, TradingTargetType tradingTargetType);
        bool IsOrderMinSizeZeroOrWithinSizeAndIncrementLimit(decimal orderMinSize, Symbol symbol, TradingTargetType tradingTargetType);
        bool IsOrderSizeWithinIncrementLimit(decimal orderSize, Symbol symbol, TradingTargetType tradingTargetType);
        RoundingResult TryRoundToSupportedSizeIncrement(Symbol symbol, TradingTargetType tradingTargetType, RoundingKind roundingKind, decimal size);
        bool TryRoundToSupportedPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType, RoundingKind roundingKind, decimal price, out decimal result);
        RoundingResult TryRoundToSupportedSizeIncrement(Symbol symbol, TradingTargetType tradingTargetType, decimal size);
    }

    public delegate void ImportantIntegratorInformationEventHandler(IImportantIntegratorInformation importantIntegratorInformation);
    

    public interface ITradingInfo
    {
        ISymbolsInfo Symbols { get; }

        event OrderTransmissionStatusChangedEventHandler OrderTransmissionStatusChanged;
        event TradingAllowedStatusChangedEventHandler TradingAllowedStatusChanged;
        /// <summary>
        /// Event for Important information from Integrator.
        /// In case exception was thrown - (Un)subscription wasn't performed and had no effect. It can be safely retryied (however underlying exceptions can still happen)
        /// </summary>
        /// <exception cref="ClientGatewayException">Thrown when first/last subscription/unsubscription fails to inform server</exception>
        event ImportantIntegratorInformationEventHandler ImportantIntegratorInformationArrived;
        bool IsOrderTransmissionEnabled { get; }
        TradingState TradingState { get; }
        decimal? GetReferencePrice(Symbol symbol, bool useOnline);
        decimal? ConvertToUsd(Currency sourceCurrency, decimal sourceAmount, bool useOnline);
    }

    public delegate void NewDealHandler(IntegratorDealInternal integratorDealInternal);
    public delegate void NewUnconfirmedDealHandler(IntegratorUnconfirmedDealInternal integratorUnconfirmedDealInternal);

    public delegate void CounterpartyRejectedIntegratorOrderHandler(CounterpartyRejectionInfo counterpartyRejectionInfo);

    public delegate void CounterpartyIgnoredIntegratorOrderHandler(CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo);

    public delegate void BankPoolClientOrderUpdatedHandler(BankPoolClientOrderUpdateInfo clientOrderUpdateInfo);
    public delegate void HotspotClientOrderUpdatedHandler(HotspotClientOrderUpdateInfo clientOrderUpdateInfo);
    public delegate void FXallClientOrderUpdatedHandler(FXallClientOrderUpdateInfo clientOrderUpdateInfo);
    public delegate void LmaxClientOrderUpdatedHandler(LmaxClientOrderUpdateInfo clientOrderUpdateInfo);
    public delegate void ClientOrderUpdatedHandler<in T>(T clientOrderUpdateInfo) where T : ClientOrderUpdateInfo;

    public delegate void GatewayClosedHandler(string reasonForClosing);


    public delegate void NewStreamingDealAcceptedByIntegratorHandler(IStreamingDealAcceptedByIntegrator streamingExecutedDeal);
    public delegate void NewStreamingDealRejectedByIntegratorHandler(IStreamingDealRejectedByIntegrator streamingRejectedDeal);
    public delegate void NewStreamingDealRejectedByCounterpartyHandler(IStreamingDealRejectedByCounterparty streamingRejectedDeal);


    public class PriceIndividualUpdateEventArgs
    {
        public enum PriceIndividualUpdateEventType
        {
            PriceNewArrived,
            PricesForCounterpartyRemoved,
            PriceIndividualInvalidated
        }

        public PriceIndividualUpdateEventArgs(Counterparty counterparty, TradingTargetType senderTradingTargetType, PriceIndividualUpdateEventType eventType, PriceObject priceObject, DateTime eventCreatedTimeUtc)
        {
            this.Counterparty = counterparty;
            this.SenderTradingTargetType = senderTradingTargetType;
            this.EventType = eventType;
            this.PriceObject = priceObject;
            this.EventCreatedTimeUtc = eventCreatedTimeUtc;
        }

        public static bool IsMatchingEvent(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            return
                priceUpdateEventArgs.PriceUpdateType == PriceUpdateType.PriceNewArrived ||
                priceUpdateEventArgs.PriceUpdateType == PriceUpdateType.PriceExistingInvalidated ||
                priceUpdateEventArgs.PriceUpdateType == PriceUpdateType.PricesRemoved;
        }

        public static PriceIndividualUpdateEventArgs CreateFromPriceUpdateEventArgs(
            PriceUpdateEventArgs priceUpdateEventArgs, ILogger logger)
        {
            Counterparty counterparty = priceUpdateEventArgs.Counterparty;
            TradingTargetType senderTradingTargetType = priceUpdateEventArgs.SenderTradingTargetType;
            PriceIndividualUpdateEventType eventType;
            PriceObject priceObject;

            switch (priceUpdateEventArgs.PriceUpdateType)
            {
                case PriceUpdateType.PriceNewArrived:
                    eventType = PriceIndividualUpdateEventType.PriceNewArrived;
                    priceObject = priceUpdateEventArgs as PriceObject;
                    break;
                case PriceUpdateType.PriceExistingInvalidated:
                    eventType = PriceIndividualUpdateEventType.PriceIndividualInvalidated;
                    priceObject = (priceUpdateEventArgs as SinglePriceChangeEventArgs).PriceObject;
                    break;
                case PriceUpdateType.PricesRemoved:
                    eventType = PriceIndividualUpdateEventType.PricesForCounterpartyRemoved;
                    priceObject = null;
                    break;

                case PriceUpdateType.PriceOnTopOfBookImproved:
                case PriceUpdateType.PriceOnTopOfBookDeteriorated:
                case PriceUpdateType.PriceOnTopOfBookAdded:
                case PriceUpdateType.PriceOnTopOfBookRemoved:
                case PriceUpdateType.PriceOnTopOfBookReplaced:
                case PriceUpdateType.VenueDealUpdate:
                default:
                    logger.Log(LogLevel.Fatal, "Receiving price update (supposedly IndividualPriceUpdate) with unexpected PriceUpdateType: {0}",
                                               priceUpdateEventArgs);
                    return null;
            }

            return new PriceIndividualUpdateEventArgs(counterparty, senderTradingTargetType, eventType, priceObject,
                                                      priceUpdateEventArgs.IntegratorEventTimeUtc);
        }

        public Counterparty Counterparty { get; private set; }

        public TradingTargetType SenderTradingTargetType { get; private set; }

        public PriceIndividualUpdateEventType EventType { get; private set; }

        public DateTime EventCreatedTimeUtc { get; private set; }

        public PriceObject PriceObject { get; private set; }

        public override string ToString()
        {
            return string.Format("PriceIndividualUpdateEventArgs [{0}]: {1} at {2}, object: {3}", this.Counterparty,
                this.EventType, this.EventCreatedTimeUtc, this.PriceObject == null ? "<NULL>" : this.PriceObject.ToString());
        }
    }

    public class PricesTopOfBookUpdateEventArgs
    {
        public enum PricesTopOfBookUpdateEventType
        {
            TopOfBookAdded,
            TopOfBookImproved,
            TopOfBookDeteriorated,
            TopOfBookReplaced,
            TopOfBookRemoved
        }

        public PricesTopOfBookUpdateEventArgs(Symbol symbol, PriceSide side, PricesTopOfBookUpdateEventType eventType, PriceObject priceObject, DateTime topOfBookChangeTimeUtc)
        {
            this.Symbol = symbol;
            this.Side = side;
            this.EventType = eventType;
            this.PriceObject = priceObject;
            this.PerSymbolAndSideTopOfBookChangeTimeUtc = topOfBookChangeTimeUtc;
        }

        public static PricesTopOfBookUpdateEventArgs CreateFromPriceUpdateEventArgs(
            PriceUpdateEventArgs priceUpdateEventArgs, ILogger logger)
        {
            SinglePriceChangeEventArgs singlePriceChangeEventArgs =
                priceUpdateEventArgs as SinglePriceChangeEventArgs;

            if (singlePriceChangeEventArgs == null)
            {
                logger.Log(LogLevel.Fatal, "Receiving price update which is null or of an unexpected type: {0}",
                                               priceUpdateEventArgs);
                return null;
            }

            PricesTopOfBookUpdateEventType tobUpdateEventType;

            switch (singlePriceChangeEventArgs.PriceUpdateType)
            {
                case PriceUpdateType.PriceOnTopOfBookImproved:
                    tobUpdateEventType = PricesTopOfBookUpdateEventType.TopOfBookImproved;
                    break;
                case PriceUpdateType.PriceOnTopOfBookDeteriorated:
                    tobUpdateEventType = PricesTopOfBookUpdateEventType.TopOfBookDeteriorated;
                    break;
                case PriceUpdateType.PriceOnTopOfBookAdded:
                    tobUpdateEventType = PricesTopOfBookUpdateEventType.TopOfBookAdded;
                    break;
                case PriceUpdateType.PriceOnTopOfBookRemoved:
                    tobUpdateEventType = PricesTopOfBookUpdateEventType.TopOfBookRemoved;
                    break;
                case PriceUpdateType.PriceOnTopOfBookReplaced:
                    tobUpdateEventType = PricesTopOfBookUpdateEventType.TopOfBookReplaced;
                    break;
                case PriceUpdateType.PriceNewArrived:
                case PriceUpdateType.PriceExistingInvalidated:
                case PriceUpdateType.PricesRemoved:
                case PriceUpdateType.VenueDealUpdate:
                default:
                    logger.Log(LogLevel.Fatal, "Receiving price update with unexpected PriceUpdateType: {0}",
                                               priceUpdateEventArgs);
                    return null;
            }

            PriceObject priceObject = PriceObjectInternal.IsNullPrice(singlePriceChangeEventArgs.PriceObject)
                                          ? null
                                          : singlePriceChangeEventArgs.PriceObject;

            //this can happen for PriceUpdateType.PriceOnTopOfBookDeteriorated and PriceUpdateType.PriceOnTopOfBookRemoved 
            if (priceObject == null)
            {
                tobUpdateEventType = PricesTopOfBookUpdateEventType.TopOfBookRemoved;
            }

            PricesTopOfBookUpdateEventArgs tobEventArgs =
                new PricesTopOfBookUpdateEventArgs(singlePriceChangeEventArgs.PriceObject.Symbol,
                                                   singlePriceChangeEventArgs.PriceObject.Side, tobUpdateEventType,
                                                   priceObject, priceUpdateEventArgs.IntegratorEventTimeUtc);

            return tobEventArgs;
        }

        public Symbol Symbol { get; private set; }

        public PriceSide Side { get; private set; }

        public PricesTopOfBookUpdateEventType EventType { get; private set; }

        public DateTime PerSymbolAndSideTopOfBookChangeTimeUtc { get; private set; }

        public PriceObject PriceObject { get; private set; }

        public override string ToString()
        {
            return string.Format("PricesTopOfBookUpdateEventArgs [{0}-{1}]: {2} at {3}, object: {4}", this.Symbol,
                                 this.Side, this.EventType, this.PerSymbolAndSideTopOfBookChangeTimeUtc, this.PriceObject);
        }
    }

    public interface IPricesTopOfBookHubActions<T> where T : ICounterparty
    {
        IntegratorRequestResult SubscribeToUpdates(TopOfBookIntegratorSubscriptionRequest<T> subscriptionRequestInfo);
        IntegratorRequestResult UnsubscribeFromUpdates(TopOfBookIntegratorUnsubscriptionRequest<T> unsubscriptionRequestInfo);

        event Action<PricesTopOfBookUpdateEventArgs> NewUpdateAvailable;
    }

    public interface IPricesTopOfBookHubVenueRequest<T> where T : ICounterparty
    {
        TopOfBookIntegratorSubscriptionRequest<T> GetSubscriptionRequest(Symbol symbol, T counterparty);
        TopOfBookIntegratorUnsubscriptionRequest<T> GetUnsubscriptionRequest(Symbol symbol, T counterparty);
    }

    public interface IPricesTopOfBookHubBankPoolRequests
    {
        TopOfBookIntegratorSubscriptionRequest<BankCounterparty> GetSubscriptionRequest(Symbol symbol);
        TopOfBookIntegratorUnsubscriptionRequest<BankCounterparty> GetUnsubscriptionRequest(Symbol symbol);
    }

    public interface IVenuePricesTopOfBookHub<T> : IPricesTopOfBookHubActions<T>, IPricesTopOfBookHubVenueRequest<T> where T : ICounterparty
    {}

    public interface IBankPoolPricesTopOfBookHub : IPricesTopOfBookHubActions<BankCounterparty>, IPricesTopOfBookHubBankPoolRequests
    { }

    public interface IPricesIndividualHub<T> where T : ICounterparty
    {
        IntegratorRequestResult SubscribeToUpdates(IndividualPricesIntegratorSubscriptionRequest<T> subscriptionRequestInfo);
        IntegratorRequestResult UnsubscribeFromUpdates(IndividualPricesIntegratorUnsubscriptionRequest<T> unsubscriptionRequestInfo);

        IndividualPricesIntegratorSubscriptionRequest<BankCounterparty> GetSubscriptionRequest(Symbol symbol);
        //TopOfBookIntegratorSubscriptionRequest<T> GetSubscriptionRequest(Symbol symbol, T counterparty);
        IndividualPricesIntegratorUnsubscriptionRequest<BankCounterparty> GetUnsubscriptionRequest(Symbol symbol);
        //TopOfBookIntegratorUnsubscriptionRequest<T> GetUnsubscriptionRequest(Symbol symbol, T counterparty);

        event Action<PriceIndividualUpdateEventArgs> NewUpdateAvailable;
    }

    public interface IVenueDealsHub<T> where T : ICounterparty
    {
        IntegratorRequestResult SubscribeToUpdates(VenueDealsIntegratorSubscriptionRequest<T> subscriptionRequestInfo);
        IntegratorRequestResult UnsubscribeFromUpdates(VenueDealsIntegratorUnsubscriptionRequest<T> unsubscriptionRequestInfo);

        VenueDealsIntegratorSubscriptionRequest<T> GetSubscriptionRequest(Symbol symbol, T counterparty);
        VenueDealsIntegratorUnsubscriptionRequest<T> GetUnsubscriptionRequest(Symbol symbol, T counterparty);

        event NewVenueDealHandler NewUpdateAvailable;
    }


    public interface IUnicastEventsTarget<out T> where T : ClientOrderUpdateInfo
    {
        event NewDealHandler NewDealDone;
        event NewUnconfirmedDealHandler NewUnconfirmedDealDone;
        event CounterpartyRejectedIntegratorOrderHandler CounterpartyRejectedIntegratorOrder;
        event CounterpartyIgnoredIntegratorOrderHandler CounterpartyIgnoredIntegratorOrder;
        event ClientOrderUpdatedHandler<T> ClientOrderUpdated;
    }
    
    [Flags]
    public enum ClientForwardingRequestType
    {
        Deals = 1,
        UnconfirmedDeals = 2,
        /// <summary>
        /// All Order updates for known orders (cancel attempts of unknown orders will not be forwarded)
        /// </summary>
        OrderUpdates = 4,
        CounterpartyRejections = 8,
        CounterpartyIgnores = 16,
        AllUpdates = Deals | UnconfirmedDeals | OrderUpdates | CounterpartyRejections | CounterpartyIgnores
    }

    public enum ClientStreamingForwardingRequestType
    {
        AllStreamingEvents
    }

    public class ClientForwardingRequest
    {
        public ClientForwardingRequest(Symbol symbol, ClientForwardingRequestType requestType)
        {
            this.Symbol = symbol;
            this.RequestType = requestType;
        }

        public Symbol Symbol { get; private set; }
        public ClientForwardingRequestType RequestType { get; private set; }
    }

    public class ClientStreamingForwardingRequest
    {
        public ClientStreamingForwardingRequest(Symbol symbol, ClientStreamingForwardingRequestType requestType)
        {
            this.Symbol = symbol;
            this.RequestType = requestType;
        }

        public Symbol Symbol { get; private set; }
        public ClientStreamingForwardingRequestType RequestType { get; private set; }
    }

    public interface IAllClientsUnicastForwarding<out T> : IUnicastEventsTarget<T> where T : ClientOrderUpdateInfo
    {
        /// <summary>
        /// Requests subscription to unicast forwarding events
        ///   Symbol and event types need to be specified inside request.
        /// REMARKS:
        ///     - OrderUpdates forwards only updates for known orders (cancel attempts of unknown orders will not be forwarded)
        ///     - OrderUpdates forwards have <see cref="ClientOrderUpdateInfo.ClientOrder"/> member always set to null     
        ///     - Multiple subscriptions are ignored (info is forwarded only once)
        /// </summary>
        /// <param name="forwardingRequest">Request specifying required info type</param>
        /// <returns>Result of the request</returns>
        IntegratorRequestResult SubscribeToUpdates(ClientForwardingRequest forwardingRequest);

        /// <summary>
        /// Requests unsubscription from unicast forwarding events
        ///   Symbol and event types need to be specified inside request.
        /// REMARKS:
        ///     - Multiple unsubscriptions or unsubscription of not subscribed events is ignored. 
        ///         So it's e.g. safe to unsubscribe ClientForwardingRequestType.AllUpdates when only ClientForwardingRequestType.Deals were subscribed
        /// </summary>
        /// <param name="forwardingRequest">Request specifying required info type</param>
        /// <returns>Result of the request</returns>
        IntegratorRequestResult UnsubscribeFromUpdates(ClientForwardingRequest forwardingRequest);
    }


    public interface IUnicastEventsAndForwardsTarget<out T> : IUnicastEventsTarget<T> where T : ClientOrderUpdateInfo
    {
        /// <summary>
        /// Target for requesting and obtaining forwarded info from all clients of current TradingTargetType
        /// </summary>
        IAllClientsUnicastForwarding<T> AllClients { get; }
    }

    public interface IFXCMStreamingEvents
    {
        event NewStreamingDealAcceptedByIntegratorHandler NewStreamingDealAcceptedByIntegrator;
        event NewStreamingDealRejectedByIntegratorHandler NewStreamingDealRejectedByIntegrator;
        event NewStreamingDealRejectedByCounterpartyHandler NewStreamingDealRejectedByCounterparty;
    }

    public interface IFXCMAllClients : IFXCMStreamingEvents
    {
        /// <summary>
        /// Requests subscription to unicast forwarding events
        ///   Symbol and event types need to be specified inside request.
        /// </summary>
        /// <param name="forwardingRequest">Request specifying required info type</param>
        /// <returns>Result of the request</returns>
        IntegratorRequestResult SubscribeToUpdates(ClientStreamingForwardingRequest forwardingRequest);

        /// <summary>
        /// Requests unsubscription from unicast forwarding events
        ///   Symbol and event types need to be specified inside request.
        /// REMARKS:
        ///     - Multiple unsubscriptions or unsubscription of not subscribed events is ignored. 
        ///         So it's e.g. safe to unsubscribe ClientForwardingRequestType.AllUpdates when only ClientForwardingRequestType.Deals were subscribed
        /// </summary>
        /// <param name="forwardingRequest">Request specifying required info type</param>
        /// <returns>Result of the request</returns>
        IntegratorRequestResult UnsubscribeFromUpdates(ClientStreamingForwardingRequest forwardingRequest);
    }

    public interface IFXCMStreamingTarget
    {
        IFXCMAllClients AllClients { get; }
    }

    public interface IBankPoolTarget : IUnicastEventsAndForwardsTarget<BankPoolClientOrderUpdateInfo>
    {
        IPricesIndividualHub<BankCounterparty> PricesIndividual { get; }
        IBankPoolPricesTopOfBookHub PricesTopOfBook { get; }

        BankPoolClientOrderRequestInfo CreateLimitOrderRequest(decimal totalAmountBaseAbs, decimal requestedPrice,
                                                               DealDirection side, Symbol symbol, bool isRiskRemovingOrder);

        BankPoolClientOrderRequestInfo CreateMarketOrderRequest(decimal totalAmountBaseAbs, DealDirection side,
                                                                Symbol symbol, bool isRiskRemovingOrder);

        BankPoolClientOrderRequestInfo CreateMarketOnImprovementOrderRequest(decimal totalAmountBaseAbs, TimeSpan waitOnImprovementTimeout,
                                                                DealDirection side, Symbol symbol, bool isRiskRemovingOrder);

        BankPoolClientOrderRequestInfo CreateQuotedOrderRequest(decimal totalAmountBaseAbs, decimal requestedPrice,
                                                                DealDirection side,
                                                                PriceObject price, bool isRiskRemovingOrder);
        BankPoolClientOrder CreateClientOrder(BankPoolClientOrderRequestInfo orderRequestInfo);

        IntegratorRequestResult SubmitOrder(BankPoolClientOrder clientOrder);
        IntegratorRequestResult CancelOrder(string clientOrderId);
        IntegratorRequestResult CancelOrder(BankPoolClientOrder clientOrder);
    }

    public interface IHotspotTarget : IUnicastEventsAndForwardsTarget<HotspotClientOrderUpdateInfo>
    {
        IVenuePricesTopOfBookHub<HotspotCounterparty> PricesTopOfBook { get; }
        IVenueDealsHub<HotspotCounterparty> VenueDeals { get; }

        HotspotClientOrderRequestInfo CreateOrderReplaceRequest(HotspotClientOrder clientOrderToCancel,
                                                                       HotspotClientOrderRequestInfo
                                                                           replacementOrderInfo);

        HotspotClientOrderRequestInfo CreateOrderReplaceRequest(string clientOrderToCancelId,
                                                                       HotspotClientOrderRequestInfo
                                                                           replacementOrderInfo);

        HotspotClientOrderRequestInfo CreateLimitOrderRequest(decimal sizeBaseAbsInitial, decimal sizeBaseAbsFillMinimum,
                                                              decimal requestedPrice, DealDirection side, Symbol symbol,
                                                              TimeInForce timeInForce, HotspotCounterparty counterparty, bool isRiskRemovingOrder);

        HotspotClientOrderRequestInfo CreateDayPegOrderRequest(decimal sizeBaseAbsInitial,
                                                               decimal sizeBaseAbsFillMinimum, decimal limitPrice,
                                                               PegType pegType, decimal pegDifferenceBasePolAlwaysAdded,
                                                               DealDirection side, Symbol symbol,
                                                               HotspotCounterparty counterparty, bool isRiskRemovingOrder);

        HotspotClientOrder CreateClientOrder(HotspotClientOrderRequestInfo orderRequestInfo);

        //Temporarily removing this
        //bool CancelAllOrdersForAllClients();
        IntegratorRequestResult SubmitOrder(HotspotClientOrder clientOrder);
        [Obsolete("Use overload with client order object for faster server side processing",false)]
        IntegratorRequestResult CancelOrder(string clientOrderId);
        IntegratorRequestResult CancelOrder(HotspotClientOrder clientOrder);
        //IntegratorRequestResult FinalizeUnconfirmedDeal(DealConfirmationAction dealConfirmationAction,
        //                                                IntegratorUnconfirmedDealInternal integratorUnconfirmedDealInternal);
    }

    public interface IFXallTarget : IUnicastEventsAndForwardsTarget<FXallClientOrderUpdateInfo>
    {
        //IPricesIndividualHub PricesIndividual { get; }
        IVenuePricesTopOfBookHub<FXallCounterparty> PricesTopOfBook { get; }

        FXallClientOrderRequestInfo CreateOrderReplaceRequest(FXallClientOrder clientOrderToCancel,
                                                                       FXallClientOrderRequestInfo
                                                                           replacementOrderInfo);

        FXallClientOrderRequestInfo CreateOrderReplaceRequest(string clientOrderToCancelId,
                                                                       FXallClientOrderRequestInfo
                                                                           replacementOrderInfo);

        FXallClientOrderRequestInfo CreateLimitOrderRequest(decimal sizeBaseAbsInitial, decimal sizeBaseAbsFillMinimum,
                                                              decimal requestedPrice, DealDirection side, Symbol symbol,
                                                              TimeInForce timeInForce, bool isRiskRemovingOrder);

        FXallClientOrder CreateClientOrder(FXallClientOrderRequestInfo orderRequestInfo);

        IntegratorRequestResult SubmitOrder(FXallClientOrder clientOrder);
        [Obsolete("Use overload with client order object for faster server side processing", false)]
        IntegratorRequestResult CancelOrder(string clientOrderId);
        IntegratorRequestResult CancelOrder(FXallClientOrder clientOrder);
    }

    public interface IFXCMTarget : IUnicastEventsAndForwardsTarget<FXCMClientOrderUpdateInfo>
    {
        //IPricesIndividualHub PricesIndividual { get; }
        IVenuePricesTopOfBookHub<FXCMCounterparty> PricesTopOfBook { get; }

        FXCMClientOrderRequestInfo CreateOrderReplaceRequest(FXCMClientOrder clientOrderToCancel,
                                                                       FXCMClientOrderRequestInfo
                                                                           replacementOrderInfo);

        FXCMClientOrderRequestInfo CreateOrderReplaceRequest(string clientOrderToCancelId,
                                                                       FXCMClientOrderRequestInfo
                                                                           replacementOrderInfo);

        FXCMClientOrderRequestInfo CreateLimitOrderRequest(decimal sizeBaseAbsInitial, decimal sizeBaseAbsFillMinimum,
                                                              decimal requestedPrice, DealDirection side, Symbol symbol,
                                                              TimeInForce timeInForce, FXCMCounterparty fxcmCounterparty,
                                                              bool isRiskRemovingOrder);

        FXCMClientOrder CreateClientOrder(FXCMClientOrderRequestInfo orderRequestInfo);

        IntegratorRequestResult SubmitOrder(FXCMClientOrder clientOrder);
        [Obsolete("Use overload with client order object for faster server side processing", false)]
        IntegratorRequestResult CancelOrder(string clientOrderId);
        IntegratorRequestResult CancelOrder(FXCMClientOrder clientOrder);
    }

    public interface ILmaxTarget : IUnicastEventsAndForwardsTarget<LmaxClientOrderUpdateInfo>
    {
        IVenuePricesTopOfBookHub<LmaxCounterparty> PricesTopOfBook { get; }

        LmaxClientOrderRequestInfo CreateOrderReplaceRequest(LmaxClientOrder clientOrderToCancel,
                                                                       LmaxClientOrderRequestInfo
                                                                           replacementOrderInfo);

        LmaxClientOrderRequestInfo CreateOrderReplaceRequest(string clientOrderToCancelId,
                                                                       LmaxClientOrderRequestInfo
                                                                           replacementOrderInfo);

        [Obsolete("Use overload specifying the LMAX counterparty")]
        LmaxClientOrderRequestInfo CreateLimitOrderRequest(decimal sizeBaseAbsInitial,
                                                              decimal requestedPrice, DealDirection side, Symbol symbol,
                                                              TimeInForce timeInForce, bool isRiskRemovingOrder);

        LmaxClientOrderRequestInfo CreateLimitOrderRequest(decimal sizeBaseAbsInitial, decimal requestedPrice,
                                                           DealDirection side, Symbol symbol, TimeInForce timeInForce,
                                                           LmaxCounterparty lmaxCounterparty, bool isRiskRemovingOrder);

        LmaxClientOrder CreateClientOrder(LmaxClientOrderRequestInfo orderRequestInfo);

        IntegratorRequestResult SubmitOrder(LmaxClientOrder clientOrder);
        [Obsolete("Use overload with client order object for faster server side processing", false)]
        IntegratorRequestResult CancelOrder(string clientOrderId);
        IntegratorRequestResult CancelOrder(LmaxClientOrder clientOrder);
    }

    public interface IAllTargets
    {
        IntegratorRequestResult CancelAllMyOrders();
    }

    public interface IClientGatewayTargets
    {
        IBankPoolTarget BankPool { get; }
        IHotspotTarget Hotspot { get; }
        IFXallTarget FXall { get; }
        ILmaxTarget LMAX { get; }
        IFXCMTarget FXCMTaking { get; }
        IFXCMStreamingTarget FXCMStreaming { get; }
        IAllTargets All { get; }
    }

    public interface IConnectionInfo
    {
        ConnectionChainBuildVersioningInfo ConnectionChainBuildVersioningInfo { get; }
        event Action<ConnectionStateChangeEventArgs> ConnectionStateChanged;
        ConnectionServiceState CurrentBussinessLogicConnectionServiceState { get; }
        ConnectionServiceState CurrentOffloadedMarketDataConnectionServiceState { get; }
        event GatewayClosedHandler GatewayClosed;
        bool IsClosed { get; }
    }

    public interface IClientGateway
    {
        ITradingInfo TradingInfo { get; }
        IClientGatewayTargets Targets { get; }
        string ClientIdentity { get; }
        IConnectionInfo ConnectionInfo { get; }
    }
}
