﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public interface IIntegratorClient
    {
        void Configure(ILogger logger, IClientGateway clientGateway, string configurationString);
        void Start();
        void Stop(string reason);
        bool TryHandleUnhandledException(Exception e);
    }
}
