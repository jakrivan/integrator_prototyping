﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public static class IntegratorStateInitializer
    {
        private static int _initializationCount;

        public static IntegratorProcessType IntegratorProcessType { get; private set; }

        public static void Initialize(IntegratorProcessType processType)
        {
            if (Interlocked.Increment(ref _initializationCount) > 1)
                throw new Exception("Integrator state initialization called more than once");

            IntegratorProcessType = processType;

            IntegratorPerformanceCounter.InitializeInstance(processType);
            switch (processType)
            {
                case IntegratorProcessType.BusinessLogic:
                    //Price book has value for each symbol, counterparty and side + 20% buffer
                    PriceObjectPool.InitializeInstance((int)(Counterparty.ValuesCount * Symbol.ValuesCount * 2 * 1.2));
                    break;
                case IntegratorProcessType.HotspotMarketData:
                    PriceObjectPool.InitializeInstance(4000);
                    break;
                case IntegratorProcessType.DataCollection:
                default:
                    //no need for price objects pool
                    break;
            }
        }
    }
}
