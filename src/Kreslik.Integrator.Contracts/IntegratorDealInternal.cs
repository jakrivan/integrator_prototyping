﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    public enum MarketOnImprovementResult
    {
        ExecutedImmediatelyOnExistingImprovement,
        ExecutedOnLaterImprovement,
        ExecutedOnTimeout
    }


    [DataContract]
    public class IntegratorDealInternal : IntegratorClientOrderInfoObjectBase
    {
        public IntegratorDealInternal(string clientOrderIdentity, string parentOrderIdentity, string clientIdentity,
                                      string integratorOrderIdentity, string counterpartyTransactionIdentity,
                                      string internalTransactionIdentity, decimal filledAmountBaseAbs, bool isTerminalFill,
                                      decimal usedPrice, DealDirection direction, Symbol symbol,
                                      DateTime integratorSentTimeUtc, DateTime integratorExecutionTimeUtc,
                                      DateTime counterpartyExecutionTimeUtc, DateTime? counterpartyTransactionTimeUtc,
                                      DateTime settlementDateLocal, DateTime settlementDateTimeCounterpartyUtc,
                                      DateTime settlementDateTimeCentralBankUtc,
                                      Counterparty counterparty, bool? isPrimaryDealFromQuoted, FlowSide? flowSide,
                                      TradingTargetType senderTradingTargetType,
                                      MarketOnImprovementResult? marketOnImprovementResult,
                                      IntegratedTradingSystemIdentification integratedTradingSystemIdentification,
                                      string counterpartyClientId)
            : base(senderTradingTargetType, clientIdentity, clientOrderIdentity, parentOrderIdentity)
        {
            this.IntegratorOrderIdentity = integratorOrderIdentity;
            this.CounterpartyTransactionIdentity = counterpartyTransactionIdentity;
            this.InternalTransactionIdentity = internalTransactionIdentity;
            this.FilledAmountBaseAbs = filledAmountBaseAbs;
            this.IsTerminalFill = isTerminalFill;
            this.Price = usedPrice;
            this.Direction = direction;
            this.Symbol = symbol;
            this.IntegratorSentOriginatingOrderTimeUtc = integratorSentTimeUtc;
            this.IntegratorReceivedExecutionTimeUtc = integratorExecutionTimeUtc;
            this.CounterpartySentExecutionTimeUtc = counterpartyExecutionTimeUtc;
            this.CounterpartyTransactionTimeUtc = counterpartyTransactionTimeUtc;
            this.SettlementDateLocal = settlementDateLocal;
            this.SettlementDateTimeCounterpartyUtc = settlementDateTimeCounterpartyUtc;
            this.SettlementDateTimeCentralBankUtc = settlementDateTimeCentralBankUtc;
            this.Counterparty = counterparty;
            this.IsPrimaryDealFromQuoted = isPrimaryDealFromQuoted;
            this.FlowSide = flowSide;
            this.MarketOnImprovementResult = marketOnImprovementResult;
            this.IntegratedTradingSystemIdentification = integratedTradingSystemIdentification;
            this.CounterpartyClientId = counterpartyClientId;
        }

        [DataMember]
        public string IntegratorOrderIdentity { get; private set; }

        [DataMember]
        public string CounterpartyTransactionIdentity { get; private set; }

        [DataMember]
        public string InternalTransactionIdentity { get; private set; }

        [DataMember]
        public decimal FilledAmountBaseAbs { get; private set; }

        public decimal FilledAmountBasePol
        {
            get { return Direction == DealDirection.Buy ? FilledAmountBaseAbs : -FilledAmountBaseAbs; }
        }

        public decimal AcquiredAmountTermPol
        {
            get { return -FilledAmountBasePol * Price; }
        }

        [DataMember]
        public bool IsTerminalFill { get; private set; }

        [DataMember]
        public decimal Price { get; private set; }

        [DataMember]
        public DealDirection Direction { get; private set; }

        public Symbol Symbol { get; private set; }

        [DataMember]
        //this member is needed so that we can (de)serialize Symbol while still using the same instance of this type
        private int SymbolSerialized
        {
            get { return (int)this.Symbol; }
            set { this.Symbol = (Symbol)value; }
        }

        [DataMember]
        public DateTime IntegratorSentOriginatingOrderTimeUtc { get; private set; }

        [DataMember]
        public DateTime IntegratorReceivedExecutionTimeUtc { get; private set; }

        [DataMember]
        public DateTime CounterpartySentExecutionTimeUtc { get; private set; }

        [DataMember]
        public DateTime? CounterpartyTransactionTimeUtc { get; private set; }

        [DataMember]
        public DateTime SettlementDateTimeCounterpartyUtc { get; private set; }

        [DataMember]
        public DateTime SettlementDateTimeCentralBankUtc { get; private set; }

        [DataMember]
        public DateTime SettlementDateLocal { get; private set; }

        public Counterparty Counterparty { get; private set; }

        [DataMember]
        private int CounterpartySerialized
        {
            get { return (int) this.Counterparty; }
            set { this.Counterparty = (Counterparty)value; }
        }

        [DataMember]
        public bool? IsPrimaryDealFromQuoted { get; private set; }

        [DataMember]
        public MarketOnImprovementResult? MarketOnImprovementResult { get; private set; }

        [DataMember]
        public FlowSide? FlowSide { get; private set; }

        [DataMember]
        public IntegratedTradingSystemIdentification IntegratedTradingSystemIdentification { get; private set; }

        [DataMember]
        public string CounterpartyClientId { get; private set; }

        public override string ToString()
        {
            return
                string.Format(
                    "{12} Deal internal for order [{0}] (executed at: {9} sent at {1:HH:mm:ss.fffffff UTC}, received at {2:HH:mm:ss.fffffff UTC}, delivery delay: {3}) {10} {4} {5} (BaseAbs) of {6} at {7} from {8}. IntegratedSystem: {11}, CptClientId: {13}",
                    ClientOrderIdentity, CounterpartySentExecutionTimeUtc, IntegratorReceivedExecutionTimeUtc,
                    (IntegratorReceivedExecutionTimeUtc - CounterpartySentExecutionTimeUtc), Direction,
                    FilledAmountBaseAbs, Symbol, Price, Counterparty,
                    CounterpartyTransactionTimeUtc == null
                        ? "<Not Specified>"
                        : CounterpartyTransactionTimeUtc.Value.ToString("HH:mm:ss.fffffff UTC"),
                        FlowSide, 
                        this.IntegratedTradingSystemIdentification == null ? "<NOT SPECIFIED>" : this.IntegratedTradingSystemIdentification.ToString(),
                        this.IsTerminalFill ? "Final" : "Intermediary",
                        this.CounterpartyClientId);
        }
    }

    [DataContract]
    public class IntegratorUnconfirmedDealInternal: IntegratorDealInternal
    {
        public IntegratorUnconfirmedDealInternal(string clientOrderIdentity, string parentOrderIdentity,
            string clientIdentity, string integratorOrderIdentity, string counterpartyTransactionIdentity,
            string internalTransactionIdentity, decimal filledAmountBaseAbs, decimal usedPrice,
            DealDirection direction, Symbol symbol, DateTime integratorSentTimeUtc, DateTime integratorExecutionTimeUtc,
            DateTime counterpartyExecutionTimeUtc, DateTime? counterpartyTransactionTimeUtc,
            DateTime settlementDateLocal, DateTime settlementDateTimeCounterpartyUtc, DateTime settlementDateTimeCentralBankUtc,
            Counterparty counterparty,
            bool? isPrimaryDealFromQuoted, FlowSide? flowSide, TradingTargetType senderTradingTargetType,
            MarketOnImprovementResult? marketOnImprovementResult,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification,
            OrderChangeEventArgs orderChangeEventArgs)
            : base(
                clientOrderIdentity, parentOrderIdentity, clientIdentity, integratorOrderIdentity,
                counterpartyTransactionIdentity, internalTransactionIdentity, filledAmountBaseAbs, false, usedPrice,
                direction, symbol, integratorSentTimeUtc, integratorExecutionTimeUtc,
                counterpartyExecutionTimeUtc, counterpartyTransactionTimeUtc,
                settlementDateLocal, settlementDateTimeCounterpartyUtc, settlementDateTimeCentralBankUtc, 
                counterparty, isPrimaryDealFromQuoted,
                flowSide, senderTradingTargetType, marketOnImprovementResult,
                integratedTradingSystemIdentification, orderChangeEventArgs.ExecutionInfo.CounterpartyClientId)
        {
            this.OrderChangeEventArgsInternal = orderChangeEventArgs;
        }

        //This is not to be send via remoting! Only for internal usage
        // It will be casted to OrderChangeEventArgsEx
        public OrderChangeEventArgs OrderChangeEventArgsInternal { get; private set; }
    }

    [DataContract]
    public class IntegratorGliderDealInternal : IntegratorDealInternal
    {
        public static IntegratorDealInternal CreateIntegratorGliderDealInternal(string clientOrderIdentity,
            string parentOrderIdentity,
            string clientIdentity, string integratorOrderIdentity, string counterpartyTransactionIdentity,
            string internalTransactionIdentity, decimal filledAmountBaseAbs, bool isTerminalFill, decimal usedPrice,
            DealDirection direction, Symbol symbol, DateTime integratorSentTimeUtc, DateTime integratorExecutionTimeUtc,
            DateTime counterpartyExecutionTimeUtc, DateTime? counterpartyTransactionTimeUtc,
            DateTime settlementDateLocal, DateTime settlementDateTimeCounterpartyUtc, DateTime settlementDateTimeCentralBankUtc,
            Counterparty counterparty,
            bool? isPrimaryDealFromQuoted, FlowSide? flowSide, TradingTargetType senderTradingTargetType,
            MarketOnImprovementResult? marketOnImprovementResult,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification, string counterpartyClientId,
            GlidingInfoBag glidingInfoBag)
        {
            if (glidingInfoBag == null)
            {
                return new IntegratorDealInternal(clientOrderIdentity, parentOrderIdentity, clientIdentity,
                    integratorOrderIdentity,
                    counterpartyTransactionIdentity, internalTransactionIdentity, filledAmountBaseAbs, isTerminalFill,
                    usedPrice, direction, symbol, integratorSentTimeUtc, integratorExecutionTimeUtc,
                    counterpartyExecutionTimeUtc, counterpartyTransactionTimeUtc,
                    settlementDateLocal, settlementDateTimeCounterpartyUtc, settlementDateTimeCentralBankUtc, 
                    counterparty, isPrimaryDealFromQuoted,
                    flowSide, senderTradingTargetType, marketOnImprovementResult, integratedTradingSystemIdentification,
                    counterpartyClientId);
            }
            else
            {
                return new IntegratorGliderDealInternal(clientOrderIdentity, parentOrderIdentity, clientIdentity,
                    integratorOrderIdentity,
                    counterpartyTransactionIdentity, internalTransactionIdentity, filledAmountBaseAbs, isTerminalFill,
                    usedPrice, direction, symbol, integratorSentTimeUtc, integratorExecutionTimeUtc,
                    counterpartyExecutionTimeUtc, counterpartyTransactionTimeUtc,
                    settlementDateLocal, settlementDateTimeCounterpartyUtc, settlementDateTimeCentralBankUtc,
                    counterparty, isPrimaryDealFromQuoted,
                    flowSide, senderTradingTargetType, marketOnImprovementResult, integratedTradingSystemIdentification,
                    counterpartyClientId, glidingInfoBag);
            }
        }

        private IntegratorGliderDealInternal(string clientOrderIdentity, string parentOrderIdentity,
            string clientIdentity, string integratorOrderIdentity, string counterpartyTransactionIdentity,
            string internalTransactionIdentity, decimal filledAmountBaseAbs, bool isTerminalFill, decimal usedPrice,
            DealDirection direction, Symbol symbol, DateTime integratorSentTimeUtc, DateTime integratorExecutionTimeUtc,
            DateTime counterpartyExecutionTimeUtc, DateTime? counterpartyTransactionTimeUtc,
            DateTime settlementDateLocal, DateTime settlementDateTimeCounterpartyUtc, DateTime settlementDateTimeCentralBankUtc,
            Counterparty counterparty,
            bool? isPrimaryDealFromQuoted, FlowSide? flowSide, TradingTargetType senderTradingTargetType,
            MarketOnImprovementResult? marketOnImprovementResult,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification, string counterpartyClientId,
            GlidingInfoBag glidingInfoBag)
            : base(
                clientOrderIdentity, parentOrderIdentity, clientIdentity, integratorOrderIdentity,
                counterpartyTransactionIdentity, internalTransactionIdentity, filledAmountBaseAbs, isTerminalFill,
                usedPrice, direction, symbol, integratorSentTimeUtc, integratorExecutionTimeUtc,
                counterpartyExecutionTimeUtc, counterpartyTransactionTimeUtc, 
                settlementDateLocal, settlementDateTimeCounterpartyUtc, settlementDateTimeCentralBankUtc,
                counterparty, isPrimaryDealFromQuoted,
                flowSide, senderTradingTargetType, marketOnImprovementResult, integratedTradingSystemIdentification,
                counterpartyClientId)
        {
            this.GlidingInfoBag = glidingInfoBag;
        }

        public GlidingInfoBag GlidingInfoBag { get; private set; }
    }
}
