﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    [DataContract]
    [KnownType(typeof(IntegratorDealInternal))]
    [KnownType(typeof(IntegratorGliderDealInternal))]
    [KnownType(typeof(IntegratorUnconfirmedDealInternal))]
    [KnownType(typeof(HotspotClientOrderUpdateInfo))]
    [KnownType(typeof(BankPoolClientOrderUpdateInfo))]
    [KnownType(typeof(FXallClientOrderUpdateInfo))]
    [KnownType(typeof(LmaxClientOrderUpdateInfo))]
    [KnownType(typeof(GliderClientOrderUpdateInfo))]
    [KnownType(typeof(CounterpartyRejectionInfo))]
    [KnownType(typeof(CounterpartyOrderIgnoringInfo))]
    [KnownType(typeof(IntegratorMulticastInfo))]
    [KnownType(typeof(RejectableMMDeal))]
    [KnownType(typeof(ImportantIntegratorInformation))]
    public abstract class IntegratorInfoObjectBase { }

    [DataContract]
    public abstract class IntegratorTradingInfoObjectBase : IntegratorInfoObjectBase
    {
        protected IntegratorTradingInfoObjectBase(TradingTargetType senderTradingTargetType, string clientIdentity)
        {
            this.SenderTradingTargetType = senderTradingTargetType;
            this.ClientIdentity = clientIdentity;
        }

        [DataMember]
        public TradingTargetType SenderTradingTargetType { get; private set; }
        [DataMember]
        public string ClientIdentity { get; private set; }
    }

    [DataContract]
    public abstract class IntegratorClientOrderInfoObjectBase: IntegratorTradingInfoObjectBase
    {
        protected IntegratorClientOrderInfoObjectBase(TradingTargetType tradingTargetType, string clientIdentity, string clientOrderIdentity, string parentClientOrderIdentity)
            :base(tradingTargetType, clientIdentity)
        {
            this.ClientOrderIdentity = clientOrderIdentity;
            this.ParentClientOrderIdentity = parentClientOrderIdentity;
        }

        [DataMember]
        public string ClientOrderIdentity { get; private set; }
        [DataMember]
        public string ParentClientOrderIdentity { get; private set; }

        //TODO: actual orders objects
    }
}
