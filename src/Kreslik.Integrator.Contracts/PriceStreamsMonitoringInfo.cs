﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    

    [DataContract]
    public class PriceStreamsMonitoringInfo
    {
        public PriceStreamsMonitoringInfo(List<PriceStreamInfoEntry> priceStreamInfoEntries,
                                          List<ToBPriceStreamInfoEntry> topOfBookInfoEntries,
                                          List<SymbolTrustworthinessInfo> symbolTrustworthinessInfo,
                                          List<SessionStatusInfo> marketDataSessionsStatusInfos,
                                          List<SessionStatusInfo> orderFlowSessionsStatusInfos,
                                          List<StpFlowSessionInfo> stpFlowSessionInfos)
        {
            this.PriceStreamInfoEntries = priceStreamInfoEntries;
            this.TopOfBookInfoEntries = topOfBookInfoEntries;
            this.SymbolTrustworthinessInfo = symbolTrustworthinessInfo;
            this.MarketDataSessionsStatusInfos = marketDataSessionsStatusInfos;
            this.OrderFlowSessionsStatusInfos = orderFlowSessionsStatusInfos;
            this.StpFlowSessionInfos = stpFlowSessionInfos;
        }

        [DataMember]
        public List<PriceStreamInfoEntry> PriceStreamInfoEntries { get; private set; }

        [DataMember]
        public List<ToBPriceStreamInfoEntry> TopOfBookInfoEntries { get; private set; }

        [DataMember]
        public List<SymbolTrustworthinessInfo> SymbolTrustworthinessInfo { get; private set; }

        [DataMember]
        public List<SessionStatusInfo> MarketDataSessionsStatusInfos { get; private set; }

        [DataMember]
        public List<SessionStatusInfo> OrderFlowSessionsStatusInfos { get; private set; }

        [DataMember]
        public List<StpFlowSessionInfo> StpFlowSessionInfos { get; private set; }

        public bool IsCounterpartyOffline(Counterparty counterparty)
        {
            int ctpIdx = (int) counterparty;
            return !this.MarketDataSessionsStatusInfos[ctpIdx].Configured ||
                   this.MarketDataSessionsStatusInfos[ctpIdx].Inactivated ||
                   this.MarketDataSessionsStatusInfos[ctpIdx].SessionState != SessionState.Running ||
                   !this.OrderFlowSessionsStatusInfos[ctpIdx].Configured ||
                   this.OrderFlowSessionsStatusInfos[ctpIdx].Inactivated ||
                   this.OrderFlowSessionsStatusInfos[ctpIdx].SessionState != SessionState.Running;
        }

        public PriceStreamsMonitoringInfo MergeMonitoringInfo(PriceStreamsMonitoringInfo priceStreamsMonitoringInfo)
        {
            if (this.PriceStreamInfoEntries != null)
            {
                if (priceStreamsMonitoringInfo.PriceStreamInfoEntries != null)
                {
                    //this.PriceStreamInfoEntries.AddRange(priceStreamsMonitoringInfo.PriceStreamInfoEntries);
                    for (int idx = 0; idx < this.PriceStreamInfoEntries.Count; idx++)
                    {
                        if (!this.PriceStreamInfoEntries[idx].SpreadBp.HasValue && priceStreamsMonitoringInfo.PriceStreamInfoEntries[idx].SpreadBp.HasValue)
                            this.PriceStreamInfoEntries[idx] = priceStreamsMonitoringInfo.PriceStreamInfoEntries[idx];
                    }
                }
            }
            else
            {
                this.PriceStreamInfoEntries = priceStreamsMonitoringInfo.PriceStreamInfoEntries;
            }

            this.TopOfBookInfoEntries = this.TopOfBookInfoEntries ?? priceStreamsMonitoringInfo.TopOfBookInfoEntries;

            this.SymbolTrustworthinessInfo = this.SymbolTrustworthinessInfo ??
                                             priceStreamsMonitoringInfo.SymbolTrustworthinessInfo;

            this.MarketDataSessionsStatusInfos = MergeStatusInfoLists(
                this.MarketDataSessionsStatusInfos, priceStreamsMonitoringInfo.MarketDataSessionsStatusInfos);

            this.OrderFlowSessionsStatusInfos = MergeStatusInfoLists(
                this.OrderFlowSessionsStatusInfos, priceStreamsMonitoringInfo.OrderFlowSessionsStatusInfos);

            this.StpFlowSessionInfos = this.StpFlowSessionInfos ?? priceStreamsMonitoringInfo.StpFlowSessionInfos;

            return this;
        }

        private static List<SessionStatusInfo> MergeStatusInfoLists(List<SessionStatusInfo> statuses1,
                                                                    List<SessionStatusInfo> statuses2)
        {
            if (statuses1 == null)
                return statuses2;
            if (statuses2 == null)
                return statuses1;

            for (int idx = 0; idx < statuses1.Count; idx++)
            {
                if (!statuses1[idx].Configured)
                    statuses1[idx] = statuses2[idx];
            }

            return statuses1;
        }
    }
}
