﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public enum SubscriptionType
    {
        ToBPricesSubscription,
        VenueDealsSubscriptions,
        IndividualPricesSubscription
    }

    [DataContract]
    //Purpose of this translating type is to prevent bloating attribute list of [KnownType] attributes
    // on  IntegratorRequestInfo - as it needs to know all subtypes
    public sealed class IntegratorTransferableSubscriptionRequest : IntegratorRequestInfo
    {
        public Symbol Symbol { get; private set; }

        [DataMember]
        //this member is needed so that we can (de)serialize Symbol while still using the same instance of this type
        private int SymbolSerialized
        {
            get { return (int)this.Symbol; }
            set { this.Symbol = (Symbol)value; }
        }

        public Counterparty Counterparty { get; private set; }

        [DataMember]
        //this member is needed so that we can (de)serialize Counterparty while still using the same instance of this type
        private int? CounterpartySerialized
        {
            get { return this.Counterparty == Counterparty.NULL ? (int?)null : (int)this.Counterparty; }
            set { this.Counterparty = value == null ? Counterparty.NULL : (Counterparty)value; }
        }

        [DataMember]
        //true to subscribe, false to unsubscribe
        public bool Subscribe { get; private set; }

        [DataMember]
        public SubscriptionType SubscriptionType { get; private set; }

        public IntegratorTransferableSubscriptionRequest(Symbol symbol, Counterparty counterparty, bool subscribe,
                                                          SubscriptionType subscriptionType)
        {
            this.Symbol = symbol;
            this.Counterparty = counterparty;
            this.Subscribe = subscribe;
            this.SubscriptionType = subscriptionType;
        }

        public IntegratorTransferableSubscriptionRequest(IntegratorSubscriptionRequestBase localSubscriptionRequest)
            : this(
                localSubscriptionRequest.Symbol, localSubscriptionRequest.Counterparty,
                localSubscriptionRequest.Subscribe, localSubscriptionRequest.SubscriptionType)
        { }

        public IntegratorTransferableSubscriptionRequest CloneAsSubscriptionRequest()
        {
            return new IntegratorTransferableSubscriptionRequest(this.Symbol, this.Counterparty, true, this.SubscriptionType);
        }

        public IntegratorTransferableSubscriptionRequest CloneAsUnsubscriptionRequest()
        {
            return new IntegratorTransferableSubscriptionRequest(this.Symbol, this.Counterparty, false, this.SubscriptionType);
        }

        public override string ToString()
        {
            return string.Format("{0}Subscription [{1}]: {2} {3}", this.Subscribe ? string.Empty : "Un", this.SubscriptionType, this.Symbol,
                                 this.Counterparty.ToString());
        }
    }

    public abstract class IntegratorSubscriptionRequestBase
    {
        public Symbol Symbol { get; private set; }
        public Counterparty Counterparty { get; private set; }
        //true to subscribe, false to unsubscribe
        public bool Subscribe { get; private set; }
        public SubscriptionType SubscriptionType { get; private set; }

        protected IntegratorSubscriptionRequestBase(Symbol symbol, bool subscribe, SubscriptionType subscriptionType)
            : this(symbol, null, subscribe, subscriptionType)
        { }

        protected IntegratorSubscriptionRequestBase(Symbol symbol, ICounterparty counterparty, bool subscribe, SubscriptionType subscriptionType)
        {
            Counterparty cpt = Counterparty.UnboxInterfaceToCounterparty(counterparty);
            this.VerifyType(cpt, this.GetType());

            this.Symbol = symbol;
            this.Counterparty = cpt;
            this.Subscribe = subscribe;
            this.SubscriptionType = subscriptionType;
        }

        private void VerifyType(Counterparty counterparty, Type subscriptionType)
        {
            if (subscriptionType.GenericTypeArguments.Length != 1)
            {
                throw new Exception("Unexpected subscription type - expected one generic parameter");
            }

            Type genericType = subscriptionType.GenericTypeArguments[0];

            if (counterparty == Counterparty.NULL)
            {
                if (!typeof(IBankCounterparty).IsAssignableFrom(genericType))
                    throw new Exception("Unexpected subscription type - only BankPool subscriptions are allowed to not specify counterparty");
            }
            else
            {
                if (typeof(IBankCounterparty).IsAssignableFrom(genericType))
                    throw new Exception("Unexpected subscription type - BankPool subscriptions cannot specify counterparty");

                if (!Counterparty.BoxCounterpartyToInterface(counterparty).GetType().IsAssignableFrom(genericType))
                    throw new Exception(
                        string.Format(
                            "Unexpected subscription type - counterparty type - {0} does not match subscription generic type - {1}",
                            counterparty.GetType(), genericType));
            }
        }

        public override string ToString()
        {
            return string.Format("Subscription: {0} {1}", this.Symbol,
                                 this.Counterparty.ToString());
        }
    }

    public class TopOfBookIntegratorSubscriptionRequest<T> : IntegratorSubscriptionRequestBase where T : ICounterparty
    {
        internal TopOfBookIntegratorSubscriptionRequest(Symbol symbol, T counterparty)
            : base(symbol, counterparty, true, SubscriptionType.ToBPricesSubscription)
        { }

        internal TopOfBookIntegratorSubscriptionRequest(Symbol symbol)
            : base(symbol, true, SubscriptionType.ToBPricesSubscription)
        { }
    }

    public class TopOfBookIntegratorUnsubscriptionRequest<T> : IntegratorSubscriptionRequestBase where T : ICounterparty
    {
        internal TopOfBookIntegratorUnsubscriptionRequest(Symbol symbol, T counterparty)
            : base(symbol, counterparty, false, SubscriptionType.ToBPricesSubscription)
        { }

        internal TopOfBookIntegratorUnsubscriptionRequest(Symbol symbol)
            : base(symbol, false, SubscriptionType.ToBPricesSubscription)
        { }
    }

    public class IndividualPricesIntegratorSubscriptionRequest<T> : IntegratorSubscriptionRequestBase where T : ICounterparty
    {
        internal IndividualPricesIntegratorSubscriptionRequest(Symbol symbol, T counterparty)
            : base(symbol, counterparty, true, SubscriptionType.IndividualPricesSubscription)
        { }

        internal IndividualPricesIntegratorSubscriptionRequest(Symbol symbol)
            : base(symbol, true, SubscriptionType.IndividualPricesSubscription)
        { }
    }

    public class VenueDealsIntegratorUnsubscriptionRequest<T> : IntegratorSubscriptionRequestBase where T : ICounterparty
    {
        internal VenueDealsIntegratorUnsubscriptionRequest(Symbol symbol, T counterparty)
            : base(symbol, counterparty, false, SubscriptionType.VenueDealsSubscriptions)
        { }

        internal VenueDealsIntegratorUnsubscriptionRequest(Symbol symbol)
            : base(symbol, false, SubscriptionType.VenueDealsSubscriptions)
        { }
    }

    public class VenueDealsIntegratorSubscriptionRequest<T> : IntegratorSubscriptionRequestBase where T : ICounterparty
    {
        internal VenueDealsIntegratorSubscriptionRequest(Symbol symbol, T counterparty)
            : base(symbol, counterparty, true, SubscriptionType.VenueDealsSubscriptions)
        { }

        internal VenueDealsIntegratorSubscriptionRequest(Symbol symbol)
            : base(symbol, true, SubscriptionType.VenueDealsSubscriptions)
        { }
    }

    public class IndividualPricesIntegratorUnsubscriptionRequest<T> : IntegratorSubscriptionRequestBase where T : ICounterparty
    {
        internal IndividualPricesIntegratorUnsubscriptionRequest(Symbol symbol, T counterparty)
            : base(symbol, counterparty, false, SubscriptionType.IndividualPricesSubscription)
        { }

        internal IndividualPricesIntegratorUnsubscriptionRequest(Symbol symbol)
            : base(symbol, false, SubscriptionType.IndividualPricesSubscription)
        { }
    }
}
