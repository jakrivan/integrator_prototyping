﻿namespace Kreslik.Integrator.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class ClientGatewayException : Exception
    {
        public ClientGatewayException()
        {
        }

        public ClientGatewayException(string message) : base(message)
        {
        }

        public ClientGatewayException(string message, Exception inner) : base(message, inner)
        {
        }

        protected ClientGatewayException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
