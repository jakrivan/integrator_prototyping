﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts
{
    //Cannot be interface as volatile would not be preserved through properties
    public class CancelReplaceOrdersHolder
    {
        public volatile VenueClientOrder OrderCurrent;
        public volatile VenueClientOrder OrderNext;
    }

    //public interface IClientDealRequest
    //{
    //    string Identity { get; }
    //    decimal SizeBaseAbsInitial { get; }
    //    decimal CounterpartyRequestedPrice { get; }
    //    DealDirection IntegratorDealDirection { get; }
    //    Common.Symbol Symbol { get; }
    //}

    //To be used only by the glider system.
    // Must be public and here due to WCF contracts (so that it can be in KnownTypes attribute)
    public class GlidingInfoBag : CancelReplaceOrdersHolder
    {
        public GlidingInfoBag(IClientDealRequest dealRequest, decimal initialGlidingSizeBaseAbs, decimal worstAcceptableFillPrice, DateTime cutoffTime)
        {
            this.ClientDealRequest = dealRequest;
            this.InitialGlidingSizeBaseAbs = initialGlidingSizeBaseAbs;
            this.WorstAcceptableFillPrice = worstAcceptableFillPrice;
            this.CutoffTime = cutoffTime;
        }

        public IClientDealRequest ClientDealRequest;
        public AtomicSize TotalInTimeFilledSizeBasePol;
        public AtomicSize TotalInTimeFilledSizeTermPol;
        public AtomicSize TotalLateFilledSizeBasePol;
        public AtomicSize TotalLateFilledSizeTermPol;
        public decimal InitialGlidingSizeBaseAbs;
        public decimal LastGlidePrice;
        public int StepsEdgesRemaining;
        public decimal WorstAcceptableFillPrice;
        public DateTime CutoffTime;
        public IAccurateTimer Timer;

        public int FinalCancellationAttemptsCnt = 0;

        public AtomicSize TotalFilledSizeBasePol { get { return TotalInTimeFilledSizeBasePol + TotalLateFilledSizeBasePol; } }

        public const int STATE_GLIDING = 0;
        public const int STATE_DECISIONING = 1;
        public const int STATE_WAITINGFORFINALGLIDE = 2;
        public const int STATE_FLATTENING = 3;
        public const int STATE_CLOSED = 4;

        public int CurrentState = STATE_GLIDING;


        private object _dealsLocker = new object();

        public enum AddingDealResult
        {
            None,
            Unexpected,
            GlideCanBeClosed
        }

        public AddingDealResult AddGliderDeal(IntegratorGliderDealInternal gliderDeal, ILogger logger)
        {
            lock (_dealsLocker)
            {
                if (this.CurrentState >= STATE_CLOSED)
                {
                    logger.Log(LogLevel.Fatal, "Glide {0} experienced unexpected deal for glide being in Closed({1}) status. {2}. This is unexpected - position MIGHT NEED TO BE FLATTED MANUALLY",
                    this.ClientDealRequest.Identity, this.CurrentState, gliderDeal);
                    return AddingDealResult.Unexpected;
                }

                AtomicSize totalFilledSizeLocal;
                if (this.CurrentState > STATE_GLIDING)
                {
                    totalFilledSizeLocal = this.TotalLateFilledSizeBasePol.InterlockedAdd(gliderDeal.FilledAmountBasePol)
                        + TotalInTimeFilledSizeBasePol;
                    TotalLateFilledSizeTermPol.InterlockedAdd(gliderDeal.AcquiredAmountTermPol);
                }
                else
                {
                    TotalInTimeFilledSizeTermPol.InterlockedAdd(gliderDeal.AcquiredAmountTermPol);
                    totalFilledSizeLocal = TotalInTimeFilledSizeBasePol.InterlockedAdd(gliderDeal.FilledAmountBasePol);
                }


                if (
                    Math.Abs(totalFilledSizeLocal.ToDecimal())
                    >
                    gliderDeal.GlidingInfoBag.InitialGlidingSizeBaseAbs)
                {
                    logger.Log(LogLevel.Fatal,
                        "Glide {0} Receiving fill [{1} pol] that causes the current system to be filled for more [{2} pol] than originally requested [{3} abs]. Stopping gliding and hard blocking system",
                        this.ClientDealRequest.Identity, gliderDeal.FilledAmountBasePol,
                        gliderDeal.GlidingInfoBag.TotalInTimeFilledSizeBasePol,
                        gliderDeal.GlidingInfoBag.InitialGlidingSizeBaseAbs);
                    //no need to flatten this now - since the closing decision and cover logic will take care about this
                    return AddingDealResult.Unexpected;
                }

                if (totalFilledSizeLocal == AtomicSize.ZERO && this.CurrentState > STATE_WAITINGFORFINALGLIDE)
                {
                    return AddingDealResult.GlideCanBeClosed;
                }

                return AddingDealResult.None;
            }
        }

        //after this point it is not possible to increase TotalInTimeFilledSizeBasePol
        public bool StopGliding()
        {
            //null check is redundant
            HighResolutionDateTime.HighResolutionTimerManager.CancelTimer(ref this.Timer);
            //after return timer is null

            return InterlockedEx.CheckAndFlipState(ref this.CurrentState, STATE_DECISIONING, STATE_GLIDING);
        }

        public bool CloseCompletely()
        {
            return InterlockedEx.CheckAndFlipState(ref this.CurrentState, STATE_CLOSED, STATE_FLATTENING);
        }

        public void PerformActionOnActiveGlider(Action<GlidingInfoBag> action)
        {
            action(this);
        }

        ~GlidingInfoBag()
        {
            if (this.Timer != null)
            {
                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Detected leak of timer in GlidingInfoBag. Releasing now. Situation should be reviewed to prevent possible performance issues. {0}", this.ClientDealRequest);
                HighResolutionDateTime.HighResolutionTimerManager.CancelTimer(ref this.Timer);
            }
        }
    }
}
