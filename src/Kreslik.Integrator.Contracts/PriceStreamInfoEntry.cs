﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    [DataContract]
    public abstract class PriceStreamInfoEntryBase
    {
        public Symbol Symbol { get; protected set; }

        [DataMember]
        protected int SymbolSerialized
        {
            get { return (int) this.Symbol; }
            set { this.Symbol = (Symbol)value; }
        }

        [DataMember]
        public TimeSpan LastAskAge { get; protected set; }
        [DataMember]
        public TimeSpan LastBidAge { get; protected set; }
        [DataMember]
        public decimal? SpreadBp { get; protected set; }

        public void ResetSpreadBp()
        {
            this.SpreadBp = null;
        }
    }

    [DataContract]
    [KnownType(typeof(WritablePriceStreamInfoEntry))]
    public abstract class PriceStreamInfoEntry : PriceStreamInfoEntryBase
    {
        public Counterparty Counterparty { get; protected set; }

        [DataMember]
        protected int CounterpartSerialized
        {
            get { return (int) this.Counterparty; }
            set { this.Counterparty = (Counterparty)value; }
        }
    }

    [DataContract]
    public class WritablePriceStreamInfoEntry : PriceStreamInfoEntry
    {
        public WritablePriceStreamInfoEntry(Symbol symbol, Counterparty counterparty)
        {
            this.Symbol = symbol;
            this.Counterparty = counterparty;
        }

        public void SetLastAskAge(TimeSpan lastAskAge)
        {
            this.LastAskAge = lastAskAge;
        }

        public void SetLastBidAge(TimeSpan lastBidAge)
        {
            this.LastBidAge = lastBidAge;
        }

        public void SetSpreadBp(decimal spreadBp)
        {
            this.SpreadBp = spreadBp;
        }

        public void ResetEntry()
        {
            this.LastAskAge = TimeSpan.MaxValue;
            this.LastBidAge = TimeSpan.MaxValue;
            this.SpreadBp = null;
        }
    }


    [DataContract]
    [KnownType(typeof(WritableToBPriceStreamInfoEntry))]
    public abstract class ToBPriceStreamInfoEntry : PriceStreamInfoEntryBase
    {
        [DataMember]
        public decimal? Ask { get; protected set; }
        [DataMember]
        public decimal? Bid { get; protected set; }

        public Counterparty AskCounterparty { get; protected set; }

        [DataMember]
        protected int? AskCounterpartSerialized
        {
            get { return this.AskCounterparty == Counterparty.NULL ? (int?)null : (int)this.AskCounterparty; }
            set { this.AskCounterparty = value == null ? Counterparty.NULL : (Counterparty)value; }
        }

        public Counterparty BidCounterparty { get; protected set; }

        [DataMember]
        protected int? BidCounterpartSerialized
        {
            get { return this.BidCounterparty == Counterparty.NULL ? (int?)null : (int)this.BidCounterparty; }
            set { this.BidCounterparty = value == null ? Counterparty.NULL : (Counterparty)value; }
        }
    }

    [DataContract]
    public class WritableToBPriceStreamInfoEntry : ToBPriceStreamInfoEntry
    {
        public WritableToBPriceStreamInfoEntry(Symbol symbol)
        {
            this.Symbol = symbol;
        }

        public void SetAsk(decimal ask, TimeSpan askAge, Counterparty askCounterparty)
        {
            this.Ask = ask;
            this.LastAskAge = askAge;
            this.AskCounterparty = askCounterparty;
        }

        public void ResetAsk()
        {
            this.Ask = null;
            this.LastAskAge = TimeSpan.MaxValue;
            this.AskCounterparty = Counterparty.NULL;
        }

        public void SetBid(decimal bid, TimeSpan bidAge, Counterparty bidCounterparty)
        {
            this.Bid = bid;
            this.LastBidAge = bidAge;
            this.BidCounterparty = bidCounterparty;
        }

        public void ResetBid()
        {
            this.Bid = null;
            this.LastBidAge = TimeSpan.MaxValue;
            this.BidCounterparty = Counterparty.NULL;
        }

        public void ResetEntry()
        {
            this.ResetAsk();
            this.ResetBid();
        }

        public void RecalculateSpreadBp()
        {
            if (!this.Ask.HasValue || !this.Bid.HasValue)
            {
                this.SpreadBp = null;
                return;
            }

            if (this.Ask == 0m && this.Bid == 0m)
                this.SpreadBp = 0m;
            else
                this.SpreadBp = 10000 * (this.Ask - this.Bid) / ((this.Ask + this.Bid) / 2);
        }
    }
}
