﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts
{
    public interface ICounterpartyRejectionSource
    {
        event Action<CounterpartyTimeoutInfo> CounterpartyDeliveredRejectionInfo;
    }

    public interface IStreamingRemoteClientDeal
    {
        Common.Symbol Symbol { get; }
        Common.Counterparty Counterparty { get; }

        [DataMember]
        string CounterpartyClientOrderIdentifier { get; }

        [DataMember]
        string CounterpartyClientId { get; }

        [DataMember]
        DealDirection IntegratorDealDirection { get; }

        [DataMember]
        decimal CounterpartyRequestedPrice { get; }

        [DataMember]
        decimal CounterpartyRequestedSizeBaseAbs { get; }

        [DataMember]
        DateTime SettlementDateLocal { get; }
        [DataMember]
        DateTime SettlementDateTimeCounterpartyUtc { get; }

        [DataMember]
        DateTime SettlementDateTimeCentralBankUtc { get; }

        [DataMember]
        string IntegratorQuoteIdentifier { get; }

        [DataMember]
        string IntegratorExecutionId { get; }

        [DataMember]
        DateTime CounterpartySentDealRequestTimeUtc { get; }

        [DataMember]
        DateTime IntegratorReceivedCounterpartyOrderTimeUtc { get; }

        [DataMember]
        DateTime IntegratorSentResponseTimeUtc { get; set; }

        [DataMember]
        IntegratedTradingSystemIdentification IntegratedTradingSystemIdentification { get; set; }

        RejectableMMDeal.DealStatus CurrentDealStatus { get; }
    }

    public interface IStreamingDealAcceptedByIntegrator : IStreamingRemoteClientDeal
    {
        decimal IntegratorFilledAmountBaseAbs { get; }

        decimal IntegratorFilledAmountBasePol { get; }
        decimal IntegratorExecutionPrice { get; }
        
    }

    public interface IStreamingDealRejected : IStreamingRemoteClientDeal
    {
        decimal IntegratorRejectedAmountBaseAbs { get; }
        decimal IntegratorRejectedAmountBasePol { get; }
    }

    public interface IStreamingDealRejectedByIntegrator : IStreamingDealRejected
    {
        RejectableMMDeal.DealRejectionReason IntegratorDealRejectionReason { get; }
    }

    public interface IStreamingDealRejectedByCounterparty : IStreamingDealRejected
    { }


    //TODO: move this class back to Kreslik.Integrator.Contracts.Internal once it wouldn't need to inherit from IntegratorInfoObjectBase
    [DataContract]
    public class RejectableMMDeal : IntegratorTradingInfoObjectBase, IExecutionInfo, IClientDealRequest, IStreamingDealAcceptedByIntegrator, IStreamingDealRejectedByIntegrator, IStreamingDealRejectedByCounterparty, IIntegratorReceivedObjectEx
    {
        public enum DealStatus
        {
            Pending = 0,
            KgtConfirmed = 1,
            KgtRejected_InternalFailure = 2,
            KgtRejected_KgtTooLate = 3,
            KgtRejected_KgtTooLateOTDelivered = 4,
            KgtRejected_DealMissed = 5,
            KgtRejected_SystemFailedToProcess = 6,
            KgtRejected_SystemUnavailableSizeToTrade = 7,
            KgtRejected_RiskMgmtDisallow = 8,
            KgtRejected_SettlDateUnavailable = 9,
            KgtRejected_IncomingOrderMismatch = 10,
            CounterpartyRejected = 11
        }

        public enum DealRejectionReason
        {
            InternalFailure = 2,
            KgtTooLate = 3,
            KgtTooLateOTDelivered = 4,
            DealMissed = 5,
            SystemFailedToProcess = 6,
            SystemUnavailableSizeToTrade = 7,
            RiskMgmtDisallow = 8,
            SettlDateUnavailable = 9,
            IncomingOrderMismatch = 10,
            CounterpartyRejected = 11
        }

        public RejectableMMDeal(Common.Symbol symbol, Counterparty counterparty, DealDirection integratorDealDirection,
            decimal counterpartyRequestedPrice, decimal counterpartyRequestedSizeBaseAbs,
            decimal counterpartyRequestedMinimumFillableQuantityBaseAbs,
            string counterpartyClientOrderIdentifier, string counterpartyClientIdentifier,
            string integratorQuoteIdentifier, DateTime counterpartySentDealRequestTimeUtc,
            ICounterpartyRejectionSource counterpartyRejectionSource)
            : this(
                symbol, counterparty, integratorDealDirection, counterpartyRequestedPrice,
                counterpartyRequestedSizeBaseAbs, counterpartyClientOrderIdentifier,
                integratorQuoteIdentifier, counterpartySentDealRequestTimeUtc, HighResolutionDateTime.UtcNow)
        {
            this.CounterpartyClientId = counterpartyClientIdentifier;
            this.CounterpartyRequestedSizeBaseAbsFillMinimum =
                counterpartyRequestedMinimumFillableQuantityBaseAbs;
            _counterpartyRejectionSource = counterpartyRejectionSource;
            _counterpartyRejectionSource.CounterpartyDeliveredRejectionInfo +=
                this.CounterpartyInfomedAboutRejectionByTimeout;
        }

        private RejectableMMDeal(Common.Symbol symbol, Counterparty counterparty, DealDirection integratorDealDirection, decimal counterpartyRequestedPrice, decimal counterpartyRequestedSizeBaseAbs, string counterpartyClientOrderIdentifier,
            string integratorQuoteIdentifier, DateTime counterpartySentDealRequestTimeUtc, DateTime integratorReveivedCounterpartyOrderTimeUtc)
            :base(counterparty.TradingTargetType, string.Empty)
        {
            this.Symbol = symbol;
            this.Counterparty = counterparty;
            this.IntegratorDealDirection = integratorDealDirection;
            this.CounterpartyRequestedPrice = counterpartyRequestedPrice;
            this.CounterpartyRequestedSizeBaseAbs = counterpartyRequestedSizeBaseAbs;
            this.CounterpartyRequestedSizeBaseAbsFillMinimum = counterpartyRequestedSizeBaseAbs;
            this.CounterpartyClientOrderIdentifier = counterpartyClientOrderIdentifier;
            this.IntegratorQuoteIdentifier = integratorQuoteIdentifier;
            this.LayerId = StreamingIdsGeneratorHelper.ExtractLayerId(integratorQuoteIdentifier);
            this.CounterpartySentDealRequestTimeUtc = counterpartySentDealRequestTimeUtc;
            this.IntegratorReceivedCounterpartyOrderTimeUtc = integratorReveivedCounterpartyOrderTimeUtc;

            if (!IsSideMatch(integratorDealDirection, integratorQuoteIdentifier))
            {
                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Quote [{0}] and deal side [{1}] do not match or were not able to be verified. {2} {3} {4}",
                    integratorQuoteIdentifier, integratorDealDirection, counterparty, symbol, this.Identity);
            }
        }

        private bool IsSideMatch(DealDirection integratorDealDirection, string integratorQuoteIdentifier)
        {
            return StreamingIdsGeneratorHelper.TryExtractIntegratorDirectionFromId(integratorQuoteIdentifier) ==
                   integratorDealDirection;
        }

        public static RejectableMMDeal CreateStoredRejectableMMDeal(Common.Symbol symbol, Counterparty counterparty, DealDirection integratorDealDirection, decimal dealPrice, decimal sizeBaseAbs, string clientOrderIdentifier,
            string quoteIdentifier, DateTime counterpartyDealTimeUtc, DateTime integratorReveivedTimeUtc, string integratorExecutionId,
            string counterpartyExecutionId, DateTime settlementDate)
        {
            return new RejectableMMDeal(symbol, counterparty, integratorDealDirection, dealPrice, sizeBaseAbs, clientOrderIdentifier,
                quoteIdentifier, counterpartyDealTimeUtc, integratorReveivedTimeUtc)
            {
                IntegratorExecutionId = integratorExecutionId, //System.Text.Encoding.ASCII.GetBytes(executionId),
                CounterpartyExecutionId = counterpartyExecutionId,
                SettlementDateLocal = settlementDate,
                SettlementDateTimeCounterpartyUtc = TradingHoursHelper.Instance.GetCounterpartyRolloverTimeInUtc(settlementDate, symbol, counterparty),
                SettlementDateTimeCentralBankUtc = TradingHoursHelper.Instance.GetCentralBankRolloverTimeInUtc(settlementDate, symbol),
                _dealStatus = (int)DealStatus.CounterpartyRejected
            };
        }

        public DateTime MaxProcessingCutoffTime { get; private set; }

        public bool SetTimeout(DateTime maxProcessingCutoffTime, Action<RejectableMMDeal, DealStatus> processingTimeExpiredAction)
        {
            this._timerExpiredAction = processingTimeExpiredAction;
            this.MaxProcessingCutoffTime = maxProcessingCutoffTime;
            return HighResolutionDateTime.HighResolutionTimerManager.TryRegisterTimerIfInFuture(this.TimeoutExpired,
                maxProcessingCutoffTime, TimerTaskFlagUtils.WorkItemPriority.Highest, TimerTaskFlagUtils.TimerPrecision.Highest,
                true, true, out _timer);
        }

        private ICounterpartyRejectionSource _counterpartyRejectionSource;

        public Common.Symbol Symbol { get; private set; }
        public Common.Counterparty Counterparty { get; private set; }

        [DataMember]
        //this member is needed so that we can (de)serialize Symbol while still using the same instance of this type
        private int SymbolSerialized
        {
            get { return (int)this.Symbol; }
            set { this.Symbol = (Symbol)value; }
        }

        [DataMember]
        private int CounterpartySerialized
        {
            get { return (int)this.Counterparty; }
            set { this.Counterparty = (Counterparty)value; }
        }

        [DataMember]
        public string CounterpartyClientOrderIdentifier { get; private set; }

        [DataMember]
        public string CounterpartyClientId { get; private set; }

        //This is direction from the Integrator perspective - not counterparty perspective
        [DataMember]
        public DealDirection IntegratorDealDirection { get; private set; }

        [DataMember]
        public decimal CounterpartyRequestedPrice { get; private set; }

        [DataMember]
        public decimal CounterpartyRequestedSizeBaseAbs { get; private set; }

        [DataMember]
        public decimal CounterpartyRequestedSizeBaseAbsFillMinimum { get; private set; }

        public decimal SizeToBeFilledBaseAbs { get; set; }

        [DataMember]
        public DateTime SettlementDateLocal { get; private set; }

        [DataMember]
        public DateTime SettlementDateTimeCounterpartyUtc { get; private set; }

        [DataMember]
        public DateTime SettlementDateTimeCentralBankUtc { get; private set; }

        [DataMember]
        public string IntegratorQuoteIdentifier { get; private set; }

        //public byte[] ExecutionId { get; private set; }
        [DataMember]
        public string IntegratorExecutionId { get; set; }

        [DataMember]
        public string CounterpartyExecutionId { get; private set; }

        [DataMember]
        public DateTime CounterpartySentDealRequestTimeUtc { get; private set; }

        [DataMember]
        public DateTime IntegratorReceivedCounterpartyOrderTimeUtc { get; private set; }

        [DataMember]
        public DateTime IntegratorSentResponseTimeUtc { get; set; }

        public int LayerId { get; private set; }

        [DataMember]
        public IntegratedTradingSystemIdentification IntegratedTradingSystemIdentification { get; set; }

        [DataMember]
        private int _dealStatus;
        private IAccurateTimer _timer;
        private Action<RejectableMMDeal, DealStatus> _timerExpiredAction;

        private void TimeoutExpired()
        {
            if (SetNewStatusIfNotClosed(DealStatus.KgtRejected_KgtTooLate))
            {
                if (this._timerExpiredAction != null)
                    this._timerExpiredAction(this, DealStatus.KgtRejected_KgtTooLate);
            }
        }

        public void CounterpartyInfomedAboutRejectionByTimeout(CounterpartyTimeoutInfo counterpartyTimeoutInfo)
        {
            if ((string.IsNullOrEmpty(counterpartyTimeoutInfo.CounterpartyExecutionId) &&
                 string.Equals(counterpartyTimeoutInfo.CounterpartyClientOrderId, this.CounterpartyClientOrderIdentifier,
                     StringComparison.Ordinal))
                ||
                string.Equals(counterpartyTimeoutInfo.CounterpartyExecutionId, this.CounterpartyExecutionId,
                    StringComparison.Ordinal))
            {
                if (SetNewStatusIfNotClosed(DealStatus.KgtRejected_KgtTooLateOTDelivered))
                {
                    if (this._timerExpiredAction != null)
                        this._timerExpiredAction(this, DealStatus.KgtRejected_KgtTooLateOTDelivered);
                }
            }
        }

        private bool SetNewStatusIfNotClosed(DealStatus dealStatus)
        {
            bool wasPending = InterlockedEx.CheckAndFlipState(ref _dealStatus, (int)dealStatus,
                (int)DealStatus.Pending);

            if (wasPending)
            {
                HighResolutionDateTime.HighResolutionTimerManager.CancelTimer(ref _timer);

                if (_counterpartyRejectionSource != null)
                {
                    _counterpartyRejectionSource.CounterpartyDeliveredRejectionInfo -=
                        this.CounterpartyInfomedAboutRejectionByTimeout;
                    _counterpartyRejectionSource = null;
                }
            }

            return wasPending;
        }

        public bool SetExecutionInfo(DealStatus dealStatus)
        {
            return this.SetNewStatusIfNotClosed(dealStatus);
            //if (this.SetNewStatusIfNotClosed(dealStatus))
            //{
            //    this.IntegratorExecutionId = executionId;//System.Text.Encoding.ASCII.GetBytes(executionId);
            //    this.SettlementDate = settlementDate;
            //    return true;
            //}
            //return false;
        }

        //public bool SetExecutionIdAfterTimeout(string executionId)
        //{
        //    DealStatus dealStatusLocal = (DealStatus) InterlockedEx.Read(ref _dealStatus);
        //    if (dealStatusLocal == DealStatus.KgtRejected_KgtTooLate || dealStatusLocal == DealStatus.KgtRejected_KgtTooLateOTDelivered)
        //    {
        //        this.IntegratorExecutionId = executionId;
        //        return true;
        //    }
        //    return false;
        //}

        public void SetSettlementDateIfNotSet(DateTime settlementDate)
        {
            if (this.SettlementDateLocal == DateTime.MinValue)
            {
                this.SettlementDateLocal = settlementDate;
                this.SettlementDateTimeCounterpartyUtc = TradingHoursHelper.Instance.GetCounterpartyRolloverTimeInUtc(settlementDate,
                    this.Symbol, this.Counterparty);
                this.SettlementDateTimeCentralBankUtc = TradingHoursHelper.Instance.GetCentralBankRolloverTimeInUtc(settlementDate,
                this.Symbol);
            }
        }

        public void SetCounterpartyExecutionInfo(DateTime settlementDate, string counterpartyExecutionId)
        {
            this.SettlementDateLocal = settlementDate;
            this.SettlementDateTimeCounterpartyUtc = TradingHoursHelper.Instance.GetCounterpartyRolloverTimeInUtc(settlementDate,
                this.Symbol, this.Counterparty);
            this.SettlementDateTimeCentralBankUtc = TradingHoursHelper.Instance.GetCentralBankRolloverTimeInUtc(settlementDate,
                this.Symbol);
            this.CounterpartyExecutionId = counterpartyExecutionId;
        }

        public bool CanTimeoutDeal()
        {
            DealStatus dealStatusLocal = (DealStatus)InterlockedEx.Read(ref _dealStatus);
            return (dealStatusLocal == DealStatus.KgtRejected_KgtTooLate ||
                    dealStatusLocal == DealStatus.KgtRejected_KgtTooLateOTDelivered);
        }

        public DealStatus CurrentDealStatus { get { return (DealStatus)_dealStatus; } }

        public bool DealRejected { get { return _dealStatus > (int)DealStatus.KgtConfirmed; } }

        public bool DealExecuted { get { return _dealStatus == (int)DealStatus.KgtConfirmed; } }

        #region IExecutionInfo
        public bool IsExecutionInfoPopulated
        {
            get { return _dealStatus != (int)DealStatus.Pending; }
        }

        public decimal IntegratorFilledAmountBaseAbs
        {
            get { return DealExecuted ? SizeBaseAbsInitial : 0m; }
        }

        public decimal IntegratorFilledAmountBasePol
        {
            get
            {
                return this.IntegratorDealDirection == DealDirection.Buy
                    ? IntegratorFilledAmountBaseAbs
                    : -IntegratorFilledAmountBaseAbs;
            }
        }

        public string ExecutionId { get { return this.CounterpartyExecutionId ?? this.IntegratorExecutionId; } }

        public decimal IntegratorExecutionPrice
        {
            get { return CounterpartyRequestedPrice; }
        }

        public decimal UsedPrice
        {
            get { return CounterpartyRequestedPrice; }
        }

        public string IntegratorTransactionId
        {
            get { return this.ExecutionId; }
        }

        public DateTime TransactionTime
        {
            get { return this.CounterpartySentDealRequestTimeUtc; }
        }

        #endregion /IExecutionInfo

        #region IIntegratorOrderInfo
        public string Identity
        {
            get { return this.ExecutionId ?? this.CounterpartyClientOrderIdentifier; }
        }

        public decimal SizeBaseAbsInitial
        {
            get { return SizeToBeFilledBaseAbs > 0 ? SizeToBeFilledBaseAbs : this.CounterpartyRequestedSizeBaseAbs; }
        }

        public decimal RequestedPrice { get { return this.CounterpartyRequestedPrice; } }

        public decimal FilledAmountBaseAbs { get { return this.IntegratorFilledAmountBaseAbs; } }

        public IntegratorOrderInfoType IntegratorOrderInfoType
        {
            get { return IntegratorOrderInfoType.ExternalClientDealRequest; }
        }
        public bool IsRiskRemovingOrder { get; set; }

        #endregion /IIntegratorOrderInfo

        public override string ToString()
        {
            return
                string.Format(
                    "RejctableMMDeal [{0} hit {1}]: integrator can {2} {3} of {4} at {5} from {6}. State: {7}, ExecId: {8}",
                    this.CounterpartyClientOrderIdentifier, this.IntegratorQuoteIdentifier, this.IntegratorDealDirection, this.CounterpartyRequestedSizeBaseAbs,
                    this.Symbol, this.CounterpartyRequestedPrice, this.Counterparty, this.CurrentDealStatus, this.ExecutionId);
        }

        #region IStreamingRejectedDeal
        public decimal IntegratorRejectedAmountBaseAbs
        {
            get { return DealRejected ? CounterpartyRequestedSizeBaseAbs : (DealExecuted ? CounterpartyRequestedSizeBaseAbs - SizeToBeFilledBaseAbs : 0m); }
        }

        public decimal IntegratorRejectedAmountBasePol
        {
            get
            {
                return this.IntegratorDealDirection == DealDirection.Buy
                    ? IntegratorRejectedAmountBaseAbs
                    : -IntegratorRejectedAmountBaseAbs;
            }
        }

        public decimal RejectedPrice
        {
            get { return this.CounterpartyRequestedPrice; }
        }
        #endregion IStreamingRejectedDeal

        #region IIntegratorReceivedObject members
        public DateTime IntegratorReceivedTimeUtc { get { return this.IntegratorReceivedCounterpartyOrderTimeUtc; } }
        public DateTime CounterpartySentTimeUtc { get { return this.CounterpartySentDealRequestTimeUtc; } }
        #endregion IIntegratorReceivedObject members

        public DealRejectionReason IntegratorDealRejectionReason
        {
            get { return (DealRejectionReason) this._dealStatus; }
        }

        public string CounterpartyDealIdentity { get { return this.CounterpartyClientId; }}
    }
}
