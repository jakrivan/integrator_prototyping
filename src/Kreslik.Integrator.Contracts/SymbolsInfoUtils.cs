﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public class SymbolsInfoUtils : ISymbolsInfo
    {
        private SymbolInfoBag[][] _tradingTargetsSymbolsInfoBags;
        private List<string>[] _ascSortedListsOfSupportedSymbolsByTradingTarget;
        private bool[] _supportedCurrenciesMap = null;
        private decimal[] _commisionsPricePerMillionMap;
        private decimal[] _commisionsPricePerUnitMap;

        /// <summary>
        /// Initializes or refreshes this instance so that it can work offline from Integrator
        ///  
        /// </summary>
        /// <param name="priceHistoryProvider"></param>
        public void InitializeStateFromBackend(IPriceHistoryProvider priceHistoryProvider, ILogger logger)
        {
            SymbolInfoBag[][] tradingTargetsSymbolsInfosBag = null;

            bool[] supportedCurrenciesMap;
            decimal[] commisionsPricePerMillionMap;
            try
            {
                tradingTargetsSymbolsInfosBag = priceHistoryProvider.GetSymbolsCounterpartyInfo(logger);
                commisionsPricePerMillionMap = priceHistoryProvider.GetCounterpartyCommisionsPricePerMillionMap(logger);
                var supportedCurrencies = priceHistoryProvider.GetSupportedCurrenciesList();
                supportedCurrenciesMap = Enumerable.Repeat(false, Currency.ValuesCount).ToArray();
                foreach (Currency supportedCurrency in supportedCurrencies)
                {
                    supportedCurrenciesMap[(int) supportedCurrency] = true;
                }
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Fatal, e, "Unexpected exception during reading symbols info from backend");
                throw;
            }

            this.InitializeState(new SymbolsInfoArgs(tradingTargetsSymbolsInfosBag, supportedCurrenciesMap, commisionsPricePerMillionMap), logger);
        }

        public void InitializeState(SymbolsInfoArgs symbolsInfoArgs, ILogger logger)
        {
            if (symbolsInfoArgs == null || symbolsInfoArgs.SymbolsInfoBag == null ||
                    symbolsInfoArgs.SymbolsInfoBag.Length != (int)TradingTargetType.ValuesCount ||
                    symbolsInfoArgs.SymbolsInfoBag.Any(symInfoArr => symInfoArr == null || symInfoArr.Length != Symbol.ValuesCount)
                || symbolsInfoArgs.SupportedCurrenciesMap == null || symbolsInfoArgs.SupportedCurrenciesMap.Length != Currency.ValuesCount
                || symbolsInfoArgs.CounterpartyCommisionsPricePerMillionMap == null || symbolsInfoArgs.CounterpartyCommisionsPricePerMillionMap.Length != Counterparty.ValuesCount)
            {
                logger.Log(LogLevel.Fatal,
                                 "Cannot update symbols info as the data coming from server are invalid (null or too short) - possible compilation mismatch");
                return;
            }

            this._supportedCurrenciesMap = symbolsInfoArgs.SupportedCurrenciesMap;
            this._commisionsPricePerMillionMap = symbolsInfoArgs.CounterpartyCommisionsPricePerMillionMap;
            this._commisionsPricePerUnitMap =
                this._commisionsPricePerMillionMap.Select(comm => comm/1000000.0m).ToArray();
            this._tradingTargetsSymbolsInfoBags = symbolsInfoArgs.SymbolsInfoBag;

            //Just for sure explicitly normalize incoming info
            for (int tradingTypeIdx = 0; tradingTypeIdx < (int)TradingTargetType.ValuesCount; tradingTypeIdx++)
            {
                for (int symbolIdx = 0; symbolIdx < Symbol.ValuesCount; symbolIdx++)
                {
                    if (this._tradingTargetsSymbolsInfoBags[tradingTypeIdx][symbolIdx] != null)
                        this._tradingTargetsSymbolsInfoBags[tradingTypeIdx][symbolIdx].Normalize();
                }
            }

            this._ascSortedListsOfSupportedSymbolsByTradingTarget = new List<string>[(int)TradingTargetType.ValuesCount];
            for (int tradingTargetIdx = 0; tradingTargetIdx < (int)TradingTargetType.ValuesCount; tradingTargetIdx++)
            {
                this._ascSortedListsOfSupportedSymbolsByTradingTarget[tradingTargetIdx] =
                    symbolsInfoArgs.SymbolsInfoBag[tradingTargetIdx].Where(symInfo => symInfo != null && symInfo.IsSupported)
                                                                 .Select(symInfo => symInfo.Symbol.ToString())
                                                                 .OrderBy(s => s)
                                                                 .ToList();
            }
        }

        public decimal GetCommisionPricePerUnit(Counterparty counterparty)
        {
            if (_commisionsPricePerUnitMap == null)
                throw new ClientGatewayException("Requesting Commisions info but this kind of data is not yet initialized from Integrator (might be caused by version mismatch)");

            return this._commisionsPricePerUnitMap[(int)counterparty];
        }

        public decimal GetCommisionPricePerMillion(Counterparty counterparty)
        {
            if (_commisionsPricePerMillionMap == null)
                throw new ClientGatewayException("Requesting Commisions info but this kind of data is not yet initialized from Integrator (might be caused by version mismatch)");

            return this._commisionsPricePerMillionMap[(int) counterparty];
        }

        public bool IsSupported(Symbol symbol, TradingTargetType tradingTargetType)
        {
            if (this._tradingTargetsSymbolsInfoBags == null)
                throw new ClientGatewayException("Requesting Symbols info but this kind of data is not yet initialized from Integrator (might be caused b yversion mismatch)");

            return this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol] != null &&
                   this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].IsSupported;
        }

        public bool IsSupported(Currency currency)
        {
            if (_supportedCurrenciesMap == null)
                throw new ClientGatewayException("Requesting Currency info but this kind of data is not yet initialized from Integrator (might be caused by version mismatch)");

            return _supportedCurrenciesMap[(int) currency];
        }

        public List<string> GetAscendingSortedStringListOfSupportedSymbols(TradingTargetType tradingTargetType)
        {
            if (this._tradingTargetsSymbolsInfoBags == null)
                throw new ClientGatewayException("Requesting supported Symbols list but this kind of data is not yet initialized from Integrator (might be caused b yversion mismatch)");

            return this._ascSortedListsOfSupportedSymbolsByTradingTarget[(int)tradingTargetType];
        }

        public decimal? GetMinimumPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType)
        {
            if (this._tradingTargetsSymbolsInfoBags == null)
                throw new ClientGatewayException("Requesting Symbols info but this kind of data is not yet initialized from Integrator (might be caused b yversion mismatch)");

            return this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol] == null
                       ? null
                       : this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].MinimumPriceIncrement;
        }

        public bool IsPriceWithinPriceIncrementLimit(decimal price, Symbol symbol, TradingTargetType tradingTargetType)
        {
            if (this._tradingTargetsSymbolsInfoBags == null)
                return false;

            if (this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol] == null ||
                !this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].MinimumPriceIncrement.HasValue)
            {
                return false;
            }

            return price % this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].MinimumPriceIncrement == 0;
        }

        public static decimal RoundToIncrement(RoundingKind roundingKind, decimal increment, decimal value)
        {
            decimal remainder = value % increment;

            //Is rounding needed at all?
            if (remainder != 0)
            {
                //if choosed to round to nearest then compare which kind of rounding will cause smaller change of price
                if (roundingKind == RoundingKind.ToNearest)
                {
                    //If remainder (in ABS) is smaller then rest of the granularity (granularity - ABS(reminder)) then round to zero
                    if ((remainder > 0 && remainder < (increment - remainder)) || (remainder < 0 && -remainder < (increment + remainder)))
                        roundingKind = RoundingKind.AlwaysToZero;
                    else
                        roundingKind = RoundingKind.AlwaysAwayFromZero;
                }
                //Ceiling is To Zero for negative and AwayFromZero for positive numbers
                else if (roundingKind == RoundingKind.ToCeiling)
                {
                    roundingKind = value < 0 ? RoundingKind.AlwaysToZero : RoundingKind.AlwaysAwayFromZero;
                }
                //And vice versa for To Floor
                else if (roundingKind == RoundingKind.ToFloor)
                {
                    roundingKind = value < 0 ? RoundingKind.AlwaysAwayFromZero : RoundingKind.AlwaysToZero;
                }

                if (roundingKind == RoundingKind.AlwaysToZero)
                {
                    value = value - remainder;
                }
                else if (roundingKind == RoundingKind.AlwaysAwayFromZero)
                {
                    if (value > 0)
                        value = value + (increment - remainder);
                    else
                        value = value - (increment + remainder);
                }
                else
                {
                    throw new ClientGatewayException(string.Format("Unknown price rounding kind: {0}", roundingKind));
                }
            }

            //Despite granularity is already normalized, the incoming price can have huge decimal part
            // but normalize only for prices
            //value = value.Normalize();

            return value;
        }

        public static RoundingResult TryRoundToIncrement(RoundingKind roundingKind, decimal increment, decimal value)
        {
            return RoundingResult.CreateSuccessResult(RoundToIncrement(roundingKind, increment, value));
        }

        public bool TryRoundToSupportedPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType,
                                                                RoundingKind roundingKind, decimal price, out decimal result)
        {
            result = 0m;
            if (this._tradingTargetsSymbolsInfoBags == null)
                return false;

            if (this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol] == null ||
                !this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].MinimumPriceIncrement.HasValue)
            {
                return false;
            }

            decimal priceIncrement =
                this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].MinimumPriceIncrement.Value;

            result = RoundToIncrement(roundingKind, priceIncrement, price).Normalize();

            return true;
        }

        public RoundingResult TryRoundToSupportedPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType,
            RoundingKind roundingKind, decimal price)
        {
            if (this._tradingTargetsSymbolsInfoBags == null)
                return RoundingResult.CreateFailedResult("Trding info not (yet) initialized at all");

            if (this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol] == null ||
                !this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].MinimumPriceIncrement.HasValue)
            {
                return RoundingResult.CreateFailedResult("Trding info for requested symbol and target is not initialized (most likely it's not enabled and populated in backend)");
            }

            decimal priceIncrement =
                this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].MinimumPriceIncrement.Value;

            return TryRoundToIncrement(roundingKind, priceIncrement, price);
        }

        public RoundingResult TryRoundToSupportedPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType, decimal price)
        {
            return this.TryRoundToSupportedPriceIncrement(symbol, tradingTargetType, RoundingKind.ToNearest, price);
        }




        public decimal? GetOrderMinimumSize(Symbol symbol, TradingTargetType tradingTargetType)
        {
            if (this._tradingTargetsSymbolsInfoBags == null)
                throw new ClientGatewayException("Requesting Symbols info but this kind of data is not yet initialized from Integrator (might be caused b yversion mismatch)");

            return this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol] == null
                       ? null
                       : this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].OrderMinimumSize;
        }

        public decimal? GetOrderMinimumSizeIncrement(Symbol symbol, TradingTargetType tradingTargetType)
        {
            if (this._tradingTargetsSymbolsInfoBags == null)
                throw new ClientGatewayException("Requesting Symbols info but this kind of data is not yet initialized from Integrator (might be caused b yversion mismatch)");

            return this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol] == null
                       ? null
                       : this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].OrderMinimumSizeIncrement;
        }

        private bool IsSizesInfoInitialized(Symbol symbol, TradingTargetType tradingTargetType)
        {
            if (this._tradingTargetsSymbolsInfoBags == null)
                return false;

            if (this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol] == null ||
                !this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].OrderMinimumSize.HasValue ||
                !this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].OrderMinimumSizeIncrement.HasValue)
            {
                return false;
            }

            return true;
        }

        public bool IsOrderSizeWithinSizeAndIncrementLimit(decimal orderSize, Symbol symbol, TradingTargetType tradingTargetType)
        {
            return 
                this.IsSizesInfoInitialized(symbol, tradingTargetType)
                &&
                orderSize >= this._tradingTargetsSymbolsInfoBags[(int) tradingTargetType][(int) symbol].OrderMinimumSize 
                &&
                orderSize % this._tradingTargetsSymbolsInfoBags[(int) tradingTargetType][(int) symbol].OrderMinimumSizeIncrement == 0;

        }

        public bool IsOrderMinSizeZeroOrWithinSizeAndIncrementLimit(decimal orderMinSize, Symbol symbol, TradingTargetType tradingTargetType)
        {
            return
                this.IsSizesInfoInitialized(symbol, tradingTargetType)
                &&
                (
                    orderMinSize == 0
                    ||
                    (
                        orderMinSize >= this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].OrderMinimumSize
                        &&
                        orderMinSize % this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].OrderMinimumSizeIncrement == 0
                    )
                );
        }

        public bool IsOrderSizeWithinIncrementLimit(decimal orderSize, Symbol symbol, TradingTargetType tradingTargetType)
        {
            return
                this.IsSizesInfoInitialized(symbol, tradingTargetType)
                &&
                orderSize % this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].OrderMinimumSizeIncrement == 0;
        }

        public RoundingResult TryRoundToSupportedSizeIncrement(Symbol symbol, TradingTargetType tradingTargetType, RoundingKind roundingKind, decimal size)
        {
            if (this._tradingTargetsSymbolsInfoBags == null)
                return RoundingResult.CreateFailedResult("Trding info not (yet) initialized at all");

            if (this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol] == null ||
                !this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].OrderMinimumSizeIncrement.HasValue)
            {
                return RoundingResult.CreateFailedResult("Trding info for requested symbol and target is not initialized (most likely it's not enabled and populated in backend)");
            }

            decimal sizeIncrement =
                this._tradingTargetsSymbolsInfoBags[(int)tradingTargetType][(int)symbol].OrderMinimumSizeIncrement.Value;

            return TryRoundToIncrement(roundingKind, sizeIncrement, size);
        }

        public RoundingResult TryRoundToSupportedSizeIncrement(Symbol symbol, TradingTargetType tradingTargetType, decimal size)
        {
            return this.TryRoundToSupportedSizeIncrement(symbol, tradingTargetType, RoundingKind.ToNearest, size);
        }
    }
}
