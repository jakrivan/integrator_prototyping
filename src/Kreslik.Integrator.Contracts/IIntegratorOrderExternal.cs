﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    public interface IIntegratorOrderExternal : IIntegratorOrderInfo
    {
        string Identity { get; }
        string UniqueInternalIdentity { get; }
        IntegratorOrderExternalStatus OrderStatus { get; }
        decimal FilledAmount { get; }
        decimal NotToBeFilledAmount { get; }
        bool HasRemainingAmount { get; }
        bool HasPendingLLDeals { get; }
        OrderRequestInfo OrderRequestInfo { get; }
        event Action<IIntegratorOrderExternal, OrderChangeEventArgs> OnOrderChanged;
        //IOrderFlowSession ParentSession { get; }
        Counterparty Counterparty { get; }
        DateTime IntegratorSentTimeUtc { get; }
        string OutgoingFixMessageRaw { get; }
        TimeSpan? QuoteReceivedToOrderSentInternalLatency { get; }
        DateTime IntegratorReceivedResultTimeUtc { get; }
        DateTime CounterpartySentResultTimeUtc { get; }
        bool IsActualizedWithInfoAboutSending { get; }
        /// <summary>
        /// Counterparty execution type. Populated only for deals (not rejections) and only if bank gives us this
        /// </summary>
        DateTime? CounterpartyTransactionTimeUtc { get; }
    }
}
