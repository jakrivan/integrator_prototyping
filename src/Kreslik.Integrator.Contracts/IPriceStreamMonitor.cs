﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts
{
    public interface IMeanTracker
    {
        AtomicDecimal Mean { get; }
    }

    public interface IMedianProvider<TNode>
    {
        TNode Median { get; }
    }

    public interface IUsdConversionProvider
    {
        decimal GetCurrentUsdConversionMultiplier(Currency currency);
        decimal GetCurrentUsdConversionMultiplier();
    }

    public interface  IPriceStreamsInfoProvider
    {
        List<PriceStreamInfoEntry> GetPriceStreamInfoEntries();
    }

    public interface IPriceDeviationCheckState
    {
        bool GetIsInitialized(Symbol symbol);
    }

    public interface IPriceStreamMonitor
    {
        decimal[] UsdConversionMultipliers { get; }
        event Action<decimal[]> NewUsdConversionMultipliersAvailable;
        bool TryGetOnlineMedianPrice(Symbol symbol, PriceSide side, out decimal price, out DateTime ageOfOldestPrice);
        bool GetHasFreshDataForSymbol(Symbol symbol, TimeSpan maxPriceAge);
        IMedianProvider<decimal> GetMedianPriceProvider(Symbol symbol, PriceSide side);
        //IMeanTracker GetSpreadMeanTracker(Symbol symbol);
        IUsdConversionProvider CreateUsdConversionProvider(Currency currency);
    }
}
