﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    public abstract class ClientGatewayBase : ClientGatewayEventsDispatcherBase, IClientGateway, IConnectionInfo
    {
        protected ILogger _logger;
        private ClientOrdersTracker _clientOrdersTracker;
        private AllTarget _allTarget;
        private ITradingTargetImplementation[] _tradingTargets =
            new ITradingTargetImplementation[TradingTargetType.ValuesCount.OwningEnumValuesCount()];


        protected ClientGatewayBase(string clientId, ILogger logger) :
            base(logger, clientId, false)
        {
            _logger = logger;
            _clientOrdersTracker = new ClientOrdersTracker(logger);

            HotspotTarget hotspotTarget = new HotspotTarget(this);
            FXallTarget fxallTarget = new FXallTarget(this);
            LmaxTarget lmaxTarget = new LmaxTarget(this);
            FXCMTarget fxcmTarget = new FXCMTarget(this);
            FXCMStreamingTarget fxcmStreamingTarget = new FXCMStreamingTarget(this);
            BankPoolTarget bankPoolTarget = new BankPoolTarget(this);
            _tradingTargets[(int) TradingTargetType.Hotspot] = hotspotTarget;
            _tradingTargets[(int) TradingTargetType.FXall] = fxallTarget;
            _tradingTargets[(int) TradingTargetType.LMAX] = lmaxTarget;
            _tradingTargets[(int) TradingTargetType.FXCM] = fxcmTarget;
            _tradingTargets[(int)TradingTargetType.FXCMMM] = fxcmStreamingTarget;
            _tradingTargets[(int) TradingTargetType.BankPool] = bankPoolTarget;

            _allTarget = new AllTarget(this);
            _targets = new ClientGatewayTargetsImpl(bankPoolTarget, hotspotTarget, fxallTarget, lmaxTarget, fxcmTarget, fxcmStreamingTarget, _allTarget);
            _tradingInfo = new TradingInfoImpl(_logger, this);
        }

        protected virtual bool CheckOrdersGranularity
        {
            get { return true; }
        }

        #region ClientGatewayEventsDispatcherBase

        private ITradingTargetImplementation TradingTarget(TradingTargetType tradingTargetType)
        {
            ITradingTargetImplementation tradingTargetImplementation = null;

            if ((int) tradingTargetType < _tradingTargets.Length)
                tradingTargetImplementation = this._tradingTargets[(int) tradingTargetType];

            if (tradingTargetImplementation == null)
            {
                this._logger.Log(LogLevel.Fatal, "Receiving update unexpected SenderTradingTargetType: {0}", tradingTargetType);
            }

            return tradingTargetImplementation;
        }

        protected override void DispatchPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            if (priceUpdateEventArgs is VenueDealObject)
            {
                this.TradingTarget(priceUpdateEventArgs.SenderTradingTargetType).InvokeNewVenueDealReported(
                    priceUpdateEventArgs as VenueDealObject);
            }
            else
            {
                this.TradingTarget(priceUpdateEventArgs.SenderTradingTargetType).InvokePriceUpdate(priceUpdateEventArgs);
            }
        }

        protected override void DispatchUnicastInfo(IntegratorInfoObjectBase integratorInfo)
        {
            //TODO: All order related unicats should have common base exposing ClientOrderId and ClientOrder
            // here we would fill it as follows (however what about parent and child orders?):
            //clientOrderUpdateInfo.ClientOrder =
            //        _clientOrdersTracker.GetClientOrder(clientOrderUpdateInfo.ClientOrderIdentity);


            if (integratorInfo is IntegratorTradingInfoObjectBase)
            {
                IntegratorTradingInfoObjectBase unicastInfo = integratorInfo as IntegratorTradingInfoObjectBase;
                this.TradingTarget(unicastInfo.SenderTradingTargetType).InvokeNewIntegratorUnicastInfo(unicastInfo);
            }
            else if (integratorInfo is IntegratorMulticastInfo)
            {
                IntegratorMulticastInfo multicastInfo = integratorInfo as IntegratorMulticastInfo;

                if (multicastInfo.ForwardedInfo != null)
                {
                    this.TradingTarget(multicastInfo.ForwardedInfo.SenderTradingTargetType).InvokeNewForwardedIntegratorUnicastInfo(multicastInfo.ForwardedInfo);
                }
                else if (multicastInfo.InfoObject is IImportantIntegratorInformation)
                {
                    IImportantIntegratorInformation importantInfo =
                        multicastInfo.InfoObject as IImportantIntegratorInformation;

                    if (this._tradingInfo._importantIntegratorInformationArrived != null)
                        this._tradingInfo._importantIntegratorInformationArrived(importantInfo);
                    else
                        this._logger.Log(LogLevel.Error, "IImportantIntegratorInformation arrived, but there is no subscriber. {0}", importantInfo);
                }
            }
            else
            {
                _logger.Log(LogLevel.Error, "Receiving unicast info of unknown type: {0}, info: {1}",
                            integratorInfo.GetType(), integratorInfo);
            }
        }

        //TODO: this is to be used forfilling gateway base with info
        protected override void DispatchBroadcastInfo(IntegratorBroadcastInfoBase broadcastInfo)
        {
            if (broadcastInfo is OrderTransmissionStatusChangedEventArgs)
            {
                OrderTransmissionStatusChangedEventArgs orderTransmissionStatusChangedEventArgs =
                    broadcastInfo as OrderTransmissionStatusChangedEventArgs;

                this.DispatchOnOrderTransmissionChanged(orderTransmissionStatusChangedEventArgs);
            }
            else if (broadcastInfo is TradingAllowedStatusChangedEventArgs)
            {
                TradingAllowedStatusChangedEventArgs tradingAllowedStatusChangedEventArgs =
                    broadcastInfo as TradingAllowedStatusChangedEventArgs;

                this._tradingInfo.OnTradingAllowedStatusChanged(tradingAllowedStatusChangedEventArgs);
            }
            else if (broadcastInfo is SymbolExchangeRatesArgs)
            {
                SymbolExchangeRatesArgs symbolExchangeRatesArgs = broadcastInfo as SymbolExchangeRatesArgs;
                this._tradingInfo.UpdateExchangeRates(symbolExchangeRatesArgs);
            }
            else if (broadcastInfo is SymbolsInfoArgs)
            {
                SymbolsInfoArgs symbolsInfoArgs = broadcastInfo as SymbolsInfoArgs;
                this._tradingInfo.UpdateTradingTargetsSymbolsInfo(symbolsInfoArgs);
            }
            else if (broadcastInfo is ConnectionStateChangeEventArgs)
            {
                ConnectionStateChangeEventArgs args = broadcastInfo as ConnectionStateChangeEventArgs;
                if (args.IntegratorProcessTypeWithChangedState == IntegratorProcessType.BusinessLogic)
                    this.CurrentBussinessLogicConnectionServiceState = args.ConnectionServiceState;
                else if (args.IntegratorProcessTypeWithChangedState == IntegratorProcessType.HotspotMarketData)
                    this.CurrentOffloadedMarketDataConnectionServiceState = args.ConnectionServiceState;

                if (this.ConnectionStateChanged != null)
                {
                    this.ConnectionStateChanged(args);
                }
            }
            else
            {
                _logger.Log(LogLevel.Error,
                            "Receiving broadcast info of unknown type: {0}, info: {1}",
                            broadcastInfo.GetType(), broadcastInfo);
            }
        }

        protected abstract void LocalResourcesCleanupRoutine();
        protected override void DispatchGatewayClosed_CloseLocalResourcesOnly(string reason)
        {
            if (!this._isInternalLogicClosedAndCleanedUp)
            {
                this._isInternalLogicClosedAndCleanedUp = true;

                this.LocalResourcesCleanupRoutine();

                if (GatewayClosed != null)
                {
                    GatewayClosed(reason);
                }
            }
        }

        private void DispatchOnOrderTransmissionChanged(OrderTransmissionStatusChangedEventArgs orderTransmissionStatusChangedEventArgs)
        {
            if (orderTransmissionStatusChangedEventArgs.IsOrderTransmissionEnabled)
            {
                this._logger.Log(LogLevel.Info, "Got signal from server that the order transmission is enabled.");
            }
            else
            {
                this._logger.Log(LogLevel.Error, "Got signal from server that the order transmission is disabled. Reason: {0}", orderTransmissionStatusChangedEventArgs.Reason);
            }

            this._tradingInfo.OnOrderTransmissionStatusChanged(orderTransmissionStatusChangedEventArgs);
        }

        #endregion /ClientGatewayEventsDispatcherBase



        #region /IClientGateway ipml

        protected readonly TradingInfoImpl _tradingInfo;
        private readonly ClientGatewayTargetsImpl _targets;


        public ITradingInfo TradingInfo
        {
            get { return this._tradingInfo; }
        }

        public IClientGatewayTargets Targets
        {
            get { return this._targets; }
        }

        protected abstract string ClientIdentityInternal { get; set; }

        public ConnectionChainBuildVersioningInfo ConnectionChainBuildVersioningInfo { get; protected set; }

        public string ClientIdentity
        {
            get { return this.ClientIdentityInternal; }
        }

        public event GatewayClosedHandler GatewayClosed;
        public event Action<ConnectionStateChangeEventArgs> ConnectionStateChanged;
        public ConnectionServiceState CurrentBussinessLogicConnectionServiceState { get; private set; }
        public ConnectionServiceState CurrentOffloadedMarketDataConnectionServiceState { get; private set; }

        public IConnectionInfo ConnectionInfo { get { return this; } }

        public IntegratorRequestResult SubmitOrder(ClientOrderBase clientOrderBase)
        {
            if (!this._tradingInfo.IsOrderTransmissionEnabled)
            {
                string error = string.Format("Client [{0}] is attempting to submit order [{1}] while order transmission is disabled",
                                 this.ClientIdentity, clientOrderBase.ClientOrderIdentity);
                this._logger.Log(LogLevel.Error, error, this.ClientIdentity, clientOrderBase.ClientOrderIdentity);
                return IntegratorRequestResult.CreateFailedResult(error);
            }

            _clientOrdersTracker.AddOrder(clientOrderBase);
            IntegratorRequestResult result = this.SubmitRequestPrivate(clientOrderBase);

            if (!result.RequestSucceeded)
            {
                _clientOrdersTracker.RemoveOrder(clientOrderBase.ClientOrderIdentity);
            }

            return result;
        }

        private IntegratorRequestResult CancelOrder(string clientOrderId, TradingTargetType tradingTargetType)
        {
            return this.SubmitRequestPrivate(new CancelOrderRequestInfo(clientOrderId, this.ClientIdentity, tradingTargetType));
        }

        private IntegratorRequestResult CancelOrder(VenueClientOrder venueClientOrder)
        {
            return this.SubmitRequestPrivate(new CancelOrderRequestInfo(venueClientOrder.ClientOrderIdentity, this.ClientIdentity, venueClientOrder.VenueClientOrderRequestInfo.Counterparty));
        }

        private IntegratorRequestResult CancelAllMyOrdersPrivate()
        {
            return this.SubmitRequestPrivate(new IntegratorCancelAllOrdersForSingleClientRequestInfo(this.ClientIdentity));
        }

        public IntegratorRequestResult Subscribe(IntegratorSubscriptionRequestBase subscriptionRequestInfo)
        {
            return this.SubmitRequestPrivate(new IntegratorTransferableSubscriptionRequest(subscriptionRequestInfo));
        }

        public IntegratorRequestResult Unsubscribe(IntegratorSubscriptionRequestBase subscriptionRequestInfo)
        {
            return this.SubmitRequestPrivate(new IntegratorTransferableSubscriptionRequest(subscriptionRequestInfo));
        }

        private IntegratorRequestResult SubmitRequestPrivate(IntegratorRequestInfo integratorRequestInfo)
        {
            this._logger.Log(LogLevel.Debug, "Client [{0}] is sending RequestInfo: {1}", this.ClientIdentity, integratorRequestInfo);

            if (this.IsClosed)
            {
                string error =
                    string.Format(
                        "Client {0} is attempting to send RequestInfo {1} but the client should have been already closed. Ignoring the attempt",
                        ClientIdentity, integratorRequestInfo);

                this._logger.Log(LogLevel.Fatal, error, ClientIdentity, integratorRequestInfo);
                return IntegratorRequestResult.CreateFailedResult(error);
            }

            if (this.CurrentBussinessLogicConnectionServiceState != ConnectionServiceState.Running &&
                this.CurrentBussinessLogicConnectionServiceState != ConnectionServiceState.InitiateShutdown)
            {
                string error =
                    string.Format(
                        "Client {0} is attempting to send RequestInfo {1} but the client is in {2} state and so sending of requests is disallowed",
                        this.ClientIdentity, integratorRequestInfo, this.CurrentBussinessLogicConnectionServiceState);
                this._logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }

            IntegratorRequestResult result = this.SubmitRequestInternal(integratorRequestInfo);

            if (!result.RequestSucceeded)
            {
                this._logger.Log(LogLevel.Warn, "Client [{0}] request: {1} Failed Reason: {2}", this.ClientIdentity, integratorRequestInfo, result.ErrorMessage);
            }

            return result;
        }

        protected abstract IntegratorRequestResult SubmitRequestInternal(IntegratorRequestInfo integratorRequestInfo);

        protected abstract bool IsConnectionChannelClosed { get; }
        private bool _isInternalLogicClosedAndCleanedUp;
        public bool IsClosed
        {
            get { return this._isInternalLogicClosedAndCleanedUp || this.IsConnectionChannelClosed; }
            protected set { this._isInternalLogicClosedAndCleanedUp = value; }
        }

        #endregion /IClientGateway ipml


        #region private implementations

        private int _counter;
        private string FormatIdentityString(Symbol symbol, string identityPrefix, bool isComposite)
        {
            return string.Format("{0}{1}{2}{3:D6}", isComposite ? "Composite_" : string.Empty,
                                 symbol == Common.Symbol.NULL ? "XXX" : symbol.ShortName, identityPrefix,
                                 Interlocked.Increment(ref _counter));
        }

        #endregion /private implementations


        protected class TradingInfoImpl : ITradingInfo
        {
            private bool _isOrderTransmissionEnabled = true;
            private TradingState _tradingState = TradingState.TradingAllowed;
            private decimal[] _usdConversionMultipliers;
            private decimal[] _symbolExchangeRates;
            private DateTime[] _usdConversionMultipliersTimeStamps;
            private DateTime[] _symbolExchangeRatesTimeStamps;
            private SymbolsInfoUtils _symbolsInfoUtils = new SymbolsInfoUtils(); 
            private ILogger _logger;
            private ClientGatewayBase _clientGatewayBase;
            private static readonly TimeSpan _ONLINE_PRICE_THRESHOLD_AGE = TimeSpan.FromMinutes(20);

            public TradingInfoImpl(ILogger logger, ClientGatewayBase clientGatewayBase)
            {
                this._logger = logger;
                this._clientGatewayBase = clientGatewayBase;
            }

            public void OnOrderTransmissionStatusChanged(
                OrderTransmissionStatusChangedEventArgs orderTransmissionStatusChangedEventArgs)
            {
                this._isOrderTransmissionEnabled = orderTransmissionStatusChangedEventArgs.IsOrderTransmissionEnabled;
                if (OrderTransmissionStatusChanged != null)
                {
                    OrderTransmissionStatusChanged(orderTransmissionStatusChangedEventArgs);
                }
            }

            public void OnTradingAllowedStatusChanged(
                TradingAllowedStatusChangedEventArgs tradingAllowedStatusChangedEventArgs)
            {
                this._tradingState = tradingAllowedStatusChangedEventArgs.TradingState;
                if (TradingAllowedStatusChanged != null)
                {
                    TradingAllowedStatusChanged(tradingAllowedStatusChangedEventArgs);
                }
            }

            public ISymbolsInfo Symbols
            {
                get { return this._symbolsInfoUtils; }
            }

            public bool IsOrderTransmissionEnabled
            {
                get { return this._isOrderTransmissionEnabled; }
            }

            public TradingState TradingState
            {
                get { return this._tradingState; }
            }

            public void UpdateExchangeRates(SymbolExchangeRatesArgs symbolExchangeRatesArgs)
            {
                if (symbolExchangeRatesArgs == null || symbolExchangeRatesArgs.SymbolExchangeRates == null ||
                    symbolExchangeRatesArgs.SymbolExchangeRates.Length != Symbol.ValuesCount ||
                    symbolExchangeRatesArgs.SymbolExchangeRatesTimeStamps == null ||
                    symbolExchangeRatesArgs.SymbolExchangeRatesTimeStamps.Length != Symbol.ValuesCount)
                {
                    this._logger.Log(LogLevel.Fatal,
                                     "Cannot update Exchange rates as the data coming from server are invalid (null or too short) - possible compilation mismatch");
                }
                else
                {
                    Interlocked.Exchange(ref this._symbolExchangeRates, symbolExchangeRatesArgs.SymbolExchangeRates);
                    Interlocked.Exchange(ref this._symbolExchangeRatesTimeStamps, symbolExchangeRatesArgs.SymbolExchangeRatesTimeStamps);

                    decimal[] tempArray = new decimal[Currency.ValuesCount];
                    DateTime[] stampsTempArray = new DateTime[Currency.ValuesCount];
                    tempArray[(int)Currency.USD] = 1m;
                    stampsTempArray[(int) Currency.USD] = DateTime.UtcNow;

                    foreach (Symbol symbol in Symbol.Values.Where(s => s.TermCurrency == Currency.USD && symbolExchangeRatesArgs.SymbolExchangeRates[(int)s] > 0m))
                    {
                        tempArray[(int)symbol.BaseCurrency] = symbolExchangeRatesArgs.SymbolExchangeRates[(int)symbol];
                        stampsTempArray[(int)symbol.BaseCurrency] = symbolExchangeRatesArgs.SymbolExchangeRatesTimeStamps[(int)symbol];
                    }

                    foreach (Symbol symbol in Symbol.Values.Where(s => s.BaseCurrency == Currency.USD && symbolExchangeRatesArgs.SymbolExchangeRates[(int)s] > 0m))
                    {
                        tempArray[(int)symbol.TermCurrency] = 1m / symbolExchangeRatesArgs.SymbolExchangeRates[(int)symbol];
                        stampsTempArray[(int)symbol.TermCurrency] = symbolExchangeRatesArgs.SymbolExchangeRatesTimeStamps[(int)symbol];
                    }

                    Interlocked.Exchange(ref this._usdConversionMultipliers, tempArray);
                    Interlocked.Exchange(ref this._usdConversionMultipliersTimeStamps, stampsTempArray);
                }
            }

            public void UpdateTradingTargetsSymbolsInfo(SymbolsInfoArgs symbolsInfoArgs)
            {
                this._symbolsInfoUtils.InitializeState(symbolsInfoArgs, this._logger);
            }

            public decimal? ConvertToUsd(Currency sourceCurrency, decimal sourceAmount, bool useOnline)
            {
                decimal? result = null;
                if (this._usdConversionMultipliers == null)
                {
                    this._logger.Log(LogLevel.Fatal, "Attempt to call ConvertToUsd but conversion multipliers are not yet initialzed (might be caused by version mismatch)");
                }
                else if (
                    !useOnline 
                    ||
                    DateTime.UtcNow - this._usdConversionMultipliersTimeStamps[(int) sourceCurrency] <=
                        _ONLINE_PRICE_THRESHOLD_AGE)
                {
                    result = sourceAmount * this._usdConversionMultipliers[(int) sourceCurrency];
                }

                return result;
            }

            public decimal? GetReferencePrice(Symbol symbol, bool useOnline)
            {
                decimal? result = null;
                if (this._symbolExchangeRates == null)
                {
                    this._logger.Log(LogLevel.Fatal, "Attempt to call GetReferencePrice but conversion multipliers are not yet initialzed (might be caused by version mismatch)");
                }
                else if (
                    !useOnline
                    ||
                    DateTime.UtcNow - this._symbolExchangeRatesTimeStamps[(int)symbol] <=
                        _ONLINE_PRICE_THRESHOLD_AGE)
                {
                    result = this._symbolExchangeRates[(int)symbol];
                }

                return result;
            }

            public event OrderTransmissionStatusChangedEventHandler OrderTransmissionStatusChanged;
            public event TradingAllowedStatusChangedEventHandler TradingAllowedStatusChanged;

            //TODO: Mount
            public event ImportantIntegratorInformationEventHandler ImportantIntegratorInformationArrived
            {
                add
                {
                    if (Interlocked.Increment(ref this._importantIntegratorInformationSubscribers) == 1)
                    {
                        var result = this._clientGatewayBase.SubmitRequestPrivate(
                            new MulticastRequestInfo(MulticastRequestType.FatalEmails, true));
                        if (!result.RequestSucceeded)
                        {
                            Interlocked.Decrement(ref this._importantIntegratorInformationSubscribers);
                            throw new ClientGatewayException(result.ErrorMessage);
                        }
                    }

                    this._importantIntegratorInformationArrived += value;
                }
                remove
                {
                    if (Interlocked.Decrement(ref this._importantIntegratorInformationSubscribers) == 0)
                    {
                        var result = this._clientGatewayBase.SubmitRequestPrivate(
                            new MulticastRequestInfo(MulticastRequestType.FatalEmails, false));
                        if (!result.RequestSucceeded)
                        {
                            Interlocked.Increment(ref this._importantIntegratorInformationSubscribers);
                            throw new ClientGatewayException(result.ErrorMessage);
                        }
                    }

                    this._importantIntegratorInformationArrived -= value;
                }
            }

            internal ImportantIntegratorInformationEventHandler _importantIntegratorInformationArrived;
            private int _importantIntegratorInformationSubscribers = 0;
        }

        private class ClientGatewayTargetsImpl : IClientGatewayTargets
        {
            public ClientGatewayTargetsImpl(IBankPoolTarget bankPool, IHotspotTarget hotspot, IFXallTarget fxall,
                ILmaxTarget lmax, IFXCMTarget fxcm, IFXCMStreamingTarget fxcmStreaming, IAllTargets allTargets)
            {
                this.BankPool = bankPool;
                this.Hotspot = hotspot;
                this.FXall = fxall;
                this.LMAX = lmax;
                this.FXCMTaking = fxcm;
                this.FXCMStreaming = fxcmStreaming;
                this.All = allTargets;
            }

            public IBankPoolTarget BankPool { get; private set; }
            public IHotspotTarget Hotspot { get; private set; }
            public IFXallTarget FXall { get; private set; }
            public ILmaxTarget LMAX { get; private set; }
            public IFXCMTarget FXCMTaking { get; private set; }

            public IFXCMStreamingTarget FXCMStreaming { get; private set; }
            public IAllTargets All { get; private set; }
        }

        private class VenuePricesTopOfBookHub<T> : IVenuePricesTopOfBookHub<T> where T : ICounterparty
        {
            private ClientGatewayBase _clientGatewayBase;

            public VenuePricesTopOfBookHub(ClientGatewayBase clientGatewayBase)
            {
                this._clientGatewayBase = clientGatewayBase;
            }

            public IntegratorRequestResult SubscribeToUpdates(TopOfBookIntegratorSubscriptionRequest<T> subscriptionRequestInfo)
            {
                return this._clientGatewayBase.Subscribe(subscriptionRequestInfo);
            }

            public IntegratorRequestResult UnsubscribeFromUpdates(TopOfBookIntegratorUnsubscriptionRequest<T> unsubscriptionRequestInfo)
            {
                return this._clientGatewayBase.Unsubscribe(unsubscriptionRequestInfo);
            }

            public TopOfBookIntegratorSubscriptionRequest<T> GetSubscriptionRequest(Symbol symbol, T counterparty)
            {
                return new TopOfBookIntegratorSubscriptionRequest<T>(symbol, counterparty);
            }

            public TopOfBookIntegratorUnsubscriptionRequest<T> GetUnsubscriptionRequest(Symbol symbol, T counterparty)
            {
                return new TopOfBookIntegratorUnsubscriptionRequest<T>(symbol, counterparty);
            }

            public void InvokePriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
            {
                if (this.NewUpdateAvailable != null)
                {
                    var eArgs = PricesTopOfBookUpdateEventArgs.CreateFromPriceUpdateEventArgs(priceUpdateEventArgs,
                                                                                              _clientGatewayBase._logger);
                    if (eArgs != null)
                    {
                        this.NewUpdateAvailable(eArgs);
                    }
                }
            }

            public event Action<PricesTopOfBookUpdateEventArgs> NewUpdateAvailable;
        }

        private class BankPoolPricesTopOfBookHub : IBankPoolPricesTopOfBookHub
        {
            private ClientGatewayBase _clientGatewayBase;

            public BankPoolPricesTopOfBookHub(ClientGatewayBase clientGatewayBase)
            {
                this._clientGatewayBase = clientGatewayBase;
            }

            public IntegratorRequestResult SubscribeToUpdates(TopOfBookIntegratorSubscriptionRequest<BankCounterparty> subscriptionRequestInfo)
            {
                return this._clientGatewayBase.Subscribe(subscriptionRequestInfo);
            }

            public IntegratorRequestResult UnsubscribeFromUpdates(TopOfBookIntegratorUnsubscriptionRequest<BankCounterparty> unsubscriptionRequestInfo)
            {
                return this._clientGatewayBase.Unsubscribe(unsubscriptionRequestInfo);
            }

            public TopOfBookIntegratorSubscriptionRequest<BankCounterparty> GetSubscriptionRequest(Symbol symbol)
            {
                return new TopOfBookIntegratorSubscriptionRequest<BankCounterparty>(symbol);
            }

            public TopOfBookIntegratorUnsubscriptionRequest<BankCounterparty> GetUnsubscriptionRequest(Symbol symbol)
            {
                return new TopOfBookIntegratorUnsubscriptionRequest<BankCounterparty>(symbol);
            }

            public void InvokePriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
            {
                if (this.NewUpdateAvailable != null)
                {
                    var eArgs = PricesTopOfBookUpdateEventArgs.CreateFromPriceUpdateEventArgs(priceUpdateEventArgs,
                                                                                              _clientGatewayBase._logger);
                    if (eArgs != null)
                    {
                        this.NewUpdateAvailable(eArgs);
                    }
                }
            }

            public event Action<PricesTopOfBookUpdateEventArgs> NewUpdateAvailable;
        }

        private class VenueDelasHub<T> : IVenueDealsHub<T> where T : ICounterparty
        {
            private ClientGatewayBase _clientGatewayBase;

            public VenueDelasHub(ClientGatewayBase clientGatewayBase)
            {
                this._clientGatewayBase = clientGatewayBase;
            }

            public IntegratorRequestResult SubscribeToUpdates(VenueDealsIntegratorSubscriptionRequest<T> subscriptionRequestInfo)
            {
                return _clientGatewayBase.Subscribe(subscriptionRequestInfo);
            }

            public IntegratorRequestResult UnsubscribeFromUpdates(VenueDealsIntegratorUnsubscriptionRequest<T> unsubscriptionRequestInfo)
            {
                return _clientGatewayBase.Unsubscribe(unsubscriptionRequestInfo);
            }

            public VenueDealsIntegratorSubscriptionRequest<T> GetSubscriptionRequest(Symbol symbol, T counterparty)
            {
                return new VenueDealsIntegratorSubscriptionRequest<T>(symbol, counterparty);
            }

            public VenueDealsIntegratorUnsubscriptionRequest<T> GetUnsubscriptionRequest(Symbol symbol, T counterparty)
            {
                return new VenueDealsIntegratorUnsubscriptionRequest<T>(symbol, counterparty);
            }

            public void InvokeNewVenueDealReported(VenueDealObject venueDealObject)
            {
                if (this.NewUpdateAvailable != null)
                {
                    this.NewUpdateAvailable(venueDealObject);
                }
            }

            public event NewVenueDealHandler NewUpdateAvailable;
        }

        private class PricesIndividualHub<T> : IPricesIndividualHub<T> where T : ICounterparty
        {
            private ClientGatewayBase _clientGatewayBase;

            public PricesIndividualHub(ClientGatewayBase clientGatewayBase)
            {
                this._clientGatewayBase = clientGatewayBase;
            }

            public IntegratorRequestResult SubscribeToUpdates(IndividualPricesIntegratorSubscriptionRequest<T> subscriptionRequestInfo)
            {
                return _clientGatewayBase.Subscribe(subscriptionRequestInfo);
            }

            public IntegratorRequestResult UnsubscribeFromUpdates(IndividualPricesIntegratorUnsubscriptionRequest<T> unsubscriptionRequestInfo)
            {
                return _clientGatewayBase.Unsubscribe(unsubscriptionRequestInfo);
            }

            public IndividualPricesIntegratorSubscriptionRequest<BankCounterparty> GetSubscriptionRequest(Symbol symbol)
            {
                return new IndividualPricesIntegratorSubscriptionRequest<BankCounterparty>(symbol);
            }

            public IndividualPricesIntegratorUnsubscriptionRequest<BankCounterparty> GetUnsubscriptionRequest(Symbol symbol)
            {
                return new IndividualPricesIntegratorUnsubscriptionRequest<BankCounterparty>(symbol);
            }

            public void InvokeNewUpdateAvailable(PriceIndividualUpdateEventArgs priceIndividualUpdateEventArgs)
            {
                if (this.NewUpdateAvailable != null)
                {
                    this.NewUpdateAvailable(priceIndividualUpdateEventArgs);
                }
            }

            public event Action<PriceIndividualUpdateEventArgs> NewUpdateAvailable;
        }

        private abstract class UnicastEventsTargetBase<T> : IFXCMStreamingEvents, IUnicastEventsTarget<T> where T : ClientOrderUpdateInfo
        {
            private ClientGatewayBase _clientGatewayBase;
            private UnicastEventsTargetBase<T> _forwardedInfoTarget;
            private bool _isForwardedEventsHub;

            protected ILogger Logger
            {
                get { return this._clientGatewayBase._logger; }
            }

            protected UnicastEventsTargetBase(ClientGatewayBase clientGatewayBase)
            {
                this._clientGatewayBase = clientGatewayBase;
            }

            protected UnicastEventsTargetBase(ClientGatewayBase clientGatewayBase, bool isForwardedEventsHub)
            {
                this._clientGatewayBase = clientGatewayBase;
                this._isForwardedEventsHub = isForwardedEventsHub;
            }

            protected UnicastEventsTargetBase<T> ForwardedInfoTarget 
            {
                set { this._forwardedInfoTarget = value; }
                get { return this._forwardedInfoTarget; }
            }

            public event NewDealHandler NewDealDone;
            public event NewUnconfirmedDealHandler NewUnconfirmedDealDone;
            public event CounterpartyRejectedIntegratorOrderHandler CounterpartyRejectedIntegratorOrder;
            public event CounterpartyIgnoredIntegratorOrderHandler CounterpartyIgnoredIntegratorOrder;
            public event ClientOrderUpdatedHandler<T> ClientOrderUpdated;

            public event NewStreamingDealAcceptedByIntegratorHandler NewStreamingDealAcceptedByIntegrator;
            public event NewStreamingDealRejectedByIntegratorHandler NewStreamingDealRejectedByIntegrator;
            public event NewStreamingDealRejectedByCounterpartyHandler NewStreamingDealRejectedByCounterparty;

            public void InvokeNewForwardedIntegratorUnicastInfo(IntegratorTradingInfoObjectBase forwardedUnicastInfo)
            {
                if (this._forwardedInfoTarget != null)
                {
                    this._forwardedInfoTarget.InvokeNewIntegratorUnicastInfo(forwardedUnicastInfo);
                }
                else
                {
                    Logger.Log(LogLevel.Fatal,
                                               "Receiving forwarded info, however forwarding is not implemented for this target, info: {0}",
                                               forwardedUnicastInfo);
                }
            }

            public void InvokeNewIntegratorUnicastInfo(IntegratorTradingInfoObjectBase unicastInfo)
            {
                if (unicastInfo is IntegratorUnconfirmedDealInternal)
                {
                    IntegratorUnconfirmedDealInternal integratorUnconfirmedDealInternal = unicastInfo as IntegratorUnconfirmedDealInternal;
                    this.InvokeNewUnconfirmedDealDone(integratorUnconfirmedDealInternal);
                }
                else if (unicastInfo is IntegratorDealInternal)
                {
                    IntegratorDealInternal integratorDealInternal = unicastInfo as IntegratorDealInternal;
                    this.InvokeNewDealDone(integratorDealInternal);
                }
                else if (unicastInfo is CounterpartyRejectionInfo)
                {
                    CounterpartyRejectionInfo counterpartyRejectionInfo = unicastInfo as CounterpartyRejectionInfo;
                    this.InvokeCounterpartyRejectedIntegratorOrder(counterpartyRejectionInfo);
                }
                else if (unicastInfo is CounterpartyOrderIgnoringInfo)
                {
                    CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo =
                        unicastInfo as CounterpartyOrderIgnoringInfo;
                    this.InvokeCounterpartyIgnoredIntegratorOrder(counterpartyOrderIgnoringInfo);
                }
                else if (unicastInfo is RejectableMMDeal)
                {
                    RejectableMMDeal rejectableMmDeal = unicastInfo as RejectableMMDeal;
                    this.InvokeStreamingDeal(rejectableMmDeal);
                }
                else if (unicastInfo is T)
                {
                    T clientOrderUpdateInfo = unicastInfo as T;

                    if (!this._isForwardedEventsHub)
                    {
                        //TODO: this is to be replaced in base, once all order related events have same base
                        clientOrderUpdateInfo.ClientOrder =
                            _clientGatewayBase._clientOrdersTracker.GetClientOrder(
                                clientOrderUpdateInfo.ClientOrderIdentity);

                        if (clientOrderUpdateInfo.ClientOrder == null)
                        {
                            Logger.Log(LogLevel.Error,
                                                           "Receiving ClientOrderUpdate event for unknown order - {0}",
                                                           clientOrderUpdateInfo);
                        }
                        //end todo
                    }

                    this.InvokeClientOrderUpdated(clientOrderUpdateInfo);
                }
                else
                {
                    Logger.Log(LogLevel.Fatal,
                                                   "Receiving unicast info of unknown type: {0}, info: {1}",
                                                   unicastInfo.GetType(), unicastInfo);
                }
            }

            private void InvokeNewDealDone(IntegratorDealInternal integratorDealInternal)
            {
                if (this.NewDealDone != null)
                {
                    this.NewDealDone(integratorDealInternal);
                }
            }

            private void InvokeNewUnconfirmedDealDone(IntegratorUnconfirmedDealInternal integratorUnconfirmedDealInternal)
            {
                if (this.NewUnconfirmedDealDone != null)
                {
                    this.NewUnconfirmedDealDone(integratorUnconfirmedDealInternal);
                }
            }

            private void InvokeCounterpartyRejectedIntegratorOrder(
                CounterpartyRejectionInfo counterpartyRejectionInfo)
            {
                if (this.CounterpartyRejectedIntegratorOrder != null)
                {
                    this.CounterpartyRejectedIntegratorOrder(counterpartyRejectionInfo);
                }
            }

            private void InvokeCounterpartyIgnoredIntegratorOrder(
                CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo)
            {
                if (this.CounterpartyIgnoredIntegratorOrder != null)
                {
                    this.CounterpartyIgnoredIntegratorOrder(counterpartyOrderIgnoringInfo);
                }
            }

            private void InvokeStreamingDeal(RejectableMMDeal rejectableMm)
            {
                switch (rejectableMm.CurrentDealStatus)
                {
                    case RejectableMMDeal.DealStatus.KgtConfirmed:
                        if (this.NewStreamingDealAcceptedByIntegrator != null)
                        {
                            this.NewStreamingDealAcceptedByIntegrator(rejectableMm);
                        }
                        break;
                    case RejectableMMDeal.DealStatus.CounterpartyRejected:
                        if (this.NewStreamingDealRejectedByCounterparty != null)
                        {
                            this.NewStreamingDealRejectedByCounterparty(rejectableMm);
                        }
                        break;
                    case RejectableMMDeal.DealStatus.Pending:
                        this.Logger.Log(LogLevel.Fatal, "Unexpected type [{0}] for streaming deal - not dispatching it: {1}",
                            rejectableMm.CurrentDealStatus, rejectableMm);
                        break;
                    default:
                        if (this.NewStreamingDealRejectedByIntegrator != null)
                        {
                            this.NewStreamingDealRejectedByIntegrator(rejectableMm);
                        }
                        break;
                }
            }

            private void InvokeClientOrderUpdated(T clientOrderUpdateInfo)
            {
                if (this.ClientOrderUpdated != null)
                {
                    this.ClientOrderUpdated(clientOrderUpdateInfo);
                }
            }
        }

        private class AllClientsUnicastForwarding<T> : UnicastEventsTargetBase<T>, IFXCMAllClients, IAllClientsUnicastForwarding<T> where T : ClientOrderUpdateInfo
        {
            private ClientGatewayBase _clientGatewayBase;
            private TradingTargetType _tradingTargetType;

            public AllClientsUnicastForwarding(ClientGatewayBase clientGatewayBase, TradingTargetType tradingTargetType, bool isForwardedEventsHub) 
                //indicade that this is a forwarded events hub
                : base(clientGatewayBase, isForwardedEventsHub)
            {
                this._clientGatewayBase = clientGatewayBase;
                this._tradingTargetType = tradingTargetType;
            }

            private ForwardingRequestType ClientForwardingRequestType2ForwardingRequestType(
                ClientForwardingRequestType clientForwardingRequestType)
            {
                switch (clientForwardingRequestType)
                {
                    case ClientForwardingRequestType.Deals:
                        return ForwardingRequestType.Deals;
                    case ClientForwardingRequestType.UnconfirmedDeals:
                        return ForwardingRequestType.UnconfirmedDeals;
                    case ClientForwardingRequestType.OrderUpdates:
                        return ForwardingRequestType.OrderUpdates;
                    case ClientForwardingRequestType.CounterpartyRejections:
                        return ForwardingRequestType.CounterpartyRejections;
                    case ClientForwardingRequestType.CounterpartyIgnores:
                        return ForwardingRequestType.CounterpartyIgnores;
                    default:
                        throw new ClientGatewayException(
                            string.Format("Unexpected ClientForwardingRequestType value: {0}",
                                          clientForwardingRequestType));
                }
            }

            //private bool CanSubscribe(ClientForwardingRequestType requestType)
            //{
            //    return
            //        (this._tradingTargetType == TradingTargetType.FXCMMM &&
            //         requestType == ClientForwardingRequestType.StreamingDealsAndRejects)
            //        ||
            //        (this._tradingTargetType != TradingTargetType.FXCMMM &&
            //         requestType != ClientForwardingRequestType.StreamingDealsAndRejects);
            //}

            private IntegratorRequestResult SubmitSubscribtionRequest(ClientForwardingRequest forwardingRequest, bool subscribe)
            {
                IntegratorRequestResult result = null;

                foreach (
                    ClientForwardingRequestType flagsEnumIndividualValue in
                        forwardingRequest.RequestType.GetFlagsEnumIndividualValues<ClientForwardingRequestType>())
                {
                    MulticastRequestInfo requestInfo =
                        new MulticastRequestInfo(this._tradingTargetType, forwardingRequest.Symbol,
                            ClientForwardingRequestType2ForwardingRequestType(flagsEnumIndividualValue),
                            subscribe);

                    result = IntegratorRequestResult.Combine(result,
                        this._clientGatewayBase.SubmitRequestPrivate(requestInfo));
                }

                return result;
            }

            public IntegratorRequestResult SubscribeToUpdates(ClientForwardingRequest forwardingRequest)
            {
                return this.SubmitSubscribtionRequest(forwardingRequest, true);
            }

            public IntegratorRequestResult UnsubscribeFromUpdates(ClientForwardingRequest forwardingRequest)
            {
                return this.SubmitSubscribtionRequest(forwardingRequest, false);
            }


            public IntegratorRequestResult SubscribeToUpdates(ClientStreamingForwardingRequest forwardingRequest)
            {
                return
                    this._clientGatewayBase.SubmitRequestPrivate(
                        new MulticastRequestInfo(this._tradingTargetType, forwardingRequest.Symbol,
                            ForwardingRequestType.RejectableStreamingDeal, true));
            }

            public IntegratorRequestResult UnsubscribeFromUpdates(ClientStreamingForwardingRequest forwardingRequest)
            {
                return
                    this._clientGatewayBase.SubmitRequestPrivate(
                        new MulticastRequestInfo(this._tradingTargetType, forwardingRequest.Symbol,
                            ForwardingRequestType.RejectableStreamingDeal, false));
            }
        }


        public interface ITradingTargetImplementation
        {
            TradingTargetType TradingTargetType { get; }
            void InvokePriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs);
            void InvokeNewIntegratorUnicastInfo(IntegratorTradingInfoObjectBase unicastInfo);
            void InvokeNewForwardedIntegratorUnicastInfo(IntegratorTradingInfoObjectBase unicastInfo);
            void InvokeNewVenueDealReported(VenueDealObject venueDealObject);
        }

        private abstract class TradingTargetImplementationBase<T> : AllClientsUnicastForwarding<T>, ITradingTargetImplementation where T : ClientOrderUpdateInfo
        {
            protected TradingTargetImplementationBase(ClientGatewayBase clientGatewayBase, TradingTargetType tradingTargetType, bool isForwardedEventsHub)
                :base(clientGatewayBase, tradingTargetType, isForwardedEventsHub)
            {
                this.TradingTargetType = tradingTargetType;
            }

            public TradingTargetType TradingTargetType { get; private set; }
            public abstract void InvokePriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs);
            public virtual void InvokeNewVenueDealReported(VenueDealObject venueDealObject)
            {
                this.Logger.Log(LogLevel.Fatal, "Receiving venue deal related event for counterparty not supporting venue deals. {0}", venueDealObject);
            }
        }

        private abstract class UnicastEventsAndForwardsTargetBase<T> : TradingTargetImplementationBase<T>, IUnicastEventsAndForwardsTarget<T> where T : ClientOrderUpdateInfo
        {
            protected UnicastEventsAndForwardsTargetBase(ClientGatewayBase clientGatewayBase, TradingTargetType tradingTargetType)
                : base(clientGatewayBase, tradingTargetType, false)
            {
                var forwardingTarget =
                    new AllClientsUnicastForwarding<T>(clientGatewayBase, tradingTargetType, true);
                this.ForwardedInfoTarget = forwardingTarget;
                this.AllClients = forwardingTarget;
            }

            public IAllClientsUnicastForwarding<T> AllClients { get; private set; }
        }

        private class HotspotTarget : UnicastEventsAndForwardsTargetBase<HotspotClientOrderUpdateInfo>, IHotspotTarget
        {
            private ClientGatewayBase _clientGatewayBase;
            private VenuePricesTopOfBookHub<HotspotCounterparty> _pricesTopOfBookHub;
            private VenueDelasHub<HotspotCounterparty> _venueDelasHub;

            public HotspotTarget(ClientGatewayBase clientGatewayBase)
                : base(clientGatewayBase, TradingTargetType.Hotspot)
            {
                this._clientGatewayBase = clientGatewayBase;
                this._pricesTopOfBookHub = new VenuePricesTopOfBookHub<HotspotCounterparty>(clientGatewayBase);
                this._venueDelasHub = new VenueDelasHub<HotspotCounterparty>(clientGatewayBase);
            }

            public IVenuePricesTopOfBookHub<HotspotCounterparty> PricesTopOfBook 
            {
                get { return this._pricesTopOfBookHub; }
            }

            public IVenueDealsHub<HotspotCounterparty> VenueDeals
            {
                get { return this._venueDelasHub; }
            }

            public HotspotClientOrderRequestInfo CreateLimitOrderRequest(decimal sizeBaseAbsInitial,
                                                                         decimal sizeBaseAbsFillMinimum,
                                                                         decimal requestedPrice, DealDirection side,
                                                                         Symbol symbol,
                                                                         TimeInForce timeInForce, HotspotCounterparty hotspotCounterparty, bool isRiskRemovingOrder)
            {
                if (!_clientGatewayBase.TradingInfo.Symbols.IsPriceWithinPriceIncrementLimit(requestedPrice, symbol, TradingTargetType.Hotspot))
                    throw new ClientGatewayException("Requested price is not within price granularity limits (or the symbol is not supported or price increment is not configured)");
                if (!_clientGatewayBase.TradingInfo.Symbols.IsOrderSizeWithinSizeAndIncrementLimit(sizeBaseAbsInitial, symbol, TradingTargetType.Hotspot))
                    throw new ClientGatewayException("Requested initial size is not within size granularity limits (or the symbol is not supported or size limits are not configured)");
                if (!_clientGatewayBase.TradingInfo.Symbols.IsOrderMinSizeZeroOrWithinSizeAndIncrementLimit(sizeBaseAbsFillMinimum, symbol, TradingTargetType.Hotspot))
                    throw new ClientGatewayException("Requested minimum fill size is not within size granularity limits (or the symbol is not supported or size limits are not configured)");
                return HotspotClientOrderRequestInfo.CreateHotspotOrder(sizeBaseAbsInitial, sizeBaseAbsFillMinimum,
                                                                        requestedPrice, side, symbol, timeInForce, hotspotCounterparty, isRiskRemovingOrder);
            }

            public HotspotClientOrderRequestInfo CreateDayPegOrderRequest(decimal sizeBaseAbsInitial,
                                                                          decimal sizeBaseAbsFillMinimum,
                                                                          decimal limitPrice,
                                                                          PegType pegType,
                                                                          decimal pegDifferenceBasePolAlwaysAdded,
                                                                          DealDirection side, Symbol symbol,
                                                                          HotspotCounterparty hotspotCounterparty, bool isRiskRemovingOrder)
            {
                if (!_clientGatewayBase.TradingInfo.Symbols.IsPriceWithinPriceIncrementLimit(limitPrice, symbol, TradingTargetType.Hotspot))
                    throw new ClientGatewayException("Requested price is not within price granularity limits (or the symbol is not supported or granularity is not configured)");
                if (!_clientGatewayBase.TradingInfo.Symbols.IsPriceWithinPriceIncrementLimit(pegDifferenceBasePolAlwaysAdded, symbol, TradingTargetType.Hotspot))
                    throw new ClientGatewayException("Requested peg differnce is not within price granularity limits (or the symbol is not supported or granularity is not configured)");
                if (!_clientGatewayBase.TradingInfo.Symbols.IsOrderSizeWithinSizeAndIncrementLimit(sizeBaseAbsInitial, symbol, TradingTargetType.Hotspot))
                    throw new ClientGatewayException("Requested initial size is not within size granularity limits (or the symbol is not supported or size limits are not configured)");
                if (!_clientGatewayBase.TradingInfo.Symbols.IsOrderMinSizeZeroOrWithinSizeAndIncrementLimit(sizeBaseAbsFillMinimum, symbol, TradingTargetType.Hotspot))
                    throw new ClientGatewayException("Requested minimum fill size is not within size granularity limits (or the symbol is not supported or size limits are not configured)");
                return HotspotClientOrderRequestInfo.CreateHotspotDayPegOrder(sizeBaseAbsInitial, sizeBaseAbsFillMinimum,
                                                                              limitPrice, pegType,
                                                                              pegDifferenceBasePolAlwaysAdded, side,
                                                                              symbol, hotspotCounterparty, isRiskRemovingOrder);
            }

            public HotspotClientOrderRequestInfo CreateOrderReplaceRequest(HotspotClientOrder clientOrderToCancel,
                                                                           HotspotClientOrderRequestInfo
                                                                               replacementOrderInfo)
            {
                return HotspotClientOrderRequestInfo.CreateHotspotOrderReplaceRequest(clientOrderToCancel,
                                                                                      replacementOrderInfo);
            }

            public HotspotClientOrderRequestInfo CreateOrderReplaceRequest(string clientOrderToCancelId,
                                                                           HotspotClientOrderRequestInfo
                                                                               replacementOrderInfo)
            {
                return HotspotClientOrderRequestInfo.CreateHotspotOrderReplaceRequest(clientOrderToCancelId,
                                                                                      replacementOrderInfo);
            }

            public HotspotClientOrder CreateClientOrder(HotspotClientOrderRequestInfo orderRequestInfo)
            {
                return
                    new HotspotClientOrder(
                        _clientGatewayBase.FormatIdentityString(orderRequestInfo.Symbol,
                                                                _clientGatewayBase.ClientIdentityInternal, false),
                        _clientGatewayBase.ClientIdentityInternal,
                        orderRequestInfo);
            }

            public IntegratorRequestResult SubmitOrder(HotspotClientOrder clientOrder)
            {
                return _clientGatewayBase.SubmitOrder(clientOrder);
            }

            public IntegratorRequestResult CancelOrder(string clientOrderId)
            {
                return _clientGatewayBase.CancelOrder(clientOrderId, TradingTargetType.Hotspot);
            }

            public IntegratorRequestResult CancelOrder(HotspotClientOrder clientOrder)
            {
                return _clientGatewayBase.CancelOrder(clientOrder);
            }

            //public IntegratorRequestResult FinalizeUnconfirmedDeal(DealConfirmationAction dealConfirmationAction,
            //                                                       IntegratorUnconfirmedDealInternal
            //                                                           integratorUnconfirmedDealInternal)
            //{
            //    return
            //        _clientGatewayBase.SubmitRequestPrivate(
            //            new IntegratorDealInternalFinalizationRequest(dealConfirmationAction,
            //                                                          integratorUnconfirmedDealInternal));
            //}

            public override void InvokePriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
            {
                this._pricesTopOfBookHub.InvokePriceUpdate(priceUpdateEventArgs);
            }

            public override void InvokeNewVenueDealReported(VenueDealObject venueDealObject)
            {
                this._venueDelasHub.InvokeNewVenueDealReported(venueDealObject);
            }
        }

        private class FXallTarget : UnicastEventsAndForwardsTargetBase<FXallClientOrderUpdateInfo>, IFXallTarget
        {
            private ClientGatewayBase _clientGatewayBase;
            private VenuePricesTopOfBookHub<FXallCounterparty> _pricesTopOfBookHub;
            //private PricesIndividualHub _pricesIndividualHub;

            public FXallTarget(ClientGatewayBase clientGatewayBase)
                :base(clientGatewayBase, TradingTargetType.FXall)
            {
                this._clientGatewayBase = clientGatewayBase;
                this._pricesTopOfBookHub = new VenuePricesTopOfBookHub<FXallCounterparty>(clientGatewayBase);
                //this._pricesIndividualHub = new PricesIndividualHub(clientGatewayBase, TradingTargetType.FXall);
            }

            //public IPricesIndividualHub PricesIndividual
            //{
            //    get { return this._pricesIndividualHub; }
            //}

            public IVenuePricesTopOfBookHub<FXallCounterparty> PricesTopOfBook
            {
                get { return this._pricesTopOfBookHub; }
            }

            public FXallClientOrderRequestInfo CreateOrderReplaceRequest(FXallClientOrder clientOrderToCancel, FXallClientOrderRequestInfo replacementOrderInfo)
            {
                return FXallClientOrderRequestInfo.CreateFXallOrderReplaceRequest(clientOrderToCancel,
                                                                                      replacementOrderInfo);
            }

            public FXallClientOrderRequestInfo CreateOrderReplaceRequest(string clientOrderToCancelId, FXallClientOrderRequestInfo replacementOrderInfo)
            {
                return FXallClientOrderRequestInfo.CreateFXallOrderReplaceRequest(clientOrderToCancelId,
                                                                                      replacementOrderInfo);
            }

            public FXallClientOrderRequestInfo CreateLimitOrderRequest(decimal sizeBaseAbsInitial, decimal sizeBaseAbsFillMinimum, decimal requestedPrice, DealDirection side, Symbol symbol, TimeInForce timeInForce, bool isRiskRemovingOrder)
            {
                if (!_clientGatewayBase.TradingInfo.Symbols.IsPriceWithinPriceIncrementLimit(requestedPrice, symbol, TradingTargetType.FXall))
                    throw new ClientGatewayException("Requested price is not within price granularity limits (or the symbol is not supported or granularity is not configured)");
                if (!_clientGatewayBase.TradingInfo.Symbols.IsOrderSizeWithinSizeAndIncrementLimit(sizeBaseAbsInitial, symbol, TradingTargetType.FXall))
                    throw new ClientGatewayException("Requested initial size is not within size granularity limits (or the symbol is not supported or size limits are not configured)");
                if (!_clientGatewayBase.TradingInfo.Symbols.IsOrderMinSizeZeroOrWithinSizeAndIncrementLimit(sizeBaseAbsFillMinimum, symbol, TradingTargetType.FXall))
                    throw new ClientGatewayException("Requested minimum fill size is not within size granularity limits (or the symbol is not supported or size limits are not configured)");
                return FXallClientOrderRequestInfo.CreateFXallOrder(sizeBaseAbsInitial, sizeBaseAbsFillMinimum,
                                                                        requestedPrice, side, symbol, timeInForce, isRiskRemovingOrder);
            }

            public FXallClientOrder CreateClientOrder(FXallClientOrderRequestInfo orderRequestInfo)
            {
                return
                    new FXallClientOrder(
                        _clientGatewayBase.FormatIdentityString(orderRequestInfo.Symbol,
                                                                _clientGatewayBase.ClientIdentityInternal, false),
                        _clientGatewayBase.ClientIdentityInternal,
                        orderRequestInfo);
            }

            public IntegratorRequestResult SubmitOrder(FXallClientOrder clientOrder)
            {
                return _clientGatewayBase.SubmitOrder(clientOrder);
            }

            public IntegratorRequestResult CancelOrder(string clientOrderId)
            {
                return _clientGatewayBase.CancelOrder(clientOrderId, TradingTargetType.FXall);
            }

            public IntegratorRequestResult CancelOrder(FXallClientOrder clientOrder)
            {
                return _clientGatewayBase.CancelOrder(clientOrder);
            }

            public override void InvokePriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
            {
                //if (PriceIndividualUpdateEventArgs.IsMatchingEvent(priceUpdateEventArgs))
                //{
                //    var eArgs = PriceIndividualUpdateEventArgs
                //        .CreateFromPriceUpdateEventArgs(priceUpdateEventArgs, this._clientGatewayBase._logger);
                //    if (eArgs != null)
                //    {
                //        this._pricesIndividualHub.InvokeNewUpdateAvailable(eArgs);
                //    }
                //}
                //else
                //{
                    this._pricesTopOfBookHub.InvokePriceUpdate(priceUpdateEventArgs);
                //}
            }
        }

        private class FXCMStreamingTarget : TradingTargetImplementationBase<ClientOrderUpdateInfo>, IFXCMStreamingTarget, IFXCMAllClients
        {
            public FXCMStreamingTarget(ClientGatewayBase clientGatewayBase)
                : base(clientGatewayBase, TradingTargetType.FXCMMM, true)
            {
                this.ForwardedInfoTarget = this;
            }

            public override void InvokePriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
            {
                this.Logger.Log(LogLevel.Fatal, "Invoking PriceUpdate on FXCMStreamingTarget - not supported");
            }

            public IFXCMAllClients AllClients { get { return this; }}
        }

        private class FXCMTarget : UnicastEventsAndForwardsTargetBase<FXCMClientOrderUpdateInfo>, IFXCMTarget
        {
            private ClientGatewayBase _clientGatewayBase;
            private VenuePricesTopOfBookHub<FXCMCounterparty> _pricesTopOfBookHub;
            //private PricesIndividualHub _pricesIndividualHub;

            public FXCMTarget(ClientGatewayBase clientGatewayBase)
                : base(clientGatewayBase, TradingTargetType.FXCM)
            {
                this._clientGatewayBase = clientGatewayBase;
                this._pricesTopOfBookHub = new VenuePricesTopOfBookHub<FXCMCounterparty>(clientGatewayBase);
                //this._pricesIndividualHub = new PricesIndividualHub(clientGatewayBase, TradingTargetType.FXall);
            }

            //public IPricesIndividualHub PricesIndividual
            //{
            //    get { return this._pricesIndividualHub; }
            //}

            public IVenuePricesTopOfBookHub<FXCMCounterparty> PricesTopOfBook
            {
                get { return this._pricesTopOfBookHub; }
            }

            public FXCMClientOrderRequestInfo CreateOrderReplaceRequest(FXCMClientOrder clientOrderToCancel, FXCMClientOrderRequestInfo replacementOrderInfo)
            {
                return FXCMClientOrderRequestInfo.CreateFXCMOrderReplaceRequest(clientOrderToCancel,
                                                                                      replacementOrderInfo);
            }

            public FXCMClientOrderRequestInfo CreateOrderReplaceRequest(string clientOrderToCancelId, FXCMClientOrderRequestInfo replacementOrderInfo)
            {
                return FXCMClientOrderRequestInfo.CreateFXCMOrderReplaceRequest(clientOrderToCancelId,
                                                                                      replacementOrderInfo);
            }

            public FXCMClientOrderRequestInfo CreateLimitOrderRequest(decimal sizeBaseAbsInitial, decimal sizeBaseAbsFillMinimum, decimal requestedPrice, DealDirection side, Symbol symbol, TimeInForce timeInForce, FXCMCounterparty fxcmCounterparty, bool isRiskRemovingOrder)
            {
                if (!_clientGatewayBase.TradingInfo.Symbols.IsPriceWithinPriceIncrementLimit(requestedPrice, symbol, TradingTargetType.FXCM))
                    throw new ClientGatewayException("Requested price is not within price granularity limits (or the symbol is not supported or granularity is not configured)");
                if (!_clientGatewayBase.TradingInfo.Symbols.IsOrderSizeWithinSizeAndIncrementLimit(sizeBaseAbsInitial, symbol, TradingTargetType.FXCM))
                    throw new ClientGatewayException("Requested initial size is not within size granularity limits (or the symbol is not supported or size limits are not configured)");
                if (!_clientGatewayBase.TradingInfo.Symbols.IsOrderMinSizeZeroOrWithinSizeAndIncrementLimit(sizeBaseAbsFillMinimum, symbol, TradingTargetType.FXCM))
                    throw new ClientGatewayException("Requested minimum fill size is not within size granularity limits (or the symbol is not supported or size limits are not configured)");
                return FXCMClientOrderRequestInfo.CreateFXCMOrder(sizeBaseAbsInitial, sizeBaseAbsFillMinimum,
                                                                        requestedPrice, side, symbol, timeInForce, fxcmCounterparty, isRiskRemovingOrder);
            }

            public FXCMClientOrder CreateClientOrder(FXCMClientOrderRequestInfo orderRequestInfo)
            {
                return
                    new FXCMClientOrder(
                        _clientGatewayBase.FormatIdentityString(orderRequestInfo.Symbol,
                                                                _clientGatewayBase.ClientIdentityInternal, false),
                        _clientGatewayBase.ClientIdentityInternal,
                        orderRequestInfo);
            }

            public IntegratorRequestResult SubmitOrder(FXCMClientOrder clientOrder)
            {
                return _clientGatewayBase.SubmitOrder(clientOrder);
            }

            public IntegratorRequestResult CancelOrder(string clientOrderId)
            {
                return _clientGatewayBase.CancelOrder(clientOrderId, TradingTargetType.FXCM);
            }

            public IntegratorRequestResult CancelOrder(FXCMClientOrder clientOrder)
            {
                return _clientGatewayBase.CancelOrder(clientOrder);
            }

            public override void InvokePriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
            {
                //if (PriceIndividualUpdateEventArgs.IsMatchingEvent(priceUpdateEventArgs))
                //{
                //    var eArgs = PriceIndividualUpdateEventArgs
                //        .CreateFromPriceUpdateEventArgs(priceUpdateEventArgs, this._clientGatewayBase._logger);
                //    if (eArgs != null)
                //    {
                //        this._pricesIndividualHub.InvokeNewUpdateAvailable(eArgs);
                //    }
                //}
                //else
                //{
                this._pricesTopOfBookHub.InvokePriceUpdate(priceUpdateEventArgs);
                //}
            }
        }

        private class LmaxTarget : UnicastEventsAndForwardsTargetBase<LmaxClientOrderUpdateInfo>, ILmaxTarget
        {
            private ClientGatewayBase _clientGatewayBase;
            private VenuePricesTopOfBookHub<LmaxCounterparty> _pricesTopOfBookHub;

            public LmaxTarget(ClientGatewayBase clientGatewayBase)
                :base(clientGatewayBase, TradingTargetType.LMAX)
            {
                this._clientGatewayBase = clientGatewayBase;
                this._pricesTopOfBookHub = new VenuePricesTopOfBookHub<LmaxCounterparty>(clientGatewayBase);
            }

            public IVenuePricesTopOfBookHub<LmaxCounterparty> PricesTopOfBook
            {
                get { return this._pricesTopOfBookHub; }
            }

            public LmaxClientOrderRequestInfo CreateOrderReplaceRequest(LmaxClientOrder clientOrderToCancel, LmaxClientOrderRequestInfo replacementOrderInfo)
            {
                return LmaxClientOrderRequestInfo.CreateLmaxOrderReplaceRequest(clientOrderToCancel,
                                                                                      replacementOrderInfo);
            }

            public LmaxClientOrderRequestInfo CreateOrderReplaceRequest(string clientOrderToCancelId, LmaxClientOrderRequestInfo replacementOrderInfo)
            {
                return LmaxClientOrderRequestInfo.CreateLmaxOrderReplaceRequest(clientOrderToCancelId,
                                                                                      replacementOrderInfo);
            }

            [Obsolete("Use overload specifying the LMAX counterparty")]
            public LmaxClientOrderRequestInfo CreateLimitOrderRequest(decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side, Symbol symbol, TimeInForce timeInForce, bool isRiskRemovingOrder)
            {
                return this.CreateLimitOrderRequest(sizeBaseAbsInitial, requestedPrice, side, symbol, timeInForce,
                                             LmaxCounterparty.L01, isRiskRemovingOrder);
            }

            public LmaxClientOrderRequestInfo CreateLimitOrderRequest(decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side, Symbol symbol, TimeInForce timeInForce, LmaxCounterparty lmaxCounterparty, bool isRiskRemovingOrder)
            {
                if (!_clientGatewayBase.TradingInfo.Symbols.IsPriceWithinPriceIncrementLimit(requestedPrice, symbol, TradingTargetType.LMAX))
                    throw new ClientGatewayException("Requested price is not within price granularity limits (or the symbol is not supported or granularity is not configured)");
                if (!_clientGatewayBase.TradingInfo.Symbols.IsOrderSizeWithinSizeAndIncrementLimit(sizeBaseAbsInitial, symbol, TradingTargetType.LMAX))
                    throw new ClientGatewayException("Requested initial size is not within size granularity limits (or the symbol is not supported or size limits are not configured)");
                return LmaxClientOrderRequestInfo.CreateLmaxOrder(sizeBaseAbsInitial, requestedPrice, side, symbol, timeInForce, lmaxCounterparty, isRiskRemovingOrder);
            }

            public LmaxClientOrder CreateClientOrder(LmaxClientOrderRequestInfo orderRequestInfo)
            {
                return
                    new LmaxClientOrder(
                        _clientGatewayBase.FormatIdentityString(orderRequestInfo.Symbol,
                                                                _clientGatewayBase.ClientIdentityInternal, false),
                        _clientGatewayBase.ClientIdentityInternal,
                        orderRequestInfo);
            }

            public IntegratorRequestResult SubmitOrder(LmaxClientOrder clientOrder)
            {
                return _clientGatewayBase.SubmitOrder(clientOrder);
            }

            public IntegratorRequestResult CancelOrder(string clientOrderId)
            {
                return _clientGatewayBase.CancelOrder(clientOrderId, TradingTargetType.LMAX);
            }

            public IntegratorRequestResult CancelOrder(LmaxClientOrder clientOrder)
            {
                return _clientGatewayBase.CancelOrder(clientOrder);
            }

            public override void InvokePriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
            {
                this._pricesTopOfBookHub.InvokePriceUpdate(priceUpdateEventArgs);
            }
        }

        private class BankPoolTarget : UnicastEventsAndForwardsTargetBase<BankPoolClientOrderUpdateInfo>, IBankPoolTarget
        {
            private ClientGatewayBase _clientGatewayBase;
            private BankPoolPricesTopOfBookHub _pricesTopOfBookHub;
            private PricesIndividualHub<BankCounterparty> _pricesIndividualHub;

            public BankPoolTarget(ClientGatewayBase clientGatewayBase)
                :base(clientGatewayBase, TradingTargetType.BankPool)
            {
                this._clientGatewayBase = clientGatewayBase;
                this._pricesTopOfBookHub = new BankPoolPricesTopOfBookHub(clientGatewayBase);
                this._pricesIndividualHub = new PricesIndividualHub<BankCounterparty>(clientGatewayBase);
            }

            public IPricesIndividualHub<BankCounterparty> PricesIndividual
            {
                get { return this._pricesIndividualHub; }
            }

            public IBankPoolPricesTopOfBookHub PricesTopOfBook
            {
                get { return this._pricesTopOfBookHub; }
            }

            public BankPoolClientOrderRequestInfo CreateLimitOrderRequest(decimal sizeBaseAbsInitial, decimal requestedPrice,
                                                                   DealDirection side, Symbol symbol, bool isRiskRemovingOrder)
            {
                if (_clientGatewayBase.CheckOrdersGranularity && !_clientGatewayBase.TradingInfo.Symbols.IsPriceWithinPriceIncrementLimit(requestedPrice, symbol, TradingTargetType.BankPool))
                    throw new ClientGatewayException("Requested price is not within price granularity limits (or the symbol is not supported or granularity is not configured)");
                if (_clientGatewayBase.CheckOrdersGranularity && !_clientGatewayBase.TradingInfo.Symbols.IsOrderSizeWithinSizeAndIncrementLimit(sizeBaseAbsInitial, symbol, TradingTargetType.BankPool))
                    throw new ClientGatewayException("Requested initial size is not within size granularity limits (or the symbol is not supported or size limits are not configured)");
                return BankPoolClientOrderRequestInfo.CreateLimit(sizeBaseAbsInitial, requestedPrice, side, symbol, isRiskRemovingOrder);
            }

            public BankPoolClientOrderRequestInfo CreateMarketOrderRequest(decimal sizeBaseAbsInitial,
                                                                           DealDirection side,
                                                                           Symbol symbol, bool isRiskRemovingOrder)
            {
                return BankPoolClientOrderRequestInfo.CreateMarket(sizeBaseAbsInitial, side, symbol, isRiskRemovingOrder);
            }

            public BankPoolClientOrderRequestInfo CreateMarketOnImprovementOrderRequest(decimal sizeBaseAbsInitial,
                                                                            TimeSpan waitOnImprovementTimeout,
                                                                           DealDirection side,
                                                                           Symbol symbol, bool isRiskRemovingOrder)
            {
                return BankPoolClientOrderRequestInfo.CreateMarketOnImprovement(sizeBaseAbsInitial, waitOnImprovementTimeout, side, symbol, isRiskRemovingOrder);
            }

            public BankPoolClientOrderRequestInfo CreateQuotedOrderRequest(decimal sizeBaseAbsInitial,
                                                                           decimal requestedPrice,
                                                                           DealDirection side,
                                                                           PriceObject price, bool isRiskRemovingOrder)
            {
                return BankPoolClientOrderRequestInfo.CreateQuoted(sizeBaseAbsInitial, requestedPrice, side, price, isRiskRemovingOrder);
            }

            public BankPoolClientOrder CreateClientOrder(BankPoolClientOrderRequestInfo orderRequestInfo)
            {
                return new BankPoolClientOrder(_clientGatewayBase.FormatIdentityString(orderRequestInfo.Symbol, _clientGatewayBase.ClientIdentityInternal, false), _clientGatewayBase.ClientIdentityInternal,
                                       orderRequestInfo);
            }

            public IntegratorRequestResult SubmitOrder(BankPoolClientOrder clientOrder)
            {
                return _clientGatewayBase.SubmitOrder(clientOrder);
            }

            public IntegratorRequestResult CancelOrder(string clientOrderId)
            {
                return _clientGatewayBase.CancelOrder(clientOrderId, TradingTargetType.BankPool);
            }

            public IntegratorRequestResult CancelOrder(BankPoolClientOrder clientOrder)
            {
                return this.CancelOrder(clientOrder.ClientOrderIdentity);
            }

            public override void InvokePriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
            {
                if (PriceIndividualUpdateEventArgs.IsMatchingEvent(priceUpdateEventArgs))
                {
                    var eArgs = PriceIndividualUpdateEventArgs
                        .CreateFromPriceUpdateEventArgs(priceUpdateEventArgs,this._clientGatewayBase._logger);
                    if (eArgs != null)
                    {
                        this._pricesIndividualHub.InvokeNewUpdateAvailable(eArgs);
                    }
                }
                else
                {
                    this._pricesTopOfBookHub.InvokePriceUpdate(priceUpdateEventArgs);
                }    
            }
        }

        private class AllTarget: IAllTargets
        {
            private ClientGatewayBase _clientGatewayBase;

            public AllTarget(ClientGatewayBase clientGatewayBase)
            {
                this._clientGatewayBase = clientGatewayBase;
            }

            public IntegratorRequestResult CancelAllMyOrders()
            {
                _clientGatewayBase._logger.Log(LogLevel.Debug, "Client [{0}] is cancelling ALL it's orders", _clientGatewayBase.ClientIdentity);
                IntegratorRequestResult result = _clientGatewayBase.CancelAllMyOrdersPrivate();

                if (!result.RequestSucceeded)
                {
                    _clientGatewayBase._logger.Log(LogLevel.Warn, "Client [{0}] FAILED to cancel ALL it's orders. Reason: {1}", _clientGatewayBase.ClientIdentity, result.ErrorMessage);
                }

                return result;
            }
        }
    }

}
