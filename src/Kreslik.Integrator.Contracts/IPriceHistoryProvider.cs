﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public interface IPriceHistoryProvider
    {
        IEnumerable<Tuple<decimal, PriceSide, DateTime>> GetLastPricesForSymbol(Symbol symbol, int pricesCount);
        IEnumerable<Tuple<Symbol, decimal, DateTime>> GetLastKnownReferencePrices();
        void UpdateReferencePrices(string askPricesArgList, string bidPricesArgList);
        SymbolInfoBag[][] GetSymbolsCounterpartyInfo(ILogger logger);
        List<Currency> GetSupportedCurrenciesList();
        decimal[] GetCounterpartyCommisionsPricePerMillionMap(ILogger logger);
    }
}
