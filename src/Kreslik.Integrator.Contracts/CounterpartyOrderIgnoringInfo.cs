﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    [DataContract]
    public class CounterpartyOrderIgnoringInfo : IntegratorClientOrderInfoObjectBase
    {
        public CounterpartyOrderIgnoringInfo(string clientOrderIdentity, string clientIdentity, string parentOrderIdentity, string integratorOrderIdentity, decimal ignoredAmountBaseAbs, decimal ignoredPrice,
                                      DealDirection direction, Symbol symbol, DateTime integratorSentTimeUtc,
                                      Counterparty counterparty, TradingTargetType senderTradingTargetType,
                                      IntegratedTradingSystemIdentification integratedTradingSystemIdentification, GlidingInfoBag glidingInfoBag)
            : base(senderTradingTargetType, clientIdentity, clientOrderIdentity, parentOrderIdentity)
        {
            this.IntegratorOrderIdentity = integratorOrderIdentity;
            this.IgnoredAmountBaseAbs = ignoredAmountBaseAbs;
            this.IgnoredPrice = ignoredPrice;
            this.Direction = direction;
            this.Symbol = symbol;
            this.IntegratorSentTimeUtc =integratorSentTimeUtc;
            this.Counterparty = counterparty;
            this.IntegratedTradingSystemIdentification = integratedTradingSystemIdentification;
            this.GlidingInfoBag = glidingInfoBag;
        }

        [DataMember]
        public string IntegratorOrderIdentity { get; private set; }

        [DataMember]
        public decimal IgnoredAmountBaseAbs { get; private set; }

        public decimal IgnoredAmountBasePol
        {
            get { return Direction == DealDirection.Buy ? IgnoredAmountBaseAbs : -IgnoredAmountBaseAbs; }
        }

        [DataMember]
        public decimal IgnoredPrice { get; private set; }

        [DataMember]
        public DealDirection Direction { get; private set; }

        public Symbol Symbol { get; private set; }

        [DataMember]
        //this member is needed so that we can (de)serialize Symbol while still using the same instance of this type
        private int SymbolSerialized
        {
            get { return (int)this.Symbol; }
            set { this.Symbol = (Symbol)value; }
        }

        [DataMember]
        public DateTime IntegratorSentTimeUtc { get; private set; }

        public Counterparty Counterparty { get; private set; }

        [DataMember]
        private int CounterpartySerialized
        {
            get { return (int)this.Counterparty; }
            set { this.Counterparty = (Counterparty)value; }
        }

        [DataMember]
        public IntegratedTradingSystemIdentification IntegratedTradingSystemIdentification { get; private set; }

        public GlidingInfoBag GlidingInfoBag { get; private set; }

        public override string ToString()
        {
            return
                string.Format(
                    "Counterparty order ignoring info for order [{0}] (sent at {1} UTC as part of external order {7}) ignored attempt to {2} {3} (BaseAbs) of {4} at {5} from {6}. IntegratedSystem: {8}",
                    ClientOrderIdentity, IntegratorSentTimeUtc, Direction, IgnoredAmountBaseAbs, Symbol,
                    IgnoredPrice, Counterparty, IntegratorOrderIdentity,
                    this.IntegratedTradingSystemIdentification == null ? "<NOT SPECIFIED>" : this.IntegratedTradingSystemIdentification.ToString());
        }
    }
}
