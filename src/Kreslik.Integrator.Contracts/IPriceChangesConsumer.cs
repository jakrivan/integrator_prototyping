﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public interface IPriceChangesConsumer
    {
        void OnNewPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall);
        void OnNewNonExecutablePrice(PriceObjectInternal price);
        void OnIgnoredPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall);
        void OnQuoteCancel(Symbol symbol, Counterparty counterparty);
        void OnPriceCancel(Symbol symbol, Counterparty counterparty, PriceSide side);
        void OnPriceInvalidate(Symbol symbol, PriceSide side, Counterparty counterparty, Guid integratorPriceIdnetity, DateTime integratorPriceReceivedUtc);

        //bool ConsumesFullDepthOfMData { get; }
        //bool ConsumesHotspotData { get; }
        bool ConsumesDataFromCounterparty(Counterparty counterparty);
        //Indiction whether we should or shouldn't unsubscribe when orderflow session is not running
        bool ConsumesMDDataIfOFNotRunning(Counterparty counterparty);
        bool ConsumesIgnoredPrices { get; }
    }

    public interface IStreamingSubscriptionInfoProvider
    {
        event Action<Counterparty, Symbol, bool> RemoteSubscriptionChanged;
    }
}
