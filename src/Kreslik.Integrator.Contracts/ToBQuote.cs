﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts
{
    public class ToBQuote
    {
        public PriceObject AskPrice { get; set; }
        public PriceObject BidPrice { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
