﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts
{
    public enum CommunicationServiceState
    {
        Inactive,
        Starting,
        Running,
        ShuttingDown
    }

    public enum CommunicationServiceStateAction
    {
        Start,
        InitiateShutdown,
        ShutDown
    }

    [DataContract]
    public class CommunicationStateChangeEventArgs
    {
        public CommunicationStateChangeEventArgs(CommunicationServiceStateAction communicationServiceStateAction)
        {
            this.CommunicationServiceStateAction = communicationServiceStateAction;
        }

        [DataMember]
        public string Reason { get; set; }

        [DataMember]
        public CommunicationServiceStateAction CommunicationServiceStateAction { get; private set; }

        public override string ToString()
        {
            return string.Format("CommunicationStateChangeEventArgstArgs - change: {0}, Reason: {1}",
                                 CommunicationServiceStateAction, Reason);
        }
    }

    public interface IOrderResultDispatchGateway
    {
        void OnIntegratorUnicastInfo(IntegratorInfoObjectBase integratorInfoObject);

        void OnIntegratorBroadcastInfo(IntegratorBroadcastInfoBase integratorInfoObject);
    }

    public interface ITradingService : IOrderResultDispatchGateway
    {
        void OnPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs);

        void CloseLocalResourcesOnly(string reason);
    }

    public interface ITradingServiceEndpoint
    {
        [OperationContract(IsOneWay = false)]
        void TransferReliablyOnPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs);

        [OperationContract(IsOneWay = true)]
        void TransferFastOnPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs);

        [OperationContract()]
        void TransferReliablyIntegratorInfoObject(IntegratorInfoObjectBase integratorInfoObject);

        [OperationContract()]
        void TransferReliablyIntegratorBroadcastInfoObject(IntegratorBroadcastInfoBase integratorInfoObject);

        [OperationContract()]
        void CloseLocalResourcesOnly(string reason);
    }
}
