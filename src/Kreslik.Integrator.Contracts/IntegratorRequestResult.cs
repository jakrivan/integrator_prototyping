﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts
{
    [DataContract]
    public class IntegratorRequestResult
    {
        public static IntegratorRequestResult GetSuccessResult()
        {
            return SUCCESS_RESULT;
        }

        public static IntegratorRequestResult CreateFailedResult(string errorMessage)
        {
            return new IntegratorRequestResult(false, errorMessage);
        }

        public static IntegratorRequestResult Combine(IntegratorRequestResult resultA, IntegratorRequestResult resultB)
        {
            if (resultA == null)
                return resultB;
            if (resultB == null)
                return resultA;

            if (resultA.RequestSucceeded && resultB.RequestSucceeded)
                return SUCCESS_RESULT;
            else
            {
                return IntegratorRequestResult.CreateFailedResult(string.Format(
                    "Combined Result [Failed]:{0}{1}{0}{2}", Environment.NewLine,
                    resultA.RequestSucceeded ? "Succeeded" : resultA.ErrorMessage,
                    resultB.RequestSucceeded ? "Succeeded" : resultB.ErrorMessage));
            }
        }

        public static IntegratorRequestResult operator +(
            IntegratorRequestResult resultA, IntegratorRequestResult resultB)
        {
            return IntegratorRequestResult.Combine(resultA, resultB);
        }

        private IntegratorRequestResult(bool requestSucceeded, string errorMessage)
        {
            this.RequestSucceeded = requestSucceeded;
            this.ErrorMessage = errorMessage;
        }

        private static IntegratorRequestResult SUCCESS_RESULT = new IntegratorRequestResult(true, null);

        [DataMember]
        public bool RequestSucceeded { get; private set; }

        [DataMember]
        public string ErrorMessage { get; private set; }

        public override string ToString()
        {
            return string.Format("IntegratorRequestResult:{0}{1}", RequestSucceeded ? "Success" : "Failure",
                RequestSucceeded ? string.Empty : " - " + ErrorMessage);
        }
    }
}
