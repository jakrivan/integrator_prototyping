﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    [DataContract]
    [KnownType(typeof(BankPoolClientOrder))]
    [KnownType(typeof(HotspotClientOrder))]
    [KnownType(typeof(FXallClientOrder))]
    [KnownType(typeof(LmaxClientOrder))]
    //[KnownType(typeof(GliderLmaxClientOrder))]
    [KnownType(typeof(FXCMClientOrder))]
    public abstract class ClientOrderBase : IntegratorRequestInfo, IClientOrder
    {
        [DataMember]
        public string ClientOrderIdentity { get; protected set; }
        [DataMember]
        public string ClientIdentity { get; protected set; }
        //public ClientOrderStatus OrderStatus { get; protected set; }
        [DataMember]
        public decimal SizeBaseAbsFilled { get; protected set; }
        [DataMember]
        public decimal SizeBaseAbsActive { get; protected set; }
        [DataMember]
        public ClientOrderRequestInfoBase OrderRequestInfo { get; protected set; }
        [DataMember]
        public TradingTargetType TradingTargetType { get; protected set; }
        [DataMember]
        public DateTime CreatedUtc { get; protected set; }
        [DataMember]
        public IntegratedTradingSystemIdentification IntegratedTradingSystemIdentification { get; set; }


        #region IIntegratorOrderInfo

        public string Identity { get { return this.ClientOrderIdentity; } }
        public Symbol Symbol { get { return this.OrderRequestInfo.Symbol; } }
        public DealDirection IntegratorDealDirection { get { return this.OrderRequestInfo.IntegratorDealDirection; } }
        public abstract Counterparty Counterparty { get; }
        public decimal SizeBaseAbsInitial { get { return this.OrderRequestInfo.SizeBaseAbsInitial; } }
        public decimal RequestedPrice { get { return this.OrderRequestInfo.RequestedPrice.Value; } }

        public IntegratorOrderInfoType IntegratorOrderInfoType
        {
            get
            {
                return this.OrderRequestInfo.RequestedPrice.HasValue
                    ? IntegratorOrderInfoType.InternalLmtOrder
                    : IntegratorOrderInfoType.InternalMktOrder;
            }
        }

        public bool IsRiskRemovingOrder { get { return this.OrderRequestInfo.IsRiskRemovingOrder; } }

        #endregion IIntegratorOrderInfo

        protected ClientOrderBase(string identity, string clientIdentity, ClientOrderRequestInfoBase orderRequestInfo,
            TradingTargetType tradingTargetType)
        {
            this.ClientOrderIdentity = identity;
            this.ClientIdentity = clientIdentity;
            this.OrderStatus = ClientOrderStatus.NewInIntegrator;
            this.SizeBaseAbsFilled = 0;
            this.SizeBaseAbsActive = orderRequestInfo.SizeBaseAbsInitial;
            this.OrderRequestInfo = orderRequestInfo;
            this.TradingTargetType = tradingTargetType;
            this.CreatedUtc = HighResolutionDateTime.UtcNow;
        }

        [DataMember]
        private volatile ClientOrderStatus _clientOrderStatus;
        public ClientOrderStatus OrderStatus
        {
            get { return this._clientOrderStatus; }
            set { this._clientOrderStatus = value; }
        }

        public bool SubtractAmountAndGetIsFullyDone(decimal currentlySubtructedAmount, bool isFill)
        {
            this.SizeBaseAbsActive -= currentlySubtructedAmount;
            if (isFill)
                this.SizeBaseAbsFilled += currentlySubtructedAmount;

            return this.SizeBaseAbsActive <= 0m;
        }

        //public void AddActiveAmountBack(decimal activeAmount)
        //{
        //    this.SizeBaseAbsActive += activeAmount;
        //}

        public ITakerUnicastInfoForwarder DispatchGateway { get; private set; }

        public void InitializeInOrderManager(ITakerUnicastInfoForwarder dispatchGateway)
        {
            this.DispatchGateway = dispatchGateway;
            this.OrderStatus = ClientOrderStatus.OpenInIntegrator;
        }

        protected ClientOrderBase() { }

        public override string ToString()
        {
            return
                string.Format("Client order [{0}] (from {4}), status: {1}, filled: {2}, active: {3}. OrderRequest: {5}",
                              ClientOrderIdentity, OrderStatus, SizeBaseAbsFilled, SizeBaseAbsActive, CreatedUtc,
                              OrderRequestInfo);
        }
    }

    public interface IBankPoolClientOrder : IClientOrder
    {
        bool IsActiveOrPending { get; }
        BankPoolClientOrderRequestInfo BankPoolClientOrderRequestInfo { get; }
        //IBankPoolClientOrder Clone();
    }

    [DataContract]
    public class BankPoolClientOrder : ClientOrderBase, IBankPoolClientOrder
    {
        internal BankPoolClientOrder(string identity, string clientIdentity,
                                   BankPoolClientOrderRequestInfo orderRequestInfo)
            : base(identity, clientIdentity, orderRequestInfo, TradingTargetType.BankPool)
        { }

        public override Counterparty Counterparty { get { return Counterparty.NULL; } }


        //public IBankPoolClientOrder Clone()
        //{
        //    return new BankPoolClientOrder(this.ClientOrderIdentity, this.ClientIdentity,
        //        this.BankPoolClientOrderRequestInfo);
        //}

        public bool IsActiveOrPending { get { return true; } }

        public BankPoolClientOrderRequestInfo BankPoolClientOrderRequestInfo
        {
            get { return this.OrderRequestInfo as BankPoolClientOrderRequestInfo; }
        }
    }

    //[DataContract]
    public class GliderBankPoolClientOrder : BankPoolClientOrder
    {
        internal GliderBankPoolClientOrder(string identity, string clientIdentity,
            BankPoolClientOrderRequestInfo orderRequestInfo, GlidingInfoBag glidingInfoBag)
            : base(identity, clientIdentity, orderRequestInfo)
        {
            this.GlidingInfoBag = glidingInfoBag;
        }

        public GlidingInfoBag GlidingInfoBag { get; private set; }
    }

    public interface IHotspotClientOrder : IClientOrder
    {
        HotspotClientOrderRequestInfo HotspotClientOrderRequestInfo { get; }
    }

    public interface IVenueClientOrder : IClientOrder
    {
        VenueClientOrderRequestInfo VenueClientOrderRequestInfo { get; }
    }

    [DataContract]
    public abstract class VenueClientOrder : ClientOrderBase, IVenueClientOrder
    {
        internal VenueClientOrder(string identity, string clientIdentity,
            VenueClientOrderRequestInfo orderRequestInfo, TradingTargetType tradingTargetType)
            : base(identity, clientIdentity, orderRequestInfo, tradingTargetType)
        { }

        public VenueClientOrderRequestInfo VenueClientOrderRequestInfo
        {
            get { return this.OrderRequestInfo as VenueClientOrderRequestInfo; }
        }

        public int PendingLLDealsCount { get; set; }

        //private decimal _remainingAmout;
        private int _pendingCancellations = 0;
        private volatile bool _closed = false;

        public bool TrySetOrderBeingCancelledIfNotClosed()
        {
            if (_closed)
                return false;

            Interlocked.Increment(ref _pendingCancellations);
            return true;
        }

        public void SetOrderCancellationDone(bool forceDoneAllCancellations)
        {
            if (forceDoneAllCancellations)
                Interlocked.Exchange(ref _pendingCancellations, 0);
            else
                Interlocked.Decrement(ref _pendingCancellations);
        }

        public bool HasPendingCancellation
        {
            get { return InterlockedEx.Read(ref this._pendingCancellations) > 0; }
        }

        public void SetConfiremdByCounterparty()
        {
            this.OrderStatus = ClientOrderStatus.ConfirmedByCounterparty;
        }

        public bool TryCloseIfNoPendingCancellation()
        {
            this.OrderStatus = ClientOrderStatus.NotActiveInIntegrator;

            if (InterlockedEx.Read(ref this._pendingCancellations) <= 0)
            {
                _closed = true;
            }

            return _closed;
        }

        //public bool IsFullyFilled(decimal curentlyFilledAmount)
        //{
        //    this._remainingAmout -= curentlyFilledAmount;
        //    return _remainingAmout <= 0m;
        //}

        //public decimal ActiveAmount
        //{
        //    get { return _remainingAmout; }
        //}
    }

    [DataContract]
    public class HotspotClientOrder : VenueClientOrder, IHotspotClientOrder
    {
        internal HotspotClientOrder(string identity, string clientIdentity,
                                   HotspotClientOrderRequestInfo orderRequestInfo)
            : base(identity, clientIdentity, orderRequestInfo, TradingTargetType.Hotspot)
        { }

        public HotspotClientOrderRequestInfo HotspotClientOrderRequestInfo
        {
            get { return this.OrderRequestInfo as HotspotClientOrderRequestInfo; }
        }

        public override Counterparty Counterparty
        {
            get { return this.HotspotClientOrderRequestInfo.Counterparty; }
        }
    }

    [DataContract]
    public class FXallClientOrder : VenueClientOrder
    {
        internal FXallClientOrder(string identity, string clientIdentity,
                                   FXallClientOrderRequestInfo orderRequestInfo)
            : base(identity, clientIdentity, orderRequestInfo, TradingTargetType.FXall)
        { }

        public FXallClientOrderRequestInfo FXallClientOrderRequestInfo
        {
            get { return this.OrderRequestInfo as FXallClientOrderRequestInfo; }
        }

        public override Counterparty Counterparty
        {
            get { return this.FXallClientOrderRequestInfo.Counterparty; }
        }
    }

    [DataContract]
    public class FXCMClientOrder : VenueClientOrder
    {
        internal FXCMClientOrder(string identity, string clientIdentity,
                                   FXCMClientOrderRequestInfo orderRequestInfo)
            : base(identity, clientIdentity, orderRequestInfo, TradingTargetType.FXCM)
        { }

        public FXCMClientOrderRequestInfo FXCMClientOrderRequestInfo
        {
            get { return this.OrderRequestInfo as FXCMClientOrderRequestInfo; }
        }

        public override Counterparty Counterparty
        {
            get { return this.FXCMClientOrderRequestInfo.Counterparty; }
        }
    }

    [DataContract]
    public class LmaxClientOrder : VenueClientOrder
    {
        protected internal LmaxClientOrder(string identity, string clientIdentity,
                                   LmaxClientOrderRequestInfo orderRequestInfo)
            : base(identity, clientIdentity, orderRequestInfo, TradingTargetType.LMAX)
        { }

        public LmaxClientOrderRequestInfo LmaxClientOrderRequestInfo
        {
            get { return this.OrderRequestInfo as LmaxClientOrderRequestInfo; }
        }

        public override Counterparty Counterparty
        {
            get { return this.LmaxClientOrderRequestInfo.Counterparty; }
        }
    }

    //[DataContract]
    public class GliderLmaxClientOrder : LmaxClientOrder
    {
        internal GliderLmaxClientOrder(string identity, string clientIdentity,
            LmaxClientOrderRequestInfo orderRequestInfo, GlidingInfoBag glidingInfoBag)
            : base(identity, clientIdentity, orderRequestInfo)
        {
            this.GlidingInfoBag = glidingInfoBag;
        }

        public GlidingInfoBag GlidingInfoBag { get; private set; }
    }
}
