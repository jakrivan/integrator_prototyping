﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    public interface IIntegratorReceivedObjectEx : IIntegratorReceivedObject
    {
        Counterparty Counterparty { get; }
    }

    public interface IDelayEvaluator
    {
        bool IsReceivedObjectDelayed(IIntegratorReceivedObject receivedObject);
        void CheckReceivedObjectDelayedWithNoStatsUpdate(IIntegratorReceivedObject receivedObject);
    }

    public interface IDelayEvaluatorMulti
    {
        bool IsReceivedObjectDelayed(IIntegratorReceivedObjectEx receivedObject);
    }

    public interface IPriceDelayEvaluatorProvider
    {
        IDelayEvaluator GetDelayEvaluator(Counterparty counterparty);
        IDelayEvaluatorMulti GetDelayEvaluatorMultiDestination();
        void SetSessionStateWatchers(IEnumerable<IUnderlyingSessionState> fixChannels);
        void ClearStateWatchers(IEnumerable<Counterparty> cpts);
    }
}
