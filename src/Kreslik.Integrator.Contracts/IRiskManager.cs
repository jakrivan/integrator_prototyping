﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    public interface IOnlinePriceDeviationChecker
    {
        bool MeetsPriceDeviationCriteria(IIntegratorOrderInfo orderInfo, out string reason);
    }

    public enum RiskManagementCheckResult
    {
        Accepted,
        Rejected_TradingDisallowedForInstance,
        Rejected_SymbolOrCurrencyNotSupported,
        Rejected_SymbolNotTrusted,
        Rejected_TransmissionDisabled,
        Rejected_GoFlatOnlyEnabled,
        Rejected_OrderRateExceeded,
        Rejected_PriceDeviation,
        Rejected_OrderSizeAboveLimit,
        Rejected_CurrentDayNopAboveLimit,
        Rejected_UnsettledNopAboveLimit,
        Rejected_UnexpectedError
    }

    public static class RiskManagementCheckResultExtensions
    {
        public static bool CanBeRetryed(this RiskManagementCheckResult result)
        {
            return
                result == RiskManagementCheckResult.Accepted ||
                result == RiskManagementCheckResult.Rejected_SymbolNotTrusted ||
                result == RiskManagementCheckResult.Rejected_UnsettledNopAboveLimit ||
                result == RiskManagementCheckResult.Rejected_GoFlatOnlyEnabled;
        }
    }

    public interface IRiskManager : IBroadcastInfosStore, IExternalOrdersTransmissionControl
    {
        RiskManagementCheckResult AddExternalOrderAndCheckIfAllowed(IIntegratorOrderExternal externalOrder, out decimal nopEstimate);
        void UpdateStateOfExternalOrder(IIntegratorOrderExternal externalOrder, OrderChangeEventArgs orderChangeEventArgs);
        void HandleDealDone(IntegratorDealDone integratorDealDone);
        void HandleDealLateRejected(IIntegratorOrderInfo orderInfo, IntegratorDealDone integratorDealDone);
        RiskManagementCheckResult AddInternalOrderAndCheckIfAllowed(IIntegratorOrderInfo clientOrder);
        RiskManagementCheckResult CheckIfSoftPriceAllowed(IIntegratorOrderInfo streamingPrice);
        bool CheckIfIncomingClientDealAllowed(IIntegratorOrderInfo rejectableDeal);
        bool CheckIfIncomingClientDealCanBeAccepted(IIntegratorOrderInfo rejectableDeal);
        //void AcceptedClientDealWasLateRejected(IIntegratorOrderInfo rejectableMmDeal);
        //void AcceptedClientDealWasLateRejected(IIntegratorOrderInfo orderInfo, decimal sizeToRemoveBaseAbs);
        bool CancelInternalOrder(IClientOrder clientOrder, decimal cancelledAmount);
        bool CancelInternalOrder(decimal canceledSizeBaseAbs, PriceObject quotedPrice);
        decimal GetMaximumOrderSizeBaseAbs(Kreslik.Integrator.Common.Currency baseCurrency);
        bool IsTradeRequestLikelyToPass(IIntegratorOrderInfo orderInfo);
        bool IsTradeRequestLikelyToPass(IIntegratorOrderInfo orderInfo, PriceObjectInternal priceObject);

        IOnlinePriceDeviationChecker OnlinePriceDeviationChecker { get; }
        ISymbolTrustworthyWatcher GetSymbolTrustworthyWatcher(Symbol symbol);
        ISymbolTrustworthinessInfoProvider SymbolTrustworthinessInfoProvider { get; }

        IExternalOrdersTransmissionControlScheduling ExternalOrdersTransmissionControlScheduling { get; }
    }
}
