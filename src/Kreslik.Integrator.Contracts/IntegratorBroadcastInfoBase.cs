﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    [DataContract]
    [KnownType(typeof(OrderTransmissionStatusChangedEventArgs))]
    [KnownType(typeof(SymbolExchangeRatesArgs))]
    [KnownType(typeof(CounterpartiesNopArgs))]
    [KnownType(typeof(SymbolsInfoArgs))]
    [KnownType(typeof(ConnectionStateChangeEventArgs))]
    [KnownType(typeof(TradingAllowedStatusChangedEventArgs))]
    public abstract class IntegratorBroadcastInfoBase
    {
        protected IntegratorBroadcastInfoBase(Guid broadcastInfoTypeIdentity, bool canBeCached)
        {
            this.BroadcastInfoTypeIdentity = broadcastInfoTypeIdentity;
            this.CanBeCached = canBeCached;
        }

        /// <summary>
        /// Distingush between different information type (even published in same C# type)
        /// If caching is used - only the last information with the same type will be cached
        /// </summary>
        [DataMember]
        internal Guid BroadcastInfoTypeIdentity { get; private set; }

        [DataMember]
        internal bool CanBeCached { get; private set; }
    }

    [DataContract]
    public class OrderTransmissionStatusChangedEventArgs : IntegratorBroadcastInfoBase
    {
        public OrderTransmissionStatusChangedEventArgs(bool isOrderTransmissionEnabled, string reason)
            : base(_broadcastInfoTypeIdentity, true)
        {
            this.IsOrderTransmissionEnabled = isOrderTransmissionEnabled;
            this.Reason = reason;
        }

        [DataMember]
        public bool IsOrderTransmissionEnabled { get; private set; }
        [DataMember]
        public string Reason { get; private set; }

        private static readonly Guid _broadcastInfoTypeIdentity = new Guid("89C5D966-C215-41D8-9314-BE8A4613722C");
    }

    public enum TradingState
    {
        TradingAllowed,
        GoFlatOnly
    }

    [DataContract]
    public class TradingAllowedStatusChangedEventArgs : IntegratorBroadcastInfoBase
    {
        public TradingAllowedStatusChangedEventArgs(bool isGoFlatOnlyEnabled)
            : this(isGoFlatOnlyEnabled ? TradingState.GoFlatOnly : TradingState.TradingAllowed)
        { }

        public TradingAllowedStatusChangedEventArgs(TradingState tradingState)
            : base(_broadcastInfoTypeIdentity, true)
        {
            this.TradingState = tradingState;
        }

        [DataMember]
        public TradingState TradingState { get; private set; }

        private static readonly Guid _broadcastInfoTypeIdentity = new Guid("7600BA6F-10C8-41A1-B50F-F2C54478AFE4");
    }

    [DataContract]
    public class SymbolExchangeRatesArgs : IntegratorBroadcastInfoBase
    {
        public SymbolExchangeRatesArgs(decimal[] symbolExcahngerates, DateTime[] symbolExchangeRatesTimeStamps)
            : base(_broadcastInfoTypeIdentity, true)
        {
            this.SymbolExchangeRates = symbolExcahngerates;
            this.SymbolExchangeRatesTimeStamps = symbolExchangeRatesTimeStamps;
        }

        [DataMember]
        public decimal[] SymbolExchangeRates { get; private set; }

        [DataMember]
        public DateTime[] SymbolExchangeRatesTimeStamps { get; private set; }

        private static readonly Guid _broadcastInfoTypeIdentity = new Guid("224420EE-DDB2-4134-A0B4-16FEDA03AC08");
    }

    [DataContract]
    public class CounterpartiesNopArgs : IntegratorBroadcastInfoBase
    {
        public CounterpartiesNopArgs(decimal[][] counterpartiesNops)
            : base(_broadcastInfoTypeIdentity, true)
        {
            this.CounterpartiesNops = counterpartiesNops;
        }

        [DataMember]
        public decimal[][] CounterpartiesNops { get; private set; }

        private static readonly Guid _broadcastInfoTypeIdentity = new Guid("105B4498-235B-4472-8CD7-816D03BDC7B3");
    }

    [DataContract]
    public class SymbolInfoBag
    {
        public SymbolInfoBag(
            Symbol symbol, 
            TradingTargetType tradingTargetType, 
            bool isSupported, 
            decimal? minimumPriceIncrement,
            decimal? orderMinimumSize,
            decimal? orderMinimumSizeIncrement)
        {
            this.Symbol = symbol;
            this.TradingTargetType = tradingTargetType;
            this.IsSupported = isSupported;
            this.MinimumPriceIncrement = minimumPriceIncrement.Normalize();
            this.OrderMinimumSize = orderMinimumSize.Normalize();
            this.OrderMinimumSizeIncrement = orderMinimumSizeIncrement.Normalize();
        }

        public void Normalize()
        {
            this.MinimumPriceIncrement = MinimumPriceIncrement.Normalize();
            this.OrderMinimumSize = OrderMinimumSize.Normalize();
            this.OrderMinimumSizeIncrement = OrderMinimumSizeIncrement.Normalize();
        }


        public Symbol Symbol { get; private set; }

        [DataMember]
        //this member is needed so that we can (de)serialize Symbol while still using the same instance of this type
        private int SymbolSerialized
        {
            get { return (int)this.Symbol; }
            set { this.Symbol = (Symbol)value; }
        }

        [DataMember]
        public TradingTargetType TradingTargetType { get; private set; }

        [DataMember]
        public bool IsSupported { get; private set; }

        [DataMember]
        public decimal? MinimumPriceIncrement { get; private set; }

        [DataMember]
        public decimal? OrderMinimumSize { get; private set; }

        [DataMember]
        public decimal? OrderMinimumSizeIncrement { get; private set; }
    }

    [DataContract]
    public class SymbolsInfoArgs : IntegratorBroadcastInfoBase
    {
        public SymbolsInfoArgs(SymbolInfoBag[][] symbolsInfoBag, bool[] supportedCurrenciesMap,
                               decimal[] counterpartyCommisionsPricePerMillionMap)
            : base(_broadcastInfoTypeIdentity, true)
        {
            this.SymbolsInfoBag = symbolsInfoBag;
            this.SupportedCurrenciesMap = supportedCurrenciesMap;
            this.CounterpartyCommisionsPricePerMillionMap = counterpartyCommisionsPricePerMillionMap;
        }

        [DataMember]
        public SymbolInfoBag[][] SymbolsInfoBag { get; private set; }

        [DataMember]
        public bool[] SupportedCurrenciesMap { get; private set; }

        [DataMember]
        public decimal[] CounterpartyCommisionsPricePerMillionMap { get; private set; }

        private static readonly Guid _broadcastInfoTypeIdentity = new Guid("E7642B44-51E8-41D4-BA69-FD1199AB1059");
    }

    public enum ConnectionServiceState
    {
        Running,
        InitiateShutdown,
        ShutDown,
        Reconnected
        //TODO: needed? isn't identical to Running?
        //,ResubscribedAndReady
    }

    [DataContract]
    public class ConnectionStateChangeEventArgs: IntegratorBroadcastInfoBase
    {
        public ConnectionStateChangeEventArgs(ConnectionServiceState connectionServiceState, IntegratorProcessType integratorProcessTypeWithChangedState)
            : base(ProcessTypeGuids.GetGuidForProcessType(integratorProcessTypeWithChangedState), true)
        {
            this.ConnectionServiceState = connectionServiceState;
            this.IntegratorProcessTypeWithChangedState = integratorProcessTypeWithChangedState;
        }

        [DataMember]
        public ConnectionServiceState ConnectionServiceState { get; private set; }

        [DataMember]
        public IntegratorProcessType IntegratorProcessTypeWithChangedState { get; private set; }

        [DataMember]
        public string Reason { get; set; }

        public override string ToString()
        {
            return string.Format("ConnectionStateChangeEventArgs - change: {0} for {1}, Reason: {2}",
                                 ConnectionServiceState, IntegratorProcessTypeWithChangedState, Reason);
        }
    }

    public class IntegratorProcessInfoToBroadcastIngoAdapter : BroadcastInfosStoreBase
    {
        public IntegratorProcessInfoToBroadcastIngoAdapter(ILogger logger, IntegratorProcessInfo integratorProcessInfo)
            : base(logger)
        {
            integratorProcessInfo.IntegratorProcessStateChanged += state =>
                {
                    if (state == IntegratorProcessState.ShuttingDown)
                    {
                        this.OnNewBroadcastInfo(
                            new ConnectionStateChangeEventArgs(ConnectionServiceState.InitiateShutdown, integratorProcessInfo.IntegratorProcessType)
                                {
                                    Reason = "Integrator instance is initiationg shutdown"
                                });
                    }
                };
        }
    }
}
