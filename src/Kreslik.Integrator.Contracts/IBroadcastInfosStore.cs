﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts
{
    public interface IBroadcastInfosStore
    {
        event NewBroadcastInfoEventHandler NewBroadcastInfo;
        IEnumerable<IntegratorBroadcastInfoBase> ExistingInfos { get; }
        //void OnNewBroadcastInfo(IntegratorBroadcastInfoBase broadcastInfo);
    }
}
