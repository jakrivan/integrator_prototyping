﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public delegate void NewVenueDealHandler(VenueDealObject venueDealObject);

    public interface IVenueDataProvider
    {
        //We need indexing by symbol, therefore we cannot use simple event
        void AddNewVenueDealHandler(Symbol symbol, NewVenueDealHandler newVenueDealHandler);
        void RemoveNewVenueDealHandler(Symbol symbol, NewVenueDealHandler newVenueDealHandler);
        void RemoveAllOccurencesOfNewVenueDealHandler(NewVenueDealHandler newVenueDealHandler);
    }

    public interface IVenueDataProviderStore
    {
        IVenueDataProvider GetVenueDataProvider(Counterparty counterparty);
        IEnumerable<IVenueDataProvider> AllVenueDataProviders { get; }
    }
}
