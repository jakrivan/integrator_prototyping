﻿using Kreslik.Integrator.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts
{
    public interface IOrderManagement
    {
        IntegratorRequestResult RegisterOrder(IClientOrder clientOrder, ITakerUnicastInfoForwarder dispatchGateway);
        IntegratorRequestResult CancelOrder(CancelOrderRequestInfo cancelOrderRequestInfo, ITakerUnicastInfoForwarder dispatchGateway);
        bool HasVenueOrder(Counterparty targetVenueCounterparty, string clientOrderId);
        IBankPoolClientOrder GetInternalBankpoolOrder(IClientOrder clientOrder);
    }
}
