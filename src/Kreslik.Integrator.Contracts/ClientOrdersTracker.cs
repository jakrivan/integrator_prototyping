﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public class ClientOrdersTracker
    {
        private Dictionary<string, IClientOrder> _orders = new Dictionary<string, IClientOrder>();
        //private Dictionary<string, decimal> _ordersFilling = new Dictionary<string, decimal>();
        private ILogger _logger;

        public ClientOrdersTracker(ILogger logger)
        {
            this._logger = logger;
        }

        public void AddOrder(IClientOrder clientOrder)
        {
            if (_orders.ContainsKey(clientOrder.ClientOrderIdentity))
            {
                this._logger.Log(LogLevel.Error, "Order [{0}] already registered.", clientOrder.ClientOrderIdentity);
                return;
            }

            _orders.Add(clientOrder.ClientOrderIdentity, clientOrder);
            //_ordersFilling.Add(clientOrder.ClientOrderIdentity, clientOrder.ActiveAmountBaseAbs);
        }

        public void RemoveOrder(string orderId)
        {
            _orders.Remove(orderId);
        }

        public void SubstractAmount(decimal amount, string orderId) { }

        public IClientOrder GetClientOrder(string orderId)
        {
            IClientOrder clientOrder;
            if (!_orders.TryGetValue(orderId, out clientOrder))
            {
                _logger.Log(LogLevel.Error, "Order [{0}] (already?) not known to client. It might havebeen already removed", orderId);
            }
            return clientOrder;
        }
    }
}
