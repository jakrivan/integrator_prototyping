﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    //public class NoIdentityQuote : QuoteBase
    //{
    //    public NoIdentityQuote(decimal askPrice, decimal askSize, decimal bidPrice, decimal bidSize, Symbol symbol,
    //                           Counterparty counterparty, string subscriptionIdentity, DateTime intagratorTimeUtc,
    //                           DateTime counterpartyTimeUtc, MarketDataRecordType marketDataRecordType)
    //        : base(askPrice, askSize, bidPrice, bidSize, symbol, counterparty, subscriptionIdentity, intagratorTimeUtc,
    //            counterpartyTimeUtc, null, null, marketDataRecordType)
    //    { }

    //    public NoIdentityQuote(decimal askPrice, decimal askSize, decimal bidPrice, decimal bidSize, Symbol symbol,
    //                               Counterparty counterparty, string subscriptionIdentity, DateTime counterpartyTimeUtc,
    //                             MarketDataRecordType marketDataRecordType)
    //        : base(askPrice, askSize, bidPrice, bidSize, symbol, counterparty, subscriptionIdentity, HighResolutionDateTime.UtcNow,
    //        counterpartyTimeUtc, null, null, marketDataRecordType)
    //    { }

    //    public NoIdentityQuote(decimal price, decimal size, PriceSide side, Symbol symbol, Counterparty counterparty,
    //                           string subscriptionIdentity, DateTime intagratorTimeStampUtc,
    //                           DateTime counterpartyTimeUtc, MarketDataRecordType marketDataRecordType)
    //        : base(price, size, side, symbol, counterparty, subscriptionIdentity, intagratorTimeStampUtc,
    //            counterpartyTimeUtc, null, marketDataRecordType)
    //    { }

    //    public NoIdentityQuote(decimal price, decimal size, PriceSide side, Symbol symbol,
    //                               Counterparty counterparty, string subscriptionIdentity, DateTime counterpartyTimeUtc,
    //                               MarketDataRecordType marketDataRecordType)
    //        : base(price, size, side, symbol, counterparty, subscriptionIdentity, HighResolutionDateTime.UtcNow, counterpartyTimeUtc, null, marketDataRecordType)
    //    { }
    //}

    //public class SingleIdentityQuote : QuoteBase
    //{
    //    public SingleIdentityQuote(decimal askPrice, decimal askSize, decimal bidPrice, decimal bidSize, Symbol symbol,
    //                               Counterparty counterparty, string subscriptionIdentity, string identity, DateTime intagratorTimeUtc,
    //                                DateTime counterpartyTimeUtc, MarketDataRecordType marketDataRecordType)
    //        : base(askPrice, askSize, bidPrice, bidSize, symbol, counterparty, subscriptionIdentity, intagratorTimeUtc, counterpartyTimeUtc, identity, identity, marketDataRecordType)
    //    { }

    //    public SingleIdentityQuote(decimal askPrice, decimal askSize, decimal bidPrice, decimal bidSize, Symbol symbol,
    //                               Counterparty counterparty, string subscriptionIdentity, string identity, DateTime counterpartyTimeUtc,
    //                                MarketDataRecordType marketDataRecordType)
    //        : base(askPrice, askSize, bidPrice, bidSize, symbol, counterparty, subscriptionIdentity, HighResolutionDateTime.UtcNow,
    //                counterpartyTimeUtc, identity, identity, marketDataRecordType)
    //    { }

    //    public SingleIdentityQuote(decimal price, decimal size, PriceSide side, Symbol symbol,
    //                               Counterparty counterparty, string subscriptionIdentity, string identity, DateTime intagratorTimeStampUtc,
    //                                DateTime counterpartyTimeUtc, MarketDataRecordType marketDataRecordType)
    //        : base(price, size, side, symbol, counterparty, subscriptionIdentity, intagratorTimeStampUtc, counterpartyTimeUtc, identity, marketDataRecordType)
    //    { }

    //    public SingleIdentityQuote(decimal price, decimal size, PriceSide side, Symbol symbol,
    //                               Counterparty counterparty, string subscriptionIdentity, string identity, DateTime counterpartyTimeUtc,
    //                               MarketDataRecordType marketDataRecordType)
    //        : base(price, size, side, symbol, counterparty, subscriptionIdentity, HighResolutionDateTime.UtcNow, counterpartyTimeUtc, identity, marketDataRecordType)
    //    { }
    //}

    //public class SideIdentityQuote : QuoteBase
    //{
    //    public SideIdentityQuote(decimal askPrice, decimal askSize, decimal bidPrice, decimal bidSize, Symbol symbol,
    //                             Counterparty counterparty, string subscriptionIdentity, DateTime intagratorTimeUtc,
    //                             DateTime counterpartyTimeUtc, string askIdentity, string bidIdentity, MarketDataRecordType marketDataRecordType)
    //        : base(
    //            askPrice, askSize, bidPrice, bidSize, symbol, counterparty, subscriptionIdentity, intagratorTimeUtc,
    //            counterpartyTimeUtc, askIdentity, bidIdentity, marketDataRecordType)
    //    { }

    //    public SideIdentityQuote(decimal askPrice, decimal askSize, decimal bidPrice, decimal bidSize, Symbol symbol,
    //                             Counterparty counterparty, string subscriptionIdentity,
    //                             DateTime counterpartyTimeUtc, string askIdentity, string bidIdentity, MarketDataRecordType marketDataRecordType)
    //        : base(
    //            askPrice, askSize, bidPrice, bidSize, symbol, counterparty, subscriptionIdentity,
    //            HighResolutionDateTime.UtcNow, counterpartyTimeUtc, askIdentity, bidIdentity, marketDataRecordType)
    //    { }

    //    public void RemoveSide(PriceSide priceSide)
    //    {
    //        this.RemoveSideInternal(priceSide);
    //        //no need to remove identity - since it should be already null
    //    }
    //}

    public class HotspotQuoteUpdateInfo
    {
        public HotspotQuoteUpdateInfo(PriceObject originalPrice, decimal newSize, decimal? newMinimalSize, decimal? newGranularity, DateTime counterpartySentTimeUtc, DateTime integratorReceivedTimeUtc)
        {
            this.OriginalPrice = originalPrice;
            this.NewSize = newSize;
            this.NewMinimalSize = newMinimalSize;
            this.NewGranularity = newGranularity;
            this.CounterpartySentTimeUtc = counterpartySentTimeUtc;
            this.IntegratorReceivedTimeUtc = integratorReceivedTimeUtc;
        }

        public PriceObject OriginalPrice { get; private set; }
        public decimal NewSize { get; private set; }
        public decimal? NewMinimalSize { get; private set; }
        public decimal? NewGranularity { get; private set; }
        public DateTime CounterpartySentTimeUtc { get; private set; }
        public DateTime IntegratorReceivedTimeUtc { get; private set; }
    }

    public class HotspotQuoteCancelInfo
    {
        public HotspotQuoteCancelInfo(PriceObject originalPrice, DateTime counterpartySentTimeUtc, DateTime integratorReceivedTimeUtc)
        {
            this.OriginalPrice = originalPrice;
            this.CounterpartySentTimeUtc = counterpartySentTimeUtc;
            this.IntegratorReceivedTimeUtc = integratorReceivedTimeUtc;
        }

        public PriceObject OriginalPrice { get; private set; }
        public DateTime CounterpartySentTimeUtc { get; private set; }
        public DateTime IntegratorReceivedTimeUtc { get; private set; }
    }
}
