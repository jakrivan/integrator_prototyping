﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts
{
    /// <summary>
    /// Build constants for Contracts module
    /// Since const fields get embadded diractly into calling code, the calling code
    ///  will be stamped with version used to build the calling code (even if new version of Contracts is used during runtime)
    /// This is the best easy way to do this, as dll checksum changes each build (even without chaning code)
    ///  see e.g.: http://social.msdn.microsoft.com/Forums/vstudio/en-US/50bb983f-b20b-4a90-8351-ae48f5918029/create-a-checksum-of-build-files?forum=csharpgeneral
    /// </summary>
    public static class BuildConstants
    {
        //All fields needs to be const (they cannot be static nor readonly nor properties)
        // so that they are directly embadded int ocalling code

        public const int CURRENT_BUILD_VERSION = 42;
        public const int MINIMUM_VERSION_NUMBER_OF_COMPATIBLE_BUILD = 42;
        //DateTime cannot be defined as const - therefore using string
        public const string CURRENT_VERSION_CREATED_UTC = "2015-10-30 10:00";
    }

    //Versions Changelog:
    //
    //      DATE        Version#    Changes
    //______________________
    //2013-11-21 17:02      1       Initial version
    //2013-11-21 17:32      2       Testing change ver. number
    //2013-11-25 14:00      3       Adding Pegged orders
    //2013-11-28 16:00      4       New IntegratorBroadcastInfo type - CounterpartiesNopArgs
    //2014-01-03 16:34      5       Venue deals contract (both ways)
    //2014-01-23 20:00      6       Unification of pricing events and subscriptions contract
    //2014-01-28 10:00      7       Unification of Venue client order OM, adding FXall
    //2014-02-06 11:00      8       Adding LM1 counterparty
    //2014-02-10 20:00      9       Removing/Adding symbols
    //2014-03-18 07:00      10      Refactoring pricing and unicast contract (SenderTradingTargetType)
    //2014-03-25 11:00      11      Extending SymbolsInfo contrct (OrderMinimumSize(Increment))
    //2014-04-16 09:16      12      Refactored IntegratorRequest contract, Subscriptions contract
    //2014-04-30 09:58      13      Unicast info forwarding, splitter subscriptions refactoring
    //2014-06-05 08:52      14      BroadcastInfo TradingStatusAllowed event
    //2014-06-09 08:52      15      BroadcastInfo TradingStatusAllowed event into attribute
    //2014-06-19 18:52      16      Adding LM2 Counterparty
    //2014-07-11 12:40      17      Adding IntegratedTradingSystemIdentification into remoting objects
    //2014-07-16 12:00      18      Adding Commisions info into SymbolsInfo contract
    //2014-07-17 13:45      19      Adding IsTerminalFill property to IntegratorDealInternal
    //2014-07-24 17:53      20      Adding ClientOrderStatus.ConfirmedByCounterparty
    //2014-07-28 13:47      21      Adding FXCM
    //2014-09-02 10:00      22      Adding FC2
    //2014-09-17 18:00      23      Adding LM3
    //2014-09-23 11:00      24      Adding HT3
    //2014-11-07 06:30      25      LowLatency and pooling refactoring changes
    //2014-11-28 16:30      26      Adding symbols and FS1
    //2015-01-22 10:30      27      Refactored Symbol and Counterparty (de)serialization (ints instead of strings)
    //2015-01-23 14:50      28      IntegratorDealInternal.CounterpartyClientId
    //2015-02-02 14:55      29      Renaming CounterpartyDealTimeUtc -> CounterpartySentDealRequestTimeUtc
    //2015-02-16 14:55      30      Mandatory Risk Add/Remove client orders contract; Removing Composite orders
    //2015-03-12 15:20      31      New LMAX pseudo-symbols
    //2015-03-23 08:39      32      Hotspot LL functionality
    //2015-03-23 10:45      33      FS2 Cpt
    //2015-07-13 12:35      34      LX1, FL1 Cpt
    //2015-07-22 13:22      35      LGA, LGC Cpt
    //2015-09-03 07:06      36      LX1 Cpt
    //2015-09-10 11:06      37      H4T, H4M Cpts
    //2015-09-22 10:21      38      Symbol, Currency classes->structs
    //2015-09-25 11:21      39      Counterparty classes->structs
    //2015-10-20 11:35      40      IntegratorDealInternal.SettlementDate
    //2015-10-21 13:35      41      ClientOrderRequestInfo.Side -> IntegratorDealDirection
    //2015-10-30 10:00      42      SettlementTimeCounterpartyUtc & SettlementTimeCentralBankUtc

    public static class DateTimeExtensions
    {
        public static string ToLogString(this DateTime? nullableDate)
        {
            if (nullableDate.HasValue)
                return nullableDate.Value.ToString("yyyy-MM-dd HH:mm:ss");
            else
                return "?";
        }
    }

    [DataContract]
    public class BuildVersioningInfo
    {
        public static string[] DATE_FORMATS = { "yyyy-MM-dd HH:mm:ss.fff", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm" };
        public static DateTimeStyles DATE_TIME_STYLES = DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal;
        public static CultureInfo DATE_TIME_CULTURE_INFO = CultureInfo.InvariantCulture;

        private static DateTime? ParseStringToDate(string dateString)
        {
            DateTime parsedDate;
            if (!System.DateTime.TryParseExact(dateString, DATE_FORMATS, DATE_TIME_CULTURE_INFO, DATE_TIME_STYLES,
                                              out parsedDate))
            {
                return null;
            }

            return parsedDate;
        }

        public BuildVersioningInfo(int currentBuildVersion, int minimumVersionNumberOfCompatibleBuild,
                                    string currentVersionCreatedUtcString)
        {
            this.CurrentBuildVersion = currentBuildVersion;
            this.MinimumVersionNumberOfCompatibleBuild = minimumVersionNumberOfCompatibleBuild;
            this.CurrentVersionCreatedUtc = ParseStringToDate(currentVersionCreatedUtcString);
        }

        [DataMember]
        public int CurrentBuildVersion { get; private set; }
        [DataMember]
        public int MinimumVersionNumberOfCompatibleBuild { get; private set; }
        [DataMember]
        public DateTime? CurrentVersionCreatedUtc { get; private set; }

        public override string ToString()
        {
            return string.Format("Contract build version: {0} (compatible with builds >= {1}), build on {2}",
                                 this.CurrentBuildVersion, this.MinimumVersionNumberOfCompatibleBuild,
                                 this.CurrentVersionCreatedUtc.ToLogString());
        }
    }

    [DataContract]
    public class ConnectionChainBuildVersioningInfo
    {
        public ConnectionChainBuildVersioningInfo(BuildVersioningInfo hostingGatewayVersionInfo, BuildVersioningInfo splitterVersionInfo, BuildVersioningInfo integratorVersionInfo)
        {
            this.HostingGatewayVersionInfo = hostingGatewayVersionInfo;
            this.SplitterVersionInfo = splitterVersionInfo;
            this.IntegratorVersionInfo = integratorVersionInfo;
        }

        [DataMember]
        public BuildVersioningInfo HostingGatewayVersionInfo { get; private set; }
        [DataMember]
        public BuildVersioningInfo SplitterVersionInfo { get; private set; }
        [DataMember]
        public BuildVersioningInfo IntegratorVersionInfo { get; private set; }

        public override string ToString()
        {
            return
                string.Format(
                    "IntegratorVersionInfo: [{0}], SplitterVersionInfo: [{1}], HostingGatewayVersionInfo: [{2}]",
                    IntegratorVersionInfo == null ? "<UNKNOWN>" : IntegratorVersionInfo.ToString(),
                    SplitterVersionInfo == null ? "<UNKNOWN>" : SplitterVersionInfo.ToString(),
                    HostingGatewayVersionInfo == null ? "<UNKNOWN>" : HostingGatewayVersionInfo.ToString());
        }
    }
}
