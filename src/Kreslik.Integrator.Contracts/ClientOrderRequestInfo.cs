﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    public enum DealConfirmationAction
    {
        Confirm,
        Reject
    }

    [DataContract]
    public class RejectableDealDefaults
    {
        public RejectableDealDefaults(TimeSpan maximumConfirmationDelay, DealConfirmationAction defaultConfirmationAction)
        {
            this.MaximumConfirmationDelay = maximumConfirmationDelay;
            this.DefaultConfirmationAction = defaultConfirmationAction;
        }

        [DataMember]
        public TimeSpan MaximumConfirmationDelay { get; private set; }
        [DataMember]
        public DealConfirmationAction DefaultConfirmationAction { get; private set; }
    }

    public enum OrderType
    {
        Limit,
        Market,
        Quoted,
        Pegged,
        MarketOnImprovement,
        MarketPreferLmax,
        LimitTillTimeout
    }

    public enum PegType
    {
        /// <summary>
        /// Peggs along the OPOSITE side of the best price
        /// </summary>
        MarketPeg,

        /// <summary>
        /// Peggs along the SAME side of the best price
        /// </summary>
        PrimaryPeg
    }

    public enum VenueRequestType
    {
        NotAVenueRequest,
        SingleOrder,
        SingleOrderReplace
    }

    public enum CompositeOrderStrategy
    {
        None,
        AllAtOnceOrImmediateAtMarket,
        AllAtOnceOrKill
    }

    [DataContract]
    [KnownType(typeof (BankPoolClientOrderRequestInfo))]
    [KnownType(typeof (HotspotClientOrderRequestInfo))]
    [KnownType(typeof(FXallClientOrderRequestInfo))]
    [KnownType(typeof(LmaxClientOrderRequestInfo))]
    [KnownType(typeof(FXCMClientOrderRequestInfo))]
    public abstract class ClientOrderRequestInfoBase : IIntegratorOrderInfo
    {
        [DataMember]
        public decimal SizeBaseAbsInitial { get; protected set; }

        [DataMember]
        public decimal? RequestedPrice { get; protected set; }

        public string Identity { get { return "ClientOrderRequestInfo"; } }

        decimal IIntegratorOrderInfo.RequestedPrice { get { return this.RequestedPrice.Value; } }

        [DataMember]
        public DealDirection IntegratorDealDirection { get; protected set; }

        //[DataMember]
        //public PriceObject Price { get; protected set; }

        public Symbol Symbol { get; protected set; }

        public abstract Counterparty Counterparty { get; }

        public IntegratorOrderInfoType IntegratorOrderInfoType
        {
            get
            {
                return this.RequestedPrice.HasValue
                    ? IntegratorOrderInfoType.InternalLmtOrder
                    : IntegratorOrderInfoType.InternalMktOrder;
            }
        }

        [DataMember]
        //this member is needed so that we can (de)serialize Symbol while still using the same instance of this type
        private int SymbolSerialized
        {
            get { return (int) this.Symbol; }
            set { this.Symbol = (Symbol) value; }
        }

        [DataMember]
        public OrderType OrderType { get; protected set; }

        [DataMember]
        public TimeSpan MaximumAcceptedPriceAge { get; set; }

        [DataMember]
        public bool IsRiskRemovingOrder { get; private set; }
        public DealInfo DealInfo
        {
            get
            {
                if(!this.RequestedPrice.HasValue)
                    throw new ClientGatewayException("Cannot create DealInfo from ClientOrderRequestInfoBase with missing RequestePrice - probably a Market order request");

                return new DealInfo(this.Symbol, this.IntegratorDealDirection, this.SizeBaseAbsInitial, this.RequestedPrice.Value);
            }
        }

        public override string ToString()
        {
            return string.Format("ClientOrderRequest {0} to {1} {2} SizeBaseAbsInitial {3} at {4}. (Risk removing: {5})", OrderType, IntegratorDealDirection, Symbol,
                                 SizeBaseAbsInitial, RequestedPrice, IsRiskRemovingOrder);
        }

        protected ClientOrderRequestInfoBase(decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side,
                                             PriceObject price, bool isRiskRemovingOrder)
            : this(sizeBaseAbsInitial, requestedPrice, side, price.Symbol, isRiskRemovingOrder)
        {
            //this.Price = price;
            this.OrderType = OrderType.Quoted;
        }

        protected ClientOrderRequestInfoBase(decimal sizeBaseAbsInitial, decimal? requestedPrice, DealDirection side,
                                             Symbol symbol, bool isRiskRemovingOrder)
        {
            this.SizeBaseAbsInitial = sizeBaseAbsInitial;
            this.RequestedPrice = requestedPrice;
            this.IntegratorDealDirection = side;
            this.Symbol = symbol;
            this.MaximumAcceptedPriceAge = TimeSpan.MaxValue;
            this.IsRiskRemovingOrder = isRiskRemovingOrder;
        }

        public static bool CanBeSatisfied(decimal sizeBaseAbs, decimal requestedPrice, DealDirection side,
                                          PriceObject price)
        {
            return
                //Is the Price populated
                !PriceObjectInternal.IsNullPrice(price)
                &&
                //Is this request for correct side?
                (price.Side == PriceSide.Ask && side == DealDirection.Buy
                 || price.Side == PriceSide.Bid && side == DealDirection.Sell)
                &&
                //does the quote meet the pricing requirements
                (side == DealDirection.Buy
                     ? price.Price <= requestedPrice
                     : price.Price >= requestedPrice
                )
                &&
                //does the quote meet the liquidity requirements
                (price.SizeBaseAbsRemaining >= sizeBaseAbs);
        }
    }

    [DataContract]
    public class BankPoolClientOrderRequestInfo : ClientOrderRequestInfoBase
    {
        //TODO: make just a simple list
        public System.Collections.Concurrent.ConcurrentQueue<Counterparty> CounterpartiesBlacklist { get; set; }

        //if set - only the listed cpts can be matched (unless GFO)
        public Counterparty[] CounterpartiesWhitelist { get; set; }

        public override Counterparty Counterparty { get { return Counterparty.NULL; } }
            
        [DataMember]
        //this member is needed so that we can (de)serialize Counterparites while still using the same instance of this type
        private List<string> CounterpartiesBlacklistSerialized
        {
            get
            {
                return this.CounterpartiesBlacklist == null
                           ? null
                           : this.CounterpartiesBlacklist.Select(c => c.ToString()).ToList();
            }
            set
            {
                this.CounterpartiesBlacklist = value == null
                                                   ? null
                                                   : new System.Collections.Concurrent.ConcurrentQueue<Counterparty>(
                                                         value.Select(s => (Counterparty)s));
            }
        }

        [DataMember]
        public TimeSpan TimeoutTillFlippingToMarket { get; private set; }

        public void ResetTypeToMarket()
        {
            this.OrderType = OrderType.Market;
            this.RequestedPrice = null;
        }

        private BankPoolClientOrderRequestInfo(decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side, PriceObject price, bool isRiskRemovingOrder)
            : base(sizeBaseAbsInitial, requestedPrice, side, price, isRiskRemovingOrder)
        { }

        private BankPoolClientOrderRequestInfo(decimal sizeBaseAbsInitial, decimal? requestedPrice, DealDirection side, Symbol symbol, bool isRiskRemovingOrder)
            : base(sizeBaseAbsInitial, requestedPrice, side, symbol, isRiskRemovingOrder)
        { }

        internal static BankPoolClientOrderRequestInfo CreateLimit(decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side, Symbol symbol, bool isRiskRemovingOrder)
        {
            return new BankPoolClientOrderRequestInfo(sizeBaseAbsInitial, requestedPrice, side, symbol, isRiskRemovingOrder)
            {
                OrderType = OrderType.Limit
            };
        }

        internal static BankPoolClientOrderRequestInfo CreateMarket(decimal sizeBaseAbsInitial, DealDirection side, Symbol symbol, bool isRiskRemovingOrder)
        {
            return new BankPoolClientOrderRequestInfo(sizeBaseAbsInitial, null, side, symbol, isRiskRemovingOrder)
            {
                OrderType = OrderType.Market
            };
        }

        internal static BankPoolClientOrderRequestInfo CreateMarketPreferLmax(decimal sizeBaseAbsInitial, DealDirection side, Symbol symbol, bool isRiskRemovingOrder)
        {
            return new BankPoolClientOrderRequestInfo(sizeBaseAbsInitial, null, side, symbol, isRiskRemovingOrder)
            {
                OrderType = OrderType.MarketPreferLmax
            };
        }

        internal static BankPoolClientOrderRequestInfo CreateMarketOnImprovement(decimal sizeBaseAbsInitial, TimeSpan waitOnImprovementTimeout, DealDirection side, Symbol symbol, bool isRiskRemovingOrder)
        {
            return new BankPoolClientOrderRequestInfo(sizeBaseAbsInitial, null, side, symbol, isRiskRemovingOrder)
            {
                OrderType = OrderType.MarketOnImprovement,
                TimeoutTillFlippingToMarket = waitOnImprovementTimeout
            };
        }

        internal static BankPoolClientOrderRequestInfo CreateLimitTillTimeout(decimal sizeBaseAbsInitial, decimal requestedPrice, TimeSpan timeoutForLimit, DealDirection side, Symbol symbol, bool isRiskRemovingOrder)
        {
            return new BankPoolClientOrderRequestInfo(sizeBaseAbsInitial, requestedPrice, side, symbol, isRiskRemovingOrder)
            {
                OrderType = OrderType.LimitTillTimeout,
                TimeoutTillFlippingToMarket = timeoutForLimit
            };
        }

        internal static BankPoolClientOrderRequestInfo CreateQuoted(decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side,
                                              PriceObject price, bool isRiskRemovingOrder)
        {
            if (ClientOrderRequestInfoBase.CanBeSatisfied(sizeBaseAbsInitial, requestedPrice, side, price))
            {
                return new BankPoolClientOrderRequestInfo(sizeBaseAbsInitial, requestedPrice, side, price, isRiskRemovingOrder);
            }
            else
            {
                throw new ClientGatewayException("Requested price and/or SizeBaseAbsInitial cannot be satisfied from given quote");
            }
        }
    }

    [DataContract]
    public class VenueClientOrderRequestInfo : ClientOrderRequestInfoBase
    {

        protected VenueClientOrderRequestInfo(decimal sizeBaseAbsInitial, decimal? requestedPrice, DealDirection side,
                                             Symbol symbol, bool isRiskRemovingOrder)
            :base(sizeBaseAbsInitial, requestedPrice, side, symbol, isRiskRemovingOrder)
        { }

        [DataMember]
        public TimeInForce TimeInForce { get; protected set; }

        [DataMember]
        public decimal SizeBaseAbsFillMinimum { get; protected set; }

        [DataMember]
        public VenueRequestType RequestType { get; protected set; }

        protected Counterparty _counterparty;
        public override Counterparty Counterparty { get { return _counterparty; } }

        [DataMember]
        //this member is needed so that we can (de)serialize Counterparty while still using the same instance of this type
        private int CounterpartySerialized
        {
            get { return (int)this._counterparty; }
            set { this._counterparty = (Counterparty)value; }
        }

        [DataMember]
        public string OrderIdToCancel { get; protected set; }

        //[DataMember]
        //public RejectableDealDefaults RejectableDealDefaults { get; protected set; }

        protected static void CheckSizes(decimal sizeBaseAbsInitial, decimal sizeBaseAbsFillMinimum)
        {
            if (sizeBaseAbsInitial < sizeBaseAbsFillMinimum)
            {
                throw new ClientGatewayException(
                    string.Format("SizeBaseAbsFillMinimum ({0}) cannot be greater that SizeBaseAbsInitial ({1})",
                                  sizeBaseAbsFillMinimum,
                                  sizeBaseAbsInitial));
            }
        }

        protected static void CheckReplacementType(VenueClientOrderRequestInfo replacementOrderInfo)
        {
            if (replacementOrderInfo.RequestType != VenueRequestType.SingleOrder &&
                replacementOrderInfo.RequestType != VenueRequestType.SingleOrderReplace)
            {
                throw new ClientGatewayException("Attempt to create OrderReplaceRequest with order to be replaced different from HotspotRequestType.SingleOrder or HotspotRequestType.SingleOrderReplace");
            }
        }

        protected static void CheckReplacementDetails(VenueClientOrder clientOrderToCancel, VenueClientOrderRequestInfo replacementOrderInfo)
        {
            if (replacementOrderInfo.OrderType != clientOrderToCancel.OrderRequestInfo.OrderType
                || replacementOrderInfo.IntegratorDealDirection != clientOrderToCancel.OrderRequestInfo.IntegratorDealDirection
                || replacementOrderInfo.Symbol != clientOrderToCancel.OrderRequestInfo.Symbol
                || replacementOrderInfo.TimeInForce != clientOrderToCancel.VenueClientOrderRequestInfo.TimeInForce)
            {
                throw new ClientGatewayException("Attempt to create OrderReplaceRequest with parameters differing from order to be replaced");
            }

            if (clientOrderToCancel.OrderRequestInfo.OrderType == OrderType.Pegged)
            {
                throw new ClientGatewayException("Attempt to replace Pegged order - this is not allowed by counterparty");
            }

            if (replacementOrderInfo.Counterparty !=
                clientOrderToCancel.VenueClientOrderRequestInfo.Counterparty)
            {
                throw new ClientGatewayException("Attempt to create OrderReplaceRequest for different Hotspot connection type than the order being replaced");
            }
        }
    }

    [DataContract]
    public class HotspotClientOrderRequestInfo : VenueClientOrderRequestInfo
    {

        public HotspotCounterparty HotspotCounterparty 
        {
            get { return (HotspotCounterparty)this.Counterparty; }
        }

        [DataMember]
        public PegType PegType { get; private set; }

        [DataMember]
        public decimal PegDifferenceBasePolAlwaysAdded { get; private set; }

        private HotspotClientOrderRequestInfo(decimal sizeBaseAbsInitial, decimal? requestedPrice, DealDirection side,
                                              Symbol symbol, HotspotCounterparty counterparty, bool isRiskRemovingOrder)
            : base(sizeBaseAbsInitial, requestedPrice, side, symbol, isRiskRemovingOrder)
        {
            this._counterparty = counterparty;
        }

        //public void SetRejectableDefaults(RejectableDealDefaults rejectableDealDefaults)
        //{
        //    if(this.TimeInForce != TimeInForce.Day)
        //        throw new ClientGatewayException("Attempt to set rejectable deal deafaults on non-Day order");

        //    this.RejectableDealDefaults = rejectableDealDefaults;
        //}

        internal static HotspotClientOrderRequestInfo CreateHotspotOrder(decimal sizeBaseAbsInitial, decimal sizeBaseAbsFillMinimum, decimal requestedPrice, DealDirection side, Symbol symbol, TimeInForce timeInForce, HotspotCounterparty counterparty, bool isRiskRemovingOrder)
        {
            VenueClientOrderRequestInfo.CheckSizes(sizeBaseAbsInitial, sizeBaseAbsFillMinimum);

            return new HotspotClientOrderRequestInfo(sizeBaseAbsInitial, requestedPrice, side, symbol, counterparty, isRiskRemovingOrder)
            {
                OrderType = OrderType.Limit,
                TimeInForce = timeInForce,
                SizeBaseAbsFillMinimum = sizeBaseAbsFillMinimum,
                RequestType = VenueRequestType.SingleOrder
            };
        }

        internal static HotspotClientOrderRequestInfo CreateHotspotDayPegOrder(decimal sizeBaseAbsInitial, decimal sizeBaseAbsFillMinimum, decimal limitPrice, PegType pegType, decimal pegDifferenceBasePolAlwaysAdded, DealDirection side, Symbol symbol, HotspotCounterparty counterparty, bool isRiskRemovingOrder)
        {
            VenueClientOrderRequestInfo.CheckSizes(sizeBaseAbsInitial, sizeBaseAbsFillMinimum);

            return new HotspotClientOrderRequestInfo(sizeBaseAbsInitial, limitPrice, side, symbol, counterparty, isRiskRemovingOrder)
            {
                OrderType = OrderType.Pegged,
                TimeInForce = TimeInForce.Day,
                SizeBaseAbsFillMinimum = sizeBaseAbsFillMinimum,
                RequestType = VenueRequestType.SingleOrder,
                PegType = pegType,
                PegDifferenceBasePolAlwaysAdded = pegDifferenceBasePolAlwaysAdded
            };
        }

        internal static HotspotClientOrderRequestInfo CreateHotspotOrderReplaceRequest(string clientOrderIdToCancel, HotspotClientOrderRequestInfo replacementOrderInfo)
        {
            VenueClientOrderRequestInfo.CheckReplacementType(replacementOrderInfo);
            VenueClientOrderRequestInfo.CheckSizes(replacementOrderInfo.SizeBaseAbsInitial, replacementOrderInfo.SizeBaseAbsFillMinimum);

            return new HotspotClientOrderRequestInfo(replacementOrderInfo.SizeBaseAbsInitial, replacementOrderInfo.RequestedPrice, replacementOrderInfo.IntegratorDealDirection, replacementOrderInfo.Symbol, replacementOrderInfo.HotspotCounterparty, replacementOrderInfo.IsRiskRemovingOrder)
            {
                OrderType = replacementOrderInfo.OrderType,
                TimeInForce = replacementOrderInfo.TimeInForce,
                SizeBaseAbsFillMinimum = replacementOrderInfo.SizeBaseAbsFillMinimum,
                RequestType = VenueRequestType.SingleOrderReplace,
                OrderIdToCancel = clientOrderIdToCancel,
            };
        }

        internal static HotspotClientOrderRequestInfo CreateHotspotOrderReplaceRequest(HotspotClientOrder clientOrderToCancel, HotspotClientOrderRequestInfo replacementOrderInfo)
        {
            VenueClientOrderRequestInfo.CheckReplacementDetails(clientOrderToCancel, replacementOrderInfo);

            return CreateHotspotOrderReplaceRequest(clientOrderToCancel.ClientOrderIdentity, replacementOrderInfo);
        }
    }


    [DataContract]
    public class FXallClientOrderRequestInfo : VenueClientOrderRequestInfo
    {
        public FXallCounterparty FXallCounterparty
        {
            get { return (FXallCounterparty)this.Counterparty; }
        }

        private FXallClientOrderRequestInfo(decimal sizeBaseAbsInitial, decimal? requestedPrice, DealDirection side,
                                              Symbol symbol, FXallCounterparty counterparty, bool isRiskRemovingOrder)
            : base(sizeBaseAbsInitial, requestedPrice, side, symbol, isRiskRemovingOrder)
        {
            this._counterparty = counterparty;
        }

        internal static FXallClientOrderRequestInfo CreateFXallOrder(decimal sizeBaseAbsInitial,
                                                                     decimal sizeBaseAbsFillMinimum,
                                                                     decimal requestedPrice, DealDirection side,
                                                                     Symbol symbol, TimeInForce timeInForce, bool isRiskRemovingOrder)
        {
            VenueClientOrderRequestInfo.CheckSizes(sizeBaseAbsInitial, sizeBaseAbsFillMinimum);

            return new FXallClientOrderRequestInfo(sizeBaseAbsInitial, requestedPrice, side, symbol,
                                                   FXallCounterparty.FA1, isRiskRemovingOrder)
                {
                    OrderType = OrderType.Limit,
                    TimeInForce = timeInForce,
                    SizeBaseAbsFillMinimum = sizeBaseAbsFillMinimum,
                    RequestType = VenueRequestType.SingleOrder
                };
        }

        internal static FXallClientOrderRequestInfo CreateFXallOrderReplaceRequest(string clientOrderIdToCancel, FXallClientOrderRequestInfo replacementOrderInfo)
        {
            VenueClientOrderRequestInfo.CheckReplacementType(replacementOrderInfo);
            VenueClientOrderRequestInfo.CheckSizes(replacementOrderInfo.SizeBaseAbsInitial, replacementOrderInfo.SizeBaseAbsFillMinimum);

            return new FXallClientOrderRequestInfo(replacementOrderInfo.SizeBaseAbsInitial, replacementOrderInfo.RequestedPrice, replacementOrderInfo.IntegratorDealDirection, replacementOrderInfo.Symbol, replacementOrderInfo.FXallCounterparty, replacementOrderInfo.IsRiskRemovingOrder)
            {
                OrderType = replacementOrderInfo.OrderType,
                TimeInForce = replacementOrderInfo.TimeInForce,
                SizeBaseAbsFillMinimum = replacementOrderInfo.SizeBaseAbsFillMinimum,
                RequestType = VenueRequestType.SingleOrderReplace,
                OrderIdToCancel = clientOrderIdToCancel,
            };
        }

        internal static FXallClientOrderRequestInfo CreateFXallOrderReplaceRequest(
            FXallClientOrder clientOrderToCancel, FXallClientOrderRequestInfo replacementOrderInfo)
        {
            VenueClientOrderRequestInfo.CheckReplacementDetails(clientOrderToCancel, replacementOrderInfo);

            return CreateFXallOrderReplaceRequest(clientOrderToCancel.ClientOrderIdentity, replacementOrderInfo);
        }
    }

    [DataContract]
    public class LmaxClientOrderRequestInfo : VenueClientOrderRequestInfo
    {
        public LmaxCounterparty LmaxCounterparty
        {
            get { return (LmaxCounterparty)this.Counterparty; }
        }

        private LmaxClientOrderRequestInfo(decimal sizeBaseAbsInitial, decimal? requestedPrice, DealDirection side,
                                              Symbol symbol, LmaxCounterparty counterparty, bool isRiskRemovingOrder)
            : base(sizeBaseAbsInitial, requestedPrice, side, symbol, isRiskRemovingOrder)
        {
            this._counterparty = counterparty;
        }

        internal static LmaxClientOrderRequestInfo CreateLmaxOrder(decimal sizeBaseAbsInitial,
                                                                     decimal requestedPrice, DealDirection side,
                                                                     Symbol symbol, TimeInForce timeInForce, LmaxCounterparty lmaxCounterparty, bool isRiskRemovingOrder)
        {
            return new LmaxClientOrderRequestInfo(sizeBaseAbsInitial, requestedPrice, side, symbol,
                                                   lmaxCounterparty, isRiskRemovingOrder)
            {
                OrderType = OrderType.Limit,
                TimeInForce = timeInForce,
                RequestType = VenueRequestType.SingleOrder
            };
        }

        internal static LmaxClientOrderRequestInfo CreateLmaxOrderReplaceRequest(string clientOrderIdToCancel,
                                                                                 LmaxClientOrderRequestInfo
                                                                                     replacementOrderInfo)
        {
            VenueClientOrderRequestInfo.CheckReplacementType(replacementOrderInfo);
            VenueClientOrderRequestInfo.CheckSizes(replacementOrderInfo.SizeBaseAbsInitial,
                                                   replacementOrderInfo.SizeBaseAbsFillMinimum);

            return new LmaxClientOrderRequestInfo(replacementOrderInfo.SizeBaseAbsInitial,
                                                  replacementOrderInfo.RequestedPrice, replacementOrderInfo.IntegratorDealDirection,
                                                  replacementOrderInfo.Symbol, replacementOrderInfo.LmaxCounterparty,
                                                  replacementOrderInfo.IsRiskRemovingOrder)
                {
                    OrderType = replacementOrderInfo.OrderType,
                    TimeInForce = replacementOrderInfo.TimeInForce,
                    SizeBaseAbsFillMinimum = replacementOrderInfo.SizeBaseAbsFillMinimum,
                    RequestType = VenueRequestType.SingleOrderReplace,
                    OrderIdToCancel = clientOrderIdToCancel,
                };
        }

        internal static LmaxClientOrderRequestInfo CreateLmaxOrderReplaceRequest(
            LmaxClientOrder clientOrderToCancel, LmaxClientOrderRequestInfo replacementOrderInfo)
        {
            VenueClientOrderRequestInfo.CheckReplacementDetails(clientOrderToCancel, replacementOrderInfo);

            return CreateLmaxOrderReplaceRequest(clientOrderToCancel.ClientOrderIdentity, replacementOrderInfo);
        }
    }

    [DataContract]
    public class FXCMClientOrderRequestInfo : VenueClientOrderRequestInfo
    {
        public FXCMCounterparty FXCMCounterparty
        {
            get { return (FXCMCounterparty)this.Counterparty; }
        }

        private FXCMClientOrderRequestInfo(decimal sizeBaseAbsInitial, decimal? requestedPrice, DealDirection side,
                                              Symbol symbol, FXCMCounterparty counterparty, bool isRiskRemovingOrder)
            : base(sizeBaseAbsInitial, requestedPrice, side, symbol, isRiskRemovingOrder)
        {
            this._counterparty = counterparty;
        }

        internal static FXCMClientOrderRequestInfo CreateFXCMOrder(decimal sizeBaseAbsInitial,
                                                                     decimal sizeBaseAbsFillMinimum,
                                                                     decimal requestedPrice, DealDirection side,
                                                                     Symbol symbol, TimeInForce timeInForce,
                                                                     FXCMCounterparty fxcmCounterparty, bool isRiskRemovingOrder)
        {
            VenueClientOrderRequestInfo.CheckSizes(sizeBaseAbsInitial, sizeBaseAbsFillMinimum);

            return new FXCMClientOrderRequestInfo(sizeBaseAbsInitial, requestedPrice, side, symbol,
                                                   fxcmCounterparty, isRiskRemovingOrder)
            {
                OrderType = OrderType.Limit,
                TimeInForce = timeInForce,
                SizeBaseAbsFillMinimum = sizeBaseAbsFillMinimum,
                RequestType = VenueRequestType.SingleOrder
            };
        }

        internal static FXCMClientOrderRequestInfo CreateFXCMOrderReplaceRequest(string clientOrderIdToCancel, FXCMClientOrderRequestInfo replacementOrderInfo)
        {
            VenueClientOrderRequestInfo.CheckReplacementType(replacementOrderInfo);
            VenueClientOrderRequestInfo.CheckSizes(replacementOrderInfo.SizeBaseAbsInitial, replacementOrderInfo.SizeBaseAbsFillMinimum);

            return new FXCMClientOrderRequestInfo(replacementOrderInfo.SizeBaseAbsInitial, replacementOrderInfo.RequestedPrice, replacementOrderInfo.IntegratorDealDirection, replacementOrderInfo.Symbol, replacementOrderInfo.FXCMCounterparty, replacementOrderInfo.IsRiskRemovingOrder)
            {
                OrderType = replacementOrderInfo.OrderType,
                TimeInForce = replacementOrderInfo.TimeInForce,
                SizeBaseAbsFillMinimum = replacementOrderInfo.SizeBaseAbsFillMinimum,
                RequestType = VenueRequestType.SingleOrderReplace,
                OrderIdToCancel = clientOrderIdToCancel,
            };
        }

        internal static FXCMClientOrderRequestInfo CreateFXCMOrderReplaceRequest(
            FXCMClientOrder clientOrderToCancel, FXCMClientOrderRequestInfo replacementOrderInfo)
        {
            VenueClientOrderRequestInfo.CheckReplacementDetails(clientOrderToCancel, replacementOrderInfo);

            return CreateFXCMOrderReplaceRequest(clientOrderToCancel.ClientOrderIdentity, replacementOrderInfo);
        }
    }

    public static class ClientOrdersBuilder_OnlyForIntegratorUsage
    {
        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static LmaxClientOrderRequestInfo CreateLmaxOrder(
            decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side, Symbol symbol,
            TimeInForce timeInForce, LmaxCounterparty lmaxCounterparty)
        {
            return LmaxClientOrderRequestInfo.CreateLmaxOrder(sizeBaseAbsInitial, requestedPrice, side, symbol,
                                                              timeInForce, lmaxCounterparty, false);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static LmaxClientOrderRequestInfo CreateLmaxOrder(
            decimal sizeBaseAbsInitial, decimal requestedPrice, DealDirection side, Symbol symbol,
            TimeInForce timeInForce, LmaxCounterparty lmaxCounterparty, bool isRiskRemoving)
        {
            return LmaxClientOrderRequestInfo.CreateLmaxOrder(sizeBaseAbsInitial, requestedPrice, side, symbol,
                                                              timeInForce, lmaxCounterparty, isRiskRemoving);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static LmaxClientOrderRequestInfo CreateLmaxReplaceOrder(
            string clientOrderIdToCancel, LmaxClientOrderRequestInfo replacementOrderInfo)
        {
            return LmaxClientOrderRequestInfo.CreateLmaxOrderReplaceRequest(
                clientOrderIdToCancel, replacementOrderInfo);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static FXallClientOrderRequestInfo CreateFXallOrder(
            decimal sizeBaseAbsInitial, decimal sizeBaseAbsMinimum, decimal requestedPrice, DealDirection side, Symbol symbol,
            TimeInForce timeInForce)
        {
            return FXallClientOrderRequestInfo.CreateFXallOrder(sizeBaseAbsInitial, sizeBaseAbsMinimum, requestedPrice, side, symbol,
                                                              timeInForce, false);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static FXallClientOrderRequestInfo CreateFXallReplaceOrder(
            string clientOrderIdToCancel, FXallClientOrderRequestInfo replacementOrderInfo)
        {
            return FXallClientOrderRequestInfo.CreateFXallOrderReplaceRequest(
                clientOrderIdToCancel, replacementOrderInfo);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static HotspotClientOrderRequestInfo CreateHotspotOrder(
            decimal sizeBaseAbsInitial, decimal sizeBaseAbsFillMinimum, decimal requestedPrice, DealDirection side, Symbol symbol,
            TimeInForce timeInForce, HotspotCounterparty counteprarty)
        {
            return HotspotClientOrderRequestInfo.CreateHotspotOrder(sizeBaseAbsInitial, sizeBaseAbsFillMinimum,
                                                                    requestedPrice, side, symbol, timeInForce,
                                                                    counteprarty, false);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static HotspotClientOrderRequestInfo CreateHotspotReplaceOrder(
            string clientOrderIdToCancel, HotspotClientOrderRequestInfo replacementOrderInfo)
        {
            return HotspotClientOrderRequestInfo.CreateHotspotOrderReplaceRequest(
                clientOrderIdToCancel, replacementOrderInfo);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static FXCMClientOrderRequestInfo CreateFXCMOrder(
            decimal sizeBaseAbsInitial, decimal sizeBaseAbsMinimum, decimal requestedPrice, DealDirection side, Symbol symbol,
            TimeInForce timeInForce, FXCMCounterparty fxcmCounterparty)
        {
            return FXCMClientOrderRequestInfo.CreateFXCMOrder(sizeBaseAbsInitial, sizeBaseAbsMinimum, requestedPrice, side, symbol,
                                                              timeInForce, fxcmCounterparty, false);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static FXCMClientOrderRequestInfo CreateFXCMOrder(
            decimal sizeBaseAbsInitial, decimal sizeBaseAbsMinimum, decimal requestedPrice, DealDirection side, Symbol symbol,
            TimeInForce timeInForce, FXCMCounterparty fxcmCounterparty, bool isRiskRemoving)
        {
            return FXCMClientOrderRequestInfo.CreateFXCMOrder(sizeBaseAbsInitial, sizeBaseAbsMinimum, requestedPrice, side, symbol,
                                                              timeInForce, fxcmCounterparty, isRiskRemoving);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static FXCMClientOrderRequestInfo CreateFXCMReplaceOrder(
            string clientOrderIdToCancel, FXCMClientOrderRequestInfo replacementOrderInfo)
        {
            return FXCMClientOrderRequestInfo.CreateFXCMOrderReplaceRequest(
                clientOrderIdToCancel, replacementOrderInfo);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static BankPoolClientOrderRequestInfo CreateMarket(decimal sizeBaseAbsInitial, DealDirection side, Symbol symbol,
            bool isRiskRemovingOrder)
        {
            return BankPoolClientOrderRequestInfo.CreateMarket(sizeBaseAbsInitial, side, symbol, isRiskRemovingOrder);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static BankPoolClientOrderRequestInfo CreateMarketOnImprovement(decimal sizeBaseAbsInitial,
                                                                               TimeSpan waitOnImprovementTimeout,
                                                                               DealDirection side, Symbol symbol, bool isRiskRemovingOrder)
        {
            return BankPoolClientOrderRequestInfo.CreateMarketOnImprovement(sizeBaseAbsInitial, waitOnImprovementTimeout,
                                                                            side, symbol, isRiskRemovingOrder);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static BankPoolClientOrderRequestInfo CreateLimitTillTimeout(decimal sizeBaseAbsInitial, decimal requestedPrice,
                                                                               TimeSpan timeoutForLimit,
                                                                               DealDirection side, Symbol symbol, bool isRiskRemovingOrder)
        {
            return BankPoolClientOrderRequestInfo.CreateLimitTillTimeout(sizeBaseAbsInitial, requestedPrice, timeoutForLimit,
                                                                            side, symbol, isRiskRemovingOrder);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static BankPoolClientOrderRequestInfo CreateMarket(
            bool preferLmax, decimal sizeBaseAbsInitial, DealDirection side, Symbol symbol, bool isRiskRemovingOrder)
        {
            if(preferLmax)
                return BankPoolClientOrderRequestInfo.CreateMarketPreferLmax(sizeBaseAbsInitial, side, symbol, isRiskRemovingOrder);
            else
                return BankPoolClientOrderRequestInfo.CreateMarket(sizeBaseAbsInitial, side, symbol, isRiskRemovingOrder);
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static LmaxClientOrder CreateLmaxClientOrder(
            string identity, string clientIdentity, LmaxClientOrderRequestInfo orderRequestInfo,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return new LmaxClientOrder(identity, clientIdentity, orderRequestInfo)
            {
                IntegratedTradingSystemIdentification = integratedTradingSystemIdentification
            };
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static GliderLmaxClientOrder CreateGliderLmaxClientOrder(
            string identity, string clientIdentity, LmaxClientOrderRequestInfo orderRequestInfo,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification, GlidingInfoBag gib)
        {
            return new GliderLmaxClientOrder(identity, clientIdentity, orderRequestInfo, gib)
            {
                IntegratedTradingSystemIdentification = integratedTradingSystemIdentification
            };
        }

        

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static FXallClientOrder CreateFXallClientOrder(
            string identity, string clientIdentity, FXallClientOrderRequestInfo orderRequestInfo,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return new FXallClientOrder(identity, clientIdentity, orderRequestInfo)
            {
                IntegratedTradingSystemIdentification = integratedTradingSystemIdentification
            };
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static HotspotClientOrder CreateHotspotClientOrder(
            string identity, string clientIdentity, HotspotClientOrderRequestInfo orderRequestInfo,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return new HotspotClientOrder(identity, clientIdentity, orderRequestInfo)
            {
                IntegratedTradingSystemIdentification = integratedTradingSystemIdentification
            };
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static FXCMClientOrder CreateFXCMClientOrder(
            string identity, string clientIdentity, FXCMClientOrderRequestInfo orderRequestInfo,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return new FXCMClientOrder(identity, clientIdentity, orderRequestInfo)
            {
                IntegratedTradingSystemIdentification = integratedTradingSystemIdentification
            };
        }

        [Obsolete("ClientOrdersBuilder_OnlyForIntegratorUsage should be used only by integrator internals")]
        public static BankPoolClientOrder CreateBankPoolClientOrder(
            string identity, string clientIdentity, BankPoolClientOrderRequestInfo orderRequestInfo,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification, GlidingInfoBag gib)
        {
            if (gib == null)
            {
                return new BankPoolClientOrder(identity, clientIdentity, orderRequestInfo)
                {
                    IntegratedTradingSystemIdentification = integratedTradingSystemIdentification
                };
            }
            else
            {
                return new GliderBankPoolClientOrder(identity, clientIdentity, orderRequestInfo, gib)
                {
                    IntegratedTradingSystemIdentification = integratedTradingSystemIdentification
                };
            }
        }
    }
}
