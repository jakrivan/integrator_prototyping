﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    [DataContract]
    [KnownType(typeof(IntegratorCancelAllOrdersForSingleClientRequestInfo))]
    [KnownType(typeof(BankPoolClientOrder))]
    [KnownType(typeof(HotspotClientOrder))]
    [KnownType(typeof(FXallClientOrder))]
    [KnownType(typeof(LmaxClientOrder))]
    [KnownType(typeof(CancelOrderRequestInfo))]
    //[KnownType(typeof(IntegratorDealInternalFinalizationRequest))]
    [KnownType(typeof(IntegratorTransferableSubscriptionRequest))]
    [KnownType(typeof(MulticastRequestInfo))]
    public abstract class IntegratorRequestInfo
    { }

    //All the followings (and others) would need to be here if we wouldn't have translating type - IntegratorTransferableSubscriptionRequest

    //[KnownType(typeof(TopOfBookIntegratorSubscriptionRequest<BankCounterparty>))]
    //[KnownType(typeof(TopOfBookIntegratorSubscriptionRequest<HotspotCounterparty>))]
    //[KnownType(typeof(TopOfBookIntegratorSubscriptionRequest<FXallCounterparty>))]
    //[KnownType(typeof(TopOfBookIntegratorSubscriptionRequest<LmaxCounterparty>))]

    //[KnownType(typeof(TopOfBookIntegratorUnsubscriptionRequest<BankCounterparty>))]
    //[KnownType(typeof(TopOfBookIntegratorUnsubscriptionRequest<HotspotCounterparty>))]
    //[KnownType(typeof(TopOfBookIntegratorUnsubscriptionRequest<FXallCounterparty>))]
    //[KnownType(typeof(TopOfBookIntegratorUnsubscriptionRequest<LmaxCounterparty>))]

    //[KnownType(typeof(IndividualPricesIntegratorSubscriptionRequest<BankCounterparty>))]
    //[KnownType(typeof(IndividualPricesIntegratorSubscriptionRequest<HotspotCounterparty>))]
    //[KnownType(typeof(IndividualPricesIntegratorSubscriptionRequest<FXallCounterparty>))]
    //[KnownType(typeof(IndividualPricesIntegratorSubscriptionRequest<LmaxCounterparty>))]

    //[KnownType(typeof(IndividualPricesIntegratorUnsubscriptionRequest<BankCounterparty>))]
    //[KnownType(typeof(IndividualPricesIntegratorUnsubscriptionRequest<HotspotCounterparty>))]
    //[KnownType(typeof(IndividualPricesIntegratorUnsubscriptionRequest<FXallCounterparty>))]
    //[KnownType(typeof(IndividualPricesIntegratorUnsubscriptionRequest<LmaxCounterparty>))]
}
