﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts
{
    public interface IPriceBookInfo<TNode>
    {
        bool IsEmpty { get; }
        TNode[] GetSortedClone();
        TNode[] GetSortedClone(Func<TNode, bool> selectorFunc);

        //this was only for testing purposes
        //TNode GetIdentical(TNode node);

        bool IsImprovement { get; }
    }

    public interface IPriceBook<TNode, TIdentity> : IPriceBookInfo<TNode>, IBookTopExtended<TNode, TIdentity>
    { }

    public interface IChangeablePriceBook<TNode, TIdentity> : IPriceBook<TNode, TIdentity>
    {
        event Action<TNode> ChangeableBookTopImproved;
        event Action<TNode> ChangeableBookTopDeteriorated;
        event Action<TNode> ChangeableBookTopReplaced;
        bool RemoveIdentical(TNode value);
        bool RemoveIdentical(Guid identificator);
        void ResortIdentical(TNode value);
        decimal GetWeightedPriceForSize(ref decimal maxSizeOnPrice, int maximumBookDepthToBeUsed);
        decimal GetWeightedPriceForExactSize(decimal sizeOnPrice, int maximumBookDepthToBeUsed);
        //decimal GetWeightedPriceForExactSize(decimal sizeOnPrice, int maximumBookDepthToBeUsed, Counterparty[] counterpartiesWhitelist);
        decimal GetMaximumSizeForExactPrice(decimal worstAcceptablePrice, int maximumBookDepthToBeUsed, decimal maximumSizeToStopProbing);
        decimal GetMaximumSizeForExactPrice(decimal worstAcceptablePrice, int maximumBookDepthToBeUsed, decimal maximumSizeToStopProbing, Counterparty[] counterpartiesWhitelist);
        AtomicDecimal GetEstimatedBestPriceFast();
    }

    public interface IPriceBookStore
    {
        IChangeablePriceBook<PriceObjectInternal, Counterparty> GetChangeableBidPriceBook(Symbol symbol);
        IChangeablePriceBook<PriceObjectInternal, Counterparty> GetChangeableAskPriceBook(Symbol symbol);
    }

    public interface IPriceBookStoreEx : IBookTopProvider<PriceObjectInternal>, IPriceBookStore
    { }

    public interface IBookTopProvidersStore
    {
        IBookTopProvider<PriceObjectInternal> GetBookTopProvider(Counterparty counterparty);
        IPriceBookStoreEx TryGetBookTopProvider(Counterparty counterparty);
        IPriceBookStoreEx BankpoolBookTopProvider { get; }
        IPriceBookStore FirmpoolBookTopProvider { get; }
        IEnumerable<IBookTopProvider<PriceObjectInternal>> AllBookTopProviders { get; }
    }
}
