﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    //[DataContract]
    //public class IntegratorDealInternalFinalizationRequest: IntegratorRequestInfo
    //{
    //    [DataMember]
    //    public DealConfirmationAction DealConfirmationAction { get; private set; }

    //    [DataMember]
    //    public string IntegratorInternalTransactionIdentity { get; private set; }

    //    public Counterparty Counterparty { get; private set; }

    //    [DataMember]
    //    //this member is needed so that we can (de)serialize Counterparty while still using the same instance of this type
    //    private int? CounterpartySerialized
    //    {
    //        get { return this.Counterparty == null ? (int?)null : (int)this.Counterparty; }
    //        set { this.Counterparty = value == null ? null : (Counterparty)value; }
    //    }

    //    public IntegratorDealInternalFinalizationRequest(DealConfirmationAction dealConfirmationAction, string integratorInternalTransactionIdentity, Counterparty counterparty)
    //    {
    //        this.DealConfirmationAction = dealConfirmationAction;
    //        this.IntegratorInternalTransactionIdentity = integratorInternalTransactionIdentity;
    //        this.Counterparty = counterparty;
    //    }

    //    public IntegratorDealInternalFinalizationRequest(DealConfirmationAction dealConfirmationAction, IntegratorUnconfirmedDealInternal integratorDealInternal)
    //        : this(dealConfirmationAction, integratorDealInternal.InternalTransactionIdentity, integratorDealInternal.Counterparty)
    //    { }

    //    public override string ToString()
    //    {
    //        return string.Format("DealInternalFinalizationRequest: {0} transaction [{1}] for {2}",
    //                             this.DealConfirmationAction, this.IntegratorInternalTransactionIdentity,
    //                             this.Counterparty);
    //    }
    //}
}
