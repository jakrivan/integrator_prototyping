﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public enum PriceUpdateType
    {
        PriceOnTopOfBookImproved,
        PriceOnTopOfBookDeteriorated,
        PriceOnTopOfBookAdded,
        PriceOnTopOfBookRemoved,
        PriceOnTopOfBookReplaced,
        PriceNewArrived,
        PriceExistingInvalidated,
        PricesRemoved,
        VenueDealUpdate
    }

    public enum PriceUpdateCategory
    {
        TobOfBookUpdate,
        IndividualPriceUpdate,
        VenueDealUpdate,
        Unknow
    }

    //This class should hold all 'business' logic tight to PriceUpdateEventArgs types
    // so that this is on one place and not scattered around the messaging and business logic
    public static class PriceUpdateEventArgsHelper
    {
        public static bool PreventRemoteTransfer(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            return
                priceUpdateEventArgs.PriceUpdateType == PriceUpdateType.PricesRemoved ||
                priceUpdateEventArgs.PriceUpdateType == PriceUpdateType.PriceExistingInvalidated;
        }

        public static Symbol GetSymbolIfPresent(PriceUpdateEventArgs priceChangeEventArgs)
        {
            if (priceChangeEventArgs is SinglePriceChangeEventArgs)
            {
                return (priceChangeEventArgs as SinglePriceChangeEventArgs).PriceObject.Symbol;
            }
            else if (priceChangeEventArgs is VenueDealObject)
            {
                return (priceChangeEventArgs as VenueDealObject).Symbol;
            }
            else
            {
                return Symbol.NULL;
            }
        }

        public static PriceObject GetPriceIfCachingOfLastPriceRequired(PriceUpdateEventArgs priceChangeEventArgs)
        {

            if ((priceChangeEventArgs.PriceUpdateType == PriceUpdateType.PriceOnTopOfBookImproved ||
                 priceChangeEventArgs.PriceUpdateType == PriceUpdateType.PriceOnTopOfBookDeteriorated ||
                 priceChangeEventArgs.PriceUpdateType == PriceUpdateType.PriceOnTopOfBookAdded ||
                 priceChangeEventArgs.PriceUpdateType == PriceUpdateType.PriceOnTopOfBookRemoved ||
                 priceChangeEventArgs.PriceUpdateType == PriceUpdateType.PriceOnTopOfBookReplaced) &&
                priceChangeEventArgs is SinglePriceChangeEventArgs)
            {
                return (priceChangeEventArgs as SinglePriceChangeEventArgs).PriceObject;
            }

            return null;
        }

        public static PriceUpdateCategory GetPriceUpdateEventCategory(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            switch (priceUpdateEventArgs.PriceUpdateType)
            {
                case PriceUpdateType.PriceOnTopOfBookImproved:
                case PriceUpdateType.PriceOnTopOfBookDeteriorated:
                case PriceUpdateType.PriceOnTopOfBookAdded:
                case PriceUpdateType.PriceOnTopOfBookRemoved:
                case PriceUpdateType.PriceOnTopOfBookReplaced:
                    return PriceUpdateCategory.TobOfBookUpdate;
                case PriceUpdateType.PriceNewArrived:
                case PriceUpdateType.PriceExistingInvalidated:
                case PriceUpdateType.PricesRemoved:
                    return PriceUpdateCategory.IndividualPriceUpdate;
                case PriceUpdateType.VenueDealUpdate:
                    return PriceUpdateCategory.VenueDealUpdate;
                default:
                    return PriceUpdateCategory.Unknow;
            }
        }
    }

    [DataContract]
    //[KnownType("GetKnownType")]
    [KnownType(typeof(PriceObjectInternal))]
    [KnownType(typeof(SinglePriceChangeEventArgs))]
    [KnownType(typeof(VenueDealObject))]
    public class PriceUpdateEventArgs : ITradingEvent
    {
        //private static Type[] GetKnownType()
        //{
        //    Type[] t = new Type[4];
        //    t[0] = typeof(WritablePriceObject);
        //    t[1] = PriceObjectPool.PooledPriceType; //typeof(PriceObjectPool.PooledWritablePriceObject);
        //    t[2] = typeof(SinglePriceChangeEventArgs);
        //    t[3] = typeof(VenueDealObject);
        //    return t;
        //}

        public PriceUpdateEventArgs(PriceUpdateType priceUpdateType, DateTime integratorEventTimeUtc, TradingTargetType senderTradingTargetType, Counterparty originatingCounterparty)
        {
            this.PriceUpdateType = priceUpdateType;
            this.IntegratorEventTimeUtc = integratorEventTimeUtc;
            this.TradingEventType = TradingEventType.PriceUpdate;
            this.SenderTradingTargetType = senderTradingTargetType;
            this.Counterparty = originatingCounterparty;
        }

        //Ctor for pooling purposes
        protected internal PriceUpdateEventArgs(PriceUpdateType priceUpdateType)
        {
            this.PriceUpdateType = priceUpdateType;
        }

        internal void OverrideContent(DateTime integratorEventTimeUtc,
            TradingTargetType senderTradingTargetType, Counterparty originatingCounterparty)
        {  
            this.IntegratorEventTimeUtc = integratorEventTimeUtc;
            this.TradingEventType = TradingEventType.PriceUpdate;
            this.SenderTradingTargetType = senderTradingTargetType;
            this.Counterparty = originatingCounterparty;
        }

        [DataMember]
        public PriceUpdateType PriceUpdateType { get; private set; }
        [DataMember]
        public DateTime IntegratorEventTimeUtc { get; private set; }

        [DataMember]
        //This can differ from actual trading target type. E.g. LMAX price sent from BankPool ToB will have this set to BankPool
        public TradingTargetType SenderTradingTargetType { get; private set; }

        public Counterparty Counterparty { get; protected set; }

        [DataMember]
        private int CounterpartySerialized
        {
            get { return (int)this.Counterparty; }
            set { this.Counterparty = (Counterparty)value; }
        }

        public override string ToString()
        {
            return string.Format("PriceUpdateEventArgs ({0}), senderTradingTargetType: {1}, time: {2:HH:mm:ss.fffffff UTC}", this.PriceUpdateType,
                                 this.SenderTradingTargetType, this.IntegratorEventTimeUtc);
        }
    }

    [DataContract]
    public class SinglePriceChangeEventArgs: PriceUpdateEventArgs, IPooledObject
    {
        public SinglePriceChangeEventArgs(PriceUpdateType priceUpdateType, DateTime integratorEventTimeUtc, PriceObject price, TradingTargetType senderTradingTargetType)
            : base(priceUpdateType, integratorEventTimeUtc, senderTradingTargetType, price.Counterparty)
        {
            this.PriceObject = price;
        }

        [DataMember]
        public PriceObject PriceObject { get; protected set; }

        public override string ToString()
        {
            return string.Format("SinglePriceChangeEventArgs ({0}), PriceObject: {1}, time: {2:HH:mm:ss.fffffff UTC}", this.PriceUpdateType,
                                 this.PriceObject, this.IntegratorEventTimeUtc);
        }

        private PriceObjectInternal GetPriceObjectInternal()
        {
            PriceObjectInternal priceInternal = this.PriceObject as PriceObjectInternal;
            if (priceInternal == null)
                throw new Exception("Attempt to perform pooling operations on non-internal priceobjects");
            return priceInternal;
        }

        public void Acquire()
        {
            this.GetPriceObjectInternal().Acquire();
        }

        public void Release()
        {
            this.GetPriceObjectInternal().Release();
        }
    }

    //TODO:
    //public class VenueDealUpdateEventArgs: PriceUpdateEventArgs
    //{
        
    //}

}
