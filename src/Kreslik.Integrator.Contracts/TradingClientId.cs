﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts
{
    [DataContract]
    public class TradingClientId
    {
        public string ClinetId { get; set; }
        public string ProxyGatewayId { get; set; }
    }
}
