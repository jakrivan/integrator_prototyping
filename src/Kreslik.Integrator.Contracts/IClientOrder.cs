﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    public enum ClientOrderStatus
    {
        NewInIntegrator,
        OpenInIntegrator,
        ConfirmedByCounterparty,
        RejectedByIntegrator,
        RemovedFromIntegrator,
        BrokenInIntegrator,
        NotActiveInIntegrator,
    }

    public enum ClientOrderCancelRequestStatus
    {
        CancelNotRequested,
        CancelRequestInProgress,
        CancelRequestAccepted,
        CancelRequestFailed
    }

    public interface IClientOrder : IIntegratorOrderInfo
    {
        string ClientOrderIdentity { get; }
        string ClientIdentity { get; }
        ClientOrderStatus OrderStatus { get; }
        decimal SizeBaseAbsFilled { get; }
        decimal SizeBaseAbsActive { get; }
        ClientOrderRequestInfoBase OrderRequestInfo { get; }
        TradingTargetType TradingTargetType { get; }
        DateTime CreatedUtc { get; }
        IntegratedTradingSystemIdentification IntegratedTradingSystemIdentification { get; set; }
    }
}
