﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    [DataContract]
    public class BankPoolClientOrderUpdateInfo: ClientOrderUpdateInfo
    {
        public static ClientOrderUpdateInfo CreateFullyRejectedClientOrderUpdateInfo(IClientOrder cancelOrderRequestInfo,
                                                                                string integratorRejectionReason)
        {
            return new BankPoolClientOrderUpdateInfo(cancelOrderRequestInfo, cancelOrderRequestInfo.ClientOrderIdentity, cancelOrderRequestInfo.ClientIdentity,
                                             ClientOrderStatus.RejectedByIntegrator,
                                             ClientOrderCancelRequestStatus.CancelNotRequested,
                                             0m, cancelOrderRequestInfo.SizeBaseAbsActive, 0m, integratorRejectionReason, 
                                             cancelOrderRequestInfo.OrderRequestInfo.RequestedPrice, cancelOrderRequestInfo.OrderRequestInfo.IntegratorDealDirection);
        }

        public static ClientOrderUpdateInfo CreateOpenClientOrderUpdateInfo(IClientOrder clientOrder)
        {
            return new BankPoolClientOrderUpdateInfo(clientOrder, clientOrder.ClientOrderIdentity, clientOrder.ClientIdentity,
                                             ClientOrderStatus.OpenInIntegrator,
                                             ClientOrderCancelRequestStatus.CancelNotRequested,
                                             0m, 0m, clientOrder.SizeBaseAbsActive, string.Empty, 
                                             clientOrder.OrderRequestInfo.RequestedPrice, clientOrder.OrderRequestInfo.IntegratorDealDirection);
        }

        //public static ClientOrderUpdateInfo CreateRemovedClientOrderUpdateInfo(
        //    string clientOrderIdentity, string clientIdentity, decimal rejectedAmountBaseAbs, decimal? requestedPrice, DealDirection? side)
        //{
        //    return new BankPoolClientOrderUpdateInfo(clientOrderIdentity, clientIdentity,
        //                                     ClientOrderStatus.NotActiveInIntegrator,
        //                                     ClientOrderCancelRequestStatus.CancelNotRequested,
        //                                     0m, rejectedAmountBaseAbs, 0m, string.Empty, requestedPrice, side);
        //}

        public static ClientOrderUpdateInfo CreateRemovedClientOrderUpdateInfo(IClientOrder clientOrder, decimal rejectedAmountBaseAbs, bool markedForCancellation)
        {
            return new BankPoolClientOrderUpdateInfo(clientOrder, clientOrder.ClientOrderIdentity, clientOrder.ClientIdentity,
                                             ClientOrderStatus.NotActiveInIntegrator,
                                             markedForCancellation ? ClientOrderCancelRequestStatus.CancelRequestAccepted : ClientOrderCancelRequestStatus.CancelNotRequested,
                                             0m, rejectedAmountBaseAbs, 0m, string.Empty, 
                                             clientOrder.OrderRequestInfo.RequestedPrice, clientOrder.OrderRequestInfo.IntegratorDealDirection);
        }

        public static ClientOrderUpdateInfo CreateCancelledClientOrderUpdateInfo(IClientOrder clientOrder,
                                                                                 decimal totalCancelledSizeBaseAbs)
        {
            return new BankPoolClientOrderUpdateInfo(clientOrder,
                                             ClientOrderCancelRequestStatus.CancelRequestAccepted,
                                             totalCancelledSizeBaseAbs, 0m, string.Empty);
        }

        public static ClientOrderUpdateInfo CreateCancellFailedClientOrderUpdateInfo(IClientOrder clientOrder, decimal totalAmountRemaining)
        {
            //ctor takeing clientOrder doesn't take PendingAmount in account - as it is not exposed!
            return new BankPoolClientOrderUpdateInfo(clientOrder, clientOrder.ClientOrderIdentity, clientOrder.ClientIdentity,
                                                     clientOrder.OrderStatus,
                                                     ClientOrderCancelRequestStatus.CancelRequestFailed, 0m, 0m,
                                                     totalAmountRemaining, string.Empty,
                                                     clientOrder.OrderRequestInfo.RequestedPrice,
                                                     clientOrder.OrderRequestInfo.IntegratorDealDirection);

            //return new BankPoolClientOrderUpdateInfo(clientOrder, ClientOrderCancelRequestStatus.CancelRequestFailed,
            //                                 0m, 0m, string.Empty);
        }

        public static ClientOrderUpdateInfo CreateCancellInProgressClientOrderUpdateInfo(IClientOrder clientOrder, decimal totalAmountRemaining, decimal canceledAmount)
        {
            //ctor takeing clientOrder doesn't take PendingAmount in account - as it is not exposed!
            return new BankPoolClientOrderUpdateInfo(clientOrder, clientOrder.ClientOrderIdentity, clientOrder.ClientIdentity,
                                                     clientOrder.OrderStatus,
                                                     ClientOrderCancelRequestStatus.CancelRequestInProgress, canceledAmount, 0m,
                                                     totalAmountRemaining, string.Empty,
                                                     clientOrder.OrderRequestInfo.RequestedPrice,
                                                     clientOrder.OrderRequestInfo.IntegratorDealDirection);

            //return new BankPoolClientOrderUpdateInfo(clientOrder, ClientOrderCancelRequestStatus.CancelRequestFailed,
            //                                 0m, 0m, string.Empty);
        }

        public GlidingInfoBag GlidingInfoBag { get; private set; }

        //Be carefull!! Cannot use clientOrder param to get other properties - as it can be null (cancel unknown order)
        public BankPoolClientOrderUpdateInfo(IClientOrder clientOrder, string clientOrderIdentity, string clientIdentity,
            ClientOrderStatus orderStatus,
            ClientOrderCancelRequestStatus cancelRequestStatus,
            decimal cancelledSizeBaseAbs, decimal sizeBaseAbsRejected, decimal sizeBaseAbsTotalRemaining,
            string integratorRejectionReason,
            decimal? requestedPrice, DealDirection? side)
            : base(clientOrderIdentity, clientIdentity, TradingTargetType.BankPool, orderStatus,
                cancelRequestStatus, cancelledSizeBaseAbs, sizeBaseAbsRejected, sizeBaseAbsTotalRemaining,
                integratorRejectionReason, requestedPrice, side)
        {
            GliderBankPoolClientOrder gliderOrder = clientOrder as GliderBankPoolClientOrder;
            if (gliderOrder != null)
                this.GlidingInfoBag = gliderOrder.GlidingInfoBag;
        }

        public BankPoolClientOrderUpdateInfo(IClientOrder clientOrder,
            ClientOrderCancelRequestStatus cancelRequestStatus,
            decimal cancelledSizeBaseAbs, decimal sizeBaseAbsRejected, string integratorRejectionReason)
            : base(
                clientOrder, TradingTargetType.BankPool, cancelRequestStatus, cancelledSizeBaseAbs, sizeBaseAbsRejected,
                integratorRejectionReason)
        {
            GliderBankPoolClientOrder gliderOrder = clientOrder as GliderBankPoolClientOrder;
            if (gliderOrder != null)
                this.GlidingInfoBag = gliderOrder.GlidingInfoBag;
        }
    }

    [DataContract]
    public abstract class VenueClientOrderUpdateInfo : ClientOrderUpdateInfo
    {
        public bool HasPendingCancellation { get; protected set; }

        public static VenueClientOrderUpdateInfo CreateOpenClientOrderUpdateInfo(IVenueClientOrder clientOrder)
        {
            return CreateVenueClientOrderUpdateInfo(clientOrder as VenueClientOrder, clientOrder.ClientOrderIdentity, clientOrder.ClientIdentity,
                                                    //CAREFULL!! - order might not be already in opened status once executing this
                                                    /*ClientOrderStatus.OpenInIntegrator*/ clientOrder.OrderStatus,
                                                    ClientOrderCancelRequestStatus.CancelNotRequested,
                                                    0m, 0m, clientOrder.SizeBaseAbsActive, string.Empty,
                                                    clientOrder.TradingTargetType,
                                                    clientOrder.OrderRequestInfo.RequestedPrice,
                                                    clientOrder.OrderRequestInfo.IntegratorDealDirection, false);
        }

        public static VenueClientOrderUpdateInfo CreateConfirmedClientOrderUpdateInfo(VenueClientOrder clientOrder,
                                                                                Counterparty counterparty,
                                                                               decimal sizeBaseAbsTotalRemaining)
        {
            return CreateVenueClientOrderUpdateInfo(clientOrder,
                                                    ClientOrderStatus.ConfirmedByCounterparty,
                                                    ClientOrderCancelRequestStatus.CancelNotRequested,
                                                    0m, 0m, sizeBaseAbsTotalRemaining, string.Empty, counterparty);
        }

        public static VenueClientOrderUpdateInfo CreateFullyRejectedClientOrderUpdateInfo(IClientOrder clientOrder,
                                                                                string integratorRejectionReason)
        {
            return CreateVenueClientOrderUpdateInfo(clientOrder as VenueClientOrder, clientOrder.ClientOrderIdentity, clientOrder.ClientIdentity,
                                                    ClientOrderStatus.RejectedByIntegrator,
                                                    ClientOrderCancelRequestStatus.CancelNotRequested,
                                                    0m, clientOrder.SizeBaseAbsActive, 0m, integratorRejectionReason,
                                                    clientOrder.TradingTargetType,
                                                    clientOrder.OrderRequestInfo.RequestedPrice,
                                                    clientOrder.OrderRequestInfo.IntegratorDealDirection, false);
        }

        public static VenueClientOrderUpdateInfo CreateNotActiveClientOrderUpdateInfo(VenueClientOrder clientOrder,
            decimal rejectedAmount, string rejectionReason, Counterparty counterparty)
        {
            return CreateVenueClientOrderUpdateInfo(clientOrder,
                                                    ClientOrderStatus.NotActiveInIntegrator,
                                                    ClientOrderCancelRequestStatus.CancelNotRequested,
                                                    0m, rejectedAmount, 0m, rejectionReason, counterparty);
        }

        public static VenueClientOrderUpdateInfo CreateFullyCancelledClientOrderUpdateInfo(VenueClientOrder clientOrder,
            ClientOrderStatus orderStatus,
            decimal cancelledSizeBaseAbs, TradingTargetType tradingTargetType)
        {

            return CreateVenueClientOrderUpdateInfo(clientOrder, clientOrder.ClientOrderIdentity, clientOrder.ClientIdentity, orderStatus,
                                                    ClientOrderCancelRequestStatus.CancelRequestAccepted,
                                                    cancelledSizeBaseAbs, 0m, 0m, string.Empty, tradingTargetType,
                                                    clientOrder.RequestedPrice,
                                                    clientOrder.OrderRequestInfo.IntegratorDealDirection, clientOrder.HasPendingCancellation);
        }

        public static VenueClientOrderUpdateInfo CreateFailedCancellationUnknownClientOrderUpdateInfo(CancelOrderRequestInfo cancelOrderRequestInfo, string integratorRejectionReason)
        {
            return CreateVenueClientOrderUpdateInfo(null,
                cancelOrderRequestInfo.ClientOrderIdentity,
                cancelOrderRequestInfo.ClientIdentity,
                ClientOrderStatus.NotActiveInIntegrator,
                ClientOrderCancelRequestStatus
                    .CancelRequestFailed, 0m, 0m, 0m,
                integratorRejectionReason, cancelOrderRequestInfo.TradingTargetType, null, null, false);
        }

        public static VenueClientOrderUpdateInfo CreateVenueClientOrderUpdateInfo(
            VenueClientOrder clientOrder, ClientOrderStatus orderStatus,
            ClientOrderCancelRequestStatus cancelRequestStatus, decimal cancelledSizeBaseAbs,
            decimal sizeBaseAbsRejected, decimal sizeBaseAbsTotalRemaining, string integratorRejectionReason,
            Counterparty counterparty)
        {
            return CreateVenueClientOrderUpdateInfo(
                clientOrder, clientOrder.ClientOrderIdentity, clientOrder.ClientIdentity,
                orderStatus, cancelRequestStatus, cancelledSizeBaseAbs,
                sizeBaseAbsRejected, sizeBaseAbsTotalRemaining, integratorRejectionReason, counterparty.TradingTargetType,
                clientOrder.RequestedPrice, clientOrder.OrderRequestInfo.IntegratorDealDirection, clientOrder.HasPendingCancellation);
        }

        private static VenueClientOrderUpdateInfo CreateVenueClientOrderUpdateInfo(VenueClientOrder clientOrder,
            string clientOrderIdentity, string clientIdentity, ClientOrderStatus orderStatus,
            ClientOrderCancelRequestStatus cancelRequestStatus, decimal cancelledSizeBaseAbs,
            decimal sizeBaseAbsRejected, decimal sizeBaseAbsTotalRemaining, string integratorRejectionReason,
            TradingTargetType tradingTargetType, decimal? requestedPrice, DealDirection? side, bool hasPendingCancellation)
        {
            switch (tradingTargetType)
            {
                case TradingTargetType.Hotspot:
                    return new HotspotClientOrderUpdateInfo(clientOrderIdentity, clientIdentity, orderStatus,
                                                            cancelRequestStatus, cancelledSizeBaseAbs,
                                                            sizeBaseAbsRejected, sizeBaseAbsTotalRemaining,
                                                            integratorRejectionReason, requestedPrice, side, hasPendingCancellation);
                case TradingTargetType.FXall:
                    return new FXallClientOrderUpdateInfo(clientOrderIdentity, clientIdentity, orderStatus,
                                                          cancelRequestStatus, cancelledSizeBaseAbs, sizeBaseAbsRejected,
                                                          sizeBaseAbsTotalRemaining,
                                                          integratorRejectionReason, requestedPrice, side, hasPendingCancellation);
                case TradingTargetType.LMAX:
                    if (clientOrder != null && clientOrder is GliderLmaxClientOrder)
                    {
                        return new GliderClientOrderUpdateInfo(clientOrderIdentity, clientIdentity, orderStatus,
                            cancelRequestStatus, cancelledSizeBaseAbs, sizeBaseAbsRejected,
                            sizeBaseAbsTotalRemaining,
                            integratorRejectionReason, requestedPrice, side, hasPendingCancellation,
                            (clientOrder as GliderLmaxClientOrder).GlidingInfoBag);
                    }
                    else
                        return new LmaxClientOrderUpdateInfo(clientOrderIdentity, clientIdentity, orderStatus,
                                                         cancelRequestStatus, cancelledSizeBaseAbs, sizeBaseAbsRejected,
                                                         sizeBaseAbsTotalRemaining,
                                                         integratorRejectionReason, requestedPrice, side, hasPendingCancellation);
                case TradingTargetType.FXCM:
                    return new FXCMClientOrderUpdateInfo(clientOrderIdentity, clientIdentity, orderStatus,
                                                         cancelRequestStatus, cancelledSizeBaseAbs, sizeBaseAbsRejected,
                                                         sizeBaseAbsTotalRemaining,
                                                         integratorRejectionReason, requestedPrice, side, hasPendingCancellation);
                case TradingTargetType.BankPool:
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected VenueClientOrderUpdateInfo(string clientOrderIdentity, string clientIdentity,
            TradingTargetType tradingTargetType, ClientOrderStatus orderStatus,
            ClientOrderCancelRequestStatus cancelRequestStatus,
            decimal cancelledSizeBaseAbs, decimal sizeBaseAbsRejected, decimal sizeBaseAbsTotalRemaining,
            string integratorRejectionReason,
            decimal? requestedPrice, DealDirection? side, bool hasPendingCancellation)
            : base(clientOrderIdentity, clientIdentity, tradingTargetType, orderStatus,
                cancelRequestStatus, cancelledSizeBaseAbs, sizeBaseAbsRejected, sizeBaseAbsTotalRemaining,
                integratorRejectionReason,
                requestedPrice, side)
        {
            this.HasPendingCancellation = hasPendingCancellation;
        }
    }

    [DataContract]
    public class FXallClientOrderUpdateInfo : VenueClientOrderUpdateInfo
    {
        internal FXallClientOrderUpdateInfo(string clientOrderIdentity, string clientIdentity, ClientOrderStatus orderStatus,
                                     ClientOrderCancelRequestStatus cancelRequestStatus,
                                     decimal cancelledSizeBaseAbs, decimal sizeBaseAbsRejected, decimal sizeBaseAbsTotalRemaining, string integratorRejectionReason,
                                     decimal? requestedPrice, DealDirection? side, bool hasPendingCancellation)
            : base(clientOrderIdentity, clientIdentity, TradingTargetType.FXall, orderStatus,
                                     cancelRequestStatus, cancelledSizeBaseAbs, sizeBaseAbsRejected, sizeBaseAbsTotalRemaining, integratorRejectionReason, 
                                     requestedPrice, side, hasPendingCancellation)
        { }
    }

    [DataContract]
    public class FXCMClientOrderUpdateInfo : VenueClientOrderUpdateInfo
    {
        internal FXCMClientOrderUpdateInfo(string clientOrderIdentity, string clientIdentity, ClientOrderStatus orderStatus,
                                     ClientOrderCancelRequestStatus cancelRequestStatus,
                                     decimal cancelledSizeBaseAbs, decimal sizeBaseAbsRejected, decimal sizeBaseAbsTotalRemaining, string integratorRejectionReason,
                                     decimal? requestedPrice, DealDirection? side, bool hasPendingCancellation)
            : base(clientOrderIdentity, clientIdentity, TradingTargetType.FXCM, orderStatus,
                                     cancelRequestStatus, cancelledSizeBaseAbs, sizeBaseAbsRejected, sizeBaseAbsTotalRemaining, integratorRejectionReason,
                                     requestedPrice, side, hasPendingCancellation)
        { }
    }

    [DataContract]
    public class LmaxClientOrderUpdateInfo : VenueClientOrderUpdateInfo
    {
        internal LmaxClientOrderUpdateInfo(string clientOrderIdentity, string clientIdentity, ClientOrderStatus orderStatus,
                                     ClientOrderCancelRequestStatus cancelRequestStatus,
                                     decimal cancelledSizeBaseAbs, decimal sizeBaseAbsRejected, decimal sizeBaseAbsTotalRemaining, string integratorRejectionReason,
                                     decimal? requestedPrice, DealDirection? side, bool hasPendingCancellation)
            : base(clientOrderIdentity, clientIdentity, TradingTargetType.LMAX, orderStatus,
                                     cancelRequestStatus, cancelledSizeBaseAbs, sizeBaseAbsRejected, sizeBaseAbsTotalRemaining, integratorRejectionReason,
                                     requestedPrice, side, hasPendingCancellation)
        { }
    }

    [DataContract]
    public class GliderClientOrderUpdateInfo : LmaxClientOrderUpdateInfo
    {
        public GliderClientOrderUpdateInfo(string clientOrderIdentity, string clientIdentity,
            ClientOrderStatus orderStatus, ClientOrderCancelRequestStatus cancelRequestStatus,
            decimal cancelledSizeBaseAbs, decimal sizeBaseAbsRejected, decimal sizeBaseAbsTotalRemaining,
            string integratorRejectionReason, decimal? requestedPrice, DealDirection? side, bool hasPendingCancellation,
            GlidingInfoBag glidingInfoBag)
            : base(
                clientOrderIdentity, clientIdentity, orderStatus, cancelRequestStatus, cancelledSizeBaseAbs,
                sizeBaseAbsRejected, sizeBaseAbsTotalRemaining, integratorRejectionReason, requestedPrice, side,
                hasPendingCancellation)
        {
            this.GlidingInfoBag = glidingInfoBag;
        }

        //let's not put it in parent class as that way the numerous ClientorderUpdateInfo objects would need to be inspected by GC
        public GlidingInfoBag GlidingInfoBag { get; private set; }
    }

    [DataContract]
    public class HotspotClientOrderUpdateInfo : VenueClientOrderUpdateInfo
    {
        internal HotspotClientOrderUpdateInfo(string clientOrderIdentity, string clientIdentity, ClientOrderStatus orderStatus,
                                     ClientOrderCancelRequestStatus cancelRequestStatus,
                                     decimal cancelledSizeBaseAbs, decimal sizeBaseAbsRejected, decimal sizeBaseAbsTotalRemaining, string integratorRejectionReason,
                                     decimal? requestedPrice, DealDirection? side, bool hasPendingCancellation)
            : base(clientOrderIdentity, clientIdentity, TradingTargetType.Hotspot, orderStatus,
                                     cancelRequestStatus, cancelledSizeBaseAbs, sizeBaseAbsRejected, sizeBaseAbsTotalRemaining, integratorRejectionReason,
                                     requestedPrice, side, hasPendingCancellation)
        { }
    }


    [DataContract]
    [KnownType(typeof(BankPoolClientOrderUpdateInfo))]
    [KnownType(typeof(HotspotClientOrderUpdateInfo))]
    [KnownType(typeof(FXallClientOrderUpdateInfo))]
    [KnownType(typeof(LmaxClientOrderUpdateInfo))]
    [KnownType(typeof(GliderClientOrderUpdateInfo))]
    [KnownType(typeof(FXCMClientOrderUpdateInfo))]
    public class ClientOrderUpdateInfo : IntegratorTradingInfoObjectBase
    {
        protected ClientOrderUpdateInfo(string clientOrderIdentity, string clientIdentity, TradingTargetType tradingTargetType, ClientOrderStatus orderStatus,
                                     ClientOrderCancelRequestStatus cancelRequestStatus,
                                     decimal cancelledSizeBaseAbs, decimal sizeBaseAbsRejected, decimal sizeBaseAbsTotalRemaining, string integratorRejectionReason,
                                     decimal? requestedPrice, DealDirection? side)
            : base(tradingTargetType, clientIdentity)
        {
            this.ClientOrderIdentity = clientOrderIdentity;
            this.OrderStatus = orderStatus;
            this.CancelRequestStatus = cancelRequestStatus;
            this.SizeBaseAbsCancelled = cancelledSizeBaseAbs;
            this.SizeBaseAbsRejected = sizeBaseAbsRejected;
            this.SizeBaseAbsTotalRemaining = sizeBaseAbsTotalRemaining;
            this.IntegratorRejectionReason = integratorRejectionReason;
            this.RequestedPrice = requestedPrice;
            this.Side = side;
        }

        protected ClientOrderUpdateInfo(IClientOrder clientOrder, TradingTargetType tradingTargetType,
                                     ClientOrderCancelRequestStatus cancelRequestStatus,
                                     decimal cancelledSizeBaseAbs, decimal sizeBaseAbsRejected, string integratorRejectionReason)
            : this(clientOrder.ClientOrderIdentity, clientOrder.ClientIdentity, tradingTargetType, clientOrder.OrderStatus,
                   cancelRequestStatus, cancelledSizeBaseAbs, sizeBaseAbsRejected, clientOrder.SizeBaseAbsActive, 
                   integratorRejectionReason, clientOrder.OrderRequestInfo.RequestedPrice, clientOrder.OrderRequestInfo.IntegratorDealDirection)
        { }

        [DataMember]
        public string ClientOrderIdentity { get; private set; }

        [DataMember]
        public ClientOrderStatus OrderStatus { get; private set; }

        [DataMember]
        public ClientOrderCancelRequestStatus CancelRequestStatus { get; private set; }

        [DataMember]
        public decimal SizeBaseAbsCancelled { get; private set; }

        [DataMember]
        public decimal SizeBaseAbsRejected { get; private set; }

        [DataMember]
        public decimal SizeBaseAbsTotalRemaining { get; private set; }

        [DataMember]
        public string IntegratorRejectionReason { get; private set; }

        [DataMember]
        public decimal? RequestedPrice { get; private set; }

        [DataMember]
        public DealDirection? Side { get; private set; }

        public IClientOrder ClientOrder { get; set; }

        public override string ToString()
        {
            return
                string.Format(
                    "ClientOrderUpdateInfo for order [{0}]: {1}, {2}{3}",
                    ClientOrderIdentity, OrderStatus, CancelRequestStatus,
                    OrderStatus == ClientOrderStatus.RejectedByIntegrator
                        ? string.Format(". Rejection reason: {0}", IntegratorRejectionReason)
                        : CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestAccepted
                              ? string.Format(" Cancelled amount (BaseAbs): {0}", SizeBaseAbsCancelled)
                              : string.Empty);
        }
    }
}
