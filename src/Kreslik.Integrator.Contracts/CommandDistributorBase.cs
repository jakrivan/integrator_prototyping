﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public interface ILocalCommandDistributor
    {
        void RegisterWorker(string commandType, Func<string, IntegratorRequestResult> commandExecutor);
    }

    public abstract class CommandDistributorBase : ILocalCommandDistributor
    {
        private Dictionary<string, Func<string, IntegratorRequestResult>> _commandExecutors =
            new Dictionary<string, Func<string, IntegratorRequestResult>>(StringComparer.OrdinalIgnoreCase);
        protected ILogger _logger;

        public CommandDistributorBase(ILogger logger)
        {
            this._logger = logger;
        }

        public void RegisterWorker(string commandType, Func<string, IntegratorRequestResult> commandExecutor)
        {
            if (this._commandExecutors.ContainsKey(commandType))
            {
                this._commandExecutors[commandType] += commandExecutor;
            }
            else
            {
                this._commandExecutors[commandType] = commandExecutor;
            }
        }

        protected IntegratorRequestResult CrackCommand(string command)
        {
            string[] parts = command.Split(new char[] { ';' }, 2);
            if (parts.Length != 2)
            {
                this._logger.Log(LogLevel.Error, "Experienced invalid command {0}", command);
                return IntegratorRequestResult.CreateFailedResult("Invalid Command: " + command);
            }

            Func<string, IntegratorRequestResult> executor;
            if (!this._commandExecutors.TryGetValue(parts[0], out executor))
            {
                this._logger.Log(LogLevel.Error, "No suitable worker registered for command {0}", command);
                return IntegratorRequestResult.CreateFailedResult("Unknown Command Type: " + command);
            }

            try
            {
                this._logger.Log(LogLevel.Trace, "Executing command: [{0}]", command);
                return executor(parts[1]);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Error, e, "Failed during executing command: {0}", command);
                return
                    IntegratorRequestResult.CreateFailedResult(
                        string.Format("Failed During Command [{0}] Execution ({1})", command, e.GetType().ToString()));
            }
        }
    }
}
