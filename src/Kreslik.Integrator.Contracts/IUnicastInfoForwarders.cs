﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public interface ITakerUnicastInfoForwarder
    {
        void OnIntegratorDealInternal(IntegratorDealInternal integratorDealInternal);
        void OnIntegratorUnconfirmedDealInternal(IntegratorUnconfirmedDealInternal integratorUnconfirmedDealInternal);
        void OnIntegratorClientOrderUpdate(ClientOrderUpdateInfo clientOrderUpdateInfo, Symbol symbol);
        void OnCounterpartyRejectedOrder(CounterpartyRejectionInfo counterpartyRejectionInfo);
        void OnCounterpartyIgnoredOrder(CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo);
    }

    public interface IStreamingUnicastInfoForwarder
    {
        void OnRejectableStreamingDeal(RejectableMMDeal rejectableMmDeal);
    }

    public interface IUnicastInfoForwardingHub
    {
        void RegisterForwardingRequest(IIntegratorMulticastRequestInfo subscription, Action<IntegratorMulticastInfo> handler);
        void RemoveAllSubscriptions(Action<IntegratorMulticastInfo> handler);

        void ForwardUnicastInfoIfNeeded(IntegratorTradingInfoObjectBase integratorInfoObjectBase, ForwardingRequestType requestType, TradingTargetType senderTradingTargetType, Symbol symbol);
        void ForwardUnicastInfoIfNeeded(IntegratorInfoObjectBase integratorInfoObjectBase, int multicastIndex);

        ITakerUnicastInfoForwarder CreateWrappedUnicastInfoForwarder(ITakerUnicastInfoForwarder forwarder);
    }
}
