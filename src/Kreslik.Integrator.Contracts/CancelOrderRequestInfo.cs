﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    [DataContract]
    public class CancelOrderRequestInfo : IntegratorRequestInfo
    {
        public CancelOrderRequestInfo(string clientOrderIdentity, string clientIdentity, TradingTargetType tradingTargetType)
        {
            this.ClientOrderIdentity = clientOrderIdentity;
            this.ClientIdentity = clientIdentity;
            this.TradingTargetType = tradingTargetType;
        }

        public CancelOrderRequestInfo(string clientOrderIdentity, string clientIdentity, Counterparty counterparty)
        {
            this.ClientOrderIdentity = clientOrderIdentity;
            this.ClientIdentity = clientIdentity;
            this.CounterpartyMayBeNull = counterparty;
            this.TradingTargetType = counterparty.TradingTargetType;
        }

        [DataMember]
        public TradingTargetType TradingTargetType { get; set; }

        public Counterparty CounterpartyMayBeNull { get; private set; }

        [DataMember]
        //this member is needed so that we can (de)serialize Counterparty while still using the same instance of this type
        private int? CounterpartySerialized
        {
            get { return this.CounterpartyMayBeNull == Counterparty.NULL ? (int?)null : (int)this.CounterpartyMayBeNull; }
            set { this.CounterpartyMayBeNull = value == null ? Counterparty.NULL : (Counterparty)value; }
        }

        [DataMember]
        public string ClientOrderIdentity { get; private set; }

        [DataMember]
        public string ClientIdentity { get; private set; }

        public override string ToString()
        {
            return string.Format("Request from client {0} to cancel client order {1}, ctp: {2}", ClientIdentity, ClientOrderIdentity, CounterpartyMayBeNull);
        }
    }

    [DataContract]
    public class IntegratorCancelAllOrdersForSingleClientRequestInfo : IntegratorRequestInfo
    {
        public IntegratorCancelAllOrdersForSingleClientRequestInfo(string integratorClientIdenetity)
        {
            this.IntegratorClientIdenetity = integratorClientIdenetity;
        }

        [DataMember]
        public string IntegratorClientIdenetity { get; private set; }
    }
}
