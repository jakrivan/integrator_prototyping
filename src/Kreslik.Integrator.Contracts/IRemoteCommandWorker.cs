﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Contracts
{
    [ServiceContract]
    public interface IRemoteCommandWorker
    {
        [OperationContract]
        IntegratorRequestResult TrySendCommand(string command);
    }
}
