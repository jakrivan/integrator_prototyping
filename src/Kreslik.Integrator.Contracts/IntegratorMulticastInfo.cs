﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    [DataContract]
    public class IntegratorMulticastInfo : IntegratorInfoObjectBase, IIntegratorMulticastInfo
    {
        public IntegratorMulticastInfo(IntegratorTradingInfoObjectBase forwardedInfo,
                                       ForwardingRequestType forwardingRequestType, Symbol symbol)
        {
            this.InfoObject = forwardedInfo;
            this.MulticastTypeIndex = MulticasInfoUtils.GetTradingMulticastIndex(forwardedInfo.SenderTradingTargetType, symbol,
                    forwardingRequestType);
        }

        public IntegratorMulticastInfo(int multicastTypeIndex, IntegratorInfoObjectBase integratorInfoObject)
        {
            this.MulticastTypeIndex = multicastTypeIndex;
            this.InfoObject = integratorInfoObject;
        }

        public IntegratorTradingInfoObjectBase ForwardedInfo
        {
            get { return this.InfoObject as IntegratorTradingInfoObjectBase; }
        }

        [DataMember]
        public IntegratorInfoObjectBase InfoObject { get; private set; }

        [DataMember]
        public int MulticastTypeIndex { get; private set; }

        public override string ToString()
        {
            return string.Format("IntegratorMulticastInfo info: {0}", InfoObject);
        }
    }
}
