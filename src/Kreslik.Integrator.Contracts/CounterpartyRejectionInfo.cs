﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    [DataContract]
    public class CounterpartyRejectionInfo : IntegratorClientOrderInfoObjectBase
    {
        public CounterpartyRejectionInfo(string clientOrderIdentity, string clientIdentity, string parentOrderIdentity, string integratorOrderIdentity, string integratorOrderExternalIdentity, decimal rejectedAmountBaseAbs, decimal rejectedPrice,
                                      DealDirection direction, Symbol symbol, DateTime integratorSentTimeUtc, DateTime integratorExecutionTimeUtc, DateTime counterpartyExecutionTimeUtc,
                                      Counterparty counterparty, string counterpartyRejectionReason, bool? isPrimaryRejectionFromQuoted, RejectionType rejectionType, TradingTargetType senderTradingTargetType,
                                      IntegratedTradingSystemIdentification integratedTradingSystemIdentification, string counterpartyClientId, string counterpartyTransactionIdentity)
        :base(senderTradingTargetType, clientIdentity, clientOrderIdentity, parentOrderIdentity)
        {
            this.IntegratorOrderIdentity = integratorOrderIdentity;
            this.IntegratorOrderExternalIdentity = integratorOrderExternalIdentity;
            this.RejectedAmountBaseAbs = rejectedAmountBaseAbs;
            this.RejectedPrice = rejectedPrice;
            this.Direction = direction;
            this.Symbol = symbol;
            this.IntegratorSentTimeUtc = integratorSentTimeUtc;
            this.IntegratorReceivedRejectionTimeUtc = integratorExecutionTimeUtc;
            this.CounterpartySentRejectionTimeUtc = counterpartyExecutionTimeUtc;
            this.Counterparty = counterparty;
            this.CounterpartyRejectionReason = counterpartyRejectionReason;
            this.IsPrimaryRejectionFromQuoted = isPrimaryRejectionFromQuoted;
            this.RejectionType = rejectionType;
            this.IntegratedTradingSystemIdentification = integratedTradingSystemIdentification;
            this.CounterpartyClientId = counterpartyClientId;
            this.CounterpartyTransactionIdentity = counterpartyTransactionIdentity;
        }

        [DataMember]
        public string IntegratorOrderIdentity { get; private set; }

        [DataMember]
        public string IntegratorOrderExternalIdentity { get; private set; }

        [DataMember]
        public string CounterpartyTransactionIdentity { get; private set; }

        [DataMember]
        public decimal RejectedAmountBaseAbs { get; private set; }

        public decimal RejectedAmountBasePol
        {
            get { return Direction == DealDirection.Buy ? RejectedAmountBaseAbs : -RejectedAmountBaseAbs; }
        }

        [DataMember]
        public decimal RejectedPrice { get; private set; }

        [DataMember]
        public string CounterpartyRejectionReason { get; private set; }

        [DataMember]
        public DealDirection Direction { get; private set; }

        public Symbol Symbol { get; private set; }

        [DataMember]
        //this member is needed so that we can (de)serialize Symbol while still using the same instance of this type
        private int SymbolSerialized
        {
            get { return (int)this.Symbol; }
            set { this.Symbol = (Symbol)value; }
        }

        [DataMember]
        public DateTime IntegratorSentTimeUtc { get; private set; }

        [DataMember]
        public DateTime IntegratorReceivedRejectionTimeUtc { get; private set; }

        [DataMember]
        public DateTime CounterpartySentRejectionTimeUtc { get; private set; }

        public Counterparty Counterparty { get; private set; }

        [DataMember]
        private int CounterpartySerialized
        {
            get { return (int)this.Counterparty; }
            set { this.Counterparty = (Counterparty)value; }
        }

        [DataMember]
        public bool? IsPrimaryRejectionFromQuoted { get; private set; }

        [DataMember]
        public RejectionType RejectionType { get; private set; }

        [DataMember]
        public IntegratedTradingSystemIdentification IntegratedTradingSystemIdentification { get; private set; }

        [DataMember]
        public string CounterpartyClientId { get; private set; }

        public override string ToString()
        {
            return
                string.Format(
                    "Counterparty rejection info for order [{0}] (sent at {1:HH:mm:ss.fffffff UTC}, received at {2:HH:mm:ss.fffffff UTC}, delivery delay: {3}. Rejected as a part of external order {10}) rejected attempt to {4} {5} (BaseAbs) of {6} at {7} from {8}. Reasoning: [{9}]. IntegratedSystem: {11}, CptClientId: {12}, CptTransId: {13}",
                    ClientOrderIdentity, CounterpartySentRejectionTimeUtc, IntegratorReceivedRejectionTimeUtc,
                    (IntegratorReceivedRejectionTimeUtc - CounterpartySentRejectionTimeUtc), Direction,
                    RejectedAmountBaseAbs, Symbol, RejectedPrice, Counterparty, CounterpartyRejectionReason,
                    IntegratorOrderIdentity,
                    this.IntegratedTradingSystemIdentification == null ? "<NOT SPECIFIED>" : this.IntegratedTradingSystemIdentification.ToString(), this.CounterpartyClientId, this.CounterpartyTransactionIdentity);
        }
    }
}
