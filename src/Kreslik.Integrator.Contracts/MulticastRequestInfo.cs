﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Contracts
{
    public enum ForwardingRequestType
    {
        Deals,
        UnconfirmedDeals,
        OrderUpdates,
        CounterpartyRejections,
        CounterpartyIgnores,
        RejectableStreamingDeal
    }

    public interface IIntegratorMulticastInfo
    {
        int MulticastTypeIndex { get; }
    }

    public interface IIntegratorMulticastRequestInfo : IIntegratorMulticastInfo
    {
        bool Subscribe { get; }
    }

    public static class MulticasInfoUtils
    {
        private static int _nonTradingMulticastsBaseIdx = TradingTargetType.ValuesCount.OwningEnumValuesCount() * Symbol.ValuesCount *
                                     ForwardingRequestType.Deals.OwningEnumValuesCount() + 1;
        private static int _forwardingRequestTypes = ForwardingRequestType.Deals.OwningEnumValuesCount();

        private static int _mutlicastEventsCount = _nonTradingMulticastsBaseIdx +
                                                   MulticastRequestType.FatalEmails.OwningEnumValuesCount();

        public static int GetTradingMulticastIndex(TradingTargetType tradingTargetType, Symbol symbol,
            ForwardingRequestType forwardingRequestType)
        {
            return (int)tradingTargetType * Symbol.ValuesCount * _forwardingRequestTypes +
                       (int)symbol * _forwardingRequestTypes + (int)forwardingRequestType;
        }

        public static int GetNontradingMulticastIndex(MulticastRequestType multicastRequestType)
        {
            return _nonTradingMulticastsBaseIdx + (int)multicastRequestType;
        }

        public static int MutlicastEventsCount { get { return _mutlicastEventsCount; } }

        //public static IntegratorRequestInfo CreateMulticastSubscriptionRequestInfo(int multicastIdx, bool subscribe)
        //{
        //    if(multicastIdx >= _nonTradingMulticastsBaseIdx)
        //        return new NontradingMulticastRequestInfo((MulticastRequestType) (multicastIdx - _nonTradingMulticastsBaseIdx), subscribe);

        //    int tradingTargetType;
        //    int symbol;
        //    int fwRequestType;

        //    fwRequestType = multicastIdx % _forwardingRequestTypes;
        //    multicastIdx /= _forwardingRequestTypes;
        //    symbol = multicastIdx % Symbol.ValuesCount;
        //    multicastIdx /= Symbol.ValuesCount;
        //    tradingTargetType = multicastIdx;

        //    return new MulticastRequestInfo((TradingTargetType) tradingTargetType, (Symbol) symbol, (ForwardingRequestType) fwRequestType, subscribe);
        //}
    }


    public enum MulticastRequestType
    {
        FatalEmails = 0
        //Forwarding has its own type
    }

    [DataContract]
    public class MulticastRequestInfo : IntegratorRequestInfo, IIntegratorMulticastRequestInfo
    {
        public MulticastRequestInfo(TradingTargetType tradingTargetType, Symbol symbol,
                                               ForwardingRequestType forwardingRequestType, bool subscribe)
        {
            this.MulticastTypeIndex = MulticasInfoUtils.GetTradingMulticastIndex(tradingTargetType, symbol, forwardingRequestType);
            this.Subscribe = subscribe;
        }

        public MulticastRequestInfo(MulticastRequestType multicastRequestType, bool subscribe)
        {
            this.MulticastTypeIndex = MulticasInfoUtils.GetNontradingMulticastIndex(multicastRequestType);
            this.Subscribe = subscribe;
        }

        public MulticastRequestInfo(int multicastConvertedIdx, bool subscribe)
        {
            this.MulticastTypeIndex = multicastConvertedIdx;
            this.Subscribe = subscribe;
        }

        [DataMember]
        public bool Subscribe { get; private set; }

        [DataMember]
        public int MulticastTypeIndex { get; private set; }

        public static MulticastRequestInfo CreateInvertedRequestInfo(MulticastRequestInfo requestInfo)
        {
            return new MulticastRequestInfo(requestInfo.MulticastTypeIndex, !requestInfo.Subscribe);
        }

        public override string ToString()
        {
            return string.Format("MulticastRequestInfo: MulticastTypeIndex: {0}, Subscribe: {1}", MulticastTypeIndex, Subscribe);
        }
    }
}
