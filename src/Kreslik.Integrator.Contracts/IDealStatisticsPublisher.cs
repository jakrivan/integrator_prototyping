﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.Contracts
{
    public interface IDealStatisticsPublisher
    {
        event Action<OrderChangeInfo, IIntegratorOrderExternal> NewExternalDealExecuted;
        event Action<RejectableMMDeal> NewRejectableDealExecuted;
        event Action<RejectableMMDeal> RejectableExecutedDealRejected;
    }
}
