﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class ErrorNotificationItchMessage : IncomingItchMessage
    {
        public ErrorNotificationItchMessage(string rawMessage, string error)
            : base(rawMessage, IncomingItchMessageType.LoginRejected)
        {
            this.Error = error;
        }

        public override bool IsAdminMessage
        {
            get { return true; }
        }

        public string Error { get; private set; }

        public override string ToString()
        {
            return string.Format("Error notification, error: {0}, {1}", Error, base.ToString());
        }

        public static bool TryParseErrorMessage(string messageString, ILogger logger, out IncomingItchMessage itchMessage)
        {
            itchMessage = null;
            int position = 1;

            if (messageString.Length != 102)
            {
                logger.Log(LogLevel.Error, "Error Notification message size [{0}] is unexpected (Expected 102): {1}", messageString.Length, messageString);
                return false;
            }

            string error = messageString.Substring(position, 100);
            position += 100;

            if (messageString[position] != ItchConstants.LF_BYTE)
            {
                logger.Log(LogLevel.Error, "Unexpected termination char [{0}] in Error Notification message: {1}", messageString[position], messageString);
                return false;
            }

            itchMessage = new ErrorNotificationItchMessage(messageString, error);
            return true;
        }
    }
}
