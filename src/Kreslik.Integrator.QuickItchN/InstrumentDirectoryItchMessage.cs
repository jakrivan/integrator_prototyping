﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class InstrumentDirectoryItchMessage : IncomingItchMessage
    {
        public InstrumentDirectoryItchMessage(string rawMessage, List<Symbol> supportedCurrencyPairs)
            : base(rawMessage, IncomingItchMessageType.InstrumentDirectory)
        {
            this.SupportedCurrencyPairs = supportedCurrencyPairs.AsReadOnly();
        }

        public override bool IsAdminMessage
        {
            get { return false; }
        }

        public IReadOnlyList<Symbol> SupportedCurrencyPairs { get; private set; }

        public static bool TryParseInstrumentDirectoryMessage(string messageString, ILogger logger, out IncomingItchMessage itchMessage)
        {
            itchMessage = null;
            int position = 1;

            if (messageString.Length < 6)
            {
                logger.Log(LogLevel.Error, "Instrument Dirrectory message size [{0}] is too low (Expected at least 6): {1}", messageString.Length, messageString);
                return false;
            }

            string numberOfPairsString = messageString.Substring(position, 4);
            position += 4;

            int numberOfPairs;
            if (!int.TryParse(numberOfPairsString, out numberOfPairs))
            {
                logger.Log(LogLevel.Error, "Unparsable Integer number of pairs [{0}] in Instrument Dirrectory message:", numberOfPairsString, messageString);
                return false;
            }

            if (messageString.Length != 6 + numberOfPairs * 7)
            {
                logger.Log(LogLevel.Error,
                                 "Instrument Dirrectory message size [{0}] is unexpected (Expected {2}): {1}",
                                 messageString.Length, messageString, 6 + numberOfPairs * 7);
                return false;
            }

            List<Symbol> currencyPairs = new List<Symbol>();
            for (int currencyIdx = 0; currencyIdx < numberOfPairs; currencyIdx++)
            {
                string currencyPair = messageString.Substring(position, 7);
                position += 7;
                Symbol symbol;
                if (Symbol.TryParse(currencyPair, out symbol))
                {
                    currencyPairs.Add(symbol);
                }
                else
                {
                    logger.Log(LogLevel.Error,
                                     "Receiving unknown currency pair: {0} in InstrumentDirectory message. Ignoring it.",
                                     currencyPair);
                }
            }

            if (messageString[position] != ItchConstants.LF_BYTE)
            {
                logger.Log(LogLevel.Error, "Unexpected termination char [{0}] in Instrument Dirrectory message: {1}", messageString[position], messageString);
                return false;
            }

            itchMessage = new InstrumentDirectoryItchMessage(messageString, currencyPairs);
            return true;
        }
    }
}
