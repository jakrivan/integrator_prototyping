﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class CancelOrderItchMessage : SequencedDataItchMessage
    {
        public CancelOrderItchMessage(string rawMessage, Symbol symbol, string counterpartyOrderId)
            : base(rawMessage, ItchSequencedDataMessageSubtype.CancelOrder)
        {
            this.Symbol = symbol;
            this.CounterpartyOrderId = counterpartyOrderId;
        }

        public Symbol Symbol { get; private set; }
        public string CounterpartyOrderId { get; private set; }

        public override string ToString()
        {
            return string.Format("CancelOrder: {0} on {1}. {2}", CounterpartyOrderId, Symbol, base.ToString());
        }

        public static bool TryParseCancelOrderMessage(string messageString, ref int position, ILogger logger, out SequencedDataItchMessage sequencedItchMessage)
        {
            sequencedItchMessage = null;

            if (messageString.Length - position != 23)
            {
                logger.Log(LogLevel.Error, "CancelOrder message size [{0}] is unexpected (Expected 23 in data packet part, was {1}): {2}", messageString.Length, messageString.Length - position, messageString);
                return false;
            }

            Symbol symbol;
            if (!Symbol.TryParse(messageString.Substring(position, 7), out symbol))
            {
                logger.Log(LogLevel.Error, "Experienced unknown currency pair: {0} in CancelOrder message: {1}",
                                 messageString.Substring(position, 7), messageString);
                return false;
            }
            position += 7;

            string counterpartyOrderId = messageString.Substring(position, 15);
            position += 15;

            sequencedItchMessage = new CancelOrderItchMessage(messageString, symbol, counterpartyOrderId);
            return true;
        }
    }
}
