﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.QuickItchN
{
    public interface IItchMessagesDuplexStream
    {
        void Initialize();
        IEnumerable<IncomingItchMessage> IncomingMessages { get; }
        void SendMessage(OutgoingItchMessage message);
        void Close();
    }
}
