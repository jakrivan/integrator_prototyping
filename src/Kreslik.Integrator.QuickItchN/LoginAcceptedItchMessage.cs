﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class LoginAcceptedItchMessage : IncomingItchMessage
    {
        public LoginAcceptedItchMessage(string rawMessage, int sequenceNumber)
            : base(rawMessage, IncomingItchMessageType.LoginAccepted)
        {
            this.SequenceNumber = sequenceNumber;
        }

        public int SequenceNumber { get; private set; }

        public override bool IsAdminMessage
        {
            get { return true; }
        }

        public static bool TryParseLoginAcceptedMessage(string messageString, ILogger logger, out IncomingItchMessage itchMessage)
        {
            itchMessage = null;
            int position = 1;

            if (messageString.Length != 12)
            {
                logger.Log(LogLevel.Error, "Logon message size [{0}] is unexpected (Expected 12): {1}", messageString.Length, messageString);
                return false;
            }

            string sequenceNumberString = messageString.Substring(position, 10);
            position += 10;

            int sequenceNumber;
            if (!int.TryParse(sequenceNumberString, out sequenceNumber))
            {
                logger.Log(LogLevel.Error, "Unparsable Integer sequence number [{0}] in logon message:", sequenceNumberString, messageString);
                return false;
            }

            if (messageString[position] != ItchConstants.LF_BYTE)
            {
                logger.Log(LogLevel.Error, "Unexpected termination char [{0}] in logon message: {1}", messageString[position], messageString);
                return false;
            }

            itchMessage = new LoginAcceptedItchMessage(messageString, sequenceNumber);
            return true;
        }
    }
}
