﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class HeartBeatItchMessage : IncomingItchMessage
    {
        public HeartBeatItchMessage(string rawMessage)
            : base(rawMessage, IncomingItchMessageType.HeartBeat)
        { }

        public override bool IsAdminMessage
        {
            get { return true; }
        }

        public static bool TryParseHeartBeatMessage(string messageString, ILogger logger, out IncomingItchMessage itchMessage)
        {
            itchMessage = null;
            int position = 1;

            if (messageString[position] != ItchConstants.LF_BYTE)
            {
                logger.Log(LogLevel.Error, "Unexpected termination char [{0}] in HeartBeat message: {1}", messageString[position], messageString);
                return false;
            }

            itchMessage = new HeartBeatItchMessage(messageString);
            return true;
        }
    }
}
