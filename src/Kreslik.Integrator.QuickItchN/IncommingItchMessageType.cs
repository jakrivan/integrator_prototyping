﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.QuickItchN
{
    public enum IncomingItchMessageType : byte
    {
        //'A'
        LoginAccepted = 0x41,
        //'J'
        LoginRejected = 0x4A,
        //'S'
        SequencedData = 0x53,
        //'H'
        HeartBeat = 0x48,
        //'E'
        ErrorNotification = 0x45,
        //'R'
        InstrumentDirectory = 0x52,
        Unknown = 0
    }
}
