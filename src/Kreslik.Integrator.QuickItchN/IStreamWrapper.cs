﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.QuickItchN
{
    public interface IStreamWrapper
    {
        //TODO: callers need to handle IOException and SocketException
        string ReadNextToken(byte tokenSeparator);
    }
}
