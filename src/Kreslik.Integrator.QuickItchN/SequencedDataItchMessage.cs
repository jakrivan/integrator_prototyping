﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class SequencedDataItchMessage : IncomingItchMessage
    {
        public SequencedDataItchMessage(string rawMessage, ItchSequencedDataMessageSubtype messageSubtype)
            : base(rawMessage, IncomingItchMessageType.SequencedData)
        {
            this.SequencedDataMessageSubtype = messageSubtype;
        }

        public override bool IsAdminMessage
        {
            get { return false; }
        }

        public ItchSequencedDataMessageSubtype SequencedDataMessageSubtype { get; private set; }

        public DateTime CounterpartySentTime { get; set; }

        public override string ToString()
        {
            return string.Format("SequencedData, Subtype: {0}, CounterpartSentTime: {1:HH:mm:ss.fff}; {2}",
                                 SequencedDataMessageSubtype, CounterpartySentTime, base.ToString());
        }

        public static bool TryParseSequncedDataMessage(string messageString, ILogger logger, out IncomingItchMessage itchMessage)
        {
            itchMessage = null;
            int position = 1;

            if (messageString.Length < 2)
            {
                logger.Log(LogLevel.Error, "SequncedData message size [{0}] is too low (Expected at least 2): {1}", messageString.Length, messageString);
                return false;
            }

            if (messageString.Length == 2 && messageString[position] == ItchConstants.LF_BYTE)
            {
                itchMessage = new EndOfSessionItchMessage(messageString);
                return true;
            }

            if (messageString.Length < 12)
            {
                logger.Log(LogLevel.Error, "SequncedData message size [{0}] is too low (Expected at least 12 for non-end-of-session data): {1}", messageString.Length, messageString);
                return false;
            }

            string timeString = messageString.Substring(position, 9);
            position += 9;

            DateTime counterpartySentTime;
            if (
                !DateTime.TryParseExact(timeString, "HHmmssfff", CultureInfo.InvariantCulture, DateTimeStyles.None,
                                        out counterpartySentTime))
            {
                logger.Log(LogLevel.Error,
                           "Experienced unparsable Transaction time string: {0} in SequencedData message: {1}",
                           timeString, messageString);
                return false;
            }
            counterpartySentTime = TradingHoursHelper.Instance.ConvertFromEtToUtc(counterpartySentTime);

            //Need to handle discrepancies around date roll (we migh assign 1 day more or less to the received time, need to check for difference from our time)
            if (counterpartySentTime - DateTime.UtcNow > TimeSpan.FromHours(23))
            {
                DateTime etNow = TradingHoursHelper.Instance.EtNow;
                //TryParseExact uses the local Date if string doesn't contain datepart (see msdn), so when UTC is already in the next day
                // ahead from ET then our parsing and converting algo will create date which is one day ahead (23 hours is safety due to DST)
                if (etNow.Date != DateTime.UtcNow.Date.Subtract(TimeSpan.FromDays(1)))
                {
                    logger.Log(LogLevel.Error,
                               "Counterparty sent time {0} is over 23 hours in future, it's probably due to date rollover, adjusting it to previous day",
                               counterpartySentTime);
                }
                counterpartySentTime = counterpartySentTime.Subtract(TimeSpan.FromDays(1));
            }
            if (DateTime.UtcNow - counterpartySentTime > TimeSpan.FromHours(23))
            {
                logger.Log(LogLevel.Error, "Counterparty sent time {0} is over 23 hours in past, it's probably due to date rollover, adjusting it to next day", counterpartySentTime);
                counterpartySentTime = counterpartySentTime.Add(TimeSpan.FromDays(1));
            }
            

            SequencedDataItchMessage sequncedDataMessage = null;
            bool parsed = false;

            byte msgSubtypeByte = (byte)messageString[position];
            position++;
            ItchSequencedDataMessageSubtype messageSubtype = IncomingItchMessage.GetMessageSubtype(msgSubtypeByte);

            switch (messageSubtype)
            {

                case ItchSequencedDataMessageSubtype.EndOfSession:
                    break;
                case ItchSequencedDataMessageSubtype.NewOrder:
                    parsed = NewOrderItchMessage.TryParseNewOrderMessage(messageString, ref position, logger, out sequncedDataMessage);
                    break;
                case ItchSequencedDataMessageSubtype.ModifyOrder:
                    parsed = ModifyOrderItchMessage.TryParseModifyOrderMessage(messageString, ref position, logger, out sequncedDataMessage);
                    break;
                case ItchSequencedDataMessageSubtype.CancelOrder:
                    parsed = CancelOrderItchMessage.TryParseCancelOrderMessage(messageString, ref position, logger, out sequncedDataMessage);
                    break;
                case ItchSequencedDataMessageSubtype.MarketSnapshot:
                    parsed = MarketSnapshotItchMessage.TryParseMarketSnapshotMessage(messageString, ref position, logger, out sequncedDataMessage);
                    break;
                case ItchSequencedDataMessageSubtype.Ticker:
                    parsed = TickerItchMessage.TryParseTickerMessage(messageString, ref position, logger, out sequncedDataMessage);
                    break;
                case ItchSequencedDataMessageSubtype.Unknown:
                default:
                    logger.Log(LogLevel.Fatal, "Unexpected subtype of message: {0}, message: {1}", messageSubtype, messageString);
                    return false;
            }


            if (parsed && sequncedDataMessage != null)
            {
                sequncedDataMessage.CounterpartySentTime = counterpartySentTime;

                if (messageString.Length >= ItchConstants.MAX_MESSAGE_SIZE)
                {
                    logger.Log(LogLevel.Error, "Experienced message of large size - at least {0} bytes, so skiping rest of it.", messageString.Length);
                }
                else if (messageString[position] != ItchConstants.LF_BYTE)
                {
                    logger.Log(LogLevel.Error, "Unexpected termination char [{0}] in SequncedData message: {1}", messageString[position], messageString);
                    return false;
                }
            }

            itchMessage = sequncedDataMessage;
            return true;
        }
    }

}
