﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class ItchClient: IItchClient
    {
        private IItchMessagesDuplexStream _itchMessagesDuplexStream;
        private ILogger _logger;
        private bool _connected = false;
        //private bool _exitRequested = false;
        private bool _disconnected = false;
        private SafeTimer _hbTimer;
        private TimeSpan _hbInterval;

        public ItchClient(string hostName, int port, ILogger logger, TimeSpan heartBeatInterval)
        {
            this._itchMessagesDuplexStream = new ItchMessagesDuplexStream(hostName, port, logger);
            this._logger = logger;
            this._hbTimer = new SafeTimer(HeartBeatRoutine)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo100ms,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
            this._hbInterval = heartBeatInterval;
        }

        public void Connect(string login, string password)
        {
            try
            {
                this._itchMessagesDuplexStream.Initialize();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Error, "Received exception during connecting to endpoint", e);
                this.Close(false);
                return;
            }
            

            this.SendMessage(new LoginRequestItchMessage(login, password, true));
            if (!_disconnected)
            {
                ThreadPool.QueueUserWorkItem(o => ReadMessages());
                TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(30), () =>
                    {
                        if (!(_connected || _disconnected))
                        {
                            this._logger.Log(LogLevel.Info, "Timed out waiting for logon response");
                            this.Close(true);
                        }
                    });
            }
        }

        public event Action<IncomingItchMessage> NewIncomingItchMessage;
        public event Action Disconnected;
        public event Action Connected;

        public void SendMessage(OutgoingItchMessage message)
        {
            if (_disconnected)
            {
                this._logger.Log(LogLevel.Warn, "Attempt to call SendMessage method on disconnected client: {0}", message);
                return;
            }

            try
            {
                this._itchMessagesDuplexStream.SendMessage(message);
                if(_hbTimer != null)
                    this._hbTimer.Change(_hbInterval, Timeout.InfiniteTimeSpan);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Error, e, "Experienced exception during sending message: {0}", message);
                this.Close(false);
            }
        }

        public void Close()
        {
            this._logger.Log(LogLevel.Info, "Received external request for logout, so logging out");
            this.Close(true);
        }

        private void Close(bool sendLogoutMessage)
        {
            if (_disconnected)
            {
                this._logger.Log(LogLevel.Warn, "Attempt to call Close method on disconnected client");
                return;
            }

            //Volatile.Write(ref _exitRequested, true);
            if (_hbTimer != null)
            {
                _hbTimer.Dispose();
                _hbTimer = null;
            }

            if (sendLogoutMessage)
            {
                this.SendMessage(new LogoutRequestItchMessage());
                TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(5), () =>
                    {
                        if (!_disconnected)
                        {
                            this._logger.Log(LogLevel.Info, "Timed out waiting for logout request");
                            this.Close(false);
                        }
                    });
            }
            else
            {
                Volatile.Write(ref _disconnected, true);
                this._itchMessagesDuplexStream.Close();
                if (Disconnected != null)
                {
                    Disconnected.SafeInvoke(this._logger);
                }
            }
        }

        private void HeartBeatRoutine()
        {
            this.SendMessage(new ClientHeartbeatItchMessage());
        }

        private void HandleAdminMessages(IncomingItchMessage adminMessage)
        {
            switch (adminMessage.ItchMessageType)
            {
                case IncomingItchMessageType.LoginAccepted:
                    if (!_connected)
                    {
                        _connected = true;
                        if (Connected != null)
                        {
                            //exceptions in this event are handeled by invoker of HandleAdminMessages
                            Connected();
                        }
                    }
                    break;
                case IncomingItchMessageType.LoginRejected:
                    if (!_connected)
                    {
                        _connected = true;
                        this._logger.Log(LogLevel.Error, "Receiving LogonRejection: {0}", adminMessage);
                        this.Close(true);
                    }
                    break;
                case IncomingItchMessageType.SequencedData:
                    if ((adminMessage as SequencedDataItchMessage).SequencedDataMessageSubtype ==
                        ItchSequencedDataMessageSubtype.EndOfSession)
                    {
                        this._logger.Log(LogLevel.Info, "Receiving EndOfSession so closing.");
                        this.Close(false);
                    }
                    else
                    {
                        this._logger.Log(LogLevel.Warn, "Unexpected message in Admin messages handler: {0}", adminMessage);
                    }
                    break;
                case IncomingItchMessageType.HeartBeat:
                    break;
                case IncomingItchMessageType.ErrorNotification:
                    this._logger.Log(LogLevel.Error, "Receiving error notification: {0}", adminMessage);
                    break;
                default:
                    this._logger.Log(LogLevel.Warn, "Unexpected message in Admin messages handler: {0}", adminMessage);
                    break;
            }
        }

        private void ReadMessages()
        {
            try
            {
                foreach (IncomingItchMessage incomingItchMessage in this._itchMessagesDuplexStream.IncomingMessages)
                {
                    if (!this._logger.IsLowDiskSpaceConstraintLoggingOn || incomingItchMessage.IsAdminMessage)
                    {
                        //log just raw message - not to waste with resources
                        this._logger.Log(LogLevel.Trace, incomingItchMessage.RawMessage.Replace((char)ItchConstants.LF_BYTE, '_'));
                        //this._logger.Log(LogLevel.Debug, "Incoming ITCH message: {0}", incomingItchMessage);
                    }

                    if (incomingItchMessage.IsAdminMessage)
                    {
                        HandleAdminMessages(incomingItchMessage);
                    }

                    if (NewIncomingItchMessage != null && !_disconnected)
                    {
                        try
                        {
                            NewIncomingItchMessage(incomingItchMessage);
                        }
                        catch (Exception e)
                        {
                            this._logger.LogException(LogLevel.Fatal, e, "Exception during parsing message: {0}", incomingItchMessage);
                        }
                       
                    }
                }
            }
            //ObjectDisposedException
            //SocketException
            //IOException
            catch (Exception e)
            {
                if (!(e is ObjectDisposedException || e is SocketException || e is System.IO.IOException))
                {
                    this._logger.Log(LogLevel.Fatal, "Experienced unexpected type of exception during reading ITCH messages: {0}", e.GetType());
                }

                this._logger.LogException(LogLevel.Error, "Receiving exception during reading messages from ItchStream. Disconnecting...", e);
                this.Close(true);
            }
        }
    }
    
}
