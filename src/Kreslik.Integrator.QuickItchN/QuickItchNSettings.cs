﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.QuickItchN.Properties;

namespace Kreslik.Integrator.QuickItchN
{
    public class QuickItchNSettings
    {
        public SessionSettings HtaSessionSettings { get; set; }
        public SessionSettings HtfSessionSettings { get; set; }
        public SessionSettings Ht3SessionSettings { get; set; }
        public SessionSettings H4tSessionSettings { get; set; }
        public SessionSettings H4mSessionSettings { get; set; }

        public class SessionSettings
        {
            public string Host { get; set; }
            public int Port { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            [System.Xml.Serialization.XmlIgnore]
            public System.TimeSpan HeartBeatInterval { get; set; }

            [System.Xml.Serialization.XmlElement("HeartBeatInterval_Seconds")]
            public int HeartBeatIntervalXml
            {
                get { return (int)HeartBeatInterval.TotalSeconds; }
                set { HeartBeatInterval = System.TimeSpan.FromSeconds(value); }
            }
        }

        [System.Xml.Serialization.XmlIgnore]
        public static QuickItchNSettings QuickItchNBehavior
        {
            get
            {
                if (_quickItchNBehavior == null)
                {
                    _quickItchNBehavior = SettingsInitializator.Instance.ReadSettings<QuickItchNSettings>(
                        @"Kreslik.Integrator.QuickItchN.dll",
                        Resources.Kreslik_Integrator_QuickItchN_dll
                        );
                }

                return _quickItchNBehavior;
            }
        }

        private static QuickItchNSettings _quickItchNBehavior;
    }
}
