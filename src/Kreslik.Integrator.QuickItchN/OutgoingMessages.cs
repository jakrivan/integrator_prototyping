﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public enum OutgoingItchMessageType : byte
    {
        //'L'
        LoginRequest = 0x4C,
        //'O'
        LogoutRequest = 0x4F,
        //'R'
        ClientHeartBeat = 0x52,
        //'M'
        MarketSnapshotRequest = 0x4D,
        //'T'
        TickerSubscribe = 0x54,
        //'U'
        TickerUnsubscribe = 0x55,
        //'A'
        MarketDataSubscribe = 0x41,
        //'B'
        MarketDataUnsubscribe = 0x42,
        //'I'
        InstrumentDirectoryRequest = 0x49,
        Unknown = 0
    }

    public class OutgoingItchMessage
    {
        public static OutgoingItchMessageType GetMessageType(byte messageTypeByte)
        {
            OutgoingItchMessageType mt = (OutgoingItchMessageType)messageTypeByte;

            switch (mt)
            {
                case OutgoingItchMessageType.LoginRequest:
                case OutgoingItchMessageType.LogoutRequest:
                case OutgoingItchMessageType.ClientHeartBeat:
                case OutgoingItchMessageType.MarketSnapshotRequest:
                case OutgoingItchMessageType.TickerSubscribe:
                case OutgoingItchMessageType.TickerUnsubscribe:
                case OutgoingItchMessageType.MarketDataSubscribe:
                case OutgoingItchMessageType.MarketDataUnsubscribe:
                case OutgoingItchMessageType.InstrumentDirectoryRequest:
                    break;
                case OutgoingItchMessageType.Unknown:
                default:
                    mt = OutgoingItchMessageType.Unknown;
                    break;
            }

            return mt;
        }

        public OutgoingItchMessage(string rawMessage, string parsedMessage, OutgoingItchMessageType messageType)
        {
            this.ItchMessageType = messageType;
            this.RawMessage = rawMessage;
            this.ParsedMessage = parsedMessage;
        }

        public OutgoingItchMessageType ItchMessageType { get; private set; }
        public string RawMessage { get; private set; }
        public string ParsedMessage { get; private set; }

        public override string ToString()
        {
            return string.Format("MessageType: {0}, ParsedMessage: {1} ### RawMessage: {2}", this.ItchMessageType,
                                 this.ParsedMessage, this.RawMessage.Replace((char)ItchConstants.LF_BYTE, '_'));
        }
    }

    public class LoginRequestItchMessage : OutgoingItchMessage
    {
        public LoginRequestItchMessage(string login, string password, bool loginWithoutAutoSubscription)
            : base(
                FormatRawLoginMessage(login, password, loginWithoutAutoSubscription),
                FormatParsedLoginMessage(login, password, loginWithoutAutoSubscription),
                OutgoingItchMessageType.LoginRequest)
        { }

        private static readonly char _subscribeChar = '\u0046';
        private static readonly char _unsubscribeChar = '\u0054';

        private static string FormatRawLoginMessage(string login, string password, bool loginWithoutAutoSubscription)
        {
            if (login.Length > 40)
            {
                throw new Exception(string.Format("Logon [{0}] too long ({1} expected max 40)", login, login.Length));
            }

            if (password.Length > 40)
            {
                throw new Exception(string.Format("Password [{0}] too long ({1} expected max 40)", password, password.Length));
            }

            return string.Format("{0}{1,-40}{2,-40}{3}{4,9}{5}", (char)OutgoingItchMessageType.LoginRequest, login,
                                 password, loginWithoutAutoSubscription ? _unsubscribeChar : _subscribeChar, 0, (char)ItchConstants.LF_BYTE);
        }

        private static string FormatParsedLoginMessage(string login, string password, bool loginWithoutAutoSubscription)
        {
            return string.Format("Login: {0}, Password: {1}, AutosubscribeToAllPrices: {2}", login, password,
                                 !loginWithoutAutoSubscription);
        }
    }

    public class LogoutRequestItchMessage : OutgoingItchMessage
    {
        public LogoutRequestItchMessage()
            : base(
                FormatRawMessage(),
                FormatParsedMessage(),
                OutgoingItchMessageType.LogoutRequest)
        { }

        private static string FormatRawMessage()
        {
            return string.Format("{0}{1}", (char)OutgoingItchMessageType.LogoutRequest, (char)ItchConstants.LF_BYTE);
        }

        private static string FormatParsedMessage()
        {
            return "Logout";
        }
    }

    public class ClientHeartbeatItchMessage : OutgoingItchMessage
    {
        public ClientHeartbeatItchMessage()
            : base(
                FormatRawMessage(),
                FormatParsedMessage(),
                OutgoingItchMessageType.ClientHeartBeat)
        { }

        private static string FormatRawMessage()
        {
            return string.Format("{0}{1}", (char)OutgoingItchMessageType.ClientHeartBeat, (char)ItchConstants.LF_BYTE);
        }

        private static string FormatParsedMessage()
        {
            return "ClientHB";
        }
    }

    public abstract class SubscribeUnsubscribeOutgoingItchMessageBase : OutgoingItchMessage
    {
        protected SubscribeUnsubscribeOutgoingItchMessageBase(string currencyPair, OutgoingItchMessageType outgoingItchMessageType)
            : base(
                FormatRawMessage(currencyPair, outgoingItchMessageType),
                FormatParsedMessage(currencyPair, outgoingItchMessageType),
                outgoingItchMessageType)
        { }

        private static string FormatRawMessage(string currencyPair, OutgoingItchMessageType outgoingItchMessageType)
        {
            return string.Format("{0}{1,-7}{2}", (char)outgoingItchMessageType, currencyPair,
                                 (char)ItchConstants.LF_BYTE);
        }

        private static string FormatParsedMessage(string currencyPair, OutgoingItchMessageType outgoingItchMessageType)
        {
            return string.Format("{0} for: {1}", outgoingItchMessageType, currencyPair);
        }
    }

    public class MarketSnapshotRequestItchMessage : SubscribeUnsubscribeOutgoingItchMessageBase
    {
        public MarketSnapshotRequestItchMessage(Symbol symbol)
            : base(symbol.ToString(), OutgoingItchMessageType.MarketSnapshotRequest)
        { }

        public MarketSnapshotRequestItchMessage()
            : base("ALL", OutgoingItchMessageType.MarketSnapshotRequest)
        { }
    }

    public class TickerSubscribeItchMessage : SubscribeUnsubscribeOutgoingItchMessageBase
    {
        public TickerSubscribeItchMessage(Symbol symbol)
            : base(symbol.ToString(), OutgoingItchMessageType.TickerSubscribe)
        { }

        public TickerSubscribeItchMessage()
            : base("ALL", OutgoingItchMessageType.TickerSubscribe)
        { }
    }

    public class TickerUnsubscribeItchMessage : SubscribeUnsubscribeOutgoingItchMessageBase
    {
        public TickerUnsubscribeItchMessage(Symbol symbol)
            : base(symbol.ToString(), OutgoingItchMessageType.TickerUnsubscribe)
        { }

        public TickerUnsubscribeItchMessage()
            : base("ALL", OutgoingItchMessageType.TickerUnsubscribe)
        { }
    }

    public class MarketDataSubscribeItchMessage : SubscribeUnsubscribeOutgoingItchMessageBase
    {
        public MarketDataSubscribeItchMessage(Symbol symbol)
            : base(symbol.ToString(), OutgoingItchMessageType.MarketDataSubscribe)
        { }

        public MarketDataSubscribeItchMessage()
            : base("ALL", OutgoingItchMessageType.MarketDataSubscribe)
        { }
    }

    public class MarketDataUnsubscribeItchMessage : SubscribeUnsubscribeOutgoingItchMessageBase
    {
        public MarketDataUnsubscribeItchMessage(Symbol symbol)
            : base(symbol.ToString(), OutgoingItchMessageType.MarketDataUnsubscribe)
        { }

        public MarketDataUnsubscribeItchMessage()
            : base("ALL", OutgoingItchMessageType.MarketDataUnsubscribe)
        { }
    }

    public class InstrumentDirectoryRequestItchMessage : OutgoingItchMessage
    {
        public InstrumentDirectoryRequestItchMessage()
            : base(
                FormatRawMessage(),
                FormatParsedMessage(),
                OutgoingItchMessageType.InstrumentDirectoryRequest)
        { }

        private static string FormatRawMessage()
        {
            return string.Format("{0}{1}", (char)OutgoingItchMessageType.InstrumentDirectoryRequest, (char)ItchConstants.LF_BYTE);
        }

        private static string FormatParsedMessage()
        {
            return "InstrumentDirectoryRequest";
        }
    }
}
