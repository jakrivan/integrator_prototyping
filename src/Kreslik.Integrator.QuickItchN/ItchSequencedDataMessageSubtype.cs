﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.QuickItchN
{
    public enum ItchSequencedDataMessageSubtype : byte
    {
        Unknown = 0,
        EndOfSession = 1,
        //'N'
        NewOrder = 0x4E,
        //'M'
        ModifyOrder = 0x4D,
        //'X'
        CancelOrder = 0x58,
        //'S'
        MarketSnapshot = 0x53,
        //'T'
        Ticker = 0x54
    }
}
