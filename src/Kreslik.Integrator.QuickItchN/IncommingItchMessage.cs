﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.QuickItchN
{
    public abstract class IncomingItchMessage
    {
        public static IncomingItchMessageType GetMessageType(byte messageTypeByte)
        {
            IncomingItchMessageType mt = (IncomingItchMessageType)messageTypeByte;

            //if (!Enum.IsDefined(typeof(ItchMessageType), mt))
            //    mt = ItchMessageType.Unknown;
            switch (mt)
            {
                case IncomingItchMessageType.LoginAccepted:
                case IncomingItchMessageType.LoginRejected:
                case IncomingItchMessageType.SequencedData:
                case IncomingItchMessageType.HeartBeat:
                case IncomingItchMessageType.ErrorNotification:
                case IncomingItchMessageType.InstrumentDirectory:
                    break;
                case IncomingItchMessageType.Unknown:
                default:
                    mt = IncomingItchMessageType.Unknown;
                    break;
            }

            return mt;
        }

        public static ItchSequencedDataMessageSubtype GetMessageSubtype(byte messageSubtypeByte)
        {
            ItchSequencedDataMessageSubtype mst = (ItchSequencedDataMessageSubtype)messageSubtypeByte;

            switch (mst)
            {
                //end of session is intentionaly ommited as it doesn't have a regular subtype byte
                //case ItchSequencedDataMessageSubtype.EndOfSession:
                case ItchSequencedDataMessageSubtype.NewOrder:
                case ItchSequencedDataMessageSubtype.ModifyOrder:
                case ItchSequencedDataMessageSubtype.CancelOrder:
                case ItchSequencedDataMessageSubtype.MarketSnapshot:
                case ItchSequencedDataMessageSubtype.Ticker:
                    break;
                case ItchSequencedDataMessageSubtype.Unknown:
                default:
                    mst = ItchSequencedDataMessageSubtype.Unknown;
                    break;
            }

            return mst;
        }


        public IncomingItchMessage(string rawMessage, IncomingItchMessageType messageType)
        {
            this.RawMessage = rawMessage;
            this.ItchMessageType = messageType;
        }

        public string RawMessage { get; protected set; }
        public DateTime IntegratorReceivedTimeUtc { get; set; }
        public IncomingItchMessageType ItchMessageType { get; protected set; }
        public abstract bool IsAdminMessage { get; }

        public override string ToString()
        {
            return string.Format("MessageType: {0}, IntegratorReceivedUtc: {1:HH:mm:ss.fffffff} ### RawMessage: {2}",
                                 this.ItchMessageType, this.IntegratorReceivedTimeUtc, this.RawMessage.Replace((char) ItchConstants.LF_BYTE, '_'));
        }
    }
}
