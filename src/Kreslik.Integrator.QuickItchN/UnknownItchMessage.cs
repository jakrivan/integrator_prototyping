﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.QuickItchN
{
    public class UnknownItchMessage : IncomingItchMessage
    {
        public UnknownItchMessage(string rawMessage)
            : base(rawMessage, IncomingItchMessageType.Unknown)
        { }

        public override bool IsAdminMessage
        {
            get { return false; }
        }
    }
}
