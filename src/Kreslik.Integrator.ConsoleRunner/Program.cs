﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.BusinessLayer;
using Kreslik.Integrator.BusinessLayer.Fakes;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.DAL.DataCollection;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.MessageBus;

namespace Kreslik.Integrator.ConsoleRunner
{
    public static class ConsoleInteropHelper
    {
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        [DllImport("kernel32")]
        private static extern bool AllocConsole();

        private const int MF_BYCOMMAND = 0x00000000;
        private const int SC_CLOSE = 0xF060;

        [DllImport("user32.dll")]
        private static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);

        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();

        public static void PreventConsoleClose()
        {
            AllocConsole();
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), SC_CLOSE, MF_BYCOMMAND);
            SetConsoleCtrlHandler(null, true);
        }
    }

    class Program
    {
        static ILogFactory logFactory = LogFactory.Instance;
        static IntegratorManager _integratorManager;

        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        private static void OnUnhandledAppDomainException(object sender,
            UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            OnUnhandledException((Exception) unhandledExceptionEventArgs.ExceptionObject,
                UnhandledExceptionType.UnhandledAppDomainException);
        }

        internal enum UnhandledExceptionType
        {
            UnhandledAppDomainException,
            UnhandledTaskException,
        }

        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        private static void OnUnhandledException(Exception unhandledException, UnhandledExceptionType unhandledExceptionType)
        {
            string errorMessage;

            switch (unhandledExceptionType)
            {
                case UnhandledExceptionType.UnhandledTaskException:
                    errorMessage = "UNHANLED TASK EXCEPTION. This will not terminate integrator, but state consistency should be verified";
                    break;
                case UnhandledExceptionType.UnhandledAppDomainException:
                default:
                    errorMessage = "UNHANDLED EXCEPTION TERMINATING INTEGRATOR!!!";
                    break;
            }

            logFactory.GetLogger(null).LogException(LogLevel.Fatal, errorMessage, unhandledException);

            if (_integratorManager != null)
            {
                _integratorManager.RiskManager.DisableTransmission("Unhandled exception");
            }

            //give logger a time to flush it to the file
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(3));
            Console.WriteLine(unhandledException);
        }

        static void Main(string[] args)
        {
            ConsoleInteropHelper.PreventConsoleClose();

            if (!File.Exists("AppIsInstalled"))
            {
                //Start elevated installation
                string executingAssembly = System.Reflection.Assembly.GetExecutingAssembly().Location;

                Process process = new Process();
                process.StartInfo.FileName = "Kreslik.Integrator.Installer.exe";
                process.StartInfo.Arguments = string.Format("\"{0}\" \"{1}\"", executingAssembly,
                    IntegratorManager.GetSystemsAssemblyName());
                process.Start();
                process.WaitForExit();

                File.Create("AppIsInstalled");

                //restart self
                ProcessStartInfo pci = new ProcessStartInfo(Process.GetCurrentProcess().MainModule.FileName);
                if (Environment.GetCommandLineArgs().Count() > 1)
                {
                    pci.Arguments = string.Join(" ", Environment.GetCommandLineArgs().Skip(1));
                }
                Process.Start(pci);

                //exit
                return;
            }

            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += OnUnhandledAppDomainException;
            TaskScheduler.UnobservedTaskException +=
                (sender, exargs) =>
                    OnUnhandledException(exargs.Exception, UnhandledExceptionType.UnhandledTaskException);

            Console.WriteLine("GC Server mode: {0}; latency mode: {1}", GCSettings.IsServerGC, GCSettings.LatencyMode);

            if (args.Length > 0)
            {
                bool unknownArguments = true;
                if (args.Length == 1)
                {
                    if (args[0].Equals("/install", StringComparison.OrdinalIgnoreCase))
                    {
                        Install(true);
                        unknownArguments = false;
                    }
                    else if (args[0].Equals("/uninstall", StringComparison.OrdinalIgnoreCase))
                    {
                        Install(false);
                        unknownArguments = false;
                    }
                    else if (args[0].Equals("/run", StringComparison.OrdinalIgnoreCase))
                    {
                        Run();
                        unknownArguments = false;
                    }
                    else if (args[0].Equals("/runfaked", StringComparison.OrdinalIgnoreCase))
                    {
                        RunFaked();
                        unknownArguments = false;
                    }
                    else if (args[0].Equals("/rundc", StringComparison.OrdinalIgnoreCase))
                    {
                        RunDCProcess();
                        unknownArguments = false;
                    }
                }
                else if (args.Length == 2 && args[0].Equals("/restart", StringComparison.OrdinalIgnoreCase))
                {
                    SessionAction(args[1], SessionActionType.RestartSession, ChooseRemoteCommandWorker(true));
                    unknownArguments = false;
                }

                if (unknownArguments)
                {
                    Console.WriteLine("Unknown argument. Expecting none or '/install', '/uninstall', '/run', '/runfaked', '/rundc' or '/restart bankcode'.");
                    Console.ReadKey();
                }
            }
            else
            {
                ConsoleKeyInfo key = new ConsoleKeyInfo();
                bool notChosen = true;

                while (notChosen)
                {
                    Console.WriteLine("Choose desired action by keypress: (R)un Run(F)aked (I)nstall (U)ninstall Run(D)C Run(H)otspotData (E)xit Restart(S)ession S(t)opSession St(a)rtSession Start(C)lient");
                    key = Console.ReadKey(true);

                    notChosen = false;
                    switch (key.Key)
                    {
                        case ConsoleKey.R:
                            Run();
                            break;
                        case ConsoleKey.D:
                            RunDCProcess();
                            break;
                        case ConsoleKey.F:
                            RunFaked();
                            break;
                        case ConsoleKey.H:
                            RunHotspotDataProcess();
                            break;
                        case ConsoleKey.I:
                            Install(true);
                            break;
                        case ConsoleKey.U:
                            Install(false);
                            break;
                        case ConsoleKey.S:
                        case ConsoleKey.T:
                        case ConsoleKey.A:
                            IRemoteCommandWorker remoteCommandWorker = ChooseRemoteCommandWorker(false);
                            if (remoteCommandWorker == null)
                                break;

                            Console.WriteLine("Enter the three letters counterpart abbreviation (or ALL for all, STPFLOW for STP flow session)");
                            string counterpartString = Console.ReadLine();
                            SessionActionType actionType = key.Key == ConsoleKey.S
                                                               ? SessionActionType.RestartSession
                                                               : key.Key == ConsoleKey.T
                                                                     ? SessionActionType.StopSession
                                                                     : SessionActionType.StartSession;
                            SessionAction(counterpartString, actionType, remoteCommandWorker);
                            break;
                        case ConsoleKey.C:
                            IRemoteCommandWorker remoteCommandWorker2 = ChooseRemoteCommandWorker(false);
                            if (remoteCommandWorker2 == null)
                                break;

                            Console.WriteLine("Enter the command string in format <UniqueClientFriandlyName>;<AssemblyPath>;<TypeName>;<ArbitraryConfigString>");
                            string commandString = Console.ReadLine();
                            StartClientAction(commandString, remoteCommandWorker2);
                            break;
                        case ConsoleKey.E:
                            break;

                        default:
                            notChosen = true;
                            Console.WriteLine("Unrecognized option: [{0}]", key.KeyChar);
                            break;
                    }
                }
            }

            //Console.WriteLine("Will exit after keypress");
            //Console.ReadKey();
        }

        private static IRemoteCommandWorker ChooseRemoteCommandWorker(bool unattended)
        {
            IRemoteCommandWorker remoteCommandWorker;

            if (unattended)
            {
                ILogger diagnosticClientLogger = (LogFactory.Instance).GetLogger("DiagClientLogger");
                IntegratorDiagnosticsClient idc = new IntegratorDiagnosticsClient(diagnosticClientLogger,
                                                                                  new IntegratorInstanceProcessUtils(
                                                                                      IntegratorProcessType
                                                                                          .BusinessLogic));
                idc.Connect(false);
                remoteCommandWorker = idc;
            }
            else
            {
                Console.WriteLine(
                    "Send command to (B)ackend: {0} or dirrectly to running (S)ervice: {1} (Service endpoint can be changed in MessagingBus.dll.xml)",
                    ExtractBackendFromConnectionString(DALBehavior.Sections.IntegratorConnection.ConnectionString),
                    SettingsInitializator.Instance.InstanceName);
                Console.WriteLine("Enter your choice: (B)ackend or (S)ervice...");

                ConsoleKeyInfo key = Console.ReadKey(true);
                switch (key.Key)
                {
                    case ConsoleKey.B:
                        remoteCommandWorker = CommandDistributor.GetRemoteCommandWorker(logFactory.GetLogger(null));
                        Console.Write("Backend commands are currently not delivering commands to specific process - therefore disabling this option");
                        remoteCommandWorker = null;
                        break;
                    case ConsoleKey.S:
                        ILogger diagnosticClientLogger = (LogFactory.Instance).GetLogger("DiagClientLogger");
                        IntegratorDiagnosticsClient idc = new IntegratorDiagnosticsClient(diagnosticClientLogger,
                                                                                          new IntegratorInstanceProcessUtils
                                                                                              (IntegratorProcessType
                                                                                                   .BusinessLogic));
                        idc.Connect(false);
                        remoteCommandWorker = idc;
                        break;
                    default:
                        Console.WriteLine("Unrecognized option: [{0}]", key.KeyChar);
                        remoteCommandWorker = null;
                        break;
                }
            }

            return remoteCommandWorker;
        }

        private static string ExtractBackendFromConnectionString(string connectionString)
        {
            SqlConnectionStringBuilder connStr = new SqlConnectionStringBuilder(connectionString);
            return string.Format("{0} (at {1})", connStr.InitialCatalog, connStr.DataSource);
        }

        private static void Install(bool install)
        {
            Kreslik.Integrator.Installer.Installer.Install(install,
                new string[]
                {System.Reflection.Assembly.GetExecutingAssembly().Location, IntegratorManager.GetSystemsAssemblyName()});
        }

        private static ConnectionChainBuildVersioningInfo ConnectionChainRootBuildVersioningInfo
        {
            get
            {
                return new ConnectionChainBuildVersioningInfo(null, null,
                                                              new BuildVersioningInfo(
                                                                  BuildConstants.CURRENT_BUILD_VERSION,
                                                                  BuildConstants
                                                                      .MINIMUM_VERSION_NUMBER_OF_COMPATIBLE_BUILD,
                                                                  BuildConstants.CURRENT_VERSION_CREATED_UTC));
            }
        }

        static void RunFaked()
        {
            Console.WriteLine("Starting RUN Faked action. Details will be in log files. (Press 'H' to run faked Hotspot data)");
            Console.WriteLine("Action will be started by key press and will be closed by two key press. Please press a key...");

            IntegratorProcessType integratorProcessType = IntegratorProcessType.BusinessLogic;
            if (Console.ReadKey().Key == ConsoleKey.H)
            {
                integratorProcessType = IntegratorProcessType.HotspotMarketData;
            }
            
            IntegratorProcessStateKeeper processStateKeeper = new IntegratorProcessStateKeeper(integratorProcessType);
            var result = processStateKeeper.TryRegisterIntegratorProcess(IntegratorProcessStartBehavior.SinglePerInstance);
            if (!result.RequestSucceeded)
            {
                Console.WriteLine("Cannot register BusinessLogic process - reason: " + result.ErrorMessage);
                Console.WriteLine("Process wil exit after keypress. Please press a key ...");
                Console.ReadKey();
                return;
            }

            ILogger businessLayerLogger = logFactory.GetLogger("BusinessLayer");


            //
            //IntegratorManager integratorManager = new IntegratorManager(businessLayerLogger);
            //

            //IPriceBookStore priceBookStore = new FakePriceBookStore();
            IBookTopProvidersStore bookTopProvidersStore = new FakeBookTopProvidersStore();
            IOrderManagementEx orderManagement = new FakeOrderManagement();
            IRiskManager riskManager = new NullRiskManager();
            IVenueDataProviderStore venueDataProviderStore = new FakeVenueDataProviderStore();


            CommandDistributor commandDistributor = new CommandDistributor(businessLayerLogger, false);
            InProcessClientsManager inProcessClientsManager =
                new InProcessClientsManager(bookTopProvidersStore, venueDataProviderStore, orderManagement, new List<IBroadcastInfosStore>() { riskManager }, businessLayerLogger);
            commandDistributor.RegisterWorker("StartClient", inProcessClientsManager.StartNewGuiClientAsync);


            ILogger wcfServicelog = logFactory.GetLogger("IntegratorServer");
            IClientRequestsHandler remoteGatewayProxy =
                new RemoteGatewayProxy(wcfServicelog, bookTopProvidersStore, venueDataProviderStore, orderManagement,
                                       new List<IBroadcastInfosStore>()
                                           {
                                               riskManager,
                                               new SymbolsInfoProvider(businessLayerLogger, new PriceHistoryProvider()),
                                               new IntegratorProcessInfoToBroadcastIngoAdapter(businessLayerLogger, processStateKeeper.IntegratorProcessInfo)
                                           });
            IntegratorService integratorService = IntegratorService.
                CreateIntegratorService(remoteGatewayProxy, wcfServicelog,
                                        string.Format("{0}_{1:yyMMddHHmm}", SettingsInitializator.Instance.InstanceName, DateTime.UtcNow),
                                        ConnectionChainRootBuildVersioningInfo, processStateKeeper.IntegratorProcessInfo);
            //integratorService.AnnounceCommunicationServiceStateChange(new CommunicationStateChangeEventArgs(CommunicationServiceStateAction.Start));

            ILogger diagServicelog = logFactory.GetLogger("DiagService");
            IntegratorDiagnosticsService ids = new IntegratorDiagnosticsService(diagServicelog,
                                                                                null,
                                                                                null,
                                                                                null,
                                                                                null,
                                                                                processStateKeeper.IntegratorProcessInfo);

            TaskEx.StartNew(processStateKeeper.SetRunning);

            ConsoleKeyInfo key = Console.ReadKey();
            while (key.Key == ConsoleKey.G)
            {
                Console.WriteLine("Running GC as requested");
                GC.Collect();
                GC.WaitForPendingFinalizers();
                key = Console.ReadKey();
            }

            Console.WriteLine("Pres key once more to exit the running action");
            Console.ReadKey();
            Console.WriteLine("Exiting the running integrator");

            processStateKeeper.SetShuttingDown();

            //integratorService.AnnounceCommunicationServiceStateChange(new CommunicationStateChangeEventArgs(CommunicationServiceStateAction.ShutDown));
            inProcessClientsManager.CloseAllClients("IntegratorClosing");

            processStateKeeper.SetInactive();
        }

        static void Run()
        {
            Console.WriteLine("Starting RUN action. Details will be in log files");
            Console.Title = "!!! INTEGRATOR HOSTING CONSOLE !!!";

            IntegratorProcessStateKeeper processStateKeeper = new IntegratorProcessStateKeeper(IntegratorProcessType.BusinessLogic);
            var result = processStateKeeper.TryRegisterIntegratorProcess(IntegratorProcessStartBehavior.SinglePerInstance);
            if (!result.RequestSucceeded)
            {
                Console.WriteLine("Cannot register BusinessLogic process - reason: " + result.ErrorMessage);
                Console.WriteLine("Process wil exit after keypress. Please press a key ...");
                Console.ReadKey();
                return;
            }

            IntegratorManager integratorManager = new IntegratorManager(logFactory.GetLogger("BusinessLayer"),
                                                                        processStateKeeper.IntegratorProcessInfo);
            _integratorManager = integratorManager;

            integratorManager.BindComponentsEvents();
            integratorManager.Start();

            ILogger wcfServicelog = logFactory.GetLogger("IntegratorServer");
            IClientRequestsHandler remoteGatewayProxy =
                new RemoteGatewayProxy(wcfServicelog, integratorManager.BookTopProvidersStore,
                                       integratorManager.VenueDataProviderStore, integratorManager.OrderManagement,
                                       integratorManager.BroadcastInfosStores);
            //gc handle hold via event subscription inside constructor
            IntegratorService integratorService = IntegratorService.CreateIntegratorService(
                remoteGatewayProxy, wcfServicelog,
                string.Format("{0}_{1:yyMMddHHmmss}", SettingsInitializator.Instance.InstanceName, DateTime.UtcNow),
                ConnectionChainRootBuildVersioningInfo, processStateKeeper.IntegratorProcessInfo);
            //integratorService.AnnounceCommunicationServiceStateChange(new CommunicationStateChangeEventArgs(CommunicationServiceStateAction.Start));
            
            ILogger diagServicelog = logFactory.GetLogger("DiagService");
            IntegratorDiagnosticsService ids = new IntegratorDiagnosticsService(diagServicelog,
                                                                                integratorManager.PriceStreamsInfoProvider,
                                                                                integratorManager.PriceBookDiagnostics,
                                                                                integratorManager.RiskManager.SymbolTrustworthinessInfoProvider,
                                                                                integratorManager.FixSessionsDiagnostics,
                                                                                processStateKeeper.IntegratorProcessInfo);
            integratorManager.CommandsExecutor.BindCommandDistributor(ids);

            TaskEx.StartNew(processStateKeeper.SetRunning);

            ManualResetEvent exitRequested = new ManualResetEvent(false);
            ManualResetEvent restartRequested = new ManualResetEvent(false);

            integratorManager.IntegratorRestartRequested +=
                () =>
                {
                    if (ExitProcess(processStateKeeper, integratorManager, true))
                        restartRequested.Set();
                };

            Task.Factory.StartNew(() =>
            {
                do
                {
                    WaitTillExit(integratorManager);
                } while (!ExitProcess(processStateKeeper, integratorManager, false));
                exitRequested.Set();
            }, TaskCreationOptions.LongRunning);

            WaitHandle.WaitAny(new[] {exitRequested, restartRequested});

            GC.KeepAlive(integratorManager);
        }

        private static void WaitTillExit(IntegratorManager integratorManager)
        {
            bool shouldExit = false;
            ConsoleKey previousKey = ConsoleKey.NoName;
            while (!shouldExit)
            {
                ConsoleKeyInfo key = Console.ReadKey();

                switch (key.Key)
                {
                    case ConsoleKey.G:
                        integratorManager.GcManager.FullCollect();
                        break;
                    case ConsoleKey.F:
                        LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Faked Fatal log requested by user. No action needed");
                        break;
                    case ConsoleKey.S:
                        integratorManager.GcManager.SetLowLatencySustainableMode();
                        break;
                    case ConsoleKey.K:
                        integratorManager.RiskManager.DisableTransmission("Trading disabled from Integratro command line");
                        break;
                    case ConsoleKey.L:
                        integratorManager.GcManager.SetLowLatencyMode();
                        break;
                    case ConsoleKey.I:
                        integratorManager.GcManager.SetInteractiveMode();
                        break;
                    case ConsoleKey.P:
                    case ConsoleKey.U:
                        previousKey = key.Key;
                        continue;
                        break;
                    case ConsoleKey.R:
                    case ConsoleKey.A:
                        if (
                            (SettingsInitializator.Instance.IsUat && previousKey == ConsoleKey.U &&
                             key.Key == ConsoleKey.A)
                            ||
                            (!SettingsInitializator.Instance.IsUat && previousKey == ConsoleKey.P &&
                             key.Key == ConsoleKey.R)
                            )
                        {
                            previousKey = key.Key;
                            continue;
                        }
                        break;
                    case ConsoleKey.O:
                    case ConsoleKey.T:
                        if (
                            (SettingsInitializator.Instance.IsUat && previousKey == ConsoleKey.A &&
                             key.Key == ConsoleKey.T)
                            ||
                            (!SettingsInitializator.Instance.IsUat && previousKey == ConsoleKey.R &&
                             key.Key == ConsoleKey.O)
                            )
                        {
                            shouldExit = true;

                        }
                        break;
                }

                previousKey = ConsoleKey.NoName;
            }
        }

        private static bool ExitProcess(IntegratorProcessStateKeeper processStateKeeper,
            IntegratorManager integratorManager, bool restart)
        {
            if (!integratorManager.RiskManager.IsTransmissionDisabled)
            {
                integratorManager.BusinessLayerLogger.Log(LogLevel.Fatal, "Attempt to stop integrator while order transmission is enabled - ignoring the attempt");
                return false;
            }

            processStateKeeper.SetShuttingDown();
            integratorManager.Stop();
            processStateKeeper.SetInactive();

            if (restart)
            {
                ProcessStartInfo pci = new ProcessStartInfo(Process.GetCurrentProcess().MainModule.FileName);
                pci.Arguments = "\"/run\"";
                Process.Start(pci);
            }

            return true;
        }

        static void RunHotspotDataProcess()
        {
            Console.WriteLine("Starting RUN HotspotData action. Details will be in log files");
            Console.Title = "Hotspot Data Process";

            IntegratorProcessStateKeeper processStateKeeper = new IntegratorProcessStateKeeper(IntegratorProcessType.HotspotMarketData);
            var result = processStateKeeper.TryRegisterIntegratorProcess(IntegratorProcessStartBehavior.SinglePerInstance);
            if (!result.RequestSucceeded)
            {
                Console.WriteLine("Cannot register Hotspot Data process - reason: " + result.ErrorMessage);
                Console.WriteLine("Process wil exit after keypress. Please press a key ...");
                Console.ReadKey();
                return;
            }


            // ... Initialize and run

            IntegratorHotspotDataManager hotspotIntegratorManager = new IntegratorHotspotDataManager(logFactory.GetLogger("HotspotData"),
                                                                        processStateKeeper.IntegratorProcessInfo);
            hotspotIntegratorManager.BindComponentsEvents();
            hotspotIntegratorManager.Start();

            ILogger wcfServicelog = logFactory.GetLogger("IntegratorServer");
            IClientRequestsHandler remoteGatewayProxy =
                new RemoteGatewayProxy(wcfServicelog, hotspotIntegratorManager.BookTopProvidersStore,
                                       hotspotIntegratorManager.VenueDataProviderStore, null,
                                       hotspotIntegratorManager.BroadcastInfosStores);
            //gc handle hold via event subscription inside constructor
            IntegratorService integratorService = IntegratorService.CreateIntegratorService(
                remoteGatewayProxy, wcfServicelog,
                string.Format("{0}_{1}_{2:yyMMddHHmmss}", SettingsInitializator.Instance.InstanceName, IntegratorProcessType.HotspotMarketData, DateTime.UtcNow),
                ConnectionChainRootBuildVersioningInfo, processStateKeeper.IntegratorProcessInfo);


            ILogger diagServicelog = logFactory.GetLogger("DiagService");
            IntegratorDiagnosticsService ids = new IntegratorDiagnosticsService(diagServicelog,
                                                                                hotspotIntegratorManager.PriceStreamsInfoProvider,
                                                                                //no ToB statistics
                                                                                null,
                                                                                //no symbol trust stats
                                                                                null,
                                                                                hotspotIntegratorManager.FixSessionsDiagnostics,
                                                                                processStateKeeper.IntegratorProcessInfo);
            hotspotIntegratorManager.CommandsExecutor.BindCommandDistributor(ids);
            //
            //
            TaskEx.StartNew(processStateKeeper.SetRunning);

            Console.ReadKey();
            Console.WriteLine("Pres key once more to exit the running Hotspot Data process");
            Console.ReadKey();
            Console.WriteLine("Exiting the running Hotpsot Data process");

            processStateKeeper.SetShuttingDown();

            //
            // Stop - tear down the service endpoints
            //
            hotspotIntegratorManager.Stop();

            processStateKeeper.SetInactive();

            Console.WriteLine("Procesing done, exited.");

            GC.KeepAlive(hotspotIntegratorManager);
        }

        static void RunDCProcess()
        {
            Console.WriteLine("Starting RUN DC action. Details will be in log files");
            Console.WriteLine("Action will be started by key press and will be closed by two key press. Please press a key...");
            Console.Title = "Data Collection Uploading Process";

            Console.ReadKey();

            IntegratorProcessStateKeeper processStateKeeper = new IntegratorProcessStateKeeper(IntegratorProcessType.DataCollection);
            var result = processStateKeeper.TryRegisterIntegratorProcess(IntegratorProcessStartBehavior.SinglePerEndpoint);
            if (!result.RequestSucceeded)
            {
                Console.WriteLine("Cannot register DC process - reason: " + result.ErrorMessage);
                Console.WriteLine("Process wil exit after keypress. Please press a key ...");
                Console.ReadKey();
                return;
            }

            IDataCollectorUploader toBdataCollectorUploader = DataCollectorUploaderManager.ToBDataUploader;
            IDataCollectorUploader mdDataCollectorUploader = DataCollectorUploaderManager.MarketDataUploader;
            IDataCollectorUploader outPricesCollectorUploader = DataCollectorUploaderManager.OutgoingPricesDataUploader;

            toBdataCollectorUploader.StartProcessing();
            mdDataCollectorUploader.StartProcessing();
            outPricesCollectorUploader.StartProcessing();

            TaskEx.StartNew(processStateKeeper.SetRunning);

            Console.ReadKey();
            Console.WriteLine("Pres key once more to exit the running data collection uploading");
            Console.ReadKey();
            Console.WriteLine("Exiting the running DC process");

            processStateKeeper.SetShuttingDown();

            toBdataCollectorUploader.StopProcessing();
            mdDataCollectorUploader.StopProcessing();
            outPricesCollectorUploader.StopProcessing();

            WaitHandle.WaitAll(
                new[]
                {
                    toBdataCollectorUploader.ReceivingDoneHandle, mdDataCollectorUploader.ReceivingDoneHandle,
                    outPricesCollectorUploader.ReceivingDoneHandle
                },
                DataCollectorUploaderManager.MaxProcessingTimeAfterShutdownSignalled);

            processStateKeeper.SetInactive();

            Console.WriteLine("Procesing done, exited.");
        }

        public enum SessionActionType
        {
            RestartSession,
            StopSession,
            StartSession
        }

        private static void SendCommandAndLogResults(string commandString, IRemoteCommandWorker remoteCommandWorker)
        {
            IntegratorRequestResult result = remoteCommandWorker.TrySendCommand(commandString);

            if (result.RequestSucceeded)
            {
                Console.WriteLine("Sending and executing of command succeded");
            }
            else
            {
                Console.WriteLine("Sending or executing of command failed. error: {0}", result.ErrorMessage);
            }
        }

        static void StartClientAction(string commandString, IRemoteCommandWorker remoteCommandWorker)
        {
            SendCommandAndLogResults("StartClient" + ";" + commandString, remoteCommandWorker);
        }

        static void SessionAction(string counterpartString, SessionActionType sessionActionType, IRemoteCommandWorker remoteCommandWorker)
        {
            counterpartString = counterpartString.Trim().ToUpperInvariant();

            if (counterpartString.Equals("ALL"))
            {
                foreach (string value in Counterparty.StringValues)
                {
                    SendCommandAndLogResults(sessionActionType.ToString() + ";" + value, remoteCommandWorker);
                }
            }
            else if (counterpartString.Equals("STPFLOW"))
            {
                SendCommandAndLogResults(sessionActionType.ToString().Replace("Session", "STPSession") + ";-", remoteCommandWorker);
            }
            else if(!Counterparty.StringValues.Contains(counterpartString))
            {
                Console.WriteLine("Unrecognized counterpart string: [{0}]", counterpartString);
            }
            else
            {
                SendCommandAndLogResults(sessionActionType.ToString() + ";" + counterpartString, remoteCommandWorker);
            }
        }
    }
}
