﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.BusinessLayer;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.MessageBus;

namespace Kreslik.Integrator.ConsoleRunner
{

    public class OrderResultRemoteGatewayProxy : ClientGatewayEventsDispatcherBase//, IRemoteClientGatewayProxy
    {
        private ITradingServiceProxy _clientGateway;
        private ClientToIntegratorConnectorHelper _clientToIntegratorConnectorHelper;
        private OrderUpdatesCheckingHelper _orderUpdatesCheckingHelper;

        public OrderResultRemoteGatewayProxy(ILogger logger, ITradingServiceProxy clientGateway,
                                             IBookTopProvidersStore bookTopProvidersStore,
                                             IVenueDataProviderStore venueDataProviderStore,
                                             IOrderManagementEx orderManagement,
                                             IEnumerable<IBroadcastInfosStore> broadcastInfosStores)
            : base(logger, clientGateway.TradingServerServiceIdentity, true)
        {
            this._clientGateway = clientGateway;

            this._clientToIntegratorConnectorHelper =
                new ClientToIntegratorConnectorHelper(logger, bookTopProvidersStore, venueDataProviderStore,
                                                      orderManagement, broadcastInfosStores,
                                                      this);
            this._orderUpdatesCheckingHelper = new OrderUpdatesCheckingHelper(logger);
        }

        public IntegratorRequestResult SubmitRequest(IntegratorRequestInfo requestInfo)
        {
            return this._clientToIntegratorConnectorHelper.SubmitRequest(requestInfo);
        }


        public void CloseProxyAndSignalDisconnect(string reason)
        {
            base.CloseLocalResourcesOnly(reason);
        }

        protected override void DispatchPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            _clientGateway.OnPriceUpdate(priceUpdateEventArgs);
        }

        protected override bool ThrottlePriceUpdateNeeded(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            return this._clientGateway.PriceUpdateThrottlingNeeded(priceUpdateEventArgs);
        }

        protected override void DispatchUnicastInfo(IntegratorInfoObjectBase unicastInfo)
        {
            this._orderUpdatesCheckingHelper.CheckEventIfExpected(unicastInfo);
            _clientGateway.OnIntegratorUnicastInfo(unicastInfo);
        }

        protected override void DispatchBroadcastInfo(IntegratorBroadcastInfoBase broadcastInfo)
        {
            _clientGateway.OnIntegratorBroadcastInfo(broadcastInfo);
        }

        protected override void DispatchGatewayClosed_CloseLocalResourcesOnly(string reason)
        {
            //We shouldn't actually notify the client. If the disconnection was initiated by client than we don't
            // want to loop the close back.
            // If it was initiated by server (IntegratorService is closing), then it already send messages to clients

            //_clientGateway.CloseLocalResourcesOnly(reason);

            this._clientToIntegratorConnectorHelper.CleanupContractWithIntegrator();
        }
    }

    public class RemoteGatewayProxy: IClientRequestsHandler
    {
        //write accesses must be snchronized (limitation of non-concurrent dictionary)
        private ConcurrentDictionary<ITradingServiceProxy, OrderResultRemoteGatewayProxy> _gatewayProxies = new ConcurrentDictionary<ITradingServiceProxy, OrderResultRemoteGatewayProxy>();
        private ILogger _logger;
        private IBookTopProvidersStore _bookTopProvidersStore;
        private IVenueDataProviderStore _venueDataProviderStore;
        private IOrderManagementEx _orderManagement;
        private IEnumerable<IBroadcastInfosStore> _broadcastInfosStores;

        public RemoteGatewayProxy(ILogger logger, IBookTopProvidersStore bookTopProvidersStore,
                                  IVenueDataProviderStore venueDataProviderStore, IOrderManagementEx orderManagement,
                                  IEnumerable<IBroadcastInfosStore> broadcastInfosStores)
        {
            this._logger = logger;
            this._bookTopProvidersStore = bookTopProvidersStore;
            this._venueDataProviderStore = venueDataProviderStore;
            this._orderManagement = orderManagement;
            this._broadcastInfosStores = broadcastInfosStores;
        }

        public void RegisterClientGateway(ITradingServiceProxy clientGateway)
        {
            var proxy = new OrderResultRemoteGatewayProxy(_logger, clientGateway, _bookTopProvidersStore,
                                                      _venueDataProviderStore, _orderManagement, _broadcastInfosStores);

            _gatewayProxies[clientGateway] = proxy;
            clientGateway.OnDisconnected += reason => this.FinalizeClientGateway(clientGateway, reason);
        }

        public void UnregisterClientGateway(ITradingServiceProxy clientGateway)
        {
            //We will get unregister in the OnDisconnested event

            //this.FinalizeClientGateway(clientGateway, "Client is unregistering");
        }

        private void FinalizeClientGateway(ITradingServiceProxy clientGateway, string reason)
        {
            if (_gatewayProxies.ContainsKey(clientGateway))
            {
                _gatewayProxies[clientGateway].CloseProxyAndSignalDisconnect(reason);
                OrderResultRemoteGatewayProxy unused;
                _gatewayProxies.TryRemove(clientGateway, out unused);
            }
        }

        public IntegratorRequestResult SubmitRequest(ITradingServiceProxy clientGateway, IntegratorRequestInfo requestInfo)
        {
            return _gatewayProxies[clientGateway].SubmitRequest(requestInfo);
        }


        public void CloseAndSignalDisconnect()
        {
            foreach (OrderResultRemoteGatewayProxy orderResultRemoteGatewayProxy in _gatewayProxies.Values)
            {
                orderResultRemoteGatewayProxy.CloseProxyAndSignalDisconnect("The whole server side proxy is closing");
            }
        }
    }
}
