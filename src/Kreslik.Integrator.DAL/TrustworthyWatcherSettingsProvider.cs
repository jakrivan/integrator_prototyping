﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.DAL
{
    public class TrustworthyWatcherSettingsProvider : ITrustworthyWatcherSettingsProvider
    {
        public IList<ITrustworthyWatcherSettings> GetSettings()
        {
            try
            {
                return this.GetSettingsInternal().ToList();
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null)
                    .LogException(LogLevel.Fatal,
                        "Couldn't obtain symbol trustworthy settings, symbols will turn untrusted as a result. Turn of the trustworthiness check in RiskManagement.xml to overcome this issue",
                        e);
            }

            return new ITrustworthyWatcherSettings[0];
        }

        private IEnumerable<ITrustworthyWatcherSettings> GetSettingsInternal()
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getSettingsCommand = new SqlCommand("[dbo].[GetSymblTrustedRates_SP]", sqlConnection))
                {
                    getSettingsCommand.CommandType = CommandType.StoredProcedure;

                    using (var reader = getSettingsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Symbol symbol = (Symbol) reader.GetString(reader.GetOrdinal("Symbol"));
                            decimal? maxTrustedSpreadBp =
                                reader.GetNullableDecimal(reader.GetOrdinal("MaxTrustedSpreadBp"));
                            decimal? minTrustedSpreadBp =
                                reader.GetNullableDecimal(reader.GetOrdinal("MinTrustedSpreadBp"));
                            decimal? maxTrustedSpreadDecimal =
                                reader.GetNullableDecimal(reader.GetOrdinal("MaxTrustedSpreadDecimal"));
                            decimal? minTrustedSpreadDecimal =
                                reader.GetNullableDecimal(reader.GetOrdinal("MinTrustedSpreadDecimal"));
                            int? maxAllowedTickAge_milliseconds =
                                reader.GetNullableInt32(reader.GetOrdinal("MaximumAllowedTickAge_milliseconds"));

                            yield return
                                new TrustworthyWatcherSettings(symbol: symbol, maxAllowedSpreadBp: maxTrustedSpreadBp,
                                    minAllowedSpreadBp: minTrustedSpreadBp, maxAllowedSpread: maxTrustedSpreadDecimal,
                                    minAllowedSpread: minTrustedSpreadDecimal,
                                    maxAllowedTickAge_milliseconds: maxAllowedTickAge_milliseconds);
                        }
                    }
                }
            }
        }

        public ITrustworthyWatcherSettings GetEmptySettings(Common.Symbol symbol)
        {
            return TrustworthyWatcherSettings.CreateEmptySettings(symbol);
        }

        private class TrustworthyWatcherSettings : ITrustworthyWatcherSettings
        {
            internal static ITrustworthyWatcherSettings CreateEmptySettings(Symbol symbol)
            {
                return new TrustworthyWatcherSettings(symbol);
            }

            private TrustworthyWatcherSettings(Symbol symbol)
            {
                this.Symbol = symbol;
                this.AreSettingsComplete = false;
            }

            internal TrustworthyWatcherSettings(Symbol symbol, decimal? maxAllowedSpreadBp, decimal? minAllowedSpreadBp,
                decimal? maxAllowedSpread, decimal? minAllowedSpread, int? maxAllowedTickAge_milliseconds)
            {
                this.Symbol = symbol;

                this.AreSettingsComplete = maxAllowedSpreadBp.HasValue && minAllowedSpreadBp.HasValue &&
                                           maxAllowedTickAge_milliseconds.HasValue;


                if (!this.AreSettingsComplete)
                {
                    return;
                }

                this.MaxAllowedSpreadBp = maxAllowedSpreadBp.Value;
                this.MinAllowedSpreadBp = minAllowedSpreadBp.Value;
                this.MaxAllowedTickAge = TimeSpan.FromMilliseconds(maxAllowedTickAge_milliseconds.Value);

                if (maxAllowedSpread.HasValue)
                {
                    if(maxAllowedSpread.Value >= 0m)
                        this.MaxAllowedSpread = AtomicDecimal.ConvertToAtomicDecimalCuttingExcessScale(maxAllowedSpread.Value);
                    else
                        LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "MaxAllowedSpread ({0}) for {1} is set as negative number - it will be considered zero",
                            maxAllowedSpread.Value, symbol);
                }

                if (minAllowedSpread.HasValue)
                {
                    if (minAllowedSpread.Value <= 0m)
                        this.MinAllowedSpreadInverted = AtomicDecimal.ConvertToAtomicDecimalCuttingExcessScale(-minAllowedSpread.Value);
                    else
                        LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "MinAllowedSpread ({0}) for {1} is set as positive number - it will be considered zero",
                            minAllowedSpread.Value, symbol);
                }
            }

            public Symbol Symbol { get; private set; }
            public bool AreSettingsComplete { get; private set; }
            public decimal MaxAllowedSpreadBp { get; private set; }
            public decimal MinAllowedSpreadBp { get; private set; }
            public AtomicDecimal MaxAllowedSpread { get; private set; }
            public AtomicDecimal MinAllowedSpreadInverted { get; private set; }
            public TimeSpan MaxAllowedTickAge { get; private set; }
        }
    }
}
