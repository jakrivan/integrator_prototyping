﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.DAL
{
    public interface ITradingGroupsCache
    {
        TradingSystemGroup GeTradingSystemGroup(int identity);
    }

    public class TradingGroupsCache : ITradingGroupsCache
    {
        private Dictionary<int, WrittableTradingSystemGroup> _groupsInMemoryCache = 
            new Dictionary<int, WrittableTradingSystemGroup>();
        private ILogger _logger;

        private const string _unknownGroupName = "<UNKNOWN>";

        public TradingGroupsCache(ILogger logger)
        {
            this._logger = logger;
            this.RefreshGroupsCacheFromBackend();
            TradingHoursHelper.Instance.MarketRollingOver += RefreshGroupsCacheFromBackend;
        }

        public TradingGroupsCache()
            //only fatal messages, no need for specific logger
            :this(LogFactory.Instance.GetLogger(null))
        { }

        public TradingSystemGroup GeTradingSystemGroup(int identity)
        {
            WrittableTradingSystemGroup tradingSystemGroup;

            try
            {
                if (!_groupsInMemoryCache.TryGetValue(identity, out tradingSystemGroup))
                {
                    this.RefreshGroupsCacheFromBackend();

                    if (!_groupsInMemoryCache.TryGetValue(identity, out tradingSystemGroup))
                    {
                        tradingSystemGroup = new WrittableTradingSystemGroup(identity, _unknownGroupName);
                        _groupsInMemoryCache[identity] = tradingSystemGroup;
                        this._logger.Log(LogLevel.Fatal, "GroupsCache received request for unknown group id: " + identity);
                    }
                }
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "GroupsCahce experienced exception when retrieving group id: {0}", identity);
                tradingSystemGroup = new WrittableTradingSystemGroup(identity, _unknownGroupName);
            }

            return tradingSystemGroup;
        }

        private void RefreshGroupsCacheFromBackend()
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand command = new SqlCommand("[dbo].[GetTradingSystemGroupNames_SP]", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int groupId = reader.GetInt32(reader.GetOrdinal("TradingSystemGroupId"));
                            string groupName = reader.GetString(reader.GetOrdinal("TradingSystemGroupName"));

                            if (_groupsInMemoryCache.ContainsKey(groupId))
                            {
                                _groupsInMemoryCache[groupId].UpdateName(groupName);
                            }
                            else
                            {
                                _groupsInMemoryCache[groupId] = new WrittableTradingSystemGroup(groupId, groupName);
                            }
                        }
                    }
                }
            }
        }
    }
}
