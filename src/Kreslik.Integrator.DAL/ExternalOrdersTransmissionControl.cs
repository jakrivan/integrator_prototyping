﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.DAL
{
    public class ExternalOrdersTransmissionControl : BroadcastInfosStoreBase, IExternalOrdersTransmissionControl, IExternalOrdersTransmissionControlScheduling
    {
        private ILogger _logger; 
        private TimeSpan _watchDogInterval = TimeSpan.FromSeconds(DALBehavior.Sections.OhterSettings.KillSwitchWatchDogIntervalSeconds);
        private TimeSpan _pollInterval = TimeSpan.FromSeconds(DALBehavior.Sections.OhterSettings.KillSwitchPollIntervalSeconds);
        private DateTime _lastUpdatedTime;
        private SafeTimer _watchDogTimer;
        private SafeTimer _pollTimer;
        private object _stateLocker = new object();
        private bool _updatingBacked = false;
        private static int _instancesCnt = 0;

        public ExternalOrdersTransmissionControl(ILogger logger)
            :base(logger)
        {
            this._logger = logger;
            this._lastUpdatedTime = DateTime.UtcNow;
            this.IsTransmissionDisabled = true;
            this.ChangeOrderTransmission(new OrderTransmissionStatusChangedEventArgs(false, "Initial TransmissionControlState (until update from backend)"));
            //setting and broadcasting initial state
            this.SetIsGoFlatOnlyEnabled(false, true);

            _watchDogTimer = new SafeTimer(WatchDogRoutine, _watchDogInterval, _watchDogInterval)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.GetTimerPrecisionForDelay(_watchDogInterval),
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Normal
            };
            _pollTimer = new SafeTimer(ControlState, _pollInterval, _pollInterval, true)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.GetTimerPrecisionForDelay(_pollInterval),
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Normal
            };

            if (Interlocked.Increment(ref _instancesCnt) > 1)
            {
                logger.Log(LogLevel.Error, "Multiple instances of ExternalOrdersTransmissionControl are being in use - this is undesirable");
            }
        }

        private static string _transmissionControlConnectionString = PrepareTransmissionControlConnectionString();

        private static string PrepareTransmissionControlConnectionString()
        {
            return (DALBehavior.Sections.IntegratorConnection.ConnectionString.EndsWith(";")
                ? DALBehavior.Sections.IntegratorConnection.ConnectionString + "Max Pool Size=2"
                : DALBehavior.Sections.IntegratorConnection.ConnectionString + ";Max Pool Size=2")
                   +
                   ";Connection Timeout=" + (DALBehavior.Sections.OhterSettings.KillSwitchWatchDogIntervalSeconds - 1);
        }

        private string TransmissionControlConnectionString
        {
            get { return _transmissionControlConnectionString; }
        }

        private void ControlState()
        {
            bool stateBeforeCheck;
            bool turnedOff;

            //this._logger.Log(LogLevel.Trace, "Starting to refresh KillSwState");

            lock (_stateLocker)
            {
                if (_updatingBacked) return;
                stateBeforeCheck = this.IsTransmissionDisabled;
                turnedOff = this.IsTransmissionDisabled;
            }

            bool refreshed = false;

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(this.TransmissionControlConnectionString))
                {
                    sqlConnection.Open();
                    using (
                        SqlCommand killSwStateCommand = new SqlCommand("SELECT [dbo].[GetExternalOrdersTransmissionSwitchCumulativeState]()",
                                                                       sqlConnection)
                        )
                    {
                        object isOrdersTransmissionEnabled = killSwStateCommand.ExecuteScalar();
                        turnedOff = true;
                        if (isOrdersTransmissionEnabled != null)
                        {
                            turnedOff = !(bool) isOrdersTransmissionEnabled;
                        }
                        refreshed = true;
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                _logger.LogException(LogLevel.Error, "Got SqlException when obtaining External Orders Transmission Control state", sqlEx);
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, "Got unexpected Exception when obtaining External Orders Transmission Control state", e);
            }


            bool isChange = false;
            lock (_stateLocker)
            {
                isChange = 
                    //if we are currantly updating the state of switch in backend, lets throw away the obtained old state
                    !_updatingBacked 
                    //if the obtained state is actually a change
                    && turnedOff != this.IsTransmissionDisabled 
                    //however if there was internal state change during the query, then we likely obtained an old value
                    && stateBeforeCheck == this.IsTransmissionDisabled;
            }

            if (isChange)
            {
                this.IsTransmissionDisabled = turnedOff;
                string reason =
                    string.Format("Changing External Orders Transmission Control state based on backend to {0}.",
                                  turnedOff ? "DISABLED" : "ENABLED");
                _logger.Log(turnedOff ? LogLevel.Error : LogLevel.Info, reason);
                this.ChangeOrderTransmission(new OrderTransmissionStatusChangedEventArgs(!IsTransmissionDisabled, reason));
            }

            if (refreshed)
            {
                _lastUpdatedTime = DateTime.UtcNow;
                _watchDogTimer.Change(_watchDogInterval, _watchDogInterval);
            }

            //this._logger.Log(LogLevel.Trace, "Finished refresh KillSwState");

            GetGoFlatOnlyState();
        }

        private void GetGoFlatOnlyState()
        {
            try
            {
                if (_updatingBacked) return;
                using (SqlConnection sqlConnection = new SqlConnection(this.TransmissionControlConnectionString))
                {
                    sqlConnection.Open();
                    using (
                        SqlCommand getGoFlatCommand = new SqlCommand("[dbo].[GetSystemsTradingControl_SP]",
                                                                       sqlConnection)
                        )
                    {
                        object isGoFlatOnlyEnabledObject = getGoFlatCommand.ExecuteScalar();
                        this.SetIsGoFlatOnlyEnabled((bool) isGoFlatOnlyEnabledObject, false);
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                _logger.LogException(LogLevel.Error, "Got SqlException when obtaining External Orders Transmission Control state", sqlEx);
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, "Got unexpected Exception when obtaining External Orders Transmission Control state", e);
            }
        }

        private void WatchDogRoutine()
        {
            //this._logger.Log(LogLevel.Trace, "Watchdog Checkpoint");
            if (!IsTransmissionDisabled && DateTime.UtcNow > _lastUpdatedTime + _watchDogInterval)
            {
                string reason =
                    string.Format(
                        "External Orders Transmission Control state wasn't updated from {0}, setting it to DISABLED",
                        _lastUpdatedTime);
                _logger.Log(LogLevel.Fatal, reason);
                this.TurnOnGoFlatOnly(reason);
                
                Thread.Sleep(TimeSpan.FromMilliseconds(300));

                IsTransmissionDisabled = true;
                this.ChangeOrderTransmission(new OrderTransmissionStatusChangedEventArgs(!IsTransmissionDisabled, reason));
            }
        }

        public bool IsTransmissionDisabled { get; private set; }
        public bool IsGoFlatOnlyEnabled { get; private set; }
        public DateTime GoFlatOnlyLastEnabledTime { get; private set; }

        private void SetIsGoFlatOnlyEnabled(bool isGoFlatOnlyEnabled, bool async)
        {
            if (this.IsGoFlatOnlyEnabled != isGoFlatOnlyEnabled)
            {
                if (isGoFlatOnlyEnabled)
                    this.GoFlatOnlyLastEnabledTime = HighResolutionDateTime.UtcNow;

                this.IsGoFlatOnlyEnabled = isGoFlatOnlyEnabled;

                if (async)
                    TaskEx.StartNew(() => SetIsGoFlatOnlyEnabledInternal(isGoFlatOnlyEnabled));
                else
                    SetIsGoFlatOnlyEnabledInternal(isGoFlatOnlyEnabled);
            }
        }

        private void SetIsGoFlatOnlyEnabledInternal(bool isGoFlatOnlyEnabled)
        {
            this._logger.Log(LogLevel.Info, "Altering state of GoFlatOnly to {0}", isGoFlatOnlyEnabled ? "ON" : "OFF");
            this.GoFlatOnlyStateChanged.SafeInvoke(this._logger, isGoFlatOnlyEnabled);
            this.OnNewBroadcastInfo(new TradingAllowedStatusChangedEventArgs(isGoFlatOnlyEnabled));
        }

        public event Action<bool> GoFlatOnlyStateChanged;

        public event Action<OrderTransmissionStatusChangedEventArgs> OrderTransmissionChanged;

        private void ChangeOrderTransmission(OrderTransmissionStatusChangedEventArgs changeArgs)
        {
            this.OrderTransmissionChanged.SafeInvoke(this._logger, changeArgs);
            this.OnNewBroadcastInfo(changeArgs);
        }

        public void DisableTransmission(string reason)
        {
            this.DisableTransmission(reason, true);
        }

        private void DisableTransmission(string reason, bool needsEscalatedLogging)
        {
            if (IsTransmissionDisabled)
            {
                _logger.Log(needsEscalatedLogging ? LogLevel.Warn : LogLevel.Info, "Ignoring redundant request for disabling External Orders Transmission");
                return;
            }

            lock (_stateLocker)
            {
                _updatingBacked = true;
                this.IsTransmissionDisabled = true;
            }

            string reasonInternal = string.Format("Internal request to disabling External Orders Transmission: {0}",
                                                  reason);
            _logger.Log(needsEscalatedLogging ? LogLevel.Error : LogLevel.Info, reasonInternal);

            TaskEx.StartNew(
                () =>
                    this.ChangeOrderTransmission(new OrderTransmissionStatusChangedEventArgs(!IsTransmissionDisabled,
                        reasonInternal)), TimerTaskFlagUtils.WorkItemPriority.Highest);

            if (!AllowPropagationOfStateChangesToBackend)
            {
                this._logger.Log(needsEscalatedLogging ? LogLevel.Fatal : LogLevel.Info,
                    "Attempt to alter backend KillSwitch (reason: {0}) from instance [{1}] with disallowed trading - ignoring the attempt",
                    reason, SettingsInitializator.Instance.InstanceName);
                _updatingBacked = false;
                return;
            }

            if (IntegratorStateInitializer.IntegratorProcessType != IntegratorProcessType.BusinessLogic)
            {
                this._logger.Log(needsEscalatedLogging ? LogLevel.Fatal : LogLevel.Info,
                    "Attempt to alter backend GoFlatOnly (reason: {0}) from process type [{1}] other than Business logic - ignoring the attempt",
                    reason, IntegratorStateInitializer.IntegratorProcessType);
                _updatingBacked = false;
                return;
            }

            TaskEx.StartNew(()
                                  =>
            {
                do
                {
                    try
                    {
                        using (
                            SqlConnection sqlConnection = new SqlConnection(this.TransmissionControlConnectionString)
                            )
                        {
                            sqlConnection.Open();
                            using (
                                SqlCommand setKillSwCommand = new SqlCommand("[dbo].[DisableExternalOrdersTransmission_SP]",
                                                                             sqlConnection))
                            {
                                setKillSwCommand.CommandType = CommandType.StoredProcedure;
                                setKillSwCommand.Parameters.AddWithValue("@SwitchName",
                                    DALBehavior.Sections.OhterSettings.IntegratorSwitchName);
                                setKillSwCommand.Parameters.AddWithValue("@ChangedBy",
                                                                         "KGT Integrator Risk Management");
                                setKillSwCommand.Parameters.AddWithValue("@ChangedFrom", Environment.MachineName);
                                setKillSwCommand.Parameters.AddWithValue("@Reason", reason);
                                setKillSwCommand.ExecuteNonQuery();
                                _updatingBacked = false;
                            }
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        _logger.LogException(LogLevel.Error, "Got SqlException when disabling External Orders Transmission", sqlEx);
                    }
                    catch (Exception e)
                    {
                        _logger.LogException(LogLevel.Fatal,
                                             "Got unexpected Exception when disabling External Orders Transmission", e);
                    }

                    if (_updatingBacked)
                    {
                        System.Threading.Thread.Sleep(_pollInterval);
                    }

                } while (_updatingBacked);
            });
        }

        public void ScheduledDisableTransmission()
        {
            this.DisableTransmission("Schedule", false);
        }

        public void ScheduledTurnOnGoFlatOnly()
        {
            this.UpdateGoFlatOnly(true, "Schedule");
        }

        public void TurnOnGoFlatOnly(string reason)
        {
            this._logger.Log(LogLevel.Fatal, "Internal request to turn GoFlatOnly ON: " + reason);
            if (!this.IsGoFlatOnlyEnabled)
            {
                this.UpdateGoFlatOnly(true, reason);
            }
        }

        public bool AllowPropagationOfStateChangesToBackend { get; set; }

        private void UpdateGoFlatOnly(bool turnOnGoFlatOnly, string reason)
        {
            lock (_stateLocker)
            {
                _updatingBacked = true;
            }

            this.SetIsGoFlatOnlyEnabled(turnOnGoFlatOnly, true);


            if (!AllowPropagationOfStateChangesToBackend)
            {
                this._logger.Log(LogLevel.Error,
                    "Attempt to alter backend GoFlatOnly (reason: {0}) from instance [{1}] with disallowed trading - ignoring the attempt",
                    reason, SettingsInitializator.Instance.InstanceName);
                _updatingBacked = false;
                return;
            }

            if (IntegratorStateInitializer.IntegratorProcessType != IntegratorProcessType.BusinessLogic)
            {
                this._logger.Log(LogLevel.Error,
                    "Attempt to alter backend GoFlatOnly (reason: {0}) from process type [{1}] other than Business logic - ignoring the attempt",
                    reason, IntegratorStateInitializer.IntegratorProcessType);
                _updatingBacked = false;
                return;
            }

            TaskEx.StartNew(()
                                  =>
            {
                do
                {
                    try
                    {
                        using (
                            SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                        {
                            sqlConnection.Open();
                            using (
                                SqlCommand setKillSwCommand = new SqlCommand("[dbo].[FlipSystemsTradingControl_SP]",
                                                                             sqlConnection))
                            {
                                setKillSwCommand.CommandType = CommandType.StoredProcedure;
                                setKillSwCommand.Parameters.AddWithValue("@GoFlatOnly", turnOnGoFlatOnly);
                                setKillSwCommand.ExecuteNonQuery();
                                _updatingBacked = false;
                                break;
                            }
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        _logger.LogException(LogLevel.Error, "Got SqlException when disabling External Orders Transmission", sqlEx);
                    }
                    catch (Exception e)
                    {
                        _logger.LogException(LogLevel.Fatal,
                                             "Got unexpected Exception when disabling External Orders Transmission", e);
                    }


                    System.Threading.Thread.Sleep(_pollInterval);

                } while (true);
            });
        }
    }
}
