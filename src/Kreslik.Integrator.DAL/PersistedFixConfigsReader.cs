﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.DAL
{
    //static since Fluent credentials are global - so we don't want to read them N-times
    internal static class FluentCredentialsReader
    {
        static FluentCredentialsReader()
        {
            _fluentCredentials = ReadFluentCredentials();
        }

        private static IFluentCredentials _fluentCredentials;
        internal static IFluentCredentials FluentCredentials { get { return _fluentCredentials; }}

        private static IFluentCredentials ReadFluentCredentials()
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand getConfigCommand = new SqlCommand("[dbo].[GetFluentCredentials_SP]", sqlConnection))
                    {
                        getConfigCommand.CommandType = CommandType.StoredProcedure;

                        using (var reader = getConfigCommand.ExecuteReader())
                        {
                            FluentCredentialsHolder hldr;
                            if (reader.Read())
                            {
                                hldr = new FluentCredentialsHolder(
                                    username: reader.GetString(reader.GetOrdinal("UserName")),
                                    password: reader.GetString(reader.GetOrdinal("Password")),
                                    onBehalfOfCompId: reader.GetString(reader.GetOrdinal("OnBehalfOfCompId"))
                                    );

                                if (reader.Read())
                                {
                                    LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "More than one Fluent Fix credentials record");
                                }
                                
                            }
                            else
                            {
                                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Couldn't find Fluent Fix credentials in backend");
                                hldr = null;
                            }

                            return hldr;
                        }
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                LogFactory.Instance.GetLogger(null).LogException(LogLevel.Fatal, "Got SqlException when obtainingFixfluent redirection state in backend", sqlEx);
                throw;
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null).LogException(LogLevel.Fatal, "Got unexpected Exception when obtainingFixfluent redirection state in backend", e);
                throw;
            }
        }

        private class FluentCredentialsHolder: IFluentCredentials
        {
            internal FluentCredentialsHolder(string username, string password, string onBehalfOfCompId)
            {
                this.UserName = username;
                this.Password = password;
                this.OnBehalfOfCompId = onBehalfOfCompId;
            }

            public string UserName { get; private set; }
            public string Password { get; private set; }
            public string OnBehalfOfCompId { get; private set; }
        }
    }

    public class PersistedFixConfigsReader : IPersistedFixConfigsReader
    {
        private readonly string _environmentName;
        private readonly string _configName;
        //private string _realConfigName;
        private ILogger _logger;

        public PersistedFixConfigsReader(string environmentName, string configName, ILogger logger)
        {
            this._configName = configName;
            this._environmentName = environmentName;
            this._logger = logger;
            //this.IsRedirectedViaFluent = ReadFluentRedirectionState();
        }

        public PersistedFixConfigsReader(string configName, ILogger logger)
            : this(SettingsInitializator.Instance.EnvironmentName, configName, logger)
        { }

        public TextReader ReadFixConfig()
        {
            string fixConfig = null;

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand getConfigCommand = new SqlCommand("[dbo].[GetFixSettingsCfg_SP]", sqlConnection))
                    {
                        getConfigCommand.CommandType = CommandType.StoredProcedure;
                        getConfigCommand.Parameters.AddWithValue("@EnvironmentName", this._environmentName);
                        getConfigCommand.Parameters.AddWithValue("@SettingName", this._configName);
                        getConfigCommand.Parameters.AddWithValue("@AllowFluentRedirection", FluentRedirectionSettings.IsRedirectionAllowed);


                        using (var reader = getConfigCommand.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                fixConfig = reader.GetString(reader.GetOrdinal("Configuration"));
                                this.IsRedirectedViaFluent = reader.GetString(reader.GetOrdinal("Name")).EndsWith("_Fluent", StringComparison.Ordinal);
                                Counterparty cpt;
                                if (this._configName.EndsWith("_ORD") && Counterparty.TryParse(this._configName.Substring(0, 3), out cpt))
                                    _fluentRedirectionInfo.SetIsOrdersSessionRedirectedViaFlunet(cpt, this.IsRedirectedViaFluent);
                            }
                            else
                            {
                                _logger.Log(LogLevel.Error,
                                            "Couldn't find Fix config [{0}] for environment [{1}] in backend",
                                            _configName, _environmentName);
                            }

                            if (reader.Read())
                            {
                                _logger.Log(LogLevel.Error,
                                            "More than one Fix config [{0}] for environment [{1}] in backend",
                                            _configName, _environmentName);
                                fixConfig = null;
                            }
                        }
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                _logger.LogException(LogLevel.Error, sqlEx, "Got SqlException when obtainingFix config [{0}] for environment [{1}] in backend",
                                            _configName, _environmentName);
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, e, "Got unexpected Exception when obtaining Fix config [{0}] for environment [{1}] in backend",
                                            _configName, _environmentName);
            }

            if (fixConfig == null)
            {
                _logger.Log(LogLevel.Fatal, "Errors when obtaining Fix config [{0}] for environment [{1}] in backend. For details see log file",
                                            _configName, _environmentName);
                return null;
            }
            else
            {
                return new StringReader(fixConfig);
            }
        }

        //be careful!! QUO sessions are not redirected - so each config has it's own state
        private bool ReadGlobalFluentRedirectionState()
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand getConfigCommand = new SqlCommand("[dbo].[GetFluentRedirectionState_SP]", sqlConnection))
                    {
                        getConfigCommand.CommandType = CommandType.StoredProcedure;

                        return (bool) getConfigCommand.ExecuteScalar();
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                _logger.LogException(LogLevel.Fatal, "Got SqlException when obtainingFixfluent redirection state in backend", sqlEx);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, "Got unexpected Exception when obtainingFixfluent redirection state in backend", e);
                throw;
            }
        }


        public string ConfigName
        {
            get { return this._configName; }
        }

        public byte[] GetCertificateFile(string certificateName)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand getCertificateCommand = new SqlCommand("[dbo].[GetCertificateFile_SP]", sqlConnection))
                    {
                        getCertificateCommand.CommandType = CommandType.StoredProcedure;
                        getCertificateCommand.Parameters.AddWithValue("@FileName", certificateName);


                        using (var reader = getCertificateCommand.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return (byte[])reader.GetValue(0);
                            }
                        }
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                _logger.LogException(LogLevel.Fatal, sqlEx, "Got SqlException when obtaining certificate [{0}] in backend",
                                            certificateName);
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, e, "Got unexpected Exception when obtaining certificate [{0}] in backend",
                                            certificateName);
            }

            return null;
        }

        public XmlDocument GetXmlDictionary(string dictionaryName)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand getDicXmlCommand = new SqlCommand("[dbo].[GetFixDictionaryXml_SP]", sqlConnection))
                    {
                        getDicXmlCommand.CommandType = CommandType.StoredProcedure;
                        getDicXmlCommand.Parameters.AddWithValue("@DictionaryName", dictionaryName);

                        using (var reader = getDicXmlCommand.ExecuteXmlReader())
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.Load(reader);
                            return doc;
                        }
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                _logger.LogException(LogLevel.Error, sqlEx, "Got SqlException when obtaining FIX Dictionary: {0}", dictionaryName);
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, e, "Got unexpected Exception when obtaining FIX Dictionary: {0}", dictionaryName);
            }

            return null;
        }


        public bool IsQuotingSession
        {
            get { return this._configName.EndsWith("_QUO", StringComparison.Ordinal); }
        }

        public bool IsRedirectedViaFluent { get; private set; }


        public IFluentCredentials FluentCredentials
        {
            get { return FluentCredentialsReader.FluentCredentials; }
        }

        private static readonly FluentRedirectionInfoInternal _fluentRedirectionInfo = new FluentRedirectionInfoInternal();
        public static IFluentRedirectionInfo FluentRedirectionInfo { get { return _fluentRedirectionInfo; } }

        private class FluentRedirectionInfoInternal : IFluentRedirectionInfo
        {
            private readonly bool[] _fluentRediractionByCounterparty = new bool[Counterparty.ValuesCount];

            internal void SetIsOrdersSessionRedirectedViaFlunet(Counterparty cpt, bool isRedirected)
            {
                this._fluentRediractionByCounterparty[(int)cpt] = isRedirected;

                if (cpt == Counterparty.L01)
                {
                    foreach (Counterparty lmaxCounterparty in LmaxCounterparty.LayersOfLM1)
                    {
                        this._fluentRediractionByCounterparty[(int)lmaxCounterparty] = isRedirected;
                    }
                }
            }

            public bool IsRedirectedViaFlunet(Counterparty cpt)
            {
                return this._fluentRediractionByCounterparty[(int)cpt];
            }
        }
    }
}
