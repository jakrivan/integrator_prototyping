﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.DAL
{
    public struct PersistedDealsStatistics
    {
        public PersistedDealsStatistics(Counterparty counterparty, int dealsNum, decimal dealsVolumeInUsd)
            :this()
        {
            this.Counterparty = counterparty;
            this.DealsNum = dealsNum;
            this.DealsVolumeInUsd = dealsVolumeInUsd;
        }

        public Counterparty Counterparty { get; private set; }
        public int DealsNum { get; private set; }
        public decimal DealsVolumeInUsd { get; private set; }
    }

    public struct PersistedCounterpartyStatistics
    {
        public PersistedCounterpartyStatistics(Counterparty counterparty, decimal nopAbs, decimal volumeBaseAbsInUsd,
                                      decimal volumeShare, int dealsNum, decimal avgDealSizeUsd, int kgtRejectionsNum,
                                      int ctpRejectionsNum, decimal kgtRejectionsRate, decimal ctpRejectionsRate,
                                      long avgIntegratorLatencyNanoseconds, long avgCounterpartyLatencyNanoseconds)
            : this()
        {
            this.Counterparty = counterparty;
            this.NopAbs = nopAbs;
            this.VolumeBaseAbsInUsd = volumeBaseAbsInUsd;
            this.VolumeShare = volumeShare;
            this.DealsNum = dealsNum;
            this.AvgDealSizeUsd = avgDealSizeUsd;
            this.KGTRejectionsNum = kgtRejectionsNum;
            this.CtpRejectionsNum = ctpRejectionsNum;
            this.KGTRejectionsRate = kgtRejectionsRate;
            this.CtpRejectionsRate = ctpRejectionsRate;
            this.AvgIntegratorLatencyNanoseconds = avgIntegratorLatencyNanoseconds;
            this.AvgCounterpartyLatencyNanoseconds = avgCounterpartyLatencyNanoseconds;
        }

        public Counterparty Counterparty { get; private set; }
        public decimal NopAbs { get; private set; }
        public decimal VolumeBaseAbsInUsd { get; private set; }
        public decimal VolumeShare { get; private set; }
        public int DealsNum { get; private set; }
        public decimal AvgDealSizeUsd { get; private set; }
        public int KGTRejectionsNum { get; private set; }
        public int CtpRejectionsNum { get; private set; }
        public decimal KGTRejectionsRate { get; private set; }
        public decimal CtpRejectionsRate { get; private set; }
        public long AvgIntegratorLatencyNanoseconds { get; private set; }
        public long AvgCounterpartyLatencyNanoseconds { get; private set; }
    }

    public interface IPersistedDealsStatisticsProvider
    {
        IEnumerable<PersistedCounterpartyStatistics> GetCompleteStatisticsPerCounterparty();
    }

    public class PersistedDealsStatisticsProvider : IPersistedDealsStatisticsProvider
    {
        public IEnumerable<PersistedCounterpartyStatistics> GetCompleteStatisticsPerCounterparty()
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getRejectionsCommand = new SqlCommand("[dbo].[GetStatisticsPerCounterparty_DailyCached_SP]", sqlConnection))
                {
                    getRejectionsCommand.CommandType = CommandType.StoredProcedure;

                    using (var reader = getRejectionsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Counterparty counterparty =
                                (Counterparty)reader.GetString(reader.GetOrdinal("Counterparty"));
                            decimal nopAbs = reader.GetDecimalNullIsZero(reader.GetOrdinal("NopAbs"));
                            decimal volumeBaseAbsInUsd = reader.GetDecimalNullIsZero(reader.GetOrdinal("VolumeBaseAbsInUsd"));
                            decimal volumeShare = reader.GetDecimalNullIsZero(reader.GetOrdinal("VolumeShare"));
                            int dealsNum = reader.GetInt32NullIsZero(reader.GetOrdinal("DealsNum"));
                            decimal avgDealSizeUsd = reader.GetDecimalNullIsZero(reader.GetOrdinal("AvgDealSizeUsd"));
                            int kgtRejectionsNum = reader.GetInt32NullIsZero(reader.GetOrdinal("KGTRejectionsNum"));
                            int ctpRejectionsNum = reader.GetInt32NullIsZero(reader.GetOrdinal("CtpRejectionsNum"));
                            decimal kgtRejectionsRate = reader.GetDecimalNullIsZero(reader.GetOrdinal("KGTRejectionsRate"));
                            decimal ctpRejectionsRate = reader.GetDecimalNullIsZero(reader.GetOrdinal("CtpRejectionsRate"));
                            long avgIntegratorLatencyNanoseconds = reader.GetInt64NullIsZero(reader.GetOrdinal("AvgIntegratorLatencyNanoseconds"));
                            long avgCounterpartyLatencyNanoseconds = reader.GetInt64NullIsZero(reader.GetOrdinal("AvgCounterpartyLatencyNanoseconds"));

                            yield return
                                new PersistedCounterpartyStatistics(counterparty, nopAbs, volumeBaseAbsInUsd,
                                                                    volumeShare, dealsNum, avgDealSizeUsd, kgtRejectionsNum,
                                                                    ctpRejectionsNum, kgtRejectionsRate, ctpRejectionsRate, 
                                                                    avgIntegratorLatencyNanoseconds, avgCounterpartyLatencyNanoseconds);
                        }
                    }
                }
            }
        }
    }

    public static class ReaderExtensions
    {
        public static decimal GetDecimalNullIsZero(this SqlDataReader reader, int idx)
        {
            return reader.IsDBNull(idx) ? 0m : reader.GetDecimal(idx);
        }

        public static int GetInt32NullIsZero(this SqlDataReader reader, int idx)
        {
            return reader.IsDBNull(idx) ? 0 : reader.GetInt32(idx);
        }

        public static long GetInt64NullIsZero(this SqlDataReader reader, int idx)
        {
            return reader.IsDBNull(idx) ? 0 : reader.GetInt64(idx);
        }
    }
}
