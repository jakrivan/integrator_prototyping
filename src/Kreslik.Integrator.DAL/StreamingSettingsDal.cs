﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.DAL
{
    public class StreamingSettingsDal : IStreamingSettings
    {
        //private TimeSpan[] _maxLLTimesInIntegrator = new TimeSpan[Counterparty.ValuesCount];

        private TimeSpan[] _maxLLTimesOnDestination = new TimeSpan[Counterparty.ValuesCount];
        private TimeSpan[] _expectedRTHalfTime = new TimeSpan[Counterparty.ValuesCount];
        private TimeSpan[] _maxLLTimesInIntegrator = new TimeSpan[Counterparty.ValuesCount];

        public StreamingSettingsDal()
        {
            this.ReadSettings();
        }

        private void ReadSettings()
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand command = new SqlCommand("[dbo].[GetStreamingSettingsPerCounterparty_SP]", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Counterparty ctp = (Counterparty)reader.GetString(reader.GetOrdinal("Counterparty"));
                            _maxLLTimesOnDestination[(int)ctp] =
                                TimeSpan.FromMilliseconds(
                                    reader.GetInt16(reader.GetOrdinal("MaxLLTimeOnDestination_milliseconds")));
                        }
                    }
                }
            }

            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand command = new SqlCommand("[dbo].[GetDelaySettings_SP]", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Counterparty ctp = (Counterparty)reader.GetString(reader.GetOrdinal("Counterparty"));

                            _expectedRTHalfTime[(int)ctp] = 
                                TimeSpan.FromTicks(reader.GetInt64(reader.GetOrdinal("ExpectedDelayTicks")));
                            _maxLLTimesInIntegrator[(int) ctp] =
                                TimeSpan.FromTicks(_maxLLTimesOnDestination[(int) ctp].Ticks -
                                                   2*_expectedRTHalfTime[(int) ctp].Ticks);
                        }
                    }
                }
            }
        }

        public TimeSpan GetMaxLLTimeInIntegrator(Counterparty counterparty)
        {
            return this._maxLLTimesInIntegrator[(int) counterparty];
        }

        public TimeSpan GetMaxLLTimeOnDestiantion(Counterparty counterparty)
        {
            return this._maxLLTimesOnDestination[(int)counterparty];
        }

        public TimeSpan GetExpectedSendingTime(Counterparty counterparty)
        {
            return this._expectedRTHalfTime[(int)counterparty];
        }
    }
}
