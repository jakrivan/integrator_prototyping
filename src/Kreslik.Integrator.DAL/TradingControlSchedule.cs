﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.DAL
{
    public class TradingControlScheduleSettingEntry
    {
        public TradingControlScheduleSettingEntry(int entryId, bool isDaily, bool isWeekly, bool isImmediatelyOnce, SystemsTradingControlScheduleAction action,
                                                  DateTime scheduleTimeZoneSpecific, TimeZoneCode timeZoneCode, string description)
        {
            this.EntryId = entryId;
            this.IsDaily = isDaily;
            this.IsWeekly = isWeekly;
            this.IsImmediatelyOnce = isImmediatelyOnce;
            this.Action = action;
            this.ScheduleTimeZoneSpecific = scheduleTimeZoneSpecific;
            this.TimeZoneCode = timeZoneCode;
            this.Description = description;
        }

        public int EntryId { get; private set; }
        public bool IsDaily { get; private set; }
        public bool IsWeekly { get; private set; }
        public bool IsImmediatelyOnce { get; private set; }

        private ScheduleTiming CurrentScheduleTiming
        {
            get
            {
                if (this.IsDaily)
                    return ScheduleTiming.Daily;
                if (this.IsWeekly)
                    return ScheduleTiming.Weekly;
                return ScheduleTiming.ExactTime;
            }
        }

        //public bool TurnOnGoFlatOnly { get; private set; }
        public SystemsTradingControlScheduleAction Action { get; private set; }
        public TimeZoneCode TimeZoneCode { get; private set; }
        public DateTime ScheduleTimeZoneSpecific { get; private set; }

        public string Description { get; private set; }

        private string GetTimeString()
        {
            if (IsDaily)
                return this.ScheduleTimeZoneSpecific.ToString("hh:mm:ss");
            if(IsWeekly)
                return this.ScheduleTimeZoneSpecific.ToString("dddd hh:mm:ss");
            if (IsImmediatelyOnce)
                return "Immediately";

            return this.ScheduleTimeZoneSpecific.ToString("yyyy-MM-dd hh:mm:ss");
        }

        private enum ScheduleTiming
        {
            Daily,
            Weekly,
            ExactTime
        }

        public override string ToString()
        {
            return string.Format("ScheduleEntry[{0}]: {1} at {2}[{3}] ({4}). Description: {5}",
                this.EntryId, this.Action, this.GetTimeString(), this.TimeZoneCode, this.CurrentScheduleTiming,
                this.Description);
        }
    }

    public interface ITradingControlSchedule
    {
        event Action<TradingControlScheduleSettingEntry> TradingControlScheduleOccured;
    }

    public class TradingControlSchedule : ITradingControlSchedule
    {
        private readonly TimeSpan _pollInterval = TimeSpan.FromSeconds(2);
        //limitation of System.Threading.Timer
        private readonly TimeSpan _maxTimerTimespan = TimeSpan.FromMilliseconds(int.MaxValue);
        private DateTime _lastTradingScheduleSettingsUpdateTimeUtc = DateTime.MinValue;
        private DateTime _nextFullRefresh;
        private ILogger _logger;
        private SafeTimer _pollTimer;
        private List<SafeTimer<Tuple<TradingControlScheduleSettingEntry, SafeTimerBase>>> _schedules; 

        public TradingControlSchedule(ILogger logger)
        {
            this._logger = logger;
            _nextFullRefresh = HighResolutionDateTime.UtcNow + _maxTimerTimespan;
            _pollTimer = new SafeTimer(PollChanges, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2), true)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1sec,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
        }

        public event Action<TradingControlScheduleSettingEntry> TradingControlScheduleOccured;

        private void InvokeTradingControlSchedule(Tuple<TradingControlScheduleSettingEntry, SafeTimerBase> state)
        {
            TradingControlScheduleSettingEntry tradingControlScheduleSettingEntry = state.Item1;
            SafeTimerBase scheduleTimer = state.Item2;

            if (TradingControlScheduleOccured != null)
                TradingControlScheduleOccured(tradingControlScheduleSettingEntry);

            if (tradingControlScheduleSettingEntry.IsDaily || tradingControlScheduleSettingEntry.IsWeekly)
            {
                this.RescheduleTimer(tradingControlScheduleSettingEntry, scheduleTimer, false);
            }
        }

        private void PollChanges()
        {
            //workaround for inability to accept timers for longer than 30 days - refresh full list every 30 days
            if (HighResolutionDateTime.UtcNow > _nextFullRefresh)
            {
                _nextFullRefresh = HighResolutionDateTime.UtcNow + _maxTimerTimespan;
                this._lastTradingScheduleSettingsUpdateTimeUtc = DateTime.MinValue;
            }

            DateTime lastTradingScheduleSettingsUpdateTimeUtc = this._lastTradingScheduleSettingsUpdateTimeUtc;
            var updatesList = this.GetUpdatedTradingScheduleSettings(lastTradingScheduleSettingsUpdateTimeUtc);
            //prevent unnecesary clearings of timers if there was no change
            if (this._lastTradingScheduleSettingsUpdateTimeUtc > lastTradingScheduleSettingsUpdateTimeUtc ||
                updatesList.Count > 0)
            {
                this.ProcessSettings(updatesList);
            }
        }

        private void RescheduleTimer(TradingControlScheduleSettingEntry tradingControlScheduleSettingEntry,
            SafeTimerBase scheduleTimer, bool firstSchedule)
        {
            if(!firstSchedule && !tradingControlScheduleSettingEntry.IsDaily && !tradingControlScheduleSettingEntry.IsWeekly)
                return;

            TimeZoneInfo targetTimeZoneInfo =
                TradingHoursHelper.Instance.GetTimeZoneInfo(tradingControlScheduleSettingEntry.TimeZoneCode);

            DateTime targetZoneNow = TimeZoneInfo.ConvertTimeFromUtc(HighResolutionDateTime.UtcNow, targetTimeZoneInfo);
            DateTime startTime;

            if (tradingControlScheduleSettingEntry.IsDaily)
            {
                startTime = targetZoneNow.Date +
                                         tradingControlScheduleSettingEntry.ScheduleTimeZoneSpecific.TimeOfDay;
                //reoccurring event should execute next day, same as event that is already in past
                if (!firstSchedule || startTime < targetZoneNow)
                    startTime = startTime.AddDays(1);
            }
            else if (tradingControlScheduleSettingEntry.IsWeekly)
            {
                DateTime targetZoneToday = targetZoneNow.Date;

                DateTime weekStart = targetZoneToday.AddDays(-(int) targetZoneToday.DayOfWeek);

                startTime =
                    weekStart.AddDays((int) tradingControlScheduleSettingEntry.ScheduleTimeZoneSpecific.DayOfWeek) +
                    tradingControlScheduleSettingEntry.ScheduleTimeZoneSpecific.TimeOfDay;
                if (!firstSchedule || startTime < targetZoneNow)
                    startTime = startTime.AddDays(7);
            }
            else
            {
                startTime = tradingControlScheduleSettingEntry.ScheduleTimeZoneSpecific;
            }

            TimeSpan nextDue;
            if (tradingControlScheduleSettingEntry.IsImmediatelyOnce)
                nextDue = TimeSpan.FromMilliseconds(1);
            else
                nextDue = startTime - targetZoneNow;

            if (nextDue > TimeSpan.Zero
                && nextDue < _maxTimerTimespan)
            {
                scheduleTimer.Change(nextDue, Timeout.InfiniteTimeSpan);
            }
        }

        private void ProcessSettings(List<TradingControlScheduleSettingEntry> settingsList)
        {
            List<SafeTimer<Tuple<TradingControlScheduleSettingEntry, SafeTimerBase>>> schedulesLocal = new List<SafeTimer<Tuple<TradingControlScheduleSettingEntry, SafeTimerBase>>>();

            foreach (TradingControlScheduleSettingEntry tradingControlScheduleSettingEntry in settingsList)
            {
                SafeTimer<Tuple<TradingControlScheduleSettingEntry, SafeTimerBase>> scheduleTimer =
                    new SafeTimer<Tuple<TradingControlScheduleSettingEntry, SafeTimerBase>>(InvokeTradingControlSchedule);
                scheduleTimer.State =
                    new Tuple<TradingControlScheduleSettingEntry, SafeTimerBase>(tradingControlScheduleSettingEntry,
                        scheduleTimer);

                this.RescheduleTimer(tradingControlScheduleSettingEntry, scheduleTimer, true);

                schedulesLocal.Add(scheduleTimer);
            }

            if (_schedules != null)
            {
                foreach (SafeTimer<Tuple<TradingControlScheduleSettingEntry, SafeTimerBase>> safeTimer in _schedules)
                {
                    safeTimer.Dispose();
                }
            }

            Interlocked.Exchange(ref this._schedules, schedulesLocal);
        }

        private List<TradingControlScheduleSettingEntry> GetUpdatedTradingScheduleSettings(DateTime lastUpdateTimeUtc)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getTradingScheduleSettingsCommand = new SqlCommand("[dbo].[GetUpdatedSystemsTradingControlSchedule_SP]",
                                                                             sqlConnection))
                {
                    getTradingScheduleSettingsCommand.CommandType = CommandType.StoredProcedure;
                    getTradingScheduleSettingsCommand.Parameters.Add("@LastUpdateTimeUtc", SqlDbType.DateTime2).Value =
                        lastUpdateTimeUtc;

                    SqlParameter lastUpdateTimeOutParam = new SqlParameter("@LastUpdateTimeServerSideUtc",
                                                                           SqlDbType.DateTime2)
                    {
                        Direction = ParameterDirection.Output
                    };
                    getTradingScheduleSettingsCommand.Parameters.Add(lastUpdateTimeOutParam);

                    List<TradingControlScheduleSettingEntry> tradingScheduleSettingsList = new List<TradingControlScheduleSettingEntry>();

                    using (var reader = getTradingScheduleSettingsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            //ignore disabled records
                            if (reader.GetBoolean(reader.GetOrdinal("IsEnabled")))
                            {
                                tradingScheduleSettingsList
                                    .Add(new TradingControlScheduleSettingEntry(
                                             entryId: reader.GetInt32(reader.GetOrdinal("ScheduleEntryId")),
                                             isDaily: reader.GetBoolean(reader.GetOrdinal("IsDaily")),
                                             isWeekly: reader.GetBoolean(reader.GetOrdinal("IsWeekly")),
                                             isImmediatelyOnce: reader.GetBoolean(reader.GetOrdinal("IsImmediatelyOnce")),
                                             action: (SystemsTradingControlScheduleAction) Enum.Parse(typeof(SystemsTradingControlScheduleAction), reader.GetString(reader.GetOrdinal("ActionName"))),
                                             scheduleTimeZoneSpecific: reader.GetDateTime(reader.GetOrdinal("ScheduleTimeZoneSpecific")),
                                             timeZoneCode: (TimeZoneCode) Enum.Parse(typeof(TimeZoneCode), reader.GetString(reader.GetOrdinal("TimeZoneCode"))),
                                             description: reader.GetString(reader.GetOrdinal("ScheduleDescription"))
                                             ));
                            }
                        }
                    }

                    var val = lastUpdateTimeOutParam.Value;

                    if (val != null && val != DBNull.Value)
                    {
                        _lastTradingScheduleSettingsUpdateTimeUtc = (DateTime)val;
                    }

                    return tradingScheduleSettingsList;
                }
            }
        }

    }
}
