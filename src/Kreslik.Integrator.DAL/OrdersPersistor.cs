﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.DAL
{
    public interface IOrdersPersistor
    {
        void PersistNewExternalOrder(IIntegratorOrderExternal integratorOrderExternal);
        void PersistNewRejectableDeal(RejectableMMDeal rejectableDeal);
        void PersistNewCounterpartyTimeoutInfo(CounterpartyTimeoutInfo counterpartyTimeoutInfo);
        RejectableMMDeal RejectExecutedRejectableDeal(string integratorExecutionId, string counterpartyExecutionId, string rejectionReason);
        void PersistExecutionReport(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal);
        void PersistExternalExecutedDeal(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal, decimal termCurrencyUsdConversionMultiplier);
        void PersistExternalRejectedDeal(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal);
        void PersistOrderExternalCancelRequest(ExternalCancelRequest externalCancelRequest, IIntegratorOrderExternal integratorOrderExternal);
        void PersistOrderExternalCancelResult(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal);
        void PersistOrderExternalIgnored(DateTime ignoreDetectedUtc, IIntegratorOrderExternal integratorOrderExternal);
        void PersistUnexpectedExecution(OrderChangeInfo orderChangeInfo, Counterparty counterparty);
        void PersistNewExternalDealExecutedTicket(TradeReportTicketInfo tradeReportTicketInfo);
        void PersistNewAggregatedTradeTicketInfo(AggregatedTradeTicketInfo aggregatedTradeTicketInfo);
    }

    public static class SqlParameterCollectionExtensions
    {
        public static SqlParameter AddWithNullValue(this SqlParameterCollection sqlParameterCollection, string parameterName, object value)
        {
            return sqlParameterCollection.AddWithValue(parameterName, value ?? DBNull.Value);
        }

        public static int? ToNullableInt32(this SqlInt32 value)
        {
            return value.IsNull ? (int?)null : value.Value;
        }

        public static int? GetNullableInt32(this SqlDataReader reader, int ordinalId)
        {
            return reader.GetSqlInt32(ordinalId).ToNullableInt32();
        }

        public static string GetNullableString(this SqlDataReader reader, int ordinalId)
        {
            return reader.IsDBNull(ordinalId) ? null : reader.GetString(ordinalId);
        }

        public static decimal? GetNullableDecimal(this SqlDataReader reader, int ordinalId)
        {
            return reader.IsDBNull(ordinalId) ? (decimal?) null : reader.GetDecimal(ordinalId);
        }
    }

    public static class AdoNetHelpers
    {
        public static void LogProcedureException(ILogger logger, Exception e, string operationDescription)
        {
            if (e is SqlException)
            {
                logger.LogException(LogLevel.Error, e, "Got SqlException when executing {0}", operationDescription);
            }
            else if((e is InvalidOperationException) && e.Message.Contains("Timeout expired"))
            {
                logger.LogException(LogLevel.Error, e,
                                         "Got Timeout Expired execption when executing {0}", operationDescription);
            }
            else
            {
                logger.LogException(LogLevel.Fatal, e,
                                         "Got unexpected Exception when executing {0}", operationDescription);
            }
        }

        public static bool IsRecoverableSqlException(Exception e)
        {
            return ((e is SqlException) && !e.Message.Contains("Cannot insert duplicate")) || ((e is InvalidOperationException) && e.Message.Contains("Timeout expired"));
        }

        private static TimeSpan[] _retryIntervals = new TimeSpan[] { TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(30), TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(10) };

        public static bool RetrySqlOperation(Action sqlOperation, string operationName, ILogger logger, int? maxRetries, bool retryOnNonSqlErrors)
        {
            bool @continue = true;
            int attempts = 0;
            do
            {
                try
                {
                    sqlOperation();
                    return true;
                }
                catch (Exception e)
                {
                    LogProcedureException(logger, e, operationName);

                    //if (command != null)
                    //{
                    //    logger.Log(LogLevel.Warn,
                    //                "Experienced error during executing sp - it might be executed by running: [{0}]",
                    //                command.GetSpExecuteString());
                    //}

                    @continue =
                        //are we allowed to recover from this error
                        (retryOnNonSqlErrors || IsRecoverableSqlException(e))
                        &&
                        //do we have retries remaining
                        (!maxRetries.HasValue || attempts++ < maxRetries);

                    if (@continue)
                    {
                        System.Threading.Thread.Sleep(_retryIntervals[Math.Min(attempts - 1, _retryIntervals.Length - 1)]);
                    }
                }

            } while (@continue);

            return false;
        }
    }

    public static class  SqlCommandExtensions
    {
        public static string GetSpExecuteString(this SqlCommand sqlCommand)
        {
            string result = null;

            try
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("exec {0} ", sqlCommand.CommandText);

                foreach (SqlParameter parameter in sqlCommand.Parameters)
                {
                    if (parameter.Value == DBNull.Value)
                    {
                        sb.AppendFormat(" {0} = NULL,", parameter.ParameterName);
                    }
                    else
                    {
                        sb.AppendFormat(" {0} = '{1}',", parameter.ParameterName, parameter.Value);
                    }
                }

                result = sb.ToString(0, sb.Length - 1);
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            return result;
        }
    }

    public class OrdersPersistor : IOrdersPersistor
    {
        private ILogger _logger;
        private const int MAX_ATTEMPTS = 20;
        private TimeSpan[] _retryIntervals = new TimeSpan[] { TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(30), TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(10) };

        public OrdersPersistor(ILogger logger)
        {
            this._logger = logger;
        }

        public void PersistNewExternalOrder(IIntegratorOrderExternal integratorOrderExternal)
        {
            //this.DataPersistTask(() => CreateCommandForNewOrderPersisting(integratorOrderExternal),
            //                     string.Format("New External Order: [{0}], [{1}]", integratorOrderExternal.Identity,
            //                                   integratorOrderExternal.UniqueInternalIdentity));

            this.LogDataOnlyWithoutDbCall(() => CreateCommandForNewOrderPersisting(integratorOrderExternal));
        }

        public void PersistNewRejectableDeal(RejectableMMDeal rejectableDeal)
        {
            this.DataPersistTask(
                () => CreateCommandForNewRejectableDealPersisting(rejectableDeal),
                string.Format("Rejectable deal: [{0}]", rejectableDeal.ExecutionId));  
        }

        public void PersistNewCounterpartyTimeoutInfo(CounterpartyTimeoutInfo counterpartyTimeoutInfo)
        {
            this.DataPersistTask(
                () => CreateCommandForNewCounterpartyTimeoutPersisting(counterpartyTimeoutInfo),
                string.Format("Counterparty timeout: [{0}]", counterpartyTimeoutInfo.CounterpartyClientOrderId));
        }

        public void PersistExecutionReport(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal)
        {
            //this.DataPersistTask(() => CreateCommandForExecutionReportPersisting(orderChangeInfo, integratorOrderExternal),
            //                     string.Format("Execution Report ({0}) for: [{1}], [{2}]", orderChangeInfo.ChangeEventArgs.ChangeState,
            //                                   orderChangeInfo.Identity, orderChangeInfo.UniqueInternalIdentity));

            this.LogDataOnlyWithoutDbCall(() => CreateCommandForExecutionReportPersisting(orderChangeInfo, integratorOrderExternal));
        }

        public void PersistExternalExecutedDeal(OrderChangeInfo orderChangeInfo,
                                                IIntegratorOrderExternal integratorOrderExternal, decimal termCurrencyUsdConversionMultiplier)
        {
            this.DataPersistTask(
                () => CreateCommandForExecutedDealPersisting(orderChangeInfo, integratorOrderExternal, termCurrencyUsdConversionMultiplier),
                string.Format("Executed External Order: [{0}], [{1}]", integratorOrderExternal.Identity,
                              integratorOrderExternal.UniqueInternalIdentity));
        }

        public void PersistExternalRejectedDeal(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal)
        {
            this.DataPersistTask(
                () => CreateCommandForRejectedDealPersisting(orderChangeInfo, integratorOrderExternal),
                string.Format("Rejected External Order: [{0}], [{1}]", integratorOrderExternal.Identity,
                              integratorOrderExternal.UniqueInternalIdentity));         
        }

        public void PersistOrderExternalCancelRequest(ExternalCancelRequest externalCancelRequest, IIntegratorOrderExternal integratorOrderExternal)
        {
            //this.DataPersistTask(
            //    () => CreateCommandForCancelRequestPersisting(externalCancelRequest, integratorOrderExternal),
            //    string.Format("External Order Cancel request: [{0}]", integratorOrderExternal == null ? "--noId--" : integratorOrderExternal.UniqueInternalIdentity));

            this.LogDataOnlyWithoutDbCall(() => CreateCommandForCancelRequestPersisting(externalCancelRequest, integratorOrderExternal));
        }

        public void PersistOrderExternalCancelResult(OrderChangeInfo orderChangeInfo,
                                                     IIntegratorOrderExternal integratorOrderExternal)
        {
            //this.DataPersistTask(
            //    () => CreateCommandForCancelResultPersisting(orderChangeInfo, integratorOrderExternal),
            //    string.Format("External Order Cancel response: [{0}], [{1}]", integratorOrderExternal.Identity,
            //                  integratorOrderExternal.UniqueInternalIdentity));

            this.LogDataOnlyWithoutDbCall(() => CreateCommandForCancelResultPersisting(orderChangeInfo, integratorOrderExternal));
        }

        public void PersistOrderExternalIgnored(DateTime ignoreDetectedUtc, IIntegratorOrderExternal integratorOrderExternal)
        {
            this.DataPersistTask(
                () => CreateCommandForIgnoredOrdersPersisting(ignoreDetectedUtc, integratorOrderExternal),
                string.Format("External Order Ignored: [{0}], [{1}]", integratorOrderExternal.Identity,
                              integratorOrderExternal.UniqueInternalIdentity));
        }

        public void PersistUnexpectedExecution(OrderChangeInfo orderChangeInfo, Counterparty counterparty)
        {
            this.DataPersistTask(
                () => CreateCommandForUnexpectedExecutionPersisting(orderChangeInfo, counterparty),
                string.Format("Unexpected execution: [{0}], [{1}]", orderChangeInfo,
                              counterparty));
        }

        

        public void PersistNewExternalDealExecutedTicket(TradeReportTicketInfo tradeReportTicketInfo)
        {
            this.DataPersistTask(
                () => CreateCommandForNewExternalDealExecutedTicketPersisting(tradeReportTicketInfo),
                string.Format("Trade report [{0}] for: [{1}]", tradeReportTicketInfo.TradeReportTicketInfoType,
                              tradeReportTicketInfo.IntegratorDealId));
        }

        public void PersistNewAggregatedTradeTicketInfo(AggregatedTradeTicketInfo aggregatedTradeTicketInfo)
        {
            this.DataPersistTask(
                () => CreateCommandForNewAggregatedTradeTicketInfoPersisting(aggregatedTradeTicketInfo),
                "Aggregated trade report");
        }

        private void LogDataOnlyWithoutDbCall(Func<SqlCommand> commandBuilder)
        {
            //no need to log those - just a waste of resources
            return;

            try
            {
                using (SqlCommand insertDataCommand = commandBuilder())
                {
                    _logger.Log(LogLevel.Info,
                            "Logging the SP only (not invoking DB call) - it might be executed by running: [{0}]",
                            insertDataCommand.GetSpExecuteString());
                }

            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, "Exception during building SP, with no backend call", e);
            }
        }

        public RejectableMMDeal RejectExecutedRejectableDeal(string integratorExecutionId, string counterpartyExecutionId, string rejectionReason)
        {

            for (int attempt = 0; attempt < 10; attempt++)
            {
                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                    {
                        sqlConnection.Open();
                        using (
                            SqlCommand updateExecutionCommand =
                                new SqlCommand("[dbo].[RejectExecutedOrderExternalIncomingRejectable_SP]", sqlConnection))
                        {
                            updateExecutionCommand.CommandType = CommandType.StoredProcedure;
                            updateExecutionCommand.Parameters.AddWithValue("@IntegratorExecId", integratorExecutionId);
                            updateExecutionCommand.Parameters.AddWithValue("@CounterpartyExecId", counterpartyExecutionId);
                            updateExecutionCommand.Parameters.AddWithValue("@RejectionReason", rejectionReason);


                            using (var reader = updateExecutionCommand.ExecuteReader())
                            {
                                if (reader.Read())
                                {
                                    return RejectableMMDeal.CreateStoredRejectableMMDeal(
                                        (Symbol) reader.GetString(reader.GetOrdinal("Symbol")),
                                        (Counterparty) reader.GetString(reader.GetOrdinal("Counterparty")),
                                        (DealDirection)Enum.Parse(typeof(DealDirection), reader.GetString(reader.GetOrdinal("DealDirection"))),
                                        reader.GetDecimal(reader.GetOrdinal("CounterpartyRequestedPrice")),
                                        Math.Abs(reader.GetDecimal(reader.GetOrdinal("OriginallyExecutedAmountBasePol"))),
                                        reader.GetString(reader.GetOrdinal("CounterpartyClientOrderId")),
                                        reader.GetString(reader.GetOrdinal("SourceIntegratorPriceIdentity")),
                                        reader.GetDateTime(reader.GetOrdinal("CounterpartySentExternalOrderUtc")),
                                        reader.GetDateTime(reader.GetOrdinal("IntegratorReceivedExternalOrderUtc")),
                                        reader.GetNullableString(reader.GetOrdinal("IntegratorExecutionId")),
                                        reader.GetNullableString(reader.GetOrdinal("CounterpartyExecutionId")),
                                        reader.GetDateTime(reader.GetOrdinal("ValueDate")));
                                }
                                else
                                {
                                    this._logger.Log(LogLevel.Error, "Couldn't revert timeouted deal [{0},{1}] and obtain originally executed position for it as it might not have been yet inserted", integratorExecutionId, counterpartyExecutionId);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Cannot revert timeouted deal {0},{1} due to exception", integratorExecutionId, counterpartyExecutionId);
                    Thread.Sleep(1000 * (attempt + 1));
                }
            }

            this._logger.Log(LogLevel.Fatal, "Couldn't revert timeouted deal [{0},{1}] and obtain originally executed position for it", integratorExecutionId, counterpartyExecutionId);

            return null;
        }

        private Task DataPersistTask(Func<SqlCommand> commandBuilder, string dataDescription)
        {
            //let's first try synchronously
            //if (TryPersistData(commandBuilder, dataDescription))
            //    return;

            return Task.Factory.StartNew(()
                                  =>
            {
                PersistTaskResult result;
                int attempts = 0;

                do
                {
                    attempts++;
                    result = TryPersistData(commandBuilder, dataDescription);

                    if (result != PersistTaskResult.Success)
                    {
                        //half hour is in idx 8, 11, 14 ... (as for sleep we decrement 1 and also we check here before sleep)
                        if (attempts > 7 && (attempts+1)%3 == 0)
                        {
                            _logger.Log(LogLevel.Fatal, "Unsuccessfuly attempting for (another) half an hour to inserting {0}", dataDescription);
                            if(result != PersistTaskResult.RecoverableFailure)
                                break;
                        }

                        System.Threading.Thread.Sleep(_retryIntervals[Math.Min(attempts - 1, _retryIntervals.Length - 1)]);
                    }
                } while (result != PersistTaskResult.Success);

                if (result != PersistTaskResult.Success)
                {
                    _logger.Log(LogLevel.Fatal, "Gave up attempts for inserting {0}", dataDescription);
                }
            });
        }

        private enum PersistTaskResult
        {
            Success,
            RecoverableFailure,
            Failure
        }

        private PersistTaskResult TryPersistData(Func<SqlCommand> commandBuilder, string dataDescription)
        {
            SqlCommand insertDataCommand = null;
            PersistTaskResult result = PersistTaskResult.Failure;

            try
            {
                using (
                    SqlConnection sqlConnection =
                        new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString)
                    )
                {
                    sqlConnection.Open();
                    insertDataCommand = commandBuilder();
                    insertDataCommand.Connection = sqlConnection;
                    insertDataCommand.ExecuteNonQuery();
                    insertDataCommand.Dispose();
                    return PersistTaskResult.Success;
                }
            }
            catch (Exception e)
            {
                AdoNetHelpers.LogProcedureException(this._logger, e, dataDescription);
                if(AdoNetHelpers.IsRecoverableSqlException(e))
                    result = PersistTaskResult.RecoverableFailure;
            }

            if (insertDataCommand != null)
            {
                _logger.Log(LogLevel.Warn,
                            "Experienced error during executing sp - it might be executed by running: [{0}]",
                            insertDataCommand.GetSpExecuteString());
                insertDataCommand.Dispose();
            }

            return result;
        }

        private SqlCommand CreateCommandForNewOrderPersisting(IIntegratorOrderExternal integratorOrderExternal)
        {
            SqlCommand insertOrderCommand = new SqlCommand("[dbo].[InsertNewOrderExternal_SP]")
            {
                CommandType = CommandType.StoredProcedure
            };

            insertOrderCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParam = insertOrderCommand.Parameters.Add("@ExternalOrderSentUtc", SqlDbType.DateTime2);
            sqlParam.Value = integratorOrderExternal.IntegratorSentTimeUtc;
            insertOrderCommand.Parameters.AddWithValue("@InternalOrderId", integratorOrderExternal.UniqueInternalIdentity);
            insertOrderCommand.Parameters.AddWithValue("@InternalClientOrderId", integratorOrderExternal.Identity);
            insertOrderCommand.Parameters.AddWithValue("@CounterpartyCode",
                                                       integratorOrderExternal.Counterparty.ToString());
            insertOrderCommand.Parameters.AddWithValue("@Symbol",
                                                       integratorOrderExternal.OrderRequestInfo.Symbol.ToString());
            insertOrderCommand.Parameters.AddWithValue("@DealDirection",
                                                       integratorOrderExternal.OrderRequestInfo.Side.ToString());

            if (integratorOrderExternal.QuoteReceivedToOrderSentInternalLatency.HasValue)
            {
                TimeSpan quoteReceivedToOrderSentInternalLatency =
                integratorOrderExternal.QuoteReceivedToOrderSentInternalLatency.Value;
                if (quoteReceivedToOrderSentInternalLatency < TimeSpan.Zero)
                {
                    this._logger.Log(LogLevel.Error,
                                     "Order [{0}], [{1}] has negative quote received to order sent latency! Setting it to zero",
                                     integratorOrderExternal.Identity, integratorOrderExternal.UniqueInternalIdentity);
                    quoteReceivedToOrderSentInternalLatency = TimeSpan.Zero;
                }
                insertOrderCommand.Parameters.AddWithValue("@QuoteReceivedToOrderSentInternalLatency",
                                                           quoteReceivedToOrderSentInternalLatency);
            }
            else
            {
                insertOrderCommand.Parameters.AddWithValue("@QuoteReceivedToOrderSentInternalLatency", DBNull.Value);
            }

            insertOrderCommand.Parameters.AddWithValue("@FixMessageSentRaw",
                                                       integratorOrderExternal.OutgoingFixMessageRaw);
            insertOrderCommand.Parameters.AddWithValue("@OrderTypeName",
                                                       integratorOrderExternal.OrderRequestInfo.OrderType.ToString());
            insertOrderCommand.Parameters.AddWithValue("@OrderTimeInForceName",
                                                       integratorOrderExternal.OrderRequestInfo.TimeInForce.ToString());
            insertOrderCommand.Parameters.AddWithValue("@RequestedPrice",
                                                       integratorOrderExternal.OrderRequestInfo.RequestedPrice);
            insertOrderCommand.Parameters.AddWithValue("@RequestedAmountBaseAbs",
                                                       integratorOrderExternal.OrderRequestInfo.SizeBaseAbsInitial);

            //Limit orders doesn't have price identity
            if (integratorOrderExternal.OrderRequestInfo.IntegratorPriceIdentity != Guid.Empty)
            {
                insertOrderCommand.Parameters.AddWithValue("@SourceIntegratorPriceIdentity",
                    integratorOrderExternal.OrderRequestInfo.IntegratorPriceIdentity);

                SqlParameter sqlPriceStampParam = insertOrderCommand.Parameters.Add("@SourceIntegratorPriceTimeUtc", SqlDbType.DateTime2);
                sqlPriceStampParam.Value = integratorOrderExternal.OrderRequestInfo.IntegratorPriceReceivedUtc;
            }
            else
            {
                insertOrderCommand.Parameters.AddWithValue("@SourceIntegratorPriceIdentity", DBNull.Value);
                insertOrderCommand.Parameters.AddWithValue("@SourceIntegratorPriceTimeUtc", DBNull.Value);
            }

            if (integratorOrderExternal.OrderRequestInfo.OrderType == OrderType.Pegged)
            {
                insertOrderCommand.Parameters.AddWithValue("@PegTypeName",
                                                           integratorOrderExternal.OrderRequestInfo.PegType.ToString());
                insertOrderCommand.Parameters.AddWithValue("@PegDifferenceBasePol",
                                                           integratorOrderExternal.OrderRequestInfo
                                                                                  .PegDifferenceBasePolAlwaysAdded);
            }

            return insertOrderCommand;
        }

        private SqlCommand CreateCommandForNewRejectableDealPersisting(RejectableMMDeal rejectableDeal)
        {
            SqlCommand insertRejectableDealCommand = new SqlCommand("[dbo].[InsertNewOrderExternalIncomingRejectable_SP]")
            {
                CommandType = CommandType.StoredProcedure
            };

            insertRejectableDealCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParam1 = insertRejectableDealCommand.Parameters.Add("@ExternalOrderReceived", SqlDbType.DateTime2);
            sqlParam1.Value = rejectableDeal.IntegratorReceivedCounterpartyOrderTimeUtc;
            SqlParameter sqlParam2 = insertRejectableDealCommand.Parameters.Add("@ExternalOrderSentUtc", SqlDbType.DateTime2);
            sqlParam2.Value = rejectableDeal.CounterpartySentDealRequestTimeUtc;
            SqlParameter sqlParam3 = insertRejectableDealCommand.Parameters.Add("@IntegratorSentExecutionReportUtc", SqlDbType.DateTime2);
            sqlParam3.Value = rejectableDeal.IntegratorSentResponseTimeUtc;
            insertRejectableDealCommand.Parameters.AddWithValue("@CounterpartyClientOrderId", rejectableDeal.CounterpartyClientOrderIdentifier);
            insertRejectableDealCommand.Parameters.AddWithNullValue("@CounterpartyClientId", rejectableDeal.CounterpartyClientId);
            

            if (rejectableDeal.IntegratedTradingSystemIdentification != null)
            {
                insertRejectableDealCommand.Parameters.AddWithValue("@TradingSystemId",
                    rejectableDeal.IntegratedTradingSystemIdentification.IntegratedTradingSystemIdentity);
            }
            if (!string.IsNullOrEmpty(rejectableDeal.IntegratorQuoteIdentifier))
            {
                insertRejectableDealCommand.Parameters.AddWithValue("@SourceIntegratorPriceIdentity",
                    rejectableDeal.IntegratorQuoteIdentifier);
            }
            insertRejectableDealCommand.Parameters.AddWithValue("@CounterpartyCode", rejectableDeal.Counterparty.ToString());
            insertRejectableDealCommand.Parameters.AddWithValue("@Symbol", rejectableDeal.Symbol.ToString());
            insertRejectableDealCommand.Parameters.AddWithValue("@DealDirection", rejectableDeal.IntegratorDealDirection.ToString());
            insertRejectableDealCommand.Parameters.AddWithValue("@RequestedPrice", rejectableDeal.CounterpartyRequestedPrice);
            insertRejectableDealCommand.Parameters.AddWithValue("@RequestedAmountBaseAbs", rejectableDeal.CounterpartyRequestedSizeBaseAbs);
            insertRejectableDealCommand.Parameters.AddWithValue("@RequestedFillMinimumBaseAbs", rejectableDeal.CounterpartyRequestedSizeBaseAbsFillMinimum);
            if(!string.IsNullOrEmpty(rejectableDeal.IntegratorExecutionId))
                insertRejectableDealCommand.Parameters.AddWithValue("@IntegratorExecId", rejectableDeal.IntegratorExecutionId);
            if(!string.IsNullOrEmpty(rejectableDeal.CounterpartyExecutionId))
                insertRejectableDealCommand.Parameters.AddWithValue("@CounterpartyExecId", rejectableDeal.CounterpartyExecutionId);
            if (rejectableDeal.SettlementDateLocal != DateTime.MinValue)
            {
                insertRejectableDealCommand.Parameters.AddWithValue("@ValueDate", rejectableDeal.SettlementDateLocal);
                insertRejectableDealCommand.Parameters.AddWithValue("@SettlementTimeCounterpartyUtc", rejectableDeal.SettlementDateTimeCounterpartyUtc);
                insertRejectableDealCommand.Parameters.AddWithValue("@SettlementTimeCentralBankUtc", rejectableDeal.SettlementDateTimeCentralBankUtc);
            }



            insertRejectableDealCommand.Parameters.AddWithValue("@OrderReceivedToExecutionReportSentLatency",
                rejectableDeal.IntegratorSentResponseTimeUtc - rejectableDeal.IntegratorReceivedCounterpartyOrderTimeUtc);

            insertRejectableDealCommand.Parameters.AddWithValue("@IntegratorExecutedAmountBaseAbs", rejectableDeal.FilledAmountBaseAbs);
            insertRejectableDealCommand.Parameters.AddWithValue("@IntegratorRejectedAmountBaseAbs", rejectableDeal.IntegratorRejectedAmountBaseAbs);

            if (rejectableDeal.DealRejected)
            {
                insertRejectableDealCommand.Parameters.AddWithValue("@RejectionReason", ((RejectableMMDeal.DealRejectionReason)rejectableDeal.CurrentDealStatus).ToString());
            }

            return insertRejectableDealCommand;
        }

        private SqlCommand CreateCommandForNewCounterpartyTimeoutPersisting(CounterpartyTimeoutInfo counterpartyTimeoutInfo)
        {
            SqlCommand insertcounterpartyTimeoutCommand = new SqlCommand("[dbo].[InsertNewOrderExternalIncomingRejectableTimeout_SP]")
            {
                CommandType = CommandType.StoredProcedure
            };

            SqlParameter sqlParam1 = insertcounterpartyTimeoutCommand.Parameters.Add("@TimeoutReceivedUtc", SqlDbType.DateTime2);
            sqlParam1.Value = counterpartyTimeoutInfo.IntegratorReceivedTimeoutUtc;
            SqlParameter sqlParam2 = insertcounterpartyTimeoutCommand.Parameters.Add("@TimeoutSentUtc", SqlDbType.DateTime2);
            sqlParam2.Value = counterpartyTimeoutInfo.CounterpartySentTimeoutUtc;
            insertcounterpartyTimeoutCommand.Parameters.AddWithValue("@CounterpartyClientOrderId",
                counterpartyTimeoutInfo.CounterpartyClientOrderId);

            return insertcounterpartyTimeoutCommand;
        }

        private SqlCommand CreateCommandForExecutionReportPersisting(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal)
        {
            return null;

            throw new NotImplementedException();

            //SqlCommand insertOrderCommand = new SqlCommand("[dbo].[InsertExecutionReportMessage_SP]")
            //{
            //    CommandType = CommandType.StoredProcedure
            //};

            //insertOrderCommand.CommandType = CommandType.StoredProcedure;

            //SqlParameter sqlParam = insertOrderCommand.Parameters.Add("@ExecutionReportReceivedUtc", SqlDbType.DateTime2);
            //sqlParam.Value = orderChangeInfo.ReceivedTimeStamp;
            //insertOrderCommand.Parameters.AddWithValue("@ExecutionReportStatus", orderChangeInfo.ChangeEventArgs.ChangeState.ToString());
            //insertOrderCommand.Parameters.AddWithValue("@InternalOrderId", orderChangeInfo.UniqueInternalIdentity);
            //SqlParameter sqlParamCtp = insertOrderCommand.Parameters.Add("@ExecutionReportCounterpartySentUtc", SqlDbType.DateTime2);
            //sqlParamCtp.Value = orderChangeInfo.CounterpartySentTime;
            //SqlParameter orderSentSqlParam = insertOrderCommand.Parameters.Add("@IntegratorSentExternalOrderUtc", SqlDbType.DateTime2);
            //orderSentSqlParam.Value = integratorOrderExternal.IntegratorSentTimeUtc;
            //insertOrderCommand.Parameters.AddWithValue("@RawFIXMessage",
            //                                           orderChangeInfo.RawFixMessage);

            //return insertOrderCommand;
        }

        private SqlCommand CreateCommandForExecutedDealPersisting(OrderChangeInfo orderChangeInfo,
                                                                  IIntegratorOrderExternal integratorOrderExternal, decimal termCurrencyUsdConversionMultiplier)
        {
            SqlCommand insertOrderCommand = new SqlCommand("[dbo].[InsertDealExternalExecuted_SP]")
                {
                    CommandType = CommandType.StoredProcedure
                };

            insertOrderCommand.CommandType = CommandType.StoredProcedure;

            if(!orderChangeInfo.ChangeEventArgs.ExecutionInfo.SettlementDate.HasValue)
                this._logger.Log(LogLevel.Fatal, "Order [{0}] execution doesn't have settlement info specified - this is unexpected and might lead to problems during storing into Backend",
                    integratorOrderExternal.Identity);


            SqlParameter sqlParam = insertOrderCommand.Parameters.Add("@ExecutionReportReceivedUtc", SqlDbType.DateTime2);
            sqlParam.Value = orderChangeInfo.IntegratorReceivedTimeUtc;
            insertOrderCommand.Parameters.AddWithValue("@CounterpartyCode",
                                                       integratorOrderExternal.Counterparty.ToString());
            insertOrderCommand.Parameters.AddWithValue("@Symbol",
                                                       integratorOrderExternal.OrderRequestInfo.Symbol.ToString());
            insertOrderCommand.Parameters.AddWithValue("@DealDirection",
                                                       integratorOrderExternal.OrderRequestInfo.Side.ToString());
            insertOrderCommand.Parameters.AddWithValue("@InternalOrderId",
                                                       integratorOrderExternal.UniqueInternalIdentity);
            insertOrderCommand.Parameters.AddWithValue("@InternalDealId",
                                                       orderChangeInfo.ChangeEventArgs.ExecutionInfo.IntegratorTransactionId);
            decimal filledAmountBasAbs = orderChangeInfo.ChangeEventArgs.ExecutionInfo.FilledAmount.Value;
            insertOrderCommand.Parameters.AddWithValue("@AmountBasePolExecuted",
                orderChangeInfo.ChangeEventArgs.ExecutionInfo.FilledAmountBasePolarized);

            decimal price = orderChangeInfo.ChangeEventArgs.ExecutionInfo.UsedPrice.Value;
            decimal amountTermBaseAbsInUsd = filledAmountBasAbs*price*
                                             termCurrencyUsdConversionMultiplier;
            insertOrderCommand.Parameters.AddWithValue("@AmountTermPolExecutedInUsd",
                                                       integratorOrderExternal.OrderRequestInfo.Side ==
                                                       DealDirection.Buy
                                                           ? -amountTermBaseAbsInUsd
                                                           : amountTermBaseAbsInUsd);
            insertOrderCommand.Parameters.AddWithValue("@Price", price);

            insertOrderCommand.Parameters.AddWithValue("@CounterpartyTransactionId",
                                                       orderChangeInfo.ChangeEventArgs.ExecutionInfo.CounterpartyTransactionId);
            SqlParameter sqlParamCtp = insertOrderCommand.Parameters.Add("@CounterpartyExecutionReportTimeStampUtc", SqlDbType.DateTime2);
            sqlParamCtp.Value = orderChangeInfo.ChangeEventArgs.ExecutionInfo.CounterpartyTransactionTime ?? (object) DBNull.Value;
            insertOrderCommand.Parameters.AddWithValue("@ValueDateCounterpartySuppliedLocMktDate",
                                                       orderChangeInfo.ChangeEventArgs.ExecutionInfo.SettlementDate);
            insertOrderCommand.Parameters.AddWithValue("@ValueDateCounterpartySuppliedRecalcedToCounterpartyUtc",
                orderChangeInfo.ChangeEventArgs.ExecutionInfo.SettlementTimeCounterpartyUtc);
            insertOrderCommand.Parameters.AddWithValue("@ValueDateCounterpartySuppliedRecalcedToCentralBankUtc",
                orderChangeInfo.ChangeEventArgs.ExecutionInfo.SettlementTimeCentralBankUtc);

            TimeSpan orderSentToExecutionReportReceivedLatency;
            if (integratorOrderExternal.IntegratorSentTimeUtc > orderChangeInfo.IntegratorReceivedTimeUtc)
            {
                this._logger.Log(LogLevel.Error,
                                 "Order [{0}], [{1}] has negative execution report received latency! Setting it to zero",
                                 integratorOrderExternal.Identity, integratorOrderExternal.UniqueInternalIdentity);
                orderSentToExecutionReportReceivedLatency = TimeSpan.Zero;
            }
            else
            {
                orderSentToExecutionReportReceivedLatency = orderChangeInfo.IntegratorReceivedTimeUtc -
                                                            integratorOrderExternal.IntegratorSentTimeUtc;
            }
            insertOrderCommand.Parameters.AddWithValue("@OrderSentToExecutionReportReceivedLatency",
                                                       orderSentToExecutionReportReceivedLatency);

            if (integratorOrderExternal.QuoteReceivedToOrderSentInternalLatency.HasValue)
            {
                TimeSpan quoteReceivedToOrderSentInternalLatency =
                integratorOrderExternal.QuoteReceivedToOrderSentInternalLatency.Value;
                if (quoteReceivedToOrderSentInternalLatency < TimeSpan.Zero)
                {
                    this._logger.Log(LogLevel.Error,
                                     "Order [{0}], [{1}] has negative quote received to order sent latency! Setting it to zero",
                                     integratorOrderExternal.Identity, integratorOrderExternal.UniqueInternalIdentity);
                    quoteReceivedToOrderSentInternalLatency = TimeSpan.Zero;
                }
                insertOrderCommand.Parameters.AddWithValue("@QuoteReceivedToOrderSentInternalLatency",
                                                           quoteReceivedToOrderSentInternalLatency);
            }
            else
            {
                insertOrderCommand.Parameters.AddWithValue("@QuoteReceivedToOrderSentInternalLatency", DBNull.Value);
            }

            SqlParameter orderSentSqlParam = insertOrderCommand.Parameters.Add("@IntegratorSentExternalOrderUtc", SqlDbType.DateTime2);
            orderSentSqlParam.Value = integratorOrderExternal.IntegratorSentTimeUtc;

            if (orderChangeInfo.CounterpartySentTime.HasValue && orderChangeInfo.CounterpartySentTime.Value != DateTime.MinValue)
            {
                SqlParameter sqlParamCtp2 = insertOrderCommand.Parameters.Add("@CounterpartySentExecutionReportUtc", SqlDbType.DateTime2);
                sqlParamCtp2.Value = orderChangeInfo.CounterpartySentTime.Value;
            }

            FlowSide? flowSide = orderChangeInfo.ChangeEventArgs.ExecutionInfo.FlowSide ??
                                 integratorOrderExternal.OrderRequestInfo.FlowSideAtSubmitTime;
            insertOrderCommand.Parameters.AddWithValue("@FlowSide", flowSide == null ? DBNull.Value : (object)flowSide.ToString());

            if (orderChangeInfo.ChangeEventArgs.ExecutionInfo.ClientSystemAllocations != null)
            {
                if (orderChangeInfo.ChangeEventArgs.ExecutionInfo.ClientSystemAllocations.SingleAllocationClientSystemId != null)
                {
                    insertOrderCommand.Parameters.AddWithValue("@SingleFillSystemId", 
                        orderChangeInfo.ChangeEventArgs.ExecutionInfo.ClientSystemAllocations.SingleAllocationClientSystemId.Value);
                }
                else if (orderChangeInfo.ChangeEventArgs.ExecutionInfo.ClientSystemAllocations.ClientSystemAllocationsIds != null)
                {
                    //sql sp expect csv list of ints (so we also convert)
                    string multipleFillsArg = string.Join(string.Empty,
                        orderChangeInfo.ChangeEventArgs.ExecutionInfo.ClientSystemAllocations.ClientSystemAllocationsIds.Zip(
                        orderChangeInfo.ChangeEventArgs.ExecutionInfo.ClientSystemAllocations.ClientSystemAllocationAmountsBaseAbs,
                        (id, amount) => string.Format(CultureInfo.InvariantCulture, "{0},{1},", id, (int) amount)));

                    insertOrderCommand.Parameters.AddWithValue("@MultipleFillsSystemIdsAndAmountsBaseAbs",
                                                               multipleFillsArg);
                }
            }

            insertOrderCommand.Parameters.AddWithNullValue("@CounterpartyClientId", orderChangeInfo.ChangeEventArgs.ExecutionInfo.CounterpartyClientId);

            return insertOrderCommand;
        }

        private SqlCommand CreateCommandForRejectedDealPersisting(OrderChangeInfo orderChangeInfo,
                                                                  IIntegratorOrderExternal integratorOrderExternal)
        {
            SqlCommand insertOrderCommand = new SqlCommand("[dbo].[InsertDealExternalRejected_SP]")
                {
                    CommandType = CommandType.StoredProcedure
                };

            insertOrderCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParam = insertOrderCommand.Parameters.Add("@ExecutionReportReceivedUtc", SqlDbType.DateTime2);
            sqlParam.Value = orderChangeInfo.IntegratorReceivedTimeUtc;
            insertOrderCommand.Parameters.AddWithValue("@CounterpartyCode",
                                                       integratorOrderExternal.Counterparty.ToString());
            insertOrderCommand.Parameters.AddWithValue("@Symbol",
                                                       integratorOrderExternal.OrderRequestInfo.Symbol.ToString());
            insertOrderCommand.Parameters.AddWithValue("@DealDirection",
                                                       integratorOrderExternal.OrderRequestInfo.Side.ToString());
            insertOrderCommand.Parameters.AddWithValue("@InternalOrderId",
                                                       integratorOrderExternal.UniqueInternalIdentity);
            decimal rejectedAmountBasAbs = orderChangeInfo.ChangeEventArgs.RejectionInfo.RejectedAmount.Value;
            insertOrderCommand.Parameters.AddWithValue("@AmountBasePolRejected",
                                                       integratorOrderExternal.OrderRequestInfo.Side ==
                                                       DealDirection.Buy
                                                           ? rejectedAmountBasAbs
                                                           : -rejectedAmountBasAbs);

            TimeSpan orderSentToExecutionReportReceivedLatency;
            if (integratorOrderExternal.IntegratorSentTimeUtc > orderChangeInfo.IntegratorReceivedTimeUtc)
            {
                this._logger.Log(LogLevel.Error,
                                 "Order [{0}], [{1}] has negative execution report received latency! Setting it to zero",
                                 integratorOrderExternal.Identity, integratorOrderExternal.UniqueInternalIdentity);
                orderSentToExecutionReportReceivedLatency = TimeSpan.Zero;
            }
            else
            {
                orderSentToExecutionReportReceivedLatency = orderChangeInfo.IntegratorReceivedTimeUtc -
                                                            integratorOrderExternal.IntegratorSentTimeUtc;
            }
            insertOrderCommand.Parameters.AddWithValue("@OrderSentToExecutionReportReceivedLatency",
                                                       orderSentToExecutionReportReceivedLatency);

            if (integratorOrderExternal.QuoteReceivedToOrderSentInternalLatency.HasValue)
            {
                TimeSpan quoteReceivedToOrderSentInternalLatency =
                integratorOrderExternal.QuoteReceivedToOrderSentInternalLatency.Value;
                if (quoteReceivedToOrderSentInternalLatency < TimeSpan.Zero)
                {
                    this._logger.Log(LogLevel.Error,
                                     "Order [{0}], [{1}] has negative quote received to order sent latency! Setting it to zero",
                                     integratorOrderExternal.Identity, integratorOrderExternal.UniqueInternalIdentity);
                    quoteReceivedToOrderSentInternalLatency = TimeSpan.Zero;
                }
                insertOrderCommand.Parameters.AddWithValue("@QuoteReceivedToOrderSentInternalLatency",
                                                           quoteReceivedToOrderSentInternalLatency);
            }
            else
            {
                insertOrderCommand.Parameters.AddWithValue("@QuoteReceivedToOrderSentInternalLatency", DBNull.Value);
            }

            SqlParameter orderSentSqlParam = insertOrderCommand.Parameters.Add("@IntegratorSentExternalOrderUtc", SqlDbType.DateTime2);
            orderSentSqlParam.Value = integratorOrderExternal.IntegratorSentTimeUtc;

            insertOrderCommand.Parameters.AddWithValue("@RejectionReason", orderChangeInfo.ChangeEventArgs.RejectionInfo.RejectionReason);
            if (orderChangeInfo.CounterpartySentTime.HasValue && orderChangeInfo.CounterpartySentTime.Value != DateTime.MinValue)
            {
                SqlParameter sqlParamCtp2 = insertOrderCommand.Parameters.Add("@CounterpartySentExecutionReportUtc", SqlDbType.DateTime2);
                sqlParamCtp2.Value = orderChangeInfo.CounterpartySentTime.Value;
            }

            insertOrderCommand.Parameters.AddWithValue("@RejectedPrice", integratorOrderExternal.OrderRequestInfo.RequestedPrice);

            if (orderChangeInfo.ChangeEventArgs.RejectionInfo.ClientSystemAllocations != null)
            {
                if (orderChangeInfo.ChangeEventArgs.RejectionInfo.ClientSystemAllocations.SingleAllocationClientSystemId != null)
                {
                    insertOrderCommand.Parameters.AddWithValue("@SingleRejectionSystemId",
                        orderChangeInfo.ChangeEventArgs.RejectionInfo.ClientSystemAllocations.SingleAllocationClientSystemId.Value);
                }
                else if (orderChangeInfo.ChangeEventArgs.RejectionInfo.ClientSystemAllocations.ClientSystemAllocationsIds != null)
                {
                    //sql sp expect csv list of ints (so we also convert)
                    string multipleRejectionsArg = string.Join(string.Empty,
                        orderChangeInfo.ChangeEventArgs.RejectionInfo.ClientSystemAllocations.ClientSystemAllocationsIds.Zip(
                        orderChangeInfo.ChangeEventArgs.RejectionInfo.ClientSystemAllocations.ClientSystemAllocationAmountsBaseAbs,
                        (id, amount) => string.Format(CultureInfo.InvariantCulture, "{0},{1},", id, (int)amount)));

                    insertOrderCommand.Parameters.AddWithValue("@MultipleRejectionsSystemIdsAndAmountsBaseAbs",
                                                               multipleRejectionsArg);
                }
            }

            insertOrderCommand.Parameters.AddWithValue("@RejectionDirection", orderChangeInfo.ChangeEventArgs.RejectionInfo.RejectionDirection.ToString());

            insertOrderCommand.Parameters.AddWithNullValue("@CounterpartyClientId", orderChangeInfo.ChangeEventArgs.RejectionInfo.CounterpartyClientId);

            return insertOrderCommand;
        }


        private SqlCommand CreateCommandForCancelRequestPersisting(ExternalCancelRequest externalCancelRequest, IIntegratorOrderExternal integratorOrderExternal)
        {
            SqlCommand insertOrderCommand = new SqlCommand("[dbo].[InsertOrderExternalCancelRequest_SP]")
            {
                CommandType = CommandType.StoredProcedure
            };

            insertOrderCommand.CommandType = CommandType.StoredProcedure;

            insertOrderCommand.Parameters.AddWithValue("@IntegratorExternalOrderPrivateId",
                                                       integratorOrderExternal == null
                                                           ? DBNull.Value
                                                           : (object) integratorOrderExternal.UniqueInternalIdentity);

            SqlParameter orderSentSqlParam = insertOrderCommand.Parameters.Add("@IntegratorSentExternalOrderUtc", SqlDbType.DateTime2);
            orderSentSqlParam.Value = integratorOrderExternal == null
                                          ? DBNull.Value
                                          : (object) integratorOrderExternal.IntegratorSentTimeUtc;

            SqlParameter sqlParam = insertOrderCommand.Parameters.Add("@IntegratorSentCancelRequestUtc", SqlDbType.DateTime2);
            sqlParam.Value = externalCancelRequest.IntegratorSentTimeUtc;

            insertOrderCommand.Parameters.AddWithValue("@OrderExternalCancelTypeName", externalCancelRequest.CancelType.ToString());
            insertOrderCommand.Parameters.AddWithValue("@RawFIXMessage", externalCancelRequest.OutgoingFixMessageRaw);

            return insertOrderCommand;
        }

        private SqlCommand CreateCommandForCancelResultPersisting(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal)
        {
            SqlCommand insertOrderCommand = new SqlCommand("[dbo].[InsertOrderExternalCancelResult_SP]")
            {
                CommandType = CommandType.StoredProcedure
            };

            insertOrderCommand.CommandType = CommandType.StoredProcedure;

            insertOrderCommand.Parameters.AddWithValue("@IntegratorExternalOrderPrivateId", integratorOrderExternal.UniqueInternalIdentity);
            SqlParameter orderSentSqlParam = insertOrderCommand.Parameters.Add("@IntegratorSentExternalOrderUtc", SqlDbType.DateTime2);
            orderSentSqlParam.Value = integratorOrderExternal.IntegratorSentTimeUtc;
            SqlParameter sqlParam = insertOrderCommand.Parameters.Add("@CounterpartySentResultUtc", SqlDbType.DateTime2);
            sqlParam.Value = orderChangeInfo.CounterpartySentTime;
            insertOrderCommand.Parameters.AddWithValue("@CancelledAmountBasePol",
                                                       orderChangeInfo.ChangeEventArgs.RejectionInfo == null
                                                            //this is for cancelRejected
                                                           ? 0
                                                           : orderChangeInfo.ChangeEventArgs.RejectionInfo
                                                                            .RejectedAmount.Value);
            insertOrderCommand.Parameters.AddWithValue("@ResultStatusName", orderChangeInfo.ChangeEventArgs.ChangeState.ToString());

            return insertOrderCommand;
        }


        private SqlCommand CreateCommandForIgnoredOrdersPersisting(DateTime ignoreDetectedUtc, IIntegratorOrderExternal integratorOrderExternal)
        {
            SqlCommand insertOrderCommand = new SqlCommand("[dbo].[InsertOrderExternalIgnored_SP]")
            {
                CommandType = CommandType.StoredProcedure
            };

            insertOrderCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParam = insertOrderCommand.Parameters.Add("@ExternalOrderSentUtc", SqlDbType.DateTime2);
            sqlParam.Value = integratorOrderExternal.IntegratorSentTimeUtc;
            SqlParameter sqlParam2 = insertOrderCommand.Parameters.Add("@ExternalOrderIgnoreDetectedUtc", SqlDbType.DateTime2);
            sqlParam2.Value = ignoreDetectedUtc;
            insertOrderCommand.Parameters.AddWithValue("@IntegratorExternalOrderPrivateId", integratorOrderExternal.UniqueInternalIdentity);
            insertOrderCommand.Parameters.AddWithValue("@InternalClientOrderId", integratorOrderExternal.Identity);
            insertOrderCommand.Parameters.AddWithValue("@RequestedPrice", integratorOrderExternal.OrderRequestInfo.RequestedPrice);
            insertOrderCommand.Parameters.AddWithValue("@RequestedAmountBaseAbs", integratorOrderExternal.OrderRequestInfo.SizeBaseAbsInitial);
            insertOrderCommand.Parameters.AddWithValue("@CounterpartyCode", integratorOrderExternal.Counterparty.ToString());
            insertOrderCommand.Parameters.AddWithValue("@Symbol", integratorOrderExternal.OrderRequestInfo.Symbol.ToString());
            insertOrderCommand.Parameters.AddWithValue("@DealDirection", integratorOrderExternal.OrderRequestInfo.Side.ToString());

            return insertOrderCommand;
        }

        private SqlCommand CreateCommandForUnexpectedExecutionPersisting(OrderChangeInfo orderChangeInfo, Counterparty counterparty)
        {
            SqlCommand insertOrderCommand = new SqlCommand("[dbo].[InsertDealExternalExecutedUnexpected_SP]")
            {
                CommandType = CommandType.StoredProcedure
            };

            insertOrderCommand.CommandType = CommandType.StoredProcedure;
            ExecutionInfo executionInfo = orderChangeInfo.ChangeEventArgs.ExecutionInfo;

            SqlParameter sqlParam = insertOrderCommand.Parameters.Add("@IntegratorReceivedExecutionUtc", SqlDbType.DateTime2);
            sqlParam.Value = orderChangeInfo.IntegratorReceivedTimeUtc;
            insertOrderCommand.Parameters.AddWithValue("@ExternalClientOrderId", orderChangeInfo.Identity);
            SqlParameter sqlParam2 = insertOrderCommand.Parameters.Add("@CounterpartySentExecutionUtc", SqlDbType.DateTime2);
            sqlParam2.Value = (object)orderChangeInfo.CounterpartySentTime ?? DBNull.Value;
            SqlParameter sqlParam3 = insertOrderCommand.Parameters.Add("@CounterpartyTransactionTimeUtc", SqlDbType.DateTime2);
            sqlParam3.Value = (object)executionInfo.CounterpartyTransactionTime ?? DBNull.Value;
            insertOrderCommand.Parameters.AddWithNullValue("@CounterpartySettlementTimeLocal", executionInfo.SettlementDate);
            insertOrderCommand.Parameters.AddWithValue("@CounterpartyTransactionId", executionInfo.CounterpartyTransactionId);
            insertOrderCommand.Parameters.AddWithValue("@CounterpartyCode", counterparty.ToString());
            insertOrderCommand.Parameters.AddWithValue("@Symbol", orderChangeInfo.Symbol.ToString());
            insertOrderCommand.Parameters.AddWithValue("@DealDirection", executionInfo.IntegratorDealDirection.ToString());
            insertOrderCommand.Parameters.AddWithNullValue("@FilledAmountBaseAbs", executionInfo.FilledAmount);
            insertOrderCommand.Parameters.AddWithNullValue("@FilledPrice", executionInfo.UsedPrice);


            return insertOrderCommand;
        }

        private SqlCommand CreateCommandForNewExternalDealExecutedTicketPersisting(TradeReportTicketInfo tradeReportTicketInfo)
        {
            SqlCommand insertOrderCommand = new SqlCommand("[dbo].[InsertNewDealTicket_SP]")
            {
                CommandType = CommandType.StoredProcedure
            };

            insertOrderCommand.CommandType = CommandType.StoredProcedure;

            insertOrderCommand.Parameters.AddWithValue("@InternalDealId", tradeReportTicketInfo.IntegratorDealId);
            SqlParameter sqlParam = insertOrderCommand.Parameters.Add("@TicketIntegratorSentOrReceivedTimeUtc", SqlDbType.DateTime2);
            sqlParam.Value = tradeReportTicketInfo.IntegratorSentOrReceivedTimeUtc;
            SqlParameter sqlParam2 = insertOrderCommand.Parameters.Add("@CounterpartySentTimeUtc", SqlDbType.DateTime2);
            sqlParam2.Value = tradeReportTicketInfo.CounterpartySentTime.HasValue ? (object) tradeReportTicketInfo.CounterpartySentTime : DBNull.Value;
            insertOrderCommand.Parameters.AddWithValue("@DealExternalExecutedTicketType", tradeReportTicketInfo.TradeReportTicketInfoType.ToString());
            insertOrderCommand.Parameters.AddWithValue("@StpCounterpartyName", tradeReportTicketInfo.StpCounterparty.ToString());
            insertOrderCommand.Parameters.AddWithValue("@FixMessageRaw", tradeReportTicketInfo.FixMessageRaw);

            return insertOrderCommand;
        }

        private SqlCommand CreateCommandForNewAggregatedTradeTicketInfoPersisting(AggregatedTradeTicketInfo aggregatedTradeTicketInfo)
        {
            SqlCommand insertAggregatedTicketCommand = new SqlCommand("[dbo].[InsertNewAggregatedDealTicket_SP]")
            {
                CommandType = CommandType.StoredProcedure
            };

            insertAggregatedTicketCommand.CommandType = CommandType.StoredProcedure;

            insertAggregatedTicketCommand.Parameters.AddWithValue("@AggregatedTradeReportCounterpartyId", aggregatedTradeTicketInfo.AggregatedTradeReportId);
            SqlParameter sqlParam = insertAggregatedTicketCommand.Parameters.Add("@TicketIntegratorReceivedTimeUtc", SqlDbType.DateTime2);
            sqlParam.Value = aggregatedTradeTicketInfo.IntegratorReceivedTimeUtc;
            SqlParameter sqlParam2 = insertAggregatedTicketCommand.Parameters.Add("@CounterpartySentTimeUtc", SqlDbType.DateTime2);
            sqlParam2.Value = aggregatedTradeTicketInfo.CounterpartySentTime.HasValue ? (object)aggregatedTradeTicketInfo.CounterpartySentTime : DBNull.Value;
            insertAggregatedTicketCommand.Parameters.AddWithValue("@AggregatedPrice", aggregatedTradeTicketInfo.AggregatedPrice);
            insertAggregatedTicketCommand.Parameters.AddWithValue("@AggregatedSizeBaseAbs", aggregatedTradeTicketInfo.AggregatedSizeBaseAbs);
            insertAggregatedTicketCommand.Parameters.AddWithValue("@StpCounterpartyName", aggregatedTradeTicketInfo.StpCounterparty.ToString());
            insertAggregatedTicketCommand.Parameters.AddWithValue("@FixMessageRaw", aggregatedTradeTicketInfo.FixMessageRaw);
            insertAggregatedTicketCommand.Parameters.AddWithValue("@ChildTradeIdsList", string.Join(",", aggregatedTradeTicketInfo.ChildTreadesIds));

            return insertAggregatedTicketCommand;
        }
    }
}
