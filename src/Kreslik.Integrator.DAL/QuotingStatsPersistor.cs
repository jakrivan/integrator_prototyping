﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.DAL
{
    public class QuotingStatsPersistor : IQuotingStatsPersistor
    {
        private class QuotingStatsPersistorProxy : IQuotingStatsPersistorProxy
        {
            public void UpdateLTStats(IActivityBandStats activityBandStats, bool ltBandRestrictionOn)
            {
                this.CurrentQuotingStats.ActivityBandStats = activityBandStats;
                this.CurrentQuotingStats.IsLtBandRestrictionOn = ltBandRestrictionOn;
            }

            public void UpdateInactivityRestrictionState(bool inactivityRestrictionOn)
            {
                this.CurrentQuotingStats.IsInactivityRestrictionOn = inactivityRestrictionOn;
            }

            public void UpdateLtBandRestrictionState(bool ltBandRestrictionOn)
            {
                this.CurrentQuotingStats.IsLtBandRestrictionOn = ltBandRestrictionOn;
            }

            public void UpdateCurrentSpread(decimal currentSpread)
            {
                this.CurrentQuotingStats.CurrentSpread = currentSpread;
            }

            public void ClearCurrentSpread()
            {
                this.CurrentQuotingStats.CurrentSpread = null;
            }

            public void SetShouldUploadStats(bool shouldUpload)
            {
                this.CurrentQuotingStats.Cleared = !shouldUpload;

                if(!shouldUpload)
                    this.CurrentQuotingStats.Clear();
            }

            public QuotingStatsPersistorProxy(int tradingSystemId)
            {
                this.TradingSystemId = tradingSystemId;
                this.CurrentQuotingStats = new QuotingStats();
                this.LastUploadedStats = new QuotingStats();
            }

            public int TradingSystemId { get; private set; }
            public QuotingStats CurrentQuotingStats { get; private set; }
            public QuotingStats LastUploadedStats { get; private set; }
        }

        private class QuotingStats
        {
            public bool Cleared { get; set; }
            public IActivityBandStats ActivityBandStats { get; set; }
            public bool IsInactivityRestrictionOn { get; set; }
            public bool IsLtBandRestrictionOn { get; set; }
            public decimal? CurrentSpread { get; set; }

            public QuotingStats()
            {
                //force upload of all
                this.Cleared = true;
            }

            public void Clear()
            {
                this.Cleared = true;
                this.ActivityBandStats = null;
                this.IsInactivityRestrictionOn = true;
                this.IsLtBandRestrictionOn = true;
                this.CurrentSpread = null;
            }

            public bool Equals(QuotingStats stats)
            {
                if (this.Cleared == true && stats.Cleared == true)
                    return true;

                if (this.ActivityBandStats == null && stats.ActivityBandStats != null ||
                    this.ActivityBandStats != null && stats.ActivityBandStats == null)
                    return false;

                return
                    (
                    (this.ActivityBandStats == null && stats.ActivityBandStats == null)
                    ||
                    this.ActivityBandStats.Equals(stats.ActivityBandStats)
                    )
                    &&
                    this.IsInactivityRestrictionOn == stats.IsInactivityRestrictionOn &&
                    this.IsLtBandRestrictionOn == stats.IsLtBandRestrictionOn &&
                    this.CurrentSpread == stats.CurrentSpread;
            }
        }

        private static readonly IQuotingStatsPersistor _instnace = new QuotingStatsPersistor();
        public static IQuotingStatsPersistor Instance { get { return _instnace; } }

        private List<QuotingStatsPersistorProxy> _proxies = new List<QuotingStatsPersistorProxy>();
        private SafeTimer _refreshingTimer;

        private QuotingStatsPersistor()
        {
            _refreshingTimer = new SafeTimer(ProcessChanges, TimeSpan.FromSeconds(5), TimeSpan.FromMilliseconds(1200), true);
        }

        public IQuotingStatsPersistorProxy CreateQuotingStatsPersistorProxy(int tradingSystemId)
        {
            QuotingStatsPersistorProxy proxy = new QuotingStatsPersistorProxy(tradingSystemId);

            lock (_proxies)
            {
                _proxies.Add(proxy);
            }

            return proxy;
        }

        private void ProcessChanges()
        {
            lock (_proxies)
            {
                foreach (QuotingStatsPersistorProxy proxy in _proxies)
                {
                    try
                    {
                        this.ProcessSingleProxy(proxy);
                    }
                    catch (Exception e)
                    {
                        LogFactory.Instance.GetLogger(null)
                            .LogException(LogLevel.Fatal, e,
                                "Failed during updating quoting stats for system [{0}]: {1}",
                                proxy.TradingSystemId, proxy.CurrentQuotingStats.ActivityBandStats);
                    }
                }
            }
        }

        private void ProcessSingleProxy(QuotingStatsPersistorProxy proxy)
        {
            if(proxy.CurrentQuotingStats.Equals(proxy.LastUploadedStats))
                return;

            if (proxy.CurrentQuotingStats.Cleared && !proxy.LastUploadedStats.Cleared)
            {
                this.ClearStats(proxy.TradingSystemId);
                proxy.LastUploadedStats.Clear();
                return;
            }

            bool wasCleared = proxy.LastUploadedStats.Cleared && !proxy.CurrentQuotingStats.Cleared;

            using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand command = new SqlCommand("[dbo].[VenueQuotingIntegratorStatsUpdateSingle_SP]", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@TradingSystemId", proxy.TradingSystemId);
                    IActivityBandStats currActivityBandStats = proxy.CurrentQuotingStats.ActivityBandStats;
                    if (currActivityBandStats != null &&
                        (proxy.LastUploadedStats.ActivityBandStats == null ||
                         !currActivityBandStats.Equals(proxy.LastUploadedStats.ActivityBandStats)))
                    {
                        command.Parameters.AddWithValue("@CurrentLmaxTickerVolumeUsd", currActivityBandStats.TotalVolumUsd);
                        command.Parameters.AddWithValue("@CurrentLmaxTickerAvgDealSizeUsd", currActivityBandStats.AvgDealSizeUsd);
                        command.Parameters.AddWithValue("@CalculatedFromFullInterval", currActivityBandStats.HadEnoughData);
                        command.Parameters.AddWithValue("@CurrentLmaxTickerPaidVolumeRatio", currActivityBandStats.PaidVolumeRatio);

                        proxy.LastUploadedStats.ActivityBandStats = currActivityBandStats;
                    }

                    bool currIsInactivityRestrictionOn = proxy.CurrentQuotingStats.IsInactivityRestrictionOn;
                    if (wasCleared || currIsInactivityRestrictionOn != proxy.LastUploadedStats.IsInactivityRestrictionOn)
                    {
                        command.Parameters.AddWithValue("@InactivityRestrictionOn", currIsInactivityRestrictionOn);
                        proxy.LastUploadedStats.IsInactivityRestrictionOn = currIsInactivityRestrictionOn;
                    }

                    bool currIsLtBandRestrictionOn = proxy.CurrentQuotingStats.IsLtBandRestrictionOn;
                    if (wasCleared || currIsLtBandRestrictionOn != proxy.LastUploadedStats.IsLtBandRestrictionOn)
                    {
                        command.Parameters.AddWithValue("@LTBandRestrictionOn", currIsLtBandRestrictionOn);
                        proxy.LastUploadedStats.IsLtBandRestrictionOn = currIsLtBandRestrictionOn;
                    }

                    decimal? currSpread = proxy.CurrentQuotingStats.CurrentSpread;
                    if (wasCleared || currSpread != proxy.LastUploadedStats.CurrentSpread)
                    {
                        if(currSpread.HasValue)
                            command.Parameters.AddWithValue("@CurrentSpreadDecimal", currSpread.Value);
                        else
                            command.Parameters.AddWithValue("@IsSpredNull", true);
                        proxy.LastUploadedStats.CurrentSpread = currSpread;
                    }
                    
                    command.ExecuteNonQuery();

                    proxy.LastUploadedStats.Cleared = false;
                }
            }
        }

        private void ClearStats(int tradingSystemId)
        {
            try
            {
                ClearStatsInternal(tradingSystemId);
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null)
                    .LogException(LogLevel.Fatal, e, "Failed during clearing quoting stats for system [{0}]",
                        tradingSystemId);
            }
        }

        private void ClearStatsInternal(int tradingSystemId)
        {
            using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand command = new SqlCommand("[dbo].[VenueQuotingIntegratorStatsClearSingle_SP]", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@TradingSystemId", tradingSystemId);

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
