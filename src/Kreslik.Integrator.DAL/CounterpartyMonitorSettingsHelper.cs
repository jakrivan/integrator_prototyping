﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.DAL
{
    public class SettingsDetail
    {
        public SettingsDetail(int id, string friendlyName)
        {
            this.Id = id;
            this.FriendlyName = friendlyName;
        }

        public int Id { get; private set; }
        public string FriendlyName { get; private set; }
    }

    public class CounterpartyMonitorSettingsHelper
    {
        public List<SettingsDetail> GetSettingsList()
        {
            List<SettingsDetail> settingsDetails = new List<SettingsDetail>();

            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getsettingsList = new SqlCommand("[dbo].[GetCounterpartyMonitorSettingsList_SP]", sqlConnection))
                {
                    getsettingsList.CommandType = CommandType.StoredProcedure;

                    using (var reader = getsettingsList.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int id = reader.GetInt32(reader.GetOrdinal("Id"));
                            string friendlyName = reader.IsDBNull(reader.GetOrdinal("FriendlyName"))
                                                      ? "<Not Specified>"
                                                      : reader.GetString(reader.GetOrdinal("FriendlyName"));

                            settingsDetails.Add(new SettingsDetail(id, friendlyName));
                        }
                    }
                }
            }

            return settingsDetails;
        }

        public string GetSettingsXmlString(string settingsFriendlyName)
        {

            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand getSettingsCommand = new SqlCommand("[dbo].[GetSettingsXml2_SP]", sqlConnection))
                {
                    getSettingsCommand.CommandType = CommandType.StoredProcedure;
                    getSettingsCommand.Parameters.AddWithValue("@SettingName",
                                                               "Kreslik.Integrator.DiagnosticsDashboard.exe");
                    getSettingsCommand.Parameters.AddWithValue("@SettingFriendlyName", settingsFriendlyName);

                    using (var reader = getSettingsCommand.ExecuteXmlReader())
                    {
                        if (reader.Read())
                        {
                            return reader.ReadOuterXml();
                        }
                    }
                }
            }

            return null;
        }

        public void UpdateCMonitorSettings(string environemntName, string settingFriendlyName, string settingXml)
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand updateSettingsCommand = new SqlCommand("[dbo].[UpdateCounterpartyMonitorSettings_SP]", sqlConnection))
                {
                    updateSettingsCommand.CommandType = CommandType.StoredProcedure;
                    updateSettingsCommand.Parameters.AddWithValue("@EnvironmentName", environemntName);
                    updateSettingsCommand.Parameters.AddWithValue("@SettingsFriendlyName", settingFriendlyName);
                    updateSettingsCommand.Parameters.AddWithValue("@SettingsXml", settingXml);

                    updateSettingsCommand.ExecuteNonQuery();
                }
            }

        }

        public string GetCurrentCMonitorSettingsFriendlyName(string environemntName)
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand updateSettingsCommand = new SqlCommand("[dbo].[GetCounterpartyMonitorSettingsName_SP]", sqlConnection))
                {
                    updateSettingsCommand.CommandType = CommandType.StoredProcedure;
                    updateSettingsCommand.Parameters.AddWithValue("@EnvironmentName", environemntName);

                    object retVal = updateSettingsCommand.ExecuteScalar();
                    if (retVal is string)
                        return retVal as string;
                }
            }

            return null;
        }
    }
}
