﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.DAL
{
    public class IntegratorInstanceProcessUtils: IIntegratorInstanceProcessUtils
    {
        private readonly string _instanceName;
        private readonly string _processName;
        private readonly IntegratorProcessType _processType;

        //Integrator process will construct this with their process name
        // Splitters will construct this with desired target process name
        public IntegratorInstanceProcessUtils(string instanceName, IntegratorProcessType processType)
        {
            this._instanceName = instanceName;
            this._processType = processType;
            this._processName = processType.ToString();
        }

        public IntegratorInstanceProcessUtils(IntegratorProcessType processType)
            : this(SettingsInitializator.Instance.InstanceName, processType)
        { }

        public bool IsKnownPrivateIp(string privateIpAddress)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand ipCommand = new SqlCommand("[dbo].[IsKnownPrivateIP_SP]", sqlConnection))
                {
                    ipCommand.CommandType = CommandType.StoredProcedure;
                    ipCommand.Parameters.AddWithValue("@PrivateIPAsString", privateIpAddress);

                    object retVal = ipCommand.ExecuteScalar();
                    return retVal != null && (int) retVal == 1;
                }
            }
        }

        public List<IntegratorProcessInfo> GetIntegratorProcessDetails()
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getProcessDetailsCommand = new SqlCommand("[dbo].[GetIntegratorProcessDetails_SP]", sqlConnection))
                {
                    getProcessDetailsCommand.CommandType = CommandType.StoredProcedure;
                    getProcessDetailsCommand.Parameters.AddWithValue("@InstanceName", this._instanceName);
                    getProcessDetailsCommand.Parameters.AddWithValue("@ProcessName", this._processName);

                    using (var reader = getProcessDetailsCommand.ExecuteReader())
                    {
                        List<IntegratorProcessInfo> integratorProcessInfos = new List<IntegratorProcessInfo>();

                        while (reader.Read())
                        {
                            integratorProcessInfos
                                .Add(new IntegratorProcessInfo(
                                         this._processType,
                                         reader.GetInt32(reader.GetOrdinal("ProcessIdentifier")),
                                         reader.GetString(reader.GetOrdinal("PrivateIPString")),
                                         reader.GetString(reader.GetOrdinal("PublicIPString")),
                                         reader.GetString(reader.GetOrdinal("Hostname")),
                                         reader.GetNullableInt32(reader.GetOrdinal("TradingAPIPort")),
                                         reader.GetNullableInt32(reader.GetOrdinal("DiagnosticsAPIPort")),
                                         (IntegratorProcessState)
                                         Enum.Parse(typeof (IntegratorProcessState),
                                                    reader.GetString(reader.GetOrdinal("State")))));
                        }

                        return integratorProcessInfos;
                    }
                }
            }
        }

        public List<IntegratorProcessInfo> TryRegisterIntegratorProcess(string privateIPAddress, out int? integratorProcessIdentifier)
        {
            integratorProcessIdentifier = null;
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand registerProcessCommand = new SqlCommand("[dbo].[RegisterIntegratorProcess_SP]", sqlConnection))
                {
                    registerProcessCommand.CommandType = CommandType.StoredProcedure;
                    registerProcessCommand.Parameters.AddWithValue("@InstanceName", this._instanceName);
                    registerProcessCommand.Parameters.AddWithValue("@ProcessName", this._processName);
                    registerProcessCommand.Parameters.AddWithValue("@PrivateIPAsString", privateIPAddress);

                    SqlParameter returnParam = registerProcessCommand.Parameters.Add("@ReturnValue", SqlDbType.Int);
                    returnParam.Direction = ParameterDirection.ReturnValue;

                    List<IntegratorProcessInfo> integratorProcessInfos = new List<IntegratorProcessInfo>();

                    using (var reader = registerProcessCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            integratorProcessInfos
                                .Add(new IntegratorProcessInfo(
                                         this._processType,
                                         reader.GetInt32(reader.GetOrdinal("ProcessIdentifier")),
                                         reader.GetString(reader.GetOrdinal("PrivateIPString")),
                                         reader.GetString(reader.GetOrdinal("PublicIPString")),
                                         reader.GetString(reader.GetOrdinal("Hostname")),
                                         reader.GetNullableInt32(reader.GetOrdinal("TradingAPIPort")),
                                         reader.GetNullableInt32(reader.GetOrdinal("DiagnosticsAPIPort")),
                                         (IntegratorProcessState)
                                         Enum.Parse(typeof (IntegratorProcessState),
                                                    reader.GetString(reader.GetOrdinal("State")))));
                        }
                    }

                    //WARNING!: return value can only be read AFTER the reader is closed!
                    if (returnParam.Value != null)
                    {
                        integratorProcessIdentifier = (int)returnParam.Value;
                    }

                    return integratorProcessInfos;

                }
            }
        }

        public void UpdateIntegratorProcessState(IntegratorProcessInfo integratorProcessInfo, IntegratorProcessState newState)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand updateProcessCommand = new SqlCommand("[dbo].[UpdateIntegratorProcessState_SP]", sqlConnection))
                {
                    updateProcessCommand.CommandType = CommandType.StoredProcedure;
                    updateProcessCommand.Parameters.AddWithValue("@ProcessIdentifier", integratorProcessInfo.ProcessIdentifier);
                    updateProcessCommand.Parameters.AddWithValue("@NewProcessState", newState.ToString());

                    updateProcessCommand.ExecuteNonQuery();
                }
            }
        }

        public void HeartBeat(IntegratorProcessInfo integratorProcessInfo)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand updateProcessHbCommand = new SqlCommand("[dbo].[UpdateIntegratorProcessHBTime_SP]", sqlConnection))
                {
                    updateProcessHbCommand.CommandType = CommandType.StoredProcedure;
                    updateProcessHbCommand.Parameters.AddWithValue("@ProcessIdentifier", integratorProcessInfo.ProcessIdentifier);

                    updateProcessHbCommand.ExecuteNonQuery();
                }
            }
        }
    }

    public class IntegratorInstanceUtils : IIntegratorInstanceUtils
    {
        private readonly TimeSpan _pollInterval = TimeSpan.FromSeconds(30);
        private readonly SafeTimer<object> _pollTimer;
        private readonly string _instanceName;
        private readonly bool[] _counterpartySessionClaimStatuses = new bool[Counterparty.ValuesCount];
        private readonly bool[] _tempCounterpartySessionClaimStatuses = new bool[Counterparty.ValuesCount];

        static IntegratorInstanceUtils()
        {
            if (string.IsNullOrEmpty(SettingsInitializator.Instance.InstanceName))
            {
                LogFactory.Instance.GetLogger(null)
                                .Log(LogLevel.Fatal,
                                     "IntegratorInstanceName not configured in app.config, cannot continue");
                Thread.Sleep(1);
                throw new Exception("IntegratorInstanceName not configured in app.config, cannot continue");
            }

            Instance = new IntegratorInstanceUtils(SettingsInitializator.Instance.InstanceName);
        }

        public static IIntegratorInstanceUtils Instance { get; private set; }

        private IntegratorInstanceUtils(string instanceName)
        {
            this._instanceName = instanceName;
            this._pollTimer = new SafeTimer<object>(ControlSessionsClaimState, _pollInterval, true)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1sec,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
            this._pollTimer.ExecuteSynchronouslyWithReschedulingIfRequested();
        }

        public bool IsCounterpartySessionClaimed(Counterparty counterparty)
        {
            return _counterpartySessionClaimStatuses[(int) counterparty];
        }

        public event Action<Counterparty, bool> CounterpartyClaimStatusChanged;

        public void ReleaseCounterpartySession(Counterparty counterparty)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand releaseSessionCommand = new SqlCommand("[dbo].[ReleaseSessionFromInstance_SP]", sqlConnection))
                {
                    releaseSessionCommand.CommandType = CommandType.StoredProcedure;
                    releaseSessionCommand.Parameters.AddWithValue("@InstanceName", this._instanceName);
                    releaseSessionCommand.Parameters.AddWithValue("@Counterparty", counterparty.ToString());

                    releaseSessionCommand.ExecuteNonQuery();
                }
            }
        }

        public IntegratorRequestResult TryClaimCounterpartySession(Counterparty counterparty)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand claimSessionCommand = new SqlCommand("[dbo].[TryClaimSessionsForInstance_SP]", sqlConnection))
                {
                    claimSessionCommand.CommandType = CommandType.StoredProcedure;
                    claimSessionCommand.Parameters.AddWithValue("@InstanceName", this._instanceName);
                    claimSessionCommand.Parameters.AddWithValue("@Counterparty", counterparty.ToString());

                    try
                    {
                        var claimingInstance = claimSessionCommand.ExecuteScalar();
                        if (claimingInstance != null && claimingInstance.ToString() != this._instanceName)
                        {
                            return
                                IntegratorRequestResult.CreateFailedResult(
                                    string.Format("Session [{0}] already claimed by instance [{1}]", counterparty,
                                                  claimingInstance));
                        }
                    }
                    catch (Exception e)
                    {
                        return
                            IntegratorRequestResult.CreateFailedResult(string.Format("Problems during claiming: {0}",
                                                                                     e.ToString()));
                    }
                }
            }

            _counterpartySessionClaimStatuses[(int) counterparty] = true;
            return IntegratorRequestResult.GetSuccessResult();
        }

        private void ControlSessionsClaimState(object unused)
        {
            Array.Clear(_tempCounterpartySessionClaimStatuses, 0, _tempCounterpartySessionClaimStatuses.Length);

            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getClaimedSessions = new SqlCommand("[dbo].[GetClaimedSessionsForInstance_SP]", sqlConnection))
                {
                    getClaimedSessions.CommandType = CommandType.StoredProcedure;
                    getClaimedSessions.Parameters.AddWithValue("@InstanceName", this._instanceName);

                    using (var reader = getClaimedSessions.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            _tempCounterpartySessionClaimStatuses[
                                (int) (Counterparty) reader.GetString(reader.GetOrdinal("CounterpartyCode"))] = true;
                        }
                    }
                }
            }

            for (int idx = 0; idx < _counterpartySessionClaimStatuses.Length; idx++)
            {
                if (_counterpartySessionClaimStatuses[idx] != _tempCounterpartySessionClaimStatuses[idx])
                {
                    _counterpartySessionClaimStatuses[idx] = _tempCounterpartySessionClaimStatuses[idx];
                    if (CounterpartyClaimStatusChanged != null)
                    {
                        CounterpartyClaimStatusChanged((Counterparty) idx, _tempCounterpartySessionClaimStatuses[idx]);
                    }
                }
            }
        } 
    }
}
