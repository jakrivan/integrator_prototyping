﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.DAL
{
    public interface ICommandDistributor : ILocalCommandDistributor, IRemoteCommandWorker
    { }

    public class CommandDistributor : CommandDistributorBase, ICommandDistributor
    {
        private TimeSpan _pollInterval = TimeSpan.FromSeconds(DALBehavior.Sections.OhterSettings.CommandDistributorPollIntervalSeconds);
        private SafeTimer _pollTimer;
        private const int COMMANDS_BATCH_SIZE = 10;

        public CommandDistributor(ILogger logger, bool startPolingThread)
            :base(logger)
        {
            this._logger = logger;
            if (startPolingThread)
            {
                _pollTimer = new SafeTimer(CheckCommands, _pollInterval, _pollInterval)
                {
                    RequiredTimerPrecision = TimerTaskFlagUtils.GetTimerPrecisionForDelay(_pollInterval),
                    TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
                };
            }
        }

        public static IRemoteCommandWorker GetRemoteCommandWorker(ILogger logger)
        {
            return new CommandDistributor(logger, false);
        }

        /// <summary>
        /// Sends command to backend
        /// </summary>
        /// <param name="command">command string to be executed</param>
        /// <param name="logger"></param>
        /// <returns>Result of the operation</returns>
        public static IntegratorRequestResult TrySendCommandStatic(string command, ILogger logger)
        {
            Exception e = null;
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand updateItemCommand = new SqlCommand("[dbo].[InsertCommandItem_SP]", sqlConnection))
                    {
                        updateItemCommand.CommandType = CommandType.StoredProcedure;
                        updateItemCommand.Parameters.AddWithValue("@CommandText", command);

                        updateItemCommand.ExecuteNonQuery();
                        return IntegratorRequestResult.GetSuccessResult();
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                logger.LogException(LogLevel.Error, sqlEx, "Got SqlException when sending Command Item {0}", command);
                e = sqlEx;
            }
            catch (Exception ex)
            {
                logger.LogException(LogLevel.Fatal, ex, "Got unexpected Exception when sending Command Item {0}", command);
                e = ex;
            }

            return IntegratorRequestResult.CreateFailedResult("ErrorType: " + e.GetType().ToString());
        }

        public IntegratorRequestResult TrySendCommand(string command)
        {
            return TrySendCommandStatic(command, this._logger);
        }

        private void CheckCommands()
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand getItemsCommand = new SqlCommand("[dbo].[GetCommandItems_SP]", sqlConnection))
                    {
                        getItemsCommand.CommandType = CommandType.StoredProcedure;
                        getItemsCommand.Parameters.AddWithValue("@MaxCount", COMMANDS_BATCH_SIZE);

                        using (var reader = getItemsCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int commandId = reader.GetInt32(reader.GetOrdinal("Id"));
                                string commandText = reader.GetString(reader.GetOrdinal("CommandText"));

                                Task.Factory.StartNew(() => this.CrackCommand(commandText))
                                    .ContinueWith(
                                        task =>
                                        UpdateCommndItem(commandId,
                                                         task.Result.RequestSucceeded
                                                             ? "Successfuly Executed"
                                                             : task.Result.ErrorMessage));
                            }
                        }
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                _logger.LogException(LogLevel.Error, "Got SqlException when obtaining Command Items", sqlEx);
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, "Got unexpected Exception when obtaining Command Items", e);
            }
        }

        private void UpdateCommndItem(int commandId, string updateStatus)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand updateItemCommand = new SqlCommand("[dbo].[UpdateCommandItem_SP]", sqlConnection))
                    {
                        updateItemCommand.CommandType = CommandType.StoredProcedure;
                        updateItemCommand.Parameters.AddWithValue("@CommandId", commandId);
                        updateItemCommand.Parameters.AddWithValue("@UpdateStatus", updateStatus);

                        updateItemCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                _logger.LogException(LogLevel.Error, sqlEx, "Got SqlException when updating Command Item {0}", commandId);
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, e, "Got unexpected Exception when updating Command Item {0}", commandId);
            }
        }
    }
}
