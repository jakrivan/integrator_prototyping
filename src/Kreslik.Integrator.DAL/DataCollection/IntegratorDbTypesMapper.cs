﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.DAL.DataCollection
{
    public class IntegratorDbTypesMapper: IIntegratorDbTypesMapper
    {
        private byte[] _symbolMappings = new byte[Symbol.ValuesCount];
        private byte[] _counterpartyMapings = new byte[Counterparty.ValuesCount];
        private byte[] _orderTypeMapings = new byte[typeof(OrderType).OwningEnumValuesCount()];
        private byte[] _timeInForceMapings = new byte[typeof(TimeInForce).OwningEnumValuesCount()];
        private byte[] _dealDirectionMapings = new byte[typeof(DealDirection).OwningEnumValuesCount()];
        private byte[] _pegTypeMapings = new byte[typeof(PegType).OwningEnumValuesCount()];

        public IntegratorDbTypesMapper(ILogger logger, string connectionString)
        {
            this.Initialize(logger, connectionString);
        }

        public byte GetCounterpartyCode(Counterparty counterparty)
        {
            return this._counterpartyMapings[(int) counterparty];
        }

        public byte GetSymbolCode(Symbol symbol)
        {
            return this._symbolMappings[(int)symbol];
        }

        public byte GetOrderTypeCode(OrderType orderType)
        {
            return this._orderTypeMapings[(int)orderType];
        }

        public byte GetOrderTimeInForceCode(TimeInForce timeInForce)
        {
            return this._timeInForceMapings[(int)timeInForce];
        }

        public byte GetDealDirectionCode(DealDirection dealDirection)
        {
            return this._dealDirectionMapings[(int)dealDirection];
        }

        public byte GetPegTypeCode(PegType pegType)
        {
            return this._pegTypeMapings[(int)pegType];
        }

        private void Initialize(ILogger logger, string connectionString)
        {
            //let's not keep those around after initialization
            int DB_RETRY_COUNT = 3;

            Dictionary<string, byte> dbIdMappings = null;

            if (!AdoNetHelpers.RetrySqlOperation(() => dbIdMappings = ReadDbEnumMappings(connectionString, "[dbo].[GetDbMappingsForSymbols_SP]"), "reading Integrator Symbols", logger,
                                            DB_RETRY_COUNT, false))
                throw new Exception("Couldn't successfully read Integrator Symbols from backend");

            this.CreateEnumTypeMappings(dbIdMappings, _symbolMappings, Symbol.Values);


            if (!AdoNetHelpers.RetrySqlOperation(() => dbIdMappings = ReadDbEnumMappings(connectionString, "[dbo].[GetDbMappingsForCounterparty_SP]"), "reading Integrator Counterparties", logger,
                                            DB_RETRY_COUNT, false))
                throw new Exception("Couldn't successfully read Integrator Counterparties from backend");

            this.CreateEnumTypeMappings(dbIdMappings, _counterpartyMapings, Counterparty.Values);


            if (!AdoNetHelpers.RetrySqlOperation(() => dbIdMappings = ReadDbEnumMappings(connectionString, "[dbo].[GetDbMappingsForOrderType_SP]"), "reading Integrator OrderTypes", logger,
                                            DB_RETRY_COUNT, false))
                throw new Exception("Couldn't successfully read Integrator OrderTypes from backend");

            this.CreateEnumTypeMappings(dbIdMappings, _orderTypeMapings, Enum.GetValues(typeof(OrderType)).Cast<OrderType>());


            if (!AdoNetHelpers.RetrySqlOperation(() => dbIdMappings = ReadDbEnumMappings(connectionString, "[dbo].[GetDbMappingsForOrderTimeInForce_SP]"), "reading Integrator OrderTimeInForces", logger,
                                            DB_RETRY_COUNT, false))
                throw new Exception("Couldn't successfully read Integrator OrderTimeInForces from backend");

            this.CreateEnumTypeMappings(dbIdMappings, _timeInForceMapings, Enum.GetValues(typeof(TimeInForce)).Cast<TimeInForce>());


            if (!AdoNetHelpers.RetrySqlOperation(() => dbIdMappings = ReadDbEnumMappings(connectionString, "[dbo].[GetDbMappingsForDealDirection_SP]"), "reading Integrator DealDirections", logger,
                                            DB_RETRY_COUNT, false))
                throw new Exception("Couldn't successfully read Integrator DealDirections from backend");

            this.CreateEnumTypeMappings(dbIdMappings, _dealDirectionMapings, Enum.GetValues(typeof(DealDirection)).Cast<DealDirection>());


            if (!AdoNetHelpers.RetrySqlOperation(() => dbIdMappings = ReadDbEnumMappings(connectionString, "[dbo].[GetDbMappingsForPegType_SP]"), "reading Integrator PegTypes", logger,
                                            DB_RETRY_COUNT, false))
                throw new Exception("Couldn't successfully read Integrator PegTypes from backend");

            this.CreateEnumTypeMappings(dbIdMappings, _pegTypeMapings, Enum.GetValues(typeof(PegType)).Cast<PegType>());
        }

        private Dictionary<string, byte> ReadDbEnumMappings(string sqlConnectionString, string spName)
        {
            Dictionary<string, byte> mappingPairs = new Dictionary<string, byte>();

            using (SqlConnection sqlConnection = new SqlConnection(sqlConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand getMappingsCommand = new SqlCommand(spName, sqlConnection))
                {
                    getMappingsCommand.CommandType = CommandType.StoredProcedure;

                    using (var reader = getMappingsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            mappingPairs[reader.GetString(reader.GetOrdinal("EnumName"))] =
                                reader.GetByte(reader.GetOrdinal("DbId"));
                        }
                    }
                }
            }

            return mappingPairs;
        }

        private void CreateEnumTypeMappings<T>(Dictionary<string, byte> dbTypeMappings, byte[] integratorTypeMappings, IEnumerable<T> integratorEnumValues)
        {
            foreach (T integratorEnumValue in integratorEnumValues)
            {
                byte dBEnumId;
                if (dbTypeMappings.TryGetValue(integratorEnumValue.ToString(), out dBEnumId))
                {
                    //Dirty workaround - defer the cast operator resolution call to runtime
                    integratorTypeMappings[(int) (dynamic)integratorEnumValue] = dBEnumId;
                }
                else
                {
                    throw new Exception(string.Format("Integrator DB is missing a id for enum value: {0} (type: {1})",
                                                      integratorEnumValue, integratorEnumValue.GetType()));
                }
            }
        }
    }
}
