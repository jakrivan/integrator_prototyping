﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.DAL.DataCollection
{
    //It is good idea to explicitly define Pack, as default value of 0 means 'platform specific'
    // We want this to be universal accross platforms
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class ToBSerializationEntry 
#if POOLS_INSTRUMENTATION
        : Kreslik.Integrator.LowLatencyUtils.TimedObjectBase
#endif
    {
        //Let's convert to ET time in destination process - as it requires quite lot of memory garbage

        //Be carefull about DateTime struct - as it has LayoutKind.Auto specified - which negates LayoutKind.Sequential
        // on enclosing type
        // This is UNDOCUMENTED, but well known: http://stackoverflow.com/questions/4132533/why-does-layoutkind-sequential-work-differently-if-a-struct-contains-a-datetime/21960547#21960547

        private byte _dataProviderID;
        private byte _fXPairID;
        private long _tickTimeUtcTicks;
        private double _bid;
        private double _ask;
        private int _bidSize;
        private int _askSize;
        private int _sourceFileID;
        private byte _bidStreamProviderId;
        private byte _offerStreamProviderId;
        private long _bidTimeUtcTicks;
        private long _offerTimeUtcTicks;


        public byte DataProviderID { get { return this._dataProviderID; } }
        public byte FXPairID { get { return this._fXPairID; } }
        public DateTime TickTimeUtc { get { return new DateTime(this._tickTimeUtcTicks); } }
        public double Bid { get { return this._bid; } }
        public double Ask { get { return this._ask; } }
        public int BidSize { get { return this._bidSize; } }
        public int AskSize { get { return this._askSize; } }
        public int SourceFileID { get { return this._sourceFileID; } }
        public byte BidStreamProviderId { get { return this._bidStreamProviderId; } }
        public byte OfferStreamProviderId { get { return this._offerStreamProviderId; } }
        public DateTime BidTimeUtc { get { return new DateTime(this._bidTimeUtcTicks); } }
        public DateTime OfferTimeUtc { get { return new DateTime(this._offerTimeUtcTicks); } }

        public void SetNewData(byte dataProviderID, byte fXPairID, DateTime tickTimeUtc, double bid, double ask, int bidSize,
                               int askSize, int sourceFileID, byte bidStreamProviderId, byte offerStreamProviderId,
                               DateTime bidTimeUtc, DateTime offerTimeUtc)
        {
            this._dataProviderID = dataProviderID;
            this._fXPairID = fXPairID;
            this._tickTimeUtcTicks = tickTimeUtc.Ticks;
            this._bid = bid;
            this._ask = ask;
            this._bidSize = bidSize;
            this._askSize = askSize;
            this._sourceFileID = sourceFileID;
            this._bidStreamProviderId = bidStreamProviderId;
            this._offerStreamProviderId = offerStreamProviderId;
            this._bidTimeUtcTicks = bidTimeUtc.Ticks;
            this._offerTimeUtcTicks = offerTimeUtc.Ticks;
        }
    }
}
