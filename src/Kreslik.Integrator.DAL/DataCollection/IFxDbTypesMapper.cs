﻿using System;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.DAL.DataCollection
{
    public interface IDbTypesMappers
    {
        IFxDbTypesMapper FxDbTypesMapper { get; }
        IIntegratorDbTypesMapper IntegratorDbTypesMapper { get; }
    }

    public class DbTypesMappers: IDbTypesMappers
    {
        public DbTypesMappers(IFxDbTypesMapper fxDbTypesMapper, IIntegratorDbTypesMapper integratorDbTypesMapper)
        {
            this.FxDbTypesMapper = fxDbTypesMapper;
            this.IntegratorDbTypesMapper = integratorDbTypesMapper;
        }

        public IFxDbTypesMapper FxDbTypesMapper { get; private set; }
        public IIntegratorDbTypesMapper IntegratorDbTypesMapper { get; private set; }
    }

    public interface IFxDbTypesMapper
    {
        int DataProviderId { get; }
        int SourceFileId { get; }
        byte GetFxDdSymbolId(Symbol symbol);
        byte GetFxDdLiquidityProviderStreamId(Counterparty counterparty);
        long ConvertTimeToTickTime(DateTime time, bool convertInputUtcToEt);
        bool GetFxSideCode(PriceSide priceSide);
        bool? GetFxSideCode(PriceSide? priceSide);
        bool? GetFxTradeSideCode(TradeSide? tradeSide);
    }

    public interface IIntegratorDbTypesMapper
    {
        byte GetCounterpartyCode(Counterparty counterparty);
        byte GetSymbolCode(Symbol symbol);
        byte GetOrderTypeCode(OrderType orderType);
        byte GetOrderTimeInForceCode(TimeInForce timeInForce);
        byte GetDealDirectionCode(DealDirection dealDirection);
        byte GetPegTypeCode(PegType pegType);
    }
}
