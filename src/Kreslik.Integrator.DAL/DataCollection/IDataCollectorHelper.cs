﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.DAL.DataCollection
{
    public interface IDataCollectorHelper<in T>
    {
        string DestinationTableName { get; }
        System.Data.DataTable CreateInitializedDataTable(string tableName);
        void AddDataRow(T item, System.Data.DataTable table);
    }
}
