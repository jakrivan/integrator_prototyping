﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.DAL.DataCollection
{
    public interface IBytesSender
    {
        void SentByteSegment(BufferSegmentItem bufferSegmentItem);
        void Flush();
        void EndSending();
    }

    public class FileBytesSender : IBytesSender
    {
        private const long BYTES_IN_GB = 1073741824;
        private readonly long _availableBytesThreshold = (long)
                (BYTES_IN_GB * CommonSettings.CommonBehavior.Logging.AvailableDiskSpaceThresholdToStopMDLogging_GB * 0.75);
        private readonly long _availableBytesThresholdToStartLogging =
            BYTES_IN_GB * (CommonSettings.CommonBehavior.Logging.AvailableDiskSpaceThresholdToStopMDLogging_GB + 1);
        private bool _isLowDiskSpace = false;

        private string _fileNamePrefix;
        private string _destinationFolder;
        private DriveInfo _driveInfo;
        private int _fileId = 0;
        private int _messagesCount = 0;
        private FileStream _stream;
        private ILogger _logger;

        public FileBytesSender(ILogger logger, string fileNamePrefix, string destinationFolder)
        {
            this._logger = logger;
            this._fileNamePrefix = string.Format("{0}_{1}{2}_{3}",
                fileNamePrefix, DateTime.UtcNow.ToString("yyMMdd"),
                ByteUtils.ConvertToBase(DateTime.UtcNow.Hour*60 + DateTime.UtcNow.Minute, true),
                System.Diagnostics.Process.GetCurrentProcess().Id);
                                                 
            this.PrepareDestination(destinationFolder);
        }

        private void PrepareDestination(string destinationFolder)
        {
            if (!Path.IsPathRooted(destinationFolder))
            {
                destinationFolder =
                    Path.Combine(
                        System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                        destinationFolder);
            }

            if (!Directory.Exists(destinationFolder))
            {
                Directory.CreateDirectory(destinationFolder);
            }

            this._destinationFolder = destinationFolder;
            _driveInfo = new DriveInfo(Directory.GetDirectoryRoot(destinationFolder));
            //force recreation of new file
            this.Flush();
        }

        /// <summary>
        /// Must be called from the same thread as SentByteSegment method
        /// </summary>
        public void Flush()
        {
            if (_stream != null && _messagesCount > 0)
            {
                this._logger.Log(LogLevel.Info, "Closing stream [{0}] with {1} messages", _stream.Name, _messagesCount);
                CloseAndRenameFile(_stream);
            }

            //Refresh low disk space condition
            if (_isLowDiskSpace)
            {
                if (this.GetAvailableFreeBytes() > _availableBytesThresholdToStartLogging)
                {
                    _isLowDiskSpace = false;
                    this._logger.Log(LogLevel.Warn, "Available disk space is {0}, so resuming data collection", this.GetAvailableFreeBytes());
                }
            }
            else
            {
                if (this.GetAvailableFreeBytes() < _availableBytesThreshold)
                {
                    _isLowDiskSpace = true;
                    this._logger.Log(LogLevel.Fatal, "Available disk space is {0}, so stopping data collection", this.GetAvailableFreeBytes());
                }
            }

            //And recreate stream if possible
            if (_isLowDiskSpace)
            {
                _stream = null;
            }
            else
            {
                this._messagesCount = 0;
                _stream = File.Open(
                    System.IO.Path.Combine(this._destinationFolder,
                                           string.Format("{0}_{1:00000}{2}", _fileNamePrefix, this._fileId++,
                                                         FileBytesConstants.TEMP_FILE_EXTENSION)),
                    FileMode.CreateNew,
                    FileAccess.Write,
                    FileShare.None);
            }
        }

        private void CloseAndRenameFile(FileStream fs)
        {
            string oldFileName = fs.Name;
            try
            {
                fs.Close();
                File.Move(oldFileName, oldFileName.Replace(FileBytesConstants.TEMP_FILE_EXTENSION, FileBytesConstants.COMUNICATION_FILE_EXTENSION));
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Error, e, "Exception during renaming file {0}", oldFileName);
            }
        }

        private long GetAvailableFreeBytes()
        {
            return _driveInfo.AvailableFreeSpace;
        }

        private readonly byte[] _messagesSeparator = new byte[] { byte.MaxValue, byte.MaxValue };

        public void SentByteSegment(BufferSegmentItem bufferSegmentItem)
        {
            Stream stream = this._stream;
            if (!_isLowDiskSpace && stream != null)
            {
                stream.Write(bufferSegmentItem.Buffer, bufferSegmentItem.ContentStart, bufferSegmentItem.ContentSize);
                stream.Write(_messagesSeparator, 0, _messagesSeparator.Length);
            }

            this._messagesCount++;
        }


        public void EndSending()
        {
            FileStream previousStream = _stream;
            _stream = null;
            if (previousStream != null)
            {
                //Thread.Sleep(500);
                CloseAndRenameFile(previousStream);
            }
        }
    }
}
