﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.DAL.DataCollection
{
    //It is good idea to explicitly define Pack, as default value of 0 means 'platform specific'
    // We want this to be universal accross platforms
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class MarketDataSerializationEntry
#if POOLS_INSTRUMENTATION        
        : TimedObjectBase
#endif
    {
        //We need to define all data as fields, as autoproperties cannot have MarshalAs attributes
        // and order of auto-implemented fields is not guaranteed

        //Nullable types are unknown to marshaler (unmanaged world) - so we need to split them into two fields

        //Be carefull about DateTime struct - as it has LayoutKind.Auto specified - which negates LayoutKind.Sequential
        // on enclosing type
        // This is UNDOCUMENTED, but well known: http://stackoverflow.com/questions/4132533/why-does-layoutkind-sequential-work-differently-if-a-struct-contains-a-datetime/21960547#21960547

        private byte _fxPairId;
        //1 byte boolean, otherwise boolean is 4 bytes!
        [MarshalAs(UnmanagedType.I1)]
        private bool _isSideIdNull;
        [MarshalAs(UnmanagedType.I1)]
        private bool _sideId;
        [MarshalAs(UnmanagedType.I1)]
        private bool _isPriceNull;
        private decimal _price;
        [MarshalAs(UnmanagedType.I1)]
        private bool _isSizeNull;
        private decimal _size;
        private byte _counterpartyId;
        [MarshalAs(UnmanagedType.I1)]
        private bool _isTradeSideIdNull;
        [MarshalAs(UnmanagedType.I1)]
        private bool _tradeSideId;
        [MarshalAs(UnmanagedType.I1)]
        private bool _isCounterpartySentTimeNull;
        private long _counterpartySentTimeUtcTicks;
        private long _integratorReceivedTimeUtcTicks;
        //We want to store this as fixed size inlined ASCII string
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = PriceObject.MAX_CTP_IDENTITY_LENGTH)]
        //Be carefull - this is not a fixed inlined array - it is actually stored elsewhere, just Marshaller inlines it
        private byte[] _counterpartyPriceIdentity;
        //following won't work correctly due to nonblittable types - decimals, bools, DateTimes ...
        //private FixedByteArray64 _counterpartyPriceIdentity;
        private Guid _integratorPriceIdentity;
        [MarshalAs(UnmanagedType.I1)]
        private bool _isMinimumSizeNull;
        private decimal _minimumSize;
        [MarshalAs(UnmanagedType.I1)]
        private bool _isGranularityNull;
        private decimal _granularity;
        private byte _recordTypeId;


        public byte FXPairId { get { return this._fxPairId; } }
        public bool? SideId { get { return this._isSideIdNull ? (bool?) null : this._sideId; } }
        public object SideIdForDbUpload { get { return this._isSideIdNull ? DBNull.Value : (object)this._sideId; } }
        public decimal? Price { get { return this._isPriceNull ? (decimal?)null : this._price; } }
        public object PriceForDbUpload { get { return this._isPriceNull ? DBNull.Value : (object)this._price; } }
        public decimal? Size { get { return this._isSizeNull ? (decimal?)null : this._size; } }
        public object SizeForDbUpload { get { return this._isSizeNull ? DBNull.Value : (object)this._size; } }
        public byte CounterpartyId { get { return this._counterpartyId; } }
        public bool? TradeSideId { get { return this._isTradeSideIdNull ? (bool?)null : this._tradeSideId; } }
        public object TradeSideIdForDbUpload { get { return this._isTradeSideIdNull ? DBNull.Value : (object)this._tradeSideId; } }
        public DateTime? CounterpartySentTimeUtc { get { return this._isCounterpartySentTimeNull ? (DateTime?)null : new DateTime(this._counterpartySentTimeUtcTicks); } }
        public object CounterpartySentTimeUtcForDbUpload { get { return this._isCounterpartySentTimeNull ? DBNull.Value : (object)this._counterpartySentTimeUtcTicks; } }
        public DateTime IntegratorReceivedTimeUtc { get { return new DateTime(this._integratorReceivedTimeUtcTicks); } }
        /// <summary>
        /// Returns string representation of internal bytes for this field
        /// WARNING! this allocates temporary memory on each call - should be used only in DC process
        /// </summary>
        public string CounterpartyPriceIdentity
        {
            get { return Encoding.ASCII.GetString(this._counterpartyPriceIdentity).TrimEnd('\0'); }
        }
        public Guid IntegratorPriceIdentity { get { return this._integratorPriceIdentity; } }
        public decimal? MinimumSize { get { return this._isMinimumSizeNull ? (decimal?)null : this._minimumSize; } }
        public object MinimumSizeForDbUpload { get { return this._isMinimumSizeNull ? DBNull.Value : (object)this._minimumSize; } }
        public decimal? Granularity { get { return this._isGranularityNull ? (decimal?)null : this._granularity; } }
        public object GranularityForDbUpload { get { return this._isGranularityNull ? DBNull.Value : (object)this._granularity; } }
        public byte RecordTypeId { get { return this._recordTypeId; } }

        //public MarketDataSerializationEntry()
        //{
        //    this._counterpartyPriceIdentity = new byte[MAX_CTP_IDENTITY_LENGTH];
        //}

        //public MarketDataSerializationEntry(MarketDataSerializationEntry original)
        //{
        //    this._counterpartyPriceIdentity = new byte[MAX_CTP_IDENTITY_LENGTH];

        //    this._integratorPriceIdentity = original._integratorPriceIdentity;
        //    Buffer.BlockCopy(original._counterpartyPriceIdentity, 0, this._counterpartyPriceIdentity, 0, MAX_CTP_IDENTITY_LENGTH);
        //}

        public void SetNewData(byte fxPairId, bool? sideId, decimal? price, decimal? size, byte counterpartyId,
                               bool? tradeSideId, DateTime? counterpartySentTimeUtc,
                               DateTime integratorReceivedTimeUtc, byte[] counterpartyPriceIdentity,
                               Guid integratorPriceIdentity, decimal? minimumSize, decimal? granularity,
                               byte recordTypeId)
        {
            //WARNING! this method needs to reset whole internal state as objects are reused

            this._fxPairId = fxPairId;

            if (sideId.HasValue)
            {
                this._isSideIdNull = false;
                this._sideId = sideId.Value;
            }
            else
                this._isSideIdNull = true;

            if (price.HasValue)
            {
                this._isPriceNull = false;
                this._price = price.Value;
            }
            else
                this._isPriceNull = true;

            if (size.HasValue)
            {
                this._isSizeNull = false;
                this._size = size.Value;
            }
            else
                this._isSizeNull = true;

            this._counterpartyId = counterpartyId;

            if (tradeSideId.HasValue)
            {
                this._isTradeSideIdNull = false;
                this._tradeSideId = tradeSideId.Value;
            }
            else
                this._isTradeSideIdNull = true;

            if (counterpartySentTimeUtc.HasValue)
            {
                this._isCounterpartySentTimeNull = false;
                this._counterpartySentTimeUtcTicks = counterpartySentTimeUtc.Value.Ticks;
            }
            else
                this._isCounterpartySentTimeNull = true;

            this._integratorReceivedTimeUtcTicks = integratorReceivedTimeUtc.Ticks;
            this._integratorPriceIdentity = integratorPriceIdentity;

            if (minimumSize.HasValue)
            {
                this._isMinimumSizeNull = false;
                this._minimumSize = minimumSize.Value;
            }
            else
                this._isMinimumSizeNull = true;

            if (granularity.HasValue)
            {
                this._isGranularityNull = false;
                this._granularity = granularity.Value;
            }
            else
                this._isGranularityNull = true;

            this._recordTypeId = recordTypeId;

            //ByteConverter.CopyAsciiStringToByteBuffer(counterpartyPriceIdentity, this._counterpartyPriceIdentity, 0, PriceObject.MAX_CTP_IDENTITY_LENGTH);
            this._counterpartyPriceIdentity = counterpartyPriceIdentity;
        }
    }
}
