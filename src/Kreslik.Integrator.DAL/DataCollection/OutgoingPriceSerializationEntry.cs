﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.DAL.DataCollection
{
        //It is good idea to explicitly define Pack, as default value of 0 means 'platform specific'
    // We want this to be universal accross platforms
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class OutgoingPriceSerializationEntry
#if POOLS_INSTRUMENTATION
        : Kreslik.Integrator.LowLatencyUtils.TimedObjectBase
#endif
    {
        //Be carefull about DateTime struct - as it has LayoutKind.Auto specified - which negates LayoutKind.Sequential
        // on enclosing type
        // This is UNDOCUMENTED, but well known: http://stackoverflow.com/questions/4132533/why-does-layoutkind-sequential-work-differently-if-a-struct-contains-a-datetime/21960547#21960547

        private byte _fxPairId;
        private byte _counterpartyId;
        //1 byte boolean, otherwise boolean is 4 bytes!
        [MarshalAs(UnmanagedType.I1)] 
        private bool _isCancelationEntry;
        [MarshalAs(UnmanagedType.I1)] 
        private bool _sideId;
        private decimal _price;
        private decimal _size;
        [MarshalAs(UnmanagedType.I1)]
        private bool _isTradingSystemIdNull;
        private int _tradingSystemId;
        private long _integratorSentTimeUtcTicks;
        //We want to store this as fixed size inlined ASCII string
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = StreamingPrice.MAX_INTEGRATOR_IDENTITY_LENGTH)]
        //Be carefull - this is not a fixed inlined array - it is actually stored elsewhere, just Marshaller inlines it
        private byte[] _integratorPriceIdentity;


        public byte FXPairId { get { return this._fxPairId; } }
        public byte CounterpartyId { get { return this._counterpartyId; } }
        public bool SideId { get { return this._sideId; } }
        public decimal? Price { get { return this._isCancelationEntry ? (decimal?)null : this._price; } }
        public object PriceForDbUpload { get { return this._isCancelationEntry ? DBNull.Value : (object)this._price; } }
        public decimal? Size { get { return this._isCancelationEntry ? (decimal?)null : this._size; } }
        public object SizeForDbUpload { get { return this._isCancelationEntry ? DBNull.Value : (object)this._size; } }
        public int? TradingSystemId { get { return this._isTradingSystemIdNull ? (int?)null : this._tradingSystemId; } }
        public object TradingSystemIdForDbUpload { get { return this._isTradingSystemIdNull ? DBNull.Value : (object)this._tradingSystemId; } }
        public DateTime IntegratorSentTimeUtc { get { return new DateTime(this._integratorSentTimeUtcTicks); } }
        /// <summary>
        /// Returns string representation of internal bytes for this field
        /// WARNING! this allocates temporary memory on each call - should be used only in DC process
        /// </summary>
        public string IntegratorPriceIdentity
        {
            get { return this._isCancelationEntry ? null : Encoding.ASCII.GetString(this._integratorPriceIdentity).TrimEnd('\0'); }
        }

        public void SetNewCancelData(byte fxPairId, byte counterpartyId, bool sideId,
            int? tradingSystemId, DateTime integratorSentTimeUtc)
        {
            //WARNING! this method needs to reset whole internal state as objects are reused

            this._isCancelationEntry = true;

            this._fxPairId = fxPairId;
            this._counterpartyId = counterpartyId;
            this._sideId = sideId;

            if (tradingSystemId.HasValue)
            {
                this._isTradingSystemIdNull = false;
                this._tradingSystemId = tradingSystemId.Value;
            }
            else
                this._isTradingSystemIdNull = true;

            this._integratorSentTimeUtcTicks = integratorSentTimeUtc.Ticks;
        }

        public void SetNewData(byte fxPairId, byte counterpartyId, bool sideId, decimal price, decimal size,
            int tradingSystemId, DateTime integratorSentTimeUtc,
            byte[] integratorPriceIdentity)
        {
            //WARNING! this method needs to reset whole internal state as objects are reused

            this._isCancelationEntry = false;
            this._fxPairId = fxPairId;
            this._counterpartyId = counterpartyId;
            this._sideId = sideId;
            this._price = price;
            this._size = size;
            this._isTradingSystemIdNull = false;
            this._tradingSystemId = tradingSystemId;
            this._integratorSentTimeUtcTicks = integratorSentTimeUtc.Ticks;

            this._integratorPriceIdentity = integratorPriceIdentity;
        }
    }
}
