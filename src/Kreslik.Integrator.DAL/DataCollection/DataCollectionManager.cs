﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.DAL.DataCollection
{
    public static class DataCollectionManager
    {
        private static ToBDataCollector _toBDataCollector;
        private static MarketDataCollector _marketDataCollector;
        private static OutgoingPriceDataCollector _outgoingPriceDataCollector;

        static DataCollectionManager()
        {
            FxDbTypesMapper fxTypesMapper =
                new FxDbTypesMapper(
                    DALBehavior.Sections.DataCollectionConnection.ConnectionString,
                    LogFactory.Instance.GetLogger(null), DALBehavior.Sections.OhterSettings.DataCollection.DataProviderId,
                    DALBehavior.Sections.OhterSettings.DataCollection.SourceFileId);

            IntegratorDbTypesMapper integratorDbTypesMapper =
                new IntegratorDbTypesMapper(LogFactory.Instance.GetLogger(null),
                                            DALBehavior.Sections.IntegratorConnection.ConnectionString);

            DbTypesMappers dbTypesMappers = new DbTypesMappers(fxTypesMapper, integratorDbTypesMapper);

            _toBDataCollector = CreateToBDataCollector(dbTypesMappers);
            _marketDataCollector = CreateMarketDataCollector(dbTypesMappers);
            _outgoingPriceDataCollector = CreateOutgoingPriceDataCollector(dbTypesMappers);
        }

        private static ToBDataCollector CreateToBDataCollector(DbTypesMappers dbTypesMappers)
        {
            ToBDataCollector tobCollector;

            if (ToBDataCollectionEnabled)
            {
                ILogger dataCollectionLogger = LogFactory.Instance.GetLogger("ToBDataCollection");

                tobCollector = new ToBDataCollector(dbTypesMappers, dataCollectionLogger,
                                                    DALBehavior.Sections.OhterSettings.DataCollection,
                                                    Counterparty.ValuesCount/4, Counterparty.ValuesCount);
            }
            else
            {
                tobCollector = ToBDataCollector.NullDataCollector;
            }

            return tobCollector;
        }

        private static MarketDataCollector CreateMarketDataCollector(DbTypesMappers dbTypesMappers)
        {
            MarketDataCollector pricesCollector;

            if (MarketDataCollectionEnabled)
            {
                ILogger dataCollectionLogger = LogFactory.Instance.GetLogger("MarketDataCollection");

                pricesCollector = new MarketDataCollector(dbTypesMappers, dataCollectionLogger,
                                                    DALBehavior.Sections.OhterSettings.DataCollection,
                                                    Counterparty.ValuesCount / 2, Counterparty.ValuesCount * 4);
            }
            else
            {
                pricesCollector = MarketDataCollector.NullDataCollector;
            }

            return pricesCollector;
        }

        private static OutgoingPriceDataCollector CreateOutgoingPriceDataCollector(DbTypesMappers dbTypesMappers)
        {
            OutgoingPriceDataCollector pricesCollector;

            if (MarketDataCollectionEnabled)
            {
                ILogger dataCollectionLogger = LogFactory.Instance.GetLogger("OutPricesDataCollection");

                pricesCollector = new OutgoingPriceDataCollector(dbTypesMappers, dataCollectionLogger,
                                                    DALBehavior.Sections.OhterSettings.DataCollection,
                                                    4, 16);
            }
            else
            {
                pricesCollector = OutgoingPriceDataCollector.NullDataCollector;
            }

            return pricesCollector;
        }

        public static ToBDataCollector ToBDataCollector
        {
            get { return _toBDataCollector; }
        }

        public static MarketDataCollector MarketDataCollector
        {
            get { return _marketDataCollector; }
        }

        public static OutgoingPriceDataCollector OutgoingPriceDataCollector
        {
            get { return _outgoingPriceDataCollector; }
        }

        public static bool ToBDataCollectionEnabled { get { return DALBehavior.Sections.DataCollectionEnabling.EnableToBDataCollection; } }

        public static bool MarketDataCollectionEnabled { get { return DALBehavior.Sections.DataCollectionEnabling.EnableMarketDataCollection; } }
    }
}
