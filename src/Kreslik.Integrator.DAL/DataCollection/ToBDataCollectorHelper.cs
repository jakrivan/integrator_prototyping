﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.DAL.DataCollection
{
    public class ToBDataCollectorHelper : IDataCollectorHelper<ToBSerializationEntry>
    {
        public string DestinationTableName
        {
            get { return "Tick"; }
        }

        public System.Data.DataTable CreateInitializedDataTable(string tableName)
        {
            DataTable dtable = new DataTable(tableName);

            dtable.Columns.Add("DataProviderID", typeof(byte));
            dtable.Columns.Add("FXPairID", typeof(byte));
            dtable.Columns.Add("TickTime", typeof(long));
            dtable.Columns.Add("Bid", typeof(double));
            dtable.Columns.Add("Ask", typeof(double));
            dtable.Columns.Add("BidSize", typeof(int));
            dtable.Columns.Add("AskSize", typeof(int));
            dtable.Columns.Add("SourceFileID", typeof(int));
            //dtable.Columns.Add("EndOfContinuousData", typeof(bool));
            dtable.Columns.Add("BidStreamProviderId", typeof(byte));
            dtable.Columns.Add("OfferStreamProviderId", typeof(byte));
            dtable.Columns.Add("BidTimeStamp", typeof(long));
            dtable.Columns.Add("OfferTimeStamp", typeof(long));

            return dtable;
        }

        public void AddDataRow(ToBSerializationEntry item, System.Data.DataTable table)
        {
            table.Rows.Add(
                item.DataProviderID,
                item.FXPairID,
                FxDbTypesMapper.ConvertTimeToTickTimeStatic(item.TickTimeUtc, true),
                item.Bid,
                item.Ask,
                item.BidSize,
                item.AskSize,
                item.SourceFileID,
                //toBItemUploadInfo.IsLastInContinuousData,
                item.BidStreamProviderId,
                item.OfferStreamProviderId,
                FxDbTypesMapper.ConvertTimeToTickTimeStatic(item.BidTimeUtc, true),
                FxDbTypesMapper.ConvertTimeToTickTimeStatic(item.OfferTimeUtc, true));
        }
    }
}
