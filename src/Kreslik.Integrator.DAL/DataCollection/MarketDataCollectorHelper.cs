﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.DAL.DataCollection
{
    public class MarketDataCollectorHelper : IDataCollectorHelper<MarketDataSerializationEntry>
    {
        public string DestinationTableName
        {
            get { return "MarketData"; }
        }

        public System.Data.DataTable CreateInitializedDataTable(string tableName)
        {
            DataTable dtable = new DataTable(tableName);

            dtable.Columns.Add("FXPairId", typeof (byte));
            dtable.Columns.Add("SideId", typeof (bool));
            dtable.Columns.Add("Price", typeof (decimal));
            dtable.Columns.Add("Size", typeof (decimal));
            dtable.Columns.Add("CounterpartyId", typeof (byte));
            dtable.Columns.Add("TradeSideId", typeof (bool));
            dtable.Columns.Add("CounterpartySentTimeUtc", typeof (DateTime));
            dtable.Columns.Add("IntegratorReceivedTimeUtc", typeof (DateTime));
            dtable.Columns.Add("CounterpartyPriceIdentity", typeof (string));
            dtable.Columns.Add("IntegratorPriceIdentity", typeof (Guid));
            dtable.Columns.Add("MinimumSize", typeof (decimal));
            dtable.Columns.Add("Granularity", typeof (decimal));
            dtable.Columns.Add("RecordTypeId", typeof (byte));

            return dtable;
        }

        public void AddDataRow(MarketDataSerializationEntry item, System.Data.DataTable table)
        {
            string counterpartyIdentity = item.CounterpartyPriceIdentity;

            //table.Rows.Add(
            //            item.FXPairId,
            //            item.SideIdForDbUpload,
            //            item.PriceForDbUpload,
            //            item.SizeForDbUpload,
            //            item.CounterpartyId,
            //            item.TradeSideIdForDbUpload,
            //            item.CounterpartySentTimeUtcForDbUpload,
            //            item.IntegratorReceivedTimeUtc,
            //            string.IsNullOrEmpty(counterpartyIdentity)
            //                ? DBNull.Value
            //                : (object)counterpartyIdentity,
            //            item.IntegratorPriceIdentity,
            //            item.MinimumSizeForDbUpload,
            //            item.GranularityForDbUpload, item.Granularity,
            //            item.RecordTypeId);

            table.Rows.Add(
                item.FXPairId,
                item.SideId ?? (object) DBNull.Value,
                item.Price ?? (object) DBNull.Value,
                item.Size ?? (object) DBNull.Value,
                item.CounterpartyId,
                item.TradeSideId ?? (object) DBNull.Value,
                item.CounterpartySentTimeUtc ?? (object) DBNull.Value,
                item.IntegratorReceivedTimeUtc,
                string.IsNullOrEmpty(counterpartyIdentity)
                    ? DBNull.Value
                    : (object) counterpartyIdentity,
                item.IntegratorPriceIdentity,
                item.MinimumSize ?? (object) DBNull.Value,
                item.Granularity ?? (object) DBNull.Value,
                item.RecordTypeId);
        }
    }
}
