﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.DAL.DataCollection
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class NewOrderExternalSerializationEntry
#if POOLS_INSTRUMENTATION        
        : TimedObjectBase
#endif
    {
        private const int INTERNAL_ORDER_ID_LENGTH = 45;
        private const int INTERNAL_CLIENT_ORDER_ID_LENGTH = 32;
        private const int FIX_MESSAGE_RAW_LENGTH = 1024;


        private long _externalOrderSentUtcTicks;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = INTERNAL_ORDER_ID_LENGTH)] 
        private byte[] _internalOrderId;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = INTERNAL_CLIENT_ORDER_ID_LENGTH)]
        private byte[] _internalClientOrderId;
        private byte _counterpartyId;
        private byte _symbolId;
        private byte _dealDirectionId;
        [MarshalAs(UnmanagedType.I1)]
        private bool _isQuoteReceivedToOrderSentInternalLatencyNull;
        private long _quoteReceivedToOrderSentInternalLatencyTicks;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = FIX_MESSAGE_RAW_LENGTH)]
        private byte[] _fixMessageRaw;
        private byte _orderTypeId;
        private byte _orderTimeInForceId;
        private decimal _requestedPrice;
        private decimal _requestedAmountBaseAbs;
        [MarshalAs(UnmanagedType.I1)]
        private bool _isIntegratorPriceNull;
        private Guid _integratorPriceIdentity;
        private DateTime _integratorPriceTimeUtc;
        [MarshalAs(UnmanagedType.I1)]
        private bool _isPegDataNull;
        private byte _pegTypeId;
        private decimal _pegDifference;


        public DateTime ExternalOrderSentUtc { get { return new DateTime(this._externalOrderSentUtcTicks); } }
        /// <summary>
        /// Returns string representation of internal bytes for this field
        /// WARNING! this allocates temporary memory on each call - should be used only in DC process
        /// </summary>
        public string InternalOrderId
        {
            get { return Encoding.ASCII.GetString(this._internalOrderId).TrimEnd('\0'); }
        }
        /// <summary>
        /// Returns string representation of internal bytes for this field
        /// WARNING! this allocates temporary memory on each call - should be used only in DC process
        /// </summary>
        public string InternalClientOrderId
        {
            get { return Encoding.ASCII.GetString(this._internalClientOrderId).TrimEnd('\0'); }
        }
        public byte CounterpartyId { get { return this._counterpartyId; } }
        public byte SymbolId { get { return this._symbolId; } }
        public byte DealDirectionId { get { return this._dealDirectionId; } }
        public TimeSpan? QuoteReceivedToOrderSentInternalLatency { get { return this._isQuoteReceivedToOrderSentInternalLatencyNull ? (TimeSpan?)null : new TimeSpan(this._quoteReceivedToOrderSentInternalLatencyTicks); } }
        /// <summary>
        /// Returns string representation of internal bytes for this field
        /// WARNING! this allocates temporary memory on each call - should be used only in DC process
        /// </summary>
        public string FixMessageRaw
        {
            get { return Encoding.ASCII.GetString(this._fixMessageRaw).TrimEnd('\0'); }
        }
        public byte OrderTypeId { get { return this._orderTypeId; } }
        public byte OrderTimeInForceId { get { return this._orderTimeInForceId; } }
        public decimal RequestedPrice { get { return this._requestedPrice; } }
        public decimal RequestedAmountBaseAbs { get { return this._requestedAmountBaseAbs; } }
        public Guid? IntegratorPriceIdentity { get { return this._isIntegratorPriceNull ? (Guid?)null : this._integratorPriceIdentity; } }
        public DateTime? IntegratorPriceTimeUtc { get { return this._isIntegratorPriceNull ? (DateTime?)null : this._integratorPriceTimeUtc; } }
        public byte? PegTypeId { get { return _isPegDataNull ? (byte?) null : this._pegTypeId; } }
        public decimal? PegDifference { get { return _isPegDataNull ? (decimal?)null : this._pegDifference; } }


        public void SetNewData(DateTime externalOrderSentUtc, string internalOrderId, string internalClientOrderId,
            byte counterpartyId, byte symbolId, byte dealDirectionId, TimeSpan? quoteReceivedToOrderSentInternalLatency,
            string fixMessageRaw, byte orderTypeId, byte orderTimeInForceId, decimal requestedPrice, decimal requestedAmountBaseAbs,
            Guid? integratorPriceIdentity, DateTime? integratorPriceTimeUtc, byte? pegTypeId, decimal? pegDifference)
        {
            //WARNING! this method needs to reset whole internal state as objects are reused

            this._externalOrderSentUtcTicks = externalOrderSentUtc.Ticks;
            ByteConverter.CopyAsciiStringToByteBuffer(internalOrderId, this._internalOrderId, 0, INTERNAL_ORDER_ID_LENGTH);
            ByteConverter.CopyAsciiStringToByteBuffer(internalClientOrderId, this._internalClientOrderId, 0, INTERNAL_CLIENT_ORDER_ID_LENGTH);
            this._counterpartyId = counterpartyId;
            this._symbolId = symbolId;
            this._dealDirectionId = dealDirectionId;
            if (quoteReceivedToOrderSentInternalLatency.HasValue)
            {
                this._isQuoteReceivedToOrderSentInternalLatencyNull = false;
                this._quoteReceivedToOrderSentInternalLatencyTicks = quoteReceivedToOrderSentInternalLatency.Value.Ticks;
            }
            else
                this._isQuoteReceivedToOrderSentInternalLatencyNull = true;
            ByteConverter.CopyAsciiStringToByteBuffer(fixMessageRaw, this._fixMessageRaw, 0, FIX_MESSAGE_RAW_LENGTH);
            this._orderTypeId = orderTypeId;
            this._orderTimeInForceId = orderTimeInForceId;
            this._requestedPrice = requestedPrice;
            this._requestedAmountBaseAbs = requestedAmountBaseAbs;
            if (integratorPriceIdentity.HasValue)
            {
                this._isIntegratorPriceNull = false;
                this._integratorPriceIdentity = integratorPriceIdentity.Value;
                this._integratorPriceTimeUtc = integratorPriceTimeUtc.Value;
            }
            else
                this._isIntegratorPriceNull = true;
            if (pegTypeId.HasValue)
            {
                this._isPegDataNull = false;
                this._pegTypeId = pegTypeId.Value;
                this._pegDifference = pegDifference.Value;
            }
            else
                this._isPegDataNull = true;
        }

        public void SetNewData(IIntegratorOrderExternal integratorOrderExternal, IIntegratorDbTypesMapper integratorDbTypesMapper)
        {
            this.SetNewData(
                integratorOrderExternal.IntegratorSentTimeUtc,
                integratorOrderExternal.UniqueInternalIdentity,
                integratorOrderExternal.Identity,
                integratorDbTypesMapper.GetCounterpartyCode(integratorOrderExternal.Counterparty),
                integratorDbTypesMapper.GetSymbolCode(integratorOrderExternal.OrderRequestInfo.Symbol),
                integratorDbTypesMapper.GetDealDirectionCode(integratorOrderExternal.OrderRequestInfo.Side),
                integratorOrderExternal.QuoteReceivedToOrderSentInternalLatency,
                integratorOrderExternal.OutgoingFixMessageRaw,
                integratorDbTypesMapper.GetOrderTypeCode(integratorOrderExternal.OrderRequestInfo.OrderType),
                integratorDbTypesMapper.GetOrderTimeInForceCode(integratorOrderExternal.OrderRequestInfo.TimeInForce),
                integratorOrderExternal.OrderRequestInfo.RequestedPrice,
                integratorOrderExternal.OrderRequestInfo.SizeBaseAbsInitial,
                integratorOrderExternal.OrderRequestInfo.IntegratorPriceIdentity == Guid.Empty
                    ? (Guid?) null
                    : integratorOrderExternal.OrderRequestInfo.IntegratorPriceIdentity,
                integratorOrderExternal.OrderRequestInfo.IntegratorPriceIdentity == Guid.Empty
                    ? (DateTime?) null
                    : integratorOrderExternal.OrderRequestInfo.IntegratorPriceReceivedUtc,
                integratorOrderExternal.OrderRequestInfo.OrderType == OrderType.Pegged
                    ? integratorDbTypesMapper.GetPegTypeCode(integratorOrderExternal.OrderRequestInfo.PegType)
                    : (byte?) null,
                integratorOrderExternal.OrderRequestInfo.OrderType == OrderType.Pegged
                    ? integratorOrderExternal.OrderRequestInfo.PegDifferenceBasePolAlwaysAdded
                    : (decimal?) null);
        }
    }
}

