﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.DAL.DataCollection
{
    public interface IDataCollectorSettings
    {
        int MaxBcpBatchSize { get; }
        int MaxInMemoryColectionSize { get; }
        TimeSpan MaxIntervalBetweenUploads { get; }
        TimeSpan MaxUploadTimeAfterWhichSerializationStarts { get; }
        TimeSpan MaxProcessingTimeAfterShutdownSignalled { get; }
        TimeSpan WaitIntervalBetweenBcpRetries { get; }
        TimeSpan WaitIntervalBeforeShutdownStartsSerialization { get; }
        string DataCollectionFolder { get; }
        List<Counterparty> ExcludedCounterparties { get; }
    }
}
