﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.DAL.DataCollection
{
    public class OutgoingPriceDataCollectorHelper : IDataCollectorHelper<OutgoingPriceSerializationEntry>
    {
        public string DestinationTableName
        {
            get { return "OutgoingSoftPrice"; }
        }

        public System.Data.DataTable CreateInitializedDataTable(string tableName)
        {
            DataTable dtable = new DataTable(tableName);

            dtable.Columns.Add("FXPairId", typeof(byte));
            dtable.Columns.Add("CounterpartyId", typeof(byte));
            dtable.Columns.Add("SideId", typeof(bool));
            dtable.Columns.Add("Price", typeof(decimal));
            dtable.Columns.Add("SizeBaseAbs", typeof(decimal));
            dtable.Columns.Add("TradingSystemId", typeof(int));
            dtable.Columns.Add("LayerOrderId", typeof(byte));
            dtable.Columns.Add("IntegratorSentTimeUtc", typeof(DateTime));
            dtable.Columns.Add("IntegratorPriceIdentity", typeof(string));

            return dtable;
        }

        public void AddDataRow(OutgoingPriceSerializationEntry item, System.Data.DataTable table)
        {
            string integratorPriceIdentity = item.IntegratorPriceIdentity;

            table.Rows.Add(
                item.FXPairId,
                item.CounterpartyId,
                item.SideId,
                item.PriceForDbUpload,
                item.SizeForDbUpload,
                item.TradingSystemIdForDbUpload,
                //Temporarily hardcoding LayerOrderId to 1
                1,
                item.IntegratorSentTimeUtc,
                string.IsNullOrEmpty(integratorPriceIdentity)
                    ? DBNull.Value
                    : (object)integratorPriceIdentity);
        }
    }
}
