﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.DAL.DataCollection
{
    public static class DataCollectorUploaderManager
    {
        private static IDataCollectorUploader _toBDataUploader;
        private static IDataCollectorUploader _marketDataUploader;
        private static IDataCollectorUploader _outPricesDataUploader;
        private static TimeSpan _maxProcessingTimeAfterShutdownSignalled;

        static DataCollectorUploaderManager()
        {
            ILogger dcTobLogger = LogFactory.Instance.GetLogger("DCToBUploader");
            ILogger dcMDLogger = LogFactory.Instance.GetLogger("DCMDUploader");
            ILogger outPricesLogger = LogFactory.Instance.GetLogger("OutPricesUploader");
            string dcConnectionString = DALBehavior.Sections.DataCollectionConnection.ConnectionString;
            var dcSettings = DALBehavior.Sections.OhterSettings.DataCollection;

            _maxProcessingTimeAfterShutdownSignalled = dcSettings.MaxProcessingTimeAfterShutdownSignalled;

            if (ToBDataCollectionEnabled)
            {
                _toBDataUploader = new ToBdataCollectorUploader(dcTobLogger, dcSettings, dcConnectionString);
            }
            else
            {
                _toBDataUploader = NullDataCollectionUploader.Instance;
            }

            if (MarketDataCollectionEnabled)
            {
                _marketDataUploader = new MDdataCollectorUploader(dcMDLogger, dcSettings, dcConnectionString);
                _outPricesDataUploader = new OutgoingPriceDataCollectorUploader(outPricesLogger, dcSettings, dcConnectionString);
            }
            else
            {
                _marketDataUploader = NullDataCollectionUploader.Instance;
                _outPricesDataUploader = NullDataCollectionUploader.Instance;
            }
        }

        public static TimeSpan MaxProcessingTimeAfterShutdownSignalled
        {
            get { return _maxProcessingTimeAfterShutdownSignalled; }
        }

        public static IDataCollectorUploader ToBDataUploader
        {
            get { return _toBDataUploader; }
        }

        public static IDataCollectorUploader MarketDataUploader
        {
            get { return _marketDataUploader; }
        }

        public static IDataCollectorUploader OutgoingPricesDataUploader
        {
            get { return _outPricesDataUploader; }
        }

        private static bool ToBDataCollectionEnabled { get { return DALBehavior.Sections.DataCollectionEnabling.EnableToBDataCollection; } }

        private static bool MarketDataCollectionEnabled { get { return DALBehavior.Sections.DataCollectionEnabling.EnableMarketDataCollection; } }
    }

    internal class ToBdataCollectorUploader: DataCollectorUploaderBase<ToBSerializationEntry>
    {
        internal ToBdataCollectorUploader(ILogger logger, IDataCollectorSettings dataCollectorSettings,
                                        string connectionString)
            : base(
                logger, new FileBytesReceiver<ToBSerializationEntry>(logger, Path.Combine(dataCollectorSettings.DataCollectionFolder, "ToB")),
                new ToBDataCollectorHelper(), dataCollectorSettings, connectionString)
        { }
    }

    internal class OutgoingPriceDataCollectorUploader : DataCollectorUploaderBase<OutgoingPriceSerializationEntry>
    {
        internal OutgoingPriceDataCollectorUploader(ILogger logger, IDataCollectorSettings dataCollectorSettings,
                                        string connectionString)
            : base(
                logger, new FileBytesReceiver<OutgoingPriceSerializationEntry>(logger, Path.Combine(dataCollectorSettings.DataCollectionFolder, "OutgoingPrice")),
                new OutgoingPriceDataCollectorHelper(), dataCollectorSettings, connectionString)
        { }
    }

    internal class MDdataCollectorUploader : DataCollectorUploaderBase<MarketDataSerializationEntry>
    {
        internal MDdataCollectorUploader(ILogger logger, IDataCollectorSettings dataCollectorSettings,
                                        string connectionString)
            : base(
                logger, new FileBytesReceiver<MarketDataSerializationEntry>(logger, Path.Combine(dataCollectorSettings.DataCollectionFolder, "MD")),
                new MarketDataCollectorHelper(), dataCollectorSettings, connectionString)
        { }
    }

    internal class NullDataCollectionUploader : IDataCollectorUploader
    {
        private Lazy<ManualResetEvent> _receivingDoneEvent = new Lazy<ManualResetEvent>(() => new ManualResetEvent(true));

        private NullDataCollectionUploader() { }
        private static IDataCollectorUploader _instance = new NullDataCollectionUploader();

        public static IDataCollectorUploader Instance
        {
            get { return _instance; }
        }

        public void StartProcessing()
        {
            //NOP
        }

        public void StopProcessing()
        {
            //NOP
        }

        public WaitHandle ReceivingDoneHandle
        {
            get { return _receivingDoneEvent.Value; }
        }
    }

    public interface IDataCollectorUploader
    {
        void StartProcessing();
        void StopProcessing();
        WaitHandle ReceivingDoneHandle { get; }
    }

    internal abstract class DataCollectorUploaderBase<T> : IDataCollectorUploader where T : class, new()
    {
        private ILogger _logger;
        private IBytesReceiver<T> _bytesReceiver;
        private IDataCollectorHelper<T> _dataCollectorHelper;
        private IDataCollectorSettings _dataCollectorSettings;
        private string _connectionString;
        private CancellationTokenSource _shuttingDownTokenSource = new CancellationTokenSource();
        private DataTable _sharedDataTable;

        protected DataCollectorUploaderBase(ILogger logger, IBytesReceiver<T> bytesReceiver, IDataCollectorHelper<T> dataCollectorHelper, IDataCollectorSettings dataCollectorSettings, string connectionString)
        {
            this._logger = logger;
            this._bytesReceiver = bytesReceiver;
            this._dataCollectorHelper = dataCollectorHelper;
            this._dataCollectorSettings = dataCollectorSettings;
            this._connectionString = connectionString;

            this.ValidateSettings(dataCollectorSettings, logger);
        }

        public void StartProcessing()
        {
            this._bytesReceiver.NewMessageAvailable += OnNewMessageAvailable;
            this._bytesReceiver.GapInContinuousMessageStream += OnGapInContinuousMessageStream;

            this._bytesReceiver.StartReceivingMessages();
        }

        public void StopProcessing()
        {
            _bytesReceiver.StopReceivingMessages();
            _shuttingDownTokenSource.CancelAfter(this._dataCollectorSettings.MaxProcessingTimeAfterShutdownSignalled);
        }

        public WaitHandle ReceivingDoneHandle
        {
            get { return this._bytesReceiver.ReceivingDoneHandle; }
        }

        private void ValidateSettings(IDataCollectorSettings collectorSettings, ILogger logger)
        {
            if (collectorSettings.MaxBcpBatchSize <= 0)
            {
                string errorString =
                    string.Format("MaxBcpBatchSize setting must be positive number, but passed value was {0}",
                                  collectorSettings.MaxBcpBatchSize);
                logger.Log(LogLevel.Fatal, errorString);
                throw new Exception(errorString);
            }

            if (collectorSettings.MaxInMemoryColectionSize <= collectorSettings.MaxBcpBatchSize)
            {
                string errorString =
                    string.Format(
                        "MaxInMemoryColectionSize setting must be greater than MaxBcpBatchSize. But MaxInMemoryColectionSize was: {0} and MaxBcpBatchSize was: {1}",
                        collectorSettings.MaxInMemoryColectionSize, collectorSettings.MaxBcpBatchSize);
                logger.Log(LogLevel.Fatal, errorString);
                throw new Exception(errorString);
            }

            if (collectorSettings.MaxUploadTimeAfterWhichSerializationStarts.Ticks <= 0)
            {
                string errorString =
                    string.Format(
                        "MaxUploadTimeAfterWhichSerializationStarts setting must be greater than 0.but passed value was {0}",
                        collectorSettings.MaxUploadTimeAfterWhichSerializationStarts);
                logger.Log(LogLevel.Fatal, errorString);
                throw new Exception(errorString);
            }
        }

        private void OnGapInContinuousMessageStream()
        {
            if (_sharedDataTable != null)
            {
                if (UploadDataTableToSqlServer(_sharedDataTable))
                {
                    this._bytesReceiver.DeleteMessagesBuffer();
                }
                _sharedDataTable = null;
            }
            else
            {
                this._bytesReceiver.DeleteMessagesBuffer();
            }
        }

        private int _tablesNum = 0;
        private void OnNewMessageAvailable(T message)
        {
            if (_sharedDataTable == null)
            {
                _sharedDataTable = _dataCollectorHelper.CreateInitializedDataTable(
                    string.Format("{0}_{1}", _dataCollectorHelper.DestinationTableName, _tablesNum++));
            }

            this._dataCollectorHelper.AddDataRow(message, _sharedDataTable);

            if (_sharedDataTable.Rows.Count > this._dataCollectorSettings.MaxBcpBatchSize)
            {
                if (UploadDataTableToSqlServer(_sharedDataTable))
                {
                    this._bytesReceiver.DeleteMessagesBuffer();
                }
                _sharedDataTable = null;
            }
        }

        private bool UploadDataTableToSqlServer(DataTable tableToUpload)
        {
            bool uploaded = false;
            DateTime startTime = HighResolutionDateTime.UtcNow;
            int totalFatalEmailsSent = 0;
            do
            {
                DateTime currentAttemptStartTime = HighResolutionDateTime.UtcNow;
                DateTime endTime;
                try
                {
                    this._logger.Log(LogLevel.Info, "Started uploading data batch {0} ({1} rows) to backend", tableToUpload.TableName, tableToUpload.Rows.Count);
                    this.BcpTableToSqlServer(tableToUpload);
                    endTime = HighResolutionDateTime.UtcNow;
                    this._logger.Log(LogLevel.Info,
                                     "Successfuly finished uploading data batch {0} to backend. Duration: {1} ({2:HH:mm:ss.fffffff UTC} - {3:HH:mm:ss.fffffff UTC})",
                                     tableToUpload.TableName,
                                     endTime - currentAttemptStartTime, endTime, currentAttemptStartTime);
                    uploaded = true;
                }
                catch (Exception e)
                {
                    endTime = HighResolutionDateTime.UtcNow;
                    this._logger.LogException(LogLevel.Error, e,
                                              "Experienced error during uploading data batch {0} to backend. Duration: {1} ({2:HH:mm:ss.fffffff UTC} - {3:HH:mm:ss.fffffff UTC})",
                                              tableToUpload.TableName,
                                              endTime - currentAttemptStartTime, endTime, currentAttemptStartTime);
                    if (((int) (endTime - startTime).TotalHours) > totalFatalEmailsSent)
                    {
                        totalFatalEmailsSent++;
                        this._logger.Log(LogLevel.Fatal,
                                         "Data collection cannot upload batch {0} to backend for extended duration: {1} ({2:HH:mm:ss.fffffff UTC} - {3:HH:mm:ss.fffffff UTC}). Check the backend.",
                                         tableToUpload.TableName, endTime - startTime, endTime, startTime);
                    }
                    this._shuttingDownTokenSource.Token.WaitHandle.WaitOne(
                        _dataCollectorSettings.WaitIntervalBetweenBcpRetries);
                }
            } while (!uploaded && !_shuttingDownTokenSource.IsCancellationRequested);

            return uploaded;
        }

        private void BcpTableToSqlServer(DataTable tbl)
        {
            if (tbl == null)
            {
                this._logger.Log(LogLevel.Error, "Attempt to bcp table that is null");
                return;
            }

            using (var sqlBulkCopyWriter = new SqlBulkCopy(this._connectionString,
                                                           SqlBulkCopyOptions.CheckConstraints |
                                                           SqlBulkCopyOptions.TableLock)
            {
                DestinationTableName = _dataCollectorHelper.DestinationTableName,
                BulkCopyTimeout = int.MaxValue
            })
            {
                for (var i = 0; i < tbl.Columns.Count; i++)
                {
                    var columnName = tbl.Columns[i].ColumnName;
                    sqlBulkCopyWriter.ColumnMappings.Add(new SqlBulkCopyColumnMapping(columnName, columnName));
                }

                sqlBulkCopyWriter.WriteToServer(tbl);
            }
        }
    }
}
