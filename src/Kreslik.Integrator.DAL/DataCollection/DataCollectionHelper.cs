﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.DAL.DataCollection
{
    public abstract class DataCollectionHelper<T> where T : 
#if POOLS_INSTRUMENTATION        
        TimedObjectBase 
#else
        class
#endif
        , new()
    {
        private IBytesSender _bytesSender;
        protected IDbTypesMappers DbTypesMappers { get; private set; }
        protected ILogger _logger;
        //following two pools are needed so that we don't have to synchronize and use single instances
        //protected IObjectPool<T> _entriesPool;
        //each counterparty has just a single thread for reading - so thode can be used without synchronization
        protected T[] _unsynchronizedEntriesPool;
        protected BufferPool _entriesBytesPool;
        protected BlockingCollection<BufferSegmentItem> _dataCollectionsegments = new BlockingCollection<BufferSegmentItem>(new ConcurrentQueue<BufferSegmentItem>());

        protected IIntegratorPerformanceCounter _perfCounters = IntegratorPerformanceCounter.Instance;
        private SafeTimer _perfCountersTimer;
        private SafeTimer _dataFlushingTimer;
        private TimeSpan _maxSpanBeforeFlushing;
        private int _maxMessagesBeforeFlushing;
        protected bool _collectionFinished = false;

        protected DataCollectionHelper(IDbTypesMappers dbTypesMappers, ILogger logger, IBytesSender bytesSender, int entriesPoolSize, int segmentsInBytePool, TimeSpan maxSpanBeforeFlushing, int maxMessagesBeforeFlushing)
        {
            //_fxTypesMapper = new FxDbTypesMapper(
            //DALBehavior.Sections.DataCollectionConnection.ConnectionString,
            //LogFactory.Instance.GetLogger(null), DALBehavior.Sections.OhterSettings.DataCollection.DataProviderId,
            //DALBehavior.Sections.OhterSettings.DataCollection.SourceFileId);

            this.DbTypesMappers = dbTypesMappers;
            this._logger = logger;
            this._bytesSender = bytesSender;
            //this._entriesPool = new GCFreeObjectPool<T>(() => new T(), null, entriesPoolSize);
            this._unsynchronizedEntriesPool =
                Enumerable.Repeat(1, Counterparty.ValuesCount).Select(i => new T()).ToArray();
            this._entriesBytesPool = new BufferPool(Marshal.SizeOf(typeof(T)), segmentsInBytePool);
            this._maxSpanBeforeFlushing = maxSpanBeforeFlushing;
            this._maxMessagesBeforeFlushing = maxMessagesBeforeFlushing;
            this.DataCollectionDoneEvent = new ManualResetEvent(false);

            ThreadPool.QueueUserWorkItem(ProcessDataCollectionSegements);
            this._perfCountersTimer = new SafeTimer(PerfCountersCallback, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2))
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1sec,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
            this._dataFlushingTimer = new SafeTimer(DataFlushingCallback, maxSpanBeforeFlushing, Timeout.InfiniteTimeSpan)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.GetTimerPrecisionForDelay(maxSpanBeforeFlushing),
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Normal
            };
            this.InitializeCountersState();
        }

        private void InitializeCountersState()
        {
            this._perfCounters.SetDCBufferSegemntsPoolSize(this._entriesBytesPool.TotalPoolSize);
            //this._perfCounters.SetDCElementsPoolSize(this._entriesPool.TotalPoolSize);
        }

        protected DataCollectionHelper()
        {
            this._collectionFinished = true;
        }

        protected int _numberOfUnpooledElementsInUse = 0;
        protected int _totalNumberOfUnpooledElementsCreated = 0;

        /// <summary>
        /// Adds entry to be serialized into buffer (using pooling of intermediate objects and pooling of buffers)
        ///  WARNING!: due to need for lambdas with implicit closures usage of this base creates garbage
        ///    There is no other way of doing this in base without creating garbage - as we don't know parameters upfront and we need to wrap them
        /// </summary>
        /// <param name="populateAction">Action that populates serialization entry with the data to be serialized</param>
        /// <param name="createIdentificationFunc">Function for identification of the entry to be serialized - used if serialization cannot be performed</param>
        /// <returns>true if action succeeded, false if it failed - it is unlikely that retry would help</returns>
        protected bool AddDcData(Action<T> populateAction, Func<string> createIdentificationFunc, Counterparty ctp)
        {
            if (_collectionFinished)
                return false;

            bool succeeded = false;
            try
            {
                T serializationEntry = this._unsynchronizedEntriesPool[(int) ctp];

                try
                {
                    populateAction(serializationEntry);

                    BufferSegmentItem bufferSegment = _entriesBytesPool.Fetch();
                    bufferSegment.ClearContent();
                    ByteConverter.AppendType(bufferSegment, serializationEntry);
                    _dataCollectionsegments.Add(bufferSegment);

                    succeeded = true;
                }
                catch (InvalidOperationException e)
                {
                    this._logger.LogException(_collectionFinished ? LogLevel.Warn : LogLevel.Fatal, "Cannot serialize DC data (collection closed)" + createIdentificationFunc(), e);
                }
                catch (/*ByteConvertingException*/Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, "Cannot serialize DC data" + createIdentificationFunc(), e);
                }
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, "Unexpected. Cannot serialize DC data" + createIdentificationFunc(), e);
                succeeded = false;
            }

            return succeeded;
        }

        private void PerfCountersCallback()
        {
            this._perfCounters.SetPooledDCBufferSegemntsInUseCount(this._entriesBytesPool.NumberOfPooledItemsInUse);
            this._perfCounters.SetUnpooledDCBufferSegemntsInUseCount(this._entriesBytesPool.NumberOfUnpooledItemsInUse);
            this._perfCounters.SetTotalUnpooledDCBufferSegemntsCreated(this._entriesBytesPool.TotalNumberOfUnpooledItemsCreated);
            //this._perfCounters.SetPooledDCElementsInUseCount(this._entriesPool.NumberOfUsedItems);
            //this._perfCounters.SetUnpooledDCElementsInUseCount(this._numberOfUnpooledElementsInUse);
            //this._perfCounters.SetTotalUnpooledDCElementsCreated(this._totalNumberOfUnpooledElementsCreated);
        }

        //this callback will ensure that flushing will occure on the same thread as sending
        private void DataFlushingCallback()
        {
            if (!_collectionFinished)
            {
                this._dataFlushingTimer.Change(this._maxSpanBeforeFlushing, Timeout.InfiniteTimeSpan);
                //do not flush if no new messages
                if(Interlocked.Exchange(ref messagesSent, 0) != 0)
                    this._dataCollectionsegments.Add(BufferSegmentItem.NULL_BUFFER_ITEM);
            }
        }

        public void StopDataCollection()
        {
            if (!_collectionFinished)
            {
                Volatile.Write(ref _collectionFinished, true);
                this._dataFlushingTimer.Dispose();
                this._dataCollectionsegments.CompleteAdding();
            }
        }

        /// <summary>
        /// Can be used by outer classes to wait for DC to completely finish, should not be signalled from outside!!
        /// </summary>
        public ManualResetEvent DataCollectionDoneEvent { get; private set; }

        private int messagesSent = 0;

        private void ProcessDataCollectionSegements(object unused)
        {
            foreach (BufferSegmentItem bufferSegmentItem in _dataCollectionsegments.GetConsumingEnumerable())
            {
                if (bufferSegmentItem == BufferSegmentItem.NULL_BUFFER_ITEM)
                {
                    _bytesSender.Flush();
                    continue;
                }

                if (bufferSegmentItem.SegmentSize != bufferSegmentItem.ContentSize || bufferSegmentItem.SegmentSize != _entriesBytesPool.SegmentSize)
                {
                    this._logger.Log(LogLevel.Fatal, "Unexpected buffer segment (used {0} instead of {1}): {2}", bufferSegmentItem.ContentSize, bufferSegmentItem.SegmentSize, bufferSegmentItem);
                }

                try
                {
                    _bytesSender.SentByteSegment(bufferSegmentItem);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Unexpected exception during sending buffer: {0}", bufferSegmentItem);
                }
                

                this._entriesBytesPool.Return(bufferSegmentItem);

                if (messagesSent++ > this._maxMessagesBeforeFlushing)
                {
                    this.DataFlushingCallback();
                }
            }

            _bytesSender.EndSending();

            this.DataCollectionDoneEvent.Set();
        }
    }

    

    public class ToBDataCollector: DataCollectionHelper<ToBSerializationEntry>
    {
        public ToBDataCollector(IDbTypesMappers dbTypesMappers, ILogger logger, IDataCollectorSettings dataCollectorSettings, int entriesPoolSize,
                                int segmentsInBytePool)
            : base(dbTypesMappers, logger, new FileBytesSender(logger, "ToBDCFile", Path.Combine(dataCollectorSettings.DataCollectionFolder, "ToB")), entriesPoolSize, segmentsInBytePool, dataCollectorSettings.MaxIntervalBetweenUploads, dataCollectorSettings.MaxBcpBatchSize)
        { }

        private ToBDataCollector()
        { }

        public static ToBDataCollector NullDataCollector
        {
            get {return new ToBDataCollector();}
        }

        // This way we'd be creating temporary closures and so memory garbage!

        //public bool AddToBData(PriceObject askPrice, PriceObject bidPrice, DateTime creationTimeUtc)
        //{
        //    return base.AddDcData(
        //        toBSerializationEntry => 
        //            toBSerializationEntry.SetNewData(

        //            (byte) this.FxDbTypesMapper.DataProviderId,
        //            this.FxDbTypesMapper.GetFxDdSymbolId(askPrice.Symbol),
        //            this.FxDbTypesMapper.ConvertTimeToTickTime(creationTimeUtc, true),
        //            (double) bidPrice.Price,
        //            (double) askPrice.Price,
        //            (int) bidPrice.SizeBaseAbsInitial,
        //            (int) askPrice.SizeBaseAbsInitial,
        //            this.FxDbTypesMapper.SourceFileId,
        //            this.FxDbTypesMapper.GetFxDdLiquidityProviderStreamId(
        //                bidPrice.Counterparty),
        //            this.FxDbTypesMapper.GetFxDdLiquidityProviderStreamId(
        //                askPrice.Counterparty),
        //            this.FxDbTypesMapper.ConvertTimeToTickTime(
        //                bidPrice.IntegratorReceivedTimeUtc, true),
        //            this.FxDbTypesMapper.ConvertTimeToTickTime(
        //                askPrice.IntegratorReceivedTimeUtc, true))
        //                ,
        //        () =>
        //        string.Format("ToB data: AskPrice [{0}] BidPrice [{1}]", askPrice, bidPrice));
        //}

        public bool AddToBDcData(PriceObject askPrice, PriceObject bidPrice, DateTime creationTimeUtc, Counterparty triggeringCounterparty)
        {
            if (_collectionFinished)
                return false;

            if (PriceObjectInternal.IsNullPrice(askPrice) || PriceObjectInternal.IsNullPrice(bidPrice))
                return false;

            bool succeeded = false;
            try
            {
                //All events arrive from a single thread as we collect only improvements 
                // and those comes only from QUO session (which has dedicate thread)
                ToBSerializationEntry serializationEntry = this._unsynchronizedEntriesPool[(int) triggeringCounterparty];

                try
                {
                    serializationEntry.SetNewData(
                        (byte) this.DbTypesMappers.FxDbTypesMapper.DataProviderId,
                        this.DbTypesMappers.FxDbTypesMapper.GetFxDdSymbolId(askPrice.Symbol),
                        creationTimeUtc,
                        (double) bidPrice.Price,
                        (double) askPrice.Price,
                        (int) bidPrice.SizeBaseAbsInitial,
                        (int) askPrice.SizeBaseAbsInitial,
                        this.DbTypesMappers.FxDbTypesMapper.SourceFileId,
                        this.DbTypesMappers.FxDbTypesMapper.GetFxDdLiquidityProviderStreamId(
                            bidPrice.Counterparty),
                        this.DbTypesMappers.FxDbTypesMapper.GetFxDdLiquidityProviderStreamId(
                            askPrice.Counterparty),
                        bidPrice.IntegratorReceivedTimeUtc,
                        askPrice.IntegratorReceivedTimeUtc);

                    BufferSegmentItem bufferSegment = _entriesBytesPool.Fetch();
                    bufferSegment.ClearContent();
                    ByteConverter.AppendType(bufferSegment, serializationEntry);
                    _dataCollectionsegments.Add(bufferSegment);

                    succeeded = true;
                }
                catch (InvalidOperationException e)
                {
                    this._logger.LogException(_collectionFinished ? LogLevel.Warn : LogLevel.Fatal, e, "Cannot serialize ToB data (collection completed): AskPrice [{0}] BidPrice [{1}]", askPrice, bidPrice);
                }
                catch (/*ByteConvertingException*/Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Cannot serialize ToB data: AskPrice [{0}] BidPrice [{1}]", askPrice, bidPrice);
                }

            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Unexpected. Cannot serialize ToB data: AskPrice [{0}] BidPrice [{1}]", askPrice, bidPrice);
                succeeded = false;
            }

            return succeeded;
        }
    }

    public class MarketDataCollector : DataCollectionHelper<MarketDataSerializationEntry>, IPriceChangesConsumer
    {
        private bool[][] _executablePriceAvailable = new bool[Symbol.ValuesCount][];
        public IEnumerable<Counterparty> ExcludedCounterparties = new List<Counterparty>();
        private MarketDataSerializationEntry[] _unsynchronizedPriceSerializationEntries
            = Enumerable.Repeat(1, Counterparty.ValuesCount).Select(i => new MarketDataSerializationEntry()).ToArray();

        public MarketDataCollector(IDbTypesMappers dbTypesMappers, ILogger logger, IDataCollectorSettings dataCollectorSettings, int entriesPoolSize,
                                   int segmentsInBytePool)
            : base(dbTypesMappers, logger, new FileBytesSender(logger, "MDDCFile", Path.Combine(dataCollectorSettings.DataCollectionFolder, "MD")), entriesPoolSize, segmentsInBytePool, dataCollectorSettings.MaxIntervalBetweenUploads, dataCollectorSettings.MaxBcpBatchSize)
        {
            for (int symbolIdx = 0; symbolIdx < Symbol.ValuesCount; symbolIdx++)
            {
                _executablePriceAvailable[symbolIdx] =
                    Enumerable.Range(0, Counterparty.ValuesCount).Select(idx => false).ToArray();
            }
            ExcludedCounterparties = dataCollectorSettings.ExcludedCounterparties;
        }

        private MarketDataCollector()
        { }

        public static MarketDataCollector NullDataCollector
        {
            get { return new MarketDataCollector(); }
        }

        private bool AddMDDcData(MarketDataRecordType marketDataRecordType, Symbol symbol, PriceSide? side,
                                 decimal? price, decimal? size, Counterparty counterparty, TradeSide? tradeSide,
                                 DateTime? counterpartySentTimeUtc, DateTime integratorReceivedTimeUtc,
                                 byte[] counterpartyPriceIdentity, Guid integratorPriceIdentity, decimal? minimumSize,
                                 decimal? granularity)
        {
            if (_collectionFinished)
                return false;

            bool succeeded = false;
            try
            {
                MarketDataSerializationEntry serializationEntry;

                //SingleQuoteCancel in main process are ariving from separate thread (ord session)
                // (ToB doesn't have this issue as it colllects only improovements)
                if (marketDataRecordType == MarketDataRecordType.SingleQuoteCancel)
                {
                    serializationEntry = this._unsynchronizedPriceSerializationEntries[(int) counterparty];
                }
                else
                {
                    serializationEntry = this._unsynchronizedEntriesPool[(int) counterparty];
                }

                try
                {
                    serializationEntry.SetNewData(
                        this.DbTypesMappers.FxDbTypesMapper.GetFxDdSymbolId(symbol),
                        this.DbTypesMappers.FxDbTypesMapper.GetFxSideCode(side),
                        price,
                        size,
                        this.DbTypesMappers.FxDbTypesMapper.GetFxDdLiquidityProviderStreamId(counterparty),
                        this.DbTypesMappers.FxDbTypesMapper.GetFxTradeSideCode(tradeSide),
                        counterpartySentTimeUtc,
                        integratorReceivedTimeUtc,
                        counterpartyPriceIdentity,
                        integratorPriceIdentity,
                        minimumSize,
                        granularity,
                        (byte) marketDataRecordType);

                    BufferSegmentItem bufferSegment = _entriesBytesPool.Fetch();
                    bufferSegment.ClearContent();
                    ByteConverter.AppendType(bufferSegment, serializationEntry);
                    _dataCollectionsegments.Add(bufferSegment);

                    succeeded = true;
                }
                catch (InvalidOperationException e)
                {
                    this._logger.LogException(_collectionFinished ? LogLevel.Warn : LogLevel.Fatal, e,
                                              "Cannot serialize MD data - collection closed (Received {4:HH:mm:ss.ffffff}): Symbol [{0}] Counterparty [{1}] IntegratorIdentity [{2}] RecordType [{3}]",
                                              symbol, counterparty, integratorPriceIdentity, marketDataRecordType,
                                              integratorReceivedTimeUtc);
                }
                catch (/*ByteConvertingException*/Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e,
                                              "Cannot serialize MD data (Received {4:HH:mm:ss.ffffff}): Symbol [{0}] Counterparty [{1}] IntegratorIdentity [{2}] RecordType [{3}]",
                                              symbol, counterparty, integratorPriceIdentity, marketDataRecordType,
                                              integratorReceivedTimeUtc);
                }
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e,
                                          "Unexpected. Cannot serialize MD data (Received {4:HH:mm:ss.ffffff}): Symbol [{0}] Counterparty [{1}] IntegratorIdentity [{2}] RecordType [{3}]",
                                          symbol, counterparty, integratorPriceIdentity, marketDataRecordType,
                                          integratorReceivedTimeUtc);
                succeeded = false;
            }

            return succeeded;
        }

        //Alternative way - however this generates memory garbage!
        private bool AddMDDcData2(MarketDataRecordType marketDataRecordType, Symbol symbol, PriceSide? side,
                                 decimal? price, decimal? size, Counterparty counterparty, TradeSide? tradeSide,
                                 DateTime counterpartySentTimeUtc, DateTime integratorReceivedTimeUtc,
                                 byte[] counterpartyPriceIdentity, Guid integratorPriceIdentity, decimal? minimumSize,
                                 decimal? granularity)
        {
            return this.AddDcData(
                serializationEntry => serializationEntry.SetNewData(
                    this.DbTypesMappers.FxDbTypesMapper.GetFxDdSymbolId(symbol),
                    this.DbTypesMappers.FxDbTypesMapper.GetFxSideCode(side),
                    price,
                    size,
                    this.DbTypesMappers.FxDbTypesMapper.GetFxDdLiquidityProviderStreamId(counterparty),
                    this.DbTypesMappers.FxDbTypesMapper.GetFxTradeSideCode(tradeSide),
                    marketDataRecordType == MarketDataRecordType.VenueTradeData && counterparty.TradingTargetType == TradingTargetType.Hotspot ? null : (DateTime?) counterpartySentTimeUtc,
                    integratorReceivedTimeUtc,
                    counterpartyPriceIdentity,
                    integratorPriceIdentity,
                    minimumSize,
                    granularity,
                    (byte) marketDataRecordType),
                () =>
                string.Format(
                    "MD data (Received {4:HH:mm:ss.ffffff}): Symbol [{0}] Counterparty [{1}] IntegratorIdentity [{2}] RecordType [{3}]",
                    symbol, counterparty, integratorPriceIdentity, marketDataRecordType,
                    integratorReceivedTimeUtc),
                    counterparty);
        }

        private void AddPriceInteral(PriceObject priceObject)
        {
            this.AddMDDcData(
                priceObject.MarketDataRecordType,
                priceObject.Symbol,
                priceObject.Side,
                priceObject.Price,
                priceObject.MarketDataRecordType == MarketDataRecordType.VenueTradeData
                    ? (decimal?)null
                    : priceObject.SizeBaseAbsInitial,
                priceObject.Counterparty,
                priceObject.TradeSide,
                priceObject.CounterpartySentTimeUtc,
                priceObject.IntegratorReceivedTimeUtc,
                priceObject.CounterpartyIdentity,
                priceObject.IntegratorIdentity,
                //following 2 are nulls for bank prices
                priceObject.SizeBaseAbsFillMinimum,
                priceObject.SizeBaseAbsFillGranularity);
        }

        public void AddPrice(PriceObject price)
        {
            if (_collectionFinished || price == null)
                return;

            if (price.Price > 0 && price.SizeBaseAbsInitial > 0)
            {
                this.AddPriceInteral(price);
            }

            try
            {
                _executablePriceAvailable[(int)price.Symbol][(int)price.Counterparty] = true;
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "UNEXP EXCEP!!!. price: {0}", price);
                
                throw;
            }
        }

        public void AddQuoteCancel(Common.Symbol symbol, Common.Counterparty counterparty, PriceSide? side)
        {
            if (_collectionFinished)
                return;

            if (_executablePriceAvailable[(int)symbol][(int)counterparty])
            {
                _executablePriceAvailable[(int)symbol][(int)counterparty] = false;

                this.AddMDDcData(
                    MarketDataRecordType.StreamCancellation,
                    symbol,
                    side,
                    null,
                    null,
                    counterparty,
                    null,
                    null,
                    HighResolutionDateTime.UtcNow,
                    null,
                    //Let's give it a guid - so that all records have a unique identification
                    GuidHelper.NewSequentialGuid(),
                    null,
                    null);
            }
        }


        public void AddHotspotPrice(PriceObject price)
        {
            if (_collectionFinished)
                return;

            this.AddPriceInteral(price);

            //this.AddMDDcData(
            //    quote.Price.MarketDataRecordType,
            //    quote.Symbol,
            //    quote.Price.Side,
            //    quote.Price.Price,
            //    quote.Price.SizeBaseAbsInitial,
            //    quote.Counterparty,
            //    quote.Price.TradeSide,
            //    quote.CounterpartySentTimeUtc,
            //    quote.IntegratorReceivedTimeUtc,
            //    quote.Price.CounterpartyIdentity,
            //    quote.Price.IntegratorIdentity,
            //    quote.Price.SizeBaseAbsFillMinimum,
            //    quote.Price.SizeBaseAbsFillGranularity);
        }

        public void AddVenuDealData(VenueDealObjectInternal venueDeal)
        {
            if (_collectionFinished)
                return;

            this.AddMDDcData(
                MarketDataRecordType.VenueTradeData,
                venueDeal.Symbol,
                venueDeal.Side,
                venueDeal.Price,
                venueDeal.DealSizeBaseAbs,
                venueDeal.Counterparty,
                venueDeal.TradeSide,
                //Hotspot trade data are not sent at the tme of deal
                venueDeal.CounterpartySuppliedTransactionTimeUtc ?? (venueDeal.Counterparty.TradingTargetType == TradingTargetType.Hotspot ? null : (DateTime?) venueDeal.CounterpartySentTimeUtc),
                venueDeal.IntegratorReceivedTimeUtc,
                venueDeal.CounterpartyDealIdentity,
                venueDeal.IntegratorIdentity,
                null, null);
        }

        public void AddHotspotQuoteUpdate(HotspotQuoteUpdateInfo quoteUpdateInfo)
        {
            if (_collectionFinished)
                return;

            this.AddMDDcData(
                MarketDataRecordType.VenueOrderChange,
                quoteUpdateInfo.OriginalPrice.Symbol,
                quoteUpdateInfo.OriginalPrice.Side,
                quoteUpdateInfo.OriginalPrice.Price,
                quoteUpdateInfo.NewSize,
                quoteUpdateInfo.OriginalPrice.Counterparty,
                quoteUpdateInfo.OriginalPrice.TradeSide,
                quoteUpdateInfo.CounterpartySentTimeUtc,
                quoteUpdateInfo.IntegratorReceivedTimeUtc,
                quoteUpdateInfo.OriginalPrice.CounterpartyIdentity,
                quoteUpdateInfo.OriginalPrice.IntegratorIdentity,
                quoteUpdateInfo.NewMinimalSize,
                quoteUpdateInfo.NewGranularity);
        }

        public void AddHotspotQuoteCancel(HotspotQuoteCancelInfo quoteCancelInfo)
        {
            if (_collectionFinished)
                return;

            this.AddMDDcData(
                MarketDataRecordType.SingleQuoteCancel,
                quoteCancelInfo.OriginalPrice.Symbol,
                quoteCancelInfo.OriginalPrice.Side,
                null,
                null,
                quoteCancelInfo.OriginalPrice.Counterparty,
                null,
                quoteCancelInfo.CounterpartySentTimeUtc,
                quoteCancelInfo.IntegratorReceivedTimeUtc,
                quoteCancelInfo.OriginalPrice.CounterpartyIdentity,
                quoteCancelInfo.OriginalPrice.IntegratorIdentity,
                null,
                null);
        }

        public void OnNewPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            this.AddPrice(price);
        }

        public void OnIgnoredPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if (price == null)
                return;

            if (this.ConsumesIgnoredPrices)
                this.OnNewPrice(price, ref heldWithoutAcquireCall);
            else
                this.OnPriceCancel(price.Symbol, price.Counterparty, price.Side);
        }

        public void OnNewNonExecutablePrice(PriceObjectInternal price)
        {
            if (price == null)
                return;

            bool _ = true;
            if (this.ConsumesIgnoredPrices)
                this.OnNewPrice(price, ref _);
            else
                this.OnPriceCancel(price.Symbol, price.Counterparty, price.Side);
        }

        public void OnQuoteCancel(Symbol symbol, Counterparty counterparty)
        {
            this.AddQuoteCancel(symbol, counterparty, null);
        }

        public void OnPriceCancel(Symbol symbol, Counterparty counterparty, PriceSide side)
        {
            this.AddQuoteCancel(symbol, counterparty, side);
        }

        public void OnPriceInvalidate(Symbol symbol, PriceSide side, Counterparty counterparty, Guid integratorPriceIdnetity,
            DateTime integratorPriceReceivedUtc)
        {
            if (_collectionFinished)
                return;

            if (_executablePriceAvailable[(int)symbol][(int)counterparty])
            {
                _executablePriceAvailable[(int)symbol][(int)counterparty] = false;

                this.AddMDDcData(
                    MarketDataRecordType.SingleQuoteCancel,
                    symbol,
                    side,
                    null,
                    null,
                    counterparty,
                    null,
                    null,
                    HighResolutionDateTime.UtcNow,
                    null,
                    integratorPriceIdnetity,
                    null,
                    null);
            }
        }

        public bool ConsumesDataFromCounterparty(Counterparty counterparty)
        {
            //Hotspot data are collected directly (from itch)
            if (counterparty.TradingTargetType == TradingTargetType.Hotspot)
            {
                return false;
            }

            return true;
        }

        public bool ConsumesMDDataIfOFNotRunning(Counterparty counterparty)
        {
            return false;
        }

        public bool ConsumesIgnoredPrices
        {
            get { return true; }
        }

        public bool ConsumesNonExecutablePrices
        {
            get { return true; }
        }
    }

    public class OutgoingPriceDataCollector : DataCollectionHelper<OutgoingPriceSerializationEntry>
    {
        //following two pools are needed so that we don't have to synchronize and use single instances
        private IObjectPool<OutgoingPriceSerializationEntry> _entriesPool;

        public OutgoingPriceDataCollector(IDbTypesMappers dbTypesMappers, ILogger logger,
            IDataCollectorSettings dataCollectorSettings, int entriesPoolSize,
            int segmentsInBytePool)
            : base(
                dbTypesMappers, logger,
                new FileBytesSender(logger, "OutgoingPriceDCFile",
                    Path.Combine(dataCollectorSettings.DataCollectionFolder, "OutgoingPrice")), entriesPoolSize,
                segmentsInBytePool, dataCollectorSettings.MaxIntervalBetweenUploads,
                dataCollectorSettings.MaxBcpBatchSize)
        {
            this._entriesPool = new GCFreeObjectPool<OutgoingPriceSerializationEntry>(() => new OutgoingPriceSerializationEntry(), null, entriesPoolSize, "OutPriceSerializedPool");
        }

        private OutgoingPriceDataCollector()
        { }

        public static OutgoingPriceDataCollector NullDataCollector
        {
            get { return new OutgoingPriceDataCollector(); }
        }

        
        public void AddOutgoingPrice(StreamingPrice streamingPrice)
        {
            if (_collectionFinished)
                return;

            try
            {
                bool pooled = true;
                OutgoingPriceSerializationEntry serializationEntry;
                if (!this._entriesPool.TryFetch(out serializationEntry))
                {
                    serializationEntry = new OutgoingPriceSerializationEntry();
                    pooled = false;
                }

                try
                {
                    byte[] priceIdentity = new byte[StreamingPrice.MAX_INTEGRATOR_IDENTITY_LENGTH];
                    System.Text.Encoding.ASCII.GetBytes(streamingPrice.IntegratorPriceIdentity)
                        .CopyToClearingRest(priceIdentity);

                    serializationEntry.SetNewData(
                        this.DbTypesMappers.FxDbTypesMapper.GetFxDdSymbolId(streamingPrice.Symbol),
                        this.DbTypesMappers.FxDbTypesMapper.GetFxDdLiquidityProviderStreamId(
                            streamingPrice.Counterparty),
                        this.DbTypesMappers.FxDbTypesMapper.GetFxSideCode(streamingPrice.Side),
                        streamingPrice.Price,
                        streamingPrice.SizeBaseAbs,
                        streamingPrice.PriceProducerIndex.IntegratedTradingSystemIdentification,
                        streamingPrice.IntegratorSentTimeUtc,
                        priceIdentity);

                    BufferSegmentItem bufferSegment = _entriesBytesPool.Fetch();
                    bufferSegment.ClearContent();
                    ByteConverter.AppendType(bufferSegment, serializationEntry);
                    _dataCollectionsegments.Add(bufferSegment);
                }
                catch (InvalidOperationException e)
                {
                    this._logger.LogException(_collectionFinished ? LogLevel.Warn : LogLevel.Fatal, e, "Cannot serialize OutgoingPrice data (collection completed): [{0}]", streamingPrice);
                }
                catch (/*ByteConvertingException*/Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Cannot serialize OutgoingPrice data: [{0}]", streamingPrice);
                }

                if (pooled)
                {
                    this._entriesPool.Return(serializationEntry);
                }
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Unexpected. Cannot serialize OutgoingPrice data: [{0}]", streamingPrice);
            }
        }

        public void AddOutgoingPriceCancel(StreamingPriceCancel streamingPriceCancel)
        {
            if (_collectionFinished)
                return;

            try
            {
                bool pooled = true;
                OutgoingPriceSerializationEntry serializationEntry;
                if (!this._entriesPool.TryFetch(out serializationEntry))
                {
                    serializationEntry = new OutgoingPriceSerializationEntry();
                    pooled = false;
                }

                try
                {
                    PriceSide side = streamingPriceCancel.Side.HasValue
                        ? streamingPriceCancel.Side.Value
                        : PriceSide.Ask;

                    serializationEntry.SetNewCancelData(
                        this.DbTypesMappers.FxDbTypesMapper.GetFxDdSymbolId(streamingPriceCancel.Symbol),
                        this.DbTypesMappers.FxDbTypesMapper.GetFxDdLiquidityProviderStreamId(
                            streamingPriceCancel.Counterparty),
                        this.DbTypesMappers.FxDbTypesMapper.GetFxSideCode(side),
                        streamingPriceCancel.PriceProducerIndex.HasValue
                            ? streamingPriceCancel.PriceProducerIndex.Value.IntegratedTradingSystemIdentification
                            : (int?) null,
                        streamingPriceCancel.IntegratorSentTimeUtc);

                    BufferSegmentItem bufferSegment = _entriesBytesPool.Fetch();
                    bufferSegment.ClearContent();
                    ByteConverter.AppendType(bufferSegment, serializationEntry);
                    _dataCollectionsegments.Add(bufferSegment);

                    if (!streamingPriceCancel.Side.HasValue)
                    {
                        serializationEntry.SetNewCancelData(
                        this.DbTypesMappers.FxDbTypesMapper.GetFxDdSymbolId(streamingPriceCancel.Symbol),
                        this.DbTypesMappers.FxDbTypesMapper.GetFxDdLiquidityProviderStreamId(
                            streamingPriceCancel.Counterparty),
                        this.DbTypesMappers.FxDbTypesMapper.GetFxSideCode(PriceSide.Bid),
                        streamingPriceCancel.PriceProducerIndex.HasValue
                            ? streamingPriceCancel.PriceProducerIndex.Value.IntegratedTradingSystemIdentification
                            : (int?)null,
                        streamingPriceCancel.IntegratorSentTimeUtc);

                        bufferSegment = _entriesBytesPool.Fetch();
                        bufferSegment.ClearContent();
                        ByteConverter.AppendType(bufferSegment, serializationEntry);
                        _dataCollectionsegments.Add(bufferSegment);
                    }
                }
                catch (InvalidOperationException e)
                {
                    this._logger.LogException(_collectionFinished ? LogLevel.Warn : LogLevel.Fatal, e, "Cannot serialize OutgoingPriceCancel data (collection completed): [{0}]", streamingPriceCancel);
                }
                catch (/*ByteConvertingException*/Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Cannot serialize OutgoingPriceCancel data: [{0}]", streamingPriceCancel);
                }

                if (pooled)
                {
                    this._entriesPool.Return(serializationEntry);
                }
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Unexpected. Cannot serialize OutgoingPriceCancel data: [{0}]", streamingPriceCancel);
            }
        }
    }

    //TODO:
    // Once DC will be used also for other datapoints we need to revisit if it's allways possible to find unique
    //  TriggeringCounterparty or if there will be need for DC overloads again using SYNCHRONIZATION
    public class NewOrderExternalCollector : DataCollectionHelper<NewOrderExternalSerializationEntry>
    {
        public NewOrderExternalCollector(IDbTypesMappers dbTypesMappers, ILogger logger, IDataCollectorSettings dataCollectorSettings, int entriesPoolSize,
                                   int segmentsInBytePool)
            : base(dbTypesMappers, logger, new FileBytesSender(logger, "OrdrDCFile", Path.Combine(dataCollectorSettings.DataCollectionFolder, "Ordr")), entriesPoolSize, segmentsInBytePool, dataCollectorSettings.MaxIntervalBetweenUploads, dataCollectorSettings.MaxBcpBatchSize)
        { }

        private NewOrderExternalCollector()
        { }

        public static NewOrderExternalCollector NullDataCollector
        {
            get { return new NewOrderExternalCollector(); }
        }

        //this generates memory garbage! (closure)
        public bool AddOrdrDcData(IIntegratorOrderExternal integratorOrderExternal)
        {
            return this.AddDcData(
                serializationEntry => serializationEntry.SetNewData(integratorOrderExternal, this.DbTypesMappers.IntegratorDbTypesMapper),
                () =>
                string.Format("New External Order: [{0}], [{1}]", integratorOrderExternal.Identity, integratorOrderExternal.UniqueInternalIdentity),
                integratorOrderExternal.Counterparty);
        }


    }
}
