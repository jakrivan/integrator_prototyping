﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.DAL.DataCollection
{
    public interface IBytesReceiver<out T> where T : class, new()
    {
        event Action<T> NewMessageAvailable;
        event Action GapInContinuousMessageStream;
        void DeleteMessagesBuffer();
        void StartReceivingMessages();
        void StopReceivingMessages();
        WaitHandle ReceivingDoneHandle { get; }
    }

    public static class FileBytesConstants
    {
        public const string TEMP_FILE_EXTENSION = ".tmp";
        public const string COMUNICATION_FILE_EXTENSION = ".bin";
    }

    public class FileBytesReceiver<T> : IBytesReceiver<T> where T : class, new()
    {
        private ILogger _logger;
        private string _destinationFolder;
        private string _coruptedFilesDestinationFolder;
        private string _toDeleteFilesDestinationFolder;
        private readonly byte[] _messagesSeparator = new byte[2];
        private readonly TimeSpan[] _retryIntervals = 
            new TimeSpan[] { TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(90), TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(15) };
        private int _currentRetryTimeoutIdx = 0;
        private FileSystemWatcher _watcher;
        private ManualResetEvent _newFileCreatedEvent = new ManualResetEvent(false);
        private CancellationTokenSource _messagesReceivingCancellation = new CancellationTokenSource();
        private List<string> _filesToIgnore = new List<string>(); 

        private BufferSegmentItem _sharedBufferSegment;
        private T _sharedMessage = new T();

        public FileBytesReceiver(ILogger logger, string destinationFolder)
        {
            this._logger = logger;
            this._destinationFolder = destinationFolder;

            int entrySize = Marshal.SizeOf(typeof(T));
            _sharedBufferSegment = new BufferSegmentItem(new byte[entrySize], 0, entrySize);

        }

        public event Action<T> NewMessageAvailable;
        public event Action GapInContinuousMessageStream;

        public void StopReceivingMessages()
        {
            _messagesReceivingCancellation.Cancel();
        }

        public void DeleteMessagesBuffer()
        {
            try
            {
                foreach (string file in Directory.GetFiles(_toDeleteFilesDestinationFolder))
                {
                    File.Delete(file);
                }
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Problem during deleting files");
            }
        }

        public void StartReceivingMessages()
        {
            Task.Factory.StartNew(ConfigureAndWaitForFolder).ContinueWith(task =>
                {
                    if (task.Exception != null)
                    {
                        this._logger.LogException(LogLevel.Fatal, "Unexpected exception during starting DC uploader",
                                                  task.Exception);
                    }
                }, TaskContinuationOptions.OnlyOnFaulted);
        }

        private void ConfigureAndWaitForFolder()
        {
            string originalDestinationfolder = _destinationFolder;
            if (!Path.IsPathRooted(_destinationFolder))
            {
                this._destinationFolder =
                    Path.Combine(
                        System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                        _destinationFolder);
            }

            while (!Directory.Exists(_destinationFolder))
            {
                this._logger.Log(LogLevel.Fatal,
                                 "Couldn't start receiving DC messages as directory [{0}] (configured as [{1}]) doesn't exist. Will retry",
                                 _destinationFolder, originalDestinationfolder);
                Thread.Sleep(_retryIntervals[_currentRetryTimeoutIdx]);
                _currentRetryTimeoutIdx = (_currentRetryTimeoutIdx + 1) % _retryIntervals.Length;
            }

            _currentRetryTimeoutIdx = 0;

            _coruptedFilesDestinationFolder = Path.Combine(_destinationFolder, "Corupted");
            if (!Directory.Exists(_coruptedFilesDestinationFolder))
            {
                Directory.CreateDirectory(_coruptedFilesDestinationFolder);
            }

            _toDeleteFilesDestinationFolder = Path.Combine(_destinationFolder, "ToDelete");
            if(Directory.Exists(_toDeleteFilesDestinationFolder))
            {
                if (Directory.EnumerateFiles(_toDeleteFilesDestinationFolder).Any())
                {
                    this._logger.Log(LogLevel.Fatal,
                                     "Directory for files to be deleted [{0}] is nonempty during startup - this might mean unexpected shutdown. Renaming folder and it should be reviewed manually",
                                     _toDeleteFilesDestinationFolder);
                    Directory.Move(_toDeleteFilesDestinationFolder, string.Format("{0}_{1:yyyy-MM-dd--hh-mm-ss}", _toDeleteFilesDestinationFolder, DateTime.UtcNow));
                }
            }

            if (!Directory.Exists(_toDeleteFilesDestinationFolder))
            {
                Directory.CreateDirectory(_toDeleteFilesDestinationFolder);
            }


            _watcher = new FileSystemWatcher(_destinationFolder, "*" + FileBytesConstants.COMUNICATION_FILE_EXTENSION);
            _watcher.NotifyFilter = NotifyFilters.FileName;
            _watcher.Renamed += WatcherOnCreated;
            _watcher.EnableRaisingEvents = true;

            ThreadPool.QueueUserWorkItem(WaitAndProcessNewFiles);
        }

        private void WatcherOnCreated(object sender, FileSystemEventArgs fileSystemEventArgs)
        {
            _newFileCreatedEvent.Set();
        }

        
        private void WaitAndProcessNewFiles(object unused)
        {
            try
            {
                WaitAndProcessNewFilesInternal();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, 
                    "Unexpected exception during receiving DC messages - no more DC mesages will be received", e);
            }
        }

        private readonly ManualResetEvent _receivingDoneEvent = new ManualResetEvent(false);

        public WaitHandle ReceivingDoneHandle 
        {
            get { return this._receivingDoneEvent; }
        }

        private bool _isFirstFilesTraversalAfterStart = true;
        private bool IsFileToBeRead(string fullFileName)
        {
            if (_filesToIgnore.Contains(fullFileName))
                return false;

            if (Path.GetExtension(fullFileName) != FileBytesConstants.COMUNICATION_FILE_EXTENSION)
            {
                if (!_isFirstFilesTraversalAfterStart)
                    return false;

                this._logger.Log(LogLevel.Error,
                                 "DC FileBytesReceiver encountered file with unexpected extension [{0}], since this is first traversal of files it will attempt to parse it",
                                 fullFileName);
            }

            return true;
        }

        private void WaitAndProcessNewFilesInternal()
        {
            DateTime lastMessageReceivedTimeUtc = DateTime.UtcNow;
            while (!_messagesReceivingCancellation.IsCancellationRequested)
            {
                this._logger.Log(LogLevel.Info, "MessagesReader is checking if there are any new messages...");

                bool someMessagesRead = false;

                DirectoryInfo di = new DirectoryInfo(this._destinationFolder);
                foreach (string fileName in 
                    di.GetFiles()
                    .OrderBy(fi => fi.CreationTime)
                    .Select(fi => fi.FullName)
                    .Where(IsFileToBeRead))
                {
                    someMessagesRead |= this.TryReadFileToEnd(fileName);
                }

                DateTime now = DateTime.UtcNow;
                if (someMessagesRead)
                {
                    _currentRetryTimeoutIdx = 0;
                    lastMessageReceivedTimeUtc = now;

                    if (this.GapInContinuousMessageStream != null)
                    {
                        this.GapInContinuousMessageStream();
                    }
                }
                else
                {
                    _currentRetryTimeoutIdx = (_currentRetryTimeoutIdx + 1) % _retryIntervals.Length;
                    if (now.IsBusinessDay() && lastMessageReceivedTimeUtc < now.AddDays(-1))
                    {
                        this._logger.Log(LogLevel.Fatal, "MessageReceiver haven't received any DC message for over 24 hours");
                        lastMessageReceivedTimeUtc = DateTime.UtcNow;
                    }
                }

                _isFirstFilesTraversalAfterStart = false;
                _newFileCreatedEvent.Reset();

                WaitHandle.WaitAny(new[] {_newFileCreatedEvent, _messagesReceivingCancellation.Token.WaitHandle},
                                   _retryIntervals[_currentRetryTimeoutIdx]);
            }

            this._logger.Log(LogLevel.Info, "Finished receiving messages as shutdown was requested");
            _receivingDoneEvent.Set();
        }

        private bool TryReadFileToEnd(string fileName)
        {
            if (_messagesReceivingCancellation.IsCancellationRequested)
                return false;

            Stream stream;
            try
            {
                stream = File.Open(
                    fileName,
                    FileMode.Open,
                    FileAccess.Read,
                    FileShare.None);
            }
            catch (IOException e)
            {
                this._logger.LogException(LogLevel.Info, e, "Cannot open file {0} due to exception - probably still in use, skipping now", fileName);
                return false;
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Cannot open file {0} due to exception. Skipping now", fileName);
                return false;
            }

            int messagesRead = 0;
            try
            {
                //Here we will read all messages to the end of file even if cancellation is requested - so that we don't lose portion of data
                while (GetNextMessage(stream, ref _sharedMessage))
                {
                    messagesRead++;
                    if (this.NewMessageAvailable != null)
                    {
                        this.NewMessageAvailable(_sharedMessage);
                    }
                }

                //reading done - delete the file
                stream.Close();
                this._logger.Log(LogLevel.Info, "Done reading file [{0}] with {1} messages. Moving to delete folder", fileName, messagesRead);
                File.Move(fileName, Path.Combine(_toDeleteFilesDestinationFolder, Path.GetFileName(fileName)));
                return true;
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e,
                                          "Issues during reading file [{0}] (successfuly read {1} messages from it). Moving it to Corputed folder",
                                          fileName, messagesRead);
                try
                {
                    stream.Close();
                    File.Move(fileName, Path.Combine(_coruptedFilesDestinationFolder, Path.GetFileName(fileName)));
                }
                catch (Exception ex)
                {
                    this._logger.LogException(LogLevel.Fatal, ex, "Issues during moving file [{0}] to Corputed folder", fileName);
                    _filesToIgnore.Add(fileName);
                }
            }
            return false;
        }

        private bool GetNextMessage(Stream stream, ref T message)
        {
            int bytesRead = stream.Read(_sharedBufferSegment.Buffer, 0, _sharedBufferSegment.SegmentSize);

            if (bytesRead == 0)
                return false;

            if (bytesRead != _sharedBufferSegment.SegmentSize)
                throw new Exception("Unexpected number of bytes read");

            if (stream.Read(_messagesSeparator, 0, _messagesSeparator.Length) != _messagesSeparator.Length)
                throw new Exception("Unexpected number of bytes read for separator");

            if (_messagesSeparator[0] != byte.MaxValue || _messagesSeparator[1] != byte.MaxValue)
                throw new Exception("Unexpected messages separator");

            _sharedBufferSegment.IncreaseContentSize(bytesRead);
            ByteConverter.ReadType<T>(_sharedBufferSegment, 0, message);
            _sharedBufferSegment.ClearContent();

            return true;
        }
    }
}
