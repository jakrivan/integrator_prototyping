﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.DAL.DataCollection
{
    public class FxDbTypesMapper : IFxDbTypesMapper
    {
        private byte[] _symbolMappings = new byte[Symbol.ValuesCount];
        private byte[] _counterpartyMapings = new byte[Counterparty.ValuesCount];

        public FxDbTypesMapper(string connectionString, ILogger logger, int dataProviderId, int sourceFileId)
        {
            //48
            this.DataProviderId = dataProviderId;
            //932696
            this.SourceFileId = sourceFileId;

            this.Initialize(logger, connectionString);
        }

        public int DataProviderId { get; private set; }
        public int SourceFileId { get; private set; }

        public byte GetFxDdSymbolId(Symbol symbol)
        {
            return this._symbolMappings[(int)symbol];
        }

        public byte GetFxDdLiquidityProviderStreamId(Counterparty counterparty)
        {
            return this._counterpartyMapings[(int)counterparty];
        }

        public long ConvertTimeToTickTime(DateTime time, bool convertInputUtcToEt)
        {
            if (convertInputUtcToEt)
            {
                time = TradingHoursHelper.Instance.ConvertFromUtcToEt(time);
            }

            return
                time.Year * 10000000000000L +
                time.Month * 100000000000L +
                time.Day * 1000000000L +
                time.Hour * 10000000L +
                time.Minute * 100000L +
                time.Second * 1000L +
                time.Millisecond;
        }

        public static long ConvertTimeToTickTimeStatic(DateTime time, bool convertInputUtcToEt)
        {
            if (convertInputUtcToEt)
            {
                time = TradingHoursHelper.Instance.ConvertFromUtcToEt(time);
            }

            return
                time.Year * 10000000000000L +
                time.Month * 100000000000L +
                time.Day * 1000000000L +
                time.Hour * 10000000L +
                time.Minute * 100000L +
                time.Second * 1000L +
                time.Millisecond;
        }

        public bool GetFxSideCode(PriceSide priceSide)
        {
            //Bid - 0 - false
            //Ask - 1 - true
            return priceSide == PriceSide.Ask;
        }

        public bool? GetFxSideCode(PriceSide? priceSide)
        {
            if (priceSide.HasValue)
            {
                //Bid - 0 - false
                //Ask - 1 - true
                return priceSide == PriceSide.Ask;
            }
            else
            {
                return null;
            }
        }

        public bool? GetFxTradeSideCode(TradeSide? tradeSide)
        {
            if (tradeSide.HasValue)
            {
                //SellGiven - 0 - false
                //BuyPaid - 1 - true
                return tradeSide == TradeSide.BuyPaid;
            }
            else
            {
                return null;
            }
        }

        private void Initialize(ILogger logger, string connectionString)
        {
            //let's not keep those around after initialization
            int DB_RETRY_COUNT = 3;

            Dictionary<string, byte> fxPairs = null;

            if(!AdoNetHelpers.RetrySqlOperation(() => fxPairs = ReadFxPairs(connectionString), "reading FxPairs", logger,
                                            DB_RETRY_COUNT, false))
                throw new Exception("Couldn't successfully read fxPairs from backend");

            this.CreateSymbolMappings(fxPairs);

            Dictionary<string, byte> liquidityProviders = null;

            if (!AdoNetHelpers.RetrySqlOperation(() => liquidityProviders = ReadLiquidityProviders(connectionString), "reading Liquidity Stream providers", logger,
                                            DB_RETRY_COUNT, false))
                throw new Exception("Couldn't successfully read Liquidity providers from backend");

            this.CreateCounterpartyMappings(liquidityProviders);
        }

        private void CreateSymbolMappings(Dictionary<string, byte> fxPairs)
        {
            foreach (Symbol symbol in Symbol.Values)
            {
                byte fxDbSymbolId;
                if (fxPairs.TryGetValue(symbol.ShortName, out fxDbSymbolId))
                {
                    _symbolMappings[(int)symbol] = fxDbSymbolId;
                }
                else
                {
                    throw new Exception(string.Format("FX DB is missing an id for symbol {0}", symbol));
                }
            }
        }

        private void CreateCounterpartyMappings(Dictionary<string, byte> liquidityProviders)
        {
            foreach (Counterparty counterparty in Counterparty.Values)
            {
                byte fxLiquidityStreamProviderId;
                if (liquidityProviders.TryGetValue(counterparty.ToString(), out fxLiquidityStreamProviderId))
                {
                    _counterpartyMapings[(int)counterparty] = fxLiquidityStreamProviderId;
                }
                else
                {
                    throw new Exception(string.Format("FX DB is missing a liquidity stream provider id for counterparty: {0}", counterparty));
                }
            }
        }


        private Dictionary<string, byte> ReadFxPairs(string sqlConnectionString)
        {
            Dictionary<string, byte> pairs = new Dictionary<string, byte>();

            using (SqlConnection sqlConnection = new SqlConnection(sqlConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand getItemsCommand = new SqlCommand("[dbo].[GetAllFXPairs]", sqlConnection))
                {
                    getItemsCommand.CommandType = CommandType.StoredProcedure;


                    using (var reader = getItemsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            pairs[reader.GetString(reader.GetOrdinal("FxpCode"))] =
                                reader.GetByte(reader.GetOrdinal("FxPairId"));
                        }
                    }
                }
            }

            return pairs;
        }

        private Dictionary<string, byte> ReadLiquidityProviders(string sqlConnectionString)
        {
            Dictionary<string, byte> liquidityProviders = new Dictionary<string, byte>();

            using (SqlConnection sqlConnection = new SqlConnection(sqlConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand getItemsCommand = new SqlCommand("[dbo].[GetLiqProviderStreamsInfo]", sqlConnection))
                {
                    getItemsCommand.CommandType = CommandType.StoredProcedure;

                    using (var reader = getItemsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            liquidityProviders[reader.GetString(reader.GetOrdinal("LiqProviderStreamName"))] =
                                reader.GetByte(reader.GetOrdinal("LiqProviderStreamId"));
                        }
                    }
                }
            }

            return liquidityProviders;
        }
    }
}
