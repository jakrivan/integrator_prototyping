﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.DAL
{
    public class DelayEvaluatorDal : IPriceDelayEvaluatorProvider, IDelayEvaluatorMulti
    {
        private ILogger _logger;
        private InternalDelayEvaluator[] _delayEvaluators;
        private IUnderlyingSessionState[] _fixChannels = new IUnderlyingSessionState[Counterparty.ValuesCount];

        private SafeTimer _hourlyTimer;
        private SafeTimer _minuteTimer;

        public IDelayEvaluator GetDelayEvaluator(Counterparty counterparty)
        {
            return this._delayEvaluators[(int) counterparty];
        }

        public IDelayEvaluatorMulti GetDelayEvaluatorMultiDestination()
        {
            return this;
        }

        public bool IsReceivedObjectDelayed(IIntegratorReceivedObjectEx receivedObject)
        {
            return this._delayEvaluators[(int) receivedObject.Counterparty].IsReceivedObjectDelayed(receivedObject);
        }

        public DelayEvaluatorDal(ILogger logger, IRiskManager riskManager)
        {
            this._logger = logger;
            this._delayEvaluators =
            Counterparty.Values.Select(ctp => new InternalDelayEvaluator(ctp, riskManager)).ToArray();
            this.ReadSettings();
            this._hourlyTimer = new SafeTimer(this.HourlyProc, TimeSpan.FromHours(1), TimeSpan.FromHours(1), true)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.GetTimerPrecisionForDelay(TimeSpan.FromHours(1)),
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
            this._minuteTimer = new SafeTimer(this.MinuteProc, TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(1), true)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.GetTimerPrecisionForDelay(TimeSpan.FromSeconds(1)),
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
        }

        public void SetSessionStateWatchers(IEnumerable<IUnderlyingSessionState> fixChannels)
        {
            foreach (IUnderlyingSessionState fixChannel in fixChannels.Where(o => o != null))
            {
                _fixChannels[(int) fixChannel.Counterparty] = fixChannel;
            }
        }

        public void ClearStateWatchers(IEnumerable<Counterparty> cpts)
        {
            foreach (var counterparty in cpts)
            {
                _fixChannels[(int) counterparty] = null;
            }
        }

        private void HourlyProc()
        {
            this.UpdateDelayedPricesStats(true,true);
        }

        private void MinuteProc()
        {
            this.UpdateDelayedPricesStats(true, false);
            this.ReadSettings();
        }

        private void UpdateDelayedPricesStats(bool resetMinuteCtls, bool resetHourlyCtls)
        {
            StringBuilder argBuilder = new StringBuilder();

            for (int ctpIdx = 0; ctpIdx < Counterparty.ValuesCount; ctpIdx++)
            {
                InternalDelayEvaluator delayEvaluator = _delayEvaluators[ctpIdx];
                //Only update stats for active counterparties (not to overwritte stats from other processes)!
                if(_fixChannels[ctpIdx] != null && _fixChannels[ctpIdx].IsReadyAndOpen)
                    delayEvaluator.AppendStatsString(argBuilder);

                if (resetMinuteCtls)
                    delayEvaluator.ResetMinuteCounters();
                if (resetHourlyCtls)
                    delayEvaluator.ResetHourlyCounters();
            }

            string csvArg = argBuilder.ToString();
            this._logger.Log(LogLevel.Info, "PriceDelayEvaluatorDal uploading price delaye stats: " + csvArg);

            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand command = new SqlCommand("[dbo].[UpdatePriceDelayStats_SP]", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CsvStatsArg", csvArg);
                    command.ExecuteNonQuery();
                }
            }
        }

        private void ReadSettings()
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand command = new SqlCommand("[dbo].[GetDelaySettings_SP]", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Counterparty ctp = (Counterparty) reader.GetString(reader.GetOrdinal("Counterparty"));
                            _delayEvaluators[(int) ctp].MaxExpectedTotalDelay =
                                reader.GetInt64(reader.GetOrdinal("ExpectedDelayTicks")) +
                                reader.GetInt64(reader.GetOrdinal("MaxAllowedAdditionalDelayTicks"));
                            _delayEvaluators[(int) ctp].AllowGoFlatOnlyFlipOnTimeStampOutOfBounds =
                                reader.GetBoolean(reader.GetOrdinal("AllowGoFlatOnlyFlipOnTimeStampOutOfBounds"));
                            _delayEvaluators[(int) ctp].MinDelayTicksToGFO =
                                reader.GetInt64(reader.GetOrdinal("MinDelayTicksFromCptAllegedSentToTurnGFO"));
                            _delayEvaluators[(int) ctp].MaxDelayTicksFromCptSentToTurnGFO =
                                reader.GetInt64(reader.GetOrdinal("MaxDelayTicksFromCptAllegedSentToTurnGFO"));
                        }
                    }
                }
            }
        }

        private class InternalDelayEvaluator: IDelayEvaluator
        {
            //public long ExpectedDelayTicks { get; set; }
            //public long MaxAllowedAdditionalDelayTicks { get; set; }

            public long MaxExpectedTotalDelay { get; set; }
            public bool AllowGoFlatOnlyFlipOnTimeStampOutOfBounds { get; set; }
            public long MinDelayTicksToGFO { get; set; }
            public long MaxDelayTicksFromCptSentToTurnGFO { get; set; }
            public long TotalReceivedPricesInLastMinute { get; private set; }
            public long DelayedPricesInLastMinute { get; private set; }
            public long TotalReceivedPricesInLastHour { get; private set; }
            public long DelayedPricesInLastHour { get; private set; }

            public Counterparty Counterparty { get; private set; }

            private IRiskManager _riskManager;

            public void AppendStatsString(StringBuilder sb)
            {
                sb.AppendFormat("{0}:{1},{2},{3},{4},", this.Counterparty, this.TotalReceivedPricesInLastMinute,
                    this.DelayedPricesInLastMinute, this.TotalReceivedPricesInLastHour, this.DelayedPricesInLastHour);
            }

            public InternalDelayEvaluator(Counterparty counterparty, IRiskManager riskManager)
            {
                this.Counterparty = counterparty;
                this.MaxExpectedTotalDelay = long.MaxValue;
                _riskManager = riskManager;
            }

            public void ResetMinuteCounters()
            {
                this.TotalReceivedPricesInLastMinute = 0;
                this.DelayedPricesInLastMinute = 0;
            }

            public void ResetHourlyCounters()
            {
                this.TotalReceivedPricesInLastHour = 0;
                this.DelayedPricesInLastHour = 0;
            }

            public bool IsReceivedObjectDelayed(IIntegratorReceivedObject receivedObject)
            {
                this.TotalReceivedPricesInLastMinute++;
                this.TotalReceivedPricesInLastHour++;

                long stampsTicksDelata = receivedObject.IntegratorReceivedTimeUtc.Ticks -
                                         receivedObject.CounterpartySentTimeUtc.Ticks;

                bool delayed = stampsTicksDelata > this.MaxExpectedTotalDelay;

                if (delayed)
                {
                    this.DelayedPricesInLastMinute++;
                    this.DelayedPricesInLastHour++;
                }

                this.CheckReceivedObjectDelayedWithNoStatsUpdateInternal(stampsTicksDelata);

                return delayed;
            }

            public void CheckReceivedObjectDelayedWithNoStatsUpdate(IIntegratorReceivedObject receivedObject)
            {
                this.CheckReceivedObjectDelayedWithNoStatsUpdateInternal(
                    receivedObject.IntegratorReceivedTimeUtc.Ticks -
                    receivedObject.CounterpartySentTimeUtc.Ticks);
            }

            private void CheckReceivedObjectDelayedWithNoStatsUpdateInternal(long stampsTicksDelata)
            {
                if (this.AllowGoFlatOnlyFlipOnTimeStampOutOfBounds &&
                    _riskManager != null &&
                    (stampsTicksDelata > this.MinDelayTicksToGFO || stampsTicksDelata < this.MaxDelayTicksFromCptSentToTurnGFO) &&
                    !_riskManager.IsGoFlatOnlyEnabled)
                {
                    _riskManager.TurnOnGoFlatOnly(
                        string.Format("Receiving delay of {0} is out of acceptable bounds for {1}",
                            TimeSpan.FromTicks(stampsTicksDelata), Counterparty));
                }
            }
        }
    }
}
