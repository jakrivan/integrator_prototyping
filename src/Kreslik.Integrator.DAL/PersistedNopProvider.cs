﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.DAL
{
    public class NopSettingsPointPerCreditLine
    {
        public NopSettingsPointPerCreditLine(int creditLineId, string creditLineName,
            decimal maximumUnsettledNopTotal, decimal maximumUnsettledNopTotalToLockDestination,
            decimal maximumUnsettledNopPerValueDate, decimal maximumUnsettledNopPerValueDateToLockDestination,
            IEnumerable<Counterparty> counterparties)
        {
            this.CreditLineId = creditLineId;
            this.CreditLineName = creditLineName;
            this.MaximumUnsettledNopTotal = maximumUnsettledNopTotal;
            this.MaximumUnsettledNopTotalToLockDestination = maximumUnsettledNopTotalToLockDestination;
            this.MaximumUnsettledNopPerValueDate = maximumUnsettledNopPerValueDate;
            this.MaximumUnsettledNopPerValueDateToLockDestination = maximumUnsettledNopPerValueDateToLockDestination;
            this.Counterparties = counterparties;
        }

        public int CreditLineId { get; private set; }
        public string CreditLineName { get; private set; }
        public decimal MaximumUnsettledNopTotal { get; private set; }
        public decimal MaximumUnsettledNopTotalToLockDestination { get; private set; }
        public decimal MaximumUnsettledNopPerValueDate { get; private set; }
        public decimal MaximumUnsettledNopPerValueDateToLockDestination { get; private set; }
        public IEnumerable<Counterparty> Counterparties { get; private set; } 
    }

    public class NopSettingsPointPerPrimeBroker : NopSettingsPointDAL
    {
        public NopSettingsPointPerPrimeBroker(int primeBrokerId, string primeBrokerName,
            decimal maximumTradeDayNopTotal, decimal maximumTradeDayNopTotalToLockDestination,
            IEnumerable<Counterparty> counterparties)
            : base(maximumTradeDayNopTotal, maximumTradeDayNopTotalToLockDestination)
        {
            this.PrimeBrokerId = primeBrokerId;
            this.PrimeBrokerName = primeBrokerName;
            this.Counterparties = counterparties;
        }
        public int PrimeBrokerId { get; private set; }
        public string PrimeBrokerName { get; private set; }
        public IEnumerable<Counterparty> Counterparties { get; private set; }
    }

    public abstract class NopSettingsPointDAL
    {
        protected NopSettingsPointDAL(decimal maximumTradeDayNopTotal, decimal maximumTradeDayNopTotalToLockDestination)
        {
            this.MaximumTradeDayNopTotal = maximumTradeDayNopTotal;
            this.MaximumTradeDayNopTotalToLockDestination = maximumTradeDayNopTotalToLockDestination;
        }

        public decimal MaximumTradeDayNopTotal { get; private set; }
        public decimal MaximumTradeDayNopTotalToLockDestination { get; private set; }
    }

    public class NopSettingsPointOverall : NopSettingsPointDAL
    {
        public NopSettingsPointOverall(decimal maximumTradeDayNopTotal, decimal maximumTradeDayNopTotalToLockDestination)
            :base(maximumTradeDayNopTotal, maximumTradeDayNopTotalToLockDestination)
        { }
    }

    public enum CreditZone
    {
        NoCheckingZone,
        SimpleCheckingZone,
        LockedZone,
        OverTheMaxZone
    }

    public interface IPersistedNopProvider
    {
        IEnumerable<IntegratorDealDone> GetNopPolPerCreditLineSettlementAndCurrency();
        IEnumerable<NopSettingsPointPerCreditLine> GetCreditLineNopSettings();
        IEnumerable<NopSettingsPointPerPrimeBroker> GetPrimeBrokerNopSettings();
        NopSettingsPointOverall GetOverallNopSettings();

        event Action<IEnumerable<NopSettingsPointPerCreditLine>> CreditLineNopSettingsChanged;
        event Action<IEnumerable<NopSettingsPointPerPrimeBroker>> PrimeBrokerNopSettingsChanged;
        event Action<NopSettingsPointOverall> OverallNopSettingsChanged;
        void FlipCreditZone(int creditLineBackendId, CreditZone newZone, DateTime? applicableSettlementDate);
        void FlipCreditZone(int? primeBrokerBackendId, CreditZone newZone);
    }

    public class PersistedNopProvider : IPersistedNopProvider
    {
        //IMPORTANT!: this field is need to keep updating the backend rollover info!
        //DOUBLE IMPORTANT: Cannot join declaration and initialization as otherwise runtime would not even call the ctor
        private static RolloverInfoCommunicator _;

        static PersistedNopProvider()
        {
            _ = new RolloverInfoCommunicator();
        }

        private SafeTimer _pollTimer;
        private DateTime _settingsLastChangedUtc = SqlDateTime.MinValue.Value;

        public PersistedNopProvider()
        {
            _pollTimer = new SafeTimer(PollChanges, TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(3), true);
        }


        private void PollChanges()
        {
            using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand getIsChangeCommand = new SqlCommand("[dbo].[GetIsCreditLineSettingsChange_SP]", sqlConnection))
                {
                    getIsChangeCommand.CommandType = CommandType.StoredProcedure;
                    getIsChangeCommand.Parameters.AddWithValue("@IsChangeFromUtc", _settingsLastChangedUtc);

                    if ((bool)getIsChangeCommand.ExecuteScalar())
                    {
                        var settingsBag = this.RetrieveNopSettings();

                        if (this.CreditLineNopSettingsChanged != null)
                            this.CreditLineNopSettingsChanged(settingsBag.PerCreditLine);

                        if (this.PrimeBrokerNopSettingsChanged != null)
                            this.PrimeBrokerNopSettingsChanged(settingsBag.PerPrimeBroker);

                        if (this.OverallNopSettingsChanged != null)
                            this.OverallNopSettingsChanged(settingsBag.Overall);
                    }
                }
            }
        }

        private Dictionary<int, List<Counterparty>> GetCreditLineCounterpartyMap()
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getcreditMapCommand = new SqlCommand("[dbo].[GetCreditLineCptMap_SP]", sqlConnection))
                {
                    getcreditMapCommand.CommandType = CommandType.StoredProcedure;
                    Dictionary<int, List<Counterparty>> map = new Dictionary<int, List<Counterparty>>();

                    using (var reader = getcreditMapCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Counterparty counterparty =
                                (Counterparty)reader.GetString(reader.GetOrdinal("Counterparty"));
                            int creditLineId = reader.GetByte(reader.GetOrdinal("CreditLineId"));

                            if(map.ContainsKey(creditLineId))
                                map[creditLineId].Add(counterparty);
                            else
                                map[creditLineId] = new List<Counterparty>(){counterparty};
                        }
                    }

                    return map;
                }
            }
        }

        private Dictionary<int, Tuple<string, List<Counterparty>>> GetPrimeBrokerCounterpartyMap()
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getcreditMapCommand = new SqlCommand("[dbo].[GetPrimeBrokerCptMap_SP]", sqlConnection))
                {
                    getcreditMapCommand.CommandType = CommandType.StoredProcedure;
                    Dictionary<int, Tuple<string, List<Counterparty>>> map = new Dictionary<int, Tuple<string, List<Counterparty>>>();

                    using (var reader = getcreditMapCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Counterparty counterparty =
                                (Counterparty)reader.GetString(reader.GetOrdinal("Counterparty"));
                            int primeBrokerId = reader.GetByte(reader.GetOrdinal("PrimeBrokerId"));
                            string primeBrokerName = reader.GetString(reader.GetOrdinal("PrimeBrokerName"));

                            if (map.ContainsKey(primeBrokerId))
                                map[primeBrokerId].Item2.Add(counterparty);
                            else
                                map[primeBrokerId] = new Tuple<string, List<Counterparty>>(primeBrokerName,
                                    new List<Counterparty>() {counterparty});
                        }
                    }

                    return map;
                }
            }
        }

        public IEnumerable<NopSettingsPointPerCreditLine> GetCreditLineNopSettings()
        {
            return this.RetrieveNopSettings().PerCreditLine;
        }

        public IEnumerable<NopSettingsPointPerPrimeBroker> GetPrimeBrokerNopSettings()
        {
            return this.RetrieveNopSettings().PerPrimeBroker;
        }

        public NopSettingsPointOverall GetOverallNopSettings()
        {
            return this.RetrieveNopSettings().Overall;
        }

        private class NopSettingsBag
        {
            public NopSettingsBag(List<NopSettingsPointPerCreditLine> perCreditLine, List<NopSettingsPointPerPrimeBroker> perPrimeBroker, NopSettingsPointOverall overall)
            {
                this.PerCreditLine = perCreditLine;
                this.PerPrimeBroker = perPrimeBroker;
                this.Overall = overall;
            }

            public List<NopSettingsPointPerCreditLine> PerCreditLine { get; private set; }
            public List<NopSettingsPointPerPrimeBroker> PerPrimeBroker { get; private set; }
            public NopSettingsPointOverall Overall { get; private set; }
        }

        private NopSettingsBag RetrieveNopSettings()
        {
            Dictionary<int, Tuple<string, List<Counterparty>>> primeBrokersMap = this.GetPrimeBrokerCounterpartyMap();
            Dictionary<int, List<Counterparty>> creditLinesMap = this.GetCreditLineCounterpartyMap();

            List<NopSettingsPointPerCreditLine> perCreditLine = new List<NopSettingsPointPerCreditLine>();
            List<NopSettingsPointPerPrimeBroker> perPrimeBroker = new List<NopSettingsPointPerPrimeBroker>();
            NopSettingsPointOverall nopSettingsPointOverall = null;

            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getNopsCommand = new SqlCommand("[dbo].[GetCreditLineSettings_SP]", sqlConnection))
                {
                    getNopsCommand.CommandType = CommandType.StoredProcedure;

                    _settingsLastChangedUtc = DateTime.UtcNow;
                    using (var reader = getNopsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.IsDBNull(reader.GetOrdinal("PrimeBrokerId")))
                            {
                                nopSettingsPointOverall = new NopSettingsPointOverall(
                                    reader.GetDecimal(reader.GetOrdinal("MaxUnsettledNopUsdTotal")),
                                    reader.GetDecimal(reader.GetOrdinal("MaxUnsettledNopUsdTotalToLockDestination")));
                            }
                            else if (reader.IsDBNull(reader.GetOrdinal("CreditLineId")))
                            {
                                int primeBrokerId = reader.GetByte(reader.GetOrdinal("PrimeBrokerId"));

                                //prime brokers that has no active mappings to cpts are not important for us
                                if (primeBrokersMap.ContainsKey(primeBrokerId))
                                {
                                    string primeBrokerName = primeBrokersMap[primeBrokerId].Item1;

                                    perPrimeBroker.Add(new NopSettingsPointPerPrimeBroker(
                                        primeBrokerId,
                                        primeBrokerName,
                                        reader.GetDecimal(reader.GetOrdinal("MaxUnsettledNopUsdTotal")),
                                        reader.GetDecimal(reader.GetOrdinal("MaxUnsettledNopUsdTotalToLockDestination")),
                                        primeBrokersMap[primeBrokerId].Item2
                                        ));
                                }
                            }
                            else
                            {
                                int creditLineId = reader.GetByte(reader.GetOrdinal("CreditLineId"));
                                string creditLineName = reader.GetString(reader.GetOrdinal("CreditLineName"));

                                perCreditLine.Add(new NopSettingsPointPerCreditLine(creditLineId, creditLineName,
                                    reader.GetDecimal(reader.GetOrdinal("MaxUnsettledNopUsdTotal")),
                                    reader.GetDecimal(reader.GetOrdinal("MaxUnsettledNopUsdTotalToLockDestination")),
                                    reader.GetDecimal(reader.GetOrdinal("MaxUnsettledNopUsdPerValueDate")),
                                    reader.GetDecimal(reader.GetOrdinal("MaxUnsettledNopUsdPerValueDateToLockDestination")),
                                    creditLinesMap[creditLineId]));
                            }
                        }
                    }
                }
            }

            return new NopSettingsBag(perCreditLine, perPrimeBroker, nopSettingsPointOverall);
        }

        public IEnumerable<IntegratorDealDone> GetNopPolPerCreditLineSettlementAndCurrency()
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getNopsCommand = new SqlCommand("[dbo].[GetUnsettledDealsPerCreditLineSettlementAndCurrency_SP]", sqlConnection))
                {
                    getNopsCommand.CommandType = CommandType.StoredProcedure;

                    using (var reader = getNopsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Counterparty counterparty = (Counterparty)reader.GetString(reader.GetOrdinal("Counterparty"));
                            Symbol symbol = (Symbol)reader.GetString(reader.GetOrdinal("Symbol"));
                            DateTime settlementDateLocal = reader.GetDateTime(reader.GetOrdinal("SettlementDateLocal"));
                            DateTime tradeDateUtc = reader.GetDateTime(reader.GetOrdinal("TradeDateUtc"));
                            DateTime settlementTimeCounterpartyUtc = reader.GetDateTime(reader.GetOrdinal("SettlementTimeCounterpartyUtc"));
                            decimal sizeBasePol = reader.GetDecimal(reader.GetOrdinal("SizeBasePol"));
                            decimal sizeTermPol = reader.GetDecimal(reader.GetOrdinal("SizeTermPol"));

                            yield return
                                new IntegratorDealDone(counterparty, symbol, settlementDateLocal, tradeDateUtc, settlementTimeCounterpartyUtc,
                                    sizeBasePol, sizeTermPol);
                        }
                    }
                }
            }
        }

        public event Action<IEnumerable<NopSettingsPointPerCreditLine>> CreditLineNopSettingsChanged;
        public event Action<IEnumerable<NopSettingsPointPerPrimeBroker>> PrimeBrokerNopSettingsChanged;
        public event Action<NopSettingsPointOverall> OverallNopSettingsChanged;

        //Another way would be with timestamp flags and only allowing newer updated. However the flags would need to be per credit line id and settelment date => too big mem usage
        private ConcurrentQueue<Action> _flipZoneUpdates = new ConcurrentQueue<Action>();
        private void ProcessZoneUpdateAsync()
        {
            Task.Factory.StartNew(() =>
            {
                //lock is here not due to protection of the queue (not needed), but to guarantee that order of executions is same as order of enqueues
                // this will be called once in the blue moon - so producer-consumer patterns (e.g. BlockingCollection) are overkill here
                lock (_flipZoneUpdates)
                {
                    Action act;
                    if (_flipZoneUpdates.TryDequeue(out act))
                    {
                        act();
                    }
                }
            }).ObserveException();
        }

        public void FlipCreditZone(int creditLineBackendId, CreditZone newZone, DateTime? applicableSettlementDate)
        {
            _flipZoneUpdates.Enqueue(() => this.FlipCreditZoneInternal(creditLineBackendId, newZone, applicableSettlementDate));
            this.ProcessZoneUpdateAsync();
        }

        public void FlipCreditZone(int? primeBrokerBackendId, CreditZone newZone)
        {
            _flipZoneUpdates.Enqueue(() => this.FlipCreditZoneInternal(primeBrokerBackendId, newZone));
            this.ProcessZoneUpdateAsync();
        }

        private void FlipCreditZoneInternal(int creditLineBackendId, CreditZone newZone, DateTime? applicableSettlementDate)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand updateZoneCommand = new SqlCommand("[dbo].[UpdatePrimeBrokerCreditLineNopCheckingZones_SP]", sqlConnection))
                    {
                        updateZoneCommand.CommandType = CommandType.StoredProcedure;
                        updateZoneCommand.Parameters.AddWithValue("@CreditLineId", creditLineBackendId);
                        updateZoneCommand.Parameters.AddWithNullValue("@SettlementDate", applicableSettlementDate);
                        updateZoneCommand.Parameters.AddWithValue("@NopCheckingZoneName", newZone.ToString());

                        int rowsUpdated = updateZoneCommand.ExecuteNonQuery();

                        if (rowsUpdated != 1)
                        {
                            LogFactory.Instance.GetLogger(null)
                            .Log(LogLevel.Fatal,
                                "Unable to update unsettled zone in backend ({0} rows updated) CreditLineId: {1}, NewZone: {2}, ValueDate: {3}",
                                rowsUpdated, creditLineBackendId, newZone, applicableSettlementDate);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null)
                        .LogException(LogLevel.Fatal, e,
                            "Exception during updating backend about unsettled zones changes: CreditLineId: {0}, NewZone: {1}, ValueDate: {2}",
                            creditLineBackendId, newZone, applicableSettlementDate);
            }
        }

        private void FlipCreditZoneInternal(int? primeBrokerBackendId, CreditZone newZone)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand updateZoneCommand = new SqlCommand("[dbo].[UpdatePrimeBrokerNopCheckingZones_SP]", sqlConnection))
                    {
                        updateZoneCommand.CommandType = CommandType.StoredProcedure;
                        updateZoneCommand.Parameters.AddWithNullValue("@PrimeBrokerId", primeBrokerBackendId);
                        updateZoneCommand.Parameters.AddWithValue("@NopCheckingZoneName", newZone.ToString());

                        int rowsUpdated = updateZoneCommand.ExecuteNonQuery();

                        if (rowsUpdated != 1)
                        {
                            LogFactory.Instance.GetLogger(null)
                            .Log(LogLevel.Fatal,
                                "Unable to update unsettled zone in backend ({0} rows updated) PrimeBrokerId: {1}, NewZone: {2}",
                                rowsUpdated, primeBrokerBackendId, newZone);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null)
                        .LogException(LogLevel.Fatal, e,
                            "Exception during updating backend about unsettled zones changes: PrimeBrokerId: {0}, NewZone: {1}",
                            primeBrokerBackendId, newZone);
            }
        }

        private class RolloverInfoCommunicator
        {
            public RolloverInfoCommunicator()
            {
                TradingHoursHelper.Instance.NZDRolloverDone += InstanceOnNzdRolloverDone;
                TradingHoursHelper.Instance.MarketRolloverDone += InstanceOnMarketRolloverDone;
                this.UpdateRolloverInfo(false, false);
            }

            private void InstanceOnMarketRolloverDone()
            {
                this.UpdateRolloverInfo(true, false);
            }

            private void InstanceOnNzdRolloverDone()
            {
                this.UpdateRolloverInfo(false, true);
            }

            private void UpdateRolloverInfo(bool isNYRolloverNow, bool isNZRolloverNow)
            {
                LogFactory.Instance.GetLogger(null).Log(LogLevel.Info, "Refreshing Settlement info");
                try
                {
                    this.UpdateRolloverInfo_Internal(isNYRolloverNow, isNZRolloverNow);
                }
                catch (Exception e)
                {
                    LogFactory.Instance.GetLogger(null)
                        .LogException(LogLevel.Fatal,
                            "Exception during updating backend about last unsettled dates - the unsettled nop info might be wrong as a result of this error",
                            e);
                }
            }

            private void UpdateRolloverInfo_Internal(bool isNYRolloverNow, bool isNZRolloverNow)
            {
                using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand updateRollCommand = new SqlCommand("[dbo].[UpdateRolloverInfo_SP]", sqlConnection))
                    {
                        updateRollCommand.CommandType = CommandType.StoredProcedure;
                        updateRollCommand.Parameters.AddWithValue("@IsNYRollover", isNYRolloverNow);
                        updateRollCommand.Parameters.AddWithValue("@IsNZRollover", isNZRolloverNow);
                        updateRollCommand.Parameters.AddWithValue("@LastCurrentlyUnsettledSettlementDateForNY", TradingHoursHelper.Instance.LastCurrentlyUnsettledSettlementDateNY);
                        updateRollCommand.Parameters.AddWithValue("@LastCurrentlyUnsettledSettlementDateForNZ", TradingHoursHelper.Instance.LastCurrentlyUnsettledSettlementDateNZ);

                        updateRollCommand.ExecuteNonQuery();
                    }
                }
            }
        }
        
    }
}
