﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.DAL.DataCollection;

namespace Kreslik.Integrator.DAL
{
    public class DALBehavior
    {
        public class DALSettings
        {
            public static DALSettings CreateDefaultInstance()
            {
                return new DALSettings() {DataCollection = new DataCollectionSettings()};
            }

            public static string SettingName
            {
                get { return @"Kreslik.Integrator.DAL.dll"; }
            }

            public int KillSwitchPollIntervalSeconds { get; set; }
            public int KillSwitchWatchDogIntervalSeconds { get; set; }
            public string IntegratorSwitchName { get; set; }
            public int CommandDistributorPollIntervalSeconds { get; set; }
            public DataCollectionSettings DataCollection { get; set; }

            public class DataCollectionSettings : IDataCollectorSettings
            {
                public int DataProviderId { get; set; }
                public int SourceFileId { get; set; }
                public int MaxBcpBatchSize { get; set; }
                public int MaxInMemoryColectionSize { get; set; }

                [System.Xml.Serialization.XmlIgnore]
                public System.TimeSpan MaxIntervalBetweenUploads { get; set; }

                [System.Xml.Serialization.XmlElement("MaxIntervalBetweenUploads_Seconds")]
                public int MaxIntervalBetweenUploadsXml
                {
                    get { return (int) MaxIntervalBetweenUploads.TotalSeconds; }
                    set { MaxIntervalBetweenUploads = System.TimeSpan.FromSeconds(value); }
                }

                [System.Xml.Serialization.XmlIgnore]
                public System.TimeSpan MaxUploadTimeAfterWhichSerializationStarts { get; set; }

                [System.Xml.Serialization.XmlElement("MaxUploadTimeAfterWhichSerializationStarts_Seconds")]
                public int MaxUploadTimeAfterWhichSerializationStartsXml
                {
                    get { return (int) MaxUploadTimeAfterWhichSerializationStarts.TotalSeconds; }
                    set { MaxUploadTimeAfterWhichSerializationStarts = System.TimeSpan.FromSeconds(value); }
                }

                [System.Xml.Serialization.XmlIgnore]
                public System.TimeSpan MaxProcessingTimeAfterShutdownSignalled { get; set; }

                [System.Xml.Serialization.XmlElement("MaxProcessingTimeAfterShutdownSignalled_Seconds")]
                public int MaxProcessingTimeAfterShutdownSignalledXml
                {
                    get { return (int) MaxProcessingTimeAfterShutdownSignalled.TotalSeconds; }
                    set { MaxProcessingTimeAfterShutdownSignalled = System.TimeSpan.FromSeconds(value); }
                }

                [System.Xml.Serialization.XmlIgnore]
                public System.TimeSpan WaitIntervalBetweenBcpRetries { get; set; }

                [System.Xml.Serialization.XmlElement("WaitIntervalBetweenBcpRetries_Seconds")]
                public int WaitIntervalBetweenBcpRetriesXml
                {
                    get { return (int) WaitIntervalBetweenBcpRetries.TotalSeconds; }
                    set { WaitIntervalBetweenBcpRetries = System.TimeSpan.FromSeconds(value); }
                }

                [System.Xml.Serialization.XmlIgnore]
                public System.TimeSpan WaitIntervalBeforeShutdownStartsSerialization { get; set; }

                [System.Xml.Serialization.XmlElement("WaitIntervalBeforeShutdownStartsSerialization_Seconds")]
                public int WaitIntervalBeforeShutdownStartsSerializationXml
                {
                    get { return (int) WaitIntervalBeforeShutdownStartsSerialization.TotalSeconds; }
                    set { WaitIntervalBeforeShutdownStartsSerialization = System.TimeSpan.FromSeconds(value); }
                }

                public string DataCollectionFolder { get; set; }

                [XmlArray(ElementName = "ExcludedCounterparties")]
                [XmlArrayItem(ElementName = "Counterparty")]
                public List<string> ExcludedCounterpartiesXml
                {
                    get;
                    set;
                }

                private List<Counterparty> _excludedCounterparties;

                [System.Xml.Serialization.XmlIgnore]
                public List<Counterparty> ExcludedCounterparties
                {
                    get
                    {
                        if (_excludedCounterparties == null)
                        {
                            _excludedCounterparties = ExcludedCounterpartiesXml == null
                                                  ? new List<Counterparty>()
                                                  : ExcludedCounterpartiesXml.Select(ctpStr => (Counterparty)ctpStr).ToList();
                        }

                        return _excludedCounterparties;
                    }

                    set
                    {
                        _excludedCounterparties = value;
                        ExcludedCounterpartiesXml = value == null ? new List<string>() : value.Select(ctp => ctp.ToString()).ToList();
                    }
                }
            }
        }

        public class DALSettings_IntegratorConnection
        {
            public static DALSettings_IntegratorConnection CreateDefaultInstance()
            {
                return new DALSettings_IntegratorConnection() {ConnectionString = "Foo"};
            }

            public static string SettingName
            {
                get { return @"Kreslik.Integrator.DAL_IntegratorConnection"; }
            }

            public string ConnectionString { get; set; }
        }

        public class DALSettings_DataCollectionConnection
        {
            public static DALSettings_DataCollectionConnection CreateDefaultInstance()
            {
                return new DALSettings_DataCollectionConnection() { ConnectionString = "Foo" };
            }

            public static string SettingName
            {
                get { return @"Kreslik.Integrator.DAL_DataCollectionConnection"; }
            }

            public string ConnectionString { get; set; }
        }

        public class DALSettings_DataCollectionEnabling
        {
            public static DALSettings_DataCollectionEnabling CreateDefaultInstance()
            {
                return new DALSettings_DataCollectionEnabling();
            }

            public static string SettingName
            {
                get { return @"Kreslik.Integrator.DAL_DataCollectionEnabling"; }
            }

            public bool EnableToBDataCollection { get; set; }
            public bool EnableMarketDataCollection { get; set; }
        }


        private bool _integratorConnectionInitialized;
        private DALSettings_IntegratorConnection _integratorConnection;

        private bool _datacollectionConnectionInitialized;
        private DALSettings_DataCollectionConnection _datacollectionConnection;

        private bool _datacollectionEnablingInitialized;
        private DALSettings_DataCollectionEnabling _datacollectionEnabling;

        private bool _dalSettingsInitialized;
        private DALSettings _dalSettings;

        public DALSettings_IntegratorConnection IntegratorConnection
        {
            get
            {
                if (!_integratorConnectionInitialized)
                {
                    this.ErrorOnUninitializedAccess("IntegratorConnection");
                }
                return this._integratorConnection;
            }

            private set { _integratorConnection = value; }
        }

        public DALSettings_DataCollectionConnection DataCollectionConnection
        {
            get
            {
                if (!_datacollectionConnectionInitialized)
                {
                    this.ErrorOnUninitializedAccess("DataCollectionConnection");
                }
                return this._datacollectionConnection;
            }

            private set { _datacollectionConnection = value; }
        }

        public DALSettings_DataCollectionEnabling DataCollectionEnabling
        {
            get
            {
                if (!_datacollectionEnablingInitialized)
                {
                    this.ErrorOnUninitializedAccess("DataCollectionEnabling");
                }
                return this._datacollectionEnabling;
            }

            private set { _datacollectionEnabling = value; }
        }

        public DALSettings OhterSettings
        {
            get
            {
                if (!_dalSettingsInitialized)
                {
                    this.ErrorOnUninitializedAccess("DALSettings");
                }
                return this._dalSettings;
            }

            private set { _dalSettings = value; }
        }

        private void ErrorOnUninitializedAccess(string settingName)
        {
            LogFactory.Instance.GetLogger(null)
                            .Log(LogLevel.Fatal,
                                 "Attempt to access uninitialized setting {0}, is it configured for your environment [{1}]?",
                                 settingName, SettingsInitializator.Instance.EnvironmentName);
        }

        private DALBehavior()
        {
            if (SettingsInitializator.Instance.HasSettings(DALSettings_IntegratorConnection.SettingName))
            {
                _integratorConnectionInitialized = true;
                _integratorConnection =
                    SettingsInitializator.Instance.ReadSettings<DALSettings_IntegratorConnection>(
                        DALSettings_IntegratorConnection.SettingName,
                        Properties.Resources.ResourceManager.GetString(DALSettings_IntegratorConnection.SettingName.Replace('.', '_')));
            }
            else
            {
                _integratorConnection = DALSettings_IntegratorConnection.CreateDefaultInstance();
            }


            if (SettingsInitializator.Instance.HasSettings(DALSettings.SettingName))
            {
                _dalSettingsInitialized = true;
                _dalSettings =
                    SettingsInitializator.Instance.ReadSettings<DALSettings>(
                        DALSettings.SettingName,
                        Properties.Resources.ResourceManager.GetString(DALSettings.SettingName.Replace('.', '_')));
            }
            else
            {
                _dalSettings = DALSettings.CreateDefaultInstance();
            }

            if (SettingsInitializator.Instance.HasSettings(DALSettings_DataCollectionConnection.SettingName))
            {
                _datacollectionConnectionInitialized = true;
                _datacollectionConnection =
                    SettingsInitializator.Instance.ReadSettings<DALSettings_DataCollectionConnection>(
                        DALSettings_DataCollectionConnection.SettingName,
                        Properties.Resources.ResourceManager.GetString(DALSettings_DataCollectionConnection.SettingName.Replace('.', '_')));
            }
            else
            {
                _datacollectionConnection = DALSettings_DataCollectionConnection.CreateDefaultInstance();
            }

            if (SettingsInitializator.Instance.HasSettings(DALSettings_DataCollectionEnabling.SettingName))
            {
                _datacollectionEnablingInitialized = true;
                _datacollectionEnabling =
                    SettingsInitializator.Instance.ReadSettings<DALSettings_DataCollectionEnabling>(
                        DALSettings_DataCollectionEnabling.SettingName,
                        Properties.Resources.ResourceManager.GetString(DALSettings_DataCollectionEnabling.SettingName.Replace('.', '_')));
            }
            else
            {
                _datacollectionEnabling = DALSettings_DataCollectionEnabling.CreateDefaultInstance();
            }
        }

        private static DALBehavior _dalBehavior = new DALBehavior();

        public static DALBehavior Sections
        {
            get { return _dalBehavior; }
        }
    }


    //public class DALSettings
    //{
    //    private bool _integratorConnectionInitialized = false;

    //    [System.Xml.Serialization.XmlIgnore]
    //    public DALSettings_IntegratorConnection IntegratorConnection
    //    {
    //        get; 
    //        private set;
    //    }

    //    public class DALSettings_IntegratorConnection
    //    {
    //        private static DALSettings_IntegratorConnection CreateDefaultInstance()
    //        {
    //            return new DALSettings_IntegratorConnection() { ConnectionString = "Foo" };
    //        }

    //        private static DALSettings_IntegratorConnection CreateConfiguredInstance()
    //        {
    //            return SettingsInitializator.Instance.ReadSettings<DALSettings_IntegratorConnection>(
    //                @"Kreslik.Integrator.DAL_IntegratorConnection",
    //                @"Kreslik.Integrator.DAL_IntegratorConnection.xsd"
    //                );
    //        }

    //        private static bool IsConfigured
    //        {
    //            get { return SettingsInitializator.Instance.HasSettings(@"Kreslik.Integrator.DAL_IntegratorConnection"); }
    //        }

    //        public string ConnectionString { get; set; }
    //    }

    //    public class DALSettings_DataCollectionConnection
    //    {
    //        public string ConnectionString { get; set; }
    //    }

    //    public class DALSettings_DataCollectionEnabling
    //    {
    //        public bool EnableToBDataCollection { get; set; }
    //        public bool EnableMarketDataCollection { get; set; }
    //    }

    //    public int KillSwitchPollIntervalSeconds { get; set; }
    //    public int KillSwitchWatchDogIntervalSeconds { get; set; }
    //    public string IntegratorSwitchName { get; set; }
    //    public int CommandDistributorPollIntervalSeconds { get; set; }
    //    public DataCollectionSettings DataCollection { get; set; }

    //    [System.Xml.Serialization.XmlIgnore]
    //    public static DALSettings DALBehavior
    //    {
    //        get
    //        {
    //            if (_dalBehavior == null)
    //            {
    //                _dalBehavior =
    //                        SettingsInitializator.Instance.ReadSettings<DALSettings2>(
    //                    @"Kreslik.Integrator.DAL.dll",
    //                    @"Kreslik.Integrator.DAL.dll.xsd"
    //                    );

                    
    //            }

    //            return _dalBehavior;
    //        }
    //    }

    //    private static DALSettings _dalBehavior;

    //    public class DataCollectionSettings : IDataCollectorSettings
    //    {
    //        public int DataProviderId { get; set; }
    //        public int SourceFileId { get; set; }
    //        public int MaxBcpBatchSize { get; set; }
    //        public int MaxInMemoryColectionSize { get; set; }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan MaxIntervalBetweenUploads { get; set; }

    //        [System.Xml.Serialization.XmlElement("MaxIntervalBetweenUploads_Seconds")]
    //        public int MaxIntervalBetweenUploadsXml
    //        {
    //            get { return (int)MaxIntervalBetweenUploads.TotalSeconds; }
    //            set { MaxIntervalBetweenUploads = System.TimeSpan.FromSeconds(value); }
    //        }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan MaxUploadTimeAfterWhichSerializationStarts { get; set; }

    //        [System.Xml.Serialization.XmlElement("MaxUploadTimeAfterWhichSerializationStarts_Seconds")]
    //        public int MaxUploadTimeAfterWhichSerializationStartsXml
    //        {
    //            get { return (int)MaxUploadTimeAfterWhichSerializationStarts.TotalSeconds; }
    //            set { MaxUploadTimeAfterWhichSerializationStarts = System.TimeSpan.FromSeconds(value); }
    //        }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan MaxProcessingTimeAfterShutdownSignalled { get; set; }

    //        [System.Xml.Serialization.XmlElement("MaxProcessingTimeAfterShutdownSignalled_Seconds")]
    //        public int MaxProcessingTimeAfterShutdownSignalledXml
    //        {
    //            get { return (int)MaxProcessingTimeAfterShutdownSignalled.TotalSeconds; }
    //            set { MaxProcessingTimeAfterShutdownSignalled = System.TimeSpan.FromSeconds(value); }
    //        }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan WaitIntervalBetweenBcpRetries { get; set; }

    //        [System.Xml.Serialization.XmlElement("WaitIntervalBetweenBcpRetries_Seconds")]
    //        public int WaitIntervalBetweenBcpRetriesXml
    //        {
    //            get { return (int)WaitIntervalBetweenBcpRetries.TotalSeconds; }
    //            set { WaitIntervalBetweenBcpRetries = System.TimeSpan.FromSeconds(value); }
    //        }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan WaitIntervalBeforeShutdownStartsSerialization { get; set; }

    //        [System.Xml.Serialization.XmlElement("WaitIntervalBeforeShutdownStartsSerialization_Seconds")]
    //        public int WaitIntervalBeforeShutdownStartsSerializationXml
    //        {
    //            get { return (int)WaitIntervalBeforeShutdownStartsSerialization.TotalSeconds; }
    //            set { WaitIntervalBeforeShutdownStartsSerialization = System.TimeSpan.FromSeconds(value); }
    //        }
    //    }
    //}












    //public class DALSettings
    //{
    //    public string ConnectionString { get; set; }
    //    public int KillSwitchPollIntervalSeconds { get; set; }
    //    public int KillSwitchWatchDogIntervalSeconds { get; set; }
    //    public string IntegratorSwitchName { get; set; }
    //    public int CommandDistributorPollIntervalSeconds { get; set; }
    //    public DataCollectionSettings DataCollection { get; set; }

    //    [System.Xml.Serialization.XmlIgnore]
    //    public static DALSettings DALBehavior
    //    {
    //        get
    //        {
    //            if (_dalBehavior == null)
    //            {
    //                _dalBehavior =
    //                        SettingsInitializator.Instance.ReadSettings<DALSettings>(
    //                    @"Kreslik.Integrator.DAL.dll",
    //                    @"Kreslik.Integrator.DAL.dll.xsd"
    //                    );
    //            }

    //            return _dalBehavior;
    //        }
    //    }

    //    private static DALSettings _dalBehavior;

    //    public class DataCollectionSettings : IDataCollectorSettings
    //    {
    //        public string DataCollectionBackendConnectionString { get; set; }
    //        public int DataProviderId { get; set; }
    //        public int SourceFileId { get; set; }
    //        public bool EnableToBDataCollection { get; set; }
    //        public bool EnableMarketDataCollection { get; set; }
    //        public int MaxBcpBatchSize { get; set; }
    //        public int MaxInMemoryColectionSize { get; set; }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan MaxIntervalBetweenUploads { get; set; }

    //        [System.Xml.Serialization.XmlElement("MaxIntervalBetweenUploads_Seconds")]
    //        public int MaxIntervalBetweenUploadsXml
    //        {
    //            get { return (int)MaxIntervalBetweenUploads.TotalSeconds; }
    //            set { MaxIntervalBetweenUploads = System.TimeSpan.FromSeconds(value); }
    //        }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan MaxUploadTimeAfterWhichSerializationStarts { get; set; }

    //        [System.Xml.Serialization.XmlElement("MaxUploadTimeAfterWhichSerializationStarts_Seconds")]
    //        public int MaxUploadTimeAfterWhichSerializationStartsXml
    //        {
    //            get { return (int)MaxUploadTimeAfterWhichSerializationStarts.TotalSeconds; }
    //            set { MaxUploadTimeAfterWhichSerializationStarts = System.TimeSpan.FromSeconds(value); }
    //        }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan MaxProcessingTimeAfterShutdownSignalled { get; set; }

    //        [System.Xml.Serialization.XmlElement("MaxProcessingTimeAfterShutdownSignalled_Seconds")]
    //        public int MaxProcessingTimeAfterShutdownSignalledXml
    //        {
    //            get { return (int)MaxProcessingTimeAfterShutdownSignalled.TotalSeconds; }
    //            set { MaxProcessingTimeAfterShutdownSignalled = System.TimeSpan.FromSeconds(value); }
    //        }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan WaitIntervalBetweenBcpRetries { get; set; }

    //        [System.Xml.Serialization.XmlElement("WaitIntervalBetweenBcpRetries_Seconds")]
    //        public int WaitIntervalBetweenBcpRetriesXml
    //        {
    //            get { return (int)WaitIntervalBetweenBcpRetries.TotalSeconds; }
    //            set { WaitIntervalBetweenBcpRetries = System.TimeSpan.FromSeconds(value); }
    //        }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan WaitIntervalBeforeShutdownStartsSerialization { get; set; }

    //        [System.Xml.Serialization.XmlElement("WaitIntervalBeforeShutdownStartsSerialization_Seconds")]
    //        public int WaitIntervalBeforeShutdownStartsSerializationXml
    //        {
    //            get { return (int)WaitIntervalBeforeShutdownStartsSerialization.TotalSeconds; }
    //            set { WaitIntervalBeforeShutdownStartsSerialization = System.TimeSpan.FromSeconds(value); }
    //        }
    //    }
    //}


    //public class DALSettings
    //{
    //    public string ConnectionString { get; set; }
    //    public int KillSwitchPollIntervalSeconds { get; set; }
    //    public int KillSwitchWatchDogIntervalSeconds { get; set; }
    //    public string IntegratorSwitchName { get; set; }
    //    public int CommandDistributorPollIntervalSeconds { get; set; }
    //    public DataCollectionSettings DataCollection { get; set; }

    //    [System.Xml.Serialization.XmlIgnore]
    //    public static DALSettings DALBehavior
    //    {
    //        get
    //        {
    //            if (_dalBehavior == null)
    //            {
    //                _dalBehavior =
    //                        SettingsInitializator.Instance.ReadSettings<DALSettings>(
    //                    @"Kreslik.Integrator.DAL.dll",
    //                    @"Kreslik.Integrator.DAL.dll.xsd"
    //                    );
    //            }

    //            return _dalBehavior;
    //        }
    //    }

    //    private static DALSettings _dalBehavior;

    //    public class DataCollectionSettings : IDataCollectorSettings
    //    {
    //        public string DataCollectionBackendConnectionString { get; set; }
    //        public int DataProviderId { get; set; }
    //        public int SourceFileId { get; set; }
    //        public bool EnableToBDataCollection { get; set; }
    //        public bool EnableMarketDataCollection { get; set; }
    //        public int MaxBcpBatchSize { get; set; }
    //        public int MaxInMemoryColectionSize { get; set; }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan MaxIntervalBetweenUploads { get; set; }

    //        [System.Xml.Serialization.XmlElement("MaxIntervalBetweenUploads_Seconds")]
    //        public int MaxIntervalBetweenUploadsXml
    //        {
    //            get { return (int)MaxIntervalBetweenUploads.TotalSeconds; }
    //            set { MaxIntervalBetweenUploads = System.TimeSpan.FromSeconds(value); }
    //        }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan MaxUploadTimeAfterWhichSerializationStarts { get; set; }

    //        [System.Xml.Serialization.XmlElement("MaxUploadTimeAfterWhichSerializationStarts_Seconds")]
    //        public int MaxUploadTimeAfterWhichSerializationStartsXml
    //        {
    //            get { return (int)MaxUploadTimeAfterWhichSerializationStarts.TotalSeconds; }
    //            set { MaxUploadTimeAfterWhichSerializationStarts = System.TimeSpan.FromSeconds(value); }
    //        }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan MaxProcessingTimeAfterShutdownSignalled { get; set; }

    //        [System.Xml.Serialization.XmlElement("MaxProcessingTimeAfterShutdownSignalled_Seconds")]
    //        public int MaxProcessingTimeAfterShutdownSignalledXml
    //        {
    //            get { return (int)MaxProcessingTimeAfterShutdownSignalled.TotalSeconds; }
    //            set { MaxProcessingTimeAfterShutdownSignalled = System.TimeSpan.FromSeconds(value); }
    //        }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan WaitIntervalBetweenBcpRetries { get; set; }

    //        [System.Xml.Serialization.XmlElement("WaitIntervalBetweenBcpRetries_Seconds")]
    //        public int WaitIntervalBetweenBcpRetriesXml
    //        {
    //            get { return (int)WaitIntervalBetweenBcpRetries.TotalSeconds; }
    //            set { WaitIntervalBetweenBcpRetries = System.TimeSpan.FromSeconds(value); }
    //        }

    //        [System.Xml.Serialization.XmlIgnore]
    //        public System.TimeSpan WaitIntervalBeforeShutdownStartsSerialization { get; set; }

    //        [System.Xml.Serialization.XmlElement("WaitIntervalBeforeShutdownStartsSerialization_Seconds")]
    //        public int WaitIntervalBeforeShutdownStartsSerializationXml
    //        {
    //            get { return (int)WaitIntervalBeforeShutdownStartsSerialization.TotalSeconds; }
    //            set { WaitIntervalBeforeShutdownStartsSerialization = System.TimeSpan.FromSeconds(value); }
    //        }
    //    }
    //}
}
