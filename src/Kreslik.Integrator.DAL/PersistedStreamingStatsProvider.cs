﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.DAL
{
    public class PersistedStreamingStatsProvider : IPersistedStreamingStatsProvider
    {

        public void GetStreamingSessionStats(Counterparty counterparty, out int dealsNum, out int rejectionsNum)
        {
            dealsNum = 0;
            rejectionsNum = 0;

            try
            {
                using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (
                        SqlCommand getStreamStatsCommand = new SqlCommand("[dbo].[StreamingSessionStatsToday_SP]", sqlConnection))
                    {
                        getStreamStatsCommand.CommandType = CommandType.StoredProcedure;
                        getStreamStatsCommand.Parameters.AddWithValue("@CounterpartyCode", counterparty.ToString());

                        using (var reader = getStreamStatsCommand.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                int? dealsNumNullable = reader.GetNullableInt32(reader.GetOrdinal("DealsNum"));
                                int? rejectionsNumNullable = reader.GetNullableInt32(reader.GetOrdinal("RejectionsNum"));

                                if (dealsNumNullable.HasValue)
                                    dealsNum = dealsNumNullable.Value;

                                if (rejectionsNumNullable.HasValue)
                                    rejectionsNum = rejectionsNumNullable.Value;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null).LogException(LogLevel.Fatal, "Unexpected exception during initializing streaming stats (for rejection rate checker) - so no history will be considered", e);
            }
        }
    }

    public class SettlementDatesInfoPersistor : ISettlementDatesInfoPersistor
    {
        public void UpdateSettlementInfo(IEnumerable<Tuple<Symbol, DateTime>> info)
        {
            try
            {
                using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (
                        SqlCommand updateStatsCommand = new SqlCommand("[dbo].[SettlementDatesUpdateSelected_SP]", sqlConnection))
                    {
                        updateStatsCommand.CommandType = CommandType.StoredProcedure;
                        updateStatsCommand.Parameters.AddWithValue("@SymbolsAndDatesList",
                            string.Join("|",
                                info.Select(
                                    tuple => string.Format("{0}|{1:yyyy-MM-dd}", tuple.Item1.ShortName, tuple.Item2))));
                        updateStatsCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null).LogException(LogLevel.Fatal, "Unexpected exception during updating settlemt dates stats. Not retrying", e);
            }
        }

        public void ClearAllSettlemntInfo()
        {
            try
            {
                using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (
                        SqlCommand updateStatsCommand = new SqlCommand("[dbo].[SettlementDatesClearAll_SP]", sqlConnection))
                    {
                        updateStatsCommand.CommandType = CommandType.StoredProcedure;
                        updateStatsCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null).LogException(LogLevel.Fatal, "Unexpected exception during clearing all settlemt dates stats. Not retrying", e);
            }
        }

        public void ClearSettlemntInfo(IEnumerable<Symbol> symbols)
        {
            try
            {
                using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (
                        SqlCommand updateStatsCommand = new SqlCommand("[dbo].[SettlementDatesClearSelected_SP]", sqlConnection))
                    {
                        updateStatsCommand.CommandType = CommandType.StoredProcedure;
                        updateStatsCommand.Parameters.AddWithValue("@SymbolsCsvList", string.Join(",", symbols.Select(s => s.ShortName)));
                        updateStatsCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null).LogException(LogLevel.Fatal, "Unexpected exception during clearing selected settlemt dates stats. Not retrying", e);
            }
        }
    }
}
