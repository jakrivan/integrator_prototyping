﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.DAL
{
    public class PriceHistoryProvider : IPriceHistoryProvider
    {
        public IEnumerable<Tuple<decimal, PriceSide, DateTime>> GetLastPricesForSymbol(Symbol symbol, int pricesCount)
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.DataCollectionConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getPricesCommand = new SqlCommand("[dbo].[GetLastLmaxPricesForPair_SP]", sqlConnection))
                {
                    getPricesCommand.CommandType = CommandType.StoredProcedure;
                    getPricesCommand.Parameters.AddWithValue("@TopCount", pricesCount*3);
                    getPricesCommand.Parameters.AddWithValue("@SymbolName", symbol.ShortName);

                    using (var reader = getPricesCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            decimal price = reader.GetDecimal(reader.GetOrdinal("Price"));
                            PriceSide side = (PriceSide)Enum.Parse(typeof(PriceSide), reader.GetString(reader.GetOrdinal("Side")));
                            DateTime timeStamp = reader.GetDateTime(reader.GetOrdinal("IntegratorReceivedTimeUtc"));
                            yield return new Tuple<decimal, PriceSide, DateTime>(price, side, timeStamp);
                        }
                    }
                }
            }
        }

        public IEnumerable<Tuple<Symbol, decimal, DateTime>> GetLastKnownReferencePrices()
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getPricesCommand = new SqlCommand("[dbo].[GetSymbolReferencePrices_SP]", sqlConnection))
                {
                    getPricesCommand.CommandType = CommandType.StoredProcedure;

                    using (var reader = getPricesCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Symbol symbol = (Symbol) reader.GetString(reader.GetOrdinal("Name"));
                            decimal price;
                            if (reader.IsDBNull(reader.GetOrdinal("LastKnownMeanReferencePrice")))
                                price = 0m;
                            else
                                price = reader.GetDecimal(reader.GetOrdinal("LastKnownMeanReferencePrice"));
                            DateTime lastUpdated;
                            if (reader.IsDBNull(reader.GetOrdinal("MeanReferencePriceUpdated")))
                                lastUpdated = DateTime.MinValue;
                            else
                                lastUpdated = reader.GetDateTime(reader.GetOrdinal("MeanReferencePriceUpdated"));
                            yield return new Tuple<Symbol, decimal, DateTime>(symbol, price, lastUpdated);
                        }
                    }
                }
            }
        }

        public void UpdateReferencePrices(string askPricesArgList, string bidPricesArgList)
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand updatePriceCommand = new SqlCommand("[dbo].[UpdateSymbolPrices_SP]", sqlConnection))
                {
                    updatePriceCommand.CommandType = CommandType.StoredProcedure;
                    updatePriceCommand.Parameters.AddWithValue("@AskPricesList", askPricesArgList);
                    updatePriceCommand.Parameters.AddWithValue("@BidPricesList", bidPricesArgList);

                    updatePriceCommand.ExecuteNonQuery();
                }
            }
        }

        public SymbolInfoBag[][] GetSymbolsCounterpartyInfo(ILogger logger)
        {
            SymbolInfoBag[][] tradingTargetsSymbolsInfoBags = new SymbolInfoBag[(int)TradingTargetType.ValuesCount][];

            for (int tradingTypeId = 0; tradingTypeId < (int)TradingTargetType.ValuesCount; tradingTypeId++)
            {
                tradingTargetsSymbolsInfoBags[tradingTypeId] = new SymbolInfoBag[Symbol.ValuesCount];
            }


            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.DataCollectionConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getSettingsCommand = new SqlCommand("[dbo].[GetCounterpartySymbolSettings_SP]", sqlConnection))
                {
                    getSettingsCommand.CommandType = CommandType.StoredProcedure;

                    using (var reader = getSettingsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TradingTargetType tradingTargetType = (TradingTargetType)
                                Enum.Parse(typeof(TradingTargetType), reader.GetString(reader.GetOrdinal("TradingTargetType")));
                            Symbol symbol = (Symbol)reader.GetString(reader.GetOrdinal("Symbol"));
                            bool supported = reader.GetBoolean(reader.GetOrdinal("Supported"));
                            decimal? minimumPriceIncrement = null;
                            decimal? orderMinimumSize = null;
                            decimal? orderMinimumSizeIncrement = null;
                            if (supported)
                            {
                                if (reader.IsDBNull(reader.GetOrdinal("MinimumPriceIncrement")))
                                {
                                    logger.Log(LogLevel.Error, "Symbol {0} for {1} is set as supported but MinimumPriceIncrement information is not provided");
                                }
                                else
                                {
                                    minimumPriceIncrement = reader.GetDecimal(reader.GetOrdinal("MinimumPriceIncrement"));
                                }

                                if (reader.IsDBNull(reader.GetOrdinal("OrderMinimumSize")))
                                {
                                    logger.Log(LogLevel.Error, "Symbol {0} for {1} is set as supported but OrderMinimumSize information is not provided");
                                }
                                else
                                {
                                    orderMinimumSize = reader.GetDecimal(reader.GetOrdinal("OrderMinimumSize"));
                                }

                                if (reader.IsDBNull(reader.GetOrdinal("OrderMinimumSizeIncrement")))
                                {
                                    logger.Log(LogLevel.Error, "Symbol {0} for {1} is set as supported but OrderMinimumSizeIncrement information is not provided");
                                }
                                else
                                {
                                    orderMinimumSizeIncrement = reader.GetDecimal(reader.GetOrdinal("OrderMinimumSizeIncrement"));
                                }
                            }

                            var symInfo = new SymbolInfoBag(symbol, tradingTargetType, supported, minimumPriceIncrement,
                                                            orderMinimumSize, orderMinimumSizeIncrement);
                            symInfo.Normalize();

                            tradingTargetsSymbolsInfoBags[(int) tradingTargetType][(int) symbol] = symInfo;

                        }
                    }
                }
            }

            return tradingTargetsSymbolsInfoBags;
        }

        public List<Currency> GetSupportedCurrenciesList()
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.DataCollectionConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getCurrenciesCommand = new SqlCommand("[dbo].[GetSupportedCurrencies_SP]", sqlConnection))
                {
                    getCurrenciesCommand.CommandType = CommandType.StoredProcedure;

                    List<Currency> supportedCurrencies = new List<Currency>();

                    using (SqlDataReader reader = getCurrenciesCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            supportedCurrencies.Add((Currency) reader.GetString(reader.GetOrdinal("CurrencyAlphaCode")));
                        }
                    }

                    return supportedCurrencies;
                }
            }
        }

        public decimal[] GetCounterpartyCommisionsPricePerMillionMap(ILogger logger)
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getComissionsCommand = new SqlCommand("[dbo].[GetCommisionsList_SP]", sqlConnection))
                {
                    getComissionsCommand.CommandType = CommandType.StoredProcedure;

                    decimal[] commisionsMap = new decimal[Counterparty.ValuesCount];
                    List<Counterparty> notPopulatedCounterparties = new List<Counterparty>(Counterparty.Values); 

                    using (SqlDataReader reader = getComissionsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Counterparty ctp = (Counterparty) reader.GetString(reader.GetOrdinal("Counterparty"));
                            decimal commisionPricePerMillion =
                                reader.GetDecimal(reader.GetOrdinal("CommissionPricePerMillion"));

                            commisionsMap[(int) ctp] = commisionPricePerMillion;
                            notPopulatedCounterparties.Remove(ctp);
                        }
                    }

                    if (notPopulatedCounterparties.Any())
                    {
                        logger.Log(LogLevel.Fatal,
                                   "Counterparties without commisions mapping in backend: [{0}]. Continuing, but this can yield some calculations incorrect",
                                   string.Join(", ", notPopulatedCounterparties));
                    }

                    return commisionsMap;
                }
            }
        }
    }
}
