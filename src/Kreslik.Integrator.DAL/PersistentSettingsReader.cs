﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.DAL
{
    public interface IPersistentSettingsReader<T> where T : class
    {
        T ReadUpdatedSettings();
        T ReadSettings();
        //void UpdateSettings(T settings);
        void RegisterSettingsHandler(Action<T> onSettingsChanged);
    }

    public class PersistentSettingsReader<T> : IPersistentSettingsReader<T> where T : class
    {
        private ILogger _logger;
        private string _settingName;
        private string _environmentName;
        private string _xsdConfiguration;
        private TimeSpan _refreshInterval;
        private SafeTimer _pollTimer;
        private DateTime _lastCheckTime = System.Data.SqlTypes.SqlDateTime.MinValue.Value;
        private Action<T> _onSettingsChanged;
        private static TimeSpan _defaultSettingsRefreshInterval = TimeSpan.FromSeconds(5);

        public PersistentSettingsReader(ILogger logger, string settingName, string environmentName, string xsdConfiguration, TimeSpan refreshInterval)
        {
            this._logger = logger;
            this._settingName = settingName;
            this._environmentName = environmentName;
            this._xsdConfiguration = xsdConfiguration;
            this._refreshInterval = refreshInterval;
        }

        public PersistentSettingsReader(ILogger logger, string settingName, string xsdConfigurationUri)
            : this(logger, settingName, SettingsInitializator.Instance.EnvironmentName, xsdConfigurationUri, _defaultSettingsRefreshInterval)
        { }

        private void CheckSettingsChanged()
        {
            T newSettingObj = this.ReadUpdatedSettings();
            if (newSettingObj != null && _onSettingsChanged != null)
            {
                _onSettingsChanged(newSettingObj);
            }
        }

        public T ReadUpdatedSettings()
        {
            return this.ReadSettings(this._lastCheckTime);
        }

        public T ReadSettings()
        {
            return this.ReadSettings(System.Data.SqlTypes.SqlDateTime.MinValue.Value);
        }

        private T ReadSettings(DateTime updatedFromTime)
        {
            T settingObj = null;

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand getItemsCommand = new SqlCommand("[dbo].[GetSettingsXml_SP]", sqlConnection))
                    {
                        getItemsCommand.CommandType = CommandType.StoredProcedure;
                        getItemsCommand.Parameters.AddWithValue("@EnvironmentName", this._environmentName);
                        getItemsCommand.Parameters.AddWithValue("@SettingName", this._settingName);
                        getItemsCommand.Parameters.AddWithValue("@UpdatedAfter", updatedFromTime);

                        DateTime newCheckTime = DateTime.UtcNow;
                        using (var reader = getItemsCommand.ExecuteXmlReader())
                        {
                            if (reader.Read())
                            {
                                string xmlString = reader.ReadOuterXml();
                                this._logger.Log(LogLevel.Info, "Updating settings, new xml: {0}", xmlString);

                                settingObj = SettingsReader.DeserializeConfigurationFromString<T>(xmlString,
                                                                                            this._xsdConfiguration);
                                this._lastCheckTime = newCheckTime;
                            }
                        }
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                _logger.LogException(LogLevel.Error, sqlEx, "Got SqlException when obtaining Settings for {0}", this._settingName);
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, e, "Got unexpected Exception when obtaining Settings for {0}", this._settingName);
            }

            return settingObj;
        }

        //public void UpdateSettings(T settings)
        //{
        //    bool updated = false;
        //    for (int i = 1; i < 6 && !updated; i++)
        //    {
        //        try
        //        {
        //            using (SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
        //            {
        //                sqlConnection.Open();
        //                using (SqlCommand updateSettingsCommand = new SqlCommand("[dbo].[UpdateSettingsXml_SP]", sqlConnection))
        //                {
        //                    updateSettingsCommand.CommandType = CommandType.StoredProcedure;
        //                    updateSettingsCommand.Parameters.AddWithValue("@SettingName", this._settingName);
        //                    updateSettingsCommand.Parameters.AddWithValue("@SettingsXml", SettingsReader.SerializeConfiguration(settings));

        //                    updateSettingsCommand.ExecuteNonQuery();
        //                    updated = true;
        //                }
        //            }
        //        }
        //        catch (SqlException sqlEx)
        //        {
        //            _logger.LogException(LogLevel.Error, sqlEx, "Got SqlException when updating Settings for {0}", this._settingName);
        //        }
        //        catch (Exception e)
        //        {
        //            _logger.LogException(LogLevel.Fatal, e, "Got unexpected Exception when updating Settings for {0}", this._settingName);
        //        }

        //        if (!updated)
        //        {
        //            Thread.Sleep(TimeSpan.FromSeconds(20*i));
        //        }
        //    }
        //}

        public void RegisterSettingsHandler(Action<T> onSettingsChanged)
        {
            if (_pollTimer != null)
            {
                _pollTimer.Dispose();
            }

            this._onSettingsChanged = onSettingsChanged;
            _pollTimer = new SafeTimer(CheckSettingsChanged, _refreshInterval, _refreshInterval)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.GetTimerPrecisionForDelay(_refreshInterval),
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
        }
    }
}
