﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.SessionManagement
{
    public class HotspotPricesAdapter : IMarketDataSession
    {
        private ToBReceiver _toBReceiver;
        private PriceReceivingHelper _priceReceivingHelper;
        private IDelayEvaluatorMulti _delayEvaluator;
        private HotspotCounterparty _counterpartyToBeIgnored;

        public HotspotPricesAdapter(ToBReceiver toBReceiver, IDelayEvaluatorMulti delayEvaluator, HotspotCounterparty counterpartyToBeIgnored)
        {
            _toBReceiver = toBReceiver;
            _priceReceivingHelper = new PriceReceivingHelper(10);
            this._delayEvaluator = delayEvaluator;
            this._counterpartyToBeIgnored = counterpartyToBeIgnored;
            _toBReceiver.NewPriceAvailable += ToBReceiverOnNewPriceAvailable;
            _toBReceiver.PriceCanceled += ToBReceiverOnPriceCanceled;
            _toBReceiver.Stopped += ToBReceiverOnStopped;
        }

        private void ToBReceiverOnPriceCanceled(Symbol symbol, PriceSide priceSide, Counterparty cpt)
        {
            if (OnCancelLastPrice != null && cpt != _counterpartyToBeIgnored)
                OnCancelLastPrice(symbol, cpt, priceSide);
        }

        private void ToBReceiverOnStopped()
        {
            if (OnCancelLastQuote != null)
            {
                foreach (HotspotCounterparty hotspotCounterparty in HotspotCounterparty.Values)
                {
                    foreach (Symbol symbol in Symbol.Values)
                    {
                        OnCancelLastQuote(symbol, hotspotCounterparty);
                    }
                }
            }
        }

        private void ToBReceiverOnNewPriceAvailable(AtomicDecimal price, decimal size, Symbol symbol, PriceSide side, Counterparty counterparty, DateTime counterpartySent, DateTime integratorReceived)
        {
            if(counterparty == _counterpartyToBeIgnored)
                return;

            PriceObjectInternal priceObject = this._priceReceivingHelper.GetPriceObject(price, size, side, symbol,
                counterparty, null, MarketDataRecordType.BankQuoteData, integratorReceived, counterpartySent);

            bool heldWithoutAcquireCall = false;

            try
            {
                bool isDelayed = this._delayEvaluator.IsReceivedObjectDelayed(priceObject);

                //order is important - we need to call into IsIgnoredPrice
                if (isDelayed)
                {
                    if (this.OnIgnoredPrice != null)
                        this.OnIgnoredPrice(priceObject, ref heldWithoutAcquireCall);
                }
                else
                {
                    if (this.OnNewPrice != null)
                        this.OnNewPrice(priceObject, ref heldWithoutAcquireCall);
                }
            }
            finally
            {
                if (!heldWithoutAcquireCall)
                    priceObject.Release();
            }
        }

        public ISubscription Subscribe(SubscriptionRequestInfo subscriptionRequestInfo)
        {
            throw new NotImplementedException();
        }

        public bool Unsubscribe(ISubscription subscriptionInfo)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ISubscription> Subscriptions
        {
            get { throw new NotImplementedException(); }
        }

        public event Action<ISubscription> OnSubscriptionChanged;

        public event HandlePriceObject OnNewPrice;
        public event Action<PriceObjectInternal> OnNewNonExecutablePrice;

        public event HandlePriceObject OnIgnoredPrice;

        public event Action<Common.Symbol, Common.Counterparty> OnCancelLastQuote;

        public event Action<Common.Symbol, Common.Counterparty, Common.PriceSide> OnCancelLastPrice;

        public void Start()
        {
            //throw new NotImplementedException();
        }

        public void Stop()
        {
            //throw new NotImplementedException();
        }

        public void StopAsync(TimeSpan delay)
        {
            throw new NotImplementedException();
        }

        public event Action OnStopped
        {
            add { _toBReceiver.Stopped += value; }
            remove { _toBReceiver.Stopped -= value; }
        }

        public Contracts.SessionState State
        {
            get { return this._toBReceiver.Ready ? SessionState.Running : SessionState.Idle; }
        }

        public bool Inactivated
        {
            get { return !this._toBReceiver.Ready; }
        }

        public event Action OnStopInitiated;

        public event Action OnStarted;

        public bool IsReadyAndOpen
        {
            get { return this._toBReceiver.Ready; }
        }

        public Common.Counterparty Counterparty
        {
            get { return HotspotCounterparty.H4T; }
        }
    }
}
