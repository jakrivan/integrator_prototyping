﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using Kreslik.Integrator.LowLatencyUtils;
using Kreslik.Integrator.QuickItchN;

namespace Kreslik.Integrator.SessionManagement
{
    public class ConnectionObjectsCollection
    {
        private static int _instancesCount = 0;
        private readonly IRiskManager _riskManager;
        private readonly ISymbolsInfo _symbolsInfo;
        private readonly IDealStatistics _dealStatistics;
        private readonly IIntegratorInstanceUtils _integratorInstanceUtils;
        private readonly ILogger _logger;

        public static ConnectionObjectsCollection CreateOnlyMarketSessionsCollection(
            IIntegratorInstanceUtils integratorInstanceUtils, ILogger logger)
        {
            return new ConnectionObjectsCollection(null, null, null, integratorInstanceUtils, null, logger, true, null);
        }

        public static ConnectionObjectsCollection CreateCompleteSessionsCollection(
            IRiskManager riskManager, ISymbolsInfo symbolsInfo, IDealStatistics dealStatistics, IIntegratorInstanceUtils integratorInstanceUtils,
            IStreamingUnicastInfoForwarder streamingUnicastInfoForwarder, ILogger logger, ToBReceiver toBReceiver)
        {
            return new ConnectionObjectsCollection(riskManager, symbolsInfo, dealStatistics, 
                integratorInstanceUtils, streamingUnicastInfoForwarder, logger, false, toBReceiver);
        }

        private ConnectionObjectsCollection(IRiskManager riskManager, ISymbolsInfo symbolsInfo, IDealStatistics dealStatistics,
            IIntegratorInstanceUtils integratorInstanceUtils, IStreamingUnicastInfoForwarder streamingUnicastInfoForwarder,
            ILogger logger, bool isOffloadingMarketDataProcess, ToBReceiver toBReceiver)
        {
            if (Interlocked.Increment(ref _instancesCount) > 1)
            {
                throw new Exception("Only one instance of ConnectionObjectsCollection is allowed per process");
            }

            this._riskManager = riskManager;
            this._symbolsInfo = symbolsInfo;
            this._dealStatistics = dealStatistics;
            this._integratorInstanceUtils = integratorInstanceUtils;
            this._logger = logger;

            this.TakerConnectionObjectsKeepers =
                new List<ITakerConnectionObjectsKeeper>(Enumerable.Repeat<ITakerConnectionObjectsKeeper>(null,
                    Counterparty.ValuesCount));

            this.ConnectionObjectsKeepers =
                new List<IConnectionObjectsKeeper>(Enumerable.Repeat<IConnectionObjectsKeeper>(null,
                    Counterparty.ValuesCount));

            IPriceDelayEvaluatorProvider priceDelayEvaluatorProvider = new DelayEvaluatorDal(DiagnosticsManager.DiagLogger, riskManager);

            if(toBReceiver != null)
                HotspotPricesAdapter = new HotspotPricesAdapter(toBReceiver, priceDelayEvaluatorProvider.GetDelayEvaluatorMultiDestination(), HotspotCounterparty.HTF);

            foreach (Counterparty counterparty in SessionsSettings.SubscriptionSettings.EnabledCounterparts)
            {
                CreateAndBindCounterparty(counterparty, isOffloadingMarketDataProcess, 
                    priceDelayEvaluatorProvider.GetDelayEvaluator(counterparty), streamingUnicastInfoForwarder);
            }

            //Setup filtering of Lmax prices from L01
            var l01Keeper = this.TakerConnectionObjectsKeepers[(int) Counterparty.L01];
            if (l01Keeper != null)
            {
                var lx1Keeper = this.TakerConnectionObjectsKeepers[(int)Counterparty.LX1];
                if (lx1Keeper != null)
                    SessionFactory.BindChannelsIfNeeded(l01Keeper.MarketDataSession, lx1Keeper.OrderFlowSession);

                var lgaKeeper = this.TakerConnectionObjectsKeepers[(int)Counterparty.LGA];
                if (lgaKeeper != null)
                    SessionFactory.BindChannelsIfNeeded(l01Keeper.MarketDataSession, lgaKeeper.OrderFlowSession);

                var lcaKeeper = this.TakerConnectionObjectsKeepers[(int)Counterparty.LGC];
                if (lcaKeeper != null)
                    SessionFactory.BindChannelsIfNeeded(l01Keeper.MarketDataSession, lcaKeeper.OrderFlowSession);
            }

            priceDelayEvaluatorProvider.SetSessionStateWatchers(
                this.TakerMarketDataSessions.Union<IUnderlyingSessionState>(this.StreamingChannels)
                    .Union(new[] {HotspotPricesAdapter}));

            if(toBReceiver == null)
                priceDelayEvaluatorProvider.ClearStateWatchers(ToBconverter.SupportedCounterparties.Select(c => (Counterparty)c));

            if (!isOffloadingMarketDataProcess)
            {
                this.StpFlowSessions = StpConnectionObjectsKeeper.CreateStpSessions(dealStatistics);
            }

            integratorInstanceUtils.CounterpartyClaimStatusChanged += OnCounterpartyClaimStatusChanged;
        }

        public IMarketDataSession HotspotPricesAdapter { get; private set; }

        private void OnCounterpartyClaimStatusChanged(Counterparty counterparty, bool claimedByCurrentnstance)
        {
            this._logger.Log(LogLevel.Info, "Session {0} became {1}available", counterparty,
                             claimedByCurrentnstance ? string.Empty : "UN");

            //need to be stopped
            if (!claimedByCurrentnstance)
            {
                this.StopOrderFlowSession(counterparty, TimeSpan.Zero);
                this.StopMarketDataSession(counterparty);
            }
        }

        private void CreateAndBindCounterparty(Counterparty counterparty, bool isOffloadingMarketDataProcess, 
            IDelayEvaluator delayEvaluator, IStreamingUnicastInfoForwarder streamingUnicastInfoForwarder)
        {
            bool marketDataInSeparateProcess =
                SessionsSettings.SubscriptionSettings.EnabledCounterpartsWithMDSessionInSeparateProcess.Contains(
                    counterparty);

            //we are in separate offloading process, but offloading was not requested for this counterparty
            if(isOffloadingMarketDataProcess && !marketDataInSeparateProcess)
                return;

            //beware on hotspot - it has streaming channel but at the same time it has INcomming pricing (itch) channel
            if (!isOffloadingMarketDataProcess && !Counterparty.IsCounterpartyForTakeing(counterparty))
            {
                var makerObjectsKeeper = MakerConnectionObjectsKeeper.CreateMakerConnectionObjectsKeeper(counterparty, 
                    _dealStatistics, _riskManager, _symbolsInfo, streamingUnicastInfoForwarder, delayEvaluator);
                ConnectionObjectsKeepers[(int)counterparty] = makerObjectsKeeper;
                return;
            }

            var conectionObjectsKeeper = TakerConnectionObjectsKeeper.CreateConnectionObjectsKeeper(
                counterparty, 
                SessionsSettings.SubscriptionSettings.ResubscribeOnReconnect, 
                _riskManager, 
                _dealStatistics,
                delayEvaluator,
                (isOffloadingMarketDataProcess && marketDataInSeparateProcess) || (!isOffloadingMarketDataProcess && !marketDataInSeparateProcess), 
                !isOffloadingMarketDataProcess);

            TakerConnectionObjectsKeepers[(int)counterparty] = conectionObjectsKeeper;
            ConnectionObjectsKeepers[(int)counterparty] = conectionObjectsKeeper;

            if (conectionObjectsKeeper == null || conectionObjectsKeeper.MarketDataSession == null)
            {
                return;
            }


            Counterparty counterpartyLocal = counterparty;
            conectionObjectsKeeper.MarketDataSession.OnStarted += () =>
            {
                List<ISubscription> subscriptions;
                if (!_subscriptionsBag.TryGetValue(counterpartyLocal, out subscriptions))
                {
                    System.Threading.Thread.Sleep(TimeSpan.FromSeconds(3));

                    subscriptions = new List<ISubscription>();
                    foreach (
                        Tuple<Symbol, decimal, string> subscriptionDetails in
                            SessionsSettings.SubscriptionSettings[counterpartyLocal])
                    {
                        var sub =
                            conectionObjectsKeeper.MarketDataSession.Subscribe(
                                new SubscriptionRequestInfo(subscriptionDetails.Item1, subscriptionDetails.Item2,
                                                            subscriptionDetails.Item3));
                        if (sub != null)
                        {
                            subscriptions.Add(sub);
                        }
                        else
                        {
                            conectionObjectsKeeper.QuotesLogger.Log(LogLevel.Error,
                                                                    "Subscription for [{0}] in [{1}] failed",
                                                                    subscriptionDetails.Item1, counterpartyLocal);
                        }
                    }
                    _subscriptionsBag.TryAdd(counterpartyLocal, subscriptions);
                }
            };
        }

        public IList<ITakerConnectionObjectsKeeper> TakerConnectionObjectsKeepers { get; private set; }
        public IList<IConnectionObjectsKeeper> ConnectionObjectsKeepers { get; private set; }

        public IEnumerable<IStpFlowSession> StpFlowSessions;

        public IEnumerable<IOrderFlowSession> TakerOrderFlowSessions
        {
            get { return TakerConnectionObjectsKeepers.Select(o => o == null ? null : o.OrderFlowSession); }
        }

        private IEnumerable<IMarketDataSession> TakerMarketDataSessions
        {
            get { return TakerConnectionObjectsKeepers.Select(o => o == null ? null : o.MarketDataSession); }
        }

        public IEnumerable<IFIXChannel> OrderFlowSessions
        {
            get { return ConnectionObjectsKeepers.Select(o => o == null ? null : o.OFSession); }
        }

        public IEnumerable<IFIXChannel> MarketDataSessions
        {
            get { return ConnectionObjectsKeepers.Select(o => o == null ? null : o.MDSession); }
        }

        public IEnumerable<IStreamingChannel> StreamingChannels
        {
            get { return ConnectionObjectsKeepers.OfType<MakerConnectionObjectsKeeper>().Where(o => o.StreamingChannel != null).Select(o => o.StreamingChannel); } 
        }

        public IntegratorRequestResult StopAndReleaseSessions(Counterparty counterparty)
        {
            var result = this.StopMarketDataSession(counterparty) + this.StopOrderFlowSession(counterparty, TimeSpan.Zero);
            if (result.RequestSucceeded)
            {
                this._integratorInstanceUtils.ReleaseCounterpartySession(counterparty);
            }

            return result;
        }

        public IntegratorRequestResult RestartMarketDataSession(Counterparty counterparty)
        {
            if (ConnectionObjectsKeepers[(int)counterparty] != null)
            {
                //Attempt to first claim the counterparty
                if (!
                    //HACK: allow to start Hotspot MD session without claiming
                    (counterparty.TradingTargetType == TradingTargetType.Hotspot ||
                      this._integratorInstanceUtils.IsCounterpartySessionClaimed(counterparty))
                    )
                {
                    var result = this._integratorInstanceUtils.TryClaimCounterpartySession(counterparty);
                    if (!result.RequestSucceeded)
                        return result;
                }

                if (Counterparty.OrdersOnlyCounterparties.Contains(counterparty))
                    return IntegratorRequestResult.GetSuccessResult();

                if (ConnectionObjectsKeepers[(int) counterparty] == null ||
                    ConnectionObjectsKeepers[(int) counterparty].MDSession == null)
                {
                    return IntegratorRequestResult.CreateFailedResult("Cannot restart MD session - MD not configured for counterparty");
                }
                else if (ConnectionObjectsKeepers[(int)counterparty].MDSession.State != SessionState.Stopping)
                {
                    ConnectionObjectsKeepers[(int)counterparty].MDSession.Stop();
                    System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(200));
                    ConnectionObjectsKeepers[(int)counterparty].MDSession.Start();

                    return IntegratorRequestResult.GetSuccessResult();
                }
                else
                {
                    ConnectionObjectsKeepers[(int)counterparty].QuotesLogger.Log(LogLevel.Warn, "Cannot restart session that is in stopping state");
                    return IntegratorRequestResult.CreateFailedResult("Cannot restart session that is in stopping state");
                }
            }

            return IntegratorRequestResult.CreateFailedResult("Cannot restart session - counterparty is not configured");
        }

        public IntegratorRequestResult RestartOrderFlowSession(Counterparty counterparty)
        {
            if (ConnectionObjectsKeepers[(int)counterparty] != null)
            {
                //Attempt to first claim the counterparty
                if (!this._integratorInstanceUtils.IsCounterpartySessionClaimed(counterparty))
                {
                    var result = this._integratorInstanceUtils.TryClaimCounterpartySession(counterparty);
                    if (!result.RequestSucceeded)
                        return result;
                }

                if (ConnectionObjectsKeepers[(int)counterparty].OFSession.State != SessionState.Stopping)
                {
                    ConnectionObjectsKeepers[(int)counterparty].OFSession.Stop();
                    System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(200));
                    ConnectionObjectsKeepers[(int)counterparty].OFSession.Start();

                    return IntegratorRequestResult.GetSuccessResult();
                }
                else
                {
                    ConnectionObjectsKeepers[(int)counterparty].OrdersLogger.Log(LogLevel.Warn, "Cannot restart session that is in stopping state");
                    return IntegratorRequestResult.CreateFailedResult("Cannot restart session that is in stopping state");
                }
            }

            return IntegratorRequestResult.CreateFailedResult("Cannot restart session - counterparty is not configured");
        }

        //This method doesn't release claimed session - as it might be just an intermediate stop caused e.g. by broken order
        public IntegratorRequestResult StopMarketDataSession(Counterparty counterparty)
        {
            if (ConnectionObjectsKeepers[(int)counterparty] != null && ConnectionObjectsKeepers[(int)counterparty].MDSession != null)
            {
                if (ConnectionObjectsKeepers[(int)counterparty].MDSession.State != SessionState.Stopping)
                {
                    ConnectionObjectsKeepers[(int)counterparty].MDSession.Stop();
                    return IntegratorRequestResult.GetSuccessResult();
                }
                else
                {
                    ConnectionObjectsKeepers[(int)counterparty].QuotesLogger.Log(LogLevel.Warn, "Cannot stop session that is in stopping state");
                    return IntegratorRequestResult.CreateFailedResult("Cannot stop session that is in stopping state");
                }
            }

            //LX1 does not have MD
            if (Counterparty.OrdersOnlyCounterparties.Contains(counterparty))
                return IntegratorRequestResult.GetSuccessResult();
            return IntegratorRequestResult.CreateFailedResult("Cannot stop session - counterparty is not configured");
        }

        public IntegratorRequestResult StartMarketDataSession(Counterparty counterparty)
        {
            if (ConnectionObjectsKeepers[(int)counterparty] != null)
            {
                //Attempt to first claim the counterparty
                if (
                    !
                    //HACK: allow to start Hotspot MD session without claiming
                    (counterparty.TradingTargetType == TradingTargetType.Hotspot ||
                      this._integratorInstanceUtils.IsCounterpartySessionClaimed(counterparty))
                    )
                {
                    var result = this._integratorInstanceUtils.TryClaimCounterpartySession(counterparty);
                    if (!result.RequestSucceeded)
                        return result;
                }

                if (Counterparty.OrdersOnlyCounterparties.Contains(counterparty))
                    return IntegratorRequestResult.GetSuccessResult();

                if (ConnectionObjectsKeepers[(int)counterparty] == null ||
                    ConnectionObjectsKeepers[(int)counterparty].MDSession == null)
                {
                    return IntegratorRequestResult.CreateFailedResult("Cannot restart MD session - MD not configured for counterparty");
                }
                else if (ConnectionObjectsKeepers[(int)counterparty].MDSession.State != SessionState.Running)
                {
                    ConnectionObjectsKeepers[(int)counterparty].MDSession.Start();
                    return IntegratorRequestResult.GetSuccessResult();
                }
                else
                {
                    ConnectionObjectsKeepers[(int)counterparty].QuotesLogger.Log(LogLevel.Warn, "Cannot start session that is in running state");
                    return IntegratorRequestResult.CreateFailedResult("Cannot start session that is in stopping state");
                }
            }

            return IntegratorRequestResult.CreateFailedResult("Cannot start session - counterparty is not configured");
        }

        //This method doesn't release claimed session - as it might be just an intermediate stop caused e.g. by broken order
        public IntegratorRequestResult StopOrderFlowSession(Counterparty counterparty, TimeSpan delay)
        {
            if (ConnectionObjectsKeepers[(int)counterparty] != null && ConnectionObjectsKeepers[(int)counterparty].OFSession != null)
            {
                if (ConnectionObjectsKeepers[(int)counterparty].OFSession.State != SessionState.Stopping)
                {
                    if (delay > TimeSpan.Zero)
                        ConnectionObjectsKeepers[(int)counterparty].OFSession.StopAsync(delay);
                    else
                        ConnectionObjectsKeepers[(int)counterparty].OFSession.Stop();
                    
                    return IntegratorRequestResult.GetSuccessResult();
                }
                else
                {
                    ConnectionObjectsKeepers[(int)counterparty].OrdersLogger.Log(LogLevel.Warn, "Cannot stop session that is in stopping state");
                    return IntegratorRequestResult.CreateFailedResult("Cannot stop session that is in stopping state");
                }
            }

            return IntegratorRequestResult.CreateFailedResult("Cannot stop session - counterparty is not configured");
        }

        public IntegratorRequestResult StartOrderFlowSession(Counterparty counterparty)
        {
            if (ConnectionObjectsKeepers[(int)counterparty] != null)
            {
                //Attempt to first claim the counterparty
                if (!this._integratorInstanceUtils.IsCounterpartySessionClaimed(counterparty))
                {
                    var result = this._integratorInstanceUtils.TryClaimCounterpartySession(counterparty);
                    if (!result.RequestSucceeded)
                        return result;
                }

                if (ConnectionObjectsKeepers[(int)counterparty].OFSession.State != SessionState.Running)
                {
                    ConnectionObjectsKeepers[(int)counterparty].OFSession.Start();
                    return IntegratorRequestResult.GetSuccessResult();
                }
                else
                {
                    ConnectionObjectsKeepers[(int)counterparty].OrdersLogger.Log(LogLevel.Warn, "Cannot start session that is in running state");
                    return IntegratorRequestResult.CreateFailedResult("Cannot start session that is in stopping state");
                }
            }

            return IntegratorRequestResult.CreateFailedResult("Cannot start session - counterparty is not configured");
        }

        private IStpFlowSession GetStpFlowSession(STPCounterparty stpCounterparty)
        {
            return this.StpFlowSessions.FirstOrDefault(session => session.StpCounterparty == stpCounterparty);
        }

        public IntegratorRequestResult StartStpSession(STPCounterparty stpCounterparty)
        {
            IStpFlowSession stpSession = this.GetStpFlowSession(stpCounterparty);

            if (stpSession != null)
            {
                stpSession.Start();
                return IntegratorRequestResult.GetSuccessResult();
            }
            else
            {
                return IntegratorRequestResult.CreateFailedResult(string.Format("Cannot start sesion {0} as it is not enabled", stpCounterparty));
            }
        }

        public IntegratorRequestResult StopStpSession(STPCounterparty stpCounterparty)
        {
            IStpFlowSession stpSession = this.GetStpFlowSession(stpCounterparty);

            if (stpSession != null)
            {
                stpSession.Stop();
                return IntegratorRequestResult.GetSuccessResult();
            }
            else
            {
                return IntegratorRequestResult.CreateFailedResult(string.Format("Cannot stop sesion {0} as it is not enabled", stpCounterparty));
            }
        }

        public IntegratorRequestResult RestartStpSession(STPCounterparty stpCounterparty)
        {
            IStpFlowSession stpSession = this.GetStpFlowSession(stpCounterparty);

            if (stpSession != null)
            {
                if (stpSession.State != SessionState.Stopping)
                {
                    stpSession.Stop();
                    System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(200));
                    stpSession.Start();
                    return IntegratorRequestResult.GetSuccessResult();
                }
                else
                {
                    stpSession.Logger.Log(LogLevel.Warn, "Cannot restart STP session that is in stopping state");
                    return IntegratorRequestResult.CreateFailedResult("Cannot restart STP session that is in stopping state");
                }
            }
            else
            {
                return IntegratorRequestResult.CreateFailedResult(string.Format("Cannot restart sesion {0} as it is not enabled", stpCounterparty));
            } 
        }

        public void StartStpSessions()
        {
            foreach (IStpFlowSession stpFlowSession in StpFlowSessions)
            {
                stpFlowSession.Start();
            }
        }

        public void StopStpSessions()
        {
            if(StpFlowSessions == null)
                return;

            foreach (IStpFlowSession stpFlowSession in StpFlowSessions)
            {
                stpFlowSession.Stop();
            }
        }

        public void StartMarketDataSessions()
        {
            foreach (IFIXChannel marketDataSession in
                this.ConnectionObjectsKeepers.Where(
                    o => o != null && o.MDSession != null && o.MDSession.State != SessionState.Running)
                    .Select(o => o.MDSession))
            {
                //Just check the status - no claiming!
                if (this._integratorInstanceUtils.IsCounterpartySessionClaimed(marketDataSession.Counterparty))
                {
                    marketDataSession.Start();
                }
            }
        }

        public void StartOrderFlowSessions()
        {
            foreach (IFIXChannel orderFlowSession in
                this.ConnectionObjectsKeepers.Where(
                    o => o != null && o.OFSession != null && o.OFSession.State != SessionState.Running)
                    .Select(o => o.OFSession))
            {
                //Just check the status - no claiming!
                if (this._integratorInstanceUtils.IsCounterpartySessionClaimed(orderFlowSession.Counterparty))
                {
                    orderFlowSession.Start();
                }
            }
        }

        public void StopMarketDataSessions()
        {
            foreach (IFIXChannel marketDataSession in
                this.ConnectionObjectsKeepers.Where(o => o != null && o.MDSession != null).Select(o => o.MDSession))
            {
                marketDataSession.Stop();
            }
        }

        public void StopOrderFlowSessions()
        {
            foreach (IFIXChannel orderFlowSession in
                this.ConnectionObjectsKeepers.Where(o => o != null && o.OFSession != null).Select(o => o.OFSession))
            {
                orderFlowSession.Stop();
            }
        }

        private readonly ConcurrentDictionary<Counterparty, List<ISubscription>> _subscriptionsBag =
            new ConcurrentDictionary<Counterparty, List<ISubscription>>();
    }

    public static class StpConnectionObjectsKeeper
    {
        private static IEnumerable<IStpFlowSession> CreateStpSessions_Internal(ILogFactory logFactory,
                                                                                     IDealStatistics dealStatistics)
        {
            logFactory.GetLogger(null).Log(LogLevel.Fatal, "Using real STP sessions - this should not happen as Fluent handles STPs");

            var enabledCtpsWithNoStp = SessionsSettings.SubscriptionSettings.EnabledCounterparts.Where(
                ctp =>
                !SessionsSettings.STPBehavior.STPDestinations.SelectMany(dest => dest.ServedCounterparties)
                                 .Contains(ctp)).ToList();
            if (enabledCtpsWithNoStp.Count > 0)
            {
                logFactory.GetLogger(null)
                          .Log(LogLevel.Fatal,
                               "Counterparties [{0}] are enabled in configuration but no STP session is configured to serve them",
                               string.Join(", ", enabledCtpsWithNoStp));
            }

            List<IStpFlowSession> stpSessions = new List<IStpFlowSession>();

            foreach (SessionsSettings.STPSettings.STPDestinationSettings stpDestinationSettings in SessionsSettings.STPBehavior.STPDestinations)
            {
                IFIXSTPChannel stpChannel;
                ILogger logger = logFactory.GetLogger(stpDestinationSettings.Name.ToString(), true);
                IPersistedFixConfigsReader persistedFixConfigsReader = new PersistedFixConfigsReader(stpDestinationSettings.Name.ToString(), logger);

                switch (stpDestinationSettings.Name)
                {
                    case STPCounterparty.CTIPB:
                        stpChannel = new FIXSTPChannel_CTI(persistedFixConfigsReader, logger,
                                                           SessionsSettings.UnexpectedMessagesTreatingBehavior,
                                                           SessionsSettings.FIXSTPChannel_CTIPBBehavior, STPCounterparty.CTIPB);
                        break;
                    case STPCounterparty.Traiana:

                        stpChannel = new FIXSTPChannel_Traiana(persistedFixConfigsReader, logger,
                                                               SessionsSettings.UnexpectedMessagesTreatingBehavior,
                                                               STPCounterparty.Traiana);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                stpSessions.Add(new StpFlowSession(dealStatistics, stpChannel,
                                                   stpDestinationSettings.ServedCounterparties,
                                                   PersistedFixConfigsReader.FluentRedirectionInfo,
                                                   stpDestinationSettings.UnconfirmedTicketTimeout,
                                                   stpDestinationSettings.Name, logger));
            }

            return stpSessions;
        }

        public static IEnumerable<IStpFlowSession> CreateStpSessions(ILogFactory logFactory,
                                                                                     IDealStatistics dealStatistics)
        {
            //return CreateStpSessions_Internal(logFactory, dealStatistics);
            return new List<IStpFlowSession>() {CreateDummyStpFlowSession()};
        }

        public static IEnumerable<IStpFlowSession> CreateStpSessions(IDealStatistics dealStatistics)
        {
            return CreateStpSessions(LogFactory.Instance, dealStatistics);
        }

        public static IStpFlowSession CreateDummyStpFlowSession()
        {
            return new DummyStpFlowSession(LogFactory.Instance.GetLogger("STP_dummy", true));
        }

        //public static IStpFlowSession CreateStpFlowSession(IDealStatistics dealStatistics)
        //{
        //    return CreateStpFlowSession(LogFactory.Instance, dealStatistics);
        //}

        //public static IStpFlowSession CreateStpFlowSession(ILogFactory logFactory,
        //                                                  IDealStatistics dealStatistics)
        //{
        //    ILogger logger = logFactory.GetLogger("CTISTP", true);
        //    IPersistedFixConfigsReader persistedFixConfigsReader = new PersistedFixConfigsReader("CTIPB", logger);
        //    FIXSTPChannel_CTI stpChannel = new FIXSTPChannel_CTI(persistedFixConfigsReader, logger, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXSTPChannel_CTIPBBehavior);
        //    return new StpFlowSession(dealStatistics, stpChannel, logger);
        //}
    }

    public interface IConnectionObjectsKeeper
    {
        Counterparty Counterparty { get; }
        ILogger QuotesLogger { get; }
        ILogger OrdersLogger { get; }
        IFIXChannel MDSession { get; }
        IFIXChannel OFSession { get; }
    }

    public interface ITakerConnectionObjectsKeeper : IConnectionObjectsKeeper
    {
        IMarketDataSession MarketDataSession { get; }
        IOrderFlowSession OrderFlowSession { get; }
    }

    public abstract class ConnectionObjectsKeeperBase : IConnectionObjectsKeeper
    {
        public Counterparty Counterparty { get; private set; }
        public ILogger QuotesLogger { get; private set; }
        public ILogger OrdersLogger { get; private set; }
        public abstract IFIXChannel MDSession { get; }
        public abstract IFIXChannel OFSession { get; }

        protected ConnectionObjectsKeeperBase(Counterparty counterparty, ILogFactory logFactory, bool createQuoSession, bool createOrdSession)
        {
            this.Counterparty = counterparty;
            if (createQuoSession) this.QuotesLogger = logFactory.GetLogger(string.Format("{0}_QUO", counterparty.ToString()), SessionsSettings.MarketDataSessionBehavior.PutErrorsInLessImportantLog, true);
            if (createOrdSession) this.OrdersLogger = logFactory.GetLogger(string.Format("{0}_ORD", counterparty.ToString()), /*SessionsSettings.OrderFlowSessionBehavior.PutErrorsInLessImportantLog*/false);   
        }
    }

    public class MakerConnectionObjectsKeeper : ConnectionObjectsKeeperBase, IConnectionObjectsKeeper
    {
        private MakerConnectionObjectsKeeper(Counterparty counterparty, ILogFactory logFactory,
            IDealStatisticsConsumer dealStatisticsConsumer, IRiskManager riskManager, ISymbolsInfo symbolsInfo,
            IStreamingUnicastInfoForwarder streamingUnicastInfoForwarder, IDelayEvaluator delayEvaluator, bool createQuoSession, bool createOrdSession)
            : base(counterparty, logFactory, createQuoSession, createOrdSession)
        {
            IPersistedFixConfigsReader persistedFixConfigsReaderOf =
                new PersistedFixConfigsReader(string.Format("{0}_ORD", counterparty.ToString()), OrdersLogger);
            IPersistedFixConfigsReader persistedFixConfigsReaderMd =
                new PersistedFixConfigsReader(string.Format("{0}_QUO", counterparty.ToString()), QuotesLogger);

            var channel = SessionFactory.CreateStreamingChannel(counterparty, persistedFixConfigsReaderMd,
                persistedFixConfigsReaderOf, dealStatisticsConsumer, QuotesLogger, OrdersLogger, riskManager,
                streamingUnicastInfoForwarder, new PersistedStreamingStatsProvider(), symbolsInfo, new StreamingSettingsDal(), delayEvaluator);
            this.StreamingChannel = channel;
            this._mdSession = channel.MdChannel;
            this._ofSession = channel.OfChannel;
        }

        private IFIXChannel _mdSession;
        private IFIXChannel _ofSession;

        public override IFIXChannel MDSession { get { return this._mdSession; } }
        public override IFIXChannel OFSession { get { return this._ofSession; } }

        public IStreamingChannel StreamingChannel { get; private set; }

        public static IConnectionObjectsKeeper CreateMakerConnectionObjectsKeeper(Counterparty counterparty,
            IDealStatisticsConsumer dealStatisticsConsumer, IRiskManager riskManager, ISymbolsInfo symbolsInfo, IStreamingUnicastInfoForwarder streamingUnicastInfoForwarder, IDelayEvaluator delayEvaluator)
        {
            return new MakerConnectionObjectsKeeper(counterparty, LogFactory.Instance, dealStatisticsConsumer,
                riskManager, symbolsInfo, streamingUnicastInfoForwarder, delayEvaluator, true, true);
        }
    }

    public class TakerConnectionObjectsKeeper : ConnectionObjectsKeeperBase, ITakerConnectionObjectsKeeper
    {
        public IMarketDataSession MarketDataSession { get; private set; }
        public IOrderFlowSession OrderFlowSession { get; private set; }
        public override IFIXChannel MDSession { get { return this.MarketDataSession; } }
        public override IFIXChannel OFSession { get { return this.OrderFlowSession; } }


        public static TakerConnectionObjectsKeeper CreateConnectionObjectsKeeper(Counterparty counterparty,
                                                                            bool resubscribeOnReconnect,
                                                                            IRiskManager riskManager,
                                                                            IDealStatisticsConsumer
                                                                                dealStatisticsConsumer, IDelayEvaluator delayEvaluator, bool createQuoSession, bool createOrdSession)
        {
            return new TakerConnectionObjectsKeeper(counterparty, resubscribeOnReconnect, riskManager, dealStatisticsConsumer, LogFactory.Instance, delayEvaluator, createQuoSession, createOrdSession);
        }

        public static TakerConnectionObjectsKeeper CreateConnectionObjectsKeeper(Counterparty counterparty,
                                                                            bool resubscribeOnReconnect,
                                                                            IRiskManager riskManager,
                                                                            IDealStatisticsConsumer
                                                                                dealStatisticsConsumer, ILogFactory logFactory, IDelayEvaluator delayEvaluator)
        {
            return new TakerConnectionObjectsKeeper(counterparty, resubscribeOnReconnect, riskManager, dealStatisticsConsumer, logFactory, delayEvaluator, true, true);
        }

        private TakerConnectionObjectsKeeper(Counterparty counterparty, bool resubscribeOnReconnect, IRiskManager riskManager, IDealStatisticsConsumer dealStatisticsConsumer, ILogFactory logFactory, IDelayEvaluator delayEvaluator, bool createQuoSession, bool createOrdSession)
            :base(counterparty, logFactory, createQuoSession, createOrdSession)
        {
            if (createOrdSession)
            {
                IPersistedFixConfigsReader persistedFixConfigsReaderOf =
                    new PersistedFixConfigsReader(string.Format("{0}_ORD", counterparty.ToString()), OrdersLogger);

                this.OrderFlowSession = SessionFactory.CreateOrderFlowSession(counterparty, persistedFixConfigsReaderOf,
                    riskManager, dealStatisticsConsumer, new StreamingSettingsDal(), delayEvaluator, OrdersLogger);
            }

            if (createQuoSession)
            {

                if (Counterparty.OrdersOnlyCounterparties.Contains(counterparty))
                { /* NOTHING - we do not want MD session for LX1, LGC, H4M */ }
                else if (counterparty.TradingTargetType == TradingTargetType.Hotspot)
                {
                    ItchSession itchSession = new ItchSession();
                    itchSession.Configure(QuotesLogger, (HotspotCounterparty)counterparty);
                    this.MarketDataSession = new HotspotMarketDataSession(QuotesLogger, itchSession,
                                                                          (HotspotCounterparty)counterparty, delayEvaluator);
                }
                else
                {
                    IPersistedFixConfigsReader persistedFixConfigsReaderMd =
                        new PersistedFixConfigsReader(string.Format("{0}_QUO", counterparty.ToString()), QuotesLogger);

                    this.MarketDataSession = SessionFactory.CreateMarketDataSession(counterparty, resubscribeOnReconnect,
                        persistedFixConfigsReaderMd, delayEvaluator, new StreamingSettingsDal(), QuotesLogger);
                }
            }

            SessionFactory.BindChannelsIfNeeded(this.MarketDataSession, this.OrderFlowSession);
        }

        public static TakerConnectionObjectsKeeper CreateHotspotObjectsKeeper_TestingOnly(HotspotCounterparty counterparty,
                                                                            IRiskManager riskManager,
                                                                            IDealStatisticsConsumer
                                                                                dealStatisticsConsumer, ILogFactory logFactory, IDelayEvaluatorMulti delayEvaluatorMulti, IDelayEvaluator delayEvaluator)
        {
            return new TakerConnectionObjectsKeeper(counterparty, riskManager, dealStatisticsConsumer, logFactory, delayEvaluatorMulti, delayEvaluator);
        }

        private TakerConnectionObjectsKeeper(HotspotCounterparty counterparty, IRiskManager riskManager,
            IDealStatisticsConsumer dealStatisticsConsumer, ILogFactory logFactory,
            IDelayEvaluatorMulti delayEvaluatorMulti, IDelayEvaluator delayEvaluator)
            : base(counterparty, logFactory, true, true)
        {

            IPersistedFixConfigsReader persistedFixConfigsReaderOf =
                new PersistedFixConfigsReader(string.Format("{0}_ORD", counterparty.ToString()), OrdersLogger);

            this.OrderFlowSession = SessionFactory.CreateOrderFlowSession(counterparty, persistedFixConfigsReaderOf,
                riskManager, dealStatisticsConsumer, new StreamingSettingsDal(), delayEvaluator, OrdersLogger);



            if (((Counterparty)counterparty).TradingTargetType == TradingTargetType.Hotspot &&
                ToBconverter.SupportedCounterparties.Contains(counterparty))
            {
                ToBReceiver toBReceiver = new ToBReceiver(QuotesLogger);
                HotspotPricesAdapter hpa = new HotspotPricesAdapter(toBReceiver, delayEvaluatorMulti, HotspotCounterparty.HTF);
                this.MarketDataSession = hpa;
            }
            else
            {
                throw new NotSupportedException();
            }
        }
    }
}
