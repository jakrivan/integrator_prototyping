﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.LowLatencyUtils;
using Kreslik.Integrator.QuickItchN;

namespace Kreslik.Integrator.SessionManagement
{
    public interface IHotspotMarketDataProvider : IVenueDataProvider
    {
        event Action<PriceObjectInternal> EachNewQuote;
        event Action<HotspotQuoteUpdateInfo> EachQuoteUpdate;
        event Action<HotspotQuoteCancelInfo> EachQuoteCancel;
        event Action CancelAllQuotes;
    }


    public class HotspotMarketDataSession : QuickItchN.MessageCrackerBase<HotspotMarketDataSession>, IHotspotMarketDataProvider, IMarketDataSession, IBookTopProvider<PriceObjectInternal>
    {
        public event Action<PriceObjectInternal> EachNewQuote;
        public event Action<HotspotQuoteUpdateInfo> EachQuoteUpdate;
        public event Action<HotspotQuoteCancelInfo> EachQuoteCancel;
        private NewVenueDealHandler[] _newVenueDealHanlers = new NewVenueDealHandler[Symbol.ValuesCount]; 
        public event Action CancelAllQuotes;





        public IEnumerable<ISubscription> Subscriptions
        {
            get { return _subscriptions; }
        }

        public event Action<ISubscription> OnSubscriptionChanged;

        //This is for changes on ToB only
        public event HandlePriceObject OnNewPrice;
        public event Action<PriceObjectInternal> OnNewNonExecutablePrice;
        public event HandlePriceObject OnIgnoredPrice;

        private void InvokeOnNewQuote(IBookTop<PriceObjectInternal> sender, PriceObjectInternal priceObject, DateTime unused)
        {
            bool heldWithoutAcquireCall = false;
            if (OnNewPrice != null && priceObject != null)
            {
                if (!this._delayEvaluator.IsReceivedObjectDelayed(priceObject))
                    OnNewPrice(priceObject, ref heldWithoutAcquireCall);
                else if (OnIgnoredPrice != null)
                    OnIgnoredPrice(priceObject, ref heldWithoutAcquireCall);
            }
        }

        public event Action<Symbol, Counterparty> OnCancelLastQuote;
        public event Action<Symbol, Counterparty, PriceSide> OnCancelLastPrice;


        public event Action OnStopInitiated
        {
            add { ItchSession.LoggedOut += value; }
            remove { ItchSession.LoggedOut -= value; }
        }

        public event Action OnStarted
        {
            add { ItchSession.LoggedIn += value; }
            remove { ItchSession.LoggedIn -= value; }
        }

        public bool IsReadyAndOpen { get { return ItchSession.ItchSessionState == ItchSessionState.Running; } }

        public event Action OnStopped
        {
            add { ItchSession.LoggedOut += value; }
            remove { ItchSession.LoggedOut -= value; }
        }

        public SessionState State
        {
            get
            {
                switch (ItchSession.ItchSessionState)
                {
                    case ItchSessionState.Unconfigured:
                        return SessionState.Idle;
                    case ItchSessionState.Created:
                        return SessionState.Idle;
                    case ItchSessionState.Starting:
                        return SessionState.Starting;
                    case ItchSessionState.Running:
                        return SessionState.Running;
                    case ItchSessionState.Stopping:
                        return SessionState.Stopping;
                    case ItchSessionState.Stopped:
                        return SessionState.Idle;
                    default:
                        return SessionState.Idle;
                }
            }
        }

        public bool Inactivated { get { return ItchSession.Inactivated; } }

        public Counterparty Counterparty
        {
            get { return HotspotCounterparty; }
        }



        public IBookTopExtended<PriceObjectInternal, Counterparty> GetBidPriceBook(Symbol symbol)
        {
            return _orderBooks[(int) PriceSide.Bid][(int) symbol];
        }

        public IBookTopExtended<PriceObjectInternal, Counterparty> GetAskPriceBook(Symbol symbol)
        {
            return _orderBooks[(int)PriceSide.Ask][(int)symbol];
        }

        public TradingTargetType TradingTargetType
        {
            get { return TradingTargetType.Hotspot; }
        }

        public PriceObjectInternal MaxNodeInternal
        {
            get
            {
                throw new NotImplementedException("Whole Hotspot session cannot have one single max value");
            }
        }

        public PriceObjectInternal MaxNode { get { throw new NotImplementedException("Whole Hotspot session cannot have one single MaxNode"); } }

        public DateTime MaxNodeUpdatedUtc
        {
            get
            {
                throw new NotImplementedException("Whole Hotspot session cannot have one single max value");
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> NewNodeArrived
        {
            add
            {
                throw new NotImplementedException();
            }

            //Not throwing as this way we can 'blindly' unsubscribe in ClientToIntegratorConnectorHelper
            remove { }
        }

        public event BookPriceUpdate<PriceObjectInternal> ExistingNodeInvalidated
        {
            add
            {
                throw new NotImplementedException();
            }

            //Not throwing as this way we can 'blindly' unsubscribe in ClientToIntegratorConnectorHelper
            remove { }
        }

        public event BookMultiplePricesUpdate<PriceObjectInternal, Counterparty> NodesRemoved
        {
            add
            {
                throw new NotImplementedException();
            }

            //Not throwing as this way we can 'blindly' unsubscribe in ClientToIntegratorConnectorHelper
            remove { }
        }

        public event BookPriceUpdate<PriceObjectInternal> BookTopImproved
        {
            add
            {
                foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Bid])
                {
                    orderBook.BookTopImproved += value;
                }

                foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Ask])
                {
                    orderBook.BookTopImproved += value;
                }
            }

            remove
            {
                foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Bid])
                {
                    orderBook.BookTopImproved -= value;
                }

                foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Ask])
                {
                    orderBook.BookTopImproved -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> BookTopReplaced
        {
            add
            {
                foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Bid])
                {
                    orderBook.BookTopReplaced += value;
                }

                foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Ask])
                {
                    orderBook.BookTopReplaced += value;
                }
            }

            remove
            {
                foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Bid])
                {
                    orderBook.BookTopReplaced -= value;
                }

                foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Ask])
                {
                    orderBook.BookTopReplaced -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> BookTopDeterioratedInternal
        {
            add
            {
                foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Bid])
                {
                    orderBook.BookTopDeterioratedInternal += value;
                }

                foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Ask])
                {
                    orderBook.BookTopDeterioratedInternal += value;
                }
            }

            remove
            {
                foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Bid])
                {
                    orderBook.BookTopDeterioratedInternal -= value;
                }

                foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Ask])
                {
                    orderBook.BookTopDeterioratedInternal -= value;
                }
            }
        }

        public event Action<IEnumerable<PriceObjectInternal>> NodesPurgedAway
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        private void InvokeEachNewQuote(PriceObjectInternal priceObject)
        {
            if (EachNewQuote != null)
            {
                EachNewQuote(priceObject);
            }
        }

        private void InvokeQuoteUpdate(HotspotQuoteUpdateInfo quoteUpdateInfo)
        {
            if (EachQuoteUpdate != null)
            {
                EachQuoteUpdate(quoteUpdateInfo);
            }
        }

        private void InvokeQuoteCancel(HotspotQuoteCancelInfo quoteCancelInfo)
        {
            if (EachQuoteCancel != null)
            {
                EachQuoteCancel(quoteCancelInfo);
            }
        }

        private void InvokeNewVenueDeal(VenueDealObject venueDealObject)
        {
            if (_newVenueDealHanlers != null && _newVenueDealHanlers[(int) venueDealObject.Symbol] != null)
            {
                _newVenueDealHanlers[(int)venueDealObject.Symbol](venueDealObject);
            }
        }

        private void InvokeCancelAllQuotes()
        {
            if (CancelAllQuotes != null)
            {
                CancelAllQuotes();
            }

            if (OnCancelLastQuote != null)
            {
                foreach (ISubscription subscription in Subscriptions)
                {
                    OnCancelLastQuote(subscription.SubscriptionRequestInfo.Symbol, this.HotspotCounterparty);
                }
            }
        }

        private void ClearAllBooks()
        {
            foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Bid])
            {
                orderBook.Clear();
            }

            foreach (IOrderBook<PriceObjectInternal> orderBook in _orderBooks[(int)PriceSide.Ask])
            {
                orderBook.Clear();
            }
        }

        private void ClearPurgedQuotes(Symbol symbol, IEnumerable<PriceObjectInternal> priceObjects)
        {
            this._logger.Log(LogLevel.Fatal, "Purging quotes from {0} order book, as it was already full", symbol);

            foreach (PriceObjectInternal priceObject in priceObjects)
            {
                if (!this._activeItchOrders[(int)symbol].Remove(priceObject.CounterpartyIdentityAsAsciiString))
                {
                    this._logger.Log(LogLevel.Warn,
                                     "Attempt to remove price [{0}] with counterpart identity {1}, but it is not known to MD layer",
                                     priceObject.IntegratorIdentity, priceObject.CounterpartyIdentity);
                }
                priceObject.Release();
            }
        }


        private Dictionary<string, PriceObjectInternal>[] _activeItchOrders;
        private IOrderBook<PriceObjectInternal>[][] _orderBooks = new IOrderBook<PriceObjectInternal>[2][];
        private ILogger _logger;
        private IDelayEvaluator _delayEvaluator;
        public HotspotCounterparty HotspotCounterparty { get; private set; }

        private const int MAX_ORDER_BOOK_SIZE = 10000;

        public IItchSession ItchSession { get; private set; }

        public HotspotMarketDataSession(ILogger logger, IItchSession itchSession, HotspotCounterparty counterparty, IDelayEvaluator delayEvaluator)
        {
            this._logger = logger;
            this.ItchSession = itchSession;
            this.HotspotCounterparty = counterparty;
            this._delayEvaluator = delayEvaluator;

            _orderBooks[0] = new IOrderBook<PriceObjectInternal>[Symbol.ValuesCount];
            _orderBooks[1] = new IOrderBook<PriceObjectInternal>[Symbol.ValuesCount];

            _activeItchOrders = new Dictionary<string, PriceObjectInternal>[Symbol.ValuesCount];
            for (int i = 0; i < Symbol.ValuesCount; i++)
            {
                Symbol symbol = (Symbol)i;
                _activeItchOrders[i] = new Dictionary<string, PriceObjectInternal>();

                _orderBooks[(int)PriceSide.Bid][i] =
                    new OrderBook<PriceObjectInternal>(MAX_ORDER_BOOK_SIZE, BidPriceComparer.Default,
                                               BidTopPriceComparerInternal.Default,
                                               PriceEqualityComparerInternal.Default,
                                               PriceObjectInternal.GetNullBidInternal(symbol, counterparty), TradingTargetType.Hotspot, logger);
                _orderBooks[(int)PriceSide.Bid][i].NodesPurgedAway +=
                    objects => this.ClearPurgedQuotes(symbol, objects);
                //_orderBooks[(int)PriceSide.Bid][i].TopChanging =
                //    price =>
                //    (price as WritablePriceObject).WritableLastShownOnTopOfBookTimeStampUtc =
                //    HighResolutionDateTime.UtcNow;

                _orderBooks[(int)PriceSide.Ask][i] =
                    new OrderBook<PriceObjectInternal>(MAX_ORDER_BOOK_SIZE, AskPriceComparer.Default,
                                               AskTopPriceComparerInternal.Default,
                                               PriceEqualityComparerInternal.Default,
                                               PriceObjectInternal.GetNullAskInternal(symbol, counterparty), TradingTargetType.Hotspot, logger);
                _orderBooks[(int)PriceSide.Ask][i].NodesPurgedAway +=
                    objects => this.ClearPurgedQuotes(symbol, objects);
                //_orderBooks[(int)PriceSide.Ask][i].TopChanging =
                //    price =>
                //    (price as WritablePriceObject).WritableLastShownOnTopOfBookTimeStampUtc =
                //    HighResolutionDateTime.UtcNow;
            }

            itchSession.LoggedOut += () =>
            {
                ClearAllBooks();

                for (int i = 0; i < Symbol.ValuesCount; i++)
                {
                    foreach (PriceObjectInternal priceObject in _activeItchOrders[i].Values)
                    {
                        priceObject.Release();
                    }
                    _activeItchOrders[i].Clear();
                }

                InvokeCancelAllQuotes();

                //clear prefetched prices
                for (int idx = 0; idx < _priceObjectsBatch.Length; idx++)
                {
                    if (_priceObjectsBatch[idx] != null)
                    {
                        _priceObjectsBatch[idx].Release();
                        _priceObjectsBatch[idx] = null;
                    }
                }

                _priceObjectsIdx = 0;
            };

            itchSession.LoggedIn += () =>
            {
                if (_subscribeAll)
                {
                    itchSession.SendMessage(new MarketDataSubscribeItchMessage());
                    itchSession.SendMessage(new TickerSubscribeItchMessage());
                    itchSession.SendMessage(new MarketSnapshotRequestItchMessage());
                }
                else
                {
                    foreach (ISubscription subscription in _subscriptions)
                    {
                        itchSession.SendMessage(new MarketDataSubscribeItchMessage(subscription.SubscriptionRequestInfo.Symbol));
                        itchSession.SendMessage(new TickerSubscribeItchMessage(subscription.SubscriptionRequestInfo.Symbol));
                    }

                    foreach (ISubscription subscription in _subscriptions)
                    {
                        itchSession.SendMessage(new MarketSnapshotRequestItchMessage(subscription.SubscriptionRequestInfo.Symbol));
                    }
                }
            };

            //here is the place where we handle exceptions in all events
            itchSession.NewIncomingItchMessage += message =>
            {
                try
                {
                    this.Crack(message);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e,
                                              "Experienced unexpected exception during message cracking: {0}",
                                              message);
                }
            };

            this.BookTopImproved += InvokeOnNewQuote;
            this.BookTopDeterioratedInternal += InvokeOnNewQuote;
            this.BookTopReplaced += InvokeOnNewQuote;
        }

        public const int PREFETCH_PRICES_BATCH_SIZE = 100;
        private PriceObjectInternal[] _priceObjectsBatch = new PriceObjectInternal[PREFETCH_PRICES_BATCH_SIZE];
        private int _priceObjectsIdx = 0;
        private PriceObjectPool _priceObjectPool = PriceObjectPool.Instance;

        private PriceObjectInternal GetNextPriceObject()
        {
            PriceObjectInternal priceObject;
            if (_priceObjectsIdx >= _priceObjectsBatch.Length || _priceObjectsBatch[_priceObjectsIdx] == null)
            {
                _priceObjectPool.TryFetchBatchOfPriceObjects(_priceObjectsBatch);
                _priceObjectsIdx = 0;
            }

            priceObject = _priceObjectsBatch[_priceObjectsIdx];
            _priceObjectsBatch[_priceObjectsIdx] = null;
            _priceObjectsIdx++;

            return priceObject;
        }

        public PriceObjectInternal CreateNewHotspotOrderPriceObject(decimal price, decimal sizeBaseAbsInitial, decimal? sizeBaseAbsFillMinimum,
                                   decimal? sizeBaseAbsFillGranularity, PriceSide side, Symbol symbol,
                                   Counterparty counterparty,
                                   MarketDataRecordType marketDataRecordType, TradeSide? tradeSide,
                                   string counterpartyIdentity, DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            PriceObjectInternal priceObject = GetNextPriceObject();

            return PriceObjectPool.OverridePrefetchedHotspotPriceObject(priceObject, price, sizeBaseAbsInitial, sizeBaseAbsFillMinimum,
                sizeBaseAbsFillGranularity, side, symbol, counterparty, marketDataRecordType, tradeSide,
                counterpartyIdentity, integratorReceivedTime, counterpartySentTime);
        }

        List<HotspotSubscription> _subscriptions = new List<HotspotSubscription>();
        private bool _subscribeAll;

        public ISubscription Subscribe(Kreslik.Integrator.FIXMessaging.SubscriptionRequestInfo subscriptionRequestInfo)
        {
            HotspotSubscription subscription = null;

            if (_subscriptions.All(info => info.SubscriptionRequestInfo.Symbol != subscriptionRequestInfo.Symbol))
            {
                subscription = new HotspotSubscription(subscriptionRequestInfo);
                _subscriptions.Add(subscription);

                if (this.ItchSession.ItchSessionState == ItchSessionState.Running)
                {
                    this.ItchSession.SendMessage(new MarketDataSubscribeItchMessage(subscriptionRequestInfo.Symbol));
                    this.ItchSession.SendMessage(new TickerSubscribeItchMessage(subscriptionRequestInfo.Symbol));
                    this.ItchSession.SendMessage(new MarketSnapshotRequestItchMessage(subscriptionRequestInfo.Symbol));
                }
            }

            return subscription;
        }

        public void SubscribeAll()
        {
            _subscribeAll = true;

            if (this.ItchSession.ItchSessionState == ItchSessionState.Running)
            {
                this.ItchSession.SendMessage(new MarketDataSubscribeItchMessage());
                this.ItchSession.SendMessage(new TickerSubscribeItchMessage());
                this.ItchSession.SendMessage(new MarketSnapshotRequestItchMessage());
            }
        }

        public bool Unsubscribe(ISubscription subscriptionInfo)
        {
            _subscriptions.RemoveAll(info => info.SubscriptionRequestInfo.Symbol == subscriptionInfo.SubscriptionRequestInfo.Symbol);
            _subscribeAll = false;

            if (this.ItchSession.ItchSessionState == ItchSessionState.Running)
            {
                this.ItchSession.SendMessage(new MarketDataUnsubscribeItchMessage(subscriptionInfo.SubscriptionRequestInfo.Symbol));
                this.ItchSession.SendMessage(new TickerUnsubscribeItchMessage(subscriptionInfo.SubscriptionRequestInfo.Symbol));
            }

            return true;
        }

        public void Start()
        {
            this.ItchSession.Start();
        }

        public void Stop()
        {
            this.ItchSession.Stop();
        }

        public void StopAsync(TimeSpan delay)
        {
            TaskEx.ExecAfterDelayWithErrorHandling(delay, this.Stop);
        }

        public void Restart()
        {
            this.ItchSession.Restart();
        }

        public void OnMessage(NewOrderItchMessage msg)
        {
            OnNewItchOrder(msg.ItchOrder, msg.IntegratorReceivedTimeUtc, msg.CounterpartySentTime);
        }

        public DateTime LastSignalSentUtc { get; private set; }

        private void OnNewItchOrder(ItchOrder itchOrder, DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            if (_activeItchOrders[(int)itchOrder.Symbol].ContainsKey(itchOrder.CounterpartyOrderId))
            {
                _logger.Log(LogLevel.Error, "Received ItchOrder, but order [{0}] is already registered, {1}", itchOrder.CounterpartyOrderId, itchOrder);
                return;
            }

            PriceObjectInternal hp = this.CreateNewHotspotOrderPriceObject(itchOrder.Price, itchOrder.Size, itchOrder.Minqty,
                                               itchOrder.Lotsize,
                                               itchOrder.Side == DealDirection.Buy ? PriceSide.Bid : PriceSide.Ask,
                                               itchOrder.Symbol,
                                               this.HotspotCounterparty, MarketDataRecordType.VenueNewOrder, null,
                                               itchOrder.CounterpartyOrderId, integratorReceivedTime, counterpartySentTime);

            _activeItchOrders[(int)itchOrder.Symbol].Add(itchOrder.CounterpartyOrderId, hp);
            _orderBooks[(int)itchOrder.Side][(int)itchOrder.Symbol].AddNew(hp);

            InvokeEachNewQuote(hp);
        }

        public void OnMessage(CancelOrderItchMessage msg)
        {
            PriceObjectInternal hp;
            if (!_activeItchOrders[(int)msg.Symbol].TryGetValue(msg.CounterpartyOrderId, out hp))
            {
                _logger.Log(LogLevel.Error, "Received CancelOrder message, but order [{0}] is not registered, {1}", msg.CounterpartyOrderId, msg);
                return;
            }


            HotspotQuoteCancelInfo hqCancelInfo = new HotspotQuoteCancelInfo(hp, msg.CounterpartySentTime, msg.IntegratorReceivedTimeUtc);
            this.LastSignalSentUtc = msg.CounterpartySentTime;

            _activeItchOrders[(int)msg.Symbol].Remove(msg.CounterpartyOrderId);
            _orderBooks[(int)hp.Side][(int)hp.Symbol].RemoveIdentical(hp);

            InvokeQuoteCancel(hqCancelInfo);
            hp.Release();
        }

        public void OnMessage(ModifyOrderItchMessage msg)
        {
            PriceObjectInternal hp;
            if (!_activeItchOrders[(int)msg.Symbol].TryGetValue(msg.CounterpartyOrderId, out hp))
            {
                _logger.Log(LogLevel.Error, "Received CancelOrder message, but order [{0}] is not registered, {1}", msg.CounterpartyOrderId, msg);
                return;
            }

            HotspotQuoteUpdateInfo hgUpdateInfo = new HotspotQuoteUpdateInfo(hp, msg.Size, msg.Minqty, msg.Lotsize,
                                                      msg.CounterpartySentTime, msg.IntegratorReceivedTimeUtc);
            this.LastSignalSentUtc = msg.CounterpartySentTime;
            _orderBooks[(int)hp.Side][(int)hp.Symbol].ResortIdentical(hp);

            InvokeQuoteUpdate(hgUpdateInfo);
        }

        public void OnMessage(MarketSnapshotItchMessage msg)
        {
            this.LastSignalSentUtc = msg.CounterpartySentTime;
            foreach (ItchOrder itchOrder in msg.ItchOrders)
            {
                this.OnNewItchOrder(itchOrder, msg.IntegratorReceivedTimeUtc, msg.CounterpartySentTime);
            }
        }

        public void OnMessage(TickerItchMessage msg)
        {
            PriceObjectInternal hp = this.CreateNewHotspotOrderPriceObject(msg.Price, 0m, null,
                                               null,
                                               msg.AggressorSide == TradeSide.BuyPaid ? PriceSide.Ask : PriceSide.Bid,
                                               msg.Symbol,
                                               this.HotspotCounterparty, MarketDataRecordType.VenueTradeData, msg.AggressorSide, 
                                               string.Empty, msg.IntegratorReceivedTimeUtc,
                //this is sending time, but not the time of the tread (DC compensates this by nullifying)
                msg.CounterpartySentTime);

            InvokeEachNewQuote(hp);

            hp.Release();

            VenueDealObject vd = new VenueDealObject(msg.Symbol, this.Counterparty, msg.AggressorSide, msg.Price,
                                                     msg.CounterpartySuppliedTransactionTime, msg.CounterpartySentTime,
                                                     msg.IntegratorReceivedTimeUtc);

            InvokeNewVenueDeal(vd);
        }

        private class HotspotSubscription : ISubscription
        {
            public HotspotSubscription(SubscriptionRequestInfo subscriptionRequestInfo)
            {
                this.SubscriptionRequestInfo = subscriptionRequestInfo;
                this.Identity = "HotspotSubscription_" + subscriptionRequestInfo.Symbol;
            }

            public string Identity { get; private set; }
            public SubscriptionRequestInfo SubscriptionRequestInfo { get; private set; }

            public SubscriptionStatus SubscriptionStatus
            {
                get { return SubscriptionStatus.Unknown; }
            }
        }

        public void AddNewVenueDealHandler(Symbol symbol, NewVenueDealHandler newVenueDealHandler)
        {
            //first subtract - to make sure that we don't have double subscriptions
            this._newVenueDealHanlers[(int) symbol] -= newVenueDealHandler;
            this._newVenueDealHanlers[(int)symbol] += newVenueDealHandler;
        }

        public void RemoveNewVenueDealHandler(Symbol symbol, NewVenueDealHandler newVenueDealHandler)
        {
            this._newVenueDealHanlers[(int)symbol] -= newVenueDealHandler;
        }

        public void RemoveAllOccurencesOfNewVenueDealHandler(NewVenueDealHandler newVenueDealHandler)
        {
            for (int idx = 0; idx < Symbol.ValuesCount; idx++)
            {
                this._newVenueDealHanlers[idx] -= newVenueDealHandler;
            }
        }
    }


}
