﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.IntegratorWatchDogSvc.Properties;

namespace Kreslik.Integrator.IntegratorWatchDogSvc
{
    public class WatchdogSettings: IWatchdogSettings
    {
        public List<IntegratorServiceDiagnosticsEndpoint> IntegratorServiceDiagnosticsEndpoints { get; set; }
        
        [System.Xml.Serialization.XmlIgnore]
        public TimeSpan IntegratorNotRunnigSendFirstEmailAfter { get; set; }

        [System.Xml.Serialization.XmlElement("IntegratorNotRunnigSendFirstEmailAfter_Minutes")]
        public int IntegratorNotRunnigSendFirstEmailAfterXml
        {
            get { return (int)IntegratorNotRunnigSendFirstEmailAfter.TotalMinutes; }
            set { IntegratorNotRunnigSendFirstEmailAfter = TimeSpan.FromMinutes(value); }
        }


        [System.Xml.Serialization.XmlIgnore]
        public TimeSpan IntegratorNotRunnigResendEmailAfter { get; set; }

        [System.Xml.Serialization.XmlElement("IntegratorNotRunnigResendEmailAfter_Hours")]
        public int IntegratorNotRunnigResendEmailAfterXml
        {
            get { return (int)IntegratorNotRunnigResendEmailAfter.TotalHours; }
            set { IntegratorNotRunnigResendEmailAfter = TimeSpan.FromHours(value); }
        }

        [System.Xml.Serialization.XmlIgnore]
        public List<string> EmailRecipientsList { get; set; }

        [System.Xml.Serialization.XmlElement("EmailRecipients")]
        public string EmailRecipientsXml
        {
            get { return string.Join(";", EmailRecipientsList); }
            set { EmailRecipientsList = new List<string>(value.Split(new []{";"}, StringSplitOptions.RemoveEmptyEntries)); }
        }


        [System.Xml.Serialization.XmlIgnore]
        public static WatchdogSettings WatchdogBehavior
        {
            get
            {
                if (_watchdogBehavior == null)
                {
                    _watchdogBehavior = SettingsReader.DeserializeConfiguration<WatchdogSettings>(
                        @"Kreslik.Integrator.IntegratorWatchDogSvc.exe.xml",
                        Resources.Kreslik_Integrator_IntegratorWatchDogSvc_exe
                        );
                }

                return _watchdogBehavior;
            }
        }

        private static WatchdogSettings _watchdogBehavior;
    }
}
