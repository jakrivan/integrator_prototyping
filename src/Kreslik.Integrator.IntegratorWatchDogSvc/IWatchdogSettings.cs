﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.IntegratorWatchDogSvc
{
    public class IntegratorServiceDiagnosticsEndpoint
    {
        public string Endpoint { get; set; }
        public int Port { get; set; }
    }

    public interface IWatchdogSettings
    {
        List<IntegratorServiceDiagnosticsEndpoint> IntegratorServiceDiagnosticsEndpoints { get; }
        TimeSpan IntegratorNotRunnigSendFirstEmailAfter { get; }
        TimeSpan IntegratorNotRunnigResendEmailAfter { get; }
        List<string> EmailRecipientsList { get; }
    }
}
