﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.IntegratorWatchDogSvc
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                bool unknownArguments = true;
                if (args.Length == 1)
                {
                    if (args[0].Equals("/install", StringComparison.OrdinalIgnoreCase))
                    {
                        Install(true);
                        unknownArguments = false;
                    }
                    else if (args[0].Equals("/uninstall", StringComparison.OrdinalIgnoreCase))
                    {
                        Install(false);
                        unknownArguments = false;
                    }
                }

                if (unknownArguments)
                {
                    Console.WriteLine("Unknown argument. Expecting none (to run as a service) or '/install', '/uninstall'");
                }

                Console.WriteLine("Will exit after keypress");
                Console.ReadKey();
                return;
            }

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new IntegratorWatchDogSvc() 
            };
            ServiceBase.Run(ServicesToRun);
        }

        static void Install(bool install)
        {
            ILogger logger = LogFactory.Instance.GetLogger("SvcInstaller");
            Console.WriteLine("Starting {0} action. Details will be in log files", install ? "Install" : "Uninstall");
            logger.Log(LogLevel.Info, "Starting {0} action. Details will be in log files", install ? "Install" : "Uninstall");

            Installer installer = new Installer();
            Console.WriteLine("Starting InstallUtil...");
            logger.Log(LogLevel.Info, "Starting InstallUtil...");
            string output = installer.StartInstallUtil(install);
            logger.Log(LogLevel.Info, output);

            if (install)
            {
                Console.WriteLine("Starting IntegratorWatchdogSvc service and waiting for it to enter running state...");
                logger.Log(LogLevel.Info, "Starting IntegratorWatchdogSvc service and waiting for it to enter running state...");

                try
                {
                    ServiceController service = new ServiceController("IntegratorWatchdogSvc");
                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(10));
                }
                catch (Exception e)
                {
                    logger.LogException(LogLevel.Fatal, "Starting of service failed", e);
                    Console.WriteLine("Starting of service failed. " + e.ToString());
                }
            }

            Console.WriteLine("DONE");
            logger.Log(LogLevel.Info, "DONE");
        }

        private class Installer
        {
            volatile bool _processExited = false;

            public string StartInstallUtil(bool install)
            {
                string netDirectory = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory();
                string ngenPath = System.IO.Path.Combine(netDirectory, "InstallUtil.exe");
                //Boolean exists = System.IO.File.Exists(ngenPath);

                string executingAssembly = System.Reflection.Assembly.GetExecutingAssembly().Location;

                Process process = new Process();
                process.StartInfo.FileName = ngenPath;
                process.StartInfo.Arguments = (install ? "/i \"" : "/u \"") + executingAssembly + "\"";

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.CreateNoWindow = true;

                process.Start();

                StringBuilder sb = new StringBuilder();

                Task writeTask = Task.Factory.StartNew(() =>
                    {
                        while (!_processExited)
                        {
                            char c = (char) process.StandardOutput.Read();
                            Console.Write(c);
                            sb.Append(c);
                            System.Threading.Thread.Sleep(1);
                        }

                        string s = process.StandardOutput.ReadToEnd();
                        Console.Write(s);
                        sb.Append(s);
                    });

                process.WaitForExit();
                _processExited = true;

                writeTask.Wait();
                return sb.ToString();
            }
        }
    }
}
