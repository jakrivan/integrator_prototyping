﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.MessageBus;

namespace Kreslik.Integrator.IntegratorWatchDogSvc
{
    public class IntegratorWatchDogSvc : ServiceBase
    {
        private readonly TimeSpan WATCHDOG_INTERVAL = TimeSpan.FromMinutes(1);
        private ILogger _logger;
        private IWatchdogSettings _watchdogSettings;
        private IntegratorDiagnosticsClient _diagnosticsClient;
        private bool _connected = false;
        private DateTime _lastConnectedTime = DateTime.UtcNow;
        private DateTime _lastEmailSentTime;
        private System.Threading.Timer _watchdogTimer;

        public IntegratorWatchDogSvc()
        {
            this.ServiceName = "IntegratorWatchdogSvc";
            this.EventLog.Log = "Application";

            // These Flags set whether or not to handle that specific
            //  type of event. Set to true if you need it, false otherwise.
            this.CanHandlePowerEvent = true;
            this.CanPauseAndContinue = true;
            this.CanShutdown = true;
            this.CanStop = true;

            _watchdogSettings = WatchdogSettings.WatchdogBehavior;
            _logger = LogFactory.Instance.GetLogger("IntegratorWatchDogSvc");
            _diagnosticsClient = new IntegratorDiagnosticsClient(_logger, null);
            ResetLastEmailSentTime();
            _watchdogTimer = new Timer(CheckIsRemoteEndpointAlive, null, WATCHDOG_INTERVAL, WATCHDOG_INTERVAL);
        }

        private void ResetLastEmailSentTime()
        {
            _lastEmailSentTime = DateTime.UtcNow -
                                 (_watchdogSettings.IntegratorNotRunnigResendEmailAfter -
                                  _watchdogSettings.IntegratorNotRunnigSendFirstEmailAfter);
        }

        private void CheckIsRemoteEndpointAlive(object unused)
        {
            if (_connected)
            {
                try
                {
                    _diagnosticsClient.Ping();
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Warn, "Couldn't ping endpoint", e);
                    _connected = TryReconnect();
                }
            }
            else
            {
                _connected = TryReconnect();
            }

            if (_connected)
            {
                _lastConnectedTime = DateTime.UtcNow;
                ResetLastEmailSentTime();
                _logger.Log(LogLevel.Debug, "Integrator diagnostics endpoint is alive");
            }
            else
            {
                if (_lastEmailSentTime + _watchdogSettings.IntegratorNotRunnigResendEmailAfter < DateTime.UtcNow)
                {
                    _lastEmailSentTime = DateTime.UtcNow;

                    _logger.Log(LogLevel.Info, "Integrator Endpoint unavailable since: " + _lastConnectedTime.ToString("yyyy/MM/dd HH:mm:ss UTC"));
                    this.EventLog.WriteEntry("Integrator Endpoint unavailable since: " + _lastConnectedTime.ToString("yyyy/MM/dd HH:mm:ss UTC"), EventLogEntryType.Information);

                    AutoMailer.TrySendMailAsync(
                        "Integrator Endpoint unavailable since: " +
                        _lastConnectedTime.ToString("yyyy/MM/dd HH:mm:ss UTC"), string.Empty, false,
                        _watchdogSettings.EmailRecipientsList, null);
                }
            }
        }

        private bool TryReconnect()
        {
            foreach (IntegratorServiceDiagnosticsEndpoint endpoint in _watchdogSettings.IntegratorServiceDiagnosticsEndpoints)
            {
                try
                {
                    _diagnosticsClient.Connect(endpoint.Endpoint, endpoint.Port);
                    _diagnosticsClient.Ping();
                    return true;
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Warn, e, "Couldn't connect or ping endpoint {0} : {1}",
                                              endpoint.Endpoint, endpoint.Port);
                }
            }

            return false;
        }

        protected override void OnStart(string[] args)
        {
            _logger.Log(LogLevel.Info, "IntegratorWatchDogSvc Starting");
            this.EventLog.WriteEntry("IntegratorWatchDogSvc Starting", EventLogEntryType.Information);

            AutoMailer.TrySendMailAsync("Integrator Watchdog Service is Starting", string.Empty, false, _watchdogSettings.EmailRecipientsList, null);

            _watchdogTimer.Change(WATCHDOG_INTERVAL, WATCHDOG_INTERVAL);

            base.OnStart(args);
        }

        protected override void OnStop()
        {
            _logger.Log(LogLevel.Info, "IntegratorWatchDogSvc Stopping");
            this.EventLog.WriteEntry("IntegratorWatchDogSvc Stopping", EventLogEntryType.Information);

            AutoMailer.TrySendMailAsync("Integrator Watchdog Service is Stopping", string.Empty, false, _watchdogSettings.EmailRecipientsList, null);

            _watchdogTimer.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);

            base.OnStop();
        }


        protected override void OnPause()
        {
            _logger.Log(LogLevel.Info, "IntegratorWatchDogSvc Pausing");
            this.EventLog.WriteEntry("IntegratorWatchDogSvc Pausing", EventLogEntryType.Information);

            AutoMailer.TrySendMailAsync("Integrator Watchdog Service is Pausing", string.Empty, false, _watchdogSettings.EmailRecipientsList, null);

            _watchdogTimer.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);

            base.OnPause();
        }

        /// <summary>
        /// OnContinue(): Put your continue code here
        /// - Un-pause working threads, etc.
        /// </summary>
        protected override void OnContinue()
        {
            _logger.Log(LogLevel.Info, "IntegratorWatchDogSvc Resuming");
            this.EventLog.WriteEntry("IntegratorWatchDogSvc Resuming", EventLogEntryType.Information);

            AutoMailer.TrySendMailAsync("Integrator Watchdog Service is Resuming", string.Empty, false, _watchdogSettings.EmailRecipientsList, null);

            _watchdogTimer.Change(WATCHDOG_INTERVAL, WATCHDOG_INTERVAL);

            base.OnContinue();
        }

        /// <summary>
        /// OnShutdown(): Called when the System is shutting down
        /// - Put code here when you need special handling
        ///   of code that deals with a system shutdown, such
        ///   as saving special data before shutdown.
        /// </summary>
        protected override void OnShutdown()
        {
            _logger.Log(LogLevel.Info, "IntegratorWatchDogSvc Shutdown");
            this.EventLog.WriteEntry("IntegratorWatchDogSvc Shutdown", EventLogEntryType.Information);

            AutoMailer.TrySendMailAsync("Integrator Watchdog Service is going throug computer shutdown", string.Empty, false, _watchdogSettings.EmailRecipientsList, null);


            base.OnShutdown();
        }

        /// <summary>
        /// OnPowerEvent(): Useful for detecting power status changes,
        ///   such as going into Suspend mode or Low Battery for laptops.
        /// </summary>
        /// <param name="powerStatus">The Power Broadcast Status
        /// (BatteryLow, Suspend, etc.)</param>
        protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus)
        {
            _logger.Log(LogLevel.Info, "IntegratorWatchDogSvc OnPowerEvent");
            this.EventLog.WriteEntry("IntegratorWatchDogSvc OnPowerEvent", EventLogEntryType.Information);

            AutoMailer.TrySendMailAsync("Integrator Watchdog Service is going throug low power event", string.Empty, false, _watchdogSettings.EmailRecipientsList, null);


            return base.OnPowerEvent(powerStatus);
        }
    }
}
