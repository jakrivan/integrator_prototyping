﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.Diagnostics
{
    public interface IDealStatisticsConsumer
    {
        void SentOrderExternal(IIntegratorOrderExternal integratorOrderExternal);
        void NewRejectableExecutedDeal(RejectableMMDeal rejectableDeal);
        void NewRejectableRejectedDeal(RejectableMMDeal rejectableDeal);
        void NewCounterpartyTimeoutInfo(CounterpartyTimeoutInfo counterpartyTimeoutInfo);
        RejectableMMDeal RejectExecutedRejectableDeal(string integratorExecutionId, string counterpartyExecutionId, string rejectionReason);
        void ExecutionReportReceived(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal);
        void ExternalDealExecuted(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal);
        void ExternalDealRejected(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal);
        void ExternalDealCancelAcked(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal);
        void ExternalDealCancelSent(ExternalCancelRequest externalCancelRequest, IIntegratorOrderExternal integratorOrderExternal);
        void ExternalDealIgnored(DateTime ignoreDetectedUtc, IIntegratorOrderExternal integratorOrderExternal);
        void UnexpectedDeal(OrderChangeInfo orderChangeInfo, Counterparty counterparty);
        void NewExternalDealExecutedTicket(TradeReportTicketInfo tradeReportTicketInfo);
        void NewAggregatedTradeTicketInfo(AggregatedTradeTicketInfo aggregatedTradeTicketInfo);
    }

    public interface IDealStatistics: IDealStatisticsConsumer, IDealStatisticsPublisher
    { }

    public class DealStatistics : IDealStatistics
    {
        private IOrdersPersistor _ordersPersistor;
        private ILogger _logger;

        public DealStatistics(IPriceStreamMonitor priceStreamMonitor)
            :this(new OrdersPersistor(DiagnosticsManager.DiagLogger),  priceStreamMonitor, DiagnosticsManager.DiagLogger)
        { }

        public DealStatistics(IOrdersPersistor ordersPersistor, IPriceStreamMonitor priceStreamMonitor, ILogger logger)
        {
            this._ordersPersistor = ordersPersistor;
            this._logger = logger;
            this._priceStreamMonitor = priceStreamMonitor;
        }

        public event Action<OrderChangeInfo, IIntegratorOrderExternal> NewExternalDealExecuted;
        public event Action<RejectableMMDeal> NewRejectableDealExecuted;
        public event Action<RejectableMMDeal> RejectableExecutedDealRejected;

        private IPriceStreamMonitor _priceStreamMonitor;

        #region CaptureInfoToBackend

        public void SentOrderExternal(IIntegratorOrderExternal integratorOrderExternal)
        {
            this._ordersPersistor.PersistNewExternalOrder(integratorOrderExternal);
        }

        public void NewRejectableExecutedDeal(RejectableMMDeal rejectableDeal)
        {
            this._ordersPersistor.PersistNewRejectableDeal(rejectableDeal);
            if (NewRejectableDealExecuted != null)
                try
                {
                    NewRejectableDealExecuted(rejectableDeal);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, "Error during publishing executed streamig deal to STP connector", e);
                }   
        }

        public void NewRejectableRejectedDeal(RejectableMMDeal rejectableDeal)
        {
            this._ordersPersistor.PersistNewRejectableDeal(rejectableDeal);
        }

        public void NewCounterpartyTimeoutInfo(CounterpartyTimeoutInfo counterpartyTimeoutInfo)
        {
            this._ordersPersistor.PersistNewCounterpartyTimeoutInfo(counterpartyTimeoutInfo);
        }

        public RejectableMMDeal RejectExecutedRejectableDeal(string integratorExecutionId, string counterpartyExecutionId, string rejectionReason)
        {
            RejectableMMDeal rejectableDeal = this._ordersPersistor.RejectExecutedRejectableDeal(integratorExecutionId, counterpartyExecutionId, rejectionReason);

            if (rejectableDeal == null)
                return null;

            if (RejectableExecutedDealRejected != null)
                TaskEx.StartNew(() => RejectableExecutedDealRejected(rejectableDeal));

            return rejectableDeal;
        }

        public void ExecutionReportReceived(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal)
        {
            if (integratorOrderExternal.IsActualizedWithInfoAboutSending)
            {
                this._ordersPersistor.PersistExecutionReport(orderChangeInfo, integratorOrderExternal);
            }
            else
            {
                this.ExecuteActionOnceConditionMet(() => integratorOrderExternal.IsActualizedWithInfoAboutSending,
                                                   () => this._ordersPersistor.PersistExecutionReport(orderChangeInfo, integratorOrderExternal),
                                                   _backendOperationWaitDelayMs, _retryCount);
            }
        }

        public void ExternalDealExecuted(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal)
        {
            decimal termCurrencyUsdMultiplier;

            decimal price = orderChangeInfo.ChangeEventArgs.ExecutionInfo.UsedPrice.Value;

            if (price != 0 && integratorOrderExternal.OrderRequestInfo.Symbol.TermCurrency == Currency.USD)
            {
                //term currency multiplier is multiplier from the term size (= base * price) - so for term = USD, we are done
                termCurrencyUsdMultiplier = 1m;
            }
            else if (price != 0 && integratorOrderExternal.OrderRequestInfo.Symbol.BaseCurrency == Currency.USD)
            {
                termCurrencyUsdMultiplier = 1m / price;
            }
            else
            {
                termCurrencyUsdMultiplier =
                this._priceStreamMonitor.UsdConversionMultipliers[
                    (int)integratorOrderExternal.OrderRequestInfo.Symbol.TermCurrency];
            }

            //safe invoke checks for null
            NewExternalDealExecuted.SafeInvoke(this._logger, orderChangeInfo, integratorOrderExternal);

            if (integratorOrderExternal.IsActualizedWithInfoAboutSending)
            {
                this._ordersPersistor.PersistExternalExecutedDeal(orderChangeInfo, integratorOrderExternal,
                                                              termCurrencyUsdMultiplier);
            }
            else
            {
                this.ExecuteActionOnceConditionMet(() => integratorOrderExternal.IsActualizedWithInfoAboutSending,
                                                   () => this._ordersPersistor.PersistExternalExecutedDeal(orderChangeInfo, integratorOrderExternal,
                                                              termCurrencyUsdMultiplier),
                                                   _backendOperationWaitDelayMs, _retryCount);
            }
        }

        public void NewExternalDealExecutedTicket(TradeReportTicketInfo tradeReportTicketInfo)
        {
            this._ordersPersistor.PersistNewExternalDealExecutedTicket(tradeReportTicketInfo);
        }

        public void NewAggregatedTradeTicketInfo(AggregatedTradeTicketInfo aggregatedTradeTicketInfo)
        {
            this._ordersPersistor.PersistNewAggregatedTradeTicketInfo(aggregatedTradeTicketInfo);
        }

        public void ExternalDealRejected(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal)
        {
            if (integratorOrderExternal.IsActualizedWithInfoAboutSending)
            {
                this._ordersPersistor.PersistExternalRejectedDeal(orderChangeInfo, integratorOrderExternal);
            }
            else
            {
                this.ExecuteActionOnceConditionMet(() => integratorOrderExternal.IsActualizedWithInfoAboutSending,
                                                   () => this._ordersPersistor.PersistExternalRejectedDeal(orderChangeInfo, integratorOrderExternal),
                                                   _backendOperationWaitDelayMs, _retryCount);
            }
        }

        private static readonly int _backendOperationWaitDelayMs = (int) TimeSpan.FromSeconds(2).TotalMilliseconds;
        private const int _retryCount = 5;

        private void ExecuteActionOnceConditionMet(Func<bool> conditionFunc, Action action,
                                                   int waitDelayBetweenRetriesMs, int maxAttempts)
        {
            if (conditionFunc() || maxAttempts <= 0)
            {
                action();
            }
            else
            {
                Task.Delay(waitDelayBetweenRetriesMs)
                    .ContinueWith(
                        t =>
                        ExecuteActionOnceConditionMet(conditionFunc, action, waitDelayBetweenRetriesMs*2, maxAttempts - 1));
            }
        }

        public void ExternalDealCancelAcked(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal)
        {
            if (integratorOrderExternal == null)
            {
                this._logger.Log(LogLevel.Fatal, "Attempt to send cancel acked for null order. change info: {0}", orderChangeInfo);
                return;
            }

            if (integratorOrderExternal.IsActualizedWithInfoAboutSending)
            {
                this._ordersPersistor.PersistOrderExternalCancelResult(orderChangeInfo, integratorOrderExternal);
            }
            else
            {
                this.ExecuteActionOnceConditionMet(() => integratorOrderExternal.IsActualizedWithInfoAboutSending,
                                                   () => this._ordersPersistor.PersistOrderExternalCancelResult(orderChangeInfo, integratorOrderExternal),
                                                   _backendOperationWaitDelayMs, _retryCount);
            }
        }

        public void ExternalDealCancelSent(ExternalCancelRequest externalCancelRequest, IIntegratorOrderExternal integratorOrderExternal)
        {
            if (integratorOrderExternal == null || integratorOrderExternal.IsActualizedWithInfoAboutSending)
            {
                this._ordersPersistor.PersistOrderExternalCancelRequest(externalCancelRequest, integratorOrderExternal);
            }
            else
            {
                this.ExecuteActionOnceConditionMet(() => integratorOrderExternal.IsActualizedWithInfoAboutSending,
                                                   () => this._ordersPersistor.PersistOrderExternalCancelRequest(externalCancelRequest, integratorOrderExternal),
                                                   _backendOperationWaitDelayMs, _retryCount);
            }
        }

        public void ExternalDealIgnored(DateTime ignoreDetectedUtc, IIntegratorOrderExternal integratorOrderExternal)
        {
            if (integratorOrderExternal.IsActualizedWithInfoAboutSending)
            {
                this._ordersPersistor.PersistOrderExternalIgnored(ignoreDetectedUtc, integratorOrderExternal);
            }
            else
            {
                this.ExecuteActionOnceConditionMet(() => integratorOrderExternal.IsActualizedWithInfoAboutSending,
                                                   () => this._ordersPersistor.PersistOrderExternalIgnored(ignoreDetectedUtc, integratorOrderExternal),
                                                   _backendOperationWaitDelayMs, _retryCount);
            }
        }

        public void UnexpectedDeal(OrderChangeInfo orderChangeInfo, Counterparty counterparty)
        {
            this._ordersPersistor.PersistUnexpectedExecution(orderChangeInfo, counterparty);
        }

        #endregion /CaptureInfoToBackend
    }
}
