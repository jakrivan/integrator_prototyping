﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.Diagnostics
{
    //Min+Max heap - adaptation of code from http://ideone.com/8VVEa
    public interface IMedianTracker<TNode> : IMedianProvider<TNode>
    {
        void InsertItem(TNode newItem, DateTime itemTimeStamp);
        bool IsFull { get; }
        TNode OldestItem { get; }
        DateTime OldestItemTimeStamp { get; }
    }

    public class MedianTracker<TNode> : IMedianTracker<TNode>
    {
        private TNode[] _data; //circualar buffer with the data
        private int _dataIdx; //current position in circualr buffer
        private int[] _heapIndexesToData; //heap holding indexes to the data  //heap
        private int[] _arrayIndexesToHeap; //indexes telling you where the data from circualar buffer (on the same index) have it's index in the heap
        //pos

        private int _maxSize;
        private int _currentSize;
        private IComparer<TNode> _rankComparer;
        private Func<TNode, TNode, int> _rankCompareFunc;
        private Func<TNode, TNode, TNode> _averageFunc;

        //circular buffer with timestamps
        private DateTime[] _itemsTimestamps;
        private int _itemsTimestampsIndex;

        private SpinLock _spinLock = new SpinLock(false);


        public MedianTracker(int maxHeapSize, IComparer<TNode> rankComparer, Func<TNode, TNode, TNode> averageFunc)
        {
            this._maxSize = maxHeapSize;
            this._currentSize = 0;
            this._rankComparer = rankComparer;
            this._rankCompareFunc = rankComparer.Compare;
            this._averageFunc = averageFunc;


            this._data = new TNode[_maxSize];
            this._dataIdx = 0;
            this._heapIndexesToData = new int[_maxSize];
            this._arrayIndexesToHeap = new int[_maxSize];

            this._itemsNumInFullMinHeap = (_maxSize - 1) / 2;
            this._itemsNumInFullMaxHeap = _maxSize / 2;
            this._heapMiddle = _maxSize / 2;

            _itemsTimestamps = new DateTime[maxHeapSize];

            //set up initial heap fill pattern: median,max,min,max,...
            for (int i = maxHeapSize - 1; i >= 0; i--)
            {
                _arrayIndexesToHeap[i] = ((i + 1) / 2) * ((i & 1) == 1 ? -1 : 1);
                this[_arrayIndexesToHeap[i]] = i;
            }
        }

        public TNode OldestItem
        {
            get
            {
                bool lockTaken = false;
                _spinLock.Enter(ref lockTaken);

                TNode oldestValue = this._data[this._dataIdx];

                if (lockTaken)
                {
                    _spinLock.Exit();
                }

                return oldestValue;
            }
        }

        public DateTime OldestItemTimeStamp
        {
            get
            {
                DateTime oldestItemTimeStamp;

                bool lockTaken = false;
                _spinLock.Enter(ref lockTaken);


                oldestItemTimeStamp = this._itemsTimestamps[this._itemsTimestampsIndex];

                if (lockTaken)
                {
                    _spinLock.Exit();
                }

                return oldestItemTimeStamp;
            }
        }

        //Inserts item, maintains median in O(lg nItems)
        public void InsertItem(TNode newItem, DateTime itemTimeStamp)
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            this._itemsTimestampsIndex = (this._itemsTimestampsIndex + 1) % this._maxSize;
            this._itemsTimestamps[this._itemsTimestampsIndex] = itemTimeStamp;

            bool isFull = this._currentSize == this._maxSize;
            int indexToHeapOfOldestItem = this._arrayIndexesToHeap[this._dataIdx];
            TNode oldestValue = this._data[this._dataIdx];
            this._data[this._dataIdx] = newItem;
            this._dataIdx = (this._dataIdx + 1) % this._maxSize;
            if (!isFull)
            {
                this._currentSize++;
            }

            //new item is in minHeap
            if (indexToHeapOfOldestItem > 0)
            {
                if (isFull && _rankCompareFunc(oldestValue, newItem) < 0)
                {
                    MinHeapSortDown(indexToHeapOfOldestItem * 2);
                }
                else if (MinHeapSortUp(indexToHeapOfOldestItem))
                {
                    MaxHeapSortDown(-1);
                }
            }
            //new item is in maxheap
            else if (indexToHeapOfOldestItem < 0)
            {
                if (isFull && _rankCompareFunc(newItem, oldestValue) < 0)
                {
                    MaxHeapSortDown(indexToHeapOfOldestItem * 2);
                }
                else if (MaxHeapSortUp(indexToHeapOfOldestItem))
                {
                    MinHeapSortDown(1);
                }
            }
            //new item is at median
            else
            {
                if (ItemsNumInMaxHeap > 0)
                {
                    MaxHeapSortDown(-1);
                }
                if (ItemsNumInMinHeap > 0)
                {
                    MinHeapSortDown(1);
                }
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }
        }

        public TNode Median
        {
            get
            {
                bool lockTaken = false;
                _spinLock.Enter(ref lockTaken);

                TNode middleValue = _data[this[0]];
                if ((_currentSize & 1) == 0)
                {
                    middleValue = this._averageFunc(middleValue, _data[this[-1]]);
                }

                if (lockTaken)
                {
                    _spinLock.Exit();
                }

                return middleValue;
            }
        }

        public bool IsFull
        {
            get { return _currentSize == _maxSize; }
        }

        public event Action<TNode> MedianUpdated;


        private readonly int _heapMiddle;
        private int this[int heapIndex]
        {
            get
            {
                return _heapIndexesToData[heapIndex + _heapMiddle];
            }
            set { _heapIndexesToData[heapIndex + _heapMiddle] = value; }
        }

        private int _itemsNumInFullMinHeap;
        private int ItemsNumInMinHeap
        {
            get
            {
                if (_currentSize == _maxSize)
                {
                    return this._itemsNumInFullMinHeap;
                }
                else
                {
                    return (_currentSize - 1) / 2;
                }
            }
        }

        private int _itemsNumInFullMaxHeap;
        private int ItemsNumInMaxHeap
        {
            get
            {
                if (_currentSize == _maxSize)
                {
                    return this._itemsNumInFullMaxHeap;
                }
                else
                {
                    return _currentSize / 2;
                }
            }
        }

        //swaps items in heap, maintains indexes
        private void SwapItemsInHeap(int idx1, int idx2)
        {
            int temp = this[idx1];
            this[idx1] = this[idx2];
            this[idx2] = temp;
            _arrayIndexesToHeap[this[idx1]] = idx1;
            _arrayIndexesToHeap[this[idx2]] = idx2;
        }

        private bool IsFirstItemInHeapLower(int firstItemIdx, int secondItemIdx)
        {
            return this._rankCompareFunc(_data[this[firstItemIdx]], _data[this[secondItemIdx]]) < 0;
        }

        //maintains minheap property for all items below idx/2.
        private void MinHeapSortDown(int idx)
        {
            for (; idx <= this.ItemsNumInMinHeap; idx *= 2)
            {
                if (idx > 1 && idx < this.ItemsNumInMinHeap && IsFirstItemInHeapLower(idx + 1, idx))
                {
                    ++idx;
                }

                if (IsFirstItemInHeapLower(idx, idx / 2))
                {
                    SwapItemsInHeap(idx, idx / 2);
                }
                else
                {
                    break;
                }
            }
        }

        //maintains maxheap property for all items below idx/2. (negative indexes)
        private void MaxHeapSortDown(int idx)
        {
            for (; idx >= -this.ItemsNumInMaxHeap; idx *= 2)
            {
                if (idx < -1 && idx > -this.ItemsNumInMaxHeap && IsFirstItemInHeapLower(idx, idx - 1))
                {
                    --idx;
                }

                if (IsFirstItemInHeapLower(idx / 2, idx))
                {
                    SwapItemsInHeap(idx / 2, idx);
                }
                else
                {
                    break;
                }
            }
        }


        //maintains minheap property for all items above idx, including median
        //returns true if median changed
        bool MinHeapSortUp(int idx)
        {
            while (idx > 0 && IsFirstItemInHeapLower(idx, idx / 2))
            {
                SwapItemsInHeap(idx, idx / 2);
                idx /= 2;
            }
            return (idx == 0);
        }

        //maintains maxheap property for all items above idx, including median
        //returns true if median changed
        bool MaxHeapSortUp(int idx)
        {
            while (idx < 0 && IsFirstItemInHeapLower(idx / 2, idx))
            {
                SwapItemsInHeap(idx / 2, idx);
                idx /= 2;
            }
            return (idx == 0);
        }
    }
}
