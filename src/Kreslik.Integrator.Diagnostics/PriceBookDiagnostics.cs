﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Diagnostics
{
    public class PriceBookDiagnostics : IPriceBookDiagnostics
    {
        private IPriceBookStoreEx _priceBookStore;

        public PriceBookDiagnostics(IPriceBookStoreEx priceBookStore)
        {
            this._priceBookStore = priceBookStore;

            for (int idx = 0; idx < Symbol.ValuesCount; idx++)
            {
                _priceStreamInfoEntries.Add(new WritableToBPriceStreamInfoEntry((Symbol)idx));
            }

        }

        List<ToBPriceStreamInfoEntry> _priceStreamInfoEntries = new List<ToBPriceStreamInfoEntry>();
        public List<ToBPriceStreamInfoEntry> GetTopOfBookInfoEntries()
        {
            DateTime utcNow = DateTime.UtcNow;

            //Dangerous but faster
            foreach (WritableToBPriceStreamInfoEntry priceStreamInfoEntry in _priceStreamInfoEntries)
            {
                PriceObject askPrice = _priceBookStore.GetAskPriceBook(priceStreamInfoEntry.Symbol).MaxNode;
                PriceObject bidPrice = _priceBookStore.GetBidPriceBook(priceStreamInfoEntry.Symbol).MaxNode;

                bool isAskPriceNull = PriceObjectInternal.IsNullPrice(askPrice);
                bool isBidPriceNull = PriceObjectInternal.IsNullPrice(bidPrice);

                if (PriceObjectInternal.IsNullPrice(askPrice))
                {
                    priceStreamInfoEntry.ResetAsk();
                }
                else
                {
                    priceStreamInfoEntry.SetAsk(askPrice.Price, utcNow - askPrice.IntegratorReceivedTimeUtc, askPrice.Counterparty);
                }

                if (PriceObjectInternal.IsNullPrice(bidPrice))
                {
                    priceStreamInfoEntry.ResetBid();
                }
                else
                {
                    priceStreamInfoEntry.SetBid(bidPrice.Price, utcNow - bidPrice.IntegratorReceivedTimeUtc, bidPrice.Counterparty);
                }

                priceStreamInfoEntry.RecalculateSpreadBp();
            }

            return _priceStreamInfoEntries;
        }
    }
}
