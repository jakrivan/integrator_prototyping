﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.Diagnostics
{
    public enum IntegratorProcessStartBehavior
    {
        SinglePerInstance,
        SinglePerEndpoint,
        Unrestricted
    }

    public class IntegratorProcessStateKeeper
    {
        private readonly IIntegratorInstanceProcessUtils _integratorInstanceProcessUtils;
        private IntegratorProcessInfo _integratorProcessInfo;
        private SafeTimer _hbTimer;
        private static TimeSpan _hbTimespan = TimeSpan.FromMinutes(1);
        private ILogger _logger;

        public IntegratorProcessStateKeeper(IIntegratorInstanceProcessUtils integratorInstanceProcessUtils, ILogger logger)
        {
            this._integratorInstanceProcessUtils = integratorInstanceProcessUtils;
            this._logger = logger;
        }

        public IntegratorProcessStateKeeper(IntegratorProcessType integratorProcessType)
            : this(new IntegratorInstanceProcessUtils(integratorProcessType), LogFactory.Instance.GetLogger(null))
        {
            IntegratorStateInitializer.Initialize(integratorProcessType);
        }

        private string GetKnownLocalPrivateIp()
        {
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName()/*"localhost"*/);
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    string localIP = ip.ToString();
                    if (this._integratorInstanceProcessUtils.IsKnownPrivateIp(localIP))
                    {
                        return localIP;
                    }
                }
            }

            return null;
        }

        private void HeartBeatCallback()
        {
            //SafeTimer will catch possible exceptions and reschedule
            this._integratorInstanceProcessUtils.HeartBeat(this._integratorProcessInfo);
        }

        //private bool IsProcessDissalowingCurrentProcessFromStarting(IntegratorProcessInfo integratorProcessInfo, IntegratorProcessStartBehavior integratorProcessStartBehavior)
        //{
        //    bool isit = integratorProcessStartBehavior == IntegratorProcessStartBehavior.SinglePerInstance &&
        //}

        private IntegratorProcessInfo SelectProcessDissalowingCurrentProcessFromStarting(IntegratorProcessInfo currentIntegratorProcessInfo, List<IntegratorProcessInfo> existingProcesses, IntegratorProcessStartBehavior integratorProcessStartBehavior)
        {
            IntegratorProcessInfo disallowingProcess = null;

            switch (integratorProcessStartBehavior)
            {
                case IntegratorProcessStartBehavior.SinglePerInstance:
                    disallowingProcess =
                        existingProcesses.FirstOrDefault(
                            process =>
                            process.ProcessIdentifier != currentIntegratorProcessInfo.ProcessIdentifier &&
                            process.IntegratorProcessState != IntegratorProcessState.Inactive &&
                            process.IntegratorProcessState != IntegratorProcessState.Unknown);
                    break;
                case IntegratorProcessStartBehavior.SinglePerEndpoint:
                    disallowingProcess =
                        existingProcesses.FirstOrDefault(
                            process =>
                            process.ProcessIdentifier != currentIntegratorProcessInfo.ProcessIdentifier &&
                            process.Hostname == currentIntegratorProcessInfo.Hostname &&
                            process.IntegratorProcessState != IntegratorProcessState.Inactive &&
                            process.IntegratorProcessState != IntegratorProcessState.Unknown);
                    break;
                case IntegratorProcessStartBehavior.Unrestricted:
                default:
                    break;
            }

            return disallowingProcess;
        }

        /// <summary>
        /// Registers the current integrator porcess and fires HB thread
        ///  if registration was unsuccessful, it will atempt to Inactivate current instance in DB, and this object is unusable
        /// </summary>
        /// <returns></returns>
        public IntegratorRequestResult TryRegisterIntegratorProcess(IntegratorProcessStartBehavior integratorProcessStartBehavior)
        {
            try
            {
                string privateIp = this.GetKnownLocalPrivateIp();
                if (string.IsNullOrEmpty(privateIp))
                    return IntegratorRequestResult.CreateFailedResult("Local machine doesn't have any private IP address known to the DB");

                int? integratorProcessIdentifier;
                List<IntegratorProcessInfo> activeProcesses =
                    this._integratorInstanceProcessUtils.TryRegisterIntegratorProcess(privateIp,
                                                                                      out integratorProcessIdentifier);
                if (!integratorProcessIdentifier.HasValue)
                    return
                        IntegratorRequestResult.CreateFailedResult(
                            "Registration procedure didn't return process identifier - cannot locate registration");
                this._integratorProcessInfo =
                    activeProcesses.FirstOrDefault(
                        process => process.ProcessIdentifier == integratorProcessIdentifier.Value);

                if (this._integratorProcessInfo == null)
                {
                    this._logger.Log(LogLevel.Fatal, "Unexpected - couldn't locate current process - id[{0}] - in backend", integratorProcessIdentifier);
                    return IntegratorRequestResult.CreateFailedResult("Couldn't locate current process in backend");
                }

                //let's do those now so that we can set the inactive state
                this.BindProcessInfoEvents(this._integratorProcessInfo);

                IntegratorProcessInfo otherActiveIntegratorProcessInfo =
                    this.SelectProcessDissalowingCurrentProcessFromStarting(this._integratorProcessInfo, activeProcesses,
                                                                            integratorProcessStartBehavior);
                if (otherActiveIntegratorProcessInfo != null)
                {
                    if (this._integratorProcessInfo != null)
                    {
                        this.SetInactive();
                    }

                    this._logger.Log(LogLevel.Fatal,
                                     "There is already different process (same name and instance) - id[{0}] in active state, while StartBehavior is {1}. Shutting down itself",
                                     otherActiveIntegratorProcessInfo.ProcessIdentifier, integratorProcessStartBehavior);
                    
                    return IntegratorRequestResult.CreateFailedResult("Differnt process with same name and instance is already active");
                }

                _hbTimer = new SafeTimer(HeartBeatCallback, _hbTimespan, _hbTimespan, true)
                {
                    RequiredTimerPrecision = TimerTaskFlagUtils.GetTimerPrecisionForDelay(_hbTimespan),
                    TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
                };
                return IntegratorRequestResult.GetSuccessResult();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, "Unexpected problems during registering Integrator process", e);
                this._integratorProcessInfo = null;
                return IntegratorRequestResult.CreateFailedResult("Unexpected problems during registering Integrator process" + e);
            }
        }

        private void BindProcessInfoEvents(IntegratorProcessInfo integratorProcessInfo)
        {
            integratorProcessInfo.IntegratorProcessStateChanged += TrySetBackendState;
        }

        private void TrySetBackendState(IntegratorProcessState newState)
        {
            bool success = false;

            if (this._integratorProcessInfo == null)
            {
                this._logger.Log(LogLevel.Fatal, "Attmept to call IntegratorProcessStateKeeper despite it wasn't successfuly registered");
            }
            else
            {
                success = AdoNetHelpers.RetrySqlOperation(
                    () =>
                    this._integratorInstanceProcessUtils.UpdateIntegratorProcessState(this._integratorProcessInfo, newState),
                    "SettingProcessState", this._logger, 3, false);
            }

            if (!success)
            {
                this._logger.Log(LogLevel.Fatal, "Failed to set process [{0}] to {1} state", this._integratorProcessInfo.ProcessIdentifier, newState);
            }
        }

        private void TrySetInMemoryState(IntegratorProcessState newState)
        {
            if (this._integratorProcessInfo == null)
            {
                this._logger.Log(LogLevel.Fatal, "Attmept to call IntegratorProcessStateKeeper despite it wasn't successfuly registered");
            }
            else
            {
                //this will trigger events and so also storing to backend
                this._integratorProcessInfo.IntegratorProcessState = newState;
            }
        }


        public void SetRunning()
        {
            TrySetInMemoryState(IntegratorProcessState.Running);
        }

        public void SetShuttingDown()
        {
            TrySetInMemoryState(IntegratorProcessState.ShuttingDown);
        }

        public void SetInactive()
        {
            TrySetInMemoryState(IntegratorProcessState.Inactive);

            if (_hbTimer != null)
            {
                _hbTimer.Dispose();
            }
        }

        public IntegratorProcessInfo IntegratorProcessInfo { get { return this._integratorProcessInfo; } }

        //public string PrivateIPAddress { get { return this._integratorProcessInfo.PrivateIPAddress; } }

        //public string PublicIPAddress { get { return this._integratorProcessInfo.PublicIPAddress; } }
    }
}
