﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.QuickItchN;

namespace Kreslik.Integrator.Diagnostics
{
    public class FIXSessionsDiagnostics : IFIXSessionsDiagnostics
    {
        private IEnumerable<IStpFlowSessionInfo> _stpFlowSessionInfos;
        private List<IFIXChannel> _ofSessions;
        private List<IFIXChannel> _mdSessions;
        private IIntegratorInstanceUtils _integratorInstanceUtils;

        private List<SessionStatusInfo> _mdSessionsStatusInfos =
            Enumerable.Range(0, Counterparty.ValuesCount)
                      .Select(dummy => new SessionStatusInfo(false, false, SessionState.Idle){Claimed = false})
                      .ToList();
        private List<SessionStatusInfo> _ofSessionsStatusInfos =
            Enumerable.Range(0, Counterparty.ValuesCount)
                      .Select(dummy => new SessionStatusInfo(false, false, SessionState.Idle){ Claimed = false })
                      .ToList();
        
        public FIXSessionsDiagnostics(IEnumerable<IFIXChannel> marketDataSessions,
                                      IEnumerable<IFIXChannel> orderFlowSessions, IIntegratorInstanceUtils integratorInstanceUtils, IEnumerable<IStpFlowSessionInfo> stpFlowSessionInfos)
        {
            this._stpFlowSessionInfos = stpFlowSessionInfos;
            this._integratorInstanceUtils = integratorInstanceUtils;

            _ofSessions =
                Counterparty.Values.Select(ctp => orderFlowSessions.FirstOrDefault(of => of != null && of.Counterparty == ctp))
                            .ToList();
            _mdSessions =
                Counterparty.Values.Select(ctp => marketDataSessions.FirstOrDefault(of => of != null && of.Counterparty == ctp))
                            .ToList();
        }

        private void RefreshCounterpartySessionsStatusInfos(List<SessionStatusInfo> counterpartySessionsStatusInfos,
                                                            List<IFIXChannel> sessions, IIntegratorInstanceUtils integratorInstanceUtils)
        {
            for (int ctpIdx = 0; ctpIdx < Counterparty.ValuesCount; ctpIdx++)
            {
                if (sessions[ctpIdx] == null)
                {
                    counterpartySessionsStatusInfos[ctpIdx].SetUnconfigured();
                }
                else if(sessions[ctpIdx].Inactivated)
                {
                    counterpartySessionsStatusInfos[ctpIdx].SetInactivated();
                }
                else
                {
                    counterpartySessionsStatusInfos[ctpIdx].SetState(sessions[ctpIdx].State);
                }

                counterpartySessionsStatusInfos[ctpIdx].Claimed =
                    integratorInstanceUtils.IsCounterpartySessionClaimed((Counterparty) ctpIdx);
            }
        }


        public List<SessionStatusInfo> MarketDataSessionsStatusInfos
        {
            get
            {
                RefreshCounterpartySessionsStatusInfos(_mdSessionsStatusInfos, _mdSessions, _integratorInstanceUtils);
                return _mdSessionsStatusInfos;
            }
        }

        public List<SessionStatusInfo> OrderFlowSessionsStatusInfos
        {
            get
            {
                RefreshCounterpartySessionsStatusInfos(_ofSessionsStatusInfos, _ofSessions, _integratorInstanceUtils);
                return _ofSessionsStatusInfos;
            }
        }

        private List<StpFlowSessionInfo> _outgoingStpFlowSessionInfos;

        private void RefreshStpFlowSessionInfos()
        {
            if (_stpFlowSessionInfos == null)
                return;

            if (_outgoingStpFlowSessionInfos == null)
            {
                _outgoingStpFlowSessionInfos =
                    _stpFlowSessionInfos.Select(
                        info =>
                        new StpFlowSessionInfo()
                            {
                                StpCounterparty = info.StpCounterparty,
                                StpFlowSessionState = info.StpFlowSessionState
                            }).ToList();
            }

            int idx = 0;
            foreach (IStpFlowSessionInfo stpFlowSessionInfo in _stpFlowSessionInfos)
            {
                _outgoingStpFlowSessionInfos[idx++].StpFlowSessionState = stpFlowSessionInfo.StpFlowSessionState;
            }
        }

        public List<StpFlowSessionInfo> StpFlowSessionInfos
        {
            get
            {
                RefreshStpFlowSessionInfos();
                return this._outgoingStpFlowSessionInfos;
            }
        }
    }
}
