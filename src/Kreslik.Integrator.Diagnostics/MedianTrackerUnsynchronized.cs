﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Diagnostics
{
    public class PriceMedianTrackerUnsynchronized : MedianTrackerUnsynchronized<AtomicDecimal>, IMedianProvider<decimal>
    {
        public PriceMedianTrackerUnsynchronized(int maxHeapSize, Func<AtomicDecimal, AtomicDecimal, int> rankCompareFunc)
            : base(maxHeapSize, rankCompareFunc)
        { }

        public PriceMedianTrackerUnsynchronized(int maxHeapSize, IComparer<AtomicDecimal> rankComparer)
            : base(maxHeapSize, rankComparer)
        { }
        decimal IMedianProvider<decimal>.Median
        {
            get { return base.Median.ToDecimal(); }
        }
    }

    public class MedianTrackerUnsynchronized<TNode> : IMedianTracker<TNode>
    {
        private TNode[] _data; //circualar buffer with the data
        private int _dataIdx; //current position in circualr buffer
        private int[] _heapIndexesToData; //heap holding indexes to the data  //heap
        private int[] _arrayIndexesToHeap; //indexes telling you where the data from circualar buffer (on the same index) have it's index in the heap
        //pos

        private readonly int _maxSize;
        private readonly int _mask;
        private int _currentSize;
        //private IComparer<TNode> _rankComparer;
        private Func<TNode, TNode, int> _rankCompareFunc;

        //circular buffer with timestamps
        private DateTime[] _itemsTimestamps;
        private int _itemsTimestampsIndex;

        private static bool IsPowerOfTwo(long x)
        {
            return (x & (x - 1)) == 0;
        }

        public MedianTrackerUnsynchronized(int maxHeapSize, Func<TNode, TNode, int> rankCompareFunc)
        {
            if (!IsPowerOfTwo(maxHeapSize))
                throw new Exception("Median tracker must have size whi is power of 2");

            this._maxSize = maxHeapSize;
            this._mask = maxHeapSize - 1;
            this._currentSize = 0;
            //this._rankComparer = rankComparer;
            this._rankCompareFunc = rankCompareFunc;


            this._data = new TNode[_maxSize];
            this._dataIdx = 0;
            this._heapIndexesToData = new int[_maxSize];
            this._arrayIndexesToHeap = new int[_maxSize];

            this._itemsNumInFullMinHeap = (_maxSize - 1) / 2;
            this._itemsNumInFullMaxHeap = _maxSize / 2;
            this._heapMiddle = _maxSize / 2;

            _itemsTimestamps = new DateTime[maxHeapSize];

            //set up initial heap fill pattern: median,max,min,max,...
            for (int i = maxHeapSize - 1; i >= 0; i--)
            {
                _arrayIndexesToHeap[i] = ((i + 1) / 2) * ((i & 1) == 1 ? -1 : 1);
                this[_arrayIndexesToHeap[i]] = i;
            }
        }

        public MedianTrackerUnsynchronized(int maxHeapSize, IComparer<TNode> rankComparer)
            :this(maxHeapSize, rankComparer.Compare)
        { }

        public TNode OldestItem
        {
            get
            {
                TNode oldestValue = this._data[this._dataIdx];
                return oldestValue;
            }
        }

        public DateTime OldestItemTimeStamp
        {
            get
            {
                DateTime oldestItemTimeStamp;
                oldestItemTimeStamp = this._itemsTimestamps[this._itemsTimestampsIndex];
                return oldestItemTimeStamp;
            }
        }

        //Inserts item, maintains median in O(lg nItems)
        public void InsertItem(TNode newItem, DateTime itemTimeStamp)
        {
            this._itemsTimestampsIndex = (this._itemsTimestampsIndex + 1) & this._mask;
            this._itemsTimestamps[this._itemsTimestampsIndex] = itemTimeStamp;

            bool isFull = this._currentSize == this._maxSize;
            int indexToHeapOfOldestItem = this._arrayIndexesToHeap[this._dataIdx];
            TNode oldestValue = this._data[this._dataIdx];
            this._data[this._dataIdx] = newItem;
            this._dataIdx = (this._dataIdx + 1) & this._mask;
            if (!isFull)
            {
                this._currentSize++;
            }

            //new item is in minHeap
            if (indexToHeapOfOldestItem > 0)
            {
                if (isFull && _rankCompareFunc(oldestValue, newItem) < 0)
                {
                    MinHeapSortDown(indexToHeapOfOldestItem << 1);
                }
                else if (MinHeapSortUp(indexToHeapOfOldestItem))
                {
                    MaxHeapSortDown(-1);
                }
            }
            //new item is in maxheap
            else if (indexToHeapOfOldestItem < 0)
            {
                if (isFull && _rankCompareFunc(newItem, oldestValue) < 0)
                {
                    MaxHeapSortDown(indexToHeapOfOldestItem << 1);
                }
                else if (MaxHeapSortUp(indexToHeapOfOldestItem))
                {
                    MinHeapSortDown(1);
                }
            }
            //new item is at median
            else
            {
                if (ItemsNumInMaxHeap > 0)
                {
                    MaxHeapSortDown(-1);
                }
                if (ItemsNumInMinHeap > 0)
                {
                    MinHeapSortDown(1);
                }
            }

            //as access is not synchronized, make sure that median is update after each insert
            this.Median = this._data[this[0]];
        }

        //we want to be sure that median is not changed during 'bubbling phase'
        public TNode Median { get; private set;
            //{
            //    return _data[this[0]];
            //}
        }

        public bool IsFull
        {
            get { return _currentSize == _maxSize; }
        }

        public event Action<TNode> MedianUpdated;


        private readonly int _heapMiddle;
        private int this[int heapIndex]
        {
            get
            {
                return _heapIndexesToData[heapIndex + _heapMiddle];
            }
            set { _heapIndexesToData[heapIndex + _heapMiddle] = value; }
        }

        private int _itemsNumInFullMinHeap;
        private int ItemsNumInMinHeap
        {
            get
            {
                if (_currentSize == _maxSize)
                {
                    return this._itemsNumInFullMinHeap;
                }
                else
                {
                    return (_currentSize - 1) >> 1;
                }
            }
        }

        private int _itemsNumInFullMaxHeap;
        private int ItemsNumInMaxHeap
        {
            get
            {
                if (_currentSize == _maxSize)
                {
                    return this._itemsNumInFullMaxHeap;
                }
                else
                {
                    return _currentSize >> 1;
                }
            }
        }

        //swaps items in heap, maintains indexes
        private void SwapItemsInHeap(int idx1, int idx2)
        {
            int temp = this[idx1];
            this[idx1] = this[idx2];
            this[idx2] = temp;
            _arrayIndexesToHeap[this[idx1]] = idx1;
            _arrayIndexesToHeap[this[idx2]] = idx2;
        }

        private bool IsFirstItemInHeapLower(int firstItemIdx, int secondItemIdx)
        {
            return this._rankCompareFunc(_data[this[firstItemIdx]], _data[this[secondItemIdx]]) < 0;
        }

        //maintains minheap property for all items below idx/2.
        private void MinHeapSortDown(int idx)
        {
            for (; idx <= this.ItemsNumInMinHeap; idx <<= 1)
            {
                if (idx > 1 && idx < this.ItemsNumInMinHeap && IsFirstItemInHeapLower(idx + 1, idx))
                {
                    ++idx;
                }

                if (IsFirstItemInHeapLower(idx, idx >> 1))
                {
                    SwapItemsInHeap(idx, idx >> 1);
                }
                else
                {
                    break;
                }
            }
        }

        //maintains maxheap property for all items below idx/2. (negative indexes)
        private void MaxHeapSortDown(int idx)
        {
            for (; idx >= -this.ItemsNumInMaxHeap; idx <<= 1)
            {
                if (idx < -1 && idx > -this.ItemsNumInMaxHeap && IsFirstItemInHeapLower(idx, idx - 1))
                {
                    --idx;
                }

                if (IsFirstItemInHeapLower((idx + 1) >> 1, idx))
                {
                    SwapItemsInHeap((idx + 1) >> 1, idx);
                }
                else
                {
                    break;
                }
            }
        }


        //maintains minheap property for all items above idx, including median
        //returns true if median changed
        bool MinHeapSortUp(int idx)
        {
            while (idx > 0 && IsFirstItemInHeapLower(idx, idx >> 1))
            {
                SwapItemsInHeap(idx, idx >> 1);
                idx >>= 1;
            }
            return (idx == 0);
        }

        //maintains maxheap property for all items above idx, including median
        //returns true if median changed
        bool MaxHeapSortUp(int idx)
        {
            while (idx < 0 && IsFirstItemInHeapLower((idx + 1) >> 1, idx))
            {
                SwapItemsInHeap((idx + 1) >> 1, idx);
                idx = (idx + 1) >> 1;
            }
            return (idx == 0);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int NegativeDivideByTwo(int i)
        {
            return (i + 1) >> 1;
        }
    }
}
