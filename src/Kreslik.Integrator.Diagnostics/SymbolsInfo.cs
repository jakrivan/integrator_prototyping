﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.Diagnostics
{

    public class SymbolsInfoProvider : BroadcastInfosStoreBase
    {
        private SymbolInfoBag[][] _tradingTargetsSymbolsInfosBag = null;
        private bool[] _supportedCurrenciesMap = null;
        private decimal[] _commisionsMap = null;
        private ILogger _logger;
        private IPriceHistoryProvider _priceHistoryProvider;
        private SymbolsInfoUtils _symbolsInfoUtils = new SymbolsInfoUtils();


        public SymbolsInfoProvider(ILogger logger, IPriceHistoryProvider priceHistoryProvider)
            :base(logger)
        {
            this._logger = logger;
            this._priceHistoryProvider = priceHistoryProvider;

            TradingHoursHelper.Instance.MarketRollingOver += this.RefreshSymbolsInfoFromBackend;
            RefreshSymbolsInfoFromBackend();
        }

        public ISymbolsInfo SymbolsInfo { get { return this._symbolsInfoUtils; }}

        private void RefreshSymbolsInfoFromBackend()
        {
            SymbolInfoBag[][] tradingTargetsSymbolsInfosBagTemp = null;
            bool[] supportedCurrenciesMapTemp = null;
            decimal[] commisionsMap = null;

            try
            {
                tradingTargetsSymbolsInfosBagTemp =
                this._priceHistoryProvider.GetSymbolsCounterpartyInfo(this._logger);

                var supportedCurrencies = this._priceHistoryProvider.GetSupportedCurrenciesList();
                commisionsMap = this._priceHistoryProvider.GetCounterpartyCommisionsPricePerMillionMap(this._logger);
                supportedCurrenciesMapTemp = Enumerable.Repeat(false, Currency.ValuesCount).ToArray();
                foreach (Currency supportedCurrency in supportedCurrencies)
                {
                    supportedCurrenciesMapTemp[(int)supportedCurrency] = true;
                }
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Unexpected exception during reading symbols info from backend");
            }

            if (tradingTargetsSymbolsInfosBagTemp != null && supportedCurrenciesMapTemp != null &&
                (!AreSame(_tradingTargetsSymbolsInfosBag, tradingTargetsSymbolsInfosBagTemp)
                ||
                !AreSame(_supportedCurrenciesMap, supportedCurrenciesMapTemp)
                ||
                !AreSame(_commisionsMap, commisionsMap)))
            {
                _tradingTargetsSymbolsInfosBag = tradingTargetsSymbolsInfosBagTemp;
                _supportedCurrenciesMap = supportedCurrenciesMapTemp;
                _commisionsMap = commisionsMap;
                this.OnNewBroadcastInfo(new SymbolsInfoArgs(tradingTargetsSymbolsInfosBagTemp, supportedCurrenciesMapTemp, commisionsMap));
                this._symbolsInfoUtils.InitializeState(new SymbolsInfoArgs(tradingTargetsSymbolsInfosBagTemp, supportedCurrenciesMapTemp, commisionsMap), this._logger);
            }
        }

        private bool AreSame(SymbolInfoBag[][] symbolsInfoBagA, SymbolInfoBag[][] symbolsInfoBagB)
        {
            if (symbolsInfoBagA == null || symbolsInfoBagB == null)
            {
                return false;
            }

            if (symbolsInfoBagA.Length != Counterparty.ValuesCount || symbolsInfoBagB.Length != Counterparty.ValuesCount)
            {
                return false;
            }

            for (int ctpIdx = 0; ctpIdx < Counterparty.ValuesCount; ctpIdx++)
            {
                if (symbolsInfoBagA[ctpIdx].Length != Symbol.ValuesCount ||
                    symbolsInfoBagB[ctpIdx].Length != Symbol.ValuesCount)
                {
                    return false;
                }

                for (int currIdx = 0; currIdx < Currency.ValuesCount; currIdx++)
                {
                    if (symbolsInfoBagA[ctpIdx][currIdx] != symbolsInfoBagB[ctpIdx][currIdx])
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private bool AreSame(bool[] mapA, bool[] mapB)
        {
            if (mapA == null || mapB == null)
            {
                return false;
            }

            if (mapA.Length != Currency.ValuesCount || mapB.Length != Currency.ValuesCount)
            {
                return false;
            }

            for (int ccyIdx = 0; ccyIdx < Currency.ValuesCount; ccyIdx++)
            {
                if (mapA[ccyIdx] != mapB[ccyIdx])
                    return false;
            }

            return true;
        }

        private bool AreSame(decimal[] mapA, decimal[] mapB)
        {
            if (mapA == null || mapB == null)
            {
                return false;
            }

            if (mapA.Length != Counterparty.ValuesCount || mapB.Length != Counterparty.ValuesCount)
            {
                return false;
            }

            for (int ctpIdx = 0; ctpIdx < Counterparty.ValuesCount; ctpIdx++)
            {
                if (mapA[ctpIdx] != mapB[ctpIdx])
                    return false;
            }

            return true;
        }
    }
}
