﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Diagnostics
{
    public class PriceStreamsMonitor : BroadcastInfosStoreBase, IPriceStreamMonitor, IPriceChangesConsumer
    {
        //low value used after quick market changes on 2014-12-16 (caused by RUB plummeting)
        private const int _MEDIAN_PRICE_HISTORY = 4; //32;
        private static readonly TimeSpan _refreshUsdConversionsInterval = TimeSpan.FromSeconds(60);
        private static readonly TimeSpan _refreshUsdConversionsFirstInterval = TimeSpan.FromMinutes(3);
        private static readonly TimeSpan _refreshBackendSymbolExchnageRatesInterval = TimeSpan.FromSeconds(2);
        private long _refreshPricesCnt = 0;

        private const int _SPREAD_MEAN_HISTORY_ITEMS = 32;
        private const int _SPREAD_MEAN_HISTORY_MS = 100;

        private PriceMedianTrackerUnsynchronized[][] _medianTrackers;
        private ILogger _logger;
        private IPriceHistoryProvider _priceHistoryProvider;
        private SafeTimer _refreshOnlinePricesTimer;
        private SafeTimer _refreshBackendPricesTimer;

        private decimal[] _symbolMidExchangeRate = new decimal[Symbol.ValuesCount];
        private DateTime[] _symbolMidExchangeRateUpdatedStamps = new DateTime[Symbol.ValuesCount];
        private DateTime _lastBackendUpdateOfExchangeRates;
        private decimal[] _usdConversionMultipliers;

        public bool ConsumesDataFromCounterparty(Counterparty counterparty)
        {
            return counterparty == Counterparty.L01;
        }

        public bool ConsumesMDDataIfOFNotRunning(Counterparty counterparty)
        {
            return counterparty.TradingTargetType == TradingTargetType.Hotspot;
        }

        public bool ConsumesIgnoredPrices
        {
            get { return false; }
        }
        public bool ConsumesNonExecutablePrices
        {
            get { return false; }
        }

        public decimal[] UsdConversionMultipliers
        {
            get { return _usdConversionMultipliers; }
        }

        public event Action<decimal[]> NewUsdConversionMultipliersAvailable;

        public IMedianProvider<decimal> GetMedianPriceProvider(Symbol symbol, PriceSide side)
        {
            return this._medianTrackers[(int) side][(int) symbol];
        }

        public IUsdConversionProvider CreateUsdConversionProvider(Currency currency)
        {
            return new UsdConversionProvider(currency, this);
        }

        public PriceStreamsMonitor(ILogger logger, IPriceHistoryProvider priceHistoryProvider, int medianPriceHistory)
            : base(logger)
        {
            this._logger = logger;
            this._priceHistoryProvider = priceHistoryProvider;

            _medianTrackers = new PriceMedianTrackerUnsynchronized[2][];
            _medianTrackers[0] = new PriceMedianTrackerUnsynchronized[Symbol.ValuesCount];
            _medianTrackers[1] = new PriceMedianTrackerUnsynchronized[Symbol.ValuesCount];
            for (int i = 0; i < Symbol.ValuesCount; i++)
            {
                _medianTrackers[0][i] = new PriceMedianTrackerUnsynchronized(medianPriceHistory, AtomicDecimal.Compare);
                _medianTrackers[1][i] = new PriceMedianTrackerUnsynchronized(medianPriceHistory, AtomicDecimal.Compare);
            }

            _priceRates[0] = new decimal[Symbol.ValuesCount];
            _priceRates[1] = new decimal[Symbol.ValuesCount];
            _priceRatesUpdateTimestamps[0] = new DateTime[Symbol.ValuesCount];
            _priceRatesUpdateTimestamps[1] = new DateTime[Symbol.ValuesCount];

            this.RefreshUsdMultipliers(this.GetOfflineRatesFromBackend(priceHistoryProvider));
            this.FillMedianTrackers(priceHistoryProvider, medianPriceHistory);

            this._refreshOnlinePricesTimer =
                new SafeTimer(() =>
                {
                    this.RefreshUsdMultipliers(this.GetOnlineRatesFromMedianTrackers());
                    //Broadcast to clients only on every 10th invoke - not to overwhlem them with too often updates
                    if (_refreshPricesCnt++%10 == 1)
                        //this also refreshes settings of trustworthy watchers
                        this.BroadcastNewSymbolExchangeRates();
                }, _refreshUsdConversionsFirstInterval, _refreshUsdConversionsInterval)
                {
                    RequiredTimerPrecision = TimerTaskFlagUtils.GetTimerPrecisionForDelay(_refreshUsdConversionsInterval),
                    TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
                };
        }

        public PriceStreamsMonitor(ILogger logger, IPriceHistoryProvider priceHistoryProvider)
            : this(logger, priceHistoryProvider, _MEDIAN_PRICE_HISTORY)
        {
        }

        private bool _ratesStoringEnabled = false;

        public void EnableStoringRatesToBackend()
        {
            this._ratesStoringEnabled = true;
            this._refreshBackendPricesTimer = new SafeTimer(this.RefreshBackendSymbolExchangeRates,
                _refreshBackendSymbolExchnageRatesInterval,
                _refreshBackendSymbolExchnageRatesInterval, true)
                {
                    RequiredTimerPrecision = TimerTaskFlagUtils.GetTimerPrecisionForDelay(_refreshBackendSymbolExchnageRatesInterval),
                    TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
                };
        }

        /// <summary>
        /// Warning: callers should make sure their L-x caches are synchronized!
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="side"></param>
        /// <param name="price"></param>
        /// <param name="ageOfOldestPrice"></param>
        /// <returns></returns>
        public bool TryGetOnlineMedianPrice(Symbol symbol, PriceSide side, out decimal price,
            out DateTime ageOfOldestPrice)
        {
            PriceMedianTrackerUnsynchronized medianTracker = this._medianTrackers[(int) side][(int) symbol];

            if (medianTracker.IsFull)
            {
                price = medianTracker.Median.ToDecimal();
                ageOfOldestPrice = medianTracker.OldestItemTimeStamp;
                return true;
            }

            price = 0m;
            ageOfOldestPrice = DateTime.MinValue;
            return false;
        }

        public bool GetHasFreshDataForSymbol(Symbol symbol, TimeSpan maxPriceAge)
        {
            DateTime maxAge = HighResolutionDateTime.UtcNow - maxPriceAge;

            return
                this._medianTrackers[0][(int) symbol].IsFull &&
                this._medianTrackers[0][(int) symbol].OldestItemTimeStamp > maxAge &&
                this._medianTrackers[1][(int) symbol].IsFull &&
                this._medianTrackers[1][(int) symbol].OldestItemTimeStamp > maxAge;
        }

        private decimal[] GetOfflineRatesFromBackend(IPriceHistoryProvider priceHistoryProvider)
        {
            foreach (
                Tuple<Symbol, decimal, DateTime> lastKnownReferencePrice in
                    priceHistoryProvider.GetLastKnownReferencePrices())
            {
                if (lastKnownReferencePrice.Item2 < 0m)
                {
                    string error = string.Format("Rate [{0}] for Symbol [{1}] appears to be invalid",
                        lastKnownReferencePrice.Item2, lastKnownReferencePrice.Item1);
                    this._logger.Log(LogLevel.Fatal, error);
                    throw new Exception(error);
                }

                _symbolMidExchangeRate[(int) lastKnownReferencePrice.Item1] = lastKnownReferencePrice.Item2;
                _symbolMidExchangeRateUpdatedStamps[(int) lastKnownReferencePrice.Item1] = lastKnownReferencePrice.Item3;
            }

            _lastBackendUpdateOfExchangeRates = DateTime.UtcNow;

            this.OnNewBroadcastInfo(new SymbolExchangeRatesArgs(this._symbolMidExchangeRate,
                _symbolMidExchangeRateUpdatedStamps));

            return this._symbolMidExchangeRate;
        }


        //update of decimal is not guaranteed to be atomical 
        // - we need to perform update in intermediate array and then swap references
        private decimal[] _tempSymbolMidExchangeRate = new decimal[Symbol.ValuesCount];
        private int _retrievingOnlineRates = 0;

        private decimal[] GetOnlineRatesFromMedianTrackers()
        {
            //just one thread can update at a time
            if (Interlocked.Exchange(ref _retrievingOnlineRates, 1) == 1)
                return _symbolMidExchangeRate;

            foreach (Symbol symbol in Symbol.Values)
            {
                decimal askRate;
                decimal bidRate;
                DateTime unused;
                if (this.TryGetOnlineMedianPrice(symbol, PriceSide.Ask, out askRate, out unused) && askRate > 0m &&
                    this.TryGetOnlineMedianPrice(symbol, PriceSide.Bid, out bidRate, out unused) && bidRate > 0m)
                {
                    decimal midRate = (askRate + bidRate)/2m;
                    //update only if different
                    if (midRate != _tempSymbolMidExchangeRate[(int) symbol])
                    {
                        _tempSymbolMidExchangeRate[(int) symbol] = midRate;
                        _symbolMidExchangeRateUpdatedStamps[(int) symbol] = DateTime.UtcNow;
                    }
                }
                //else
                //{
                //    this._logger.Log(LogLevel.Info, "Couldn't get online rate for {0}", symbol);
                //}
            }

            _tempSymbolMidExchangeRate = Interlocked.Exchange(ref this._symbolMidExchangeRate,
                _tempSymbolMidExchangeRate);

            //update is done
            Interlocked.Exchange(ref _retrievingOnlineRates, 0);

            return this._symbolMidExchangeRate;
        }

        private void BroadcastNewSymbolExchangeRates()
        {
            this.OnNewBroadcastInfo(new SymbolExchangeRatesArgs(this._symbolMidExchangeRate,
                _symbolMidExchangeRateUpdatedStamps));
        }

        private void FillMedianTrackers(IPriceHistoryProvider priceHistoryProvider, int historyDepth)
        {
            TaskEx.StartNew(()
                =>
            {
                bool experiencedTimeout = false;

                Parallel.ForEach(Symbol.Values, symbol =>
                {
                    this._logger.Log(LogLevel.Info, "Start filling prices history into price tracker for {0}",
                        symbol);

                    try
                    {
                        foreach (
                            Tuple<decimal, PriceSide, DateTime> tuple in
                                priceHistoryProvider.GetLastPricesForSymbol(symbol, historyDepth))
                        {
                            _medianTrackers[(int) tuple.Item2][(int) symbol].InsertItem(new AtomicDecimal(tuple.Item1),
                                tuple.Item3);
                        }

                        this._logger.Log(LogLevel.Info,
                            "Finished filling prices history into price tracker for {0}", symbol);
                    }
                    catch (SqlException e)
                    {
                        LogLevel level = LogLevel.Error;

                        //see http://forums.asp.net/t/1575883.aspx or
                        //SELECT * FROM master.dbo.sysmessages
                        if (e.Number == -2)
                        {
                            experiencedTimeout = true;
                            level = LogLevel.Info;
                        }

                        this._logger.LogException(level, e,
                            "Experienced SQL exception when filling price tracker for {0} from backend",
                            symbol);
                    }
                    catch (Exception e)
                    {
                        this._logger.LogException(LogLevel.Error, e,
                            "Experienced exception when filling price tracker for {0} from backend",
                            symbol);
                    }
                });

                this._logger.Log(LogLevel.Info, "Finished filling prices history into all price trackers");

                if (experiencedTimeout)
                {
                    this._logger.Log(LogLevel.Error,
                        "Experienced at least one timeout when filling price tracker from backend. Check Diag log for detailed info.");
                }
            });
        }

        private void RefreshUsdMultipliers(decimal[] symbolExchangeRate)
        {
            decimal[] tempArray = new decimal[Currency.ValuesCount];

            if (this._usdConversionMultipliers != null && this._usdConversionMultipliers.Length == Currency.ValuesCount)
            {
                this._usdConversionMultipliers.CopyTo(tempArray, 0);
            }

            tempArray[(int) Currency.USD] = 1m;

            foreach (
                Symbol symbol in
                    Symbol.Values.Where(s => s.TermCurrency == Currency.USD && symbolExchangeRate[(int) s] > 0m))
            {
                tempArray[(int) symbol.BaseCurrency] = symbolExchangeRate[(int) symbol];
            }

            foreach (
                Symbol symbol in
                    Symbol.Values.Where(s => s.BaseCurrency == Currency.USD && symbolExchangeRate[(int) s] > 0m))
            {
                tempArray[(int) symbol.TermCurrency] = 1m/symbolExchangeRate[(int) symbol];
            }

            var noDirectUsdConversionCurrencies =
                Symbol.UsedCurrencies
                    .Where(c => tempArray[(int) c] <= 0m)
                    .ToList();

            if (noDirectUsdConversionCurrencies.Count > 0)
            {
                string error =
                    "There is no direct conversion to/from USD for following currencies (used in Symbols): " +
                    string.Join(", ", noDirectUsdConversionCurrencies);
                this._logger.Log(LogLevel.Info, error);
            }

            Interlocked.Exchange(ref this._usdConversionMultipliers, tempArray);

            if (this.NewUsdConversionMultipliersAvailable != null)
            {
                this.NewUsdConversionMultipliersAvailable(this._usdConversionMultipliers);
            }
        }


        private decimal[][] _priceRates = new decimal[2][];
        private DateTime[][] _priceRatesUpdateTimestamps = new DateTime[2][];

        //this function needs to be called only from one thread and passed argument cannot bu used concurrently for reading
        private void GetOnlineRatesFromMedianTrackers(ref decimal[][] priceRates)
        {
            for (int symbolIdx = 0; symbolIdx < Symbol.ValuesCount; symbolIdx++)
            {
                if (_medianTrackers[(int) PriceSide.Ask][symbolIdx].IsFull)
                {
                    decimal askMedian = _medianTrackers[(int)PriceSide.Ask][symbolIdx].Median.ToDecimal();

                    if (askMedian != priceRates[(int)PriceSide.Ask][symbolIdx])
                    {
                        priceRates[(int)PriceSide.Ask][symbolIdx] = askMedian;
                        _priceRatesUpdateTimestamps[(int)PriceSide.Ask][symbolIdx] = DateTime.UtcNow;
                    }
                }


                if (_medianTrackers[(int)PriceSide.Bid][symbolIdx].IsFull)
                {
                    decimal bidMedian = _medianTrackers[(int)PriceSide.Bid][symbolIdx].Median.ToDecimal();

                    if (bidMedian != priceRates[(int)PriceSide.Bid][symbolIdx])
                    {
                        priceRates[(int)PriceSide.Bid][symbolIdx] = bidMedian;
                        _priceRatesUpdateTimestamps[(int)PriceSide.Bid][symbolIdx] = DateTime.UtcNow;
                    }
                }
            }
        }

        private void RefreshBackendSymbolExchangeRates()
        {
            if (!this._ratesStoringEnabled)
                return;


            this.GetOnlineRatesFromMedianTrackers(ref this._priceRates);

            StringBuilder askPricesArgBuilder = new StringBuilder();
            StringBuilder bidPricesArgBuilder = new StringBuilder();
            for (int symbolIdx = 0; symbolIdx < Symbol.ValuesCount; symbolIdx++)
            {
                Symbol symbol = (Symbol)symbolIdx;

                if (_priceRatesUpdateTimestamps[(int)PriceSide.Ask][symbolIdx] > _lastBackendUpdateOfExchangeRates)
                {
                    askPricesArgBuilder.AppendFormat(CultureInfo.InvariantCulture, "{0}|{1}|",
                                                     symbol.ShortName, this._priceRates[(int)PriceSide.Ask][symbolIdx]);
                }

                if (_priceRatesUpdateTimestamps[(int)PriceSide.Bid][symbolIdx] > _lastBackendUpdateOfExchangeRates)
                {
                    bidPricesArgBuilder.AppendFormat(CultureInfo.InvariantCulture, "{0}|{1}|",
                                                     symbol.ShortName, this._priceRates[(int)PriceSide.Bid][symbolIdx]);
                }
            }


            if (askPricesArgBuilder.Length > 0 || bidPricesArgBuilder.Length > 0)
            {
                try
                {
                    this._priceHistoryProvider.UpdateReferencePrices(askPricesArgBuilder.ToString(),
                                                                        bidPricesArgBuilder.ToString());
                }
                catch (Exception e)
                {
                    AdoNetHelpers.LogProcedureException(this._logger, e, "RefreshingBackendToBPrices");
                }
            }

            this._lastBackendUpdateOfExchangeRates = DateTime.UtcNow;
        }

        public void RefreshBackendValues()
        {
            this.RefreshBackendSymbolExchangeRates();
        }

        private static Counterparty _referenceCounterparty = Counterparty.L01;
        public void OnNewPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if (PriceObjectInternal.IsNullPrice(price) || price.Counterparty != _referenceCounterparty)
                return;

            //unsynchronized update
            this._medianTrackers[(int) price.Side][(int) price.Symbol].InsertItem(price.ConvertedPrice,
                price.IntegratorReceivedTimeUtc);
        }

        public void OnIgnoredPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if (price == null)
                return;

            if (this.ConsumesIgnoredPrices)
                this.OnNewPrice(price, ref heldWithoutAcquireCall);
            else
                this.OnPriceCancel(price.Symbol, price.Counterparty, price.Side);
        }

        public void OnNewNonExecutablePrice(PriceObjectInternal price)
        {
            if (price == null)
                return;

            bool _ = true;
            if (this.ConsumesIgnoredPrices)
                this.OnNewPrice(price, ref _);
            else
                this.OnPriceCancel(price.Symbol, price.Counterparty, price.Side);
        }

        //when ctp cancelling, but also if it disconnects
        public void OnQuoteCancel(Symbol symbol, Counterparty counterparty)
        {
            //nothing
        }

        //counterparty canceling sided price
        public void OnPriceCancel(Symbol symbol, Counterparty counterparty, PriceSide side)
        {
            //nothing
        }

        //rejected or broken order
        public void OnPriceInvalidate(Symbol symbol, PriceSide side, Counterparty counterparty, Guid integratorPriceIdnetity, DateTime integratorPriceReceivedUtc)
        {
            //Do nothing - we do not know if quote was already replaced or not
            //also trustworthy watchers do not handle this as almost each rejection would lead to untrustworthiness
        }


        private class UsdConversionProvider : IUsdConversionProvider
        {
            private Currency _currency;
            private IPriceStreamMonitor _priceStreamMonitor;

            public UsdConversionProvider(Currency currency, IPriceStreamMonitor priceStreamMonitor)
            {
                this._currency = currency;
                this._priceStreamMonitor = priceStreamMonitor;
            }

            public decimal GetCurrentUsdConversionMultiplier(Currency currency)
            {
                return this._priceStreamMonitor.UsdConversionMultipliers[(int)currency];
            }

            public decimal GetCurrentUsdConversionMultiplier()
            {
                return this.GetCurrentUsdConversionMultiplier(this._currency);
            }
        }

    }

    

    public class PriceStreamsInfoProvider : IPriceStreamsInfoProvider, IPriceChangesConsumer
    {
        QuteInfoEntry[][] _entries = new QuteInfoEntry[Counterparty.ValuesCount][];
        List<PriceStreamInfoEntry> _priceStreamInfoEntries = new List<PriceStreamInfoEntry>();
        private IStreamingSubscriptionInfoProvider _streamingSubscriptionInfoProvider;
        private const int _RECALCULATING_IN_PROGRESS = 1;
        private const int _IDLE = 0;
        private int _recalculatingStatsState = _IDLE;

        public PriceStreamsInfoProvider(IEnumerable<IStreamingSubscriptionInfoProvider> streamingSubscriptionInfoProviders)
        {
            for (int i = 0; i < Counterparty.ValuesCount; i++)
            {
                _entries[i] = Enumerable.Range(0, Symbol.ValuesCount).Select(dummy => new QuteInfoEntry()).ToArray();
            }

            for (int i = 0; i < Counterparty.ValuesCount; i++)
            {
                for (int j = 0; j < Symbol.ValuesCount; j++)
                {
                    _priceStreamInfoEntries.Add(new WritablePriceStreamInfoEntry((Symbol)j, (Counterparty)i));
                }
            }

            if (streamingSubscriptionInfoProviders != null)
            {
                foreach (IStreamingSubscriptionInfoProvider streamingSubscriptionInfoProvider in streamingSubscriptionInfoProviders)
                {
                    streamingSubscriptionInfoProvider.RemoteSubscriptionChanged += (counterparty, symbol, subscribed) =>
                    {
                        _entries[(int)counterparty][(int)symbol].LastAskTimeUtc = subscribed
                            ? DateTime.MaxValue
                            : DateTime.MinValue;
                        _entries[(int)counterparty][(int)symbol].LastBidTimeUtc = subscribed
                            ? DateTime.MaxValue
                            : DateTime.MinValue;
                    };
                }
            }
        }

        public void OnNewPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if(price == null)
                return;

            if (PriceObjectInternal.IsNullPrice(price))
            {
                this.OnPriceCancel(price.Symbol, price.Counterparty, price.Side);
                return;
            }

            if (price.Side == PriceSide.Ask)
            {
                _entries[(int) price.Counterparty][(int) price.Symbol].LastAskTimeUtc = price.IntegratorReceivedTimeUtc;
                _entries[(int) price.Counterparty][(int) price.Symbol].LastAskPriceConverted = price.ConvertedPrice;
            }
            else
            {
                _entries[(int) price.Counterparty][(int) price.Symbol].LastBidTimeUtc = price.IntegratorReceivedTimeUtc;
                _entries[(int) price.Counterparty][(int) price.Symbol].LastBidPriceConverted = price.ConvertedPrice;
            }
        }

        public void OnIgnoredPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if (price == null)
                return;

            if (this.ConsumesIgnoredPrices)
                this.OnNewPrice(price, ref heldWithoutAcquireCall);
            else
                this.OnPriceCancel(price.Symbol, price.Counterparty, price.Side);
        }

        public void OnNewNonExecutablePrice(PriceObjectInternal price)
        {
            if (price == null)
                return;

            bool _ = true;
            if (this.ConsumesIgnoredPrices)
                this.OnNewPrice(price, ref _);
            else
                this.OnPriceCancel(price.Symbol, price.Counterparty, price.Side);
        }

        public void OnQuoteCancel(Symbol symbol, Counterparty counterparty)
        {
            _entries[(int)counterparty][(int)symbol].LastBidPriceConverted = 0;
            _entries[(int)counterparty][(int)symbol].LastAskPriceConverted = 0;

            _entries[(int)counterparty][(int)symbol].LastAskTimeUtc = DateTime.MinValue;
            _entries[(int)counterparty][(int)symbol].LastBidTimeUtc = DateTime.MinValue;
        }

        public void OnPriceCancel(Symbol symbol, Counterparty counterparty, PriceSide side)
        {
            if(side == PriceSide.Ask)
                _entries[(int)counterparty][(int)symbol].LastAskPriceConverted = 0;
            else
                _entries[(int)counterparty][(int)symbol].LastBidPriceConverted = 0;
        }

        public void OnPriceInvalidate(Symbol symbol, PriceSide side, Counterparty counterparty, Guid integratorPriceIdnetity, DateTime integratorPriceReceivedUtc)
        { /* Nothing */ }

        public bool ConsumesDataFromCounterparty(Counterparty counterparty)
        {
            return true;
        }

        public bool ConsumesMDDataIfOFNotRunning(Counterparty counterparty)
        {
            return counterparty.TradingTargetCategory == TradingTargetCategory.Venue;
        }

        public bool ConsumesIgnoredPrices
        {
            get { return true; }
        }
        public bool ConsumesNonExecutablePrices
        {
            get { return false; }
        }

        public List<PriceStreamInfoEntry> GetPriceStreamInfoEntries()
        {
            this.TryPopulatePriceStreamInfoEntriesIfNeeded();
            return this._priceStreamInfoEntries;
        }

        private void TryPopulatePriceStreamInfoEntriesIfNeeded()
        {
            if (InterlockedEx.CheckAndFlipState(ref this._recalculatingStatsState, _RECALCULATING_IN_PROGRESS, _IDLE))
            {
                try
                {
                    this.PopulatePriceStreamInfoEntries();
                }
                catch (Exception e)
                {
                    LogFactory.Instance.GetLogger(null).LogException(LogLevel.Fatal, "Unexpected error during calculationg stats for C-Monitor", e);
                }

                Interlocked.Exchange(ref this._recalculatingStatsState, _IDLE);
            }
        }

        private void PopulatePriceStreamInfoEntries()
        {
            DateTime utcNow = DateTime.UtcNow;

            foreach (WritablePriceStreamInfoEntry priceStreamInfoEntry in _priceStreamInfoEntries)
            {
                QuteInfoEntry entry =
                    _entries[(int)priceStreamInfoEntry.Counterparty][(int)priceStreamInfoEntry.Symbol];
                if (entry == null)
                {
                    priceStreamInfoEntry.ResetEntry();
                }
                else
                {
                    AtomicDecimal askPrice = entry.LastAskPriceConverted;
                    AtomicDecimal bidPrice = entry.LastBidPriceConverted;
                    if (bidPrice != AtomicDecimal.Zero && askPrice != AtomicDecimal.Zero)
                    {
                        priceStreamInfoEntry.SetSpreadBp(CalculateSpreadBp(askPrice.ToDecimal(), bidPrice.ToDecimal()));
                    }
                    else
                    {
                        priceStreamInfoEntry.ResetSpreadBp();
                    }
                    priceStreamInfoEntry.SetLastAskAge(utcNow - entry.LastAskTimeUtc);
                    priceStreamInfoEntry.SetLastBidAge(utcNow - entry.LastBidTimeUtc);
                }
            }
        }

        private decimal CalculateSpreadBp(decimal askPrice, decimal bidPrice)
        {
            return 10000 * (askPrice - bidPrice) / ((askPrice + bidPrice) / 2);
        }

        private class QuteInfoEntry
        {
            public DateTime LastBidTimeUtc { get; set; }
            public DateTime LastAskTimeUtc { get; set; }
            public AtomicDecimal LastBidPriceConverted { get; set; }
            public AtomicDecimal LastAskPriceConverted { get; set; }
        }
    }
}
