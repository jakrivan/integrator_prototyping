﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.Diagnostics
{
    public static class DiagnosticsManager
    {
        public static ILogger DiagLogger { get; private set; }

        public static ISettlementDatesKeeper GetSettlementDatesKeeper(Counterparty counterparty)
        {
            if (counterparty.TradingTargetType == TradingTargetType.FXCMMM)
                return _fastMatchSettlementDatesKeeper;
            else if (counterparty.TradingTargetType == TradingTargetType.Hotspot || counterparty == Counterparty.FL1)
                return _dummySettlementDatesKeeper;
            else
                throw new ArgumentOutOfRangeException("counterparty",
                    "Unsupported counterparty for GetSettlementDatesKeeper");
        }

        private static ISettlementDatesKeeper _fastMatchSettlementDatesKeeper;
        private static ISettlementDatesKeeper _dummySettlementDatesKeeper;

        static DiagnosticsManager()
        {
            DiagLogger = (LogFactory.Instance).GetLogger("Diag");
            _fastMatchSettlementDatesKeeper = new SettlementDatesKeeper(new SettlementDatesInfoPersistor(), DiagLogger);
            _dummySettlementDatesKeeper = new DummySettlmentDatesKeeper();
        }

        public static PriceStreamsMonitor CreatePriceStreamMonitor()
        {
            return new PriceStreamsMonitor(DiagLogger, new PriceHistoryProvider());
        }

    }
}
