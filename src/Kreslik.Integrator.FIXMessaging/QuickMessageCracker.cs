﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.Fields;

namespace Kreslik.Integrator.FIXMessaging
{

    //Need tristate - parsed OK / not known / Stop parsing
    // Or use exceptions to stop parsing and another exception type for fatal errors
    public delegate void TagParserDelegate(BufferSegmentBase bufferSegment);

    internal class QuickMessageCracker
    {
        private ParsedBufferSegment _sharedParsedBufferSegment = new ParsedBufferSegment(null, 0, 0);
        private ParsedBufferSegment _sharedBodyBufferSegment = new ParsedBufferSegment(null, 0, 0);
        private IFixMessageCracker[] _fixMessageCrackers;
        private byte[] _recognizedMessages;
        private ILogger _logger;
        private bool _isMdSession;

        internal QuickMessageCracker(ILogger logger, bool isMdSession, params IFixMessageCracker[] fixMessageCrackers)
        {
            this._logger = logger;
            this._isMdSession = isMdSession;
            if (fixMessageCrackers.Length == 0)
                throw new Exception("No message crackers supplied");

            this._fixMessageCrackers = fixMessageCrackers;
            this._recognizedMessages = fixMessageCrackers.Select(f => f.RecognizedMessageType).ToArray();
        }

        internal QuickMessageCracker(ILogger logger, bool isMdSession, IFixMessageCracker fixMessageCracker, byte[] recognizedMessagetypes)
        {
            this._logger = logger;
            this._isMdSession = isMdSession;

            this._recognizedMessages = recognizedMessagetypes;
            this._fixMessageCrackers = Enumerable.Repeat(fixMessageCracker, recognizedMessagetypes.Length).ToArray();
        }

        internal void OverrideCommonMessageParser(Kreslik.Integrator.Contracts.Internal.IFixMessageCracker parser)
        {
            bool found = false;
            for (int idx = 0; idx < this._recognizedMessages.Length; idx++)
            {
                if (this._recognizedMessages[idx] == parser.RecognizedMessageType)
                {
                    _fixMessageCrackers[idx] = parser;
                    found = true;
                    break;
                }
            }

            if (found)
            {
                this._logger.Log(LogLevel.Trace,
                    "Replaced quick parser for message type: " + parser.RecognizedMessageType);
            }
            else
            {
                byte[] recognizedMessagesTemp = new byte[this._recognizedMessages.Length + 1];
                Array.Copy(this._recognizedMessages, recognizedMessagesTemp, this._recognizedMessages.Length);
                recognizedMessagesTemp[this._recognizedMessages.Length] = parser.RecognizedMessageType;
                this._recognizedMessages = recognizedMessagesTemp;

                IFixMessageCracker[] fixMessageCrackersTemp = new IFixMessageCracker[this._fixMessageCrackers.Length + 1];
                Array.Copy(this._fixMessageCrackers, fixMessageCrackersTemp, this._fixMessageCrackers.Length);
                fixMessageCrackersTemp[this._fixMessageCrackers.Length] = parser;
                this._fixMessageCrackers = fixMessageCrackersTemp;

                this._logger.Log(LogLevel.Trace,
                    "Added quick parser for message type: " + parser.RecognizedMessageType);
            }
        }


        IFixMessageCracker GetMessageCracker(byte messageType)
        {
            IFixMessageCracker fixMessageCracker = null;

            for (int idx = 0; idx < this._recognizedMessages.Length; idx++)
            {
                if (this._recognizedMessages[idx] == messageType)
                {
                    fixMessageCracker = _fixMessageCrackers[idx];
                    break;
                }
            }

            return fixMessageCracker;
        }

        internal bool ParseMessage(QuickFixSubsegment bufferSegment)
        {
            DateTime receivedTime = HighResolutionDateTime.UtcNow;

            //here announce to upper layer
            if (this._logger.IsEnabled(LogLevel.Debug) && !(this._isMdSession && this._logger.IsLowDiskSpaceConstraintLoggingOn))
            {
                this._logger.Log(LogLevel.Debug, bufferSegment);
            }

            bufferSegment.GetBodyBufferSegment(ref _sharedBodyBufferSegment);

            //35=
            if (_sharedBodyBufferSegment[0] != BufferSegmentUtils.THREE ||
                _sharedBodyBufferSegment[1] != BufferSegmentUtils.FIVE ||
                _sharedBodyBufferSegment[2] != BufferSegmentUtils.VALS_SEPARATOR)
                throw new Exception("Corrupted message [Unexpected chars isntead of message type]");

            byte messageType = _sharedBodyBufferSegment[3];

            IFixMessageCracker fixMessageCracker = null;
            //Not supporting message types defined by multichars - e.g.: U1, U2 ...
            if (_sharedBodyBufferSegment[4] == BufferSegmentUtils.TAGS_SEPARATOR)
            {
                fixMessageCracker = GetMessageCracker(messageType);
            }

            if (fixMessageCracker == null)
                return false;

            //After this point we know cracker - we shouldn't fallback to old style parsing

            fixMessageCracker.Reset(receivedTime, bufferSegment);

            int errorsCnt = 0;
            for (int idx = 0; idx < _sharedBodyBufferSegment.ContentSize; idx++)
            {

                int tagId = _sharedBodyBufferSegment.ToInt(ref idx);
                if (idx == _sharedBodyBufferSegment.ContentSize ||
                    _sharedBodyBufferSegment[idx] != BufferSegmentUtils.VALS_SEPARATOR)
                    throw new MessageParsingException("Corrupted message [02]");

                int valStartIdx = idx + 1;

                if (!_sharedBodyBufferSegment.FindByte(BufferSegmentUtils.TAGS_SEPARATOR, ref idx))
                    throw new MessageParsingException("Corrupted message [03]");

                _sharedParsedBufferSegment.OverrideContent(_sharedBodyBufferSegment, valStartIdx, idx - valStartIdx);


                try
                {
                    fixMessageCracker.ConsumeFixTag(tagId, _sharedParsedBufferSegment);
                }
                catch (NonHaltingTagException e)
                {
                    this._logger.LogException(LogLevel.Error, e,
                        "Unexpected, but continuable error while parsing message: " +
                        bufferSegment.GetContentAsAsciiString());
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e,
                        string.Format("Unexpected error while parsing message: [{0}] {1} trying to continue",
                            bufferSegment.GetContentAsAsciiString(),
                            fixMessageCracker.TryRecoverFromParsingErrors ? string.Empty : " NOT"));
                    if (!fixMessageCracker.TryRecoverFromParsingErrors || errorsCnt++ > 10)
                        throw;
                }

            }

            fixMessageCracker.MessageParsingDone();


            return true;
        }
    }



    internal class VenueCancelRejectCracker : IFixMessageCracker
    {
        private IOrdersParentChannel _parentChannel;
        private ILogger _logger;
        private DateTime _currentMessageReceived;

        public VenueCancelRejectCracker(IOrdersParentChannel parentChannel, ILogger logger)
        {
            this._parentChannel = parentChannel;
            this._logger = logger;
        }

        private const byte _recognizedType = (byte)'9';

        public byte RecognizedMessageType
        {
            get { return _recognizedType; }
        }

        public bool TryRecoverFromParsingErrors
        {
            get { return false; }
        }

        public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
        {
            switch (tagId)
            {
                case 41:
                    OnTag41(tagValueSegment);
                    break;
                case 11:
                    OnTag11(tagValueSegment);
                    break;
                case 102:
                    OnTag102(tagValueSegment);
                    break;
                case 58:
                    OnTag58(tagValueSegment);
                    break;
                case 55:
                    OnTag55(tagValueSegment);
                    break;
                case 52:
                    OnTag52(tagValueSegment);
                    break;
                case 122:
                    OnTag122(tagValueSegment);
                    break;
            }
        }

        private string _cancelOperationId;
        private string _orderId;
        private string _freeTextInfo;
        //private int _cxlRejReasonId;
        private DateTime _sendingTime = DateTime.MinValue;
        private DateTime _origSendingTime = DateTime.MinValue;
        private QuickFixSubsegment _currentMessage;
        private Common.Symbol _symbol;

        public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
        {
            this._currentMessageReceived = currentMessageReceivedTimeUtc;
            this._currentMessage = currentMessage;
            this._cancelOperationId = null;
            this._orderId = null;
            this._freeTextInfo = null;
            this._sendingTime = DateTime.MinValue;
            this._origSendingTime = DateTime.MinValue;
            this._symbol = Common.Symbol.NULL;
        }

        public void MessageParsingDone()
        {
            if (this._orderId == null || this._cancelOperationId == null)
                throw new HaltingTagException("Incomplete message - missing tag", this._orderId == null ? 41 : 11);

            if (this._parentChannel.IsDivertedViaFluent)
            {
                if (this._origSendingTime == DateTime.MinValue)
                    this._logger.Log(LogLevel.Info,
                        "Session diverted via fluent but OrigSendingTime tag (122) is missing or invalid");
                else
                    this._sendingTime = this._origSendingTime;
            }

            this._logger.Log(LogLevel.Info, "Request [{0}] to cancell order [{1}] was REJECTED (\"{2}\")", this._cancelOperationId, this._orderId, this._freeTextInfo);

            this._parentChannel.SendOrderChange(new OrderChangeInfo(this._orderId, this._symbol, IntegratorOrderExternalChange.CancelRejected, this._sendingTime, this._currentMessage,
                                                     null, null, this._freeTextInfo));

            //inform upperlayers about rejection of a cancel request (or cancel replace request)
            //this is the place where we flip Cancel/Replace reject into regular reject - so indicate this type of reject
            this._parentChannel.SendOrderChange(new OrderChangeInfo(this._cancelOperationId, this._symbol, IntegratorOrderExternalChange.Rejected,
                                                     this._sendingTime, this._currentMessage, null,
                                                     new RejectionInfo(this._freeTextInfo, null, RejectionType.CancelReplaceReject), this._currentMessageReceived));
        }

        //OrigClOrdID = 41
        private void OnTag41(BufferSegmentBase bufferSegment)
        {
            this._orderId = bufferSegment.GetContentAsAsciiString();
        }

        //ClOrdID = 11
        private void OnTag11(BufferSegmentBase bufferSegment)
        {
            this._cancelOperationId = bufferSegment.GetContentAsAsciiString();
        }

        //CxlRejReason = 102
        private void OnTag102(BufferSegmentBase bufferSegment)
        {
            int cxlRejReasonId = bufferSegment.ToInt();

            this._freeTextInfo = string.Format("Reason code: {0} [{1}]. Description: {2}", cxlRejReasonId,
                    this.MapCxlRejReasonId(cxlRejReasonId), this._freeTextInfo);
        }

        //Text = 58
        private void OnTag58(BufferSegmentBase bufferSegment)
        {
            if (string.IsNullOrEmpty(this._freeTextInfo))
                this._freeTextInfo = bufferSegment.GetContentAsAsciiString();
            else
                this._freeTextInfo += bufferSegment.GetContentAsAsciiString();
        }

        //Symbol = 55
        private void OnTag55(BufferSegmentBase bufferSegment)
        {
            //Hotspot can send 'unknown' on cancel reject after throttling
            bufferSegment.TryConvertToSymbol(out this._symbol);
        }

        //OrigSendingTime
        private void OnTag122(BufferSegmentBase bufferSegment)
        {
            this._origSendingTime = bufferSegment.ToDateTime();
        }


        private void OnTag52(BufferSegmentBase bufferSegment)
        {
            try
            {
                this._sendingTime = bufferSegment.ToDateTime();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Error, e, "Couldn't convert value {0} to datetime",
                    bufferSegment.GetContentAsAsciiString());
            }
        }

        private string MapCxlRejReasonId(int cxlRejReasonId)
        {
            //public const int TOO_LATE_TO_CANCEL = 0;
            //public const int UNKNOWN_ORDER = 1;
            //public const int BROKER = 2;
            //public const int ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS = 3;
            //public const int UNABLE_TO_PROCESS_ORDER_MASS_CANCEL_REQUEST = 4;
            //public const int ORIGORDMODTIME = 5;
            //public const int DUPLICATE_CLORDID = 6;
            //public const int OTHER = 99;

            switch (cxlRejReasonId)
            {
                case 0:
                    return "Too Late";
                case 1:
                    return "Unknown Order";
                case 2:
                    return "Broker/Exchange Option";
                case 3:
                    return "Order Already in Pending Cancel or Pending Replace Status";
                case 6:
                    return "Duplicate ClOrdID Received";
                case 99:
                    return "Other";
                default:
                    return string.Format("<Unknown CxlRejReasonId!: {0}>", cxlRejReasonId);
            }
        }
    }


    internal abstract class BankExecReportCracker : IFixMessageCracker
    {
        private IOrdersParentChannel _parentChannel;
        protected ILogger _logger;
        private DateTime _currentMessageReceived;

        protected BankExecReportCracker(IOrdersParentChannel parentChannel, ILogger logger)
        {
            this._parentChannel = parentChannel;
            this._logger = logger;
        }

        private const byte _recognizedType = (byte)'8';

        public byte RecognizedMessageType
        {
            get { return _recognizedType; }
        }

        public bool TryRecoverFromParsingErrors
        {
            get { return true; }
        }

        protected virtual void ConsumeNonstandardFixTag(int tagId, BufferSegmentBase tagValueSegment) { }

        public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
        {
            switch (tagId)
            {
                case 52:
                    OnTag52(tagValueSegment);
                    break;
                case 58:
                    OnTag58(tagValueSegment);
                    break;
                case 55:
                    OnTag55(tagValueSegment);
                    break;
                case 38:
                    OnTag38(tagValueSegment);
                    break;
                case 39:
                    OnTag39(tagValueSegment);
                    break;
                case 54:
                    OnTag54(tagValueSegment);
                    break;
                case 11:
                    OnTag11(tagValueSegment);
                    break;
                case 31:
                    OnTag31(tagValueSegment);
                    break;
                case 44:
                    OnTag44(tagValueSegment);
                    break;
                case 32:
                    OnTag32(tagValueSegment);
                    break;
                case 17:
                    OnTag17(tagValueSegment);
                    break;
                case 60:
                    OnTag60(tagValueSegment);
                    break;
                case 64:
                    OnTag64(tagValueSegment);
                    break;
                case 122:
                    OnTag122(tagValueSegment);
                    break;
                default:
                    this.ConsumeNonstandardFixTag(tagId, tagValueSegment);
                    break;
            }
        }

        private decimal? _lastQty;
        private decimal? _orderQty;
        private decimal? _lastPx;
        private decimal? _price;
        private string _execId;
        private DateTime? _transactTime;
        private DateTime? _settlDate;
        private char _fixOrderStatus;
        private string _orderId;
        private string _freeTextInfo;
        private DateTime _sendingTime = DateTime.MinValue;
        private DateTime _origSendingTime = DateTime.MinValue;
        private Common.Symbol _symbol;
        private QuickFixSubsegment _currentMessage;
        private char _dealDirection;

        public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
        {
            this._currentMessageReceived = currentMessageReceivedTimeUtc;
            this._currentMessage = currentMessage;

            this._lastQty = null;
            this._orderQty = null;
            this._lastPx = null;
            this._price = null;
            this._execId = null;
            this._transactTime = null;
            this._settlDate = null;
            this._fixOrderStatus = char.MaxValue;
            this._orderId = null;
            this._freeTextInfo = null;
            this._sendingTime = DateTime.MinValue;
            this._origSendingTime = DateTime.MinValue;
            this._symbol = Common.Symbol.NULL;
            this._dealDirection = char.MaxValue;

            //this.ResetState();
        }

        //protected virtual void ResetState() { }

        private IntegratorOrderExternalChange OnExecReportNew()
        {
            this._logger.Log(LogLevel.Info, "Order [{0}] was received and enqeued by other end", this._orderId);
            return IntegratorOrderExternalChange.ConfirmedNew;
        }

        private IntegratorOrderExternalChange OnExecReportRejected(out RejectionInfo rejectionInfo)
        {
            this._logger.Log(LogLevel.Info, "Order [{0}] REJECTED", this._orderId);
            rejectionInfo = new RejectionInfo(this._freeTextInfo, null, RejectionType.OrderReject);
            return IntegratorOrderExternalChange.Rejected;
        }

        private DealDirection GetDealDirection()
        {
            switch (this._dealDirection)
            {
                case '1':
                    return DealDirection.Buy;
                case '2':
                    return DealDirection.Sell;
                default:
                    throw new HaltingTagException(
                        string.Format("Unrecognized value for tag 54 (Side): [{0}]. Expected 1 or 2",
                            this._dealDirection), 54);
            }
        }

        private IntegratorOrderExternalChange OnExecReportTrade(out ExecutionInfo executionInfo, bool partiall)
        {
            this._logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", this._orderId);

            executionInfo = new ExecutionInfo(
                this._lastQty.HasValue ? this._lastQty : this._orderQty,
                this._lastPx.HasValue ? this._lastPx : this._price,
                this._execId,
                this._transactTime,
                this._settlDate,
                GetDealDirection());

            return partiall ? IntegratorOrderExternalChange.PartiallyFilled : IntegratorOrderExternalChange.Filled;
        }

        private IntegratorOrderExternalChange OnExecReportUnknown()
        {
            this._logger.Log(LogLevel.Fatal, "Order [{0}] received unsupported OrderType report: [{1}]",
                        this._orderId, this._fixOrderStatus);
            return IntegratorOrderExternalChange.InBrokenState;
        }


        protected enum OrderStatus
        {
            New,
            Trade,
            PartiallTrade,
            Reject,
            ToBeIgnored,
            Unrecognized
        }

        protected abstract OrderStatus GetTranslatedOrderStatus(char externalOrderStatus);

        //protected virtual void AfterTradeReportedToIntegrator(QuickFixSubsegment currentMessage) { }

        public void MessageParsingDone()
        {
            int missingTag = 0;

            if (this._orderId == null)
                missingTag = 11;
            if (this._fixOrderStatus == char.MaxValue)
                missingTag = 39;
            if (this._dealDirection == char.MaxValue)
                missingTag = 54;

            if (missingTag != 0)
            {
                this._logger.Log(LogLevel.Fatal,
                    "Receiving (and rejecting!) incomplete execution report");
                throw new HaltingTagException("Incomplete message - missing tag", missingTag);
            }

            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;

            if (this._parentChannel.IsDivertedViaFluent && this._fixOrderStatus != OrdStatus.PENDING_NEW)
            {
                if (this._origSendingTime == DateTime.MinValue)
                    this._logger.Log(LogLevel.Info,
                        "Session diverted via fluent but OrigSendingTime tag (122) is missing or invalid");
                else
                    this._sendingTime = this._origSendingTime;
            }

            switch (this.GetTranslatedOrderStatus(this._fixOrderStatus))
            {
                case OrderStatus.New:
                    orderStatus = this.OnExecReportNew();
                    break;
                case OrderStatus.Trade:
                    orderStatus = this.OnExecReportTrade(out executionInfo, false);
                    break;
                case OrderStatus.PartiallTrade:
                    orderStatus = this.OnExecReportTrade(out executionInfo, true);
                    break;
                case OrderStatus.Reject:
                    orderStatus = this.OnExecReportRejected(out rejectionInfo);
                    break;
                case OrderStatus.ToBeIgnored:
                    return;
                case OrderStatus.Unrecognized:
                default:
                    orderStatus = this.OnExecReportUnknown();
                    break;
            }

            this._parentChannel.SendOrderChange(new OrderChangeInfo(this._orderId, this._symbol, orderStatus, this._sendingTime,
                    this._currentMessage, executionInfo, rejectionInfo, this._currentMessageReceived));
        }

        //Text = 58
        private void OnTag58(BufferSegmentBase bufferSegment)
        {
            this._freeTextInfo = bufferSegment.GetContentAsAsciiString();
        }

        //Symbol = 55
        private void OnTag55(BufferSegmentBase bufferSegment)
        {
            this._symbol = bufferSegment.ToSymbol();
        }

        //OrdStatus = 39
        private void OnTag39(BufferSegmentBase bufferSegment)
        {
            this._fixOrderStatus = (char)bufferSegment[0];
        }

        //Side = 54
        private void OnTag54(BufferSegmentBase bufferSegment)
        {
            this._dealDirection = (char)bufferSegment[0];
        }

        //ClOrdID = 11
        private void OnTag11(BufferSegmentBase bufferSegment)
        {
            this._orderId = bufferSegment.GetContentAsAsciiString();
        }

        //LastQty = 32
        private void OnTag32(BufferSegmentBase bufferSegment)
        {
            this._lastQty = bufferSegment.ToDecimal();
        }

        //OrderQty = 38
        private void OnTag38(BufferSegmentBase bufferSegment)
        {
            this._orderQty = bufferSegment.ToDecimal();
        }

        //LastPx = 31
        private void OnTag31(BufferSegmentBase bufferSegment)
        {
            this._lastPx = bufferSegment.ToDecimal();
        }

        //Price = 44
        private void OnTag44(BufferSegmentBase bufferSegment)
        {
            this._price = bufferSegment.ToDecimal();
        }

        //ExecID = 17
        private void OnTag17(BufferSegmentBase bufferSegment)
        {
            this._execId = bufferSegment.GetContentAsAsciiString();
        }

        //TransactTime = 60
        private void OnTag60(BufferSegmentBase bufferSegment)
        {
            this._transactTime = bufferSegment.ToDateTime();
        }

        //SettlDate = 64
        private void OnTag64(BufferSegmentBase bufferSegment)
        {
            bufferSegment.TryToDateOnly(out this._settlDate);
            //this._settlDate = bufferSegment.ToDateOnly();
        }

        private void OnTag52(BufferSegmentBase bufferSegment)
        {
            this._sendingTime = bufferSegment.ToDateTime();
        }

        //OrigSendingTime
        private void OnTag122(BufferSegmentBase bufferSegment)
        {
            this._origSendingTime = bufferSegment.ToDateTime();
        }
    }

    internal sealed class SimpleBankExecReportsCracker : BankExecReportCracker
    {
        public SimpleBankExecReportsCracker(IOrdersParentChannel parentChannel, ILogger logger)
            : base(parentChannel, logger)
        { }

        protected override OrderStatus GetTranslatedOrderStatus(char externalOrderStatus)
        {
            switch (externalOrderStatus)
            {
                case OrdStatus.FILLED:
                    return OrderStatus.Trade;
                case OrdStatus.REJECTED:
                    return OrderStatus.Reject;
                default:
                    return OrderStatus.Unrecognized;
            }
        }
    }
    internal sealed class SimpleBankWithNewStatusExecReportsCracker : BankExecReportCracker
    {
        public SimpleBankWithNewStatusExecReportsCracker(IOrdersParentChannel parentChannel, ILogger logger)
            : base(parentChannel, logger)
        { }

        protected override OrderStatus GetTranslatedOrderStatus(char externalOrderStatus)
        {
            switch (externalOrderStatus)
            {
                case OrdStatus.NEW:
                case OrdStatus.PENDING_NEW:
                    return OrderStatus.New;
                case OrdStatus.FILLED:
                    return OrderStatus.Trade;
                case OrdStatus.PARTIALLY_FILLED:
                    return OrderStatus.PartiallTrade;
                case OrdStatus.REJECTED:
                case OrdStatus.CANCELED:
                    return OrderStatus.Reject;
                default:
                    return OrderStatus.Unrecognized;
            }
        }
    }

    internal abstract class VenueExecReportCracker : IFixMessageCracker
    {
        protected IOrdersParentChannel _parentChannel;
        protected ILogger _logger;
        private DateTime _currentMessageReceived;
        private readonly bool _needsDealConfirmation;

        protected VenueExecReportCracker(IOrdersParentChannel parentChannel, ILogger logger, bool needsDealConfirmation)
        {
            this._parentChannel = parentChannel;
            this._logger = logger;
            this._needsDealConfirmation = needsDealConfirmation;
        }

        private const byte _recognizedType = (byte) '8';

        public byte RecognizedMessageType
        {
            get { return _recognizedType; }
        }

        public bool TryRecoverFromParsingErrors
        {
            get { return true; }
        }

        protected virtual void ConsumeNonstandardFixTag(int tagId, BufferSegmentBase tagValueSegment) { }

        public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
        {
            switch (tagId)
            {
                case 52:
                    OnTag52(tagValueSegment);
                    break;
                case 58:
                    OnTag58(tagValueSegment);
                    break;
                case 55:
                    OnTag55(tagValueSegment);
                    break;
                case 39:
                    OnTag39(tagValueSegment);
                    break;
                case 54:
                    OnTag54(tagValueSegment);
                    break;
                case 38:
                    OnTag38(tagValueSegment);
                    break;
                case 150:
                    OnTag150(tagValueSegment);
                    break;
                case 11:
                    OnTag11(tagValueSegment);
                    break;
                case 151:
                    OnTag151(tagValueSegment);
                    break;
                case 31:
                    OnTag31(tagValueSegment);
                    break;
                case 32:
                    OnTag32(tagValueSegment);
                    break;
                case 17:
                    OnTag17(tagValueSegment);
                    break;
                case 37:
                    OnTag37(tagValueSegment);
                    break;
                case 60:
                    OnTag60(tagValueSegment);
                    break;
                case 64:
                    OnTag64(tagValueSegment);
                    break;
                case 41:
                    OnTag41(tagValueSegment);
                    break;
                case 14:
                    OnTag14(tagValueSegment);
                    break;
                case 59:
                    OnTag59(tagValueSegment);
                    break;
                case 122:
                    OnTag122(tagValueSegment);
                    break;
                default:
                    this.ConsumeNonstandardFixTag(tagId, tagValueSegment);
                    break;
            }
        }

        private decimal? _leavesQty;
        protected decimal? _lastQty;
        private decimal? _cumQty;
        private decimal? _orderQty;
        protected decimal? _lastPx;
        private string _execId;
        private DateTime? _transactTime;
        private DateTime? _settlDate;
        private char _fixOrderStatus;
        protected char _execType;
        protected char _timeInForce;
        protected string _orderId;
        protected string _remoteOrderId;
        protected string _origOrderId;
        protected string _freeTextInfo;
        private DateTime _sendingTime = DateTime.MinValue;
        private DateTime _origSendingTime = DateTime.MinValue;
        private QuickFixSubsegment _currentMessage;
        protected int? _orderRejReason;
        protected Common.Symbol _symbol;
        private char _dealDirection;

        protected void MultiplyConvertedSizes(decimal conversionMultiplier)
        {
            if (_leavesQty.HasValue)
                _leavesQty *= conversionMultiplier;
            if (_lastQty.HasValue)
                _lastQty *= conversionMultiplier;
            if (_cumQty.HasValue)
                _cumQty *= conversionMultiplier;
            if (_orderQty.HasValue)
                _orderQty *= conversionMultiplier;
        }

        public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
        {
            this._currentMessageReceived = currentMessageReceivedTimeUtc;
            this._currentMessage = currentMessage;

            this._leavesQty = null;
            this._lastQty = null;
            this._cumQty = null;
            this._orderQty = null;
            this._lastPx = null;
            this._execId = null;
            this._transactTime = null;
            this._settlDate = null;
            this._fixOrderStatus = char.MaxValue;
            this._execType = char.MaxValue;
            this._timeInForce = char.MaxValue;
            this._orderId = null;
            this._remoteOrderId = null;
            this._origOrderId = null;
            this._freeTextInfo = null;
            this._sendingTime = DateTime.MinValue;
            this._origSendingTime = DateTime.MinValue;
            this._symbol = Common.Symbol.NULL;
            this._dealDirection = char.MaxValue;

            this.ResetState();
        }

        protected virtual void ResetState() { }

        protected virtual int GetMissingTag() { return 0; }
        protected virtual void UpdateInternalStatusIfneeded() { }

        private IntegratorOrderExternalChange OnExecReportNew(out decimal? initialQuantity)
        {
            this._logger.Log(LogLevel.Info, "Order [{0}] was received and enqeued by other end", this._orderId);
            initialQuantity = this._leavesQty;
            return IntegratorOrderExternalChange.ConfirmedNew;
        }

        private IntegratorOrderExternalChange OnExecReportReplace(QuickFixSubsegment message, out decimal? initialQuantity)
        {
            if (this._lastQty.HasValue && this._lastQty.Value != 0m)
            {
                this._logger.Log(LogLevel.Fatal,
                    "Received replace ExecutionReport, but last shares is nonzero: ", message);
            }

            //Cancel original order
            if (!string.IsNullOrEmpty(this._origOrderId))
            {
                string origClId = this._origOrderId;

                this._logger.Log(LogLevel.Info,
                    "Order [{0}] was replaced and order [{1}] was created and enqeued by other end", origClId,
                    this._orderId);

                this._parentChannel.SendOrderChange(new OrderChangeInfo(origClId, this._symbol,
                    IntegratorOrderExternalChange.Cancelled, this._sendingTime, message,
                    null, new RejectionInfo(this._freeTextInfo, null,
                        RejectionType.OrderCancel), this._currentMessageReceived));
            }
            else
            {
                this._logger.Log(LogLevel.Fatal,
                    "Received replace ExecutionReport, but OrigClOrdID is not set: ", message);
            }

            initialQuantity = this._leavesQty;
            if (!initialQuantity.HasValue)
            {
                this._logger.Log(LogLevel.Fatal,
                    "Received replace ExecutionReport, but LeavesQty is not set: ", message);
            }

            return IntegratorOrderExternalChange.ConfirmedNew;
        }

        //hotspot will override
        protected virtual IntegratorOrderExternalChange OnExecReportRejected(out RejectionInfo rejectionInfo, out ExecutionInfo executionInfo)
        {
            this._logger.Log(LogLevel.Info, "Order [{0}] REJECTED", this._orderId);

            if (this._orderRejReason.HasValue)
            {
                this._freeTextInfo = string.Format("Reason code: {0} [{1}]. Description: {2}",
                    this._orderRejReason.Value, MapOrdRejReasonId(this._orderRejReason.Value),
                    this._freeTextInfo);
            }

            decimal? rejectedAmount = GetRejectedAmount();
            rejectionInfo = new RejectionInfo(this._freeTextInfo, rejectedAmount, RejectionType.OrderReject);
            executionInfo = null;

            return IntegratorOrderExternalChange.Rejected;
        }

        protected virtual bool IsIoCMiss
        {
            get { return this._timeInForce == TimeInForce.IMMEDIATE_OR_CANCEL; }
        }

        protected decimal? GetRejectedAmount()
        {
            if (this._cumQty.HasValue && this._orderQty.HasValue)
            {
                return this._orderQty - this._cumQty;
            }

            return null;
        }

        private IntegratorOrderExternalChange OnExecReportCancelled(out RejectionInfo rejectionInfo)
        {
            IntegratorOrderExternalChange orderStatus;

            //hotspot will not have this
            if (!string.IsNullOrEmpty(this._origOrderId))
            {
                this._logger.Log(LogLevel.Info,
                    "Cancel Execution report for order: {0} did have OrigClOrdID populated (as: {1}) so changing orderId to: {1}",
                    this._orderId, this._origOrderId);

                this._orderId = this._origOrderId;
            }

            if (this.IsIoCMiss)
            {
                this._logger.Log(LogLevel.Info, "IoC Order [{0}] Rejected", this._orderId);
                orderStatus = IntegratorOrderExternalChange.Rejected;
                this._freeTextInfo += "<Integrator Supplied Reason: IoC miss>";
            }
            else
            {
                this._logger.Log(LogLevel.Info, "Order [{0}] Cancelled", this._orderId);
                orderStatus = IntegratorOrderExternalChange.Cancelled;
            }

            decimal? rejectedAmount = GetRejectedAmount();
            rejectionInfo = new RejectionInfo(this._freeTextInfo, rejectedAmount,
                orderStatus == IntegratorOrderExternalChange.Cancelled
                    ? RejectionType.OrderCancel
                    : RejectionType.OrderReject);

            return orderStatus;
        }

        private IntegratorOrderExternalChange OnExecReportPendingCancel()
        {
            if (!string.IsNullOrEmpty(this._origOrderId))
            {
                this._logger.Log(LogLevel.Info, "Cancel Execution report for order: {0} did have OrigClOrdID populated (as: {1}) so changing orderId to: {1}",
                    this._orderId, this._origOrderId);

                this._orderId = this._origOrderId;
            }
            this._logger.Log(LogLevel.Info, "Order [{0}] is in pending cancel state", this._orderId);
            return IntegratorOrderExternalChange.PendingCancel;
        }

        private IntegratorOrderExternalChange OnExecReportStatus(QuickFixSubsegment message, out RejectionInfo rejectionInfo, out decimal? initialQuantity)
        {
            bool cancelled = true;
            initialQuantity = this._leavesQty;
            if (!initialQuantity.HasValue || initialQuantity.Value != 0)
            {
                this._logger.Log(LogLevel.Fatal, "Received OrderStatus ExecutionReport, but LeavesQty is not set or it is nonzero: ", message);
                cancelled = false;
            }

            if (this._fixOrderStatus != OrdStatus.CANCELED)
            {
                this._logger.Log(LogLevel.Fatal, "Received OrderStatus ExecutionReport, but OrderStatus is not Cancelled: ", message);
                cancelled = false;
            }

            decimal? rejectedAmount = GetRejectedAmount();

            this._freeTextInfo += "<Integrator Supplied Reason: Order Status after reconnect>";
            rejectionInfo = new RejectionInfo(this._freeTextInfo, rejectedAmount, RejectionType.OrderReject);

            //After reconnect all pending orders should have been cancelled (in Integrator language it's rejected)
            return cancelled ? IntegratorOrderExternalChange.Rejected : IntegratorOrderExternalChange.InBrokenState;
        }

        protected abstract FlowSide? GetFlowSide();

        private DealDirection GetDealDirection()
        {
            switch (this._dealDirection)
            {
                case '1':
                    return DealDirection.Buy;
                case '2':
                    return DealDirection.Sell;
                default:
                    throw new HaltingTagException(
                        string.Format("Unrecognized value for tag 54 (Side): [{0}]. Expected 1 or 2",
                            this._dealDirection), 54);
            }
        }

        protected IntegratorOrderExternalChange OnExecReportTradeNoConfirmation(out ExecutionInfo executionInfo)
        {
            IntegratorOrderExternalChange orderStatus;
            decimal? filledAmount = this._lastQty;
            if (this._fixOrderStatus == OrdStatus.FILLED)
            {
                orderStatus = IntegratorOrderExternalChange.Filled;
                if (!filledAmount.HasValue) filledAmount = this._cumQty;
            }
            else
            {
                if (this._fixOrderStatus != OrdStatus.PARTIALLY_FILLED)
                {
                    this._logger.Log(LogLevel.Fatal,
                                    "Order [{0}] with Trade ExecReport is in unexpected state [{1}]", this._orderId,
                                    this._fixOrderStatus);
                }

                orderStatus = IntegratorOrderExternalChange.PartiallyFilled;

                if (!filledAmount.HasValue)
                {
                    _logger.Log(LogLevel.Fatal,
                               "Cannot get filled converted size for partial fill execution report (instrument, instrument mapping, or size is missing).");
                    throw new HaltingTagException("Cannot get filled converted size for partial fill execution report.", 32);
                }
            }

            //logged by caller
            //this._logger.Log(LogLevel.Info, "Order [{0}] flipped to {1}", this._orderId, orderStatus);

            FlowSide? flowSide = this.GetFlowSide();

            executionInfo = new ExecutionInfo(
                    filledAmount,
                    this._lastPx,
                    this._execId,
                    this._transactTime,
                    this._settlDate, GetDealDirection()) { FlowSide = flowSide };

            return orderStatus;
        }

        protected ExecutionInfo GetExecutionInfo(decimal filledAmount)
        {
            return new ExecutionInfo(
                        filledAmount,
                        this._lastPx,
                        this._execId,
                        this._transactTime,
                        this._settlDate, GetDealDirection());
        }

        //we want to minimize virtual methods on critical path
        private IntegratorOrderExternalChange OnExecReportTrade(out ExecutionInfo executionInfo)
        {
            IntegratorOrderExternalChange orderStatus = this.OnExecReportTradeNoConfirmation(out executionInfo);

            this._logger.Log(LogLevel.Info, "Order [{0}] flipped to {1}", this._orderId, orderStatus);

            return orderStatus;
        }

        protected virtual void AppendInfoAfterTradeOrReject(AllocationInfo allocationInfo) { }

        protected virtual IntegratorOrderExternalChange OnExecReportTradeMayBeRejectable(out ExecutionInfo executionInfo)
        {
            IntegratorOrderExternalChange orderStatus = this.OnExecReportTradeNoConfirmation(out executionInfo);

            this._logger.Log(LogLevel.Info, "Order [{0}] flipped to {1}", this._orderId, orderStatus);

            return orderStatus;
        }

        private IntegratorOrderExternalChange OnExecReportUnknown()
        {
            this._logger.Log(LogLevel.Fatal, "Order [{0}] received unsupported ExecType report: [{1}]",
                        this._orderId, this._execType);
            return IntegratorOrderExternalChange.InBrokenState;
        }


        protected enum ExecutionReportStatus
        {
            FluentNew,
            New,
            Trade,
            //we want to minimize virtual methods on critical paths
            TradeMayBeRejectable,
            Replace,
            Reject,
            Cancel,
            OrderStatus,
            PendingCancelOrReplace,
            ToBeIgnored,
            Unrecognized  
        }

        protected abstract ExecutionReportStatus GetTranslatedExecutionReportStatus(char externalExecType);

        public void MessageParsingDone()
        {
            int missingTag = this.GetMissingTag();

            if (this._orderId == null)
                missingTag = 11;
            if (this._execType == char.MaxValue)
                missingTag = 150;
            if (this._fixOrderStatus == char.MaxValue)
                missingTag = 39;
            if (this._dealDirection == char.MaxValue)
                missingTag = 54;

            if (missingTag != 0)
            {
                this._logger.Log(LogLevel.Fatal,
                    "Receiving (and rejecting!) incomplete execution report");
                throw new HaltingTagException("Incomplete message - missing tag", missingTag);
            }

            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            decimal? initialQuantity = null;

            if (this._parentChannel.IsDivertedViaFluent && this._fixOrderStatus != OrdStatus.PENDING_NEW)
            {
                if (this._origSendingTime == DateTime.MinValue)
                    this._logger.Log(LogLevel.Info,
                        "Session diverted via fluent but OrigSendingTime tag (122) is missing or invalid");
                else
                    this._sendingTime = this._origSendingTime;
            }

            this.UpdateInternalStatusIfneeded();

            switch (this.GetTranslatedExecutionReportStatus(this._execType))
            {
                case ExecutionReportStatus.FluentNew:
                    this._logger.Log(LogLevel.Trace, "order {0} acknowledged by Fluent. Not processing further",
                        this._orderId);
                    return;
                case ExecutionReportStatus.New:
                    orderStatus = this.OnExecReportNew(out initialQuantity);
                    break;
                case ExecutionReportStatus.Trade:
                    orderStatus = this.OnExecReportTrade(out executionInfo);
                    AppendInfoAfterTradeOrReject(executionInfo);
                    break;
                case ExecutionReportStatus.TradeMayBeRejectable:
                    orderStatus = this.OnExecReportTradeMayBeRejectable(out executionInfo);
                    AppendInfoAfterTradeOrReject(executionInfo);
                    break;
                case ExecutionReportStatus.Replace:
                    orderStatus = this.OnExecReportReplace(this._currentMessage, out initialQuantity);
                    break;
                case ExecutionReportStatus.Reject:
                    orderStatus = this.OnExecReportRejected(out rejectionInfo, out executionInfo);
                    AppendInfoAfterTradeOrReject(rejectionInfo);
                    break;
                case ExecutionReportStatus.Cancel:
                    orderStatus = this.OnExecReportCancelled(out rejectionInfo);
                    AppendInfoAfterTradeOrReject(rejectionInfo);
                    break;
                case ExecutionReportStatus.OrderStatus:
                    orderStatus = this.OnExecReportStatus(this._currentMessage, out rejectionInfo, out initialQuantity);
                    break;
                case ExecutionReportStatus.PendingCancelOrReplace:
                    orderStatus = this.OnExecReportPendingCancel();
                    break;
                case ExecutionReportStatus.ToBeIgnored:
                    return;
                case ExecutionReportStatus.Unrecognized:
                default:
                    orderStatus = this.OnExecReportUnknown();
                    break;
            }

            if (this._needsDealConfirmation 
                &&
                (orderStatus == IntegratorOrderExternalChange.Filled ||
                 orderStatus == IntegratorOrderExternalChange.PartiallyFilled)
                &&
                executionInfo != null && executionInfo.FlowSide != FlowSide.LiquidityMaker)
            {
                this._parentChannel.SendOrderChange(new OrderChangeInfoEx(this._orderId, this._symbol, this._remoteOrderId, orderStatus, this._sendingTime,
                    this._currentMessage, executionInfo, null, initialQuantity, this._currentMessageReceived));
            }
            else if (orderStatus != IntegratorOrderExternalChange.NonConfirmedDeal)
            {
                this._parentChannel.SendOrderChange(new OrderChangeInfo(this._orderId, this._symbol, this._remoteOrderId, orderStatus, this._sendingTime,
                    this._currentMessage,
                    executionInfo, rejectionInfo, initialQuantity, this._currentMessageReceived));
            }
            else
            {
                //TODO: handle this better
                //TODO: we do not have fix messages for back confirmation
                this._parentChannel.SendOrderChange(new OrderChangeInfoEx(this._orderId, this._symbol, this._remoteOrderId, orderStatus, this._sendingTime,
                    this._currentMessage, executionInfo, null, initialQuantity, this._currentMessageReceived));
                //throw new Exception("Trade confirmation is not enabled in bypassing FIX mode");
            }
        }


        private string MapOrdRejReasonId(int ordRejReasonId)
        {
            //public const int UNKNOWN_SYMBOL = 1;
            //public const int EXCHANGE_CLOSED = 2;
            //public const int TOO_LATE_TO_ENTER = 4;
            //public const int UNKNOWN_ORDER = 5;
            //public const int DUPLICATE_ORDER = 6;
            //99 - other

            switch (ordRejReasonId)
            {
                case 1:
                    return "Unknown Symbol";
                case 2:
                    return "Exchange Closed";
                case 4:
                    return "Too late to enter";
                case 5:
                    return "Unknown Order";
                case 6:
                    return "Duplicate Order";
                case 99:
                    return "Other";
                default:
                    return string.Format("<Unknown OrdRejReasonId!: {0}>", ordRejReasonId);
            }
        }



        //Text = 58
        private void OnTag58(BufferSegmentBase bufferSegment)
        {
            this._freeTextInfo = bufferSegment.GetContentAsAsciiString();
        }

        //Symbol = 55
        private void OnTag55(BufferSegmentBase bufferSegment)
        {
            this._symbol = bufferSegment.ToSymbol();
        }

        //private void OnTag48(BufferSegmentBase bufferSegment)
        //{
        //    if (!this._parentChannel.TryGetSymbolForLmaxId(bufferSegment.ToInt(), out _symbol))
        //    {
        //        string error = string.Format("There is no lmax mapping from symbol id: {0}",
        //            bufferSegment.GetContentAsAsciiString());
        //        this._logger.Log(LogLevel.Error, error);
        //        throw new HaltingTagException(error, 48);
        //    }
        //}

        //OrdStatus = 39
        private void OnTag39(BufferSegmentBase bufferSegment)
        {
            this._fixOrderStatus = (char) bufferSegment[0];
        }

        //Side = 54
        private void OnTag54(BufferSegmentBase bufferSegment)
        {
            this._dealDirection = (char)bufferSegment[0];
        }

        //ExecType = 150
        private void OnTag150(BufferSegmentBase bufferSegment)
        {
            this._execType = (char) bufferSegment[0];
        }


        //ClOrdID = 11
        private void OnTag11(BufferSegmentBase bufferSegment)
        {
            this._orderId = bufferSegment.GetContentAsAsciiString();
        }

        //OrderQty = 38
        private void OnTag38(BufferSegmentBase bufferSegment)
        {
            this._orderQty = bufferSegment.ToDecimal();
        }

        //LeavesQty = 151
        private void OnTag151(BufferSegmentBase bufferSegment)
        {
            this._leavesQty = bufferSegment.ToDecimal();
        }

        //LastQty = 32
        private void OnTag32(BufferSegmentBase bufferSegment)
        {
            this._lastQty = bufferSegment.ToDecimal();
        }

        //LastPx = 31
        private void OnTag31(BufferSegmentBase bufferSegment)
        {
            this._lastPx = bufferSegment.ToDecimal();
        }

        //ExecID = 17
        private void OnTag17(BufferSegmentBase bufferSegment)
        {
            this._execId = bufferSegment.GetContentAsAsciiString();
        }

        //OrdId = 37
        private void OnTag37(BufferSegmentBase bufferSegment)
        {
            this._remoteOrderId = bufferSegment.GetContentAsAsciiString();
        }

        //TransactTime = 60
        private void OnTag60(BufferSegmentBase bufferSegment)
        {
            this._transactTime = bufferSegment.ToDateTime();
        }

        //SettlDate = 64
        private void OnTag64(BufferSegmentBase bufferSegment)
        {
            this._settlDate = bufferSegment.ToDateOnly();
        }

        //OrigClOrdID = 41
        private void OnTag41(BufferSegmentBase bufferSegment)
        {
            this._origOrderId = bufferSegment.GetContentAsAsciiString();
        }

        //CumQty = 14
        private void OnTag14(BufferSegmentBase bufferSegment)
        {
            this._cumQty = bufferSegment.ToDecimal();
        }

        //TimeInForce = 59
        private void OnTag59(BufferSegmentBase bufferSegment)
        {
            this._timeInForce = (char) bufferSegment[0];
        }


        private void OnTag52(BufferSegmentBase bufferSegment)
        {
            this._sendingTime = bufferSegment.ToDateTime();
        }

        //OrigSendingTime
        private void OnTag122(BufferSegmentBase bufferSegment)
        {
            this._origSendingTime = bufferSegment.ToDateTime();
        }
    }
}
