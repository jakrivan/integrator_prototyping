﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using QuickFix;
using QuickFix.FIX44;
using Message = QuickFix.Message;
using SessionState = Kreslik.Integrator.Contracts.SessionState;

namespace Kreslik.Integrator.FIXMessaging
{
    public class TradeCaptureSubscriptionChangeArgs
    {
        public TradeCaptureSubscriptionChangeArgs(bool subscribed)
        {
            this.Subscribed = subscribed;
        }

        public bool Subscribed { get; private set; }

        //TODO ExecId, TradeId ...
    }

    public interface IFIXSTPChannel : IFIXChannel
    {
        bool TryCreateTradeCaptureReport(IExecutionInfo executionInfo, IIntegratorOrderInfo integratorOrderInfo, bool isDealCancellation, out QuickFix.Message stpTicket);
        bool TrySendMessage(QuickFix.Message stp);
        event Action<TradeCaptureSubscriptionChangeArgs> TradeCaptureSubscriptionChanged;
        event Action<TradeReportTicketInfo> NewTradeReportTicketInfo;
        event Action<AggregatedTradeTicketInfo> NewAggregatedTradeTicketInfo;
        TradeReportTicketInfoType InitialTradeReportTicketInfoTypeOfOutgoingTicket { get; }
    }

    public interface IStpFlowSession : IStpFlowSessionInfo
    {
        ILogger Logger { get; }
        void Start();
        void Stop();
        SessionState State { get; }
    }

    public class DummyStpFlowSession : IStpFlowSession
    {
        private ILogger _logger;

        public DummyStpFlowSession(ILogger logger)
        {
            this._logger = logger;
        }

        public ILogger Logger
        {
            get { return this._logger; }
        }

        public STPCounterparty StpCounterparty
        {
            get { return STPCounterparty.CTIPB; }
        }

        public void Start()
        { }

        public void Stop()
        { }

        public SessionState State
        {
            get { return SessionState.Idle; }
        }

        public StpFlowSessionState StpFlowSessionState
        {
            get { return StpFlowSessionState.Disconnected; }
        }
    }

    public class StpFlowSession : IStpFlowSession
    {
        private IFIXSTPChannel _fixStpChannel;
        private bool _consumerSubscribed;
        private ConcurrentQueue<Tuple<Message, string>> _unsentStps = new ConcurrentQueue<Tuple<Message, string>>();
        private ConcurrentDictionary<string, DateTime> _sentTickets = new ConcurrentDictionary<string, DateTime>();
        private IDealStatistics _dealStatistics;
        private readonly TimeSpan _responseThreshold;
        private List<Counterparty> _servedCounterparties;
        private SafeTimer _checkResponcesTimer;
        private IFluentRedirectionInfo _fluentRedirectionInfo;

        public StpFlowSession(IDealStatistics dealStatistics, IFIXSTPChannel fixStpChannel, List<Counterparty> servedCounterparties, IFluentRedirectionInfo fluentRedirectionInfo, TimeSpan responseThreshold, STPCounterparty stpCounterparty, ILogger logger)
        {
            this.Logger = logger;
            this._fixStpChannel = fixStpChannel;
            this._dealStatistics = dealStatistics;
            this._fixStpChannel.TradeCaptureSubscriptionChanged += FixStpChannelTradeCaptureSubscriptionChanged;
            this._fixStpChannel.NewTradeReportTicketInfo += this.OnNewTradeReportTicketInfo;
            this._fixStpChannel.NewAggregatedTradeTicketInfo += this.OnNewAggregatedTradeTicketInfo;
            this._servedCounterparties = servedCounterparties;
            this.StpCounterparty = stpCounterparty;
            this._fluentRedirectionInfo = fluentRedirectionInfo;
            dealStatistics.NewExternalDealExecuted += DealStatisticsOnNewExternalDealExecuted;
            dealStatistics.NewRejectableDealExecuted += DealStatisticsOnNewRejectableDealExecuted;
            dealStatistics.RejectableExecutedDealRejected += DealStatisticsOnRejectableExecutedDealRejected;
            this._fixStpChannel.OnStopped += () =>
            {
                this._consumerSubscribed = false;
            };
            this._responseThreshold = responseThreshold;
            _checkResponcesTimer = new SafeTimer(CheckResponces, _responseThreshold, _responseThreshold, true)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.GetTimerPrecisionForDelay(_responseThreshold),
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
        }

        public ILogger Logger { get; private set; }

        public STPCounterparty StpCounterparty { get; private set; }

        public void Start()
        {
            this._fixStpChannel.Start();
        }

        public void Stop()
        {
            this._fixStpChannel.Stop();
        }

        public SessionState State
        {
            get { return this._fixStpChannel.State; }
        }

        public IFIXChannel FixChannel
        {
            get { return this._fixStpChannel; }
        }

        public StpFlowSessionState StpFlowSessionState
        {
            get
            {
                if (this._fixStpChannel.State == SessionState.Running)
                {
                    return this._consumerSubscribed
                               ? StpFlowSessionState.ConnectedSubscribed
                               : StpFlowSessionState.ConnectedUnsubscribed;
                }
                else
                {
                    return StpFlowSessionState.Disconnected;
                }
            }
        }

        void FixStpChannelTradeCaptureSubscriptionChanged(TradeCaptureSubscriptionChangeArgs args)
        {
            LogLevel level = args.Subscribed ? LogLevel.Info : LogLevel.Error;
            this.Logger.Log(level, args.Subscribed ? "STP Consumer is now subscribed" : "STP Consumer is now UNSUBSCRIBED");
            this._consumerSubscribed = args.Subscribed;
            if (!_unsentStps.IsEmpty)
            {
                this.Logger.Log(LogLevel.Warn, "There are some unsent STP reports when PB subscribed - sending them now");

                ConcurrentQueue<Tuple<Message, string>> unsentStpsLocal = _unsentStps;
                _unsentStps = new ConcurrentQueue<Tuple<Message, string>>();

                foreach (Tuple<Message, string> stpMessageTuple in unsentStpsLocal)
                {
                    TrySentStpMessage(stpMessageTuple.Item1, stpMessageTuple.Item2);
                }
            }
        }

        public void DealStatisticsPublisherOnNewDeal(IExecutionInfo executionInfo, IIntegratorOrderInfo integratorOrderInfo, bool isDealCancellation)
        {
            //counterparty check
            if (this._servedCounterparties.Contains(integratorOrderInfo.Counterparty))
            {
                if (this._fluentRedirectionInfo.IsRedirectedViaFlunet(integratorOrderInfo.Counterparty))
                {
                    this.Logger.Log(LogLevel.Info,
                        "Not sending STP for deal [{0}] (on order [{1}]) since it came from Fluent redirected session",
                        executionInfo.IntegratorTransactionId, integratorOrderInfo.Identity);
                    return;
                }

                QuickFix.Message stp;
                if (!_fixStpChannel.TryCreateTradeCaptureReport(executionInfo, integratorOrderInfo, isDealCancellation, out stp))
                {
                    this.Logger.Log(LogLevel.Fatal, "Couldn't create STP ticket FIX message for order [{0}]",
                                    integratorOrderInfo.Identity);
                    return;
                }

                TrySentStpMessage(stp, executionInfo.IntegratorTransactionId);
            }
        }

        private void DealStatisticsOnNewExternalDealExecuted(OrderChangeInfo orderChangeInfo, IIntegratorOrderExternal integratorOrderExternal)
        {
            this.DealStatisticsPublisherOnNewDeal(orderChangeInfo, integratorOrderExternal, false);
        }

        private void DealStatisticsOnRejectableExecutedDealRejected(RejectableMMDeal rejectableMmDeal)
        {
            this.DealStatisticsPublisherOnNewDeal(rejectableMmDeal, rejectableMmDeal, true);
        }

        private void DealStatisticsOnNewRejectableDealExecuted(RejectableMMDeal rejectableMmDeal)
        {
            this.DealStatisticsPublisherOnNewDeal(rejectableMmDeal, rejectableMmDeal, false);
        }

        private void TrySentStpMessage(QuickFix.Message stpMessage, string integratorDealId)
        {
            bool sent = false;

            _sentTickets.TryAdd(integratorDealId, DateTime.UtcNow);

            if (_fixStpChannel.State != SessionState.Running)
            {
                this.Logger.Log(LogLevel.Error, "Couldn't sent STP FIX message as STPFIX session is in {0} state", _fixStpChannel.State);
            }
            else if (!_consumerSubscribed)
            {
                this.Logger.Log(LogLevel.Error, "Couldn't sent STP FIX message as STP Tickets consuming counterparty is not subscribed");
            }
            else if (!_fixStpChannel.TrySendMessage(stpMessage))
            {
                this.Logger.Log(LogLevel.Error, "Couldn't sent STP FIX message due to errors in messaging layer", _fixStpChannel.State);
            }
            else
            {
                sent = true;
            }

            if (sent)
            {
                this.OnNewTradeReportTicketInfo(new TradeReportTicketInfo(integratorDealId,
                                                                          HighResolutionDateTime.UtcNow, null,
                                                                          _fixStpChannel.InitialTradeReportTicketInfoTypeOfOutgoingTicket,
                                                                          this.StpCounterparty,
                                                                          stpMessage.ToString()));
            }
            else
            {
                DateTime unused;
                _sentTickets.TryRemove(integratorDealId, out unused);

                OnStpCouldNotBeSent(stpMessage, integratorDealId);
            }
        }

        private void CheckResponces()
        {
            DateTime thresholdTime = DateTime.UtcNow - _responseThreshold;

            foreach (KeyValuePair<string, DateTime> keyValuePair in _sentTickets.Where(pair => pair.Value < thresholdTime))
            {
                this.Logger.Log(LogLevel.Fatal, "STP ticket for [{0}] sent at [{1}] didn't receive any response yet",
                                keyValuePair.Key, keyValuePair.Value);
                DateTime unusedDt;
                _sentTickets.TryRemove(keyValuePair.Key, out unusedDt);
            }
        }

        private void OnNewTradeReportTicketInfo(TradeReportTicketInfo tradeReportTicketInfo)
        {
            if (tradeReportTicketInfo.TradeReportTicketInfoType != _fixStpChannel.InitialTradeReportTicketInfoTypeOfOutgoingTicket)
            {
                DateTime unused;
                _sentTickets.TryRemove(tradeReportTicketInfo.IntegratorDealId, out unused);
            }

            this._dealStatistics.NewExternalDealExecutedTicket(tradeReportTicketInfo);
        }

        private void OnNewAggregatedTradeTicketInfo(AggregatedTradeTicketInfo aggregatedTradeTicketInfo)
        {
            this._dealStatistics.NewAggregatedTradeTicketInfo(aggregatedTradeTicketInfo);
        }

        private void OnStpCouldNotBeSent(QuickFix.Message stpMessage, string integratorDealId)
        {
            string rawMsg = stpMessage.ToString();
            string parsedMsg = stpMessage.ToFormattedString();
            this.Logger.Log(LogLevel.Fatal,
                             "Couldn't sent STP FIX message (detailed reason is in log file). Buffering it for now.: {0}{1}{0}{2}", Environment.NewLine, rawMsg, parsedMsg);
            _unsentStps.Enqueue(new Tuple<Message, string>(stpMessage, integratorDealId));
        }
    }
}
