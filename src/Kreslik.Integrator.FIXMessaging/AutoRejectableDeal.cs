﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using QuickFix;

namespace Kreslik.Integrator.FIXMessaging
{
    public class AutoRejectableDeal : IClientDealRequest, IIntegratorReceivedObjectEx
    {
        public enum RejectableTakerDealStatus
        {
            Pending = 0,
            KgtConfirmed = 1,
            KgtRejected_InternalFailure = 2,
            KgtRejected_KgtTooLate = 3,
            KgtRejected_KgtTooLateOTDelivered = 4,
            KgtRejected_DealMissed = 5,
            KgtRejected_SystemFailedToProcess = 6,
            KgtRejected_SystemUnavailableSizeToTrade = 7,
            KgtRejected_RiskMgmtDisallow = 8,
            KgtRejected_SettlDateUnavailable = 9,
            KgtRejected_IncomingOrderMismatch = 10,
            CounterpartyRejected = 11
        }

        public enum RejectableTakerDealRejectionReason
        {
            InternalFailure = 2,
            KgtTooLate = 3,
            KgtTooLateOTDelivered = 4,
            DealMissed = 5,
            SystemFailedToProcess = 6,
            SystemUnavailableSizeToTrade = 7,
            RiskMgmtDisallow = 8,
            SettlDateUnavailable = 9,
            IncomingOrderMismatch = 10,
            CounterpartyRejected = 11
        }

        public string CounterpartyDealIdentity { get; private set; }
        public string IntegratorOrderIdentity { get; private set; }

        private int _dealStatus;
        private IAccurateTimer _timer;
        private Action<AutoRejectableDeal, RejectableTakerDealRejectionReason> _timerExpiredAction;
        public QuickFix.Message DealExecutionReport { get; private set; }

        public AutoRejectableDeal(OrderChangeInfoEx orderChangeInfo, IIntegratorOrderExternal order)
        {
            this.CounterpartyDealIdentity = orderChangeInfo.ChangeEventArgs.ExecutionInfo.CounterpartyTransactionId;
            this.IntegratorOrderIdentity = orderChangeInfo.Identity;
            this.Identity = this.IntegratorOrderIdentity + ":" + this.CounterpartyDealIdentity;
            this.DealExecutionReport = orderChangeInfo.Message;

            this.Symbol = order.Symbol;
            this.IntegratorDealDirection = order.IntegratorDealDirection;
            this.Counterparty = order.Counterparty;
            this.SizeBaseAbsInitial = orderChangeInfo.ChangeEventArgs.ExecutionInfo.FilledAmount.Value;
            this.RequestedPrice = orderChangeInfo.ChangeEventArgs.ExecutionInfo.UsedPrice.Value;
            this.IntegratorReceivedTimeUtc = orderChangeInfo.IntegratorReceivedTimeUtc;
            this.CounterpartySentTimeUtc = orderChangeInfo.CounterpartySentTime.Value;

            this.SettlementDate = orderChangeInfo.ChangeEventArgs.ExecutionInfo.SettlementDate.Value;
            this.SettlementDateTimeUtc = orderChangeInfo.ChangeEventArgs.ExecutionInfo.SettlementTimeCounterpartyUtc;
        }


        public DateTime MaxProcessingCutoffTime { get; private set; }
        public bool SetTimeout(OrderChangeInfoEx orderChangeInfo, DateTime maxProcessingCutoff, Action<AutoRejectableDeal, RejectableTakerDealRejectionReason> processingTimeExpiredAction)
        {
            this._timerExpiredAction = processingTimeExpiredAction;
            this.MaxProcessingCutoffTime = maxProcessingCutoff;
            return HighResolutionDateTime.HighResolutionTimerManager.TryRegisterTimerIfInFuture(this.TimeoutExpired,
                maxProcessingCutoff, TimerTaskFlagUtils.WorkItemPriority.Highest, TimerTaskFlagUtils.TimerPrecision.Highest,
                true, true, out _timer);
        }

        private void TimeoutExpired()
        {
            if (this._timerExpiredAction != null)
                this._timerExpiredAction(this, RejectableTakerDealRejectionReason.KgtTooLate);
        }

        private bool SetNewStatusIfNotClosed(RejectableTakerDealStatus dealStatus)
        {
            bool wasPending = InterlockedEx.CheckAndFlipState(ref _dealStatus, (int)dealStatus,
                (int)RejectableTakerDealStatus.Pending);

            if (wasPending)
            {
                HighResolutionDateTime.HighResolutionTimerManager.CancelTimer(ref _timer);
            }

            return wasPending;
        }

        public bool SetExecutionChange(RejectableTakerDealStatus dealStatus)
        {
            return this.SetNewStatusIfNotClosed(dealStatus);
        }

        public RejectableTakerDealStatus CurrentDealStatus { get { return (RejectableTakerDealStatus)_dealStatus; } }

        public override string ToString()
        {
            return this.Identity;
        }

        #region IIntegratorOrderInfo

        public string Identity { get; private set; }
        public Symbol Symbol { get; private set; }
        public DealDirection IntegratorDealDirection { get; private set; }
        public Counterparty Counterparty { get; private set; }
        public decimal SizeBaseAbsInitial { get; private set; }

        public decimal CounterpartyRequestedSizeBaseAbsFillMinimum { get { return this.SizeBaseAbsInitial; } }
        public decimal CounterpartyRequestedSizeBaseAbs { get { return this.SizeBaseAbsInitial; } }
        public decimal RequestedPrice { get; private set; }
        public DateTime SettlementDate { get; private set; }
        public DateTime SettlementDateTimeUtc { get; private set; }

        public IntegratorOrderInfoType IntegratorOrderInfoType
        {
            get { return IntegratorOrderInfoType.ExternalClientDealRequest; }
        }
        public bool IsRiskRemovingOrder { get; set; }

        #endregion IIntegratorOrderInfo

        #region IIntegratorReceivedObject members
        public DateTime IntegratorReceivedTimeUtc { get; private set; }
        public DateTime CounterpartySentTimeUtc { get; private set; }
        #endregion IIntegratorReceivedObject members


        #region IClientDealRequest

        public decimal CounterpartyRequestedPrice
        {
            get { return this.RequestedPrice; }
        }

        public decimal IntegratorFilledAmountBaseAbs
        {
            get { return _dealStatus == (int)RejectableTakerDealStatus.KgtConfirmed ? this.SizeBaseAbsInitial : 0m; }
        }

        public decimal IntegratorFilledAmountBasePol
        {
            get
            {
                return this.IntegratorDealDirection == DealDirection.Buy
                    ? IntegratorFilledAmountBaseAbs
                    : -IntegratorFilledAmountBaseAbs;
            }
        }

        #endregion /IClientDealRequest
    }










    public class OrderChangeInfoEx: OrderChangeInfo
    {
        public OrderChangeInfoEx(string identity, Symbol symbol, string remotePlatformOrderId,
            IntegratorOrderExternalChange orderStatus,
            DateTime? counterpartySentTime, IPooledBufferSegment rawMessageHandle,
            ExecutionInfo executionInfo, QuickFix.Message message, decimal? initaialConfirmedAmount,
            DateTime integratorIntegratorReceivedUtc)
            : base(
                identity, symbol, remotePlatformOrderId, orderStatus, counterpartySentTime, rawMessageHandle,
                executionInfo, null, initaialConfirmedAmount, integratorIntegratorReceivedUtc)
        {
            this.Message = message;
        }

        public QuickFix.Message Message { get; private set; }

        public void CalculateFixMessageIfNeeded(ILogger logger)
        {
            if (this.Message == null && this.DangerousMessageHandle != null)
            {
                try
                {
                    CreateQuickFixMessageFromRawBuffer(logger);
                }
                catch (Exception e)
                {
                    logger.LogException(LogLevel.Fatal, "Exception during reconstructing ExecutionReport message from rawBuffer", e);
                }
            }   
        }

        private void CreateQuickFixMessageFromRawBuffer(ILogger logger)
        {
            string msgStr = this.DangerousMessageHandle.GetContentAsAsciiString();

            QuickFix.Fields.MsgType msgType = Message.IdentifyType(msgStr);
            string beginString = Message.ExtractBeginString(msgStr);

            this.Message = QuickFix.DefaultMessageFactory.Instance.Create(beginString, msgType.Obj);
            this.Message.FromString(
                msgStr,
                true,
                DataDictionaryProvider.EmptyDataDictionary,
                DataDictionaryProvider.EmptyDataDictionary,
                QuickFix.DefaultMessageFactory.Instance);


            //The repeating group workaround - as we do not have dictionaries
            {
                if (this.Message.IsSetField(382) && this.Message.IsSetField(375))
                {
                    var grp = new QuickFix.FIX42.ExecutionReport.NoContraBrokersGroup
                    {
                        ContraBroker = new QuickFix.Fields.ContraBroker(this.Message.GetString(375))
                    };
                    this.Message.AddGroup(grp);
                    this.Message.RemoveField(375);
                    //the multiple tags inside repeating group workaround
                    if (this.Message.RepeatedTags != null)
                    {
                        var field = this.Message.RepeatedTags.FirstOrDefault(fld => fld.Tag == 375);
                        if (field != null)
                        {
                            var grp2 = new QuickFix.FIX42.ExecutionReport.NoContraBrokersGroup
                            {
                                ContraBroker = new QuickFix.Fields.ContraBroker(field.ToString())
                            };
                            this.Message.AddGroup(grp2);
                            this.Message.RepeatedTags.Remove(field);
                        }
                    }

                }

                if (this.Message.RepeatedTags != null && this.Message.RepeatedTags.Any())
                {
                    logger.Log(LogLevel.Fatal, "Incoming LL deal message has unexpected repeated tags ({0}). Stripping those. This might cause LL action ignore by ECN. Message: {1}",
                        string.Join(",", this.Message.RepeatedTags.Select(f => f.Tag.ToString())), msgStr);
                    this.Message.RepeatedTags.Clear();
                }

                if (this.Message.Header.RepeatedTags != null && this.Message.Header.RepeatedTags.Any())
                {
                    logger.Log(LogLevel.Fatal, "Incoming LL deal message header has unexpected repeated tags ({0}). Stripping those. This might cause LL action ignore by ECN. Message: {1}",
                        string.Join(",", this.Message.Header.RepeatedTags.Select(f => f.Tag.ToString())), msgStr);
                    this.Message.Header.RepeatedTags.Clear();
                }
            }
        }

        private Kreslik.Integrator.LowLatencyUtils.QuickFixSubsegment _unacquiredFixRawMessage;

        public void UpdateChangeEventArgs(AutoRejectableDeal autoRejectableDeal)
        {
            this.ChangeEventArgs = new OrderChangeEventArgsEx(ChangeEventArgs, autoRejectableDeal);
        }
    }

    public class OrderChangeEventArgsEx : OrderChangeEventArgs
    {
        public OrderChangeEventArgsEx(OrderChangeEventArgs origOrderChangeEventArgs, AutoRejectableDeal autoRejectableDeal)
            : base(
                origOrderChangeEventArgs.ChangeState, origOrderChangeEventArgs.ExecutionInfo,
                origOrderChangeEventArgs.RejectionInfo, origOrderChangeEventArgs.CounterpartyReason)
        {
            this.AutoRejectableDeal = autoRejectableDeal;
        }

        public AutoRejectableDeal AutoRejectableDeal { get; private set; }
    }
}
