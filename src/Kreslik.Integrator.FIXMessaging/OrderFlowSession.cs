﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;

namespace Kreslik.Integrator.FIXMessaging
{
    public class OrderFlowSession : FIXSessionBase, IOrderFlowSession
    {
        private IOrderFlowMessagingAdapter _orderFlowMessagingAdapter;
        protected ConcurrentDictionary<string, WritableOrder> _openedOrdersMap = new ConcurrentDictionary<string, WritableOrder>();
        protected IRiskManager _riskManager;
        protected IDealStatisticsConsumer _dealStatisticsConsumer;
        private RemovableEventsRateChecker[] _defaultRejectionRateChecker;
        private Tuple<string, RemovableEventsRateChecker>[][] _specialCaseRejectionRateCheckers;
        private RemovableEventsRateChecker[] _cancelReplaceRejectionRateChecker;
        private IIntegratorPerformanceCounter IntegratorPerfCounter { get; set; }
        private IDelayEvaluator _delayEvaluator;

        private Counterparty[] GetSublayers(Counterparty counterparty)
        {
            if (counterparty == LmaxCounterparty.L01)
                return LmaxCounterparty.LayersOfLM1;
            else
                return new Counterparty[]{counterparty};
        }

        public OrderFlowSession(IFIXChannel fixChannel, IOrderFlowMessagingAdapter orderFlowMessagingAdapter,
                                ILogger logger, Counterparty counterparty, IRiskManager riskManager,
                                IDealStatisticsConsumer dealStatisticsConsumer, OrderFlowSessionSettings settings, IDelayEvaluator delayEvaluator)
            : base(fixChannel, logger, counterparty)
        {
            this._settings = settings;
            this._orderFlowMessagingAdapter = orderFlowMessagingAdapter;
            this._delayEvaluator = delayEvaluator;
            fixChannel.OnStopped += () =>
                {
                    if (this._openedOrdersMap.Count > 0)
                    {
                        this.Logger.Log(LogLevel.Fatal,
                                        "Receiving FIX channel stop event, but there is {0} orders in open state ({1}). If this object will be disposed it will not be possible to automatically match the incoming execution reports",
                                        this._openedOrdersMap.Count, string.Join(", ", this._openedOrdersMap.Keys));
                    }
                };
            fixChannel.OnStarted += OnChannelStarted;

            orderFlowMessagingAdapter.OnOrderChanged += HandleOrderChanged;
            orderFlowMessagingAdapter.OnBusinessReject += InvokeOnBusinessReject;
            orderFlowMessagingAdapter.OnBusinessReject +=
                m => riskManager.TurnOnGoFlatOnly("Receiving business reject message on ORD session");
            if (settings.ExternalOrdersRejectionRate.CheckAllowed)
            {
                this._defaultRejectionRateChecker =
                        this.GetSublayers(counterparty).Select(c => CreateDefaultRejectionsRateChecker(settings)).ToArray();
                this._cancelReplaceRejectionRateChecker =
                    this.GetSublayers(counterparty).Select(c => CreateNongenuineRejectionsRateChecker(settings)).ToArray();

                if (settings.ExternalOrdersRejectionRate.SpecialRejectionsHandling != null)
                {
                    _specialCaseRejectionRateCheckers =
                        this.GetSublayers(counterparty)
                            .Select(c => CreateSpecificRejectionsRateCheckers(settings))
                            .ToArray();
                }
            }
            this._riskManager = riskManager;
            this._dealStatisticsConsumer = dealStatisticsConsumer;
            this.IntegratorPerfCounter = IntegratorPerformanceCounter.Instance;
        }

        private RemovableEventsRateChecker CreateDefaultRejectionsRateChecker(OrderFlowSessionSettings settings)
        {
            return new RemovableEventsRateChecker(settings.ExternalOrdersRejectionRate.TimeIntervalToCheck,
                                          settings.ExternalOrdersRejectionRate.MaximumAllowedInstancesPerTimeInterval);
        }

        private RemovableEventsRateChecker CreateNongenuineRejectionsRateChecker(OrderFlowSessionSettings settings)
        {
            return new RemovableEventsRateChecker(settings.ExternalOrdersRejectionRate.TimeIntervalToCheck,
                                          settings.ExternalOrdersRejectionRate.MaximumAllowedNongenuineInstancesPerTimeInterval);
        }

        private Tuple<string, RemovableEventsRateChecker>[] CreateSpecificRejectionsRateCheckers(OrderFlowSessionSettings settings)
        {
            return settings.ExternalOrdersRejectionRate.SpecialRejectionsHandling.Select(
                            setting =>
                            new Tuple<string, RemovableEventsRateChecker>(
                                setting.TriggerPhrase,
                                new RemovableEventsRateChecker(
                                    setting.TimeIntervalToCheck,
                                    setting.MaximumAllowedInstancesPerTimeInterval)))
                                .ToArray();
        }

        private int _stopRequestsCnt = 0;

        protected bool SetStopInProcess()
        {
            //first increment (Interlocked returns the NEW value)
            bool isFirstRequest = (Interlocked.Increment(ref this._stopRequestsCnt) == 1);

            if (isFirstRequest)
            {
                this.Logger.Log(LogLevel.Info, "Initiated stopping - will invoke event if any subscribers available");
            }

            return isFirstRequest;
        }

        protected void ClearStopInProgress()
        {
            Interlocked.Exchange(ref this._stopRequestsCnt, 0);
        }

        protected bool GetIsStopInProgress(bool forceCrossThreadsSynchronisation)
        {
            return
                (forceCrossThreadsSynchronisation
                    ? InterlockedEx.Read(ref this._stopRequestsCnt)
                    : this._stopRequestsCnt)
                != 0;
        }

        protected virtual void OnChannelStarted()
        {
            this.ClearStopInProgress();
            _outstandingOrdersCancelled = false;
        }

        public override void Stop()
        {
            this.SetStopInProcess();
            base.Stop();
        }

        public override void StopAsync(TimeSpan delay)
        {
            if (this.SetStopInProcess())
            {
                this.Logger.Log(LogLevel.Info, "Triggering delayed stop (will not be executed if stopped in meantime)");

                TaskEx.ExecAfterDelayWithErrorHandling(delay, () =>
                {
                    //if session started in the meantime, then we do not stop
                    if (this.GetIsStopInProgress(true))
                        this.Stop();
                });
            }
        }

        protected OrderFlowSessionSettings _settings;
        //protected bool _stopInProcess = false;
        protected bool _outstandingOrdersCancelled = false;

        public virtual IIntegratorOrderExternal GetNewExternalOrder(OrderRequestInfo orderRequestInfo, out string errorMessage)
        {
            return this.GetNewExternalOrder(orderRequestInfo, this.Counterparty, out errorMessage);
        }

        public IIntegratorOrderExternal GetNewExternalOrder(OrderRequestInfo orderRequestInfo, Counterparty counterparty, out string errorMessage)
        {
            if (orderRequestInfo == OrderRequestInfo.INVALID_ORDER_REQUEST || orderRequestInfo == null)
            {
                errorMessage = "Attempt ot send INVALID_ORDER_REQUEST (probably created from unmatching quote)";
                this.Logger.Log(LogLevel.Error, errorMessage);
                return null;
            }

            if (this.State != SessionState.Running)
            {
                errorMessage = string.Format("Cannot send orders in current orderflow session state: [{0}]", this.State);
                this.Logger.Log(LogLevel.Error, errorMessage);
                return null;
            }

            if (this.GetIsStopInProgress(false))
            {
                errorMessage = "Cannot submit orders as session is currently stopping";
                this.Logger.Log(LogLevel.Error, errorMessage);
                return null;
            }

            errorMessage = null;
            string uniqueInternalIdentity;
            string orderIdentity = this.FormatIdentityString(orderRequestInfo.Symbol, orderRequestInfo.Side, out uniqueInternalIdentity);
            WritableOrder writableOrder = new WritableOrder(
                orderIdentity: orderIdentity,
                uniqueInternalIdentity: uniqueInternalIdentity,
                orderRequestInfo: orderRequestInfo,
                counterparty: counterparty
                );

            return writableOrder;
        }

        protected delegate bool SendMessageFunc(OrderRequestInfo newOrderInfo, string newOrderIdentity, out string outgoingMessageRaw);

        public virtual bool DealRejectionSupported { get { return false; } }

        protected SubmissionResult SubmitOrder(IIntegratorOrderExternal newOrderExternal, SendMessageFunc sendMessageFunc)
        {
            WritableOrder writableOrder = newOrderExternal as WritableOrder;

            if (writableOrder == null)
            {
                this.Logger.Log(LogLevel.Error, "Couldn't submit external order as it is null or of an incorrect type");
                return SubmissionResult.Failed_OrderTypeIncorrect;
            }

            if (this.GetIsStopInProgress(false))
            {
                this.Logger.Log(LogLevel.Error, "Cannot submit orders as session is currently stopping");
                return SubmissionResult.Failed_SessionNotRunning;
            }

            decimal nopEstimate = 0;
            RiskManagementCheckResult riskResult = this.DealRejectionSupported
                ? this._riskManager.CheckIfSoftPriceAllowed(newOrderExternal)
                : this._riskManager.AddExternalOrderAndCheckIfAllowed(newOrderExternal, out nopEstimate);

            if (riskResult != RiskManagementCheckResult.Accepted)
            {
                LogLevel logLevel = LogLevel.Fatal;
                if(riskResult == RiskManagementCheckResult.Rejected_GoFlatOnlyEnabled && HighResolutionDateTime.UtcNow - this._riskManager.GoFlatOnlyLastEnabledTime < TimeSpan.FromMilliseconds(5))
                    logLevel = LogLevel.Error;
                this.Logger.Log(logLevel, "External order [{0}] was not allowed by risk manager ({1}), throwing it away", newOrderExternal.Identity, riskResult);
                return riskResult.CanBeRetryed() ? SubmissionResult.Failed_RiskManagementDisallowedCanBeRetryed : SubmissionResult.Failed_RiskManagementDisallowed;
            }

            //We add it to dictionary BEFORE sending in order to prevent potential race condition (exec report coming before we put it in the dic)
            if (!_openedOrdersMap.TryAdd(writableOrder.Identity, writableOrder))
            {
                this.Logger.Log(LogLevel.Warn, "Couldn't add external order [{0}] to internal store", writableOrder.Identity);
                this._riskManager.UpdateStateOfExternalOrder(newOrderExternal, new OrderChangeEventArgs(IntegratorOrderExternalChange.NotSubmitted));
                return SubmissionResult.Failed_InternalError;
            }

            writableOrder.OrderStatus = IntegratorOrderExternalStatus.Submitted;

            bool successfulySent = false;
            string outgoingFixMessageRaw = null;
            try
            {
                if (OrderSubmitted != null)
                    OrderSubmitted(newOrderExternal);
                successfulySent = sendMessageFunc(writableOrder.OrderRequestInfo, writableOrder.Identity,
                                                  out outgoingFixMessageRaw);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Error, e, "Adapter for {0} failed to send an order message [{1}]", this.Counterparty, newOrderExternal);
            }

            if (successfulySent)
            {
                DateTime now = HighResolutionDateTime.UtcNow;
                if (newOrderExternal.OrderRequestInfo.TimeInForce == TimeInForce.ImmediateOrKill)
                {
                    //Small potential for race condition is fine here as we are fine with order without timeout in case it was executed promptly
                    writableOrder.OnTimeout += HandleOrderTimeout;
                    writableOrder.SetTimeout(this._settings.UnconfirmedOrderTimeout);
                }

                if (newOrderExternal.OrderRequestInfo.OrderType == OrderType.Quoted)
                {
                    this.Logger.Log(LogLevel.Info,
                                    "Price received to Order [{0}] Sent integrator delay: {1} (= {2} - {3})",
                                    writableOrder.Identity,
                                    now - writableOrder.OrderRequestInfo.IntegratorPriceReceivedUtc,
                                    now.ToString("HH:mm:ss.fffffff"),
                                    writableOrder.OrderRequestInfo.IntegratorPriceReceivedUtc.ToString(
                                        "HH:mm:ss.fffffff"));
                    writableOrder.QuoteReceivedToOrderSentInternalLatency = now -
                                                                             writableOrder.OrderRequestInfo.IntegratorPriceReceivedUtc;
                }

                writableOrder.IntegratorSentTimeUtc = now;
                writableOrder.OutgoingFixMessageRaw = outgoingFixMessageRaw;
                writableOrder.IsActualizedWithInfoAboutSending = true;

                this.IntegratorPerfCounter.ExternalOrderSent();
                if (!this.DealRejectionSupported)
                    this.IntegratorPerfCounter.NopEstimateChanged(nopEstimate);
                this._dealStatisticsConsumer.SentOrderExternal(writableOrder);
                return SubmissionResult.Success;
            }
            else
            {
                writableOrder.OrderStatus = IntegratorOrderExternalStatus.NotSubmitted;
                WritableOrder removed;
                if (!_openedOrdersMap.TryRemove(writableOrder.Identity, out removed))
                {
                    this.Logger.Log(LogLevel.Error,
                                    "Failed during removing failed external order [{0}] from internal store",
                                    writableOrder.Identity);
                }
                this.Logger.Log(LogLevel.Error,
                             "Couldn't send order [{0}] due to messaging layer errors (lost connection?). Order can be resubmitted.",
                             writableOrder.Identity);
                this._riskManager.UpdateStateOfExternalOrder(newOrderExternal, new OrderChangeEventArgs(IntegratorOrderExternalChange.NotSubmitted));
                if (OrderNotActive != null)
                    OrderNotActive(newOrderExternal, HighResolutionDateTime.UtcNow);
                return SubmissionResult.Failed_MessagingLayerError;
            }
        }


        public SubmissionResult SubmitOrder(IIntegratorOrderExternal newOrderExternal)
        {
            return this.SubmitOrder(newOrderExternal, this._orderFlowMessagingAdapter.SendOrderMessage);
        }

        //this is called only from prototype testing projects
        public IIntegratorOrderExternal SendOrder(OrderRequestInfo orderRequestInfo)
        {
            string errorMessage;
            IIntegratorOrderExternal newOrder = this.GetNewExternalOrder(orderRequestInfo, out errorMessage);
            if (this.SubmitOrder(newOrder) == SubmissionResult.Success)
            {
                return newOrder;
            }
            else
            {
                return null;
            }
        }

        private bool CheckUsedPrice(ExecutionInfo executionInfo, WritableOrder writableOrder)
        {
            if (executionInfo.UsedPrice.HasValue)
            {
                decimal usedPrice = executionInfo.UsedPrice.Value;
                if (usedPrice != writableOrder.OrderRequestInfo.RequestedPrice)
                {
                    if (usedPrice <= 0)
                    {
                        this.Logger.Log(LogLevel.Fatal,
                                    "Invalid price ({0}) in exec report for Order {1}. Inoring the whole execution report.",
                                    usedPrice, writableOrder.Identity);
                        return false;
                    }

                    if (writableOrder.OrderRequestInfo.Side == DealDirection.Buy
                            ? usedPrice < writableOrder.OrderRequestInfo.RequestedPrice
                            : usedPrice > writableOrder.OrderRequestInfo.RequestedPrice)
                    {
                        this.Logger.Log(LogLevel.Info,
                                        "Order [{0}], [{1}] created with request for price [{2}] but better price [{3}] was used instead",
                                        writableOrder.Identity, writableOrder.UniqueInternalIdentity, writableOrder.OrderRequestInfo.RequestedPrice,
                                        usedPrice);
                    }

                    if (writableOrder.OrderRequestInfo.Side == DealDirection.Buy
                            ? usedPrice > writableOrder.OrderRequestInfo.RequestedPrice
                            : usedPrice < writableOrder.OrderRequestInfo.RequestedPrice)
                    {
                        this.Logger.Log(LogLevel.Fatal,
                                        "Order [{0}], [{1}] created with request for price [{2}] but WORSE price [{3}] was used instead",
                                        writableOrder.Identity, writableOrder.UniqueInternalIdentity, writableOrder.OrderRequestInfo.RequestedPrice,
                                        usedPrice);
                    }
                }
            }
            else
            {
                this.Logger.Log(LogLevel.Fatal,
                                "Order [{0}], [{1}] created with request for price [{2}] but used price is unknown",
                                writableOrder.Identity, writableOrder.UniqueInternalIdentity, writableOrder.OrderRequestInfo.RequestedPrice);
                executionInfo.SetUsedPrice(writableOrder.OrderRequestInfo.RequestedPrice);
            }
            return true;
        }

        private bool CheckAndRetriveFilledAmount(ExecutionInfo executionInfo, WritableOrder writableOrder, out decimal filledAmount)
        {
            if (executionInfo.FilledAmount.HasValue)
            {
                filledAmount = executionInfo.FilledAmount.Value;

                if (filledAmount <= 0)
                {
                    this.Logger.Log(LogLevel.Fatal,
                                "Invalid filled amount ({0}) in exec report for Order {1}. Inoring the whole execution report.",
                                filledAmount, writableOrder.Identity);
                    return false;
                }
            }
            else
            {
                this.Logger.Log(LogLevel.Fatal,
                                "Filled amount not specified in exec report for Order {0}. Requested size ({1}) will be used.",
                                writableOrder.Identity, writableOrder.OrderRequestInfo.SizeBaseAbsInitial);
                filledAmount = writableOrder.OrderRequestInfo.SizeBaseAbsInitial;
                executionInfo.SetFilledAmount(filledAmount);
            }

            return true;
        }

        private bool CheckAndAddFilledAmount(ExecutionInfo executionInfo, WritableOrder writableOrder)
        {
            decimal filledAmount;
            this.CheckAndRetriveFilledAmount(executionInfo, writableOrder, out filledAmount);

            writableOrder.FilledAmount += filledAmount;

            if (writableOrder.HasPendingLLDeals)
            {
                writableOrder.NotToBeFilledAmount -= filledAmount;
                writableOrder.RemovePendingLLDeal();
            }

            if (writableOrder.FilledAmount + writableOrder.NotToBeFilledAmount >
                writableOrder.OrderRequestInfo.SizeBaseAbsInitial)
            {
                this.Logger.Log(LogLevel.Fatal,
                                "Order {0} received exec reports (last was fill) for more ({2}) than what was originally requested: {1}.",
                                writableOrder.Identity, writableOrder.OrderRequestInfo.SizeBaseAbsInitial, writableOrder.FilledAmount + writableOrder.NotToBeFilledAmount);
            }

            return true;
        }

        private bool CheckAndSubstractRejectedAmount(RejectionInfo rejectionInfo, WritableOrder writableOrder)
        {
            decimal rejectedAmount;
            if (rejectionInfo.RejectedAmount.HasValue)
            {
                rejectedAmount = rejectionInfo.RejectedAmount.Value;
            }
            else
            {
                rejectedAmount = writableOrder.OrderRequestInfo.SizeBaseAbsInitial - writableOrder.FilledAmount -
                                 writableOrder.NotToBeFilledAmount;
            }

            if (rejectionInfo.IsLLContractReject)
            {
                writableOrder.NotToBeFilledAmount -= rejectedAmount;
                writableOrder.RemovePendingLLDeal();
            }
            else
            {
                //check that we don't cancel more than whats remaining
                decimal remainingAmount = writableOrder.OrderRequestInfo.SizeBaseAbsInitial - writableOrder.FilledAmount -
                                          writableOrder.NotToBeFilledAmount;

                if (rejectedAmount > remainingAmount)
                {
                    this.Logger.Log(LogLevel.Warn,
                    "Order {0} received reject exec ({1}) report for more ({2}) than what is remaining available size: {3}. Adjusting the rejected size to remaining size. This is expected if originally replaced order LL rejected the exceeding amount. Or if Cancellation came during pending LL",
                    writableOrder.Identity, rejectionInfo.RejectionType, rejectedAmount, remainingAmount);

                    rejectionInfo.ForceSetRejectedAmount(remainingAmount);
                    rejectedAmount = remainingAmount;
                }
                else if (rejectedAmount < remainingAmount &&
                         rejectionInfo.RejectionType != RejectionType.InternalRejectTriggeredByReplace)
                {
                    this.Logger.Log(LogLevel.Warn,
                    "Order {0} received reject exec ({1}) report for less ({2}) than what is remaining available size: {3}. Adjusting the rejected size to remaining size. This is expected if originally replaced order had pending LL during replace.",
                    writableOrder.Identity, rejectionInfo.RejectionType, rejectedAmount, remainingAmount);

                    rejectionInfo.ForceSetRejectedAmount(remainingAmount);
                    rejectedAmount = remainingAmount;
                }

            }

            //CancelReplaceReject are allowed to be for 0, otherwise needs to be nonzero
            if (rejectedAmount < 0)
            {
                this.Logger.Log(LogLevel.Fatal,
                                "Invalid rejected amount ({0}) in exec report for Order {1}. Inoring the whole execution report.",
                                rejectedAmount, writableOrder.Identity);
                return false;
            }
            //CancelReplaceReject are allowed to be for 0, otherwise needs to be nonzero
            else if (rejectionInfo.RejectionType != RejectionType.CancelReplaceReject && rejectedAmount == 0)
            {
                this.Logger.Log(LogLevel.Fatal,
                                "Invalid rejected amount ({0}) in exec report for Order {1}. Passing the execution report further to propagate rejection info.",
                                rejectedAmount, writableOrder.Identity);
            }

            rejectionInfo.SetRejectedAmount(rejectedAmount);

            writableOrder.NotToBeFilledAmount += rejectedAmount;

            if (rejectionInfo.RejectionDirection == RejectionDirection.KGTRejectd)
            {
                writableOrder.KgtRejectedAmount += rejectedAmount;
            }

            if (writableOrder.FilledAmount + writableOrder.NotToBeFilledAmount >
                writableOrder.OrderRequestInfo.SizeBaseAbsInitial + writableOrder.KgtRejectedAmount)
            {
                this.Logger.Log(LogLevel.Error,
                    "Order {0} received exec (last was reject) reports for more ({2}) than what was originally requested: {1} + Kgt rejected {3}. This is expected if originally replaced order LL rejected the exceeding amount. Or if Cancellation came during pending LL",
                    writableOrder.Identity, writableOrder.OrderRequestInfo.SizeBaseAbsInitial,
                    writableOrder.FilledAmount + writableOrder.NotToBeFilledAmount, writableOrder.KgtRejectedAmount);
            }

            return true;
        }

        protected virtual bool HandleNonconfirmedDeal(OrderChangeInfo orderChangeInfo, WritableOrder writableOrder)
        {
            this.Logger.Log(LogLevel.Fatal,
                            "Received nonconfirmed deal [{0}] on a session that is not enabled for LL confirmation",
                            writableOrder.Identity);
            return false;
        }

        protected virtual bool HandleDealAckConfirmation(OrderChangeInfo orderChangeInfo, WritableOrder writableOrder)
        {
            this.Logger.Log(LogLevel.Fatal,
                            "Received deal requiring Ack [{0}] on a session that is not enabled for LL confirmation",
                            writableOrder.Identity);
            return false;
        }

        //protected virtual void HandleRequestedDealReject(OrderChangeInfo orderChangeInfo, WritableOrder writableOrder)
        //{
        //    this.Logger.Log(LogLevel.Fatal,
        //                    "Received confirmed deal reject [{0}] on a session that is not enabled for LL confirmation",
        //                    writableOrder.Identity);
        //}

        private void HandleOrderChanged(OrderChangeInfo orderChangeInfo)
        {
            WritableOrder writableOrder;
            if (this._openedOrdersMap.TryGetValue(orderChangeInfo.Identity, out writableOrder))
            {
                this.Logger.Log(LogLevel.Info, "Order [{0}], [{1}] flipped to {2} state. OrderSent to ExecutionReport Received counterparty delay: {3} (= {4} - {5})",
                                orderChangeInfo.Identity, writableOrder.UniqueInternalIdentity, orderChangeInfo.ChangeEventArgs.ChangeState,
                                orderChangeInfo.IntegratorReceivedTimeUtc - writableOrder.IntegratorSentTimeUtc,
                                orderChangeInfo.IntegratorReceivedTimeUtc.ToString("HH:mm:ss.fffffff"),
                                writableOrder.IntegratorSentTimeUtc.ToString("HH:mm:ss.fffffff"));
                orderChangeInfo.UniqueInternalIdentity = writableOrder.UniqueInternalIdentity;
                orderChangeInfo.Validate(this.Logger, this.Counterparty);
                if (!string.IsNullOrEmpty(orderChangeInfo.RemotePlatformOrderId))
                    writableOrder.RemotePlatformOrderId = orderChangeInfo.RemotePlatformOrderId;
                this._delayEvaluator.CheckReceivedObjectDelayedWithNoStatsUpdate(orderChangeInfo);

                switch (orderChangeInfo.ChangeEventArgs.ChangeState)
                {

                    case IntegratorOrderExternalChange.Submitted:
                        this.Logger.Log(LogLevel.Fatal,
                                 "Receiving OrderChanged event with Submitted state - likely a refactoring mismatch. Passing the update through. {0}",
                                 orderChangeInfo);
                        break;

                    case IntegratorOrderExternalChange.ConfirmedNew:
                        bool closeTheOrder = false;
                        if (orderChangeInfo.InitaialConfirmedAmount.HasValue &&
                            orderChangeInfo.InitaialConfirmedAmount !=
                            writableOrder.OrderRequestInfo.SizeBaseAbsInitial)
                        {
                            if (orderChangeInfo.InitaialConfirmedAmount >
                                writableOrder.OrderRequestInfo.SizeBaseAbsInitial)
                            {
                                this.Logger.Log(LogLevel.Fatal,
                                                "Order [{0}] created for size {1}, but confirmed initial size is greater: {2}. Ignoring the greater size",
                                                writableOrder.Identity,
                                                writableOrder.OrderRequestInfo.SizeBaseAbsInitial,
                                                orderChangeInfo.InitaialConfirmedAmount);
                            }
                            else
                            {
                                writableOrder.IntegratorReceivedResultTimeUtc = orderChangeInfo.IntegratorReceivedTimeUtc;
                                writableOrder.CounterpartySentResultTimeUtc = orderChangeInfo.IntegratorReceivedTimeUtc;
                                closeTheOrder = orderChangeInfo.InitaialConfirmedAmount <= 0;
                                IntegratorOrderExternalStatus nextState = closeTheOrder
                                                                              ? IntegratorOrderExternalStatus.Rejected
                                                                              : IntegratorOrderExternalStatus.ConfirmedNew;
                                decimal rejectedAmout = writableOrder.OrderRequestInfo
                                                                      .SizeBaseAbsInitial -
                                                        orderChangeInfo.InitaialConfirmedAmount.Value;
                                writableOrder.NotToBeFilledAmount += rejectedAmout; 
                                //internaly reject the reminder of the size
                                FlipOrderState(writableOrder, nextState,
                                               new OrderChangeEventArgs(IntegratorOrderExternalChange.Rejected,
                                                   new RejectionInfo(
                                                       "<Integrator Internal Reject caused by external partial replacement>", 
                                                       rejectedAmout,
                                                       RejectionType.InternalRejectTriggeredByReplace)), orderChangeInfo.CounterpartySentTimeUtc,
                                               closeTheOrder);
                            }
                        }

                        // Unless the order was initially fully rejected (eg by c/replace to lower size after it was partially filled)
                        //  then flip it to submitted (even after partiall internal reject) - so that triggers can consider order ACKed
                        // However we don't want to ACK before partiall internal reject (as we'd consider the order full)
                        // Also do not Ack already acked order (some destinations have pending new and new)
                        if (!closeTheOrder && writableOrder.OrderStatus != IntegratorOrderExternalStatus.ConfirmedNew)
                        {
                            //TODO: HACK: Hardcoded - should be performed probably by connection adapter
                            if (this.Counterparty == Counterparty.L01 &&
                                orderChangeInfo.IntegratorReceivedTimeUtc - writableOrder.IntegratorSentTimeUtc >
                                TimeSpan.FromMilliseconds(15) && !SettingsInitializator.Instance.IsUat)
                            {
                                this.Logger.Log(LogLevel.Error,
                                                "L01 confirming order [{0}] with delay of [{1}]. KGT receiving delay: [{2}]",
                                                writableOrder.Identity,
                                                orderChangeInfo.IntegratorReceivedTimeUtc - writableOrder.IntegratorSentTimeUtc,
                                                orderChangeInfo.IntegratorReceivedTimeUtc - orderChangeInfo.CounterpartySentTime);
                            }

                            this.FlipOrderState(writableOrder, IntegratorOrderExternalStatus.ConfirmedNew,
                                                orderChangeInfo.ChangeEventArgs, orderChangeInfo.CounterpartySentTimeUtc, false);
                        }

                        break;
                    case IntegratorOrderExternalChange.PendingCancel:
                        break;

                    case IntegratorOrderExternalChange.NonConfirmedDeal:
                        decimal unused;
                        if (!CheckUsedPrice(orderChangeInfo.ChangeEventArgs.ExecutionInfo, writableOrder) ||
                            !CheckAndRetriveFilledAmount(orderChangeInfo.ChangeEventArgs.ExecutionInfo, writableOrder, out unused))
                            break;
                        writableOrder.IntegratorReceivedResultTimeUtc = orderChangeInfo.IntegratorReceivedTimeUtc;
                        if (orderChangeInfo.CounterpartySentTime != null)
                            writableOrder.CounterpartySentResultTimeUtc = orderChangeInfo.CounterpartySentTime.Value;
                        writableOrder.CounterpartyTransactionTimeUtc =
                            orderChangeInfo.ChangeEventArgs.ExecutionInfo.CounterpartyTransactionTime;
                        orderChangeInfo.ChangeEventArgs.ExecutionInfo.UpdateIntegratorTransactionId(
                            writableOrder.GetNextDealId(orderChangeInfo.ChangeEventArgs.ExecutionInfo.CounterpartyTransactionId));
                        writableOrder.AddPendingLLDeal(this.HandleOrderTimeout);
                        writableOrder.NotToBeFilledAmount += orderChangeInfo.ChangeEventArgs.ExecutionInfo.FilledAmount.Value;
                        if(this.HandleNonconfirmedDeal(orderChangeInfo, writableOrder))
                            this.FlipOrderState(writableOrder, IntegratorOrderExternalStatus.NonConfirmedDeal,
                                                orderChangeInfo.ChangeEventArgs, orderChangeInfo.CounterpartySentTimeUtc, false);
                        break;

                    case IntegratorOrderExternalChange.Filled:
                    case IntegratorOrderExternalChange.PartiallyFilled:
                        if (!CheckUsedPrice(orderChangeInfo.ChangeEventArgs.ExecutionInfo, writableOrder) ||
                            !CheckAndAddFilledAmount(orderChangeInfo.ChangeEventArgs.ExecutionInfo, writableOrder))
                            break;
                        writableOrder.IntegratorReceivedResultTimeUtc = orderChangeInfo.IntegratorReceivedTimeUtc;
                        if (orderChangeInfo.CounterpartySentTime != null)
                            writableOrder.CounterpartySentResultTimeUtc = orderChangeInfo.CounterpartySentTime.Value;
                        writableOrder.CounterpartyTransactionTimeUtc =
                            orderChangeInfo.ChangeEventArgs.ExecutionInfo.CounterpartyTransactionTime;
                        orderChangeInfo.ChangeEventArgs.ExecutionInfo.UpdateIntegratorTransactionId(
                            writableOrder.GetNextDealId(orderChangeInfo.ChangeEventArgs.ExecutionInfo.CounterpartyTransactionId));
                        FlipOrderState(writableOrder,
                                       orderChangeInfo.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.Filled
                                           ? IntegratorOrderExternalStatus.Filled
                                           : IntegratorOrderExternalStatus.PartiallyFilled,
                                       orderChangeInfo.ChangeEventArgs, orderChangeInfo.CounterpartySentTimeUtc, !writableOrder.HasRemainingAmount);
                        this._dealStatisticsConsumer.ExternalDealExecuted(orderChangeInfo, writableOrder);

                        //only confirm aggressive trades on rejectable session
                        if (this.DealRejectionSupported && (orderChangeInfo.ChangeEventArgs.ExecutionInfo.FlowSide != FlowSide.LiquidityMaker))
                        {
                            if(!this.HandleDealAckConfirmation(orderChangeInfo, writableOrder))
                                this.Logger.Log(LogLevel.Fatal, "Error during immediate mandatory confirmation of deal [{0}]", writableOrder.Identity);
                        }

                        break;
                    case IntegratorOrderExternalChange.Cancelled:
                        if(!CheckAndSubstractRejectedAmount(orderChangeInfo.ChangeEventArgs.RejectionInfo, writableOrder))
                            break;

                        writableOrder.IntegratorReceivedResultTimeUtc = orderChangeInfo.IntegratorReceivedTimeUtc;
                        if (orderChangeInfo.CounterpartySentTime != null)
                            writableOrder.CounterpartySentResultTimeUtc = orderChangeInfo.CounterpartySentTime.Value;
                        writableOrder.SetOrderCancellationDone();
                        FlipOrderState(writableOrder, IntegratorOrderExternalStatus.Cancelled,
                                       orderChangeInfo.ChangeEventArgs, orderChangeInfo.CounterpartySentTimeUtc, !writableOrder.HasRemainingAmount);
                        this._dealStatisticsConsumer.ExternalDealCancelAcked(orderChangeInfo, writableOrder);
                        break;
                    case IntegratorOrderExternalChange.Rejected:

                        if (CheckAndSubstractRejectedAmount(orderChangeInfo.ChangeEventArgs.RejectionInfo,
                                                             writableOrder))
                        {
                            //WARNING: No removal from risk mgmt needed - we add it only after final confirmation from cpt

                            writableOrder.IntegratorReceivedResultTimeUtc = orderChangeInfo.IntegratorReceivedTimeUtc;
                            if (orderChangeInfo.CounterpartySentTime != null)
                                writableOrder.CounterpartySentResultTimeUtc = orderChangeInfo.CounterpartySentTime.Value;
                            FlipOrderState(writableOrder, IntegratorOrderExternalStatus.Rejected,
                                           orderChangeInfo.ChangeEventArgs, orderChangeInfo.CounterpartySentTimeUtc, !writableOrder.HasRemainingAmount);
                            if (orderChangeInfo.ChangeEventArgs.RejectionInfo.RejectionType ==
                                RejectionType.CancelReplaceReject)
                                this._dealStatisticsConsumer.ExternalDealCancelAcked(orderChangeInfo, writableOrder);
                            else
                                this._dealStatisticsConsumer.ExternalDealRejected(orderChangeInfo, writableOrder);
                        }

                        //perform rejection check and possible session stop AFTER the order is deregistered
                        this.HandleExternalOrderRejection(orderChangeInfo.ChangeEventArgs.RejectionInfo, writableOrder.Counterparty);

                        break;
                    case IntegratorOrderExternalChange.CancelRejected:
                        writableOrder.SetOrderCancellationDone();
                        bool removeOrder =
                            writableOrder.OrderStatus == IntegratorOrderExternalStatus.Cancelled ||
                            //order can by partially rejected due to LL or ECN reject
                            (writableOrder.OrderStatus == IntegratorOrderExternalStatus.Rejected && !writableOrder.HasRemainingAmount)||
                            writableOrder.OrderStatus == IntegratorOrderExternalStatus.Filled ||
                            (writableOrder.OrderStatus == IntegratorOrderExternalStatus.PartiallyFilled &&
                             !writableOrder.HasRemainingAmount);
                        FlipOrderState(writableOrder, writableOrder.OrderStatus, orderChangeInfo.ChangeEventArgs, orderChangeInfo.CounterpartySentTimeUtc, removeOrder);
                        this._dealStatisticsConsumer.ExternalDealCancelAcked(orderChangeInfo, writableOrder);
                        break;
                    case IntegratorOrderExternalChange.InBrokenState:
                    case IntegratorOrderExternalChange.NotSubmitted:
                    default:
                        this.Logger.Log(LogLevel.Fatal,
                                 "Receiving OrderChanged event with unexpected state - likely an unexpected message was received from counterpart. {0}",
                                 orderChangeInfo);

                        DateTime orderBrokenTimeUtc = HighResolutionDateTime.UtcNow;
                        writableOrder.SetRestAmountBroken();
                        FlipOrderState(writableOrder, IntegratorOrderExternalStatus.InBrokenState,
                                       new OrderChangeEventArgs(IntegratorOrderExternalChange.InBrokenState), null, true);
                        this._dealStatisticsConsumer.ExternalDealIgnored(orderBrokenTimeUtc, writableOrder);
                        this._riskManager.TurnOnGoFlatOnly("Order in broken or unknown status");

                        break;
                }

                this._dealStatisticsConsumer.ExecutionReportReceived(orderChangeInfo, writableOrder);
            }
            else
            {
                if (orderChangeInfo.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.Filled ||
                    orderChangeInfo.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.PartiallyFilled ||
                    orderChangeInfo.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.NonConfirmedDeal)
                {
                    this.Logger.Log(LogLevel.Fatal, "Receiving [{0}] for unknown order. This might lead to HIDDEN OPEN POSITION. Stopping session and should be investigated. {1}",
                        orderChangeInfo.ChangeEventArgs.ChangeState, orderChangeInfo);
                    this.StopAsync(TimeSpan.FromSeconds(5));
                    this._riskManager.TurnOnGoFlatOnly("Receiving fill (might be rejectable) on unknown order");

                    if(orderChangeInfo.ChangeEventArgs.ExecutionInfo != null)
                        this._dealStatisticsConsumer.UnexpectedDeal(orderChangeInfo, this.Counterparty);
                    else
                        this.Logger.Log(LogLevel.Fatal, "Receiving info about execution but no ExecutionInfo is populated. THIS MUST BE MANUALLY INVESTIGATED");

                    return;
                }

                if ((orderChangeInfo.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.PendingCancel &&
                    orderChangeInfo.Identity.Equals("0"))
                    ||
                    (orderChangeInfo.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.Cancelled &&
                    orderChangeInfo.Identity.Equals("OPEN_ORDER")))
                {
                    this.Logger.Log(LogLevel.Info, "Receiving (pending) cancel for order [{0}] - this likely means that all client orders are going to be cancelled", orderChangeInfo.Identity);
                }
                else if (orderChangeInfo.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.PendingCancel &&
                    this.Counterparty.TradingTargetType == TradingTargetType.Hotspot)
                {
                    this.Logger.Log(LogLevel.Error, "Receiving pending cancel for unknown order [{0}] - this is likely known Hotspot updates ordering issue", orderChangeInfo.Identity);
                }
                else if (!IsKnownOperation(orderChangeInfo.Identity))
                {
                    if (orderChangeInfo.ChangeEventArgs.ChangeState == IntegratorOrderExternalChange.CancelRejected &&
                        this.Counterparty.TradingTargetType == TradingTargetType.Hotspot)
                    {
                        this.Logger.Log(LogLevel.Error,
                            "Receiving cancel rejected for unknown order [{0}] - this is likely known Hotspot case of earlier auto-canceling LL-rejected order",
                            orderChangeInfo.Identity);
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Fatal,
                            "Receiving OrderChanged event for unknow order (possible result of abnormal session disconnect or order timeout): {0}",
                            orderChangeInfo);

                    }
                }
            }
        }

        private void HandleExternalOrderRejection(RejectionInfo rejectionInfo, Counterparty counterparty)
        {
            try
            {
                HandleExternalOrderRejectionInternal(rejectionInfo, counterparty);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, e,
                    "Session {0} experiencing unexpected exception during rejection handling. Delay stopping it",
                    counterparty);
                if (!this.GetIsStopInProgress(false))
                {
                    this.StopAsync(TimeSpan.FromSeconds(30));
                    this._riskManager.TurnOnGoFlatOnly("Session level rejection rate check exceeded");
                }
            }  
        }

        private void HandleExternalOrderRejectionInternal(RejectionInfo rejectionInfo, Counterparty counterparty)
        {
            if (!rejectionInfo.IsGenuineReject)
            {
                if (this._cancelReplaceRejectionRateChecker != null &&
                    !this._cancelReplaceRejectionRateChecker[(int)counterparty - (int)this.Counterparty].AddNextEventAndCheckIsAllowed())
                {
                    this.Logger.Log(LogLevel.Fatal,
                                    "Session {0} exceeded controlled rate of cancel/replace (or other non-genuine) rejections. Not doing anything - just reporting",
                                    counterparty);
                    this._cancelReplaceRejectionRateChecker[(int)counterparty - (int)this.Counterparty].Reset();
                }
            }
            else
            {
                RemovableEventsRateChecker rejectionChecker = SelectExternalOrderRejectionChecker(rejectionInfo, counterparty);
                if (rejectionChecker != null && !rejectionChecker.AddNextEventAndCheckIsAllowed())
                {
                    this.Logger.Log(LogLevel.Fatal,
                                    "Session {0} exceeded allowed rate of order rejections (encountered {1} rejections per configured time interval - [{2}]), stopping the session",
                                    counterparty, rejectionChecker.MaximumAllowedEventsPerInterval, rejectionChecker.TimeIntervalToCheck);
                    if (!this.GetIsStopInProgress(false))
                    {
                        this.StopAsync(TimeSpan.FromSeconds(30));
                        this._riskManager.TurnOnGoFlatOnly("Session level rejection rate check exceeded");
                    }
                }
            }
        }

        private RemovableEventsRateChecker SelectExternalOrderRejectionChecker(RejectionInfo rejectionInfo, Counterparty counterparty)
        {
            if (_specialCaseRejectionRateCheckers == null || rejectionInfo == null || string.IsNullOrEmpty(rejectionInfo.RejectionReason))
                return this._defaultRejectionRateChecker == null ? null : this._defaultRejectionRateChecker[(int)counterparty - (int)this.Counterparty];

            var checker =
                _specialCaseRejectionRateCheckers[(int)counterparty - (int)this.Counterparty].Where(
                    tuple =>
                    rejectionInfo.RejectionReason.IndexOf(tuple.Item1, StringComparison.InvariantCultureIgnoreCase) >= 0)
                                                 .Select(t => t.Item2)
                                                 .FirstOrDefault();

            return checker ?? this._defaultRejectionRateChecker[(int)counterparty - (int)this.Counterparty];
        }

        protected virtual bool IsKnownOperation(string operartionIdenetity)
        {
            return false;
        }

        private void HandleOrderTimeout(WritableOrder order)
        {
            DateTime orderBrokenTimeUtc = HighResolutionDateTime.UtcNow;
            if (order.HasPendingLLDeals)
                this.Logger.Log(LogLevel.Fatal, "Order is timing out - it has {1} unconfirmed LL deal(s). {0}", order, order.PendingLLsCountInternal);
            else
                this.Logger.Log(LogLevel.Fatal, "Order is timing out (likely because missing fill/reject message). {0}", order);

            FlipOrderState(order, IntegratorOrderExternalStatus.InBrokenState, new OrderChangeEventArgs(IntegratorOrderExternalChange.InBrokenState), null,  true);
            this._dealStatisticsConsumer.ExternalDealIgnored(orderBrokenTimeUtc, order);
        }

        protected void FlipOrderState(WritableOrder order, IntegratorOrderExternalStatus orderStatus, OrderChangeEventArgs orderChangeEventArgs, DateTime? counterpartySentTimeUtc, bool close)
        {
            if (orderStatus == IntegratorOrderExternalStatus.InBrokenState)
            {
                //First of all prevent trading on this session and flip GFO
                this.StopAsync(TimeSpan.FromSeconds(15));
                this._riskManager.TurnOnGoFlatOnly("Broken order");
            }

            order.OrderStatus = orderStatus;

            if (close)
            {

                if (!order.HasPendingLLDeals && order.HasRemainingAmount)
                {
                    this.Logger.Log(LogLevel.Fatal,
                                     "Order [{0}], [{1}] created with request for amount [{2}] but amount [{3}] was filled and amount [{4}] marekd as not filled. Order is in {5} state",
                                     order.Identity, order.UniqueInternalIdentity, order.OrderRequestInfo.SizeBaseAbsInitial,
                                     order.FilledAmount, order.NotToBeFilledAmount, order.OrderStatus);
                }

                if (order.HasPendingLLDeals && orderStatus != IntegratorOrderExternalStatus.InBrokenState)
                {
                    this.Logger.Log(LogLevel.Info,
                                    "Not yet removing Order [{0}] from internal store as it still has {1} pending LL deals",
                                    order.Identity, order.PendingLLsCountInternal);
                }
                else if (!order.TryCloseIfNoPendingCancellations(orderStatus == IntegratorOrderExternalStatus.InBrokenState))
                {
                    this.Logger.Log(LogLevel.Info,
                                    "Not yet removing Order [{0}] from internal store as it still has pending cancellation operation",
                                    order.Identity);
                }
                else
                {
                    RemoveOrderFromMappings(order.Identity);
                    if (OrderNotActive != null)
                        OrderNotActive(order, counterpartySentTimeUtc.HasValue ? counterpartySentTimeUtc.Value : HighResolutionDateTime.UtcNow);
                }
            }

            try
            {
                if(!this.DealRejectionSupported)
                    this._riskManager.UpdateStateOfExternalOrder(order, orderChangeEventArgs);
                if (orderChangeEventArgs.ChangeState == IntegratorOrderExternalChange.Filled ||
                    orderChangeEventArgs.ChangeState == IntegratorOrderExternalChange.PartiallyFilled)
                {
                    IntegratorDealDone deal = new IntegratorDealDone(this.Counterparty, order.Symbol,
                        orderChangeEventArgs.ExecutionInfo.SettlementDate.Value,
                        (orderChangeEventArgs.ExecutionInfo.CounterpartyTransactionTime ?? DateTime.UtcNow).Date,
                        orderChangeEventArgs.ExecutionInfo.SettlementTimeCounterpartyUtc,
                        orderChangeEventArgs.ExecutionInfo.FilledAmountBasePolarized,
                        orderChangeEventArgs.ExecutionInfo.FilledAmountTermPolarized);
                    this._riskManager.HandleDealDone(deal);
                }
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, e, "Unhandled exception during calling UpdateStateOfExternalOrder on risk manager");
            }

            if (this.OnOrderChanged != null)
            {
                try
                {
                    this.OnOrderChanged(order, orderChangeEventArgs);
                }
                catch (Exception e)
                {
                    this.Logger.LogException(LogLevel.Fatal, e, "Unhandled exception during IOrderFlowSession.OnOrderChanged");
                }
            }
            try
            {
                order.InvokeOnOrderChanged(orderChangeEventArgs);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, e, "Unhandled exception during IOrder OnOrderChanged");
            }
        }

        protected void RemoveOrderFromMappings(string orderIdentity)
        {
            this.Logger.Log(LogLevel.Info, "Removing Order [{0}] from internal store", orderIdentity);
            WritableOrder removed;
            if (!_openedOrdersMap.TryRemove(orderIdentity, out removed))
            {
                this.Logger.Log(LogLevel.Error,
                                "Failed during removing done external order [{0}] from internal store",
                                orderIdentity);
            }
        }

        private void InvokeOnBusinessReject(QuickFix.Message bussinessReject)
        {
            var openedOrders =
                _openedOrdersMap.Where(ord => ord.Value.OrderStatus == IntegratorOrderExternalStatus.Submitted).Select(o => o.Value).Take(2).ToList();

            if (openedOrders.Any())
            {
                //business rejection of order
                if (bussinessReject.IsSetField(QuickFix.Fields.Tags.RefMsgType) &&
                    bussinessReject.GetString(QuickFix.Fields.Tags.RefMsgType) == "D"
                    && bussinessReject.IsSetField(QuickFix.Fields.Tags.RefSeqNum))
                {
                    string seqNumPattern = string.Format("{0}34={1}{0}", QuickFix.Message.SOH,
                                                         bussinessReject.GetString(QuickFix.Fields.Tags.RefSeqNum));

                    var ordersToReject =
                        _openedOrdersMap.Where(ord => ord.Value.OutgoingFixMessageRaw.Contains(seqNumPattern))
                                        .Select(o => o.Value)
                                        .Take(2)
                                        .ToList();

                    if (ordersToReject.Count == 1)
                    {
                        WritableOrder orderToReject = ordersToReject.First();
                        this.Logger.Log(LogLevel.Info, "Finding single order unambigiously rejected by the business reject: {0}", orderToReject);

                        string rejectionReason = "<Integrator supplied reason: Business Reject>";
                        OrderChangeInfo ochi = new OrderChangeInfo(orderToReject.Identity, Common.Symbol.NULL,
                                                                   IntegratorOrderExternalChange.Rejected, null,
                                                                   null, null,
                                                                   new RejectionInfo(rejectionReason,
                                                                                     orderToReject.OrderRequestInfo
                                                                                                  .SizeBaseAbsInitial,
                                                                                     RejectionType.OrderReject),
                                                                   rejectionReason);
                        this.HandleOrderChanged(ochi);
                        return;
                    }
                }

                if (openedOrders.Count() > 1)
                {
                    this.Logger.Log(LogLevel.Error,
                                     "Receiving business reject while multiple open orders are unconfirmed. Cannot decide which to cancel");
                    return;
                }
                else
                {
                    WritableOrder openedOrder = openedOrders.First();
                    this.Logger.Log(LogLevel.Error,
                             "Receiving business reject while [{0}] is the only unconfirmed order - so cancelling it.", openedOrder.Identity);
                    DateTime orderBrokenTimeUtc = HighResolutionDateTime.UtcNow;
                    FlipOrderState(openedOrder, IntegratorOrderExternalStatus.InBrokenState, new OrderChangeEventArgs(IntegratorOrderExternalChange.InBrokenState), null, true);
                    this._dealStatisticsConsumer.ExternalDealIgnored(orderBrokenTimeUtc, openedOrder);

                    return;
                }
            }
            else
            {
                this.Logger.Log(LogLevel.Error,
                             "Receiving business reject but cannot find unconfirmed order to cancel.");
            }
        }

        public event Action<IIntegratorOrderExternal, OrderChangeEventArgs> OnOrderChanged;
        public event Action<IIntegratorOrderExternal> OrderSubmitted;
        public event Action<IIntegratorOrderExternal, DateTime> OrderNotActive;

        public IEnumerable<IIntegratorOrderExternal> ActiveOrderList
        {
            get { return this._openedOrdersMap.Values; }
        }

        protected class WritableOrder : IIntegratorOrderExternal
        {
            public string Identity { get; set; }
            public string UniqueInternalIdentity { get; set; }
            public IntegratorOrderExternalStatus OrderStatus { get; set; }
            public decimal FilledAmount { get; set; }
            public decimal NotToBeFilledAmount { get; set; }
            public decimal KgtRejectedAmount { get; set; }
            public OrderRequestInfo OrderRequestInfo { get; set; }
            public event Action<WritableOrder> OnTimeout;
            private event Action<WritableOrder> OnCancelOperationTimeout;
            public event Action<IIntegratorOrderExternal, OrderChangeEventArgs> OnOrderChanged;
            public DateTime IntegratorSentTimeUtc { get; set; }
            public TimeSpan? QuoteReceivedToOrderSentInternalLatency { get; set; }
            public string OutgoingFixMessageRaw { get; set; }
            public DateTime IntegratorReceivedResultTimeUtc { get; set; }
            public DateTime CounterpartySentResultTimeUtc { get; set; }
            public DateTime? CounterpartyTransactionTimeUtc { get; set; }
            public Counterparty Counterparty { get; set; }
            public bool IsActualizedWithInfoAboutSending { get; set; }
            public bool IsSentOrderPersisted { get; set; }
            public string RemotePlatformOrderId { get; set; }
            public IntegratorOrderInfoType IntegratorOrderInfoType
            {
                get { return IntegratorOrderInfoType.ExternalOrder; }
            }
            public bool IsRiskRemovingOrder { get { return this.OrderRequestInfo.IsRiskRemovingOrder; } }

            private SafeTimer _cancelOperationTimeoutTimer;
            private SafeTimer _timeoutTimer;
            private SafeTimer _llConfirmTimeoutTimer;
            private bool _closed = false;
            private int _dealsNum = -1;
            private List<Tuple<string, string>> _dealIdMappings = new List<Tuple<string, string>>();

            public WritableOrder(string orderIdentity, string uniqueInternalIdentity, OrderRequestInfo orderRequestInfo, Counterparty counterparty)
            {
                this.Identity = orderIdentity;
                this.UniqueInternalIdentity = uniqueInternalIdentity;
                this.OrderRequestInfo = orderRequestInfo;
                this.OrderStatus = IntegratorOrderExternalStatus.NotSubmitted;
                this.Counterparty = counterparty;
            }

            public bool HasRemainingAmount { get { return this.OrderRequestInfo.SizeBaseAbsInitial - this.FilledAmount - this.NotToBeFilledAmount > 0; } }

            internal void SetRestAmountBroken()
            {
                this.NotToBeFilledAmount += this.OrderRequestInfo.SizeBaseAbsInitial - this.FilledAmount;
            }

            public string GetNextDealId(string externalId)
            {
                string internalId = null;
                internalId = _dealIdMappings.Where(t => t.Item1 == externalId).Select(t => t.Item2).FirstOrDefault();

                if (internalId == null)
                {
                    internalId = string.Format("{0}-{1:0000}", this.UniqueInternalIdentity,
                                         Interlocked.Increment(ref _dealsNum));
                    _dealIdMappings.Add(new Tuple<string, string>(externalId, internalId));
                }

                return internalId;
            }

            public void InvokeOnOrderChanged(OrderChangeEventArgs orderChangeEventArgs)
            {
                if (OnOrderChanged != null)
                {
                    OnOrderChanged(this, orderChangeEventArgs);
                }

                //If we called this synchronously after order is closed, prevent all subsequent invocations
                //  (and also remove references caused by event subscription - so that this can be collected)
                if (this._closed)
                    OnOrderChanged = null;
            }

            public void SetTimeout(TimeSpan span)
            {
                //No need for locking or interlocked synchronization, as callback wouldn't result to event on closed order
                if (!_closed)
                {
                    this._timeoutTimer = new SafeTimer(TimeoutCallback, span, Timeout.InfiniteTimeSpan);
                }
                else
                {
                    OnTimeout = null;
                }
            }

            private int _pendingLLsCount = 0;

            internal int PendingLLsCountInternal { get { return _pendingLLsCount; } }

            public void AddPendingLLDeal(Action<WritableOrder> timeoutHandler)
            {
                if (Interlocked.Increment(ref this._pendingLLsCount) == 1)
                {
                    this.OnTimeout -= timeoutHandler;
                    this.OnTimeout += timeoutHandler;
                    this._llConfirmTimeoutTimer = new SafeTimer(TimeoutCallback,
                        SessionsSettings.OrderFlowSessionBehavior.UnconfirmedOrderTimeout, Timeout.InfiniteTimeSpan);
                }
            }

            public void RemovePendingLLDeal()
            {
                if (Interlocked.Decrement(ref this._pendingLLsCount) == 0)
                {
                    if(_llConfirmTimeoutTimer != null)
                        _llConfirmTimeoutTimer.Dispose();
                }
            }

            public bool HasPendingLLDeals
            {
                get { return this._pendingLLsCount > 0; }
            }


            private int _totalCancellationsSent = 0;
            private const int _MAX_CANCELLATIONS_BEFORE_ORDER_BROKEN = 36;
            private const int _MAX_CANCELLATIONS_BEFORE_ORDER_FLAGED = 31;
            private int _pendingCancellationsCount = 0;
            public bool TrySetOrderBeingCancelledIfNotClosed(TimeSpan cancellationTimeout, Action<WritableOrder> onCancelOperationTimeout)
            {
                _totalCancellationsSent++;
                if (_totalCancellationsSent > _MAX_CANCELLATIONS_BEFORE_ORDER_BROKEN)
                {
                    LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "order [{0}] experienced {1} attempts for cancellation yet it is still active - setting to broken state",
                        this.Identity, _totalCancellationsSent);
                    Task.Factory.StartNew(() => onCancelOperationTimeout(this));
                    Thread.Sleep(1);
                    return false;
                }

                if (_totalCancellationsSent == _MAX_CANCELLATIONS_BEFORE_ORDER_FLAGED || _totalCancellationsSent == _MAX_CANCELLATIONS_BEFORE_ORDER_FLAGED + 1)
                {
                    LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "order [{0}] experienced {1} attempts for cancellation yet it is still active - just flagging",
                        this.Identity, _totalCancellationsSent);
                    Thread.Sleep(10);
                }
                else if (_totalCancellationsSent > _MAX_CANCELLATIONS_BEFORE_ORDER_FLAGED)
                {
                    Thread.Sleep(10);
                }

                // Callback wouldn't result to event on closed order
                //BUT we need locking due to 2 variables (_closed and _pendingCancellationsCount) dependency with
                // TryCloseIfNoPendingCancellations method
                // (otherwise we could close order, but in the meantime allowed to send Cancellation request that wouldn't then be boubled up)
                lock (this)
                {
                    if (!_closed)
                    {
                        if (Interlocked.Increment(ref _pendingCancellationsCount) == 1)
                        {
                            //we will set handler just once, as the cancel result migh come just once
                            this.OnCancelOperationTimeout += onCancelOperationTimeout;
                            this._cancelOperationTimeoutTimer = new SafeTimer(CancelOperationTimeoutCallback,
                                cancellationTimeout, Timeout.InfiniteTimeSpan);
                        }
                        return true;
                    }
                    else
                    {
                        this.OnCancelOperationTimeout = null;
                        return false;
                    }
                }
            }

            public void SetOrderCancellationDone()
            {
                if (Interlocked.Decrement(ref _pendingCancellationsCount) == 0)
                {
                    StopCancelOperationTimeout();
                }
            }

            private void StopCancelOperationTimeout()
            {
                if (this._cancelOperationTimeoutTimer != null)
                {
                    //multiple dispose is fine
                    this._cancelOperationTimeoutTimer.Dispose();
                }
            }

            public bool TryCloseIfNoPendingCancellations(bool forceClosing)
            {
                //Coordination with TrySetOrderBeingCancelledIfNotClosed - se comment there
                lock (this)
                {
                    if (forceClosing || _pendingCancellationsCount <= 0)
                    {
                        this._closed = true;
                        if (this._timeoutTimer != null)
                        {
                            this._timeoutTimer.Dispose();
                        }
                        if (this._cancelOperationTimeoutTimer != null)
                        {
                            //multiple dispose is fine
                            this._cancelOperationTimeoutTimer.Dispose();
                        }
                        OnTimeout = null;
                        OnCancelOperationTimeout = null;
                        //BEWARE: we cannot deregister here as we are still invoking subscrubires synchronously after close
                        // but then we want to deregister all - so we perform this on InvokeOnOrderChanged method
                        //OnOrderChanged = null;

                        return true;
                    }
                }

                return false;
            }

            public bool TryCloseIfDone()
            {
                if (!this.HasRemainingAmount && this._pendingLLsCount <= 0
                    &&
                    (this.OrderStatus == IntegratorOrderExternalStatus.Cancelled
                     ||
                     this.OrderStatus == IntegratorOrderExternalStatus.Filled
                     ||
                     this.OrderStatus == IntegratorOrderExternalStatus.InBrokenState
                     ||
                     this.OrderStatus == IntegratorOrderExternalStatus.Rejected)
                    )
                {
                    this._closed = true;
                    OnTimeout = null;
                    OnCancelOperationTimeout = null;
                    OnOrderChanged = null;

                    return true;
                }

                return false;
            }

            private void CancelOperationTimeoutCallback()
            {
                if (this._timeoutTimer != null)
                {
                    this._timeoutTimer.Dispose();
                }
                //Check for _closed replaces the need for synchronization during setting the timeout
                if (OnCancelOperationTimeout != null && !_closed)
                {
                    OnCancelOperationTimeout(this);
                }
                //here if (_closed && OnCancelOperationTimeout != null) we know that order was not cancelled within timeout
                OnCancelOperationTimeout = null;
            }

            private void TimeoutCallback()
            {
                if (this._timeoutTimer != null)
                    this._timeoutTimer.Dispose();
                if(this._llConfirmTimeoutTimer != null)
                    this._llConfirmTimeoutTimer.Dispose();
                //Check for _closed replaces the need for synchronization during setting the timeout
                if (OnTimeout != null && !_closed)
                {
                    OnTimeout(this);
                }
                OnTimeout = null;
            }

            public override string ToString()
            {
                return string.Format("Order [{0}], [{1}]: Status: {2}, Filled amount: {3}. Created from request: {4}", Identity, UniqueInternalIdentity, OrderStatus, FilledAmount, OrderRequestInfo);
            }



            #region IIntegratorOrderInfo

            public Symbol Symbol
            {
                get { return this.OrderRequestInfo.Symbol; }
            }

            public DealDirection IntegratorDealDirection
            {
                get { return this.OrderRequestInfo.Side; }
            }

            public decimal SizeBaseAbsInitial
            {
                get { return this.OrderRequestInfo.SizeBaseAbsInitial; }
            }

            public decimal RequestedPrice { get { return this.OrderRequestInfo.RequestedPrice; } }

            #endregion IIntegratorOrderInfo
        }

    }
}
