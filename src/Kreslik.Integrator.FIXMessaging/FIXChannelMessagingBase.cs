﻿using System;
using System.Collections.Generic;
using System.Text;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix.FIX44;
using QuickFix.Fields;
using TimeInForce = QuickFix.Fields.TimeInForce;
using Symbol = Kreslik.Integrator.Common.Symbol; 

namespace Kreslik.Integrator.FIXMessaging
{
    internal interface IParentChannel
    {
        Counterparty Counterparty { get; }

        bool IsDivertedViaFluent { get; }
    }

    internal interface IOrdersParentChannel : IParentChannel
    {
        void SendOrderChange(OrderChangeInfo orderChangeInfo);
    }

    internal interface IPriceReceivingHelper
    {
        QuoteObject SharedQuote { get; }
        PriceObjectInternal GetPriceObject(AtomicDecimal priceConverted, decimal sizeBaseAbsInitial, PriceSide side,
            Symbol symbol,
            Counterparty counterparty, byte[] counterpartyIdentity, MarketDataRecordType marketDataRecordType,
            DateTime integratorReceivedTime, DateTime counterpartySentTime);
    }

    internal interface IMarketDataPerentChannel : IParentChannel
    {
        IPriceReceivingHelper PriceReceivingHelper { get; }
        string GetSubscriptionIdentity(BufferSegmentBase buffer);
        string TryGetSubscriptionIdentity(BufferSegmentBase buffer);
        void SendNewQuote(QuoteObject quote);
        void SendNewPrice(PriceObjectInternal price, string subscriptionIdentity);
        void SendNewNonexecutablePrice(PriceObjectInternal price);
        void SendLastQuoteCancel(string subscriptionIdentity);
        void SendLastPriceCancel(Symbol symbol, Counterparty counterparty, PriceSide side);
        void SendSubscriptionChange(SubscriptionChangeInfo subscriptionChangeInfo);
    }

    public class PriceReceivingHelper : IPriceReceivingHelper
    {
        internal const int PREFETCH_PRICES_BATCH_SIZE = 20;
        private QuoteObject _sharedQuote;
        private PriceObjectInternal[] _priceObjectsBatch;
        private int _priceObjectsIdx = 0;
        private PriceObjectPool _priceObjectPool;

        QuoteObject IPriceReceivingHelper.SharedQuote { get { return _sharedQuote; } }

        public PriceReceivingHelper(int pricesPoolSize)
        {
            _sharedQuote = QuoteObject.GetNewUninitializedQuote(this.GetNextPriceObject);
            _priceObjectPool = PriceObjectPool.Instance;
            _priceObjectsBatch = new PriceObjectInternal[pricesPoolSize];
        }

        public PriceReceivingHelper()
            :this(PREFETCH_PRICES_BATCH_SIZE)
        { }

        public void ClearInternalState()
        {
            //clear prefetched prices
            for (int idx = _priceObjectsIdx; idx < _priceObjectsBatch.Length; idx++)
            {
                if (_priceObjectsBatch[idx] != null)
                {
                    _priceObjectsBatch[idx].Release();
                    _priceObjectsBatch[idx] = null;
                }
            }

            _priceObjectsIdx = 0;
        }

        private PriceObjectInternal GetNextPriceObject()
        {
            PriceObjectInternal priceObject;
            if (_priceObjectsIdx >= _priceObjectsBatch.Length || _priceObjectsBatch[_priceObjectsIdx] == null)
            {
                _priceObjectPool.TryFetchBatchOfPriceObjects(_priceObjectsBatch);
                _priceObjectsIdx = 0;
            }

            priceObject = _priceObjectsBatch[_priceObjectsIdx];
            _priceObjectsBatch[_priceObjectsIdx] = null;
            _priceObjectsIdx++;

            return priceObject;
        }

        [Obsolete("AtomicDecimal and BufferSegmentBase should be used directly to create PriceObjects")]
        public PriceObjectInternal GetPriceObject(decimal price, decimal sizeBaseAbsInitial, PriceSide side,
            Symbol symbol,
            Counterparty counterparty, string counterpartyIdentity, MarketDataRecordType marketDataRecordType,
            DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            PriceObjectInternal priceObject = GetNextPriceObject();
            return PriceObjectPool.OverridePrefetchedPriceObject(priceObject, new AtomicDecimal(price), sizeBaseAbsInitial, side,
                symbol, counterparty, counterpartyIdentity.ToAsciiBuffer(), marketDataRecordType, integratorReceivedTime,
                counterpartySentTime);
        }

        public PriceObjectInternal GetPriceObject(AtomicDecimal priceConverted, decimal sizeBaseAbsInitial, PriceSide side,
            Symbol symbol,
            Counterparty counterparty, byte[] counterpartyIdentity, MarketDataRecordType marketDataRecordType,
            DateTime integratorReceivedTime, DateTime counterpartySentTime)
        {
            PriceObjectInternal priceObject = GetNextPriceObject();
            return PriceObjectPool.OverridePrefetchedPriceObject(priceObject, priceConverted, sizeBaseAbsInitial, side,
                symbol, counterparty, counterpartyIdentity, marketDataRecordType, integratorReceivedTime,
                counterpartySentTime);
        }
    }

    internal abstract class FIXChannelMessagingBase<TDerivedType> : FIXChannelBase<TDerivedType>,
                                                                  IMarketDataMessagingAdapter,
                                                                  IOrderFlowMessagingAdapter,
                                                                  IOrdersParentChannel,
                                                                  IMarketDataPerentChannel
        where TDerivedType : MessageCrackerBase<TDerivedType>
    {
        //public const int PREFETCH_PRICES_BATCH_SIZE = 20;
        internal const int _MAX_COUNTERPARTYCLIENTID_LENGTH = 20;
        //protected QuoteObject _sharedQuote;
        //public QuoteObject SharedQuote { get { return _sharedQuote; } }
        protected PriceReceivingHelper _priceReceivingHelper;
        public IPriceReceivingHelper PriceReceivingHelper { get { return _priceReceivingHelper; } }

        protected FIXChannelMessagingBase(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings settings)
            : this(persistedFixConfigsReader, logger, counterparty, settings, FIXMessaging.PriceReceivingHelper.PREFETCH_PRICES_BATCH_SIZE)
        { }

        protected FIXChannelMessagingBase(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger, Counterparty counterparty, UnexpectedMessagesTreatingSettings settings, int pricesPoolSize)
            : base(persistedFixConfigsReader, logger, settings)
        {
            this.Counterparty = counterparty;

            if (persistedFixConfigsReader.IsQuotingSession)
            {
                _priceReceivingHelper = new PriceReceivingHelper(pricesPoolSize);
            }
        }

        public abstract bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo,
                                                     string subscriptionIdentity);

        public abstract bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo,
                                                       string subscriptionIdentity);


        private Dictionary<BufferSegmentBase, string> _subscriptionIdentities
            = new Dictionary<BufferSegmentBase, string>();

        void IMarketDataMessagingAdapter.AddSubscriptionIdentity(string subscriptionIdentity)
        {
            //there can be multiple subs. request for same id (especially if unsucessfull)
            // so override each other
            _subscriptionIdentities[ParsedBufferSegment.CreateFromAsciiString(subscriptionIdentity)] = subscriptionIdentity;
        }

        public void ClearInternalStateAfterStop()
        {
            _subscriptionIdentities.Clear();

            //clear prefetched prices
            _priceReceivingHelper.ClearInternalState();
        }

        public string GetSubscriptionIdentity(BufferSegmentBase buffer)
        {
            string subscriptionIdentity;

            if (!_subscriptionIdentities.TryGetValue(buffer, out subscriptionIdentity))
            {
                subscriptionIdentity = buffer.GetContentAsAsciiString();
                this.Logger.Log(LogLevel.Error, "Received message with unknown subscription identity: {0}", subscriptionIdentity);
            }

            return subscriptionIdentity;
        }

        public string TryGetSubscriptionIdentity(BufferSegmentBase buffer)
        {
            string subscriptionIdentity;

            _subscriptionIdentities.TryGetValue(buffer, out subscriptionIdentity);

            return subscriptionIdentity;
        }

        protected virtual bool SupportsLimitOrders
        {
            get { return false; }
        }

        protected virtual bool SendsQuotedOrdersAsLimit
        {
            get { return false; }
        }

        protected virtual bool SupportsPeggedOrders
        {
            get { return false; }
        }

        public bool SendOrderMessage(OrderRequestInfo orderInfo, string identity, out string outgoingFixMessageRaw)
        {
            if (orderInfo.OrderType == OrderType.Limit && !this.SupportsLimitOrders)
            {
                this.Logger.Log(LogLevel.Error,
                                "Attempt to submit order [{0}] created from LIMIT request info ({1}), but through adapter that doesn't support limit orders",
                                identity, orderInfo);
                outgoingFixMessageRaw = null;
                return false;
            }

            if (orderInfo.OrderType == OrderType.Pegged && !this.SupportsPeggedOrders)
            {
                this.Logger.Log(LogLevel.Error,
                                "Attempt to submit order [{0}] created from PEGGED request info ({1}), but through adapter that doesn't support pegged orders",
                                identity, orderInfo);
                outgoingFixMessageRaw = null;
                return false;
            }

            QuickFix.Message orderMessage =
                this.IsDivertedViaFluent
                    ? this.CreateOrderMessageFluent(orderInfo, identity)
                    : this.CreateOrderMessage(orderInfo, identity);

            bool sent;
            if (orderMessage != null)
            {
                sent = QuickFix.Session.SendToTarget(orderMessage, this.SessionId);
            }
            else
            {
                sent = false;
            }
            

            if (sent)
            {
                outgoingFixMessageRaw = orderMessage.ToString();
            }
            else
            {
                outgoingFixMessageRaw = null;
            }

            return sent;
        }

        protected abstract QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity);

        private QuickFix.Message CreateOrderMessageFluent(OrderRequestInfo orderInfo, string identity)
        {

            var orderMessage = new NewOrderSingle();
            orderMessage.ClOrdID = new ClOrdID(identity);
            orderMessage.Symbol = orderInfo.Symbol.ToQuickFixSymbol();
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();

            orderMessage.Side = orderInfo.Side.ToQuickFixSide();

            //Cancel on disconnect
            //orderMessage.ExecInst = new ExecInst(ExecInst.CANCEL_ON_SYSTEM_FAILURE);

            orderMessage.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);
            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);

            orderMessage.Price = new Price(orderInfo.RequestedPrice);

            if (orderInfo.OrderType == OrderType.Limit || this.SendsQuotedOrdersAsLimit && orderInfo.OrderType == OrderType.Quoted)
            {
                orderMessage.OrdType = new OrdType(OrdType.LIMIT);

                orderMessage.TimeInForce =
                    new TimeInForce(orderInfo.TimeInForce == Contracts.TimeInForce.Day
                        ? TimeInForce.DAY
                        //on exchanges we want to send IoC, elsewhere FOK
                        : (SupportsLimitOrders ? TimeInForce.IMMEDIATE_OR_CANCEL : TimeInForce.FILL_OR_KILL));

                //this applies also on our IoC orders
                if (orderInfo.SizeBaseAbsFillMinimum > 0)
                    orderMessage.MinQty = new MinQty(orderInfo.SizeBaseAbsFillMinimum);
            }
            else if(orderInfo.OrderType == OrderType.Quoted)
            {
                orderMessage.OrdType = new OrdType(OrdType.PREVIOUSLY_QUOTED);
                orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);
                orderMessage.TimeInForce = new TimeInForce(TimeInForce.FILL_OR_KILL);
            }
            else
            {
                this.Logger.Log(LogLevel.Fatal, "Attempt to send [{0}] order {1}. Unsupported - ignoring",
                    orderInfo.OrderType, identity);
                return null;
            }

            this.AppendAdditionalTagsToOrderMessage(orderMessage, orderInfo);

            return orderMessage;
        }

        protected virtual void AppendAdditionalTagsToOrderMessage(NewOrderSingle orderMessage, OrderRequestInfo orderInfo) { }

        public event HandleQuoteObjectInternal OnNewQuote;
        public event HandlePriceObjectInternal OnNewPrice;
        public event Action<PriceObjectInternal> OnNewNonexecutablePrice;

        public event Action<string> OnCancelLastQuote;
        public event Action<Symbol, Counterparty, PriceSide> OnCancelLastPrice;

        public event Action<SubscriptionChangeInfo> OnSubscriptionChanged;

        public event Action<OrderChangeInfo> OnOrderChanged;

        public override Counterparty Counterparty { get; protected set; }

        //rather make this public so that we do not need to indirect the call and make it slower
        public void SendNewQuote(QuoteObject quote)
        {
            bool askHeldWithoutAcquireCall = false;
            bool bidHeldWithoutAcquireCall = false;
            try
            {
                this.IntegratorPerfCounter.ExecutableQuoteReceived();
                if (this.OnNewQuote != null)
                {
                    //all subscribers receive the price synchronously - so we don't want any of them to accidentaly
                    // cause the release call
                    quote.MarkReleaseDisallowed();
                    this.OnNewQuote(quote, ref askHeldWithoutAcquireCall, ref bidHeldWithoutAcquireCall);
                }
            }
            finally
            {
                quote.ClearReleaseDisallowed();
                quote.Release(askHeldWithoutAcquireCall, bidHeldWithoutAcquireCall);
            } 
        }

        public void SendNewPrice(PriceObjectInternal price, string subscriptionIdentity)
        {
            bool heldWithoutAcquireCall = false;
            try
            {
                this.IntegratorPerfCounter.ExecutableQuoteReceived();
                if (this.OnNewPrice != null)
                {
                    price.MarkReleaseDisallowed();
                    this.OnNewPrice(price, subscriptionIdentity, ref heldWithoutAcquireCall);
                }
            }
            finally 
            {
                price.ClearReleaseDisallowed();
                //Perf optimiziation.
                //Since on each price we put it to book (which is acquiring), then communicate and prevent
                // unnecesary pair of acquire-release call
                if(!heldWithoutAcquireCall)
                    price.Release();
            }
        }

        public void SendNewNonexecutablePrice(PriceObjectInternal price)
        {
            try
            {
                if (this.OnNewNonexecutablePrice != null)
                    this.OnNewNonexecutablePrice(price);
            }
            finally
            {
                price.Release();
            }
        }

        public void SendLastPriceCancel(Symbol symbol, Counterparty counterparty, PriceSide side)
        {
            if (this.OnCancelLastPrice != null)
                this.OnCancelLastPrice(symbol, counterparty, side);
        }

        //void IMarketDataPerentChannel.SendNewQuote(QuoteBase quote)
        //{
        //    this.SendNewQuote(quote);
        //}

        protected void SendLastQuoteCancel(string subscriptionIdentity)
        {
            if (this.OnCancelLastQuote != null)
            {
                this.OnCancelLastQuote(subscriptionIdentity);
            }
        }

        void IMarketDataPerentChannel.SendLastQuoteCancel(string subscriptionIdentity)
        {
            this.SendLastQuoteCancel(subscriptionIdentity);
        }

        protected void SendSubscriptionChange(SubscriptionChangeInfo subscriptionChangeInfo)
        {
            if (this.OnSubscriptionChanged != null)
            {
                this.OnSubscriptionChanged(subscriptionChangeInfo);
            }
        }

        void IMarketDataPerentChannel.SendSubscriptionChange(SubscriptionChangeInfo subscriptionChangeInfo)
        {
            this.SendSubscriptionChange(subscriptionChangeInfo);
        }

        protected void SendOrderChange(OrderChangeInfo orderChangeInfo)
        {
            if (this.OnOrderChanged != null)
            {
                this.OnOrderChanged(orderChangeInfo);
            }
        }

        void IOrdersParentChannel.SendOrderChange(OrderChangeInfo orderChangeInfo)
        {
            this.SendOrderChange(orderChangeInfo);
        }

        //public void OnMessage(QuickFix.FIX42.Reject m, QuickFix.SessionID s)
        //{
        //    this.HandleBusinessReject(m);
        //}

        //public void OnMessage(QuickFix.FIX43.Reject m, QuickFix.SessionID s)
        //{
        //    this.HandleBusinessReject(m);
        //}

        //public void OnMessage(QuickFix.FIX44.Reject m, QuickFix.SessionID s)
        //{
        //    this.HandleBusinessReject(m);
        //}

        //public void OnMessage(QuickFix.FIX42.BusinessMessageReject m, QuickFix.SessionID s)
        //{
        //    this.HandleBusinessReject(m);
        //}

        //public void OnMessage(QuickFix.FIX43.BusinessMessageReject m, QuickFix.SessionID s)
        //{
        //    this.HandleBusinessReject(m);
        //}

        //public void OnMessage(QuickFix.FIX44.BusinessMessageReject m, QuickFix.SessionID s)
        //{
        //    this.HandleBusinessReject(m);
        //}

        //public void OnMessage(QuickFix.FIX50.BusinessMessageReject m, QuickFix.SessionID s)
        //{
        //    this.HandleBusinessReject(m);
        //}

        protected override bool TryHandleMessage(QuickFix.Message message)
        {
            string msgType = message.Header.GetField(QuickFix.Fields.Tags.MsgType);
            if (msgType == QuickFix.Fields.MsgType.REJECT || msgType == QuickFix.Fields.MsgType.BUSINESS_MESSAGE_REJECT)
            {
                this.HandleBusinessReject(message);
                return true;
            }

            return false;
        }

        private void HandleBusinessReject(QuickFix.Message bussinessReject)
        {
            this.Logger.Log(LogLevel.Fatal, "Receiving Business message reject: [{0}]",
                            QuickFixUtils.FormatMessage(bussinessReject));
            if (OnBusinessReject != null)
            {
                OnBusinessReject(bussinessReject);
            }
            //CancelLastRequest();
        }

        //private void CancelLastRequest()
        //{
        //    if (_lastOrderRequestInfo != null)
        //    {
        //        this.SendOrderChange(new ExecutedOrderInfo(_lastOrderRequestInfo.Identity, OrderStatus.InBrokenState, 0));
        //    }
        //    else if (_lastSubscriptionRequestInfo != null)
        //    {
        //        this.SendSubscriptionChange(new SubscriptionChangeInfo(_lastSubscriptionRequestInfo.Identity, SubscriptionStatus.Unknown));
        //    }
        //}

        //private SubscriptionRequestInfo _lastSubscriptionRequestInfo;
        //private OrderRequestInfo _lastOrderRequestInfo;


        public event Action<QuickFix.Message> OnBusinessReject;
    }
}
