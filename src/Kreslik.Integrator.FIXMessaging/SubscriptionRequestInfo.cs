﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.FIXMessaging
{
    /// <summary>
    /// Property bag for subscription request
    /// Property bag is read-only after the creation so that it cannot be changed after request submission
    /// </summary>
    public class SubscriptionRequestInfo
    {
        public SubscriptionRequestInfo(Symbol symbol, decimal quantity)
        {
            this.Symbol = symbol;
            this.Quantity = quantity;
        }

        public SubscriptionRequestInfo(Symbol symbol, decimal quantity, string senderSubId)
            : this(symbol, quantity)
        {
            this.SenderSubId = senderSubId;
        }

        public Symbol Symbol { get; private set; }
        public decimal Quantity { get; private set; }
        public string SenderSubId { get; private set; }

        protected bool Equals(SubscriptionRequestInfo other)
        {
            return Equals(Symbol, other.Symbol) && Quantity == other.Quantity && string.Equals(SenderSubId, other.SenderSubId);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Symbol != Common.Symbol.NULL ? Symbol.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Quantity.GetHashCode();
                hashCode = (hashCode * 397) ^ (SenderSubId != null ? SenderSubId.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(SubscriptionRequestInfo left, SubscriptionRequestInfo right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(SubscriptionRequestInfo left, SubscriptionRequestInfo right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((SubscriptionRequestInfo) obj);
        }
        
        public override string ToString()
        {
            if (string.IsNullOrEmpty(SenderSubId))
            {
                return string.Format("Subscription - Symbol: {0}, quantity: {1}.", Symbol, Quantity);
            }
            else
            {
                return string.Format("Subscription - Symbol: {0}, quantity: {1}. SenderSubId: {2}.",
                                     Symbol, Quantity, SenderSubId);
            }
        }
    }
}
