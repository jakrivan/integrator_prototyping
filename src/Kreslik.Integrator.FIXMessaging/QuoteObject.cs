﻿using System;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.FIXMessaging
{
    internal class QuoteObject : IIntegratorReceivedObjectEx
    {
        //public PriceObject GetPrice(PriceSide side)
        //{
        //    return side == PriceSide.Ask ? this.AskPrice : this.BidPrice;
        //}

        public PriceObjectInternal AskPrice { get; private set; }
        public PriceObjectInternal BidPrice { get; private set; }

        public string SubscriptionIdentity { get; private set; }
        public DateTime IntegratorReceivedTimeUtc { get; private set; }

        public DateTime CounterpartySentTimeUtc { get; private set; }

        public Counterparty Counterparty { get; private set; }

        internal void Release(bool askHeldWithoutAcquireCall, bool bidHeldWithoutAcquireCall)
        {
            if(this.AskPrice == null && this.BidPrice == null)
                throw new Exception("OverReleasing of Quote Object");

            if (this.AskPrice != null)
            {
                if (!askHeldWithoutAcquireCall)
                    this.AskPrice.Release();
                this.AskPrice = null;
            }

            if (this.BidPrice != null)
            {
                if (!bidHeldWithoutAcquireCall)
                    this.BidPrice.Release();
                this.BidPrice = null;
            }
        }

        internal void MarkReleaseDisallowed()
        {
            if(AskPrice != null)
                AskPrice.MarkReleaseDisallowed();
            if(BidPrice != null)
                BidPrice.MarkReleaseDisallowed();
        }

        internal void ClearReleaseDisallowed()
        {
            if (AskPrice != null)
                AskPrice.ClearReleaseDisallowed();
            if (BidPrice != null)
                BidPrice.ClearReleaseDisallowed();
        }

        private void CheckReleased()
        {
            if (this.AskPrice != null || this.BidPrice != null)
            {
                this.Release(false, false);
                throw new Exception("QuoteObject not properly released before reusing");
            }
        }

        private QuoteObject() { }

        internal static readonly QuoteObject EMPTY_QUOTE = new QuoteObject();

        internal static QuoteObject GetNewUninitializedQuote(Func<PriceObjectInternal> priceObjectFactory)
        {
            return new QuoteObject(){_priceObjectFactory = priceObjectFactory};
        }

        private Func<PriceObjectInternal> _priceObjectFactory;

        [Obsolete("AtomicDecimal and BufferSegmentBase should be used directly to populate Quotes")]
        internal void OverrideContentObsolete(decimal askPrice, decimal askSize, decimal bidPrice, decimal bidSize, Symbol symbol,
                            Counterparty counterparty, string subscriptionIdentity, DateTime intagratorTimeUtc,
                            DateTime counterpartyTimeUtc, string counterpartyAskIdentity, string counterpartyBidIdentity,
                            MarketDataRecordType marketDataRecordType)
        {
            this.CheckReleased();

            this.SubscriptionIdentity = subscriptionIdentity;
            this.IntegratorReceivedTimeUtc = intagratorTimeUtc;
            this.CounterpartySentTimeUtc = counterpartyTimeUtc;
            this.Counterparty = counterparty;

            AskPrice = _priceObjectFactory();
            AskPrice.OverrideContent
                (
                priceConverted: new AtomicDecimal(askPrice),
                sizeBaseAbsInitial: askSize,
                side: PriceSide.Ask,
                symbol: symbol,
                counterparty: counterparty,
                counterpartyIdentity: counterpartyAskIdentity.ToAsciiBuffer(),
                marketDataRecordType: marketDataRecordType,
                integratorReceivedTime: intagratorTimeUtc,
                counterpartySentTime: counterpartyTimeUtc,
                sizeBaseAbsFillMinimum: null,
                sizeBaseAbsFillGranularity: null,
                tradeSide: null
                );

            BidPrice =_priceObjectFactory();
            BidPrice.OverrideContent
                (
                priceConverted: new AtomicDecimal(bidPrice),
                sizeBaseAbsInitial: bidSize,
                side: PriceSide.Bid,
                symbol: symbol,
                counterparty: counterparty,
                counterpartyIdentity: counterpartyBidIdentity.ToAsciiBuffer(),
                marketDataRecordType: marketDataRecordType,
                integratorReceivedTime: intagratorTimeUtc,
                counterpartySentTime: counterpartyTimeUtc,
                sizeBaseAbsFillMinimum: null,
                sizeBaseAbsFillGranularity: null,
                tradeSide: null
                );
        }

        [Obsolete("AtomicDecimal and BufferSegmentBase should be used directly to populate Quotes")]
        internal void OverrideContentObsoleteNoIdentity(decimal askPrice, decimal askSize, decimal bidPrice, decimal bidSize,
            Symbol symbol,
            Counterparty counterparty, string subscriptionIdentity, DateTime counterpartyTimeUtc,
            MarketDataRecordType marketDataRecordType)
        {
            this.OverrideContentObsolete(askPrice, askSize, bidPrice, bidSize, symbol, counterparty,
                subscriptionIdentity, HighResolutionDateTime.UtcNow, counterpartyTimeUtc, null, null,
                marketDataRecordType);
        }

        [Obsolete("AtomicDecimal and BufferSegmentBase should be used directly to populate Quotes")]
        internal void OverrideContentObsoleteSingleIdentity(decimal askPrice, decimal askSize, decimal bidPrice,
            decimal bidSize, Symbol symbol,
            Counterparty counterparty, string subscriptionIdentity, string identity, DateTime counterpartyTimeUtc,
            MarketDataRecordType marketDataRecordType)
        {
            this.OverrideContentObsolete(askPrice, askSize, bidPrice, bidSize, symbol, counterparty,
                subscriptionIdentity, HighResolutionDateTime.UtcNow,
                counterpartyTimeUtc, identity, identity, marketDataRecordType);
        }

        [Obsolete("AtomicDecimal and BufferSegmentBase should be used directly to populate Quotes")]
        internal void OverrideContentObsoleteSidedIdentity(decimal askPrice, decimal askSize, decimal bidPrice,
            decimal bidSize, Symbol symbol,
            Counterparty counterparty, string subscriptionIdentity,
            DateTime counterpartyTimeUtc, string askIdentity, string bidIdentity,
            MarketDataRecordType marketDataRecordType)
        {
            this.OverrideContentObsolete(askPrice, askSize, bidPrice, bidSize, symbol, counterparty,
                subscriptionIdentity,
                HighResolutionDateTime.UtcNow, counterpartyTimeUtc, askIdentity, bidIdentity, marketDataRecordType);
        }

        internal void OverrideContent(AtomicDecimal askPrice, decimal askSize, AtomicDecimal bidPrice, decimal bidSize,
            Symbol symbol,
            Counterparty counterparty, string subscriptionIdentity, DateTime intagratorTimeUtc,
            DateTime counterpartyTimeUtc, byte[] counterpartyAskIdentity,
            byte[] counterpartyBidIdentity,
            MarketDataRecordType marketDataRecordType)
        {
            this.CheckReleased();

            this.SubscriptionIdentity = subscriptionIdentity;
            this.IntegratorReceivedTimeUtc = intagratorTimeUtc;
            this.CounterpartySentTimeUtc = counterpartyTimeUtc;
            this.Counterparty = counterparty;

            AskPrice = _priceObjectFactory();
            AskPrice.OverrideContent
                (
                priceConverted: askPrice,
                sizeBaseAbsInitial: askSize,
                side: PriceSide.Ask,
                symbol: symbol,
                counterparty: counterparty,
                counterpartyIdentity: counterpartyAskIdentity,
                marketDataRecordType: marketDataRecordType,
                integratorReceivedTime: intagratorTimeUtc,
                counterpartySentTime: counterpartyTimeUtc,
                sizeBaseAbsFillMinimum: null,
                sizeBaseAbsFillGranularity: null,
                tradeSide: null
                );

            BidPrice = _priceObjectFactory();
            BidPrice.OverrideContent
                (
                priceConverted: bidPrice,
                sizeBaseAbsInitial: bidSize,
                side: PriceSide.Bid,
                symbol: symbol,
                counterparty: counterparty,
                counterpartyIdentity: counterpartyBidIdentity,
                marketDataRecordType: marketDataRecordType,
                integratorReceivedTime: intagratorTimeUtc,
                counterpartySentTime: counterpartyTimeUtc,
                sizeBaseAbsFillMinimum: null,
                sizeBaseAbsFillGranularity: null,
                tradeSide: null
                );
        }
    }
}