﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickFix.Fields;

namespace Kreslik.Integrator.FIXMessaging
{
    public class TagDescription_HSBC : IDescriptionsProvider
    {
        public IDictionary<int, string> TagDescriptions
        {
            get
            { // TODO load from FIX XML file
                var fixTagDescriptionsDictionary = new SortedDictionary<int, string>
                {
                    {9054, "SecondaryQty"},
                    {9063, "SettType"},
                    {9064, "SettDate"},
                };

                return fixTagDescriptionsDictionary;
            }
        }
    }

    public class TagDescription_DB: IDescriptionsProvider
    {

        public IDictionary<int, string> TagDescriptions
        {
            get
            { // TODO load from FIX XML file
                var fixTagDescriptionsDictionary = new SortedDictionary<int, string>
                {
                    {5232, "Currency"},
                    {5233, "OrderQty"},
                    {6138, "TickSize"},
                    {6215, "TenorValue"},
                    {6054, "QuotedQty"}
                };

                return fixTagDescriptionsDictionary;
            }
        }
    }

    public interface IDescriptionsProvider
    {
        IDictionary<int, string> TagDescriptions { get; } 
    }

    public static class CustomTagDescription
    {
        private static readonly List<IDescriptionsProvider> _descriptionsProviders = new List<IDescriptionsProvider>(){new TagDescription_HSBC(), new TagDescription_DB()};

        static CustomTagDescription()
        {
            _descriptionsDictionary =
                new SortedDictionary<int, string>(
                    _descriptionsProviders
                        .SelectMany(dict => dict.TagDescriptions)
                        .ToLookup(pair => pair.Key, pair => pair.Value)
                        .ToDictionary(group => group.Key, group =>
                            {
                                var val = group.First();
                                if (group.Any(x => x != val))
                                {
                                    throw new Exception(
                                        "Key already present with different value " +
                                        group.Key);
                                }
                                return val;
                            }));
        }

        public static bool NumericCodeExists(int numericCode)
        {
            return _descriptionsDictionary.ContainsKey(numericCode);
        }

        public static string ConvertNumericCodeToDescription(int numericCode)
        {
            return _descriptionsDictionary[numericCode];
        }

        static readonly SortedDictionary<int, string> _descriptionsDictionary;
    }

    public sealed class SettlementType_HSBC : StringField
    {
        public SettlementType_HSBC()
            : base(Tags_HSBC.SettType) { }
        public SettlementType_HSBC(string val)
            : base(Tags_HSBC.SettType, val) { }

        public const string SPOT = "SP";
    }

    public static class Tags_HSBC
    {
        public const int SettType = 9063;
    }

    public sealed class Currency_DB : StringField
    {
        public Currency_DB()
            : base(Tags_DB.Currency) { }
        public Currency_DB(string val)
            : base(Tags_DB.Currency, val) { }
    }

    public sealed class OrderQty_DB : DecimalField
    {
        public OrderQty_DB()
            : base(Tags_DB.OrderQty) { }
        public OrderQty_DB(decimal val)
            : base(Tags_DB.OrderQty, val) { }
    }

    public sealed class TenorValue_DB : StringField
    {
        public TenorValue_DB()
            : base(Tags_DB.TenorValue) { }
        public TenorValue_DB(string val)
            : base(Tags_DB.TenorValue, val) { }

        public const string SPOT = "SP";
    }

    public static class Tags_DB
    {
        public const int Currency = 5232;
        public const int OrderQty = 5233;
        public const int TickSize = 6138;
        public const int TenorValue = 6215;
        public const int QuotedQty = 6054;
    }


    public sealed class TenorValue_SG : StringField
    {
        public TenorValue_SG()
            : base(Tags_SG.TenorValue) { }
        public TenorValue_SG(string val)
            : base(Tags_SG.TenorValue, val) { }

        public const string SPOT = "0";
    }

    public static class Tags_SG
    {
        public const int TenorValue = 6215;
    }
}
