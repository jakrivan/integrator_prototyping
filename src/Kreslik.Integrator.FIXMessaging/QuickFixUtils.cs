﻿using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickFix.Fields;

namespace Kreslik.Integrator.FIXMessaging
{
    public static class QuickFixUtils
    {
        private static readonly QuickFix.Fields.Symbol[] quickfixSymbolConversions =
            Enumerable.Repeat((QuickFix.Fields.Symbol) null, Common.Symbol.ValuesCount).ToArray();
        private static readonly QuickFix.Fields.Currency[] quickfixCurrencyConversions =
            Enumerable.Repeat((QuickFix.Fields.Currency)null, Common.Currency.ValuesCount).ToArray();
        private static readonly Dictionary<BufferSegmentBase, Common.Symbol> symbolMappings = new Dictionary<BufferSegmentBase, Common.Symbol>();

        static QuickFixUtils()
        {
            foreach (Common.Symbol symbol in Common.Symbol.Values)
            {
                quickfixSymbolConversions[(int) symbol] = new QuickFix.Fields.Symbol(symbol.ToString());
                symbolMappings[ParsedBufferSegment.CreateFromAsciiString(symbol.ToString())] = symbol;
                symbolMappings[ParsedBufferSegment.CreateFromAsciiString(symbol.ShortName)] = symbol;
            }

            foreach (Common.Currency currency in Common.Symbol.UsedCurrencies)
            {
                quickfixCurrencyConversions[(int)currency] = new QuickFix.Fields.Currency(currency.ToString());
            }
        }

        public static Common.Symbol ToSymbol(this BufferSegmentBase bufferSegment)
        {
            return symbolMappings[bufferSegment];
        }

        public static bool TryConvertToSymbol(this BufferSegmentBase bufferSegment, out Common.Symbol s)
        {
            return symbolMappings.TryGetValue(bufferSegment, out s);
        }

        public static QuickFix.Fields.Symbol ToQuickFixSymbol(this Kreslik.Integrator.Common.Symbol symbol)
        {
            return quickfixSymbolConversions[(int)symbol];
        }

        public static QuickFix.Fields.Currency ToQuickBaseCurrency(this Kreslik.Integrator.Common.Symbol symbol)
        {
            return quickfixCurrencyConversions[(int)symbol.BaseCurrency];
        }

        public static DealDirection ToDealDirection(this QuickFix.Fields.Side side)
        {
            return ToDealDirection(side.getValue());
        }

        public static DealDirection ToDealDirection(char sideChar)
        {
            switch (sideChar)
            {
                case QuickFix.Fields.Side.BUY:
                    return DealDirection.Buy;
                case QuickFix.Fields.Side.SELL:
                    return DealDirection.Sell;
                default:
                    throw new Exception("Unknown side " + sideChar);
            }
        }

        public static QuickFix.Fields.Currency ToQuickFixCurrency(this Kreslik.Integrator.Common.Currency currency)
        {
            return quickfixCurrencyConversions[(int)currency];
        }

        //see http://stackoverflow.com/questions/4525854/remove-trailing-zeros/7983330#7983330
        private static decimal NormalizeDecimal(decimal value)
        {
            return value / 1.000000000000000000000000000000000m;
        }

        public static decimal? ToNullableDecimal(this FieldMap message, int tagId)
        {
            if (message.IsSetField(tagId))
            {
                return message.GetDecimal(tagId).Normalize();
            }
            else
            {
                return null;
            }
        }

        public static decimal? ToNullableDecimalCoalesce(this FieldMap message, int tagId, int alternateTagId)
        {
            if (message.IsSetField(tagId))
            {
                return message.GetDecimal(tagId).Normalize();
            }
            else if(message.IsSetField(alternateTagId))
            {
                return message.GetDecimal(alternateTagId).Normalize();
            }
            else
            {
                return null;
            }
        }

        public static DateTime ToDateTime(this FieldMap message, int tagId, bool dateOnly, ILogger logger)
        {
            if (message.IsSetField(tagId))
            {
                try
                {
                    if (dateOnly)
                    {
                        return message.GetDateOnly(tagId);
                    }
                    else
                    {
                        return message.GetDateTime(tagId);
                    }
                }
                catch (Exception e)
                {
                    logger.LogException(LogLevel.Error, e, "Couldn't convert value {0} to datetime", message.GetString(tagId));
                    return DateTime.MinValue;
                }
            }
            else
            {
                logger.Log(LogLevel.Warn, "DateTime tag {0} not present in message {1}", tagId, message);
                return DateTime.MinValue;
            }
        }

        public static DateTime? ToNullableDateTime(this FieldMap message, int tagId, bool dateOnly, ILogger logger)
        {
            if (message.IsSetField(tagId))
            {
                try
                {
                    if (dateOnly)
                    {
                        return message.GetDateOnly(tagId);
                    }
                    else
                    {
                        return message.GetDateTime(tagId);
                    }
                }
                catch (Exception e)
                {
                    logger.LogException(LogLevel.Error, e, "Couldn't convert value {0} to datetime", message.GetString(tagId));
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static DealDirection GetIntegratorDealDirection(this QuickFix.Message m)
        {
            switch (m.GetChar(54))
            {
                case '1':
                    return DealDirection.Buy;
                case '2':
                    return DealDirection.Sell;
                default:
                    throw new Exception(string.Format("Unrecognized value for tag 54 (Side): [{0}]. Expected 1 or 2",
                        m.GetChar(54)));
            } 
        }

        public static string ToNullableString(this FieldMap message, int tagId)
        {
            if (message.IsSetField(tagId))
            {
                return message.GetString(tagId);
            }
            else
            {
                return string.Empty;
            }
        }

        //public static Nullable<T> ToNullableType(this Message message, int tagId) where T: struct
        //{
        //    if (message.IsSetField(tagId))
        //    {
        //        return message.get
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        public static string ToFormattedString(this QuickFix.Message m)
        {
            StringBuilder sb = new StringBuilder();
            m.AppendSelf(sb, QuickFixUtils.AppendFormattedTag);
            return sb.ToString();
        }

        private static QuickFix.Fields.Side _buySide = new Side(Side.BUY);
        private static QuickFix.Fields.Side _sellSide = new Side(Side.SELL);

        public static QuickFix.Fields.Side ToQuickFixSide(this DealDirection dealDirection)
        {
            // TODO take care of unexpected Side
            return dealDirection == DealDirection.Buy ? _buySide : _sellSide;
        }

        private static char[] fieldsSplitters = new char[] { Message.SOH[0] };
        private static char[] fieldPartsSplitters = new char[] { '=' };

        public static void AppendFormattedTag(StringBuilder sb, int tagNumericCode, string tagAlphaCode)
        {
            AppendTagTranslation(sb, tagNumericCode); 
            sb.Append('=');
            TagValueDescription.AppendAlphaCodeDescription(sb, tagNumericCode, tagAlphaCode);
        }

        public static void AppendTagTranslation(StringBuilder sb, int tagNumericCode)
        {
            sb.AppendFormat("{0} [{1}]", tagNumericCode,
                            TagDescription_FIX44.NumericCodeExists(tagNumericCode)
                                ? TagDescription_FIX44.ConvertNumericCodeToDescription(tagNumericCode)
                                : CustomTagDescription.NumericCodeExists(tagNumericCode)
                                      ? CustomTagDescription.ConvertNumericCodeToDescription(tagNumericCode)
                                      : tagNumericCode.ToString());
        }

        public static string FormatMessage(Message msg)
        {
            StringBuilder sb = new StringBuilder();
            msg.AppendSelf(sb, AppendFormattedTag);
            return sb.ToString();

            //const string divisor = " # ";
            //const string equalitySign = "=";

            //var fixFields = msg.ToString().Split(fieldsSplitters, StringSplitOptions.None);

            //var outputMsg = "";

            //for (var i = 0; i < fixFields.Length - 1; i++) // QuickFix/n returns empty last field (wtf?)
            //{
            //    var numericCodeAndAlphaCode = fixFields[i].Split(fieldPartsSplitters, 2, StringSplitOptions.None);
            //    var tagNumericCode = int.Parse(numericCodeAndAlphaCode[0]);
            //    var alphaCode = numericCodeAndAlphaCode[1];

            //    outputMsg += string.Format("{0} [{1}]", tagNumericCode,
            //                               TagDescription_FIX44.NumericCodeExists(tagNumericCode)
            //                                   ? TagDescription_FIX44.ConvertNumericCodeToDescription(tagNumericCode)
            //                                   : CustomTagDescription.NumericCodeExists(tagNumericCode)
            //                                         ? CustomTagDescription.ConvertNumericCodeToDescription(tagNumericCode)
            //                                         : tagNumericCode.ToString()); // TODO handle missing custom tagCode
            //    outputMsg += equalitySign;
            //    outputMsg += TagValueDescription.ConvertAlphaCodeToDescription(tagNumericCode, alphaCode);


            //    if (i < fixFields.Length - 2) // to last but one field, see above
            //    {
            //        outputMsg += divisor; // no divisor at EOF
            //    }
            //}

            //return outputMsg;
        }
    }
}
