﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.Fields;
using QuickFix.Transport;
using SessionState = Kreslik.Integrator.Contracts.SessionState;

namespace Kreslik.Integrator.FIXMessaging
{
    public class FixLogFascade : ILog, QuickFix.ILogFactory
    {
        private readonly ILogger _logger;

        public FixLogFascade(ILogger logger)
        {
            this._logger = logger;
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }


        //REMARKS:
        //Not logging the direction of the messages as it can be inffered from the thread id
        //If it would be needed, then it should bedone only for outgoing (to prevent unnecesary str concatentaion)

        public void OnIncoming(string msg)
        {
            if (_logger.IsEnabled(LogLevel.Debug) && !this._logger.IsLowDiskSpaceConstraintLoggingOn)
            {
                _logger.Log(LogLevel.Debug, msg);
            }
        }

        public void OnOutgoing(string msg)
        {
            if (_logger.IsEnabled(LogLevel.Debug))
            {
                _logger.Log(LogLevel.Debug, msg);
            }
        }

        public void OnExpectedEvent(string s)
        {
            _logger.Log(LogLevel.Info, s);
        }

        public void OnFatalEvent(string s)
        {
            _logger.Log(LogLevel.Fatal, s);
        }

        public void OnEvent(string s)
        {
            _logger.Log(LogLevel.Warn, s);
        }

        public ILog Create(SessionID sessionID)
        {
            return this;
        }
    }

    public abstract class FIXChannelBase<TDerivedType> : MessageCrackerBase<TDerivedType>, IApplication, IFIXChannel
        where TDerivedType : MessageCrackerBase<TDerivedType>
    {
        protected ILogger Logger { get; private set; }
        protected IIntegratorPerformanceCounter IntegratorPerfCounter { get; private set; }
        protected SessionID SessionId { get; private set; }
        private StateMachine<SessionState, SessionEvent> _stateMachine;
        private SocketInitiator _initiator;
        private FixLogFascade _logFacade;
        private RemovableEventsRateChecker _unexpectedMessagesRateChecker = null;
        private SingleThreadedEventsRateChecker _unparsableMessagesRateChecker = null;
        private SingleThreadedEventsRateChecker _unparsableMessagesRateCheckerBackup = null;
        private IPersistedFixConfigsReader _persistedFixConfigsReader;

        public abstract Counterparty Counterparty { get; protected set; }

        protected internal FIXChannelBase(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger, UnexpectedMessagesTreatingSettings settings)
        {
            this._settings = settings;
            this.Logger = logger;
            this._persistedFixConfigsReader = persistedFixConfigsReader;
            this.IntegratorPerfCounter = IntegratorPerformanceCounter.Instance;

            //this.Initialize(configPath, logger);

            this._stateMachine = new StateMachine<SessionState, SessionEvent>(false, logger);
            ConfigureCommonStateMachineTransitions();
            this._stateMachine.Initialize(SessionState.Idle);

            if (persistedFixConfigsReader.IsQuotingSession)
            {
                TradingHoursHelper.Instance.MarketRolloverApproaching += () =>
                {
                    this._unparsableMessagesRateChecker = null;
                };

                TradingHoursHelper.Instance.MarketRolloverDone += () =>
                {
                    if(this._stateMachine.CurrentState == SessionState.Running)
                        this._unparsableMessagesRateChecker = this._unparsableMessagesRateCheckerBackup;
                };
            }

            persistedFixConfigsReader.ReadFixConfig();
            this._isDivertedViaFluent = persistedFixConfigsReader.IsRedirectedViaFluent;
            this._onBehalfOfCompId = persistedFixConfigsReader.FluentCredentials.OnBehalfOfCompId;

            if (this._isDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions_Internal();
        }

        

        public byte[] GetCertificateFile(string certificateName)
        {
            return this._persistedFixConfigsReader.GetCertificateFile(certificateName);
        }

        public XmlDocument GetXmlDictionary(string dictionaryName)
        {
            return this._persistedFixConfigsReader.GetXmlDictionary(dictionaryName);
        }

        private QuickMessageCracker _quickMessageCracker;

        public void OverrideCommonMessageParser(Kreslik.Integrator.Contracts.Internal.IFixMessageCracker parser)
        {
            if(_quickMessageCracker != null)
                _quickMessageCracker.OverrideCommonMessageParser(parser);
        }

        protected void SetQuickMessageParsers(bool isMdSession, params IFixMessageCracker[] fixMessageCrackers)
        {
            _quickMessageCracker = new QuickMessageCracker(this.Logger, isMdSession, fixMessageCrackers);
        }

        protected void SetQuickMessageParsers(bool isMdSession, IFixMessageCracker fixMessageCracker, byte[] recognizedMessageTypes)
        {
            _quickMessageCracker = new QuickMessageCracker(this.Logger, isMdSession, fixMessageCracker, recognizedMessageTypes);
        }

        public bool CanCrackRawMessages { get { return _quickMessageCracker != null; } }

        public bool CrackRawMessage(QuickFixSubsegment bufferSegment)
        {
            if (_quickMessageCracker == null)
                return false;
            else
                return _quickMessageCracker.ParseMessage(bufferSegment);
        }

        private UnexpectedMessagesTreatingSettings _settings;

        protected virtual void SettingsRead(bool isRedirectedViaFluent, bool isMdChannel) { }

        private bool Initialize(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger)
        {
            if (_initiator != null && !_initiator.IsStopped)
            {
                this.Logger.Log(LogLevel.Error, "Attempt to start session that appears to be already started. You need to Restart or Stop first");
                return false;
            }
            else if (_initiator != null)
            {
                _initiator.Dispose();
            }

            TextReader settingsReader = persistedFixConfigsReader.ReadFixConfig();
            if (persistedFixConfigsReader.IsRedirectedViaFluent != this.IsDivertedViaFluent)
            {
                this.Logger.Log(LogLevel.Fatal,
                    "Attempt to flip fluent redirection state ({0}->{1}) without full Integrator restart - this is not supported [{2}]",
                    this.IsDivertedViaFluent, persistedFixConfigsReader.IsRedirectedViaFluent,
                    persistedFixConfigsReader.ConfigName);
                return false;
            }
            this.SettingsRead(persistedFixConfigsReader.IsRedirectedViaFluent, persistedFixConfigsReader.IsQuotingSession);
            if (settingsReader == null)
            {
                this.Logger.Log(LogLevel.Fatal, "Attempt to Initialize session with null fix config [{0}]", persistedFixConfigsReader.ConfigName);
                return false;
            }

            SessionSettings settings;

            try
            {
                settings = new SessionSettings(settingsReader);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, e,
                                         "Unexpected exception during initialization fix session with config [{0}]",
                                         persistedFixConfigsReader.ConfigName);
                return false;
            }
            finally
            {
                settingsReader.Close();
            }

            if (persistedFixConfigsReader.ConfigName.Contains("ORD"))
            {
                settings.DefaultRelaxedValidationMessageTypes = new List<string>() { "8" };
                settings.DisconnectOnMismatches = true;
            }


            //Prevents persisting messages to disk!
            var storeFactory = new MemoryStoreFactory(false);
            this._logFacade = new FixLogFascade(logger);
            var logFactory = _logFacade;

            _initiator = new SocketInitiator(this, storeFactory, settings, logFactory);

            if (_settings.EnableThreasholdingOfUnexpectedMessages)
            {
                this._unexpectedMessagesRateChecker =
                    new RemovableEventsRateChecker(_settings.TimeIntervalToCheck, _settings.MaximumAllowedInstancesPerTimeInterval + 1);
            }

            this._unparsableMessagesRateCheckerBackup = new SingleThreadedEventsRateChecker(TimeSpan.FromSeconds(60), 150);

            return true;
        }

        private int _stopRequestsCnt = 0;
        private bool _wasStarted = false;

        private void CallOnStopInitiatedIfFristStopAttempt(bool async)
        {
            if (Interlocked.Increment(ref this._stopRequestsCnt) == 1 && this.OnStopInitiated != null)
                if (async)
                    TaskEx.StartNew(OnStopInitiated);
                else
                    this.OnStopInitiated();
        }

        private void ConfigureCommonStateMachineTransitions()
        {
            this._stateMachine
                .In(SessionState.Idle)
                    .On(SessionEvent.SendingLogon).Goto(SessionState.LoggingOn)
                    .On(SessionEvent.LogoutReceived).Goto(SessionState.Idle)
                    .ExecuteOnEntry(() =>
                    {
                        if (_wasStarted)
                        {
                            CallOnStopInitiatedIfFristStopAttempt(false);
                            if (this.OnStopped != null)
                                this.OnStopped();
                        }
                        _wasStarted = false;
                        this._unparsableMessagesRateChecker = null;
                    })
                    .On(SessionEvent.PhysicallyConnected).Goto(SessionState.PhysicallyConnected)
                .In(SessionState.PhysicallyConnected)
                    .On(SessionEvent.SendingLogon).Goto(SessionState.LoggingOn)
                    .On(SessionEvent.LogoutReceived).Goto(SessionState.Idle)
                    .ExecuteOnEntry(() =>
                    {
                        if (_wasStarted)
                        {
                            CallOnStopInitiatedIfFristStopAttempt(false);
                            if (this.OnStopped != null)
                                this.OnStopped();
                        }
                        _wasStarted = false;
                    })
                    .On(SessionEvent.StoppingSession).Goto(SessionState.Stopping)
                    .On(SessionEvent.PhysicallyConnected).Goto(SessionState.PhysicallyConnected)
                .In(SessionState.LoggingOn)
                    .On(SessionEvent.LogoutReceived).Goto(SessionState.Idle)
                    .On(SessionEvent.StoppingSession).Goto(SessionState.Stopping)
                    .On(SessionEvent.PhysicallyConnected).Goto(SessionState.PhysicallyConnected)
                    .ExecuteOnEntry(this.StartInitiator)
                .In(SessionState.Running)
                    .On(SessionEvent.StoppingSession).Goto(SessionState.Stopping)
                    .On(SessionEvent.LogoutReceived).Goto(SessionState.Idle)
                    .On(SessionEvent.SessionStatusReceived).Goto(SessionState.Running)
                    .ExecuteOnEntry(() =>
                    {
                        if (this.OnStarted != null)
                            this.OnStarted();
                        _wasStarted = true;
                        Interlocked.Exchange(ref this._stopRequestsCnt, 0);
                        //do not control unexpected messages for first minute
                        TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(60), () =>
                        {
                            this._unparsableMessagesRateChecker = this._unparsableMessagesRateCheckerBackup;
                        });
                    })
                .In(SessionState.Stopping)
                    .On(SessionEvent.StoppingSession).Goto(SessionState.Stopping)
                    .On(SessionEvent.LogoutReceived).Goto(SessionState.Idle)
                    .On(SessionEvent.SendingLogon).Goto(SessionState.LoggingOn)
                    .ExecuteOnEntry(this.StopInitiator);
        }

        private void StopInitiator()
        {
            if (this._initiator == null)
            {
                this.Logger.Log(LogLevel.Fatal, "Attempting to call Stop on initiator when it's null");
            }
            else
            {
                this._initiator.Stop();
            }
        }

        private void StartInitiator()
        {
            if (this._initiator == null)
            {
                this.Logger.Log(LogLevel.Fatal, "Attempting to call Start on initiator when it's null");
            }
            else
            {
                this._initiator.Start();
            }
        }

        protected void ConfigureStateMachineBasicTransitions()
        {
            if(this._isDivertedViaFluent)
                throw new Exception("Diverted sessions are not allowed to set their state machine");

            this.ConfigureStateMachineBasicTransitions_Internal();
        }

        private void ConfigureStateMachineBasicTransitions_Internal()
        {
            this._stateMachine
                .In(SessionState.Idle)
                    .On(SessionEvent.LogonResponseReceived).Goto(SessionState.Running)
                .In(SessionState.PhysicallyConnected)
                    .On(SessionEvent.LogonResponseReceived).Goto(SessionState.Running)
                .In(SessionState.LoggingOn)
                    .On(SessionEvent.LogonResponseReceived).Goto(SessionState.Running);
        }

        protected void ConfigureStateMachineToWaitForUnsolicitedSessionStatus()
        {
            if (this._isDivertedViaFluent)
                throw new Exception("Diverted sessions are not allowed to set their state machine");

            this._stateMachine
                .In(SessionState.Idle)
                    .On(SessionEvent.LogonResponseReceived).Goto(SessionState.Starting)
                .In(SessionState.PhysicallyConnected)
                    .On(SessionEvent.LogonResponseReceived).Goto(SessionState.Starting)
                .In(SessionState.LoggingOn)
                    .On(SessionEvent.LogonResponseReceived).Goto(SessionState.Starting)
                .In(SessionState.Starting)
                    .On(SessionEvent.SessionStatusReceived).Goto(SessionState.Running)
                    .On(SessionEvent.LogoutReceived).Goto(SessionState.Idle)
                    .On(SessionEvent.StoppingSession).Goto(SessionState.Stopping);
        }

        protected void ConfigureStateMachineToWaitForSessionStatusHandshake()
        {
            if (this._isDivertedViaFluent)
                throw new Exception("Diverted sessions are not allowed to set their state machine");

            this._stateMachine
                .In(SessionState.Idle)
                    .On(SessionEvent.LogonResponseReceived).Goto(SessionState.LoggedOn)
                .In(SessionState.PhysicallyConnected)
                    .On(SessionEvent.LogonResponseReceived).Goto(SessionState.LoggedOn)
                .In(SessionState.LoggingOn)
                    .On(SessionEvent.LogonResponseReceived).Goto(SessionState.LoggedOn)
                .In(SessionState.LoggedOn)
                    .On(SessionEvent.SendingSessionStatusRequest).Goto(SessionState.Starting)
                    .On(SessionEvent.LogoutReceived).Goto(SessionState.Idle)
                    .On(SessionEvent.StoppingSession).Goto(SessionState.Stopping)
                    .ExecuteOnEntry(() =>
                    {
                        SendSessionStatusRequest();
                        this._stateMachine.FireEvent(SessionEvent.SendingSessionStatusRequest);
                    })
                .In(SessionState.Starting)
                    .On(SessionEvent.SessionStatusReceived).Goto(SessionState.Running)
                    .On(SessionEvent.LogoutReceived).Goto(SessionState.Idle)
                    .On(SessionEvent.StoppingSession).Goto(SessionState.Stopping)
                .In(SessionState.Running);
        }

        protected virtual void SendSessionStatusRequest()
        {
        }

        protected void SessionStatusReceived()
        {
            this._stateMachine.FireEvent(SessionEvent.SessionStatusReceived);
        }

        public void ToAdmin(Message message, SessionID sessionID)
        {
            if (this.IsDivertedViaFluent)
            {
                if (message.Header.GetField(QuickFix.Fields.Tags.MsgType) == QuickFix.Fields.MsgType.LOGON)
                {
                    //auto populated from fix config
                    //message.SetField(new DefaultApplVerID("9"));
                    message.SetField(new Username(this._persistedFixConfigsReader.FluentCredentials.UserName));
                    message.SetField(new Password(this._persistedFixConfigsReader.FluentCredentials.Password));
                }

                message.SetField(new OnBehalfOfCompID(this._onBehalfOfCompId));
            }
            else
            {
                this.OnOutgoingAdminMessage(message, sessionID);
            }

            

            this.IntegratorPerfCounter.FixMessageSend();
            //do not log HBs
            if (this.Logger.IsEnabled(LogLevel.Debug) &&
                !NONTRANSLATED_MSG_TYPES.Contains(message.Header.GetField(QuickFix.Fields.Tags.MsgType)))
            {
                StringBuilder sb = new StringBuilder("---> Outgoing admin: ");
                message.AppendSelf(sb, QuickFixUtils.AppendFormattedTag);

                this.Logger.Log(LogLevel.Debug, sb.ToString());
            }
        }

        List<string> EXPECTED_MSG_TYPES = new List<string>() { QuickFix.Fields.MsgType.HEARTBEAT, QuickFix.Fields.MsgType.TESTREQUEST, QuickFix.Fields.MsgType.LOGON, QuickFix.Fields.MsgType.LOGOUT };
        List<string> NONTRANSLATED_MSG_TYPES = new List<string>() { QuickFix.Fields.MsgType.HEARTBEAT, QuickFix.Fields.MsgType.TESTREQUEST };

        public void FromAdmin(Message message, SessionID sessionID)
        {
            this.IntegratorPerfCounter.FixMessageReceived();

            string msgType = message.Header.GetField(QuickFix.Fields.Tags.MsgType);

            //do not log HBs
            if (this.Logger.IsEnabled(LogLevel.Debug) && msgType != QuickFix.Fields.MsgType.HEARTBEAT)
            {
                StringBuilder sb = new StringBuilder("<--- Incoming admin: ");
                message.AppendSelf(sb, QuickFixUtils.AppendFormattedTag);

                this.Logger.Log(LogLevel.Debug, sb.ToString());
            }

            if (QuickFix.Fields.MsgType.REJECT.Equals(msgType))
            {
                this.CrackMessage(message, sessionID);
            }

            if (_unexpectedMessagesRateChecker != null && EXPECTED_MSG_TYPES.Contains(msgType))
            {
                this._unexpectedMessagesRateChecker.FlagLastEventAsRemoved();
            }
        }

        protected virtual void OnOutgoingMessage(Message message, SessionID sessionId) { }
        protected virtual void OnOutgoingAdminMessage(Message message, SessionID sessionId) { }

        public void ToApp(Message message, SessionID sessionId)
        {
            if (this.IsDivertedViaFluent)
            {
                message.SetField(new QuickFix.Fields.OnBehalfOfCompID(this._onBehalfOfCompId));
            }
            else
            {
                this.OnOutgoingMessage(message, sessionId);
            }

            this.IntegratorPerfCounter.FixMessageSend();
        }

        public void FromApp(Message message, SessionID sessionID)
        {
            this.IntegratorPerfCounter.FixMessageReceived();

            this.CrackMessage(message, sessionID);

            if (_unexpectedMessagesRateChecker != null)
            {
                this._unexpectedMessagesRateChecker.FlagLastEventAsRemoved();
            }
        }


        private readonly bool _isDivertedViaFluent;
        //to speed up
        private readonly string _onBehalfOfCompId;
        public bool IsDivertedViaFluent { get { return _isDivertedViaFluent; } }

        public void OnBytesRead(int bytesCount)
        {
            this.IntegratorPerfCounter.BytesRead(bytesCount);
        }

        public void OnSlowParsedMessage()
        {
            if (this._unparsableMessagesRateChecker != null && !this._unparsableMessagesRateChecker.AddNextEventAndCheckIsAllowed())
            {
                this.Logger.Log(LogLevel.Fatal,
                    "Exceeded maximum allowed rate of FIX messages that have a type which is unknown to quick parser - stopping the session. Messages might still be valid, however parsing is slower and more resources intensive. Comment BypassParsing=Y in .cfg to disable quick parser.");

                if (this.State == SessionState.Stopping)
                {
                    this.Logger.Log(LogLevel.Error, "Encountered mesage unparsable by quick parser in stopping state - forcefully closing the session");
                    this._initiator.ForcefullStop("Too many uncommon messages");
                }
                else
                {
                    TaskEx.StartNew(this.Stop);
                }

            }
        }

        public void OnUnverifiedMessage()
        {
            if (this._unexpectedMessagesRateChecker != null && !this._unexpectedMessagesRateChecker.AddNextEventAndCheckIsAllowed())
            {
                this.Logger.Log(LogLevel.Fatal, "Exceeded maximum allowed rate of unexpected FIX messages - stopping the session");

                if (this.State == SessionState.Stopping)
                {
                    this.Logger.Log(LogLevel.Error, "Encountered unexpected mesage in stopping state - forcefully closing the session");
                    this._initiator.ForcefullStop("Too many unexpected messages");
                }
                else
                {
                    TaskEx.StartNew(this.Stop);
                }

            }
        }

        private void CrackMessage(Message message, SessionID sessionID)
        {
            bool cracked = false;

            try
            {
                cracked = this.Crack(message, sessionID) || this.TryHandleMessage(message);
            }
            catch (FieldNotFoundException e)
            {
                this.Logger.LogException(LogLevel.Error,
                                         string.Format(
                                             "Experienced unexpectedly formatted message during cracking. Missing tag {0}",
                                             e.Field), e);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Error, "Experienced unexpected error during message cracking.", e);
                throw;
            }


            if (!cracked)
            {
                this.Logger.Log(LogLevel.Error,
                                "Encountered message of unsupported type or formatting. Type: [{0}], Message: [{1}]",
                                message.GetType(), message);
            }
        }

        protected virtual bool TryHandleMessage(Message message)
        {
            return false;
        }

        public void OnCreate(SessionID sessionID)
        {
            this.SessionId = sessionID;

            if (this.Logger.IsEnabled(LogLevel.Info))
            {
                StringBuilder sb = new StringBuilder("Creating new session: ");
                sb.AppendLine();
                this.AppendSessionInfo(sb, sessionID);
                this.Logger.Log(LogLevel.Info, sb.ToString());
            }
        }

        public void OnPhysicllyConnected(SessionID sessionID)
        {
            this._stateMachine.FireEvent(SessionEvent.PhysicallyConnected);
        }

        public void OnLogout(SessionID sessionID)
        {
            //this._sessionId = null;

            if (this.Logger.IsEnabled(LogLevel.Info))
            {
                StringBuilder sb = new StringBuilder("Logged out from: ");
                sb.AppendLine();
                this.AppendSessionInfo(sb, sessionID);
                this.Logger.Log(LogLevel.Info, sb.ToString());
            }

            //Wait a bit so that physically connected state can be visible in monitoring UI
            if (this._stateMachine.CurrentState == SessionState.PhysicallyConnected)
                Thread.Sleep(TimeSpan.FromSeconds(10));

            this._stateMachine.FireEvent(SessionEvent.LogoutReceived);
        }

        public void OnLogon(SessionID sessionID)
        {
            if (this.Logger.IsEnabled(LogLevel.Info))
            {
                StringBuilder sb = new StringBuilder("Successfuly Logged on: ");
                sb.AppendLine();
                this.AppendSessionInfo(sb, sessionID);
                this.Logger.Log(LogLevel.Info, sb.ToString());
            }
            this._stateMachine.FireEvent(SessionEvent.LogonResponseReceived);
        }

        private void AppendSessionInfo(StringBuilder sb, SessionID sessionID)
        {
            sb.AppendFormat("BeginString: {0}", sessionID.BeginString);
            sb.AppendLine();
            sb.AppendFormat("SenderCompID: {0}", sessionID.SenderCompID);
            sb.AppendLine();
            sb.AppendFormat("TargetCompID: {0}", sessionID.TargetCompID);
        }

        public void Start()
        {
            this.Logger.Log(LogLevel.Info, "Creating and starting session initiator...");
            if (this.Initialize(this._persistedFixConfigsReader, this.Logger))
                this._stateMachine.FireEvent(SessionEvent.SendingLogon);
        }

        public void Stop()
        {
            this.Logger.Log(LogLevel.Info, "Stopping session initiator..");

            if (_initiator == null)
                return;

            CallOnStopInitiatedIfFristStopAttempt(false);
            if (this._stateMachine.CurrentState == SessionState.Idle)
            {
                if (this._initiator.IsStopped)
                {
                    this.Logger.Log(LogLevel.Info, "Stopping stopped initiator - we will dispose the session store and it can be now overwritten");
                    _initiator.Dispose();
                }
                else
                {
                    this._initiator.Stop();
                }
            }
            else
            {
                this._stateMachine.FireEvent(SessionEvent.StoppingSession);
            }
        }

        public void StopAsync(TimeSpan delay)
        {
            CallOnStopInitiatedIfFristStopAttempt(true);
            TaskEx.ExecAfterDelayWithErrorHandling(delay, this.Stop);
        }

        public event Action OnStarted;

        public event Action OnStopped;
        public event Action OnStopInitiated;

        public SessionState State
        {
            get { return this._stateMachine.CurrentState; }
        }

        public bool IsReadyAndOpen { get { return this._stateMachine.CurrentState == SessionState.Running; } }

        public bool Inactivated { get { return this._initiator == null || this._initiator.IsStopped; } }
    }


}
