﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.Fields;

namespace Kreslik.Integrator.FIXMessaging
{
    //TODO:
    // overload indicative quote detection
    //
    // Used by: CZB, SOC, HSB (FAL could potentially use - but it cracks Incremental and Full refreshes by single handler)
    internal class BankMarketDataSnapshotFullRefreshCracker : IFixMessageCracker
    {
        private IMarketDataPerentChannel _parentChannel;
        private ILogger _logger;
        private DateTime _currentMessageReceived;
        private QuoteObject _sharedQuote;

        public BankMarketDataSnapshotFullRefreshCracker(IMarketDataPerentChannel parentChannel, ILogger logger)
        {
            this._parentChannel = parentChannel;
            this._logger = logger;
            this._sharedQuote = parentChannel.PriceReceivingHelper.SharedQuote;
        }

        private const byte _recognizedType = (byte) 'W';

        public byte RecognizedMessageType
        {
            get { return _recognizedType; }
        }

        public bool TryRecoverFromParsingErrors
        {
            get { return false; }
        }

        public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
        {
            switch (tagId)
            {
                case 262:
                    OnTag262(tagValueSegment);
                    break;
                case 268:
                    OnTag268(tagValueSegment);
                    break;
                case 269:
                    OnTag269(tagValueSegment);
                    break;
                case 270:
                    OnTag270(tagValueSegment);
                    break;
                case 271:
                    OnTag271(tagValueSegment);
                    break;
                case 52:
                    OnTag52(tagValueSegment);
                    break;
                case 276:
                    OnTag276(tagValueSegment);
                    break;
                case 299:
                    OnTag299(tagValueSegment);
                    break;
                case 55:
                    OnTag55(tagValueSegment);
                    break;
                default:
                    OnCustomTag(tagId, tagValueSegment);
                    break;
            }
        }

        protected virtual void OnCustomTag(int tagId, BufferSegmentBase tagValueSegment){ }

        private Common.Symbol _symbol = Common.Symbol.NULL;
        private string _subscriptionIdentity = string.Empty;
        private AtomicDecimal _bidPrice = 0;
        private AtomicDecimal _askPrice = 0;
        private decimal _bidSize = 0;
        private decimal _askSize = 0;
        private DateTime _sendingTime = DateTime.MinValue;
        protected bool? _isBid = null;
        protected readonly byte[] _bidCounterpartyIdentity = new byte[PriceObject.MAX_CTP_IDENTITY_LENGTH];
        protected readonly byte[] _askCounterpartyIdentity = new byte[PriceObject.MAX_CTP_IDENTITY_LENGTH];
        protected bool _bidIsIndicative;
        protected bool _askIsIndicative;

        public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
        {
            this._currentMessageReceived = currentMessageReceivedTimeUtc;
            this._symbol = Common.Symbol.NULL;
            this._subscriptionIdentity = null;
            this._sendingTime = DateTime.MinValue;
            this.ResetTier();
        }

        private void ResetTier()
        {
            this._bidPrice = AtomicDecimal.Zero;
            this._askPrice = AtomicDecimal.Zero;
            this._bidSize = 0;
            this._askSize = 0;
            this._isBid = null;
            this._bidCounterpartyIdentity[0] = 0;
            this._askCounterpartyIdentity[0] = 0;
            this._bidIsIndicative = false;
            this._askIsIndicative = false;
        }

        public void MessageParsingDone()
        {
            if (this._symbol == Common.Symbol.NULL || this._subscriptionIdentity == null)
                throw new HaltingTagException("Incomplete message - missing tag", this._symbol == Common.Symbol.NULL ? 55 : 262);

            DateTime receivedAndParsed = this._currentMessageReceived;

            if (this._bidIsIndicative)
                this._bidSize = 0;
            if (this._askIsIndicative)
                this._askSize = 0;


            if (this._bidSize == 0 && this._askSize == 0)
            {
                this._parentChannel.SendLastQuoteCancel(this._subscriptionIdentity);
            }
            else
            {
                _sharedQuote.OverrideContent(
                    askPrice: this._askPrice,
                    askSize: this._askSize,
                    bidPrice: this._bidPrice,
                    bidSize: this._bidSize,
                    symbol: this._symbol,
                    counterparty: this._parentChannel.Counterparty,
                    subscriptionIdentity: this._subscriptionIdentity,
                    counterpartyTimeUtc: this._sendingTime,
                    marketDataRecordType: MarketDataRecordType.BankQuoteData,
                    intagratorTimeUtc: receivedAndParsed,
                    counterpartyAskIdentity: this._askCounterpartyIdentity,
                    counterpartyBidIdentity: this._bidCounterpartyIdentity);

                this._parentChannel.SendNewQuote(_sharedQuote);
            }
        }


        private void OnTag262(BufferSegmentBase bufferSegment)
        {
            this._subscriptionIdentity = this._parentChannel.GetSubscriptionIdentity(bufferSegment);
        }

        private const int MaxNumOfMDEntries = 2;

        private void OnTag268(BufferSegmentBase bufferSegment)
        {
            if (bufferSegment.ToInt() > MaxNumOfMDEntries)
            {
                string error = string.Format("Received quote with unexpected number of layers: {0}",
                    bufferSegment.GetContentAsAsciiString());
                this._logger.Log(LogLevel.Error, error);
                throw new HaltingTagException(error, 268);
            }
        }

        private void OnTag269(BufferSegmentBase bufferSegment)
        {
            if (bufferSegment[0] == '0')
                _isBid = true;
            else if (bufferSegment[0] == '1')
                _isBid = false;
            else
                _isBid = null;
        }

        private void OnTag270(BufferSegmentBase bufferSegment)
        {
            if (this._isBid.HasValue)
            {
                AtomicDecimal pxValue = bufferSegment.ToAtomicDecimal();
                if (this._isBid.Value)
                    this._bidPrice = pxValue;
                else
                    this._askPrice = pxValue;
            }
        }

        private void OnTag271(BufferSegmentBase bufferSegment)
        {
            if (this._isBid.HasValue)
            {
                decimal sizeValue = bufferSegment.ToDecimal();
                if (this._isBid.Value)
                    this._bidSize = sizeValue;
                else
                    this._askSize = sizeValue;
            }
        }

        //QuoteEntryID = 299
        private void OnTag299(BufferSegmentBase bufferSegment)
        {
            if (this._isBid.HasValue)
            {
                if (this._isBid.Value)
                    bufferSegment.CopyToClearingRest(this._bidCounterpartyIdentity);
                else
                    bufferSegment.CopyToClearingRest(this._askCounterpartyIdentity);
            }
        }

        //QuoteCondition = 276
        private void OnTag276(BufferSegmentBase bufferSegment)
        {
            if (this._isBid.HasValue)
            {
                bool isIndicative;
                switch ((char) bufferSegment[0])
                {
                    case 'A': //QuoteCondition.OPEN:
                        isIndicative = false;
                        break;
                    case 'B': //QuoteCondition.CLOSED:
                    case 'I': //QuoteCondition.NON_FIRM
                        isIndicative = true;
                        break;
                    default:
                        throw new NonHaltingTagException(
                            "Receiving (and ignoring) Incremental MD refresh with unexpected update type", 279);
                }

                if (this._isBid.Value)
                    this._bidIsIndicative = isIndicative;
                else
                    this._askIsIndicative = isIndicative;
            }
        }

        //Symbol = 55
        private void OnTag55(BufferSegmentBase bufferSegment)
        {
            if (this._symbol == Common.Symbol.NULL)
                this._symbol = bufferSegment.ToSymbol();
            else if (this._symbol != bufferSegment.ToSymbol())
                throw new HaltingTagException("Receiving MD update for multiple symbols in one message", 55);
        }

        private void OnTag52(BufferSegmentBase bufferSegment)
        {
            this._sendingTime = bufferSegment.ToDateTime();
        }
    }

    internal sealed class MGSBankMarketDataSnapshotFullRefreshCracker : BankMarketDataSnapshotFullRefreshCracker
    {
        public MGSBankMarketDataSnapshotFullRefreshCracker(IMarketDataPerentChannel parentChannel, ILogger logger)
            : base(parentChannel, logger)
        {

        }

        protected override void OnCustomTag(int tagId, BufferSegmentBase tagValueSegment)
        {
            if (tagId == 1070 /*Tags.MDQuoteType*/)
            {
                if (this._isBid.HasValue)
                {
                    bool isIndicative;
                    switch ((char)tagValueSegment[0])
                    {
                        case '1': //MDQuoteType.TRADEABLE:
                            isIndicative = false;
                            break;
                        case '0': //MDQuoteType.INDICATIVE:
                            isIndicative = true;
                            break;
                        default:
                            throw new NonHaltingTagException(
                                "Receiving (and ignoring) Incremental MD refresh with unexpected update type", 1070);
                    }

                    if (this._isBid.Value)
                        this._bidIsIndicative = isIndicative;
                    else
                        this._askIsIndicative = isIndicative;
                }
            }
        }
    }

    internal sealed class RBSBankMarketDataSnapshotFullRefreshCracker : BankMarketDataSnapshotFullRefreshCracker
    {
        public RBSBankMarketDataSnapshotFullRefreshCracker(IMarketDataPerentChannel parentChannel, ILogger logger)
            : base(parentChannel, logger)
        {

        }

        protected override void OnCustomTag(int tagId, BufferSegmentBase tagValueSegment)
        {
            if (tagId == 117 /*Tags.QuoteID*/)
            {
                tagValueSegment.CopyToClearingRest(this._bidCounterpartyIdentity);
                tagValueSegment.CopyToClearingRest(this._askCounterpartyIdentity);
            }
        }
    }

    //JPM
    internal class BankMarketDataIncrementalRefreshCracker : IFixMessageCracker
    {
        private IMarketDataPerentChannel _parentChannel;
        private ILogger _logger;
        private DateTime _currentMessageReceived;
        private QuoteObject _sharedQuote;

        public BankMarketDataIncrementalRefreshCracker(IMarketDataPerentChannel parentChannel, ILogger logger)
        {
            this._parentChannel = parentChannel;
            this._logger = logger;
            this._sharedQuote = parentChannel.PriceReceivingHelper.SharedQuote;
        }

        private const byte _recognizedType = (byte)'X';

        public byte RecognizedMessageType
        {
            get { return _recognizedType; }
        }

        public bool TryRecoverFromParsingErrors
        {
            get { return false; }
        }

        public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
        {
            switch (tagId)
            {
                case 262:
                    OnTag262(tagValueSegment);
                    break;
                case 268:
                    OnTag268(tagValueSegment);
                    break;
                case 269:
                    OnTag269(tagValueSegment);
                    break;
                case 270:
                    OnTag270(tagValueSegment);
                    break;
                case 271:
                    OnTag271(tagValueSegment);
                    break;
                case 52:
                    OnTag52(tagValueSegment);
                    break;
                case 279:
                    OnTag279(tagValueSegment);
                    break;
                case 299:
                    OnTag299(tagValueSegment);
                    break;
                case 55:
                    OnTag55(tagValueSegment);
                    break;
                default:
                    OnCustomTag(tagId, tagValueSegment);
                    break;
            }
        }

        protected virtual void OnCustomTag(int tagId, BufferSegmentBase tagValueSegment) { }

        private Common.Symbol _symbol = Common.Symbol.NULL;
        private string _subscriptionIdentity = string.Empty;
        private AtomicDecimal _price = 0;
        private decimal _size = 0;
        private DateTime _sendingTime = DateTime.MinValue;
        protected bool? _isBid = null;
        protected readonly byte[] _counterpartyIdentity = new byte[PriceObject.MAX_CTP_IDENTITY_LENGTH];
        protected bool _isIndicative;

        public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
        {
            this._currentMessageReceived = currentMessageReceivedTimeUtc;
            this._symbol = Common.Symbol.NULL;
            this._subscriptionIdentity = null;
            this._sendingTime = DateTime.MinValue;
            this.ResetTier();
        }

        private void ResetTier()
        {
            this._price = AtomicDecimal.Zero;
            this._size = 0m;
            this._isBid = null;
            this._counterpartyIdentity[0] = 0;
            this._isIndicative = false;
        }

        public void MessageParsingDone()
        {
            if (this._subscriptionIdentity == null)
                throw new HaltingTagException("Incomplete message - missing tag", 262);

            DateTime receivedAndParsed = this._currentMessageReceived;

            if (this._isIndicative)
            {
                this._size = 0;
            }

            if (this._size == 0 || !this._isBid.HasValue)
            {
                this._parentChannel.SendLastQuoteCancel(this._subscriptionIdentity);
            }
            else
            {
                if (this._symbol == Common.Symbol.NULL)
                    throw new HaltingTagException("Incomplete message - missing tag", 55);

                PriceObjectInternal priceObject = this._parentChannel.PriceReceivingHelper.GetPriceObject(
                        priceConverted: this._price,
                        sizeBaseAbsInitial: this._size,
                        side: this._isBid.Value ? PriceSide.Bid : PriceSide.Ask,
                        symbol: this._symbol,
                        counterparty: this._parentChannel.Counterparty,
                        counterpartyIdentity: this._counterpartyIdentity,
                        marketDataRecordType: MarketDataRecordType.BankQuoteData,
                        integratorReceivedTime: receivedAndParsed,
                        counterpartySentTime: this._sendingTime);

                this._parentChannel.SendNewPrice(priceObject, this._subscriptionIdentity);
            }
        }


        private void OnTag262(BufferSegmentBase bufferSegment)
        {
            this._subscriptionIdentity = this._parentChannel.GetSubscriptionIdentity(bufferSegment);
        }

        private const int MaxNumOfMDEntries = 1;

        //NoMDEntries = 268
        private void OnTag268(BufferSegmentBase bufferSegment)
        {
            if (bufferSegment.ToInt() > MaxNumOfMDEntries)
            {
                string error = string.Format("Received quote with unexpected number of layers: {0}",
                    bufferSegment.GetContentAsAsciiString());
                this._logger.Log(LogLevel.Error, error);
                throw new HaltingTagException(error, 268);
            }
        }

        //MDEntryType = 269
        private void OnTag269(BufferSegmentBase bufferSegment)
        {
            if (bufferSegment[0] == '0')
                _isBid = true;
            else if (bufferSegment[0] == '1')
                _isBid = false;
            else
                _isBid = null;
        }

        // MDEntryPx = 270
        private void OnTag270(BufferSegmentBase bufferSegment)
        {
            this._price = bufferSegment.ToAtomicDecimal();
        }

        //MDEntrySize = 271
        private void OnTag271(BufferSegmentBase bufferSegment)
        {
            this._size = bufferSegment.ToDecimal();
        }

        //QuoteEntryID = 299
        private void OnTag299(BufferSegmentBase bufferSegment)
        {
            bufferSegment.CopyToClearingRest(this._counterpartyIdentity);
        }

        //MDUpdateAction = 279
        private void OnTag279(BufferSegmentBase bufferSegment)
        {

            switch ((char) bufferSegment[0])
            {
                case MDUpdateAction.NEW:
                case MDUpdateAction.CHANGE:
                    break;
                case MDUpdateAction.DELETE:
                    this._isIndicative = true;
                    break;
                default:
                    this._logger.Log(LogLevel.Error,
                        "Receiving (and ignoring) Incremental MD refresh with unexpected update type: {0}",
                        (char) bufferSegment[0]);
                    this._isIndicative = true;
                    break;
            }
        }

        //Symbol = 55
        private void OnTag55(BufferSegmentBase bufferSegment)
        {
            if (this._symbol == Common.Symbol.NULL)
                this._symbol = bufferSegment.ToSymbol();
            else if (this._symbol != bufferSegment.ToSymbol())
                throw new HaltingTagException("Receiving MD update for multiple symbols in one message", 55);
        }

        private void OnTag52(BufferSegmentBase bufferSegment)
        {
            this._sendingTime = bufferSegment.ToDateTime();
        }
    }



    // BOA, CRS, CTI, GLS, NOM
    internal sealed class BankQuoteDataCracker : IFixMessageCracker
    {
        private IMarketDataPerentChannel _parentChannel;
        private ILogger _logger;
        private DateTime _currentMessageReceived;
        private QuoteObject _sharedQuote;

        public BankQuoteDataCracker(IMarketDataPerentChannel parentChannel, ILogger logger)
        {
            this._parentChannel = parentChannel;
            this._logger = logger;
            this._sharedQuote = parentChannel.PriceReceivingHelper.SharedQuote;
        }

        private const byte _recognizedType = (byte)'S';

        public byte RecognizedMessageType
        {
            get { return _recognizedType; }
        }

        public bool TryRecoverFromParsingErrors
        {
            get { return false; }
        }

        public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
        {
            switch (tagId)
            {
                case 132:
                    OnTag132(tagValueSegment);
                    break;
                case 133:
                    OnTag133(tagValueSegment);
                    break;
                case 134:
                    OnTag134(tagValueSegment);
                    break;
                case 135:
                    OnTag135(tagValueSegment);
                    break;
                case 52:
                    OnTag52(tagValueSegment);
                    break;
                case 117:
                    OnTag117(tagValueSegment);
                    break;
                case 131:
                    OnTag131(tagValueSegment);
                    break;
                case 537:
                    OnTag537(tagValueSegment);
                    break;
                case 55:
                    OnTag55(tagValueSegment);
                    break;
            }
        }

        private Common.Symbol _symbol = Common.Symbol.NULL;
        private string _subscriptionIdentity = string.Empty;
        private AtomicDecimal _bidPrice = 0;
        private AtomicDecimal _askPrice = 0;
        private decimal _bidSize = 0;
        private decimal _askSize = 0;
        private DateTime _sendingTime = DateTime.MinValue;
        private readonly byte[] _counterpartyIdentity = new byte[PriceObject.MAX_CTP_IDENTITY_LENGTH];
        private bool _bidIsIndicative;
        private bool _askIsIndicative;

        public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
        {
            this._currentMessageReceived = currentMessageReceivedTimeUtc;
            this._symbol = Common.Symbol.NULL;
            this._subscriptionIdentity = null;
            this._sendingTime = DateTime.MinValue;
            this.ResetTier();
        }

        private void ResetTier()
        {
            this._bidPrice = AtomicDecimal.Zero;
            this._askPrice = AtomicDecimal.Zero;
            this._bidSize = 0;
            this._askSize = 0;
            this._counterpartyIdentity[0] = 0;
            this._bidIsIndicative = false;
            this._askIsIndicative = false;
        }

        public void MessageParsingDone()
        {
            if (this._symbol == Common.Symbol.NULL || this._subscriptionIdentity == null)
                throw new HaltingTagException("Incomplete message - missing tag", this._symbol == Common.Symbol.NULL ? 55 : 131);

            DateTime receivedAndParsed = this._currentMessageReceived;

            if (this._bidIsIndicative)
                this._bidSize = 0;
            if (this._askIsIndicative)
                this._askSize = 0;

            if (this._bidSize == 0 && this._askSize == 0)
            {
                this._parentChannel.SendLastQuoteCancel(this._subscriptionIdentity);
            }
            else
            {
                //bid not set - just one sided quote
                if (!this._bidIsIndicative && this._bidSize == 0m)
                {
                    PriceObjectInternal askPrice = this._parentChannel.PriceReceivingHelper.GetPriceObject(
                        priceConverted: this._askPrice,
                        sizeBaseAbsInitial: this._askSize,
                        side: PriceSide.Ask,
                        symbol: this._symbol,
                        counterparty: this._parentChannel.Counterparty,
                        counterpartyIdentity: this._counterpartyIdentity,
                        marketDataRecordType: MarketDataRecordType.BankQuoteData,
                        integratorReceivedTime: receivedAndParsed,
                        counterpartySentTime: this._sendingTime);

                    this._parentChannel.SendNewPrice(askPrice, this._subscriptionIdentity);
                }
                //ask not set - just one sided quote
                else if (!this._askIsIndicative && this._askSize == 0m)
                {
                    PriceObjectInternal bidPrice = this._parentChannel.PriceReceivingHelper.GetPriceObject(
                        priceConverted: this._bidPrice,
                        sizeBaseAbsInitial: this._bidSize,
                        side: PriceSide.Bid,
                        symbol: this._symbol,
                        counterparty: this._parentChannel.Counterparty,
                        counterpartyIdentity: this._counterpartyIdentity,
                        marketDataRecordType: MarketDataRecordType.BankQuoteData,
                        integratorReceivedTime: receivedAndParsed,
                        counterpartySentTime: this._sendingTime);

                    this._parentChannel.SendNewPrice(bidPrice, this._subscriptionIdentity);
                }
                else
                {
                    //if anything is zero - higher layers will cancel

                    _sharedQuote.OverrideContent(
                        askPrice: this._askPrice,
                        askSize: this._askSize,
                        bidPrice: this._bidPrice,
                        bidSize: this._bidSize,
                        symbol: this._symbol,
                        counterparty: this._parentChannel.Counterparty,
                        subscriptionIdentity: this._subscriptionIdentity,
                        counterpartyTimeUtc: this._sendingTime,
                        marketDataRecordType: MarketDataRecordType.BankQuoteData,
                        intagratorTimeUtc: receivedAndParsed,
                        //override content will copy those buffers to it's internal buffer, so we can reuse
                        counterpartyAskIdentity: this._counterpartyIdentity,
                        counterpartyBidIdentity: this._counterpartyIdentity);

                    this._parentChannel.SendNewQuote(_sharedQuote);
                }
            }
        }


        //BidPx = 132
        private void OnTag132(BufferSegmentBase bufferSegment)
        {
            this._bidPrice = bufferSegment.ToAtomicDecimal();
            if (this._bidPrice == AtomicDecimal.Zero)
                this._bidIsIndicative = true;
        }

        //OfferPx = 133
        private void OnTag133(BufferSegmentBase bufferSegment)
        {
            this._askPrice = bufferSegment.ToAtomicDecimal();
            if (this._askPrice == AtomicDecimal.Zero)
                this._askIsIndicative = true;
        }

        //BidSize = 134
        private void OnTag134(BufferSegmentBase bufferSegment)
        {
            this._bidSize = bufferSegment.ToDecimal();
            if (this._bidSize == 0m)
                this._bidIsIndicative = true;
        }

        //OfferSize = 135
        private void OnTag135(BufferSegmentBase bufferSegment)
        {
            this._askSize = bufferSegment.ToDecimal();
            if (this._askSize == 0m)
                this._askIsIndicative = true;
        }

        //QuoteID = 117
        private void OnTag117(BufferSegmentBase bufferSegment)
        {
            bufferSegment.CopyToClearingRest(this._counterpartyIdentity);
        }


        //Symbol = 55
        private void OnTag55(BufferSegmentBase bufferSegment)
        {
            if (this._symbol == Common.Symbol.NULL)
                this._symbol = bufferSegment.ToSymbol();
            else if (this._symbol != bufferSegment.ToSymbol())
                throw new HaltingTagException("Receiving MD update for multiple symbols in one message", 55);
        }

        private void OnTag52(BufferSegmentBase bufferSegment)
        {
            this._sendingTime = bufferSegment.ToDateTime();
        }

        //QuoteRequestId = 131
        private void OnTag131(BufferSegmentBase bufferSegment)
        {
            this._subscriptionIdentity = this._parentChannel.GetSubscriptionIdentity(bufferSegment);
        }

        //QuoteCondition = 537
        private void OnTag537(BufferSegmentBase bufferSegment)
        {
            int condition = bufferSegment.ToInt();
            if (condition != 1 && condition != 2)
            {
                this._askIsIndicative = true;
                this._bidIsIndicative = true;
            }
        }
    }



    //BNP
    internal sealed class BankMassQuoteDataCracker : IFixMessageCracker
    {
        private IMarketDataPerentChannel _parentChannel;
        private ILogger _logger;
        private DateTime _currentMessageReceived;
        private QuoteObject _sharedQuote;

        public BankMassQuoteDataCracker(IMarketDataPerentChannel parentChannel, ILogger logger)
        {
            this._parentChannel = parentChannel;
            this._logger = logger;
            this._sharedQuote = parentChannel.PriceReceivingHelper.SharedQuote;
        }

        private const byte _recognizedType = (byte)'i';

        public byte RecognizedMessageType
        {
            get { return _recognizedType; }
        }

        public bool TryRecoverFromParsingErrors
        {
            get { return false; }
        }

        public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
        {
            switch (tagId)
            {
                case 296:
                    OnTag296(tagValueSegment);
                    break;
                case 295:
                    OnTag295(tagValueSegment);
                    break;
                case 132:
                    OnTag132(tagValueSegment);
                    break;
                case 133:
                    OnTag133(tagValueSegment);
                    break;
                case 134:
                    OnTag134(tagValueSegment);
                    break;
                case 135:
                    OnTag135(tagValueSegment);
                    break;
                case 52:
                    OnTag52(tagValueSegment);
                    break;
                case 299:
                    OnTag299(tagValueSegment);
                    break;
                case 131:
                    OnTag131(tagValueSegment);
                    break;
                case 55:
                    OnTag55(tagValueSegment);
                    break;
            }
        }

        private Common.Symbol _symbol = Common.Symbol.NULL;
        private string _subscriptionIdentity = string.Empty;
        private AtomicDecimal _bidPrice = 0;
        private AtomicDecimal _askPrice = 0;
        private decimal _bidSize = 0;
        private decimal _askSize = 0;
        private DateTime _sendingTime = DateTime.MinValue;
        private readonly byte[] _counterpartyIdentity = new byte[PriceObject.MAX_CTP_IDENTITY_LENGTH];

        public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
        {
            this._currentMessageReceived = currentMessageReceivedTimeUtc;
            this._symbol = Common.Symbol.NULL;
            this._subscriptionIdentity = null;
            this._sendingTime = DateTime.MinValue;
            this.ResetTier();
        }

        private void ResetTier()
        {
            this._bidPrice = AtomicDecimal.Zero;
            this._askPrice = AtomicDecimal.Zero;
            this._bidSize = 0;
            this._askSize = 0;
            this._counterpartyIdentity[0] = 0;
        }

        public void MessageParsingDone()
        {
            if (this._symbol == Common.Symbol.NULL || this._subscriptionIdentity == null)
                throw new HaltingTagException("Incomplete message - missing tag", this._symbol == Common.Symbol.NULL ? 55 : 131);

            DateTime receivedAndParsed = this._currentMessageReceived;


            if ((this._bidSize == 0 && this._askSize == 0) ||
                (this._bidPrice == AtomicDecimal.Zero && this._askPrice == AtomicDecimal.Zero))
            {
                this._parentChannel.SendLastQuoteCancel(this._subscriptionIdentity);
            }
            else
            {
                //if anything is zero - higher layers will cancel
                _sharedQuote.OverrideContent(
                    askPrice: this._askPrice,
                    askSize: this._askSize,
                    bidPrice: this._bidPrice,
                    bidSize: this._bidSize,
                    symbol: this._symbol,
                    counterparty: this._parentChannel.Counterparty,
                    subscriptionIdentity: this._subscriptionIdentity,
                    counterpartyTimeUtc: this._sendingTime,
                    marketDataRecordType: MarketDataRecordType.BankQuoteData,
                    intagratorTimeUtc: receivedAndParsed,
                    //override content will copy those buffers to it's internal buffer, so we can reuse
                    counterpartyAskIdentity: this._counterpartyIdentity,
                    counterpartyBidIdentity: this._counterpartyIdentity);

                this._parentChannel.SendNewQuote(_sharedQuote);
            }
        }


        //NoQuoteSets = 296
        private void OnTag296(BufferSegmentBase bufferSegment)
        {
            if (bufferSegment.ToInt() != 1)
            {
                string error = string.Format("Received massquote with unexpected number of quote sets: {0}",
                    bufferSegment.ToInt());
                this._logger.Log(LogLevel.Error, error);
                throw new HaltingTagException(error, 296);
            }
        }

        //NoQuoteEntries = 295
        private void OnTag295(BufferSegmentBase bufferSegment)
        {
            if (bufferSegment.ToInt() != 1)
            {
                string error = string.Format("Received massquote with unexpected number of quote entries: {0}",
                    bufferSegment.ToInt());
                this._logger.Log(LogLevel.Error, error);
                throw new HaltingTagException(error, 295);
            }
        }

        //BidPx = 132
        private void OnTag132(BufferSegmentBase bufferSegment)
        {
            this._bidPrice = bufferSegment.ToAtomicDecimal();
        }

        //OfferPx = 133
        private void OnTag133(BufferSegmentBase bufferSegment)
        {
            this._askPrice = bufferSegment.ToAtomicDecimal();
        }

        //BidSize = 134
        private void OnTag134(BufferSegmentBase bufferSegment)
        {
            this._bidSize = bufferSegment.ToDecimal();
        }

        //OfferSize = 135
        private void OnTag135(BufferSegmentBase bufferSegment)
        {
            this._askSize = bufferSegment.ToDecimal();
        }

        //QuoteEntryID = 299
        private void OnTag299(BufferSegmentBase bufferSegment)
        {
            bufferSegment.CopyToClearingRest(this._counterpartyIdentity);
        }


        //Symbol = 55
        private void OnTag55(BufferSegmentBase bufferSegment)
        {
            if (this._symbol == Common.Symbol.NULL)
                this._symbol = bufferSegment.ToSymbol();
            else if (this._symbol != bufferSegment.ToSymbol())
                throw new HaltingTagException("Receiving MD update for multiple symbols in one message", 55);
        }

        private void OnTag52(BufferSegmentBase bufferSegment)
        {
            this._sendingTime = bufferSegment.ToDateTime();
        }

        //QuoteRequestId = 131
        private void OnTag131(BufferSegmentBase bufferSegment)
        {
            this._subscriptionIdentity = this._parentChannel.GetSubscriptionIdentity(bufferSegment);
        }
    }

    //PXM
    internal sealed class PXMMassQuoteDataCracker : IFixMessageCracker
    {
        private IMarketDataPerentChannel _parentChannel;
        private IMassQuoteAcknowledger _quoteAcker;
        private ILogger _logger;
        private DateTime _currentMessageReceived;
        private QuoteObject _sharedQuote;

        private AtomicDecimal[][] _lastPrices = new AtomicDecimal[2][]
        {new AtomicDecimal[Common.Symbol.ValuesCount], new AtomicDecimal[Common.Symbol.ValuesCount]};

        private decimal[][] _lastSizes = new decimal[2][]
        {new decimal[Common.Symbol.ValuesCount], new decimal[Common.Symbol.ValuesCount]};

        public PXMMassQuoteDataCracker(IMarketDataPerentChannel parentChannel, IMassQuoteAcknowledger quoteAcker, ILogger logger)
        {
            this._parentChannel = parentChannel;
            this._quoteAcker = quoteAcker;
            this._logger = logger;
            this._sharedQuote = parentChannel.PriceReceivingHelper.SharedQuote;
        }

        private const byte _recognizedType = (byte)'i';

        public byte RecognizedMessageType
        {
            get { return _recognizedType; }
        }

        public bool TryRecoverFromParsingErrors
        {
            get { return false; }
        }

        public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
        {
            switch (tagId)
            {
                //case 296:
                //    OnTag296(tagValueSegment);
                //    break;
                case 295:
                    OnTag295(tagValueSegment);
                    break;
                case 188:
                    OnTag188(tagValueSegment);
                    break;
                case 190:
                    OnTag190(tagValueSegment);
                    break;
                case 134:
                    OnTag134(tagValueSegment);
                    break;
                case 135:
                    OnTag135(tagValueSegment);
                    break;
                case 52:
                    OnTag52(tagValueSegment);
                    break;
                case 299:
                    OnTag299(tagValueSegment);
                    break;
                case 302:
                    OnTag302(tagValueSegment);
                    break;
                case 117:
                    OnTag117(tagValueSegment);
                    break;
            }
        }

        private Common.Symbol _symbol = Common.Symbol.NULL;
        private string _subscriptionIdentity = string.Empty;
        private AtomicDecimal _bidPrice = 0;
        private AtomicDecimal _askPrice = 0;
        private decimal _bidSize = 0;
        private decimal _askSize = 0;
        private DateTime _sendingTime = DateTime.MinValue;
        private readonly byte[] _counterpartyIdentity = new byte[PriceObject.MAX_CTP_IDENTITY_LENGTH];
        private string _quoteId;

        public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
        {
            this._currentMessageReceived = currentMessageReceivedTimeUtc;
            this._sendingTime = DateTime.MinValue;
            this._quoteId = null;
            this.ResetTier();
        }

        private void ResetTier()
        {
            this._symbol = Common.Symbol.NULL;
            this._subscriptionIdentity = null;
            this._bidPrice = AtomicDecimal.Zero;
            this._askPrice = AtomicDecimal.Zero;
            this._bidSize = 0;
            this._askSize = 0;
            this._counterpartyIdentity[0] = 0;
        }

        private void ParseSingleQuote()
        {
            if (this._symbol == Common.Symbol.NULL || this._subscriptionIdentity == null)
                throw new HaltingTagException("Incomplete message - missing tag", 302);

            DateTime receivedAndParsed = HighResolutionDateTime.UtcNow; //this._currentMessageReceived;

            bool askChange = false, bidChange = false;

            if (this._bidSize < 0)
                this._parentChannel.SendLastPriceCancel(this._symbol, this._parentChannel.Counterparty, PriceSide.Bid);
            else
                bidChange = this._bidSize > 0 || this._bidPrice > AtomicDecimal.Zero;

            if (this._askSize < 0)
                this._parentChannel.SendLastPriceCancel(this._symbol, this._parentChannel.Counterparty, PriceSide.Ask);
            else
                askChange = this._askSize > 0 || this._askPrice > AtomicDecimal.Zero;

            if (bidChange)
            {
                if (this._bidSize == 0)
                    this._bidSize = this._lastSizes[(int)PriceSide.Bid][(int)this._symbol];
                else
                    this._lastSizes[(int)PriceSide.Bid][(int)this._symbol] = this._bidSize;

                if (this._bidPrice == AtomicDecimal.Zero)
                    this._bidPrice = this._lastPrices[(int)PriceSide.Bid][(int)this._symbol];
                else
                    this._lastPrices[(int)PriceSide.Bid][(int)this._symbol] = this._bidPrice;
            }

            if (askChange)
            {
                if (this._askSize == 0)
                    this._askSize = this._lastSizes[(int)PriceSide.Ask][(int)this._symbol];
                else
                    this._lastSizes[(int)PriceSide.Ask][(int)this._symbol] = this._askSize;

                if (this._askPrice == AtomicDecimal.Zero)
                    this._askPrice = this._lastPrices[(int)PriceSide.Ask][(int)this._symbol];
                else
                    this._lastPrices[(int)PriceSide.Ask][(int)this._symbol] = this._askPrice;
            }


            if (bidChange && askChange)
            {
                //if anything is zero - higher layers will cancel
                _sharedQuote.OverrideContent(
                    askPrice: this._askPrice,
                    askSize: this._askSize,
                    bidPrice: this._bidPrice,
                    bidSize: this._bidSize,
                    symbol: this._symbol,
                    counterparty: this._parentChannel.Counterparty,
                    subscriptionIdentity: this._subscriptionIdentity,
                    counterpartyTimeUtc: this._sendingTime,
                    marketDataRecordType: MarketDataRecordType.BankQuoteData,
                    intagratorTimeUtc: receivedAndParsed,
                    //override content will copy those buffers to it's internal buffer, so we can reuse
                    counterpartyAskIdentity: this._counterpartyIdentity,
                    counterpartyBidIdentity: this._counterpartyIdentity);

                this._parentChannel.SendNewQuote(_sharedQuote);
            }
            else if (askChange)
            {
                PriceObjectInternal askPrice = this._parentChannel.PriceReceivingHelper.GetPriceObject(
                    priceConverted: this._askPrice,
                    sizeBaseAbsInitial: this._askSize,
                    side: PriceSide.Ask,
                    symbol: this._symbol,
                    counterparty: this._parentChannel.Counterparty,
                    counterpartyIdentity: this._counterpartyIdentity,
                    marketDataRecordType: MarketDataRecordType.BankQuoteData,
                    integratorReceivedTime: receivedAndParsed,
                    counterpartySentTime: this._sendingTime);

                this._parentChannel.SendNewPrice(askPrice, this._subscriptionIdentity);
            }
            //ask not set - just one sided quote
            else if (bidChange)
            {
                PriceObjectInternal bidPrice = this._parentChannel.PriceReceivingHelper.GetPriceObject(
                    priceConverted: this._bidPrice,
                    sizeBaseAbsInitial: this._bidSize,
                    side: PriceSide.Bid,
                    symbol: this._symbol,
                    counterparty: this._parentChannel.Counterparty,
                    counterpartyIdentity: this._counterpartyIdentity,
                    marketDataRecordType: MarketDataRecordType.BankQuoteData,
                    integratorReceivedTime: receivedAndParsed,
                    counterpartySentTime: this._sendingTime);

                this._parentChannel.SendNewPrice(bidPrice, this._subscriptionIdentity);
            }
            //if any change than cancel was already sent
        }

        public void MessageParsingDone()
        {
            this.ParseSingleQuote();

            if (_quoteId != null)
            {
                this._quoteAcker.AckMassQuote(_quoteId);
            }
        }


        ////NoQuoteSets = 296
        //private void OnTag296(BufferSegmentBase bufferSegment)
        //{
        //    if (bufferSegment.ToInt() != 1)
        //    {
        //        string error = string.Format("Received massquote with unexpected number of quote sets: {0}",
        //            bufferSegment.ToInt());
        //        this._logger.Log(LogLevel.Error, error);
        //        throw new HaltingTagException(error, 296);
        //    }
        //}

        //NoQuoteEntries = 295
        private void OnTag295(BufferSegmentBase bufferSegment)
        {
            if (bufferSegment.ToInt() != 1)
            {
                string error = string.Format("Received massquote with unexpected number of quote entries: {0}",
                    bufferSegment.ToInt());
                this._logger.Log(LogLevel.Error, error);
                throw new HaltingTagException(error, 295);
            }
        }

        //BidSpotRate = 188
        private void OnTag188(BufferSegmentBase bufferSegment)
        {
            this._bidPrice = bufferSegment.ToAtomicDecimal();
        }

        //OfferSpotRate = 190
        private void OnTag190(BufferSegmentBase bufferSegment)
        {
            this._askPrice = bufferSegment.ToAtomicDecimal();
        }

        //BidSize = 134
        private void OnTag134(BufferSegmentBase bufferSegment)
        {
            this._bidSize = bufferSegment.ToDecimal();
            //not present size means there is no change. But we encode not present as 0
            // so lets convert explicit 0 to -1
            if (this._bidSize == 0)
                this._bidSize = -1;
        }

        //OfferSize = 135
        private void OnTag135(BufferSegmentBase bufferSegment)
        {
            this._askSize = bufferSegment.ToDecimal();
            if (this._askSize == 0)
                this._askSize = -1;
        }

        //QuoteEntryID = 299
        private void OnTag299(BufferSegmentBase bufferSegment)
        {
            bufferSegment.CopyToClearingRest(this._counterpartyIdentity);
        }

        private void OnTag52(BufferSegmentBase bufferSegment)
        {
            this._sendingTime = bufferSegment.ToDateTime();
        }

        //QuoteSetID = 302
        private void OnTag302(BufferSegmentBase bufferSegment)
        {
            if (this._subscriptionIdentity != null)
            {
                //this is begining of next quote in this single message
                this.ParseSingleQuote();
                this.ResetTier();
            }

            this._subscriptionIdentity = this._parentChannel.GetSubscriptionIdentity(bufferSegment);

            if (this._symbol == Common.Symbol.NULL)
                this._symbol = (Common.Symbol)bufferSegment.ToInt();
            else if (this._symbol != bufferSegment.ToSymbol())
                throw new HaltingTagException("Receiving MassQuote update for multiple symbols in one message", 302);
        }

        //QuoteId = 117
        private void OnTag117(BufferSegmentBase bufferSegment)
        {
            this._quoteId = bufferSegment.GetContentAsAsciiString();
        }
    }



    //HSB, BOA, CTI, CRS, GLS, NOM
    internal class QuoteCancelCracker : IFixMessageCracker
    {
        private IMarketDataPerentChannel _parentChannel;
        private ILogger _logger;
        private DateTime _currentMessageReceived;
        private QuoteObject _sharedQuote;

        public QuoteCancelCracker(IMarketDataPerentChannel parentChannel, ILogger logger)
        {
            this._parentChannel = parentChannel;
            this._logger = logger;
            this._sharedQuote = parentChannel.PriceReceivingHelper.SharedQuote;
        }

        private const byte _recognizedType = (byte)'Z';

        public byte RecognizedMessageType
        {
            get { return _recognizedType; }
        }

        public bool TryRecoverFromParsingErrors
        {
            get { return false; }
        }

        public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
        {
            switch (tagId)
            {
                case 117:
                    OnTag117(tagValueSegment);
                    break;
                case 131:
                    OnTag131(tagValueSegment);
                    break;
            }
        }

        private string _subscriptionIdentity = string.Empty;
        private QuoteIdValue _quoteIdValue = QuoteIdValue.Unpopulated;
        private SubscriptionChangeInfo _sharedSubscriptionChangeInfo = new SubscriptionChangeInfo(null, SubscriptionStatus.Rejected);

        internal enum SubscriptionCancelType
        {
            Unknown,
            LastQuote,
            WholeSubscription
        }

        internal enum QuoteIdValue
        {
            Unpopulated,
            Star,
            All,
            Unsubscribe,
            Other
        }

        public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
        {
            this._subscriptionIdentity = null;
            _quoteIdValue = QuoteIdValue.Unpopulated;
        }

        public void MessageParsingDone()
        {
            if (this._subscriptionIdentity == null)
                this._subscriptionIdentity = "*";
                //throw new HaltingTagException("Incomplete message - missing tag", 131);

            DateTime receivedAndParsed = this._currentMessageReceived;


            switch (this.GetSubscriptionCancelType(this._quoteIdValue))
            {
                case SubscriptionCancelType.LastQuote:
                    this._parentChannel.SendLastQuoteCancel(this._subscriptionIdentity);
                    break;
                case SubscriptionCancelType.WholeSubscription:
                    _sharedSubscriptionChangeInfo.OverrideContent(this._subscriptionIdentity, SubscriptionStatus.Rejected);
                    this._parentChannel.SendSubscriptionChange(_sharedSubscriptionChangeInfo);
                    break;
                //nothing
                case SubscriptionCancelType.Unknown:
                default:
                    break;
            }
        }

        internal virtual SubscriptionCancelType GetSubscriptionCancelType(QuoteIdValue quoteIdValue)
        {
            switch (quoteIdValue)
            {
                case QuoteIdValue.Star:
                    case QuoteIdValue.Unsubscribe:
                    return SubscriptionCancelType.WholeSubscription;
                case QuoteIdValue.All:
                    return SubscriptionCancelType.LastQuote;
                case QuoteIdValue.Unpopulated:
                case QuoteIdValue.Other:
                default:
                    return SubscriptionCancelType.LastQuote;
            }
        }

        //QuoteId = 117
        private void OnTag117(BufferSegmentBase bufferSegment)
        {
            if (bufferSegment.ContentSize > 0 && bufferSegment[0] == '*')
            {
                this._quoteIdValue = QuoteIdValue.Star;
            }
            else if (bufferSegment.ContentSize > 2 && bufferSegment[0] == 'A' && bufferSegment[1] == 'L' && bufferSegment[2] == 'L')
            {
                this._quoteIdValue = QuoteIdValue.All;
            }
            else if ((bufferSegment.ContentSize > 11 && bufferSegment[0] == 'U' && bufferSegment[1] == 'N' &&
                      bufferSegment[2] == 'S' && bufferSegment[3] == 'U' && bufferSegment[4] == 'B' &&
                      bufferSegment[5] == 'S'))
            {
                this._quoteIdValue = QuoteIdValue.Unsubscribe;
            }
            else if (bufferSegment.ContentSize > 0)
            {
                this._quoteIdValue = QuoteIdValue.Other;
            }
            else
            {
                this._quoteIdValue = QuoteIdValue.Unpopulated;
            }
        }

        //QuoteRequestId = 131
        private void OnTag131(BufferSegmentBase bufferSegment)
        {
            this._subscriptionIdentity = this._parentChannel.GetSubscriptionIdentity(bufferSegment);
        }
    }

    internal sealed class NomQuoteCancelCracker : QuoteCancelCracker
    {
        public NomQuoteCancelCracker(IMarketDataPerentChannel parentChannel, ILogger logger) : base(parentChannel, logger)
        {
        }

        internal override SubscriptionCancelType GetSubscriptionCancelType(QuoteIdValue quoteIdValue)
        {
            switch (quoteIdValue)
            {
                case QuoteIdValue.Unpopulated:
                    return SubscriptionCancelType.WholeSubscription;
                case QuoteIdValue.Star:
                case QuoteIdValue.All:
                case QuoteIdValue.Unsubscribe:
                case QuoteIdValue.Other:
                default:
                    return SubscriptionCancelType.LastQuote;
            }
        }
    }

    internal sealed class GlsQuoteCancelCracker : QuoteCancelCracker
    {
        public GlsQuoteCancelCracker(IMarketDataPerentChannel parentChannel, ILogger logger) : base(parentChannel, logger)
        {
        }

        internal override SubscriptionCancelType GetSubscriptionCancelType(QuoteIdValue quoteIdValue)
        {
            switch (quoteIdValue)
            {
                case QuoteIdValue.Unpopulated:
                case QuoteIdValue.Star:
                case QuoteIdValue.All:
                case QuoteIdValue.Unsubscribe:
                case QuoteIdValue.Other:
                default:
                    return SubscriptionCancelType.WholeSubscription;
            }
        }
    }

}
