﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX42;
using Price = QuickFix.Fields.Price;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal class FIXChannel_MGS : FIXChannelMessagingBase<FIXChannel_MGS>
    {
        public FIXChannel_MGS(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            FIXChannel_MGSSettings settings, bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new MGSBankMarketDataSnapshotFullRefreshCracker(this, logger));
            }
            else
                this.SetQuickMessageParsers(isMdChannel, new MGSExecReportsCrckers(this, logger));
        }

        private FIXChannel_MGSSettings _settings;

        protected override void OnOutgoingAdminMessage(QuickFix.Message message, SessionID sessionId)
        {
            message.SetField(new OnBehalfOfCompID(this._settings.OnBehalfOfCompId));
        }

        protected override void OnOutgoingMessage(QuickFix.Message message, SessionID sessionId)
        {
            message.SetField(new OnBehalfOfCompID(this._settings.OnBehalfOfCompId));
        }

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, true),
                                        this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, false),
                                        this.SessionId);
        }

        private MarketDataRequest CreateSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity,
                                                                           bool subscribe)
        {
            var message = new MarketDataRequest(new MDReqID(subscriptionIdentity),
                                                               new SubscriptionRequestType(subscribe
                                                                                               ? SubscriptionRequestType
                                                                                                     .SNAPSHOT_PLUS_UPDATES
                                                                                               : SubscriptionRequestType
                                                                                                     .DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST),
                                                               new MarketDepth(0));

            message.MDUpdateType = new MDUpdateType(MDUpdateType.FULL_REFRESH);

            // bid:
            var marketDataEntryGroupBid = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupBid.Set(new MDEntryType(MDEntryType.BID));
            message.AddGroup(marketDataEntryGroupBid);

            // offer:
            var marketDataEntryGroupOffer = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupOffer.Set(new MDEntryType(MDEntryType.OFFER));
            message.AddGroup(marketDataEntryGroupOffer);

            // symbol group:
            var symbolGroup = new MarketDataRequest.NoRelatedSymGroup();
            symbolGroup.Symbol = subscriptionInfo.Symbol.ToQuickFixSymbol();
            symbolGroup.SecurityType = new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT);
            symbolGroup.SetField(subscriptionInfo.Symbol.ToQuickBaseCurrency());
            symbolGroup.SetField(new SettlType(SettlType.REGULAR));
            symbolGroup.SetField(new MDEntrySize(subscriptionInfo.Quantity));

            message.AddGroup(symbolGroup);

            return message;
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            var orderMessage = new QuickFix.FIX44.NewOrderSingle(
                new ClOrdID(identity),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                new OrdType(OrdType.FOREX_PREVIOUSLY_QUOTED));

            orderMessage.Account = new Account(this._settings.Account);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            //orderMessage.SetField(new QuoteReqID(orderInfo.PriceObject.SubscriptionIdentity));
            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);
            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Price = new Price(orderInfo.RequestedPrice);
            orderMessage.SecurityType = new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT);

            return orderMessage;
        }
        #endregion /FIX message composing

        #region FIX message cracking

        public void OnMessage(MarketDataRequestReject m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received market data request reject: {0}", QuickFixUtils.FormatMessage(m));
            this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(),
                                                                       SubscriptionStatus.Rejected));
        }

        public void OnMessage(MarketDataSnapshotFullRefresh m, SessionID s)
        {
            QuoteObject quote = this.CreateQuoteFromMessage(m);

            if (quote == null)
            {
                this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(), SubscriptionStatus.Broken));
                return;
            }

            if (object.ReferenceEquals(quote, QuoteObject.EMPTY_QUOTE))
            {
                //if (!this.Logger.IsLowDiskSpaceConstraintLoggingOn) this.Logger.Log(LogLevel.Info, "Received indicative quote (ignoring it and cancelling the previous): {0}", QuickFixUtils.FormatMessage(m));
                this.SendLastQuoteCancel(m.MDReqID.getValue());
                return;
            }

            SendNewQuote(quote);
        }

        private const int MaxNumOfMDEntries = 2;

        private QuoteObject CreateQuoteFromMessage(MarketDataSnapshotFullRefresh quoteMessage)
        {
            Kreslik.Integrator.Common.Symbol symbol = (Kreslik.Integrator.Common.Symbol)quoteMessage.Symbol.getValue();
            string subscriptionIdentity = quoteMessage.MDReqID.getValue();

            int mdEntriesNum = quoteMessage.GetInt(Tags.NoMDEntries);

            if (mdEntriesNum > MaxNumOfMDEntries)
            {
                this.Logger.Log(LogLevel.Error, "Received quote with unexpected number of layers: {0}",
                                mdEntriesNum);
                return null;
            }

            decimal bidPrice = 0;
            string bidIdentity = null;
            decimal bidSize = 0;
            decimal askPrice = 0;
            string askIdentity = null;
            decimal askSize = 0;

            for (int groupIndex = 1; groupIndex <= mdEntriesNum; groupIndex += 1)
            {
                Group noMDEntriesGroup = quoteMessage.GetGroup(groupIndex, Tags.NoMDEntries);
                if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.BID)
                {
                    bidPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    bidIdentity = noMDEntriesGroup.GetField(Tags.QuoteEntryID);
                    if (this.GetIsIndicative(noMDEntriesGroup))
                    {
                        bidSize = 0;
                    }
                    else
                    {
                        bidSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                    }
                }
                else if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.OFFER)
                {
                    askPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    askIdentity = noMDEntriesGroup.GetField(Tags.QuoteEntryID);
                    if (this.GetIsIndicative(noMDEntriesGroup))
                    {
                        askSize = 0;
                    }
                    else
                    {
                        askSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                    }
                }
            }

            //Zero sizes are documented way of sending indicative price
            if (bidSize == 0 && askSize == 0)
                return QuoteObject.EMPTY_QUOTE;

            PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSidedIdentity(askPrice: askPrice, askSize: askSize, bidPrice: bidPrice,
                bidSize: bidSize,
                symbol: symbol, counterparty: this.Counterparty,
                subscriptionIdentity: subscriptionIdentity,
                counterpartyTimeUtc: quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                askIdentity: askIdentity,
                bidIdentity: bidIdentity,
                marketDataRecordType: MarketDataRecordType.BankQuoteData);

            return PriceReceivingHelper.SharedQuote;
        }
        
        private bool GetIsIndicative(Group noMDEntriesGroup)
        {
            int quoteCondition = noMDEntriesGroup.GetInt(Tags.MDQuoteType);
            bool indicative = true;

            switch (quoteCondition)
            {
                case MDQuoteType.TRADEABLE:
                    indicative = false;
                    break;
                case MDQuoteType.INDICATIVE:
                    if (!this.Logger.IsLowDiskSpaceConstraintLoggingOn) this.Logger.Log(LogLevel.Info, "Received indicative side of quote, ignoring the side");
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Received quote side in unsupported condition: {0}. Ignoring side", quoteCondition);
                    break;
            }

            return indicative;
        }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            if (!m.IsSetClOrdID())
            {
                this.Logger.Log(LogLevel.Fatal,
                                 "Received execution report WITHOUT Client Order ID (and so we cannot match it to our order): {0}",
                                 QuickFixUtils.FormatMessage(m));
                return;
            }

            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            string orderId = m.ClOrdID.getValue();

            switch (m.OrdStatus.getValue())
            {
                case OrdStatus.NEW:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] was received and enqeued by other end", orderId);
                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;
                    break;
                case OrdStatus.FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Filled;
                    executionInfo = new ExecutionInfo(m.ToNullableDecimal(Tags.LastQty),
                                                                    m.ToNullableDecimalCoalesce(Tags.LastPx, Tags.Price),
                                                                    m.ToNullableString(Tags.ExecID),
                                                                    m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                                                                    m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                                                                    m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.PARTIALLY_FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.PartiallyFilled;
                    executionInfo = new ExecutionInfo(
                        //We intentiaonally don't convert to nullable 
                        //  - as quantity is absolutely required with partial fills
                        m.GetDecimal(Tags.LastQty),
                        m.ToNullableDecimal(Tags.Price),
                        m.ToNullableString(Tags.ExecID),
                        m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                        m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                        m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.CANCELED:
                case OrdStatus.REJECTED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), null, RejectionType.OrderReject);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Order [{0}] was put to unsupported status: [{1}]", orderId, m.OrdStatus.getValue());
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     executionInfo, rejectionInfo));
        }

        #endregion /FIX message cracking

        private sealed class MGSExecReportsCrckers : BankExecReportCracker
        {
            public MGSExecReportsCrckers(IOrdersParentChannel parentChannel, ILogger logger)
                : base(parentChannel, logger)
            { }

            protected override OrderStatus GetTranslatedOrderStatus(char externalOrderStatus)
            {
                switch (externalOrderStatus)
                {
                    case OrdStatus.NEW:
                        return OrderStatus.New;
                    case OrdStatus.FILLED:
                        return OrderStatus.Trade;
                    case OrdStatus.PARTIALLY_FILLED:
                        return OrderStatus.PartiallTrade;
                    case OrdStatus.CANCELED:
                    case OrdStatus.REJECTED:
                        return OrderStatus.Reject;
                    default:
                        return OrderStatus.Unrecognized;
                }
            }
        }
    }
}
