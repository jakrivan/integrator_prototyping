﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using QuickFix;
using QuickFix.FIX44;
using QuickFix.Fields;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    public class FIXSTPChannel_Traiana : FIXChannelBase<FIXSTPChannel_Traiana>, IFIXSTPChannel
    {
        public FIXSTPChannel_Traiana(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
                                 UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings, STPCounterparty stpCounterparty)
            : base(persistedFixConfigsReader, logger, unexpectedMessagesTreatingSettings)
        {
            this._stpCounterparty = stpCounterparty;
            this.ConfigureStateMachineBasicTransitions();

            this.OnStarted += () =>
            {
                if (TradeCaptureSubscriptionChanged != null)
                {
                    TradeCaptureSubscriptionChanged(new TradeCaptureSubscriptionChangeArgs(true));
                }
            };
        }

        private STPCounterparty _stpCounterparty;

        public override Counterparty Counterparty
        {
            get
            {
                this.Logger.Log(LogLevel.Error, "Counterparty property getter unexpectedly called: {0}",
                                new StackTrace(true));
                return Counterparty.CTI;
            }
            protected set
            {
                this.Logger.Log(LogLevel.Error, "Counterparty property setter unexpectedly called: {0}",
                                new StackTrace(true));
            }
        }

        public bool TryCreateTradeCaptureReport(IExecutionInfo executionInfo, IIntegratorOrderInfo integratorOrderInfo,
            bool isDealCancellation, out QuickFix.Message stpTicket)
        {
            try
            {
                return this.CreateTradeCaptureReportInternal(executionInfo, integratorOrderInfo, isDealCancellation, out stpTicket);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, "Unexpected exception during creating STP ticket", e);
                stpTicket = null;
                return false;
            }
        }

        private bool CreateTradeCaptureReportInternal(IExecutionInfo executionInfo,
            IIntegratorOrderInfo integratorOrderInfo, bool isDealCancellation, out QuickFix.Message stpTicket)
        {
            stpTicket = null;

            if (executionInfo == null || !executionInfo.IsExecutionInfoPopulated)
            {
                this.Logger.Log(LogLevel.Error,
                    "Requested to send STP (for order [{0}]), but required information is null or not populated",
                    integratorOrderInfo.Identity);
                return false;
            }

            decimal filledAmountBaseAbs = executionInfo.FilledAmountBaseAbs;

            //TODO: for rejected deal FilledAmountBaseAbs is 0 - so we would need rejected amount. Or test if 0 is ok 
            if (isDealCancellation)
                filledAmountBaseAbs = integratorOrderInfo.SizeBaseAbsInitial;

            decimal usedPrice = executionInfo.UsedPrice;

            TradeCaptureReport stp = new TradeCaptureReport();
            try
            {
                stp.TradeReportID = new TradeReportID(executionInfo.IntegratorTransactionId);
                if (isDealCancellation)
                    //5049 (Deal Link Reference) 
                    stp.SetField(new StringField(5049, executionInfo.IntegratorTransactionId));
                stp.ExecID = new ExecID(executionInfo.ExecutionId);
                stp.PreviouslyReported = new PreviouslyReported(PreviouslyReported.NO);
                stp.OrdStatus = new OrdStatus(isDealCancellation ? OrdStatus.CANCELED : OrdStatus.NEW);
                stp.Symbol = integratorOrderInfo.Symbol.ToQuickFixSymbol();
                //stp.SecurityType = new SecurityType(SecurityType.FX_SPOT);

                //stp NoSides
                TradeCaptureReport.NoSidesGroup noSidesGrp = new TradeCaptureReport.NoSidesGroup();
                noSidesGrp.Side = integratorOrderInfo.IntegratorDealDirection.ToQuickFixSide();
                noSidesGrp.ClOrdID = new ClOrdID(integratorOrderInfo.Identity);
                //Or should this be tag 37 - OrderId from the ExecReport?
                //No - this is 'Secondary ID' - not used by traiana at all
                noSidesGrp.OrderID = new OrderID(executionInfo.IntegratorTransactionId);

                TradeCaptureReport.NoSidesGroup.NoPartyIDsGroup partyIDsGrp_Counterparty =
                    new TradeCaptureReport.NoSidesGroup.NoPartyIDsGroup();
                partyIDsGrp_Counterparty.PartyID =
                    new PartyID(integratorOrderInfo.Counterparty.GetTradingTargetStringCode());
                partyIDsGrp_Counterparty.PartyIDSource = new PartyIDSource(PartyIDSource.PROPRIETARY);
                partyIDsGrp_Counterparty.PartyRole = new PartyRole(PartyRole.CONTRA_FIRM);
                noSidesGrp.AddGroup(partyIDsGrp_Counterparty);

                TradeCaptureReport.NoSidesGroup.NoPartyIDsGroup partyIDsGrp_Broker =
                    new TradeCaptureReport.NoSidesGroup.NoPartyIDsGroup();
                partyIDsGrp_Broker.PartyID = new PartyID("Citi");
                partyIDsGrp_Broker.PartyIDSource = new PartyIDSource(PartyIDSource.PROPRIETARY);
                partyIDsGrp_Broker.PartyRole = new PartyRole(PartyRole.SPONSORING_FIRM);
                noSidesGrp.AddGroup(partyIDsGrp_Broker);

                //TODO: account
                //noSidesGrp.Account = new Account(this._settings.Account);
                noSidesGrp.Currency = integratorOrderInfo.Symbol.BaseCurrency.ToQuickFixCurrency();
                //noSidesGrp.SettlCurrAmt =
                //    new SettlCurrAmt(filledAmountBaseAbs * usedPrice);
                noSidesGrp.SettlCurrency =
                    new SettlCurrency(integratorOrderInfo.Symbol.TermCurrency.ToString());
                stp.AddGroup(noSidesGrp);


                stp.TradeDate = new TradeDate(DateTime.UtcNow.ToString("yyyyMMdd"));
                DateTime settlementDate = executionInfo.SettlementDateLocal;
                stp.SettlDate = new SettlDate(settlementDate.ToString("yyyyMMdd"));
                stp.LastQty = new LastQty(filledAmountBaseAbs);
                stp.LastPx = new LastPx(usedPrice);
                stp.LastSpotRate = new LastSpotRate(usedPrice);
                stp.TransactTime = new TransactTime(executionInfo.TransactionTime, true);
                //stp.SetField(new ExerciseStyle(ExerciseStyle.EUROPEAN));
                stpTicket = stp;
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, e, "Exception during STP FIX message building");
                return false;
            }

            return true;
        }

        public bool TrySendMessage(QuickFix.Message stp)
        {
            try
            {
                return Session.SendToTarget(stp, this.SessionId);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Error, e, "Unexpected exception during STP sending");
                return false;
            }
        }

        public event Action<TradeCaptureSubscriptionChangeArgs> TradeCaptureSubscriptionChanged;
        public event Action<TradeReportTicketInfo> NewTradeReportTicketInfo;
        public event Action<AggregatedTradeTicketInfo> NewAggregatedTradeTicketInfo;

        public TradeReportTicketInfoType InitialTradeReportTicketInfoTypeOfOutgoingTicket { get{return TradeReportTicketInfoType.SentToTraiana;}}

        public void OnMessage(TradeCaptureReportAck m, SessionID s)
        {
            DateTime integratorReceivedTime = HighResolutionDateTime.UtcNow;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);
            string integratorDealId = m.TradeReportRefID.getValue();
            TradeReportTicketInfoType tradeReportTicketInfoType;

            if (m.IsSetTradeReportRejectReason())
            {
                this.Logger.Log(LogLevel.Error, "STP consumer sent report ACK with reject reason: {0}, ack message: {1}",
                                m.TradeReportRejectReason.getValue(), m.ToString());
            }

            tradeReportTicketInfoType = this.ExtractTradeCaptureReportAckStatus(m);

            if (this.NewTradeReportTicketInfo != null)
            {
                this.NewTradeReportTicketInfo(new TradeReportTicketInfo(integratorDealId, integratorReceivedTime,
                                                                        counterpartySendTime, tradeReportTicketInfoType,
                                                                        this._stpCounterparty,
                                                                        m.ToString()));
            }
        }

        private TradeReportTicketInfoType ExtractTradeCaptureReportAckStatus(TradeCaptureReportAck m)
        {
            if (m.IsSetTrdRptStatus())
            {
                int reportStatus = m.TrdRptStatus.getValue();

                if (reportStatus == TrdRptStatus.REJECTED)
                {
                    this.Logger.Log(LogLevel.Fatal, "STP consumer sent report ACK with REJECTED status{0}: {1}",
                                        m.IsSetTradeReportRejectReason()
                                            ? " (" + m.TradeReportRejectReason.getValue() + ")"
                                            : string.Empty, m.ToString());
                    return TradeReportTicketInfoType.RejectedByTraiana;
                }
            }
            else
            {
                this.Logger.Log(LogLevel.Fatal, "STP consumer sent report ACK without TrdRptStatus(939) tag which is required: {0}", m.ToString());
                return TradeReportTicketInfoType.Unexpected;
            }

            if (m.IsSetText())
            {
                string textField = m.Text.getValue();

                switch (textField)
                {
                    case "Draft":
                        return TradeReportTicketInfoType.AcceptedByTraiana;
                    case "Active":
                        return TradeReportTicketInfoType.AcceptedByPB;
                    case "Done":
                        return TradeReportTicketInfoType.BookedByPB;
                    case "Error":
                        return TradeReportTicketInfoType.RejectedByPB;
                    case "Deleted":
                        return TradeReportTicketInfoType.DeletedByTraiana;
                    //"Invalid"
                    //"PB Deleted"
                    //"Replaced"
                    //"Request_Modify"
                    //etc.
                    default:
                        this.Logger.Log(LogLevel.Fatal, "STP consumer sent report ACK with unexpected status: {0}, ticket: {1}", textField, m.ToString());
                        return TradeReportTicketInfoType.Unexpected;
                }
            }
            else
            {
                this.Logger.Log(LogLevel.Fatal, "STP consumer sent report ACK without Text(58) tag which is required: {0}", m.ToString());
                return TradeReportTicketInfoType.Unexpected;
            }
        }

        //Aggregates
        public void OnMessage(TradeCaptureReport m, SessionID s)
        {
            DateTime integratorReceivedTime = HighResolutionDateTime.UtcNow;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);

            //9579 - NoNettedTrades
            int noNettedTrades = m.GetInt(9579);

            if (noNettedTrades <= 0)
            {
                this.Logger.Log(LogLevel.Fatal, "Received TradeCaptureReport with unexpected number of trades: {0}. msg: {1}",
                                noNettedTrades, m.ToString());
            }

            List<string> orderIds = new List<string>();

            for (int groupIndex = 1; groupIndex <= noNettedTrades; groupIndex += 1)
            {
                Group noNettedTradesGroup = m.GetGroup(groupIndex, 9579);

                if (noNettedTradesGroup.IsSetField(9581))
                {
                    orderIds.Add(noNettedTradesGroup.GetString(9581));
                }
                else
                {
                    this.Logger.Log(LogLevel.Fatal, "Received TradeCaptureReport without OrderId(9581) tag set msg: {0}",
                                m.ToString());
                }
            }

            AggregatedTradeTicketInfo aggregatedTradeTicketInfo =
                new AggregatedTradeTicketInfo(m.TradeReportID.getValue(), integratorReceivedTime, counterpartySendTime,
                                              m.LastPx.getValue(), m.LastQty.getValue(), orderIds, this._stpCounterparty,
                                              m.ToString());

            if (this.NewAggregatedTradeTicketInfo != null)
            {
                this.NewAggregatedTradeTicketInfo(aggregatedTradeTicketInfo);
            }
        }
    }
}
