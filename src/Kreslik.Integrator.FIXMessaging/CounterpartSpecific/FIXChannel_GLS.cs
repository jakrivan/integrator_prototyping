﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using QuickFix;
using QuickFix.FIX44;
using QuickFix.Fields;
using Price = QuickFix.Fields.Price;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal sealed class FIXChannel_GLS : FIXChannelMessagingBase<FIXChannel_GLS>
    {
        public FIXChannel_GLS(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new BankQuoteDataCracker(this, logger));
            }
            else
                this.SetQuickMessageParsers(isMdChannel, new SimpleBankWithNewStatusExecReportsCracker(this, logger));
        }

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            var qr = new QuoteRequest(new QuoteReqID(subscriptionIdentity));
            var symbolGroup = new QuoteRequest.NoRelatedSymGroup();
            symbolGroup.Symbol = subscriptionInfo.Symbol.ToQuickFixSymbol();
            symbolGroup.OrderQty = new OrderQty(subscriptionInfo.Quantity);
            symbolGroup.Currency = subscriptionInfo.Symbol.ToQuickBaseCurrency();
            qr.AddGroup(symbolGroup);
            return Session.SendToTarget(qr, this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            QuoteCancel qc = new QuoteCancel(new QuoteID("*"),
                                                 new QuoteCancelType(QuoteCancelType.CANCEL_FOR_SYMBOL));
            qc.QuoteReqID = new QuoteReqID(subscriptionIdentity);

            var quoteEntriesGroup = new QuoteCancel.NoQuoteEntriesGroup();
            quoteEntriesGroup.Symbol = subscriptionInfo.Symbol.ToQuickFixSymbol();
            qc.AddGroup(quoteEntriesGroup);

            return Session.SendToTarget(qc, this.SessionId);
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            var orderMessage = new NewOrderSingle(
                new ClOrdID(identity),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                new OrdType(OrdType.FOREX_PREVIOUSLY_QUOTED));

            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.HandlInst = new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION);
            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);
            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Price = new Price(orderInfo.RequestedPrice);

            return orderMessage;
        }
        #endregion /FIX message composing


        #region FIX message cracking

        public void OnMessage(MassQuoteAcknowledgement m, SessionID s)
        {
            if (!m.IsSetQuoteReqID())
            {
                this.Logger.Log(LogLevel.Error,
                                "Receiving (and ignoring) QuoteAck with unspecified subscription id: {0}",
                                QuickFixUtils.FormatMessage(m));
                return;
            }

            string subscriptionId = m.QuoteReqID.getValue();
            SubscriptionStatus subscriptionStatus;

            switch (m.GetInt(Tags.QuoteAckStatus))
            {
                case 0:
                    subscriptionStatus = SubscriptionStatus.Subscribed_ValidationPending;
                    break;
                case 5:
                    this.Logger.Log(LogLevel.Warn,
                                    "Receiving QuoteAck rejecting subscription [{0}]: {1}", subscriptionId,
                                    QuickFixUtils.FormatMessage(m));
                    subscriptionStatus = SubscriptionStatus.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error,
                                "Receiving (and ignoring) QuoteAck with unexpected AckStatus: {0}",
                                QuickFixUtils.FormatMessage(m));
                    return;
            }

            this.SendSubscriptionChange(new SubscriptionChangeInfo(subscriptionId, subscriptionStatus));
        }

        public void OnMessage(QuoteCancel m, SessionID s)
        {
            if (!m.IsSetQuoteReqID())
            {
                this.Logger.Log(LogLevel.Error,
                                "Receiving (and ignoring) QuoteAck with unspecified subscription id: {0}",
                                QuickFixUtils.FormatMessage(m));
                return;
            }

            string subscriptionId = m.QuoteReqID.getValue();

            this.SendSubscriptionChange(new SubscriptionChangeInfo(subscriptionId,
                                                                       SubscriptionStatus.Rejected));
        }

        public void OnMessage(Quote m, SessionID s)
        {
            QuoteObject quote = this.CreateQuoteFromMessage(m);

            //Indicative prices are not documented at all, however confirmed by GLS contat that they can normally occure
            if (object.ReferenceEquals(quote, QuoteObject.EMPTY_QUOTE))
            {
                //if (!this.Logger.IsLowDiskSpaceConstraintLoggingOn) this.Logger.Log(LogLevel.Info, "Received indicative quote (ignoring it and cancelling the previous): {0}", QuickFixUtils.FormatMessage(m));
                this.SendLastQuoteCancel(m.QuoteReqID.getValue());
                return;
            }

            SendNewQuote(quote);
        }

        private QuoteObject CreateQuoteFromMessage(Quote quoteMessage)
        {
            decimal askSize, bidSize;
            askSize = quoteMessage.OfferSize.getValue();
            bidSize = quoteMessage.BidSize.getValue();

            //Indicative prices are not documented at all, however confirmed by GLS contat that they can normally occure
            if (askSize == 0 && bidSize == 0)
                return QuoteObject.EMPTY_QUOTE;

            PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSingleIdentity(askPrice: quoteMessage.OfferPx.getValue(),
                                           askSize: askSize,
                                           bidPrice: quoteMessage.BidPx.getValue(),
                                           bidSize: bidSize,
                                           symbol: (Kreslik.Integrator.Common.Symbol)quoteMessage.Symbol.getValue(),
                                           counterparty: this.Counterparty,
                                           subscriptionIdentity: quoteMessage.QuoteReqID.getValue(),
                                           identity: quoteMessage.QuoteID.getValue(),
                                           counterpartyTimeUtc: quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                                           marketDataRecordType: MarketDataRecordType.BankQuoteData);
            return PriceReceivingHelper.SharedQuote;
        }


        public void OnMessage(ExecutionReport m, SessionID s)
        {
            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            string orderId = m.ClOrdID.getValue();

            switch (m.OrdStatus.getValue())
            {
                case OrdStatus.NEW:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] was received and enqeued by other end", orderId);
                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;
                    break;
                case OrdStatus.FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Filled;
                    executionInfo = new ExecutionInfo(m.ToNullableDecimal(Tags.CumQty),
                                                                    m.ToNullableDecimalCoalesce(Tags.LastPx, Tags.Price),
                                                                    m.ToNullableString(Tags.ExecID),
                                                                    m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                                                                    m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                                                                    m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.REJECTED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), null, RejectionType.OrderReject);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Order [{0}] was put to unsupported status: [{1}]", orderId, m.OrdStatus.getValue());
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     executionInfo, rejectionInfo));
        }

        #endregion /FIX message cracking
    }
}
