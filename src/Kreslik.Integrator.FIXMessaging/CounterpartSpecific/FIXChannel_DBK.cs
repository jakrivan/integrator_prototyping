﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX44;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal sealed class FIXChannel_DBK : FIXChannelMessagingBase<FIXChannel_DBK>
    {
        public FIXChannel_DBK(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            FIXChannel_DBKSettings settings, bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new BankMarketDataSnapshotFullRefreshCracker(this, logger));
            }
            else
                this.SetQuickMessageParsers(isMdChannel, new DBKExecReportsCrckers(this, logger));
        }

        private FIXChannel_DBKSettings _settings;

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, true),
                                        this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, false),
                                        this.SessionId);
        }

        private MarketDataRequest CreateSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity,
                                                                           bool subscribe)
        {
            var message = new MarketDataRequest(new MDReqID(subscriptionIdentity),
                                                               new SubscriptionRequestType(subscribe
                                                                                               ? SubscriptionRequestType
                                                                                                     .SNAPSHOT_PLUS_UPDATES
                                                                                               : SubscriptionRequestType
                                                                                                     .DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST),
                                                               new MarketDepth(1));

            message.MDUpdateType = new MDUpdateType(MDUpdateType.FULL_REFRESH);

            // bid:
            var marketDataEntryGroupBid = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupBid.Set(new MDEntryType(MDEntryType.BID));
            message.AddGroup(marketDataEntryGroupBid);

            // offer:
            var marketDataEntryGroupOffer = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupOffer.Set(new MDEntryType(MDEntryType.OFFER));
            message.AddGroup(marketDataEntryGroupOffer);

            // symbol group:
            var symbolGroup = new MarketDataRequest.NoRelatedSymGroup();
            symbolGroup.Set(subscriptionInfo.Symbol.ToQuickFixSymbol());
            //symbolGroup.SetField(new Currency_DB(subscriptionInfo.Symbol.ToString().Substring(0, 3)));
            //symbolGroup.SetField(new OrderQty_DB(subscriptionInfo.Quantity));
            //symbolGroup.SetField(new TenorValue_DB(TenorValue_DB.SPOT));
            message.AddGroup(symbolGroup);

            return message;
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            var orderMessage = new QuickFix.FIX44.NewOrderSingle(
                new ClOrdID(identity),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                new OrdType(OrdType.PREVIOUSLY_QUOTED));

            orderMessage.Account = new Account(_settings.Account);
            orderMessage.HandlInst = new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION);
            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.OrdType = new OrdType(OrdType.PREVIOUSLY_QUOTED);
            orderMessage.Price = new Price(orderInfo.RequestedPrice);
            orderMessage.TimeInForce = new TimeInForce(TimeInForce.FILL_OR_KILL);

            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);

            return orderMessage;
        }
        #endregion /FIX message composing


        #region FIX message cracking

        public void OnMessage(MarketDataRequestReject m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received market data request reject: {0}", QuickFixUtils.FormatMessage(m));
            this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(),
                                                                       SubscriptionStatus.Rejected));
        }

        public void OnMessage(MarketDataSnapshotFullRefresh m, SessionID s)
        {
            QuoteObject quote = this.CreateQuoteFromMessage(m);

            if (quote == null)
            {
                this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(), SubscriptionStatus.Broken));
                return;
            }

            if (object.ReferenceEquals(quote, QuoteObject.EMPTY_QUOTE))
            {
                //if (!this.Logger.IsLowDiskSpaceConstraintLoggingOn) this.Logger.Log(LogLevel.Info, "Received indicative quote (ignoring it and cancelling the previous): {0}", QuickFixUtils.FormatMessage(m));
                this.SendLastQuoteCancel(m.MDReqID.getValue());
                return;
            }

            SendNewQuote(quote);
        }

        private const int MaxNumOfMDEntries = 2;

        private QuoteObject CreateQuoteFromMessage(MarketDataSnapshotFullRefresh quoteMessage)
        {
            Kreslik.Integrator.Common.Symbol symbol = (Kreslik.Integrator.Common.Symbol)quoteMessage.Symbol.getValue();
            string subscriptionIdentity = quoteMessage.MDReqID.getValue();

            int mdEntriesNum = quoteMessage.GetInt(Tags.NoMDEntries);

            if (mdEntriesNum > MaxNumOfMDEntries)
            {
                this.Logger.Log(LogLevel.Error, "Received quote with unexpected number of layers: {0}",
                                mdEntriesNum);
                return null;
            }

            decimal bidPrice = 0;
            decimal bidSize = 0;
            decimal askPrice = 0;
            decimal askSize = 0;
            string askIdentity = null;
            string bidIdentity = null;

            for (int groupIndex = 1; groupIndex <= mdEntriesNum; groupIndex += 1)
            {
                Group noMDEntriesGroup = quoteMessage.GetGroup(groupIndex, Tags.NoMDEntries);
                if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.BID)
                {
                    bidPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    bidSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                    bidIdentity = noMDEntriesGroup.GetString(Tags.QuoteEntryID);
                }
                else if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.OFFER)
                {
                    askPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    askSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                    askIdentity = noMDEntriesGroup.GetString(Tags.QuoteEntryID);
                }
            }

            //Zero sizes are documented way of sending indicative price
            if (bidSize == 0 && askSize == 0)
                return QuoteObject.EMPTY_QUOTE;

            PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSidedIdentity(
                askPrice: askPrice, askSize: askSize, bidPrice: bidPrice, bidSize: bidSize, symbol: symbol,
                counterparty: Counterparty, subscriptionIdentity: subscriptionIdentity,
                counterpartyTimeUtc: quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                askIdentity: askIdentity, bidIdentity: bidIdentity,
                marketDataRecordType: MarketDataRecordType.BankQuoteData);

            return PriceReceivingHelper.SharedQuote;
        }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);

            string orderId = m.ClOrdID.getValue();
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            switch (m.OrdStatus.getValue())
            {
                case OrdStatus.NEW:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] was received and enqeued by other end", orderId);
                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;
                    break;
                case OrdStatus.FILLED:
                case 'F':
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Filled;
                    executionInfo = new ExecutionInfo(m.ToNullableDecimal(Tags.OrderQty),
                        //same according to doc
                                                                    m.ToNullableDecimalCoalesce(Tags.LastPx, Tags.Price),
                                                                    m.ToNullableString(Tags.ExecID),
                                                                    m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                                                                    m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                                                                    m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.PARTIALLY_FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.PartiallyFilled;
                    executionInfo = new ExecutionInfo(
                        //We intentiaonally don't convert to nullable 
                        //  - as quantity is absolutely required with partial fills
                        m.OrderQty.getValue(),
                        m.ToNullableDecimalCoalesce(Tags.LastPx, Tags.Price),
                        m.ToNullableString(Tags.ExecID),
                        m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                        m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                        m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.CANCELED:
                case OrdStatus.REJECTED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), null, RejectionType.OrderReject);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Order [{0}] was put to unsupported status: [{1}]", orderId, m.OrdStatus.getValue());
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     executionInfo, rejectionInfo));
        }

        #endregion /FIX message cracking

        private sealed class DBKExecReportsCrckers : BankExecReportCracker
        {
            public DBKExecReportsCrckers(IOrdersParentChannel parentChannel, ILogger logger)
                : base(parentChannel, logger)
            { }

            protected override OrderStatus GetTranslatedOrderStatus(char externalOrderStatus)
            {
                switch (externalOrderStatus)
                {
                    case OrdStatus.NEW:
                    case OrdStatus.PENDING_NEW:
                        return OrderStatus.New;
                    case OrdStatus.FILLED:
                    case 'F':
                        return OrderStatus.Trade;
                    case OrdStatus.PARTIALLY_FILLED:
                        return OrderStatus.PartiallTrade;
                    case OrdStatus.CANCELED:
                    case OrdStatus.REJECTED:
                        return OrderStatus.Reject;
                    default:
                        return OrderStatus.Unrecognized;
                }
            }
        }
    }
}
