﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX42;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal class FIXChannel_JPM : FIXChannelMessagingBase<FIXChannel_JPM>
    {
        public FIXChannel_JPM(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            FIXChannel_JPMSettings settings, bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new BankMarketDataIncrementalRefreshCracker(this, logger));
            }
            else
                this.SetQuickMessageParsers(isMdChannel, new SimpleBankWithNewStatusExecReportsCracker(this, logger));
        }

        private FIXChannel_JPMSettings _settings;

        protected override void OnOutgoingAdminMessage(QuickFix.Message message, SessionID sessionID)
        {
            if (message is Logon)
            {
                message.SetField(new RawDataLength(this._settings.PasswordLength));
                message.SetField(new RawData(this._settings.Password));
            }
        }

        protected override bool SendsQuotedOrdersAsLimit
        {
            get { return true; }
        }

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, true),
                                        this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, false),
                                        this.SessionId);
        }

        private MarketDataRequest CreateSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity,
                                                                           bool subscribe)
        {
            var message = new MarketDataRequest(new MDReqID(subscriptionIdentity),
                                                               new SubscriptionRequestType(subscribe
                                                                                               ? SubscriptionRequestType
                                                                                                     .SNAPSHOT_PLUS_UPDATES
                                                                                               : SubscriptionRequestType
                                                                                                     .DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST),
                                                               new MarketDepth(0));

            message.MDUpdateType = new MDUpdateType(MDUpdateType.FULL_REFRESH);
            message.AggregatedBook = new AggregatedBook(true);
            message.SetField(new DecimalField(9001, subscriptionInfo.Quantity));

            // bid:
            var marketDataEntryGroupBid = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupBid.Set(new MDEntryType(MDEntryType.BID));
            message.AddGroup(marketDataEntryGroupBid);

            // offer:
            var marketDataEntryGroupOffer = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupOffer.Set(new MDEntryType(MDEntryType.OFFER));
            message.AddGroup(marketDataEntryGroupOffer);

            // symbol group:
            var symbolGroup = new MarketDataRequest.NoRelatedSymGroup();
            symbolGroup.Symbol = subscriptionInfo.Symbol.ToQuickFixSymbol();
            message.AddGroup(symbolGroup);

            return message;
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            NewOrderSingle orderMessage = new NewOrderSingle(
                new ClOrdID(identity),
                new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                new OrdType(OrdType.PREVIOUSLY_QUOTED)
                );

            orderMessage.SecurityType = new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT);
            orderMessage.Account = new Account(this._settings.Account);
            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Price = new Price(orderInfo.RequestedPrice);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.OrdType = new OrdType(OrdType.FOREX_LIMIT);
            orderMessage.TimeInForce = new TimeInForce(TimeInForce.FILL_OR_KILL);
            return orderMessage;
        }
        #endregion /FIX message composing

        #region FIX message cracking

        public void OnMessage(MarketDataRequestReject m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received market data request reject: {0}", QuickFixUtils.FormatMessage(m));
            this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(),
                                                                       SubscriptionStatus.Rejected));
        }

        public void OnMessage(MarketDataIncrementalRefresh m, SessionID s)
        {
            int groupIndex = m.GetInt(Tags.NoMDEntries);

            if (groupIndex != 1)
            {
                this.Logger.Log(LogLevel.Error,
                                "Receiving (and ignoring) Incremental MD refresh with unexpected number of groups: {0}",
                                QuickFixUtils.FormatMessage(m));
                this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(), SubscriptionStatus.Broken));
                return;
            }

            if (!m.IsSetMDReqID())
            {
                this.Logger.Log(LogLevel.Error, "Receiving (and ignoring) MD refresh without subscription id: {0}",
                                QuickFixUtils.FormatMessage(m));
                return;
            }

            string subscriptionId = m.MDReqID.getValue();

            Group noMDEntriesGroup = m.GetGroup(groupIndex, Tags.NoMDEntries);

            switch (noMDEntriesGroup.GetChar(Tags.MDUpdateAction))
            {
                case MDUpdateAction.DELETE:
                    //this.Logger.Log(LogLevel.Info, "Receiving MD Refresh deleting previous quote: {0}",
                    //                QuickFixUtils.FormatMessage(m));
                    this.SendLastQuoteCancel(subscriptionId);
                    return;
                case  MDUpdateAction.NEW:
                case MDUpdateAction.CHANGE:
                    break;
                default:
                    this.Logger.Log(LogLevel.Error,
                                "Receiving (and ignoring) Incremental MD refresh with unexpected update type: {0}",
                                QuickFixUtils.FormatMessage(m));
                    break;
            }

            if (!noMDEntriesGroup.IsSetField(Tags.MDEntrySize) || !noMDEntriesGroup.IsSetField(Tags.Symbol))
            {
                this.Logger.Log(LogLevel.Error,
                                "Receiving (and ignoring) MD refresh without size or symbol specified (this shouldn't happen as full refresh was requested): {0}",
                                QuickFixUtils.FormatMessage(m));
                return;
            }

            char mdEntryType = noMDEntriesGroup.GetChar(Tags.MDEntryType);
            switch (mdEntryType)
            {
                case MDEntryType.BID:
                case MDEntryType.OFFER:

                    var priceObject = this._priceReceivingHelper.GetPriceObject(
                            price: noMDEntriesGroup.GetDecimal(Tags.MDEntryPx),
                            sizeBaseAbsInitial: noMDEntriesGroup.GetDecimal(Tags.MDEntrySize),
                            side: mdEntryType == MDEntryType.BID ? PriceSide.Bid : PriceSide.Ask,
                            symbol: (Kreslik.Integrator.Common.Symbol) noMDEntriesGroup.GetString(Tags.Symbol),
                            counterparty: this.Counterparty,
                            counterpartyIdentity: null,
                            marketDataRecordType: MarketDataRecordType.BankQuoteData,
                            integratorReceivedTime: HighResolutionDateTime.UtcNow,
                            counterpartySentTime: m.Header.ToDateTime(Tags.SendingTime, false, this.Logger));

                    this.SendNewPrice(priceObject, subscriptionId);

                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Receiving (and ignoring) MD refresh with unknown MDEntryType: {0}",
                                QuickFixUtils.FormatMessage(m));
                    return;
            }
        }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            string orderId = m.ClOrdID.getValue();

            switch (m.OrdStatus.getValue())
            {
                case OrdStatus.FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Filled;
                    executionInfo = new ExecutionInfo(m.ToNullableDecimal(Tags.CumQty),
                                                                    m.ToNullableDecimalCoalesce(Tags.LastPx, Tags.AvgPx),
                                                                    m.ToNullableString(Tags.ExecID),
                                                                    m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                                                                    m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                                                                    m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.REJECTED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), null, RejectionType.OrderReject);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Order [{0}] was put to unsupported status: [{1}]", orderId, m.OrdStatus.getValue());
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     executionInfo, rejectionInfo));
        }

        #endregion /FIX message cracking
    }
}
