﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.Diagnostics;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    public static class SessionFactory
    {
        public static IOrderFlowSession CreateOrderFlowSession(Counterparty counterparty,
            IPersistedFixConfigsReader persistedFixConfigsReaderOf, IRiskManager riskManager,
            IDealStatisticsConsumer dealStatisticsConsumer, IStreamingSettings streamingSettings, IDelayEvaluator delayEvaluator, ILogger logger)
        {
            object channelORD = CreateFixChannel(counterparty, persistedFixConfigsReaderOf, streamingSettings, logger, false);

            if (channelORD == null)
                return null;
            if (counterparty.TradingTargetCategory == TradingTargetCategory.Venue)
            {
                return new VenueOrderFlowSession((IVenueFIXConnector) channelORD,
                    logger,
                    counterparty,
                    riskManager,
                    dealStatisticsConsumer,
                    SessionsSettings.OrderFlowSessionBehavior, delayEvaluator);
            }
            else
            {
                return new OrderFlowSession((IFIXChannel) channelORD,
                    (IOrderFlowMessagingAdapter) channelORD, logger,
                    counterparty, riskManager, dealStatisticsConsumer,
                    SessionsSettings.OrderFlowSessionBehavior, delayEvaluator);
            }
        }

        public static IMarketDataSession CreateMarketDataSession(Counterparty counterparty, bool resubscribeOnReconnect,
            IPersistedFixConfigsReader persistedFixConfigsReaderMd, IDelayEvaluator delayEvaluator, IStreamingSettings streamingSettings,
            ILogger logger)
        {

            object channelMD = CreateFixChannel(counterparty, persistedFixConfigsReaderMd, streamingSettings, logger, true);

            if (channelMD == null)
                return null;
            if (counterparty.TradingTargetCategory == TradingTargetCategory.Venue)
            {
                return new VenueMarketDataSession((IVenueFIXConnector) channelMD,
                    (IMarketDataMessagingAdapter) channelMD, logger,
                    counterparty, resubscribeOnReconnect,
                    SessionsSettings.MarketDataSessionBehavior, delayEvaluator);
            }
            else
            {
                return new MarketDataSession((IFIXChannel) channelMD,
                    (IMarketDataMessagingAdapter) channelMD, logger,
                    counterparty, resubscribeOnReconnect,
                    SessionsSettings.MarketDataSessionBehavior, delayEvaluator);
            }
        }

        public static StreamingChannel CreateStreamingChannel(Counterparty counterparty,
            IPersistedFixConfigsReader persistedFixConfigsReaderMd,
            IPersistedFixConfigsReader persistedFixConfigsReaderOf, IDealStatisticsConsumer dealStatisticsConsumer,
            ILogger mdLogger, ILogger ofLogger, IRiskManager riskManager,
            IStreamingUnicastInfoForwarder streamingUnicastInfoForwarder, IPersistedStreamingStatsProvider statsProvider,
            ISymbolsInfo symbolsInfo, IStreamingSettings streamingSettings, IDelayEvaluator delayEvaluator)
        {
            if (Counterparty.IsCounterpartyForTakeing(counterparty))
                throw new Exception("MM and Taker sessions mismatch");

            IFIXStreamingChannelPricing pricingChannel = null;
            IFIXStreamingChannelOrders ordersChannel = null;
            IStreamingChannelSettings settings = null;


            if (counterparty.TradingTargetType == TradingTargetType.FXCMMM)
            {
                pricingChannel =
                new FIXStreamingChannelPricing_FSM(persistedFixConfigsReaderMd, mdLogger,
                    counterparty, symbolsInfo, SessionsSettings.UnexpectedMessagesTreatingBehavior);
                ordersChannel =
                    new FIXStreamingChannelOrders_FSM(persistedFixConfigsReaderOf, ofLogger,
                        counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior);

                switch (counterparty.ToString())
                {
                    case Counterparty.EnumValues.FS1:
                        settings = SessionsSettings.FIXChannel_FS1Behavior;
                        break;
                    case Counterparty.EnumValues.FS2:
                        settings = SessionsSettings.FIXChannel_FS2Behavior;
                        break;
                    default:
                        throw new ArgumentException("Unexpected FXCMMMCounterparty counterparty");
                }
            }
            else if (counterparty == Counterparty.FL1)
            {
                pricingChannel =
                new FIXStreamingChannelPricing_FAL(persistedFixConfigsReaderMd, mdLogger,
                    counterparty, symbolsInfo, SessionsSettings.UnexpectedMessagesTreatingBehavior);
                ordersChannel =
                    new FIXStreamingChannelOrders_FAL(persistedFixConfigsReaderOf, ofLogger,
                        counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior);
                settings = SessionsSettings.FIXChannel_FL1Behavior;
            }
            else if (counterparty.TradingTargetType == TradingTargetType.Hotspot)
            {
                FIXChannel_HOTSettings takerSettings; 

                switch (counterparty.ToString())
                {
                    case Counterparty.EnumValues.HTA:
                        settings = SessionsSettings.FIXChannel_HTAstreamBehavior;
                        takerSettings = SessionsSettings.FIXChannel_HTABehavior;
                        break;
                    case Counterparty.EnumValues.HT3:
                        settings = SessionsSettings.FIXChannel_HT3streamBehavior;
                        takerSettings = SessionsSettings.FIXChannel_HT3Behavior;
                        break;
                    default:
                        throw new ArgumentException("Unexpected FXCMMMCounterparty counterparty");
                }

                pricingChannel = new FIXStreamingChannelPricingAndOrders_HOT(persistedFixConfigsReaderOf, ofLogger,
                    counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, riskManager, symbolsInfo,
                    takerSettings);
                ordersChannel = pricingChannel as FIXStreamingChannelPricingAndOrders_HOT;
            }
            else
            {
                throw new ArgumentException("Unexpected streaming counterparty " + counterparty);
            }




            StreamingChannel streamChannel = new StreamingChannel(pricingChannel, ordersChannel, null, dealStatisticsConsumer,
                settings, riskManager, streamingUnicastInfoForwarder, statsProvider, DiagnosticsManager.GetSettlementDatesKeeper(counterparty), 
                streamingSettings, delayEvaluator, ofLogger);
            return streamChannel;
        }

        public static void BindChannelsIfNeeded(IMarketDataSession marketDataSession,
            IOrderFlowSession orderFlowSession)
        {
            if (marketDataSession != null && orderFlowSession != null &&
                marketDataSession.Counterparty.TradingTargetType == TradingTargetType.LMAX &&
                orderFlowSession.Counterparty.TradingTargetType == TradingTargetType.LMAX)
            {
                IPriceFilter priceFilter =
                    ((marketDataSession as VenueMarketDataSession).FixChannel as FIXChannel_LMX).PriceFilter;

                if (priceFilter != null)
                {
                    orderFlowSession.OrderSubmitted += priceFilter.HandleDealingOnPrice;
                    orderFlowSession.OrderNotActive += priceFilter.HandleOrderNotActive;
                }
                else
                {
                    LogFactory.Instance.GetLogger(null)
                        .Log(LogLevel.Fatal,
                            "Cannot setup LMAX prices filter (probably quick parsing is disabled) - so we will receive duplicate pricing and our own prices");
                }
            }
        }

        private static object CreateFixChannel(Counterparty counterparty, IPersistedFixConfigsReader persistedFixConfigsReader, IStreamingSettings streamingSettings, ILogger logger, bool isMdChannel)
        {
            object channel;


            if (counterparty == Counterparty.HSB)
            {
                var subscriptionSettings = SessionsSettings.SubscriptionSettings[Counterparty.HSB].FirstOrDefault();
                string senderSubId = null;
                if (subscriptionSettings != null)
                    senderSubId = subscriptionSettings.Item3;

                channel = new FIXChannel_HSB(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, senderSubId, isMdChannel);
            }
            else if (counterparty == Counterparty.CRS)
            {
                channel = new FIXChannel_CRS(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_CRSBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.UBS)
            {
                channel = new FIXChannel_UBS(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_UBSBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.DBK)
            {
                channel = new FIXChannel_DBK(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_DBKBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.CTI)
            {
                channel = new FIXChannel_CTI(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_CTIBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.BOA)
            {
                channel = new FIXChannel_BOA(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.MGS)
            {
                channel = new FIXChannel_MGS(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_MGSBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.RBS)
            {
                channel = new FIXChannel_RBS(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_RBSBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.JPM)
            {
                channel = new FIXChannel_JPM(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_JPMBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.GLS)
            {
                channel = new FIXChannel_GLS(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.BNP)
            {
                channel = new FIXChannel_BNP(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_BNPBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.CZB)
            {
                channel = new FIXChannel_CZB(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_CZBBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.BRX)
            {
                channel = new FIXChannel_BRX(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior);
            }
            else if (counterparty == Counterparty.SOC)
            {
                channel = new FIXChannel_SOC(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_SOCBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.NOM)
            {
                channel = new FIXChannel_NOM(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_NOMBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.HTA)
            {
                channel = new FIXChannel_HOT(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_HTABehavior, streamingSettings);
            }
            else if (counterparty == Counterparty.HTF)
            {
                channel = new FIXChannel_HOT(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_HTFBehavior, streamingSettings);
            }
            else if (counterparty == Counterparty.HT3)
            {
                channel = new FIXChannel_HOT(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_HT3Behavior, streamingSettings);
            }
            else if (counterparty == Counterparty.H4T)
            {
                channel = new FIXChannel_HOT(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_H4TBehavior, streamingSettings);
            }
            else if (counterparty == Counterparty.H4M)
            {
                channel = new FIXChannel_HOT(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_H4MBehavior, streamingSettings);
            }
            else if (counterparty == Counterparty.FA1)
            {
                channel = new FIXChannel_FAL(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_FALBehavior, isMdChannel);
            }
            else if (counterparty == Counterparty.L01)
            {
                channel = new FIXChannel_LMX(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_LM1Behavior, LmaxCounterparty.L01, isMdChannel);
            }
            else if (counterparty == Counterparty.LM2)
            {
                channel = new FIXChannel_LMX(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_LM2Behavior, LmaxCounterparty.LM2, isMdChannel);
            }
            else if (counterparty == Counterparty.LM3)
            {
                channel = new FIXChannel_LMX(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_LM3Behavior, LmaxCounterparty.LM3, isMdChannel);
            }
            else if (counterparty == Counterparty.LX1)
            {
                channel = new FIXChannel_LMX(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_LX1Behavior, LmaxCounterparty.LX1, isMdChannel);
            }
            else if (counterparty == Counterparty.LGA)
            {
                channel = new FIXChannel_LMX(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_LGABehavior, LmaxCounterparty.LGA, isMdChannel);
                if (isMdChannel)
                    ((FIXChannel_LMX) channel).SubscribeToTickers = true;
            }
            else if (counterparty == Counterparty.LGC)
            {
                channel = new FIXChannel_LMX(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_LGCBehavior, LmaxCounterparty.LGC, isMdChannel);
            }
            else if (counterparty == Counterparty.FC1)
            {
                channel = new FIXChannel_FCM(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_FC1Behavior, isMdChannel);
                if(isMdChannel)
                    (DiagnosticsManager.GetSettlementDatesKeeper(Counterparty.FS1) as SettlementDatesKeeper).RegisterSettlementDatesProvider(channel as FIXChannel_FCM);
            }
            else if (counterparty == Counterparty.FC2)
            {
                channel = new FIXChannel_FCM(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_FC2Behavior, isMdChannel);
                if(isMdChannel)
                    (DiagnosticsManager.GetSettlementDatesKeeper(Counterparty.FS1) as SettlementDatesKeeper).RegisterSettlementDatesProvider(channel as FIXChannel_FCM);
            }
            else if (counterparty == Counterparty.PX1)
            {
                channel = new FIXChannel_PXM(persistedFixConfigsReader, logger, counterparty, SessionsSettings.UnexpectedMessagesTreatingBehavior, SessionsSettings.FIXChannel_PX1Behavior, isMdChannel);
            }
            else
            {
                channel = null;
            }

            return channel;
        }
    }
}
