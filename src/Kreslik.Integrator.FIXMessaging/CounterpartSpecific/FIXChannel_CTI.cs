﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using QuickFix;
using QuickFix.FIX44;
using QuickFix.Fields;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal class FIXChannel_CTI : FIXChannelMessagingBase<FIXChannel_CTI>
    {
        public FIXChannel_CTI(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            FIXChannel_CTISettings settings, bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new BankQuoteDataCracker(this, logger), new QuoteCancelCracker(this, logger));
            }
            else
                this.SetQuickMessageParsers(isMdChannel, new SimpleBankWithNewStatusExecReportsCracker(this, logger));
        }

        private FIXChannel_CTISettings _settings;

        protected override void OnOutgoingAdminMessage(QuickFix.Message message, QuickFix.SessionID sessionID)
        {
            if (message is Logon)
            {
                message.SetField(new Password(this._settings.Password));
            }
        }

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            //Send Quote Request SER
            string quoteId = subscriptionIdentity;
            var qr = new QuoteRequest(new QuoteReqID(quoteId));
            qr.NoRelatedSym = new NoRelatedSym(1);

            QuoteRequest.NoRelatedSymGroup symGroup = new QuoteRequest.NoRelatedSymGroup();
            symGroup.Symbol = subscriptionInfo.Symbol.ToQuickFixSymbol();

            symGroup.OrderQty = new OrderQty(subscriptionInfo.Quantity);
            symGroup.Currency = subscriptionInfo.Symbol.ToQuickBaseCurrency();
            symGroup.QuoteRequestType = new QuoteRequestType(QuoteRequestType.AUTOMATIC);
            qr.AddGroup(symGroup);

            return Session.SendToTarget(qr, this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            QuoteResponse quoteCancelMessage =
                new QuoteResponse(new QuoteRespID(subscriptionIdentity), new QuoteRespType(QuoteRespType.PASS),
                                  subscriptionInfo.Symbol.ToQuickFixSymbol());
            quoteCancelMessage.SetField(new QuoteReqID(subscriptionIdentity));

            return Session.SendToTarget(quoteCancelMessage, this.SessionId);
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            var orderMessage = new NewOrderSingle(
                new ClOrdID(identity),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                new OrdType(OrdType.PREVIOUSLY_QUOTED));

            orderMessage.Account = new Account(this._settings.Account);
            orderMessage.HandlInst = new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION);
            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.OrdType = new OrdType(OrdType.PREVIOUSLY_QUOTED);
            orderMessage.TimeInForce = new QuickFix.Fields.TimeInForce(QuickFix.Fields.TimeInForce.FILL_OR_KILL);
            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);
            return orderMessage;
        }
        #endregion /FIX message composing

        #region FIX message cracking

        public void OnMessage(QuoteRequestReject m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received quote request reject: {0}", QuickFixUtils.FormatMessage(m));

            this.SendSubscriptionChange(new SubscriptionChangeInfo(m.QuoteReqID.getValue(),
                                                                           SubscriptionStatus.Rejected));
        }
        
        public void OnMessage(QuoteCancel m, SessionID s)
        {
            string reqId;

            if (!m.IsSetQuoteReqID())
            {
                this.Logger.Log(LogLevel.Error,
                                "Received quote cancel without QuoteReqId populated. This likely means all streams from Citi are canceled and should be resubscribed. Quote: {0}",
                                QuickFixUtils.FormatMessage(m));

                // Indicate to MarketDataSession that all subscription are being rejected
                reqId = "*";
            }
            else
            {
                reqId = m.QuoteReqID.getValue();
            }

            string quoteId = m.QuoteID.getValue();

            if (string.Equals(quoteId, "UNSUBSCRIBE", StringComparison.OrdinalIgnoreCase))
            {
                //this.Logger.Log(LogLevel.Debug, "Received quote cancel cancelling the entire stream: {0}", QuickFixUtils.FormatMessage(m));

                this.SendSubscriptionChange(new SubscriptionChangeInfo(reqId, SubscriptionStatus.Rejected));
            }
            else
            {
                //this.Logger.Log(LogLevel.Info, "Received quote cancel : {0}", QuickFixUtils.FormatMessage(m));

                this.SendLastQuoteCancel(reqId);
            }
        }

        public void OnMessage(Quote m, SessionID s)
        {
            switch (m.QuoteType.getValue())
            {
                case QuoteType.RESTRICTED_TRADEABLE:
                    break;
                case QuoteType.INDICATIVE:
                    //if (!this.Logger.IsLowDiskSpaceConstraintLoggingOn) this.Logger.Log(LogLevel.Info, "Received indicative quote (ignoring it and cancelling the previous): {0}", QuickFixUtils.FormatMessage(m));
                    this.SendLastQuoteCancel(m.QuoteReqID.getValue());
                    return;
                default:
                    this.Logger.Log(LogLevel.Error, "Received quote with unsuported type (ignoring it and cancelling the previous): {0}", QuickFixUtils.FormatMessage(m));
                    this.SendLastQuoteCancel(m.QuoteReqID.getValue());
                    return;
            }

            SendNewQuote(this.CreateQuoteFromMessage(m));
        }

        private QuoteObject CreateQuoteFromMessage(Quote quoteMessage)
        {
            PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSingleIdentity(askPrice: quoteMessage.OfferPx.getValue(),
                                           askSize: quoteMessage.IsSetOfferSize() ? quoteMessage.OfferSize.getValue() : 0,
                                           bidPrice: quoteMessage.BidPx.getValue(),
                                           bidSize: quoteMessage.IsSetBidSize() ? quoteMessage.BidSize.getValue() : 0,
                                           symbol: (Kreslik.Integrator.Common.Symbol)quoteMessage.Symbol.getValue(),
                                           counterparty: this.Counterparty,
                                           subscriptionIdentity: quoteMessage.QuoteReqID.getValue(),
                                           identity: quoteMessage.QuoteID.getValue(),
                                           counterpartyTimeUtc: quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                                           marketDataRecordType: MarketDataRecordType.BankQuoteData);
            return PriceReceivingHelper.SharedQuote;
        }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            string orderId = m.ClOrdID.getValue();

            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            switch (m.OrdStatus.getValue())
            {
                case OrdStatus.NEW:
                case OrdStatus.PENDING_NEW:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] was received and enqeued by other end", orderId);
                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;
                    break;
                case OrdStatus.FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Filled;
                    executionInfo = new ExecutionInfo(m.ToNullableDecimal(Tags.CumQty),
                                                                    m.ToNullableDecimalCoalesce(Tags.LastPx, Tags.Price),
                                                                    m.ToNullableString(Tags.ExecID),
                                                                    m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                                                                    m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                                                                    m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.REJECTED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), null, RejectionType.OrderReject);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Order [{0}] was put to unsupported status: [{1}]", orderId, m.OrdStatus.getValue());
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     executionInfo, rejectionInfo));
        }

        #endregion /FIX message cracking
    }
}
