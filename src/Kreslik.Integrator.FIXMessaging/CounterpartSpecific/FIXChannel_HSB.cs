﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX44;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal class FIXChannel_HSB : FIXChannelMessagingBase<FIXChannel_HSB>
    {
        public FIXChannel_HSB(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            string senderSubId, bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();
            this.OnStarted += SendTradingSessionStatus;
            this._singleSenderSubId = senderSubId;

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new BankMarketDataSnapshotFullRefreshCracker(this, logger), new QuoteCancelCracker(this, logger));
            }
            else
                this.SetQuickMessageParsers(isMdChannel, new SimpleBankExecReportsCracker(this, logger));
        }

        private void SendTradingSessionStatus()
        {
            var request = new TradingSessionStatus(new TradingSessionID("TestTradingSessionID"), new TradSesStatus(TradSesStatus.OPEN));
            request.SetField(new Text("KGT trading session initilized"));
            Session.SendToTarget(request, this.SessionId);
        }

        private readonly string _singleSenderSubId;

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            QuickFix.Message subscriptionMessage = this.GetMarketDataSubscriptionMsg(subscriptionInfo, subscriptionIdentity, true);
            return Session.SendToTarget(subscriptionMessage, this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            QuickFix.Message subscriptionMessage = this.GetMarketDataSubscriptionMsg(subscriptionInfo, subscriptionIdentity, false);
            return Session.SendToTarget(subscriptionMessage, this.SessionId);
        }

        QuickFix.Message GetMarketDataSubscriptionMsg(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity, bool subscribe)
        {
            var mdReqID = new MDReqID(subscriptionIdentity);

            var subType = new SubscriptionRequestType(subscribe ? SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES : SubscriptionRequestType.DISABLE_PREVIOUS);
            var marketDepth = new MarketDepth(1);

            var message = new MarketDataRequest(mdReqID, subType, marketDepth);

            // bid:

            var marketDataEntryGroupBid = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupBid.Set(new MDEntryType(MDEntryType.BID));
            message.AddGroup(marketDataEntryGroupBid);

            // offer:

            var marketDataEntryGroupOffer = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupOffer.Set(new MDEntryType(MDEntryType.OFFER));
            message.AddGroup(marketDataEntryGroupOffer);

            // symbol group:

            var symbolGroup = new MarketDataRequest.NoRelatedSymGroup();
            symbolGroup.Set(subscriptionInfo.Symbol.ToQuickFixSymbol());

            var underlyingsGroup = new MarketDataRequest.NoRelatedSymGroup.NoUnderlyingsGroup
            {
                UnderlyingQty = new UnderlyingQty(subscriptionInfo.Quantity)
            };
            symbolGroup.AddGroup(underlyingsGroup);

            message.AddGroup(symbolGroup);

            message.SetField(new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)));

            if (!string.IsNullOrEmpty(subscriptionInfo.SenderSubId))
            {
                message.SetField(new SenderSubID(subscriptionInfo.SenderSubId));
                if(this._singleSenderSubId != subscriptionInfo.SenderSubId)
                    throw new Exception("Only a single SenderSubId per session is allowed by Integrator");
            }

            return message;
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            var orderMessage = new NewOrderSingle(
                new ClOrdID(identity),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                new OrdType(OrdType.FOREX_PREVIOUSLY_QUOTED));

            var td = TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now);
            var tradeDate = td.Year.ToString("d4") + td.Month.ToString("d2") + td.Day.ToString("d2");

            orderMessage.TradeDate = new TradeDate(tradeDate);
            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.TimeInForce = new TimeInForce(TimeInForce.FILL_OR_KILL); // HSBC API: "Current version supports value “4” only"

            orderMessage.SetField(new SettlementType_HSBC(SettlementType_HSBC.SPOT)); // custom field 9063!
            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);
            orderMessage.Price = new Price(orderInfo.RequestedPrice);

            if (!string.IsNullOrEmpty(this._singleSenderSubId))
            {
                orderMessage.SetField(new SenderSubID(this._singleSenderSubId));
            }


            return orderMessage;
        }
        #endregion /FIX message composing

        #region FIX message cracking

        public void OnMessage(MarketDataRequestReject m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received market data request reject: {0}", QuickFixUtils.FormatMessage(m));
            SubscriptionChangeInfo info = new SubscriptionChangeInfo(m.MDReqID.getValue(), SubscriptionStatus.Rejected);
            this.SendSubscriptionChange(info);
        }

        public void OnMessage(MarketDataSnapshotFullRefresh m, SessionID s)
        {
            QuoteObject quote = this.CreateQuoteFromMessage(m);

            if (quote == null)
            {
                this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(), SubscriptionStatus.Broken));
                return;
            }

            SendNewQuote(quote);
        }

        private const int MaxNumOfMDEntries = 2;

        private QuoteObject CreateQuoteFromMessage(MarketDataSnapshotFullRefresh quoteMessage)
        {
            //For HSBC there is no Idicative/Executable flag in MD snapshot
            //Also zero sizes are not documented way of sending indicative price

            Kreslik.Integrator.Common.Symbol symbol = (Kreslik.Integrator.Common.Symbol)quoteMessage.Symbol.getValue();
            string subscriptionIdentity = quoteMessage.MDReqID.getValue();

            //string targetSubId = quoteMessage.Header.GetField(Tags.TargetSubID);

            int mdEntriesNum = quoteMessage.GetInt(Tags.NoMDEntries);

            if (mdEntriesNum > MaxNumOfMDEntries)
            {
                this.Logger.Log(LogLevel.Error, "Received quote with unexpected number of layers: {0}",
                                mdEntriesNum);
                return null;
            }

            decimal bidPrice = 0;
            string bidIdentity = null;
            decimal bidSize = 0;
            decimal askPrice = 0;
            string askIdentity = null;
            decimal askSize = 0;

            for (int groupIndex = 1; groupIndex <= mdEntriesNum; groupIndex += 1)
            {
                Group noMDEntriesGroup = quoteMessage.GetGroup(groupIndex, Tags.NoMDEntries);
                if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.BID)
                {
                    bidPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    bidIdentity = noMDEntriesGroup.GetField(Tags.QuoteEntryID);
                    bidSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                }
                else if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.OFFER)
                {
                    askPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    askIdentity = noMDEntriesGroup.GetField(Tags.QuoteEntryID);
                    askSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                }
            }

            PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSidedIdentity(askPrice: askPrice, askSize: askSize, bidPrice: bidPrice,
                                                            bidSize: bidSize, symbol: symbol,
                                                            counterparty: this.Counterparty,
                                                            subscriptionIdentity: subscriptionIdentity,
                                                            counterpartyTimeUtc:
                                                                quoteMessage.Header.ToDateTime(Tags.SendingTime, false,
                                                                                               this.Logger),
                                                            askIdentity: askIdentity,
                                                            bidIdentity: bidIdentity,
                                                            marketDataRecordType: MarketDataRecordType.BankQuoteData);
            //quote.TargertSubId = targetSubId;

            return PriceReceivingHelper.SharedQuote;
        }

        public void OnMessage(QuoteCancel m, SessionID s)
        {
            switch (m.QuoteID.getValue())
            {
                case "ALL":
                    //this.Logger.Log(LogLevel.Info, "Received quote cancel: {0}", QuickFixUtils.FormatMessage(m));
                    this.SendLastQuoteCancel(m.QuoteReqID.getValue());
                    break;
                case "*":
                    this.SendSubscriptionChange(new SubscriptionChangeInfo(m.QuoteReqID.getValue(),
                                                                           SubscriptionStatus.Rejected));
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Received quote cancel with unsupported QuoteId: {0}", m.QuoteReqID.getValue());
                    break;
            }
        }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);

            string orderId = m.ClOrdID.getValue();
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            switch (m.OrdStatus.getValue())
            {
                case OrdStatus.FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Filled;
                    executionInfo = new ExecutionInfo(m.ToNullableDecimal(Tags.OrderQty),
                                                                    m.ToNullableDecimalCoalesce(Tags.LastPx, Tags.Price),
                                                                    m.ToNullableString(Tags.ExecID),
                                                                    m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                                                                    m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                                                                    m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.REJECTED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), null, RejectionType.OrderReject);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Order [{0}] was put to unsupported status: [{1}]", orderId, m.OrdStatus.getValue());
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     executionInfo, rejectionInfo));
        }

        public void OnMessage(QuickFix.Message m, SessionID s)
        {
            if (m.Header.GetString(35).Equals("U4", StringComparison.OrdinalIgnoreCase))
            {
                m.Header.SetField(new StringField(35, "U2"));
                m.SetField(new TransactTime(DateTime.UtcNow));
                Session.SendToTarget(m, s);
            }
            else
            {
                this.Logger.Log(LogLevel.Error, "Received unknown message: [{0}]", QuickFixUtils.FormatMessage(m));
            }
        }

        #endregion /FIX message cracking
    }
}
