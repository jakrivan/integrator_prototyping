﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX43;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal class FIXChannel_UBS : FIXChannelMessagingBase<FIXChannel_UBS>
    {
        public FIXChannel_UBS(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            FIXChannel_UBSSettings settings, bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new BankMarketDataSnapshotFullRefreshCracker(this, logger));
            }
            else
                this.SetQuickMessageParsers(isMdChannel, new UBSExecReportsCrckers(this, logger));
        }

        private FIXChannel_UBSSettings _settings;

        protected override void OnOutgoingAdminMessage(QuickFix.Message message, QuickFix.SessionID sessionID)
        {
            if (message is Logon)
            {
                message.SetField(new Username(this._settings.Username));
                message.SetField(new Password(this._settings.Password));
            }
        }

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, true),
                                        this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, false),
                                        this.SessionId);
        }

        private MarketDataRequest CreateSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity,
                                                                           bool subscribe)
        {
            var message = new MarketDataRequest(new MDReqID(subscriptionIdentity),
                                                               new SubscriptionRequestType(subscribe
                                                                                               ? SubscriptionRequestType
                                                                                                     .SNAPSHOT_PLUS_UPDATES
                                                                                               : SubscriptionRequestType
                                                                                                     .DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST),
                                                               new MarketDepth(1));

            message.MDUpdateType = new MDUpdateType(MDUpdateType.FULL_REFRESH);

            // bid:
            var marketDataEntryGroupBid = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupBid.Set(new MDEntryType(MDEntryType.BID));
            message.AddGroup(marketDataEntryGroupBid);

            // offer:
            var marketDataEntryGroupOffer = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupOffer.Set(new MDEntryType(MDEntryType.OFFER));
            message.AddGroup(marketDataEntryGroupOffer);

            // symbol group:
            var symbolGroup = new QuickFix.FIX43.MarketDataRequest.NoRelatedSymGroup();
            symbolGroup.Set(subscriptionInfo.Symbol.ToQuickFixSymbol());
            message.AddGroup(symbolGroup);

            message.SetField(new MDEntrySize(subscriptionInfo.Quantity));

            return message;
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            var orderMessage = new NewOrderSingle(
                new ClOrdID(identity),
                new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                new OrdType(OrdType.PREVIOUSLY_QUOTED));

            NewOrderSingle.NoPartyIDsGroup partyIDsGroup = new NewOrderSingle.NoPartyIDsGroup();
            partyIDsGroup.PartyID = new PartyID(this._settings.PartyId);
            partyIDsGroup.PartyIDSource = new PartyIDSource(PartyIDSource.PROPRIETARY_CUSTOM_CODE);
            partyIDsGroup.PartyRole = new PartyRole(PartyRole.ORDER_ORIGINATION_FIRM);
            orderMessage.AddGroup(partyIDsGroup);

            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.TimeInForce = new TimeInForce(TimeInForce.FILL_OR_KILL);
            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);
            orderMessage.Price = new Price(orderInfo.RequestedPrice);

            return orderMessage;
        }
        #endregion /FIX message composing


        #region FIX message cracking

        public void OnMessage(MarketDataRequestReject m, SessionID s)
        {
            this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(),
                                                                       SubscriptionStatus.Rejected));
        }

        public void OnMessage(MarketDataSnapshotFullRefresh m, SessionID s)
        {
            QuoteObject quote = this.CreateQuoteFromMessage(m);

            if (quote == null)
            {
                this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(), SubscriptionStatus.Broken));
                return;
            }

            if (object.ReferenceEquals(quote, QuoteObject.EMPTY_QUOTE))
            {
                //if (!this.Logger.IsLowDiskSpaceConstraintLoggingOn) this.Logger.Log(LogLevel.Info, "Received indicative quote (ignoring it and cancelling the previous): {0}", QuickFixUtils.FormatMessage(m));
                this.SendLastQuoteCancel(m.MDReqID.getValue());
                return;
            }

            SendNewQuote(quote);
        }

        private const int MaxNumOfMDEntries = 2;

        private QuoteObject CreateQuoteFromMessage(MarketDataSnapshotFullRefresh quoteMessage)
        {
            Kreslik.Integrator.Common.Symbol symbol = (Kreslik.Integrator.Common.Symbol)quoteMessage.Symbol.getValue();
            string subscriptionIdentity = quoteMessage.MDReqID.getValue();

            int mdEntriesNum = quoteMessage.GetInt(Tags.NoMDEntries);

            if (mdEntriesNum > MaxNumOfMDEntries)
            {
                this.Logger.Log(LogLevel.Error, "Received quote with unexpected number of layers: {0}",
                                mdEntriesNum);
                return null;
            }

            decimal bidPrice = 0;
            string bidIdentity = null;
            decimal bidSize = 0;
            decimal askPrice = 0;
            string askIdentity = null;
            decimal askSize = 0;

            for (int groupIndex = 1; groupIndex <= mdEntriesNum; groupIndex += 1)
            {
                Group noMDEntriesGroup = quoteMessage.GetGroup(groupIndex, Tags.NoMDEntries);
                if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.BID)
                {
                    bidPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    bidIdentity = noMDEntriesGroup.GetField(Tags.QuoteEntryID);
                    if (this.GetIsIndicative(noMDEntriesGroup))
                    {
                        bidSize = 0;
                    }
                    else
                    {
                        bidSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                    }
                }
                else if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.OFFER)
                {
                    askPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    askIdentity = noMDEntriesGroup.GetField(Tags.QuoteEntryID);
                    if (this.GetIsIndicative(noMDEntriesGroup))
                    {
                        askSize = 0;
                    }
                    else
                    {
                        askSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                    }
                }
            }

            //Zero sizes are documented way of sending indicative price
            if (bidSize == 0 && askSize == 0)
                return QuoteObject.EMPTY_QUOTE;

            PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSidedIdentity(askPrice: askPrice, askSize: askSize, bidPrice: bidPrice, bidSize: bidSize,
                                      symbol: symbol, counterparty: this.Counterparty,
                                      subscriptionIdentity: subscriptionIdentity,
                                      counterpartyTimeUtc:
                                          quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                                      askIdentity: askIdentity,
                                      bidIdentity: bidIdentity,
                                      marketDataRecordType: MarketDataRecordType.BankQuoteData);

            return PriceReceivingHelper.SharedQuote;
        }

        private bool GetIsIndicative(Group noMDEntriesGroup)
        {
            string quoteCondition = noMDEntriesGroup.GetField(Tags.QuoteCondition);
            bool indicative = true;

            switch (quoteCondition)
            {
                case QuoteCondition.OPEN_ACTIVE:
                    indicative = false;
                    break;
                case QuoteCondition.NON_FIRM:
                    if (!this.Logger.IsLowDiskSpaceConstraintLoggingOn) this.Logger.Log(LogLevel.Info, "Received indicative side of quote, ignoring the side");
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Received quote side in unsupported condition: {0}. Ignoring side", quoteCondition);
                    break;
            }

            return indicative;
        }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            string orderId = m.ClOrdID.getValue();

            switch (m.OrdStatus.getValue())
            {
                case OrdStatus.NEW:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] was received and enqeued by other end", orderId);
                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;
                    break;
                case OrdStatus.FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Filled;
                    executionInfo = new ExecutionInfo(m.ToNullableDecimal(Tags.LastQty),
                                                                    m.ToNullableDecimalCoalesce(Tags.LastPx, Tags.Price),
                                                                    m.ToNullableString(Tags.ExecID),
                                                                    m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                                                                    m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                                                                    m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.PARTIALLY_FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.PartiallyFilled;
                    executionInfo = new ExecutionInfo(
                        //We intentiaonally don't convert to nullable 
                        //  - as quantity is absolutely required with partial fills
                        m.LastQty.getValue(),
                        m.ToNullableDecimal(Tags.Price),
                        m.ToNullableString(Tags.ExecID),
                        m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                        m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                        m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.CANCELED:
                case OrdStatus.REJECTED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), null, RejectionType.OrderReject);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Order [{0}] was put to unsupported status: [{1}]", orderId, m.OrdStatus.getValue());
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     executionInfo, rejectionInfo));
        }

        #endregion /FIX message cracking

        private sealed class UBSExecReportsCrckers : BankExecReportCracker
        {
            public UBSExecReportsCrckers(IOrdersParentChannel parentChannel, ILogger logger)
                : base(parentChannel, logger)
            { }

            protected override OrderStatus GetTranslatedOrderStatus(char externalOrderStatus)
            {
                switch (externalOrderStatus)
                {
                    case OrdStatus.NEW:
                        return OrderStatus.New;
                    case OrdStatus.FILLED:
                        return OrderStatus.Trade;
                    case OrdStatus.PARTIALLY_FILLED:
                        return OrderStatus.PartiallTrade;
                    case OrdStatus.CANCELED:
                    case OrdStatus.REJECTED:
                        return OrderStatus.Reject;
                    default:
                        return OrderStatus.Unrecognized;
                }
            }
        }
    }
}
