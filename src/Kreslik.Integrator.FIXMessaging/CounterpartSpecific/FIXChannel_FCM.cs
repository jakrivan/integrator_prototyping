﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX42;
using Message = QuickFix.Message;
using Price = QuickFix.Fields.Price;
using SessionState = Kreslik.Integrator.Contracts.SessionState;
using Symbol = Kreslik.Integrator.Common.Symbol;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    public interface IFxcmChannel
    {
        bool CancelAllSent { get; }
    }

    internal class FIXChannel_FCM : FIXChannel_OutFluent<FIXChannel_FCM>, IVenueFIXConnector, IFxcmChannel, ISettlementDatesProvider
    {
        public FIXChannel_FCM(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            FIXChannel_FCMSettings settings, bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();

            if (isMdChannel)
                this.SetQuickMessageParsers(isMdChannel, new FXCMQuoteCracker(this, this, logger), 
                    new FXCMSecurityInfoCracker(this));
            else
                this.SetQuickMessageParsers(isMdChannel, 
                    new FXCMExecReportCracker(this, this, logger),
                    new VenueCancelRejectCracker(this, logger));
        }

        private FIXChannel_FCMSettings _settings;

        protected override bool SupportsLimitOrders
        {
            get { return true; }
        }

        protected override bool SendsQuotedOrdersAsLimit
        {
            get { return true; }
        }

        public bool CancelAllSent
        {
            get { return this._cancelAllSent; }
        }

        #region FIX message composing

        public bool TryRequestSettlemtDatesInfo()
        {
            if (this.State != SessionState.Running)
                return false;

            QuickFix.FIX44.SecurityListRequest req = new QuickFix.FIX44.SecurityListRequest(new SecurityReqID("SecReq__" + DateTime.UtcNow.ToString("yyyyMMdd_HHmmss")),
                                                                                            new SecurityListRequestType(
                                                                                                SecurityListRequestType
                                                                                                    .SYMBOL));
            req.Symbol = new QuickFix.Fields.Symbol("NA");
            req.Product = new Product(Product.CURRENCY);
            return Session.SendToTarget(req, this.SessionId);
        }

        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            QuickFix.Message subscriptionMessage = this.GetMarketDataSubscriptionMsg(subscriptionInfo, subscriptionIdentity, true);
            return Session.SendToTarget(subscriptionMessage, this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            QuickFix.Message subscriptionMessage = this.GetMarketDataSubscriptionMsg(subscriptionInfo, subscriptionIdentity, false);
            return Session.SendToTarget(subscriptionMessage, this.SessionId);
        }

        QuickFix.Message GetMarketDataSubscriptionMsg(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity, bool subscribe)
        {
            var message = new MarketDataRequest(new MDReqID(subscriptionIdentity),
                                                new SubscriptionRequestType(subscribe
                                                                                ? SubscriptionRequestType
                                                                                      .SNAPSHOT_PLUS_UPDATES
                                                                                : SubscriptionRequestType
                                                                                      .DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST),
                                                new MarketDepth(1));

            //message.SetField(new OnBehalfOfCompID(this._settings.OnBehalfOfCompID));

            message.AggregatedBook = new AggregatedBook(true);
            //message.SetField(new OrderQty(subscriptionInfo.Quantity));

            message.MDUpdateType = new MDUpdateType(MDUpdateType.INCREMENTAL_REFRESH);

            // bid:
            var marketDataEntryGroupBid = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupBid.Set(new MDEntryType(MDEntryType.BID));
            message.AddGroup(marketDataEntryGroupBid);

            // offer:
            var marketDataEntryGroupOffer = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupOffer.Set(new MDEntryType(MDEntryType.OFFER));
            message.AddGroup(marketDataEntryGroupOffer);

            //trade:
            var marketDataEntryGroupTrade = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupTrade.Set(new MDEntryType(MDEntryType.TRADE));
            message.AddGroup(marketDataEntryGroupTrade);

            // symbol group:
            var symbolGroup = new MarketDataRequest.NoRelatedSymGroup();
            symbolGroup.Symbol = subscriptionInfo.Symbol.ToQuickFixSymbol();
            message.AddGroup(symbolGroup);

            return message;
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            QuickFix.FIX44.NewOrderSingle orderMessage = new QuickFix.FIX44.NewOrderSingle(
                new ClOrdID(identity),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(HighResolutionDateTime.UtcNow),
                new OrdType(OrdType.LIMIT));



            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.SecurityType = new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT);
            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.MinQty = new MinQty(orderInfo.SizeBaseAbsFillMinimum);
            orderMessage.TimeInForce =
                new TimeInForce(orderInfo.TimeInForce == Contracts.TimeInForce.Day
                                    ? TimeInForce.DAY :
                                _settings.IocApplyTtl ?
                                    'Z' //Good for time
                                    : TimeInForce.IMMEDIATE_OR_CANCEL);

            if (_settings.ApplyMaxHoldTime)
            {
                orderMessage.SetField(new IntField(9020, (int) _settings.MaxHoldTime));
            }

            if (orderInfo.TimeInForce == Contracts.TimeInForce.ImmediateOrKill && _settings.IocApplyTtl)
            {
                orderMessage.SetField(new IntField(9030, _settings.IocTtlMs));
            }

            if (orderInfo.OrderType == OrderType.Quoted)
            {
                orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);
            }

            orderMessage.Price = new Price(orderInfo.RequestedPrice);

            //remove flag for cancelling all orders;
            _cancelAllSent = false;

            return orderMessage;
        }
        #endregion /FIX message composing

        #region FIX message cracking

        public event Action<IEnumerable<Tuple<Common.Symbol, DateTime>>> NewSettlemtDatesInfo;

        public void OnMessage(QuickFix.FIX44.SecurityList m, SessionID s)
        {
            if(this.NewSettlemtDatesInfo == null)
                return;

            this.NewSettlemtDatesInfo(this.ParseSecurityList(m));
        }

        private IEnumerable<Tuple<Common.Symbol, DateTime>> ParseSecurityList(QuickFix.FIX44.SecurityList m)
        {
            int symbolsNum = m.GetInt(Tags.NoRelatedSym);

            for (int symIdx = 1; symIdx <= symbolsNum; symIdx++)
            {
                Group relatedSymGroup = m.GetGroup(symIdx, Tags.NoRelatedSym);
                Common.Symbol symbol;
                if (Common.Symbol.TryParse(relatedSymGroup.GetField(Tags.Symbol), out symbol))
                {
                    DateTime? settlDate = relatedSymGroup.ToNullableDateTime(Tags.SettlDate, true, this.Logger);
                    if(settlDate.HasValue)
                        yield return new Tuple<Symbol, DateTime>(symbol, settlDate.Value);
                }
            }
        }

        public void OnMessage(MarketDataRequestReject m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received market data request reject: {0}", QuickFixUtils.FormatMessage(m));
            SubscriptionChangeInfo info = new SubscriptionChangeInfo(m.MDReqID.getValue(), SubscriptionStatus.Rejected);
            this.SendSubscriptionChange(info);
        }

        //new and delete per each side
        private const int MaxNumOfMDEntries = 4;
        private VenueDealObjectInternal _tickerObject = VenueDealObjectInternal.GetNewUninitializedObject();

        public void OnMessage(MarketDataIncrementalRefresh m, SessionID s)
        {
            int mdEntriesNum = m.GetInt(Tags.NoMDEntries);

            if (mdEntriesNum > MaxNumOfMDEntries)
            {
                this.Logger.Log(LogLevel.Error,
                                "Receiving (and ignoring) Incremental MD refresh with unexpected number of groups ({0}): {1}",
                                mdEntriesNum, QuickFixUtils.FormatMessage(m));
                this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(), SubscriptionStatus.Broken));
                return;
            }

            if (!m.IsSetMDReqID())
            {
                this.Logger.Log(LogLevel.Error, "Receiving (and ignoring) MD refresh without subscription id: {0}",
                                QuickFixUtils.FormatMessage(m));
                return;
            }

            string subscriptionId = m.MDReqID.getValue();
            Common.Symbol symbol = Common.Symbol.NULL;


            decimal bidPrice = 0;
            string bidIdentity = null;
            decimal bidSize = 0;
            bool bidDeletePrevious = false;
            decimal askPrice = 0;
            string askIdentity = null;
            decimal askSize = 0;
            bool askDeletePrevious = false;

            for (int groupIndex = 1; groupIndex <= mdEntriesNum; groupIndex += 1)
            {
                Group noMDEntriesGroup = m.GetGroup(groupIndex, Tags.NoMDEntries);
                bool isBid;
                char sideChar = noMDEntriesGroup.GetChar(Tags.MDEntryType);
                if (sideChar == MDEntryType.BID)
                    isBid = true;
                else if (sideChar == MDEntryType.OFFER)
                    isBid = false;
                else if (sideChar == MDEntryType.TRADE)
                {
                    //tickers

                    decimal price = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    decimal size = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                    Common.Symbol symbolLocal = (Common.Symbol)noMDEntriesGroup.GetField(Tags.Symbol);
                    DateTime sentTime = m.Header.ToDateTime(Tags.SendingTime, false, this.Logger);
                    DateTime? transactionTime = noMDEntriesGroup.ToNullableDateTime(9700, false, this.Logger);
                    string ctpIdentity = noMDEntriesGroup.GetField(Tags.MDEntryID);

                    _tickerObject.OverrideContent(symbolLocal, this.Counterparty, null, price, size,
                        transactionTime, sentTime, HighResolutionDateTime.UtcNow,
                        ctpIdentity.ToAsciiBuffer());

                    RaiseNewVenueDeal(_tickerObject);

                    continue;
                }
                else
                {
                    this.Logger.Log(LogLevel.Error,
                                    "Receiving (and ignoring) Incremental MD refresh with unexpected side ({0}): {1}",
                                    sideChar, m);
                    continue;
                }

                switch (noMDEntriesGroup.GetChar(Tags.MDUpdateAction))
                {
                    case MDUpdateAction.DELETE:
                        if (isBid)
                            bidDeletePrevious = true;
                        else
                            askDeletePrevious = true;
                        break;
                    case MDUpdateAction.NEW:
                    case MDUpdateAction.CHANGE:

                        Common.Symbol symbolLocal = (Common.Symbol) noMDEntriesGroup.GetField(Tags.Symbol);
                        if (symbol == Common.Symbol.NULL)
                            symbol = symbolLocal;
                        else if (symbol != symbolLocal)
                        {
                            this.Logger.Log(LogLevel.Fatal,
                                    "Receiving Incremental MD refresh with multiple updates on multiple symbols, ignoring it: {0}", m);
                            return;
                        }

                        if (isBid)
                        {
                            if (bidIdentity != null)
                            {
                                this.Logger.Log(LogLevel.Error,
                                    "Receiving Incremental MD refresh with multiple updates on same side, ignoring additional updates: {0}", m);
                                continue;
                            }

                            bidPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                            bidSize = bidPrice == 0 ? 0 : noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                            bidIdentity = noMDEntriesGroup.GetField(Tags.MDEntryID);
                        }
                        else
                        {
                            if (askIdentity != null)
                            {
                                this.Logger.Log(LogLevel.Error,
                                    "Receiving Incremental MD refresh with multiple updates on same side, ignoring additional updates: {0}", m);
                                continue;
                            }

                            askPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                            askSize = askPrice == 0 ? 0 : noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                            askIdentity = noMDEntriesGroup.GetField(Tags.MDEntryID);
                        }
                        break;
                    default:
                        this.Logger.Log(LogLevel.Error,
                                    "Receiving (and ignoring) Incremental MD refresh with unexpected update type: {0}", m);
                        break;
                }
            }

            if (bidIdentity == null && askIdentity == null && (bidDeletePrevious || askDeletePrevious))
            {
                //just canceling
                this.SendLastQuoteCancel(subscriptionId);
            }
            else if (bidIdentity != null || askIdentity != null)
            {
                //if one of the side wasn't populated and neither was it deleted - jsut send the otehr side
                // (to prevent nullifying the missing side from book)
                if (bidIdentity == null && !bidDeletePrevious)
                {
                    this.SendNewPrice(this._priceReceivingHelper.GetPriceObject(
                        price: askPrice,
                        sizeBaseAbsInitial: askSize,
                        side: PriceSide.Ask,
                        symbol: symbol,
                        counterparty: Counterparty,
                        counterpartyIdentity: askIdentity,
                        marketDataRecordType: MarketDataRecordType.VenueNewOrder,
                        integratorReceivedTime: HighResolutionDateTime.UtcNow,
                        counterpartySentTime: m.Header.ToDateTime(Tags.SendingTime, false, this.Logger)),
                        subscriptionId);
                    return;
                }

                if (askIdentity == null && !askDeletePrevious)
                {
                    this.SendNewPrice(this._priceReceivingHelper.GetPriceObject(
                        price: bidPrice,
                        sizeBaseAbsInitial: bidSize,
                        side: PriceSide.Bid,
                        symbol: symbol,
                        counterparty: Counterparty,
                        counterpartyIdentity: bidIdentity,
                        marketDataRecordType: MarketDataRecordType.VenueNewOrder,
                        integratorReceivedTime: HighResolutionDateTime.UtcNow,
                        counterpartySentTime: m.Header.ToDateTime(Tags.SendingTime, false, this.Logger)),
                        subscriptionId);
                    return;
                }

                PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSidedIdentity(
                    askPrice: askPrice,
                    askSize: askSize,
                    bidPrice: bidPrice,
                    bidSize: bidSize,
                    symbol: symbol,
                    counterparty: Counterparty,
                    subscriptionIdentity: subscriptionId,
                    counterpartyTimeUtc: m.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                    askIdentity: askIdentity,
                    bidIdentity: bidIdentity,
                    marketDataRecordType: MarketDataRecordType.VenueNewOrder);

                SendNewQuote(PriceReceivingHelper.SharedQuote);
            }
            //ticker or so
        }

        private const int AGGRESSOR_INDICATOR = 9200;

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            string freeTextInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);

            decimal? rejectedAmount = null;
            decimal? initialQuantity = null;

            char fixOrderStatus = m.OrdStatus.getValue();
            char execType = m.ExecType.getValue();

            string orderId = m.ClOrdID.getValue();

            string rawMessage = m.ToString();
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            switch (execType)
            {
                case ExecType.NEW:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] was received and enqeued by other end", orderId);
                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;
                    if (m.IsSetLeavesQty())
                    {
                        initialQuantity = m.LeavesQty.getValue();
                    }
                    break;
                case ExecType.TRADE:
                case ExecType.FILL:
                case ExecType.PARTIAL_FILL:
                    decimal? filledAmount;
                    if (fixOrderStatus == OrdStatus.FILLED)
                    {
                        this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                        orderStatus = IntegratorOrderExternalChange.Filled;
                        filledAmount = m.ToNullableDecimal(Tags.LastQty) ?? m.ToNullableDecimal(Tags.CumQty);
                    }
                    else
                    {
                        if (fixOrderStatus != OrdStatus.PARTIALLY_FILLED)
                        {
                            this.Logger.Log(LogLevel.Fatal,
                                            "Order [{0}] with Trade ExecReport is in unexpected state [{1}]", orderId,
                                            fixOrderStatus);
                        }

                        this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly partially filled", orderId);
                        orderStatus = IntegratorOrderExternalChange.PartiallyFilled;
                        //We intentiaonally don't convert to nullable 
                        //  - as quantity is absolutely required with partial fills
                        filledAmount = m.GetDecimal(Tags.LastQty);
                    }

                    FlowSide? flowSide = null;

                    if (m.IsSetField(AGGRESSOR_INDICATOR))
                    {
                        //not getting this as char to prevent quickfixn conversion exception
                        switch (m.GetField(AGGRESSOR_INDICATOR))
                        {
                            //liquidity added against another taker
                            case "A":
                                flowSide = FlowSide.LiquidityMaker;
                                break;
                            //liquidity removed against another taker
                            case "R":
                            //liquidity removed against LLP
                            case "S":
                                flowSide = FlowSide.LiquidityTaker;
                                break;
                        }
                    }

                    if (flowSide == null)
                    {
                        this.Logger.Log(LogLevel.Error,
                                        "FlowSide (Tag 9200) not specified or invalid in execution message. {0}",
                                        m);
                    }

                    string clientCounterpartyId = null;
                    if (m.IsSetField(9300))
                    {
                        try
                        {
                            clientCounterpartyId = m.GetField(9300);
                            if (clientCounterpartyId.Length > _MAX_COUNTERPARTYCLIENTID_LENGTH)
                            {
                                this.Logger.Log(LogLevel.Fatal,
                                    "ClientCounterpartyId: [{0}] longer then expected max ({1}). Trimming",
                                    clientCounterpartyId, _MAX_COUNTERPARTYCLIENTID_LENGTH);
                                clientCounterpartyId = clientCounterpartyId.Substring(0,
                                    _MAX_COUNTERPARTYCLIENTID_LENGTH);
                            }
                        }
                        catch (Exception e)
                        {
                            this.Logger.LogException(LogLevel.Fatal, "Exception when extracting clientCounterpartyId from exec report " + m, e);
                        }
                    }

                    executionInfo = new ExecutionInfo(
                        filledAmount,
                        m.ToNullableDecimal(Tags.LastPx),
                        m.ToNullableString(Tags.ExecID),
                        m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                        m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                        m.GetIntegratorDealDirection())
                    {
                        FlowSide = flowSide,
                        CounterpartyClientId = clientCounterpartyId
                    };
                    break;
                case ExecType.RESTATED:

                    this.Logger.Log(LogLevel.Fatal,
                                    "FXCM adapter receiving execution report in RESTATED status - ignoring it. Message:{0}{1}{0}parsed:{0}{2}",
                                    Environment.NewLine, m, m.ToFormattedString());
                    return;

                case ExecType.REPLACED:

                    if (m.IsSetLastShares() && m.LastShares.getValue() != 0m)
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received replace ExecutionReport, but last shares is nonzero: [{0}]", rawMessage);
                    }

                    //Cancel original order
                    if (m.IsSetOrigClOrdID())
                    {
                        string origClId = m.OrigClOrdID.getValue();

                        this.Logger.Log(LogLevel.Info, "Order [{0}] was replaced and order [{1}] was created and enqeued by other end", origClId, orderId);

                        this.SendOrderChange(new OrderChangeInfo(origClId, symbol, IntegratorOrderExternalChange.Cancelled,
                                                                 counterpartySendTime, null, null,
                                                                 new RejectionInfo(m.ToNullableString(Tags.Text), null,
                                                                                   RejectionType.OrderCancel)));
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received replace ExecutionReport, but OrigClOrdID is not set: [{0}]", rawMessage);
                    }

                    if (m.IsSetLeavesQty())
                    {
                        initialQuantity = m.LeavesQty.getValue();
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received replace ExecutionReport, but LeavesQty is not set: [{0}]", rawMessage);
                    }

                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;

                    break;
                case ExecType.REJECTED:
                case ExecType.EXPIRED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    freeTextInfo = m.ToNullableString(Tags.Text);

                    if (m.IsSetOrdRejReason())
                    {
                        int ordRejReasonId = m.OrdRejReason.getValue();

                        freeTextInfo = string.Format("Reason code: {0} [{1}]. Description: {2}", ordRejReasonId, MapOrdRejReasonId(ordRejReasonId), freeTextInfo);
                    }

                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    if (m.IsSetCumQty() && m.IsSetOrderQty())
                    {
                        rejectedAmount = m.OrderQty.getValue() - m.CumQty.getValue();
                    }
                    rejectionInfo = new RejectionInfo(freeTextInfo, rejectedAmount, RejectionType.OrderReject);
                    break;
                case ExecType.CANCELLED:
                    freeTextInfo = m.ToNullableString(Tags.Text);

                    if (m.IsSetOrigClOrdID())
                    {
                        orderId = m.OrigClOrdID.getValue();
                        this.Logger.Log(LogLevel.Info, "Cancel Execution report for order: {0} did have OrigClOrdID populated (as: {1}) so changing orderId to: {1}",
                            m.ClOrdID.getValue(), orderId);
                    }

                    //The only difference between requeste cancel and IoC Cancel is the OrigOrderId tag
                    //However cancels after CancellAll doesn't populate this tag
                    if (m.IsSetOrigClOrdID() || this._cancelAllSent)
                    {
                        this.Logger.Log(LogLevel.Info, "Order [{0}] Cancelled", orderId);
                        orderStatus = IntegratorOrderExternalChange.Cancelled;
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Info, "IoC Order [{0}] Rejected", orderId);
                        orderStatus = IntegratorOrderExternalChange.Rejected;
                        freeTextInfo += "<Integrator Supplied Reason: IoC miss>";
                    }

                    if (m.IsSetCumQty() && m.IsSetOrderQty())
                    {
                        rejectedAmount = m.OrderQty.getValue() - m.CumQty.getValue();
                    }
                    rejectionInfo = new RejectionInfo(freeTextInfo, rejectedAmount, orderStatus == IntegratorOrderExternalChange.Cancelled ? RejectionType.OrderCancel : RejectionType.OrderReject);
                    break;
                default:
                    this.Logger.Log(LogLevel.Fatal, "Order [{0}] received unsupported ExecType report: [{1}]", orderId, execType);
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                         executionInfo, rejectionInfo, initialQuantity));
        }

        public void OnMessage(OrderCancelReject m, SessionID s)
        {
            IntegratorOrderExternalChange orderStatus = IntegratorOrderExternalChange.CancelRejected;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);

            string cancelOperationId = m.ClOrdID.getValue();
            string orderId = m.OrigClOrdID.getValue();
            string freeTextInfo = m.ToNullableString(Tags.Text);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            if (m.IsSetCxlRejReason())
            {
                int cxlRejReasonId = m.CxlRejReason.getValue();

                freeTextInfo = string.Format("Reason code: {0} [{1}]. Description: {2}", cxlRejReasonId, MapCxlRejReasonId(cxlRejReasonId), freeTextInfo);
            }

            //FXCM specific code - orig cl id is in text
            string origOrderIdFromTxt = GetSubstring(freeTextInfo, _orderIdPrefix, _orderIdSuffix);
            if (!string.IsNullOrEmpty(origOrderIdFromTxt))
            {
                this.Logger.Log(LogLevel.Info, "Found orig order id [{0}] in comment - so using it and ignoring [{1}]",
                    origOrderIdFromTxt, orderId);
                orderId = origOrderIdFromTxt;
            }

            this.Logger.Log(LogLevel.Info, "Request [{0}] to cancell order [{1}] was REJECTED (\"{2}\")", cancelOperationId, orderId, freeTextInfo);


            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     null, null, freeTextInfo));

            //inform upperlayers about rejection of a cancel request (or cancel replace request)
            //this is the place where we flip Cancel/Replace reject into regular reject - so indicate this type of reject
            this.SendOrderChange(new OrderChangeInfo(cancelOperationId, symbol, IntegratorOrderExternalChange.Rejected,
                                                     counterpartySendTime, null, null,
                                                     new RejectionInfo(freeTextInfo, null, RejectionType.CancelReplaceReject)));

            //this way we can get replace request orig client id
            //if (m.CxlRejResponseTo.getValue() == CxlRejResponseTo.ORDER_CANCEL_REPLACE_REQUEST)
            //{
            //    m.OrigClOrdID.getValue()
            //} 
        }

        private static string _orderIdPrefix = "Unable to locate order by ClOrdID <";
        private static string _orderIdSuffix = ">";

        private static string GetSubstring(string sourceString, string prefix, string suffix)
        {
            if (string.IsNullOrEmpty(sourceString) || string.IsNullOrEmpty(prefix) || string.IsNullOrEmpty(suffix))
                return null;

            int startIdx = sourceString.IndexOf(prefix);
            if (startIdx >= 0)
            {
                startIdx += prefix.Length;
                int endIdx = sourceString.IndexOf(suffix, startIdx);
                if (endIdx > startIdx)
                {
                    return sourceString.Substring(startIdx, endIdx - startIdx);
                }
            }

            return null;
        }

        private string MapCxlRejReasonId(int cxlRejReasonId)
        {
            //public const int TOO_LATE_TO_CANCEL = 0;
            //public const int UNKNOWN_ORDER = 1;
            //public const int BROKER = 2;
            //public const int ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS = 3;
            //public const int UNABLE_TO_PROCESS_ORDER_MASS_CANCEL_REQUEST = 4;
            //public const int ORIGORDMODTIME = 5;
            //public const int DUPLICATE_CLORDID = 6;
            //public const int OTHER = 99;

            switch (cxlRejReasonId)
            {
                case 0:
                    return "Too Late";
                case 1:
                    return "Unknown Order";
                case 3:
                    return "Order Already in Pending Cancel or Pending Replace Status";
                case 6:
                    return "Duplicate ClOrdID Received";
                case 99:
                    return "Other";
                default:
                    return string.Format("<Unknown CxlRejReasonId!: {0}>", cxlRejReasonId);
            }
        }

        private string MapOrdRejReasonId(int ordRejReasonId)
        {
            //public const int UNKNOWN_SYMBOL = 1;
            //public const int EXCHANGE_CLOSED = 2;
            //public const int TOO_LATE_TO_ENTER = 4;
            //public const int UNKNOWN_ORDER = 5;
            //public const int DUPLICATE_ORDER = 6;
            //99 - other

            switch (ordRejReasonId)
            {
                case 1:
                    return "Unknown Symbol";
                case 2:
                    return "Exchange Closed";
                case 4:
                    return "Too late to enter";
                case 5:
                    return "Unknown Order";
                case 6:
                    return "Duplicate Order";
                case 99:
                    return "Other";
                default:
                    return string.Format("<Unknown OrdRejReasonId!: {0}>", ordRejReasonId);
            }
        }

        #endregion /FIX message cracking

        //public bool SendCancelOrderMessage(OrderRequestInfo originalOrderInfo, string originalIdentity, string remotePlatformOriginalOrderId,
        //                                   string cancellationOperationIdentity, out string fixMessageRaw)
        //{
        //    var cancelOrderMessage = new OrderCancelRequest(
        //        new OrigClOrdID(originalIdentity),
        //        new ClOrdID(cancellationOperationIdentity),
        //        originalOrderInfo.Symbol.ToQuickFixSymbol(),
        //        originalOrderInfo.Side.ToQuickFixSide(),
        //        new TransactTime(HighResolutionDateTime.UtcNow));

        //    //cancelOrderMessage.OrderQty = new OrderQty(originalOrderInfo.SizeBaseAbsInitial);

        //    bool successfulySent = false;
        //    try
        //    {
        //        successfulySent = QuickFix.Session.SendToTarget(cancelOrderMessage, this.SessionId);
        //    }
        //    catch (Exception e)
        //    {
        //        this.Logger.LogException(LogLevel.Error, e, "Adapter for {0} failed to send Cancel message for order {1}", this.Counterparty, originalIdentity);
        //    }

        //    if (successfulySent)
        //    {
        //        fixMessageRaw = cancelOrderMessage.ToString();
        //        return true;
        //    }
        //    else
        //    {
        //        fixMessageRaw = string.Empty;
        //        return false;
        //    }
        //}

        protected override Message CreateCancelMessage(OrderRequestInfo originalOrderInfo, string originalIdentity,
            string cancellationOperationIdentity)
        {
            return new OrderCancelRequest(
                new OrigClOrdID(originalIdentity),
                new ClOrdID(cancellationOperationIdentity),
                originalOrderInfo.Symbol.ToQuickFixSymbol(),
                originalOrderInfo.Side.ToQuickFixSide(),
                new TransactTime(HighResolutionDateTime.UtcNow));
        }

        //public bool CancelAllSupported { get { return true; } }

        private bool _cancelAllSent = false;
        //public bool SendCancelAllOrdersMessage(string cancellationOperationIdentity, out string fixMessageRaw)
        //{
        //    var cancelAllMessage = new OrderCancelRequest()
        //    {
        //        OrigClOrdID = new OrigClOrdID("OPEN_ORDER"),
        //        ClOrdID = new ClOrdID(cancellationOperationIdentity),
        //        TransactTime = new TransactTime(HighResolutionDateTime.UtcNow)
        //    };

        //    fixMessageRaw = cancelAllMessage.ToString();

        //    _cancelAllSent = true;
        //    return QuickFix.Session.SendToTarget(cancelAllMessage, this.SessionId);
        //}

        //public bool SendCancelReplaceOrder(OrderRequestInfo originalOrderInfo, string originalIdentity, string remotePlatformOriginalOrderId,
        //                                   OrderRequestInfo replacingOrderInfo, string replacingOrderIdentity,
        //                                   out string outgoingFixMessageRaw)
        //{
        //    var cancelReplaceMessage = new OrderCancelReplaceRequest()
        //        {
        //            Symbol = replacingOrderInfo.Symbol.ToQuickFixSymbol(),
        //            SecurityType = new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT),
        //            OrigClOrdID = new OrigClOrdID(originalIdentity),
        //            ClOrdID = new ClOrdID(replacingOrderIdentity),
        //            Side = replacingOrderInfo.Side.ToQuickFixSide(),
        //            OrderQty = new OrderQty(replacingOrderInfo.SizeBaseAbsInitial),
        //            OrdType = new OrdType(OrdType.LIMIT),
        //            TimeInForce = new TimeInForce(replacingOrderInfo.TimeInForce == Contracts.TimeInForce.Day
        //                                              ? TimeInForce.DAY
        //                                              : TimeInForce.IMMEDIATE_OR_CANCEL),
        //            Price = new Price(replacingOrderInfo.RequestedPrice),
        //            TransactTime = new TransactTime(HighResolutionDateTime.UtcNow),
        //            MinQty = new MinQty(replacingOrderInfo.SizeBaseAbsFillMinimum)
        //        };

        //    outgoingFixMessageRaw = cancelReplaceMessage.ToString();

        //    return QuickFix.Session.SendToTarget(cancelReplaceMessage, this.SessionId);
        //}

        protected override Message CreateCancelReplaceMessage(OrderRequestInfo originalOrderInfo, string originalIdentity,
            OrderRequestInfo replacingOrderInfo, string replacingOrderIdentity)
        {
            return new OrderCancelReplaceRequest()
            {
                Symbol = replacingOrderInfo.Symbol.ToQuickFixSymbol(),
                SecurityType = new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT),
                OrigClOrdID = new OrigClOrdID(originalIdentity),
                ClOrdID = new ClOrdID(replacingOrderIdentity),
                Side = replacingOrderInfo.Side.ToQuickFixSide(),
                OrderQty = new OrderQty(replacingOrderInfo.SizeBaseAbsInitial),
                OrdType = new OrdType(OrdType.LIMIT),
                TimeInForce = new TimeInForce(replacingOrderInfo.TimeInForce == Contracts.TimeInForce.Day
                                                  ? TimeInForce.DAY
                                                  : TimeInForce.IMMEDIATE_OR_CANCEL),
                Price = new Price(replacingOrderInfo.RequestedPrice),
                TransactTime = new TransactTime(HighResolutionDateTime.UtcNow),
                MinQty = new MinQty(replacingOrderInfo.SizeBaseAbsFillMinimum)
            };
        }

        //public bool OrderStatusReportingSupported { get { return false; } }

        //public bool SendOrderStatusRequest(OrderRequestInfo originalOrderInfo, string originalIdentity, string operationIdentity)
        //{
        //    //Not supporting for now
        //    throw new NotImplementedException();

        //    //var orderStatusRequestMessage = new OrderStatusRequest();

        //    //orderStatusRequestMessage.ClOrdID = new ClOrdID(originalIdentity);

        //    //orderStatusRequestMessage.Symbol = originalOrderInfo.Symbol.ToQuickFixSymbol();
        //    //orderStatusRequestMessage.Side = new Side(originalOrderInfo.Side.ToQuickFixSide());

        //    //try
        //    //{
        //    //    return QuickFix.Session.SendToTarget(orderStatusRequestMessage, this.SessionId);
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //    this.Logger.LogException(LogLevel.Fatal, e, "Adapter for {0} failed to send OrderStatusRequest message for order {1}", this.Counterparty, originalIdentity);
        //    //}

        //    //return false;
        //}

        //public bool DealRejectionSupported { get { return false; } }

        //public bool TryRejectDeal(AutoRejectableDeal rejectableDeal,
        //    AutoRejectableDeal.RejectableTakerDealRejectionReason rejectionReason)
        //{
        //    throw new NotImplementedException();
        //}

        //public bool TryAcceptDeal(AutoRejectableDeal rejectableDeal)
        //{
        //    throw new NotImplementedException();
        //}



        private sealed class FXCMExecReportCracker : VenueExecReportCracker
        {
            public FXCMExecReportCracker(IOrdersParentChannel parentChannel, IFxcmChannel fxcmChannel, ILogger logger)
                : base(parentChannel, logger, false)
            {
                this._fxcmChannel = fxcmChannel;
            }

            private IFxcmChannel _fxcmChannel;
            private char _aggressorIndicator;
            private string _counterpartyClientId;

            protected override bool IsIoCMiss
            {
                get 
                { 
                    //The only difference between requeste cancel and IoC Cancel is the OrigOrderId tag
                    //However cancels after CancellAll doesn't populate this tag
                    return string.IsNullOrEmpty(this._origOrderId) && !this._fxcmChannel.CancelAllSent;
                }
            }

            protected override void ConsumeNonstandardFixTag(int tagId, BufferSegmentBase tagValueSegment)
            {
                switch (tagId)
                {
                    case 9200:
                        OnTag9200(tagValueSegment);
                        break;
                    case 9300:
                        OnTag9300(tagValueSegment);
                        break;
                    case 103:
                        OnTag103(tagValueSegment);
                        break;
                }
            }

            protected override void ResetState()
            {
                this._aggressorIndicator = char.MaxValue;
                this._orderRejReason = null;
                this._counterpartyClientId = null;
            }

            protected override FlowSide? GetFlowSide()
            {
                FlowSide? flowSide = null;

                //not getting this as char to prevent quickfixn conversion exception
                switch (this._aggressorIndicator)
                {
                    //liquidity added against another taker
                    case 'A':
                        flowSide = FlowSide.LiquidityMaker;
                        break;
                    //liquidity removed against another taker
                    case 'R':
                    //liquidity removed against LLP
                    case 'S':
                        flowSide = FlowSide.LiquidityTaker;
                        break;
                    //intentionaly no default - if tag is missing
                }


                if (flowSide == null)
                {
                    this._logger.Log(LogLevel.Error, "FlowSide (Tag 9200) not specified or invalid in execution message.");
                }

                return flowSide;
            }

            protected override ExecutionReportStatus GetTranslatedExecutionReportStatus(char externalExecType)
            {
                switch (externalExecType)
                {
                    case ExecType.PENDING_NEW:
                        return this._parentChannel.IsDivertedViaFluent ? ExecutionReportStatus.FluentNew : ExecutionReportStatus.New;
                    case ExecType.NEW:
                        return ExecutionReportStatus.New;
                    case ExecType.TRADE:
                    case ExecType.FILL:
                    case ExecType.PARTIAL_FILL:
                        return ExecutionReportStatus.Trade;
                    case ExecType.REPLACE:
                        return ExecutionReportStatus.Replace;
                    case ExecType.REJECTED:
                    case ExecType.EXPIRED:
                        return ExecutionReportStatus.Reject;
                    case ExecType.CANCELLED:
                        return ExecutionReportStatus.Cancel;
                    case ExecType.RESTATED:
                        this._logger.Log(LogLevel.Fatal, "FXCM adapter receiving execution report in RESTATED status - ignoring it.");
                        return ExecutionReportStatus.ToBeIgnored;
                    default:
                        return ExecutionReportStatus.Unrecognized;
                }
            }

            protected override void AppendInfoAfterTradeOrReject(AllocationInfo allocationInfo)
            {
                if (!string.IsNullOrEmpty(this._counterpartyClientId))
                    allocationInfo.CounterpartyClientId = this._counterpartyClientId;
            }

            //9200
            private void OnTag9200(BufferSegmentBase bufferSegment)
            {
                this._aggressorIndicator = (char)bufferSegment[0];
            }

            //9300
            private void OnTag9300(BufferSegmentBase bufferSegment)
            {
                this._counterpartyClientId = bufferSegment.GetContentAsAsciiString();

                if (this._counterpartyClientId.Length > _MAX_COUNTERPARTYCLIENTID_LENGTH)
                {
                    this._logger.Log(LogLevel.Fatal,
                        "ClientCounterpartyId: [{0}] longer then expected max ({1}). Trimming",
                        this._counterpartyClientId, _MAX_COUNTERPARTYCLIENTID_LENGTH);
                    this._counterpartyClientId = this._counterpartyClientId.Substring(0,
                        _MAX_COUNTERPARTYCLIENTID_LENGTH);
                }
            }

            //OrdRejReason = 103
            private void OnTag103(BufferSegmentBase bufferSegment)
            {
                this._orderRejReason = bufferSegment.ToInt();
            }
        }


        private class FXCMSecurityInfoCracker: IFixMessageCracker
        {
            private const byte _recognizedType = (byte)'y';
            private FIXChannel_FCM _parentChannel;

            public FXCMSecurityInfoCracker(FIXChannel_FCM parentChannel)
            {
                this._parentChannel = parentChannel;
            }

            public byte RecognizedMessageType
            {
                get { return _recognizedType; }
            }

            public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
            {
                switch (tagId)
                {
                    case 64:
                        OnTag64(tagValueSegment);
                        break;
                    case 55:
                        OnTag55(tagValueSegment);
                        break;
                }
            }

            public bool TryRecoverFromParsingErrors
            {
                get { return false; }
            }

            public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
            {
                _lastSymbolRecognized = false;
                _lastSymbol = Common.Symbol.NULL;
            }

            public void MessageParsingDone()
            {
                if (this._parentChannel.NewSettlemtDatesInfo != null)
                    this._parentChannel.NewSettlemtDatesInfo(_settlDates);

                _settlDates.Clear();
            }


            private bool _lastSymbolRecognized;
            private Symbol _lastSymbol;
            private List<Tuple<Symbol, DateTime>> _settlDates = new List<Tuple<Symbol, DateTime>>(); 

            //Symbol = 55
            private void OnTag55(BufferSegmentBase bufferSegment)
            {
                _lastSymbolRecognized = bufferSegment.TryConvertToSymbol(out _lastSymbol);
            }

            //SettlDate (YYYYMMDD) = 64
            private void OnTag64(BufferSegmentBase bufferSegment)
            {
                if (_lastSymbolRecognized)
                {
                    _settlDates.Add(new Tuple<Symbol, DateTime>(_lastSymbol, bufferSegment.ToDateOnly()));
                    _lastSymbolRecognized = false;
                }
            }
        }

        private class FXCMQuoteCracker : IFixMessageCracker
        {
            private IMarketDataPerentChannel _parentChannel;
            private FIXChannel_FCM _fxcmChannel;
            private ILogger _logger;
            private DateTime _currentMessageReceived;
            private QuoteObject _sharedQuote;
            private VenueDealObjectInternal _tickerObject = VenueDealObjectInternal.GetNewUninitializedObject();

            public FXCMQuoteCracker(IMarketDataPerentChannel parentChannel, FIXChannel_FCM fxcmChannel, ILogger logger)
            {
                this._parentChannel = parentChannel;
                this._fxcmChannel = fxcmChannel;
                this._logger = logger;
                this._sharedQuote = parentChannel.PriceReceivingHelper.SharedQuote;
            }

            private const byte _recognizedType = (byte)'X';

            public byte RecognizedMessageType
            {
                get { return _recognizedType; }
            }

            public bool TryRecoverFromParsingErrors
            {
                get { return false; }
            }

            public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
            {
                switch (tagId)
                {
                    case 262:
                        OnTag262(tagValueSegment);
                        break;
                    case 268:
                        OnTag268(tagValueSegment);
                        break;
                    case 269:
                        OnTag269(tagValueSegment);
                        break;
                    case 270:
                        OnTag270(tagValueSegment);
                        break;
                    case 271:
                        OnTag271(tagValueSegment);
                        break;
                    case 52:
                        OnTag52(tagValueSegment);
                        break;

                    case 9700:
                        OnTag9700(tagValueSegment);
                        break;
                    case 278:
                        OnTag278(tagValueSegment);
                        break;
                    case 279:
                        OnTag279(tagValueSegment);
                        break;
                    case 55:
                        OnTag55(tagValueSegment);
                        break;
                }
            }

            private Common.Symbol _symbol = Common.Symbol.NULL;
            private string _subscriptionIdentity = string.Empty;
            private AtomicDecimal _bidPrice = 0;
            private decimal _bidSize = 0;
            private AtomicDecimal _askPrice = 0;
            private decimal _askSize = 0;
            private DateTime _sendingTime = DateTime.MinValue;
            private bool? _isBid = null;
            //private bool _hasBid = false;
            //private bool _hasAsk = false;
            //private int _tierId = 0;

            private bool _isTrade;
            private decimal _tradeSize;
            private decimal _tradePrice;
            private DateTime? _transactionTime;

            private readonly byte[] _counterpartyBidIdentity = new byte[PriceObject.MAX_CTP_IDENTITY_LENGTH];
            private readonly byte[] _counterpartyAskIdentity = new byte[PriceObject.MAX_CTP_IDENTITY_LENGTH];
            private readonly byte[] _counterpartyTradeIdentity = new byte[PriceObject.MAX_CTP_IDENTITY_LENGTH];
            private bool _deletePrevious;
            private bool _deletePreviousBid;
            private bool _deletePreviousAsk;

            public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
            {
                this._currentMessageReceived = currentMessageReceivedTimeUtc;
                this._symbol = Common.Symbol.NULL;
                this._subscriptionIdentity = null;
                this._sendingTime = DateTime.MinValue;
                //this._tierId = 0;
                this.ResetTier();
            }

            private void ResetTier()
            {
                this._bidPrice = 0;
                this._askPrice = 0;
                this._bidSize = 0;
                this._askSize = 0;
                this._isBid = null;
                //this._hasBid = false;
                //this._hasAsk = false;
                this._isTrade = false;
                this._tradePrice = 0;
                this._tradeSize = 0;
                this._transactionTime = null;
                this._counterpartyAskIdentity[0] = 0;
                this._counterpartyBidIdentity[0] = 0;
                this._counterpartyTradeIdentity[0] = 0;
                this._deletePrevious = false;
                this._deletePreviousBid = false;
                this._deletePreviousAsk = false;
            }

            public void MessageParsingDone()
            {
                if (this._symbol == Common.Symbol.NULL || this._subscriptionIdentity == null)
                    throw new HaltingTagException("Incomplete message - missing tag", this._symbol == Common.Symbol.NULL ? 55 : 262);

                DateTime receivedAndParsed = this._currentMessageReceived;

                if (this._isTrade)
                {
                    _tickerObject.OverrideContent(this._symbol, this._parentChannel.Counterparty,
                        null, this._tradePrice, this._tradeSize,
                        this._transactionTime, this._sendingTime, receivedAndParsed, this._counterpartyTradeIdentity);

                    this._fxcmChannel.RaiseNewVenueDeal(_tickerObject);
                }

                //This is for theoretical case of one message containing new and then delete (of 2 different) 
                //  - then we still want the new
                if (this._deletePreviousBid && this._bidSize > 0)
                    this._deletePreviousBid = false;
                if (this._deletePreviousAsk && this._askSize > 0)
                    this._deletePreviousAsk = false;

                if (this._counterpartyBidIdentity[0] == 0 && this._counterpartyAskIdentity[0] == 0 &&
                    (this._deletePreviousBid || this._deletePreviousAsk))
                {
                    //just canceling
                    this._parentChannel.SendLastQuoteCancel(this._subscriptionIdentity);
                }
                else if (this._counterpartyBidIdentity[0] != 0 || this._counterpartyAskIdentity[0] != 0)
                {
                    //if one of the side wasn't populated and neither was it deleted - jsut send the otehr side
                    // (to prevent nullifying the missing side from book)
                    if (this._counterpartyBidIdentity[0] == 0 && !this._deletePreviousBid)
                    {
                        this._parentChannel.SendNewPrice(this._parentChannel.PriceReceivingHelper.GetPriceObject(
                            priceConverted: this._askPrice,
                            sizeBaseAbsInitial: this._askSize,
                            side: PriceSide.Ask,
                            symbol: this._symbol,
                            counterparty: this._parentChannel.Counterparty,
                            counterpartyIdentity: this._counterpartyAskIdentity,
                            marketDataRecordType: MarketDataRecordType.VenueNewOrder,
                            integratorReceivedTime: receivedAndParsed,
                            counterpartySentTime: this._sendingTime),
                            this._subscriptionIdentity);
                        return;
                    }

                    if (this._counterpartyAskIdentity[0] == 0 && !this._deletePreviousAsk)
                    {
                        this._parentChannel.SendNewPrice(this._parentChannel.PriceReceivingHelper.GetPriceObject(
                            priceConverted: this._bidPrice,
                            sizeBaseAbsInitial: this._bidSize,
                            side: PriceSide.Bid,
                            symbol: this._symbol,
                            counterparty: this._parentChannel.Counterparty,
                            counterpartyIdentity: this._counterpartyBidIdentity,
                            marketDataRecordType: MarketDataRecordType.VenueNewOrder,
                            integratorReceivedTime: receivedAndParsed,
                            counterpartySentTime: this._sendingTime),
                            this._subscriptionIdentity);
                        return;
                    }

                    _sharedQuote.OverrideContent(
                        askPrice: this._askPrice,
                        askSize: this._askSize,
                        bidPrice: this._bidPrice,
                        bidSize: this._bidSize,
                        symbol: this._symbol,
                        counterparty: this._parentChannel.Counterparty,
                        subscriptionIdentity: this._subscriptionIdentity,
                        counterpartyTimeUtc: this._sendingTime,
                        marketDataRecordType: MarketDataRecordType.VenueNewOrder,
                        intagratorTimeUtc: receivedAndParsed,
                        counterpartyAskIdentity: this._counterpartyAskIdentity,
                        counterpartyBidIdentity: this._counterpartyBidIdentity);

                    this._parentChannel.SendNewQuote(_sharedQuote);
                }
            }


            private void OnTag262(BufferSegmentBase bufferSegment)
            {
                this._subscriptionIdentity = this._parentChannel.GetSubscriptionIdentity(bufferSegment);
            }

            private void OnTag268(BufferSegmentBase bufferSegment)
            {
                if (bufferSegment.ToInt() > 4)
                {
                    string error = string.Format("Received quote with unexpected number of layers: {0}",
                        bufferSegment.GetContentAsAsciiString());
                    this._logger.Log(LogLevel.Error, error);
                    throw new HaltingTagException(error, 268);
                }
            }

            private void OnTag269(BufferSegmentBase bufferSegment)
            {
                //if deletePrevious (279=2) is active, then mark current side as being deleted
                //if it is not active, then clear the flag (so that we do not delete also the new price)

                if (bufferSegment[0] == '0')
                {
                    this._isBid = true;
                    this._deletePreviousBid = this._deletePrevious;
                }
                else if (bufferSegment[0] == '1')
                {
                    this._isBid = false;
                    this._deletePreviousAsk = this._deletePrevious;
                }
                else
                    this._isBid = null;



                    //trade
                 if (bufferSegment[0] == '2')
                    _isTrade = true;
            }

            private void OnTag270(BufferSegmentBase bufferSegment)
            {
                //if we are deleting previous price, then just pass through
                if (this._deletePrevious)
                    return;

                if (this._isBid.HasValue)
                {
                    AtomicDecimal pxValue = bufferSegment.ToAtomicDecimal();
                    if (this._isBid.Value)
                        this._bidPrice = pxValue;
                    else
                        this._askPrice = pxValue;
                }
                else if(_isTrade)
                    this._tradePrice = bufferSegment.ToDecimal();

            }

            private void OnTag271(BufferSegmentBase bufferSegment)
            {
                //if we are deleting previous price, then just pass through
                if (this._deletePrevious)
                    return;

                if (this._isBid.HasValue)
                {
                    decimal sizeValue = bufferSegment.ToDecimal();
                    if (this._isBid.Value)
                        this._bidSize = sizeValue;
                    else
                        this._askSize = sizeValue;
                }
                else if (_isTrade)
                    this._tradeSize = bufferSegment.ToDecimal();
            }

            //9700
            private void OnTag9700(BufferSegmentBase bufferSegment)
            {
                if (_isTrade)
                    this._transactionTime = bufferSegment.ToDateTime();
            }

            //MDEntryID = 278
            private void OnTag278(BufferSegmentBase bufferSegment)
            {
                //if we are deleting previous price, then just pass through
                if (this._deletePrevious)
                    return;

                if (this._isBid.HasValue || _isTrade)
                {
                    if (this._isBid.HasValue)
                    {
                        if (this._isBid.Value)
                             bufferSegment.CopyToClearingRest(this._counterpartyBidIdentity);
                        else
                            bufferSegment.CopyToClearingRest(this._counterpartyAskIdentity);
                    }
                    else if (this._isTrade)
                        bufferSegment.CopyToClearingRest(this._counterpartyTradeIdentity);
                }
            }

            //MDUpdateAction = 279
            private void OnTag279(BufferSegmentBase bufferSegment)
            {
                switch ((char) bufferSegment[0])
                {
                    case MDUpdateAction.DELETE:
                        this._deletePrevious = true;
                        break;
                    case MDUpdateAction.NEW:
                    case MDUpdateAction.CHANGE:
                        this._deletePrevious = false;
                        break;
                    default:
                        throw new NonHaltingTagException("Receiving (and ignoring) Incremental MD refresh with unexpected update type", 279);
                }
            }

            //Symbol = 55
            private void OnTag55(BufferSegmentBase bufferSegment)
            {
                if (this._symbol == Common.Symbol.NULL)
                    this._symbol = bufferSegment.ToSymbol();
                else if(this._symbol != bufferSegment.ToSymbol())
                    throw new HaltingTagException("Receiving MD update for multiple symbols in one message", 55);
            }

            private void OnTag52(BufferSegmentBase bufferSegment)
            {
                this._sendingTime = bufferSegment.ToDateTime();
            }
        }
    }
}
