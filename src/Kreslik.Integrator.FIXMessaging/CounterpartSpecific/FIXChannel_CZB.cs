﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.FIX44;
using QuickFix.Fields;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal sealed class FIXChannel_CZB : FIXChannelMessagingBase<FIXChannel_CZB>
    {
        public FIXChannel_CZB(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            FIXChannel_CZBSettings settings, bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineToWaitForSessionStatusHandshake();

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new BankMarketDataSnapshotFullRefreshCracker(this, logger));
            }
            else
                this.SetQuickMessageParsers(isMdChannel, new SimpleBankWithNewStatusExecReportsCracker(this, logger));
        }

        private FIXChannel_CZBSettings _settings;

        protected override void SendSessionStatusRequest()
        {
            if (!this.IsDivertedViaFluent)
            {
                var tssr =
                    new TradingSessionStatusRequest(new TradSesReqID(Guid.NewGuid().ToString()),
                        new SubscriptionRequestType(SubscriptionRequestType.SNAPSHOT));
                tssr.TradingSessionID = new TradingSessionID(DateTime.UtcNow.ToString("yyyyMMdd"));

                Session.SendToTarget(tssr, this.SessionId);
            }
        }

        //protected override bool SendsQuotedOrdersAsLimit
        //{
        //    get { return true; }
        //}

        #region FIX message composing

        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo,
            string subscriptionIdentity)
        {
            QuickFix.Message subscriptionMessage = this.GetMarketDataSubscriptionMsg(subscriptionInfo,
                subscriptionIdentity, true);
            return Session.SendToTarget(subscriptionMessage, this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo,
            string subscriptionIdentity)
        {
            QuickFix.Message subscriptionMessage = this.GetMarketDataSubscriptionMsg(subscriptionInfo,
                subscriptionIdentity, false);
            return Session.SendToTarget(subscriptionMessage, this.SessionId);
        }

        private QuickFix.Message GetMarketDataSubscriptionMsg(SubscriptionRequestInfo subscriptionInfo,
            string subscriptionIdentity, bool subscribe)
        {
            var mdReqID = new MDReqID(subscriptionIdentity);
            var subType =
                new SubscriptionRequestType(subscribe
                    ? SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES
                    : SubscriptionRequestType.DISABLE_PREVIOUS);
            var marketDepth = new MarketDepth(1);

            var message = new MarketDataRequest(mdReqID, subType, marketDepth);

            message.MDUpdateType = new MDUpdateType(MDUpdateType.FULL_REFRESH);
            message.SetField(new OrderQty(subscriptionInfo.Quantity));

            // bid:
            var marketDataEntryGroupBid = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupBid.Set(new MDEntryType(MDEntryType.BID));
            message.AddGroup(marketDataEntryGroupBid);

            // offer:
            var marketDataEntryGroupOffer = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupOffer.Set(new MDEntryType(MDEntryType.OFFER));
            message.AddGroup(marketDataEntryGroupOffer);

            // symbol group:
            var symbolGroup = new MarketDataRequest.NoRelatedSymGroup();
            symbolGroup.Set(subscriptionInfo.Symbol.ToQuickFixSymbol());
            message.AddGroup(symbolGroup);

            return message;
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            var orderMessage = new QuickFix.FIX44.NewOrderSingle(
                new ClOrdID(identity),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                //new OrdType(OrdType.LIMIT));
                new OrdType(OrdType.PREVIOUSLY_QUOTED));

            this.AppendAdditionalTagsToOrderMessage(orderMessage, orderInfo);

            orderMessage.Account = new Account(this._settings.Account);
            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.TimeInForce = new TimeInForce(TimeInForce.FILL_OR_KILL);
            orderMessage.Price = new Price(orderInfo.RequestedPrice);

            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);

            return orderMessage;
        }

        protected override void AppendAdditionalTagsToOrderMessage(NewOrderSingle orderMessage, OrderRequestInfo orderInfo)
        {
            //This is needed by Commerz bank
            //orderMessage.SetField(new MDReqID(string.Format("{0}_{1}_001", this.Counterparty, orderInfo.Symbol.ShortName)));
        }

        #endregion /FIX message composing


        #region FIX message cracking

        public void OnMessage(TradingSessionStatus m, SessionID s)
        {
            if (m.TradSesStatus.getValue() == TradSesStatus.OPEN)
            {
                this.SessionStatusReceived();
            }
            else
            {
                this.Logger.Log(LogLevel.Error,
                    "Receiving unexpected TradSesStatus of TradingSessionStatus message: [{0}].",
                    m.TradSesStatus.getValue());
            }
        }

        public void OnMessage(MarketDataRequestReject m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received market data request reject: {0}", QuickFixUtils.FormatMessage(m));
            SubscriptionChangeInfo info = new SubscriptionChangeInfo(m.MDReqID.getValue(), SubscriptionStatus.Rejected);
            this.SendSubscriptionChange(info);
        }

        public void OnMessage(MarketDataSnapshotFullRefresh m, SessionID s)
        {
            QuoteObject quote = this.CreateQuoteFromMessage(m);

            if (quote == null)
            {
                this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(), SubscriptionStatus.Broken));
                return;
            }

            if (object.ReferenceEquals(quote, QuoteObject.EMPTY_QUOTE))
            {
                //if (!this.Logger.IsLowDiskSpaceConstraintLoggingOn)
                //    this.Logger.Log(LogLevel.Info,
                //        "Received indicative quote (ignoring it and cancelling the previous): {0}",
                //        QuickFixUtils.FormatMessage(m));
                this.SendLastQuoteCancel(m.MDReqID.getValue());
                return;
            }

            SendNewQuote(quote);
        }

        private const int MaxNumOfMDEntries = 2;

        private QuoteObject CreateQuoteFromMessage(MarketDataSnapshotFullRefresh quoteMessage)
        {
            Kreslik.Integrator.Common.Symbol symbol = (Kreslik.Integrator.Common.Symbol) quoteMessage.Symbol.getValue();
            string subscriptionIdentity = quoteMessage.MDReqID.getValue();

            int mdEntriesNum = quoteMessage.GetInt(Tags.NoMDEntries);

            if (mdEntriesNum > MaxNumOfMDEntries)
            {
                this.Logger.Log(LogLevel.Error, "Received quote with unexpected number of layers: {0}",
                    mdEntriesNum);
                return null;
            }

            decimal bidPrice = 0;
            string bidIdentity = null;
            decimal bidSize = 0;
            decimal askPrice = 0;
            string askIdentity = null;
            decimal askSize = 0;

            for (int groupIndex = 1; groupIndex <= mdEntriesNum; groupIndex += 1)
            {
                Group noMDEntriesGroup = quoteMessage.GetGroup(groupIndex, Tags.NoMDEntries);
                if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.BID)
                {
                    bidPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    bidIdentity = noMDEntriesGroup.GetField(Tags.QuoteEntryID);
                    if (this.GetIsIndicative(noMDEntriesGroup))
                    {
                        bidSize = 0;
                    }
                    else
                    {
                        bidSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                    }
                }
                else if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.OFFER)
                {
                    askPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    askIdentity = noMDEntriesGroup.GetField(Tags.QuoteEntryID);
                    if (this.GetIsIndicative(noMDEntriesGroup))
                    {
                        askSize = 0;
                    }
                    else
                    {
                        askSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                    }
                }
            }

            //Zero sizes are documented way of sending indicative price
            if (bidSize == 0 && askSize == 0)
                return QuoteObject.EMPTY_QUOTE;

            PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSidedIdentity(askPrice: askPrice, askSize: askSize, bidPrice: bidPrice,
                bidSize: bidSize,
                symbol: symbol, counterparty: this.Counterparty,
                subscriptionIdentity: subscriptionIdentity,
                counterpartyTimeUtc: quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                askIdentity: askIdentity,
                bidIdentity: bidIdentity,
                marketDataRecordType: MarketDataRecordType.BankQuoteData);

            return PriceReceivingHelper.SharedQuote;
        }

        private bool GetIsIndicative(Group noMDEntriesGroup)
        {
            string quoteCondition = noMDEntriesGroup.GetField(Tags.QuoteCondition);
            bool indicative = true;

            switch (quoteCondition)
            {
                case QuoteCondition.OPEN_ACTIVE:
                    indicative = false;
                    break;
                case QuoteCondition.CLOSED_INACTIVE:
                    if (!this.Logger.IsLowDiskSpaceConstraintLoggingOn)
                        this.Logger.Log(LogLevel.Info, "Received indicative side of quote, ignoring the side");
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Received quote side in unsupported condition: {0}. Ignoring side",
                        quoteCondition);
                    break;
            }

            return indicative;
        }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            string orderId = m.ClOrdID.getValue();

            switch (m.OrdStatus.getValue())
            {
                case OrdStatus.NEW:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] was received and enqeued by other end", orderId);
                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;
                    break;
                case OrdStatus.FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Filled;
                    executionInfo = new ExecutionInfo(m.ToNullableDecimal(Tags.LastQty),
                        //Doc: 'Price - Dealt price of the fill; LastPx - Price of this fill'
                        m.ToNullableDecimalCoalesce(Tags.LastPx, Tags.Price),
                        m.ToNullableString(Tags.ExecID),
                        m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                        m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                                                                    m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.REJECTED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), null, RejectionType.OrderReject);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Order [{0}] was put to unsupported status: [{1}]", orderId,
                        m.OrdStatus.getValue());
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                executionInfo, rejectionInfo));
        }

        #endregion /FIX message cracking
    }
}
