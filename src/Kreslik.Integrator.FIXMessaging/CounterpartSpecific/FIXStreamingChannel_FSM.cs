﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX42;
using Message = QuickFix.Message;
using SessionState = Kreslik.Integrator.Contracts.SessionState;
using Symbol = Kreslik.Integrator.Common.Symbol;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    public interface IFIXStreamingChannel : IFIXChannel
    {
        bool IsSessionOpen { get; set; }
        void SendTradingStatus(bool open);
    }

    public interface IFIXStreamingChannelPricing : IFIXStreamingChannel
    {
        event Action<StreamingPrice> NewPriceSent;
        event Action<StreamingPriceCancel> NewPriceCancelSent;
        event Action<Common.Symbol, bool> SubscriptionChanged;
        void CancelPriceStreams(StreamingPriceCancel streamingPriceCancel);
        bool IsSubscribed(Common.Symbol symbol);
        int ExpectedPricingDepth { get; set; }
        SubmissionResult SendOrder(StreamingPrice streamingPrice);
    }

    public delegate void CounterpartyRejectedDealDelegate(Symbol symbol, string integratorExecutionId, string counterpartyExecutionId, string integratorGeneratedPriceIdentity);

    public interface IFIXStreamingChannelOrders : IFIXStreamingChannel
    {
        event Action<RejectableMMDeal> NewRejectableDealArrived;
        event CounterpartyRejectedDealDelegate NewCounterpartyRejectedDeal;
        event Action<CounterpartyTimeoutInfo> NewCounterpartyTimeoutInfo;
        event Action<RejectableMMDeal> DealRejected;

        //used to inform pending rejectable deal about delivered OrderTimeout from destination
        ICounterpartyRejectionSource CounterpartyRejectionSource { set; }

        QuickFix.Message CreateAcceptMessage(RejectableMMDeal deal);
        QuickFix.Message CreateRejectionMessage(RejectableMMDeal deal, RejectableMMDeal.DealStatus rejectionReason);
        bool UpdateAcceptMessageWithRejectRestInfo(RejectableMMDeal deal, QuickFix.Message msg);
        bool SendMessage(QuickFix.Message message);
    }



    public abstract class FIXStreamingChannelBase<TDerivedType> : FIXChannelBase<TDerivedType>, IFIXStreamingChannel
        where TDerivedType : MessageCrackerBase<TDerivedType>
    {
        protected FIXStreamingChannelBase(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings settings)
            : base(persistedFixConfigsReader, logger, settings)
        {
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();
            this.Counterparty = counterparty;
        }
        public bool IsSessionOpen { get; set; }
        public abstract void SendTradingStatus(bool open);
        public override Counterparty Counterparty { get; protected set; }

        protected bool CheckTargetSymbolSupported(ISymbolsInfo symbolsInfo, Symbol symbol)
        {
            bool supported = false;

            try
            {
                supported = symbolsInfo.IsSupported(symbol, this.Counterparty.TradingTargetType) &&
                            symbolsInfo.IsSupported(symbol.BaseCurrency) && symbolsInfo.IsSupported(symbol.TermCurrency);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Error, "Exception during condition checking", e);
            }

            return supported;
        }

        public bool SendMessage(QuickFix.Message message)
        {
            try
            {
                return Session.SendToTarget(message, this.SessionId);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, e, "Exception during sending message: {0}", message);
            }

            return false;
        }
    }

    public class FIXStreamingChannelBase_FSM<TDerivedType> : FIXStreamingChannelBase<TDerivedType>
       where TDerivedType : MessageCrackerBase<TDerivedType>
    {
        public FIXStreamingChannelBase_FSM(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings settings)
            : base(persistedFixConfigsReader, logger, counterparty, settings)
        {
            this.OnStarted +=
                () => Task.Delay(TimeSpan.FromMilliseconds(1000)).ContinueWith(t => this.SendTradingStatus(true));
        }
        protected virtual void TradingSessionStatusClosedSent() { }

        public override void SendTradingStatus(bool open)
        {
            if (this.State == SessionState.Running)
            {
                var request = new TradingSessionStatus(new TradingSessionID("KGTTradingSession"),
                    new TradSesStatus(open ? TradSesStatus.OPEN : TradSesStatus.CLOSED));

                //request.TradSesReqID = new TradSesReqID("none");
                //request.SetField(new StringField(1301, "none"));
                //request.SetField(new StringField(1300, "none"));
                Session.SendToTarget(request, this.SessionId);
                this.IsSessionOpen = open;
                if (!open)
                    this.TradingSessionStatusClosedSent();
            }
        }
    }

    public abstract class SubscriptionTrackingHelperBase
    {
        public SubscriptionTrackingHelperBase(ISymbolsInfo symbolsInfo, ILogger logger, Counterparty counterparty)
        {
            this._symbolsInfo = symbolsInfo;
            this._logger = logger;
            this._counterparty = counterparty;
        }

        private ISymbolsInfo _symbolsInfo;
        protected ILogger _logger;
        protected Counterparty _counterparty;
        private int _expectedPricingDepth;

        public int ExpectedPricingDepth
        {
            get { return _expectedPricingDepth; }
            set { this._expectedPricingDepth = value; OnPricingDepthChanged(value); }
        }

        protected virtual void OnPricingDepthChanged(int newPricingDepth) { }

        bool[] _subscribedSymbols = new bool[Common.Symbol.ValuesCount];

        protected bool CheckTargetSymbolSupported(Symbol symbol)
        {
            bool supported = false;

            try
            {
                supported = _symbolsInfo.IsSupported(symbol, this._counterparty.TradingTargetType) &&
                            _symbolsInfo.IsSupported(symbol.BaseCurrency) && _symbolsInfo.IsSupported(symbol.TermCurrency);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Error, "Exception during condition checking", e);
            }

            return supported;
        }

        public event Action<Common.Symbol, bool> SubscriptionChanged;

        protected void InvokeSubscriptionChanged(Symbol symbol, bool subscribed)
        {
            if (_subscribedSymbols[(int)symbol] == subscribed)
                return;

            if(subscribed)
                _subscribedSymbols[(int)symbol] = subscribed;
            if (SubscriptionChanged != null)
                SubscriptionChanged(symbol, subscribed);
            if (!subscribed)
                _subscribedSymbols[(int)symbol] = subscribed;
        }

        public bool IsSubscribed(Common.Symbol symbol)
        {
            return _subscribedSymbols[(int)symbol];
        }

        protected abstract void ResetAllSubscriptionsIds();

        public void ResetAllSubscriptions()
        {
            for (int symbolIdx = 0; symbolIdx < _subscribedSymbols.Length; symbolIdx++)
            {
                if (_subscribedSymbols[symbolIdx])
                {
                    this.InvokeSubscriptionChanged((Common.Symbol)symbolIdx, false);
                }
            }

            this.ResetAllSubscriptionsIds();
        }
    }

    public class FSMSubscriptionTrackingHelper : SubscriptionTrackingHelperBase
    {
        public FSMSubscriptionTrackingHelper(ISymbolsInfo symbolsInfo, ILogger logger, Counterparty counterparty)
            : base(symbolsInfo, logger, counterparty)
        {
        }

        //todo will be byte[][]
        string[] _subscriptionReqIds = new string[Common.Symbol.ValuesCount];

        protected override void ResetAllSubscriptionsIds()
        {
            Array.Clear(_subscriptionReqIds, 0, _subscriptionReqIds.Length);
        }

        public void OnMessage(MarketDataRequest m, SessionID s)
        {
            string symbolString = m.GetGroup(1, Tags.NoRelatedSym).GetField(Tags.Symbol);
            Common.Symbol symbol;
            if (Common.Symbol.TryParse(symbolString, out symbol) && this.CheckTargetSymbolSupported(symbol))
            {
                bool subscribe = m.GetChar(Tags.SubscriptionRequestType) ==
                                 SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES;
                int depth = m.GetInt(Tags.MarketDepth);
                if (depth != this.ExpectedPricingDepth)
                    this._logger.Log(LogLevel.Error, "Receiving subscription for unexpected depth ({0} instead of {1})",
                        depth, this.ExpectedPricingDepth);
                string subscriptionReqId = subscribe ? m.MDReqID.getValue() : null;
                _subscriptionReqIds[(int)symbol] = subscriptionReqId;

                this.InvokeSubscriptionChanged(symbol, subscribe);
            }
            else
            {
                MarketDataRequestReject mdReject = new MarketDataRequestReject(m.MDReqID);
                mdReject.MDReqRejReason = new MDReqRejReason(MDReqRejReason.UNKNOWN_SYMBOL);
                Session.SendToTarget(mdReject, s);
            }
        }
        public string GetSubscriptionReqId(Common.Symbol symbol)
        {
            return this._subscriptionReqIds[(int)symbol];
        }
    }

    public class FALSubscriptionTrackingHelper : SubscriptionTrackingHelperBase
    {
        public FALSubscriptionTrackingHelper(ISymbolsInfo symbolsInfo, ILogger logger, Counterparty counterparty)
            : base(symbolsInfo, logger, counterparty)
        {
        }

        //CAREFULL - 2 layers per actual 1 layer in Integrator (bid and ask separately)
        //todo will be byte[][][]
        private string[][] _subscriptionReqIds;

        protected override void OnPricingDepthChanged(int newPricingDepth)
        {
            if (newPricingDepth > 0)
                _subscriptionReqIds =
                    Enumerable.Repeat(1, Symbol.ValuesCount).Select(i => new string[newPricingDepth * 2]).ToArray();
        }

        public string GetSubscriptionReqId(Common.Symbol symbol, PriceSide side, int layerId)
        {
            string reqId = null;
            int internalIdx = layerId*2 + (int) side;
            if (_subscriptionReqIds[(int)symbol].Length > internalIdx)
                reqId = _subscriptionReqIds[(int)symbol][internalIdx];

            return reqId;
        }

        public bool VerifyAllSubscriptionsReceived()
        {
            foreach (string[] layerReqIds in _subscriptionReqIds)
            {
                if (Array.IndexOf(layerReqIds, null) == -1)
                    return true;
            }

            return false;
        }

        public int GetMaxLayersCount()
        {
            return _subscriptionReqIds.Max(layer => layer.Count(req => req != null))/2;
        }

        protected override void ResetAllSubscriptionsIds()
        {
            if(_subscriptionReqIds != null)
                _subscriptionReqIds.ClearWhole();
        }

        public void OnMessage(QuickFix.FIX44.QuoteRequest m, SessionID s)
        {
            if (m.NoRelatedSym.getValue() > 1)
                throw new Exception("Subscription message on more than single symbol are not currently supported");

            //as dictionary is not configured, the repeating groups are not present
            string symbolString = m.GetField(Tags.Symbol); //m.GetGroup(1, Tags.NoRelatedSym).GetField(Tags.Symbol);
            Common.Symbol symbol;
            if (Common.Symbol.TryParse(symbolString, out symbol) && this.CheckTargetSymbolSupported(symbol))
            {
                string subscriptionReqId = m.QuoteReqID.getValue();

                int layerId = Array.IndexOf(_subscriptionReqIds[(int)symbol], null);

                if (layerId == -1)
                {
                    this._logger.Log(LogLevel.Fatal,
                        "{0} receiving subscription [{1}] for {2}, while all {3} layers (bid and ask counted separately) already has subscriptions specified (existing subscriptions: {4}). This is caused either by receiving subscriptions to unexpected number of layers or receiving subscriptions at unexpected time. The extra subscription is ignored and existing are used. However restart of the session is recommended",
                        this._counterparty, subscriptionReqId, symbol, this._subscriptionReqIds[(int)symbol].Length,
                        string.Join(", ", this._subscriptionReqIds[(int)symbol]));
                }
                else
                {
                    _subscriptionReqIds[(int)symbol][layerId] = subscriptionReqId;
                }

                this.InvokeSubscriptionChanged(symbol, true);
            }
            else
            {
                QuickFix.FIX44.QuoteRequestReject mdReject = new QuickFix.FIX44.QuoteRequestReject(m.QuoteReqID,
                    new QuoteRequestRejectReason(QuoteRequestRejectReason.UNKNOWN_SYMBOL));
                QuickFix.FIX44.QuoteRequestReject.NoRelatedSymGroup symGroup = new QuickFix.FIX44.QuoteRequestReject.NoRelatedSymGroup();
                symGroup.Symbol = new QuickFix.Fields.Symbol(symbolString);
                mdReject.AddGroup(symGroup);

                //this would be shortcut should the dictionary be enabled
                //mdReject.NoRelatedSym = m.NoRelatedSym;

                Session.SendToTarget(mdReject, s);
            };
        }
    }

    public class FIXStreamingChannelPricing_FSM : FIXStreamingChannelBase_FSM<FIXStreamingChannelPricing_FSM>, IFIXStreamingChannelPricing
    {
        public FIXStreamingChannelPricing_FSM(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, ISymbolsInfo symbolsInfo, UnexpectedMessagesTreatingSettings unexpectedMessagessettings)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagessettings)
        {
            this._subscriptionTrackingHelper = new FSMSubscriptionTrackingHelper(symbolsInfo, logger, counterparty);
            this.OnStarted += this._subscriptionTrackingHelper.ResetAllSubscriptions;
            this.OnStopped += this._subscriptionTrackingHelper.ResetAllSubscriptions;
            this.OnStopped += InformAllPricesAreCanceled;
            this.ExpectedPricingDepth = -1;
        }

        private FSMSubscriptionTrackingHelper _subscriptionTrackingHelper;

        public int ExpectedPricingDepth
        {
            get { return this._subscriptionTrackingHelper.ExpectedPricingDepth; }
            set { this._subscriptionTrackingHelper.ExpectedPricingDepth = value; }
        }

        public event Action<Common.Symbol, bool> SubscriptionChanged
        {
            add { this._subscriptionTrackingHelper.SubscriptionChanged += value; }
            remove { this._subscriptionTrackingHelper.SubscriptionChanged -= value; }
        }

        public event Action<StreamingPrice> NewPriceSent;
        public event Action<StreamingPriceCancel> NewPriceCancelSent;

        public bool IsSubscribed(Common.Symbol symbol)
        {
            return _subscriptionTrackingHelper.IsSubscribed(symbol);
        }

        protected override void TradingSessionStatusClosedSent()
        {
            this.InformAllPricesAreCanceled();
        }

        private void InformAllPricesAreCanceled()
        {
            if (this.NewPriceCancelSent != null)
            {
                foreach (Symbol symbol in Symbol.Values)
                {
                    StreamingPriceCancel priceCancel = new StreamingPriceCancel(symbol, this.Counterparty, null, null);
                    priceCancel.SetSentPriceCancelInfo(HighResolutionDateTime.UtcNow);
                    this.NewPriceCancelSent(priceCancel);
                }
            }
        }

        public void OnMessage(MarketDataRequest m, SessionID s)
        {
            this._subscriptionTrackingHelper.OnMessage(m, s);
        }

        private string GetMDEntryId(Common.Symbol s, PriceSide side, int layerId)
        {
            return string.Format("{0}_Entry{1}{2}", s.ShortName, side == PriceSide.Ask ? "A" : "B", layerId);
        }

        public SubmissionResult SendOrder(StreamingPrice streamingPrice)
        {
            string subscriptionReqId = this._subscriptionTrackingHelper.GetSubscriptionReqId(streamingPrice.Symbol);
            if (string.IsNullOrEmpty(subscriptionReqId))
            {
                if (!this._subscriptionTrackingHelper.IsSubscribed(streamingPrice.Symbol))
                    return SubmissionResult.Failed_NotSubscribed;

                this.Logger.Log(LogLevel.Error,
                    "Unexpected during sending price: Subscription request id is not populated for " +
                    streamingPrice.Symbol);
                return SubmissionResult.Failed_InternalError;
            }

            MarketDataIncrementalRefresh md = new MarketDataIncrementalRefresh();
            md.MDReqID = new MDReqID(subscriptionReqId);

            var marketDataEntryGroup = new MarketDataIncrementalRefresh.NoMDEntriesGroup();

            marketDataEntryGroup.QuoteEntryID = new QuoteEntryID(streamingPrice.IntegratorPriceIdentity);
            //New for existing quote will work like change
            marketDataEntryGroup.MDUpdateAction = new MDUpdateAction(MDUpdateAction.NEW);
            marketDataEntryGroup.Symbol = streamingPrice.Symbol.ToQuickFixSymbol();
            marketDataEntryGroup.MDEntryType = new MDEntryType(streamingPrice.Side == PriceSide.Ask ? MDEntryType.OFFER : MDEntryType.BID);
            marketDataEntryGroup.MDEntryID = new MDEntryID(GetMDEntryId(streamingPrice.Symbol, streamingPrice.Side, streamingPrice.LayerId));
            marketDataEntryGroup.MDEntryPx = new MDEntryPx(streamingPrice.Price);
            marketDataEntryGroup.MDEntrySize = new MDEntrySize(streamingPrice.SizeBaseAbs);
            marketDataEntryGroup.MinQty = new MinQty(streamingPrice.MinSizeBaseAbs);
            marketDataEntryGroup.Currency = streamingPrice.Symbol.ToQuickBaseCurrency();
            md.AddGroup(marketDataEntryGroup);

            bool sent = this.SendMessage(md);

            if (sent && this.NewPriceSent != null)
            {
                streamingPrice.IntegratorSentTimeUtc = HighResolutionDateTime.UtcNow;
                this.NewPriceSent(streamingPrice);
            }

            return sent ? SubmissionResult.Success : SubmissionResult.Failed_MessagingLayerError;
        }

        public void CancelPriceStreams(StreamingPriceCancel streamingPriceCancel)
        {
            Symbol symbol = streamingPriceCancel.Symbol;
            int layerId = streamingPriceCancel.PriceProducerIndex.HasValue
                ? streamingPriceCancel.PriceProducerIndex.Value.LayerId
                : 1;

            string subscriptionReqId = this._subscriptionTrackingHelper.GetSubscriptionReqId(streamingPriceCancel.Symbol);
            if (string.IsNullOrEmpty(subscriptionReqId))
                return;

            MarketDataIncrementalRefresh md = new MarketDataIncrementalRefresh();
            md.MDReqID = new MDReqID(subscriptionReqId);


            if (streamingPriceCancel.Side == null || streamingPriceCancel.Side.Value == PriceSide.Ask)
            {
                var askMarketDataEntryGroup = new MarketDataIncrementalRefresh.NoMDEntriesGroup();
                //askMarketDataEntryGroup.QuoteEntryID = new QuoteEntryID(this.GetQuoteEntryId(symbol, layerId));
                askMarketDataEntryGroup.MDUpdateAction = new MDUpdateAction(MDUpdateAction.DELETE);
                askMarketDataEntryGroup.Symbol = symbol.ToQuickFixSymbol();
                askMarketDataEntryGroup.MDEntryType = new MDEntryType(MDEntryType.OFFER);
                askMarketDataEntryGroup.MDEntryID = new MDEntryID(GetMDEntryId(symbol, PriceSide.Ask, layerId));
                askMarketDataEntryGroup.MDEntryPx = new MDEntryPx(0);
                askMarketDataEntryGroup.MDEntrySize = new MDEntrySize(0);
                askMarketDataEntryGroup.Currency = symbol.ToQuickBaseCurrency();
                md.AddGroup(askMarketDataEntryGroup);
            }

            if (streamingPriceCancel.Side == null || streamingPriceCancel.Side.Value == PriceSide.Bid)
            {
                var bidMarketDataEntryGroup = new MarketDataIncrementalRefresh.NoMDEntriesGroup();
                //bidMarketDataEntryGroup.QuoteEntryID = new QuoteEntryID(this.GetQuoteEntryId(symbol, layerId));
                bidMarketDataEntryGroup.MDUpdateAction = new MDUpdateAction(MDUpdateAction.DELETE);
                bidMarketDataEntryGroup.Symbol = symbol.ToQuickFixSymbol();
                bidMarketDataEntryGroup.MDEntryType = new MDEntryType(MDEntryType.BID);
                bidMarketDataEntryGroup.MDEntryID = new MDEntryID(GetMDEntryId(symbol, PriceSide.Bid, layerId));
                bidMarketDataEntryGroup.MDEntryPx = new MDEntryPx(0);
                bidMarketDataEntryGroup.MDEntrySize = new MDEntrySize(0);
                bidMarketDataEntryGroup.Currency = symbol.ToQuickBaseCurrency();
                md.AddGroup(bidMarketDataEntryGroup);
            }

            this.SendMessage(md);
            if (this.NewPriceCancelSent != null)
            {
                streamingPriceCancel.SetSentPriceCancelInfo(HighResolutionDateTime.UtcNow);
                this.NewPriceCancelSent(streamingPriceCancel);
            }
        }
    }

    //FIXChannel_OutFluentBasic<FIXStreamingChannelOrders_FSM>
    public class FIXStreamingChannelOrders_FSM : FIXStreamingChannelBase_FSM<FIXStreamingChannelOrders_FSM>, IFIXStreamingChannelOrders
    {
        public FIXStreamingChannelOrders_FSM(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagessettings)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagessettings)
        { }

        private const int _MAX_COUNTERPARTYCLIENTID_LENGTH = 20;

        public event Action<RejectableMMDeal> NewRejectableDealArrived;
        public event CounterpartyRejectedDealDelegate NewCounterpartyRejectedDeal;
        public event Action<CounterpartyTimeoutInfo> NewCounterpartyTimeoutInfo;

        public event Action<RejectableMMDeal> DealRejected;
        public ICounterpartyRejectionSource CounterpartyRejectionSource { private get; set; }

        private static QuickFix.Fields.TimeInForce _fokTimeInForce =
            new QuickFix.Fields.TimeInForce(QuickFix.Fields.TimeInForce.FILL_OR_KILL);

        public void OnMessage(QuickFix.FIX50.NewOrderSingle m, SessionID s)
        {
            this.HandleOrderMessage(m);
        }

        public void OnMessage(NewOrderSingle m, SessionID s)
        {
            this.HandleOrderMessage(m);
        }

        private void HandleOrderMessage(QuickFix.Message m)
        {
            RejectableMMDeal rejectableMmDeal;
            try
            {
                Symbol symbol = (Common.Symbol)m.GetField(Tags.Symbol);
                string clientOrderId = m.GetField(Tags.ClOrdID);
                if (m.IsSetField(Tags.Currency) && !m.GetField(Tags.Currency).Equals(symbol.BaseCurrency.ToString()))
                    throw new Exception(string.Format("Received deal [{0}] for symbol {1} but currency {2}. Not continuing to parse and rejecting",
                        clientOrderId, symbol, m.GetField(Tags.Currency)));

                //if (m.IsSetField(Tags.TimeInForce) && !m.GetChar(Tags.TimeInForce).Equals(QuickFix.Fields.TimeInForce.FILL_OR_KILL))
                //    this.Logger.Log(LogLevel.Fatal, "Receiving order [{0}] with non-FOK time in force ({1}), treating as FOK",
                //        clientOrderId, m.GetField(Tags.TimeInForce));

                decimal qty = m.GetDecimal(Tags.OrderQty);
                decimal minimumFillableQunatity = qty;
                if (m.IsSetField(Tags.TimeInForce) &&
                    m.GetChar(Tags.TimeInForce).Equals(QuickFix.Fields.TimeInForce.IMMEDIATE_OR_CANCEL) &&
                    m.IsSetField(Tags.MinQty))
                {
                    minimumFillableQunatity = Math.Min(qty, m.GetDecimal(Tags.MinQty));
                }

                string counterpartyClientId = null;
                if (m.IsSetField(Tags.NoPartyIDs) && m.GetInt(Tags.NoPartyIDs) > 0)
                {
                    try
                    {
                        //for case without dictionary
                        counterpartyClientId = m.GetField(Tags.PartyID);
                        //for case with dictionary - so this is repeating group
                        //counterpartyClientId = m.GetGroup(1, Tags.NoPartyIDs).GetField(Tags.PartyID);
                        if (counterpartyClientId.Length > _MAX_COUNTERPARTYCLIENTID_LENGTH)
                        {
                            this.Logger.Log(LogLevel.Fatal, "ClientCounterpartyId: [{0}] longer then expected max ({1}). Trimming", counterpartyClientId, _MAX_COUNTERPARTYCLIENTID_LENGTH);
                            counterpartyClientId = counterpartyClientId.Substring(0,
                                _MAX_COUNTERPARTYCLIENTID_LENGTH);
                        }
                    }
                    catch (Exception e)
                    {
                        this.Logger.LogException(LogLevel.Fatal, "Exception when extracting clientCounterpartyId from exec report " + m, e);
                    }
                }

                DateTime sendingTime;
                if (this.IsDivertedViaFluent)
                {
                    if (m.Header.IsSetField(Tags.OrigSendingTime))
                    {
                        sendingTime = m.Header.ToDateTime(Tags.OrigSendingTime, false, this.Logger);
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Error, "FastMatch streaming order redirected via Fluent, but OrigSendingTime tag (122) not set");
                        sendingTime = m.Header.ToDateTime(Tags.SendingTime, false, this.Logger);
                    }
                }
                else
                    sendingTime = m.Header.ToDateTime(Tags.SendingTime, false, this.Logger);

                rejectableMmDeal = new RejectableMMDeal(
                symbol, this.Counterparty, QuickFixUtils.ToDealDirection(m.GetChar(Tags.Side)).ToOpositeDirection(), m.GetDecimal(Tags.Price),
                qty, minimumFillableQunatity, clientOrderId, counterpartyClientId, m.GetField(Tags.QuoteID),
                sendingTime, this.CounterpartyRejectionSource);
            }
            catch (Exception e)
            {
                //send reject
                this.RejectOrder(m);
                this.Logger.LogException(LogLevel.Fatal, "Could not create internal deal from received order message - auto-rejecting it and stopping session", e);
                this.StopAsync(TimeSpan.FromMilliseconds(200));
                return;
            }

            if (this.NewRejectableDealArrived != null)
            {
                try
                {
                    this.NewRejectableDealArrived(rejectableMmDeal);
                }
                catch (Exception e)
                {
                    //this.RejectDeal(rejectableMmDeal, RejectableMMDeal.DealStatus.KgtRejected_InternalFailure);
                    this.RejectOrder(m);
                    this.Logger.LogException(LogLevel.Fatal, "Auto rejecting deal due to errors during handling the client order", e);
                }
            }
            else
            {
                //this.RejectDeal(rejectableMmDeal, RejectableMMDeal.DealStatus.KgtRejected_InternalFailure);
                this.RejectOrder(m);
                this.Logger.Log(LogLevel.Fatal, "Auto rejecting deal as business layer is not connected to messaging (Streaming should be stopped)");
            }
        }

        private void OnDealRejected(RejectableMMDeal deal)
        {
            if (this.DealRejected != null)
            {
                try
                {
                    DealRejected(deal);
                }
                catch (Exception e)
                {
                    this.Logger.LogException(LogLevel.Fatal, "Rejected deal callback - manual investigation needed", e);
                }
            }
        }

        private void RejectOrder(QuickFix.Message m)
        {
            ExecutionReport execRep = new ExecutionReport();

            //Fluent required//
            execRep.Account = new Account("none");
            execRep.SetField(new Product(Product.CURRENCY));
            execRep.SecurityType = new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT);
            execRep.AvgPx = new AvgPx(0);
            execRep.OrdType = new OrdType(OrdType.PREVIOUSLY_QUOTED);
            execRep.OrdRejReason = new OrdRejReason(OrdRejReason.BROKER_EXCHANGE_OPTION);
            //End of Fluent required//

            execRep.ClOrdID = new ClOrdID(m.GetField(Tags.ClOrdID));
            execRep.OrderID = new OrderID(m.GetField(Tags.ClOrdID));
            execRep.ExecTransType = new ExecTransType(ExecTransType.NEW);
            execRep.ExecID = new ExecID("R000");
            execRep.OrdStatus = new OrdStatus(OrdStatus.REJECTED);
            execRep.Symbol = new QuickFix.Fields.Symbol(m.GetField(Tags.Symbol));
            execRep.Side = new QuickFix.Fields.Side(m.GetChar(Tags.Side));
            execRep.OrderQty = new OrderQty(m.GetDecimal(Tags.OrderQty));
            execRep.LastShares = new LastShares(0);
            execRep.LastPx = new LastPx(0);
            execRep.LeavesQty = new LeavesQty(m.GetDecimal(Tags.OrderQty));
            //execRep.Currency = m.Currency;
            execRep.CumQty = new CumQty(0);
            execRep.ExecType = new ExecType(ExecType.REJECTED);
            execRep.Text = new Text("IncomingOrderMismatch");
            execRep.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);

            Session.SendToTarget(execRep, this.SessionId);

            this.OnDealRejected(null);
        }

        public QuickFix.Message CreateAcceptMessage(RejectableMMDeal deal)
        {
            ExecutionReport execRep = new ExecutionReport();

            //Fluent required//
            execRep.Account = new Account("none");
            execRep.SetField(new Product(Product.CURRENCY));
            execRep.SecurityType = new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT);
            execRep.OrdType = new OrdType(OrdType.PREVIOUSLY_QUOTED);
            //End of Fluent required//

            execRep.ClOrdID = new ClOrdID(deal.CounterpartyClientOrderIdentifier);
            execRep.OrderID = new OrderID(deal.CounterpartyClientOrderIdentifier);
            execRep.ExecTransType = new ExecTransType(ExecTransType.NEW);
            execRep.ExecID = new ExecID(deal.IntegratorExecutionId);
            execRep.OrdStatus = new OrdStatus(OrdStatus.FILLED);
            execRep.FutSettDate = new FutSettDate(deal.SettlementDateLocal.ToString("yyyyMMdd"));
            execRep.Symbol = deal.Symbol.ToQuickFixSymbol();
            execRep.Side = deal.IntegratorDealDirection.ToOpositeDirection().ToQuickFixSide();
            execRep.OrderQty = new OrderQty(deal.CounterpartyRequestedSizeBaseAbs);
            execRep.LastShares = new LastShares(deal.SizeToBeFilledBaseAbs);
            execRep.LastPx = new LastPx(deal.CounterpartyRequestedPrice);
            execRep.LeavesQty = new LeavesQty(deal.CounterpartyRequestedSizeBaseAbs - deal.SizeToBeFilledBaseAbs);
            execRep.CumQty = new CumQty(deal.SizeToBeFilledBaseAbs);
            execRep.AvgPx = new AvgPx(deal.CounterpartyRequestedPrice);
            execRep.Currency = deal.Symbol.BaseCurrency.ToQuickFixCurrency();
            execRep.ExecType = new ExecType(ExecType.FILL);
            execRep.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);
            execRep.TimeInForce =
                new QuickFix.Fields.TimeInForce(deal.CounterpartyRequestedSizeBaseAbsFillMinimum <
                                                deal.CounterpartyRequestedSizeBaseAbs
                    ? QuickFix.Fields.TimeInForce.IMMEDIATE_OR_CANCEL
                    : QuickFix.Fields.TimeInForce.FILL_OR_KILL);

            return execRep;
        }

        public bool UpdateAcceptMessageWithRejectRestInfo(RejectableMMDeal deal, QuickFix.Message msg)
        {
            ExecutionReport execRep = msg as ExecutionReport;

            if (execRep == null)
                return false;

            execRep.OrdStatus = new OrdStatus(OrdStatus.REJECTED);
            execRep.ExecType = new ExecType(ExecType.REJECTED);
            execRep.LeavesQty = new LeavesQty(0);
            execRep.LastShares = new LastShares(deal.CounterpartyRequestedSizeBaseAbs - deal.SizeToBeFilledBaseAbs);
            execRep.LastPx = new LastPx(0);
            execRep.CumQty = new CumQty(deal.CounterpartyRequestedSizeBaseAbs);
            execRep.ExecID = new ExecID('R' + deal.IntegratorExecutionId.Substring(1));

            return true;
        }

        public QuickFix.Message CreateRejectionMessage(RejectableMMDeal deal, RejectableMMDeal.DealStatus rejectionReason)
        {
            ExecutionReport execRep = new ExecutionReport();

            //Fluent required//
            execRep.Account = new Account("none");
            execRep.SetField(new Product(Product.CURRENCY));
            execRep.SecurityType = new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT);
            execRep.AvgPx = new AvgPx(0);
            execRep.OrdType = new OrdType(OrdType.PREVIOUSLY_QUOTED);
            execRep.OrdRejReason = new OrdRejReason(OrdRejReason.BROKER_EXCHANGE_OPTION);
            //End of Fluent required//

            execRep.ClOrdID = new ClOrdID(deal.CounterpartyClientOrderIdentifier);
            execRep.OrderID = new OrderID(deal.CounterpartyClientOrderIdentifier);
            execRep.ExecTransType = new ExecTransType(ExecTransType.NEW);
            execRep.ExecID = new ExecID(deal.IntegratorExecutionId);
            execRep.OrdStatus = new OrdStatus(OrdStatus.REJECTED);
            execRep.Symbol = deal.Symbol.ToQuickFixSymbol();
            execRep.Side = deal.IntegratorDealDirection.ToOpositeDirection().ToQuickFixSide();
            execRep.OrderQty = new OrderQty(deal.CounterpartyRequestedSizeBaseAbs);
            execRep.LastShares = new LastShares(0);
            execRep.LastPx = new LastPx(0);
            execRep.LeavesQty = new LeavesQty(deal.CounterpartyRequestedSizeBaseAbs);
            execRep.CumQty = new CumQty(0);
            execRep.Currency = deal.Symbol.BaseCurrency.ToQuickFixCurrency();
            execRep.ExecType = new ExecType(ExecType.REJECTED);
            execRep.Text = new Text(((RejectableMMDeal.DealRejectionReason)rejectionReason).ToString());
            execRep.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);

            return execRep;
        }

        public void OnMessage(QuickFix.FIX50.DontKnowTrade m, SessionID s)
        {
            this.HandleDontKnowTrade(m);
        }
        public void OnMessage(DontKnowTrade m, SessionID s)
        {
            this.HandleDontKnowTrade(m);
        }

        private const byte _rejectId = (byte)'R';
        private const byte _acceptId = (byte)'A';

        private void HandleDontKnowTrade(QuickFix.Message m)
        {
            Symbol symbol = (Symbol)m.GetField(Tags.Symbol);
            string execId = m.GetField(Tags.ExecID);
            bool wasLate = m.GetChar(Tags.DKReason) == DKReason.OTHER;

            if ((byte)execId[0] == _rejectId)
            {
                this.Logger.Log(LogLevel.Warn, "Order Execution {0} was rejected late - no need to process further",
                    execId);
                if (!wasLate)
                {
                    this.Logger.Log(LogLevel.Fatal,
                        "Unexpected DK reson on integrator rejected order: {0}. MANUAL FOLLOWUP REQUIRED",
                        m.GetField(Tags.DKReason));
                    this.SendTradingStatus(false);
                    this.StopAsync(TimeSpan.FromMilliseconds(100));
                }
                return;
            }

            if ((byte)execId[0] == _acceptId)
            {
                if (wasLate)
                {
                    this.Logger.Log(LogLevel.Fatal,
                        "Order Execution [{0}] was accepted late - integrator attempting to update positions info. Sessions will be stopped",
                        execId);
                }
                else
                {
                    this.Logger.Log(LogLevel.Fatal,
                        "Unexpected DK reson ({0}) on integrator accpeted order: {1}. - integrator attempting to update positions info. Sessions will be stopped",
                        m.GetField(Tags.DKReason), execId);
                }

                try
                {
                    if (this.NewCounterpartyRejectedDeal != null)
                        this.NewCounterpartyRejectedDeal(symbol, execId, null, execId);
                }
                catch (Exception e)
                {
                    this.Logger.LogException(LogLevel.Fatal, e, "Unexpected error during receiving info about rejected execution [{0}] on {1}. MANUAL INVESTIGATION REQUIRED",
                        execId, symbol);
                }
            }
            else
            {
                this.Logger.Log(LogLevel.Fatal,
                    "Execution {0} on {1} reported as not known - but it is not known to integrator either. Check with counterparty!",
                    execId, symbol);
            }
        }

        public void OnMessage(QuickFix.Message m, SessionID s)
        {
            if (m.Header.GetString(35).Equals("OT", StringComparison.OrdinalIgnoreCase))
            {
                string clOrdId = m.GetField(Tags.ClOrdID);
                this.Logger.Log(LogLevel.Error, "Counterparty timeouting client order [{0}]", clOrdId);
                CounterpartyTimeoutInfo timeoutInfo = new CounterpartyTimeoutInfo(HighResolutionDateTime.UtcNow,
                    m.Header.ToDateTime(Tags.SendingTime, false, this.Logger), clOrdId, null);



                if (this.NewCounterpartyTimeoutInfo != null)
                    this.NewCounterpartyTimeoutInfo(timeoutInfo);
            }
            else
            {
                this.Logger.Log(LogLevel.Fatal, "Received unknown message: [{0}]", QuickFixUtils.FormatMessage(m));
            }
        }
    }
}