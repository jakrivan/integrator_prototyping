﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.Diagnostics;
using QuickFix;
using QuickFix.FIX44;
using QuickFix.Fields;
using Message = QuickFix.Message;
using SessionState = Kreslik.Integrator.Contracts.SessionState;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    public class FIXSTPChannel_CTI : FIXChannelBase<FIXSTPChannel_CTI>, IFIXSTPChannel
    {
        public FIXSTPChannel_CTI(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
                                 UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings, FIXSTPChannel_CTIPBSettings settings, STPCounterparty stpCounterparty)
            : base(persistedFixConfigsReader, logger, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;
            this._stpCounterparty = stpCounterparty;
            this.ConfigureStateMachineBasicTransitions();
        }

        private FIXSTPChannel_CTIPBSettings _settings;
        private STPCounterparty _stpCounterparty;

        protected override void OnOutgoingAdminMessage(QuickFix.Message message, QuickFix.SessionID sessionID)
        {
            if (message is Logon)
            {
                message.SetField(new Password(this._settings.Password));
            }
        }

        public override Counterparty Counterparty
        {
            get
            {
                this.Logger.Log(LogLevel.Error, "Counterparty property getter unexpectedly called: {0}",
                                new StackTrace(true));
                return Counterparty.CTI;
            }
            protected set
            {
                this.Logger.Log(LogLevel.Error, "Counterparty property setter unexpectedly called: {0}",
                                new StackTrace(true));
            }
        }

        public bool TryCreateTradeCaptureReport(IExecutionInfo executionInfo, IIntegratorOrderInfo integratorOrderInfo,
            bool isDealCancellation, out QuickFix.Message stpTicket)
        {
            try
            {
                return this.CreateTradeCaptureReportInternal(executionInfo, integratorOrderInfo, isDealCancellation, out stpTicket);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, "Unexpected exception during creating STP ticket", e);
                stpTicket = null;
                return false;
            }
        }

        private bool CreateTradeCaptureReportInternal(IExecutionInfo executionInfo, IIntegratorOrderInfo integratorOrderInfo, bool isDealCancellation, out QuickFix.Message stpTicket)
        {
            stpTicket = null;

            if (executionInfo == null || !executionInfo.IsExecutionInfoPopulated)
            {
                this.Logger.Log(LogLevel.Error,
                                "Requested to send STP (for order [{0}]), but required information is null or not populated",
                                integratorOrderInfo.Identity);
                return false;
            }

            decimal filledAmountBaseAbs = executionInfo.FilledAmountBaseAbs;

            //TODO: for rejected deal FilledAmountBaseAbs is 0 - so we would need rejected amount. Or test if 0 is ok 
            if (isDealCancellation)
                filledAmountBaseAbs = integratorOrderInfo.SizeBaseAbsInitial;

            decimal usedPrice = executionInfo.UsedPrice;

            TradeCaptureReport stp = new TradeCaptureReport();
            try
            {
                string trdReportId = executionInfo.IntegratorTransactionId;
                //Even though this is documented as unique id, it needs to be repeated for cancellations
                //if (isDealCancellation)
                //    trdReportId += "__CANCEL";
                stp.TradeReportID = new TradeReportID(trdReportId);
                stp.ExecID = new ExecID(executionInfo.ExecutionId);
                stp.Symbol = integratorOrderInfo.Symbol.ToQuickFixSymbol();

                //stp NoSides
                TradeCaptureReport.NoSidesGroup noSidesGrp = new TradeCaptureReport.NoSidesGroup();
                noSidesGrp.Side = integratorOrderInfo.IntegratorDealDirection.ToQuickFixSide();

                TradeCaptureReport.NoSidesGroup.NoPartyIDsGroup noPartyIDsGrp = new TradeCaptureReport.NoSidesGroup.NoPartyIDsGroup();
                noPartyIDsGrp.PartyID = new PartyID(integratorOrderInfo.Counterparty.GetTradingTargetStringCode());
                noPartyIDsGrp.PartyIDSource = new PartyIDSource(PartyIDSource.PROPRIETARY);
                noPartyIDsGrp.PartyRole = new PartyRole(PartyRole.CONTRA_FIRM);
                noSidesGrp.AddGroup(noPartyIDsGrp);

                noSidesGrp.Account = new Account(this._settings.Account);
                noSidesGrp.Currency = integratorOrderInfo.Symbol.BaseCurrency.ToQuickFixCurrency();
                noSidesGrp.SettlCurrAmt =
                    new SettlCurrAmt(filledAmountBaseAbs * usedPrice);
                noSidesGrp.SettlCurrency =
                    new SettlCurrency(integratorOrderInfo.Symbol.TermCurrency.ToString());
                stp.AddGroup(noSidesGrp);

                stp.PreviouslyReported = new PreviouslyReported(PreviouslyReported.NO);
                stp.TradeDate = new TradeDate(DateTime.UtcNow.ToString("yyyyMMdd"));
                DateTime settlementDate = executionInfo.SettlementDateLocal;
                stp.SettlDate = new SettlDate(settlementDate.ToString("yyyyMMdd"));
                stp.LastQty = new LastQty(filledAmountBaseAbs);
                stp.LastPx = new LastPx(usedPrice);
                stp.LastSpotRate = new LastSpotRate(usedPrice);
                stp.TransactTime = new TransactTime(executionInfo.TransactionTime, true);
                //stp.SetField(new ExerciseStyle(ExerciseStyle.EUROPEAN));

                if(isDealCancellation)
                    stp.ExecType = new ExecType(ExecType.CANCELED);

                stpTicket = stp;
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, e, "Exception during STP FIX message building");
                return false;
            }
            
            return true;
        }

        public bool TrySendMessage(QuickFix.Message stp)
        {
            try
            {
                return Session.SendToTarget(stp, this.SessionId);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Error, e, "Unexpected exception during STP sending");
                return false;
            }
        }

        public event Action<TradeCaptureSubscriptionChangeArgs> TradeCaptureSubscriptionChanged;
        public event Action<TradeReportTicketInfo> NewTradeReportTicketInfo;
        //never invoked. Citi has no aggregates
        public event Action<AggregatedTradeTicketInfo> NewAggregatedTradeTicketInfo;

        private string BuildSetTagsList(QuickFix.Message m, params int[] tags)
        {
            StringBuilder setTagsListBuilder = new StringBuilder();

            foreach (int tag in tags)
            {
                if (m.IsSetField(tag))
                {
                    QuickFixUtils.AppendTagTranslation(setTagsListBuilder, tag);
                    setTagsListBuilder.Append(", ");
                }
            }

            if (setTagsListBuilder.Length > 2)
            {
                //remove trialing ', '
                return setTagsListBuilder.ToString(0, setTagsListBuilder.Length - 2);
            }
            return null;
        }

        public void OnMessage(TradeCaptureReportRequest m, SessionID s)
        {
            this.Logger.Log(LogLevel.Info, "Received TradeCaptureReportRequest: {0}", m);

            bool subscribed = true;
            if (m.IsSetSubscriptionRequestType())
            {
                switch (m.SubscriptionRequestType.getValue())
                {
                    case SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES:
                        subscribed = true;
                        break;
                    case SubscriptionRequestType.DISABLE_PREVIOUS:
                        subscribed = false;
                        break;
                    case SubscriptionRequestType.SNAPSHOT:
                    default:
                        SendRejectedRequestAck(
                            string.Format("Unsupported SubscriptionRequestType: {0}",
                                          m.SubscriptionRequestType.getValue()), m.TradeRequestID);
                        return;
                }
            }

            if (subscribed)
            {
                string unsupportedFeature = null;

                unsupportedFeature = BuildSetTagsList(m, Tags.TradeReportID, Tags.ExecID, Tags.TradeInputSource,
                                                      Tags.OrderID, Tags.ClOrdID, Tags.MatchStatus, Tags.TrdType,
                                                      Tags.TrdMatchID, Tags.NoDates, Tags.Side);

                if (unsupportedFeature != null)
                {
                    SendRejectedRequestAck(
                        string.Format("Currently not supporting {0}in TradeCaptureReportRequest", unsupportedFeature),
                        m.TradeRequestID);
                    return;
                }
            }

            TradeCaptureReportRequestAck ack = new TradeCaptureReportRequestAck(
                m.TradeRequestID, 
                new TradeRequestType(TradeRequestType.ALL_TRADES), 
                new TradeRequestResult(TradeRequestResult.SUCCESSFUL), 
                new TradeRequestStatus(TradeRequestStatus.ACCEPTED));

            TrySendMessage(ack);

            if (TradeCaptureSubscriptionChanged != null)
            {
                TradeCaptureSubscriptionChanged(new TradeCaptureSubscriptionChangeArgs(subscribed));
            }
        }

        private void SendRejectedRequestAck(string reason, TradeRequestID requestId)
        {
            this.Logger.Log(LogLevel.Fatal, "Rejecting STP subscriber request, reason: {0}", reason);

            TradeCaptureReportRequestAck nack = new TradeCaptureReportRequestAck(
                requestId, 
                new TradeRequestType(TradeRequestType.ALL_TRADES),
                new TradeRequestResult(TradeRequestResult.TRADEREQUESTTYPE_NOT_SUPPORTED), 
                new TradeRequestStatus(TradeRequestStatus.REJECTED));

            TrySendMessage(nack);
        }


        public void OnMessage(TradeCaptureReportAck m, SessionID s)
        {
            DateTime integratorReceivedTime = HighResolutionDateTime.UtcNow;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);
            string integratorDealId = m.TradeReportID.getValue();
            TradeReportTicketInfoType tradeReportTicketInfoType;

            if (m.IsSetTradeReportRejectReason())
            {
                this.Logger.Log(LogLevel.Error, "STP consumer sent report ACK with reject reason: {0}, ack message: {1}",
                                m.TradeReportRejectReason.getValue(), m.ToString());
            }
            
            if (m.IsSetTrdRptStatus())
            {
                int reportStatus = m.TrdRptStatus.getValue();

                switch (reportStatus)
                {
                    case TrdRptStatus.ACCEPTED:
                        tradeReportTicketInfoType = TradeReportTicketInfoType.AcceptedByPB;
                        break;
                    case TrdRptStatus.BOOKED:
                        tradeReportTicketInfoType = TradeReportTicketInfoType.BookedByPB;
                        break;
                    case TrdRptStatus.REJECTED:
                        this.Logger.Log(LogLevel.Fatal, "STP consumer sent report ACK with REJECTED status{0}: {1}",
                                        m.IsSetTradeReportRejectReason()
                                            ? " (" + m.TradeReportRejectReason.getValue() + ")"
                                            : string.Empty, m.ToString());
                        tradeReportTicketInfoType = TradeReportTicketInfoType.RejectedByPB;
                        break;
                    default:
                        this.Logger.Log(LogLevel.Fatal, "STP consumer sent report ACK with unexpected status: {0}", m.ToString());
                        tradeReportTicketInfoType = TradeReportTicketInfoType.Unexpected;
                        break;
                }
            }
            else
            {
                tradeReportTicketInfoType = TradeReportTicketInfoType.AcceptedByPB;
            }

            if (this.NewTradeReportTicketInfo != null)
            {
                this.NewTradeReportTicketInfo(new TradeReportTicketInfo(integratorDealId, integratorReceivedTime,
                                                                        counterpartySendTime, tradeReportTicketInfoType,
                                                                        this._stpCounterparty,
                                                                        m.ToString()));
            }
        }


        public TradeReportTicketInfoType InitialTradeReportTicketInfoTypeOfOutgoingTicket
        {
            get { return TradeReportTicketInfoType.SentToPB; }
        }
    }
}
