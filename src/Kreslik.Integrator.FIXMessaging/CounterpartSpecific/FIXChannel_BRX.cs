﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using QuickFix;
using QuickFix.FIX42;
using QuickFix.Fields;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal class FIXChannel_BRX : FIXChannelMessagingBase<FIXChannel_BRX>
    {
        public FIXChannel_BRX(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger, Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();
        }


        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            QuickFix.Message subscriptionMessage = this.GetMarketDataSubscriptionMsg(subscriptionInfo, subscriptionIdentity, true);
            return Session.SendToTarget(subscriptionMessage, this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            QuickFix.Message subscriptionMessage = this.GetMarketDataSubscriptionMsg(subscriptionInfo, subscriptionIdentity, false);
            return Session.SendToTarget(subscriptionMessage, this.SessionId);
        }

        QuickFix.Message GetMarketDataSubscriptionMsg(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity, bool subscribe)
        {
            string quoteId = subscriptionIdentity;
            var qr = new QuoteRequest(new QuoteReqID(quoteId));
            qr.NoRelatedSym = new NoRelatedSym(1);
            qr.SetField(subscriptionInfo.Symbol.ToQuickFixSymbol());
            qr.SetField(new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT));
            qr.SetField(new OrderQty(subscriptionInfo.Quantity));
            qr.SetField(new OrdType(OrdType.FOREX_MARKET));
            qr.SetField(new TransactTime(DateTime.UtcNow));
            qr.SetField(subscriptionInfo.Symbol.ToQuickBaseCurrency());
            qr.SetField(new FutSettDate("SP"));
            qr.SetField(new IntField(6065, subscribe ? 0 : -1));

            return qr;
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            NewOrderSingle orderMessage = new NewOrderSingle(
                new ClOrdID(identity),
                new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PUBLIC_BROKER_INTERVENTION_OK),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                new OrdType(OrdType.PREVIOUSLY_QUOTED)
                );

            orderMessage.SecurityType = new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT);

            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Price = new Price(orderInfo.RequestedPrice);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);
            orderMessage.FutSettDate = new FutSettDate("SP");
            return orderMessage;
        }
        #endregion /FIX message composing

        #region FIX message cracking

        public void OnMessage(QuoteAcknowledgement m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received quote acknowledgement (= reject for BRX): {0}", QuickFixUtils.FormatMessage(m));

            this.SendSubscriptionChange(new SubscriptionChangeInfo(m.QuoteReqID.getValue(),
                                                                       SubscriptionStatus.Rejected));
        }

        public void OnMessage(QuoteCancel m, SessionID s)
        {
            //this.Logger.Log(LogLevel.Info, "Received quote cancel (this is expected right after subscription): {0}", QuickFixUtils.FormatMessage(m));
            this.SendLastQuoteCancel(m.QuoteReqID.getValue());
        }
        
        public void OnMessage(Quote m, SessionID s)
        {
            //0 size or price should not be present

            QuoteObject quote = this.CreateQuoteFromMessage(m);

            //Zero sizes are not documented way of sending indicative price

            SendNewQuote(quote);
        }

        private QuoteObject CreateQuoteFromMessage(Quote quoteMessage)
        {
            PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSingleIdentity(askPrice: quoteMessage.OfferPx.getValue(),
                                           askSize: quoteMessage.OfferSize.getValue(),
                                           bidPrice: quoteMessage.BidPx.getValue(),
                                           bidSize: quoteMessage.BidSize.getValue(),
                                           symbol: (Kreslik.Integrator.Common.Symbol)quoteMessage.Symbol.getValue(),
                                           counterparty: this.Counterparty,
                                           subscriptionIdentity: quoteMessage.QuoteReqID.getValue(),
                                           identity: quoteMessage.QuoteID.getValue(),
                                           counterpartyTimeUtc: quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                                           marketDataRecordType: MarketDataRecordType.BankQuoteData);
            return PriceReceivingHelper.SharedQuote;
        }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            string orderId = m.ClOrdID.getValue();

            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            switch (m.OrdStatus.getValue())
            {
                case OrdStatus.FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Filled;
                    executionInfo = new ExecutionInfo(m.ToNullableDecimal(Tags.OrderQty),
                                                                    //Doc: 'LastPx allways same as Price'
                                                                    m.ToNullableDecimalCoalesce(Tags.LastPx, Tags.Price),
                                                                    m.ToNullableString(Tags.ExecID),
                                                                    m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                                                                    m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                                                                    m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.REJECTED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), null, RejectionType.OrderReject);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Order [{0}] was put to unsupported status: [{1}]", orderId, m.OrdStatus.getValue());
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     executionInfo, rejectionInfo));
        }

        #endregion /FIX message cracking
    }
}
