﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX42;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    public interface IVenueFIXConnector : IFIXChannel, IOrderFlowMessagingAdapter
    {
        bool SendCancelOrderMessage(OrderRequestInfo originalOrderInfo, string originalIdentity, string remotePlatformOriginalOrderId,
                                           string cancellationOperationIdentity, out string fixMessageRaw);

        bool CancelAllSupported { get; }

        bool SendCancelAllOrdersMessage(string cancellationOperationIdentity, out string fixMessageRaw);

        bool SendCancelReplaceOrder(OrderRequestInfo originalOrderInfo, string originalIdentity, string remotePlatformOriginalOrderId,
                                    OrderRequestInfo replacingOrderInfo, string replacingOrderIdentity,
                                    out string outgoingFixMessageRaw);

        bool OrderStatusReportingSupported { get; }
        bool SendOrderStatusRequest(OrderRequestInfo originalOrderInfo, string originalIdentity,
                                    string operationIdentity);

        bool DealRejectionSupported { get; }
        bool TryRejectDeal(AutoRejectableDeal rejectableDeal, AutoRejectableDeal.RejectableTakerDealRejectionReason rejectionReason);
        bool TryAcceptDeal(AutoRejectableDeal rejectableDeal);
        TimeSpan MaximumDealConfirmationDelayWithinIntegrator { get; }
        DateTime MaximumDealProcessingCutoff(OrderChangeInfo orderChangeInfo);

        event Action<VenueDealObjectInternal> NewVenueDeal;
    }

    public interface ILLChannel
    {
        bool IsOrderInAcceptedShortMemory(string counterpartyTransactionId);
    }

    internal sealed class FIXChannel_HOT : FIXChannel_OutFluent<FIXChannel_HOT>, ILLChannel/*FIXChannelMessagingBase<FIXChannel_HOT>, IVenueFIXConnector*/
    {
        public FIXChannel_HOT(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger, Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings, FIXChannel_HOTSettings settings, IStreamingSettings streamingSettings)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;
            this._maximumDealConfirmationDelayWithinIntegrator = streamingSettings.GetMaxLLTimeInIntegrator(counterparty);
            this._expectedSendingTime = streamingSettings.GetExpectedSendingTime(counterparty);
            this._maxLLTimesOnDestination = streamingSettings.GetMaxLLTimeOnDestiantion(counterparty);

            if (settings.DealConfirmationSupported)
            {
                if(_maximumDealConfirmationDelayWithinIntegrator <= TimeSpan.Zero)
                    logger.Log(LogLevel.Fatal, "MaxOrderHoldTime set to {0}", _maximumDealConfirmationDelayWithinIntegrator);
                if (_expectedSendingTime < TimeSpan.Zero)
                {
                    logger.Log(LogLevel.Fatal, "ExpectedSendingTime configured to {0}. Setting to zero", _expectedSendingTime);
                    _expectedSendingTime = TimeSpan.Zero;
                }
                if (_maxLLTimesOnDestination <= TimeSpan.Zero)
                    logger.Log(LogLevel.Fatal, "MaxOrderHoldTime (Counterparty view) set to {0}", _maxLLTimesOnDestination);
            }

            if(!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();

            //if(_settings.DealConfirmationSupported)
            //    throw new Exception("Deal confrimation not supported in bypassing mode");

            this.SetQuickMessageParsers(false,
                new HotspotExecReportCracker(this, this, settings.DealConfirmationSupported, logger),
                new VenueCancelRejectCracker(this, logger));
        }

        private FIXChannel_HOTSettings _settings;

        protected override bool SupportsLimitOrders
        {
            get { return true; }
        }

        protected override bool SendsQuotedOrdersAsLimit
        {
            get { return true; }
        }

        protected override void OnOutgoingMessage(QuickFix.Message message, SessionID sessionId)
        {
            if (!this.IsDivertedViaFluent)
            {
                message.SetField(new SenderSubID(this._settings.SenderSubId));
            }
        }

        protected override void OnOutgoingAdminMessage(QuickFix.Message message, QuickFix.SessionID sessionID)
        {
            if (!this.IsDivertedViaFluent)
            {
                if (message is Logon)
                {
                    message.SetField(new Username(this._settings.Username));
                    message.SetField(new Password(this._settings.Password));
                }

                message.SetField(new SenderSubID(this._settings.SenderSubId));
            }
        }

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            throw new NotImplementedException("HOT adpater shuld be used only for OrderFlow session");
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            throw new NotImplementedException("HOT adpater shuld be used only for OrderFlow session");
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {

            //if (orderInfo.TimeInForce == Contracts.TimeInForce.Day)
            //{
            //    Quote qt = new Quote();
            //    qt.QuoteID = new QuoteID(identity);
            //    qt.Symbol = orderInfo.Symbol.ToQuickFixSymbol();
            //    qt.SetField(new StringField(7225, "1"));
            //    if (orderInfo.Side == DealDirection.Buy)
            //    {
            //        qt.BidPx = new BidPx(orderInfo.RequestedPrice);
            //        qt.SetField(new StringField(123, "Foo"));
            //        qt.BidSize = new BidSize(orderInfo.SizeBaseAbsInitial);
            //    }
            //    else
            //    {
            //        qt.OfferPx = new OfferPx(orderInfo.RequestedPrice);
            //        qt.SetField(new StringField(123, "Bar"));
            //        qt.OfferSize = new OfferSize(orderInfo.SizeBaseAbsInitial);
            //    }

            //    return qt;
            //}




            var orderMessage = new NewOrderSingle(
                new ClOrdID(identity),
                new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                new OrdType(orderInfo.OrderType == OrderType.Pegged ? OrdType.PEGGED : OrdType.LIMIT));

            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.MinQty = new MinQty(orderInfo.SizeBaseAbsFillMinimum);
            orderMessage.Price = new Price(orderInfo.RequestedPrice);

            if (orderInfo.OrderType == OrderType.Pegged)
            {
                orderMessage.ExecInst =
                    new ExecInst(orderInfo.PegType == PegType.MarketPeg ? ExecInst.MARKET_PEG : ExecInst.PRIMARY_PEG);
                orderMessage.PegDifference = new PegDifference(orderInfo.PegDifferenceBasePolAlwaysAdded);
            }

            if (orderInfo.TimeInForce == Contracts.TimeInForce.Day)
            {
                orderMessage.TimeInForce = new TimeInForce(TimeInForce.DAY);
            }
            else
            {
                orderMessage.TimeInForce = new TimeInForce(TimeInForce.IMMEDIATE_OR_CANCEL);
            }

            return orderMessage;
        }
        #endregion /FIX message composing

        #region custom Hotspot FIX message composing

        protected override QuickFix.Message CreateCancelMessage(OrderRequestInfo originalOrderInfo, string originalIdentity,
                                           string cancellationOperationIdentity)
        {
            return new OrderCancelRequest(
                new OrigClOrdID(originalIdentity),
                new ClOrdID(cancellationOperationIdentity),
                originalOrderInfo.Symbol.ToQuickFixSymbol(),
                originalOrderInfo.Side.ToQuickFixSide(),
                new TransactTime(DateTime.UtcNow));
        }

        //public bool SendCancelOrderMessage(OrderRequestInfo originalOrderInfo, string originalIdentity, string remotePlatformOriginalOrderId, string cancellationOperationIdentity, out string fixMessageRaw)
        //{
        //    var cancelOrderMessage = new OrderCancelRequest(
        //        new OrigClOrdID(originalIdentity),
        //        new ClOrdID(cancellationOperationIdentity),
        //        originalOrderInfo.Symbol.ToQuickFixSymbol(),
        //        originalOrderInfo.Side.ToQuickFixSide(),
        //        new TransactTime(DateTime.UtcNow));


        //    bool successfulySent = false;
        //    try
        //    {
        //        successfulySent = QuickFix.Session.SendToTarget(cancelOrderMessage, this.SessionId);
        //    }
        //    catch (Exception e)
        //    {
        //        this.Logger.LogException(LogLevel.Error, e, "Adapter for {0} failed to send Cancel message for order {1}", this.Counterparty, originalIdentity);
        //    }

        //    if (successfulySent)
        //    {
        //        fixMessageRaw = cancelOrderMessage.ToString();
        //        return true;
        //    }
        //    else
        //    {
        //        fixMessageRaw = string.Empty;
        //        return false;
        //    }
        //}

        //public bool CancelAllSupported
        //{
        //    get { return true; }
        //}

        //public bool SendCancelAllOrdersMessage(string cancellationOperationIdentity, out string fixMessageRaw)
        //{
        //    var cancelOrderMessage = new OrderCancelRequest(
        //        new OrigClOrdID("0"),
        //        new ClOrdID(cancellationOperationIdentity),
        //        new QuickFix.Fields.Symbol("CANCEL"),
        //        new Side(Side.UNDISCLOSED),
        //        new TransactTime(DateTime.UtcNow));

        //    bool successfulySent = false;
        //    try
        //    {
        //        successfulySent = QuickFix.Session.SendToTarget(cancelOrderMessage, this.SessionId);
        //    }
        //    catch (Exception e)
        //    {
        //        this.Logger.LogException(LogLevel.Error, e, "Adapter for {0} failed to send CancelAll", this.Counterparty);
        //    }

        //    if (successfulySent)
        //    {
        //        fixMessageRaw = cancelOrderMessage.ToString();
        //        return true;
        //    }
        //    else
        //    {
        //        fixMessageRaw = string.Empty;
        //        return false;
        //    }
        //}

        protected override QuickFix.Message CreateCancelReplaceMessage(OrderRequestInfo originalOrderInfo, string originalIdentity,
            OrderRequestInfo replacingOrderInfo, string replacingOrderIdentity)
        {
            var cancelReplaceMessage = new OrderCancelReplaceRequest(new OrigClOrdID(originalIdentity),
                                                                     new ClOrdID(replacingOrderIdentity),
                                                                     new HandlInst(
                                                                         HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE),
                                                                     replacingOrderInfo.Symbol.ToQuickFixSymbol(),
                                                                     replacingOrderInfo.Side.ToQuickFixSide(),
                                                                     new TransactTime(DateTime.UtcNow),
                                                                     new OrdType(OrdType.LIMIT));

            cancelReplaceMessage.OrderQty = new OrderQty(replacingOrderInfo.SizeBaseAbsInitial);
            cancelReplaceMessage.MinQty = new MinQty(replacingOrderInfo.SizeBaseAbsFillMinimum);
            cancelReplaceMessage.Price = new Price(replacingOrderInfo.RequestedPrice);

            return cancelReplaceMessage;
        }

        //public bool SendCancelReplaceOrder(OrderRequestInfo originalOrderInfo, string originalIdentity, string remotePlatformOriginalOrderId,
        //                                   OrderRequestInfo replacingOrderInfo, string replacingOrderIdentity,
        //    out string outgoingFixMessageRaw)
        //{
        //    var cancelReplaceMessage = new OrderCancelReplaceRequest(new OrigClOrdID(originalIdentity),
        //                                                             new ClOrdID(replacingOrderIdentity),
        //                                                             new HandlInst(
        //                                                                 HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE),
        //                                                             replacingOrderInfo.Symbol.ToQuickFixSymbol(),
        //                                                             replacingOrderInfo.Side.ToQuickFixSide(),
        //                                                             new TransactTime(DateTime.UtcNow),
        //                                                             new OrdType(OrdType.LIMIT));

        //    cancelReplaceMessage.OrderQty = new OrderQty(replacingOrderInfo.SizeBaseAbsInitial);
        //    cancelReplaceMessage.MinQty = new MinQty(replacingOrderInfo.SizeBaseAbsFillMinimum);
        //    cancelReplaceMessage.Price = new Price(replacingOrderInfo.RequestedPrice);

        //    outgoingFixMessageRaw = cancelReplaceMessage.ToString();

        //    return QuickFix.Session.SendToTarget(cancelReplaceMessage, this.SessionId);
        //}

        //public bool OrderStatusReportingSupported
        //{
        //    get { return false; }
        //}

        //public bool SendOrderStatusRequest(OrderRequestInfo originalOrderInfo, string originalIdentity, string operationIdentity)
        //{
        //    throw new NotImplementedException();
        //}

        public override bool DealRejectionSupported { get { return _settings.DealConfirmationSupported; } }

        private bool TrySendDealConfirmation(AutoRejectableDeal rejectableDeal, AutoRejectableDeal.RejectableTakerDealStatus dealStatus)
        {
            QuickFix.Message execReportMessage = rejectableDeal.DealExecutionReport;
            if (execReportMessage == null ||
                execReportMessage.Header.GetField(QuickFix.Fields.Tags.MsgType) !=
                QuickFix.Fields.MsgType.EXECUTIONREPORT)
            {
                this.Logger.Log(LogLevel.Fatal, "Invalid execution report used to confirm/reject deal. {0}. RejectableDeal: {1}", execReportMessage, rejectableDeal);
                return false;
            }

            if (dealStatus == AutoRejectableDeal.RejectableTakerDealStatus.KgtConfirmed)
            {
                execReportMessage.SetField(new ExecType(ExecType.TRADE));
            }
            else
            {
                execReportMessage.SetField(new ExecType(ExecType.REJECTED));
                execReportMessage.SetField(new Text(((AutoRejectableDeal.RejectableTakerDealRejectionReason)dealStatus).ToString()));
            }

            bool sent = Session.SendToTarget(execReportMessage, this.SessionId);
            this.Logger.Log(LogLevel.Info, "Sent [{0}] status for deal [{1}]]", dealStatus, rejectableDeal.CounterpartyDealIdentity);
            return sent;


            //ExecutionReport execReport = rejectableDeal.DealExecutionReport as ExecutionReport;
            //if (execReport == null)
            //{
            //    this.Logger.Log(LogLevel.Fatal, "Invalid execution report used to confirm/reject deal. {0}", rejectableDeal);
            //    return false;
            //}

            //if (dealStatus == AutoRejectableDeal.RejectableTakerDealStatus.KgtConfirmed)
            //{
            //    execReport.ExecType = new ExecType(ExecType.TRADE);
            //}
            //else
            //{
            //    execReport.ExecType = new ExecType(ExecType.REJECTED);
            //    execReport.Text = new Text(((AutoRejectableDeal.RejectableTakerDealRejectionReason)dealStatus).ToString());
            //}

            //bool sent = Session.SendToTarget(execReport, this.SessionId);
            //this.Logger.Log(LogLevel.Info, "Sent [{0}] status for deal [{1}]]", dealStatus, rejectableDeal.CounterpartyDealIdentity);
            //return sent;
        }

        public override bool TryRejectDeal(AutoRejectableDeal rejectableDeal,
            AutoRejectableDeal.RejectableTakerDealRejectionReason rejectionReason)
        {
            return this.TrySendDealConfirmation(rejectableDeal,
                (AutoRejectableDeal.RejectableTakerDealStatus) rejectionReason);
        }

        public override bool TryAcceptDeal(AutoRejectableDeal rejectableDeal)
        {
            this.AddAcceptedOrderToShortMemory(rejectableDeal.CounterpartyDealIdentity);

            return this.TrySendDealConfirmation(rejectableDeal, AutoRejectableDeal.RejectableTakerDealStatus.KgtConfirmed);
        }

        private readonly List<Tuple<string, DateTime>> _recentlyAcceptedOrders = new List<Tuple<string, DateTime>>();
        private void AddAcceptedOrderToShortMemory(string counterpartyTransactionId)
        {
            DateTime now = HighResolutionDateTime.UtcNow;
            DateTime cuttof = now.Subtract(TimeSpan.FromSeconds(2));
            Tuple<string, DateTime> itemToAdd = new Tuple<string, DateTime>(counterpartyTransactionId, now);

            lock (_recentlyAcceptedOrders)
            {
                for (int idx = 0; idx < _recentlyAcceptedOrders.Count; idx++)
                {
                    if (_recentlyAcceptedOrders[idx].Item2 < cuttof)
                    {
                        _recentlyAcceptedOrders[idx] = itemToAdd;
                        return;
                    }
                }

                _recentlyAcceptedOrders.Add(itemToAdd);
            }
        }

        public bool IsOrderInAcceptedShortMemory(string counterpartyTransactionId)
        {
            lock (_recentlyAcceptedOrders)
            {
                return _recentlyAcceptedOrders.Any(tpl => tpl.Item1 == counterpartyTransactionId);
            }
        }

        private TimeSpan _maximumDealConfirmationDelayWithinIntegrator;
        private TimeSpan _expectedSendingTime;
        private TimeSpan _maxLLTimesOnDestination;

        public override TimeSpan MaximumDealConfirmationDelayWithinIntegrator { get { return _maximumDealConfirmationDelayWithinIntegrator; } }

        public override DateTime MaximumDealProcessingCutoff(OrderChangeInfo orderChangeInfo)
        {
            if (orderChangeInfo.CounterpartySentTime.HasValue)
            {
                return
                    DateTimeEx.Min(orderChangeInfo.CounterpartySentTime.Value,
                        orderChangeInfo.IntegratorReceivedTimeUtc - _expectedSendingTime)
                    + this._maxLLTimesOnDestination - this._expectedSendingTime;
            }
            else
            {
                return orderChangeInfo.IntegratorReceivedTimeUtc - _expectedSendingTime
                       + this._maxLLTimesOnDestination - this._expectedSendingTime;
            }
        }

        //DateTime cutoffTime =
        //        rejectableMmDeal.CounterpartySentDealRequestTimeUtc
        //        + TimeSpanEx.Max(this._expectedSendingTime, rejectableMmDeal.IntegratorReceivedTimeUtc - rejectableMmDeal.CounterpartySentDealRequestTimeUtc)
        //        + (this._maxLLTimesOnDestination - this._expectedSendingTime.Times(2));

        #endregion /custom Hotspot FIX message composing

        #region FIX message cracking

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);

            decimal? rejectedAmount = null;
            decimal? initialQuantity = null;

            char fixOrderStatus = m.OrdStatus.getValue();
            char execType = m.ExecType.getValue();

            string orderId = m.ClOrdID.getValue();
            string rawMessage = m.ToString();
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetSymbol())
            {
                symbol = (Common.Symbol) m.Symbol.getValue();
            }


            // check the tag 7226 first, as for force expired deal, ExecReport shows Trade, but it is actually rejection
            bool isFinalTradeStatusSet = false;
            RejectionType? rejectionType = null;
            if (m.IsSetField(7226))
            {
                //This is TradeConfirm reply
                isFinalTradeStatusSet = true;

                switch (m.GetInt(7226))
                {
                    //client accepted with no error (= trade)
                    case 1:
                        break;
                    //client declined (= rejected)
                    case 2:
                        rejectionType = RejectionType.DealRejectByIntegrator;
                        break;
                    //ECN expired the trade (= reject)
                    case 3:
                        rejectionType = RejectionType.DealRejectByCounterpartyForced;
                        break;
                    //Error in client acceptance (= reject)
                    case 4:
                        rejectionType = RejectionType.DealRejectByCounterpartyForced;
                        break;
                }

                string transactId = m.ToNullableString(Tags.ExecID);
                if (this.IsOrderInAcceptedShortMemory(transactId))
                {
                    if (rejectionType.HasValue)
                    {
                        rejectionType = RejectionType.DealRejectByCounterpartyForced;
                        this.Logger.Log(LogLevel.Fatal,
                                    "Deal for order [{0}] force rejected by ECN (problem in timely confirming). {1}", orderId,
                                    rawMessage);
                        execType = ExecType.REJECTED;
                    }
                }
                else
                {
                    if (rejectionType.HasValue)
                    {
                        if (rejectionType == RejectionType.DealRejectByCounterpartyForced)
                        {
                            this.Logger.Log(LogLevel.Error,
                                "{0} Received ECN reject/timeout. The order in question [{1}:{2}] was not found earlier accepted by Integrator - so flipping the rejection to expected KGT rejection confirm",
                                this.Counterparty, orderId, transactId);

                            rejectionType = RejectionType.DealRejectByIntegrator;
                        }
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Fatal, "{0} Received order in client accepted status (7226=1). However the order in question [{1}:{2}] was not found earlier accepted by Integrator - THIS SHOULD BE MANUALLY INVESTIGATED (Possible Fluent mismatch)",
                            this.Counterparty, orderId, transactId);
                    }
                }
            }

            switch (execType)
            {
                case ExecType.NEW:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] was received and enqeued by other end", orderId);
                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;
                    if (m.IsSetLeavesQty())
                    {
                        initialQuantity = m.LeavesQty.getValue();
                    }
                    break;
                case ExecType.TRADE:

                    FlowSide? flowSide = null;

                    if (m.IsSetExecBroker())
                    {
                        switch (m.ExecBroker.getValue())
                        {
                            //passive trade
                            case "N":
                                flowSide = FlowSide.LiquidityMaker;
                                break;
                            //aggressive trade
                            case "Y":
                                flowSide = FlowSide.LiquidityTaker;
                                break;
                        }
                    }

                    if (flowSide == null)
                    {
                        this.Logger.Log(LogLevel.Fatal,
                                        "FlowSide (Tag 76) not specified or invalid in execution message - we cannot proceess rejection logic. {0}",
                                        m);
                    }

                    bool isNonconfiremdDeal = this.DealRejectionSupported && flowSide.HasValue &&
                                               flowSide.Value == FlowSide.LiquidityMaker && !isFinalTradeStatusSet;

                    decimal? filledAmount;
                    if (fixOrderStatus == OrdStatus.FILLED)
                    {
                        orderStatus = IntegratorOrderExternalChange.Filled;
                        filledAmount = m.IsSetLastShares() ? m.LastShares.getValue() : m.ToNullableDecimal(Tags.CumQty);
                    }
                    else
                    {
                        if (fixOrderStatus != OrdStatus.PARTIALLY_FILLED)
                        {
                            this.Logger.Log(LogLevel.Fatal,
                                            "Order [{0}] with Trade ExecReport is in unexpected state [{1}]", orderId,
                                            fixOrderStatus);
                        }

                        orderStatus = IntegratorOrderExternalChange.PartiallyFilled;
                        //We intentiaonally don't convert to nullable 
                        //  - as quantity is absolutely required with partial fills
                        filledAmount = m.LastShares.getValue();
                    }

                    //override status if confirmation is required for this deal
                    if (isNonconfiremdDeal)
                    {
                        orderStatus = IntegratorOrderExternalChange.NonConfirmedDeal;
                    }

                    string clientCounterpartyId = null;
                    if (m.IsSetNoContraBrokers() && m.GetInt(Tags.NoContraBrokers) == 2)
                    {
                        try
                        {
                            clientCounterpartyId = m.GetGroup(2, Tags.NoContraBrokers).GetField(Tags.ContraBroker);
                            if (clientCounterpartyId.Length > _MAX_COUNTERPARTYCLIENTID_LENGTH)
                            {
                                this.Logger.Log(LogLevel.Fatal, "ClientCounterpartyId: [{0}] longer then expected max ({1}). Trimming", clientCounterpartyId, _MAX_COUNTERPARTYCLIENTID_LENGTH);
                                clientCounterpartyId = clientCounterpartyId.Substring(0,
                                    _MAX_COUNTERPARTYCLIENTID_LENGTH);
                            }
                        }
                        catch (Exception e)
                        {
                            this.Logger.LogException(LogLevel.Fatal, "Exception when extracting clientCounterpartyId from exec report " + m, e);
                        }
                    }

                    this.Logger.Log(LogLevel.Info, "Order [{0}] flipped to {1}", orderId, orderStatus);

                    executionInfo = new ExecutionInfo(
                            filledAmount,
                            m.ToNullableDecimal(Tags.LastPx),
                            m.ToNullableString(Tags.ExecID),
                            m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                            m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                            m.GetIntegratorDealDirection()) { FlowSide = flowSide, CounterpartyClientId = clientCounterpartyId };
                    break;
                case ExecType.REPLACE:
                    if (m.IsSetLastShares() && m.LastShares.getValue() != 0m)
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received replace ExecutionReport, but last shares is nonzero: [{0}]", rawMessage);
                    }

                    //Cancel original order
                    if (m.IsSetOrigClOrdID())
                    {
                        string origClId = m.OrigClOrdID.getValue();

                        this.Logger.Log(LogLevel.Info, "Order [{0}] was replaced and order [{1}] was created and enqeued by other end", origClId, orderId);

                        this.SendOrderChange(new OrderChangeInfo(origClId, symbol, IntegratorOrderExternalChange.Cancelled,
                                                                 counterpartySendTime, null, null,
                                                                 new RejectionInfo(m.ToNullableString(Tags.Text), null,
                                                                                   RejectionType.OrderCancel)));
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received replace ExecutionReport, but OrigClOrdID is not set: [{0}]", rawMessage);
                    }

                    if (m.IsSetLeavesQty())
                    {
                        initialQuantity = m.LeavesQty.getValue();
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received replace ExecutionReport, but LeavesQty is not set: [{0}]", rawMessage);
                    }

                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;

                    break;
                case ExecType.REJECTED:
                case ExecType.EXPIRED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    string rejectionReason = m.ToNullableString(Tags.Text);
                    if (isFinalTradeStatusSet)
                    {
                        //This has to be set, otherwise we don't know what amountwas the deal for
                        rejectedAmount = m.LastShares.getValue();
                        //rejectionReason += "<Integrator Reason: LL Reject>";

                        executionInfo = new ExecutionInfo(
                            0m,
                            m.ToNullableDecimal(Tags.LastPx),
                            m.ToNullableString(Tags.ExecID),
                            m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                            m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                            m.GetIntegratorDealDirection());
                    }
                    else if (m.IsSetCumQty() && m.IsSetOrderQty())
                    {
                        rejectedAmount = m.OrderQty.getValue() - m.CumQty.getValue();
                    }
                    rejectionInfo = new RejectionInfo(rejectionReason, rejectedAmount,
                                                      rejectionType.HasValue
                                                          ? rejectionType.Value
                                                          : RejectionType.OrderReject);
                    break;
                case ExecType.CANCELLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] Cancelled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Cancelled;
                    if (m.IsSetCumQty() && m.IsSetOrderQty())
                    {
                        rejectedAmount = m.OrderQty.getValue() - m.CumQty.getValue();
                    }
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), rejectedAmount, RejectionType.OrderCancel);
                    break;
                case ExecType.PENDING_REPLACE:
                case ExecType.PENDING_CANCEL:
                    if (m.IsSetOrigClOrdID())
                    {
                        orderId = m.OrigClOrdID.getValue();
                        this.Logger.Log(LogLevel.Info, "Execution report for pending state of order: {0} did have OrigClOrdID populated (as: {1}) so changing orderId to: {1}",
                            m.ClOrdID.getValue(), orderId);
                    }
                    this.Logger.Log(LogLevel.Info, "Order [{0}] is in pending cancel state", orderId);
                    orderStatus = IntegratorOrderExternalChange.PendingCancel;
                    break;
                default:
                    this.Logger.Log(LogLevel.Fatal, "Order [{0}] received unsupported ExecType report: [{1}]", orderId, execType);
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            if (this._settings.DealConfirmationSupported && execType == ExecType.TRADE)
            {
                string remoteOrderId = null;
                if (m.IsSetOrderID())
                {
                    remoteOrderId = m.OrderID.getValue();
                }

                this.SendOrderChange(new OrderChangeInfoEx(orderId, symbol, remoteOrderId, orderStatus, counterpartySendTime, null,
                                                 executionInfo, m, null, HighResolutionDateTime.UtcNow));
            }
            else
            {
                this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                    executionInfo, rejectionInfo, initialQuantity));
            }
        }

        public void OnMessage(OrderCancelReject m, SessionID s)
        {
            IntegratorOrderExternalChange orderStatus = IntegratorOrderExternalChange.CancelRejected;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);

            string cancelOperationId = m.ClOrdID.getValue();
            string orderId = m.OrigClOrdID.getValue();
            string freeTextInfo = m.ToNullableString(Tags.Text);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            if (m.IsSetCxlRejReason())
            {
                int cxlRejReasonId = m.CxlRejReason.getValue();

                freeTextInfo = string.Format("Reason code: {0} [{1}]. Description: {2}", cxlRejReasonId, MapCxlRejReasonId(cxlRejReasonId), freeTextInfo);
            }

            this.Logger.Log(LogLevel.Info, "Request [{0}] to cancell order [{1}] was REJECTED (\"{2}\")", cancelOperationId, orderId, freeTextInfo);

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     null, null, freeTextInfo));


            //inform upperlayers about rejection of a cancel request (or cancel replace request)
            //this is the place where we flip Cancel/Replace reject into regular reject - so indicate this type of reject
            this.SendOrderChange(new OrderChangeInfo(cancelOperationId, symbol, IntegratorOrderExternalChange.Rejected,
                                                     counterpartySendTime, null, null,
                                                     new RejectionInfo(freeTextInfo, null, RejectionType.CancelReplaceReject)));
        }

        private string MapCxlRejReasonId(int cxlRejReasonId)
        {
            //public const int TOO_LATE_TO_CANCEL = 0;
            //public const int UNKNOWN_ORDER = 1;
            //public const int BROKER = 2;
            //public const int ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS = 3;
            //public const int UNABLE_TO_PROCESS_ORDER_MASS_CANCEL_REQUEST = 4;
            //public const int ORIGORDMODTIME = 5;
            //public const int DUPLICATE_CLORDID = 6;
            //public const int OTHER = 99;

            switch (cxlRejReasonId)
            {
                case 0:
                    return "Too Late";
                case 1:
                    return "Unknown Order";
                case 3:
                    return "Order Already in Pending Cancel or Pending Replace Status";
                case 6:
                    return "Duplicate ClOrdID Received";
                case 99:
                    return "Other";
                default:
                    return string.Format("<Unknown CxlRejReasonId!: {0}>", cxlRejReasonId);
            }


        }

        #endregion /FIX message cracking

    }

    internal sealed class HotspotExecReportCracker : VenueExecReportCracker
    {
        private bool _dealConfirmationSupported;
        private ILLChannel _llChannel;

        public HotspotExecReportCracker(IOrdersParentChannel parentChannel, ILLChannel llChannel, bool dealConfirmationSupported,
            ILogger logger)
            : base(parentChannel, logger, dealConfirmationSupported)
        {
            this._dealConfirmationSupported = dealConfirmationSupported;
            this._llChannel = llChannel;
        }

        private char _tradeConfirmStatus;
        private char _execBroker;
        private bool _isFinalTradeStatusSet;
        private RejectionType? _rejectionType;
        private bool _primaryContraBrokerVisited;
        private string _counterpartyClientId;

        protected override bool IsIoCMiss
        {
            get { return this._timeInForce == TimeInForce.IMMEDIATE_OR_CANCEL; }
        }

        protected override void ConsumeNonstandardFixTag(int tagId, BufferSegmentBase tagValueSegment)
        {
            switch (tagId)
            {
                case 7226:
                    OnTag7226(tagValueSegment);
                    break;
                case 76:
                    OnTag76(tagValueSegment);
                    break;
                case 375:
                    OnTag375(tagValueSegment);
                    break;
            }
        }

        protected override void ResetState()
        {
            this._tradeConfirmStatus = char.MaxValue;
            this._execBroker = char.MaxValue;
            this._isFinalTradeStatusSet = false;
            this._rejectionType = null;
            this._primaryContraBrokerVisited = false;
            this._counterpartyClientId = null;
        }

        protected override FlowSide? GetFlowSide()
        {
            FlowSide? flowSide = null;

            switch (this._execBroker)
            {
                //passive trade
                case 'N':
                    flowSide = FlowSide.LiquidityMaker;
                    break;
                //aggressive trade
                case 'Y':
                    flowSide = FlowSide.LiquidityTaker;
                    break;
            }

            if (flowSide == null)
            {
                this._logger.Log(LogLevel.Fatal,
                                "FlowSide (Tag 76) not specified or invalid in execution message - we cannot proceess rejection logic.");
            }

            return flowSide;
        }

        protected override ExecutionReportStatus GetTranslatedExecutionReportStatus(char externalExecType)
        {
            switch (externalExecType)
            {
                case ExecType.PENDING_NEW:
                    return this._parentChannel.IsDivertedViaFluent ? ExecutionReportStatus.FluentNew : ExecutionReportStatus.New;
                case ExecType.NEW:
                    return ExecutionReportStatus.New;
                case ExecType.TRADE:
                    return ExecutionReportStatus.TradeMayBeRejectable;
                case ExecType.REPLACE:
                    return ExecutionReportStatus.Replace;
                case ExecType.REJECTED:
                case ExecType.EXPIRED:
                    return ExecutionReportStatus.Reject;
                case ExecType.CANCELLED:
                    return ExecutionReportStatus.Cancel;
                case ExecType.PENDING_REPLACE:
                case ExecType.PENDING_CANCEL:
                    return ExecutionReportStatus.PendingCancelOrReplace;
                default:
                    return ExecutionReportStatus.Unrecognized;
            }
        }

        protected override void UpdateInternalStatusIfneeded()
        {
            this._isFinalTradeStatusSet = false;
            this._rejectionType = null;
            if (this._tradeConfirmStatus != char.MaxValue)
            {
                //This is TradeConfirm reply
                _isFinalTradeStatusSet = true;

                switch (this._tradeConfirmStatus)
                {
                    //client accepted with no error (= trade)
                    case '1':
                        break;
                    //client declined (= rejected)
                    case '2':
                        this._rejectionType = RejectionType.DealRejectByIntegrator;
                        break;
                    //ECN expired the trade (= reject)
                    case '3':
                        this._rejectionType = RejectionType.DealRejectByCounterpartyForced;
                        break;
                    //Error in client acceptance (= reject)
                    case '4':
                        this._rejectionType = RejectionType.DealRejectByCounterpartyForced;
                        break;
                }

                if (this._rejectionType.HasValue && this._rejectionType.Value == RejectionType.DealRejectByCounterpartyForced)
                {
                    this._execType = ExecType.REJECTED;
                }
            }
        }

        protected override IntegratorOrderExternalChange OnExecReportTradeMayBeRejectable(out ExecutionInfo executionInfo)
        {
            IntegratorOrderExternalChange orderStatus = base.OnExecReportTradeNoConfirmation(out executionInfo);

            bool isNonconfiremdDeal = this._dealConfirmationSupported && executionInfo.FlowSide.HasValue &&
                                       executionInfo.FlowSide.Value == FlowSide.LiquidityMaker && !this._isFinalTradeStatusSet;

            //override status if confirmation is required for this deal
            if (isNonconfiremdDeal)
            {
                orderStatus = IntegratorOrderExternalChange.NonConfirmedDeal;
            }

            this._logger.Log(LogLevel.Info, "Order [{0}] flipped to {1}", this._orderId, orderStatus);

            return orderStatus;
        }

        protected override void AppendInfoAfterTradeOrReject(AllocationInfo allocationInfo)
        {
            if (!string.IsNullOrEmpty(this._counterpartyClientId))
                allocationInfo.CounterpartyClientId = this._counterpartyClientId;
        }

        protected override IntegratorOrderExternalChange OnExecReportRejected(out RejectionInfo rejectionInfo, out ExecutionInfo executionInfo)
        {
            this._logger.Log(LogLevel.Info, "Order [{0}] REJECTED", this._orderId);
            IntegratorOrderExternalChange orderStatus = IntegratorOrderExternalChange.Rejected;
            decimal? rejectedAmount = null;
            if (_isFinalTradeStatusSet)
            {
                //This has to be set, otherwise we don't know what amountwas the deal for
                if (!this._lastQty.HasValue)
                    throw new HaltingTagException("LastQty needs to be set on expired deals", 32);
                rejectedAmount = this._lastQty.Value;
                //this._freeTextInfo += "<Integrator Reason: LL Reject>";
                //So that we can propagate info about rejected (or ECN rejected) fill
                executionInfo = GetExecutionInfo(0);

                if (_llChannel != null)
                {
                    if (this._llChannel.IsOrderInAcceptedShortMemory(executionInfo.CounterpartyTransactionId))
                    {
                        if (_rejectionType.HasValue)
                        {
                            _rejectionType = RejectionType.DealRejectByCounterpartyForced;
                            this._logger.Log(LogLevel.Fatal,
                                "Deal for order [{0}] force rejected by ECN (problem in timely confirming).",
                                this._orderId);
                        }
                    }
                    else
                    {
                        if (_rejectionType.HasValue)
                        {
                            if (_rejectionType == RejectionType.DealRejectByCounterpartyForced)
                            {
                                this._logger.Log(LogLevel.Error,
                                    "{0} Received ECN reject/timeout. The order in question [{1}:{2}] was not found earlier accepted by Integrator - so flipping the rejection to expected KGT rejection confirm",
                                    this._parentChannel.Counterparty, this._orderId,
                                    executionInfo.CounterpartyTransactionId);

                                _rejectionType = RejectionType.DealRejectByIntegrator;
                            }
                        }
                        else
                        {
                            this._logger.Log(LogLevel.Fatal,
                                "{0} Received order in client accepted status (7226=1). However the order in question [{1}:{2}] was not found earlier accepted by Integrator - THIS SHOULD BE MANUALLY INVESTIGATED (Possible Fluent mismatch)",
                                this._parentChannel.Counterparty, this._orderId, executionInfo.CounterpartyTransactionId);
                        }
                    }
                }

            }
            else
            {
                rejectedAmount = GetRejectedAmount();
                executionInfo = null;
            }

            rejectionInfo = new RejectionInfo(this._freeTextInfo, rejectedAmount,
                                          _rejectionType.HasValue
                                              ? _rejectionType.Value
                                              : RejectionType.OrderReject);

            return orderStatus;
        }

        //7226 - TradeConfirmStatus
        private void OnTag7226(BufferSegmentBase bufferSegment)
        {
            this._tradeConfirmStatus = (char)bufferSegment[0];
        }

        //ExecBroker = 76
        private void OnTag76(BufferSegmentBase bufferSegment)
        {
            this._execBroker = (char)bufferSegment[0];
        }

        //ContraBroker = 375
        private void OnTag375(BufferSegmentBase bufferSegment)
        {
            if (this._primaryContraBrokerVisited)
            {
                this._counterpartyClientId = bufferSegment.GetContentAsAsciiString();

                if (this._counterpartyClientId.Length > FIXChannel_HOT._MAX_COUNTERPARTYCLIENTID_LENGTH)
                {
                    this._logger.Log(LogLevel.Fatal,
                        "ClientCounterpartyId: [{0}] longer then expected max ({1}). Trimming",
                        this._counterpartyClientId, FIXChannel_HOT._MAX_COUNTERPARTYCLIENTID_LENGTH);
                    this._counterpartyClientId = this._counterpartyClientId.Substring(0,
                        FIXChannel_HOT._MAX_COUNTERPARTYCLIENTID_LENGTH);
                }
            }
            else
            {
                this._primaryContraBrokerVisited = true;
            }
        }
    }
}
