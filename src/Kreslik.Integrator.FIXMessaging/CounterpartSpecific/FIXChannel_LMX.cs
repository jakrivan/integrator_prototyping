﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX44;
using Group = QuickFix.Group;
using Message = QuickFix.Message;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal interface IPriceFilter
    {
        void HandleDealingOnPrice(IIntegratorOrderExternal order);
        void HandleOrderNotActive(IIntegratorOrderExternal order, DateTime counterpartySentTimeUtc);
    }

    internal sealed class FIXChannel_LMX : FIXChannel_OutFluent<FIXChannel_LMX>
    {
        public FIXChannel_LMX(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty,
            UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            FIXChannel_LMXSettings settings, LmaxCounterparty lmaxCounterparty, bool isMdChannel)
            : base(
                persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings,
                lmaxCounterparty == LmaxCounterparty.L01 ? 400 : Kreslik.Integrator.FIXMessaging.PriceReceivingHelper.PREFETCH_PRICES_BATCH_SIZE)
        {
            this._settings = settings;
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();
            this._isLayeredSession = lmaxCounterparty == LmaxCounterparty.L01;

            this.InitializeMappingLists(lmaxCounterparty == LmaxCounterparty.LM2
                ? LMAXMappingsProvider.LMAXInstitutionalMappings
                : LMAXMappingsProvider.LMAXProfessionalMappings);

            if (SettingsInitializator.Instance.IsUat)
            {
                if (lmaxCounterparty == LmaxCounterparty.LM2)
                {
                    this.Logger.Log(LogLevel.Fatal,
                        "LMAX mappings set to reatil as Integrator started in UAT mode. Institutional trading will be busted! STOP THE LMAX TRADING IMMEDIATELY.");
                    this.InitializeMappingLists(LMAXMappingsProvider.LMAXProfessionalMappings);
                }

                //if (lmaxCounterparty == LmaxCounterparty.L01 && !isMdChannel)
                //{
                //    this._settings = new FIXChannel_LMXSettings() { Password = "Password1", Username = "KKuat1" };
                //    this.Logger.Log(LogLevel.Fatal, "Hardcoding credentials for L01 ORD session as in the UAT mode");
                //}
            }

            if (isMdChannel)
            {
                Counterparty[] layersCtps = this._isLayeredSession
                    ? LmaxCounterparty.LayersOfLM1.Take(settings.L01LayersNum).ToArray()
                    : new Counterparty[] { lmaxCounterparty };
                LMAXQuoteCracker cracker = new LMAXQuoteCracker(this, this, layersCtps, logger);
                this.PriceFilter = cracker;
                this.SetQuickMessageParsers(isMdChannel, cracker);
            }
        }

        internal bool SubscribeToTickers
        {
            set
            {
                if (value)
                {
                    this.OnStarted += () => TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(1),
                        SubscribeToAllTickers);
                }
            }
        }
        internal IPriceFilter PriceFilter { get; private set; }

        protected override void SettingsRead(bool isRedirectedViaFluent, bool isMdChannel)
        {
            if (!isMdChannel)
            {
                if (this.IsDivertedViaFluent)
                    this.SetQuickMessageParsers(isMdChannel, new FluentLMAXExecReportCracker(this, this, this.Logger),
                        new VenueCancelRejectCracker(this, this.Logger));
                else
                    this.SetQuickMessageParsers(isMdChannel, new LMAXExecReportCracker(this, this, this.Logger),
                        new VenueCancelRejectCracker(this, this.Logger));
            }
        }

        private bool _isLayeredSession;

        public FIXChannel_LMXSettings _settings;

        protected override void OnOutgoingAdminMessage(QuickFix.Message message, SessionID sessionId)
        {
            if (!this.IsDivertedViaFluent)
            {
                if (message is Logon)
                {
                    message.SetField(new Username(_settings.Username));
                    message.SetField(new Password(_settings.Password));
                }
            }
        }

        protected override bool SupportsLimitOrders
        {
            get { return true; }
        }

        protected override bool SendsQuotedOrdersAsLimit
        {
            get { return true; }
        }

        private string[] _symbolToLmaxIdMapping = Enumerable.Repeat(string.Empty, Common.Symbol.ValuesCount).ToArray();
        private decimal[] _symbolToLmaxQuantityMultiplier = Enumerable.Repeat(0m, Common.Symbol.ValuesCount).ToArray();
        private Dictionary<string, Common.Symbol> _lmaxIdToSymbolMapping = new Dictionary<string, Common.Symbol>();
        private Dictionary<int, Common.Symbol> _lmaxIntIdToSymbolMapping = new Dictionary<int, Common.Symbol>();
        private decimal _minimumFillSize;


        private class LMAXMappings
        {
            public LMAXMappings(List<Tuple<string, string, decimal>> symbolContractMappings, decimal minimumFillSize)
            {
                this.SymbolContractMappings = symbolContractMappings;
                this.MinimumFillSize = minimumFillSize;
            }

            public List<Tuple<string, string, decimal>> SymbolContractMappings { get; private set; }
            public decimal MinimumFillSize { get; private set; }
        }

        private static class LMAXMappingsProvider
        {
            static LMAXMappingsProvider()
            {
                LMAXProfessionalMappings = new LMAXMappings(
                    new List<Tuple<string, string, decimal>>()
                    {
                        new Tuple<string, string, decimal>("AUD/JPY", "4008", 10000),
                        new Tuple<string, string, decimal>("AUD/USD", "4007", 10000),
                        new Tuple<string, string, decimal>("CHF/JPY", "4009", 10000),
                        new Tuple<string, string, decimal>("EUR/AUD", "4016", 10000),
                        new Tuple<string, string, decimal>("EUR/CAD", "4015", 10000),
                        new Tuple<string, string, decimal>("EUR/CHF", "4011", 10000),
                        new Tuple<string, string, decimal>("EUR/GBP", "4003", 10000),
                        new Tuple<string, string, decimal>("EUR/JPY", "4006", 10000),
                        new Tuple<string, string, decimal>("EUR/USD", "4001", 10000),
                        new Tuple<string, string, decimal>("GBP/AUD", "4017", 10000),
                        new Tuple<string, string, decimal>("GBP/CAD", "4014", 10000),
                        new Tuple<string, string, decimal>("GBP/CHF", "4012", 10000),
                        new Tuple<string, string, decimal>("GBP/JPY", "4005", 10000),
                        new Tuple<string, string, decimal>("GBP/USD", "4002", 10000),
                        new Tuple<string, string, decimal>("USD/CAD", "4013", 10000),
                        new Tuple<string, string, decimal>("USD/CHF", "4010", 10000),
                        new Tuple<string, string, decimal>("USD/JPY", "4004", 10000),
                        new Tuple<string, string, decimal>("EUR/CZK", "100479", 10000),
                        new Tuple<string, string, decimal>("GBP/CZK", "100481", 10000),
                        new Tuple<string, string, decimal>("USD/CZK", "100483", 10000),
                        new Tuple<string, string, decimal>("EUR/DKK", "100485", 10000),
                        new Tuple<string, string, decimal>("GBP/DKK", "100487", 10000),
                        new Tuple<string, string, decimal>("USD/DKK", "100489", 10000),
                        new Tuple<string, string, decimal>("EUR/HKD", "100491", 10000),
                        new Tuple<string, string, decimal>("GBP/HKD", "100493", 10000),
                        new Tuple<string, string, decimal>("USD/HKD", "100495", 10000),
                        new Tuple<string, string, decimal>("EUR/HUF", "100497", 10000),
                        new Tuple<string, string, decimal>("GBP/HUF", "100499", 10000),
                        new Tuple<string, string, decimal>("USD/HUF", "100501", 10000),
                        new Tuple<string, string, decimal>("EUR/MXN", "100503", 10000),
                        new Tuple<string, string, decimal>("GBP/MXN", "100505", 10000),
                        new Tuple<string, string, decimal>("USD/MXN", "100507", 10000),
                        new Tuple<string, string, decimal>("EUR/NOK", "100509", 10000),
                        new Tuple<string, string, decimal>("GBP/NOK", "100511", 10000),
                        new Tuple<string, string, decimal>("USD/NOK", "100513", 10000),
                        new Tuple<string, string, decimal>("EUR/NZD", "100515", 10000),
                        new Tuple<string, string, decimal>("GBP/NZD", "100517", 10000),
                        new Tuple<string, string, decimal>("EUR/PLN", "100519", 10000),
                        new Tuple<string, string, decimal>("GBP/PLN", "100521", 10000),
                        new Tuple<string, string, decimal>("USD/PLN", "100523", 10000),
                        new Tuple<string, string, decimal>("EUR/SEK", "100525", 10000),
                        new Tuple<string, string, decimal>("GBP/SEK", "100527", 10000),
                        new Tuple<string, string, decimal>("USD/SEK", "100529", 10000),
                        new Tuple<string, string, decimal>("EUR/SGD", "100531", 10000),
                        new Tuple<string, string, decimal>("GBP/SGD", "100533", 10000),
                        new Tuple<string, string, decimal>("USD/SGD", "100535", 10000),
                        new Tuple<string, string, decimal>("EUR/TRY", "100537", 10000),
                        new Tuple<string, string, decimal>("GBP/TRY", "100539", 10000),
                        new Tuple<string, string, decimal>("USD/TRY", "100541", 10000),
                        new Tuple<string, string, decimal>("EUR/ZAR", "100543", 10000),
                        new Tuple<string, string, decimal>("GBP/ZAR", "100545", 10000),
                        new Tuple<string, string, decimal>("USD/ZAR", "100547", 10000),
                        new Tuple<string, string, decimal>("NZD/USD", "100613", 10000),
                        new Tuple<string, string, decimal>("AUD/NZD", "100615", 10000),
                        new Tuple<string, string, decimal>("NZD/JPY", "100617", 10000),
                        new Tuple<string, string, decimal>("AUD/CHF", "100619", 10000),
                        new Tuple<string, string, decimal>("AUD/CAD", "100667", 10000),
                        new Tuple<string, string, decimal>("CAD/CHF", "100671", 10000),
                        new Tuple<string, string, decimal>("CAD/JPY", "100669", 10000),
                        new Tuple<string, string, decimal>("NZD/CAD", "100673", 10000),
                        new Tuple<string, string, decimal>("NZD/CHF", "100675", 10000),
                        new Tuple<string, string, decimal>("NZD/SGD", "100677", 10000),
                        new Tuple<string, string, decimal>("EUR/RUB", "100679", 10000),
                        new Tuple<string, string, decimal>("USD/RUB", "100681", 10000),
                        new Tuple<string, string, decimal>("USD/CNH", "100892", 10000),
                        new Tuple<string, string, decimal>("NOK/SEK", "100555", 10000),
                        new Tuple<string, string, decimal>("AUS/AUD", "100888", 1),
                        new Tuple<string, string, decimal>("STX/EUR", "100101", 1),
                        new Tuple<string, string, decimal>("CAC/EUR", "100099", 1),
                        new Tuple<string, string, decimal>("DAX/EUR", "100097", 2.5m),
                        new Tuple<string, string, decimal>("DXY/EUR", "110097", 1),
                        new Tuple<string, string, decimal>("XAU/USD", "100637", 10),
                        new Tuple<string, string, decimal>("NIK/JPY", "100105", 100),
                        new Tuple<string, string, decimal>("XAG/USD", "100639", 500),
                        new Tuple<string, string, decimal>("FTS/GBP", "100089", 1),
                        new Tuple<string, string, decimal>("BRE/USD", "100805", 100),
                        new Tuple<string, string, decimal>("WTI/USD", "100800", 100),
                        new Tuple<string, string, decimal>("GAS/USD", "100926", 1000),
                        new Tuple<string, string, decimal>("SPX/USD", "100093", 25),
                        new Tuple<string, string, decimal>("SPY/USD", "110093", 10),
                        new Tuple<string, string, decimal>("NDX/USD", "100095", 10),
                        new Tuple<string, string, decimal>("DJI/USD", "100091", 1),
                        new Tuple<string, string, decimal>("XAG/AUD", "100804", 500),
                        new Tuple<string, string, decimal>("XAU/AUD", "100803", 10),
                        new Tuple<string, string, decimal>("XAU/EUR", "100925", 10),
                        new Tuple<string, string, decimal>("XPD/USD", "100807", 10),
                        new Tuple<string, string, decimal>("XPT/USD", "100806", 5),
                        new Tuple<string, string, decimal>("USD/ILS", "100927", 10000)
                    },
                    1000m);

                LMAXInstitutionalMappings = new LMAXMappings(
                    new List<Tuple<string, string, decimal>>()
                        {
                            new Tuple<string, string, decimal>("AUD/JPY", "5008", 100000),
                            new Tuple<string, string, decimal>("AUD/USD", "5007", 100000),
                            new Tuple<string, string, decimal>("EUR/AUD", "5016", 100000),
                            new Tuple<string, string, decimal>("EUR/CHF", "5011", 100000),
                            new Tuple<string, string, decimal>("EUR/GBP", "5003", 100000),
                            new Tuple<string, string, decimal>("EUR/JPY", "5006", 100000),
                            new Tuple<string, string, decimal>("EUR/USD", "5001", 100000),
                            new Tuple<string, string, decimal>("GBP/JPY", "5005", 100000),
                            new Tuple<string, string, decimal>("GBP/USD", "5002", 100000),
                            new Tuple<string, string, decimal>("NZD/USD", "5053", 100000),
                            new Tuple<string, string, decimal>("USD/CAD", "5013", 100000),
                            new Tuple<string, string, decimal>("USD/CHF", "5010", 100000),
                            new Tuple<string, string, decimal>("USD/JPY", "5004", 100000),
                            new Tuple<string, string, decimal>("CHF/JPY", "5009", 100000),
                            new Tuple<string, string, decimal>("GBP/CHF", "5012", 100000),
                            new Tuple<string, string, decimal>("GBP/CAD", "5014", 100000),
                            new Tuple<string, string, decimal>("EUR/CAD", "5015", 100000),
                            new Tuple<string, string, decimal>("GBP/AUD", "5017", 100000),
                            new Tuple<string, string, decimal>("EUR/DKK", "5021", 100000),
                            new Tuple<string, string, decimal>("GBP/DKK", "5022", 100000),
                            new Tuple<string, string, decimal>("USD/DKK", "5023", 100000),
                            new Tuple<string, string, decimal>("EUR/HKD", "5024", 100000),
                            new Tuple<string, string, decimal>("GBP/HKD", "5025", 100000),
                            new Tuple<string, string, decimal>("USD/HKD", "5026", 100000),
                            new Tuple<string, string, decimal>("EUR/MXN", "5030", 100000),
                            new Tuple<string, string, decimal>("GBP/MXN", "5031", 100000),
                            new Tuple<string, string, decimal>("USD/MXN", "5032", 100000),
                            new Tuple<string, string, decimal>("EUR/NOK", "5033", 100000),
                            new Tuple<string, string, decimal>("GBP/NOK", "5034", 100000),
                            new Tuple<string, string, decimal>("USD/NOK", "5035", 100000),
                            new Tuple<string, string, decimal>("EUR/NZD", "5036", 100000),
                            new Tuple<string, string, decimal>("GBP/NZD", "5037", 100000),
                            new Tuple<string, string, decimal>("EUR/SEK", "5041", 100000),
                            new Tuple<string, string, decimal>("GBP/SEK", "5042", 100000),
                            new Tuple<string, string, decimal>("USD/SEK", "5043", 100000),
                            new Tuple<string, string, decimal>("EUR/SGD", "5044", 100000),
                            new Tuple<string, string, decimal>("GBP/SGD", "5045", 100000),
                            new Tuple<string, string, decimal>("USD/SGD", "5046", 100000),
                            new Tuple<string, string, decimal>("EUR/ZAR", "5050", 100000),
                            new Tuple<string, string, decimal>("GBP/ZAR", "5051", 100000),
                            new Tuple<string, string, decimal>("USD/ZAR", "5052", 100000),
                            new Tuple<string, string, decimal>("AUD/NZD", "5054", 100000),
                            new Tuple<string, string, decimal>("NZD/JPY", "5055", 100000),
                            new Tuple<string, string, decimal>("AUD/CHF", "5056", 100000),
                            new Tuple<string, string, decimal>("AUD/CAD", "5059", 100000),
                            new Tuple<string, string, decimal>("CAD/CHF", "5060", 100000),
                            new Tuple<string, string, decimal>("CAD/JPY", "5061", 100000),
                            new Tuple<string, string, decimal>("NZD/CAD", "5062", 100000),
                            new Tuple<string, string, decimal>("NZD/CHF", "5063", 100000),
                            new Tuple<string, string, decimal>("NZD/SGD", "5064", 100000)
                        },
                    10000m);
            }

            public static LMAXMappings LMAXProfessionalMappings { get; private set; }
            public static LMAXMappings LMAXInstitutionalMappings { get; private set; }
        }

        private void SubscribeToAllTickers()
        {
            foreach (Common.Symbol symbol in _lmaxIntIdToSymbolMapping.Values)
            {
                Session.SendToTarget(
                    this.CreateSubscriptionMessage(new SubscriptionRequestInfo(symbol, 0),
                        string.Format("{0}_{1}_TRD", this.Counterparty, symbol.ShortName), true, true),
                    this.SessionId);
            }
        }

        private void InitializeMappingLists(LMAXMappings lmaxMappings)
        {
            foreach (Tuple<string, string, decimal> lmaxSymbolsDef in lmaxMappings.SymbolContractMappings)
            {
                Common.Symbol symbol;
                if (Common.Symbol.TryParse(lmaxSymbolsDef.Item1, out symbol))
                {
                    _symbolToLmaxIdMapping[(int)symbol] = lmaxSymbolsDef.Item2;
                    _symbolToLmaxQuantityMultiplier[(int)symbol] = lmaxSymbolsDef.Item3;
                    _lmaxIdToSymbolMapping.Add(lmaxSymbolsDef.Item2, symbol);
                    _lmaxIntIdToSymbolMapping.Add(int.Parse(lmaxSymbolsDef.Item2), symbol);
                }
            }

            this._minimumFillSize = lmaxMappings.MinimumFillSize;
        }

        private bool TryGetSymbolForLmaxId(string lmaxId, out Common.Symbol symbol)
        {
            return _lmaxIdToSymbolMapping.TryGetValue(lmaxId, out symbol);
        }

        private bool TryGetSymbolForLmaxId(int lmaxId, out Common.Symbol symbol)
        {
            return _lmaxIntIdToSymbolMapping.TryGetValue(lmaxId, out symbol);
        }

        private bool TryGetLmaxIdForSymbol(Common.Symbol symbol, out string lmaxId)
        {
            lmaxId = _symbolToLmaxIdMapping[(int)symbol];
            return !string.IsNullOrEmpty(lmaxId);
        }

        private bool TryGetLmaxMultiplierForLmaxId(string lmaxId, out decimal multiplier)
        {
            multiplier = 0;

            Common.Symbol symbol;
            if (_lmaxIdToSymbolMapping.TryGetValue(lmaxId, out symbol))
            {
                multiplier = _symbolToLmaxQuantityMultiplier[(int)symbol];
                return true;
            }

            return false;
        }

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, true, false),
                                        this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, false, false),
                                        this.SessionId);
        }

        private const int _LAYERS_ALL = 0;
        private const int _LAYERS_TOB = 1;

        private MarketDataRequest CreateSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity,
                                                                           bool subscribe, bool trades)
        {
            //full vs ToB
            int depth = this._isLayeredSession ? /*_LAYERS_ALL*/ this._settings.L01LayersNum : _LAYERS_TOB;

            var message = new MarketDataRequest(new MDReqID(subscriptionIdentity),
                                                               new SubscriptionRequestType(subscribe
                                                                                               ? SubscriptionRequestType
                                                                                                     .SNAPSHOT_PLUS_UPDATES
                                                                                               : SubscriptionRequestType
                                                                                                     .DISABLE_PREVIOUS),
                                                               new MarketDepth(depth));
            message.MDUpdateType = new MDUpdateType(MDUpdateType.FULL_REFRESH);

            string lmaxSymbolId;
            if (!this.TryGetLmaxIdForSymbol(subscriptionInfo.Symbol, out lmaxSymbolId))
            {
                throw new Exception(string.Format("There is no internal mapping to lmax symbol id for symbol {0}", subscriptionInfo.Symbol));
            }

            // symbol group:
            var symbolGroup = new MarketDataRequest.NoRelatedSymGroup();
            symbolGroup.SecurityID = new SecurityID(lmaxSymbolId);
            symbolGroup.SecurityIDSource = new SecurityIDSource(SecurityIDSource.EXCHANGE_SYMBOL);
            message.AddGroup(symbolGroup);

            if (trades)
            {
                // trade:
                var marketDataEntryGroupTrade = new MarketDataRequest.NoMDEntryTypesGroup();
                marketDataEntryGroupTrade.Set(new MDEntryType(MDEntryType.TRADE));
                message.AddGroup(marketDataEntryGroupTrade);
            }
            else
            {
                // bid:
                var marketDataEntryGroupBid = new MarketDataRequest.NoMDEntryTypesGroup();
                marketDataEntryGroupBid.Set(new MDEntryType(MDEntryType.BID));
                message.AddGroup(marketDataEntryGroupBid);

                // offer:
                var marketDataEntryGroupOffer = new MarketDataRequest.NoMDEntryTypesGroup();
                marketDataEntryGroupOffer.Set(new MDEntryType(MDEntryType.OFFER));
                message.AddGroup(marketDataEntryGroupOffer);
            }

            return message;
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {

            //var orderMessage = new NewOrderSingle(new ClOrdID(identity), orderInfo.Symbol.ToQuickFixSymbol(),
            //                                      new Side(orderInfo.Side.ToQuickFixSide()),
            //                                      new TransactTime(HighResolutionDateTime.UtcNow),
            //                                      new OrdType(OrdType.LIMIT));

            //No need for special code for Quoted orders, as RequestedPrice/Size and symbol are already populated


            if (orderInfo.SizeBaseAbsInitial < _minimumFillSize || orderInfo.SizeBaseAbsInitial % _minimumFillSize != 0)
            {
                this.Logger.Log(LogLevel.Fatal,
                                "Attempt to submit LMAX order [{0}] with size {1} not passingthe granularity check. Refusing",
                                identity, orderInfo.SizeBaseAbsInitial);
                return null;
            }

            if (orderInfo.RequestedPrice.NumberOfSignificantFractionDecimals(true) > 6)
            {
                this.Logger.Log(LogLevel.Fatal,
                                "Attempt to submit LMAX order [{0}] with price {1} not passingthe granularity check. Refusing",
                                identity, orderInfo.RequestedPrice);
                return null;
            }

            var orderMessage = new NewOrderSingle();
            orderMessage.ClOrdID = new ClOrdID(identity);

            string lmaxSymbolId;
            if (!this.TryGetLmaxIdForSymbol(orderInfo.Symbol, out lmaxSymbolId))
            {
                throw new Exception(string.Format("There is no internal mapping to lmax symbol id for symbol {0}", orderInfo.Symbol));
            }
            orderMessage.SecurityID = new SecurityID(lmaxSymbolId);
            orderMessage.SecurityIDSource = new SecurityIDSource(SecurityIDSource.EXCHANGE_SYMBOL);
            orderMessage.Side = orderInfo.Side.ToQuickFixSide();

            //Cancel on disconnect
            orderMessage.ExecInst = new ExecInst(ExecInst.CANCEL_ON_SYSTEM_FAILURE);

            orderMessage.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);
            orderMessage.OrderQty =
                new OrderQty(orderInfo.SizeBaseAbsInitial / _symbolToLmaxQuantityMultiplier[(int)orderInfo.Symbol]);

            orderMessage.OrdType = new OrdType(OrdType.LIMIT);
            orderMessage.Price = new Price(orderInfo.RequestedPrice);

            orderMessage.TimeInForce =
                new TimeInForce(orderInfo.TimeInForce == Contracts.TimeInForce.Day
                                    ? TimeInForce.DAY
                                    : TimeInForce.IMMEDIATE_OR_CANCEL);

            return orderMessage;
        }
        #endregion /FIX message composing

        #region FIX message cracking

        public void OnMessage(MarketDataRequestReject m, SessionID s)
        {
            this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(),
                                                                       SubscriptionStatus.Rejected));
        }

        public void OnMessage(MarketDataSnapshotFullRefresh m, SessionID s)
        {
            QuoteObject quote = this.CreateQuoteFromMessage(m);

            if (quote == null)
            {
                this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(), SubscriptionStatus.Broken));
                return;
            }

            if (object.ReferenceEquals(quote, QuoteObject.EMPTY_QUOTE))
            {
                //if (!this.Logger.IsLowDiskSpaceConstraintLoggingOn) this.Logger.Log(LogLevel.Info, "Received indicative quote (ignoring it and cancelling the previous): {0}", QuickFixUtils.FormatMessage(m));
                this.SendLastQuoteCancel(m.MDReqID.getValue());
                return;
            }

            SendNewQuote(quote);
        }

        private const int MaxNumOfMDEntries = 2;

        private QuoteObject CreateQuoteFromMessage(MarketDataSnapshotFullRefresh quoteMessage)
        {
            Kreslik.Integrator.Common.Symbol symbol;
            if (!this.TryGetSymbolForLmaxId(quoteMessage.SecurityID.getValue(), out symbol))
            {
                this.Logger.Log(LogLevel.Error, "There is no lmax mapping from symbol id: {0}", quoteMessage.SecurityID.getValue());
                return null;
            }

            string subscriptionIdentity = quoteMessage.MDReqID.getValue();

            int mdEntriesNum = quoteMessage.GetInt(Tags.NoMDEntries);

            if (mdEntriesNum > MaxNumOfMDEntries)
            {
                this.Logger.Log(LogLevel.Error, "Received quote with unexpected number of layers: {0}",
                                mdEntriesNum);
                return null;
            }

            decimal bidPrice = 0;
            decimal bidSize = 0;
            decimal askPrice = 0;
            decimal askSize = 0;

            for (int groupIndex = 1; groupIndex <= mdEntriesNum; groupIndex += 1)
            {
                Group noMDEntriesGroup = quoteMessage.GetGroup(groupIndex, Tags.NoMDEntries);
                if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.BID)
                {
                    bidPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    bidSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize) * _symbolToLmaxQuantityMultiplier[(int)symbol];
                }
                else if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.OFFER)
                {
                    askPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    askSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize) * _symbolToLmaxQuantityMultiplier[(int)symbol];
                }
            }

            //Zero sizes are documented way of sending indicative price
            if (bidSize == 0 && askSize == 0)
                return QuoteObject.EMPTY_QUOTE;

            PriceReceivingHelper.SharedQuote.OverrideContentObsoleteNoIdentity(askPrice: askPrice, askSize: askSize, bidPrice: bidPrice, bidSize: bidSize,
                                      symbol: symbol, counterparty: this.Counterparty,
                                      subscriptionIdentity: subscriptionIdentity,
                                      counterpartyTimeUtc:
                                          quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                                          marketDataRecordType: MarketDataRecordType.VenueNewOrder);

            return PriceReceivingHelper.SharedQuote;
        }

        private decimal? GetConvertedSize(ExecutionReport m, int filedTagId)
        {
            decimal? filledSize = m.ToNullableDecimal(filedTagId);
            if (filledSize.HasValue && m.IsSetSecurityID())
            {
                decimal multiplier;
                if (this.TryGetLmaxMultiplierForLmaxId(m.SecurityID.getValue(), out multiplier))
                {
                    filledSize *= multiplier;
                }
                else
                {
                    filledSize = null;
                }
            }

            return filledSize;
        }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            string freeTextInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);

            decimal? rejectedAmount = null;
            decimal? initialQuantity = null;

            char fixOrderStatus = m.OrdStatus.getValue();
            char execType = m.ExecType.getValue();

            string orderId = m.ClOrdID.getValue();
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            string rawMessage = m.ToString();

            switch (execType)
            {
                case ExecType.NEW:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] was received and enqeued by other end", orderId);
                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;
                    initialQuantity = this.GetConvertedSize(m, Tags.LeavesQty);
                    break;
                case ExecType.TRADE:
                    decimal? filledAmount = this.GetConvertedSize(m, Tags.LastQty);
                    if (fixOrderStatus == OrdStatus.FILLED)
                    {
                        this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                        orderStatus = IntegratorOrderExternalChange.Filled;
                    }
                    else
                    {
                        if (fixOrderStatus != OrdStatus.PARTIALLY_FILLED)
                        {
                            this.Logger.Log(LogLevel.Fatal,
                                            "Order [{0}] with Trade ExecReport is in unexpected state [{1}]", orderId,
                                            fixOrderStatus);
                        }

                        this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly partially filled", orderId);
                        orderStatus = IntegratorOrderExternalChange.PartiallyFilled;

                        if (!filledAmount.HasValue)
                        {
                            Logger.Log(LogLevel.Fatal,
                                       "Cannot get filled converted size for partial fill execution report (instrument, instrument mapping, or size is missing). {0}",
                                       m);
                            throw new Exception("Cannot get filled converted size for partial fill execution report.");
                        }
                    }

                    executionInfo = new ExecutionInfo(
                            filledAmount,
                            m.ToNullableDecimal(Tags.LastPx),
                            m.ToNullableString(Tags.ExecID),
                            m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                            m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                            m.GetIntegratorDealDirection());
                    break;
                case ExecType.REPLACE:
                    if (m.IsSetLastQty() && m.LastQty.getValue() != 0m)
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received replace ExecutionReport, but last shares is nonzero: [{0}]", rawMessage);
                    }

                    //Cancel original order
                    if (m.IsSetOrigClOrdID())
                    {
                        string origClId = m.OrigClOrdID.getValue();

                        this.Logger.Log(LogLevel.Info, "Order [{0}] was replaced and order [{1}] was created and enqeued by other end", origClId, orderId);

                        this.SendOrderChange(new OrderChangeInfo(origClId, symbol, IntegratorOrderExternalChange.Cancelled, counterpartySendTime, null,
                                                                         null, new RejectionInfo(m.ToNullableString(Tags.Text), null,
                                                                                   RejectionType.OrderCancel)));
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received replace ExecutionReport, but OrigClOrdID is not set: [{0}]", rawMessage);
                    }

                    initialQuantity = this.GetConvertedSize(m, Tags.LeavesQty);
                    if (!initialQuantity.HasValue)
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received replace ExecutionReport, but LeavesQty is not set: [{0}]", rawMessage);
                    }

                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;

                    break;
                case ExecType.REJECTED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    if (m.IsSetCumQty() && m.IsSetOrderQty())
                    {
                        rejectedAmount = this.GetConvertedSize(m, Tags.OrderQty) - this.GetConvertedSize(m, Tags.CumQty);
                    }
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), rejectedAmount, RejectionType.OrderReject);
                    break;
                case ExecType.CANCELLED:
                    freeTextInfo = m.ToNullableString(Tags.Text);

                    if (m.IsSetOrigClOrdID())
                    {
                        orderId = m.OrigClOrdID.getValue();
                        this.Logger.Log(LogLevel.Info, "Cancel Execution report for order: {0} did have OrigClOrdID populated (as: {1}) so changing orderId to: {1}",
                            m.ClOrdID.getValue(), orderId);
                    }

                    //The only difference between requeste cancel and IoC Cancel is the TimeInForce tag
                    if (m.IsSetTimeInForce() && m.TimeInForce.getValue() == TimeInForce.IMMEDIATE_OR_CANCEL)
                    {
                        this.Logger.Log(LogLevel.Info, "IoC Order [{0}] Rejected", orderId);
                        orderStatus = IntegratorOrderExternalChange.Rejected;
                        freeTextInfo += "<Integrator Supplied Reason: IoC miss>";
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Info, "Order [{0}] Cancelled", orderId);
                        orderStatus = IntegratorOrderExternalChange.Cancelled;
                    }

                    //NOTE: CumQty is traditional overall cumulative qunatity, however OrdQty is size from the last replacement
                    // therefore ther is unfortunately no way how to cross-check the rejection amount for the Cancel on LMAX
                    //  Commenting following code out due to this reason and leaving the rejection amount retrieval on higher layer
                    //
                    //if (m.IsSetCumQty() && m.IsSetOrderQty())
                    //{
                    //    rejectedAmount = this.GetConvertedSize(m, Tags.OrderQty) - this.GetConvertedSize(m, Tags.CumQty);
                    //}
                    rejectionInfo = new RejectionInfo(freeTextInfo, rejectedAmount, orderStatus == IntegratorOrderExternalChange.Cancelled ? RejectionType.OrderCancel : RejectionType.OrderReject);
                    break;
                case ExecType.ORDER_STATUS:

                    bool cancelled = true;
                    initialQuantity = this.GetConvertedSize(m, Tags.LeavesQty);
                    if (!initialQuantity.HasValue || initialQuantity.Value != 0)
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received OrderStatus ExecutionReport, but LeavesQty is not set or it is nonzero: [{0}]", rawMessage);
                        cancelled = false;
                    }

                    if (fixOrderStatus != OrdStatus.CANCELED)
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received OrderStatus ExecutionReport, but OrderStatus is not Cancelled: [{0}]", rawMessage);
                        cancelled = false;
                    }

                    if (m.IsSetCumQty() && m.IsSetOrderQty())
                    {
                        rejectedAmount = this.GetConvertedSize(m, Tags.OrderQty) - this.GetConvertedSize(m, Tags.CumQty);
                    }

                    //After reconnect all pending orders should have been cancelled (in Integrator language it's rejected)
                    orderStatus = cancelled
                                      ? IntegratorOrderExternalChange.Rejected
                                      : IntegratorOrderExternalChange.InBrokenState;

                    freeTextInfo = m.ToNullableString(Tags.Text);
                    freeTextInfo += "<Integrator Supplied Reason: Order Status after reconnect>";
                    rejectionInfo = new RejectionInfo(freeTextInfo, rejectedAmount, RejectionType.OrderReject);
                    break;

                default:
                    this.Logger.Log(LogLevel.Fatal, "Order [{0}] received unsupported ExecType report: [{1}]", orderId, execType);
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                         executionInfo, rejectionInfo, initialQuantity));
        }

        public void OnMessage(OrderCancelReject m, SessionID s)
        {
            IntegratorOrderExternalChange orderStatus = IntegratorOrderExternalChange.CancelRejected;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);

            string cancelOperationId = m.ClOrdID.getValue();
            string orderId = m.OrigClOrdID.getValue();
            string freeTextInfo = m.ToNullableString(Tags.Text);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            if (m.IsSetCxlRejReason())
            {
                int cxlRejReasonId = m.CxlRejReason.getValue();

                freeTextInfo = string.Format("Reason code: {0} [{1}]. Description: {2}", cxlRejReasonId, MapCxlRejReasonId(cxlRejReasonId), freeTextInfo);
            }

            this.Logger.Log(LogLevel.Info, "Request [{0}] to cancell order [{1}] was REJECTED (\"{2}\")", cancelOperationId, orderId, freeTextInfo);

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     null, null, freeTextInfo));

            //inform upperlayers about rejection of a cancel request (or cancel replace request)
            //this is the place where we flip Cancel/Replace reject into regular reject - so indicate this type of reject
            this.SendOrderChange(new OrderChangeInfo(cancelOperationId, symbol, IntegratorOrderExternalChange.Rejected,
                                                     counterpartySendTime, null, null,
                                                     new RejectionInfo(freeTextInfo, null, RejectionType.CancelReplaceReject)));

            //this way we can get replace request orig client id
            //if (m.CxlRejResponseTo.getValue() == CxlRejResponseTo.ORDER_CANCEL_REPLACE_REQUEST)
            //{
            //    m.OrigClOrdID.getValue()
            //} 
        }

        private string MapCxlRejReasonId(int cxlRejReasonId)
        {
            switch (cxlRejReasonId)
            {
                case 1:
                    return "Unknown Order";
                case 2:
                    return "Broker/Exchange Option";
                case 6:
                    return "Duplicate ClOrdID Received";
                default:
                    return string.Format("<Unknown CxlRejReasonId!: {0}>", cxlRejReasonId);
            }
        }

        #endregion /FIX message cracking


        protected override QuickFix.Message CreateCancelMessage(OrderRequestInfo originalOrderInfo, string originalIdentity,
                                           string cancellationOperationIdentity)
        {
            var cancelOrderMessage = new OrderCancelRequest();

            cancelOrderMessage.OrigClOrdID = new OrigClOrdID(originalIdentity);
            cancelOrderMessage.ClOrdID = new ClOrdID(cancellationOperationIdentity);

            string lmaxSymbolId;
            if (!this.TryGetLmaxIdForSymbol(originalOrderInfo.Symbol, out lmaxSymbolId))
            {
                this.Logger.Log(LogLevel.Fatal, "There is no internal mapping to lmax symbol id for symbol {0}", originalOrderInfo.Symbol);
                return null;
            }
            cancelOrderMessage.SecurityID = new SecurityID(lmaxSymbolId);
            cancelOrderMessage.SecurityIDSource = new SecurityIDSource(SecurityIDSource.EXCHANGE_SYMBOL);

            cancelOrderMessage.Side = originalOrderInfo.Side.ToQuickFixSide();
            cancelOrderMessage.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);

            cancelOrderMessage.OrderQty =
                new OrderQty((originalOrderInfo.SizeBaseAbsInitial / _symbolToLmaxQuantityMultiplier[(int)originalOrderInfo.Symbol]) / 2);

            return cancelOrderMessage;
        }

        //public bool SendCancelOrderMessage(OrderRequestInfo originalOrderInfo, string originalIdentity, string remotePlatformOriginalOrderId,
        //                                   string cancellationOperationIdentity, out string fixMessageRaw)
        //{
        //    fixMessageRaw = string.Empty;

        //    var cancelOrderMessage = new OrderCancelRequest();

        //    cancelOrderMessage.OrigClOrdID = new OrigClOrdID(originalIdentity);
        //    cancelOrderMessage.ClOrdID = new ClOrdID(cancellationOperationIdentity);

        //    string lmaxSymbolId;
        //    if (!this.TryGetLmaxIdForSymbol(originalOrderInfo.Symbol, out lmaxSymbolId))
        //    {
        //        this.Logger.Log(LogLevel.Fatal, "There is no internal mapping to lmax symbol id for symbol {0}", originalOrderInfo.Symbol);
        //        return false;
        //    }
        //    cancelOrderMessage.SecurityID = new SecurityID(lmaxSymbolId);
        //    cancelOrderMessage.SecurityIDSource = new SecurityIDSource(SecurityIDSource.EXCHANGE_SYMBOL);

        //    cancelOrderMessage.Side = originalOrderInfo.Side.ToQuickFixSide();
        //    cancelOrderMessage.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);

        //    cancelOrderMessage.OrderQty =
        //        new OrderQty((originalOrderInfo.SizeBaseAbsInitial / _symbolToLmaxQuantityMultiplier[(int)originalOrderInfo.Symbol]) / 2);

        //    bool successfulySent = false;
        //    try
        //    {
        //        successfulySent = QuickFix.Session.SendToTarget(cancelOrderMessage, this.SessionId);
        //    }
        //    catch (Exception e)
        //    {
        //        this.Logger.LogException(LogLevel.Error, e, "Adapter for {0} failed to send Cancel message for order {1}", this.Counterparty, originalIdentity);
        //    }

        //    if (successfulySent)
        //    {
        //        fixMessageRaw = cancelOrderMessage.ToString();
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //public bool CancelAllSupported { get { return false; } }

        //public bool SendCancelAllOrdersMessage(string cancellationOperationIdentity, out string fixMessageRaw)
        //{
        //    throw new NotImplementedException();
        //}


        protected override QuickFix.Message CreateCancelReplaceMessage(OrderRequestInfo originalOrderInfo, string originalIdentity,
            OrderRequestInfo replacingOrderInfo, string replacingOrderIdentity)
        {
            if (replacingOrderInfo.SizeBaseAbsInitial < _minimumFillSize || replacingOrderInfo.SizeBaseAbsInitial % _minimumFillSize != 0)
            {
                this.Logger.Log(LogLevel.Fatal,
                                "Attempt to submit LMAX order [{0} replacing {1}] with size {2} not passingthe granularity check. Refusing",
                                replacingOrderIdentity, originalIdentity, replacingOrderInfo.SizeBaseAbsInitial);
                return null;
            }

            if (replacingOrderInfo.RequestedPrice.NumberOfSignificantFractionDecimals(true) > 6)
            {
                this.Logger.Log(LogLevel.Fatal,
                                "Attempt to submit LMAX order [{0} replacing {1}] with price {2} not passingthe granularity check. Refusing",
                                replacingOrderIdentity, originalIdentity, replacingOrderInfo.RequestedPrice);
                return null;
            }

            var cancelReplaceMessage = new OrderCancelReplaceRequest();


            cancelReplaceMessage.OrigClOrdID = new OrigClOrdID(originalIdentity);
            cancelReplaceMessage.ClOrdID = new ClOrdID(replacingOrderIdentity);

            string lmaxSymbolId;
            if (!this.TryGetLmaxIdForSymbol(originalOrderInfo.Symbol, out lmaxSymbolId))
            {
                this.Logger.Log(LogLevel.Fatal, "There is no internal mapping to lmax symbol id for symbol {0}", originalOrderInfo.Symbol);
                return null;
            }
            cancelReplaceMessage.SecurityID = new SecurityID(lmaxSymbolId);
            cancelReplaceMessage.SecurityIDSource = new SecurityIDSource(SecurityIDSource.EXCHANGE_SYMBOL);

            cancelReplaceMessage.Side = originalOrderInfo.Side.ToQuickFixSide();
            cancelReplaceMessage.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);

            cancelReplaceMessage.OrderQty =
                new OrderQty(replacingOrderInfo.SizeBaseAbsInitial / _symbolToLmaxQuantityMultiplier[(int)replacingOrderInfo.Symbol]);



            //Cancel on disconnect
            cancelReplaceMessage.ExecInst = new ExecInst(ExecInst.CANCEL_ON_SYSTEM_FAILURE);
            cancelReplaceMessage.OrdType = new OrdType(OrdType.LIMIT);
            cancelReplaceMessage.Price = new Price(replacingOrderInfo.RequestedPrice);

            cancelReplaceMessage.TimeInForce =
                new TimeInForce(replacingOrderInfo.TimeInForce == Contracts.TimeInForce.Day
                                    ? TimeInForce.DAY
                                    : TimeInForce.IMMEDIATE_OR_CANCEL);

            return cancelReplaceMessage;
        }

        //public bool SendCancelReplaceOrder(OrderRequestInfo originalOrderInfo, string originalIdentity, string remotePlatformOriginalOrderId,
        //                                   OrderRequestInfo replacingOrderInfo, string replacingOrderIdentity,
        //                                   out string outgoingFixMessageRaw)
        //{
        //    outgoingFixMessageRaw = string.Empty;

        //    if (replacingOrderInfo.SizeBaseAbsInitial < _minimumFillSize || replacingOrderInfo.SizeBaseAbsInitial % _minimumFillSize != 0)
        //    {
        //        this.Logger.Log(LogLevel.Fatal,
        //                        "Attempt to submit LMAX order [{0} replacing {1}] with size {2} not passingthe granularity check. Refusing",
        //                        replacingOrderIdentity, originalIdentity, replacingOrderInfo.SizeBaseAbsInitial);
        //        return false;
        //    }

        //    if (replacingOrderInfo.RequestedPrice.NumberOfSignificantFractionDecimals(true) > 6)
        //    {
        //        this.Logger.Log(LogLevel.Fatal,
        //                        "Attempt to submit LMAX order [{0} replacing {1}] with price {2} not passingthe granularity check. Refusing",
        //                        replacingOrderIdentity, originalIdentity, replacingOrderInfo.RequestedPrice);
        //        return false;
        //    }

        //    var cancelReplaceMessage = new OrderCancelReplaceRequest();


        //    cancelReplaceMessage.OrigClOrdID = new OrigClOrdID(originalIdentity);
        //    cancelReplaceMessage.ClOrdID = new ClOrdID(replacingOrderIdentity);

        //    string lmaxSymbolId;
        //    if (!this.TryGetLmaxIdForSymbol(originalOrderInfo.Symbol, out lmaxSymbolId))
        //    {
        //        this.Logger.Log(LogLevel.Fatal, "There is no internal mapping to lmax symbol id for symbol {0}", originalOrderInfo.Symbol);
        //        return false;
        //    }
        //    cancelReplaceMessage.SecurityID = new SecurityID(lmaxSymbolId);
        //    cancelReplaceMessage.SecurityIDSource = new SecurityIDSource(SecurityIDSource.EXCHANGE_SYMBOL);

        //    cancelReplaceMessage.Side = originalOrderInfo.Side.ToQuickFixSide();
        //    cancelReplaceMessage.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);

        //    cancelReplaceMessage.OrderQty =
        //        new OrderQty(replacingOrderInfo.SizeBaseAbsInitial / _symbolToLmaxQuantityMultiplier[(int)replacingOrderInfo.Symbol]);



        //    //Cancel on disconnect
        //    cancelReplaceMessage.ExecInst = new ExecInst(ExecInst.CANCEL_ON_SYSTEM_FAILURE);
        //    cancelReplaceMessage.OrdType = new OrdType(OrdType.LIMIT);
        //    cancelReplaceMessage.Price = new Price(replacingOrderInfo.RequestedPrice);

        //    cancelReplaceMessage.TimeInForce =
        //        new TimeInForce(replacingOrderInfo.TimeInForce == Contracts.TimeInForce.Day
        //                            ? TimeInForce.DAY
        //                            : TimeInForce.IMMEDIATE_OR_CANCEL);


        //    outgoingFixMessageRaw = cancelReplaceMessage.ToString();

        //    return QuickFix.Session.SendToTarget(cancelReplaceMessage, this.SessionId);
        //}

        //public bool OrderStatusReportingSupported
        //{
        //    get { return true; }
        //}

        //public bool SendOrderStatusRequest(OrderRequestInfo originalOrderInfo, string originalIdentity, string operationIdentity)
        //{
        //    var orderStatusRequestMessage = new OrderStatusRequest();

        //    orderStatusRequestMessage.ClOrdID = new ClOrdID(originalIdentity);
        //    orderStatusRequestMessage.OrdStatusReqID = new OrdStatusReqID(operationIdentity);

        //    string lmaxSymbolId;
        //    if (!this.TryGetLmaxIdForSymbol(originalOrderInfo.Symbol, out lmaxSymbolId))
        //    {
        //        this.Logger.Log(LogLevel.Fatal, "There is no internal mapping to lmax symbol id for symbol {0}", originalOrderInfo.Symbol);
        //        return false;
        //    }
        //    orderStatusRequestMessage.SecurityID = new SecurityID(lmaxSymbolId);
        //    orderStatusRequestMessage.SecurityIDSource = new SecurityIDSource(SecurityIDSource.EXCHANGE_SYMBOL);

        //    orderStatusRequestMessage.Side = originalOrderInfo.Side.ToQuickFixSide();

        //    try
        //    {
        //        return QuickFix.Session.SendToTarget(orderStatusRequestMessage, this.SessionId);
        //    }
        //    catch (Exception e)
        //    {
        //        this.Logger.LogException(LogLevel.Fatal, e, "Adapter for {0} failed to send OrderStatusRequest message for order {1}", this.Counterparty, originalIdentity);
        //    }

        //    return false;
        //}

        //public bool DealRejectionSupported { get { return false; } }

        public bool TryRejectDeal(AutoRejectableDeal rejectableDeal,
            AutoRejectableDeal.RejectableTakerDealRejectionReason rejectionReason)
        {
            throw new NotImplementedException();
        }

        public bool TryAcceptDeal(AutoRejectableDeal rejectableDeal)
        {
            throw new NotImplementedException();
        }

        //public TimeSpan MaximumDealConfirmationDelay { get { return TimeSpan.Zero; } }


        private sealed class LMAXExecReportCracker : VenueExecReportCracker
        {
            public LMAXExecReportCracker(IOrdersParentChannel parentChannel, FIXChannel_LMX lmxChannel, ILogger logger)
                : base(parentChannel, logger, false)
            {
                this._lmxChannel = lmxChannel;
            }

            private FIXChannel_LMX _lmxChannel;

            protected override bool IsIoCMiss
            {
                get { return this._timeInForce == TimeInForce.IMMEDIATE_OR_CANCEL; }
            }

            protected override void ConsumeNonstandardFixTag(int tagId, BufferSegmentBase tagValueSegment)
            {
                switch (tagId)
                {
                    case 48:
                        OnTag48(tagValueSegment);
                        break;
                }
            }

            protected override void ResetState()
            {
                this._symbol = Common.Symbol.NULL;
            }

            protected override FlowSide? GetFlowSide()
            {
                return null;
            }

            protected override int GetMissingTag()
            {
                if (this._symbol == Common.Symbol.NULL)
                    return 48;
                else
                    return 0;
            }

            protected override void UpdateInternalStatusIfneeded()
            {
                decimal conversionMultiplier = this._lmxChannel._symbolToLmaxQuantityMultiplier[(int)this._symbol];
                this.MultiplyConvertedSizes(conversionMultiplier);
            }

            protected override ExecutionReportStatus GetTranslatedExecutionReportStatus(char externalExecType)
            {
                switch (externalExecType)
                {
                    case ExecType.NEW:
                        return ExecutionReportStatus.New;
                    case ExecType.TRADE:
                        return ExecutionReportStatus.Trade;
                    case ExecType.REPLACE:
                        return ExecutionReportStatus.Replace;
                    case ExecType.REJECTED:
                        return ExecutionReportStatus.Reject;
                    case ExecType.CANCELLED:
                        return ExecutionReportStatus.Cancel;
                    case ExecType.ORDER_STATUS:
                        return ExecutionReportStatus.OrderStatus;
                    default:
                        return ExecutionReportStatus.Unrecognized;
                }
            }

            private void OnTag48(BufferSegmentBase bufferSegment)
            {
                if (!this._lmxChannel.TryGetSymbolForLmaxId(bufferSegment.ToInt(), out _symbol))
                {
                    string error = string.Format("There is no lmax mapping from symbol id: {0}",
                        bufferSegment.GetContentAsAsciiString());
                    this._logger.Log(LogLevel.Error, error);
                    throw new HaltingTagException(error, 48);
                }
            }
        }

        private sealed class FluentLMAXExecReportCracker : VenueExecReportCracker
        {
            public FluentLMAXExecReportCracker(IOrdersParentChannel parentChannel, FIXChannel_LMX lmxChannel, ILogger logger)
                : base(parentChannel, logger, false)
            {
                this._lmxChannel = lmxChannel;
            }

            private FIXChannel_LMX _lmxChannel;

            protected override FlowSide? GetFlowSide()
            {
                return null;
            }

            protected override ExecutionReportStatus GetTranslatedExecutionReportStatus(char externalExecType)
            {
                switch (externalExecType)
                {
                    case ExecType.PENDING_NEW:
                        return this._parentChannel.IsDivertedViaFluent ? ExecutionReportStatus.FluentNew : ExecutionReportStatus.New;
                    case ExecType.NEW:
                        return ExecutionReportStatus.New;
                    case ExecType.TRADE:
                        return ExecutionReportStatus.Trade;
                    case ExecType.REPLACE:
                        return ExecutionReportStatus.Replace;
                    case ExecType.REJECTED:
                        return ExecutionReportStatus.Reject;
                    case ExecType.CANCELLED:
                        return ExecutionReportStatus.Cancel;
                    case ExecType.ORDER_STATUS:
                        return ExecutionReportStatus.OrderStatus;
                    default:
                        return ExecutionReportStatus.Unrecognized;
                }
            }
        }

        private class LMAXQuoteCracker : IFixMessageCracker, IPriceFilter
        {
            private class PriceFilter
            {
                private readonly int _layersCnt;
                private PriceObjectDimensions[][][] _priceDimensions;
                private SafeTimer _closedOrderSweepTimer;
                private static readonly TimeSpan FILTER_KGT_PRICE_AFTER_CLOSED_SPAN = TimeSpan.FromMilliseconds(5);
                private static readonly TimeSpan FILTER_START_SWEEPING_AFTER_CLOSED_SPAN = TimeSpan.FromMilliseconds(8);

                public PriceFilter(int layersCnt)
                {
                    _layersCnt = layersCnt;
                    _priceDimensions = new PriceObjectDimensions[2][][]
                    {
                        Enumerable.Range(0, layersCnt).Select(i => new PriceObjectDimensions[Kreslik.Integrator.Common.Symbol.ValuesCount]).ToArray(),
                        Enumerable.Range(0, layersCnt).Select(i => new PriceObjectDimensions[Kreslik.Integrator.Common.Symbol.ValuesCount]).ToArray()
                    };
                    this._closedOrderSweepTimer = new SafeTimer(SweepClosedOrders);
                }

                // Our best prices agnostic to the layers num
                // WARNING: We need to SWITCH SIDES of our outgoing orders (as our order on outside bid is actually ask for other cpts, and so also for us in MD feed)
                private OrderObjectDimensions[][] _kgtBestPrices = new OrderObjectDimensions[2][]
                {
                    new OrderObjectDimensions[Kreslik.Integrator.Common.Symbol.ValuesCount],
                    new OrderObjectDimensions[Kreslik.Integrator.Common.Symbol.ValuesCount]
                };

                private bool _hasKgtOrders = false;
                //private DateTime _kgtOrderInvalidBy = DateTime.MinValue;

                internal bool CheckIsChange(PriceSide side, int layerId, Common.Symbol symbol, decimal sizeInContractsInitial, AtomicDecimal convertedPrice)
                {
                    //WARNING! Since using structs -
                    //   if using temp variable, it would then need to be copied back

                    if (_priceDimensions[(int)side][layerId][(int)symbol].Price != convertedPrice
                        ||
                        _priceDimensions[(int)side][layerId][(int)symbol].SizeInContracts != sizeInContractsInitial)
                    {
                        _priceDimensions[(int)side][layerId][(int)symbol].Price = convertedPrice;
                        _priceDimensions[(int)side][layerId][(int)symbol].SizeInContracts = sizeInContractsInitial;
                        return true;
                    }

                    return false;
                }

                internal decimal SizeNominalToBeFilteredAsKgtOwn(PriceSide side, Common.Symbol symbol,
                    AtomicDecimal convertedPrice)
                {
                    if (_hasKgtOrders)
                    {
                        OrderObjectDimensions localOrder = _kgtBestPrices[(int)side][(int)symbol];
                        if (localOrder.Price == convertedPrice)
                        {
                            //here we might want to mark next layer as change - but keeping this on calling code
                            return localOrder.SizeNominal;
                        }
                    }

                    return 0m;
                }

                internal void MarkKgtOrder(IIntegratorOrderExternal order)
                {
                    if (order.Counterparty.TradingTargetType != TradingTargetType.LMAX)
                        return;

                    LmaxCounterparty lmaxCpt = (LmaxCounterparty) order.Counterparty;

                    int layerId = lmaxCpt.OrdinalValueWithinSubcategory;
                    if (layerId < _layersCnt && layerId >= 0)
                        this.MarkNextPriceAsChange(order.Symbol,
                            order.IntegratorDealDirection.ToInitiatingPriceDirection(), layerId);

                    if (this.IsNewBestPrice(order))
                    {
                        _hasKgtOrders = true;
                        //_kgtOrderInvalidBy = DateTime.MaxValue;
                    }
                }

                internal void MarkKgtOrderClosed(IIntegratorOrderExternal order, DateTime counterpartySentUtc)
                {
                    if (_kgtBestPrices[(int)order.OrderRequestInfo.Side.ToInitiatingPriceDirection().ToOppositeSide()][
                        (int) order.OrderRequestInfo.Symbol].Identity == order.Identity)
                    {
                        _kgtBestPrices[(int)order.OrderRequestInfo.Side.ToInitiatingPriceDirection().ToOppositeSide()][
                            (int) order.OrderRequestInfo.Symbol].TimeClosedUtc = counterpartySentUtc;
                        this._closedOrderSweepTimer.Change(FILTER_START_SWEEPING_AFTER_CLOSED_SPAN, Timeout.InfiniteTimeSpan);
                    }
                }

                private void SweepClosedOrders()
                {
                    if (_hasKgtOrders)
                    {
                        DateTime earliestTimeToConsiderOrderLive = HighResolutionDateTime.UtcNow -
                                                                   FILTER_KGT_PRICE_AFTER_CLOSED_SPAN;

                        bool hasKgtOrdersLocal = false;

                        for (int sideId = 0; sideId < 2; sideId++)
                        {
                            for (int symbolIdx = 0; symbolIdx < Common.Symbol.ValuesCount; symbolIdx++)
                            {
                                if (_kgtBestPrices[sideId][symbolIdx].TimeClosedUtc <= earliestTimeToConsiderOrderLive)
                                {
                                    if (_kgtBestPrices[sideId][symbolIdx].TimeClosedUtc > DateTime.MinValue)
                                    {
                                        _kgtBestPrices[sideId][symbolIdx].TimeClosedUtc = DateTime.MinValue;
                                        _kgtBestPrices[sideId][symbolIdx].SizeNominal = 0;
                                        _kgtBestPrices[sideId][symbolIdx].Price = 0;
                                    }
                                }
                                else
                                    hasKgtOrdersLocal = true;
                            }
                        }

                        _hasKgtOrders = hasKgtOrdersLocal;
                    }
                }

                internal void MarkNextPriceAsChange(Common.Symbol symbol, PriceSide side, int layerId)
                {
                    _priceDimensions[(int)side][layerId][(int)symbol].Nullify();
                }

                private struct PriceObjectDimensions
                {
                    internal AtomicDecimal Price { get; set; }
                    //Warning - this is converted size (in contracts)
                    internal decimal SizeInContracts { get; set; }

                    internal void Nullify()
                    {
                        this.Price = AtomicDecimal.Zero;
                        this.SizeInContracts = 0m;
                    }
                }


                private bool IsOldPriceBetterOrZero(AtomicDecimal oldPrice, AtomicDecimal newPrice, DealDirection integratorDealDircetion)
                {
                    return oldPrice == AtomicDecimal.Zero || oldPrice.IsBetterPrice(newPrice, integratorDealDircetion);
                }

                private bool IsNewBestPrice(IIntegratorOrderExternal order)
                {
                    OrderObjectDimensions localOrder =
                        _kgtBestPrices[(int) order.OrderRequestInfo.Side.ToInitiatingPriceDirection().ToOppositeSide()][
                            (int) order.OrderRequestInfo.Symbol];

                    bool isNewBestPrice = localOrder.Identity == null;

                    if (!isNewBestPrice)
                    {
                        //if old price is better from our view, then new is better from LMAX view
                        isNewBestPrice = this.IsOldPriceBetterOrZero(localOrder.Price,
                            new AtomicDecimal(order.OrderRequestInfo.RequestedPrice), order.OrderRequestInfo.Side);
                    }

                    if (isNewBestPrice)
                    {
                        localOrder.Identity = order.Identity;
                        localOrder.Price = new AtomicDecimal(order.OrderRequestInfo.RequestedPrice);
                        localOrder.TimeClosedUtc = DateTime.MaxValue;
                        localOrder.SizeNominal = order.OrderRequestInfo.SizeBaseAbsInitial;

                        //it is a struct, so must copy back
                        _kgtBestPrices[(int)order.OrderRequestInfo.Side.ToInitiatingPriceDirection().ToOppositeSide()][
                            (int) order.OrderRequestInfo.Symbol] = localOrder;
                    }

                    return isNewBestPrice;
                }

                private struct OrderObjectDimensions
                {
                    internal AtomicDecimal Price { get; set; }
                    //Warning - this is converted size (in contracts)
                    internal decimal SizeNominal { get; set; }

                    internal string Identity { get; set; }
                    internal DateTime TimeClosedUtc { get; set; }

                    internal void Nullify()
                    {
                        this.Price = AtomicDecimal.Zero;
                        this.SizeNominal = 0m;
                        this.TimeClosedUtc = DateTime.MaxValue;
                    }
                }
            }


            private IMarketDataPerentChannel _parentChannel;
            private FIXChannel_LMX _lmaxChannel;
            private ILogger _logger;
            private DateTime _currentMessageReceived;
            private QuoteObject _sharedQuote;
            private Counterparty[] _lmaxLayersCounterparties;
            private int _expectedLayersCount;
            private PriceFilter _priceFilter;
            private VenueDealObjectInternal _tickerObject = VenueDealObjectInternal.GetNewUninitializedObject();

            public LMAXQuoteCracker(IMarketDataPerentChannel parentChannel, FIXChannel_LMX lmaxChannel, Counterparty[] lmaxLayersCounterparties, ILogger logger)
            {
                this._parentChannel = parentChannel;
                this._lmaxChannel = lmaxChannel;
                this._logger = logger;
                this._sharedQuote = parentChannel.PriceReceivingHelper.SharedQuote;
                this._lmaxLayersCounterparties = lmaxLayersCounterparties;
                this._expectedLayersCount = lmaxLayersCounterparties.Length;
                //this._baseCptLayer = (int)lmaxLayersCounterparties[0];
                this._priceFilter = new PriceFilter(lmaxLayersCounterparties.Length);
            }

            private const byte _recognizedType = (byte)'W';

            public byte RecognizedMessageType
            {
                get { return _recognizedType; }
            }

            public bool TryRecoverFromParsingErrors
            {
                get { return false; }
            }

            public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
            {
                switch (tagId)
                {
                    case 48:
                        OnTag48(tagValueSegment);
                        break;
                    case 262:
                        OnTag262(tagValueSegment);
                        break;
                    //case 268:
                    //    OnTag268(tagValueSegment);
                    //    break;
                    case 269:
                        OnTag269(tagValueSegment);
                        break;
                    case 270:
                        OnTag270(tagValueSegment);
                        break;
                    case 271:
                        OnTag271(tagValueSegment);
                        break;
                    case 272:
                        OnTag272(tagValueSegment);
                        break;
                    case 273:
                        OnTag273(tagValueSegment);
                        break;
                    case 52:
                        OnTag52(tagValueSegment);
                        break;
                }
            }

            private Common.Symbol _symbol = Common.Symbol.NULL;
            private string _subscriptionIdentity = string.Empty;
            private decimal _size = 0;
            private AtomicDecimal _price = 0;
            private DateTime _sendingTime = DateTime.MinValue;
            private DateTime _transactionTime = DateTime.MinValue;
            private bool? _isBid = null;
            private int _asksNum = 0;
            private int _bidsNum = 0;
            private bool _isTrade;

            public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
            {
                this._currentMessageReceived = currentMessageReceivedTimeUtc;
                this._symbol = Common.Symbol.NULL;
                this._subscriptionIdentity = null;
                this._sendingTime = DateTime.MinValue;
                this._asksNum = 0;
                this._bidsNum = 0;
                this._isBid = null;
                this._isTrade = false;
                this.ResetTier();
            }

            private void ResetTier()
            {
                this._price = AtomicDecimal.Zero;
                this._size = 0;
                this._transactionTime = DateTime.MinValue;
            }

            //private readonly int _baseCptLayer ;
            void IPriceFilter.HandleDealingOnPrice(IIntegratorOrderExternal order)
            {
                _priceFilter.MarkKgtOrder(order);
            }

            void IPriceFilter.HandleOrderNotActive(IIntegratorOrderExternal order, DateTime counterpartySentTimeUtc)
            {
                _priceFilter.MarkKgtOrderClosed(order, counterpartySentTimeUtc);
            }

            public void MessageParsingDone()
            {
                if (this._symbol == Common.Symbol.NULL || (this._subscriptionIdentity == null && !this._isTrade))
                    throw new HaltingTagException("Incomplete message - missing tag", this._symbol == Common.Symbol.NULL ? 48 : 262);

                //if(!_isBid.HasValue) - is rendundant - checked inside.
                // and if set it means that at least one layer was sent
                this.ParseSidedLayer();

                if(this._isTrade)
                    return;

                //remove prices for tiers that wasn't priced in this message
                for (int bidId = this._bidsNum; bidId < this._expectedLayersCount; bidId++)
                {
                    this._priceFilter.MarkNextPriceAsChange(this._symbol, PriceSide.Bid, bidId);
                    this._parentChannel.SendLastPriceCancel(this._symbol, this._lmaxLayersCounterparties[bidId], PriceSide.Bid);
                }

                for (int askId = this._asksNum; askId < this._expectedLayersCount; askId++)
                {
                    this._priceFilter.MarkNextPriceAsChange(this._symbol, PriceSide.Ask, askId);
                    this._parentChannel.SendLastPriceCancel(this._symbol, this._lmaxLayersCounterparties[askId], PriceSide.Ask);
                }
            }

            private void ParseSidedLayer()
            {
                if (!_isBid.HasValue && !this._isTrade)
                    return;

                if (this._symbol == Common.Symbol.NULL || (this._subscriptionIdentity == null && !this._isTrade))
                    throw new HaltingTagException("Incomplete message - missing tag", this._symbol == Common.Symbol.NULL ? 48 : 262);

                if (this._isTrade)
                {
                    decimal sizeMultiplier = this._lmaxChannel._symbolToLmaxQuantityMultiplier[(int)_symbol];
                    decimal nominalSize = this._size * sizeMultiplier;

                    _tickerObject.OverrideContent(this._symbol, this._parentChannel.Counterparty, nominalSize > 0 ? TradeSide.BuyPaid : TradeSide.SellGiven,
                        this._price.ToDecimal(), Math.Abs(nominalSize),
                        this._transactionTime, this._sendingTime, HighResolutionDateTime.UtcNow, null);

                    this._lmaxChannel.RaiseNewVenueDeal(_tickerObject);

                    return;
                }

                int layerCnt = this._isBid.Value ? this._bidsNum : this._asksNum;

                if (layerCnt > this._expectedLayersCount)
                {
                    this._logger.Log(LogLevel.Error, "Receiving higher number of layers then expected");
                    return;
                }

                if (this._size == 0m || this._price == AtomicDecimal.Zero)
                {
                    this._parentChannel.SendLastPriceCancel(this._symbol, this._lmaxLayersCounterparties[layerCnt - 1],
                        this._isBid.Value ? PriceSide.Bid : PriceSide.Ask);
                    return;
                }

                if (this._priceFilter.CheckIsChange(this._isBid.Value ? PriceSide.Bid : PriceSide.Ask, layerCnt - 1,
                    this._symbol, this._size, this._price))
                {
                    decimal sizeMultiplier = this._lmaxChannel._symbolToLmaxQuantityMultiplier[(int) _symbol];
                    decimal nominalSize = this._size*sizeMultiplier;

                    if (layerCnt == 1)
                    {
                        decimal sizeToBeFiltered =
                            this._priceFilter.SizeNominalToBeFilteredAsKgtOwn(
                                this._isBid.Value ? PriceSide.Bid : PriceSide.Ask, this._symbol, this._price);

                        if (sizeToBeFiltered > 0)
                        {
                            var priceObjectNonEx = this._parentChannel.PriceReceivingHelper.GetPriceObject(
                                priceConverted: this._price,
                                sizeBaseAbsInitial: Math.Min(sizeToBeFiltered, nominalSize),
                                side: this._isBid.Value ? PriceSide.Bid : PriceSide.Ask,
                                symbol: this._symbol,
                                counterparty: this._lmaxLayersCounterparties[layerCnt - 1],
                                counterpartyIdentity: null,
                                marketDataRecordType: MarketDataRecordType.KgtOwnOrderOnVenue,
                                integratorReceivedTime: /*this._currentMessageReceived*/ HighResolutionDateTime.UtcNow,
                                counterpartySentTime: this._sendingTime);

                            this._parentChannel.SendNewNonexecutablePrice(priceObjectNonEx);

                            nominalSize -= sizeToBeFiltered;

                            if (nominalSize <= 0 
                                //we do not care about the layer +1 in case there is no +1 layer
                                && layerCnt < this._expectedLayersCount)
                            {
                                //now we know that we will be removing L01 price, so wee need to be sure that L02 is not removed so it can replace it
                                this._priceFilter.MarkNextPriceAsChange(this._symbol, this._isBid.Value ? PriceSide.Bid : PriceSide.Ask, layerCnt);
                                return;
                            }
                        }
                    }

                    var priceObject = this._parentChannel.PriceReceivingHelper.GetPriceObject(
                        priceConverted: this._price,
                        sizeBaseAbsInitial: nominalSize,
                        side: this._isBid.Value ? PriceSide.Bid : PriceSide.Ask,
                        symbol: this._symbol,
                        counterparty: this._lmaxLayersCounterparties[layerCnt - 1],
                        counterpartyIdentity: null,
                        marketDataRecordType: MarketDataRecordType.VenueNewOrder,
                        integratorReceivedTime: /*this._currentMessageReceived*/ HighResolutionDateTime.UtcNow,
                        counterpartySentTime: this._sendingTime);

                    this._parentChannel.SendNewPrice(priceObject, this._subscriptionIdentity);
                }
            }

            private void OnTag48(BufferSegmentBase bufferSegment)
            {
                if (!this._lmaxChannel.TryGetSymbolForLmaxId(bufferSegment.ToInt(), out _symbol))
                {
                    string error = string.Format("There is no lmax mapping from symbol id: {0}",
                        bufferSegment.GetContentAsAsciiString());
                    this._logger.Log(LogLevel.Error, error);
                    throw new HaltingTagException(error, 48);
                }
            }

            private void OnTag262(BufferSegmentBase bufferSegment)
            {
                this._subscriptionIdentity = this._parentChannel.TryGetSubscriptionIdentity(bufferSegment);
            }

            //private void OnTag268(BufferSegmentBase bufferSegment)
            //{
            //    if (bufferSegment.ToInt() > 4)
            //    {
            //        string error = string.Format("Received quote with unexpected number of layers: {0}",
            //            bufferSegment.GetContentAsAsciiString());
            //        this._logger.Log(LogLevel.Error, error);
            //        throw new HaltingTagException(error, 268);
            //    }
            //}


            //////////////
            // version for multiple tiers where bid and ask repeats
            //         LMAX sends all bids and then all asks
            /////////////
            //WARNING: LMAX sends all bids and then all asks
            private void OnTag269(BufferSegmentBase bufferSegment)
            {
                //if this is consequent layer - parse the previous
                if (this._isBid.HasValue || this._isTrade)
                {
                    this.ParseSidedLayer();
                    this.ResetTier();
                }

                if (bufferSegment[0] == '0')
                {
                    this._isBid = true;
                    this._bidsNum++;
                }
                else if (bufferSegment[0] == '1')
                {
                    this._isBid = false;
                    this._asksNum++;
                }
                else if (bufferSegment[0] == '2')
                {
                    this._isTrade = true;
                }
                else
                {
                    this._isBid = null;
                    
                }
            }

            private void OnTag270(BufferSegmentBase bufferSegment)
            {
                if (this._isBid.HasValue || this._isTrade)
                {
                    this._price = bufferSegment.ToAtomicDecimal();
                }
            }

            private void OnTag271(BufferSegmentBase bufferSegment)
            {
                if (this._isBid.HasValue || this._isTrade)
                {
                    this._size = bufferSegment.ToDecimal();
                }
            }

            private void OnTag272(BufferSegmentBase bufferSegment)
            {
                this._transactionTime = bufferSegment.ToDateOnly() + this._transactionTime.TimeOfDay;
            }

            private void OnTag273(BufferSegmentBase bufferSegment)
            {
                this._transactionTime = this._transactionTime.Date + bufferSegment.ToTimeOnly();
            }

            private void OnTag52(BufferSegmentBase bufferSegment)
            {
                this._sendingTime = bufferSegment.ToDateTime();
            }
        }
    }
}
