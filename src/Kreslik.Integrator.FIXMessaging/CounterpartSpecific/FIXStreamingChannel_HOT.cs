﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX42;
using SessionState = Kreslik.Integrator.Contracts.SessionState;
using Symbol = Kreslik.Integrator.Common.Symbol;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal interface IHotspotStreamingChannel
    {
        IRiskManager RiskManager { get; }
        event FIXStreamingChannelPricingAndOrders_HOT.ConfirmingInfoDeliveredHandler ConfirmingInfoDelivered;
        void OnLLConfirmationTimeout(FIXStreamingChannelPricingAndOrders_HOT.RejectableMMDeal_Hotspot timeoutedDeal);
        void OnAccepteDealRejectedByECN(FIXStreamingChannelPricingAndOrders_HOT.RejectableMMDeal_Hotspot rejectedDeal);
    }

    public class FIXStreamingChannelPricingAndOrders_HOT : FIXStreamingChannelBase<FIXStreamingChannelPricingAndOrders_HOT>,
        IFIXStreamingChannelPricing, IFIXStreamingChannelOrders, IOrdersParentChannel, IHotspotStreamingChannel
    {
        private IRiskManager _riskManager;
        private FIXChannel_HOTSettings _settings;
        private ISymbolsInfo _symbolsInfo;

        IRiskManager IHotspotStreamingChannel.RiskManager { get { return this._riskManager; } }
        public event ConfirmingInfoDeliveredHandler ConfirmingInfoDelivered;

        void IHotspotStreamingChannel.OnLLConfirmationTimeout(
            FIXStreamingChannelPricingAndOrders_HOT.RejectableMMDeal_Hotspot timeoutedDeal)
        {
            string error =
                string.Format(
                    "Timeouted before getting confirmation to deal [{0}] in [{1}] status. Treating it as not done (THIS SHOULD BE RECONFIRMED WITH HOTSPOT). {2}",
                    timeoutedDeal.Identity, timeoutedDeal.CurrentDealStatus, timeoutedDeal);

            this.TurnOfSessionDueToError(error);

            if (timeoutedDeal.DealExecuted)
            {
                if (this.NewCounterpartyRejectedDeal != null)
                    this.NewCounterpartyRejectedDeal(timeoutedDeal.Symbol, null, timeoutedDeal.CounterpartyExecutionId, timeoutedDeal.IntegratorQuoteIdentifier);
            }
        }

        void IHotspotStreamingChannel.OnAccepteDealRejectedByECN(
            FIXStreamingChannelPricingAndOrders_HOT.RejectableMMDeal_Hotspot timeoutedDeal)
        {
            string error =
                string.Format(
                    "ECN sending force rejection to deal [{0}] in [{1}] status. So checking its status and reverting if needed. {2}",
                    timeoutedDeal.Identity, timeoutedDeal.CurrentDealStatus, timeoutedDeal);

            this.Logger.Log(LogLevel.Fatal, error);

            if (timeoutedDeal.DealExecuted)
            {
                if (this.NewCounterpartyRejectedDeal != null)
                    this.NewCounterpartyRejectedDeal(timeoutedDeal.Symbol, null, timeoutedDeal.CounterpartyExecutionId, timeoutedDeal.IntegratorQuoteIdentifier);
            }
        }

        public FIXStreamingChannelPricingAndOrders_HOT(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings, IRiskManager riskManager, ISymbolsInfo symbolsInfo, FIXChannel_HOTSettings settings)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._riskManager = riskManager;
            this._symbolsInfo = symbolsInfo;
            this.OnStopped += InformAllPricesAreCanceled;
            this.ExpectedPricingDepth = -1;
            this._settings = settings;

            if (!settings.DealConfirmationSupported)
            {
                this.Logger.Log(LogLevel.Fatal, "Attempt to start streaming channel for Hotspot session not allowed to be LL!");
                throw new Exception("Attempt to start streaming channel for Hotspot session not allowed to be LL!");
            }

            this.SetQuickMessageParsers(false,
                new HotspotExecReportCracker(this, null, true, logger),
                new QuoteAckCracker(this, logger)); //VenueCancelRejectCracker - likely not needed

            //this.IsSessionOpen = true;
            this.OnStarted += () => this.IsSessionOpen = true;
        }

        protected override void OnOutgoingMessage(QuickFix.Message message, SessionID sessionId)
        {
            if (!this.IsDivertedViaFluent)
            {
                message.SetField(new SenderSubID(this._settings.SenderSubId));
            }
        }

        protected override void OnOutgoingAdminMessage(QuickFix.Message message, QuickFix.SessionID sessionID)
        {
            if (!this.IsDivertedViaFluent)
            {
                if (message is Logon)
                {
                    message.SetField(new Username(this._settings.Username));
                    message.SetField(new Password(this._settings.Password));
                }

                message.SetField(new SenderSubID(this._settings.SenderSubId));
            }
        }

        public override void SendTradingStatus(bool open)
        {
            if (IsSessionOpen && !open && this.State == SessionState.Running)
            {
                this.CancelAllPrices();
            }

            //do not set to open unless session is running
            if (!open || this.State == SessionState.Running)
                this.IsSessionOpen = open;
        }

        private void InformAllPricesAreCanceled()
        {
            if (this.NewPriceCancelSent != null)
            {
                foreach (Symbol symbol in Symbol.Values)
                {
                    StreamingPriceCancel priceCancel = new StreamingPriceCancel(symbol, this.Counterparty, null, null);
                    priceCancel.SetSentPriceCancelInfo(HighResolutionDateTime.UtcNow);
                    this.NewPriceCancelSent(priceCancel);
                }
            }
        }

        private void CancelAllPrices()
        {
            if (this.NewPriceCancelSent != null)
            {

                foreach (Symbol symbol in Symbol.Values)
                {
                    if (this.CheckTargetSymbolSupported(_symbolsInfo, symbol))
                    {
                        StreamingPriceCancel priceCancel = new StreamingPriceCancel(symbol, this.Counterparty, null,
                            null);
                        this.CancelPriceStreams(priceCancel);
                    }
                }
            }
        }

        public event Action<StreamingPrice> NewPriceSent;
        public event Action<StreamingPriceCancel> NewPriceCancelSent;
        public event Action<Symbol, bool> SubscriptionChanged;
        public void CancelPriceStreams(StreamingPriceCancel streamingPriceCancel)
        {
            Quote q = new Quote();
            q.QuoteID = new QuoteID("Cancellation_Quote");
            q.Symbol = streamingPriceCancel.Symbol.ToQuickFixSymbol();

            if (streamingPriceCancel.Side == null || streamingPriceCancel.Side.Value == PriceSide.Bid)
            {
                q.BidPx = new BidPx(0);
                q.BidSize = new BidSize(0);
            }

            if (streamingPriceCancel.Side == null || streamingPriceCancel.Side.Value == PriceSide.Ask)
            {
                q.OfferPx = new OfferPx(0);
                q.OfferSize = new OfferSize(0);
            }

            int minLayerId = streamingPriceCancel.PriceProducerIndex.HasValue
                ? streamingPriceCancel.PriceProducerIndex.Value.LayerId + 1
                : 1;
            int maxLayerId = streamingPriceCancel.PriceProducerIndex.HasValue
                ? streamingPriceCancel.PriceProducerIndex.Value.LayerId + 1
                : this.ExpectedPricingDepth;

            for (int layerId = minLayerId; layerId <= maxLayerId; layerId++)
            {
                q.SetField(new IntField(7225, layerId));
                this.SendMessage(q);
                if (this.NewPriceCancelSent != null)
                {
                    streamingPriceCancel.SetSentPriceCancelInfo(HighResolutionDateTime.UtcNow);
                    this.NewPriceCancelSent(streamingPriceCancel);
                }
            }
        }

        public bool IsSubscribed(Symbol symbol)
        {
            //we might want to control ISymbolSettings here for allowed symbols
            return true;
        }

        public int ExpectedPricingDepth { get; set; }
        public SubmissionResult SendOrder(StreamingPrice streamingPrice)
        {
            Quote q = new Quote();
            q.QuoteID = new QuoteID(streamingPrice.IntegratorPriceIdentity);
            q.Symbol = streamingPrice.Symbol.ToQuickFixSymbol();
            //1 based ordering
            q.SetField(new IntField(7225, streamingPrice.LayerId + 1));

            if (streamingPrice.Side == PriceSide.Bid)
            {
                q.BidPx = new BidPx(streamingPrice.Price);
                q.BidSize = new BidSize(streamingPrice.SizeBaseAbs);
            }
            else if (streamingPrice.Side == PriceSide.Ask)
            {
                q.OfferPx = new OfferPx(streamingPrice.Price);
                q.OfferSize = new OfferSize(streamingPrice.SizeBaseAbs);
            }

            bool sent = this.SendMessage(q);

            if (sent && this.NewPriceSent != null)
            {
                streamingPrice.IntegratorSentTimeUtc = HighResolutionDateTime.UtcNow;
                this.NewPriceSent(streamingPrice);
            }

            return sent ? SubmissionResult.Success : SubmissionResult.Failed_MessagingLayerError;
        }

        public event Action<RejectableMMDeal> NewRejectableDealArrived;
        public event CounterpartyRejectedDealDelegate NewCounterpartyRejectedDeal;
        public event Action<CounterpartyTimeoutInfo> NewCounterpartyTimeoutInfo;
        public event Action<RejectableMMDeal> DealRejected;
        public ICounterpartyRejectionSource CounterpartyRejectionSource { set; private get; }


        private QuickFix.Message CreateExecReportFromDeal(RejectableMMDeal deal, RejectableMMDeal.DealStatus dealStatus)
        {
            RejectableMMDeal_Hotspot hotspotDeal = deal as RejectableMMDeal_Hotspot;

            if (hotspotDeal == null)
            {
                this.Logger.Log(LogLevel.Fatal, "Invalid or null deal type used to confirm/reject deal. {0}", deal);
                return null;
            }

            var execReport = hotspotDeal.ExecutionReport;

            if (execReport == null ||
                execReport.Header.GetField(QuickFix.Fields.Tags.MsgType) !=
                QuickFix.Fields.MsgType.EXECUTIONREPORT)
            {
                this.Logger.Log(LogLevel.Fatal, "Invalid or null execution report used to confirm/reject deal. {0}. Deal: {1}", execReport, deal);
                return null;
            }

            if (dealStatus == RejectableMMDeal.DealStatus.KgtConfirmed)
            {
                execReport.SetField(new ExecType(ExecType.TRADE));
            }
            else
            {
                execReport.SetField(new ExecType(ExecType.REJECTED));
                execReport.SetField(new Text(((RejectableMMDeal.DealRejectionReason)dealStatus).ToString()));
            }

            return execReport;
        }

        public QuickFix.Message CreateAcceptMessage(RejectableMMDeal deal)
        {
            return this.CreateExecReportFromDeal(deal, RejectableMMDeal.DealStatus.KgtConfirmed);
        }

        public bool UpdateAcceptMessageWithRejectRestInfo(RejectableMMDeal deal, QuickFix.Message msg)
        {
            this.Logger.Log(LogLevel.Fatal, "Attempt to create partial reject message on Hotspot adapter (no support for partial fills)");
            return false;
        }

        public QuickFix.Message CreateRejectionMessage(RejectableMMDeal deal, RejectableMMDeal.DealStatus rejectionReason)
        {
            return this.CreateExecReportFromDeal(deal, rejectionReason);
        }

        private void RejectDealInternal(RejectableMMDeal deal, RejectableMMDeal.DealStatus rejectionReason)
        {
            var message = this.CreateRejectionMessage(deal, rejectionReason);
            if (message != null)
                this.SendMessage(message);

            if (this.DealRejected != null)
                this.DealRejected(null);
        }


        public void SendOrderChange(OrderChangeInfo orderChangeInfo)
        {
            orderChangeInfo.Validate(this.Logger, this.Counterparty);

            switch (orderChangeInfo.ChangeEventArgs.ChangeState)
            {
                case IntegratorOrderExternalChange.Filled:
                case IntegratorOrderExternalChange.PartiallyFilled:

                    bool matchedWithPendingAcceptedDeal = false;

                    try
                    {
                        if (this.ConfirmingInfoDelivered != null)
                            this.ConfirmingInfoDelivered(orderChangeInfo, DealConfirmationStatus.ConfirmedAccepted, ref matchedWithPendingAcceptedDeal);
                    }
                    catch (Exception e)
                    {
                        this.Logger.LogException(LogLevel.Fatal,
                            "Unexpected exception during handling accepted confirmation", e);
                    }

                    if (!matchedWithPendingAcceptedDeal)
                    {
                        string error =
                            string.Format(
                                "Received Hotspot confirmation of KGT alleged accept, but there is no unconfirmed deal matching this info! NOT INSERTING the deal info into backend and any statistics. This must be investigated ASAP! {0}",
                                orderChangeInfo);
                        this.TurnOfSessionDueToError(error);
                    }


                    break;
                case IntegratorOrderExternalChange.Rejected:

                    if (orderChangeInfo.ChangeEventArgs.RejectionInfo.RejectionType ==
                        RejectionType.DealRejectByIntegrator)
                    {
                        bool matchedWithPendingRejectedDeal = false;

                        try
                        {
                            if (this.ConfirmingInfoDelivered != null)
                                this.ConfirmingInfoDelivered(orderChangeInfo, DealConfirmationStatus.ConfirmedRejected, ref matchedWithPendingRejectedDeal);
                        }
                        catch (Exception e)
                        {
                            this.Logger.LogException(LogLevel.Fatal, "Unexpected exception during handling reject confirmation", e);
                        }

                        if (!matchedWithPendingRejectedDeal)
                            this.Logger.Log(LogLevel.Fatal, "Received Hotspot confirmation of KGT reject, but there is no unconfirmed deal matching this info. {0}", orderChangeInfo);

                    }
                    else if (orderChangeInfo.ChangeEventArgs.RejectionInfo.RejectionType ==
                             RejectionType.DealRejectByCounterpartyForced)
                    {
                        //try to mark reject internally
                        //upper layer informs all active deals by an event - so even if those are different object it will match based on order id or exec id
                        if (this.NewCounterpartyTimeoutInfo != null)
                            this.NewCounterpartyTimeoutInfo(new CounterpartyTimeoutInfo(orderChangeInfo.IntegratorReceivedTimeUtc, orderChangeInfo.CounterpartySentTimeUtc, orderChangeInfo.RemotePlatformOrderId, orderChangeInfo.ExecutionId));


                        bool matchedWithPendingRejectedDeal = false;

                        try
                        {
                            if (this.ConfirmingInfoDelivered != null)
                                this.ConfirmingInfoDelivered(orderChangeInfo, DealConfirmationStatus.ForceRejected, ref matchedWithPendingRejectedDeal);
                        }
                        catch (Exception e)
                        {
                            this.Logger.LogException(LogLevel.Fatal, "Unexpected exception during handling reject confirmation", e);
                        }

                        if (!matchedWithPendingRejectedDeal)
                            this.Logger.Log(LogLevel.Fatal, "Received Hotspot force reject, but there is no unconfirmed deal matching this info. {0}", orderChangeInfo);

                    }
                    else
                    {
                        this.OnUnexpectedOrderChange(orderChangeInfo);
                    }
                    break;
                case IntegratorOrderExternalChange.NonConfirmedDeal:

                    OrderChangeInfoEx orderChangeInfoEx = orderChangeInfo as OrderChangeInfoEx;

                    if (orderChangeInfoEx == null)
                    {
                        this.TurnOfSessionDueToError(string.Format("Invalid OrderChangeInfo report used to confirm/reject deal. {0}", orderChangeInfo));
                        return;
                    }

                    orderChangeInfoEx.CalculateFixMessageIfNeeded(this.Logger);

                    if (orderChangeInfoEx.Message == null)
                    {
                        this.TurnOfSessionDueToError(string.Format("OrderChangeInfo report doesn't contain the Fix message to confirm/reject deal. {0}", orderChangeInfo));
                    }

                    RejectableMMDeal_Hotspot deal = new RejectableMMDeal_Hotspot(
                        orderChangeInfo.Symbol, this.Counterparty, orderChangeInfo.ChangeEventArgs.ExecutionInfo.IntegratorDealDirection,
                        orderChangeInfo.ChangeEventArgs.ExecutionInfo.UsedPrice.Value,
                        orderChangeInfo.ChangeEventArgs.ExecutionInfo.FilledAmount.Value,
                        orderChangeInfo.RemotePlatformOrderId ?? ExtractHotspotEncodedQuoteId(orderChangeInfo.Identity),
                        orderChangeInfo.ChangeEventArgs.ExecutionInfo.CounterpartyClientId, ExtractHotspotEncodedQuoteId(orderChangeInfo.Identity),
                        orderChangeInfo.CounterpartySentTime.Value, this.CounterpartyRejectionSource,
                        orderChangeInfoEx.Message, orderChangeInfo.SettlementDateLocal, orderChangeInfo.ExecutionId, this);


                    if (this.NewRejectableDealArrived != null)
                    {
                        try
                        {
                            this.NewRejectableDealArrived(deal);
                        }
                        catch (Exception e)
                        {
                            this.TurnOfSessionDueToError("Error during handling new client deal request. Autorejecting it", e);
                            this.RejectDealInternal(deal, RejectableMMDeal.DealStatus.KgtRejected_InternalFailure);
                        }
                    }
                    else
                    {
                        this.TurnOfSessionDueToError("No listener registered to handle client deal request. Autorejecting it");
                        this.RejectDealInternal(deal, RejectableMMDeal.DealStatus.KgtRejected_InternalFailure);
                    }

                    break;
                case IntegratorOrderExternalChange.NotSubmitted:
                case IntegratorOrderExternalChange.Submitted:
                case IntegratorOrderExternalChange.ConfirmedNew:
                case IntegratorOrderExternalChange.PendingCancel:
                case IntegratorOrderExternalChange.Cancelled:
                case IntegratorOrderExternalChange.CancelRejected:
                case IntegratorOrderExternalChange.InBrokenState:
                default:
                    this.OnUnexpectedOrderChange(orderChangeInfo);
                    break;
            }
        }

        private static string ExtractHotspotEncodedQuoteId(string encodedQuoteId)
        {
            //117=181KX1HO62wd1
            //11=181KX1HO62wd1L2S
            return encodedQuoteId.Substring(0, encodedQuoteId.Length - 3);
        }

        private void TurnOfSessionDueToError(string error, Exception e)
        {
            this.Logger.LogException(LogLevel.Fatal, error, e);
            this.StopAsync(TimeSpan.FromMilliseconds(400));
            this._riskManager.TurnOnGoFlatOnly(error);
        }

        private void TurnOfSessionDueToError(string error)
        {
            this.Logger.Log(LogLevel.Fatal, error);
            this.StopAsync(TimeSpan.FromMilliseconds(400));
            this._riskManager.TurnOnGoFlatOnly(error);
        }

        private void OnUnexpectedOrderChange(OrderChangeInfo orderChangeInfo)
        {
            string error = string.Format("Receiving OrderChanged event on LL session with unexpected state - likely an unexpected message was received from counterparty. {0}",
                                 orderChangeInfo);
            this.TurnOfSessionDueToError(error);
        }

        public enum DealConfirmationStatus
        {
            ConfirmedRejected,
            ConfirmedAccepted,
            ForceRejected
        }
        public delegate void ConfirmingInfoDeliveredHandler(OrderChangeInfo orderChangeInfo, DealConfirmationStatus dealConfirmationStatus, ref bool matchedWithPendingDeal);


        internal class RejectableMMDeal_Hotspot : RejectableMMDeal
        {
            private SafeTimer _llConfirmationTimer;
            private IHotspotStreamingChannel _hotspotStreamingChannel;
            internal QuickFix.Message ExecutionReport { get; private set; }

            public RejectableMMDeal_Hotspot(Symbol symbol, Counterparty counterparty,
                DealDirection integratorDealDirection, decimal counterpartyRequestedPrice,
                decimal counterpartyRequestedSizeBaseAbs, string counterpartyClientOrderIdentifier,
                string counterpartyClientIdentifier, string integratorQuoteIdentifier,
                DateTime counterpartySentDealRequestTimeUtc, ICounterpartyRejectionSource counterpartyRejectionSource,
                QuickFix.Message executionReport, DateTime settlementDate, string executionId,
                IHotspotStreamingChannel hotspotStreamingChannel)
                : base(
                    symbol, counterparty, integratorDealDirection, counterpartyRequestedPrice,
                    counterpartyRequestedSizeBaseAbs, counterpartyRequestedSizeBaseAbs, counterpartyClientOrderIdentifier, counterpartyClientIdentifier,
                    integratorQuoteIdentifier, counterpartySentDealRequestTimeUtc, counterpartyRejectionSource)
            {
                this._hotspotStreamingChannel = hotspotStreamingChannel;
                this.ExecutionReport = executionReport;
                this.SetCounterpartyExecutionInfo(settlementDate, executionId);
                this._llConfirmationTimer = new SafeTimer(OnLLConfirmTimeout,
                    SessionsSettings.OrderFlowSessionBehavior.UnconfirmedOrderTimeout, Timeout.InfiniteTimeSpan);
                hotspotStreamingChannel.ConfirmingInfoDelivered += HotspotStreamingChannelOnConfirmingInfoDelivered;
            }

            private void HotspotStreamingChannelOnConfirmingInfoDelivered(OrderChangeInfo orderChangeInfo, DealConfirmationStatus dealConfirmationStatus, ref bool matchedWithPendingDeal)
            {
                if (orderChangeInfo.ExecutionId != this.CounterpartyExecutionId)
                    return;

                if (matchedWithPendingDeal)
                    Common.LogFactory.Instance.GetLogger(null)
                        .Log(LogLevel.Fatal,
                            "Internal deal [{0}] receivnig orderchange info that was already matched with different deal. {1}",
                            this.Identity, orderChangeInfo);

                matchedWithPendingDeal = true;


                if (_hotspotStreamingChannel == null)
                {
                    Common.LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Attempt to double confirm [{0}]. {1}",
                        this.Identity, orderChangeInfo);
                    return;
                }

                bool mismatchedDealConfirmation = false;

                if (dealConfirmationStatus == DealConfirmationStatus.ConfirmedAccepted)
                {
                    if (!this.DealExecuted)
                    {
                        Common.LogFactory.Instance.GetLogger(null)
                        .Log(LogLevel.Fatal,
                            "Received DealAcceptance confirmation for [{0}], but deal is internally in [{1}] status! {2}",
                            this.Identity, this.CurrentDealStatus, orderChangeInfo);
                        mismatchedDealConfirmation = true;
                    }

                    if (this.SizeBaseAbsInitial != orderChangeInfo.ChangeEventArgs.ExecutionInfo.FilledAmount
                        || this.UsedPrice != orderChangeInfo.ChangeEventArgs.ExecutionInfo.UsedPrice)
                    {
                        Common.LogFactory.Instance.GetLogger(null)
                            .Log(LogLevel.Fatal,
                                "Received DealAcceptance confirmation for [{0}], but size or price does not match what KGT confirmed (KGT: {1} at {2}, Cpt: {3} at {4})! {5}",
                                this.Identity, this.SizeBaseAbsInitial, this.UsedPrice,
                                orderChangeInfo.ChangeEventArgs.ExecutionInfo.FilledAmount,
                                orderChangeInfo.ChangeEventArgs.ExecutionInfo.UsedPrice, orderChangeInfo);
                        mismatchedDealConfirmation = true;
                    }
                }
                else if (dealConfirmationStatus == DealConfirmationStatus.ConfirmedRejected || dealConfirmationStatus == DealConfirmationStatus.ForceRejected)
                {
                    if (this.SizeBaseAbsInitial != orderChangeInfo.ChangeEventArgs.RejectionInfo.RejectedAmount)
                    {
                        Common.LogFactory.Instance.GetLogger(null)
                            .Log(LogLevel.Fatal,
                                "Received DealRejected confirmation for [{0}], but size does not match what KGT confirmed (KGT: {1} Cpt: {2})! {3}",
                                this.Identity, this.SizeBaseAbsInitial,
                                orderChangeInfo.ChangeEventArgs.RejectionInfo.RejectedAmount, orderChangeInfo);
                        mismatchedDealConfirmation = true;
                    }

                    if (!this.DealRejected)
                    {
                        if (dealConfirmationStatus == DealConfirmationStatus.ForceRejected && this.DealExecuted)
                        {
                            //IMPORTANT: if force rejected after we accepted then inform upper layer and flat back 
                            this._hotspotStreamingChannel.OnAccepteDealRejectedByECN(this);
                        }
                        else
                        {
                            Common.LogFactory.Instance.GetLogger(null)
                                .Log(LogLevel.Fatal,
                                    "Received DealRejected confirmation for [{0}], but deal is internally in [{1}] status! {2}",
                                    this.Identity, this.CurrentDealStatus, orderChangeInfo);
                            mismatchedDealConfirmation = true;
                        }
                    }
                }

                if (mismatchedDealConfirmation)
                    _hotspotStreamingChannel.RiskManager.TurnOnGoFlatOnly("Mismatched counterparty confirmation of KGT finalized deal");


                _hotspotStreamingChannel.ConfirmingInfoDelivered -= HotspotStreamingChannelOnConfirmingInfoDelivered;
                _hotspotStreamingChannel = null;
                this._llConfirmationTimer.Dispose();
            }

            private void OnLLConfirmTimeout()
            {
                if (this._hotspotStreamingChannel != null)
                    this._hotspotStreamingChannel.OnLLConfirmationTimeout(this);
            }
        }
    }

    internal class QuoteAckCracker : IFixMessageCracker
    {
        private IOrdersParentChannel _parentChannel;
        private ILogger _logger;
        private DateTime _currentMessageReceived;

        public QuoteAckCracker(IOrdersParentChannel parentChannel, ILogger logger)
        {
            this._parentChannel = parentChannel;
            this._logger = logger;
        }

        private const byte _recognizedType = (byte)'b';

        public byte RecognizedMessageType
        {
            get { return _recognizedType; }
        }

        public bool TryRecoverFromParsingErrors
        {
            get { return false; }
        }

        public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
        {
            switch (tagId)
            {
                case 117:
                    OnTag117(tagValueSegment);
                    break;
                case 297:
                    OnTag297(tagValueSegment);
                    break;
                case 300:
                    OnTag300(tagValueSegment);
                    break;
                case 58:
                    OnTag58(tagValueSegment);
                    break;
                case 52:
                    OnTag52(tagValueSegment);
                    break;
                case 122:
                    OnTag122(tagValueSegment);
                    break;
            }
        }

        private string _quoteId;
        private string _freeTextInfo;
        private bool _isRejected;
        private DateTime _sendingTime = DateTime.MinValue;
        private DateTime _origSendingTime = DateTime.MinValue;
        private QuickFixSubsegment _currentMessage;

        public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
        {
            this._currentMessageReceived = currentMessageReceivedTimeUtc;
            this._currentMessage = currentMessage;
            this._quoteId = null;
            this._freeTextInfo = null;
            this._isRejected = true;
            this._sendingTime = DateTime.MinValue;
            this._origSendingTime = DateTime.MinValue;
        }

        public void MessageParsingDone()
        {
            if (this._quoteId == null)
                throw new HaltingTagException("Incomplete message - missing tag", this._quoteId == null ? 41 : 11);

            if (this._parentChannel.IsDivertedViaFluent)
            {
                if (this._origSendingTime == DateTime.MinValue)
                    this._logger.Log(LogLevel.Info,
                        "Session diverted via fluent but OrigSendingTime tag (122) is missing or invalid");
                else
                    this._sendingTime = this._origSendingTime;
            }

            //TODO: handle better
            this._logger.Log(LogLevel.Error, "KGT Quote {0} {1}: {2}", this._quoteId,
                this._isRejected ? "rejected" : "accepted", this._freeTextInfo);
        }

        //QuoteID = 117
        private void OnTag117(BufferSegmentBase bufferSegment)
        {
            this._quoteId = bufferSegment.GetContentAsAsciiString();
        }

        //QuoteAckStatus = 297
        private void OnTag297(BufferSegmentBase bufferSegment)
        {
            int quoteRejReasonId = bufferSegment.ToInt();

            if (quoteRejReasonId == 0)
                this._isRejected = false;
            else if (quoteRejReasonId == 5)
                this._isRejected = true;
            else
                throw new NonHaltingTagException(
                    string.Format("Unrecognized value ({0}) for tag 297 (expected 0 or 5)", quoteRejReasonId), 297);
        }

        //QuoteAckStatus = 300
        private void OnTag300(BufferSegmentBase bufferSegment)
        {
            int quoteRejReasonId = bufferSegment.ToInt();

            this._freeTextInfo = string.Format("Reason code: {0} [{1}]. Description: {2}", quoteRejReasonId,
                    this.MapQuoteRejReasonId(quoteRejReasonId), this._freeTextInfo);
        }

        //Text = 58
        private void OnTag58(BufferSegmentBase bufferSegment)
        {
            if (string.IsNullOrEmpty(this._freeTextInfo))
                this._freeTextInfo = bufferSegment.GetContentAsAsciiString();
            else
                this._freeTextInfo += bufferSegment.GetContentAsAsciiString();
        }

        //OrigSendingTime
        private void OnTag122(BufferSegmentBase bufferSegment)
        {
            this._origSendingTime = bufferSegment.ToDateTime();
        }


        private void OnTag52(BufferSegmentBase bufferSegment)
        {
            try
            {
                this._sendingTime = bufferSegment.ToDateTime();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Error, e, "Couldn't convert value {0} to datetime",
                    bufferSegment.GetContentAsAsciiString());
            }
        }

        private string MapQuoteRejReasonId(int quoteRejReasonId)
        {
            //public const int TOO_LATE_TO_CANCEL = 0;
            //public const int UNKNOWN_ORDER = 1;
            //public const int BROKER = 2;
            //public const int ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS = 3;
            //public const int UNABLE_TO_PROCESS_ORDER_MASS_CANCEL_REQUEST = 4;
            //public const int ORIGORDMODTIME = 5;
            //public const int DUPLICATE_CLORDID = 6;
            //public const int OTHER = 99;

            switch (quoteRejReasonId)
            {
                case 1:
                    return "Unknown Order";
                case 2:
                    return "Symbol Closed";
                case 5:
                    return "Unknown quote";
                case 8:
                    return "Invalid price or quantity";
                case 99:
                    return "Other";
                default:
                    return string.Format("<Unknown QuoteRejectReasonId!: {0}>", quoteRejReasonId);
            }
        }
    }
}
