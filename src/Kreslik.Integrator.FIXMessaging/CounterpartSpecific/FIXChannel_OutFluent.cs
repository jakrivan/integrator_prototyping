﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX44;
using Group = QuickFix.Group;
using Message = QuickFix.Message;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{


    //internal abstract class FIXChannel_OutFluentBasic<TDerivedType> : FIXChannelMessagingBase<TDerivedType>
    //    where TDerivedType : MessageCrackerBase<TDerivedType>
    //{
    //    protected FIXChannel_OutFluentBasic(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
    //        Counterparty counterparty,
    //        UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings)
    //        : base(
    //            persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
    //    {
    //        //force read correct state of IsRedirectedViaFluent
    //        persistedFixConfigsReader.ReadFixConfig();
    //        this._isDivertedViaFluent = persistedFixConfigsReader.IsRedirectedViaFluent;
    //        this._onBehalfOfCompId = persistedFixConfigsReader.FluentCredentials.OnBehalfOfCompId;

    //        if (this._isDivertedViaFluent)
    //            this.ConfigureStateMachineBasicTransitions();
    //    }

    //    protected FIXChannel_OutFluentBasic(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
    //        Counterparty counterparty,
    //        UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings, int pricesPoolSize)
    //        : base(
    //            persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings, pricesPoolSize)
    //    {
    //        persistedFixConfigsReader.ReadFixConfig();
    //        this._isDivertedViaFluent = persistedFixConfigsReader.IsRedirectedViaFluent;
    //        this._onBehalfOfCompId = persistedFixConfigsReader.FluentCredentials.OnBehalfOfCompId;

    //        if (this._isDivertedViaFluent)
    //            this.ConfigureStateMachineBasicTransitions();
    //    }

    //    private readonly bool _isDivertedViaFluent;
    //    //to speed up
    //    private readonly string _onBehalfOfCompId;
    //    public override bool IsDivertedViaFluent { get { return _isDivertedViaFluent; } }

    //    protected override void OnOutgoingMessage(QuickFix.Message message, SessionID sessionId)
    //    {
    //        if (this.IsDivertedViaFluent)
    //        {
    //            //needed?
    //            message.SetField(new OnBehalfOfCompID(this._onBehalfOfCompId));
    //        }
    //    }

    //    protected override void OnOutgoingAdminMessage(QuickFix.Message message, QuickFix.SessionID sessionID)
    //    {
    //        if (this.IsDivertedViaFluent)
    //        {
    //            //probably not needed
    //            if (message.Header.GetField(QuickFix.Fields.Tags.MsgType) == QuickFix.Fields.MsgType.LOGON)
    //            {
    //                //auto populated from fix config
    //                //message.SetField(new DefaultApplVerID("9"));
    //                message.SetField(new Username(this._persistedFixConfigsReader.FluentCredentials.UserName));
    //                message.SetField(new Password(this._persistedFixConfigsReader.FluentCredentials.Password));
    //                //message.SetField(new Username("saxo_kgt1"));
    //                //message.SetField(new Password("k1g2t3"));
    //            }

    //            //probably not needed
    //            message.SetField(new OnBehalfOfCompID(this._onBehalfOfCompId));
    //        }
    //    }

    //    #region FIX message composing

    //    //MD messages by the overriders

    //    protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
    //    {

    //        var orderMessage = new NewOrderSingle();
    //        orderMessage.ClOrdID = new ClOrdID(identity);
    //        orderMessage.Symbol = orderInfo.Symbol.ToQuickFixSymbol();
    //        orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();

    //        orderMessage.Side = orderInfo.Side.ToQuickFixSide();

    //        //Cancel on disconnect
    //        //orderMessage.ExecInst = new ExecInst(ExecInst.CANCEL_ON_SYSTEM_FAILURE);

    //        orderMessage.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);
    //        orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);

    //        orderMessage.Price = new Price(orderInfo.RequestedPrice);

    //        if (orderInfo.OrderType == OrderType.Limit)
    //        {
    //            orderMessage.OrdType = new OrdType(OrdType.LIMIT);

    //            orderMessage.TimeInForce =
    //                new TimeInForce(orderInfo.TimeInForce == Contracts.TimeInForce.Day
    //                    ? TimeInForce.DAY
    //                    : TimeInForce.IMMEDIATE_OR_CANCEL);
    //        }
    //        else if(orderInfo.OrderType == OrderType.Quoted)
    //        {
    //            orderMessage.OrdType = new OrdType(OrdType.PREVIOUSLY_QUOTED);
    //            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);
    //            orderMessage.TimeInForce = new TimeInForce(TimeInForce.FILL_OR_KILL);
    //        }
    //        else
    //        {
    //            this.Logger.Log(LogLevel.Fatal, "Attempt to send [{0}] order {1}. Unsupported - ignoring",
    //                orderInfo.OrderType, identity);
    //            return null;
    //        }

    //        return orderMessage;
    //    }
    //    #endregion /FIX message composing
    //}


    internal abstract class FIXChannel_OutFluent<TDerivedType> : FIXChannelMessagingBase<TDerivedType>, IVenueFIXConnector
        where TDerivedType : MessageCrackerBase<TDerivedType>
    {
        protected FIXChannel_OutFluent(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty,
            UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings)
            : base(
                persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        { }

        protected FIXChannel_OutFluent(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty,
            UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings, int pricesPoolSize)
            : base(
                persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings, pricesPoolSize)
        { }


        protected override bool SupportsLimitOrders
        {
            get { return true; }
        }

        public event Action<VenueDealObjectInternal> NewVenueDeal;

        protected void RaiseNewVenueDeal(VenueDealObjectInternal o)
        {
            if (NewVenueDeal != null)
                NewVenueDeal(o);
        }

        public bool SendCancelOrderMessage(OrderRequestInfo originalOrderInfo, string originalIdentity, string remotePlatformOriginalOrderId,
                                           string cancellationOperationIdentity, out string fixMessageRaw)
        {
            fixMessageRaw = string.Empty;

            QuickFix.Message cancelOrderMessage =
                this.IsDivertedViaFluent
                    ? this.CreateCancelMessageFluent(originalOrderInfo, originalIdentity, remotePlatformOriginalOrderId, cancellationOperationIdentity)
                    : this.CreateCancelMessage(originalOrderInfo, originalIdentity, cancellationOperationIdentity);

            if (cancelOrderMessage == null)
                return false;

            bool successfulySent = false;
            try
            {
                successfulySent = QuickFix.Session.SendToTarget(cancelOrderMessage, this.SessionId);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Error, e, "Adapter for {0} failed to send Cancel message for order {1}", this.Counterparty, originalIdentity);
            }

            if (successfulySent)
            {
                fixMessageRaw = cancelOrderMessage.ToString();
                return true;
            }
            else
            {
                return false;
            }
        }

        protected abstract QuickFix.Message CreateCancelMessage(OrderRequestInfo originalOrderInfo, string originalIdentity, string cancellationOperationIdentity);

        private QuickFix.Message CreateCancelMessageFluent(OrderRequestInfo originalOrderInfo, string originalIdentity, string remotePlatformOriginalOrderId,
                                           string cancellationOperationIdentity)
        {

            var cancelOrderMessage = new OrderCancelRequest();

            cancelOrderMessage.OrigClOrdID = new OrigClOrdID(originalIdentity);
            if (!string.IsNullOrEmpty(remotePlatformOriginalOrderId))
                cancelOrderMessage.OrderID = new OrderID(remotePlatformOriginalOrderId);
            cancelOrderMessage.ClOrdID = new ClOrdID(cancellationOperationIdentity);
            cancelOrderMessage.Symbol = originalOrderInfo.Symbol.ToQuickFixSymbol();
            cancelOrderMessage.Side = originalOrderInfo.Side.ToQuickFixSide();
            cancelOrderMessage.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);

            return cancelOrderMessage;
        }

        public bool CancelAllSupported { get { return false; } }

        public bool SendCancelAllOrdersMessage(string cancellationOperationIdentity, out string fixMessageRaw)
        {
            throw new NotImplementedException();
        }

        public bool SendCancelReplaceOrder(OrderRequestInfo originalOrderInfo, string originalIdentity, string remotePlatformOriginalOrderId,
                                           OrderRequestInfo replacingOrderInfo, string replacingOrderIdentity,
                                           out string outgoingFixMessageRaw)
        {
            outgoingFixMessageRaw = string.Empty;

            QuickFix.Message cancelReplaceMessage =
                this.IsDivertedViaFluent
                    ? this.CreateCancelReplaceMessageFluent(originalOrderInfo, originalIdentity, remotePlatformOriginalOrderId,
                                           replacingOrderInfo, replacingOrderIdentity)
                    : this.CreateCancelReplaceMessage(originalOrderInfo, originalIdentity, replacingOrderInfo, replacingOrderIdentity);

            if (cancelReplaceMessage == null)
                return false;

            outgoingFixMessageRaw = cancelReplaceMessage.ToString();

            return QuickFix.Session.SendToTarget(cancelReplaceMessage, this.SessionId);
        }

        private QuickFix.Message CreateCancelReplaceMessageFluent(OrderRequestInfo originalOrderInfo,
            string originalIdentity, string remotePlatformOriginalOrderId,
            OrderRequestInfo replacingOrderInfo, string replacingOrderIdentity)
        {
            var cancelReplaceMessage = new OrderCancelReplaceRequest();


            cancelReplaceMessage.OrigClOrdID = new OrigClOrdID(originalIdentity);
            if (!string.IsNullOrEmpty(remotePlatformOriginalOrderId))
                cancelReplaceMessage.OrderID = new OrderID(remotePlatformOriginalOrderId);
            cancelReplaceMessage.ClOrdID = new ClOrdID(replacingOrderIdentity);
            cancelReplaceMessage.Symbol = originalOrderInfo.Symbol.ToQuickFixSymbol();
            cancelReplaceMessage.Side = originalOrderInfo.Side.ToQuickFixSide();
            cancelReplaceMessage.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);

            cancelReplaceMessage.OrderQty = new OrderQty(replacingOrderInfo.SizeBaseAbsInitial);
            cancelReplaceMessage.OrdType = new OrdType(OrdType.LIMIT);
            cancelReplaceMessage.Price = new Price(replacingOrderInfo.RequestedPrice);
            if (replacingOrderInfo.SizeBaseAbsFillMinimum > 0)
                cancelReplaceMessage.MinQty = new MinQty(replacingOrderInfo.SizeBaseAbsFillMinimum);

            cancelReplaceMessage.TimeInForce =
                new TimeInForce(replacingOrderInfo.TimeInForce == Contracts.TimeInForce.Day
                                    ? TimeInForce.DAY
                                    : TimeInForce.IMMEDIATE_OR_CANCEL);

            return cancelReplaceMessage;
        }

        protected abstract QuickFix.Message CreateCancelReplaceMessage(OrderRequestInfo originalOrderInfo, string originalIdentity,
                                           OrderRequestInfo replacingOrderInfo, string replacingOrderIdentity);

        public bool OrderStatusReportingSupported
        {
            get { return false; }
        }

        public bool SendOrderStatusRequest(OrderRequestInfo originalOrderInfo, string originalIdentity, string operationIdentity)
        {
            var orderStatusRequestMessage = new OrderStatusRequest();

            orderStatusRequestMessage.ClOrdID = new ClOrdID(originalIdentity);
            orderStatusRequestMessage.OrdStatusReqID = new OrdStatusReqID(operationIdentity);
            orderStatusRequestMessage.Symbol = originalOrderInfo.Symbol.ToQuickFixSymbol();

            orderStatusRequestMessage.Side = originalOrderInfo.Side.ToQuickFixSide();

            try
            {
                return QuickFix.Session.SendToTarget(orderStatusRequestMessage, this.SessionId);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, e, "Adapter for {0} failed to send OrderStatusRequest message for order {1}", this.Counterparty, originalIdentity);
            }

            return false;
        }

        public virtual bool DealRejectionSupported { get { return false; } }

        public virtual bool TryRejectDeal(AutoRejectableDeal rejectableDeal, AutoRejectableDeal.RejectableTakerDealRejectionReason rejectionReason)
        {
            throw new NotImplementedException();
        }
        public virtual bool TryAcceptDeal(AutoRejectableDeal rejectableDeal)
        {
            throw new NotImplementedException();
        }

        public virtual TimeSpan MaximumDealConfirmationDelayWithinIntegrator { get { return TimeSpan.Zero; } }

        public virtual DateTime MaximumDealProcessingCutoff(OrderChangeInfo orderChangeInfo)
        {
            return DateTime.MinValue;
        }
    }
}
