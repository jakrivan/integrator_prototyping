﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX43;
using Message = QuickFix.Message;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal class FIXChannel_FAL : FIXChannel_OutFluent<FIXChannel_FAL>, IVenueFIXConnector
    {
        public FIXChannel_FAL(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            FIXChannel_FALSettings settings, bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new FXallQuoteCracker(this, logger), new[] {(byte) 'X', (byte) 'W'});
            }
            else
                this.SetQuickMessageParsers(isMdChannel, 
                    new FxAllExecReportCracker(this, logger),
                    new VenueCancelRejectCracker(this, logger));
        }

        private FIXChannel_FALSettings _settings;

        protected override bool SupportsLimitOrders
        {
            get { return true; }
        }

        //protected override bool SupportsPeggedOrders
        //{
        //    get { return true; }
        //}


        public event Action<VenueDealObjectInternal> NewVenueDeal;

        protected override void OnOutgoingAdminMessage(QuickFix.Message message, QuickFix.SessionID sessionID)
        {
            if (message is Logon)
            {
                message.SetField(new Username(this._settings.Username));
                message.SetField(new Password(this._settings.Password));
            }
        }

        private SecurityListRequest CreateSecurityListRequest()
        {
            SecurityListRequest securityListRequest =
                new SecurityListRequest(new SecurityReqID(string.Format("SecReq_{0}", Guid.NewGuid())),
                                        new SecurityListRequestType(SecurityListRequestType.ALL_SECURITIES));

            return securityListRequest;
        }

        private SecurityDefinitionRequest CreateSecurityDefinitionRequest(Kreslik.Integrator.Common.Symbol symbol)
        {
            SecurityDefinitionRequest securityDefinitionRequest =
                new SecurityDefinitionRequest(new SecurityReqID(string.Format("SecReq_{0}", Guid.NewGuid())),
                                              new SecurityRequestType(
                                                  SecurityRequestType.REQUEST_SECURITY_IDENTITY_AND_SPECIFICATIONS));

            securityDefinitionRequest.Symbol = symbol.ToQuickFixSymbol();
            return securityDefinitionRequest;
        }

        private SecurityDefinitionRequest CreateSecurityDefinitionRequest(string symbol)
        {
            SecurityDefinitionRequest securityDefinitionRequest =
                new SecurityDefinitionRequest(new SecurityReqID(string.Format("SecReq_{0}", Guid.NewGuid())),
                                              new SecurityRequestType(
                                                  SecurityRequestType.REQUEST_SECURITY_IDENTITY_AND_SPECIFICATIONS));

            securityDefinitionRequest.Symbol = new QuickFix.Fields.Symbol(symbol);
            return securityDefinitionRequest;
        }

        private void GetSymbolDefinitions()
        {
            Session.SendToTarget(this.CreateSecurityListRequest(),
                                        this.SessionId);

            //foreach (Kreslik.Integrator.Common.Symbol symbol in Kreslik.Integrator.Common.Symbol.Values)
            //{
            //    Session.SendToTarget(this.CreateSecurityDefinitionRequest(symbol),
            //                            this.SessionId);
            //} 
        }

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            //GetSymbolDefinitions();
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, true),
                                        this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, false),
                                        this.SessionId);
        }

        private MarketDataRequest CreateSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity,
                                                                           bool subscribe)
        {
            var message = new MarketDataRequest(new MDReqID(subscriptionIdentity),
                                                               new SubscriptionRequestType(subscribe
                                                                                               ? SubscriptionRequestType
                                                                                                     .SNAPSHOT_PLUS_UPDATES
                                                                                               /*SubscriptionRequestType
                                                                                                     .SNAPSHOT*/
                                                                                               : SubscriptionRequestType
                                                                                                     .DISABLE_PREVIOUS),
                                                               new MarketDepth(1));

            // bid:
            var marketDataEntryGroupBid = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupBid.Set(new MDEntryType(MDEntryType.BID));
            message.AddGroup(marketDataEntryGroupBid);

            // offer:
            var marketDataEntryGroupOffer = new MarketDataRequest.NoMDEntryTypesGroup();
            marketDataEntryGroupOffer.Set(new MDEntryType(MDEntryType.OFFER));
            message.AddGroup(marketDataEntryGroupOffer);

            // symbol group:
            var symbolGroup = new MarketDataRequest.NoRelatedSymGroup();
            symbolGroup.Set(subscriptionInfo.Symbol.ToQuickFixSymbol());
            message.AddGroup(symbolGroup);

            return message;
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            var orderMessage = new NewOrderSingle(
                new ClOrdID(identity),
                new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(HighResolutionDateTime.UtcNow),
                new OrdType(OrdType.LIMIT));

            orderMessage.Account = new Account(this._settings.Account);
            orderMessage.FutSettDate = new FutSettDate("SPOT");
            orderMessage.MinQty = new MinQty(orderInfo.SizeBaseAbsFillMinimum);
            orderMessage.Price = new Price(orderInfo.RequestedPrice);
            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.TimeInForce =
                new TimeInForce(orderInfo.TimeInForce == Contracts.TimeInForce.Day
                                    ? TimeInForce.DAY
                                    : TimeInForce.IMMEDIATE_OR_CANCEL);

            return orderMessage;
        }
        #endregion /FIX message composing


        #region FIX message cracking

        public void OnMessage(MarketDataRequestReject m, SessionID s)
        {
            this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(),
                                                                       SubscriptionStatus.Rejected));
        }

        public void OnMessage(MarketDataSnapshotFullRefresh m, SessionID s)
        {
            QuoteObject quote = this.CreateQuoteFromMessage(m);

            if (quote == null)
            {
                this.SendSubscriptionChange(new SubscriptionChangeInfo(m.MDReqID.getValue(), SubscriptionStatus.Broken));
                return;
            }

            if (object.ReferenceEquals(quote, QuoteObject.EMPTY_QUOTE))
            {
                //if (!this.Logger.IsLowDiskSpaceConstraintLoggingOn) this.Logger.Log(LogLevel.Info, "Received indicative quote (ignoring it and cancelling the previous): {0}", QuickFixUtils.FormatMessage(m));
                this.SendLastQuoteCancel(m.MDReqID.getValue());
                return;
            }

            SendNewQuote(quote);
        }

        private const int MaxNumOfMDEntries = 2;

        private QuoteObject CreateQuoteFromMessage(MarketDataSnapshotFullRefresh quoteMessage)
        {
            Kreslik.Integrator.Common.Symbol symbol = (Kreslik.Integrator.Common.Symbol)quoteMessage.Symbol.getValue();
            string subscriptionIdentity = quoteMessage.MDReqID.getValue();

            int mdEntriesNum = quoteMessage.GetInt(Tags.NoMDEntries);

            if (mdEntriesNum > MaxNumOfMDEntries)
            {
                this.Logger.Log(LogLevel.Error, "Received quote with unexpected number of layers: {0}",
                                mdEntriesNum);
                return null;
            }

            decimal bidPrice = 0;
            string bidIdentity = null;
            decimal bidSize = 0;
            decimal askPrice = 0;
            string askIdentity = null;
            decimal askSize = 0;

            for (int groupIndex = 1; groupIndex <= mdEntriesNum; groupIndex += 1)
            {
                Group noMDEntriesGroup = quoteMessage.GetGroup(groupIndex, Tags.NoMDEntries);
                if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.BID)
                {
                    bidPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    bidIdentity = noMDEntriesGroup.GetField(Tags.MDEntryID);
                    if (this.GetIsIndicative(noMDEntriesGroup))
                    {
                        bidSize = 0;
                    }
                    else
                    {
                        bidSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                    }
                }
                else if (noMDEntriesGroup.GetChar(Tags.MDEntryType) == MDEntryType.OFFER)
                {
                    askPrice = noMDEntriesGroup.GetDecimal(Tags.MDEntryPx);
                    askIdentity = noMDEntriesGroup.GetField(Tags.MDEntryID);
                    if (this.GetIsIndicative(noMDEntriesGroup))
                    {
                        askSize = 0;
                    }
                    else
                    {
                        askSize = noMDEntriesGroup.GetDecimal(Tags.MDEntrySize);
                    }
                }
            }

            //Zero sizes are documented way of sending indicative price
            if (bidSize == 0 && askSize == 0)
                return QuoteObject.EMPTY_QUOTE;

            PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSidedIdentity(askPrice: askPrice, askSize: askSize, bidPrice: bidPrice,
                bidSize: bidSize,
                symbol: symbol, counterparty: this.Counterparty,
                subscriptionIdentity: subscriptionIdentity,
                counterpartyTimeUtc:
                    quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                askIdentity: askIdentity,
                bidIdentity: bidIdentity,
                marketDataRecordType: MarketDataRecordType.VenueNewOrder);

            return PriceReceivingHelper.SharedQuote;
        }

        public void OnMessage(MarketDataIncrementalRefresh m, SessionID s)
        {
            int mdEntriesNum = m.GetInt(Tags.NoMDEntries);
            string subscriptionId = m.MDReqID.getValue();

            if (mdEntriesNum > MaxNumOfMDEntries)
            {
                this.Logger.Log(LogLevel.Error, "Received quote with unexpected number of layers: {0}",
                                mdEntriesNum);
                this.SendSubscriptionChange(new SubscriptionChangeInfo(subscriptionId, SubscriptionStatus.Broken));
                return;
            }

            Kreslik.Integrator.Common.Symbol symbol = (Kreslik.Integrator.Common.Symbol) m.GetString(Tags.Symbol);

            for (int groupIndex = 1; groupIndex <= mdEntriesNum; groupIndex += 1)
            {
                Group noMDEntriesGroup = m.GetGroup(groupIndex, Tags.NoMDEntries);

                switch (noMDEntriesGroup.GetChar(Tags.MDUpdateAction))
                {
                    case MDUpdateAction.DELETE:
                        //this.Logger.Log(LogLevel.Info, "Receiving MD Refresh deleting previous quote: {0}",
                        //                QuickFixUtils.FormatMessage(m));
                        this.SendLastQuoteCancel(subscriptionId);
                        return;
                    case MDUpdateAction.NEW:
                    case MDUpdateAction.CHANGE:
                        break;
                    default:
                        this.Logger.Log(LogLevel.Error,
                                    "Receiving (and ignoring) Incremental MD refresh with unexpected update type: {0}",
                                    QuickFixUtils.FormatMessage(m));
                        break;
                }

                if (!noMDEntriesGroup.IsSetField(Tags.MDEntrySize))
                {
                    this.Logger.Log(LogLevel.Error,
                                    "Receiving (and ignoring) MD refresh without size specified: {0}",
                                    QuickFixUtils.FormatMessage(m));
                    return;
                }

                char mdEntryType = noMDEntriesGroup.GetChar(Tags.MDEntryType);
                switch (mdEntryType)
                {
                    case MDEntryType.BID:
                    case MDEntryType.OFFER:
                        var priceObject = this._priceReceivingHelper.GetPriceObject(
                            price: noMDEntriesGroup.GetDecimal(Tags.MDEntryPx),
                            sizeBaseAbsInitial: noMDEntriesGroup.GetDecimal(Tags.MDEntrySize),
                            side: mdEntryType == MDEntryType.BID ? PriceSide.Bid : PriceSide.Ask,
                            symbol: symbol,
                            counterparty: this.Counterparty,
                            counterpartyIdentity: null,
                            marketDataRecordType: MarketDataRecordType.VenueNewOrder,
                            integratorReceivedTime: HighResolutionDateTime.UtcNow,
                            counterpartySentTime: m.Header.ToDateTime(Tags.SendingTime, false, this.Logger));

                        this.SendNewPrice(priceObject, subscriptionId);

                        break;
                    default:
                        this.Logger.Log(LogLevel.Error,
                            "Receiving (and ignoring) MD refresh with unknown MDEntryType: {0}",
                            QuickFixUtils.FormatMessage(m));
                        return;
                }
            }
        }

        private bool GetIsIndicative(Group noMDEntriesGroup)
        {
            if (!noMDEntriesGroup.IsSetField(Tags.QuoteType))
                return false;

            int quoteCondition = noMDEntriesGroup.GetInt(Tags.QuoteType);
            bool indicative = true;

            switch (quoteCondition)
            {
                case QuoteType.TRADEABLE:
                    indicative = false;
                    break;
                case QuoteType.INDICATIVE:
                    this.Logger.Log(LogLevel.Info, "Received indicative side of quote, ignoring the side");
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Received quote side in unsupported condition: {0}. Ignoring side", quoteCondition);
                    break;
            }

            return indicative;
        }

        private const int AGGRESSOR_INDICATOR = 1057;

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            string freeTextInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);

            decimal? rejectedAmount = null;
            decimal? initialQuantity = null;

            char fixOrderStatus = m.OrdStatus.getValue();
            char execType = m.ExecType.getValue();

            string orderId = m.ClOrdID.getValue();

            string rawMessage = m.ToString();
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            switch (execType)
            {
                case ExecType.NEW:
                case ExecType.PENDING_NEW:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] was received and enqeued by other end", orderId);
                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;
                    if (m.IsSetLeavesQty())
                    {
                        initialQuantity = m.LeavesQty.getValue();
                    }
                    break;
                case ExecType.TRADE:
                    decimal? filledAmount;
                    if (fixOrderStatus == OrdStatus.FILLED)
                    {
                        this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                        orderStatus = IntegratorOrderExternalChange.Filled;
                        filledAmount = m.IsSetLastQty() ? m.LastQty.getValue() : m.ToNullableDecimal(Tags.CumQty);
                    }
                    else
                    {
                        if (fixOrderStatus != OrdStatus.PARTIALLY_FILLED)
                        {
                            this.Logger.Log(LogLevel.Fatal,
                                            "Order [{0}] with Trade ExecReport is in unexpected state [{1}]", orderId,
                                            fixOrderStatus);
                        }

                        this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly partially filled", orderId);
                        orderStatus = IntegratorOrderExternalChange.PartiallyFilled;
                        //We intentiaonally don't convert to nullable 
                        //  - as quantity is absolutely required with partial fills
                        filledAmount = m.LastQty.getValue();
                    }

                    FlowSide? flowSide = null;

                    if (m.IsSetField(AGGRESSOR_INDICATOR))
                    {
                        //not getting this as char to prevent quickfixn conversion exception
                        switch (m.GetField(AGGRESSOR_INDICATOR))
                        {
                            //passive trade
                            case "N":
                                flowSide = FlowSide.LiquidityMaker;
                                break;
                            //aggressive trade
                            case "Y":
                                flowSide = FlowSide.LiquidityTaker;
                                break;
                        }
                    }

                    if (flowSide == null)
                    {
                        this.Logger.Log(LogLevel.Error,
                                        "FlowSide (Tag 1057) not specified or invalid in execution message. {0}",
                                        m);
                    }

                    executionInfo = new ExecutionInfo(
                            filledAmount,
                            m.ToNullableDecimal(Tags.LastSpotRate),
                            m.ToNullableString(Tags.ExecID),
                            m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                            m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                            m.GetIntegratorDealDirection()) { FlowSide = flowSide };
                    break;
                case ExecType.REPLACE:
                    if (m.IsSetLastQty() && m.LastQty.getValue() != 0m)
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received replace ExecutionReport, but last shares is nonzero: [{0}]", rawMessage);
                    }

                    //Cancel original order
                    if (m.IsSetOrigClOrdID())
                    {
                        string origClId = m.OrigClOrdID.getValue();

                        this.Logger.Log(LogLevel.Info, "Order [{0}] was replaced and order [{1}] was created and enqeued by other end", origClId, orderId);

                        this.SendOrderChange(new OrderChangeInfo(origClId, symbol, IntegratorOrderExternalChange.Cancelled,
                                                                 counterpartySendTime, null, null,
                                                                 new RejectionInfo(m.ToNullableString(Tags.Text), null,
                                                                                   RejectionType.OrderCancel)));
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received replace ExecutionReport, but OrigClOrdID is not set: [{0}]", rawMessage);
                    }

                    if (m.IsSetLeavesQty())
                    {
                        initialQuantity = m.LeavesQty.getValue();
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Fatal, "Received replace ExecutionReport, but LeavesQty is not set: [{0}]", rawMessage);
                    }

                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;

                    break;
                case ExecType.REJECTED:
                case ExecType.EXPIRED:
                case ExecType.DONE_FOR_DAY:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    freeTextInfo = m.ToNullableString(Tags.Text);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    if (m.IsSetCumQty() && m.IsSetOrderQty())
                    {
                        rejectedAmount = m.OrderQty.getValue() - m.CumQty.getValue();
                    }
                    rejectionInfo = new RejectionInfo(freeTextInfo, rejectedAmount, RejectionType.OrderReject);
                    break;
                case ExecType.CANCELLED:
                    freeTextInfo = m.ToNullableString(Tags.Text);

                    if (m.IsSetOrigClOrdID())
                    {
                        orderId = m.OrigClOrdID.getValue();
                        this.Logger.Log(LogLevel.Info, "Cancel Execution report for order: {0} did have OrigClOrdID populated (as: {1}) so changing orderId to: {1}",
                            m.ClOrdID.getValue(), orderId);
                    }

                    //The only difference between requeste cancel and IoC Cancel is the TimeInForce tag
                    if (m.IsSetTimeInForce() && m.TimeInForce.getValue() == TimeInForce.IMMEDIATE_OR_CANCEL)
                    {
                        this.Logger.Log(LogLevel.Info, "IoC Order [{0}] Rejected", orderId);
                        orderStatus = IntegratorOrderExternalChange.Rejected;
                        freeTextInfo += "<Integrator Supplied Reason: IoC miss>";
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Info, "Order [{0}] Cancelled", orderId);
                        orderStatus = IntegratorOrderExternalChange.Cancelled;
                    }

                    if (m.IsSetCumQty() && m.IsSetOrderQty())
                    {
                        rejectedAmount = m.OrderQty.getValue() - m.CumQty.getValue();
                    }
                    rejectionInfo = new RejectionInfo(freeTextInfo, rejectedAmount, orderStatus == IntegratorOrderExternalChange.Cancelled ? RejectionType.OrderCancel : RejectionType.OrderReject);
                    break;
                case ExecType.PENDING_REPLACE:
                case ExecType.PENDING_CANCEL:
                    if (m.IsSetOrigClOrdID())
                    {
                        orderId = m.OrigClOrdID.getValue();
                        this.Logger.Log(LogLevel.Info, "Execution report for pending state of order: {0} did have OrigClOrdID populated (as: {1}) so changing orderId to: {1}",
                            m.ClOrdID.getValue(), orderId);
                    }
                    this.Logger.Log(LogLevel.Info, "Order [{0}] is in pending cancel state", orderId);
                    orderStatus = IntegratorOrderExternalChange.PendingCancel;
                    break;
                default:
                    this.Logger.Log(LogLevel.Fatal, "Order [{0}] received unsupported ExecType report: [{1}]", orderId, execType);
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                         executionInfo, rejectionInfo, initialQuantity));
        }

        public void OnMessage(OrderCancelReject m, SessionID s)
        {
            IntegratorOrderExternalChange orderStatus = IntegratorOrderExternalChange.CancelRejected;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);

            string cancelOperationId = m.ClOrdID.getValue();
            string orderId = m.OrigClOrdID.getValue();
            string freeTextInfo = m.ToNullableString(Tags.Text);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            if (m.IsSetCxlRejReason())
            {
                int cxlRejReasonId = m.CxlRejReason.getValue();

                freeTextInfo = string.Format("Reason code: {0} [{1}]. Description: {2}", cxlRejReasonId, MapCxlRejReasonId(cxlRejReasonId), freeTextInfo);
            }

            this.Logger.Log(LogLevel.Info, "Request [{0}] to cancell order [{1}] was REJECTED (\"{2}\")", cancelOperationId, orderId, freeTextInfo);


            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     null, null, freeTextInfo));

            //inform upperlayers about rejection of a cancel request (or cancel replace request)
            //this is the place where we flip Cancel/Replace reject into regular reject - so indicate this type of reject
            this.SendOrderChange(new OrderChangeInfo(cancelOperationId, symbol, IntegratorOrderExternalChange.Rejected,
                                                     counterpartySendTime, null, null,
                                                     new RejectionInfo(freeTextInfo, null, RejectionType.CancelReplaceReject)));

            //this way we can get replace request orig client id
            //if (m.CxlRejResponseTo.getValue() == CxlRejResponseTo.ORDER_CANCEL_REPLACE_REQUEST)
            //{
            //    m.OrigClOrdID.getValue()
            //} 
        }

        private string MapCxlRejReasonId(int cxlRejReasonId)
        {
            //public const int TOO_LATE_TO_CANCEL = 0;
            //public const int UNKNOWN_ORDER = 1;
            //public const int BROKER = 2;
            //public const int ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS = 3;
            //public const int UNABLE_TO_PROCESS_ORDER_MASS_CANCEL_REQUEST = 4;
            //public const int ORIGORDMODTIME = 5;
            //public const int DUPLICATE_CLORDID = 6;
            //public const int OTHER = 99;

            switch (cxlRejReasonId)
            {
                case 0:
                    return "Too Late";
                case 1:
                    return "Unknown Order";
                case 3:
                    return "Order Already in Pending Cancel or Pending Replace Status";
                case 6:
                    return "Duplicate ClOrdID Received";
                case 99:
                    return "Other";
                default:
                    return string.Format("<Unknown CxlRejReasonId!: {0}>", cxlRejReasonId);
            }


        }

        public void OnMessage(SecurityDefinition m, SessionID s)
        {
            this.Logger.Log(LogLevel.Info, "Security definition: {0}", m);

            string basisPoit = "?";
            if (m.IsSetField(7103))
            {
                basisPoit = m.GetField(7103);
            }

            string ratePrecision = "?";
            if (m.IsSetField(7104))
            {
                ratePrecision = m.GetField(7104);
            }

            this.Logger.Log(LogLevel.Info, "SymbolSetings - symbol:{0}, BasisPoint:{1}, RatePrecision:{2}", m.Symbol,
                            basisPoit, ratePrecision);
        }

        public void OnMessage(SecurityList m, SessionID s)
        {
            this.Logger.Log(LogLevel.Info, "Security list: {0}", m);


            List<string> supportedSymbols = new List<string>();

            int symbolEntriesNum = m.GetInt(Tags.NoRelatedSym);
            for (int groupIndex = 1; groupIndex <= symbolEntriesNum; groupIndex += 1)
            {
                Group NoRelatedSymGroup = m.GetGroup(groupIndex, Tags.NoRelatedSym);

                string symbol = NoRelatedSymGroup.GetField(Tags.Symbol);
                supportedSymbols.Add(symbol);
            }

            this.Logger.Log(LogLevel.Info, "Supported Symbols: {0}", string.Join(", ", supportedSymbols));

            foreach (string supportedSymbol in supportedSymbols)
            {
                Session.SendToTarget(this.CreateSecurityDefinitionRequest(supportedSymbol),
                                     this.SessionId);
            }
        }

        #endregion /FIX message cracking

        //public bool SendCancelOrderMessage(OrderRequestInfo originalOrderInfo, string originalIdentity, string remotePlatformOriginalOrderId,
        //                                   string cancellationOperationIdentity, out string fixMessageRaw)
        //{
        //    var cancelOrderMessage = new OrderCancelRequest(
        //        new OrigClOrdID(originalIdentity),
        //        new ClOrdID(cancellationOperationIdentity),
        //        originalOrderInfo.Symbol.ToQuickFixSymbol(),
        //        originalOrderInfo.Side.ToQuickFixSide(),
        //        new TransactTime(HighResolutionDateTime.UtcNow));

        //    cancelOrderMessage.SetField(new FutSettDate("SPOT"));


        //    bool successfulySent = false;
        //    try
        //    {
        //        successfulySent = QuickFix.Session.SendToTarget(cancelOrderMessage, this.SessionId);
        //    }
        //    catch (Exception e)
        //    {
        //        this.Logger.LogException(LogLevel.Error, e, "Adapter for {0} failed to send Cancel message for order {1}", this.Counterparty, originalIdentity);
        //    }

        //    if (successfulySent)
        //    {
        //        fixMessageRaw = cancelOrderMessage.ToString();
        //        return true;
        //    }
        //    else
        //    {
        //        fixMessageRaw = string.Empty;
        //        return false;
        //    }
        //}

        protected override Message CreateCancelMessage(OrderRequestInfo originalOrderInfo, string originalIdentity,
            string cancellationOperationIdentity)
        {
            var cancelOrderMessage = new OrderCancelRequest(
                new OrigClOrdID(originalIdentity),
                new ClOrdID(cancellationOperationIdentity),
                originalOrderInfo.Symbol.ToQuickFixSymbol(),
                originalOrderInfo.Side.ToQuickFixSide(),
                new TransactTime(HighResolutionDateTime.UtcNow));

            cancelOrderMessage.SetField(new FutSettDate("SPOT"));

            return cancelOrderMessage;
        }

        //public bool CancelAllSupported 
        //{
        //    get { return false; }
        //}

        //public bool SendCancelAllOrdersMessage(string cancellationOperationIdentity, out string fixMessageRaw)
        //{
        //    throw new NotImplementedException();
        //}

        //public bool SendCancelReplaceOrder(OrderRequestInfo originalOrderInfo, string originalIdentity, string remotePlatformOriginalOrderId,
        //                                   OrderRequestInfo replacingOrderInfo, string replacingOrderIdentity,
        //                                   out string outgoingFixMessageRaw)
        //{
        //    var cancelReplaceMessage = new OrderCancelReplaceRequest(new OrigClOrdID(originalIdentity),
        //                                                             new ClOrdID(replacingOrderIdentity),
        //                                                             new HandlInst(
        //                                                                 HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE),
        //                                                             replacingOrderInfo.Symbol.ToQuickFixSymbol(),
        //                                                             replacingOrderInfo.Side.ToQuickFixSide(),
        //                                                             new TransactTime(HighResolutionDateTime.UtcNow),
        //                                                             new OrdType(OrdType.LIMIT));

        //    cancelReplaceMessage.Account = new Account(this._settings.Account);
        //    cancelReplaceMessage.FutSettDate = new FutSettDate("SPOT");

        //    cancelReplaceMessage.OrderQty = new OrderQty(replacingOrderInfo.SizeBaseAbsInitial);
        //    cancelReplaceMessage.MinQty = new MinQty(replacingOrderInfo.SizeBaseAbsFillMinimum);
        //    cancelReplaceMessage.Price = new Price(replacingOrderInfo.RequestedPrice);

        //    cancelReplaceMessage.TimeInForce =
        //        new TimeInForce(replacingOrderInfo.TimeInForce == Contracts.TimeInForce.Day
        //                            ? TimeInForce.DAY
        //                            : TimeInForce.IMMEDIATE_OR_CANCEL);

        //    outgoingFixMessageRaw = cancelReplaceMessage.ToString();

        //    return QuickFix.Session.SendToTarget(cancelReplaceMessage, this.SessionId);
        //}

        protected override Message CreateCancelReplaceMessage(OrderRequestInfo originalOrderInfo, string originalIdentity,
            OrderRequestInfo replacingOrderInfo, string replacingOrderIdentity)
        {
            var cancelReplaceMessage = new OrderCancelReplaceRequest(new OrigClOrdID(originalIdentity),
                                                                     new ClOrdID(replacingOrderIdentity),
                                                                     new HandlInst(
                                                                         HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE),
                                                                     replacingOrderInfo.Symbol.ToQuickFixSymbol(),
                                                                     replacingOrderInfo.Side.ToQuickFixSide(),
                                                                     new TransactTime(HighResolutionDateTime.UtcNow),
                                                                     new OrdType(OrdType.LIMIT));

            cancelReplaceMessage.Account = new Account(this._settings.Account);
            cancelReplaceMessage.FutSettDate = new FutSettDate("SPOT");

            cancelReplaceMessage.OrderQty = new OrderQty(replacingOrderInfo.SizeBaseAbsInitial);
            cancelReplaceMessage.MinQty = new MinQty(replacingOrderInfo.SizeBaseAbsFillMinimum);
            cancelReplaceMessage.Price = new Price(replacingOrderInfo.RequestedPrice);

            cancelReplaceMessage.TimeInForce =
                new TimeInForce(replacingOrderInfo.TimeInForce == Contracts.TimeInForce.Day
                                    ? TimeInForce.DAY
                                    : TimeInForce.IMMEDIATE_OR_CANCEL);

            return cancelReplaceMessage;
        }

        //public bool OrderStatusReportingSupported
        //{
        //    get { return false; }
        //}

        //public bool SendOrderStatusRequest(OrderRequestInfo originalOrderInfo, string originalIdentity, string operationIdentity)
        //{
        //    throw new NotImplementedException();
        //}

        //public bool DealRejectionSupported { get { return false; } }

        //public bool TryRejectDeal(AutoRejectableDeal rejectableDeal,
        //    AutoRejectableDeal.RejectableTakerDealRejectionReason rejectionReason)
        //{
        //    throw new NotImplementedException();
        //}

        //public bool TryAcceptDeal(AutoRejectableDeal rejectableDeal)
        //{
        //    throw new NotImplementedException();
        //}

        //public TimeSpan MaximumDealConfirmationDelayWithinIntegrator { get { return TimeSpan.Zero; } }

        //public DateTime MaximumDealProcessingCutoff(OrderChangeInfo orderChangeInfo)
        //{
        //    return DateTime.MinValue;
        //}

        private sealed class FxAllExecReportCracker : VenueExecReportCracker
        {
            public FxAllExecReportCracker(IOrdersParentChannel parentChannel, ILogger logger)
                : base(parentChannel, logger, false)
            { }

            private char _aggressorIndicator;

            protected override bool IsIoCMiss
            {
                get { return this._timeInForce == TimeInForce.IMMEDIATE_OR_CANCEL; }
            }

            protected override void ConsumeNonstandardFixTag(int tagId, BufferSegmentBase tagValueSegment)
            {
                switch (tagId)
                {
                    case 194:
                        OnTag194(tagValueSegment);
                        break;
                    case 1057:
                        OnTag1057(tagValueSegment);
                        break;
                }
            }

            protected override void ResetState()
            {
                this._aggressorIndicator = char.MaxValue;
            }

            protected override FlowSide? GetFlowSide()
            {
                FlowSide? flowSide = null;

                //not getting this as char to prevent quickfixn conversion exception
                switch (this._aggressorIndicator)
                {
                    //passive trade
                    case 'N':
                        flowSide = FlowSide.LiquidityMaker;
                        break;
                    //aggressive trade
                    case 'Y':
                        flowSide = FlowSide.LiquidityTaker;
                        break;
                    //intentionaly no default - if tag is missing
                }


                if (flowSide == null)
                {
                    this._logger.Log(LogLevel.Error,
                        "FlowSide (Tag 1057) not specified or invalid in execution message.");
                }

                return flowSide;
            }

            protected override ExecutionReportStatus GetTranslatedExecutionReportStatus(char externalExecType)
            {
                switch (externalExecType)
                {
                    case ExecType.NEW:
                    case ExecType.PENDING_NEW:
                        return ExecutionReportStatus.New;
                    case ExecType.TRADE:
                        return ExecutionReportStatus.Trade;
                    case ExecType.REPLACE:
                        return ExecutionReportStatus.Replace;
                    case ExecType.REJECTED:
                    case ExecType.EXPIRED:
                    case ExecType.DONE_FOR_DAY:
                        return ExecutionReportStatus.Reject;
                    case ExecType.CANCELLED:
                        return ExecutionReportStatus.Cancel;
                    case ExecType.PENDING_REPLACE:
                    case ExecType.PENDING_CANCEL:
                        return ExecutionReportStatus.PendingCancelOrReplace;
                    default:
                        return ExecutionReportStatus.Unrecognized;
                }
            }

            //AggressorIndicator = 1057
            private void OnTag1057(BufferSegmentBase bufferSegment)
            {
                this._aggressorIndicator = (char)bufferSegment[0];
            }

            //LastSpotRate = 194;
            private void OnTag194(BufferSegmentBase bufferSegment)
            {
                this._lastPx = bufferSegment.ToDecimal();
            }
        }

        private class FXallQuoteCracker : IFixMessageCracker
        {
            private IMarketDataPerentChannel _parentChannel;
            private ILogger _logger;
            private DateTime _currentMessageReceived;

            public FXallQuoteCracker(IMarketDataPerentChannel parentChannel, ILogger logger)
            {
                this._parentChannel = parentChannel;
                this._logger = logger;
            }

            //private const byte _recognizedType = (byte)'X' and 'W';

            public byte RecognizedMessageType
            {
                get { throw new NotImplementedException("This class recognizes two message types - X and W"); }
            }

            public bool TryRecoverFromParsingErrors
            {
                get { return false; }
            }

            public void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment)
            {
                switch (tagId)
                {
                    case 262:
                        OnTag262(tagValueSegment);
                        break;
                    case 268:
                        OnTag268(tagValueSegment);
                        break;
                    case 269:
                        OnTag269(tagValueSegment);
                        break;
                    case 270:
                        OnTag270(tagValueSegment);
                        break;
                    case 271:
                        OnTag271(tagValueSegment);
                        break;
                    case 52:
                        OnTag52(tagValueSegment);
                        break;

                    case 278:
                        OnTag278(tagValueSegment);
                        break;
                    case 279:
                        OnTag279(tagValueSegment);
                        break;
                    case 55:
                        OnTag55(tagValueSegment);
                        break;
                    case 537:
                        OnTag537(tagValueSegment);
                        break;
                }
            }

            private Common.Symbol _symbol = Common.Symbol.NULL;
            private string _subscriptionIdentity = string.Empty;
            private AtomicDecimal _price = 0;
            private decimal _size = 0;
            private DateTime _sendingTime = DateTime.MinValue;
            private bool? _isBid = null;
            private readonly byte[] _counterpartyIdentity = new byte[PriceObject.MAX_CTP_IDENTITY_LENGTH];
            private bool _deletePrevious;
            private bool _isIndicative;

            public void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage)
            {
                this._currentMessageReceived = currentMessageReceivedTimeUtc;
                this._symbol = Common.Symbol.NULL;
                this._subscriptionIdentity = null;
                this._sendingTime = DateTime.MinValue;
                this.ResetTier();
            }

            private void ResetTier()
            {
                this._price = 0;
                this._size = 0;
                this._isBid = null;
                this._counterpartyIdentity[0] = 0;
                this._deletePrevious = false;
                this._isIndicative = false;
            }

            public void MessageParsingDone()
            {
                if (this._symbol == Common.Symbol.NULL || this._subscriptionIdentity == null)
                    throw new HaltingTagException("Incomplete message - missing tag", this._symbol == Common.Symbol.NULL ? 55 : 262);

                DateTime receivedAndParsed = this._currentMessageReceived;


                if (this._deletePrevious)
                {
                    //just canceling
                    if (this._isBid.HasValue)
                    {
                        this._parentChannel.SendLastPriceCancel(this._symbol, this._parentChannel.Counterparty,
                            this._isBid.Value ? PriceSide.Bid : PriceSide.Ask);
                    }
                    else
                    {
                        this._parentChannel.SendLastQuoteCancel(this._subscriptionIdentity);
                    }
                }
                else if (this._isBid.HasValue)
                {
                    var priceObject = this._parentChannel.PriceReceivingHelper.GetPriceObject(
                        priceConverted: this._price,
                            sizeBaseAbsInitial: this._isIndicative ? 0m : this._size,
                            side: this._isBid.Value ? PriceSide.Bid : PriceSide.Ask,
                            symbol: this._symbol,
                            counterparty: this._parentChannel.Counterparty,
                            counterpartyIdentity: this._counterpartyIdentity,
                            marketDataRecordType: MarketDataRecordType.VenueNewOrder,
                            integratorReceivedTime: receivedAndParsed,
                            counterpartySentTime: this._sendingTime);

                    this._parentChannel.SendNewPrice(priceObject, this._subscriptionIdentity);
                }
            }


            private void OnTag262(BufferSegmentBase bufferSegment)
            {
                this._subscriptionIdentity = this._parentChannel.GetSubscriptionIdentity(bufferSegment);
            }

            private void OnTag268(BufferSegmentBase bufferSegment)
            {
                if (bufferSegment.ToInt() > 1)
                {
                    string error = string.Format("Received quote with unexpected number of layers: {0}",
                        bufferSegment.GetContentAsAsciiString());
                    this._logger.Log(LogLevel.Error, error);
                    throw new HaltingTagException(error, 268);
                }
            }

            private void OnTag269(BufferSegmentBase bufferSegment)
            {
                if (bufferSegment[0] == '0')
                    _isBid = true;
                else if (bufferSegment[0] == '1')
                    _isBid = false;
                else
                    _isBid = null;
            }

            private void OnTag270(BufferSegmentBase bufferSegment)
            {
                this._price = bufferSegment.ToAtomicDecimal();
            }

            private void OnTag271(BufferSegmentBase bufferSegment)
            {
                this._size = bufferSegment.ToDecimal();
            }

            //MDEntryID = 278
            private void OnTag278(BufferSegmentBase bufferSegment)
            {
                bufferSegment.CopyToClearingRest(this._counterpartyIdentity);
            }

            //MDUpdateAction = 279
            private void OnTag279(BufferSegmentBase bufferSegment)
            {
                switch ((char)bufferSegment[0])
                {
                    case MDUpdateAction.DELETE:
                        this._deletePrevious = true;
                        break;
                    case MDUpdateAction.NEW:
                    case MDUpdateAction.CHANGE:
                        this._deletePrevious = false;
                        break;
                    default:
                        throw new NonHaltingTagException("Receiving (and ignoring) Incremental MD refresh with unexpected update type", 279);
                }
            }

            //Symbol = 55
            private void OnTag55(BufferSegmentBase bufferSegment)
            {
                if (this._symbol == Common.Symbol.NULL)
                    this._symbol = bufferSegment.ToSymbol();
                else if (this._symbol != bufferSegment.ToSymbol())
                    throw new HaltingTagException("Receiving MD update for multiple symbols in one message", 55);
            }

            //QuoteType = 537
            private void OnTag537(BufferSegmentBase bufferSegment)
            {
                if ((char) bufferSegment[0] == '1')
                    this._isIndicative = false;
                else
                    this._isIndicative = true;

            }

            private void OnTag52(BufferSegmentBase bufferSegment)
            {
                this._sendingTime = bufferSegment.ToDateTime();
            }
        }
    }
}
