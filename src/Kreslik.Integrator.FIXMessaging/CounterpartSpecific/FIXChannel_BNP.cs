﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.FIX44;
using QuickFix.Fields;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal class FIXChannel_BNP : FIXChannelMessagingBase<FIXChannel_BNP>
    {
        public FIXChannel_BNP(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            FIXChannel_BNPSettings settings, bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;
            this.ConfigureStateMachineToWaitForUnsolicitedSessionStatus();

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new BankMassQuoteDataCracker(this, logger));
            }
            else
                this.SetQuickMessageParsers(isMdChannel, new BNPExecReportsCracker(this, logger));
        }

        private FIXChannel_BNPSettings _settings;


        protected override void OnOutgoingAdminMessage(QuickFix.Message message, SessionID sessionID)
        {
            if (message is Logon)
            {
                message.SetField(new Password(this._settings.Password));
            }
        }

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            var message = new QuoteRequest(new QuoteReqID(subscriptionIdentity));
            QuoteRequest.NoRelatedSymGroup symbolGroup = new QuoteRequest.NoRelatedSymGroup();
            symbolGroup.Symbol = subscriptionInfo.Symbol.ToQuickFixSymbol();
            message.AddGroup(symbolGroup);

            return Session.SendToTarget(message, this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            var message = new QuoteResponse(new QuoteRespID(subscriptionIdentity), new QuoteRespType(QuoteRespType.EXPIRED),
                                                         subscriptionInfo.Symbol.ToQuickFixSymbol());

            return Session.SendToTarget(message, this.SessionId);
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            var orderMessage = new NewOrderSingle(
                new ClOrdID(identity),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                new OrdType(OrdType.PREVIOUSLY_QUOTED));

            orderMessage.Account = new Account(this._settings.Account);

            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.OrdType = new OrdType(OrdType.PREVIOUSLY_QUOTED);

            orderMessage.Price = new Price(orderInfo.RequestedPrice);

            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);
            
            //according to Siamak this is not required for SPOT
            //orderMessage.SettlDate = new SettlDate(quote.TradeDate); //

            return orderMessage;
        }
        #endregion /FIX message composing

        #region FIX message cracking

        public void OnMessage(TradingSessionStatus m, SessionID s)
        {
            if (m.TradSesStatus.getValue() == TradSesStatus.OPEN)
            {
                this.SessionStatusReceived();
            }
            else
            {
                this.Logger.Log(LogLevel.Error, "Receiving unexpected TradSesStatus of TradingSessionStatus message: [{0}].", m.TradSesStatus.getValue());
            }
        }

        public void OnMessage(QuoteRequestReject m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received quote request reject: {0}", QuickFixUtils.FormatMessage(m));

            this.SendSubscriptionChange(new SubscriptionChangeInfo(m.QuoteReqID.getValue(),
                                                                           SubscriptionStatus.Rejected));
        }
        public void OnMessage(MassQuote massQuote, SessionID s)
        {
            //only one side might be present (0 seize or price means that side is undealable!)

            int quoteSetsNum = massQuote.GetInt(Tags.NoQuoteSets);
            if (quoteSetsNum != 1)
            {
                this.Logger.Log(LogLevel.Error,
                                "Unexpected number of QuoteSets in incoming BNP mass quote. Expected 1, Actual: " +
                                quoteSetsNum);

                this.SendSubscriptionChange(new SubscriptionChangeInfo(massQuote.QuoteReqID.getValue(), SubscriptionStatus.Broken));
            }

            Group quoteSetsGroup = massQuote.GetGroup(1, Tags.NoQuoteSets);
            int quoteEntriesNum = quoteSetsGroup.GetInt(Tags.NoQuoteEntries);
            if (quoteEntriesNum != 1)
            {
                this.Logger.Log(LogLevel.Error,
                                "Unexpected number of quote entries in incoming BNP mass quote. Expected 1, Actual: " +
                                quoteEntriesNum);

                this.SendSubscriptionChange(new SubscriptionChangeInfo(massQuote.QuoteReqID.getValue(), SubscriptionStatus.Broken));
            }

            Group noQuoteEntriesGroup = quoteSetsGroup.GetGroup(1, Tags.NoQuoteEntries);

            decimal askPrice, askSize, bidPrice, bidSize;
            askPrice = noQuoteEntriesGroup.GetDecimal(Tags.OfferPx);
            askSize = noQuoteEntriesGroup.GetDecimal(Tags.OfferSize);
            bidPrice = noQuoteEntriesGroup.GetDecimal(Tags.BidPx);
            bidSize = noQuoteEntriesGroup.GetDecimal(Tags.BidSize);

            if ((askSize == 0 && bidSize == 0) || (askPrice == 0 && bidPrice == 0))
            {
                //if (!this.Logger.IsLowDiskSpaceConstraintLoggingOn) this.Logger.Log(LogLevel.Info, "Received indicative quote (ignoring it and cancelling the previous): {0}",
                //                QuickFixUtils.FormatMessage(massQuote));
                this.SendLastQuoteCancel(massQuote.QuoteReqID.getValue());
            }
            else
            {
                //Acording to Siamak this is not needed in order messages
                //TradeDate = noQuoteEntriesGroup.GetString(Tags.SettlDate)
                PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSingleIdentity(askPrice: askPrice,
                                                                    askSize: askSize,
                                                                    bidPrice: bidPrice,
                                                                    bidSize: bidSize,
                                                                    symbol:
                                                                        (Kreslik.Integrator.Common.Symbol)
                                                                        noQuoteEntriesGroup.GetString(Tags.Symbol),
                                                                    counterparty: this.Counterparty,
                                                                    subscriptionIdentity: massQuote.QuoteReqID.getValue(),
                                                                    identity:
                                                                        noQuoteEntriesGroup.GetString(Tags.QuoteEntryID),
                                                                    counterpartyTimeUtc: massQuote.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                                                                    marketDataRecordType: MarketDataRecordType.BankQuoteData);

                this.SendNewQuote(PriceReceivingHelper.SharedQuote);
            }
        }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            string orderId = m.ClOrdID.getValue();

            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            switch (m.OrdStatus.getValue())
            {
                case OrdStatus.FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Filled;
                    executionInfo = new ExecutionInfo(m.ToNullableDecimal(Tags.CumQty),
                                                                    m.ToNullableDecimalCoalesce(Tags.LastPx, Tags.AvgPx),
                                                                    m.ToNullableString(Tags.ExecID),
                                                                    m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                                                                    m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                                                                    m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.REJECTED:
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), null, RejectionType.OrderReject);
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);

                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Order [{0}] was put to unsupported status: [{1}]", orderId, m.OrdStatus.getValue());
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     executionInfo, rejectionInfo));


            if (orderStatus == IntegratorOrderExternalChange.Filled)
            {
                //confirm the status back
                m.ExecType = new ExecType(ExecType.ORDER_STATUS);
                Session.SendToTarget(m, s);
            }
        }

        #endregion /FIX message cracking

        private sealed class BNPExecReportsCracker : BankExecReportCracker
        {
            public BNPExecReportsCracker(IOrdersParentChannel parentChannel, ILogger logger)
                : base(parentChannel, logger)
            { }

            protected override OrderStatus GetTranslatedOrderStatus(char externalOrderStatus)
            {
                switch (externalOrderStatus)
                {
                    case OrdStatus.NEW:
                    case OrdStatus.PENDING_NEW:
                        return OrderStatus.New;
                    case OrdStatus.FILLED:
                        return OrderStatus.Trade;
                    case OrdStatus.CANCELED:
                    case OrdStatus.EXPIRED:
                    case OrdStatus.REJECTED:
                        return OrderStatus.Reject;
                    default:
                        return OrderStatus.Unrecognized;
                }
            }

            //TODO: !!! two-way confirmation
            //protected override void AfterTradeReportedToIntegrator(QuickFixSubsegment currentMessage)
            //{
            //    string
            //}
        }
    }
}
