﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX44;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal class FIXChannel_NOM : FIXChannelMessagingBase<FIXChannel_NOM>
    {
        public FIXChannel_NOM(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            FIXChannel_NOMSettings settings, bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new BankQuoteDataCracker(this, logger), new NomQuoteCancelCracker(this, logger));
            }
            else
                this.SetQuickMessageParsers(isMdChannel, new NOMExecReportsCracker(this, logger));
        }

        public FIXChannel_NOMSettings _settings;

        protected override void OnOutgoingAdminMessage(QuickFix.Message message, SessionID sessionId)
        {
            message.SetField(new OnBehalfOfCompID(this._settings.OnBehalfOfCompID));
            message.SetField(new SenderSubID(this._settings.SenderSubId));
        }

        protected override void OnOutgoingMessage(QuickFix.Message message, SessionID sessionId)
        {
            message.SetField(new OnBehalfOfCompID(this._settings.OnBehalfOfCompID));
            message.SetField(new SenderSubID(this._settings.SenderSubId));
        }

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            var message = new QuoteRequest(new QuoteReqID(subscriptionIdentity));
            QuoteRequest.NoRelatedSymGroup symbolGroup = new QuoteRequest.NoRelatedSymGroup();
            symbolGroup.Symbol = subscriptionInfo.Symbol.ToQuickFixSymbol();
            symbolGroup.QuoteRequestType = new QuoteRequestType(QuoteRequestType.AUTOMATIC);
            symbolGroup.QuoteType = new QuoteType(QuoteType.RESTRICTED_TRADEABLE);
            symbolGroup.OrderQty = new OrderQty(subscriptionInfo.Quantity);
            symbolGroup.Account = new Account(this._settings.Account);
            symbolGroup.OrdType = new OrdType(OrdType.PREVIOUSLY_QUOTED);
            message.AddGroup(symbolGroup);

            return Session.SendToTarget(message,
                                        this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            var message = new QuoteResponse(new QuoteRespID(subscriptionIdentity), new QuoteRespType(QuoteRespType.PASS),
                                                         subscriptionInfo.Symbol.ToQuickFixSymbol());
            message.SetField(new QuoteReqID(subscriptionIdentity));

            return Session.SendToTarget(message, this.SessionId);
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            var orderMessage = new QuickFix.FIX44.NewOrderSingle(
                new ClOrdID(identity),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(DateTime.UtcNow),
                new OrdType(OrdType.PREVIOUSLY_QUOTED));

            orderMessage.Account = new Account(this._settings.Account);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.TimeInForce = new TimeInForce(TimeInForce.FILL_OR_KILL);
            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);
            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Price = new Price(orderInfo.RequestedPrice);

            return orderMessage;
        }
        #endregion /FIX message composing

        #region FIX message cracking

        public void OnMessage(QuoteRequestReject m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received quote request reject: {0}", QuickFixUtils.FormatMessage(m));

            this.SendSubscriptionChange(new SubscriptionChangeInfo(m.QuoteReqID.getValue(),
                                                                           SubscriptionStatus.Rejected));
        }

        public void OnMessage(QuoteCancel m, SessionID s)
        {
            //this.Logger.Log(LogLevel.Info, "Received quote cancel: {0}", QuickFixUtils.FormatMessage(m));

            if (!m.IsSetQuoteID() || string.IsNullOrEmpty(m.QuoteID.getValue()))
            {
                this.SendSubscriptionChange(new SubscriptionChangeInfo(m.QuoteReqID.getValue(),
                                                                       SubscriptionStatus.Rejected));
            }
            else
            {
                this.SendLastQuoteCancel(m.QuoteReqID.getValue());
            }
        }

        public void OnMessage(Quote quoteMessage, SessionID s)
        {
            QuoteObject quote;
            decimal bidSize;
            decimal askSzie;
            string subscriptionidentity = quoteMessage.QuoteReqID.getValue();

            if (!quoteMessage.IsSetOfferPx())
            {
                if (!quoteMessage.IsSetBidPx())
                {
                    this.SendLastQuoteCancel(subscriptionidentity);
                    return;
                }

                bidSize = quoteMessage.IsSetBidSize() ? quoteMessage.BidSize.getValue() : 0;

                if (bidSize == 0)
                    this.SendLastQuoteCancel(subscriptionidentity);
                else
                    this.SendNewPrice(this._priceReceivingHelper.GetPriceObject(price: quoteMessage.BidPx.getValue(),
                        sizeBaseAbsInitial: bidSize,
                        side: PriceSide.Bid,
                        symbol: (Kreslik.Integrator.Common.Symbol) quoteMessage.Symbol.getValue(),
                        counterparty: this.Counterparty,
                        counterpartyIdentity: quoteMessage.QuoteID.getValue(),
                        marketDataRecordType: MarketDataRecordType.BankQuoteData,
                        integratorReceivedTime: HighResolutionDateTime.UtcNow,
                        counterpartySentTime: quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger)
                        ), subscriptionidentity);
                return;
            }

            if (!quoteMessage.IsSetBidPx())
            {
                askSzie = quoteMessage.IsSetOfferSize() ? quoteMessage.OfferSize.getValue() : 0;

                if(askSzie == 0)
                    this.SendLastQuoteCancel(subscriptionidentity);
                else
                    this.SendNewPrice(this._priceReceivingHelper.GetPriceObject(price: quoteMessage.OfferPx.getValue(),
                        sizeBaseAbsInitial: askSzie,
                        side: PriceSide.Ask,
                        symbol: (Kreslik.Integrator.Common.Symbol)quoteMessage.Symbol.getValue(),
                        counterparty: this.Counterparty,
                        counterpartyIdentity: quoteMessage.QuoteID.getValue(),
                        marketDataRecordType: MarketDataRecordType.BankQuoteData,
                        integratorReceivedTime: HighResolutionDateTime.UtcNow,
                        counterpartySentTime: quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger)
                        ), subscriptionidentity);
                return;
            }

            bidSize = quoteMessage.IsSetBidSize() ? quoteMessage.BidSize.getValue() : 0;
            askSzie = quoteMessage.IsSetOfferSize() ? quoteMessage.OfferSize.getValue() : 0;

            if (bidSize == 0 && askSzie == 0)
                this.SendLastQuoteCancel(subscriptionidentity);
            else
            {
                PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSingleIdentity(askPrice: quoteMessage.OfferPx.getValue(),
                    askSize: quoteMessage.IsSetOfferSize() ? quoteMessage.OfferSize.getValue() : 0,
                    bidPrice: quoteMessage.BidPx.getValue(),
                    bidSize: quoteMessage.IsSetBidSize() ? quoteMessage.BidSize.getValue() : 0,
                    symbol: (Kreslik.Integrator.Common.Symbol) quoteMessage.Symbol.getValue(),
                    counterparty: this.Counterparty,
                    subscriptionIdentity: quoteMessage.QuoteReqID.getValue(),
                    identity: quoteMessage.QuoteID.getValue(),
                    counterpartyTimeUtc: quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                    marketDataRecordType: MarketDataRecordType.BankQuoteData);
                this.SendNewQuote(PriceReceivingHelper.SharedQuote);
            }
        }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            if (!m.IsSetClOrdID())
            {
                this.Logger.Log(LogLevel.Fatal,
                                 "Received execution report WITHOUT Client Order ID (and so we cannot match it to our order): {0}",
                                 QuickFixUtils.FormatMessage(m));
                return;
            }

            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            string orderId = m.ClOrdID.getValue();

            switch (m.OrdStatus.getValue())
            {
                case OrdStatus.NEW:
                case OrdStatus.PENDING_NEW:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] was received and enqeued by other end", orderId);
                    orderStatus = IntegratorOrderExternalChange.ConfirmedNew;
                    break;
                case OrdStatus.FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Filled;
                    executionInfo = new ExecutionInfo(m.ToNullableDecimal(Tags.LastQty),
                                                                    //Doc: 'LastPx allways same as AvgPx'
                                                                    m.ToNullableDecimal(Tags.LastPx),
                                                                    m.ToNullableString(Tags.ExecID),
                                                                    m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                                                                    m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                                                                    m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.CANCELED:
                case OrdStatus.EXPIRED:
                case OrdStatus.REJECTED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), null, RejectionType.OrderReject);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Order [{0}] was put to unsupported status: [{1}]", orderId, m.OrdStatus.getValue());
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     executionInfo, rejectionInfo));
        }

        #endregion /FIX message cracking

        private sealed class NOMExecReportsCracker : BankExecReportCracker
        {
            public NOMExecReportsCracker(IOrdersParentChannel parentChannel, ILogger logger)
                : base(parentChannel, logger)
            { }

            protected override OrderStatus GetTranslatedOrderStatus(char externalOrderStatus)
            {
                switch (externalOrderStatus)
                {
                    case OrdStatus.NEW:
                    case OrdStatus.PENDING_NEW:
                        return OrderStatus.New;
                    case OrdStatus.FILLED:
                        return OrderStatus.Trade;
                    case OrdStatus.CANCELED:
                    case OrdStatus.EXPIRED:
                    case OrdStatus.REJECTED:
                        return OrderStatus.Reject;
                    default:
                        return OrderStatus.Unrecognized;
                }
            }
        }
    }
}
