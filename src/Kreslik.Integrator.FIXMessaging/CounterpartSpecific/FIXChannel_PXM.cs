﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.FIX44;
using QuickFix.Fields;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal interface IMassQuoteAcknowledger
    {
        void AckMassQuote(string quoteId);
    }

    internal class FIXChannel_PXM : FIXChannelMessagingBase<FIXChannel_PXM>, IMassQuoteAcknowledger
    {
        public FIXChannel_PXM(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            FIXChannel_PXMSettings settings, bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;

            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new PXMMassQuoteDataCracker(this, this, logger));
            }
            else
                this.SetQuickMessageParsers(isMdChannel, new SimpleBankWithNewStatusExecReportsCracker(this, logger));
        }

        private FIXChannel_PXMSettings _settings;


        protected override void OnOutgoingAdminMessage(QuickFix.Message message, SessionID sessionID)
        {
            if (message is Logon)
            {
                message.SetField(new Username(this._settings.Username));
                message.SetField(new Password(this._settings.Password));
            }
        }

        //we want to send orders as IoCs
        protected override bool SupportsLimitOrders
        {
            get { return true; }
        }

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo,
            string subscriptionIdentity)
        {
            QuickFix.Message subscriptionMessage = this.GetMarketDataSubscriptionMsg(subscriptionInfo,
                subscriptionIdentity, true);
            return Session.SendToTarget(subscriptionMessage, this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo,
            string subscriptionIdentity)
        {
            QuickFix.Message subscriptionMessage = this.GetMarketDataSubscriptionMsg(subscriptionInfo,
                subscriptionIdentity, false);
            return Session.SendToTarget(subscriptionMessage, this.SessionId);
        }

        private QuickFix.Message GetMarketDataSubscriptionMsg(SubscriptionRequestInfo subscriptionInfo,
            string subscriptionIdentity, bool subscribe)
        {
            var mdReqID = new MDReqID(subscriptionIdentity);
            var subType =
                new SubscriptionRequestType(subscribe
                    ? SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES
                    : SubscriptionRequestType.DISABLE_PREVIOUS);
            var marketDepth = new MarketDepth(1);

            var message = new MarketDataRequest(mdReqID, subType, marketDepth);

            // symbol group:
            var symbolGroup = new MarketDataRequest.NoRelatedSymGroup();
            symbolGroup.Set(subscriptionInfo.Symbol.ToQuickFixSymbol());
            message.AddGroup(symbolGroup);

            return message;
        }

        protected override bool SendsQuotedOrdersAsLimit
        {
            get { return true; }
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            var orderMessage = new NewOrderSingle(
                new ClOrdID(identity),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                new OrdType(OrdType.LIMIT));

            //orderMessage.Account = new Account(this._settings.Account);

            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.OrdType = new OrdType(OrdType.PREVIOUSLY_QUOTED);

            orderMessage.Price = new Price(orderInfo.RequestedPrice);

            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);

            return orderMessage;
        }

        protected override void AppendAdditionalTagsToOrderMessage(NewOrderSingle orderMessage, OrderRequestInfo orderInfo)
        {
            orderMessage.SetField(new StringField(10000, _settings.IocTtlMs.ToString()));

            //This is needed by Commerz bank
            //orderMessage.SetField(new MDReqID(string.Format("{0}_{1}_001", this.Counterparty, orderInfo.Symbol.ShortName)));
        }

        public void AckMassQuote(string quoteId)
        {
            MassQuoteAcknowledgement ack = new MassQuoteAcknowledgement();
            ack.QuoteID = new QuoteID(quoteId);
            Session.SendToTarget(ack, this.SessionId);
        }

        #endregion /FIX message composing

        #region FIX message cracking

        public void OnMessage(MarketDataRequestReject m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received market data request reject: {0}", QuickFixUtils.FormatMessage(m));
            SubscriptionChangeInfo info = new SubscriptionChangeInfo(m.MDReqID.getValue(), SubscriptionStatus.Rejected);
            this.SendSubscriptionChange(info);
        }

        #endregion /FIX message cracking
    }
}
