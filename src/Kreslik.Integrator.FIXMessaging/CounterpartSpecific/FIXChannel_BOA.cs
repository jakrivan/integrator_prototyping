﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using QuickFix;
using QuickFix.FIX43;
using QuickFix.Fields;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal class FIXChannel_BOA : FIXChannelMessagingBase<FIXChannel_BOA>
    {
        public FIXChannel_BOA(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            if (!this.IsDivertedViaFluent)
                this.ConfigureStateMachineBasicTransitions();

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new BankQuoteDataCracker(this, logger), new QuoteCancelCracker(this, logger));
            }
            else
                this.SetQuickMessageParsers(isMdChannel, new SimpleBankWithNewStatusExecReportsCracker(this, logger));
        }

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, true),
                                        this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            return Session.SendToTarget(this.CreateSubscriptionMessage(subscriptionInfo, subscriptionIdentity, false),
                                        this.SessionId);
        }

        private QuoteRequest CreateSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity,
                                                                           bool subscribe)
        {
            var message = new QuoteRequest(new QuoteReqID(subscriptionIdentity));

            var symbolGroup = new QuoteRequest.NoRelatedSymGroup();
            symbolGroup.Symbol = subscriptionInfo.Symbol.ToQuickFixSymbol();
            symbolGroup.QuoteRequestType = new QuoteRequestType(subscribe ? 103 : 101); // NEW_RFS : CANCEL_REQUEST
            symbolGroup.Currency = subscriptionInfo.Symbol.ToQuickBaseCurrency();
            symbolGroup.OrderQty = new OrderQty(subscriptionInfo.Quantity);
            symbolGroup.SetField(new StringField(6215, "Spot")); //Tenor value
            message.AddGroup(symbolGroup);

            return message;
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            var orderMessage = new NewOrderSingle(
                new ClOrdID(identity),
                new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                new OrdType(OrdType.PREVIOUSLY_QUOTED));

            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();

            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);

            orderMessage.Price = new Price(orderInfo.RequestedPrice);

            return orderMessage;
        }
        #endregion /FIX message composing

        #region FIX message cracking

        public void OnMessage(QuoteRequestReject m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received quote request reject: {0}", QuickFixUtils.FormatMessage(m));
            this.SendSubscriptionChange(new SubscriptionChangeInfo(m.QuoteReqID.getValue(),
                                                                       SubscriptionStatus.Rejected));
        }

        public void OnMessage(Quote m, SessionID s)
        {
            //0 size or price should not be present
            //m.ToNullableDateTime(Tags.ValidUntilTime, false);


            this.SendNewQuote(this.CreateQuoteFromMessage(m));
        }

        private QuoteObject CreateQuoteFromMessage(Quote quoteMessage)
        {
            PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSingleIdentity(askPrice: quoteMessage.OfferPx.getValue(),
                                           askSize: quoteMessage.OfferSize.getValue(),
                                           bidPrice: quoteMessage.BidPx.getValue(),
                                           bidSize: quoteMessage.BidSize.getValue(),
                                           symbol: (Kreslik.Integrator.Common.Symbol)quoteMessage.Symbol.getValue(),
                                           counterparty: this.Counterparty,
                                           subscriptionIdentity: quoteMessage.QuoteReqID.getValue(),
                                           identity: quoteMessage.QuoteID.getValue(),
                                           counterpartyTimeUtc: quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                                           marketDataRecordType: MarketDataRecordType.BankQuoteData);

            return PriceReceivingHelper.SharedQuote;
        }

        public void OnMessage(QuoteCancel m, SessionID s)
        {
            this.SendLastQuoteCancel(m.QuoteReqID.getValue());
        }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            string orderId = m.ClOrdID.getValue();
            RejectionInfo rejectionInfo = null;
            ExecutionInfo executionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            switch (m.OrdStatus.getValue())
            {
                case OrdStatus.FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Filled;
                    executionInfo = new ExecutionInfo(m.ToNullableDecimal(Tags.CumQty),
                                                                    //Doc: 'LastPx will allways be the same as Price'
                                                                    m.ToNullableDecimalCoalesce(Tags.LastPx, Tags.Price),
                                                                    m.ToNullableString(Tags.ExecID),
                                                                    m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                                                                    m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                                                                    m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.REJECTED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), null, RejectionType.OrderReject);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Order [{0}] was put to unsupported status: [{1}]", orderId, m.OrdStatus.getValue());
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     executionInfo, rejectionInfo));
        }

        #endregion /FIX message cracking
    }
}
