﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using QuickFix;
using QuickFix.FIX42;
using QuickFix.Fields;
using Price = QuickFix.Fields.Price;
using TimeInForce = QuickFix.Fields.TimeInForce;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{
    internal class FIXChannel_CRS : FIXChannelMessagingBase<FIXChannel_CRS>
    {
        public FIXChannel_CRS(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagesTreatingSettings,
            FIXChannel_CRSSettings settings, bool isMdChannel)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagesTreatingSettings)
        {
            this._settings = settings;
            this.ConfigureStateMachineToWaitForSessionStatusHandshake();

            if (isMdChannel)
            {
                this.SetQuickMessageParsers(isMdChannel, new BankQuoteDataCracker(this, logger), new QuoteCancelCracker(this, logger));
            }
            else
                this.SetQuickMessageParsers(isMdChannel, new SimpleBankExecReportsCracker(this, logger));
        }

        private FIXChannel_CRSSettings _settings;

        protected override void SendSessionStatusRequest()
        {
            TradingSessionStatusRequest tssr =
                new TradingSessionStatusRequest(new TradSesReqID(Guid.NewGuid().ToString()),
                                                new SubscriptionRequestType(SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES));
            Session.SendToTarget(tssr, this.SessionId);
        }

        #region FIX message composing
        public override bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            string quoteId = subscriptionIdentity;
            var qr = new QuoteRequest(new QuoteReqID(quoteId));
            qr.NoRelatedSym = new NoRelatedSym(1);
            qr.SetField(subscriptionInfo.Symbol.ToQuickFixSymbol());
            qr.SetField(new SecurityType("FOR"));
            qr.SetField(new SecurityDesc("SPOT"));
            qr.SetField(new OrderQty(subscriptionInfo.Quantity));
            qr.SetField(new OrdType(OrdType.MARKET));
            qr.SetField(subscriptionInfo.Symbol.ToQuickBaseCurrency());

            return Session.SendToTarget(qr, this.SessionId);
        }

        public override bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity)
        {
            QuoteCancel qc = new QuoteCancel(new QuoteID("*"),
                                                 new QuoteCancelType(QuoteCancelType.CANCEL_FOR_SYMBOL));
            qc.QuoteReqID = new QuoteReqID(subscriptionIdentity);
            qc.NoQuoteEntries = new NoQuoteEntries(1);
            qc.SetField(subscriptionInfo.Symbol.ToQuickFixSymbol());
            qc.SetField(new SecurityType("FOR"));

            return Session.SendToTarget(qc, this.SessionId);
        }

        protected override QuickFix.Message CreateOrderMessage(OrderRequestInfo orderInfo, string identity)
        {
            NewOrderSingle orderMessage = new NewOrderSingle(
                new ClOrdID(identity),
                new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION),
                orderInfo.Symbol.ToQuickFixSymbol(),
                orderInfo.Side.ToQuickFixSide(),
                new TransactTime(TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now)),
                new OrdType(OrdType.FOREX_PREVIOUSLY_QUOTED)
                );

            orderMessage.ClientID = new ClientID(this._settings.ClientId);
            orderMessage.Account = new Account(this._settings.Account);
            orderMessage.FutSettDate = new FutSettDate("SPOT");

            orderMessage.OrderQty = new OrderQty(orderInfo.SizeBaseAbsInitial);
            orderMessage.Price = new Price(orderInfo.RequestedPrice);
            orderMessage.Currency = orderInfo.Symbol.ToQuickBaseCurrency();
            orderMessage.QuoteID = new QuoteID(orderInfo.CounterpartyIdentityAsAsciiString);
            return orderMessage;
        }
        #endregion /FIX message composing


        #region FIX message cracking

        public void OnMessage(Email m, SessionID s)
        {
            m.EmailType = new EmailType(EmailType.REPLY);
            m.Subject = new Subject("APPLICATION PING REPLY");
            Session.SendToTarget(m, s);
        }

        public void OnMessage(TradingSessionStatus m, SessionID s)
        {
            if (m.TradSesStatus.getValue() == TradSesStatus.OPEN)
            {
                this.SessionStatusReceived();
            }
            else
            {
                this.Logger.Log(LogLevel.Error, "Receiving unexpected TradSesStatus of TradingSessionStatus message: [{0}].", m.TradSesStatus.getValue());
            }
        }

        public void OnMessage(News m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "News message received from counterpart. Headline: {0}. Text: {1}", m.Headline.getValue(), m.GetString(Tags.Text));
        }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            string orderId = m.ClOrdID.getValue();

            ExecutionInfo executionInfo = null;
            RejectionInfo rejectionInfo = null;
            IntegratorOrderExternalChange orderStatus;
            DateTime? counterpartySendTime = m.Header.ToNullableDateTime(Tags.SendingTime, false, this.Logger);
            Common.Symbol symbol = Common.Symbol.NULL;
            if (m.IsSetField(Tags.Symbol))
            {
                symbol = (Common.Symbol)m.GetField(Tags.Symbol);
            }

            switch (m.OrdStatus.getValue())
            {
                case OrdStatus.FILLED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] successfuly filled", orderId);
                    orderStatus = IntegratorOrderExternalChange.Filled;
                    executionInfo = new ExecutionInfo(m.ToNullableDecimal(Tags.OrderQty),
                                                                    m.ToNullableDecimalCoalesce(Tags.LastSpotRate, Tags.Price),
                                                                    m.ToNullableString(Tags.ExecID),
                                                                    m.ToNullableDateTime(Tags.TransactTime, false, this.Logger),
                                                                    m.ToNullableDateTime(Tags.SettlDate, true, this.Logger),
                                                                    m.GetIntegratorDealDirection());
                    break;
                case OrdStatus.REJECTED:
                    this.Logger.Log(LogLevel.Info, "Order [{0}] REJECTED", orderId);
                    rejectionInfo = new RejectionInfo(m.ToNullableString(Tags.Text), null, RejectionType.OrderReject);
                    orderStatus = IntegratorOrderExternalChange.Rejected;
                    break;
                default:
                    this.Logger.Log(LogLevel.Error, "Order [{0}] was put to unsupported status: [{1}]", orderId, m.OrdStatus.getValue());
                    orderStatus = IntegratorOrderExternalChange.InBrokenState;
                    break;
            }

            this.SendOrderChange(new OrderChangeInfo(orderId, symbol, orderStatus, counterpartySendTime, null,
                                                     executionInfo, rejectionInfo));
        }


        public void OnMessage(QuoteAcknowledgement m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received quote acknowledgement (= reject for CRS): {0}", QuickFixUtils.FormatMessage(m));

            if (m.IsSetQuoteReqID())
            {
                this.SendSubscriptionChange(new SubscriptionChangeInfo(m.QuoteReqID.getValue(),
                                                                       SubscriptionStatus.Rejected));
            }
            //This is documented case, however we should always get QuoteRequestID, otherwise we'd need to search through all streams from one bank
            //else if (m.IsSetQuoteID())
            //{
            //    this.SendLastQuoteCancel(m.QuoteID.getValue());
            //}
            else
            {
                this.Logger.Log(LogLevel.Error, "Received quote acknowledgement which doesn't contain QuoteRequestID: {0}", m);
            }
        }

        public void OnMessage(QuickFix.FIX44.QuoteRequestReject m, SessionID s)
        {
            this.Logger.Log(LogLevel.Warn, "Received quote request reject: {0}", QuickFixUtils.FormatMessage(m));

            this.SendSubscriptionChange(new SubscriptionChangeInfo(m.QuoteReqID.getValue(),
                                                                       SubscriptionStatus.Rejected));
        }

        public void OnMessage(QuoteCancel m, SessionID s)
        {
            //this.Logger.Log(LogLevel.Warn, "Received quote cancel: {0}", QuickFixUtils.FormatMessage(m));

            if (string.Equals(m.QuoteID.getValue(), "*", StringComparison.Ordinal))
            {
                //! QuoteReqID can be '*' which means unsubscription for all subscriptions - MarketDataSession will handle this
                this.SendSubscriptionChange(new SubscriptionChangeInfo(m.QuoteReqID.getValue(),
                                                                       SubscriptionStatus.Rejected));
            }
            else
            {
                this.SendLastQuoteCancel(m.QuoteReqID.getValue());
            }
        }

        public void OnMessage(Quote m, SessionID s)
        {
            QuoteObject quote = this.CreateQuoteFromMessage(m);

            if (object.ReferenceEquals(quote, QuoteObject.EMPTY_QUOTE))
            {
                //if (!this.Logger.IsLowDiskSpaceConstraintLoggingOn) this.Logger.Log(LogLevel.Info, "Received indicative quote (ignoring it and cancelling the previous): {0}", QuickFixUtils.FormatMessage(m));
                this.SendLastQuoteCancel(m.QuoteReqID.getValue());
                return;
            }

            SendNewQuote(quote);
        }

        private QuoteObject CreateQuoteFromMessage(Quote quoteMessage)
        {
            decimal askSize, bidSize;
            askSize = quoteMessage.OfferSize.getValue();
            bidSize = quoteMessage.BidSize.getValue();

            //Zero sizes are documented way of sending indicative price
            if (askSize == 0 && bidSize == 0)
                return QuoteObject.EMPTY_QUOTE;

            PriceReceivingHelper.SharedQuote.OverrideContentObsoleteSingleIdentity(askPrice: quoteMessage.OfferPx.getValue(),
                                           askSize: askSize,
                                           bidPrice: quoteMessage.BidPx.getValue(),
                                           bidSize: bidSize,
                                           symbol: (Kreslik.Integrator.Common.Symbol) quoteMessage.Symbol.getValue(),
                                           counterparty: this.Counterparty,
                                           subscriptionIdentity: quoteMessage.QuoteReqID.getValue(),
                                           identity: quoteMessage.QuoteID.getValue(),
                                           counterpartyTimeUtc: quoteMessage.Header.ToDateTime(Tags.SendingTime, false, this.Logger),
                                           marketDataRecordType: MarketDataRecordType.BankQuoteData);
            return PriceReceivingHelper.SharedQuote;
        }

        #endregion /FIX message cracking
    }
}
