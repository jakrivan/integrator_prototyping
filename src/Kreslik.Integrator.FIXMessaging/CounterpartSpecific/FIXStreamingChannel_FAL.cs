﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX44;
using Message = QuickFix.Message;
using SessionState = Kreslik.Integrator.Contracts.SessionState;
using Symbol = Kreslik.Integrator.Common.Symbol;

namespace Kreslik.Integrator.FIXMessaging.CounterpartSpecific
{

    public class FIXStreamingChannelBase_FAL<TDerivedType> : FIXStreamingChannelBase<TDerivedType>
       where TDerivedType : MessageCrackerBase<TDerivedType>
    {
        public FIXStreamingChannelBase_FAL(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings settings)
            : base(persistedFixConfigsReader, logger, counterparty, settings)
        {
            this.OnStarted +=
                () =>
                    Task.Delay(TimeSpan.FromMilliseconds(1000))
                        .ContinueWith(t => this.SendTradingStatus(true))
                        .ContinueWith(t => this.CheckIsSessionOpen());
        }
        protected virtual void TradingSessionStatusClosedSent() { }

        protected virtual void CheckIsSessionOpen()
        {
            this.IsSessionOpen = true;
        }

        public override void SendTradingStatus(bool open)
        {
            if (this.State == SessionState.Running)
            {
                var request = new TradingSessionStatus(new TradingSessionID("KGTTradingSession"),
                    new TradSesStatus(open ? TradSesStatus.OPEN : TradSesStatus.CLOSED));
                request.UnsolicitedIndicator = new UnsolicitedIndicator(true);

                Session.SendToTarget(request, this.SessionId);

                this.IsSessionOpen = open;
                if (!open)
                    this.TradingSessionStatusClosedSent();
            }
        }
    }

    public class FIXStreamingChannelPricing_FAL : FIXStreamingChannelBase_FAL<FIXStreamingChannelPricing_FAL>, IFIXStreamingChannelPricing
    {
        public FIXStreamingChannelPricing_FAL(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, ISymbolsInfo symbolsInfo, UnexpectedMessagesTreatingSettings unexpectedMessagessettings)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagessettings)
        {
            this._subscriptionTrackingHelper = new FALSubscriptionTrackingHelper(symbolsInfo, logger, counterparty);
            this.OnStarted += this._subscriptionTrackingHelper.ResetAllSubscriptions;
            this.OnStopped += this._subscriptionTrackingHelper.ResetAllSubscriptions;
            this.OnStopped += InformAllPricesAreCanceled;
            this.ExpectedPricingDepth = -1;
        }

        private FALSubscriptionTrackingHelper _subscriptionTrackingHelper;

        public int ExpectedPricingDepth
        {
            get { return this._subscriptionTrackingHelper.ExpectedPricingDepth; }
            set { this._subscriptionTrackingHelper.ExpectedPricingDepth = value; }
            //get { return 1; }
            //set
            //{
            //    if (value > 1)
            //        this.Logger.Log(LogLevel.Fatal,
            //            "{0} Expected pricing depth in settings is {1}, however only 1 can be supported",
            //            this.Counterparty, value);
            //}
        }

        protected override void CheckIsSessionOpen()
        {
            TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(1),() =>
            {
                for (int attempt = 1; attempt < 11; attempt++)
                {
                    if (_subscriptionTrackingHelper.VerifyAllSubscriptionsReceived())
                    {
                        this.IsSessionOpen = true;
                        return;
                    }
                    Thread.Sleep(1000);
                }

                int subscribedLayers = _subscriptionTrackingHelper.GetMaxLayersCount();
                bool open = subscribedLayers > 0;
                this.Logger.Log(LogLevel.Fatal, "{0} receiving unexpectedly low number of subscriptions ({1} instead of {2}). Session will be opened if there are nonzero subscriptions, but some higher layer systems might not operate correctly",
                    this.Counterparty, subscribedLayers, this.ExpectedPricingDepth);
                this.IsSessionOpen = open;
            });

            
        }

        protected override void TradingSessionStatusClosedSent()
        {
            //WARNING: FxAll does not resent subscriptions contrary to their documentation
            //this._subscriptionTrackingHelper.ResetAllSubscriptions();
            this.InformAllPricesAreCanceled();
        }

        public event Action<Common.Symbol, bool> SubscriptionChanged
        {
            add { this._subscriptionTrackingHelper.SubscriptionChanged += value; }
            remove { this._subscriptionTrackingHelper.SubscriptionChanged -= value; }
        }

        public event Action<StreamingPrice> NewPriceSent;
        public event Action<StreamingPriceCancel> NewPriceCancelSent;

        public bool IsSubscribed(Common.Symbol symbol)
        {
            return _subscriptionTrackingHelper.IsSubscribed(symbol);
        }

        private void InformAllPricesAreCanceled()
        {
            if (this.NewPriceCancelSent != null)
            {
                foreach (Symbol symbol in Symbol.Values)
                {
                    StreamingPriceCancel priceCancel = new StreamingPriceCancel(symbol, this.Counterparty, null, null);
                    priceCancel.SetSentPriceCancelInfo(HighResolutionDateTime.UtcNow);
                    this.NewPriceCancelSent(priceCancel);
                }
            }
        }

        public void OnMessage(QuoteRequest m, SessionID s)
        {
            if(m.NoRelatedSym.getValue() > 1)
                throw new Exception("Subscription message on more than single symbol are not currently supported");

            //Num of layers - just a single one

            //TODO: validate settlement type and settlement data

            this._subscriptionTrackingHelper.OnMessage(m, s);
        }

        private string GetMDEntryId(Common.Symbol s, PriceSide side, int layerId)
        {
            return string.Format("{0}_Entry{1}{2}", s.ShortName, side == PriceSide.Ask ? "A" : "B", layerId);
        }

        public SubmissionResult SendOrder(StreamingPrice streamingPrice)
        {
            //Message q = this.CreateMassQuote(streamingPrice);

            Quote q = new Quote();

            string subReqId = _subscriptionTrackingHelper.GetSubscriptionReqId(streamingPrice.Symbol, streamingPrice.Side,
                streamingPrice.LayerId);
            if (subReqId == null)
                return SubmissionResult.Failed_NotSubscribed;
            q.QuoteReqID = new QuoteReqID(subReqId);
            q.QuoteID = new QuoteID(streamingPrice.IntegratorPriceIdentity);
            q.QuoteType = new QuoteType(QuoteType.TRADEABLE);
            q.Symbol = streamingPrice.Symbol.ToQuickFixSymbol();

            if (streamingPrice.Side == PriceSide.Bid)
            {
                q.BidPx = new BidPx(streamingPrice.Price);
                q.BidSize = new BidSize(streamingPrice.SizeBaseAbs);
            }
            else if (streamingPrice.Side == PriceSide.Ask)
            {
                q.OfferPx = new OfferPx(streamingPrice.Price);
                q.OfferSize = new OfferSize(streamingPrice.SizeBaseAbs);
            }


            //MassQuote q = new MassQuote();

            //q.QuoteReqID = new QuoteReqID(_subscriptionTrackingHelper.GetSubscriptionReqId(streamingPrice.Symbol, streamingPrice.Side, streamingPrice.LayerId));
            //q.QuoteID = new QuoteID(streamingPrice.IntegratorPriceIdentity);
            //q.QuoteType = new QuoteType(QuoteType.TRADEABLE);

            //MassQuote.NoQuoteSetsGroup setsGroup = new MassQuote.NoQuoteSetsGroup();
            //setsGroup.QuoteSetID = new QuoteSetID("1");
            //setsGroup.TotNoQuoteEntries = new TotNoQuoteEntries(1);

            //MassQuote.NoQuoteSetsGroup.NoQuoteEntriesGroup entryGroup = new MassQuote.NoQuoteSetsGroup.NoQuoteEntriesGroup();
            //entryGroup.QuoteEntryID = new QuoteEntryID(streamingPrice.IntegratorPriceIdentity);
            //entryGroup.Symbol = streamingPrice.Symbol.ToQuickFixSymbol();
            //if (streamingPrice.Side == PriceSide.Bid)
            //{
            //    entryGroup.BidPx = new BidPx(streamingPrice.Price);
            //    entryGroup.BidSize = new BidSize(streamingPrice.SizeBaseAbs);
            //}
            //else if (streamingPrice.Side == PriceSide.Ask)
            //{
            //    entryGroup.OfferPx = new OfferPx(streamingPrice.Price);
            //    entryGroup.OfferSize = new OfferSize(streamingPrice.SizeBaseAbs);
            //}

            //setsGroup.AddGroup(entryGroup);
            //q.AddGroup(setsGroup);

            bool sent = this.SendMessage(q);

            if (sent && this.NewPriceSent != null)
            {
                streamingPrice.IntegratorSentTimeUtc = HighResolutionDateTime.UtcNow;
                this.NewPriceSent(streamingPrice);
            }

            return sent ? SubmissionResult.Success : SubmissionResult.Failed_MessagingLayerError;
        }

        public QuickFix.Message CreateQuote(StreamingPrice streamingPrice)
        {
            Quote q = new Quote();

            q.QuoteReqID = new QuoteReqID(_subscriptionTrackingHelper.GetSubscriptionReqId(streamingPrice.Symbol, streamingPrice.Side, streamingPrice.LayerId));
            q.QuoteID = new QuoteID(streamingPrice.IntegratorPriceIdentity);
            q.QuoteType = new QuoteType(QuoteType.TRADEABLE);
            q.Symbol = streamingPrice.Symbol.ToQuickFixSymbol();

            //TODO: Do not allow higher layer id
            //streamingPrice.LayerId

            if (streamingPrice.Side == PriceSide.Bid)
            {
                q.BidPx = new BidPx(streamingPrice.Price);
                q.BidSize = new BidSize(streamingPrice.SizeBaseAbs);
            }
            else if (streamingPrice.Side == PriceSide.Ask)
            {
                q.OfferPx = new OfferPx(streamingPrice.Price);
                q.OfferSize = new OfferSize(streamingPrice.SizeBaseAbs);
            }

            return q;
        }

        private QuickFix.Message CreateMassQuote(StreamingPrice streamingPrice)
        {
            MassQuote q = new MassQuote();

            q.QuoteReqID = new QuoteReqID(_subscriptionTrackingHelper.GetSubscriptionReqId(streamingPrice.Symbol, streamingPrice.Side, streamingPrice.LayerId));
            q.QuoteID = new QuoteID(streamingPrice.IntegratorPriceIdentity);
            q.QuoteType = new QuoteType(QuoteType.TRADEABLE);

            MassQuote.NoQuoteSetsGroup setsGroup = new MassQuote.NoQuoteSetsGroup();
            setsGroup.QuoteSetID = new QuoteSetID("1");
            setsGroup.TotNoQuoteEntries = new TotNoQuoteEntries(1);
            
            MassQuote.NoQuoteSetsGroup.NoQuoteEntriesGroup entryGroup = new MassQuote.NoQuoteSetsGroup.NoQuoteEntriesGroup();
            entryGroup.QuoteEntryID = new QuoteEntryID(streamingPrice.IntegratorPriceIdentity);
            entryGroup.Symbol = streamingPrice.Symbol.ToQuickFixSymbol();
            if (streamingPrice.Side == PriceSide.Bid)
            {
                entryGroup.BidPx = new BidPx(streamingPrice.Price);
                entryGroup.BidSize = new BidSize(streamingPrice.SizeBaseAbs);
            }
            else if (streamingPrice.Side == PriceSide.Ask)
            {
                entryGroup.OfferPx = new OfferPx(streamingPrice.Price);
                entryGroup.OfferSize = new OfferSize(streamingPrice.SizeBaseAbs);
            }

            setsGroup.AddGroup(entryGroup);
            q.AddGroup(setsGroup);

            return q;
        }

        private void CancelPriceStreamImpl(Symbol symbol, PriceSide side, int layerId)
        {
            QuoteCancel qc = new QuoteCancel();
            qc.QuoteReqID = new QuoteReqID(_subscriptionTrackingHelper.GetSubscriptionReqId(symbol, side, layerId));
            qc.QuoteCancelType = new QuoteCancelType(QuoteCancelType.CANCEL_ALL_QUOTES);
            qc.SetField(symbol.ToQuickFixSymbol());

            this.SendMessage(qc);
        }

        public void CancelPriceStreams(StreamingPriceCancel streamingPriceCancel)
        {
            Symbol symbol = streamingPriceCancel.Symbol;

            if (streamingPriceCancel.Side.HasValue)
            {
                this.CancelPriceStreamImpl(symbol, streamingPriceCancel.Side.Value,
                    streamingPriceCancel.PriceProducerIndex.Value.LayerId);
            }
            else
            {
                this.CancelPriceStreamImpl(symbol, PriceSide.Ask, 
                    streamingPriceCancel.PriceProducerIndex.Value.LayerId);
                this.CancelPriceStreamImpl(symbol, PriceSide.Bid,
                    streamingPriceCancel.PriceProducerIndex.Value.LayerId);
            }

            if (this.NewPriceCancelSent != null)
            {
                streamingPriceCancel.SetSentPriceCancelInfo(HighResolutionDateTime.UtcNow);
                this.NewPriceCancelSent(streamingPriceCancel);
            }
        }
    }




    public class FIXStreamingChannelOrders_FAL : FIXStreamingChannelBase_FAL<FIXStreamingChannelOrders_FAL>, IFIXStreamingChannelOrders
    {
        public FIXStreamingChannelOrders_FAL(IPersistedFixConfigsReader persistedFixConfigsReader, ILogger logger,
            Counterparty counterparty, UnexpectedMessagesTreatingSettings unexpectedMessagessettings)
            : base(persistedFixConfigsReader, logger, counterparty, unexpectedMessagessettings)
        { }

        private const int _MAX_COUNTERPARTYCLIENTID_LENGTH = 20;

        public event Action<RejectableMMDeal> NewRejectableDealArrived;
        public event CounterpartyRejectedDealDelegate NewCounterpartyRejectedDeal;
        public event Action<CounterpartyTimeoutInfo> NewCounterpartyTimeoutInfo;

        public event Action<RejectableMMDeal> DealRejected;
        public ICounterpartyRejectionSource CounterpartyRejectionSource { private get; set; }

        public void OnMessage(ExecutionReport m, SessionID s)
        {
            this.HandleExecReportMessage(m);
        }

        public void OnMessage(QuickFix.FIX50.NewOrderSingle m, SessionID s)
        {
            string clOrdId = m.ClOrdID.getValue();

            {
                ExecutionReport execRep = new ExecutionReport();

                //Fluent required//
                execRep.Account = new Account("none");
                execRep.SetField(new Product(Product.CURRENCY));
                execRep.SecurityType = new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT);
                execRep.OrdType = new OrdType(OrdType.PREVIOUSLY_QUOTED);
                //End of Fluent required//


                execRep.ClOrdID = m.ClOrdID;
                execRep.OrderID = new OrderID(m.ClOrdID.getValue());
                //execRep.ExecTransType = new ExecTransType(ExecTransType.NEW);
                execRep.ExecID = new ExecID("FooBar");

                execRep.OrdStatus = new OrdStatus(OrdStatus.NEW);
                //execRep.FutSettDate = new FutSettDate(deal.SettlementDateLocal.ToString("yyyyMMdd"));
                execRep.Symbol = m.Symbol;
                execRep.Side = m.Side;
                execRep.OrderQty = m.OrderQty;
                execRep.LastQty = new LastQty(0);
                execRep.LastPx = new LastPx(0);
                execRep.Price = m.Price;
                //
                execRep.LeavesQty = new LeavesQty(m.OrderQty.getValue());
                execRep.CumQty = new CumQty(m.OrderQty.getValue());
                execRep.AvgPx = new AvgPx(m.Price.getValue());
                execRep.Currency = ((Symbol)m.Symbol.getValue()).BaseCurrency.ToQuickFixCurrency();
                execRep.ExecType = new ExecType(ExecType.NEW);
                execRep.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);
                execRep.TimeInForce =
                    new QuickFix.Fields.TimeInForce(QuickFix.Fields.TimeInForce.FILL_OR_KILL);

                this.SendMessage(execRep);
            }

            Thread.Sleep(TimeSpan.FromMilliseconds(350));

            //TODO: delay

            //Accept
            //{
            //    ExecutionReport execRep = new ExecutionReport();

            //    //Fluent required//
            //    execRep.Account = new Account("none");
            //    execRep.SetField(new Product(Product.CURRENCY));
            //    execRep.SecurityType = new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT);
            //    execRep.OrdType = new OrdType(OrdType.PREVIOUSLY_QUOTED);
            //    //End of Fluent required//


            //    execRep.ClOrdID = m.ClOrdID;
            //    execRep.OrderID = new OrderID(m.ClOrdID.getValue());
            //    //execRep.ExecTransType = new ExecTransType(ExecTransType.NEW);
            //    execRep.ExecID = new ExecID("FooBar");

            //    execRep.OrdStatus = new OrdStatus(OrdStatus.FILLED);
            //    //execRep.FutSettDate = new FutSettDate(deal.SettlementDateLocal.ToString("yyyyMMdd"));
            //    execRep.Symbol = m.Symbol;
            //    execRep.Side = m.Side;
            //    execRep.OrderQty = m.OrderQty;
            //    execRep.LastQty = new LastQty(m.OrderQty.getValue());
            //    execRep.LastPx = new LastPx(m.Price.getValue());
            //    execRep.Price = m.Price;
            //    //
            //    execRep.LeavesQty = new LeavesQty(0m);
            //    execRep.CumQty = new CumQty(m.OrderQty.getValue());
            //    execRep.AvgPx = new AvgPx(m.Price.getValue());
            //    execRep.Currency = ((Symbol) m.Symbol.getValue()).BaseCurrency.ToQuickFixCurrency();
            //    execRep.ExecType = new ExecType(ExecType.FILL);
            //    execRep.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);
            //    execRep.TimeInForce =
            //        new QuickFix.Fields.TimeInForce(QuickFix.Fields.TimeInForce.FILL_OR_KILL);

            //    this.SendMessage(execRep);
            //}


            //Reject
            {
                ExecutionReport execRep = new ExecutionReport();

                //Fluent required//
                execRep.Account = new Account("none");
                execRep.SetField(new Product(Product.CURRENCY));
                execRep.SecurityType = new SecurityType(SecurityType.FOREIGN_EXCHANGE_CONTRACT);
                execRep.OrdType = new OrdType(OrdType.PREVIOUSLY_QUOTED);
                //End of Fluent required//


                execRep.ClOrdID = m.ClOrdID;
                execRep.OrderID = new OrderID(m.ClOrdID.getValue());
                //execRep.ExecTransType = new ExecTransType(ExecTransType.NEW);
                execRep.ExecID = new ExecID("FooBar");

                execRep.OrdStatus = new OrdStatus(OrdStatus.REJECTED);
                //execRep.FutSettDate = new FutSettDate(deal.SettlementDateLocal.ToString("yyyyMMdd"));
                execRep.Symbol = m.Symbol;
                execRep.Side = m.Side;
                execRep.OrderQty = m.OrderQty;
                execRep.LastQty = new LastQty(0);
                execRep.LastPx = new LastPx(0);
                execRep.Price = m.Price;
                //
                execRep.LeavesQty = new LeavesQty(0m);
                execRep.CumQty = new CumQty(0);
                execRep.AvgPx = new AvgPx(0);
                execRep.Currency = ((Symbol)m.Symbol.getValue()).BaseCurrency.ToQuickFixCurrency();
                execRep.ExecType = new ExecType(ExecType.REJECTED);
                execRep.TransactTime = new TransactTime(HighResolutionDateTime.UtcNow);
                execRep.TimeInForce =
                    new QuickFix.Fields.TimeInForce(QuickFix.Fields.TimeInForce.FILL_OR_KILL);

                this.SendMessage(execRep);
            }

        }

        private void HandleExecReportMessage(QuickFix.Message m)
        {
//2015-10-29 08:12:31.7423105 UTC 31 DEBUG 8=FIX.4.4|9=262|35=8|34=21|49=FXALL|52=20151029-08:12:31.583|56=KGTINV-TRD-TEST|1=RABD|11=0B41MGM4d1De1|15=EUR|17=155852499|31=1.0938|32=100000.00|37=155852499|39=2|54=1|55=EUR/USD|60=20151029-08:12:31.583|64=20151102|75=20151029|150=F|194=1.0938|382=1|375=RABD|337=RABD|1057=N|10=124|
//2015-10-29 08:12:31.7433111 UTC 31 FATAL DEAL: 8=FIX.4.4|9=262|35=8|34=21|49=FXALL|52=20151029-08:12:31.583|56=KGTINV-TRD-TEST|1=RABD|11=0B41MGM4d1De1|15=EUR|17=155852499|31=1.0938|32=100000.00|37=155852499|39=2|54=1|55=EUR/USD|60=20151029-08:12:31.583|64=20151102|75=20151029|150=F|194=1.0938|337=RABD|375=RABD|382=1|1057=N|10=124|
//2015-10-29 08:12:39.7113768 UTC 31 DEBUG 8=FIX.4.4|9=262|35=8|34=22|49=FXALL|52=20151029-08:12:39.552|56=KGTINV-TRD-TEST|1=RABD|11=0B41UGM4d1De1|15=EUR|17=155852500|31=1.0938|32=100000.00|37=155852500|39=2|54=1|55=EUR/USD|60=20151029-08:12:39.552|64=20151102|75=20151029|150=F|194=1.0938|382=1|375=RABD|337=RABD|1057=N|10=107|
//2015-10-29 08:12:39.7113768 UTC 31 FATAL DEAL: 8=FIX.4.4|9=262|35=8|34=22|49=FXALL|52=20151029-08:12:39.552|56=KGTINV-TRD-TEST|1=RABD|11=0B41UGM4d1De1|15=EUR|17=155852500|31=1.0938|32=100000.00|37=155852500|39=2|54=1|55=EUR/USD|60=20151029-08:12:39.552|64=20151102|75=20151029|150=F|194=1.0938|337=RABD|375=RABD|382=1|1057=N|10=107|

            RejectableMMDeal rejectableMmDeal;
            try
            {
                Symbol symbol = (Common.Symbol)m.GetField(Tags.Symbol);
                string counterpartyOrderId = m.GetField(Tags.OrderID);
                if (m.IsSetField(Tags.Currency) && !m.GetField(Tags.Currency).Equals(symbol.BaseCurrency.ToString()))
                    throw new Exception(string.Format("Received deal [{0}] for symbol {1} but currency {2}. Not continuing to parse and rejecting",
                        counterpartyOrderId, symbol, m.GetField(Tags.Currency)));

                if (m.GetChar(Tags.ExecType) != ExecType.TRADE)
                {
                    throw new Exception(string.Format("Received ExecReport [{0}] for symbol {1} but execution type is not expected: [{2}] (it's not trade). Not continuing to parse and rejecting",
                        counterpartyOrderId, symbol, m.GetField(Tags.ExecType)));

                }

                decimal qty = m.GetDecimal(Tags.LastQty);
                decimal minimumFillableQunatity = qty;

                //for case without dictionary
                string counterpartyClientId = m.IsSetField(Tags.ContraTrader) ? m.GetField(Tags.ContraTrader) : null;

                DateTime sendingTime;
                if (this.IsDivertedViaFluent)
                {
                    if (m.Header.IsSetField(Tags.OrigSendingTime))
                    {
                        sendingTime = m.Header.ToDateTime(Tags.OrigSendingTime, false, this.Logger);
                    }
                    else
                    {
                        this.Logger.Log(LogLevel.Error, "FxAll streaming order redirected via Fluent, but OrigSendingTime tag (122) not set");
                        sendingTime = m.Header.ToDateTime(Tags.SendingTime, false, this.Logger);
                    }
                }
                else
                    sendingTime = m.Header.ToDateTime(Tags.SendingTime, false, this.Logger);

                rejectableMmDeal = new RejectableMMDeal(
                symbol, this.Counterparty, QuickFixUtils.ToDealDirection(m.GetChar(Tags.Side)), m.GetDecimal(Tags.LastPx),
                qty, minimumFillableQunatity, counterpartyOrderId, counterpartyClientId, m.GetField(Tags.ClOrdID),
                sendingTime, this.CounterpartyRejectionSource);

                DateTime? settlementDateLocal = m.ToNullableDateTime(Tags.SettlDate, true, this.Logger);
                if (settlementDateLocal.HasValue)
                {
                    rejectableMmDeal.SetCounterpartyExecutionInfo(settlementDateLocal.Value, m.GetField(Tags.ExecID));
                }
                else
                {
                    throw new Exception(string.Format("Received ExecReport [{0}] for symbol {1} but settlement date is not present or valid (tag 64). Not continuing to parse and rejecting",
                        counterpartyOrderId, symbol));
                }
            }
            catch (Exception e)
            {
                //send reject
                this.RejectOrder(m);
                this.Logger.LogException(LogLevel.Fatal, "Could not create internal deal from received order message - auto-rejecting it and stopping session", e);
                this.StopAsync(TimeSpan.FromMilliseconds(200));
                return;
            }

            if (this.NewRejectableDealArrived != null)
            {
                try
                {
                    this.NewRejectableDealArrived(rejectableMmDeal);
                }
                catch (Exception e)
                {
                    this.RejectOrder(m);
                    this.Logger.LogException(LogLevel.Fatal, "Auto rejecting deal due to errors during handling the client order", e);
                }
            }
            else
            {
                this.RejectOrder(m);
                this.Logger.Log(LogLevel.Fatal, "Auto rejecting deal as business layer is not connected to messaging (Streaming should be stopped)");
            }
        }

        





        private void OnDealRejected(RejectableMMDeal deal)
        {
            if (this.DealRejected != null)
            {
                try
                {
                    DealRejected(deal);
                }
                catch (Exception e)
                {
                    this.Logger.LogException(LogLevel.Fatal, "Rejected deal callback - manual investigation needed", e);
                }
            }
        }

        private void RejectOrder(QuickFix.Message m)
        {
            QuickFix.FIX50.ExecutionAcknowledgement ackMessage = new QuickFix.FIX50.ExecutionAcknowledgement();

            ackMessage.OrderID = new OrderID(m.GetField(Tags.OrderID));
            ackMessage.ClOrdID = new ClOrdID(m.GetField(Tags.ClOrdID));
            ackMessage.ExecAckStatus = new ExecAckStatus(ExecAckStatus.REJECTED);
            ackMessage.ExecID = new ExecID(m.GetField(Tags.ExecID));
            ackMessage.Symbol = new QuickFix.Fields.Symbol(m.GetField(Tags.Symbol));
            ackMessage.Side = new Side(m.GetChar(Tags.Side));
            ackMessage.LastQty = new LastQty(m.GetDecimal(Tags.LastQty));
            ackMessage.LastPx = new LastPx(m.GetDecimal(Tags.LastPx));

            Session.SendToTarget(ackMessage, this.SessionId);

            this.OnDealRejected(null);
        }

        public QuickFix.Message CreateAcceptMessage(RejectableMMDeal deal)
        {
            QuickFix.FIX50.ExecutionAcknowledgement ackMessage = new QuickFix.FIX50.ExecutionAcknowledgement();

            ackMessage.OrderID = new OrderID(deal.CounterpartyClientOrderIdentifier);
            ackMessage.ClOrdID = new ClOrdID(deal.IntegratorQuoteIdentifier);
            ackMessage.ExecAckStatus = new ExecAckStatus(ExecAckStatus.ACCEPTED);
            ackMessage.ExecID = new ExecID(deal.CounterpartyExecutionId);
            ackMessage.Symbol = deal.Symbol.ToQuickFixSymbol();
            ackMessage.Side = deal.IntegratorDealDirection.ToOpositeDirection().ToQuickFixSide();
            ackMessage.LastQty = new LastQty(deal.SizeToBeFilledBaseAbs);
            ackMessage.LastPx = new LastPx(deal.CounterpartyRequestedPrice);


            return ackMessage;
        }

        public bool UpdateAcceptMessageWithRejectRestInfo(RejectableMMDeal deal, QuickFix.Message msg)
        {
            QuickFix.FIX50.ExecutionAcknowledgement ackMessage = msg as QuickFix.FIX50.ExecutionAcknowledgement;

            if(ackMessage == null)
                return false;

            ackMessage.ExecAckStatus = new ExecAckStatus(ExecAckStatus.REJECTED);
            ackMessage.LastQty = new LastQty(0);

            return true;
        }

        public QuickFix.Message CreateRejectionMessage(RejectableMMDeal deal, RejectableMMDeal.DealStatus rejectionReason)
        {
            QuickFix.FIX50.ExecutionAcknowledgement ackMessage = new QuickFix.FIX50.ExecutionAcknowledgement();

            ackMessage.OrderID = new OrderID(deal.CounterpartyClientOrderIdentifier);
            ackMessage.ClOrdID = new ClOrdID(deal.IntegratorQuoteIdentifier);
            ackMessage.ExecAckStatus = new ExecAckStatus(ExecAckStatus.REJECTED);
            ackMessage.ExecID = new ExecID(deal.CounterpartyExecutionId);
            ackMessage.Symbol = deal.Symbol.ToQuickFixSymbol();
            ackMessage.Side = deal.IntegratorDealDirection.ToOpositeDirection().ToQuickFixSide();
            ackMessage.LastQty = new LastQty(0);
            ackMessage.LastPx = new LastPx(deal.CounterpartyRequestedPrice);

            return ackMessage;
        }

        public void OnMessage(QuickFix.FIX50.ExecutionAcknowledgement ackMessage, SessionID s)
        {
            this.HandleExecutionAcknowledgement(ackMessage);
        }

        public void OnMessage(QuickFix.Message m, SessionID s)
        {
            if (m.Header.GetString(35).Equals("BN", StringComparison.OrdinalIgnoreCase))
            {
                this.HandleExecutionAcknowledgement(m);
            }
            else
            {
                this.Logger.Log(LogLevel.Fatal, "Received unknown message: [{0}]", QuickFixUtils.FormatMessage(m));
            }
        }

        private void HandleExecutionAcknowledgement(QuickFix.Message m)
        {
            try
            {
                this.HandleExecutionAcknowledgementImpl(m);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, e, "{0} failed during handling Exec Acknowledgement message. It is possible that counterparty was rejecting deal we accepted!! Positions should be reviewed! {1}",
                    this.Counterparty, m);
                this.SendTradingStatus(false);
                this.StopAsync(TimeSpan.FromMilliseconds(100));
            }
        }

        private void HandleExecutionAcknowledgementImpl(QuickFix.Message m)
        {
            switch (m.GetChar(Tags.ExecAckStatus))
            {
                case  ExecAckStatus.ACCEPTED:
                    this.Logger.Log(LogLevel.Info, "Counterparty confirmed earlier accepted deal. No further action required");
                    return;
                case ExecAckStatus.REJECTED:
                    break;
                default:
                    throw new Exception(
                        string.Format(
                            "Unexpected value [{0}] for tag 1036. Cannot infer if this is Accept or Rejection",
                            m.GetChar(Tags.ExecAckStatus)));
            }

            //Here we are processing rejections and timeouts
            string cptOrdId = m.GetField(Tags.OrderID);
            string cptExecId = m.GetField(Tags.ExecID);


            if (!m.IsSetField(Tags.DKReason))
            {
                //this is timeout

                this.Logger.Log(LogLevel.Fatal, "FxAll timeouting client order [{0}]. Session will be stopped. FxAll should be contacted to unblock our pricing from their book", cptOrdId);
                CounterpartyTimeoutInfo timeoutInfo = new CounterpartyTimeoutInfo(HighResolutionDateTime.UtcNow,
                    m.Header.ToDateTime(Tags.SendingTime, false, this.Logger), cptOrdId, cptExecId);

                if (this.NewCounterpartyTimeoutInfo != null)
                    this.NewCounterpartyTimeoutInfo(timeoutInfo);
            }
            else
            {
                char dkReason = m.GetChar(Tags.DKReason);
                Symbol symbol = (Symbol) m.GetString(Tags.Symbol);
                string priceIdentity = m.GetField(Tags.ClOrdID);

                if (dkReason == DKReason.NO_MATCHING_ORDER)
                {
                    this.Logger.Log(LogLevel.Fatal,
                        "Order Execution [{0}] was accepted late - integrator attempting to update positions info. Sessions will be stopped",
                        cptExecId);
                }
                else
                {
                    this.Logger.Log(LogLevel.Fatal,
                        "Unexpected DK reson ({0}) on integrator accpeted order: {1}. - integrator attempting to update positions info. Sessions will be stopped",
                        dkReason, cptExecId);
                }

                //CounterpartyRejectedDealDelegate(Symbol symbol, string integratorExecutionId, string counterpartyExecutionId, string integratorGeneratedIdentity);
                if (this.NewCounterpartyRejectedDeal != null)
                    this.NewCounterpartyRejectedDeal(symbol, null, cptExecId, priceIdentity);
            }
        }
    }


}
