﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.FIXMessaging
{
    public class MDSessionToPriceBookAdapter : IPriceBookStoreEx, IPriceChangesConsumer
    {
        private interface ISingleSymbolDirectionPriceBook : IChangeablePriceBook<PriceObjectInternal, Counterparty>
        {
            void AddPrice(PriceObjectInternal priceObject);
            void RemovePrices(Counterparty counterparty);
            bool RemoveIdentical(Counterparty counterparty, Guid identificator);
        }

        private ISingleSymbolDirectionPriceBook[][] _priceBooks;
        private Counterparty _counterparty;
        private int _counterpartiesNum;

        public MDSessionToPriceBookAdapter(Counterparty counterparty)
        {
            SingleSymbolDirectionPriceBook[][] priceBooksTemp = 
            {
                Enumerable.Range(0, Symbol.ValuesCount).Select(idx => new SingleSymbolDirectionPriceBook(BidTopPriceComparerInternal.Default, BidTopPriceComparerInternal.Default, PriceObjectInternal.GetNullBidInternal((Symbol) idx), counterparty)).ToArray(),
                Enumerable.Range(0, Symbol.ValuesCount).Select(idx => new SingleSymbolDirectionPriceBook(AskTopPriceComparerInternal.Default, AskTopPriceComparerInternal.Default, PriceObjectInternal.GetNullAskInternal((Symbol) idx), counterparty)).ToArray()
            };

            _priceBooks = priceBooksTemp;
            _counterparty = counterparty;
        }

        public MDSessionToPriceBookAdapter(Counterparty startCounterparty, int counterpartiesNum)
        {
            SingleSymbolDirectionLmaxLayersPriceBook[][] priceBooksTemp = 
            {
                Enumerable.Range(0, Symbol.ValuesCount).Select(idx => new SingleSymbolDirectionLmaxLayersPriceBook(BidTopPriceComparerInternal.Default, BidTopPriceComparerInternal.Default, PriceObjectInternal.GetNullBidInternal((Symbol) idx), startCounterparty, counterpartiesNum)).ToArray(),
                Enumerable.Range(0, Symbol.ValuesCount).Select(idx => new SingleSymbolDirectionLmaxLayersPriceBook(AskTopPriceComparerInternal.Default, AskTopPriceComparerInternal.Default, PriceObjectInternal.GetNullAskInternal((Symbol) idx), startCounterparty, counterpartiesNum)).ToArray()
            };

            _priceBooks = priceBooksTemp;
            _counterparty = startCounterparty;
            _counterpartiesNum = counterpartiesNum;
        }

        #region IBookTopProvider<PriceObject>

        public IChangeablePriceBook<PriceObjectInternal, Counterparty> GetChangeableBidPriceBook(Symbol symbol)
        {
            return this._priceBooks[(int)PriceSide.Bid][(int)symbol];
        }

        public IChangeablePriceBook<PriceObjectInternal, Counterparty> GetChangeableAskPriceBook(Symbol symbol)
        {
            return this._priceBooks[(int)PriceSide.Ask][(int)symbol];
        }

        public IBookTopExtended<PriceObjectInternal, Counterparty> GetBidPriceBook(Symbol symbol)
        {
            return this.GetChangeableBidPriceBook(symbol);
        }

        public IBookTopExtended<PriceObjectInternal, Counterparty> GetAskPriceBook(Symbol symbol)
        {
            return this.GetChangeableAskPriceBook(symbol);
        }

        public event BookPriceUpdate<PriceObjectInternal> NewNodeArrived
        {
            add
            {
                foreach (ISingleSymbolDirectionPriceBook priceBook in _priceBooks.SelectMany(o => o))
                {
                    priceBook.NewNodeArrived += value;
                }
            }
            remove
            {
                foreach (ISingleSymbolDirectionPriceBook priceBook in _priceBooks.SelectMany(o => o))
                {
                    priceBook.NewNodeArrived -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> ExistingNodeInvalidated
        {
            add
            {
                foreach (ISingleSymbolDirectionPriceBook priceBook in _priceBooks.SelectMany(o => o))
                {
                    priceBook.ExistingNodeInvalidated += value;
                }
            }
            remove
            {
                foreach (ISingleSymbolDirectionPriceBook priceBook in _priceBooks.SelectMany(o => o))
                {
                    priceBook.ExistingNodeInvalidated -= value;
                }
            }
        }

        public event BookMultiplePricesUpdate<PriceObjectInternal, Counterparty> NodesRemoved
        {
            add
            {
                foreach (ISingleSymbolDirectionPriceBook priceBook in _priceBooks.SelectMany(o => o))
                {
                    priceBook.NodesRemoved += value;
                }
            }
            remove
            {
                foreach (ISingleSymbolDirectionPriceBook priceBook in _priceBooks.SelectMany(o => o))
                {
                    priceBook.NodesRemoved -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> BookTopImproved
        {
            add
            {
                foreach (ISingleSymbolDirectionPriceBook priceBook in _priceBooks.SelectMany(o => o))
                {
                    priceBook.BookTopImproved += value;
                }
            }
            remove
            {
                foreach (ISingleSymbolDirectionPriceBook priceBook in _priceBooks.SelectMany(o => o))
                {
                    priceBook.BookTopImproved -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> BookTopDeterioratedInternal
        {
            add
            {
                foreach (ISingleSymbolDirectionPriceBook priceBook in _priceBooks.SelectMany(o => o))
                {
                    priceBook.BookTopDeterioratedInternal += value;
                }
            }
            remove
            {
                foreach (ISingleSymbolDirectionPriceBook priceBook in _priceBooks.SelectMany(o => o))
                {
                    priceBook.BookTopDeterioratedInternal -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> BookTopReplaced
        {
            add
            {
                foreach (ISingleSymbolDirectionPriceBook priceBook in _priceBooks.SelectMany(o => o))
                {
                    priceBook.BookTopReplaced += value;
                }
            }
            remove
            {
                foreach (ISingleSymbolDirectionPriceBook priceBook in _priceBooks.SelectMany(o => o))
                {
                    priceBook.BookTopReplaced -= value;
                }
            }
        }

        public TradingTargetType TradingTargetType
        {
            get { return this._counterparty.TradingTargetType; }
        }

        public PriceObjectInternal MaxNodeInternal
        {
            get { throw new NotImplementedException(); }
        }

        public PriceObjectInternal MaxNode
        {
            get { throw new NotImplementedException(); }
        }

        public DateTime MaxNodeUpdatedUtc
        {
            get { throw new NotImplementedException(); }
        }

        private class SingleSymbolDirectionPriceBook : ISingleSymbolDirectionPriceBook
        {
            private Func<PriceObjectInternal, PriceObjectInternal, int> _topCompareFunc;
            private Func<decimal, decimal, int> _topPriceCompareFunc;
            private PriceObjectInternal _nullPrice;
            private Counterparty _counterparty;

            public SingleSymbolDirectionPriceBook(IComparer<PriceObjectInternal> topComparer, IComparer<decimal> topPriceComparer, PriceObjectInternal nullPrice, Counterparty counterparty)
            {
                this._topCompareFunc = topComparer.Compare;
                this._topPriceCompareFunc = topPriceComparer.Compare;
                this._nullPrice = nullPrice;
                this.MaxNodeInternal = nullPrice;
                this._counterparty = counterparty;
            }

            public event BookPriceUpdate<PriceObjectInternal> NewNodeArrived;

            public event BookPriceUpdate<PriceObjectInternal> ExistingNodeInvalidated;

            public event BookMultiplePricesUpdate<PriceObjectInternal, Counterparty> NodesRemoved;

            public event BookPriceUpdate<PriceObjectInternal> BookTopImproved;

            public event BookPriceUpdate<PriceObjectInternal> BookTopDeterioratedInternal;

            public event BookPriceUpdate<PriceObjectInternal> BookTopReplaced;

            public PriceObjectInternal MaxNode
            {
                get { throw new NotImplementedException(); }
            }

            private volatile PriceObjectInternal _maxNodeInternal;
            private SpinLock _maxNodeSpinLock = new SpinLock(false);
            public PriceObjectInternal MaxNodeInternal
            {
                get
                {
                    // Unfortunately there is no other way than using same constrained region as code updating the top
                    //  Otherwise the top might be released, than acquired by someone else and we might not notice (ABA problem)
                    bool lockTaken = false;
                    _maxNodeSpinLock.Enter(ref lockTaken);

                    var maxNodeLocal = this._maxNodeInternal;
                    //Callers of MaxNodeInternal are releasing, so acquire here
                    maxNodeLocal.Acquire();

                    if (lockTaken)
                    {
                        _maxNodeSpinLock.Exit();
                    }

                    return maxNodeLocal;
                }
                private set
                {
                    bool lockTaken = false;
                    _maxNodeSpinLock.Enter(ref lockTaken);

                    var previousTop = this._maxNodeInternal;
                    this._maxNodeInternal = value;
                    if (value != null)
                        value.Acquire();
                    if (previousTop != null)
                        previousTop.Release();

                    if (lockTaken)
                    {
                        _maxNodeSpinLock.Exit();
                    }  
                }
            }

            public AtomicDecimal GetEstimatedBestPriceFast()
            {
                return this._maxNodeInternal.ConvertedPrice;
            }

            public decimal GetWeightedPriceForSize(ref decimal maxSizeOnPrice, int maximumBookDepthToBeUsed)
            {
                if (maximumBookDepthToBeUsed <= 0)
                {
                    maxSizeOnPrice = 0;
                    return 0;
                }

                var maxNodeLocal = this._maxNodeInternal;
                maxSizeOnPrice = Math.Max(maxSizeOnPrice, maxNodeLocal.SizeBaseAbsRemaining);
                decimal weightedPrice = maxNodeLocal.Price;

                if (maxNodeLocal.Counterparty == _counterparty && maxSizeOnPrice > 0m &&
                    !PriceObjectInternal.IsNullPrice(weightedPrice))
                    return weightedPrice;
                else
                {
                    maxSizeOnPrice = 0;
                    return 0;
                }
            }

            public decimal GetWeightedPriceForExactSize(decimal sizeOnPrice, int maximumBookDepthToBeUsed)
            {
                if (maximumBookDepthToBeUsed <= 0)
                {
                    return 0;
                }

                var maxNodeLocal = this._maxNodeInternal;
                decimal weightedPrice = maxNodeLocal.Price;

                if (maxNodeLocal.Counterparty == _counterparty && maxNodeLocal.SizeBaseAbsRemaining >= sizeOnPrice
                    && !PriceObjectInternal.IsNullPrice(weightedPrice))
                    return weightedPrice;
                else
                {
                    return 0;
                }
            }

            public decimal GetMaximumSizeForExactPrice(decimal worstAcceptablePrice, int maximumBookDepthToBeUsed, decimal maximumSizeToStopProbing)
            {
                if (maximumBookDepthToBeUsed <= 0)
                {
                    return 0;
                }
                
                var maxNodeLocal = this._maxNodeInternal;
                decimal weightedPrice = maxNodeLocal.Price;

                if (maxNodeLocal.Counterparty == _counterparty && this._topPriceCompareFunc(weightedPrice, worstAcceptablePrice) >= 0)
                    return maxNodeLocal.SizeBaseAbsRemaining;
                else
                {
                    return 0;
                }
            }

            public decimal GetMaximumSizeForExactPrice(decimal worstAcceptablePrice, int maximumBookDepthToBeUsed,
                decimal maximumSizeToStopProbing, Counterparty[] counterpartiesWhitelist)
            {
                throw new NotImplementedException("Whitelisting not supported");
            }

            public DateTime MaxNodeUpdatedUtc { get; private set; }

            public TradingTargetType TradingTargetType
            {
                get { return this._counterparty.TradingTargetType; }
            }

            public void AddPrice(PriceObjectInternal priceObject)
            {
                if(priceObject.Counterparty != this._counterparty)
                    return;

                int rank = this._topCompareFunc(priceObject, this._maxNodeInternal);
                this.MaxNodeInternal = priceObject;
                this.MaxNodeUpdatedUtc = HighResolutionDateTime.UtcNow;
                this.IsImprovement = rank > 0;

                if (rank > 0)
                {
                    if (ChangeableBookTopImproved != null)
                    {
                        ChangeableBookTopImproved(priceObject);
                    }
                    if (BookTopImproved != null)
                    {
                        BookTopImproved(this, priceObject, this.MaxNodeUpdatedUtc);
                    }
                }
                else if(rank < 0)
                {
                    if (ChangeableBookTopDeteriorated != null)
                    {
                        ChangeableBookTopDeteriorated(priceObject);
                    }
                    if (BookTopDeterioratedInternal != null)
                    {
                        BookTopDeterioratedInternal(this, priceObject, this.MaxNodeUpdatedUtc);
                    }
                }
                else
                {
                    if (ChangeableBookTopReplaced != null)
                    {
                        ChangeableBookTopReplaced(priceObject);
                    }
                    if (BookTopReplaced != null)
                    {
                        BookTopReplaced(this, priceObject, this.MaxNodeUpdatedUtc);
                    }
                }

                if (NewNodeArrived != null && !PriceObjectInternal.IsNullPrice(priceObject))
                {
                    NewNodeArrived(this, priceObject, priceObject.IntegratorReceivedTimeUtc);
                }
            }

            public void RemovePrices(Counterparty counterparty)
            {
                if(counterparty != this._counterparty)
                    return;

                RemoveTopPrice();
                if (NodesRemoved != null)
                {
                    NodesRemoved(this, _counterparty, this.MaxNodeUpdatedUtc);
                }
            }

            private void RemoveTopPrice()
            {
                MaxNodeInternal = _nullPrice;
                this.MaxNodeUpdatedUtc = HighResolutionDateTime.UtcNow;
                if (ChangeableBookTopDeteriorated != null)
                {
                    ChangeableBookTopDeteriorated(this._maxNodeInternal);
                }
                if (BookTopDeterioratedInternal != null)
                {
                    BookTopDeterioratedInternal(this, this._maxNodeInternal, this.MaxNodeUpdatedUtc);
                }
            }

            #region IChangeablePriceBook

            public event Action<PriceObjectInternal> ChangeableBookTopImproved;

            public event Action<PriceObjectInternal> ChangeableBookTopDeteriorated;

            public event Action<PriceObjectInternal> ChangeableBookTopReplaced;

            public bool RemoveIdentical(PriceObjectInternal value)
            {
                return this.RemoveIdentical(value.Counterparty, value.IntegratorIdentity);
            }

            public bool RemoveIdentical(Counterparty counterparty, Guid identificator)
            {
                return this.RemoveIdentical(identificator);
            }

            public bool RemoveIdentical(Guid identificator)
            {
                bool found = identificator.Equals(this._maxNodeInternal.IntegratorIdentity);
                if (found)
                {
                    RemoveTopPrice();
                }

                return found;
            }

            public void ResortIdentical(PriceObjectInternal value)
            {
                //this is it
            }

            public bool IsEmpty
            {
                get { return this._maxNodeInternal == _nullPrice; }
            }

            public PriceObjectInternal[] GetSortedClone()
            {
                //Should not be needed
                throw new NotImplementedException();
            }

            public PriceObjectInternal[] GetSortedClone(Func<PriceObjectInternal, bool> selectorFunc)
            {
                throw new NotImplementedException();
            }

            public bool IsImprovement { get; private set; }

            #endregion IChangeablePriceBook
        }


        private class SingleSymbolDirectionLmaxLayersPriceBook : ISingleSymbolDirectionPriceBook
        {
            private Func<PriceObjectInternal, PriceObjectInternal, int> _topCompareFunc;
            private Func<decimal, decimal, int> _topPriceCompareFunc;
            private PriceObjectInternal _nullPrice;
            private Counterparty _startCounterparty;

            public SingleSymbolDirectionLmaxLayersPriceBook(IComparer<PriceObjectInternal> topComparer,
                IComparer<decimal> topPriceComparer, PriceObjectInternal nullPrice, 
                Counterparty startCounterparty, int counterpartiesNum)
            {
                this._topCompareFunc = topComparer.Compare;
                this._topPriceCompareFunc = topPriceComparer.Compare;
                this._nullPrice = nullPrice;
                this._startCounterparty = startCounterparty;

                this._pricesLader = new PriceObjectInternal[counterpartiesNum];
                this._prices = new AtomicDecimal[counterpartiesNum];
                this._sizes = new decimal[counterpartiesNum];
                for (int priceIdx = 0; priceIdx < this._pricesLader.Length; priceIdx++)
                {
                    nullPrice.Acquire();
                    this._pricesLader[priceIdx] = nullPrice;
                }
            }

            public event BookPriceUpdate<PriceObjectInternal> NewNodeArrived;

            public event BookPriceUpdate<PriceObjectInternal> ExistingNodeInvalidated;

            public event BookMultiplePricesUpdate<PriceObjectInternal, Counterparty> NodesRemoved;

            public event BookPriceUpdate<PriceObjectInternal> BookTopImproved;

            public event BookPriceUpdate<PriceObjectInternal> BookTopDeterioratedInternal;

            public event BookPriceUpdate<PriceObjectInternal> BookTopReplaced;

            public PriceObjectInternal MaxNode
            {
                get { throw new NotImplementedException(); }
            }

            private volatile PriceObjectInternal[] _pricesLader;
            private AtomicDecimal[] _prices;
            private decimal[] _sizes;
            private SpinLock _maxNodeSpinLock = new SpinLock(false);

            private void ChangePrice(int priceIdx, PriceObjectInternal priceObject)
            {

                bool lockTaken = false;
                if (priceIdx == 0)
                {
                    // Unfortunately there is no other way than using same constrained region as code updating the top
                    //  Otherwise the top might be released, than acquired by someone else and we might not notice (ABA problem)
                    _maxNodeSpinLock.Enter(ref lockTaken);
                }

                var previousVal = this._pricesLader[priceIdx];
                this._pricesLader[priceIdx] = priceObject;
                this._prices[priceIdx] = priceObject.ConvertedPrice;
                this._sizes[priceIdx] = priceObject.SizeBaseAbsRemaining;
                priceObject.Acquire();
                previousVal.Release();

                if (lockTaken)
                {
                    _maxNodeSpinLock.Exit();
                }

            }

            //private volatile PriceObjectInternal _maxNodeInternal;
            public PriceObjectInternal MaxNodeInternal
            {
                get
                {

                    bool lockTaken = false;
                    // Unfortunately there is no other way than using same constrained region as code updating the top
                    //  Otherwise the top might be released, than acquired by someone else and we might not notice (ABA problem)
                    _maxNodeSpinLock.Enter(ref lockTaken);

                    var maxNodeLocal = this._pricesLader[0];
                    //Callers of MaxNodeInternal are releasing, so acquire here
                    maxNodeLocal.Acquire();

                    if (lockTaken)
                    {
                        _maxNodeSpinLock.Exit();
                    }

                    return maxNodeLocal;
                }
            }

            public AtomicDecimal GetEstimatedBestPriceFast()
            {
                return _prices[0];
            }

            public decimal GetMaximumSizeForExactPrice(decimal worstAcceptablePrice, int maximumBookDepthToBeUsed, decimal maximumSizeToStopProbing)
            {
                decimal currentMaxSize = 0m;
                decimal weightedPriceInternal = 0m;

                Thread.MemoryBarrier();

                int maxIdx = Math.Min(this._pricesLader.Length, maximumBookDepthToBeUsed);
                for (int idx = 0; idx < maxIdx; idx++)
                {
                    //using sizes and prices arrays as those are not subject to pooling ABA problem
                    // and so we can avoid need for synchronization
                    //BUT: be careful about meantime changes
                    // Torn read can happen here!! We need the AtomicSize (for now the Min function will save us)
                    decimal currentSize = _sizes[idx];
                    decimal currentLayerPrice = _prices[idx].ToDecimal();
                    if (currentSize > 0m && !PriceObjectInternal.IsNullPrice(currentLayerPrice))
                    {
                        decimal currentLayerSizeUsed;
                        //we can use current layer in whole
                        if (_topPriceCompareFunc(currentLayerPrice, worstAcceptablePrice) >= 0)
                        {
                            currentLayerSizeUsed = currentSize;
                        }
                        //need to calc how much we can use
                        else
                        {
                            decimal maxSizeToMeetWorstAcceptablePrice = (currentMaxSize * worstAcceptablePrice -
                                                                         weightedPriceInternal) /
                                                                        (currentLayerPrice - worstAcceptablePrice);

                            currentLayerSizeUsed = Math.Min(maxSizeToMeetWorstAcceptablePrice, currentSize);

                            if (maxSizeToMeetWorstAcceptablePrice <= 0)
                                break;
                        }

                        currentMaxSize += currentLayerSizeUsed;

                        if (currentMaxSize >= maximumSizeToStopProbing)
                            break;

                        weightedPriceInternal += currentLayerSizeUsed * currentSize;
                    }
                }

                return Math.Min(currentMaxSize, maximumSizeToStopProbing);
            }

            public decimal GetMaximumSizeForExactPrice(decimal worstAcceptablePrice, int maximumBookDepthToBeUsed,
                decimal maximumSizeToStopProbing, Counterparty[] counterpartiesWhitelist)
            {
                throw new NotImplementedException("Whitelisting not supported");
            }

            public decimal GetWeightedPriceForSize(ref decimal maxSizeOnPrice, int maximumBookDepthToBeUsed)
            {
                decimal remainingSize, weightedPriceInternal;
                weightedPriceInternal = this.GetWeightedPriceForSize_Internal(maxSizeOnPrice, maximumBookDepthToBeUsed,
                    out remainingSize);

                maxSizeOnPrice -= remainingSize;

                if (maxSizeOnPrice > 0)
                {
                    return weightedPriceInternal / maxSizeOnPrice;
                }
                else
                {
                    return 0;
                }
            }

            public decimal GetWeightedPriceForExactSize(decimal sizeOnPrice, int maximumBookDepthToBeUsed)
            {
                decimal remainingSize, weightedPriceInternal;
                weightedPriceInternal = this.GetWeightedPriceForSize_Internal(sizeOnPrice, maximumBookDepthToBeUsed,
                    out remainingSize);
                if (remainingSize <= 0 && sizeOnPrice > 0)
                {
                    return weightedPriceInternal / sizeOnPrice;
                }
                else
                {
                    return 0;
                }
            }

            public decimal GetWeightedPriceForSize_Internal(decimal maxSizeOnPrice, int maximumBookDepthToBeUsed, out decimal remainingSize)
            {
                remainingSize = maxSizeOnPrice;
                decimal weightedPriceInternal = 0m;

                Thread.MemoryBarrier();

                int maxIdx = Math.Min(this._pricesLader.Length, maximumBookDepthToBeUsed);
                for (int idx = 0; idx < maxIdx; idx++)
                {
                    //using sizes and prices arrays as those are not subject to pooling ABA problem
                    // and so we can avoid need for synchronization
                    //BUT: be careful about meantime changes
                    // Torn read can happen here!! We need the AtomicSize (for now the Min function will save us)
                    decimal currentSize = Math.Min(remainingSize, _sizes[idx]);
                    decimal currentPrice = _prices[idx].ToDecimal();
                    if (currentSize > 0m && !PriceObjectInternal.IsNullPrice(currentPrice))
                    {
                        weightedPriceInternal += currentSize * currentPrice;
                        remainingSize -= currentSize;
                        if (remainingSize <= 0)
                            break;
                    }
                }

                return weightedPriceInternal;
            }

            public DateTime MaxNodeUpdatedUtc { get; private set; }

            public TradingTargetType TradingTargetType
            {
                get { return this._startCounterparty.TradingTargetType; }
            }

            public void AddPrice(PriceObjectInternal priceObject)
            {
                int priceIdx = (int) priceObject.Counterparty - (int) this._startCounterparty;
                if(priceIdx >= this._pricesLader.Length || priceIdx < 0)
                    return;

                //if ToB is nullPrice and this is second layer - then treat it as first
                if (priceIdx == 1 && PriceObjectInternal.IsNullPrice(this._pricesLader[0]))
                {
                    priceIdx = 0;
                }

                var previousVal = this._pricesLader[priceIdx];

                int rank = priceIdx == 0 ? this._topCompareFunc(priceObject, previousVal) : 0;

                this.ChangePrice(priceIdx, priceObject);

                if (priceIdx == 0)
                {
                    this.MaxNodeUpdatedUtc = HighResolutionDateTime.UtcNow;
                    this.IsImprovement = rank > 0;

                    if (rank > 0)
                    {
                        if (ChangeableBookTopImproved != null)
                        {
                            ChangeableBookTopImproved(priceObject);
                        }
                        if (BookTopImproved != null)
                        {
                            BookTopImproved(this, priceObject, this.MaxNodeUpdatedUtc);
                        }
                    }
                    else if (rank < 0)
                    {
                        if (ChangeableBookTopDeteriorated != null)
                        {
                            ChangeableBookTopDeteriorated(priceObject);
                        }
                        if (BookTopDeterioratedInternal != null)
                        {
                            BookTopDeterioratedInternal(this, priceObject, this.MaxNodeUpdatedUtc);
                        }
                    }
                    else
                    {
                        if (ChangeableBookTopReplaced != null)
                        {
                            ChangeableBookTopReplaced(priceObject);
                        }
                        if (BookTopReplaced != null)
                        {
                            BookTopReplaced(this, priceObject, this.MaxNodeUpdatedUtc);
                        }
                    }

                    if (NewNodeArrived != null && !PriceObjectInternal.IsNullPrice(priceObject))
                    {
                        NewNodeArrived(this, priceObject, priceObject.IntegratorReceivedTimeUtc);
                    }
                }
            }

            public void RemovePrices(Counterparty counterparty)
            {
                int priceIdx = (int)counterparty - (int)this._startCounterparty;
                if (priceIdx >= this._pricesLader.Length || priceIdx < 0)
                    return;

                RemovePrice(priceIdx);
                if (priceIdx== 0 && NodesRemoved != null)
                {
                    NodesRemoved(this, counterparty, this.MaxNodeUpdatedUtc);
                }
            }


            private void RemovePrice(int priceIdx)
            {
                this.ChangePrice(priceIdx, _nullPrice);

                if (priceIdx == 0)
                {
                    this.MaxNodeUpdatedUtc = HighResolutionDateTime.UtcNow;
                    if (ChangeableBookTopDeteriorated != null)
                    {
                        ChangeableBookTopDeteriorated(_nullPrice);
                    }
                    if (BookTopDeterioratedInternal != null)
                    {
                        BookTopDeterioratedInternal(this, _nullPrice, this.MaxNodeUpdatedUtc);
                    }
                }
            }

            #region IChangeablePriceBook

            public event Action<PriceObjectInternal> ChangeableBookTopImproved;

            public event Action<PriceObjectInternal> ChangeableBookTopDeteriorated;

            public event Action<PriceObjectInternal> ChangeableBookTopReplaced;

            public bool RemoveIdentical(PriceObjectInternal value)
            {
                return this.RemoveIdentical(value.Counterparty, value.IntegratorIdentity);
            }

            public bool RemoveIdentical(Counterparty counterparty, Guid identificator)
            {
                int priceIdx = (int)counterparty - (int)this._startCounterparty;
                if (priceIdx >= this._pricesLader.Length || priceIdx < 0)
                    return false;

                bool found = identificator.Equals(this._pricesLader[priceIdx].IntegratorIdentity);
                if (found)
                {
                    RemovePrice(priceIdx);
                }

                return found;
            }

            public bool RemoveIdentical(Guid identificator)
            {
                int priceIdx = 0;
                for (; priceIdx < this._pricesLader.Length; priceIdx++)
                {
                    if(identificator.Equals(this._pricesLader[priceIdx].IntegratorIdentity))
                        break;
                }

                if (priceIdx < this._pricesLader.Length)
                {
                    RemovePrice(priceIdx);
                    return true;
                }

                return false;
            }

            public void ResortIdentical(PriceObjectInternal value)
            {
                //this is it
            }

            public bool IsEmpty
            {
                get { return this._pricesLader[0] == _nullPrice; }
            }

            public PriceObjectInternal[] GetSortedClone()
            {
                //Should not be needed
                throw new NotImplementedException();
            }

            public PriceObjectInternal[] GetSortedClone(Func<PriceObjectInternal, bool> selectorFunc)
            {
                throw new NotImplementedException();
            }

            public bool IsImprovement { get; private set; }

            #endregion IChangeablePriceBook
        }


        #endregion IBookTopProvider<PriceObject>

        #region IPriceChangesConsumer

        public void OnNewPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if (price == null)
                return;

            if(price.Price > 0 && price.SizeBaseAbsRemaining > 0)
                this._priceBooks[(int)price.Side][(int)price.Symbol].AddPrice(price);
        }

        public void OnIgnoredPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if (price == null)
                return;

            if (this.ConsumesIgnoredPrices)
                this.OnNewPrice(price, ref heldWithoutAcquireCall);
            else
                this.OnPriceCancel(price.Symbol, price.Counterparty, price.Side);
        }

        public void OnNewNonExecutablePrice(PriceObjectInternal price)
        {
            if (price == null)
                return;

            bool _ = true;
            if (this.ConsumesIgnoredPrices)
                this.OnNewPrice(price, ref _);
            else
                this.OnPriceCancel(price.Symbol, price.Counterparty, price.Side);
        }

        public void OnQuoteCancel(Symbol symbol, Counterparty counterparty)
        {
            this._priceBooks[(int) PriceSide.Bid][(int) symbol].RemovePrices(counterparty);
            this._priceBooks[(int) PriceSide.Ask][(int) symbol].RemovePrices(counterparty);
        }

        public void OnPriceCancel(Symbol symbol, Counterparty counterparty, PriceSide side)
        {
            this._priceBooks[(int) side][(int) symbol].RemovePrices(counterparty);
        }

        public void OnPriceInvalidate(Symbol symbol, PriceSide side, Counterparty counterparty, Guid integratorPriceIdnetity, DateTime integratorPriceReceivedUtc)
        {
            this._priceBooks[(int)side][(int)symbol].RemoveIdentical(counterparty, integratorPriceIdnetity);
        }

        public bool ConsumesDataFromCounterparty(Counterparty counterparty)
        {
            int distance = (int) counterparty - (int) _counterparty;

            return distance >= 0 && distance <= _counterpartiesNum;
        }

        public bool ConsumesMDDataIfOFNotRunning(Counterparty counterparty)
        {
            return false;
        }

        public bool ConsumesIgnoredPrices
        {
            get { return false; }
        }

        public bool ConsumesNonExecutablePrices
        {
            get { return false; }
        }

        #endregion IPriceChangesConsumer
    }
}
