﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.FIXMessaging
{
    /// <summary>
    /// Property bag for subscription request
    /// Property bag is read-only after the creation so that it cannot be changed after request submission
    /// </summary>
    public struct SubscriptionRequestInfo
    {
        public SubscriptionRequestInfo(Symbol symbol, decimal quantity)
            : this()
        {
            this.Symbol = symbol;
            this.Quantity = quantity;
        }

        public SubscriptionRequestInfo(Symbol symbol, decimal quantity, string senderSubId)
            : this(symbol, quantity)
        {
            this.SenderSubId = senderSubId;
        }

        public Symbol Symbol { get; private set; }
        public decimal Quantity { get; private set; }
        public string SenderSubId { get; private set; }
    }

    public enum SubscriptionStatus
    {
        NotSubscribed,
        Unknown,
        Subscribed,
        Rejected
    }

    public interface ISubscriptionInfo
    {
        string Identity { get; }
        SubscriptionStatus SubscriptionStatus { get; }
        SubscriptionRequestInfo SubscriptionRequestInfo { get; }
    }

    public interface IQuote
    {
        decimal AskPrice { get; }
        decimal BidPrice { get; }
        decimal AskSize { get; }
        decimal BidSize { get; }
        Symbol Symbol { get; }
        string Identity { get; }
        //IOrdersSession OrdersSession { get; }
    }

    public abstract class QuoteBase : IQuote
    {
        public decimal AskPrice { get; set; }
        public decimal BidPrice { get; set; }
        public decimal AskSize { get; set; }
        public decimal BidSize { get; set; }
        public Symbol Symbol { get; set; }
        public string Identity { get; set; }
    }

    public interface IMarektDataMessagingAdapter
    {
        bool SendSubscriptionMessage(ISubscriptionInfo subscriptionInfo);
        bool SendUnsubscriptionMessage(ISubscriptionInfo subscriptionInfo);
        event Action<IQuote> OnNewQuote;
        event Action<string> OnQuoteCancel;
        event Action<string> OnSubscriptionChanged;
    }




    /// <summary>
    /// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// </summary>



    public struct DealInfo
    {
        public DealDirection DealDirection { get; set; }
        public decimal DealSize { get; set; }
        public string Symbol { get; set; }
        public string SenderSubId { get; set; }
        public decimal? Price { get; set; }

        public bool CanBeSatisfied(IQuote quote)
        {
            return
                //is side populated in quote?
                (DealDirection == DealDirection.Buy
                        ? quote.AskPrice > 0
                        : quote.BidPrice > 0)
                &&
                //is it matchinq quote
                quote.Symbol.ToString().Equals(this.Symbol)
                &&
                //does the quote meet the pricing requirements
                (!Price.HasValue ||
                    (DealDirection == DealDirection.Buy
                        ? quote.AskPrice <= Price
                        : quote.BidPrice >= Price)
                   );
        }
    }

    public enum DealDirection
    {
        Buy,
        Sell
    }



    /// <summary>
    /// Property bag for order request
    /// Property bag is read-only after the creation so that it cannot be changed after request submission
    /// </summary>
    public struct OrderRequestInfo
    {
        public OrderRequestInfo(decimal totalAmount, decimal requestedPrice, DealDirection side, IQuote quote)
            : this()
        {
            this.TotalAmount = totalAmount;
            this.RequestedPrice = requestedPrice;
            this.Side = side;
            this.Quote = quote;
        }

        public OrderRequestInfo(decimal totalAmount, decimal requestedPrice, DealDirection side, IQuote quote, string senderSubId)
            : this(totalAmount, requestedPrice, side, quote)
        {
            this.SenderSubId = senderSubId;
        }

        public decimal TotalAmount { get; private set; }
        public decimal RequestedPrice { get; private set; }
        public DealDirection Side { get; private set; }
        public IQuote Quote { get; private set; }
        public string SenderSubId { get; private set; }

        public static OrderRequestInfo INVALID_ORDER_REQUEST = new OrderRequestInfo();

        public static OrderRequestInfo Create(DealInfo dealInfo, IQuote quote)
        {
            if (dealInfo.CanBeSatisfied(quote))
            {
                return new OrderRequestInfo
                    (dealInfo.DealSize,
                     dealInfo.DealDirection == DealDirection.Buy
                         ? quote.AskPrice
                         : quote.BidPrice,
                     dealInfo.DealDirection,
                     quote,
                     dealInfo.SenderSubId);
            }
            else
            {
                return OrderRequestInfo.INVALID_ORDER_REQUEST;
            }
        }
    }

    public enum OderStatus
    {
        NotSubmitted,
        Submitted,
        Filled,
        Rejected,
        InBrokenState,
        PartiallyFilled
    }

    public interface IExecutedOrderInfo
    {
        string Identity { get; }
        OderStatus OderStatus { get; }
        OrderRequestInfo OrderRequestInfo { get; }
        decimal FilledAmount { get; }
    }








    public interface IOrderFlowMessagingAdapter
    {
        bool SendOrderMessage(IExecutedOrderInfo orderInfo);
        event Action<string> OnOrderChange;
    }
}
