﻿using System;
using System.Collections.Generic;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.FIXMessaging
{
    public delegate void HandlePriceObject(PriceObjectInternal priceObject, ref bool heldWithoutAcquireCall);

    public interface IMarketDataSession: IFIXChannel
    {
        ISubscription Subscribe(SubscriptionRequestInfo subscriptionRequestInfo);
        bool Unsubscribe(ISubscription subscriptionInfo);
        IEnumerable<ISubscription> Subscriptions { get; }
        event Action<ISubscription> OnSubscriptionChanged;
        /*event Action<PriceObjectInternal> OnNewPrice;
        event Action<PriceObjectInternal> OnIgnoredPrice;*/
        event HandlePriceObject OnNewPrice;
        event Action<PriceObjectInternal> OnNewNonExecutablePrice;
        event HandlePriceObject OnIgnoredPrice;
        event Action<Symbol, Counterparty> OnCancelLastQuote;
        event Action<Symbol, Counterparty, PriceSide> OnCancelLastPrice;
    }
}
