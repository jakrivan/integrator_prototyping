﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.FIXMessaging
{
    public abstract class FIXSessionBase : IFIXChannel
    {
        private IFIXChannel _fixChannel;
        private string _identityPrefix;
        private int _counter;

        protected ILogger Logger { get; private set; }
        public Counterparty Counterparty { get; private set; }
        protected CancellationTokenSource GlobalCancellationTokenSource { get; private set; }

        protected FIXSessionBase(IFIXChannel fixChannel, ILogger logger, Counterparty counterparty)
        {
            this._fixChannel = fixChannel;
            this.Logger = logger;
            this.Counterparty = counterparty;

            this._fixChannel.OnStarted += this.OnFixChannelStart;
            this._fixChannel.OnStopped += this.OnFixChannelStop;

            this._identityPrefix = string.Format("{0}{1}{2}", counterparty, DateTime.UtcNow.ToString("yMMdd"),
                                                 ByteUtils.ConvertToBase(DateTime.UtcNow.Hour*60 + DateTime.UtcNow.Minute, true));
        }

        private const int MAX_ID_LENGTH = 20;

        protected string FormatIdentityString(Symbol symbol, string senderSubId)
        {
            int unused;
            return this.FormatIdentityStringInternal(symbol, senderSubId, out unused);
        }

        protected string FormatIdentityString(Symbol symbol, DealDirection side, out string globallyUniqueIdentityString)
        {
            int counterValue;
            string identityString = this.FormatIdentityStringInternal(symbol, null, out counterValue);
            globallyUniqueIdentityString = string.Format("{0}-{1}-{2}-{3}", GuidHelper.NewSequentialGuid().ToString("N"), this.Counterparty, side == DealDirection.Buy ? "B" : "S", symbol.ShortName);
            return identityString;
        }

        private string FormatIdentityStringInternal(Symbol symbol, string senderSubId, out int currentCounterValue)
        {
            currentCounterValue = Interlocked.Increment(ref _counter);
            string id = string.Format("{0}{1}{2}{3}", senderSubId, _identityPrefix, symbol.ShortName,
                ByteUtils.ConvertToBase(currentCounterValue, false));

            int length = id.Length;
            if (length > MAX_ID_LENGTH)
            {
                id = id.Substring(length - MAX_ID_LENGTH, MAX_ID_LENGTH);
            }
            return id;
        }

        private void OnFixChannelStart()
        {
            this.GlobalCancellationTokenSource = new CancellationTokenSource();
        }

        private void OnFixChannelStop()
        {
            if (this.GlobalCancellationTokenSource != null)
            {
                this.GlobalCancellationTokenSource.Cancel();
            }
        }

        public void Start()
        {
            try
            {
                this.OnFixChannelStart();
                this._fixChannel.Start();
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, e, "Unexpected exception when starting fix session for {0}", this.Counterparty);
                throw;
            }
        }

        public virtual void Stop()
        {
            try
            {
                this.OnFixChannelStop();
                this._fixChannel.Stop();
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, e, "Unexpected exception when stopping fix session for {0}", this.Counterparty);
                throw;
            }
        }

        public virtual void StopAsync(TimeSpan delay)
        {
            TaskEx.ExecAfterDelayWithErrorHandling(delay, this.Stop);
        }

        public event Action OnStarted
        {
            add { _fixChannel.OnStarted += value; }
            remove { _fixChannel.OnStopped -= value; }
        }

        public event Action OnStopped
        {
            add { _fixChannel.OnStopped += value; }
            remove { _fixChannel.OnStopped -= value; }
        }

        public event Action OnStopInitiated
        {
            add { _fixChannel.OnStopInitiated += value; }
            remove { _fixChannel.OnStopInitiated -= value; }
        }

        public SessionState State
        {
            get { return this._fixChannel.State; }
        }

        public bool Inactivated
        {
            get { return this._fixChannel.Inactivated; }
        }

        public bool IsReadyAndOpen
        {
            get { return this._fixChannel.IsReadyAndOpen; }
        }
    }
}
