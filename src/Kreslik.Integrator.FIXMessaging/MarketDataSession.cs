﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using QuickFix.Fields;
using Symbol = Kreslik.Integrator.Common.Symbol;

namespace Kreslik.Integrator.FIXMessaging
{
    public interface ITickerProvider
    {
        event Action<VenueDealObjectInternal> NewVenueDeal;
    }

    public class VenueMarketDataSession : MarketDataSession, ITickerProvider
    {
        private IVenueFIXConnector _venueMessagingAdapter;

        internal object FixChannel { get { return _venueMessagingAdapter; } }

        internal VenueMarketDataSession(IVenueFIXConnector venueMessagingAdapter,
                                      IMarketDataMessagingAdapter marketDataMessagingAdapter,
                                      ILogger logger, Counterparty counterparty, bool autoResubscribe,
                                      MarketDataSessionSettings settings, IDelayEvaluator delayEvaluator)
            : base(venueMessagingAdapter, marketDataMessagingAdapter, logger, counterparty, autoResubscribe, settings, delayEvaluator)
        {
            this._venueMessagingAdapter = venueMessagingAdapter;
        }

        public event Action<VenueDealObjectInternal> NewVenueDeal
        {
            add { this._venueMessagingAdapter.NewVenueDeal += value; }
            remove { this._venueMessagingAdapter.NewVenueDeal -= value; }
        }
    }

    public class MarketDataSession : FIXSessionBase, IMarketDataSession
    {
        private IMarketDataMessagingAdapter _marketDataMessagingAdapter;
        private Dictionary<string, WritableSubscription> _subscriptionInfoMap = new Dictionary<string, WritableSubscription>();
        private int _unconfirmedSubscriptionsNum = 0;
        private bool _autoResubscribe;
        private IDelayEvaluator _delayEvaluator;

        internal MarketDataSession(IFIXChannel fixChannel, IMarketDataMessagingAdapter marketDataMessagingAdapter,
                                 ILogger logger, Counterparty counterparty, bool autoResubscribe, MarketDataSessionSettings settings, IDelayEvaluator delayEvaluator)
            : base(fixChannel, logger, counterparty)
        {
            this._settings = settings;
            this._marketDataMessagingAdapter = marketDataMessagingAdapter;
            this._autoResubscribe = autoResubscribe;
            this._delayEvaluator = delayEvaluator;

            //clear state of adapter (shared state, pool fetches ..)
            fixChannel.OnStopped += _marketDataMessagingAdapter.ClearInternalStateAfterStop;

            //subscriptions
            if (autoResubscribe)
            {
                fixChannel.OnStarted += ResubmitActiveSubscriptions;
            }
            else
            {
                fixChannel.OnStopped += () =>
                {
                    _subscriptionInfoMap.Clear();
                    _unconfirmedSubscriptionsNum = 0;
                };
            }
            marketDataMessagingAdapter.OnSubscriptionChanged += this.InvokeSubscriptionStateChange;
            marketDataMessagingAdapter.OnCancelLastQuote += InvokeOnCancelLastQuoteInternal;
            marketDataMessagingAdapter.OnNewQuote += this.InvokeOnNewQuoteInternal;
            marketDataMessagingAdapter.OnNewPrice += this.InvokeOnNewPriceInternal;
            marketDataMessagingAdapter.OnNewNonexecutablePrice += InvokeOnNewNonExecutablePriceInternal;
            marketDataMessagingAdapter.OnBusinessReject += this.InvokeOnBusinessReject;

            //if(counterparty == Counterparty.CTI)
            //    _priceImprovementEvaluator = new PriceImprovementEvaluator(true);
        }

        internal MarketDataSession(IFIXChannel fixChannel, IMarketDataMessagingAdapter marketDataMessagingAdapter,
                                 ILogger logger, Counterparty counterparty, MarketDataSessionSettings settings, IDelayEvaluator delayEvaluator)
            : this(fixChannel, marketDataMessagingAdapter, logger, counterparty, false, settings, delayEvaluator)
        { }

        private MarketDataSessionSettings _settings;

        public ISubscription Subscribe(SubscriptionRequestInfo subscriptionRequestInfo)
        {
            if (this.State != SessionState.Running)
            {
                this.Logger.Log(LogLevel.Error, "Cannot subscribe to price stream in current state: [{0}]", this.State);
                return null;
            }

            //PrimeXM allows only 3 characters for subscription id
            string subscriptionIdentity =
                this.Counterparty == Counterparty.PX1
                    ? ((int) subscriptionRequestInfo.Symbol).ToString()
                    : string.Format("{0}_{1}_001", this.Counterparty, subscriptionRequestInfo.Symbol.ShortName);

            WritableSubscription subscription;
            if (!this._subscriptionInfoMap.TryGetValue(subscriptionIdentity, out subscription))
            {
                subscription = new WritableSubscription()
                {
                    Identity = subscriptionIdentity,
                    SubscriptionRequestInfo = subscriptionRequestInfo,
                    SubscriptionStatus = SubscriptionStatus.NotSubscribed
                };
                subscription.OnTimeout += OnUnconfirmedSubscriptionTimeout;
                this._subscriptionInfoMap.Add(subscriptionIdentity, subscription);
            }
            else if (subscription.SubscriptionRequestInfo.Quantity != subscriptionRequestInfo.Quantity)
            {
                this.Logger.Log(LogLevel.Warn, "Cannot request different quantity for existing subscription: [{0}]", subscriptionIdentity);
                return null;
            }

            //As this was requested externally - reset the rejections count
            subscription.RejectionsCount = 0;

            return SubscribeInternal(subscription);
        }

        private ISubscription SubscribeInternal(WritableSubscription subscription)
        {
            if (subscription.RejectionsCount >= this._settings.MaxAutoResubscribeCount)
            {
                this.Logger.Log(LogLevel.Error,
                                "Sunscription [{0}] exceeded the autoresubscribe count of {1}. Not autoresubscribing anymore",
                                subscription.Identity, this._settings.MaxAutoResubscribeCount);
                return null;
            }

            if (subscription.SubscriptionStatus == SubscriptionStatus.Subscribed)
            {
                this.Logger.Log(LogLevel.Error, "Subscription [{0}] is already present in subscribed state. Ignoring other attempt for identical subscription",
                    subscription.Identity);

                return null;
            }

            //this is to prevent situation where two or more requests were sent before we got any reply
            // if we would increment always then we wouldn't never decrement to 0
            bool subscribingFirstTime = false;
            if (subscription.SubscriptionStatus != SubscriptionStatus.Unknown)
            {
                int unconfirmedSubscriptionsNum = Interlocked.Increment(ref _unconfirmedSubscriptionsNum);
                this.Logger.Log(LogLevel.Trace, "Unconfirmed subscription count: {0}", unconfirmedSubscriptionsNum);
                subscription.SubscriptionStatus = SubscriptionStatus.Unknown;
                subscribingFirstTime = true;
            }

            bool successfulySent = false;
            try
            {
                this._marketDataMessagingAdapter.AddSubscriptionIdentity(subscription.Identity);
                successfulySent =
                    this._marketDataMessagingAdapter.SendSubscriptionMessage(subscription.SubscriptionRequestInfo,
                                                                             subscription.Identity);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Error, e, "Adapter for {0} failed to send a subscription message", this.Counterparty);
            }

            if (successfulySent)
            {
                subscription.SetTimeout(this._settings.UnconfirmedSubscriptionTimeout);
                return subscription;
            }
            else
            {
                if (subscribingFirstTime)
                {
                    int unconfirmedSubscriptionsNum = Interlocked.Decrement(ref  _unconfirmedSubscriptionsNum);
                    this.Logger.Log(LogLevel.Trace, "Unconfirmed subscription count: {0}", unconfirmedSubscriptionsNum);
                    if (_unconfirmedSubscriptionsNum < 0)
                    {
                        this.Logger.Log(LogLevel.Error, "Unconfirmed subscription count below zero! Reseting it.");
                        _unconfirmedSubscriptionsNum = 0;
                    }
                }
                subscription.SubscriptionStatus = SubscriptionStatus.NotSubscribed;
                this.Logger.Log(LogLevel.Error, "Messaging layer failed during sending subscription [{0}]", subscription.Identity);

                return null;
            }
        }

        private void OnUnconfirmedSubscriptionTimeout(WritableSubscription writableSubscription)
        {
            if (writableSubscription.SubscriptionStatus == SubscriptionStatus.Subscribed_ValidationPending)
            {
                this.Logger.Log(LogLevel.Error,
                                "Subscription couldn't be validated within the timeout (probably we were only receiving indicative quotes). Setting it to Subscribed to prevent performance issues, however stream validity should be reviewed manually. {0}",
                                writableSubscription);
                this.InvokeSubscriptionStateChangeInternal(writableSubscription, SubscriptionStatus.Subscribed);
            }
            else
            {
                this.Logger.Log(LogLevel.Error, "Subscription is timing out - we haven't received any confirmation from counterparty. It should be reviewed later manualy. {0}",
                                writableSubscription);
                this.InvokeSubscriptionStateChangeInternal(writableSubscription, SubscriptionStatus.Subscribed);
            }
        }

        private async void SubscribeInternalWithDelay(WritableSubscription subscription)
        {
            if (subscription.RejectionsCount >= 0)
            {
                if (subscription.RejectionsCount >= this._settings.MaxAutoResubscribeCount)
                {
                    this.Logger.Log(LogLevel.Error,
                                    "Subscription [{0}] exceeded the autoresubscribe count of {1}. Not autoresubscribing anymore",
                                    subscription.Identity, this._settings.MaxAutoResubscribeCount);
                    return;
                }

                try
                {
                    TimeSpan retryInterval = subscription.RejectionsCount >= this._settings.RetryIntervalsSequence.Length
                                                 ? this._settings.RetryIntervalsSequence[this._settings.RetryIntervalsSequence.Length - 1]
                                                 : this._settings.RetryIntervalsSequence[subscription.RejectionsCount];
                    await Task.Delay(retryInterval, GlobalCancellationTokenSource.Token);
                }
                catch (TaskCanceledException)
                {
                    this.Logger.Log(LogLevel.Warn,
                                    "Subscription [{0}] waited for resubscription attempt, but shutdown request came. Not autoresubscribing anymore",
                                    subscription.Identity);
                    return;
                }
            }

            this.SubscribeInternal(subscription);
        }

        public bool Unsubscribe(ISubscription subscriptionInfo)
        {
            if (subscriptionInfo == null)
            {
                this.Logger.Log(LogLevel.Error, "Invoking Unsubscribe with null argument!");
                return false;
            }

            if (this.State != SessionState.Running)
            {
                this.Logger.Log(LogLevel.Error, "Cannot unsubscribe from price stream in current state: [{0}]",
                                 this.State);
                return false;
            }

            string subscriptionIdentity = subscriptionInfo.Identity;
            WritableSubscription subscription;
            if (!this._subscriptionInfoMap.TryGetValue(subscriptionIdentity, out subscription))
            {
                this.Logger.Log(LogLevel.Error, "Receiving unsubscription attempt for nonexisting subscription [{0}]",
                                 subscriptionIdentity);
                return false;
            }

            if (subscription.SubscriptionStatus == SubscriptionStatus.NotSubscribed ||
                subscription.SubscriptionStatus == SubscriptionStatus.Rejected ||
                subscription.SubscriptionStatus == SubscriptionStatus.Broken)
            {
                this.Logger.Log(LogLevel.Error,
                                 "Subscription [{0}] is already in unsubscribed state - ignoring attempt for unsubscription.",
                                 subscriptionIdentity);
                return false;
            }

            bool successfulySent = false;
            try
            {
                successfulySent =
                    this._marketDataMessagingAdapter.SendUnsubscriptionMessage(subscription.SubscriptionRequestInfo,
                                                                               subscriptionIdentity);
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Error, e, "Adapter for {0} failed to send an unsubscription message", this.Counterparty);
            }

            if (!successfulySent)
            {
                this.Logger.Log(LogLevel.Error,
                                 "Unsubscription [{0}] failed due to messaging layer error(s). Unsubscription can be resubmitted",
                                 subscriptionIdentity);
                return false;
            }

            this.InvokeSubscriptionStateChangeInternal(subscription, SubscriptionStatus.NotSubscribed);

            return true;
        }

        private void ResubmitActiveSubscriptions()
        {
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(3));
            _unconfirmedSubscriptionsNum = 0;

            foreach (
                WritableSubscription writableSubscription in
                    _subscriptionInfoMap.Values/*.Where(
                        sub =>
                        sub.SubscriptionStatus != SubscriptionStatus.NotSubscribed &&
                        sub.SubscriptionStatus != SubscriptionStatus.Broken)*/
                )
            {
                writableSubscription.SubscriptionStatus = SubscriptionStatus.Rejected;
                writableSubscription.RejectionsCount = 0;
                this.SubscribeInternal(writableSubscription);
            }
        }

        private void InvokeSubscriptionStateChangeInternal(WritableSubscription subscription, SubscriptionStatus newSubscriptionStatus)
        {
            // is this a change of state at all?
            if (subscription.SubscriptionStatus != newSubscriptionStatus)
            {
                // rejected subscriptions can be also a way of confirming unsubscription
                if (newSubscriptionStatus == SubscriptionStatus.Rejected)
                {
                    // this looks like simple confirmation of unsubscription
                    if (subscription.SubscriptionStatus == SubscriptionStatus.NotSubscribed ||
                        subscription.SubscriptionStatus == SubscriptionStatus.Broken)
                    {
                        this.Logger.Log(LogLevel.Debug,
                                        "Receiving subscription reject (likely unsubscription confirmation) for subscription [{0}]",
                                        subscription.Identity);

                        // subscription is not subscribed anyway - so no reason to change state and bubble up
                        return;
                    }
                    // this looks like an unexpeced subscription reject
                    else
                    {
                        this.Logger.Log(LogLevel.Warn,
                                        "Receiving reject (without prior unsubscription request) for subscription [{0}]",
                                        subscription.Identity);
                        subscription.RejectionsCount++;
                    }
                }
                //this is not rejection - so reseting the rejection count
                else
                {
                    subscription.RejectionsCount = 0;
                }

                //if this is change to subscription that was not yet confirmed/rejected or that was not yet validated
                if (subscription.SubscriptionStatus == SubscriptionStatus.Unknown || subscription.SubscriptionStatus == SubscriptionStatus.Subscribed_ValidationPending)
                {
                    //Validation pending will ned to be yet validated - so we need to continue in checking quotes
                    //Broken will get unsubscribed first (which will decrement counter as needed)
                    if (newSubscriptionStatus != SubscriptionStatus.Subscribed_ValidationPending &&
                        newSubscriptionStatus != SubscriptionStatus.Broken)
                    {
                        subscription.StopTimeout();
                        int unconfirmedSubscriptionsNum = Interlocked.Decrement(ref  _unconfirmedSubscriptionsNum);
                        this.Logger.Log(LogLevel.Trace, "Unconfirmed subscription count: {0}",
                                        unconfirmedSubscriptionsNum);
                        if (_unconfirmedSubscriptionsNum < 0)
                        {
                            this.Logger.Log(LogLevel.Error, "Unconfirmed subscription count below zero! Reseting it.");
                            _unconfirmedSubscriptionsNum = 0;
                        }
                    }
                }

                if (newSubscriptionStatus == SubscriptionStatus.Broken)
                {
                    this.Logger.Log(LogLevel.Error, "Subscription [{0}] was found invalid, unsubscribing.",
                                    subscription.Identity);
                    this.Unsubscribe(subscription);
                }

                subscription.SubscriptionStatus = newSubscriptionStatus;

                if (this.OnSubscriptionChanged != null)
                {
                    this.OnSubscriptionChanged(subscription);
                }

                if (subscription.SubscriptionStatus == SubscriptionStatus.Rejected && this._autoResubscribe)
                {
                    this.Logger.Log(LogLevel.Debug, "Autoresubscribing subscription [{0}] for the {1}. time",
                                    subscription.Identity, subscription.RejectionsCount);
                    this.SubscribeInternalWithDelay(subscription);
                }
            }
        }

        private void InvokeSubscriptionStateChange(SubscriptionChangeInfo subscriptionChangeInfo)
        {
            WritableSubscription subscription;
            if (this._subscriptionInfoMap.TryGetValue(subscriptionChangeInfo.Identity, out subscription))
            {
                InvokeSubscriptionStateChangeInternal(subscription, subscriptionChangeInfo.SubscriptionStatus);
            }
            // Unsubscription for all subscriptions.
            //  this code path currently used by: CRS
            else if (string.Equals(subscriptionChangeInfo.Identity, "*", StringComparison.Ordinal))
            {
                _unconfirmedSubscriptionsNum = 0;

                foreach (
                    WritableSubscription activeSubscription in
                        this._subscriptionInfoMap.Values.Where(
                            s =>
                            s.SubscriptionStatus == SubscriptionStatus.Subscribed ||
                            s.SubscriptionStatus == SubscriptionStatus.Unknown ||
                            s.SubscriptionStatus == SubscriptionStatus.Subscribed_ValidationPending))
                {
                    activeSubscription.StopTimeout();
                    activeSubscription.SubscriptionStatus = SubscriptionStatus.Rejected;
                    activeSubscription.RejectionsCount++;
                    if (this.OnSubscriptionChanged != null)
                    {
                        this.OnSubscriptionChanged(activeSubscription);
                    }

                    if (this._autoResubscribe)
                    {
                        this.Logger.Log(LogLevel.Debug, "Autoresubscribing subscription [{0}] for the {1}. time",
                                        activeSubscription.Identity, activeSubscription.RejectionsCount);
                        this.SubscribeInternalWithDelay(activeSubscription);
                    }
                }
            }
            else
            {
                this.Logger.Log(LogLevel.Error, "Receiving subscription change for nonexisting subscription [{0}]",
                                 subscriptionChangeInfo.Identity);
            }
        }

        private class PriceImprovementEvaluator
        {
            private decimal[] _lastBids;
            private decimal[] _lastAsks;
            private readonly bool _checkPricesImproving = false;

            public PriceImprovementEvaluator(bool checkPricesImproving)
            {
                this._checkPricesImproving = checkPricesImproving;
                if (checkPricesImproving)
                {
                    _lastBids = Enumerable.Repeat(decimal.MaxValue, Symbol.ValuesCount).ToArray();
                    _lastAsks = Enumerable.Repeat(decimal.MinValue, Symbol.ValuesCount).ToArray();
                }
            }

            public bool IsIgnoredPrice(PriceObject price)
            {
                if (!_checkPricesImproving)
                    return false;

                bool isImprovement;
                if (price.Side == PriceSide.Ask)
                {
                    isImprovement = price.Price < _lastAsks[(int)price.Symbol];
                    _lastAsks[(int)price.Symbol] = price.Price;
                }
                else
                {
                    isImprovement = price.Price > _lastBids[(int)price.Symbol];
                    _lastBids[(int)price.Symbol] = price.Price;
                }

                return !isImprovement;
            }
        }

        private readonly PriceImprovementEvaluator _priceImprovementEvaluator = null;

        //private bool IsIgnoredPrice(PriceObject price)
        //{
        //    if (_priceImprovementEvaluator == null)
        //        return false;

        //    return this._priceImprovementEvaluator.IsIgnoredPrice(price);
        //}

        private void CheckUnconfirmedSubscriptions(string subscriptionIdentity)
        {
            if (_unconfirmedSubscriptionsNum > 0)
            {
                this.Logger.Log(LogLevel.Debug,
                                 "Unconfirmed subscription count: {0}. This is OK short time after subscribing. After that it should raise concerns (performance of quotes receiving can be affected)",
                                 _unconfirmedSubscriptionsNum);

                WritableSubscription subscription;
                if (this._subscriptionInfoMap.TryGetValue(subscriptionIdentity, out subscription))
                {
                    this.InvokeSubscriptionStateChangeInternal(subscription, SubscriptionStatus.Subscribed);
                }
                else
                {
                    this.Logger.Log(LogLevel.Error, "Receiving pricing from unknown subscription: {0}", subscriptionIdentity);
                }
            }
        }

        private void InvokeOnNewNonExecutablePriceInternal(PriceObjectInternal priceObjectInternal)
        {
            if (this.OnNewNonExecutablePrice != null)
                this.OnNewNonExecutablePrice(priceObjectInternal);
        }
        private void InvokeOnNewPriceInternal(PriceObjectInternal priceObject, string subscriptionIdentity, ref bool heldWithoutAcquireCall)
        {
            this.CheckUnconfirmedSubscriptions(subscriptionIdentity);

            bool isDelayed = this._delayEvaluator.IsReceivedObjectDelayed(priceObject);


            //order is important - we need to call into IsIgnoredPrice
            if (/*this.IsIgnoredPrice(priceObject) ||*/ isDelayed)
                this.OnIgnoredPriceInternal(priceObject, ref heldWithoutAcquireCall);
            else
                this.OnNewPriceInternal(priceObject, ref heldWithoutAcquireCall);
        }

        private void InvokeOnNewQuoteInternal(QuoteObject quote, ref bool askHeldWithoutAcquireCall, ref bool bidHeldWithoutAcquireCall)
        {
            this.CheckUnconfirmedSubscriptions(quote.SubscriptionIdentity);

            bool isDelayed = this._delayEvaluator.IsReceivedObjectDelayed(quote);

            if (quote.AskPrice != null)
            {
                //order is important - we need to call into IsIgnoredPrice
                if (/*this.IsIgnoredPrice(quote.AskPrice) ||*/ isDelayed)
                    this.OnIgnoredPriceInternal(quote.AskPrice, ref askHeldWithoutAcquireCall);
                else
                    this.OnNewPriceInternal(quote.AskPrice, ref askHeldWithoutAcquireCall);
            }

            if (quote.BidPrice != null)
            {
                //order is important - we need to call into IsIgnoredPrice
                if (/*this.IsIgnoredPrice(quote.BidPrice) ||*/ isDelayed)
                    this.OnIgnoredPriceInternal(quote.BidPrice, ref bidHeldWithoutAcquireCall);
                else
                    this.OnNewPriceInternal(quote.BidPrice, ref bidHeldWithoutAcquireCall);
            }
        }

        private void OnIgnoredPriceInternal(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if (this.OnIgnoredPrice != null)
                this.OnIgnoredPrice(price, ref heldWithoutAcquireCall);
        }

        private void OnNewPriceInternal(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if (this.OnNewPrice != null)
                this.OnNewPrice(price, ref heldWithoutAcquireCall);
        }

        private void InvokeOnCancelLastQuoteInternal(string subscriptionIdentity)
        {
            WritableSubscription subscription = null;
            if (this._subscriptionInfoMap.TryGetValue(subscriptionIdentity, out subscription))
            {
                //if we have unconfirmed subscriptions AND this is one of the unconfirmed subscriptions
                if (_unconfirmedSubscriptionsNum > 0 &&
                    subscription.SubscriptionStatus != SubscriptionStatus.Subscribed &&
                    subscription.SubscriptionStatus != SubscriptionStatus.Subscribed_ValidationPending)
                {
                    this.Logger.Log(LogLevel.Debug,
                                    "Unconfirmed subscription count: {0}. This is OK short time after subscribing. After that it should raise concerns (performance of quotes receiving can be affected)",
                                    _unconfirmedSubscriptionsNum);


                    this.InvokeSubscriptionStateChange(new SubscriptionChangeInfo(subscriptionIdentity,
                                                                                  SubscriptionStatus
                                                                                      .Subscribed_ValidationPending));
                }

                if (this.OnCancelLastQuote != null)
                {
                    this.OnCancelLastQuote(subscription.SubscriptionRequestInfo.Symbol, this.Counterparty);
                }

            }
            else if (string.Equals(subscriptionIdentity, "*", StringComparison.Ordinal))
            {
                this.Logger.Log(LogLevel.Info, "Receiving subscription/quote cancel for all active subscriptions");
                this.InvokeSubscriptionStateChange(new SubscriptionChangeInfo(subscriptionIdentity, SubscriptionStatus.Rejected));
            }
            else
            {
                this.Logger.Log(LogLevel.Error, "Receiving event cancelling last quote for unknown subscription identity: [{0}]", subscriptionIdentity);
            }
        }

        private void InvokeOnBusinessReject(QuickFix.Message bussinessReject)
        {
            if (_unconfirmedSubscriptionsNum > 0)
            {
                if (_unconfirmedSubscriptionsNum > 1)
                {
                    this.Logger.Log(LogLevel.Error,
                                     "Receiving business reject while {0} subscriptions are unconfirmed. Cannot decide which to cancel",
                                     _unconfirmedSubscriptionsNum);
                    return;
                }
                else
                {
                    WritableSubscription unconfirmedSubscription =
                        this._subscriptionInfoMap.Where(
                            sub => sub.Value.SubscriptionStatus == SubscriptionStatus.Unknown)
                            .Select(s => s.Value)
                            .FirstOrDefault();

                    if (unconfirmedSubscription != null)
                    {
                        this.Logger.Log(LogLevel.Error,
                             "Receiving business reject while [{0}] is the only unconfirmed subscription - so cancelling it.", unconfirmedSubscription.Identity);
                        this.InvokeSubscriptionStateChangeInternal(unconfirmedSubscription, SubscriptionStatus.Rejected);
                        return;
                    }
                }
            }

            this.Logger.Log(LogLevel.Error,
                             "Receiving business reject but cannot find unconfirmed subscription to cancel.");
        }

        public IEnumerable<ISubscription> Subscriptions
        {
            get { return this._subscriptionInfoMap.Values; }
        }

        public event Action<ISubscription> OnSubscriptionChanged;

        public event HandlePriceObject OnNewPrice;
        public event Action<PriceObjectInternal> OnNewNonExecutablePrice;
        public event HandlePriceObject OnIgnoredPrice;

        public event Action<Symbol, Counterparty> OnCancelLastQuote;
        public event Action<Symbol, Counterparty, PriceSide> OnCancelLastPrice
        {
            add { this._marketDataMessagingAdapter.OnCancelLastPrice += value; }
            remove { this._marketDataMessagingAdapter.OnCancelLastPrice -= value; }
        }

        private class WritableSubscription : ISubscription
        {
            public string Identity { get; set; }
            public SubscriptionRequestInfo SubscriptionRequestInfo { get; set; }
            public SubscriptionStatus SubscriptionStatus { get; set; }
            public int RejectionsCount { get; set; }
            public event Action<WritableSubscription> OnTimeout;

            private SafeTimer _timeoutTimer;

            public void SetTimeout(TimeSpan span)
            {
                if (this._timeoutTimer != null)
                {
                    this._timeoutTimer.Dispose();
                }

                this._timeoutTimer = new SafeTimer(TimeoutCallback, span, Timeout.InfiniteTimeSpan);
            }

            public void StopTimeout()
            {
                if (this._timeoutTimer != null)
                {
                    this._timeoutTimer.Dispose();
                    this._timeoutTimer = null;
                }
            }

            private void TimeoutCallback()
            {
                this._timeoutTimer.Dispose();
                this._timeoutTimer = null;
                if (OnTimeout != null)
                {
                    OnTimeout(this);
                }
            }

            public override string ToString()
            {
                return string.Format("Subscription [{0}], RejectCount: {1}, Status: {2}", Identity, RejectionsCount,
                                     SubscriptionStatus);
            }
        }
    }
}
