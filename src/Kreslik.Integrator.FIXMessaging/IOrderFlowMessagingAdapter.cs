﻿using System;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.FIXMessaging
{
    public interface IOrderFlowMessagingAdapter
    {
        bool SendOrderMessage(OrderRequestInfo orderInfo, string identity, out string outgoingFixMessageRaw);
        event Action<OrderChangeInfo> OnOrderChanged;
        event Action<QuickFix.Message> OnBusinessReject;
    }
}
