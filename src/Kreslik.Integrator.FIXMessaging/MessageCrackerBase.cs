﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using QuickFix;

namespace Kreslik.Integrator.FIXMessaging
{
    /// <summary>
    /// Helper class for delegating message types for various FIX versions to
    /// type-safe OnMessage methods.
    /// </summary>
    public abstract class MessageCrackerBase<TDerivedType> where TDerivedType : MessageCrackerBase<TDerivedType>
    {
        private Dictionary<Type, Action<TDerivedType, QuickFix.Message, QuickFix.SessionID>> _handlerMethods =
            new Dictionary<Type, Action<TDerivedType, QuickFix.Message, QuickFix.SessionID>>();

        public MessageCrackerBase()
        {
            initialize(this);
        }

        private void initialize(Object messageHandler)
        {
            Type handlerType = messageHandler.GetType();

            MethodInfo[] methods = handlerType.GetMethods();
            foreach (MethodInfo m in methods)
            {
                if (IsHandlerMethod(m))
                {
                    _handlerMethods[m.GetParameters()[0].ParameterType] = MagicMethod<TDerivedType>(m);
                }
            }
        }

        //Precompiling the delegate is far more efficient (also no GC will be involved during calling)
        // and it's type safe and exceptions are being thrown directly
        private Action<T, QuickFix.Message, QuickFix.SessionID> MagicMethod<T>(MethodInfo method)
        {
            var parameter1 = method.GetParameters().First();
            var parameter2 = method.GetParameters().Single(p => p != parameter1);
            var instance = Expression.Parameter(typeof(T), "instance");
            var argument1 = Expression.Parameter(typeof(QuickFix.Message), "messageArgument");
            var argument2 = Expression.Parameter(typeof(QuickFix.SessionID), "sessionIdArgument");
            var methodCall = Expression.Call(
                instance,
                method,
                Expression.Convert(argument1, parameter1.ParameterType),
                Expression.Convert(argument2, parameter2.ParameterType)
                );
            return Expression.Lambda<Action<T, QuickFix.Message, QuickFix.SessionID>>(
                Expression.Convert(methodCall, typeof(void)),
                instance, argument1, argument2
                ).Compile();
        }

        static public bool IsHandlerMethod(MethodInfo m)
        {
            return (m.IsPublic == true
                && m.Name.Equals("OnMessage")
                && m.GetParameters().Length == 2
                //&& m.GetParameters()[0].ParameterType.IsSubclassOf(typeof(QuickFix.Message))
                && typeof(QuickFix.Message).IsAssignableFrom(m.GetParameters()[0].ParameterType)
                && typeof(QuickFix.SessionID).IsAssignableFrom(m.GetParameters()[1].ParameterType)
                && m.ReturnType == typeof(void));
        }


        /// <summary>
        /// Process ("crack") a FIX message and call the registered handlers for that type, if any
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sessionID"></param>
        public bool Crack(Message message, SessionID sessionID)
        {
            Type messageType = message.GetType();
            Action<TDerivedType, QuickFix.Message, QuickFix.SessionID> handler = null;

            if (_handlerMethods.TryGetValue(messageType, out handler))
            {
                handler((TDerivedType) this, message, sessionID);

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
