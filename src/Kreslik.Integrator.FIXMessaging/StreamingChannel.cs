﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using Kreslik.Integrator.LowLatencyUtils;
using QuickFix.Fields;
using Symbol = Kreslik.Integrator.Common.Symbol;

namespace Kreslik.Integrator.FIXMessaging
{
    

    public interface IRejectedExecutionsHandler
    {
        void HandleRejectedExecution(Common.Symbol symbol, decimal sizeBasePol);
    }

    public interface IStreamingPricesProducer
    {
        Common.Symbol Symbol { get; }
        DealDirection? WillProduceOrdersOnSide { get; }
        IntegratedTradingSystemIdentification IntegratedTradingSystemIdentification { get; }
        void StartStreaming();
        void StopStreaming();
        bool HandleNewUnconfirmedDeal(RejectableMMDeal deal, out RejectableMMDeal.DealRejectionReason rejectionReason);
        bool TryHandleCounterpartyRejectedExecutedDeal(Common.Symbol symbol, decimal sizeBasePol);
    }

    public interface IStreamingChannelProxy
    {
        bool UnregisterStreamingPricesProducer();
        bool RegisterStreamingPricesProducer();
        bool IsAllowedToStream { get; }
        bool OverrideStreamingPrice(StreamingPrice p, Symbol s, Counterparty counterparty, PriceSide side, decimal sizeBaseAbs, decimal minSizeBaseAbs,
            decimal price);
        void SuspendPricing();
        void CancelLastPriceIfRegistered();
        void ResumePricing();
    }

    internal class StreamingChannelProxy : IStreamingChannelProxy
    {
        private IStreamingPricesProducer _producer;
        private StreamingPricesProducerIndex _producerIndex = StreamingPricesProducerIndex.NullIndex;
        private IStreamingChannelInternal _channel;
        private ILogger _logger;

        internal StreamingChannelProxy(IStreamingPricesProducer producer, IStreamingChannelInternal channel, ILogger logger)
        {
            this._producer = producer;
            this._channel = channel;
            this._logger = logger;
        }

        public bool UnregisterStreamingPricesProducer()
        {
            _producerIndex = StreamingPricesProducerIndex.NullIndex;
            return this._channel.UnregisterStreamingPricesProducer(this._producer);
        }

        public bool RegisterStreamingPricesProducer()
        {
            this._producerIndex = this._channel.RegisterStreamingPricesProducer(this._producer);
            return this._producerIndex != StreamingPricesProducerIndex.NullIndex;
        }

        public bool IsAllowedToStream
        {
            get
            {
                //this is called after system is registered so we can check producer index
                return this._producerIndex != StreamingPricesProducerIndex.NullIndex &&
                       this._channel.IsAllowedToStream(this._producer);
            }
        }

        public bool OverrideStreamingPrice(StreamingPrice p, Symbol s, Counterparty counterparty, PriceSide side, decimal sizeBaseAbs, decimal minSizeBaseAbs, decimal price)
        {
            if (this._producerIndex == StreamingPricesProducerIndex.NullIndex)
            {
                this._logger.Log(LogLevel.Error, "Attempt to create price by unregistered producer");
                return false;
            }

            if (this._producer.WillProduceOrdersOnSide.HasValue && side.ToOutgoingDealDirection() == this._producer.WillProduceOrdersOnSide.Value)
            {
                this._logger.Log(LogLevel.Error, "Attempt to create price on direction other than registered");
                return false;
            }

            p.OverrideContent(s, counterparty, side, sizeBaseAbs, minSizeBaseAbs,
                price, this._producerIndex);

            return true;
        }

        public void SuspendPricing()
        {
            if (this._producerIndex != StreamingPricesProducerIndex.NullIndex)
                this._channel.SuspendPricing(this._producer, this._producerIndex);
        }

        public void CancelLastPriceIfRegistered()
        {
            if (this._producerIndex != StreamingPricesProducerIndex.NullIndex)
                this._channel.CancelLastPrice(this._producer, this._producerIndex);
        }

        public void ResumePricing()
        {
            this._channel.ResumePricing(this._producer, this._producerIndex);
        }
    }


    public interface IStreamingChannel : IStreamingSubscriptionInfoProvider, IUnderlyingSessionState
    {
        Counterparty Counterparty { get; }
        event Action<RejectableMMDeal> NewRejectableDealArrived;
        event Action<RejectableMMDeal> ExecutedRejectableDealRejectedByCounterparty;
        bool IsOrderHoldTimeWithinLimits(TimeSpan orderHoldTime);
        IStreamingChannelProxy GetStreamingChannelProxy(IStreamingPricesProducer producer);
        SubmissionResult SendPrice(StreamingPrice streamingPrice);
        bool AcceptDeal(RejectableMMDeal deal);
        bool AcceptDeal(RejectableMMDeal deal, decimal sizeBaseAbsToBeAccepted);
        void RejectDeal(RejectableMMDeal deal, RejectableMMDeal.DealRejectionReason rejectionReason);
    }

    public interface IStreamingChannelDCInfo
    {
        event Action<StreamingPrice> NewPriceSent;
        event Action<StreamingPriceCancel> NewPriceCancelSent;
    }

    internal interface IStreamingChannelInternal : IStreamingChannel, IStreamingChannelDCInfo
    {
        bool UnregisterStreamingPricesProducer(IStreamingPricesProducer producer);
        StreamingPricesProducerIndex RegisterStreamingPricesProducer(IStreamingPricesProducer producer);
        bool IsAllowedToStream(IStreamingPricesProducer producer);
        void SuspendPricing(IStreamingPricesProducer producer, StreamingPricesProducerIndex pricesProducerIndex);
        void CancelLastPrice(IStreamingPricesProducer producer, StreamingPricesProducerIndex pricesProducerIndex);
        void ResumePricing(IStreamingPricesProducer producer, StreamingPricesProducerIndex pricesProducerIndex);
    }

    internal class RejectionRateChecker
    {
        private int _minCasesToApplyRateCheck;
        private decimal _maxRejectionRate;
        private int _allCases;
        private int _rejectionCases;

        public RejectionRateChecker(bool allowCheck, decimal maxRejectionRatePercent, int minCasesToApplyCheck, int initialDealsNum, int initialRejectionsNum)
        {
            this._maxRejectionRate = allowCheck ? maxRejectionRatePercent / 100m : 1m;
            this._minCasesToApplyRateCheck = minCasesToApplyCheck;
            this._allCases = initialDealsNum + initialRejectionsNum;
            this._rejectionCases = initialRejectionsNum;
        }

        public void AddDealCase()
        {
            Interlocked.Increment(ref _allCases);
        }

        public void AddRejectionCase()
        {
            int allCases = Interlocked.Increment(ref _allCases);
            int rejectionCases = Interlocked.Increment(ref _rejectionCases);
            this.PerformCheck(allCases, rejectionCases);
        }

        public void AddRejectedCaseRemovingPreviousDeal()
        {
            int allCases = Interlocked.Decrement(ref _allCases);
            int rejectionCases = Interlocked.Increment(ref _rejectionCases);
            this.PerformCheck(allCases, rejectionCases);
        }

        private void PerformCheck(int allCases, int rejectionCases)
        {
            if (!this.CheckAllowed(allCases, rejectionCases) &&
                RejectionRateExceeded != null)
            {
                this.RejectionRateExceeded((decimal)rejectionCases / (decimal)allCases);
            }
        }

        private bool CheckAllowed(int allCases, int rejectionCases)
        {
            return allCases < this._minCasesToApplyRateCheck ||
                   (decimal) rejectionCases/(decimal) allCases < this._maxRejectionRate;
        }

        public bool CheckAllowed()
        {
            return this.CheckAllowed(this._allCases, this._rejectionCases);
        }

        public event Action<decimal> RejectionRateExceeded;
    }

    public class StreamingChannel : IStreamingChannelInternal, IRejectedExecutionsHandler, ICounterpartyRejectionSource
    {
        private IFIXStreamingChannelPricing _quoChannel;
        private IFIXStreamingChannelOrders _ordChannel;
        private IRejectedExecutionsHandler _rejectedExecutionsHandler;
        private IDealStatisticsConsumer _dealStatisticsConsumer;
        private ILogger _logger;
        private IRiskManager _riskManager;
        private EventsRateChecker _pricingTotalMessagesRateChecker;
        private EventsRateChecker[] _perSymbolPricingMessagesRateCheckers;
        //private TimeSpan _maxOrderHoldTime;
        private TimeSpan _expectedSendingTime;
        private TimeSpan _maxLLTimesOnDestination;
        private readonly int _expectedPricingDepth;
        //Symbol, Side, Layer
        private IStreamingPricesProducer[][][] _registeredProducers;
        private bool[][][] _allowedPricingMap;
        private RejectionRateChecker _rejectionRateChecker;
        private IStreamingUnicastInfoForwarder _streamingUnicastInfoForwarder;
        private ISettlementDatesKeeper _settlementDatesKeeper;
        private readonly IDelayEvaluator _dealDelayEvaluator;
        private readonly StreamingIdsGeneratorHelper _generatedIdsHelper;

        public Counterparty Counterparty { get { return this._quoChannel.Counterparty; } }

        public IFIXChannel MdChannel { get { return object.ReferenceEquals(_quoChannel, _ordChannel) ? null : _quoChannel; } }

        public IFIXChannel OfChannel
        {
            get { return _ordChannel; }
        }

        public event Action<Counterparty, Symbol, bool> RemoteSubscriptionChanged;

        public event Action<StreamingPrice> NewPriceSent
        {
            add { this._quoChannel.NewPriceSent += value; }
            remove { this._quoChannel.NewPriceSent -= value; }
        }
        public event Action<StreamingPriceCancel> NewPriceCancelSent
        {
            add { this._quoChannel.NewPriceCancelSent += value; }
            remove { this._quoChannel.NewPriceCancelSent -= value; }
        }

        public IStreamingChannelProxy GetStreamingChannelProxy(IStreamingPricesProducer producer)
        {
            return new StreamingChannelProxy(producer, this, this._logger);
        }

        public StreamingChannel(IFIXStreamingChannelPricing quoChannel, IFIXStreamingChannelOrders ordChannel,
            IRejectedExecutionsHandler rejectedExecutionsHandler, IDealStatisticsConsumer dealStatisticsConsumer,
            IStreamingChannelSettings settings, IRiskManager riskManager, IStreamingUnicastInfoForwarder streamingUnicastInfoForwarder,
            IPersistedStreamingStatsProvider streamingStatsProvider, ISettlementDatesKeeper settlementDatesKeeper, 
            IStreamingSettings streamingSettings, IDelayEvaluator delayEvaluator,  ILogger logger)
        {
            if(quoChannel.Counterparty != ordChannel.Counterparty)
                logger.Log(LogLevel.Fatal, "Counterparties mismatch during StreamingChannel creation ({0} vs {1})",
                    quoChannel.Counterparty, ordChannel.Counterparty);

            this._quoChannel = quoChannel;
            this._ordChannel = ordChannel;
            this._rejectedExecutionsHandler = rejectedExecutionsHandler ?? this;
            this._dealStatisticsConsumer = dealStatisticsConsumer;
            this._logger = logger;
            this._riskManager = riskManager;
            this._streamingUnicastInfoForwarder = streamingUnicastInfoForwarder;
            this._dealDelayEvaluator = delayEvaluator;
            _pricingTotalMessagesRateChecker = new EventsRateChecker(settings.MaxTotalPricesIntervalToCheck,
                    settings.MaxTotalPricesPerTimeInterval);
            if (settings.MaxPerSymbolPricesPerSecondSetting != null)
            {
                _perSymbolPricingMessagesRateCheckers =
                    Symbol.Values.Select(
                        s =>
                            new EventsRateChecker(settings.MaxPerSymbolPricesPerSecondSetting.DefaultMaxAllowedIntervalToCheck,
                                settings.MaxPerSymbolPricesPerSecondSetting.DefaultMaxAllowedPerTimeInterval)).ToArray();

                if (settings.MaxPerSymbolPricesPerSecondSetting.SpecialSymbolRateSettings != null)
                {
                    foreach (
                        ISymbolRateSetting specialSymbolRateSetting in
                            settings.MaxPerSymbolPricesPerSecondSetting.SpecialSymbolRateSettings)
                    {
                        _perSymbolPricingMessagesRateCheckers[(int) specialSymbolRateSetting.Symbol] =
                            new EventsRateChecker(specialSymbolRateSetting.MaxAllowedIntervalToCheck,
                                specialSymbolRateSetting.MaxAllowedPerTimeInterval);
                    }
                }
            }
            //_maxOrderHoldTime = streamingSettings.GetMaxLLTimeInIntegrator(ordChannel.Counterparty);
            //if(_maxOrderHoldTime <= TimeSpan.Zero)
            //    logger.Log(LogLevel.Fatal, "MaxOrderHoldTime (Integrator view) set to {0}", _maxOrderHoldTime);
            _expectedSendingTime = streamingSettings.GetExpectedSendingTime(ordChannel.Counterparty);
            if (_expectedSendingTime < TimeSpan.Zero)
            {
                logger.Log(LogLevel.Fatal, "ExpectedSendingTime configured to {0}. Setting to zero", _expectedSendingTime);
                _expectedSendingTime = TimeSpan.Zero;
            }
            _maxLLTimesOnDestination = streamingSettings.GetMaxLLTimeOnDestiantion(ordChannel.Counterparty);
            if (_maxLLTimesOnDestination <= TimeSpan.Zero)
                logger.Log(LogLevel.Fatal, "MaxOrderHoldTime (Counterparty view) set to {0}", _maxLLTimesOnDestination);
            _onDealExpired = new Action<RejectableMMDeal, RejectableMMDeal.DealStatus>(this.OnDealExpired);
            this._expectedPricingDepth = settings.ExpectedPricingDepth;
            this._settlementDatesKeeper = settlementDatesKeeper;
            settlementDatesKeeper.AllInfoAvailabilityRemoved += SettlementDatesKeeperOnAllInfoAvailabilityRemoved;
            settlementDatesKeeper.InfoAvailabilityChanged += SettlementDatesKeeperOnInfoAvailabilityChanged;

            int initialDealsNum;
            int initialrejectionsNum;
            streamingStatsProvider.GetStreamingSessionStats(ordChannel.Counterparty, out initialDealsNum, out initialrejectionsNum);
            this._rejectionRateChecker =
                new RejectionRateChecker(settings.RejectionRateDisconnectSettings.RiskCheckAllowed,
                    settings.RejectionRateDisconnectSettings.MaxAllowedRejectionRatePercent,
                    settings.RejectionRateDisconnectSettings.MinimumOrdersToApplyCheck,
                    initialDealsNum, initialrejectionsNum);
            this._rejectionRateChecker.RejectionRateExceeded += RejectionRateCheckerOnRejectionRateExceeded;

            this._registeredProducers =
            Enumerable.Repeat(0, Common.Symbol.ValuesCount)
                .Select(i => new IStreamingPricesProducer[2][]{new IStreamingPricesProducer[settings.ExpectedPricingDepth], new IStreamingPricesProducer[settings.ExpectedPricingDepth]})
                .ToArray();
            this._allowedPricingMap = new bool[2][][];
            this._allowedPricingMap[(int) DealDirection.Buy] =
                Enumerable.Repeat(0, settings.ExpectedPricingDepth).Select(i => new bool[Symbol.ValuesCount]).ToArray();
            this._allowedPricingMap[(int)DealDirection.Sell] =
                Enumerable.Repeat(0, settings.ExpectedPricingDepth).Select(i => new bool[Symbol.ValuesCount]).ToArray();

            quoChannel.OnStarted += this.OnOneSessionStarted;
            ordChannel.OnStarted += this.OnOneSessionStarted;
            quoChannel.ExpectedPricingDepth = settings.ExpectedPricingDepth;
            ordChannel.CounterpartyRejectionSource = this;
            quoChannel.SubscriptionChanged += QuoChannelOnSubscriptionChanged;
            ordChannel.NewRejectableDealArrived += OrdChannelOnNewRejectableDealArrived;
            ordChannel.NewCounterpartyRejectedDeal += OrdChannelOnNewCounterpartyRejectedDeal;
            ordChannel.DealRejected += OrdChannelOnDealRejected;
            ordChannel.NewCounterpartyTimeoutInfo += OrdChannelOnNewCounterpartyTimeoutInfo;
            riskManager.GoFlatOnlyStateChanged += RiskManagerOnGoFlatOnlyStateChanged;
            riskManager.OrderTransmissionChanged += RiskManagerOnOrderTransmissionChanged;

            _generatedIdsHelper = new StreamingIdsGeneratorHelper(this._quoChannel.Counterparty);

            if (!this._rejectionRateChecker.CheckAllowed())
            {
                this._logger.Log(LogLevel.Fatal, "{0} Allowed rejection rate was exceeded previously today - safe exiting from session", ordChannel.Counterparty);
                this.SafeExitFromSessions();
            }
        }

        private void SettlementDatesKeeperOnInfoAvailabilityChanged(Symbol symbol, bool available)
        {
            //symbol was not allowed and this change cannot influence it
            if (!this._quoChannel.IsSubscribed(symbol))
                return;

            this.SymbolAvailabilityChanged(symbol, available);
        }

        private void SettlementDatesKeeperOnAllInfoAvailabilityRemoved()
        {
            foreach (Symbol symbol in Symbol.Values)
            {
                this.SettlementDatesKeeperOnInfoAvailabilityChanged(symbol, false);
            }
        }

        private void QuoChannelOnSubscriptionChanged(Symbol symbol, bool subscribed)
        {
            //symbol was not allowed and this change cannot influence it
            if (!this._settlementDatesKeeper.HasSettlementDateInfo(symbol))
                return;

            this.SymbolAvailabilityChanged(symbol, subscribed);
        }

        private void SymbolAvailabilityChanged(Symbol symbol, bool available)
        {
            IStreamingPricesProducer[][] registeredProducersForSymbol = _registeredProducers[(int)symbol];

            lock (registeredProducersForSymbol)
            {
                for (int producerIdx = 0; producerIdx < registeredProducersForSymbol[0].Length; producerIdx++)
                {
                    if (registeredProducersForSymbol[0][producerIdx] != null)
                    {
                        if (available)
                            registeredProducersForSymbol[0][producerIdx].StartStreaming();
                        else
                            registeredProducersForSymbol[0][producerIdx].StopStreaming();
                    }

                    if (registeredProducersForSymbol[1][producerIdx] != null)
                    {
                        if (available)
                            registeredProducersForSymbol[1][producerIdx].StartStreaming();
                        else
                            registeredProducersForSymbol[1][producerIdx].StopStreaming();
                    }
                }
            }

            if (this.RemoteSubscriptionChanged != null)
                this.RemoteSubscriptionChanged(this.OfChannel.Counterparty, symbol, available);
        }

        public event Action<CounterpartyTimeoutInfo> CounterpartyDeliveredRejectionInfo;

        private void OrdChannelOnNewCounterpartyTimeoutInfo(CounterpartyTimeoutInfo counterpartyTimeoutInfo)
        {
            if (this.CounterpartyDeliveredRejectionInfo != null)
            {
                this.CounterpartyDeliveredRejectionInfo(counterpartyTimeoutInfo);
            }

            this._dealStatisticsConsumer.NewCounterpartyTimeoutInfo(counterpartyTimeoutInfo);

            this.SafeExitFromSessions();
        }

        public bool IsOrderHoldTimeWithinLimits(TimeSpan orderHoldTime)
        {
            return orderHoldTime < this._maxLLTimesOnDestination - this._expectedSendingTime.Times(2) - Constansts.MinimumSpanBetweenLLandWatchdog;
        }

        private void OrdChannelOnDealRejected(RejectableMMDeal rejectableMmDeal)
        {
            this._rejectionRateChecker.AddRejectionCase();

            if (rejectableMmDeal != null)
            {
                this._dealStatisticsConsumer.NewRejectableRejectedDeal(rejectableMmDeal);
                if (this._streamingUnicastInfoForwarder != null)
                    this._streamingUnicastInfoForwarder.OnRejectableStreamingDeal(rejectableMmDeal);
            }
        }

        private void OnDealAccepted(RejectableMMDeal rejectableMmDeal)
        {
            this._rejectionRateChecker.AddDealCase();

            if (rejectableMmDeal != null)
                this._dealStatisticsConsumer.NewRejectableExecutedDeal(rejectableMmDeal);

            if (this._streamingUnicastInfoForwarder != null)
                this._streamingUnicastInfoForwarder.OnRejectableStreamingDeal(rejectableMmDeal);
        }

        private void RejectionRateCheckerOnRejectionRateExceeded(decimal rejectionRate)
        {
            this._logger.Log(LogLevel.Fatal,
                "Rejection rate {0} exceeded allowed setting -> stopping the streaming sessions", rejectionRate);
            this.SafeExitFromSessions();
        }

        private void SafeExitFromSessions()
        {
            //prevent sending of other prices
            this._quoChannel.IsSessionOpen = false;
            this._ordChannel.IsSessionOpen = false;

            //perform delayed stop - allow other possible timeouts to come before we unregister systems
            // (asyncStop would disconnect system immedaitely even before dealy)
            TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromMilliseconds(200), this.Stop);

            //imediately indicate that we do not want to trade
            _ordChannel.SendTradingStatus(false);
        }

        private void RiskManagerOnOrderTransmissionChanged(OrderTransmissionStatusChangedEventArgs orderTransmissionStatusChangedEventArgs)
        {
            this.FlipAllPricing(!_riskManager.IsTransmissionDisabled && !_riskManager.IsGoFlatOnlyEnabled);
        }

        //TODO: do we want to stop pricing immediately after go flat only???
        private void RiskManagerOnGoFlatOnlyStateChanged(bool goFlatOnlyEnabled)
        {
            this.FlipAllPricing(!_riskManager.IsTransmissionDisabled && !_riskManager.IsGoFlatOnlyEnabled);
        }


        private void FlipAllPricing(bool pricingAllowed)
        {
            //do not flip _allowedPricingMap here - as afterwards it wouldn't be clear what wasn't allowed before flip

            _quoChannel.SendTradingStatus(pricingAllowed);
            if(!object.ReferenceEquals(_quoChannel, _ordChannel))
                _ordChannel.SendTradingStatus(pricingAllowed);
        }

        public void Start()
        {
            TaskEx.StartNew(_quoChannel.Start);
            if (!object.ReferenceEquals(_quoChannel, _ordChannel))
                TaskEx.StartNew(_ordChannel.Start);
        }

        public void Stop()
        {
            TaskEx.StartNew(_quoChannel.Stop, TimerTaskFlagUtils.WorkItemPriority.Highest);
            if (!object.ReferenceEquals(_quoChannel, _ordChannel))
                TaskEx.StartNew(_ordChannel.Stop, TimerTaskFlagUtils.WorkItemPriority.Highest);
        }

        public void HandleRejectedExecution(Symbol symbol, decimal sizeBasePol)
        {
            string reason =
                string.Format(
                    "Integrator executed (polarized) {0} of {1}, but counterparty rejected it (too late). Integrator couldn't notify internal systems - so position might have been already hedged and might need to be manually hedged again",
                    sizeBasePol, symbol);
            //this._logger.Log(LogLevel.Fatal, reason);
            this._riskManager.TurnOnGoFlatOnly(reason);
        }

        private void OrdChannelOnNewCounterpartyRejectedDeal(Symbol symbol, string integratorExecutionId, string counterpartyExecutionId, string integratorGeneratedPriceIdentity)
        {
            TaskEx.StartNew(()
                =>
            {
                try
                {
                    int layerId = ByteUtils.ConvertFromBaseByte((byte)integratorGeneratedPriceIdentity[1]);
                    IStreamingPricesProducer producer = null;

                    if (layerId < 0 || layerId >= _expectedPricingDepth)
                    {
                        this._logger.Log(LogLevel.Fatal,
                            "Received rejected execution with unexpected layer: {0} - probably due to malformed id: [{1}]",
                            layerId, integratorGeneratedPriceIdentity);
                    }
                    else
                    {

                        DealDirection? integratorDealDirection = StreamingIdsGeneratorHelper.TryExtractIntegratorDirectionFromId(integratorGeneratedPriceIdentity);

                        if (!integratorDealDirection.HasValue)
                        {
                            IStreamingPricesProducer buyProducer =
                                _registeredProducers[(int) symbol][(int) DealDirection.Buy][layerId];
                            IStreamingPricesProducer sellProducer =
                                _registeredProducers[(int) symbol][(int) DealDirection.Sell][layerId];

                            if (buyProducer != null)
                            {
                                integratorDealDirection = DealDirection.Buy;
                            }
                            else if (sellProducer != null)
                            {
                                integratorDealDirection = DealDirection.Sell;
                            }
                            else
                            {
                                this._logger.Log(LogLevel.Fatal,
                                    "Received info about rejected execution with probably malformed integrator identity [{0}] so side cannot be inferred.",
                                    integratorGeneratedPriceIdentity);
                                integratorDealDirection = null;
                            }
                        }

                        if(integratorDealDirection.HasValue)
                            producer = _registeredProducers[(int) symbol][(int) integratorDealDirection][(int) layerId];
                    }

                    RejectableMMDeal rejectableDeal = _dealStatisticsConsumer.RejectExecutedRejectableDeal(integratorExecutionId, counterpartyExecutionId,
                        RejectableMMDeal.DealStatus.CounterpartyRejected.ToString());
                    decimal sizeRejectedBasePol = rejectableDeal == null ? 0m : rejectableDeal.IntegratorRejectedAmountBasePol;

                    IntegratorDealDone dealDone = new IntegratorDealDone(this.Counterparty, rejectableDeal.Symbol,
                        rejectableDeal.SettlementDateLocal,
                        rejectableDeal.CounterpartySentTimeUtc.Date,
                        rejectableDeal.SettlementDateTimeCounterpartyUtc,
                        rejectableDeal.IntegratorRejectedAmountBasePol,
                        rejectableDeal.RejectedPrice * (-rejectableDeal.IntegratorRejectedAmountBasePol));
                    this._riskManager.HandleDealLateRejected(rejectableDeal, dealDone);

                    if (producer == null || !producer.TryHandleCounterpartyRejectedExecutedDeal(symbol, sizeRejectedBasePol))
                    {
                        this._rejectedExecutionsHandler.HandleRejectedExecution(symbol, sizeRejectedBasePol);
                    }

                    if (this._streamingUnicastInfoForwarder != null)
                        this._streamingUnicastInfoForwarder.OnRejectableStreamingDeal(rejectableDeal);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Unexpected error during receiving info about rejected execution [{0}] on {1}. MANUAL INVESTIGATION REQUIRED",
                        integratorExecutionId ?? counterpartyExecutionId, symbol);
                }
            }, TimerTaskFlagUtils.WorkItemPriority.Highest);

            this.SafeExitFromSessions();

            this._rejectionRateChecker.AddRejectedCaseRemovingPreviousDeal();
        }

        private void OrdChannelOnNewRejectableDealArrived(RejectableMMDeal rejectableMmDeal)
        {
            if (_dealDelayEvaluator.IsReceivedObjectDelayed(rejectableMmDeal))
                this._logger.Log(LogLevel.Error,
                    "Receiving delayed FastMatch deal (either our GC or FastMatch sending issue): {0}",
                    rejectableMmDeal);

            IStreamingPricesProducer producer =
                _registeredProducers[(int)rejectableMmDeal.Symbol][(int)rejectableMmDeal.IntegratorDealDirection][(int)rejectableMmDeal.LayerId];

            if (producer == null)
            {
                this.RejectDealInternal(rejectableMmDeal, RejectableMMDeal.DealRejectionReason.InternalFailure, false);
                this._logger.Log(LogLevel.Fatal, "Autorejecting deal for unregistered system");
                return;
            }
            //populate  tradingsystemid of the system
            else
            {
                rejectableMmDeal.IntegratedTradingSystemIdentification = producer.IntegratedTradingSystemIdentification;
            }

            if (!this._riskManager.CheckIfIncomingClientDealAllowed(rejectableMmDeal))
            {
                this.RejectDealInternal(rejectableMmDeal, RejectableMMDeal.DealRejectionReason.RiskMgmtDisallow, false);
                return;
            }

            DateTime cutoffTime =
                DateTimeEx.Min(rejectableMmDeal.CounterpartySentDealRequestTimeUtc,
                    rejectableMmDeal.IntegratorReceivedTimeUtc - _expectedSendingTime)
                + this._maxLLTimesOnDestination - this._expectedSendingTime;

            //not doing this earlier so that IntegratedTradingSystemIdentification is populated if possible
            if (!rejectableMmDeal.SetTimeout(cutoffTime, _onDealExpired))
            {
                this.RejectDealInternal(rejectableMmDeal, RejectableMMDeal.DealRejectionReason.KgtTooLate, false);
                return;
            }

            RejectableMMDeal.DealRejectionReason rejectionReason;
            if (!producer.HandleNewUnconfirmedDeal(rejectableMmDeal, out rejectionReason))
            {
                this.RejectDealInternal(rejectableMmDeal, rejectionReason, false);
            }
        }

        private Action<RejectableMMDeal, RejectableMMDeal.DealStatus> _onDealExpired;
        private void OnDealExpired(RejectableMMDeal deal, RejectableMMDeal.DealStatus rejectStatus)
        {
            this._logger.Log(LogLevel.Error, "Rejecting deal as its timeout expired [{0}]", rejectStatus);
            //ordinary rejection would not go through as the order is already in the closed state (by timer)!!
            this.RejectDealInternal(deal, (RejectableMMDeal.DealRejectionReason) rejectStatus, true);
        }

        public event Action<RejectableMMDeal> NewRejectableDealArrived;

        public event Action<RejectableMMDeal> ExecutedRejectableDealRejectedByCounterparty;

        StreamingPricesProducerIndex IStreamingChannelInternal.RegisterStreamingPricesProducer(IStreamingPricesProducer producer)
        {
            //There are N layers for each side
            // 2 uni-sided systems can have same layerIf if on different sides
            // two-sided system can only have 1 layer id - so it'll not be able to register it if layers are taken this way
            //
            // LayerId   Bid   Ask 
            //   1        X     -
            //   2        -     X

            //Plus some counterparties (e.g. FxAll) support only one side per layer!!

            IStreamingPricesProducer[][] registeredProducersForSymbol = _registeredProducers[(int)producer.Symbol];

            StreamingPricesProducerIndex producerIndex = StreamingPricesProducerIndex.NullIndex;
            bool registered = false;

            int producerIdx = -1;
            lock (registeredProducersForSymbol)
            {
                if (!producer.WillProduceOrdersOnSide.HasValue)
                {
                    for (int idx = 0; idx < registeredProducersForSymbol[0].Length; idx++)
                    {
                        if (registeredProducersForSymbol[(int)DealDirection.Buy][idx] == null &&
                            registeredProducersForSymbol[(int)DealDirection.Sell][idx] == null)
                        {
                            producerIdx = idx;
                            break;
                        }
                    }
                }
                else if (producer.WillProduceOrdersOnSide.Value == DealDirection.Buy)
                {
                    producerIdx = Array.IndexOf(registeredProducersForSymbol[(int)DealDirection.Buy], null);
                }
                else
                {
                    producerIdx = Array.IndexOf(registeredProducersForSymbol[(int)DealDirection.Sell], null);
                }

                if (producerIdx != -1)
                {
                    registered = true;

                    if (!producer.WillProduceOrdersOnSide.HasValue)
                    {
                        _allowedPricingMap[(int)DealDirection.Buy][producerIdx][(int)producer.Symbol] = true;
                        _allowedPricingMap[(int)DealDirection.Sell][producerIdx][(int)producer.Symbol] = true;
                        registeredProducersForSymbol[(int)DealDirection.Buy][producerIdx] = producer;
                        registeredProducersForSymbol[(int)DealDirection.Sell][producerIdx] = producer;
                    }
                    else if (producer.WillProduceOrdersOnSide.Value == DealDirection.Buy)
                    {
                        _allowedPricingMap[(int) DealDirection.Buy][producerIdx][(int) producer.Symbol] = true;
                        registeredProducersForSymbol[(int) DealDirection.Buy][producerIdx] = producer;
                    }
                    else
                    {
                        _allowedPricingMap[(int) DealDirection.Sell][producerIdx][(int) producer.Symbol] = true;
                        registeredProducersForSymbol[(int) DealDirection.Sell][producerIdx] = producer;
                    }
                }
            }

            if (!registered)
            {
                this._logger.Log(LogLevel.Error, "Couldn't register StreamingPricesProducer as there wasn't empty slot for it - all layers are already streamed");
            }
            else
            {
                producerIndex = new StreamingPricesProducerIndex(producerIdx,
                    producer.IntegratedTradingSystemIdentification.IntegratedTradingSystemIdentity);
            }

            return producerIndex;
        }

        bool IStreamingChannelInternal.IsAllowedToStream(IStreamingPricesProducer producer)
        {
            //no checks if registered - as this is called before registration
            return this._quoChannel.IsSubscribed(producer.Symbol) &&
                   this._settlementDatesKeeper.HasSettlementDateInfo(producer.Symbol);
        }

        public bool UnregisterStreamingPricesProducer(IStreamingPricesProducer producer)
        {
            IStreamingPricesProducer[][] registeredProducersForSymbol = _registeredProducers[(int)producer.Symbol];
            int layerId = -1;

            lock (registeredProducersForSymbol)
            {
                int buyProducerIdx = Array.IndexOf(registeredProducersForSymbol[(int)DealDirection.Buy], producer);
                int sellProducerIdx = Array.IndexOf(registeredProducersForSymbol[(int)DealDirection.Sell], producer);

                if (buyProducerIdx != -1)
                {
                    _allowedPricingMap[(int)DealDirection.Buy][buyProducerIdx][(int)producer.Symbol] = false;
                    registeredProducersForSymbol[(int)DealDirection.Buy][buyProducerIdx] = null;
                }

                if (sellProducerIdx != -1)
                {
                    _allowedPricingMap[(int)DealDirection.Sell][sellProducerIdx][(int)producer.Symbol] = false;
                    registeredProducersForSymbol[(int)DealDirection.Sell][sellProducerIdx] = null;
                }

                layerId = Math.Max(buyProducerIdx, sellProducerIdx);
            }

            if (layerId != -1)
            {
                this._quoChannel.CancelPriceStreams(new StreamingPriceCancel(producer.Symbol, this.Counterparty,
                    producer.WillProduceOrdersOnSide.ToIntegratorOutgoingPriceSide(),
                    new StreamingPricesProducerIndex(layerId,
                        producer.IntegratedTradingSystemIdentification.IntegratedTradingSystemIdentity)));
            }
            return layerId != -1;
        }

        public SubmissionResult SendPrice(StreamingPrice streamingPrice)
        {
            if (streamingPrice.LayerId < 0 || streamingPrice.LayerId >= _expectedPricingDepth)
            {
                this._logger.Log(LogLevel.Error, "Attempt to send price while streaming layer is invalid: {0}", streamingPrice.LayerId);
                return SubmissionResult.Failed_ProdeucerNotRegistered;
            }

            var riskResult = this._riskManager.CheckIfSoftPriceAllowed(streamingPrice);
            if (riskResult != RiskManagementCheckResult.Accepted)
            {
                this._logger.Log(LogLevel.Error, "Cannot send next price - risk management disallowed ({0})", riskResult);
                this._quoChannel.CancelPriceStreams(new StreamingPriceCancel(streamingPrice));
                return riskResult.CanBeRetryed() ? SubmissionResult.Failed_RiskManagementDisallowedCanBeRetryed : SubmissionResult.Failed_RiskManagementDisallowed;
            }

            if (!_allowedPricingMap[(int) streamingPrice.IntegratorDealDirection][streamingPrice.LayerId][(int)streamingPrice.Symbol])
            {
                this._logger.Log(LogLevel.Error,
                    "Attempt to send streaming price on disallowed symbol and layer: {0}-{1}", streamingPrice.Symbol,
                    streamingPrice.LayerId);
                return SubmissionResult.Failed_PricingForSenderDisallowed;
            }

            if (_quoChannel.State != SessionState.Running || _ordChannel.State != SessionState.Running
                || !_quoChannel.IsSessionOpen || !_ordChannel.IsSessionOpen)
            {
                this._logger.Log(LogLevel.Error, "Attempt to send price while underlying sessions are not running or open");
                return SubmissionResult.Failed_SessionNotRunning;
            }

            if (!_settlementDatesKeeper.HasSettlementDateInfo(streamingPrice.Symbol))
            {
                this._logger.Log(LogLevel.Error, "Attempt to send price on {0}, but settlement date info is missing", streamingPrice.Symbol);
                return SubmissionResult.Failed_PricingForSenderDisallowed;
            }

            if (_perSymbolPricingMessagesRateCheckers != null && !_perSymbolPricingMessagesRateCheckers[(int)streamingPrice.Symbol].AddNextEventAndCheckIsAllowed())
            {
                this._logger.Log(LogLevel.Error, "Cannot send next price - symbol throttled");
                this._quoChannel.CancelPriceStreams(new StreamingPriceCancel(streamingPrice));
                return SubmissionResult.Failed_TooHighFrequency;
            }

            if (!_pricingTotalMessagesRateChecker.AddNextEventAndCheckIsAllowed())
            {
                this._logger.Log(LogLevel.Error, "Cannot send next price - throttled");
                this._quoChannel.CancelPriceStreams(new StreamingPriceCancel(streamingPrice));
                return SubmissionResult.Failed_TooHighFrequency;
            }

            streamingPrice.IntegratorPriceIdentity = this._generatedIdsHelper.GetQuoteEntryId(streamingPrice.Symbol, streamingPrice.Side, streamingPrice.LayerId);

            try
            {
                return this._quoChannel.SendOrder(streamingPrice);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, "Cannot send soft streaming price due to internal failure", e);
                return SubmissionResult.Failed_MessagingLayerError;
            } 
        }

        private bool VerifyProducerIndex(IStreamingPricesProducer producer, StreamingPricesProducerIndex pricesProducerIndex)
        {
            bool invalid;

            invalid = pricesProducerIndex.LayerId < 0 || pricesProducerIndex.LayerId >= _expectedPricingDepth;

            if (!invalid)
            {
                if (!producer.WillProduceOrdersOnSide.HasValue ||
                    producer.WillProduceOrdersOnSide.Value == DealDirection.Buy)
                {
                    if (
                        _registeredProducers[(int) producer.Symbol][(int) DealDirection.Buy][pricesProducerIndex.LayerId
                            ] !=
                        producer)
                        invalid = true;
                }

                if (!producer.WillProduceOrdersOnSide.HasValue ||
                    producer.WillProduceOrdersOnSide.Value == DealDirection.Sell)
                {
                    if (
                        _registeredProducers[(int) producer.Symbol][(int) DealDirection.Sell][
                            pricesProducerIndex.LayerId] !=
                        producer)
                        invalid = true;
                }
            }

            if (invalid)
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to perform action on {0} on producer index [{1}] that belongs to different producer",
                    producer.Symbol, pricesProducerIndex.LayerId);
            }

            return !invalid;
        }

        void IStreamingChannelInternal.SuspendPricing(IStreamingPricesProducer producer,
            StreamingPricesProducerIndex pricesProducerIndex)
        {
            if (!VerifyProducerIndex(producer, pricesProducerIndex))
                return;

            if (producer.WillProduceOrdersOnSide == null || producer.WillProduceOrdersOnSide.Value == DealDirection.Buy)
                _allowedPricingMap[(int)DealDirection.Buy][pricesProducerIndex.LayerId][(int)producer.Symbol] = false;
            if (producer.WillProduceOrdersOnSide == null || producer.WillProduceOrdersOnSide.Value == DealDirection.Sell)
                _allowedPricingMap[(int)DealDirection.Sell][pricesProducerIndex.LayerId][(int)producer.Symbol] = false;

            this._quoChannel.CancelPriceStreams(new StreamingPriceCancel(producer.Symbol, this.Counterparty, producer.WillProduceOrdersOnSide.ToIntegratorOutgoingPriceSide(), pricesProducerIndex));
        }

        void IStreamingChannelInternal.CancelLastPrice(IStreamingPricesProducer producer, StreamingPricesProducerIndex pricesProducerIndex)
        {
            if (!VerifyProducerIndex(producer, pricesProducerIndex) || !this._quoChannel.IsReadyAndOpen)
                return;

            this._quoChannel.CancelPriceStreams(new StreamingPriceCancel(producer.Symbol, this.Counterparty,
                producer.WillProduceOrdersOnSide.ToIntegratorOutgoingPriceSide(), pricesProducerIndex));
        }

        void IStreamingChannelInternal.ResumePricing(IStreamingPricesProducer producer,
            StreamingPricesProducerIndex pricesProducerIndex)
        {
            if (!VerifyProducerIndex(producer, pricesProducerIndex))
                return;

            if (producer.WillProduceOrdersOnSide == null || producer.WillProduceOrdersOnSide.Value == DealDirection.Buy)
                _allowedPricingMap[(int)DealDirection.Buy][pricesProducerIndex.LayerId][(int)producer.Symbol] = true;
            if (producer.WillProduceOrdersOnSide == null || producer.WillProduceOrdersOnSide.Value == DealDirection.Sell)
                _allowedPricingMap[(int)DealDirection.Sell][pricesProducerIndex.LayerId][(int)producer.Symbol] = true;
            Thread.MemoryBarrier();
        }

        public bool AcceptDeal(RejectableMMDeal deal, decimal sizeBaseAbsToBeAccepted)
        {
            if (deal.SizeToBeFilledBaseAbs != 0 && deal.SizeToBeFilledBaseAbs != sizeBaseAbsToBeAccepted)
            {
                this._logger.Log(LogLevel.Fatal, "Streaming channel receiving request to accept base size {0} from deal {1}, but it is already marked to accept {2} size. Using only the originaly requested fill amount, ignoring the new.",
                    sizeBaseAbsToBeAccepted, deal.Identity, deal.SizeToBeFilledBaseAbs);
            }
            else
                deal.SizeToBeFilledBaseAbs = sizeBaseAbsToBeAccepted;

            if (deal.SizeToBeFilledBaseAbs <= 0 || deal.SizeToBeFilledBaseAbs > deal.CounterpartyRequestedSizeBaseAbs ||
                deal.SizeToBeFilledBaseAbs < deal.CounterpartyRequestedSizeBaseAbsFillMinimum)
            {
                this._logger.Log(LogLevel.Fatal, "Attempt to accept deal [{0}] with invalid size to accept: {1}. AutoRejecting the deal",
                    deal, deal.SizeToBeFilledBaseAbs);
                this.RejectDeal(deal, RejectableMMDeal.DealRejectionReason.InternalFailure);
                return false;
            }

            return AcceptDeal_Internal(deal);
        }

        public bool AcceptDeal(RejectableMMDeal deal)
        {
            return this.AcceptDeal(deal, deal.CounterpartyRequestedSizeBaseAbs);
        }

        public bool AcceptDeal_Internal(RejectableMMDeal deal)
        {
            //Session state check:
            //NOT NEEDED - as deals are comming from the same session - and we need to attempt to response at all means

            DateTime? settlementDate = this._settlementDatesKeeper.GetSettlementDate(deal.Symbol);
            if (!settlementDate.HasValue)
            {
                this.RejectDealInternal(deal, RejectableMMDeal.DealRejectionReason.SettlDateUnavailable, false);
                return false;
            }
            deal.SetSettlementDateIfNotSet(settlementDate.Value);

            if (!_riskManager.CheckIfIncomingClientDealCanBeAccepted(deal))
            {
                this.RejectDealInternal(deal, RejectableMMDeal.DealRejectionReason.RiskMgmtDisallow, false);
                return false;
            }

            bool accepted = false;
            try
            {
                accepted = this.AcceptDealInternal(deal);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, "Unexpected failure during accepting deal", e);
            }

            if (accepted)
            {
                IntegratorDealDone dealDone = new IntegratorDealDone(this.Counterparty, deal.Symbol,
                        deal.SettlementDateLocal,
                        deal.TransactionTime.Date,
                        deal.SettlementDateTimeCounterpartyUtc,
                        deal.IntegratorFilledAmountBasePol,
                        deal.UsedPrice * (-deal.IntegratorFilledAmountBasePol));
                this._riskManager.HandleDealDone(dealDone);
            }

            return accepted;
        }

        private bool AcceptDealInternal(RejectableMMDeal deal)
        {
            deal.IntegratorExecutionId = _generatedIdsHelper.GetExecId(deal.Symbol, deal.IntegratorDealDirection, deal.LayerId, false);
            var message = this._ordChannel.CreateAcceptMessage(deal);

            if(message == null)
                return false;

            if (deal.MaxProcessingCutoffTime > HighResolutionDateTime.UtcNow)
            {
                if (deal.SetExecutionInfo(RejectableMMDeal.DealStatus.KgtConfirmed))
                {
                    //quickfixn returns allways true
                    bool sent = this._ordChannel.SendMessage(message);
                    deal.IntegratorSentResponseTimeUtc = HighResolutionDateTime.UtcNow;

                    this.OnDealAccepted(deal);

                    if (deal.CounterpartyRequestedSizeBaseAbs - deal.SizeToBeFilledBaseAbs > 0)
                    {
                        if(this._ordChannel.UpdateAcceptMessageWithRejectRestInfo(deal, message))
                            this._ordChannel.SendMessage(message);
                        else
                        {
                            this._logger.Log(LogLevel.Fatal, "Unable to create reject rest exec report for partially filled order");
                            this._riskManager.TurnOnGoFlatOnly("Unable to create reject rest exec report for partially filled order");
                        }
                    }

                    return sent;
                }
                return false;
            }
            else
            {
                this.RejectDealInternal(deal, RejectableMMDeal.DealRejectionReason.KgtTooLate, false);
                this._logger.Log(LogLevel.Error, "AutoRejecting deal that was attempted to be accepted to late (Expected max by {0:HH:mm:ss.fffffff})", deal.MaxProcessingCutoffTime);
                return false;
            }
        }

        private void RejectDealInternal(RejectableMMDeal deal, RejectableMMDeal.DealRejectionReason rejectionReason, bool isAutoRejectionAfterTimeout)
        {
            deal.IntegratorExecutionId = _generatedIdsHelper.GetExecId(deal.Symbol, deal.IntegratorDealDirection, deal.LayerId, true);
            var message = this._ordChannel.CreateRejectionMessage(deal, (RejectableMMDeal.DealStatus) rejectionReason);


            if (
                !isAutoRejectionAfterTimeout && deal.SetExecutionInfo((RejectableMMDeal.DealStatus) rejectionReason)
                ||
                isAutoRejectionAfterTimeout && deal.CanTimeoutDeal()
                )
            {
                this._ordChannel.SendMessage(message);
                deal.IntegratorSentResponseTimeUtc = HighResolutionDateTime.UtcNow;

                this.OrdChannelOnDealRejected(deal);
            }
            else
            {
                this._logger.Log(isAutoRejectionAfterTimeout ? LogLevel.Fatal : LogLevel.Warn,
                    "Rejection of deal {0} was not performed as it was already in closed status", deal.CounterpartyClientOrderIdentifier);
            }
        }

        public void RejectDeal(RejectableMMDeal deal, RejectableMMDeal.DealRejectionReason rejectionReason)
        {
            this.RejectDealInternal(deal, rejectionReason, false);
        }

        public event Action OnStopInitiated
        {
            add
            {
                if (!object.ReferenceEquals(_quoChannel, _ordChannel))
                    this._ordChannel.OnStopInitiated += value;
                this._quoChannel.OnStopInitiated += value;
            }
            remove
            {
                if (!object.ReferenceEquals(_quoChannel, _ordChannel))
                    this._ordChannel.OnStopInitiated -= value;
                this._quoChannel.OnStopInitiated -= value;
            }
        }

        public event Action OnStarted;


        private void OnOneSessionStarted()
        {
            if (this.IsReadyAndOpen && this.OnStarted != null)
                this.OnStarted();
        }

        public bool IsReadyAndOpen { get { return this._ordChannel.IsReadyAndOpen && this._quoChannel.IsReadyAndOpen; } }
    }
}
