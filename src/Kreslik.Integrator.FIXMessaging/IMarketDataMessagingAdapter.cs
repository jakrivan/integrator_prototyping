﻿using System;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Symbol = Kreslik.Integrator.Common.Symbol;

namespace Kreslik.Integrator.FIXMessaging
{
    internal delegate void HandlePriceObjectInternal(PriceObjectInternal priceObject, string subscriptionIdentity, ref bool heldWithoutAcquireCall);
    internal delegate void HandleQuoteObjectInternal(QuoteObject quoteObject, ref bool askHeldWithoutAcquireCall, ref bool bidHeldWithoutAcquireCall);

    internal interface IMarketDataMessagingAdapter
    {
        bool SendSubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity);
        bool SendUnsubscriptionMessage(SubscriptionRequestInfo subscriptionInfo, string subscriptionIdentity);
        void ClearInternalStateAfterStop();
        void AddSubscriptionIdentity(string subscriptionIdentity);

        /*event Action<QuoteObject> OnNewQuote;
        event Action<PriceObjectInternal, string> OnNewPrice;*/
        event HandleQuoteObjectInternal OnNewQuote;
        event HandlePriceObjectInternal OnNewPrice;
        event Action<PriceObjectInternal> OnNewNonexecutablePrice;
        event Action<string> OnCancelLastQuote;
        event Action<Symbol, Counterparty, PriceSide> OnCancelLastPrice;
        event Action<SubscriptionChangeInfo> OnSubscriptionChanged;
        event Action<QuickFix.Message> OnBusinessReject;
    }
}
