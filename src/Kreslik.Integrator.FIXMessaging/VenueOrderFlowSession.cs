﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;

namespace Kreslik.Integrator.FIXMessaging
{
    public sealed class VenueOrderFlowSession : OrderFlowSession
    {
        private IVenueFIXConnector _venueOrderFlowMessagingAdapter;

        public VenueOrderFlowSession(
            IVenueFIXConnector venueOrderFlowMessagingAdapter,
            ILogger logger, Counterparty counterparty, IRiskManager riskManager,
            IDealStatisticsConsumer dealStatisticsConsumer, OrderFlowSessionSettings settings, IDelayEvaluator delayEvaluator)
            : base(
                venueOrderFlowMessagingAdapter, venueOrderFlowMessagingAdapter, logger, counterparty, riskManager,
                dealStatisticsConsumer, settings, delayEvaluator)
        {
            this._venueOrderFlowMessagingAdapter = venueOrderFlowMessagingAdapter;
            this._dealRejectionSupported = this._venueOrderFlowMessagingAdapter.DealRejectionSupported;
        }
        protected override void OnChannelStarted()
        {
            DateTime startTime = HighResolutionDateTime.UtcNow;
            if (this._openedOrdersMap.Count > 0)
            {
                Thread.Sleep(TimeSpan.FromSeconds(2));
            }

            if (this._openedOrdersMap.Count > 0)
            {
                if (this._venueOrderFlowMessagingAdapter.OrderStatusReportingSupported)
                {
                    this.Logger.Log(LogLevel.Error,
                                    "Receiving FIX channel start event, but there are {0} orders in open state ({1}). Attempting to get their status.",
                                    this._openedOrdersMap.Count,
                                    string.Join(", ", this._openedOrdersMap.Keys));

                    int orderId = 1;
                    foreach (WritableOrder writableOrder in _openedOrdersMap.Values.Where(ord => ord != null && ord.IntegratorSentTimeUtc < startTime))
                    {
                        //Max 16 hexa chars with no leading zeros for some venues (LMAX)
                        string operationIdentity = string.Format("{0}", orderId++);
                        this.AddCancelOperation(operationIdentity);

                        this._venueOrderFlowMessagingAdapter.SendOrderStatusRequest(
                            writableOrder.OrderRequestInfo,
                            writableOrder.Identity,
                            operationIdentity);
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(2));
                }
                else
                {
                    this.Logger.Log(LogLevel.Error,
                                    "Receiving FIX channel start event, but there is {0} orders in open state ({1}). Cancelling those as vanue should autocancel orders on disconnect.",
                                    this._openedOrdersMap.Count,
                                    string.Join(", ", this._openedOrdersMap.Keys));
                }

                foreach (WritableOrder writableOrder in _openedOrdersMap.Values.Where(ord => ord != null && ord.IntegratorSentTimeUtc < startTime))
                {
                    this.Logger.Log(LogLevel.Fatal,
                                    "Order {0} will be autocancelled by Integrator as it stayed open cross {1} session reset and we didn't get any info from {1} about it",
                                    writableOrder, this.Counterparty);
                    FlipOrderState(writableOrder, IntegratorOrderExternalStatus.Cancelled,
                                   new OrderChangeEventArgs(IntegratorOrderExternalChange.Cancelled, null,
                                                            new RejectionInfo(
                                                                "Integrator autocanecling order as venue session disconnected",
                                                                writableOrder.OrderRequestInfo
                                                                             .SizeBaseAbsInitial -
                                                                writableOrder.FilledAmount -
                                                                writableOrder.NotToBeFilledAmount,
                                                                RejectionType.OrderReject), null), null, true);
                }

            }

            base.OnChannelStarted();
        }

        public override void Stop()
        {
            this.SetStopInProcess();
            this.StopInternal(TimeSpan.Zero);
        }

        public override void StopAsync(TimeSpan delay)
        {
            if (this.SetStopInProcess())
            {
                this.Logger.Log(LogLevel.Info, "Triggering delayed stop (will not be executed if stopped in meantime)");

                TaskEx.StartNew(() => this.StopInternal(delay));
            }
        }

        private readonly bool _dealRejectionSupported;
        public override bool DealRejectionSupported { get { return _dealRejectionSupported; } }

        public bool IsOrderHoldTimeWithinLimits(TimeSpan orderHoldTime)
        {
            return orderHoldTime < this._venueOrderFlowMessagingAdapter.MaximumDealConfirmationDelayWithinIntegrator - Constansts.MinimumSpanBetweenLLandWatchdog;
        }

        private void StopInternal(TimeSpan delay)
        {
            //If there are no outstanding orders, then stop immediately and even multiple times
            //If there are multiple orders, then first attempt to cancel them (but just once!) and only then stop (again - jsut once)

            if (this._openedOrdersMap.Count > 0)
            {
                if (!_outstandingOrdersCancelled)
                {
                    _outstandingOrdersCancelled = true;

                    //following must not fail - we must stop the session eventualy
                    Extensions.PerformActionWithErrorHandling(() =>
                    {
                        this.Logger.Log(LogLevel.Fatal,
                                        "Receiving FIX channel stop event, but there is {0} orders in open state ({1}). Will attempt to automatically cancell all of those",
                                        this._openedOrdersMap.Count, string.Join(", ", this._openedOrdersMap.Keys));

                        if (!CancelAllOrders())
                        {
                            this.Logger.Log(LogLevel.Error,
                                            "Cancelling of all open orders didn't finish successfully");
                        }
                    });

                    Thread.Sleep(TimeSpan.FromSeconds(3));
                }
                else
                {
                    //other thread is already stopping
                    return;
                }
            }

            if(delay > TimeSpan.Zero)
                Thread.Sleep(delay);

            //if immeadiate stop was requested or if session was not started in the meantime after dealy - stop
            if (delay == TimeSpan.Zero || this.GetIsStopInProgress(true))
                base.Stop();
        }

        public bool CancelAllOrders()
        {
            if (this._venueOrderFlowMessagingAdapter.CancelAllSupported)
            {
                //Set timeout for all active orders - they all should be cancelled, otherwise they will flip into broken state
                foreach (WritableOrder integratorOrderExternal in ActiveOrderList)
                {
                    this.TrySetCancellationOperationTimeoutIfNotClosed(integratorOrderExternal);
                }

                string operationIdentity = Guid.NewGuid().ToString("N");
                this.AddCancelOperation(operationIdentity);
                string outgoingMessageRaw;
                if (this._venueOrderFlowMessagingAdapter.SendCancelAllOrdersMessage(operationIdentity,
                                                                                    out outgoingMessageRaw))
                {
                    this._dealStatisticsConsumer.ExternalDealCancelSent(
                        new ExternalCancelRequest(HighResolutionDateTime.UtcNow, CancelType.CancelAll,
                                                  outgoingMessageRaw), null);
                    return true;
                }
                else
                {
                    foreach (WritableOrder integratorOrderExternal in ActiveOrderList)
                    {
                        integratorOrderExternal.SetOrderCancellationDone();
                    }

                    return false;
                }
            }
            else
            {
                bool succeed = true;
                foreach (WritableOrder writableOrder in _openedOrdersMap.Values)
                {
                    succeed = succeed && this.CancelOrder(writableOrder.Identity) == CancelRequestResult.Success;
                }

                return succeed;
            }
        }

        public CancelRequestResult CancelOrder(string existingOrderIdentity)
        {
            WritableOrder existingOrderExternal;
            if (!_openedOrdersMap.TryGetValue(existingOrderIdentity, out existingOrderExternal))
            {
                this.Logger.Log(LogLevel.Info,
                                "Attempt to cancel order [{0}] that is already not known to OrderFlow session (it is probably already done). Ignoring the attempt",
                                existingOrderIdentity);
                return CancelRequestResult.Failed_OrderNotKnown;
            }

            if (!this.TrySetCancellationOperationTimeoutIfNotClosed(existingOrderExternal))
            {
                this.Logger.Log(LogLevel.Info,
                                "Attempt to cancel order [{0}] that is already closed. Ignoring the attempt",
                                existingOrderIdentity);
                return CancelRequestResult.Failed_OrderAlreadyClosed;
            }

            //Max 20 chars for some venues (LMAX)
            string operationIdentity = this.FormatIdentityString(existingOrderExternal.OrderRequestInfo.Symbol, "C");
            this.AddCancelOperation(operationIdentity);

            string outgoingMessageRaw;
            if (this._venueOrderFlowMessagingAdapter.SendCancelOrderMessage(
                existingOrderExternal.OrderRequestInfo, existingOrderExternal.Identity,
                existingOrderExternal.RemotePlatformOrderId, operationIdentity, out outgoingMessageRaw))
            {
                this._dealStatisticsConsumer.ExternalDealCancelSent(
                    new ExternalCancelRequest(HighResolutionDateTime.UtcNow, CancelType.Cancel, outgoingMessageRaw),
                    existingOrderExternal);
                return CancelRequestResult.Success;
            }
            else
            {
                existingOrderExternal.SetOrderCancellationDone();
                return CancelRequestResult.Failed_MessagingLayerError;
            }
        }

        public bool ReplaceOrder(string existingOrderIdentity, IIntegratorOrderExternal replacingOrder)
        {
            if (replacingOrder == null)
            {
                this.Logger.Log(LogLevel.Error,
                                "Attempt to replace order [{0}] with order that is NULL (probably createdfrom invalid request). Ignoring the attempt",
                                existingOrderIdentity);
                return false;
            }

            WritableOrder existingOrderExternal;
            if (!_openedOrdersMap.TryGetValue(existingOrderIdentity, out existingOrderExternal))
            {
                this.Logger.Log(LogLevel.Error,
                                "Attempt to replace order [{0}] (with order [{1}]) that is already not known to OrderFlow session (it is probably already done). Ignoring the attempt",
                                existingOrderIdentity, replacingOrder.Identity);
                return false;
            }

            OrderRequestInfo existingOrderInfo = existingOrderExternal.OrderRequestInfo;

            if (existingOrderInfo.OrderType == OrderType.Pegged)
            {
                this.Logger.Log(LogLevel.Error,
                                "Attempt to replace pegged order [{0}]. Cancel/Replace is not allowed for pegged orders",
                                existingOrderIdentity);
                return false;
            }

            if (replacingOrder.OrderRequestInfo.OrderType != existingOrderInfo.OrderType
                || replacingOrder.OrderRequestInfo.Side != existingOrderInfo.Side
                || replacingOrder.OrderRequestInfo.Symbol != existingOrderInfo.Symbol
                || replacingOrder.OrderRequestInfo.TimeInForce != existingOrderInfo.TimeInForce)
            {
                this.Logger.Log(LogLevel.Error,
                                "Attempt to replace order [{0}] with order ([{1}]) that differ in different parameter(s) than just OrderQty, MaxShow or Price. (old: {2} new: {3})",
                                existingOrderIdentity, replacingOrder.Identity, existingOrderInfo, replacingOrder.OrderRequestInfo);
                return false;
            }

            if (!this.TrySetCancellationOperationTimeoutIfNotClosed(existingOrderExternal))
            {
                this.Logger.Log(LogLevel.Error,
                                "Attempt to replace order [{0}] (with order [{1}]) that is already closed. Ignoring the attempt",
                                existingOrderIdentity, replacingOrder.Identity);
                return false;
            }

            //Fluent specific: Fluent is keeping the tag 37 (OrderId) across C/Replaces
            if (!string.IsNullOrEmpty(existingOrderExternal.RemotePlatformOrderId))
            {
                WritableOrder writableReplacingOrder = replacingOrder as WritableOrder;

                if (writableReplacingOrder != null)
                {
                    writableReplacingOrder.RemotePlatformOrderId = existingOrderExternal.RemotePlatformOrderId;
                }
            }

            //IIntegratorOrderExternal newOrder = this.GetNewExternalOrder(replacingOrderRequestInfo);
            string outgoingMessageRawCaptured = string.Empty;
            this.AddCancelOperation(replacingOrder.Identity);
            this.AddCancelOperation(existingOrderIdentity);
            if (this.SubmitOrder(replacingOrder,
                                 (OrderRequestInfo newOrderInfo, string newOrderIdentity, out string outgoingMessageRaw) =>
                                 {
                                     bool success;
                                     success = this._venueOrderFlowMessagingAdapter.SendCancelReplaceOrder(
                                         existingOrderInfo, existingOrderIdentity,
                                         existingOrderExternal.RemotePlatformOrderId, newOrderInfo,
                                         newOrderIdentity, out outgoingMessageRawCaptured);
                                     outgoingMessageRaw = outgoingMessageRawCaptured;
                                     return success;
                                 }) == SubmissionResult.Success)
            {
                this._dealStatisticsConsumer.ExternalDealCancelSent(
                    new ExternalCancelRequest(replacingOrder.IntegratorSentTimeUtc, CancelType.CancelAndReplace,
                                              outgoingMessageRawCaptured),
                    existingOrderExternal);
                return true;
            }
            else
            {
                existingOrderExternal.SetOrderCancellationDone();
                return false;
            }
        }

        public override IIntegratorOrderExternal GetNewExternalOrder(OrderRequestInfo orderRequestInfo,
                                                                     out string errorMessage)
        {
            if (orderRequestInfo.OrderType == OrderType.Pegged)
            {
                //limit price required
                //allways true

                //TiF needs to be Day
                if (orderRequestInfo.TimeInForce != TimeInForce.Day)
                {
                    errorMessage = "Couldn't submit external IoC Pegged order - only Day Pegged orders are allowed";
                    this.Logger.Log(LogLevel.Error, errorMessage);
                    return null;
                }

                //Only one pegged per side and symbol
                if (
                    this._openedOrdersMap.Any(
                        ord =>
                        ord.Value.OrderRequestInfo.OrderType == OrderType.Pegged &&
                        ord.Value.OrderRequestInfo.Symbol == orderRequestInfo.Symbol &&
                        ord.Value.OrderRequestInfo.Side == orderRequestInfo.Side))
                {
                    errorMessage =
                        string.Format(
                            "Couldn't submit {0} pegged order on {1}, as this symbol and side is already pegged",
                            orderRequestInfo.Side, orderRequestInfo.Symbol);
                    this.Logger.Log(LogLevel.Error, errorMessage);
                    return null;
                }
            }

            //if (this._venueOrderFlowMessagingAdapter.DealRejectionSupported)
            //{
            //    if (orderRequestInfo.TimeInForce == TimeInForce.Day && orderRequestInfo.RejectableDealDefaults == null)
            //    {
            //        errorMessage =
            //            string.Format(
            //                "Couldn't submit {0} day order on {1} through this adapter without RejectableDealDefaults populated",
            //                orderRequestInfo.Side, orderRequestInfo.Symbol);
            //        this.Logger.Log(LogLevel.Error, errorMessage);
            //        return null;
            //    }
            //}

            return base.GetNewExternalOrder(orderRequestInfo, out errorMessage);
        }

        private bool TrySetCancellationOperationTimeoutIfNotClosed(WritableOrder writableOrder)
        {
            return writableOrder.TrySetOrderBeingCancelledIfNotClosed(this._settings.UnconfirmedOrderTimeout,
                                                                      HandleCancellationTimeout);
        }

        private void HandleCancellationTimeout(WritableOrder order)
        {
            bool isBroken;

            //ensure exclusive access and single broken even even for multiple cancellation request tight to single order
            lock (order)
            {
                isBroken = !order.TryCloseIfDone();

                if (isBroken)
                {
                    order.OrderStatus = IntegratorOrderExternalStatus.InBrokenState;
                    order.SetRestAmountBroken();
                }
            }

            if (isBroken)
            {
                this.Logger.Log(LogLevel.Fatal, "Order cancellation is timing out or too many cancellation attempts were unsuccessful. {0}", order);
                DateTime orderBrokenTimeUtc = HighResolutionDateTime.UtcNow;
                FlipOrderState(order, IntegratorOrderExternalStatus.InBrokenState,
                               new OrderChangeEventArgs(IntegratorOrderExternalChange.InBrokenState), null, true);
                this._dealStatisticsConsumer.ExternalDealIgnored(orderBrokenTimeUtc, order);
            }
            else
            {
                this.Logger.Log(LogLevel.Error,
                                "Order cancellation is timing out. But order is already done and in {1} status. {0}",
                                order, order.OrderStatus);
                //todo - this might be redundand. s if already done, then probably also removed
                RemoveOrderFromMappings(order.Identity);
            }
        }

        private void AddCancelOperation(string operartionIdenetity)
        {
            lock (_cancelOperations)
            {

                if (_cancelOperations.Count > 10)
                {
                    _cancelOperations.RemoveAll(
                        tupple => tupple.Item1 < DateTime.UtcNow - this._settings.UnconfirmedOrderTimeout);
                }

                _cancelOperations.Add(new Tuple<DateTime, string>(DateTime.UtcNow, operartionIdenetity));
            }
        }

        protected override bool IsKnownOperation(string operartionIdenetity)
        {
            lock (_cancelOperations)
            {
                return _cancelOperations.Any(tupple => tupple.Item2.Equals(operartionIdenetity));
            }
        }

        private List<Tuple<DateTime, string>> _cancelOperations = new List<Tuple<DateTime, string>>();


        private void TurnOfSessionDueToError(string error, Exception e)
        {
            this.Logger.LogException(LogLevel.Fatal, error, e);
            this.StopAsync(TimeSpan.FromMilliseconds(400));
            this._riskManager.TurnOnGoFlatOnly(error);
        }

        private void TurnOfSessionDueToError(string error)
        {
            this.Logger.Log(LogLevel.Fatal, error);
            this.StopAsync(TimeSpan.FromMilliseconds(400));
            this._riskManager.TurnOnGoFlatOnly(error);
        }

        protected override bool HandleNonconfirmedDeal(OrderChangeInfo orderChangeInfo, WritableOrder writableOrder)
        {
            try
            {
                return this.HandleNonconfirmedDealInternal(orderChangeInfo, writableOrder, false);
            }
            catch (Exception e)
            {
                this.TurnOfSessionDueToError("Unexpected error during handling rejectable deal", e);
                return false;
            }
        }

        protected override bool HandleDealAckConfirmation(OrderChangeInfo orderChangeInfo, WritableOrder writableOrder)
        {
            try
            {
                return this.HandleNonconfirmedDealInternal(orderChangeInfo, writableOrder, true);
            }
            catch (Exception e)
            {
                this.TurnOfSessionDueToError("Unexpected error during handling rejectable deal", e);
                return false;
            }
        }

        private bool HandleNonconfirmedDealInternal(OrderChangeInfo orderChangeInfo, WritableOrder writableOrder, bool confirmImmediately)
        {
            if (!this._venueOrderFlowMessagingAdapter.DealRejectionSupported)
            {
                this.TurnOfSessionDueToError(string.Format("Received nonconfirmed deal [{0}] on a venue session that is not enabled for LL confirmation",
                                writableOrder.Identity));
                return false;
            }

            OrderChangeInfoEx orderChangeInfoEx = orderChangeInfo as OrderChangeInfoEx;

            if (orderChangeInfoEx == null)
            {
                this.TurnOfSessionDueToError(string.Format("Invalid OrderChangeInfo report used to confirm/reject deal. {0}", orderChangeInfo));
                return false;
            }

            orderChangeInfoEx.CalculateFixMessageIfNeeded(this.Logger);

            if (orderChangeInfoEx.Message == null)
            {
                this.TurnOfSessionDueToError(string.Format("OrderChangeInfo report doesn't contain the Fix message to confirm/reject deal. {0}", orderChangeInfo));
                return false;
            }

            AutoRejectableDeal rejectableDeal = new AutoRejectableDeal(orderChangeInfoEx, writableOrder);
            if (!this._riskManager.CheckIfIncomingClientDealAllowed(rejectableDeal))
            {
                this.TryRejectDeal(rejectableDeal, AutoRejectableDeal.RejectableTakerDealRejectionReason.RiskMgmtDisallow);
                return false;
            }

            if (confirmImmediately)
            {
                return this._venueOrderFlowMessagingAdapter.TryAcceptDeal(rejectableDeal);
            }
            else
            {
                if (!rejectableDeal.SetTimeout(orderChangeInfoEx,
                    this._venueOrderFlowMessagingAdapter.MaximumDealProcessingCutoff(orderChangeInfoEx), TryRejectDeal))
                {
                    this.TryRejectDeal(rejectableDeal, AutoRejectableDeal.RejectableTakerDealRejectionReason.KgtTooLate);
                    return false;
                }

                orderChangeInfoEx.UpdateChangeEventArgs(rejectableDeal);
            }

            return true;
        }


        //protected override void HandleRequestedDealReject(OrderChangeInfo orderChangeInfo, WritableOrder writableOrder)
        //{
        //    //If ECN force rejected our deal, than attempt to discard the local object so that we don't send out unnecesary messages
        //    if (orderChangeInfo.ChangeEventArgs.RejectionInfo.RejectionType == RejectionType.ECNForcedDealReject)
        //    {
        //        //get the transaction id (needs to be populated)
        //        string integratorTransactionId =
        //            writableOrder.GetNextDealId(orderChangeInfo.ChangeEventArgs.ExecutionInfo.CounterpartyTransactionId);

        //        AutoRejectableDeal nonconfiremedDeal;
        //        if (_nonconfirmedDeals.TryGetValue(integratorTransactionId, out nonconfiremedDeal))
        //        {
        //            nonconfiremedDeal.Discard();
        //        }
        //    }
        //}

        public void TryRejectDeal(AutoRejectableDeal rejectableDeal,
            AutoRejectableDeal.RejectableTakerDealRejectionReason rejectionReason)
        {

            if (rejectableDeal.SetExecutionChange((AutoRejectableDeal.RejectableTakerDealStatus)rejectionReason))
            {
                try
                {
                    this._venueOrderFlowMessagingAdapter.TryRejectDeal(rejectableDeal, rejectionReason);
                }
                catch (Exception e)
                {
                    this.Logger.LogException(LogLevel.Fatal, e, "Error during performing [{0}] action on deal [{1}]", rejectionReason, rejectableDeal);
                }
            }
            else
            {
                this.Logger.Log(LogLevel.Fatal, "Attempt to perform [{0}] action on deal [{1}] that was already in [{2}] status.",
                    rejectionReason, rejectableDeal, rejectableDeal.CurrentDealStatus);
            }
        }

        public bool TryAcceptDeal(AutoRejectableDeal rejectableDeal)
        {
            if (!this._riskManager.CheckIfIncomingClientDealCanBeAccepted(rejectableDeal))
            {
                this.TryRejectDeal(rejectableDeal,
                    AutoRejectableDeal.RejectableTakerDealRejectionReason.RiskMgmtDisallow);
                return false;
            }

            bool accepted = false;
            if (rejectableDeal.SetExecutionChange(AutoRejectableDeal.RejectableTakerDealStatus.KgtConfirmed))
            {
                try
                {
                    accepted = this._venueOrderFlowMessagingAdapter.TryAcceptDeal(rejectableDeal);
                }
                catch (Exception e)
                {
                    this.Logger.LogException(LogLevel.Fatal, e, "Error during performing Accept action on deal [{0}]", rejectableDeal);
                }
            }
            else
            {
                this.Logger.Log(LogLevel.Fatal, "Attempt to perform Accept action on deal [{0}] that was already in [{1}] status",
                    rejectableDeal, rejectableDeal.CurrentDealStatus);
            }

            //WARNING: No need to handle deal here - it will be handled once the confirmation arrives
            return accepted;
        }
    }
}
