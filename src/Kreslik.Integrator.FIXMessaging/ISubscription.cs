﻿
namespace Kreslik.Integrator.FIXMessaging
{
    public interface ISubscription
    {
        string Identity { get; }
        SubscriptionRequestInfo SubscriptionRequestInfo { get; }
        SubscriptionStatus SubscriptionStatus { get; }
    }
}
