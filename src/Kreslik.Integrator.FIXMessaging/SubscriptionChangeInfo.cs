﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.FIXMessaging
{
    public enum SubscriptionStatus
    {
        NotSubscribed,
        Unknown,
        Subscribed_ValidationPending,
        Subscribed,
        Rejected,
        Broken
    }

    public class SubscriptionChangeInfo
    {
        public SubscriptionChangeInfo(string identity, SubscriptionStatus subscriptionStatus)
        {
            this.Identity = identity;
            this.SubscriptionStatus = subscriptionStatus;
        }

        public void OverrideContent(string identity, SubscriptionStatus subscriptionStatus)
        {
            this.Identity = identity;
            this.SubscriptionStatus = subscriptionStatus;
        }

        public string Identity { get; private set; }
        public SubscriptionStatus SubscriptionStatus { get; private set; }
    }
}
