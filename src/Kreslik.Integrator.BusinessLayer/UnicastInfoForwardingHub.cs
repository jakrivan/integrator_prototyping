﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.BusinessLayer
{
    public class UnicastInfoForwarder : ITakerUnicastInfoForwarder, IStreamingUnicastInfoForwarder
    {
        private IUnicastInfoForwardingHub _unicastInfoForwardingHub;
        private IOrderResultDispatchGateway _dispatchGateway;
        private ITakerUnicastInfoForwarder _forwarder;
        //private IStreamingUnicastInfoForwarder _streamingForwarder;

        public UnicastInfoForwarder(IUnicastInfoForwardingHub unicastInfoForwardingHub, IOrderResultDispatchGateway dispatchGateway)
        {
            this._unicastInfoForwardingHub = unicastInfoForwardingHub;
            this._dispatchGateway = dispatchGateway;
        }

        public UnicastInfoForwarder(IUnicastInfoForwardingHub unicastInfoForwardingHub, ITakerUnicastInfoForwarder forwarder)
        {
            this._unicastInfoForwardingHub = unicastInfoForwardingHub;
            this._forwarder = forwarder;
        }


        public void OnIntegratorDealInternal(IntegratorDealInternal integratorDealInternal)
        {
            if(this._dispatchGateway != null)
                this._dispatchGateway.OnIntegratorUnicastInfo(integratorDealInternal);
            this._unicastInfoForwardingHub.ForwardUnicastInfoIfNeeded(integratorDealInternal,
                                                                      ForwardingRequestType.Deals,
                                                                      integratorDealInternal.SenderTradingTargetType,
                                                                      integratorDealInternal.Symbol);
            if (this._forwarder != null)
                this._forwarder.OnIntegratorDealInternal(integratorDealInternal);
        }

        public void OnIntegratorUnconfirmedDealInternal(IntegratorUnconfirmedDealInternal integratorUnconfirmedDealInternal)
        {
            if (this._dispatchGateway != null)
                this._dispatchGateway.OnIntegratorUnicastInfo(integratorUnconfirmedDealInternal);
            this._unicastInfoForwardingHub.ForwardUnicastInfoIfNeeded(integratorUnconfirmedDealInternal,
                                                                      ForwardingRequestType.UnconfirmedDeals,
                                                                      integratorUnconfirmedDealInternal.SenderTradingTargetType,
                                                                      integratorUnconfirmedDealInternal.Symbol);
            if (this._forwarder != null)
                this._forwarder.OnIntegratorUnconfirmedDealInternal(integratorUnconfirmedDealInternal);
        }

        public void OnIntegratorClientOrderUpdate(ClientOrderUpdateInfo clientOrderUpdateInfo, Symbol symbol)
        {
            if (this._dispatchGateway != null)
                this._dispatchGateway.OnIntegratorUnicastInfo(clientOrderUpdateInfo);
            if (symbol != Common.Symbol.NULL)
            {
                this._unicastInfoForwardingHub.ForwardUnicastInfoIfNeeded(clientOrderUpdateInfo,
                                                                          ForwardingRequestType.OrderUpdates,
                                                                          clientOrderUpdateInfo.SenderTradingTargetType,
                                                                          symbol);
            }
            if (this._forwarder != null)
                this._forwarder.OnIntegratorClientOrderUpdate(clientOrderUpdateInfo, symbol);
        }

        public void OnCounterpartyRejectedOrder(CounterpartyRejectionInfo counterpartyRejectionInfo)
        {
            if (this._dispatchGateway != null)
                this._dispatchGateway.OnIntegratorUnicastInfo(counterpartyRejectionInfo);
            this._unicastInfoForwardingHub.ForwardUnicastInfoIfNeeded(counterpartyRejectionInfo,
                                                                      ForwardingRequestType.CounterpartyRejections,
                                                                      counterpartyRejectionInfo.SenderTradingTargetType,
                                                                      counterpartyRejectionInfo.Symbol);
            if (this._forwarder != null)
                this._forwarder.OnCounterpartyRejectedOrder(counterpartyRejectionInfo);
        }

        public void OnCounterpartyIgnoredOrder(CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo)
        {
            if (this._dispatchGateway != null)
                this._dispatchGateway.OnIntegratorUnicastInfo(counterpartyOrderIgnoringInfo);
            this._unicastInfoForwardingHub.ForwardUnicastInfoIfNeeded(counterpartyOrderIgnoringInfo,
                                                                      ForwardingRequestType.CounterpartyIgnores,
                                                                      counterpartyOrderIgnoringInfo.SenderTradingTargetType,
                                                                      counterpartyOrderIgnoringInfo.Symbol);
            if (this._forwarder != null)
                this._forwarder.OnCounterpartyIgnoredOrder(counterpartyOrderIgnoringInfo);
        }

        public void OnRejectableStreamingDeal(RejectableMMDeal rejectableMmDeal)
        {
            if (this._dispatchGateway != null)
                this._dispatchGateway.OnIntegratorUnicastInfo(rejectableMmDeal);
            this._unicastInfoForwardingHub.ForwardUnicastInfoIfNeeded(rejectableMmDeal,
                                                                      ForwardingRequestType.RejectableStreamingDeal,
                                                                      rejectableMmDeal.SenderTradingTargetType,
                                                                      rejectableMmDeal.Symbol);
            //if (this._streamingForwarder != null)
            //    this._streamingForwarder.OnRejectableStreamingDeal(rejectableMmDeal);
        }
    }

    //forwarding hub: will take info from order maangers
    public class UnicastInfoForwardingHub : IUnicastInfoForwardingHub
    {
        private static IUnicastInfoForwardingHub _instance = new UnicastInfoForwardingHub();

        public static IUnicastInfoForwardingHub Instance
        {
            get { return _instance; }
        }

        //
        private Action<IntegratorMulticastInfo>[] _unicastInfoHandlers;

        private UnicastInfoForwardingHub()
        {
            _unicastInfoHandlers = new Action<IntegratorMulticastInfo>[MulticasInfoUtils.MutlicastEventsCount];
            this.MountImportantInfoForwarding(LogFactory.ImportantInfoProvider);
        }

        private void MountImportantInfoForwarding(IImportantInfoProvider importantInfoProvider)
        {
            importantInfoProvider.NewImportantInfo +=
                (subject, body, count) =>
                    this.ForwardUnicastInfoIfNeeded(new ImportantIntegratorInformation(subject, body, count),
                        MulticasInfoUtils.GetNontradingMulticastIndex(MulticastRequestType.FatalEmails));
        }

        #region IUnicastInfoForwardingHub implementations

        public void RegisterForwardingRequest(IIntegratorMulticastRequestInfo subscription, Action<IntegratorMulticastInfo> handler)
        {
            this._unicastInfoHandlers[subscription.MulticastTypeIndex] -= handler;

            if (subscription.Subscribe)
            {
                this._unicastInfoHandlers[subscription.MulticastTypeIndex] += handler;
            }
        }

        public void RemoveAllSubscriptions(Action<IntegratorMulticastInfo> handler)
        {
            for (int idx = 0; idx < MulticasInfoUtils.MutlicastEventsCount; idx++)
            {
                this._unicastInfoHandlers[idx] -= handler;
            }
        }

        public void ForwardUnicastInfoIfNeeded(IntegratorTradingInfoObjectBase integratorInfoObjectBase, ForwardingRequestType requestType, TradingTargetType senderTradingTargetType, Symbol symbol)
        {
            Action<IntegratorMulticastInfo> subscribedHandlers =
                _unicastInfoHandlers[MulticasInfoUtils.GetTradingMulticastIndex(senderTradingTargetType, symbol, requestType)];
            if (subscribedHandlers != null)
            {
                subscribedHandlers(new IntegratorMulticastInfo(integratorInfoObjectBase, requestType, symbol));
            }
        }

        public void ForwardUnicastInfoIfNeeded(IntegratorInfoObjectBase integratorInfoObjectBase, int multicastIndex)
        {
            Action<IntegratorMulticastInfo> subscribedHandlers = _unicastInfoHandlers[multicastIndex];
            if (subscribedHandlers != null)
            {
                subscribedHandlers(new IntegratorMulticastInfo(multicastIndex, integratorInfoObjectBase));
            }
        }

        public ITakerUnicastInfoForwarder CreateWrappedUnicastInfoForwarder(ITakerUnicastInfoForwarder forwarder)
        {
            return new UnicastInfoForwarder(this, forwarder);
        }

        #endregion /IUnicastInfoForwardingHub implementations
    }
}
