﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.FIXMessaging;

namespace Kreslik.Integrator.BusinessLayer
{
    public enum MarketableClientOrdersMatchingStrategy
    {
        ImmediateMatching,
        NextFreshPriceMatching
    }

    public interface IOrderRejectionsInfo
    {
        int RejectionCount { get; }
        TimeSpan RejectionsDuration { get; }
    }

    public static class DestinationSizeGranularityUtils
    {
        private const decimal _MAX_GRANULARITY = 10000m;
        private static decimal[] _granularities = Enumerable.Repeat(_MAX_GRANULARITY, Counterparty.ValuesCount).ToArray();

        static DestinationSizeGranularityUtils()
        {
            _granularities[(int) Counterparty.L01] = 1000m;
            _granularities[(int)Counterparty.L02] = 1000m;
            _granularities[(int)Counterparty.L03] = 1000m;
            _granularities[(int)Counterparty.L04] = 1000m;
            _granularities[(int)Counterparty.L05] = 1000m;
            _granularities[(int)Counterparty.L06] = 1000m;
            _granularities[(int)Counterparty.L07] = 1000m;
            _granularities[(int)Counterparty.L08] = 1000m;
            _granularities[(int)Counterparty.L09] = 1000m;
            _granularities[(int)Counterparty.L10] = 1000m;
            _granularities[(int)Counterparty.L11] = 1000m;
            _granularities[(int)Counterparty.L12] = 1000m;
            _granularities[(int)Counterparty.L13] = 1000m;
            _granularities[(int)Counterparty.L14] = 1000m;
            _granularities[(int)Counterparty.L15] = 1000m;
            _granularities[(int)Counterparty.L16] = 1000m;
            _granularities[(int)Counterparty.L17] = 1000m;
            _granularities[(int)Counterparty.L18] = 1000m;
            _granularities[(int)Counterparty.L19] = 1000m;
            _granularities[(int)Counterparty.L20] = 1000m;
            _granularities[(int)Counterparty.LM3] = 1000m;
            _granularities[(int) Counterparty.LM2] = 10000m;

            foreach (BankCounterparty bankCounterparty in BankCounterparty.Values)
            {
                _granularities[(int) bankCounterparty] = 0.01m;
            }
        }

        public static decimal GetCounterpartySizeGranularityIncrement(Counterparty counterparty)
        {
            return _granularities[(int) counterparty];
        }
    }

    public class ConsecutiveRejectionsStopStrategyClientOrdersMatchingEvaluator
    {
        private ConsecutiveRejectionsStopStrategyClientOrdersMatchingEvaluator()
        { }

        public static ConsecutiveRejectionsStopStrategyClientOrdersMatchingEvaluator CreateFromSettings(
            BusinessLayerSettings.ConsecutiveRejectionsStopStrategySettings settings)
        {
            ConsecutiveRejectionsStopStrategyClientOrdersMatchingEvaluator evaluator = new ConsecutiveRejectionsStopStrategyClientOrdersMatchingEvaluator();
            evaluator.StrategyEnabled = settings.StrategyEnabled;

            if (settings.StrategyEnabled)
            {
                evaluator.CounterpartyRanks =
                    Enumerable.Repeat(settings.DefaultCounterpartyRank, Counterparty.ValuesCount).ToArray();
                foreach (BusinessLayerSettings.ConsecutiveRejectionsStopStrategySettings.CounterpartyRank counterpartyRank in settings.CounterpartyRanks)
                {
                    evaluator.CounterpartyRanks[(int) (Counterparty) counterpartyRank.CounterpartyCode] = counterpartyRank.Rank;
                }

                evaluator.NumerOfRejectionsToTriggerStrategy = settings.NumerOfRejectionsToTriggerStrategy;
                evaluator.DurationOfRejectionsToTriggerStrategy = settings.DurationOfRejectionsToTriggerStrategy;
                evaluator.CounterpartyRankWeightMultiplier = settings.CounterpartyRankWeightMultiplier;
                evaluator.PriceAgeRankWeightMultiplier = settings.PriceAgeRankWeightMultiplier;
                evaluator.PriceBpRankWeightMultiplier = settings.PriceBpRankWeightMultiplier;
            }

            return evaluator;
        }

        public bool NeedsTunnelBreakingMatching(IOrderRejectionsInfo order)
        {
            return this.StrategyEnabled &&
                   (order.RejectionCount >= this.NumerOfRejectionsToTriggerStrategy ||
                    order.RejectionsDuration >= this.DurationOfRejectionsToTriggerStrategy);
        }

        public bool StrategyEnabled { get; private set; }
        public int NumerOfRejectionsToTriggerStrategy { get; private set; }
        public TimeSpan DurationOfRejectionsToTriggerStrategy { get; private set; }
        public decimal[] CounterpartyRanks { get; private set; }
        public decimal CounterpartyRankWeightMultiplier { get; private set; }
        public decimal PriceAgeRankWeightMultiplier { get; private set; }
        public decimal PriceBpRankWeightMultiplier { get; private set; }

        private decimal GetPriceRank(PriceObjectInternal priceObject, PriceObjectInternal bestPrice, DateTime referenceTime)
        {
            decimal priceRank = Math.Abs(priceObject.Price - bestPrice.Price) / (priceObject.Price + bestPrice.Price) * 2 * 10000 * this.PriceBpRankWeightMultiplier;
            //consider total latency (from counterparty sent). If counterparty doesn't populate sending time penailze it with default value
            //Total latency is better during the tunnel caused by GC, as counterparties with quicker catch-up after freez time will have better ranking
            decimal priceAgeRank =
                Math.Max((decimal) (referenceTime - priceObject.CounterpartySentTimeUtc).TotalMilliseconds, 0m) *
                PriceAgeRankWeightMultiplier;

            decimal counterpartyRank = CounterpartyRanks[(int) priceObject.Counterparty]*
                                       CounterpartyRankWeightMultiplier;

            return priceRank + priceAgeRank + counterpartyRank;
        }

        public List<Tuple<PriceObjectInternal, decimal>> SortPricesByRankingAsc(PriceObjectInternal[] prices, PriceObjectInternal bestPrice, DateTime referenceTime)
        {
            if(prices == null || prices.Length == 0 || PriceObjectInternal.IsNullPrice(bestPrice))
                return new List<Tuple<PriceObjectInternal, decimal>>();

            return prices.Select(
                price => new Tuple<PriceObjectInternal, decimal>(price, this.GetPriceRank(price, bestPrice, referenceTime)))
                  .OrderBy(tupple => tupple.Item2, Comparer<decimal>.Default)
                  .ToList();
        }
    }

    public class SingleSymbolDirectionOrderManager
    {
        private volatile IntegratorOrderInternal _bestOrder;
        private volatile bool _hasMarketOrders;
        private volatile bool _hasMarketOnImprovementOrders;
        private List<IntegratorOrderInternal> _marketOrders;
        private List<IntegratorOrderInternal> _marketIfImprovementOrders;
        private List<IntegratorOrderInternal> _limitOrders;
        private List<IntegratorOrderInternal> _pendingInactiveOrders;
        private List<IntegratorOrderInternal> _closedOrders;
        private Dictionary<IIntegratorOrderExternal, List<IntegratorOrderInternal>> _externalOrdersMap;
        private Func<IClientOrder, PriceObjectInternal, bool> _meetsPriceCriteriaFunc;
        private IComparer<IClientOrder> _fillPreferenceComparer;
        private IComparer<IClientOrder> _orderRankComparer;
        private ILogger _logger;
        private DealDirection _dealDirection;
        private Symbol _symbol;
        private IRiskManager _riskManager;

        private IChangeablePriceBook<PriceObjectInternal, Counterparty> _priceBook;
        private IList<IOrderFlowSession> _orderFlowSessions;
        private object _matchingLocker = new object();
        //private int _matchingStatus = IDLE;
        //private const int IDLE = 0;
        //private const int BUSY = 1;

        private static List<string> _existingInstacesIdentifications = new List<string>();

        private bool _autoKillInternalOrdersOnOrdersTransmissionDisabled;
        private MarketableClientOrdersMatchingStrategy _ordersMatchingStrategy;
        private ConsecutiveRejectionsStopStrategyClientOrdersMatchingEvaluator _consecutiveRejectionsStopStrategyClientOrdersMatchingEvaluator;
        private List<Counterparty> _counterpartiesToAutoBlackListAfterPreviousReject = new List<Counterparty>(); 

        private static bool IsInstanceUnique(Symbol symbol, DealDirection dealDirection)
        {
            string identifier = symbol.ShortName + dealDirection;
            bool exists = _existingInstacesIdentifications.Any(sid => sid.Equals(identifier));
            _existingInstacesIdentifications.Add(identifier);
            return !exists;
        }

        public bool KeepClosedOrders { get; set; }

        public SingleSymbolDirectionOrderManager(IOrderEvaluator orderEvaluator,
                                                 IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook,
                                                 IList<IOrderFlowSession> orderFlowSessions,
                                                 Symbol symbol, DealDirection dealDirection, ILogger logger,
                                                 IRiskManager riskManager, MarketableClientOrdersMatchingStrategy ordersMatchingStrategy)
        {
            this._logger = logger;
            this._riskManager = riskManager;

            if (!IsInstanceUnique(symbol, dealDirection))
            {
                logger.Log(LogLevel.Fatal,
                           "SingleSymbolDirectionOrderManager already created for combination {0} dealDirection={1}",
                           symbol, dealDirection);
                throw new Exception(
                    "SingleSymbolDirectionOrderManager is thread safe only if there is one instance per symbol and direction in the system");
            }

            if (
                (dealDirection == DealDirection.Sell && orderEvaluator.IsBuyOrdersEvaluator)
                ||
                (dealDirection == DealDirection.Buy && !orderEvaluator.IsBuyOrdersEvaluator)
                )
            {
                logger.Log(LogLevel.Fatal,
                           "SingleSymbolDirectionOrderManager created for {0} direction but passed order evaluator is {1}buy orders evaluator.",
                           dealDirection, orderEvaluator.IsBuyOrdersEvaluator ? string.Empty : "not ");
                throw new Exception(
                    string.Format(
                        "SingleSymbolDirectionOrderManager created for {0} direction but passed order evaluator is {1}buy orders evaluator.",
                        dealDirection, orderEvaluator.IsBuyOrdersEvaluator ? string.Empty : "not "));
            }

            this._fillPreferenceComparer = orderEvaluator.FillPreferenceComparer;
            this._meetsPriceCriteriaFunc = orderEvaluator.MeetsPriceCriteria;
            this._orderRankComparer = orderEvaluator.OrderRankComparer;
            this._marketOrders = new List<IntegratorOrderInternal>();
            this._marketIfImprovementOrders = new List<IntegratorOrderInternal>();
            this._limitOrders = new List<IntegratorOrderInternal>();
            this._pendingInactiveOrders = new List<IntegratorOrderInternal>();
            this._closedOrders = new List<IntegratorOrderInternal>();
            this._externalOrdersMap = new Dictionary<IIntegratorOrderExternal, List<IntegratorOrderInternal>>();
            this._dealDirection = dealDirection;
            this._symbol = symbol;
            this._priceBook = priceBook;
            this._orderFlowSessions = orderFlowSessions;
            this._ordersMatchingStrategy = ordersMatchingStrategy;

            this._autoKillInternalOrdersOnOrdersTransmissionDisabled = true;

            priceBook.NewNodeArrived += this.ProcessNewValidPrice;
            priceBook.ChangeableBookTopImproved += this.ProcessBookImprovement;
        }

        public void UpdateSettings(BusinessLayerSettings businessSettings)
        {
            this._autoKillInternalOrdersOnOrdersTransmissionDisabled =
                businessSettings.AutoKillInternalOrdersOnOrdersTransmissionDisabled;
            this._ordersMatchingStrategy = businessSettings.MarketableClientOrdersMatchingStrategy;

            ConsecutiveRejectionsStopStrategyClientOrdersMatchingEvaluator tempEvaluator = null;
            try
            {
                tempEvaluator =
                ConsecutiveRejectionsStopStrategyClientOrdersMatchingEvaluator.CreateFromSettings(
                    businessSettings.ConsecutiveRejectionsStopStrategyBehavior);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e,
                                          "Got exception when reading ConsecutiveRejectionsStopStrategySettings from BusinessLayer settings, therefore ignoring this portion of setting (old values will be used)");
            }
            
            if (tempEvaluator != null)
            {
                Interlocked.Exchange(ref this._consecutiveRejectionsStopStrategyClientOrdersMatchingEvaluator,
                                     tempEvaluator);
            }

            try
            {
                _counterpartiesToAutoBlackListAfterPreviousReject =
                    businessSettings.PreventMatchingToSameCounterpartyAfterOrderRejectBehavior.StrategyEnabled
                        ? businessSettings.PreventMatchingToSameCounterpartyAfterOrderRejectBehavior.CounterpartyCodes
                                          .Select(code => (Counterparty) code).ToList()
                        : new List<Counterparty>();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e,
                                          "Got exception when reading PreventMatchingToSameCounterpartyAfterOrderRejectBehavior from BusinessLayer settings, therefore ignoring this portion of setting (old values will be used)");
            }
        }

        public int ActiveLimitOrdersCount
        {
            get
            {
                lock (_matchingLocker)
                {
                    return _limitOrders.Count;
                }
            }
        }

        public int ActiveMarketOrders
        {
            get
            {
                lock (_matchingLocker)
                {
                    return _marketOrders.Count;
                }
            }
        }

        public int ActiveMarketIfImprovmentOrders
        {
            get
            {
                lock (_matchingLocker)
                {
                    return _marketIfImprovementOrders.Count;
                }
            }
        }

        public int InactivePendingOrdersCount
        {
            get
            {
                lock (_matchingLocker)
                {
                    return _pendingInactiveOrders.Count;
                }
            }
        }

        public event Action<string> OrderClosing;

        private void InvokeOrderClosing(IClientOrder order)
        {
            if(order == null)
                return;

            if (order is IntegratorOrderInternalWithTimeout)
            {
                (order as IntegratorOrderInternalWithTimeout).Discard();
            }

            if (OrderClosing != null)
            {
                OrderClosing(order.ClientOrderIdentity);
            }
        }

        private void RefreshFlagsUnlocked()
        {
            if (this._marketOrders.Count == 0)
                this._hasMarketOrders = false;
            else
                this._hasMarketOrders = true;

            if (this._marketIfImprovementOrders.Count == 0)
                this._hasMarketOnImprovementOrders = false;
            else
                this._hasMarketOnImprovementOrders = true;
        }

        //This translation is important as client orders (created externally to this class) are copied inside this class
        // and so internal changes are not reflected outside (even though it can be facilitated, but it still wouldn't
        //  be very clever to autopropagate let's say accross WCF boundaries)
        public IBankPoolClientOrder GetOrder(string clientOrderIdentity)
        {
            IBankPoolClientOrder orderInternal = _marketOrders.FirstOrDefault(ord => ord.ClientOrderIdentity.Equals(clientOrderIdentity));
            if (orderInternal != null)
            {
                return orderInternal;
            }

            orderInternal = _limitOrders.FirstOrDefault(ord => ord.ClientOrderIdentity.Equals(clientOrderIdentity));
            if (orderInternal != null)
            {
                return orderInternal;
            }

            orderInternal = _marketIfImprovementOrders.FirstOrDefault(ord => ord.ClientOrderIdentity.Equals(clientOrderIdentity));
            if (orderInternal != null)
            {
                return orderInternal;
            }

            orderInternal = _pendingInactiveOrders.FirstOrDefault(ord => ord.ClientOrderIdentity.Equals(clientOrderIdentity));
            if (orderInternal != null)
            {
                return orderInternal;
            }

            if (KeepClosedOrders)
            {
                orderInternal = _closedOrders.FirstOrDefault(ord => ord.ClientOrderIdentity.Equals(clientOrderIdentity));
                if (orderInternal != null)
                {
                    return orderInternal;
                }
            }

            return null;
        }

        private List<IntegratorOrderInternal> GetOrdersList(OrderType orderType)
        {
            switch (orderType)
            {
                case OrderType.Limit:
                case OrderType.LimitTillTimeout:
                    return _limitOrders;
                case OrderType.Market:
                case OrderType.MarketPreferLmax:
                    return _marketOrders;
                case OrderType.MarketOnImprovement:
                    return _marketIfImprovementOrders;
                default:
                    return new List<IntegratorOrderInternal>();
            }
        }

        private IIntegratorOrderExternal MatchOrdersWithPrice(PriceObjectInternal price, OrderType orderType, IIntegratorOrderExternal existingExternalOrder)
        {
            if (orderType != OrderType.Limit && orderType != OrderType.Market && orderType != OrderType.MarketOnImprovement && orderType != OrderType.LimitTillTimeout)
            {
                this._logger.Log(LogLevel.Error, "OrderType {0} was unexpected during matching", orderType);
                return existingExternalOrder;
            }

            List<IntegratorOrderInternal> internalOrdersList = this.GetOrdersList(orderType);

            List<IntegratorOrderInternal> ordersToRemoveFromActive = null;

            lock (_matchingLocker)
            {
                var ordersToFill = internalOrdersList.Where(
                    order =>
                        orderType == OrderType.Market || orderType == OrderType.MarketOnImprovement || _meetsPriceCriteriaFunc(order, price))
                    .OrderBy(order => order, _fillPreferenceComparer);

                foreach (IntegratorOrderInternal integratorOrderInternal in ordersToFill)
                {
                    if (price.SizeBaseAbsRemaining <= 0)
                        break;

                    this._logger.Log(LogLevel.Info, "Price {0} with remaining size {1} (from priceObject: {2}) found matchable with order {3}",
                                 price.Price, price.SizeBaseAbsRemaining, price, integratorOrderInternal.ClientOrderIdentity);

                    if (!IsPriceOnOrderBlacklist(price, integratorOrderInternal))
                    {
                        existingExternalOrder = CreateOrderFromMatchingPrice(integratorOrderInternal, price,
                                                                             existingExternalOrder);

                        if (integratorOrderInternal.SizeBaseAbsActive <= 0)
                        {
                            if (ordersToRemoveFromActive == null)
                                ordersToRemoveFromActive = new List<IntegratorOrderInternal>();

                            ordersToRemoveFromActive.Add(integratorOrderInternal);
                        }  
                    }
                }

                if (ordersToRemoveFromActive != null)
                {
                    foreach (IntegratorOrderInternal internalOrderToRemove in ordersToRemoveFromActive)
                    {
                        internalOrdersList.Remove(internalOrderToRemove);
                    }

                    if (orderType == OrderType.Limit || orderType == OrderType.LimitTillTimeout)
                    {
                        UpdateBestOrder();
                    }
                    else
                    {
                        //refresh internal flags
                        this.RefreshFlagsUnlocked();
                    }
                }

                UpdatePriceBookIfNeeded(price);
            }

            return existingExternalOrder;
        }

        private void UpdateBestOrder()
        {
            if (_bestOrder == null || _bestOrder.SizeBaseAbsActive <= 0)
            {
                _bestOrder = _limitOrders.OrderByDescending(order => order, this._orderRankComparer).Take(1).FirstOrDefault();
            }
        }

        private void CheckPriceWithRiskManagement(PriceObjectInternal price)
        {
            if (price.SizeBaseAbsRemaining > this._riskManager.GetMaximumOrderSizeBaseAbs(price.Symbol.BaseCurrency))
            {
                this._logger.Log(LogLevel.Warn,
                                 "Price {0} has remaining size of {1} which is greater then limit allowed by risk manager. So trimming the size",
                                 price, price.SizeBaseAbsRemaining);

                if (
                    !price.SubstractSizeBaseAbs(price.SizeBaseAbsRemaining -
                                                  this._riskManager.GetMaximumOrderSizeBaseAbs(price.Symbol.BaseCurrency)))
                {
                    this._logger.Log(LogLevel.Fatal, "Couldn't substract size {0} from price {1}",
                                     price.SizeBaseAbsRemaining -
                                     this._riskManager.GetMaximumOrderSizeBaseAbs(price.Symbol.BaseCurrency), price);
                }
            }
        }

        //this handles arbitrary new price coming to price book, not necessarily just top change
        private void ProcessNewValidPrice(IBookTop<PriceObjectInternal> sender, PriceObjectInternal price, DateTime timestamp)
        {
            if (!CheckIsPriceValid(price)) return;

            if (!_riskManager.IsTransmissionDisabled)
            {
                //prefetch the volatile fields in one shot
                var bestOrderLocal = _bestOrder;
                var hasMarketOrdersLocal = _hasMarketOrders;
                IIntegratorOrderExternal externalOrder = null;
                if (bestOrderLocal != null && price.SizeBaseAbsRemaining > 0 && _meetsPriceCriteriaFunc(bestOrderLocal, price))
                {
                    externalOrder = MatchOrdersWithPrice(price, OrderType.Limit, externalOrder);
                }

                if (price.SizeBaseAbsRemaining > 0 && hasMarketOrdersLocal)
                {
                    externalOrder = MatchOrdersWithPrice(price, OrderType.Market, externalOrder);
                }

                //It is cheaper to perform following in separate handle without checking volatile field
                // also then we are sure that the field is not changed under our hands
                //if (price.SizeBaseAbsRemaining > 0 && _hasMarketOnImprovementOrders && this._priceBook.IsImprovement)
                //{
                //    externalOrder = MatchOrdersWithPrice(price, OrderType.MarketOnImprovement, externalOrder);
                //}

                //we were waiting for a price - so the order was first (=> liq maker)
                this.SubmitExternalOrder(externalOrder, FlowSide.LiquidityMaker);
            //TODO: might not be needed any more
            }
        }

        private void SubmitExternalOrder(IIntegratorOrderExternal externalOrder, FlowSide flowSide)
        {
            if(externalOrder == null)
                return;

            externalOrder.OrderRequestInfo.FlowSideAtSubmitTime = flowSide;

            IOrderFlowSession orderFlowSession = null;
            if (_orderFlowSessions != null && (int)externalOrder.Counterparty < _orderFlowSessions.Count)
            {
                orderFlowSession = _orderFlowSessions[(int)externalOrder.Counterparty];
            }

            if (orderFlowSession == null)
            {
                _logger.Log(LogLevel.Fatal,
                    "Couldn't find valid OrderFlow Session for counterparty {0}", externalOrder.Counterparty);
                return;
            }

            SubmissionResult submissionResult = orderFlowSession.SubmitOrder(externalOrder);
            if (submissionResult != SubmissionResult.Success)
            {
                _logger.Log(LogLevel.Error, "Couldn't submit external order [{0}]",
                            externalOrder.Identity);

                _priceBook.RemoveIdentical(externalOrder.OrderRequestInfo.IntegratorPriceIdentity);

                //prevent recursion by asynchrony
                TaskEx.StartNew(() => this.ExternalOrderFailedDuringSubmission(externalOrder, submissionResult));
            }
        }

        private void ProcessBookImprovement(PriceObjectInternal price)
        {
            //processing this in separatet handle - as only here we are sure that the change is uptick
            if (CheckIsPriceValid(price) && price.SizeBaseAbsRemaining > 0 && !_riskManager.IsTransmissionDisabled && _hasMarketOnImprovementOrders)
            {
                IIntegratorOrderExternal externalOrder = null;
                externalOrder = MatchOrdersWithPrice(price, OrderType.MarketOnImprovement, externalOrder);

                //we were waiting for a price - so the order was first (=> liq maker)
                this.SubmitExternalOrder(externalOrder, FlowSide.LiquidityMaker);
            }
        }

        private bool CheckIsPriceValid(PriceObjectInternal price)
        {
            if (price == null)
            {
                this._logger.Log(LogLevel.Error,
                                 "Order management unit ({0}:{1}) receiving null price",
                                 this._symbol, this._dealDirection);
                return false;
            }

            if (
                (price.Side == PriceSide.Ask && this._dealDirection == DealDirection.Sell)
                ||
                (price.Side == PriceSide.Bid && this._dealDirection == DealDirection.Buy)
                )
            {
                this._logger.Log(LogLevel.Error,
                                 "Order management unit ({0}:{1}) receiving price of mismatched side {2}",
                                 this._symbol, this._dealDirection, price.Side);
                return false;
            }

            if (price.Symbol != this._symbol)
            {
                this._logger.Log(LogLevel.Fatal,
                                 "Order management unit ({0}:{1}) receiving price of mismatched symbol {2}",
                                 this._symbol, this._dealDirection, price.Symbol);
                return false;
            }

            return true;
        }


        private bool TryCancelAllOrdersConditionaly(Func<IntegratorOrderInternal, bool> cancelCondition, List<IntegratorOrderInternal> internalOrdersList)
        {
            bool foundOne = false;

            //Beware! - .ToList() is very important here - as we need to put the items to different temporary structure
            //  during removing them from underlying structure (otherwise we would get exception that collection changed)
            foreach (
                IntegratorOrderInternal orderInternal in internalOrdersList.Where(cancelCondition).ToList())
            {
                this.TryCancelOrder(orderInternal, internalOrdersList);
                foundOne = true;
            }

            return foundOne;
        }

        private void TryCancelOrder(IntegratorOrderInternal orderToCancel, List<IntegratorOrderInternal> internalOrdersList)
        {
            ITakerUnicastInfoForwarder dispatchGateway = orderToCancel.DispatchGateway;

            if (orderToCancel.OrderStatus == ClientOrderStatus.RemovedFromIntegrator ||
                    orderToCancel.OrderStatus == ClientOrderStatus.RejectedByIntegrator ||
                    orderToCancel.OrderStatus == ClientOrderStatus.NotActiveInIntegrator)
            {
                this._logger.Log(LogLevel.Warn, "Attempt to cancel order [{0}] which is not in cancelable state", orderToCancel);
                dispatchGateway.OnIntegratorClientOrderUpdate(
                    BankPoolClientOrderUpdateInfo.CreateCancellFailedClientOrderUpdateInfo(orderToCancel, orderToCancel.SizeBaseAbsActive), orderToCancel.OrderRequestInfo.Symbol);
                return;
            }

            decimal cancelledAmount = this.CancelOrderPartiall(orderToCancel);
            this._logger.Log(LogLevel.Info, "Bankpool cancelling client order {0} ({1} cancelled, {2} pending)",
                orderToCancel.Identity, cancelledAmount, orderToCancel.PendingAmount);

            internalOrdersList.Remove(orderToCancel);
            if (internalOrdersList == this._limitOrders)
            {
                this.UpdateBestOrder();
            }
            else
            {
                //refresh internal flags
                this.RefreshFlagsUnlocked();
            }

            if (orderToCancel.PendingAmount > 0)
            {
                _pendingInactiveOrders.Add(orderToCancel);

                dispatchGateway.OnIntegratorClientOrderUpdate(
                    BankPoolClientOrderUpdateInfo.CreateCancellInProgressClientOrderUpdateInfo(orderToCancel,
                        orderToCancel.SizeBaseAbsActive + orderToCancel.PendingAmount, cancelledAmount),
                    orderToCancel.OrderRequestInfo.Symbol);
            }
            else
            {
                this.CancelOrderPostProcess(orderToCancel, cancelledAmount);
            }
        }

        private void TryCancelOrder(IntegratorOrderInternal orderToCancel, bool isInInternalCollection)
        {
            if (isInInternalCollection)
            {
                int removeCnt = 0;

                if (this._limitOrders.Remove(orderToCancel))
                    removeCnt++;
                if(this._marketOrders.Remove(orderToCancel))
                    removeCnt++;
                if(this._marketIfImprovementOrders.Remove(orderToCancel))
                    removeCnt++;
                if(this._pendingInactiveOrders.Remove(orderToCancel))
                    removeCnt++;

                if(removeCnt != 1)
                    this._logger.Log(LogLevel.Fatal, "{0} order manager - unexpected removal count ({1} expected 1) for order being cancelled. {2}",
                        this._symbol, removeCnt, orderToCancel);

                this.UpdateBestOrder();
                this.RefreshFlagsUnlocked();
            }

            decimal cancelledAmount = this.CancelOrderPartiall(orderToCancel);
            this.CancelOrderPostProcess(orderToCancel, cancelledAmount);
        }

        private decimal CancelOrderPartiall(IntegratorOrderInternal orderToCancel)
        {
            orderToCancel.IsCancellationRequested = true;
            orderToCancel.NotToBeFilledAmount += orderToCancel.SizeBaseAbsActive;
            decimal substracted = 0, remainig = 0;
            orderToCancel.TrySubstractAmount(orderToCancel.SizeBaseAbsActive, ref substracted, ref remainig);
            this._riskManager.CancelInternalOrder(orderToCancel, substracted);
            return substracted;
        }

        private void CancelOrderPostProcess(IntegratorOrderInternal orderToCancel, decimal currentlyCancelledAmount)
        {
            if (KeepClosedOrders)
            {
                _closedOrders.Add(orderToCancel);
            }
            this.InvokeOrderClosing(orderToCancel);
            orderToCancel.OrderStatus = ClientOrderStatus.RemovedFromIntegrator;
            orderToCancel.DispatchGateway.OnIntegratorClientOrderUpdate(
               BankPoolClientOrderUpdateInfo.CreateCancelledClientOrderUpdateInfo(orderToCancel, currentlyCancelledAmount), orderToCancel.OrderRequestInfo.Symbol);
        }

        private bool CancelMatchingOrders(Func<IntegratorOrderInternal, bool> cancelCondition, bool cancelFirstMatchOnly)
        {
            lock (_matchingLocker)
            {
                if (TryCancelAllOrdersConditionaly(cancelCondition, _limitOrders) &&
                    cancelFirstMatchOnly)
                {
                    return true;
                }

                if (TryCancelAllOrdersConditionaly(cancelCondition, _marketOrders) &&
                    cancelFirstMatchOnly)
                {
                    return true;
                }

                if (TryCancelAllOrdersConditionaly(cancelCondition, _marketIfImprovementOrders) &&
                    cancelFirstMatchOnly)
                {
                    return true;
                }

                if (TryCancelAllOrdersConditionaly(cancelCondition, _pendingInactiveOrders) &&
                    cancelFirstMatchOnly)
                {
                    return true;
                }
            }

            //for group cancellations we are successful even if we haven't found anything
            return !cancelFirstMatchOnly;
        }

        public IntegratorRequestResult CancelOrder(CancelOrderRequestInfo cancelOrderRequestInfo, ITakerUnicastInfoForwarder dispatchGateway)
        {
            bool found = this.CancelMatchingOrders(ord => ord.ClientOrderIdentity.Equals(cancelOrderRequestInfo.ClientOrderIdentity), true);

            if (found)
            {
                return IntegratorRequestResult.GetSuccessResult();
            }
            else
            {

                lock (_matchingLocker)
                {
                    if (
                        TryCancelAllOrdersConditionaly(
                            ord => ord.ClientOrderIdentity.Equals(cancelOrderRequestInfo.ClientOrderIdentity),
                            _closedOrders))
                    {
                        return IntegratorRequestResult.GetSuccessResult();
                    }
                }

                string error = string.Format("Unknown client order [{0}], cannot perform cancel action",
                                             cancelOrderRequestInfo.ClientOrderIdentity);

                this._logger.Log(LogLevel.Error, error);

                dispatchGateway.OnIntegratorClientOrderUpdate(
                    new BankPoolClientOrderUpdateInfo(null, cancelOrderRequestInfo.ClientOrderIdentity,
                                                      cancelOrderRequestInfo.ClientIdentity,
                                                      ClientOrderStatus.NotActiveInIntegrator,
                                                      ClientOrderCancelRequestStatus
                                                          .CancelRequestFailed, 0m, 0m, 0m, "Order is unknown", null, null), Common.Symbol.NULL);

                return IntegratorRequestResult.CreateFailedResult(error);
            }
        }

        public int ActiveOrdersCount
        {
            get
            {
                return _limitOrders.Count + _marketOrders.Count + _marketIfImprovementOrders.Count +
                       _pendingInactiveOrders.Count;
            }
        }

        public bool CancelAllOrders()
        {
            return this.CancelMatchingOrders(ord => true, false);
        }

        public bool CancelAllOrdersForDispatchGateway(ITakerUnicastInfoForwarder dispatchGateway)
        {
            return this.CancelMatchingOrders(ord => ord.DispatchGateway.Equals(dispatchGateway), false);
        }

        public bool CancelAllOrdersForClient(string clientIdentity, ITakerUnicastInfoForwarder dispatchGateway)
        {
            return this.CancelMatchingOrders(ord => ord.ClientIdentity.Equals(clientIdentity), false);
        }

        public IntegratorRequestResult RegisterOrder(IBankPoolClientOrder clientOrder, ITakerUnicastInfoForwarder dispatchGateway)
        {
            if (dispatchGateway == null)
            {
                _logger.Log(LogLevel.Fatal, "DispatchGateway is null - cannot register any order");
                return IntegratorRequestResult.CreateFailedResult("DispatchGateway is null - cannot register any order");
            }

            if (clientOrder == null)
            {
                _logger.Log(LogLevel.Fatal, "ClientOrder is null - cannot register it");
                return IntegratorRequestResult.CreateFailedResult("ClientOrder is null - cannot register it");;
            }

            var riskResult = this._riskManager.AddInternalOrderAndCheckIfAllowed(clientOrder);
            if (riskResult != RiskManagementCheckResult.Accepted)
            {
                _logger.Log(LogLevel.Error, "Attempt to register client order [{0}], but risk manager disallowed it ({1}). Throwing the order away", clientOrder.ClientOrderIdentity, riskResult);
                dispatchGateway.OnIntegratorClientOrderUpdate(
                    BankPoolClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                        string.Format("Disallowed by risk management ({0})", riskResult)),
                    clientOrder.OrderRequestInfo.Symbol);
                this.InvokeOrderClosing(clientOrder);
                return IntegratorRequestResult.CreateFailedResult("Risk manager disallowed the order");
            }

            _logger.Log(LogLevel.Info, "Registering order: {0}", clientOrder);

            IntegratorOrderInternal orderInternal;

            if (clientOrder.OrderRequestInfo.OrderType == OrderType.MarketOnImprovement ||
                clientOrder.OrderRequestInfo.OrderType == OrderType.LimitTillTimeout)
                orderInternal = new IntegratorOrderInternalWithTimeout();
            else
                orderInternal = new IntegratorOrderInternal();

            {
                orderInternal.ClientOrderIdentity = clientOrder.ClientOrderIdentity;
                orderInternal.ClientIdentity = clientOrder.ClientIdentity;
                orderInternal.OrderStatus = clientOrder.OrderStatus;
                orderInternal.SizeBaseAbsFilled = clientOrder.SizeBaseAbsFilled;
                orderInternal.SizeBaseAbsActive = clientOrder.SizeBaseAbsActive;
                orderInternal.BankPoolClientOrderRequestInfo = clientOrder.BankPoolClientOrderRequestInfo;
                orderInternal.CreatedUtc = clientOrder.CreatedUtc;
                orderInternal.DispatchGateway = dispatchGateway;
                orderInternal.IntegratedTradingSystemIdentification = clientOrder.IntegratedTradingSystemIdentification;
            }

            if (clientOrder is GliderBankPoolClientOrder)
            {
                orderInternal.GlidingInfoBag = (clientOrder as GliderBankPoolClientOrder).GlidingInfoBag;
            }
            

            //IntegratorOrderInternal orderInternal = new IntegratorOrderInternal()
            //{
            //    ClientOrderIdentity = clientOrder.ClientOrderIdentity,
            //    ClientIdentity = clientOrder.ClientIdentity,
            //    OrderStatus = clientOrder.OrderStatus,
            //    SizeBaseAbsFilled = clientOrder.SizeBaseAbsFilled,
            //    SizeBaseAbsActive = clientOrder.SizeBaseAbsActive,
            //    BankPoolClientOrderRequestInfo = clientOrder.BankPoolClientOrderRequestInfo,
            //    CreatedUtc = clientOrder.CreatedUtc,
            //    DispatchGateway = dispatchGateway,
            //    ParentOrder = parentOrder
            //};

            if (!CheckIsClientOrderValid(clientOrder))
            {
                _logger.Log(LogLevel.Trace, "Rejecting client order [{0}]", clientOrder.ClientOrderIdentity);
                dispatchGateway.OnIntegratorClientOrderUpdate(BankPoolClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(orderInternal, "Invalid Client Order"), clientOrder.OrderRequestInfo.Symbol);
                this.InvokeOrderClosing(clientOrder);
                return IntegratorRequestResult.CreateFailedResult("Order is invalid");
            }
            else
            {
                orderInternal.OrderStatus = ClientOrderStatus.OpenInIntegrator;
                dispatchGateway.OnIntegratorClientOrderUpdate(
                    BankPoolClientOrderUpdateInfo.CreateOpenClientOrderUpdateInfo(orderInternal), clientOrder.OrderRequestInfo.Symbol);
            }

            if (orderInternal.OrderRequestInfo.OrderType == OrderType.MarketOnImprovement ||
                orderInternal.OrderRequestInfo.OrderType == OrderType.LimitTillTimeout)
            {
                if (orderInternal.BankPoolClientOrderRequestInfo.TimeoutTillFlippingToMarket <= TimeSpan.Zero)
                {
                    this._logger.Log(LogLevel.Info,
                        "Registering {2} order [{0}] with timeout [{1}] - immediately changing it to MarketOrder",
                        orderInternal.ClientIdentity,
                        orderInternal.BankPoolClientOrderRequestInfo.TimeoutTillFlippingToMarket,
                        orderInternal.OrderRequestInfo.OrderType);
                    orderInternal.BankPoolClientOrderRequestInfo.ResetTypeToMarket();
                }
                else
                {
                    _logger.Log(LogLevel.Info, "Registering {2}: {0}, timeout: {1}",
                        orderInternal,
                        orderInternal.BankPoolClientOrderRequestInfo.TimeoutTillFlippingToMarket,
                        orderInternal.OrderRequestInfo.OrderType);

                    (orderInternal as IntegratorOrderInternalWithTimeout).SetTimeOut(
                        orderInternal.BankPoolClientOrderRequestInfo.TimeoutTillFlippingToMarket,
                        OnOrderWithTimeoutExpired);
                }
            }

            this.RegisterOrderInternal(orderInternal, false);
            return IntegratorRequestResult.GetSuccessResult();
        }

        private bool IsPriceOnOrderBlacklist(PriceObjectInternal price, IntegratorOrderInternal orderInternal)
        {
            bool isBlacklisted =
                (
                orderInternal.BankPoolClientOrderRequestInfo.CounterpartiesBlacklist != null &&
                orderInternal.BankPoolClientOrderRequestInfo.CounterpartiesBlacklist.Contains(
                    price.Counterparty)
                )
                ||
                (
                orderInternal.BankPoolClientOrderRequestInfo.CounterpartiesWhitelist != null &&
                !orderInternal.BankPoolClientOrderRequestInfo.CounterpartiesWhitelist.Contains(
                price.Counterparty)
                );

            if (isBlacklisted)
            {
                this._logger.Log(LogLevel.Info,
                                 "Price {0} cannot be used for satisfying order {1} as it is blacklisted (or not whitelisted) by the order",
                                 price, orderInternal);
            }
            else if (HighResolutionDateTime.UtcNow - price.IntegratorReceivedTimeUtc >
                     orderInternal.OrderRequestInfo.MaximumAcceptedPriceAge)
            {
                this._logger.Log(LogLevel.Info,
                                 "Price {0} cannot be used for satisfying order {1} as it is older then threshold allowed by the order {2}",
                                 price, orderInternal, orderInternal.OrderRequestInfo.MaximumAcceptedPriceAge);

                isBlacklisted = true;
            }

            if (isBlacklisted && this._riskManager.IsGoFlatOnlyEnabled)
            {
                this._logger.Log(LogLevel.Info, "Reverting the blacklist flag for order as we are in go-flat-only mode");
                isBlacklisted = false;
            }

            if (!this._riskManager.IsTradeRequestLikelyToPass(orderInternal, price))
            {
                this._logger.Log(LogLevel.Info, "Attempt to match disallowed as trade request is unlikely to pass (NOP check)");
                isBlacklisted = true;
            }

            return isBlacklisted;
        }

        private void MatchInternalOrderByTunnelBreakingStrategy(IntegratorOrderInternal orderInternal,
                                                                List<IIntegratorOrderExternal> externalOrders,
            PriceObjectInternal bestPrice)
        {
            PriceObjectInternal[] prices = _priceBook.GetSortedClone();
            DateTime referenceTime = HighResolutionDateTime.UtcNow;

            this._logger.Log(LogLevel.Info, "Trying to match internal order by TunnelBreaking strategy. Order: {0}", orderInternal);

            try
            {
                foreach (
                    Tuple<PriceObjectInternal, decimal> priceWithRankTuple in
                        this._consecutiveRejectionsStopStrategyClientOrdersMatchingEvaluator.SortPricesByRankingAsc(prices, bestPrice,
                                                                                                 referenceTime))
                {
                    PriceObjectInternal price = priceWithRankTuple.Item1;
                    decimal rank = priceWithRankTuple.Item2;

                    this._logger.Log(LogLevel.Info, "Trying price with rank: {0}, price: {1}", rank, price);

                    if (!TryMatchPriceWithOrderAndContinue(price, orderInternal, externalOrders))
                        break;
                }
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e,
                                          "Unhandled exception during tunnel-breaking matching of order {0}",
                                          orderInternal);
            }

            Array.ForEach(prices, price => price.Release());
        }

        private bool TryMatchPriceWithOrderAndContinue(PriceObjectInternal price, IntegratorOrderInternal orderInternal,
                                                       List<IIntegratorOrderExternal> externalOrders)
        {
            //We match MarketOnImprovement as ordinary market in case this is tunnel breaking
            // MarketOnImprovement shouldn't get here in other way
            if (
                !PriceObjectInternal.IsNullPrice(price)
                &&
                (orderInternal.OrderRequestInfo.OrderType == OrderType.Market || orderInternal.OrderRequestInfo.OrderType == OrderType.MarketPreferLmax
                 ||
                MeetsMarketOnImprovementMatchingCriteria(orderInternal)
                 ||
                 _meetsPriceCriteriaFunc(orderInternal, price))
                )
            {
                if (!IsPriceOnOrderBlacklist(price, orderInternal))
                {
                    this._logger.Log(LogLevel.Info,
                            "Price {0} (from priceObject: {1}) found matchable with order {2}",
                            price.Price, price, orderInternal.ClientOrderIdentity);

                    externalOrders.Add(CreateOrderFromMatchingPrice(orderInternal, price, null));
                    UpdatePriceBookIfNeeded(price);
                    if (orderInternal.SizeBaseAbsActive <= 0)
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        private bool MeetsMarketOnImprovementMatchingCriteria(IntegratorOrderInternal orderInternal)
        {
            return orderInternal.OrderRequestInfo.OrderType == OrderType.MarketOnImprovement
                   &&
                   ////Do not match on very first registration of the order
                   //orderInternal.RejectionCount > 0
                   //&&
                   this._priceBook.IsImprovement;
        }

        private static bool LmaxPricesSelector(PriceObjectInternal price)
        {
            return price.Counterparty.TradingTargetType == TradingTargetType.LMAX;
        }

        private void RegisterOrderInternal(IntegratorOrderInternal orderInternal, bool isOrderAlreadyInInternalCollection)
        {
            List<IIntegratorOrderExternal> externalOrders = null;

            //this lock might not be even needed - but then we'd need thread safe decrement in Price impl.
            lock (_matchingLocker)
            {
                if (orderInternal.IsCancellationRequested)
                {
                    this.TryCancelOrder(orderInternal, isOrderAlreadyInInternalCollection);
                    return;
                }

                //Try do the matching
                PriceObjectInternal bestPriceLocal = _priceBook.MaxNodeInternal;


                if (!this._riskManager.IsTransmissionDisabled &&
                    this._consecutiveRejectionsStopStrategyClientOrdersMatchingEvaluator != null &&
                    this._consecutiveRejectionsStopStrategyClientOrdersMatchingEvaluator.NeedsTunnelBreakingMatching(orderInternal))
                {
                    externalOrders = new List<IIntegratorOrderExternal>();
                    MatchInternalOrderByTunnelBreakingStrategy(orderInternal, externalOrders, bestPriceLocal);
                }
                else if (
                    this._ordersMatchingStrategy == MarketableClientOrdersMatchingStrategy.ImmediateMatching
                    &&
                    !this._riskManager.IsTransmissionDisabled
                    &&
                    !PriceObjectInternal.IsNullPrice(bestPriceLocal)
                    &&
                    (
                    orderInternal.OrderRequestInfo.OrderType == OrderType.Market || orderInternal.OrderRequestInfo.OrderType == OrderType.MarketPreferLmax
                    ||
                    MeetsMarketOnImprovementMatchingCriteria(orderInternal)
                    ||
                    ((orderInternal.OrderRequestInfo.OrderType == OrderType.Limit || orderInternal.OrderRequestInfo.OrderType == OrderType.LimitTillTimeout) 
                        && _meetsPriceCriteriaFunc(orderInternal, bestPriceLocal))
                    )
                   )
                {
                    externalOrders = new List<IIntegratorOrderExternal>();

                    //Special case for PreferLmax
                    if (orderInternal.OrderRequestInfo.OrderType == OrderType.MarketPreferLmax)
                    {
                        //Make sure we execute special case just once
                        //NOTE - we actually want the same behavior even going forward
                        //orderInternal.BankPoolClientOrderRequestInfo.ResetTypeToMarket();

                        //if current best price is OK - use it
                        if(bestPriceLocal.Counterparty.TradingTargetType == TradingTargetType.LMAX)
                            TryMatchPriceWithOrderAndContinue(bestPriceLocal, orderInternal, externalOrders);

                        if (orderInternal.SizeBaseAbsActive > 0)
                        {
                            //if needed, select all LMAX prices and use them
                            // if that will not be enaugh, we will continue with remaining prices
                            PriceObjectInternal[] prices = _priceBook.GetSortedClone(LmaxPricesSelector);

                            foreach (PriceObjectInternal price in prices)
                            {
                                if (!TryMatchPriceWithOrderAndContinue(price, orderInternal, externalOrders))
                                    break;
                            }

                            Array.ForEach(prices, price => price.Release());
                        }
                    }
                    else
                    {
                        TryMatchPriceWithOrderAndContinue(bestPriceLocal, orderInternal, externalOrders);
                    }

                    //Following is specific for new orders only, as for existing orders we already inspected all previous
                    // quote book prices
                    if (orderInternal.SizeBaseAbsActive > 0 && orderInternal.OrderRequestInfo.OrderType != OrderType.MarketOnImprovement)
                    {
                        PriceObjectInternal[] prices = _priceBook.GetSortedClone();

                        foreach (PriceObjectInternal price in prices)
                        {
                            if (!TryMatchPriceWithOrderAndContinue(price, orderInternal, externalOrders))
                                break;
                        }

                        Array.ForEach(prices, price => price.Release());
                    }
                }

                //If matching dind't happen or did not fully satisfy the order - add to an apropriate list
                if (orderInternal.SizeBaseAbsActive > 0)
                {
                    if (!isOrderAlreadyInInternalCollection)
                    {
                        if (orderInternal.OrderRequestInfo.OrderType == OrderType.Limit || orderInternal.OrderRequestInfo.OrderType == OrderType.LimitTillTimeout)
                        {
                            _limitOrders.Add(orderInternal);
                            _logger.Log(LogLevel.Info, "Adding order to limit list: {0}", orderInternal);

                            if (this._orderRankComparer.Compare(orderInternal, _bestOrder) > 0)
                            {
                                _bestOrder = orderInternal;
                            }
                        }

                        else if (orderInternal.OrderRequestInfo.OrderType == OrderType.MarketOnImprovement)
                        {
                            _logger.Log(LogLevel.Info, "Adding order to marketOnImprovemebt list: {0}", orderInternal);
                            _marketIfImprovementOrders.Add(orderInternal);
                        }

                        else if (orderInternal.OrderRequestInfo.OrderType == OrderType.Market || orderInternal.OrderRequestInfo.OrderType == OrderType.MarketPreferLmax)
                        {
                            _logger.Log(LogLevel.Info, "Adding order to market list: {0}", orderInternal);
                            _marketOrders.Add(orderInternal);
                        }

                        else
                        {
                            this._logger.Log(LogLevel.Fatal, "Order [{0}] with remaining size {1} was not added to any internal orders list",
                                orderInternal, orderInternal.SizeBaseAbsActive);
                        }

                        //refresh internal flags
                        this.RefreshFlagsUnlocked();
                    }
                    else
                    {
                        _logger.Log(LogLevel.Trace, "Client order [{0}] was not satisfied but it is already in internal queues", orderInternal.ClientOrderIdentity);
                    }
                }
                else
                {
                    _logger.Log(LogLevel.Trace, "Client order [{0}] was satisfied without putting to internal queues", orderInternal.ClientOrderIdentity);
                }

                bestPriceLocal.Release();
            }

            //If we created any external orders - send them
            if (externalOrders != null)
            {
                foreach (
                    IIntegratorOrderExternal integratorOrderExternal in externalOrders.Where(ord => ord != null))
                {
                    //we aggress on existing price
                    this.SubmitExternalOrder(integratorOrderExternal, FlowSide.LiquidityTaker);
                }
            }
        }

        private void OnOrderWithTimeoutExpired(IntegratorOrderInternalWithTimeout order)
        {
            lock (_matchingLocker)
            {
                var orderList = this.GetOrdersList(order.OrderRequestInfo.OrderType);

                //reset the type anyway - as order can be currently in external matching
                order.BankPoolClientOrderRequestInfo.ResetTypeToMarket();

                this._logger.Log(LogLevel.Info, "Reseting order [{0}] to Market as timeout expired", order.ClientOrderIdentity);

                if (orderList.Remove(order))
                {
                    if (order.SizeBaseAbsActive > 0)
                    {
                        //however re-register it onlyif it is waiting
                        this.RegisterOrderInternal(order, false);
                    }
                }
                else
                {
                    this._logger.Log(LogLevel.Info,
                                     "Order [{0}] timeout expired but it was missing from MarketOnImprovement internal list (probably in external matching or already cleared)",
                                     order.ClientOrderIdentity);
                }
            }
        }

        private IIntegratorOrderExternal CreateOrderFromMatchingPrice(IntegratorOrderInternal orderInternal, PriceObjectInternal price, IIntegratorOrderExternal existingExternalOrder)
        {
            IIntegratorOrderExternal externalOrder = existingExternalOrder;

            PriceObjectInternal priceInternal = price as PriceObjectInternal;
            if (priceInternal == null)
            {
                this._logger.Log(LogLevel.Fatal, "Experiencing unexpected type of PriceObject: {0}", price);
                return externalOrder;
            }

            this.CheckPriceWithRiskManagement(priceInternal);

            decimal originalOrderSize = orderInternal.SizeBaseAbsActive < price.SizeBaseAbsRemaining
                                                ? orderInternal.SizeBaseAbsActive
                                                : price.SizeBaseAbsRemaining;

            //make sure that we create order only for supported granularity
            decimal orderSize = SymbolsInfoUtils.RoundToIncrement(
                RoundingKind.AlwaysToZero,
                DestinationSizeGranularityUtils.GetCounterpartySizeGranularityIncrement(price.Counterparty),
                originalOrderSize);

            if (orderSize <= 0)
            {
                if(originalOrderSize <= 0)
                    this._logger.Log(LogLevel.Warn, "Requesting to match order with price where either price size or order ramining amount is 0");
                else
                    this._logger.Log(LogLevel.Info, "Requesting to match order witch price from counterparty that has minimum granularity higher than the order size");
                return externalOrder;
            }

            decimal substractedSizeBaseAbs = 0;
            decimal remaining = 0;
            if (orderInternal.TrySubstractAmount(orderSize, ref substractedSizeBaseAbs, ref remaining))
            {
                bool substractedAmountAddedToExternalOrder = false;

                OrderRequestInfo orderRequestInfo = OrderRequestInfo.CreateQuoted(substractedSizeBaseAbs,
                    orderInternal.OrderRequestInfo.RequestedPrice ?? price.Price, this._dealDirection, price, orderInternal.IsRiskRemovingOrder);

                if (orderRequestInfo == OrderRequestInfo.INVALID_ORDER_REQUEST)
                {
                    _logger.Log(LogLevel.Error, "Couldn't create valid order request for client order {0}",
                                orderInternal.ClientOrderIdentity);
                    //nothing follows - order will be stored and it might be matched later
                }
                else
                {
                    IOrderFlowSession orderFlowSession = null;
                    if (_orderFlowSessions != null && (int)price.Counterparty < _orderFlowSessions.Count)
                    {
                        orderFlowSession = _orderFlowSessions[(int)price.Counterparty];
                    }

                    if (orderFlowSession == null)
                    {
                        _logger.Log(LogLevel.Error,
                                    "Couldn't find valid OrderFlow Session for price [{0}] with counterpart {1}",
                                    price, price.Counterparty);
                        //nothing follows - order will be stored and it might be matched later
                    }
                    else
                    {
                        if (existingExternalOrder == null)
                        {
                            string errorMessage;
                            externalOrder = orderFlowSession.GetNewExternalOrder(orderRequestInfo, price.Counterparty,
                                out errorMessage);
                            if (externalOrder != null)
                            {
                                externalOrder.OnOrderChanged += ExternalOrderChanged;
                            }
                        }
                        else
                        {

                            if (existingExternalOrder.OrderRequestInfo.TryAdd(orderRequestInfo))
                            {
                                externalOrder = existingExternalOrder;
                            }
                            else
                            {
                                this._logger.Log(LogLevel.Error,
                                                 "Failed to add orderRequest [{0}] to external order [{1}] with orderRequest [{2}]",
                                                 orderRequestInfo, existingExternalOrder.Identity,
                                                 existingExternalOrder.OrderRequestInfo);
                            }
                        }

                        if (externalOrder != null)
                        {
                            if (!priceInternal.SubstractSizeBaseAbs(substractedSizeBaseAbs))
                                _logger.Log(LogLevel.Error, "Couldn't substract {0} from price size {1} in price object {3}",
                                            substractedSizeBaseAbs, price.SizeBaseAbsRemaining, price);

                            this._logger.Log(LogLevel.Info, "Created/Updated external order [{0}] for internal order [{1}] (for size {2})", externalOrder.Identity, orderInternal.ClientOrderIdentity, substractedSizeBaseAbs);

                            //And add the order mappings to the lookup
                            List<IntegratorOrderInternal> internalOrders;
                            if (!_externalOrdersMap.TryGetValue(externalOrder, out internalOrders))
                            {
                                _externalOrdersMap.Add(externalOrder, internalOrders = new List<IntegratorOrderInternal>());
                            }
                            internalOrders.Add(orderInternal);

                            orderInternal.AddExternalPartition(externalOrder, substractedSizeBaseAbs);
                            substractedAmountAddedToExternalOrder = true;

                            if (orderInternal.SizeBaseAbsActive <= 0)
                            {
                                if (orderInternal.SizeBaseAbsActive < 0)
                                    this._logger.Log(LogLevel.Error, "Active amount of order [{0}] is {1}",
                                                     orderInternal.ClientOrderIdentity, orderInternal.SizeBaseAbsActive);

                                this._logger.Log(LogLevel.Info, "Internal order [{0}] now doesn't have any more active nonmatched amount", orderInternal.ClientOrderIdentity);
                                _pendingInactiveOrders.Add(orderInternal);
                            }
                        }
                    }
                }

                if (!substractedAmountAddedToExternalOrder)
                {
                    this._logger.Log(LogLevel.Error, "Substracted {0} from internal order {1}, but external order wasn't update. Adding the amout back", substractedSizeBaseAbs, orderInternal.ClientOrderIdentity);
                    orderInternal.SizeBaseAbsActive += substractedSizeBaseAbs;
                }
            }
            else
            {
                this._logger.Log(LogLevel.Error,
                                 "Could substract amount {0} or it's part from internal order {1}. Active amount: {2}",
                                 orderSize, orderInternal.ClientOrderIdentity, orderInternal.SizeBaseAbsActive);
            }

            return externalOrder;
        }

        private void UpdatePriceBookIfNeeded(PriceObjectInternal price)
        {
            if (price.SizeBaseAbsRemaining <= 0)
            {
                if (price.SizeBaseAbsRemaining < 0) this._logger.Log(LogLevel.Error, "One price of price [{0}] going below zero size", price);
                _priceBook.RemoveIdentical(price);
            }
            else
            {
                _priceBook.ResortIdentical(price);
            }
        }

        private bool CheckIsClientOrderValid(IClientOrder clientOrder)
        {
            lock (_matchingLocker)
            {
                if (!(clientOrder is BankPoolClientOrder))
                {
                    _logger.Log(LogLevel.Warn,
                                "Client order {0} is of a mismatched type - expected BankPoolClientOrder",
                                clientOrder);
                    return false;
                }

                if (
                    _marketOrders.Any(ord => ord.ClientOrderIdentity.Equals(clientOrder.ClientOrderIdentity))
                    ||
                    _marketIfImprovementOrders.Any(ord => ord.ClientOrderIdentity.Equals(clientOrder.ClientOrderIdentity))
                    ||
                    _limitOrders.Any(ord => ord.ClientOrderIdentity.Equals(clientOrder.ClientOrderIdentity))
                    ||
                    _closedOrders.Any(ord => ord.ClientOrderIdentity.Equals(clientOrder.ClientOrderIdentity))
                    ||
                    _pendingInactiveOrders.Any(ord => ord.ClientOrderIdentity.Equals(clientOrder.ClientOrderIdentity))
                    )
                {
                    _logger.Log(LogLevel.Warn,
                                "There is already order with identity [{0}], refusing to register another one",
                                clientOrder.ClientOrderIdentity);
                    return false;
                }
            }

            if (clientOrder.OrderStatus != ClientOrderStatus.NewInIntegrator)
            {
                _logger.Log(LogLevel.Warn, "Cannot register order {0} which is already in state {1}", clientOrder.ClientOrderIdentity, clientOrder.OrderStatus);
                return false;
            }

            if (clientOrder.SizeBaseAbsFilled != 0)
            {
                _logger.Log(LogLevel.Warn, "Cannot register order {0} which has filled amount of {1}", clientOrder.ClientOrderIdentity, clientOrder.SizeBaseAbsFilled);
                return false;
            }

            if (clientOrder.SizeBaseAbsActive != clientOrder.OrderRequestInfo.SizeBaseAbsInitial)
            {
                _logger.Log(LogLevel.Warn,
                            "Cannot register order {0} which has Active amount of {1} while {2} was requested",
                            clientOrder.ClientOrderIdentity, clientOrder.SizeBaseAbsActive, clientOrder.OrderRequestInfo.SizeBaseAbsInitial);
                return false;
            }

            if (clientOrder.OrderRequestInfo.OrderType != OrderType.Limit &&
                clientOrder.OrderRequestInfo.OrderType != OrderType.Market &&
                clientOrder.OrderRequestInfo.OrderType != OrderType.MarketOnImprovement &&
                clientOrder.OrderRequestInfo.OrderType != OrderType.MarketPreferLmax &&
                clientOrder.OrderRequestInfo.OrderType != OrderType.LimitTillTimeout)
            {
                _logger.Log(LogLevel.Warn, "Cannot register order {0}, OrderType {1} is not supported",
                    clientOrder.ClientOrderIdentity, clientOrder.OrderRequestInfo.OrderType);
                return false;
            }

            if (clientOrder.OrderRequestInfo.IntegratorDealDirection != this._dealDirection)
            {
                _logger.Log(LogLevel.Error, "Client order [{0}] with direction {1} send to incorrect order management unit", clientOrder.ClientOrderIdentity, clientOrder.OrderRequestInfo.IntegratorDealDirection);
                return false;
            }

            if (clientOrder.OrderRequestInfo.Symbol != this._symbol)
            {
                _logger.Log(LogLevel.Error, "Client order [{0}] with symbol {1} send to incorrect order management unit", clientOrder.ClientOrderIdentity, clientOrder.OrderRequestInfo.Symbol);
                return false;
            }

            return true;
        }

        private void MoveFailedOrders(List<IntegratorOrderInternal> internalOrders, IIntegratorOrderExternal externalOrder, 
            bool isRejection, string rejectionReason, decimal totalRejectedAmount, OrderChangeEventArgs orderChangeEventArgs, bool rejectClientOrders)
        {
            foreach (IntegratorOrderInternal integratorOrderInternal in internalOrders)
            {
                bool isOrderAlreadyInInternalCollection = true;

                decimal rejectedAmount;

                //atomically remove from orders and add the amount
                // keep the lock until the item is rematched or removed from list - as cancel request can come in the meantime
                lock (_matchingLocker)
                {
                    rejectedAmount = integratorOrderInternal.ExecutedPartitionsMap[externalOrder];

                    //this internal order was already filled/rejected by different ExecutionReport message for same External order
                    if (rejectedAmount <= 0)
                        continue;

                    rejectedAmount = Math.Min(rejectedAmount, totalRejectedAmount);

                    //first check if order WAS inactive-pending, as in that case we need to remove it from pending list and mark for adding to active lists
                    if (integratorOrderInternal.SizeBaseAbsActive <= 0)
                    {
                        isOrderAlreadyInInternalCollection = false;

                        //this order should be in pending inactive
                        if (!_pendingInactiveOrders.Remove(integratorOrderInternal))
                        {
                            this._logger.Log(LogLevel.Error,
                                             "Receiving execution change for internal order [{0}] that has no active size but is missing from pending orders. Skipping this internal order",
                                             integratorOrderInternal.ClientOrderIdentity);
                            continue;
                        }
                    }

                    //then we can adjust active amount
                    integratorOrderInternal.SizeBaseAbsActive += rejectedAmount;
                    integratorOrderInternal.ExecutedPartitionsMap[externalOrder] -= rejectedAmount;

                    int submissionFailuresCount = 0;

                    if (isRejection)
                    {
                        integratorOrderInternal.DispatchGateway.OnCounterpartyRejectedOrder(
                            new CounterpartyRejectionInfo(integratorOrderInternal.ClientOrderIdentity,
                                integratorOrderInternal.ClientIdentity,
                                string.Empty,
                                externalOrder.UniqueInternalIdentity,
                                externalOrder.Identity, 
                                rejectedAmount,
                                externalOrder.OrderRequestInfo.RequestedPrice,
                                integratorOrderInternal.OrderRequestInfo.IntegratorDealDirection,
                                externalOrder.OrderRequestInfo.Symbol,
                                externalOrder.IntegratorSentTimeUtc,
                                externalOrder.IntegratorReceivedResultTimeUtc,
                                externalOrder.CounterpartySentResultTimeUtc,
                                externalOrder.Counterparty,
                                rejectionReason, null,
                                orderChangeEventArgs == null
                                    ? RejectionType.OrderReject
                                    : orderChangeEventArgs.RejectionInfo.RejectionType,
                                TradingTargetType.BankPool,
                                integratorOrderInternal.IntegratedTradingSystemIdentification,
                                orderChangeEventArgs.RejectionInfo.CounterpartyClientId,
                                orderChangeEventArgs.ExecutionInfo != null ? orderChangeEventArgs.ExecutionInfo.CounterpartyTransactionId : null));

                        integratorOrderInternal.MarkRejected(externalOrder.IntegratorSentTimeUtc);
                    }
                    else
                    {
                        submissionFailuresCount = integratorOrderInternal.MarkSubmissionFailed();
                    }

                    if ((rejectClientOrders && submissionFailuresCount == 3) || submissionFailuresCount > MAX_SUBMISSION_FAIL_COUNT)
                    {
                        string error =
                            string.Format(
                                "External order [{0}] didn't pass the risk management checks or failed to many times. (submissionFailureCount: {1}). This may lead to OPEN POSITION THAT MIGHT NEED TO BE CLOSED MANUALLY",
                                externalOrder.Identity, submissionFailuresCount);

                        this._logger.Log(LogLevel.Fatal, error);

                        integratorOrderInternal.NotToBeFilledAmount += rejectedAmount;
                        decimal substracted = 0, remainig = 0;
                        integratorOrderInternal.TrySubstractAmount(rejectedAmount, ref substracted, ref remainig);
                        this._riskManager.CancelInternalOrder(integratorOrderInternal, substracted);

                        if (!integratorOrderInternal.IsActiveOrPending)
                        {
                            if (isOrderAlreadyInInternalCollection)
                            {
                                this.AttemptToRemoveOrderFromActiveLists(integratorOrderInternal);
                            }
                            if (KeepClosedOrders)
                            {
                                _closedOrders.Add(integratorOrderInternal);
                            }
                            this.InvokeOrderClosing(integratorOrderInternal);
                            integratorOrderInternal.OrderStatus = ClientOrderStatus.RemovedFromIntegrator;
                            integratorOrderInternal.DispatchGateway.OnIntegratorClientOrderUpdate(
                                new BankPoolClientOrderUpdateInfo(integratorOrderInternal,
                                    ClientOrderCancelRequestStatus.CancelNotRequested, 0m,
                                    substracted,
                                    error), integratorOrderInternal.OrderRequestInfo.Symbol);
                        }
                        this._riskManager.TurnOnGoFlatOnly(error);
                    }
                    else
                    {
                        //then Re-register it and add to market or limit orders if it was already in pending
                        this.RegisterOrderInternal(integratorOrderInternal, isOrderAlreadyInInternalCollection);

                        //this is handling the rare condition where price comes in and then reject, but rejcet handling
                        // actually acquires the matching lock first
                        if (isOrderAlreadyInInternalCollection && integratorOrderInternal.SizeBaseAbsActive <= 0)
                        {
                            this.AttemptToRemoveOrderFromActiveLists(integratorOrderInternal);
                        }
                    }

                    if (orderChangeEventArgs != null)
                        orderChangeEventArgs.RejectionInfo.AddClientSystemAllocation(integratorOrderInternal.IntegratedTradingSystemIdentification, rejectedAmount);

                    totalRejectedAmount -= rejectedAmount;
                    //for partial reject, we cannot satisfy all internal orders
                    if (totalRejectedAmount <= 0)
                    {
                        break;
                    }
                }
            }
        }

        private void AttemptToRemoveOrderFromActiveLists(IntegratorOrderInternal integratorOrderInternal)
        {
            OrderType orderType = integratorOrderInternal.OrderRequestInfo.OrderType;
            List<IntegratorOrderInternal> internalOrdersList = this.GetOrdersList(orderType);

            _logger.Log(LogLevel.Info, "Removing the order from {1} list (as it was fully rematched or rejected): {0}", integratorOrderInternal, orderType);
            if (!internalOrdersList.Remove(integratorOrderInternal))
            {
                this._logger.Log(LogLevel.Error,
                         "Order [{0}] got fully rematched or rejected, but is missing from {1} list (but it would be now removed anyway)",
                         integratorOrderInternal.ClientOrderIdentity, orderType);
            }

            if (orderType == OrderType.Limit || orderType == OrderType.LimitTillTimeout)
            {
                UpdateBestOrder();
            }
            else
            {
                //refresh internal flags
                this.RefreshFlagsUnlocked();
            }
        }

        private void CloseBrokenPendingOrders(List<IntegratorOrderInternal> internalOrders,
                                        IIntegratorOrderExternal externalOrder)
        {
            foreach (IntegratorOrderInternal integratorOrderInternal in internalOrders)
            {
                decimal notFilledAmount;

                //we manipulate the amounts and collection - we need to serialize access
                lock (_matchingLocker)
                {
                    notFilledAmount = integratorOrderInternal.ExecutedPartitionsMap[externalOrder];
                    integratorOrderInternal.NotToBeFilledAmount += notFilledAmount;

                    integratorOrderInternal.DispatchGateway.OnCounterpartyIgnoredOrder(
                        new CounterpartyOrderIgnoringInfo(integratorOrderInternal.ClientOrderIdentity,
                                                          integratorOrderInternal.ClientIdentity,
                                                          string.Empty,
                                                          externalOrder.UniqueInternalIdentity, notFilledAmount,
                                                          externalOrder.OrderRequestInfo.RequestedPrice,
                                                          integratorOrderInternal.OrderRequestInfo.IntegratorDealDirection,
                                                          externalOrder.OrderRequestInfo.Symbol,
                                                          externalOrder.IntegratorSentTimeUtc,
                                                          externalOrder.Counterparty,
                                                          TradingTargetType.BankPool,
                                                          integratorOrderInternal.IntegratedTradingSystemIdentification,
                                                          integratorOrderInternal.GlidingInfoBag));

                    if (!integratorOrderInternal.IsActiveOrPending)
                    {
                        this._logger.Log(LogLevel.Info,
                                         "Internal order [{0}] was abandoned as last asociated external order is broken",
                                         integratorOrderInternal.ClientOrderIdentity);
                        integratorOrderInternal.OrderStatus = ClientOrderStatus.BrokenInIntegrator;
                        _pendingInactiveOrders.Remove(integratorOrderInternal);

                        if (KeepClosedOrders)
                        {
                            _closedOrders.Add(integratorOrderInternal);
                        }
                        this.InvokeOrderClosing(integratorOrderInternal);

                        integratorOrderInternal.DispatchGateway.OnIntegratorClientOrderUpdate(
                            BankPoolClientOrderUpdateInfo.CreateRemovedClientOrderUpdateInfo(integratorOrderInternal, 0m, integratorOrderInternal.IsCancellationRequested), integratorOrderInternal.OrderRequestInfo.Symbol);
                    }
                    else
                    {
                        integratorOrderInternal.DispatchGateway.OnIntegratorClientOrderUpdate(
                            new BankPoolClientOrderUpdateInfo(integratorOrderInternal, integratorOrderInternal.ClientOrderIdentity,
                                                              integratorOrderInternal.ClientIdentity,
                                                              ClientOrderStatus.BrokenInIntegrator,
                                                              ClientOrderCancelRequestStatus.CancelNotRequested, 0m, 0m,
                                                              integratorOrderInternal.SizeBaseAbsActive, string.Empty,
                                                              integratorOrderInternal.OrderRequestInfo.RequestedPrice,
                                                              integratorOrderInternal.OrderRequestInfo.IntegratorDealDirection),
                            integratorOrderInternal.OrderRequestInfo.Symbol);
                    }
                }
            }
        }

        private MarketOnImprovementResult? GetMarketOnImprovementResult(IntegratorOrderInternal internalOrder,
                                                                        IIntegratorOrderExternal externalOrder)
        {
            //Only MarektOnImprovement orders have this set
            if (internalOrder is IntegratorOrderInternalWithTimeout)
            {
                //If it is already MKT then timeout was reached
                if(internalOrder.OrderRequestInfo.OrderType == OrderType.Market)
                    return MarketOnImprovementResult.ExecutedOnTimeout;

                //If we were execution imediately on existing uptick then we were liq taker (and only then we were liq taker)
                if(internalOrder.RejectionCount == 0 && externalOrder.OrderRequestInfo.FlowSideAtSubmitTime == FlowSide.LiquidityTaker)
                    return MarketOnImprovementResult.ExecutedImmediatelyOnExistingImprovement;
                //otherwise we are executing on later improvement
                else
                    return MarketOnImprovementResult.ExecutedOnLaterImprovement;
            }

            return null;
        }

        private void CloseFilledPendingOrders(List<IntegratorOrderInternal> internalOrders,
                                        IIntegratorOrderExternal externalOrder, OrderChangeEventArgs orderChangeEventArgs, decimal totalFilledAmount)
        {
            foreach (IntegratorOrderInternal integratorOrderInternal in internalOrders)
            {
                decimal filledAmount;

                //we manipulate the amounts and collection - we need to serialize access
                // serialize access even for events dispatching - as asynchronous cancel can came in and 
                //  we don't want cancel to think that order is removed BEFORE the deal is dispatched
                lock (_matchingLocker)
                {
                    filledAmount = Math.Min(integratorOrderInternal.ExecutedPartitionsMap[externalOrder],
                                            totalFilledAmount);

                    //this internal order was already filled/rejected by different ExecutionReport message for same External order
                    if (filledAmount <= 0)
                        continue;

                    integratorOrderInternal.SizeBaseAbsFilled += filledAmount;
                    integratorOrderInternal.ExecutedPartitionsMap[externalOrder] -= filledAmount;

                    if (!integratorOrderInternal.IsActiveOrPending)
                    {
                        this._logger.Log(LogLevel.Info, "Internal order [{0}] was fully filled",
                                         integratorOrderInternal.ClientOrderIdentity);
                        integratorOrderInternal.OrderStatus = ClientOrderStatus.NotActiveInIntegrator;
                        _pendingInactiveOrders.Remove(integratorOrderInternal);
                        if (KeepClosedOrders)
                        {
                            _closedOrders.Add(integratorOrderInternal);
                        }
                        this.InvokeOrderClosing(integratorOrderInternal);
                    }

                    string counterpartyTransactionId = orderChangeEventArgs.ExecutionInfo.CounterpartyTransactionId;
                    string internalTransactionId = orderChangeEventArgs.ExecutionInfo.IntegratorTransactionId;

                    //send deal update - outside from the lock, so that we can perform matching concurrently
                    integratorOrderInternal.DispatchGateway.OnIntegratorDealInternal(
                        IntegratorGliderDealInternal.CreateIntegratorGliderDealInternal(integratorOrderInternal.ClientOrderIdentity, string.Empty,
                                                   integratorOrderInternal.ClientIdentity,
                                                   externalOrder.Identity,
                                                   counterpartyTransactionId,
                                                   internalTransactionId,
                                                   filledAmount, !integratorOrderInternal.IsActiveOrPending,
                                                   orderChangeEventArgs.ExecutionInfo.UsedPrice.Value,
                                                   externalOrder.OrderRequestInfo.Side,
                                                   externalOrder.OrderRequestInfo.Symbol,
                                                   externalOrder.IntegratorSentTimeUtc,
                                                   externalOrder.IntegratorReceivedResultTimeUtc,
                                                   externalOrder.CounterpartySentResultTimeUtc,
                                                   externalOrder.CounterpartyTransactionTimeUtc,
                                                   orderChangeEventArgs.ExecutionInfo.SettlementDate.Value,
                                                   orderChangeEventArgs.ExecutionInfo.SettlementTimeCounterpartyUtc,
                                                   orderChangeEventArgs.ExecutionInfo.SettlementTimeCentralBankUtc,
                                                   externalOrder.Counterparty, null,
                                                   externalOrder.OrderRequestInfo.FlowSideAtSubmitTime,
                                                   TradingTargetType.BankPool,
                                                   this.GetMarketOnImprovementResult(integratorOrderInternal, externalOrder),
                                                   integratorOrderInternal.IntegratedTradingSystemIdentification,
                                                   orderChangeEventArgs.ExecutionInfo.CounterpartyClientId, integratorOrderInternal.GlidingInfoBag));

                    //send order update (if done) AFTER the deal update
                    if (!integratorOrderInternal.IsActiveOrPending)
                    {
                        integratorOrderInternal.DispatchGateway.OnIntegratorClientOrderUpdate(
                            BankPoolClientOrderUpdateInfo.CreateRemovedClientOrderUpdateInfo(integratorOrderInternal, 0m, integratorOrderInternal.IsCancellationRequested), integratorOrderInternal.OrderRequestInfo.Symbol);
                    }

                    orderChangeEventArgs.ExecutionInfo.AddClientSystemAllocation(integratorOrderInternal.IntegratedTradingSystemIdentification, filledAmount);

                    totalFilledAmount -= filledAmount;
                    //for partial external fills, we cannot satisfy all internal orders
                    if (totalFilledAmount <= 0)
                    {
                        break;
                    }
                }
            }
        }

        private void ExternalOrderFailedDuringSubmission(IIntegratorOrderExternal externalOrder,
            SubmissionResult submissionResult)
        {
            List<IntegratorOrderInternal> internalOrders;
            lock (_matchingLocker)
            {
                if (this._externalOrdersMap.TryGetValue(externalOrder, out internalOrders))
                {
                    if (!externalOrder.HasRemainingAmount)
                    {
                        this._externalOrdersMap.Remove(externalOrder);
                    }
                }
                else
                {
                    this._logger.Log(LogLevel.Fatal,
                                     "Receiving external order [{0}] but cannot find matching internal order. This can happen if unexpectedly multiple execution reports come in.",
                                     externalOrder.Identity);
                    return;
                }
            }

            //error and add the amount back to order, put it back to _limit or _market list if it's in pending
            this._logger.Log(LogLevel.Error, "External order [{0}] failed during submission",
                             externalOrder.Identity);
            MoveFailedOrders(internalOrders, externalOrder, false, "External order couldn't be submited",
                externalOrder.OrderRequestInfo.SizeBaseAbsInitial, null,
                this._autoKillInternalOrdersOnOrdersTransmissionDisabled &&
                (this._riskManager.IsTransmissionDisabled ||
                 submissionResult == SubmissionResult.Failed_RiskManagementDisallowed));
        }

        //on order changed
        private void ExternalOrderChanged(IIntegratorOrderExternal externalOrder, OrderChangeEventArgs orderChangeEventArgs)
        {
            IntegratorOrderExternalChange changeStatus = orderChangeEventArgs.ChangeState;

            List<IntegratorOrderInternal> internalOrders;
            lock (_matchingLocker)
            {
                if (this._externalOrdersMap.TryGetValue(externalOrder, out internalOrders))
                {
                    if (!externalOrder.HasRemainingAmount)
                    {
                        this._externalOrdersMap.Remove(externalOrder);
                    }
                }
                else
                {
                    this._logger.Log(LogLevel.Fatal,
                                     "Receiving external order [{0}] but cannot find matching internal order. This can happen if unexpectedly multiple execution reports come in.",
                                     externalOrder.Identity);
                    return;
                }
            }

            switch (changeStatus)
            {
                case IntegratorOrderExternalChange.NotSubmitted:
                    //error and add the amount back to order, put it back to _limit or _market list if it's in pending
                    this._logger.Log(LogLevel.Error, "External order [{0}] failed during submission",
                                     externalOrder.Identity);
                    MoveFailedOrders(internalOrders, externalOrder, false, "External order couldn't be submited",
                                     externalOrder.OrderRequestInfo.SizeBaseAbsInitial, null,
                                     this._riskManager.IsTransmissionDisabled &&
                                     this._autoKillInternalOrdersOnOrdersTransmissionDisabled);
                    break;
                case IntegratorOrderExternalChange.Submitted:
                case IntegratorOrderExternalChange.ConfirmedNew:
                case IntegratorOrderExternalChange.PendingCancel:
                case IntegratorOrderExternalChange.CancelRejected:
                    //no need to do anything
                    break;
                case IntegratorOrderExternalChange.Filled:
                case IntegratorOrderExternalChange.PartiallyFilled:
                    //if in pending list AND this is the last unknown external order - remove
                    decimal filledAmount = orderChangeEventArgs.ExecutionInfo.FilledAmount.Value;

                    this._logger.Log(LogLevel.Info, "External order [{0}] filled (amount: {1} price: {2})",
                        externalOrder.Identity, filledAmount, orderChangeEventArgs.ExecutionInfo.UsedPrice.Value);
                    CloseFilledPendingOrders(internalOrders, externalOrder, orderChangeEventArgs, filledAmount);
                    break;
                case IntegratorOrderExternalChange.Rejected:
                case IntegratorOrderExternalChange.Cancelled:
                    //log and add the amount back to order, put it back to _limit or _market list if it's in pending
                    this._logger.Log(LogLevel.Info, "External order [{0}] was {1} (amount: {2})", externalOrder.Identity, changeStatus, orderChangeEventArgs.RejectionInfo.RejectedAmount);
                    BlacklistCounterpartyAfterRejectIfNeeded(internalOrders, externalOrder.Counterparty);
                    MoveFailedOrders(internalOrders, externalOrder, true, orderChangeEventArgs.RejectionInfo.RejectionReason, 
                        orderChangeEventArgs.RejectionInfo.RejectedAmount.Value, orderChangeEventArgs, false);
                    break;
                case IntegratorOrderExternalChange.InBrokenState:
                    //Log error. if in pending AND this is the last unknown external order - remove
                    this._logger.Log(LogLevel.Error, "External order [{0}] is in Broken state",
                                     externalOrder.Identity);
                    CloseBrokenPendingOrders(internalOrders, externalOrder);
                    break;
                case IntegratorOrderExternalChange.NonConfirmedDeal:
                default:
                    this._logger.Log(LogLevel.Fatal, "Unexpected IntegratorOrderExternalChange value: {0}", changeStatus);
                    break;
            }
        }

        private const int MAX_SUBMISSION_FAIL_COUNT = 10;

        private void BlacklistCounterpartyAfterRejectIfNeeded(List<IntegratorOrderInternal> internalOrders, Counterparty counterpartyToBlacklist)
        {
            if (this._counterpartiesToAutoBlackListAfterPreviousReject != null &&
                this._counterpartiesToAutoBlackListAfterPreviousReject.Contains(counterpartyToBlacklist))
            {
                foreach (IntegratorOrderInternal order in internalOrders)
                {
                    if (order.BankPoolClientOrderRequestInfo.CounterpartiesBlacklist == null)
                        order.BankPoolClientOrderRequestInfo.CounterpartiesBlacklist =
                            new ConcurrentQueue<Counterparty>();
                    order.BankPoolClientOrderRequestInfo.CounterpartiesBlacklist.Enqueue(
                        counterpartyToBlacklist);
                }
            }
        }



        private class IntegratorOrderInternal : IBankPoolClientOrder, IOrderRejectionsInfo
        {
            public string ClientOrderIdentity { get; set; }
            public string ClientIdentity { get; set; }
            public ClientOrderStatus OrderStatus { get; set; }
            public decimal SizeBaseAbsFilled { get; set; }
            public decimal SizeBaseAbsActive { get; set; }
            public decimal NotToBeFilledAmount { get; set; }
            public int RejectionCount { get; private set; }
            private DateTime _timeFirstSentExternallyUtc = DateTime.MaxValue;

            public TimeSpan RejectionsDuration
            {
                get { return HighResolutionDateTime.UtcNow - _timeFirstSentExternallyUtc; }
            }

            public int SubmissionFailCount { get; private set; }
            public IntegratedTradingSystemIdentification IntegratedTradingSystemIdentification { get; set; }
            public bool IsCancellationRequested { get; set; }

            #region IIntegratorOrderInfo

            public string Identity { get { return this.ClientOrderIdentity; } }
            public Symbol Symbol { get { return this.OrderRequestInfo.Symbol; } }
            public DealDirection IntegratorDealDirection { get { return this.OrderRequestInfo.IntegratorDealDirection; } }
            public Counterparty Counterparty { get { return Counterparty.NULL; } }
            public decimal SizeBaseAbsInitial { get { return this.OrderRequestInfo.SizeBaseAbsInitial; } }
            public decimal RequestedPrice { get { return this.OrderRequestInfo.RequestedPrice.Value; } }

            internal GlidingInfoBag GlidingInfoBag { get; set; }

            public IntegratorOrderInfoType IntegratorOrderInfoType
            {
                get
                {
                    return this.OrderRequestInfo.RequestedPrice.HasValue
                        ? IntegratorOrderInfoType.InternalLmtOrder
                        : IntegratorOrderInfoType.InternalMktOrder;
                }
            }
            public bool IsRiskRemovingOrder { get { return this.OrderRequestInfo.IsRiskRemovingOrder; } }

            #endregion IIntegratorOrderInfo

            public void MarkRejected(DateTime orderSentExternalyUtc)
            {
                this.RejectionCount++;
                if(this._timeFirstSentExternallyUtc == DateTime.MaxValue)
                    this._timeFirstSentExternallyUtc = orderSentExternalyUtc;

                //reset submission fail count as we were genuinely rejected
                this.SubmissionFailCount = 0;
            }

            public int MarkSubmissionFailed()
            {
                return ++this.SubmissionFailCount;
            }

            //public IBankPoolClientOrder Clone()
            //{
            //    //return new IntegratorOrderInternal()
            //    //    {
            //    //        ClientOrderIdentity = ClientOrderIdentity,
            //    //        ClientIdentity = ClientIdentity,
            //    //        BankPoolClientOrderRequestInfo = BankPoolClientOrderRequestInfo
            //    //    };

            //    return new IntegratorOrderInternal()
            //    {
            //        ClientOrderIdentity = ClientOrderIdentity,
            //        ClientIdentity = ClientIdentity,
            //        OrderStatus = OrderStatus,
            //        SizeBaseAbsFilled = SizeBaseAbsFilled,
            //        SizeBaseAbsActive = SizeBaseAbsActive,
            //        BankPoolClientOrderRequestInfo = BankPoolClientOrderRequestInfo,
            //        CreatedUtc = CreatedUtc,
            //        DispatchGateway = DispatchGateway,
            //        IntegratedTradingSystemIdentification = IntegratedTradingSystemIdentification
            //    };
            //}

            public ClientOrderRequestInfoBase OrderRequestInfo
            {
                get { return this.BankPoolClientOrderRequestInfo; }
            }

            public BankPoolClientOrderRequestInfo BankPoolClientOrderRequestInfo { get; set; }

            public ITakerUnicastInfoForwarder DispatchGateway { get; set; }
            public DateTime CreatedUtc { get; set; }
            //we need to be able to backtrack m:n mapping of external to internal orders
            // during the order rejects etc.
            public Dictionary<IIntegratorOrderExternal, decimal> ExecutedPartitionsMap { get; private set; }

            public override string ToString()
            {
                return string.Format("IntegratorOrderInternal [{0}] status: {1}, filled: {2}, active: {3}", ClientOrderIdentity,
                                     OrderStatus, SizeBaseAbsFilled, SizeBaseAbsActive);
            }

            public void AddExternalPartition(IIntegratorOrderExternal externalOrder, decimal size)
            {
                if (this.ExecutedPartitionsMap == null)
                {
                    this.ExecutedPartitionsMap = new Dictionary<IIntegratorOrderExternal, decimal>();
                }

                this.ExecutedPartitionsMap[externalOrder] = size;
            }

            public decimal PendingAmount
            {
                get
                {
                    return this.OrderRequestInfo.SizeBaseAbsInitial - this.SizeBaseAbsFilled - this.NotToBeFilledAmount -
                           this.SizeBaseAbsActive;
                }
            }

            public bool IsActiveOrPending
            {
                get { return this.OrderRequestInfo.SizeBaseAbsInitial - this.SizeBaseAbsFilled - this.NotToBeFilledAmount > 0; }
            }

            public bool IsPartiallyBroken
            {
                get { return this.NotToBeFilledAmount > 0; }
            }

            public bool IsNonActivePending
            {
                get
                {
                    return this.SizeBaseAbsActive <= 0 &&
                           this.OrderRequestInfo.SizeBaseAbsInitial - this.SizeBaseAbsFilled - this.NotToBeFilledAmount > 0;
                }
            }

            private SpinLock _spinLock = new SpinLock(false);

            //thread safe
            public bool TrySubstractAmount(decimal sizeToSubstract, ref decimal substracted, ref decimal remaining)
            {
                if (this.SizeBaseAbsActive <= 0)
                {
                    return false;
                }

                bool success = false;

                bool lockTaken = false;
                _spinLock.Enter(ref lockTaken);

                if (this.SizeBaseAbsActive > 0)
                {
                    if (sizeToSubstract >= this.SizeBaseAbsActive)
                    {
                        substracted = this.SizeBaseAbsActive;
                        this.SizeBaseAbsActive = 0;
                        remaining = 0;
                    }
                    else
                    {
                        substracted = sizeToSubstract;
                        remaining = this.SizeBaseAbsActive - sizeToSubstract;
                        this.SizeBaseAbsActive = remaining;
                    }

                    success = true;
                }

                if (lockTaken)
                {
                    _spinLock.Exit();
                }

                return success;
            }

            public TradingTargetType TradingTargetType
            {
                get { return TradingTargetType.BankPool; }
            }
        }

        private class IntegratorOrderInternalWithTimeout: IntegratorOrderInternal
        {
            private const int _UNSET = -1;
            private const int _ACTIVE = 0;
            private const int _DEACTIVATED = 1;

            private IAccurateTimer _timer;
            private int _state = _UNSET;
            private Action<IntegratorOrderInternalWithTimeout> _orderTimeoutExpired;

            public bool SetTimeOut(TimeSpan timeout, Action<IntegratorOrderInternalWithTimeout> orderTimeoutExpired)
            {
                if (InterlockedEx.CheckAndFlipState(ref this._state, _ACTIVE, _UNSET))
                {
                    this._orderTimeoutExpired += orderTimeoutExpired;
                    _timer = HighResolutionDateTime.HighResolutionTimerManager.RegisterTimer(this.TimeoutExpired, timeout,
                        TimerTaskFlagUtils.WorkItemPriority.Highest, TimerTaskFlagUtils.TimerPrecision.Highest, true, false);
                    return true;
                }

                return false;
            }

            //This is redundant check that workarounds .NET timers deficiency
            //public bool ShouldExpired
            //{
            //    get { return this._expireTimeUtc < HighResolutionDateTime.UtcNow; }
            //}

            private void TimeoutExpired()
            {
                if (InterlockedEx.CheckAndFlipState(ref this._state, _DEACTIVATED, _ACTIVE))
                {
                    HighResolutionDateTime.HighResolutionTimerManager.CancelTimer(ref _timer);
                    if (this._orderTimeoutExpired != null)
                    {
                        this._orderTimeoutExpired(this);
                    }
                }
            }

            public void Discard()
            {
                if (InterlockedEx.CheckAndFlipState(ref this._state, _DEACTIVATED, _ACTIVE))
                {
                    HighResolutionDateTime.HighResolutionTimerManager.CancelTimer(ref _timer);
                }
            }
        }
    }
}
