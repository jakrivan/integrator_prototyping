﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinessLayer
{
    public class BusinessLayerSettings
    {
        public MarketableClientOrdersMatchingStrategy MarketableClientOrdersMatchingStrategy { get; set; }
        public ConsecutiveRejectionsStopStrategySettings ConsecutiveRejectionsStopStrategyBehavior { get; set; }
        public bool StartWithSessionsStopped { get; set; }
        public bool AutoKillInternalOrdersOnOrdersTransmissionDisabled { get; set; }
        public string StartStopEmailSubject { get; set; }
        public string StartStopEmailBody { get; set; }
        public string StartStopEmailTo { get; set; }
        public string StartStopEmailCc { get; set; }
        public string FatalRejectedOrderTriggerPhrasesCsv
        {
            get { return string.Join(",", this.FatalRejectedOrderTriggerPhrases); }
            set { this.FatalRejectedOrderTriggerPhrases = value.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries); }
        }
        public List<IgnoredOrderCounterpartyContact> IgnoredOrderCounterpartyContacts { get; set; }
        public string IgnoredOrderEmailCc { get; set; }
        

        public PreventMatchingToSameCounterpartyAfterOrderRejectSettings
            PreventMatchingToSameCounterpartyAfterOrderRejectBehavior { get; set; }

        public TimeGapsWatchingSettings TimeGapsWatchingBehavior { get; set; }


        [XmlIgnore]
        public IEnumerable<string> FatalRejectedOrderTriggerPhrases { get; private set; }
        
        public class IgnoredOrderCounterpartyContact
        {
            [System.Xml.Serialization.XmlAttribute]
            public string CounterpartyCode { get; set; }
            [System.Xml.Serialization.XmlAttribute]
            public string EmailContacts { get; set; }
        }

        public class ConsecutiveRejectionsStopStrategySettings
        {
            public bool StrategyEnabled { get; set; }
            public int NumerOfRejectionsToTriggerStrategy { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public System.TimeSpan DurationOfRejectionsToTriggerStrategy { get; set; }

            [System.Xml.Serialization.XmlElement("DurationOfRejectionsToTriggerStrategy_Milliseconds")]
            public int DurationOfRejectionsToTriggerStrategyXml
            {
                get { return (int)DurationOfRejectionsToTriggerStrategy.TotalMilliseconds; }
                set { DurationOfRejectionsToTriggerStrategy = System.TimeSpan.FromMilliseconds(value); }
            }

            public decimal CounterpartyRankWeightMultiplier { get; set; }
            public decimal PriceAgeRankWeightMultiplier { get; set; }
            public decimal PriceBpRankWeightMultiplier { get; set; }
            public decimal DefaultCounterpartyRank { get; set; }
            public List<CounterpartyRank> CounterpartyRanks { get; set; }

            public class CounterpartyRank
            {
                [System.Xml.Serialization.XmlAttribute]
                public string CounterpartyCode { get; set; }
                [System.Xml.Serialization.XmlAttribute]
                public decimal Rank { get; set; }
            }
        }

        public class PreventMatchingToSameCounterpartyAfterOrderRejectSettings
        {
            public bool StrategyEnabled { get; set; }

            [XmlArray(ElementName = "CounterpartyCodes")]
            [XmlArrayItem(ElementName = "Counterparty")]
            public List<string> CounterpartyCodes { get; set; }
        }

        public class TimeGapsWatchingSettings
        {
            public bool LogFatalsDuringOffBusinessHours { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public System.TimeSpan MinimumTimeGapToWatchFor { get; set; }

            [System.Xml.Serialization.XmlElement("MinimumTimeGapToWatchFor_Milliseconds")]
            public int MinimumTimeGapToWatchForXml
            {
                get { return (int)MinimumTimeGapToWatchFor.TotalMilliseconds; }
                set { MinimumTimeGapToWatchFor = System.TimeSpan.FromMilliseconds(value); }
            }

            [System.Xml.Serialization.XmlIgnore]
            public System.TimeSpan MinimumTimeGapToLogFatal { get; set; }

            [System.Xml.Serialization.XmlElement("MinimumTimeGapToLogFatal_Milliseconds")]
            public int MinimumTimeGapToLogFatalXml
            {
                get { return (int)MinimumTimeGapToLogFatal.TotalMilliseconds; }
                set { MinimumTimeGapToLogFatal = System.TimeSpan.FromMilliseconds(value); }
            }

            public bool EnableWatchingForGapsClusters { get; set; }

            public int NumberOfGapsInClusterToLogFatal { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public System.TimeSpan ClusterDuration { get; set; }

            [System.Xml.Serialization.XmlElement("ClusterDuration_Seconds")]
            public int ClusterDurationXml
            {
                get { return (int)ClusterDuration.TotalSeconds; }
                set { ClusterDuration = System.TimeSpan.FromSeconds(value); }
            }
        }
    }
}
