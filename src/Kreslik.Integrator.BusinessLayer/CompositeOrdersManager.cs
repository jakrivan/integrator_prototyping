﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.BusinessLayer
{
    //public class CompositeOrdersManager : IUnicastInfoForwarder
    //{
    //    private IPriceBookStore _priceBookStore;
    //    private IList<IOrderFlowSession> _orderFlowSessions;
    //    private ILogger _logger;
    //    private IRiskManager _riskManager;
    //    private IOrderManagement _orderManagement;
    //    private Dictionary<string, ChildOrderInfo> _childOrderInfos = new Dictionary<string, ChildOrderInfo>();
    //    private Dictionary<IIntegratorOrderExternal, IBankPoolClientOrder> _externalOrdersMap = new Dictionary<IIntegratorOrderExternal, IBankPoolClientOrder>();

    //    public CompositeOrdersManager(ILogger logger, IPriceBookStore priceBookStore,
    //                                  IList<IOrderFlowSession> orderFlowSessions, IRiskManager riskManager,
    //                                  IOrderManagement orderManagement)
    //    {
    //        this._logger = logger;
    //        this._priceBookStore = priceBookStore;
    //        this._orderFlowSessions = orderFlowSessions;
    //        this._riskManager = riskManager;
    //        this._orderManagement = orderManagement;
    //    }

    //    private bool ShouldRejectCompositeClientOrder(IBankPoolClientOrder clientOrder, out string reason)
    //    {
    //        if (!clientOrder.IsComposite || clientOrder.ChildOrders == null || clientOrder.ChildOrders.Count == 0)
    //        {
    //            this._logger.Log(LogLevel.Error, "CompositeOrdersManagement encountered invalid composite order [{0}]", clientOrder);
    //            reason = "Order is invalid composite order";
    //            return true;
    //        }

    //        if (clientOrder.ChildOrders.Count > 2)
    //        {
    //            this._logger.Log(LogLevel.Error,
    //                             "CompositeOrdersManagement currently allows only composite orders with up to 2 child orders. Composite order [{0}] doesn't meet this condition",
    //                             clientOrder);
    //            reason = "Composite order manager currently allows only composite orders with up to 2 child orders";
    //            return true;
    //        }

    //        //TODO: needed?
    //        if (clientOrder.ChildOrders.Any(ord => ord.OrderRequestInfo.OrderType != OrderType.Quoted))
    //        {
    //            this._logger.Log(LogLevel.Error,
    //                             "CompositeOrdersManagement encountered composite order ([{0}]) which contained non-quoted orders",
    //                             clientOrder);
    //            reason = "Composite order manager currently allows only quoted child orders";
    //            return true;
    //        }

    //        if (clientOrder.CompositeOrderStrategy != CompositeOrderStrategy.AllAtOnceOrImmediateAtMarket &&
    //            clientOrder.CompositeOrderStrategy != CompositeOrderStrategy.AllAtOnceOrKill)
    //        {
    //            reason = string.Format("Disallowed CompositeOrderStrategy [{0}] for composite order",
    //                                   clientOrder.CompositeOrderStrategy);
    //            this._logger.Log(LogLevel.Error, reason);
    //            return true;
    //        }

    //        if (_riskManager.IsTransmissionDisabled)
    //        {
    //            reason = "Order Transmission is currently disabled";
    //            return true;
    //        }

    //        reason = null;
    //        return false;
    //    }

    //    public void RegisterCompositeOrder(IBankPoolClientOrder clientOrder, IUnicastInfoForwarder dispatchGateway)
    //    {
    //        string rejectReason;
    //        if (ShouldRejectCompositeClientOrder(clientOrder, out rejectReason))
    //        {
    //            dispatchGateway.OnIntegratorClientOrderUpdate(
    //                BankPoolClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder, rejectReason),
    //                clientOrder.ChildOrders == null
    //                    ? null
    //                    : clientOrder.ChildOrders.Count == 0
    //                          ? null
    //                          : clientOrder.ChildOrders[0].OrderRequestInfo == null
    //                                ? null
    //                                : clientOrder.ChildOrders[0].OrderRequestInfo.Symbol);
    //            return;
    //        }

    //        this._logger.Log(LogLevel.Info,
    //                         "Composite orders manager registering order: {0}{1}Current HighResolutionDateTime: {2}, Delay of newest price in CompositeOrder: {3}",
    //                         clientOrder, Environment.NewLine, HighResolutionDateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fffffff UTC"),
    //                         HighResolutionDateTime.UtcNow -
    //                         clientOrder.ChildOrders.Select(
    //                             ord => ord.OrderRequestInfo.Price.IntegratorReceivedTimeUtc).Max());

    //        dispatchGateway.OnIntegratorClientOrderUpdate(
    //            BankPoolClientOrderUpdateInfo.CreateOpenClientOrderUpdateInfo(clientOrder), clientOrder.ChildOrders[0].OrderRequestInfo.Symbol);

    //        //Lets clone the client order from request into our internal object
    //        // this way we'll not touch the client objects and also we'll be able to perform restricted operations (changing of request info etc.)
    //        clientOrder = new CompositeClientOrderInternal(clientOrder);

    //        //check if there is a matching price for each order, and substract size from it and create external order for each of those
    //        //if anything failed, add the substracted amount back to prices - and send rejection to client
    //        //else - try send all external orders
    //        //         if anything fails turn the remaining into market orders and send via SingleSymbolOrder ...

    //        //in the handler - if filled, then send info to client (and mark it filled) if rejected or so - turn in market

    //        List<IIntegratorOrderExternal> externalOrders = new List<IIntegratorOrderExternal>();
    //        bool matchingFailed = false;

    //        foreach (IBankPoolClientOrder childOrder in clientOrder.ChildOrders.OrderBy(clOrd => clOrd.OrderRequestInfo.Price.IntegratorReceivedTimeUtc))
    //        {
    //            IIntegratorOrderExternal externalOrder = CreateExternalOrderForImmediateClientOrder(childOrder, out rejectReason);
    //            if (externalOrder != null)
    //            {
    //                externalOrders.Add(externalOrder);

    //                //Add mapping to client child order and parent order
    //                _childOrderInfos.Add(childOrder.ClientOrderIdentity,
    //                                     new ChildOrderInfo()
    //                                     {
    //                                         OrderResultDispatchGateway = dispatchGateway,
    //                                         ParentOrderId = clientOrder.ClientOrderIdentity,
    //                                         ParentOrder = clientOrder,
    //                                         RemainingOpenSize = childOrder.OrderRequestInfo.SizeBaseAbsInitial
    //                                     });
    //                _externalOrdersMap.Add(externalOrder, childOrder);
    //            }
    //            else
    //            {
    //                matchingFailed = true;
    //                break;
    //            }
    //        }

    //        if (matchingFailed)
    //        {
    //            this._logger.Log(LogLevel.Error, "CompositeOrderMatching couldn't fully match client order [{0}] so cancelling all of its parts", clientOrder.ClientOrderIdentity);

    //            foreach (IIntegratorOrderExternal integratorOrderExternal in externalOrders)
    //            {
    //                integratorOrderExternal.OnOrderChanged -= ExternalOrderChanged;
    //                //WritablePriceObjectInternal price = integratorOrderExternal.OrderRequestInfo.PriceObjectInternal as WritablePriceObject;
    //                //if (price == null)
    //                //{
    //                //    this._logger.Log(LogLevel.Fatal, "External order [{0}] doesn't contain expected price object", integratorOrderExternal.UniqueInternalIdentity);
    //                //}
    //                //else
    //                //{
    //                //    price.AddSizeBaseAbs(integratorOrderExternal.OrderRequestInfo.SizeBaseAbsInitial);
    //                //}

    //                _externalOrdersMap.Remove(integratorOrderExternal);
    //            }

    //            dispatchGateway.OnIntegratorClientOrderUpdate(BankPoolClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder, rejectReason), clientOrder.ChildOrders[0].OrderRequestInfo.Symbol);
    //        }
    //        else
    //        {
    //            bool submissionFailed = false;

    //            //Now we want to submit orders in parallel to prevent waiting on slow orders
    //            // however to provide speedup we want to submit the oldest order (from oldest price) synchronously on this thread

    //            Task[] orderSubmitTasks = new Task[externalOrders.Count-1];
    //            for (int ordIdx = 1; ordIdx < externalOrders.Count; ordIdx++)
    //            {
    //                IIntegratorOrderExternal integratorOrderExternal = externalOrders[ordIdx];
    //                orderSubmitTasks[ordIdx - 1] =
    //                    Task.Factory.StartNew(
    //                        () => TrySubmitExternalOrder(integratorOrderExternal, ref submissionFailed));
    //            }

    //            TrySubmitExternalOrder(externalOrders[0], ref submissionFailed);
    //            Task.WaitAll(orderSubmitTasks);

    //            //Close positions if needed
    //            if (submissionFailed)
    //            {
    //                foreach (
    //                    IIntegratorOrderExternal integratorOrderExternal in
    //                        externalOrders.Where(
    //                            ord =>
    //                            ord != null &&
    //                            ord.OrderStatus == IntegratorOrderExternalStatus.NotSubmitted))
    //                {
    //                    integratorOrderExternal.OnOrderChanged -= ExternalOrderChanged;

    //                    IBankPoolClientOrder childOrder = _externalOrdersMap[integratorOrderExternal];
    //                    ChildOrderInfo childOrderInfo = _childOrderInfos[childOrder.ClientOrderIdentity];

    //                    childOrderInfo.OrderResultDispatchGateway.OnCounterpartyRejectedOrder(
    //                    new CounterpartyRejectionInfo(childOrder.ClientOrderIdentity,
    //                                                  childOrder.ClientIdentity,
    //                                                  childOrderInfo.ParentOrderId,
    //                                                  integratorOrderExternal.UniqueInternalIdentity,
    //                                                  integratorOrderExternal.OrderRequestInfo.SizeBaseAbsInitial,
    //                                                  integratorOrderExternal.OrderRequestInfo.RequestedPrice,
    //                                                  childOrder.OrderRequestInfo.Side,
    //                                                  integratorOrderExternal.OrderRequestInfo.Symbol,
    //                                                  integratorOrderExternal.IntegratorSentTimeUtc,
    //                                                  integratorOrderExternal.IntegratorReceivedResultTimeUtc,
    //                                                  integratorOrderExternal.CounterpartySentResultTimeUtc,
    //                                                  integratorOrderExternal.ParentSession.Counterparty,
    //                                                  "Failed during submission on Integrator side", true,
    //                                                  integratorOrderExternal.ParentSession.Counterparty.TradingTargetType,
    //                                                  childOrder.IntegratedTradingSystemIdentification));

    //                    TransformToMarketOrder(childOrder, childOrderInfo);
    //                }
    //            }
    //        }
    //    }

    //    private void TrySubmitExternalOrder(IIntegratorOrderExternal integratorOrderExternal, ref bool submissionFailed)
    //    {
    //        _riskManager.AddInternalOrderAndCheckIfAllowed(_externalOrdersMap[integratorOrderExternal]);
    //        if (!integratorOrderExternal.ParentSession.SubmitOrder(integratorOrderExternal))
    //        {
    //            _logger.Log(LogLevel.Error, "Couldn't submit external order [{0}]",
    //                        integratorOrderExternal.Identity);
    //            submissionFailed = true;
    //            _riskManager.CancelInternalOrder(_externalOrdersMap[integratorOrderExternal],
    //                                             integratorOrderExternal.OrderRequestInfo.SizeBaseAbsInitial);
    //        }
    //    }


    //    private void TransformToMarketOrder(IBankPoolClientOrder childOrder, ChildOrderInfo childOrderInfo)
    //    {

    //        this._logger.Log(LogLevel.Info,
    //                         "Client order [{0}] external matching failed, and strategy is set to [{1}] so {2}continuing in processing it further as market order",
    //                         childOrder.ClientOrderIdentity, childOrder.CompositeOrderStrategy,
    //                         childOrder.CompositeOrderStrategy == CompositeOrderStrategy.AllAtOnceOrKill
    //                             ? "NOT "
    //                             : string.Empty);

    //        if (childOrder.CompositeOrderStrategy == CompositeOrderStrategy.AllAtOnceOrKill)
    //        {
    //            childOrderInfo.OrderResultDispatchGateway.OnIntegratorClientOrderUpdate(
    //                BankPoolClientOrderUpdateInfo.CreateRemovedClientOrderUpdateInfo(childOrder, childOrderInfo.RemainingOpenSize), childOrder.OrderRequestInfo.Symbol);

    //            return;
    //        }

    //        BankPoolClientOrderRequestInfo marketOrderRequestInfo =
    //            ClientOrdersBuilder_OnlyForIntegratorUsage.CreateMarket(childOrderInfo.RemainingOpenSize,
    //                                                childOrder.OrderRequestInfo.Side,
    //                                                childOrder.OrderRequestInfo.Symbol);

    //        //Counterparties of composite orders on same pair and oposite direction
    //        var blacklistCandidates = childOrderInfo.ParentOrder.ChildOrders.Where(
    //            ord =>
    //            ord.OrderRequestInfo.Price != null &&
    //            ord.OrderRequestInfo.Symbol == childOrder.OrderRequestInfo.Symbol &&
    //            ord.OrderRequestInfo.Side != childOrder.OrderRequestInfo.Side).ToList();

    //        //Blacklist self in blacklist candidates (as after transforming to MKT order, current Counterparty will be lost)
    //        Counterparty currentQuotedCounterparty = childOrder.OrderRequestInfo.Price.Counterparty;
    //        foreach (IBankPoolClientOrder blacklistCandidate in blacklistCandidates)
    //        {
    //            if (blacklistCandidate.BankPoolClientOrderRequestInfo.CounterpartiesBlacklist == null)
    //                blacklistCandidate.BankPoolClientOrderRequestInfo.CounterpartiesBlacklist = new ConcurrentQueue<Counterparty>();
    //            blacklistCandidate.BankPoolClientOrderRequestInfo.CounterpartiesBlacklist.Enqueue(currentQuotedCounterparty);

    //            //Blacklist blacklist candidates
    //            //multiple blacklisting of same counterparty doesn't matter
    //            if (childOrder.BankPoolClientOrderRequestInfo.CounterpartiesBlacklist == null)
    //                childOrder.BankPoolClientOrderRequestInfo.CounterpartiesBlacklist = new ConcurrentQueue<Counterparty>();
    //            childOrder.BankPoolClientOrderRequestInfo.CounterpartiesBlacklist.Enqueue(
    //                blacklistCandidate.OrderRequestInfo.Price.Counterparty);
    //        }

    //        marketOrderRequestInfo.CounterpartiesBlacklist = childOrder.BankPoolClientOrderRequestInfo.CounterpartiesBlacklist;

    //        if (childOrder is CompositeClientOrderInternal)
    //        {
    //            //override order req. info so that we can later adjust blacklists
    //            //we need this despite we are creating new order below. That's because Parent order has only access
    //            // to this particular order below, and via that it has access to OrderRequestInfo (that is passed below)
    //            // So going forward, this client order is used just as a bridge to orderRequestInfo
    //            (childOrder as CompositeClientOrderInternal).OverrideOrderRequestInfo(marketOrderRequestInfo);

    //            _orderManagement.RegisterOrder(
    //                childOrder.Clone(), this,
    //                childOrderInfo.ParentOrder);
    //        }
    //        else
    //        {
    //            this._logger.Log(LogLevel.Fatal,
    //                             "Client order {0} is of a unexpected type {1}. Cannot transform it to market order",
    //                             childOrder, childOrder.GetType());
    //        }
    //    }

    //    private IIntegratorOrderExternal CreateExternalOrderForImmediateClientOrder(IClientOrder childOrder, out string rejectReason)
    //    {
    //        IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook;
    //        if (childOrder.OrderRequestInfo.Side == DealDirection.Buy)
    //        {
    //            priceBook = _priceBookStore.GetChangeableAskPriceBook(childOrder.OrderRequestInfo.Symbol);
    //        }
    //        else
    //        {
    //            priceBook = _priceBookStore.GetChangeableBidPriceBook(childOrder.OrderRequestInfo.Symbol);
    //        }

    //        PriceObjectInternal price;

    //        //switch (childOrder.OrderRequestInfo.OrderType)
    //        //{
    //        //    case OrderType.Limit:
    //        //        price = priceBook.MaxNode;
    //        //        if(price != null && !PriceObject.IsNullPrice(price))
    //        //        break;
    //        //    case OrderType.Market:
    //        //        price = priceBook.MaxNode;
    //        //        break;
    //        //    case OrderType.Quoted:
    //        //        break;
    //        //    default:
    //        //        throw new ArgumentOutOfRangeException();
    //        //}

    //        price = priceBook.GetIdentical(childOrder.OrderRequestInfo.Price) as PriceObjectInternal;

    //        if (price == null || PriceObjectInternal.IsNullPrice(price))
    //        {
    //            rejectReason = string.Format("Cannot locate price quoted by client order {0}. It might have been already replaced.", childOrder);
    //            this._logger.Log(LogLevel.Warn, rejectReason);
    //            return null;
    //        }

    //        if (HighResolutionDateTime.UtcNow - price.IntegratorReceivedTimeUtc >
    //            childOrder.OrderRequestInfo.MaximumAcceptedPriceAge)
    //        {
    //            rejectReason = string.Format("Client order {0} could not be satisfied from price {1} as it is older then threshold allowed by the order {2}, (Age: {3})",
    //                             childOrder, price, childOrder.OrderRequestInfo.MaximumAcceptedPriceAge,
    //                             HighResolutionDateTime.UtcNow - price.IntegratorReceivedTimeUtc);
    //            this._logger.Log(LogLevel.Error, rejectReason);
    //            return null;
    //        }

    //        if (price.SizeBaseAbsRemaining < childOrder.OrderRequestInfo.SizeBaseAbsInitial)
    //        {
    //            rejectReason =
    //                string.Format(
    //                    "Client order {0} could not be satisfied from price {1} as it doesn't have sufficient liquidity",
    //                    childOrder, price);

    //            this._logger.Log(LogLevel.Error, rejectReason);
    //            return null;
    //        }

    //        //Create the external order
    //        OrderRequestInfo orderRequestInfo = OrderRequestInfo.CreateQuoted(childOrder.OrderRequestInfo.SizeBaseAbsInitial, price.Price,
    //                                                                    childOrder.OrderRequestInfo.Side, price);

    //        if (orderRequestInfo == OrderRequestInfo.INVALID_ORDER_REQUEST)
    //        {
    //            rejectReason = string.Format("Couldn't create valid order request for client order {0}", childOrder.ClientOrderIdentity);
    //            _logger.Log(LogLevel.Error, rejectReason);
    //            return null;
    //        }
    //        else
    //        {
    //            if (!price.SubstractSizeBaseAbs(childOrder.OrderRequestInfo.SizeBaseAbsInitial))
    //            {
    //                rejectReason =
    //                    string.Format(
    //                        "Client order {0} could not be satisfied from price {1} as it doesn't have sufficient liquidity (it might have been 'stolen' in between time)",
    //                        childOrder, price);
    //                this._logger.Log(LogLevel.Error, rejectReason);
    //                return null;
    //            }

    //            IOrderFlowSession orderFlowSession = null;
    //            if (_orderFlowSessions != null &&
    //                (int)price.Counterparty < _orderFlowSessions.Count)
    //            {
    //                orderFlowSession = _orderFlowSessions[(int)price.Counterparty];
    //            }

    //            if (orderFlowSession == null)
    //            {
    //                rejectReason =
    //                    string.Format("Couldn't find valid OrderFlow Session for price [{0}] with counterpart {1}",
    //                                  price, price.Counterparty);
    //                _logger.Log(LogLevel.Error, rejectReason);
    //                return null;
    //            }
    //            else
    //            {
    //                IIntegratorOrderExternal externalOrder = orderFlowSession.GetNewExternalOrder(orderRequestInfo, out rejectReason);
    //                if (externalOrder != null)
    //                {
    //                    this._logger.Log(LogLevel.Info,
    //                                     "Client order {0} (from composite order) found matchable with price {1}. External order {2} was created",
    //                                     childOrder.ClientOrderIdentity, price.IntegratorIdentity, externalOrder.Identity);
    //                    externalOrder.OnOrderChanged += ExternalOrderChanged;
    //                }
    //                UpdatePriceBookIfNeeded(priceBook, price);
    //                return externalOrder;
    //            }
    //        }
    //    }

    //    private void UpdatePriceBookIfNeeded(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook, PriceObjectInternal price)
    //    {
    //        if (price.SizeBaseAbsRemaining <= 0)
    //        {
    //            if (price.SizeBaseAbsRemaining < 0) this._logger.Log(LogLevel.Error, "Price [{0}] going below zero size", price);
    //            priceBook.RemoveIdentical(price);
    //        }
    //        else
    //        {
    //            priceBook.ResortIdentical(price);
    //        }
    //    }

    //    private void ExternalOrderChanged(IIntegratorOrderExternal externalOrder, OrderChangeEventArgs orderChangeEventArgs)
    //    {
    //        IntegratorOrderExternalChange changeStatus = orderChangeEventArgs.ChangeState;
    //        IBankPoolClientOrder childOrder;
    //        if (this._externalOrdersMap.TryGetValue(externalOrder, out childOrder))
    //        {
    //            if (changeStatus != IntegratorOrderExternalChange.ConfirmedNew && !externalOrder.HasRemainingAmount)
    //            {
    //                this._externalOrdersMap.Remove(externalOrder);
    //            }
    //        }
    //        else
    //        {
    //            this._logger.Log(LogLevel.Fatal,
    //                             "Receiving external order [{0}] but cannot find matching internal child order. This can happen if unexpectedly multiple execution reports come in.",
    //                             externalOrder.Identity);
    //            return;
    //        }

    //        ChildOrderInfo childOrderInfo;
    //        if (!this._childOrderInfos.TryGetValue(childOrder.ClientOrderIdentity, out childOrderInfo))
    //        {
    //            this._logger.Log(LogLevel.Fatal,
    //                             "Receiving external order [{0}] cannot find matching child order info for corresponding order {1}",
    //                             externalOrder.Identity, childOrder.ClientOrderIdentity);
    //            return;
    //        }
    //        else
    //        {
    //            this._childOrderInfos.Remove(childOrder.ClientOrderIdentity);
    //        }


    //        switch (changeStatus)
    //        {
    //            case IntegratorOrderExternalChange.Submitted:
    //            case IntegratorOrderExternalChange.ConfirmedNew:
    //            case IntegratorOrderExternalChange.PendingCancel:
    //            case IntegratorOrderExternalChange.CancelRejected:
    //                //no need to do anything
    //                break;
    //            case IntegratorOrderExternalChange.Filled:
    //            case IntegratorOrderExternalChange.PartiallyFilled:
    //                //if in pending list AND this is the last unknown external order - remove
    //                decimal filledAmount = orderChangeEventArgs.ExecutionInfo.FilledAmount.Value;
    //                this._logger.Log(LogLevel.Info, "CompositeOrderManager: External order [{0}] filled (amount: {1} price: {2})",
    //                                 externalOrder.Identity, filledAmount,
    //                                 externalOrder.OrderRequestInfo.RequestedPrice);
    //                childOrderInfo.RemainingOpenSize -= filledAmount;
    //                if (childOrderInfo.RemainingOpenSize != 0m)
    //                {
    //                    this._logger.Log(changeStatus == IntegratorOrderExternalChange.PartiallyFilled ? LogLevel.Info : LogLevel.Error,
    //                                     "Receiving external order {0} for internal order {1} but there is a remaining size {2}",
    //                                     externalOrder.Identity, childOrder.ClientOrderIdentity,
    //                                     childOrderInfo.RemainingOpenSize);
    //                }

    //                string counterpartyTransactionId = orderChangeEventArgs.ExecutionInfo.CounterpartyTransactionId;
    //                string internalTransactionId = orderChangeEventArgs.ExecutionInfo.IntegratorTransactionId;

    //                childOrderInfo.OrderResultDispatchGateway.OnIntegratorDealInternal(
    //                    new IntegratorDealInternal(childOrder.ClientOrderIdentity, childOrderInfo.ParentOrderId,
    //                                               childOrder.ClientIdentity,
    //                                               externalOrder.Identity,
    //                                               counterpartyTransactionId,
    //                                               internalTransactionId,
    //                                               filledAmount, childOrderInfo.RemainingOpenSize == 0m,
    //                                               orderChangeEventArgs.ExecutionInfo.UsedPrice.Value,
    //                                               externalOrder.OrderRequestInfo.Side,
    //                                               externalOrder.OrderRequestInfo.Symbol,
    //                                               externalOrder.IntegratorSentTimeUtc,
    //                                               externalOrder.IntegratorReceivedResultTimeUtc,
    //                                               externalOrder.CounterpartySentResultTimeUtc,
    //                                               externalOrder.CounterpartyTransactionTimeUtc,
    //                                               externalOrder.ParentSession.Counterparty, true,
    //                                               FlowSide.LiquidityTaker, 
    //                                               externalOrder.ParentSession.Counterparty.TradingTargetType, null,
    //                                               childOrder.IntegratedTradingSystemIdentification));

    //                orderChangeEventArgs.ExecutionInfo.AddClientSystemAllocation(childOrder.IntegratedTradingSystemIdentification, filledAmount);

    //                break;
    //            case IntegratorOrderExternalChange.Rejected:
    //            case IntegratorOrderExternalChange.Cancelled:

    //                this._logger.Log(LogLevel.Info,
    //                                     "CompositeOrderManager: External order [{0}] was rejected (mapped to internal order [{1}])",
    //                                     externalOrder.Identity, childOrder.ClientOrderIdentity);

    //                decimal rejectedAmount = orderChangeEventArgs.RejectionInfo.RejectedAmount.Value;

    //                childOrderInfo.OrderResultDispatchGateway.OnCounterpartyRejectedOrder(
    //                    new CounterpartyRejectionInfo(childOrder.ClientOrderIdentity,
    //                                                  childOrder.ClientIdentity,
    //                                                  childOrderInfo.ParentOrderId,
    //                                                  externalOrder.UniqueInternalIdentity,
    //                                                  rejectedAmount,
    //                                                  externalOrder.OrderRequestInfo.RequestedPrice,
    //                                                  childOrder.OrderRequestInfo.Side,
    //                                                  externalOrder.OrderRequestInfo.Symbol,
    //                                                  externalOrder.IntegratorSentTimeUtc,
    //                                                  externalOrder.IntegratorReceivedResultTimeUtc,
    //                                                  externalOrder.CounterpartySentResultTimeUtc,
    //                                                  externalOrder.ParentSession.Counterparty,
    //                                                  orderChangeEventArgs.RejectionInfo.RejectionReason, true,
    //                                                  externalOrder.ParentSession.Counterparty.TradingTargetType,
    //                                                  childOrder.IntegratedTradingSystemIdentification));

    //                //error and add the amount back to order, put it back to _limit or _market list if it's in pending
    //                if (childOrder.CompositeOrderStrategy == CompositeOrderStrategy.AllAtOnceOrImmediateAtMarket)
    //                {
    //                    this._childOrderInfos.Add(childOrder.ClientOrderIdentity, childOrderInfo);
    //                }

    //                orderChangeEventArgs.RejectionInfo.AddClientSystemAllocation(childOrder.IntegratedTradingSystemIdentification, rejectedAmount);

    //                TransformToMarketOrder(childOrder, childOrderInfo);
    //                break;
    //            case IntegratorOrderExternalChange.InBrokenState:
    //                //Log error. if in pending AND this is the last unknown external order - remove
    //                this._logger.Log(LogLevel.Fatal, "CompositeOrderManager: External order [{0}] is in Broken state",
    //                                 externalOrder.Identity);

    //                childOrderInfo.OrderResultDispatchGateway.OnCounterpartyIgnoredOrder(
    //                    new CounterpartyOrderIgnoringInfo(childOrder.ClientOrderIdentity,
    //                                                      childOrder.ClientIdentity,
    //                                                      childOrderInfo.ParentOrderId,
    //                                                      externalOrder.UniqueInternalIdentity,
    //                                                      externalOrder.OrderRequestInfo.SizeBaseAbsInitial,
    //                                                      externalOrder.OrderRequestInfo.RequestedPrice,
    //                                                      childOrder.OrderRequestInfo.Side,
    //                                                      externalOrder.OrderRequestInfo.Symbol,
    //                                                      externalOrder.IntegratorSentTimeUtc,
    //                                                      externalOrder.ParentSession.Counterparty,
    //                                                      externalOrder.ParentSession.Counterparty.TradingTargetType,
    //                                                      childOrder.IntegratedTradingSystemIdentification));

    //                childOrderInfo.OrderResultDispatchGateway.OnIntegratorClientOrderUpdate(
    //                    new BankPoolClientOrderUpdateInfo(childOrder.ClientOrderIdentity, childOrder.ClientIdentity,
    //                                                      ClientOrderStatus.BrokenInIntegrator,
    //                                                      ClientOrderCancelRequestStatus.CancelNotRequested, 0m, 0m, 0m,
    //                                                      string.Empty, childOrder.OrderRequestInfo.RequestedPrice,
    //                                                      childOrder.OrderRequestInfo.Side),
    //                    childOrder.OrderRequestInfo.Symbol);
    //                break;
    //            case IntegratorOrderExternalChange.NonConfirmedDeal:
    //            default:
    //                this._logger.Log(LogLevel.Fatal,
    //                                 "CompositeOrderManager: Unexpected IntegratorOrderExternalChange: {0} for external order: {1}",
    //                                 changeStatus, externalOrder);
    //                break;
    //        }
    //    }


    //    public void OnIntegratorClientOrderUpdate(ClientOrderUpdateInfo clientOrderUpdateInfo, Symbol symbol)
    //    {
    //        LogLevel logLevel = LogLevel.Info;

    //        if (clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.RejectedByIntegrator ||
    //            clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.RemovedFromIntegrator)
    //        {
    //            logLevel = LogLevel.Error;

    //            ChildOrderInfo childOrderInfo;
    //            if (!this._childOrderInfos.TryGetValue(clientOrderUpdateInfo.ClientOrderIdentity, out childOrderInfo))
    //            {
    //                this._logger.Log(LogLevel.Fatal,
    //                                 "CompositeOrderManager receiving an OnClientOrderUpdated [{0}]. Cannot find matching child order info for corresponding order {1}",
    //                                 clientOrderUpdateInfo, clientOrderUpdateInfo.ClientOrderIdentity);
    //            }
    //            else
    //            {
    //                childOrderInfo.OrderResultDispatchGateway.OnIntegratorClientOrderUpdate(
    //                    BankPoolClientOrderUpdateInfo.CreateRemovedClientOrderUpdateInfo(
    //                        clientOrderUpdateInfo.ClientOrderIdentity, clientOrderUpdateInfo.ClientIdentity,
    //                        childOrderInfo.RemainingOpenSize, clientOrderUpdateInfo.RequestedPrice,
    //                        clientOrderUpdateInfo.Side), symbol);
    //            }
    //        }

    //        this._logger.Log(logLevel, "CompositeOrderManager receiving an order update for order {0}: {1}",
    //                         clientOrderUpdateInfo.ClientOrderIdentity, clientOrderUpdateInfo);
    //    }

    //    public void OnIntegratorUnconfirmedDealInternal(IntegratorUnconfirmedDealInternal integratorUnconfirmedDealInternal)
    //    {
    //        this._logger.Log(LogLevel.Fatal, "CompositeOrdersManager receiving unconfirmed deal: {0}", integratorUnconfirmedDealInternal);
    //    }

    //    public void OnIntegratorDealInternal(IntegratorDealInternal integratorDealInternal)
    //    {
    //        ChildOrderInfo childOrderInfo;
    //        if (!this._childOrderInfos.TryGetValue(integratorDealInternal.ClientOrderIdentity, out childOrderInfo))
    //        {
    //            this._logger.Log(LogLevel.Fatal,
    //                             "CompositeOrderManager receiving an OnNewDeal [{0}]. Cannot find matching child order info for corresponding order {1}",
    //                             integratorDealInternal, integratorDealInternal.ClientOrderIdentity);
    //            return;
    //        }

    //        childOrderInfo.RemainingOpenSize -= integratorDealInternal.FilledAmountBaseAbs;
    //        if (childOrderInfo.RemainingOpenSize == 0m)
    //        {
    //            this._childOrderInfos.Remove(integratorDealInternal.ClientOrderIdentity);
    //            this._logger.Log(LogLevel.Info,
    //                             "CompositeOrderManager receiving OnNewDeal {0} for internal order {1} and fully satisfying it - removing it from Composite order lists",
    //                             integratorDealInternal, integratorDealInternal.ClientOrderIdentity,
    //                             childOrderInfo.RemainingOpenSize);
    //        }
    //        else
    //        {
    //            this._logger.Log(LogLevel.Info,
    //                             "CompositeOrderManager receiving OnNewDeal {0} for internal order {1}. There is a remaining size {2} that still needs to be filled",
    //                             integratorDealInternal, integratorDealInternal.ClientOrderIdentity,
    //                             childOrderInfo.RemainingOpenSize);
    //        }

    //        childOrderInfo.OrderResultDispatchGateway.OnIntegratorDealInternal(
    //            new IntegratorDealInternal(integratorDealInternal.ClientOrderIdentity, childOrderInfo.ParentOrderId,
    //                                       integratorDealInternal.ClientIdentity,
    //                                       integratorDealInternal.IntegratorOrderIdentity,
    //                                       integratorDealInternal.CounterpartyTransactionIdentity,
    //                                       integratorDealInternal.InternalTransactionIdentity,
    //                                       integratorDealInternal.FilledAmountBaseAbs,
    //                                       integratorDealInternal.IsTerminalFill,
    //                                       integratorDealInternal.Price,
    //                                       integratorDealInternal.Direction,
    //                                       integratorDealInternal.Symbol,
    //                                       integratorDealInternal.IntegratorSentOriginatingOrderTimeUtc,
    //                                       integratorDealInternal.IntegratorReceivedExecutionTimeUtc,
    //                                       integratorDealInternal.CounterpartySentExecutionTimeUtc,
    //                                       integratorDealInternal.CounterpartyTransactionTimeUtc,
    //                                       integratorDealInternal.Counterparty, false,
    //                                       integratorDealInternal.FlowSide,
    //                                       integratorDealInternal.Counterparty.TradingTargetType, null,
    //                                       integratorDealInternal.IntegratedTradingSystemIdentification));
    //    }

    //    public void OnCounterpartyRejectedOrder(CounterpartyRejectionInfo counterpartyRejectionInfo)
    //    {
    //        ChildOrderInfo childOrderInfo;
    //        if (!this._childOrderInfos.TryGetValue(counterpartyRejectionInfo.ClientOrderIdentity, out childOrderInfo))
    //        {
    //            this._logger.Log(LogLevel.Fatal,
    //                             "CompositeOrderManager receiving an OnCounterpartyRejectedIntegratorOrder [{0}]. Cannot find matching child order info for corresponding order {1}",
    //                             counterpartyRejectionInfo, counterpartyRejectionInfo.ClientOrderIdentity);
    //            return;
    //        }

    //        childOrderInfo.OrderResultDispatchGateway.OnCounterpartyRejectedOrder(
    //            new CounterpartyRejectionInfo(counterpartyRejectionInfo.ClientOrderIdentity,
    //                                          counterpartyRejectionInfo.ClientIdentity,
    //                                          childOrderInfo.ParentOrderId,
    //                                          counterpartyRejectionInfo.IntegratorOrderIdentity,
    //                                          counterpartyRejectionInfo.RejectedAmountBaseAbs,
    //                                          counterpartyRejectionInfo.RejectedPrice,
    //                                          counterpartyRejectionInfo.Direction, counterpartyRejectionInfo.Symbol,
    //                                          counterpartyRejectionInfo.IntegratorSentTimeUtc,
    //                                          counterpartyRejectionInfo.IntegratorReceivedRejectionTimeUtc,
    //                                          counterpartyRejectionInfo.CounterpartySentRejectionTimeUtc,
    //                                          counterpartyRejectionInfo.Counterparty,
    //                                          counterpartyRejectionInfo.CounterpartyRejectionReason, false,
    //                                          counterpartyRejectionInfo.Counterparty.TradingTargetType,
    //                                          counterpartyRejectionInfo.IntegratedTradingSystemIdentification));
    //    }

    //    public void OnCounterpartyIgnoredOrder(CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo)
    //    {
    //        ChildOrderInfo childOrderInfo;
    //        if (!this._childOrderInfos.TryGetValue(counterpartyOrderIgnoringInfo.ClientOrderIdentity, out childOrderInfo))
    //        {
    //            this._logger.Log(LogLevel.Fatal,
    //                             "CompositeOrderManager receiving an OnCounterpartyIgnoredIntegratorOrder [{0}]. Cannot find matching child order info for corresponding order {1}",
    //                             counterpartyOrderIgnoringInfo, counterpartyOrderIgnoringInfo.ClientOrderIdentity);
    //            return;
    //        }

    //        childOrderInfo.OrderResultDispatchGateway.OnCounterpartyIgnoredOrder(
    //            new CounterpartyOrderIgnoringInfo(counterpartyOrderIgnoringInfo.ClientOrderIdentity,
    //                                              counterpartyOrderIgnoringInfo.ClientIdentity,
    //                                              childOrderInfo.ParentOrderId,
    //                                              counterpartyOrderIgnoringInfo.IntegratorOrderIdentity,
    //                                              counterpartyOrderIgnoringInfo.IgnoredAmountBaseAbs,
    //                                              counterpartyOrderIgnoringInfo.IgnoredPrice,
    //                                              counterpartyOrderIgnoringInfo.Direction,
    //                                              counterpartyOrderIgnoringInfo.Symbol,
    //                                              counterpartyOrderIgnoringInfo.IntegratorSentTimeUtc,
    //                                              counterpartyOrderIgnoringInfo.Counterparty,
    //                                              counterpartyOrderIgnoringInfo.Counterparty.TradingTargetType,
    //                                              counterpartyOrderIgnoringInfo.IntegratedTradingSystemIdentification));
    //    }

    //    private class ChildOrderInfo
    //    {
    //        public IUnicastInfoForwarder OrderResultDispatchGateway { get; set; }
    //        public string ParentOrderId { get; set; }
    //        public IBankPoolClientOrder ParentOrder { get; set; }
    //        public decimal RemainingOpenSize { get; set; }
    //    }

    //    private class CompositeClientOrderInternal : IBankPoolClientOrder
    //    {
    //        public CompositeClientOrderInternal(IBankPoolClientOrder clientOrder)
    //        {
    //            this.ClientOrderIdentity = clientOrder.ClientOrderIdentity;
    //            this.ClientIdentity = clientOrder.ClientIdentity;
    //            this.OrderStatus = clientOrder.OrderStatus;
    //            this.SizeBaseAbsFilled = clientOrder.SizeBaseAbsFilled;
    //            this.BankPoolClientOrderRequestInfo = clientOrder.BankPoolClientOrderRequestInfo;
    //            this.CreatedUtc = clientOrder.CreatedUtc;
    //            this.IsComposite = clientOrder.IsComposite;
    //            this.CompositeOrderStrategy = clientOrder.CompositeOrderStrategy;

    //            if (clientOrder.ChildOrders != null && clientOrder.ChildOrders.Count > 0)
    //            {
    //                this.ChildOrders = new List<IBankPoolClientOrder>(clientOrder.ChildOrders.Count);
    //                foreach (IBankPoolClientOrder childOrder in clientOrder.ChildOrders)
    //                {
    //                    CompositeClientOrderInternal childCompositeOrder = new CompositeClientOrderInternal(childOrder);
    //                    childCompositeOrder.CompositeOrderStrategy = clientOrder.CompositeOrderStrategy;
    //                    this.ChildOrders.Add(childCompositeOrder);
    //                }
    //            }
    //        }

    //        public IBankPoolClientOrder Clone()
    //        {
    //            return new CompositeClientOrderInternal(this);
    //        }

    //        public string ClientOrderIdentity { get; private set; }

    //        public string ClientIdentity { get; private set; }

    //        public ClientOrderStatus OrderStatus { get; private set; }

    //        public decimal SizeBaseAbsFilled { get; private set; }

    //        public decimal SizeBaseAbsActive { get; private set; }

    //        public ClientOrderRequestInfoBase OrderRequestInfo
    //        {
    //            get { return this.BankPoolClientOrderRequestInfo; }
    //        }

    //        public BankPoolClientOrderRequestInfo BankPoolClientOrderRequestInfo { get; set; }

    //        public DateTime CreatedUtc { get; private set; }

    //        public bool IsComposite { get; private set; }

    //        public CompositeOrderStrategy CompositeOrderStrategy { get; private set; }

    //        public List<IBankPoolClientOrder> ChildOrders { get; private set; }

    //        public void OverrideOrderRequestInfo(BankPoolClientOrderRequestInfo orderRequestInfo)
    //        {
    //            this.BankPoolClientOrderRequestInfo = orderRequestInfo;
    //        }

    //        public IntegratedTradingSystemIdentification IntegratedTradingSystemIdentification {
    //            get { return null; }
    //            set { throw new NotImplementedException("IntegratedTradingSystemIdentification not implemented for composite orders"); }
    //        }

    //        public override string ToString()
    //        {
    //            if (IsComposite)
    //            {
    //                return
    //                    string.Format("Composite client order [{0}] (from {1}), status: {2}. Child orders:{3}{4}",
    //                                  ClientOrderIdentity, CreatedUtc, OrderStatus, Environment.NewLine,
    //                                  string.Join(Environment.NewLine, this.ChildOrders.Select(cho => cho.ToString())));
    //            }
    //            else
    //            {
    //                return
    //                string.Format("Client order [{0}] (from {4}), status: {1}, filled: {2}, active: {3}. OrderRequest: {5}",
    //                              ClientOrderIdentity, OrderStatus, SizeBaseAbsFilled, SizeBaseAbsActive, CreatedUtc, OrderRequestInfo);
    //            }

    //        }

    //        public TradingTargetType TradingTargetType
    //        {
    //            get { return TradingTargetType.BankPool; }
    //        }
    //    }
    //}
}
