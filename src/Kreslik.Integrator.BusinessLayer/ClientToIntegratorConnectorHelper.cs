﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinessLayer
{
    //Helper class for connecting local client base or local proxy of a remote client to Intagrator 
    // Be careful! - local proxy of a remote client represents multiple distinct clients!
    public class ClientToIntegratorConnectorHelper //: IRemoteClientGatewayProxy
    {
        private ILogger _logger;
        private IBookTopProvidersStore _bookTopProvidersStore;
        private IVenueDataProviderStore _venueDataProviderStore;
        private IOrderManagementEx _orderManagement;
        private IEnumerable<IBroadcastInfosStore> _broadcastInfosStores;
        private ClientGatewayEventsDispatcherBase _eventsDispatcher;
        private ITakerUnicastInfoForwarder _unicastInfoForwarder;
        private IUnicastInfoForwardingHub _unicastInfoForwardingHub = UnicastInfoForwardingHub.Instance;

        public ClientToIntegratorConnectorHelper(ILogger logger, IBookTopProvidersStore bookTopProvidersStore,
                                                 IVenueDataProviderStore venueDataProviderStore,
                                                 IOrderManagementEx orderManagement,
                                                 IEnumerable<IBroadcastInfosStore> broadcastInfosStores,
                                                 ClientGatewayEventsDispatcherBase eventsDispatcher)
        {
            this._logger = logger;
            this._bookTopProvidersStore = bookTopProvidersStore;
            this._venueDataProviderStore = venueDataProviderStore;
            this._orderManagement = orderManagement;
            this._broadcastInfosStores = broadcastInfosStores;
            this._eventsDispatcher = eventsDispatcher;
            this._unicastInfoForwarder = new UnicastInfoForwarder(_unicastInfoForwardingHub, eventsDispatcher);

            foreach (IBroadcastInfosStore broadcastInfosStore in broadcastInfosStores)
            {
                broadcastInfosStore.NewBroadcastInfo += eventsDispatcher.OnIntegratorBroadcastInfo;
                foreach (IntegratorBroadcastInfoBase orderTransmissionArgs in broadcastInfosStore.ExistingInfos)
                {
                    eventsDispatcher.OnIntegratorBroadcastInfo(orderTransmissionArgs);
                }
            }
        }

        private void SendFirstPriceUpdate(IBookTop<PriceObjectInternal> sender, PriceObjectInternal price, DateTime timeStamp)
        {
            if (price == null || PriceObjectInternal.IsNullPrice(price))
            {
                this._eventsDispatcher.OnPriceUpdate(new SinglePriceChangeEventArgs(PriceUpdateType.PriceOnTopOfBookRemoved, timeStamp, price, sender.TradingTargetType));
            }
            else
            {
                this._eventsDispatcher.OnPriceUpdate(new SinglePriceChangeEventArgs(PriceUpdateType.PriceOnTopOfBookAdded, timeStamp, price, sender.TradingTargetType));
            }
        }

        private IntegratorRequestResult Subscribe(IntegratorTransferableSubscriptionRequest subscriptionRequestInfo)
        {
            if (subscriptionRequestInfo.SubscriptionType == SubscriptionType.ToBPricesSubscription)
            {
                return this.SubscribeToToBs(subscriptionRequestInfo);
            }
            else if (subscriptionRequestInfo.SubscriptionType == SubscriptionType.VenueDealsSubscriptions)
            {
                return this.SubscribeToAllVenueDealsReports(subscriptionRequestInfo);
            }
            else if (subscriptionRequestInfo.SubscriptionType == SubscriptionType.IndividualPricesSubscription)
            {
                return this.SubscribeToIndividualPrices(subscriptionRequestInfo);
            }
            else
            {
                string error =
                        string.Format(
                            "Receiving unexpected type of unsubscription: {0}", subscriptionRequestInfo);
                this._logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }
        }

        private IntegratorRequestResult Unsubscribe(IntegratorTransferableSubscriptionRequest subscriptionRequestInfo)
        {
            if (subscriptionRequestInfo.SubscriptionType == SubscriptionType.ToBPricesSubscription)
            {
                return this.UnsubscribeFromToBs(subscriptionRequestInfo);
            }
            else if (subscriptionRequestInfo.SubscriptionType == SubscriptionType.VenueDealsSubscriptions)
            {
                return this.UnsubscribeFromAllVenueDealsReports(subscriptionRequestInfo);
            }
            else if (subscriptionRequestInfo.SubscriptionType == SubscriptionType.IndividualPricesSubscription)
            {
                return this.UnsubscribeFromIndividualPrices(subscriptionRequestInfo);
            }
            else
            {
                string error =
                        string.Format(
                            "Receiving unexpected type of unsubscription: {0}", subscriptionRequestInfo);
                this._logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }
        }

        private IntegratorRequestResult SubscribeToToBs(IntegratorTransferableSubscriptionRequest subscriptionRequestInfo)
        {
            //Try to unsubscribe first - for the case if client send duplicate request by error
            this.UnsubscribeFromToBs(subscriptionRequestInfo);

            IBookTopProvider<PriceObjectInternal> bookTopProvider =
                this._bookTopProvidersStore.GetBookTopProvider(subscriptionRequestInfo.Counterparty);

            //Handle cases where provider is not available (e.g. attempt to subscribe to HTF ToB on Integrator without HTF configured)
            if (bookTopProvider == null)
            {
                return
                    IntegratorRequestResult.CreateFailedResult(
                        "Price provider for requested subscription (and counterparty) is not available in this instance of Integrator");
            }

            var askPriceBook = bookTopProvider.GetAskPriceBook(subscriptionRequestInfo.Symbol);
            var bidPriceBook = bookTopProvider.GetBidPriceBook(subscriptionRequestInfo.Symbol);

            //First send the actual state of quotebook
            PriceObjectInternal askTop = askPriceBook.MaxNodeInternal;
            PriceObjectInternal bidTop = bidPriceBook.MaxNodeInternal;
            this.SendFirstPriceUpdate(askPriceBook, askTop, askPriceBook.MaxNodeUpdatedUtc);
            this.SendFirstPriceUpdate(bidPriceBook, bidTop, bidPriceBook.MaxNodeUpdatedUtc);
            askTop.Release();
            bidTop.Release();

            //and then subscribe for all subsequent updates
            askPriceBook.BookTopImproved += this._eventsDispatcher.OnPriceImproved;
            bidPriceBook.BookTopImproved += this._eventsDispatcher.OnPriceImproved;

            askPriceBook.BookTopDeterioratedInternal += this._eventsDispatcher.OnPriceDeteriorated;
            bidPriceBook.BookTopDeterioratedInternal += this._eventsDispatcher.OnPriceDeteriorated;

            askPriceBook.BookTopReplaced += this._eventsDispatcher.OnPriceReplaced;
            bidPriceBook.BookTopReplaced += this._eventsDispatcher.OnPriceReplaced;

            return IntegratorRequestResult.GetSuccessResult();
        }

        private IntegratorRequestResult UnsubscribeFromToBs(IntegratorTransferableSubscriptionRequest subscriptionRequestInfo)
        {
            IBookTopProvider<PriceObjectInternal> bookTopProvider =
                this._bookTopProvidersStore.GetBookTopProvider(subscriptionRequestInfo.Counterparty);

            //Handle cases where provider is not available (e.g. attempt to subscribe to HTF ToB on Integrator without HTF configured)
            if (bookTopProvider == null)
            {
                return
                    IntegratorRequestResult.CreateFailedResult(
                        "Price provider for requested subscription (and counterparty) is not available in this instance of Integrator");
            }

            bookTopProvider.GetAskPriceBook(subscriptionRequestInfo.Symbol).BookTopImproved -=
                this._eventsDispatcher.OnPriceImproved;
            bookTopProvider.GetBidPriceBook(subscriptionRequestInfo.Symbol).BookTopImproved -=
                this._eventsDispatcher.OnPriceImproved;

            bookTopProvider.GetAskPriceBook(subscriptionRequestInfo.Symbol).BookTopDeterioratedInternal -=
                this._eventsDispatcher.OnPriceDeteriorated;
            bookTopProvider.GetBidPriceBook(subscriptionRequestInfo.Symbol).BookTopDeterioratedInternal -=
                this._eventsDispatcher.OnPriceDeteriorated;

            bookTopProvider.GetAskPriceBook(subscriptionRequestInfo.Symbol).BookTopReplaced -=
                this._eventsDispatcher.OnPriceReplaced;
            bookTopProvider.GetBidPriceBook(subscriptionRequestInfo.Symbol).BookTopReplaced -=
                this._eventsDispatcher.OnPriceReplaced;

            return IntegratorRequestResult.GetSuccessResult();
        }

        private IntegratorRequestResult SubscribeToAllVenueDealsReports(
            IntegratorTransferableSubscriptionRequest subscriptionRequestInfo)
        {
            IVenueDataProvider venueDataProvider =
                this._venueDataProviderStore.GetVenueDataProvider(subscriptionRequestInfo.Counterparty);

            //Handle cases where provider is not available (e.g. attempt to subscribe to HTF data on Integrator without HTF configured)
            if (venueDataProvider == null)
            {
                return
                    IntegratorRequestResult.CreateFailedResult(
                        "VenueData provider for requested subscription (and counterparty) is not available in this instance of Integrator");
            }

            //this handles accident multiple subscriptions
            venueDataProvider.AddNewVenueDealHandler(subscriptionRequestInfo.Symbol, this._eventsDispatcher.OnPriceUpdate);

            return IntegratorRequestResult.GetSuccessResult();
        }

        private IntegratorRequestResult UnsubscribeFromAllVenueDealsReports(
            IntegratorTransferableSubscriptionRequest subscriptionRequestInfo)
        {
            IVenueDataProvider venueDataProvider =
                this._venueDataProviderStore.GetVenueDataProvider(subscriptionRequestInfo.Counterparty);

            //Handle cases where provider is not available (e.g. attempt to subscribe to HTF data on Integrator without HTF configured)
            if (venueDataProvider == null)
            {
                return
                    IntegratorRequestResult.CreateFailedResult(
                        "VenueData provider for requested unsubscription (and counterparty) is not available in this instance of Integrator");
            }

            venueDataProvider.RemoveNewVenueDealHandler(subscriptionRequestInfo.Symbol,
                                                        this._eventsDispatcher.OnPriceUpdate);

            return IntegratorRequestResult.GetSuccessResult();
        }

        private IntegratorRequestResult SubscribeToIndividualPrices(IntegratorTransferableSubscriptionRequest subscriptionRequestInfo)
        {
            //Try to unsubscribe first - for the case if client send duplicate request by error
            this.UnsubscribeFromIndividualPrices(subscriptionRequestInfo);

            //Subscribe to following only for bank targets
            if (subscriptionRequestInfo.Counterparty == Counterparty.NULL ||
                subscriptionRequestInfo.Counterparty.TradingTargetType == TradingTargetType.BankPool)
            {
                IBookTopProvider<PriceObjectInternal> bookTopProvider =
                this._bookTopProvidersStore.GetBookTopProvider(subscriptionRequestInfo.Counterparty);

                //Handle cases where provider is not available (e.g. attempt to subscribe to HTF ToB on Integrator without HTF configured)
                if (bookTopProvider == null)
                {
                    return
                        IntegratorRequestResult.CreateFailedResult(
                            "Price provider for requested subscription (and counterparty) is not available in this instance of Integrator");
                }

                bookTopProvider.GetAskPriceBook(subscriptionRequestInfo.Symbol).NewNodeArrived +=
                    this._eventsDispatcher.OnNewPriceArrived;
                bookTopProvider.GetBidPriceBook(subscriptionRequestInfo.Symbol).NewNodeArrived +=
                    this._eventsDispatcher.OnNewPriceArrived;

                bookTopProvider.GetAskPriceBook(subscriptionRequestInfo.Symbol).ExistingNodeInvalidated +=
                    this._eventsDispatcher.OnExistingPriceInvalidated;
                bookTopProvider.GetBidPriceBook(subscriptionRequestInfo.Symbol).ExistingNodeInvalidated +=
                    this._eventsDispatcher.OnExistingPriceInvalidated;

                bookTopProvider.GetAskPriceBook(subscriptionRequestInfo.Symbol).NodesRemoved +=
                    this._eventsDispatcher.OnPricesRemoved;
                bookTopProvider.GetBidPriceBook(subscriptionRequestInfo.Symbol).NodesRemoved +=
                    this._eventsDispatcher.OnPricesRemoved;
            }

            return IntegratorRequestResult.GetSuccessResult();
        }

        private IntegratorRequestResult UnsubscribeFromIndividualPrices(IntegratorTransferableSubscriptionRequest subscriptionRequestInfo)
        {
            //Subscribe to following only for bank targets
            if (subscriptionRequestInfo.Counterparty == Counterparty.NULL ||
                subscriptionRequestInfo.Counterparty.TradingTargetType == TradingTargetType.BankPool)
            {
                IBookTopProvider<PriceObjectInternal> bookTopProvider =
                this._bookTopProvidersStore.GetBookTopProvider(subscriptionRequestInfo.Counterparty);

                //Handle cases where provider is not available (e.g. attempt to subscribe to HTF ToB on Integrator without HTF configured)
                if (bookTopProvider == null)
                {
                    return
                        IntegratorRequestResult.CreateFailedResult(
                            "Price provider for requested subscription (and counterparty) is not available in this instance of Integrator");
                }

                bookTopProvider.GetAskPriceBook(subscriptionRequestInfo.Symbol).NewNodeArrived -=
                    this._eventsDispatcher.OnNewPriceArrived;
                bookTopProvider.GetBidPriceBook(subscriptionRequestInfo.Symbol).NewNodeArrived -=
                    this._eventsDispatcher.OnNewPriceArrived;

                bookTopProvider.GetAskPriceBook(subscriptionRequestInfo.Symbol).ExistingNodeInvalidated -=
                    this._eventsDispatcher.OnExistingPriceInvalidated;
                bookTopProvider.GetBidPriceBook(subscriptionRequestInfo.Symbol).ExistingNodeInvalidated -=
                    this._eventsDispatcher.OnExistingPriceInvalidated;

                bookTopProvider.GetAskPriceBook(subscriptionRequestInfo.Symbol).NodesRemoved -=
                    this._eventsDispatcher.OnPricesRemoved;
                bookTopProvider.GetBidPriceBook(subscriptionRequestInfo.Symbol).NodesRemoved -=
                    this._eventsDispatcher.OnPricesRemoved;
            }

            return IntegratorRequestResult.GetSuccessResult();
        }

        public IntegratorRequestResult SubmitRequest(IntegratorRequestInfo requestInfo)
        {
            try
            {
                return SubmitRequestInternal(requestInfo);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Unexpected exception during submiting Integrator action");
                return IntegratorRequestResult.CreateFailedResult("Unexpected exception order manager action: " + e);
            }
        }

        private IntegratorRequestResult SubmitRequestInternal(IntegratorRequestInfo requestInfo)
        {
            if (requestInfo is ClientOrderBase)
            {
                return this._orderManagement.RegisterOrder(requestInfo as ClientOrderBase, this._unicastInfoForwarder);
            }
            else if(requestInfo is CancelOrderRequestInfo)
            {
                return this._orderManagement.CancelOrder(requestInfo as CancelOrderRequestInfo, this._unicastInfoForwarder);
            }
            //else if (requestInfo is IntegratorDealInternalFinalizationRequest)
            //{
            //    return this._orderManagement.FinalizeDeal(requestInfo as IntegratorDealInternalFinalizationRequest, this._unicastInfoForwarder);
            //}
            else if (requestInfo is IntegratorCancelAllOrdersForSingleClientRequestInfo)
            {
                return this._orderManagement.CancelAllClientOrders((requestInfo as IntegratorCancelAllOrdersForSingleClientRequestInfo).IntegratorClientIdenetity, this._unicastInfoForwarder);
            }
            else if (requestInfo is IntegratorTransferableSubscriptionRequest)
            {
                IntegratorTransferableSubscriptionRequest integratorSubscriptionRequestInfo =
                    requestInfo as IntegratorTransferableSubscriptionRequest;

                if (integratorSubscriptionRequestInfo.Subscribe)
                {
                    return this.Subscribe(integratorSubscriptionRequestInfo);
                }
                else
                {
                    return this.Unsubscribe(integratorSubscriptionRequestInfo);
                }
            }
            else if (requestInfo is MulticastRequestInfo)
            {
                MulticastRequestInfo forwardingRequest = requestInfo as MulticastRequestInfo;
                _unicastInfoForwardingHub.RegisterForwardingRequest(forwardingRequest, this._eventsDispatcher.OnIntegratorUnicastInfo);
                return IntegratorRequestResult.GetSuccessResult();
            }
            else
            {
                string error = string.Format(
                            "Receiving IntegratorRequestInfo of unknown type: {0}, info: {1}. Splitter will not route this request",
                            requestInfo.GetType(), requestInfo);
                _logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }
        }

        //this is invoked from dispatch gateway, in try-catch block
        public void CleanupContractWithIntegrator()
        {
            foreach (IBookTopProvider<PriceObjectInternal> bookTopProvider in this._bookTopProvidersStore.AllBookTopProviders.Where(o => o != null))
            {
                bookTopProvider.BookTopImproved -= this._eventsDispatcher.OnPriceImproved;
                bookTopProvider.BookTopDeterioratedInternal -= this._eventsDispatcher.OnPriceDeteriorated;
                bookTopProvider.BookTopReplaced -= this._eventsDispatcher.OnPriceReplaced;
                bookTopProvider.NewNodeArrived -= this._eventsDispatcher.OnNewPriceArrived;
                bookTopProvider.ExistingNodeInvalidated -= this._eventsDispatcher.OnExistingPriceInvalidated;
                bookTopProvider.NodesRemoved -= this._eventsDispatcher.OnPricesRemoved;
            }

            foreach (
                IVenueDataProvider venueDataProvider in
                    _venueDataProviderStore.AllVenueDataProviders.Where(o => o != null))
            {
                venueDataProvider.RemoveAllOccurencesOfNewVenueDealHandler(this._eventsDispatcher.OnPriceUpdate);
            }

            foreach (IBroadcastInfosStore broadcastInfosStore in this._broadcastInfosStores)
            {
                broadcastInfosStore.NewBroadcastInfo -= this._eventsDispatcher.OnIntegratorBroadcastInfo;
            }

            //this will cancel all orders
            if(this._orderManagement != null)
                this._orderManagement.CancelAllMyOrders(this._unicastInfoForwarder);

            this._unicastInfoForwardingHub.RemoveAllSubscriptions(this._eventsDispatcher.OnIntegratorUnicastInfo);
        }
    }
}
