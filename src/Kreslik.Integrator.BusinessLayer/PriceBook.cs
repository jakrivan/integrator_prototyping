﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.FIXMessaging;
using System.Threading;

namespace Kreslik.Integrator.BusinessLayer
{
    public class FirmPoolBookStore : IPriceBookStore, IPriceChangesConsumer
    {
        //
        //By editing this field the custom book can be configured
        //
        private Counterparty[] _SUPPORTED_COUNTERPARTIES = new Counterparty[] { Counterparty.L01, Counterparty.FC1, Counterparty.H4T, Counterparty.LM2 };

        private ILogger _logger;
        private readonly int _cptsNum;
        private PriceBookInternal[][] _books = new PriceBookInternal[2][]
        {
            new PriceBookInternal[Symbol.ValuesCount],
            new PriceBookInternal[Symbol.ValuesCount]
        };
        private List<int> _shortcutSymbolList = new List<int>(8);

        public FirmPoolBookStore(ILogger logger)
        {
            this._logger = logger;
            this._cptsNum = GetCptsNum(_SUPPORTED_COUNTERPARTIES);
        }

        public IChangeablePriceBook<PriceObjectInternal, Counterparty> GetChangeableBidPriceBook(Symbol symbol)
        {
            return this.CreateBook(PriceSide.Bid, symbol);
        }

        public IChangeablePriceBook<PriceObjectInternal, Counterparty> GetChangeableAskPriceBook(Symbol symbol)
        {
            return this.CreateBook(PriceSide.Ask, symbol);
        }

        private int GetCptsNum(Counterparty[] supportedCounterparties)
        {
            int cptsNum = 0;
            foreach (Counterparty counterparty in supportedCounterparties)
            {
                if (counterparty == Counterparty.L01)
                    cptsNum += SessionsSettings.FIXChannel_LM1Behavior.L01LayersNum;
                else if ((counterparty is LmaxCounterparty) &&
                         ((LmaxCounterparty) counterparty).IsSubLayerPseoudoCounterparty)
                    continue;
                else
                    cptsNum++;
            }

            return cptsNum;
        }

        private PriceBookInternal CreateBook(PriceSide side, Symbol symbol)
        {
            PriceBookInternal pb = _books[(int) side][(int) symbol];

            if (pb == null)
            {
                pb = new PriceBookInternal
                    (_cptsNum,
                        side == PriceSide.Ask
                            ? (IComparer<PriceObjectInternal>) AskPriceComparerInternal.Default
                            : (IComparer<PriceObjectInternal>) BidPriceComparerInternal.Default,
                        side == PriceSide.Ask
                            ? (IComparer<decimal>) AskTopPriceComparerInternal.Default
                            : (IComparer<decimal>) BidTopPriceComparerInternal.Default,
                        side == PriceSide.Ask
                            ? (IComparer<PriceObjectInternal>) AskTopPriceComparerInternal.Default
                            : (IComparer<PriceObjectInternal>) BidTopPriceComparerInternal.Default,
                        PriceCounterpartEqualityComparerInternal.Default,
                        PriceObjectInternal.GetNullBidInternal(symbol),
                        this._logger);
                _books[(int) side][(int) symbol] = pb;
                if (_shortcutSymbolList != null && !_shortcutSymbolList.Contains((int) symbol))
                {
                    _shortcutSymbolList.Add((int) symbol);
                    if (_shortcutSymbolList.Count > 8)
                        _shortcutSymbolList = null;
                }
            }

            return pb;
        }

        #region IPriceChangesConsumer
        public void OnNewPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if (price == null)
                return;

            if(_shortcutSymbolList != null && !_shortcutSymbolList.Contains((int)price.Symbol))
                return;

            PriceBookInternal pb = this._books[(int)price.Side][(int)price.Symbol];
            if (pb == null)
                return;

            if (price.Price > 0 && price.SizeBaseAbsRemaining > 0)
                pb.AddOrUpdate(price, ref heldWithoutAcquireCall);
            else
                pb.Remove(price.Counterparty);

        }

        public void OnIgnoredPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if (price == null)
                return;

            if (this.ConsumesIgnoredPrices)
                this.OnNewPrice(price, ref heldWithoutAcquireCall);
            else
                this.OnPriceCancel(price.Symbol, price.Counterparty, price.Side);
        }

        public void OnNewNonExecutablePrice(PriceObjectInternal price)
        {
            if (price == null)
                return;

            bool _ = true;
            if (this.ConsumesIgnoredPrices)
                this.OnNewPrice(price, ref _);
            else
                this.OnPriceCancel(price.Symbol, price.Counterparty, price.Side);
        }

        public void OnQuoteCancel(Symbol symbol, Counterparty counterparty)
        {
            this.OnPriceCancel(symbol, counterparty, PriceSide.Ask);
            this.OnPriceCancel(symbol, counterparty, PriceSide.Bid);
        }

        public void OnPriceCancel(Symbol symbol, Counterparty counterparty, PriceSide side)
        {
            if (_shortcutSymbolList != null && !_shortcutSymbolList.Contains((int)symbol))
                return;

            PriceBookInternal pb = this._books[(int)side][(int)symbol];
            
            if(pb != null)
                pb.Remove(counterparty);
        }

        public void OnPriceInvalidate(Symbol symbol, PriceSide side, Counterparty counterparty, Guid integratorPriceIdnetity,
            DateTime integratorPriceReceivedUtc)
        {
            PriceBookInternal pb = this._books[(int)side][(int)symbol];

            if (integratorPriceIdnetity != Guid.Empty && pb != null)
                pb.RemoveIdentical(integratorPriceIdnetity);
        }

        public bool ConsumesDataFromCounterparty(Counterparty counterparty)
        {
            return _SUPPORTED_COUNTERPARTIES.Contains(counterparty);
        }

        public bool ConsumesMDDataIfOFNotRunning(Counterparty counterparty)
        {
            return false;
        }

        public bool ConsumesIgnoredPrices
        {
            get { return false; }
        }

        public bool ConsumesNonExecutablePrices
        {
            get { return false; }
        }
        #endregion /#region IPriceChangesConsumer
    }

    public class PriceBookStore : IPriceBookStoreEx, IPriceChangesConsumer
    {
        private PriceBookInternal[][] _books = new PriceBookInternal[2][]
        {
            new PriceBookInternal[Symbol.ValuesCount],
            new PriceBookInternal[Symbol.ValuesCount]
        };


        public IBookTopExtended<PriceObjectInternal, Counterparty> GetBidPriceBook(Symbol symbol)
        {
            return this._books[(int)PriceSide.Bid][(int)symbol];
        }

        public IBookTopExtended<PriceObjectInternal, Counterparty> GetAskPriceBook(Symbol symbol)
        {
            return this._books[(int)PriceSide.Ask][(int)symbol];
        }

        public IChangeablePriceBook<PriceObjectInternal, Counterparty> GetChangeableBidPriceBook(Symbol symbol)
        {
            return this._books[(int)PriceSide.Bid][(int)symbol];
        }

        public IChangeablePriceBook<PriceObjectInternal, Counterparty> GetChangeableAskPriceBook(Symbol symbol)
        {
            return this._books[(int)PriceSide.Ask][(int)symbol];
        }

        public PriceObjectInternal MaxNodeInternal { get { throw new NotImplementedException("PriceBookStore cannot have one single MaxNode"); } }

        public PriceObjectInternal MaxNode { get { throw new NotImplementedException("PriceBookStore cannot have one single MaxNode"); } }

        public DateTime MaxNodeUpdatedUtc { get { throw new NotImplementedException("PriceBookStore cannot have one single MaxNode"); } }

        public TradingTargetType TradingTargetType
        {
            get { return TradingTargetType.BankPool; }
        }

        public event BookPriceUpdate<PriceObjectInternal> BookTopImproved
        {
            add
            {
                foreach (PriceBookInternal book in _books.SelectMany(b => b))
                {
                    book.BookTopImproved += value;
                }
            }
            remove
            {
                foreach (PriceBookInternal book in _books.SelectMany(b => b))
                {
                    book.BookTopImproved -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> BookTopDeterioratedInternal
        {
            add
            {
                foreach (PriceBookInternal book in _books.SelectMany(b => b))
                {
                    book.BookTopDeterioratedInternal += value;
                }
            }
            remove
            {
                foreach (PriceBookInternal book in _books.SelectMany(b => b))
                {
                    book.BookTopDeterioratedInternal -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> BookTopReplaced
        {
            add
            {
                foreach (PriceBookInternal book in _books.SelectMany(b => b))
                {
                    book.BookTopReplaced += value;
                }
            }
            remove
            {
                foreach (PriceBookInternal book in _books.SelectMany(b => b))
                {
                    book.BookTopReplaced -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> NewNodeArrived
        {
            add
            {
                foreach (PriceBookInternal book in _books.SelectMany(b => b))
                {
                    book.NewNodeArrived += value;
                }
            }
            remove
            {
                foreach (PriceBookInternal book in _books.SelectMany(b => b))
                {
                    book.NewNodeArrived -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> ExistingNodeInvalidated
        {
            add
            {
                foreach (PriceBookInternal book in _books.SelectMany(b => b))
                {
                    book.ExistingNodeInvalidated += value;
                }
            }
            remove
            {
                foreach (PriceBookInternal book in _books.SelectMany(b => b))
                {
                    book.ExistingNodeInvalidated -= value;
                }
            }
        }

        public event BookMultiplePricesUpdate<PriceObjectInternal, Counterparty> NodesRemoved
        {
            add
            {
                foreach (PriceBookInternal book in _books.SelectMany(b => b))
                {
                    book.NodesRemoved += value;
                }
            }
            remove
            {
                foreach (PriceBookInternal book in _books.SelectMany(b => b))
                {
                    book.NodesRemoved -= value;
                }
            }
        }

        public PriceBookStore(ILogger logger)
        {
            for (int idx = 0; idx < Symbol.ValuesCount; idx++)
            {
                BidTopPriceComparerInternal bidTopPriceComparer = BidTopPriceComparerInternal.Default;
                AskTopPriceComparerInternal askTopPriceComparer = AskTopPriceComparerInternal.Default;
                BidPriceComparerInternal bidPriceComparer = BidPriceComparerInternal.Default;
                AskPriceComparerInternal askPriceComparer = AskPriceComparerInternal.Default;
                PriceCounterpartEqualityComparerInternal priceCounterpartEqualityComparer = PriceCounterpartEqualityComparerInternal.Default;

                _books[(int)PriceSide.Bid][idx] = new PriceBookInternal
                    (Counterparty.ValuesCount, bidPriceComparer, bidTopPriceComparer, bidTopPriceComparer, priceCounterpartEqualityComparer,
                     PriceObjectInternal.GetNullBidInternal((Symbol)idx), logger);

                _books[(int)PriceSide.Ask][idx] = new PriceBookInternal
                    (Counterparty.ValuesCount, askPriceComparer, askTopPriceComparer, askTopPriceComparer, priceCounterpartEqualityComparer,
                     PriceObjectInternal.GetNullAskInternal((Symbol)idx), logger);
            }
        }

        public void OnNewPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if (price == null)
                return;

            if (price.Price > 0 && price.SizeBaseAbsRemaining > 0)
                this._books[(int) price.Side][(int) price.Symbol].AddOrUpdate(price, ref heldWithoutAcquireCall);
            else
                this._books[(int)price.Side][(int)price.Symbol].Remove(price.Counterparty);

        }

        public void OnIgnoredPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if (price == null)
                return;

            if (this.ConsumesIgnoredPrices)
                this.OnNewPrice(price, ref heldWithoutAcquireCall);
            else
                this.OnPriceCancel(price.Symbol, price.Counterparty, price.Side);
        }

        public void OnNewNonExecutablePrice(PriceObjectInternal price)
        {
            if (price == null)
                return;

            bool _ = true;
            if (this.ConsumesIgnoredPrices)
                this.OnNewPrice(price, ref _);
            else
                this.OnPriceCancel(price.Symbol, price.Counterparty, price.Side);
        }

        public void OnQuoteCancel(Symbol symbol, Counterparty counterparty)
        {
            this._books[(int) PriceSide.Bid][(int) symbol].Remove(counterparty);
            this._books[(int) PriceSide.Ask][(int)symbol].Remove(counterparty);
        }

        public void OnPriceCancel(Symbol symbol, Counterparty counterparty, PriceSide side)
        {
            this._books[(int)side][(int)symbol].Remove(counterparty);
        }

        //called on order reject
        public void OnPriceInvalidate(Symbol symbol, PriceSide side, Counterparty counterparty, Guid integratorPriceIdnetity, DateTime integratorPriceReceivedUtc)
        {
            if(integratorPriceIdnetity != Guid.Empty)
                this._books[(int)side][(int)symbol].RemoveIdentical(integratorPriceIdnetity);
        }

        //Price book currently consumes only bank data
        public bool ConsumesDataFromCounterparty(Counterparty counterparty)
        {
            if (counterparty.TradingTargetType == TradingTargetType.BankPool 
                //this will add LMAX prices to DC ToB and into BankPool Matching
                || counterparty.TradingTargetType == TradingTargetType.LMAX
                || counterparty.TradingTargetType == TradingTargetType.FXCM)
                //|| counterparty == Counterparty.L01 || counterparty == Counterparty.LM2)
            {
                return true;
            }

            return false;
        }

        public bool ConsumesMDDataIfOFNotRunning(Counterparty counterparty)
        {
            return false;
        }

        public bool ConsumesIgnoredPrices
        {
            get { return false; }
        }

        public bool ConsumesNonExecutablePrices
        {
            get { return false; }
        }

        //public bool ConsumesFullDepthOfMData
        //{
        //    get { return false; }
        //}

        //public bool ConsumesHotspotData
        //{
        //    get { return false; }
        //}
    }
}
