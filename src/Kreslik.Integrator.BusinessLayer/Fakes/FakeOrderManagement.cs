﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.BusinessLayer.Fakes
{
    public class FakeOrderManagement : IOrderManagementEx
    {
        private Random _random = new Random();
        private ILogger _logger = new LogFactory().GetLogger("FakeOrderManager");

        public IntegratorRequestResult RegisterOrder(IClientOrder clientOrder, ITakerUnicastInfoForwarder dispatchGateway,
                           IBankPoolClientOrder parentOrder)
        {
            TaskEx.ExecAfterDelayWithErrorHandling(GetDue(), () => ProcessOrder(clientOrder, dispatchGateway));


            if (clientOrder is BankPoolClientOrder)
            {
                dispatchGateway.OnIntegratorClientOrderUpdate(
                BankPoolClientOrderUpdateInfo.CreateOpenClientOrderUpdateInfo(clientOrder), clientOrder.OrderRequestInfo.Symbol);
            }
            else if (clientOrder is VenueClientOrder)
            {
                dispatchGateway.OnIntegratorClientOrderUpdate(
                    VenueClientOrderUpdateInfo.CreateOpenClientOrderUpdateInfo(clientOrder as VenueClientOrder), clientOrder.OrderRequestInfo.Symbol);
            }
            else
            {
                throw new ArgumentException("Unknown order type");
            }

            return IntegratorRequestResult.GetSuccessResult();
        }

        public IntegratorRequestResult RegisterOrder(IClientOrder clientOrder, ITakerUnicastInfoForwarder dispatchGateway)
        {
            return this.RegisterOrder(clientOrder, dispatchGateway, null);
        }

        private TimeSpan GetDue()
        {
            return TimeSpan.FromSeconds(1 + (_random.NextDouble() * 2));
        }

        private void ProcessOrder(IClientOrder clientOrder, ITakerUnicastInfoForwarder dispatchGateway)
        {
            Counterparty counterparty = Counterparty.LM2;
            TradingTargetType senderTradingTargetType = TradingTargetType.BankPool;
            if (clientOrder is VenueClientOrder)
            {
                counterparty = (clientOrder as VenueClientOrder).VenueClientOrderRequestInfo.Counterparty;
            }

            if (_random.Next(10) > 7)
            {
                dispatchGateway.OnCounterpartyRejectedOrder(
                    new CounterpartyRejectionInfo(clientOrder.ClientOrderIdentity, clientOrder.ClientIdentity,
                        string.Empty, "BLAH0002", "gfgf", 1000m, 1.222m,
                        clientOrder.OrderRequestInfo.IntegratorDealDirection,
                        clientOrder.OrderRequestInfo.Symbol,
                        DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow,
                        counterparty, "Dummy reason", null, RejectionType.OrderReject,  senderTradingTargetType,
                        new IntegratedTradingSystemIdentification(258, IntegratedTradingSystemType.Cross,
                            Counterparty.HTF, new TradingSystemGroup(-5, "FooBar")), "foobar", null));

                System.Threading.Thread.Sleep(200);
                ProcessOrder(clientOrder, dispatchGateway);
                return;
            }

            if (_random.Next(20) > 18)
            {
                decimal ignoredAmount = clientOrder.OrderRequestInfo.SizeBaseAbsInitial - _random.Next(2) * 10;

                dispatchGateway.OnCounterpartyIgnoredOrder(new CounterpartyOrderIgnoringInfo(clientOrder.ClientOrderIdentity, clientOrder.ClientIdentity, string.Empty, "BLAH0002", ignoredAmount, 1.222m,
                                                  clientOrder.OrderRequestInfo.IntegratorDealDirection, clientOrder.OrderRequestInfo.Symbol,
                                                  DateTime.UtcNow, counterparty, senderTradingTargetType, null, null));

                if (ignoredAmount < clientOrder.OrderRequestInfo.SizeBaseAbsInitial)
                {
                    System.Threading.Thread.Sleep(200);

                    dispatchGateway.OnIntegratorDealInternal(new IntegratorDealInternal(clientOrder.ClientOrderIdentity, string.Empty, clientOrder.ClientIdentity, string.Empty, string.Empty, string.Empty,
                                                                     clientOrder.OrderRequestInfo.SizeBaseAbsInitial - ignoredAmount, false,
                                                                     clientOrder.OrderRequestInfo.RequestedPrice
                                                                                .HasValue
                                                                         ? clientOrder.OrderRequestInfo.RequestedPrice
                                                                                      .Value
                                                                         : 1m, clientOrder.OrderRequestInfo.IntegratorDealDirection,
                                                                     clientOrder.OrderRequestInfo.Symbol,
                                                                     DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow.Date, DateTime.UtcNow.Date, DateTime.UtcNow.Date, counterparty, null, null, senderTradingTargetType, null,
                                                                     new IntegratedTradingSystemIdentification(20, IntegratedTradingSystemType.MM, Counterparty.LM2, new TradingSystemGroup(-10, "BAH")), null));
                }
            }
            else
            {

                dispatchGateway.OnIntegratorDealInternal(new IntegratorDealInternal(clientOrder.ClientOrderIdentity, string.Empty, clientOrder.ClientIdentity, string.Empty, string.Empty,  string.Empty,
                                                                     clientOrder.OrderRequestInfo.SizeBaseAbsInitial, true,
                                                                     clientOrder.OrderRequestInfo.RequestedPrice
                                                                                .HasValue
                                                                         ? clientOrder.OrderRequestInfo.RequestedPrice
                                                                                      .Value
                                                                         : 1m, clientOrder.OrderRequestInfo.IntegratorDealDirection,
                                                                     clientOrder.OrderRequestInfo.Symbol,
                                                                     DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow.Date, DateTime.UtcNow.Date, DateTime.UtcNow.Date, counterparty, null, FlowSide.LiquidityTaker, senderTradingTargetType, MarketOnImprovementResult.ExecutedOnLaterImprovement,
                                                                     null, null));
            }

        }

        public bool HasVenueOrder(Counterparty targetVenueCounterparty, string clientOrderId)
        {
            throw new NotImplementedException();
        }

        public IBankPoolClientOrder GetInternalBankpoolOrder(IClientOrder clientOrder)
        {
            throw new NotImplementedException();
        }

        public IntegratorRequestResult CancelOrder(CancelOrderRequestInfo cancelOrderRequestInfo, ITakerUnicastInfoForwarder dispatchGateway)
        {
            Symbol symbol = Common.Symbol.NULL;

            if (cancelOrderRequestInfo.TradingTargetType == TradingTargetType.BankPool)
            {

                dispatchGateway.OnIntegratorClientOrderUpdate(
                    new BankPoolClientOrderUpdateInfo(null, cancelOrderRequestInfo.ClientOrderIdentity,
                                                      cancelOrderRequestInfo.ClientIdentity,
                                                      ClientOrderStatus.OpenInIntegrator,
                                                      ClientOrderCancelRequestStatus
                                                          .CancelRequestFailed, 0, 0, 0, string.Empty, 5.2m, DealDirection.Buy), symbol);
            }
            else if (cancelOrderRequestInfo.TradingTargetType == TradingTargetType.FXall ||
                     cancelOrderRequestInfo.TradingTargetType == TradingTargetType.Hotspot)
            {
                var clientOrder = ClientOrdersBuilder_OnlyForIntegratorUsage
                .CreateLmaxClientOrder(cancelOrderRequestInfo.ClientOrderIdentity, cancelOrderRequestInfo.ClientIdentity,
                ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxOrder(5, 2.2m, DealDirection.Sell, Symbol.EUR_AUD, TimeInForce.Day, LmaxCounterparty.L01),
                null);

                VenueClientOrderUpdateInfo.CreateFullyCancelledClientOrderUpdateInfo(
                    clientOrder,
                    ClientOrderStatus.OpenInIntegrator, 5, cancelOrderRequestInfo.TradingTargetType);
            }
            else
            {
                throw new ArgumentException("CancelRequestType");
            }


            return IntegratorRequestResult.GetSuccessResult();
        }

        public IntegratorRequestResult CancelAllClientOrders(string clientId, ITakerUnicastInfoForwarder dispatchGateway)
        {
            this._logger.Log(LogLevel.Info, "CancelAllClientOrders, clientId: {0}, gateway: {1}", clientId, dispatchGateway);

            return IntegratorRequestResult.GetSuccessResult();
        }

        public IntegratorRequestResult CancelAllMyOrders(ITakerUnicastInfoForwarder dispatchGateway)
        {
            this._logger.Log(LogLevel.Info, "CancelAllMyOrders, gateway: {0}", dispatchGateway);

            return IntegratorRequestResult.GetSuccessResult();
        }


        public void StopOrderManagement()
        {
            throw new NotImplementedException();
        }


        public void UpdateSettings(BusinessLayerSettings businessSettings)
        {
            throw new NotImplementedException();
        }


        //public IntegratorRequestResult FinalizeDeal(IntegratorDealInternalFinalizationRequest finalizeRequest, ITakerUnicastInfoForwarder dispatchGateway)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
