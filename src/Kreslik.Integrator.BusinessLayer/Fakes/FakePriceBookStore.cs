﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.BusinessLayer.Fakes
{
    public class FakeBookTopProvidersStore: IBookTopProvidersStore
    {
        private IPriceBookStoreEx _fakePriceBookStore;

        private IPriceBookStoreEx[] _fakePriceBookStores;

        public FakeBookTopProvidersStore()
        {
            IntegratorPerformanceCounter.InitializeInstance(IntegratorProcessType.BusinessLogic);
            PriceObjectPool.InitializeInstance(100000);

            _fakePriceBookStore = new FakePriceBookStore();
            _fakePriceBookStores = Enumerable.Repeat((IPriceBookStoreEx) null, Counterparty.ValuesCount).ToArray();

            foreach (Counterparty venueCounterparty in Counterparty.Values.Where(c => !BankCounterparty.Values.Select(c1 => (Counterparty)c1).Contains(c)))
            {
                _fakePriceBookStores[(int) venueCounterparty] = new FakePriceBookStore(venueCounterparty);
            }
        }

        public IPriceBookStoreEx TryGetBookTopProvider(Counterparty counterparty)
        {
            if (counterparty == Counterparty.NULL || counterparty.TradingTargetType == TradingTargetType.BankPool)
            {
                return this._fakePriceBookStore;
            }
            else
            {
                return this._fakePriceBookStores[(int)counterparty];
            }
        }

        public IPriceBookStoreEx BankpoolBookTopProvider { get { return this._fakePriceBookStore; } }


        public IBookTopProvider<PriceObjectInternal> GetBookTopProvider(Counterparty counterparty)
        {
            return this.TryGetBookTopProvider(counterparty);
        }

        public IEnumerable<IBookTopProvider<PriceObjectInternal>> AllBookTopProviders
        {
            get
            {
                yield return _fakePriceBookStore;
                foreach (IPriceBookStoreEx fakePriceBookStore in _fakePriceBookStores)
                {
                    yield return fakePriceBookStore;
                }
            }
        }


        public IPriceBookStore FirmpoolBookTopProvider
        {
            get { throw new NotImplementedException(); }
        }
    }

    public class FakePriceBookStore : IPriceBookStoreEx
    {
        private IChangeablePriceBook<PriceObjectInternal, Counterparty>[] _bidBooks = new IChangeablePriceBook<PriceObjectInternal, Counterparty>[Symbol.ValuesCount];
        private IChangeablePriceBook<PriceObjectInternal, Counterparty>[] _askBooks = new IChangeablePriceBook<PriceObjectInternal, Counterparty>[Symbol.ValuesCount];

        public FakePriceBookStore()
        {
            for (int idx = 0; idx < Symbol.ValuesCount; idx++)
            {
                _bidBooks[idx] = new FakePriceBook((Symbol)idx, true);
                _askBooks[idx] = new FakePriceBook((Symbol)idx, false);
            }
        }

        public FakePriceBookStore(Counterparty counterparty)
        {
            for (int idx = 0; idx < Symbol.ValuesCount; idx++)
            {
                _bidBooks[idx] = new FakePriceBook((Symbol)idx, counterparty, true);
                _askBooks[idx] = new FakePriceBook((Symbol)idx, counterparty, false);
            }
        }

        public IBookTopExtended<PriceObjectInternal, Counterparty> GetBidPriceBook(Symbol symbol)
        {
            return _bidBooks[(int)symbol];
        }

        public IBookTopExtended<PriceObjectInternal, Counterparty> GetAskPriceBook(Symbol symbol)
        {
            return _askBooks[(int)symbol];
        }

        public IChangeablePriceBook<PriceObjectInternal, Counterparty> GetChangeableBidPriceBook(Symbol symbol)
        {
            return _bidBooks[(int)symbol];
        }

        public IChangeablePriceBook<PriceObjectInternal, Counterparty> GetChangeableAskPriceBook(Symbol symbol)
        {
            return _askBooks[(int)symbol];
        }

        public TradingTargetType TradingTargetType { get { return _askBooks.First().TradingTargetType; } }

        public PriceObjectInternal MaxNodeInternal { get { throw new NotImplementedException("PriceBookStore cannot have one single MaxNode"); } }

        public PriceObjectInternal MaxNode { get { throw new NotImplementedException("PriceBookStore cannot have one single MaxNode"); } }

        public DateTime MaxNodeUpdatedUtc { get { throw new NotImplementedException("PriceBookStore cannot have one single MaxNode"); } }

        public event BookPriceUpdate<PriceObjectInternal> BookTopImproved
        {
            add
            {
                foreach (IPriceBook<PriceObjectInternal, Counterparty> bidBook in _bidBooks)
                {
                    bidBook.BookTopImproved += value;
                }

                foreach (IPriceBook<PriceObjectInternal, Counterparty> askBook in _askBooks)
                {
                    askBook.BookTopImproved += value;
                }
            }
            remove
            {
                foreach (IPriceBook<PriceObjectInternal, Counterparty> bidBook in _bidBooks)
                {
                    bidBook.BookTopImproved -= value;
                }

                foreach (IPriceBook<PriceObjectInternal, Counterparty> askBook in _askBooks)
                {
                    askBook.BookTopImproved -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> BookTopDeterioratedInternal
        {
            add
            {
                foreach (IPriceBook<PriceObjectInternal, Counterparty> bidBook in _bidBooks)
                {
                    bidBook.BookTopDeterioratedInternal += value;
                }

                foreach (IPriceBook<PriceObjectInternal, Counterparty> askBook in _askBooks)
                {
                    askBook.BookTopDeterioratedInternal += value;
                }
            }
            remove
            {
                foreach (IPriceBook<PriceObjectInternal, Counterparty> bidBook in _bidBooks)
                {
                    bidBook.BookTopDeterioratedInternal -= value;
                }

                foreach (IPriceBook<PriceObjectInternal, Counterparty> askBook in _askBooks)
                {
                    askBook.BookTopDeterioratedInternal -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> BookTopReplaced
        {
            add
            {
                foreach (IPriceBook<PriceObjectInternal, Counterparty> bidBook in _bidBooks)
                {
                    bidBook.BookTopReplaced += value;
                }

                foreach (IPriceBook<PriceObjectInternal, Counterparty> askBook in _askBooks)
                {
                    askBook.BookTopReplaced += value;
                }
            }
            remove
            {
                foreach (IPriceBook<PriceObjectInternal, Counterparty> bidBook in _bidBooks)
                {
                    bidBook.BookTopReplaced -= value;
                }

                foreach (IPriceBook<PriceObjectInternal, Counterparty> askBook in _askBooks)
                {
                    askBook.BookTopReplaced -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> NewNodeArrived
        {
            add
            {
                foreach (IPriceBook<PriceObjectInternal, Counterparty> bidBook in _bidBooks)
                {
                    bidBook.NewNodeArrived += value;
                }

                foreach (IPriceBook<PriceObjectInternal, Counterparty> askBook in _askBooks)
                {
                    askBook.NewNodeArrived += value;
                }
            }
            remove
            {
                foreach (IPriceBook<PriceObjectInternal, Counterparty> bidBook in _bidBooks)
                {
                    bidBook.NewNodeArrived -= value;
                }

                foreach (IPriceBook<PriceObjectInternal, Counterparty> askBook in _askBooks)
                {
                    askBook.NewNodeArrived -= value;
                }
            }
        }

        public event BookPriceUpdate<PriceObjectInternal> ExistingNodeInvalidated
        {
            add
            {
                foreach (IPriceBook<PriceObjectInternal, Counterparty> bidBook in _bidBooks)
                {
                    bidBook.ExistingNodeInvalidated += value;
                }

                foreach (IPriceBook<PriceObjectInternal, Counterparty> askBook in _askBooks)
                {
                    askBook.ExistingNodeInvalidated += value;
                }
            }
            remove
            {
                foreach (IPriceBook<PriceObjectInternal, Counterparty> bidBook in _bidBooks)
                {
                    bidBook.ExistingNodeInvalidated -= value;
                }

                foreach (IPriceBook<PriceObjectInternal, Counterparty> askBook in _askBooks)
                {
                    askBook.ExistingNodeInvalidated -= value;
                }
            }
        }

        public event BookMultiplePricesUpdate<PriceObjectInternal, Counterparty> NodesRemoved
        {
            add
            {
                foreach (IPriceBook<PriceObjectInternal, Counterparty> bidBook in _bidBooks)
                {
                    bidBook.NodesRemoved += value;
                }

                foreach (IPriceBook<PriceObjectInternal, Counterparty> askBook in _askBooks)
                {
                    askBook.NodesRemoved += value;
                }
            }
            remove
            {
                foreach (IPriceBook<PriceObjectInternal, Counterparty> bidBook in _bidBooks)
                {
                    bidBook.NodesRemoved -= value;
                }

                foreach (IPriceBook<PriceObjectInternal, Counterparty> askBook in _askBooks)
                {
                    askBook.NodesRemoved -= value;
                }
            }
        }


        public class FakePriceBook : IChangeablePriceBook<PriceObjectInternal, Counterparty>
        {
            private Symbol _symbol;
            private bool _isBid;
            private decimal _lastValue = 0;
            private PriceObjectInternal _lastMax;
            private SafeTimer _timer;
            private Random _random = new Random();
            private int _quotesCnt = 0;
            private readonly Counterparty _counterparty;

            public FakePriceBook(Symbol symbol, bool isBid)
                : this(symbol, Counterparty.L01, isBid)
            {
                this.TradingTargetType = TradingTargetType.BankPool;
            }

            public FakePriceBook(Symbol symbol, Counterparty counterparty, bool isBid)
            {
                this._symbol = symbol;
                this._counterparty = counterparty;
                this.TradingTargetType = counterparty.TradingTargetType;
                this._isBid = isBid;
                _timer = new SafeTimer(TimerTick, GetNextDue().Add(TimeSpan.FromMilliseconds(10 * (int)symbol)), Timeout.InfiniteTimeSpan);
                _lastMax = this._isBid ? PriceObjectInternal.GetNullBidInternal(this._symbol, this._counterparty) : PriceObjectInternal.GetNullAskInternal(this._symbol, this._counterparty);
            }

            ~FakePriceBook()
            {
                if (_lastMax != null)
                    _lastMax.Release();
            }

            public AtomicDecimal GetEstimatedBestPriceFast()
            {
                throw new NotImplementedException("If needed ask for implementation. FakePriceBook is a testing low-maintained code");
            }

            public decimal GetWeightedPriceForSize(ref decimal maxSizeOnPrice, int maximumBookDepthToBeUsed)
            {
                throw new NotImplementedException("If needed ask for implementation. FakePriceBook is a testing low-maintained code");
            }

            public decimal GetWeightedPriceForExactSize(decimal sizeOnPrice, int maximumBookDepthToBeUsed)
            {
                throw new NotImplementedException("If needed ask for implementation. FakePriceBook is a testing low-maintained code");
            }

            public decimal GetMaximumSizeForExactPrice(decimal worstAcceptablePrice, int maximumBookDepthToBeUsed,
                decimal maximumSizeToStopProbing)
            {
                throw new NotImplementedException();
            }

            public decimal GetMaximumSizeForExactPrice(decimal worstAcceptablePrice, int maximumBookDepthToBeUsed,
                decimal maximumSizeToStopProbing, Counterparty[] counterpartiesWhitelist)
            {
                throw new NotImplementedException("If needed ask for implementation. FakePriceBook is a testing low-maintained code");
            }

            public TradingTargetType TradingTargetType { get; private set; }

            private TimeSpan GetNextDue()
            {
                //return TimeSpan.FromSeconds(1 + _random.NextDouble());
                return TimeSpan.FromMilliseconds(100 + _random.NextDouble()*500);
            }

            private string GetQuoteIdentity()
            {
                return string.Format("{0}_{1}_{2}", _symbol, _isBid ? "BID" : "ASK", Interlocked.Increment(ref _quotesCnt));
            }

            private void TimerTick()
            {
                if (_random.Next(20) > 18)
                {
                    if (NodesRemoved != null)
                    {
                        NodesRemoved(this, this._counterparty, HighResolutionDateTime.UtcNow);
                    }
                }
                else if (_random.Next(20) < 2)
                {
                    if (ExistingNodeInvalidated != null && _lastMax != null)
                    {
                        ExistingNodeInvalidated(this, _lastMax, HighResolutionDateTime.UtcNow);
                    }
                }
                else
                {

                    decimal value = (decimal) (1 + (_random.NextDouble() - 0.5)*0.01);
                    value = decimal.Round(value, 5);

                    PriceObjectInternal price;
                    if (_random.Next(20) == 10)
                    {
                        //null price
                        value = _isBid ? 0 : decimal.MaxValue;
                        //price = PriceObjectInternal.GetAllocatedTestingPriceObject(value, 0, _isBid ? PriceSide.Bid : PriceSide.Ask, _symbol,
                        //    this._counterparty, null, MarketDataRecordType.BankQuoteData, DateTime.UtcNow,
                        //    DateTime.MinValue);
                        //price.Acquire();

                        price = PriceObjectPool.Instance.GetPriceObject(new AtomicDecimal(value), 0,
                            _isBid ? PriceSide.Bid : PriceSide.Ask, _symbol, this._counterparty,
                            null,
                            MarketDataRecordType.BankQuoteData, DateTime.UtcNow, DateTime.MinValue);
                    }
                    else
                    {
                        //price = WritablePriceObject.GetAllocatedTestingPriceObject(value, 1000000, _isBid ? PriceSide.Bid : PriceSide.Ask, _symbol, this._counterparty,
                        //    GetQuoteIdentity(), MarketDataRecordType.BankQuoteData, DateTime.MinValue, DateTime.MinValue);

                        price = PriceObjectPool.Instance.GetPriceObject(new AtomicDecimal(value), 1000000,
                            _isBid ? PriceSide.Bid : PriceSide.Ask, _symbol, this._counterparty,
                            GetQuoteIdentity().ToAsciiBuffer(),
                            MarketDataRecordType.BankQuoteData, DateTime.UtcNow, DateTime.MinValue);
                    }

                    if (NewNodeArrived != null)
                    {
                        NewNodeArrived(this, price, HighResolutionDateTime.UtcNow);
                    }

                    if (value == _lastValue)
                    {
                        InvokeBookTopReplaced(price);
                    }
                    else if ((_isBid && value > _lastValue) || (!_isBid && value < _lastValue))
                    {
                        InvokeBookTopImproved(price);
                    }
                    else
                    {
                        InvokeBookTopDeteriorated(price);
                    }

                    if (_lastMax != null)
                        _lastMax.Release();

                    _lastValue = value;
                    _lastMax = price;
                    this.MaxNodeUpdatedUtc = HighResolutionDateTime.UtcNow;
                }

                _timer.Change(GetNextDue(), Timeout.InfiniteTimeSpan);
            }

            private void InvokeBookTopImproved(PriceObjectInternal price)
            {
                if (BookTopImproved != null)
                {
                    BookTopImproved(this, price, HighResolutionDateTime.UtcNow);
                }

                if (ChangeableBookTopImproved != null)
                {
                    ChangeableBookTopImproved(price);
                }
            }

            private void InvokeBookTopDeteriorated(PriceObjectInternal price)
            {
                if (BookTopDeterioratedInternal != null)
                {
                    BookTopDeterioratedInternal(this, price, HighResolutionDateTime.UtcNow);
                }

                if (ChangeableBookTopDeteriorated != null)
                {
                    ChangeableBookTopDeteriorated(price);
                }
            }

            private void InvokeBookTopReplaced(PriceObjectInternal price)
            {
                if (BookTopReplaced != null)
                {
                    BookTopReplaced(this, price, HighResolutionDateTime.UtcNow);
                }
            }

            public event BookPriceUpdate<PriceObjectInternal> BookTopImproved;
            public event BookPriceUpdate<PriceObjectInternal> BookTopDeterioratedInternal;
            public event BookPriceUpdate<PriceObjectInternal> BookTopReplaced;
            public event BookPriceUpdate<PriceObjectInternal> NewNodeArrived;
            public event BookPriceUpdate<PriceObjectInternal> ExistingNodeInvalidated;
            public event BookMultiplePricesUpdate<PriceObjectInternal, Counterparty> NodesRemoved;

            

            public bool IsEmpty
            {
                get { throw new NotImplementedException(); }
            }

            public PriceObjectInternal MaxNodeInternal
            {
                get
                {
                    PriceObjectInternal value = _lastMax;
                    value.Acquire();

                    return value;
                }
            }

            public PriceObjectInternal MaxNode
            {
                get
                {
                    var maxNode = _lastMax;
                    if (PriceObjectInternal.IsNullPrice(maxNode)) return null;
                    else return maxNode;
                }
            }

            public DateTime MaxNodeUpdatedUtc { get; private set; }

            public bool RemoveIdentical(PriceObjectInternal value)
            {
                throw new NotImplementedException();
            }

            public void ResortIdentical(PriceObjectInternal value)
            {
                throw new NotImplementedException();
            }

            public bool RemoveIdentical(Guid identificator)
            {
                throw new NotImplementedException();
            }

            public PriceObjectInternal[] GetSortedClone()
            {
                throw new NotImplementedException();
            }

            public PriceObjectInternal[] GetSortedClone(Func<PriceObjectInternal, bool> selectorFunc)
            {
                throw new NotImplementedException();
            }


            public PriceObjectInternal GetIdentical(PriceObjectInternal node)
            {
                throw new NotImplementedException();
            }

            public event Action<PriceObjectInternal> ChangeableBookTopImproved;

            public event Action<PriceObjectInternal> ChangeableBookTopDeteriorated;

            public event Action<PriceObjectInternal> ChangeableBookTopReplaced;


            public bool IsImprovement
            {
                get { throw new NotImplementedException(); }
            }
        }





        public void OnQuoteCancel(Symbol symbol, Counterparty counterparty)
        {
            throw new NotImplementedException();
        }
    }
}
