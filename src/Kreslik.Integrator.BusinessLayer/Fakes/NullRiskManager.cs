﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.BusinessLayer.Fakes
{
    public class NullRiskManager : IRiskManager
    {
        public RiskManagementCheckResult AddExternalOrderAndCheckIfAllowed(IIntegratorOrderExternal externalOrder, out decimal nopEstimate)
        {
            nopEstimate = 0;
            return RiskManagementCheckResult.Accepted;
        }

        public RiskManagementCheckResult AddInternalOrderAndCheckIfAllowed(IIntegratorOrderInfo clientOrder)
        {
            return RiskManagementCheckResult.Accepted;
        }

        public RiskManagementCheckResult CheckIfSoftPriceAllowed(IIntegratorOrderInfo streamingPrice)
        {
            return RiskManagementCheckResult.Accepted;
        }

        public bool IsTransmissionDisabled
        {
            get { return false; }
        }

        public event OrderTransmissionStatusChangedEventHandler StateChanged;

        public void DisableTransmission(string reason)
        {

        }


        public bool CancelInternalOrder(IClientOrder clientOrder, decimal cancelledAmount)
        {
            return true;
        }

        public bool CancelInternalOrder(decimal canceledSizeBaseAbs, PriceObject quotedPrice)
        {
            return true;
        }


        public void UpdateStateOfExternalOrder(IIntegratorOrderExternal externalOrder, OrderChangeEventArgs orderChangeEventArgs)
        {
        }


        public decimal GetMaximumOrderSizeBaseAbs(Common.Currency baseCurrency)
        {
            return decimal.MaxValue;
        }

        public decimal[] UsdConversionMultipliers { get; private set; }


        public void UpdateBackendSettings(object unused)
        {
            return;
        }

        public IPriceChangesConsumer NewPricesConsumer
        {
            get { return null; }
        }

        public event NewBroadcastInfoEventHandler NewBroadcastInfo;

        public IEnumerable<IntegratorBroadcastInfoBase> ExistingInfos
        {
            get { return new List<IntegratorBroadcastInfoBase>(); }
        }


        public bool IsGoFlatOnlyEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public event Action<bool> GoFlatOnlyStateChanged;


        public event Action<OrderTransmissionStatusChangedEventArgs> OrderTransmissionChanged;


        public bool CheckIfIncomingClientDealAllowed(IIntegratorOrderInfo rejectableDeal)
        {
            return true;
        }

        public bool CheckIfIncomingClientDealCanBeAccepted(IIntegratorOrderInfo rejectableDeal)
        {
            return true;
        }

        //public void AcceptedClientDealWasLateRejected(IIntegratorOrderInfo rejectableMmDeal)
        //{
        //    //
        //}

        //public void AcceptedClientDealWasLateRejected(IIntegratorOrderInfo orderInfo, decimal sizeToRemoveBaseAbs)
        //{
        //    //
        //}

        public void TurnOnGoFlatOnly(string reason)
        {
            //
        }


        public IOnlinePriceDeviationChecker OnlinePriceDeviationChecker
        {
            get { throw new NotImplementedException(); }
        }


        public ISymbolTrustworthyWatcher GetSymbolTrustworthyWatcher(Common.Symbol symbol)
        {
            throw new NotImplementedException();
        }


        public ISymbolTrustworthinessInfoProvider SymbolTrustworthinessInfoProvider
        {
            get { throw new NotImplementedException(); }
        }


        public bool AllowPropagationOfStateChangesToBackend
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public DateTime GoFlatOnlyLastEnabledTime
        {
            get { throw new NotImplementedException(); }
        }


        public IExternalOrdersTransmissionControlScheduling ExternalOrdersTransmissionControlScheduling
        {
            get { return null; }
        }


        public void HandleDealDone(IntegratorDealDone integratorDealDone)
        {
            throw new NotImplementedException();
        }

        public void HandleDealLateRejected(IIntegratorOrderInfo orderInfo, IntegratorDealDone integratorDealDone)
        {
            throw new NotImplementedException();
        }

        public bool IsTradeRequestLikelyToPass(IIntegratorOrderInfo orderInfo)
        {
            throw new NotImplementedException();
        }

        public bool IsTradeRequestLikelyToPass(IIntegratorOrderInfo orderInfo, PriceObjectInternal priceObject)
        {
            throw new NotImplementedException();
        }
    }
}
