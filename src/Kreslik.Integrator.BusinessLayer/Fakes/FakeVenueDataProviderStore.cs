﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinessLayer.Fakes
{
    public class FakeVenueDataProviderStore: IVenueDataProviderStore
    {
        private IVenueDataProvider _fakeHtaVenueDataProvider = new FakeVenueDataProvider(Counterparty.HTA);
        private IVenueDataProvider _fakeHtfVenueDataProvider = new FakeVenueDataProvider(Counterparty.HTF);

        public IVenueDataProvider GetVenueDataProvider(Counterparty counterparty)
        {
            if (counterparty == Counterparty.HTA)
            {
                return this._fakeHtaVenueDataProvider;
                //return null;
            }
            else if (counterparty == Counterparty.HTF)
            {
                return this._fakeHtfVenueDataProvider;
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        public IEnumerable<IVenueDataProvider> AllVenueDataProviders
        {
            get
            {
                return new List<IVenueDataProvider>() { this._fakeHtaVenueDataProvider, this._fakeHtfVenueDataProvider };
            }
        }
    }

    public class FakeVenueDataProvider: IVenueDataProvider
    {
        private FakeTickersPerSymbolProvider[] tickerProviders;

        public FakeVenueDataProvider(Counterparty counterparty)
        {
            tickerProviders = Symbol.Values.Select(sym => new FakeTickersPerSymbolProvider(sym, counterparty)).ToArray();
        }


        public void AddNewVenueDealHandler(Symbol symbol, NewVenueDealHandler newVenueDealHandler)
        {
            tickerProviders[(int) symbol].NewVenueDealHandler += newVenueDealHandler;
        }

        public void RemoveNewVenueDealHandler(Symbol symbol, NewVenueDealHandler newVenueDealHandler)
        {
            tickerProviders[(int)symbol].NewVenueDealHandler -= newVenueDealHandler;
        }


        public void RemoveAllOccurencesOfNewVenueDealHandler(NewVenueDealHandler newVenueDealHandler)
        {
            foreach (FakeTickersPerSymbolProvider fakeTickersPerSymbolProvider in tickerProviders)
            {
                fakeTickersPerSymbolProvider.NewVenueDealHandler -= newVenueDealHandler;
            }
        }

        private class FakeTickersPerSymbolProvider
        {
            private Symbol _symbol;
            private Counterparty _counterparty;
            private SafeTimer _timer;
            private Random _random = new Random();

            public FakeTickersPerSymbolProvider(Symbol symbol, Counterparty counterparty)
            {
                this._symbol = symbol;
                this._counterparty = counterparty;
                _timer = new SafeTimer(TimerTick, GetNextDue().Add(TimeSpan.FromMilliseconds(10 * (int)symbol)), Timeout.InfiniteTimeSpan);
            }

            public event NewVenueDealHandler NewVenueDealHandler;

            private TimeSpan GetNextDue()
            {
                //return TimeSpan.FromSeconds(1 + _random.NextDouble());
                return TimeSpan.FromMilliseconds(100 + _random.NextDouble() * 500);
            }

            private void TimerTick()
            {
                TradeSide side = _random.Next(10) > 5 ? TradeSide.BuyPaid : TradeSide.SellGiven;
                decimal price = (decimal)(1 + (_random.NextDouble() - 0.5) * 0.01);

                VenueDealObject venueDealObject = new VenueDealObject(_symbol, _counterparty, side, price, DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow);


                if (NewVenueDealHandler != null)
                {
                    NewVenueDealHandler(venueDealObject);
                }

                _timer.Change(GetNextDue(), Timeout.InfiniteTimeSpan);
            }
        }
    }
}
