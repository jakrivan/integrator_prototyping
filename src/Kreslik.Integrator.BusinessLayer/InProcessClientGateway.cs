﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;

namespace Kreslik.Integrator.BusinessLayer
{
    public class InProcessClientGateway : ClientGatewayBase
    {

        private static int _inprocessClientsCounter = 1;
        private ClientToIntegratorConnectorHelper _clientToIntegratorConnectorHelper;
        private OrderUpdatesCheckingHelper _orderUpdatesCheckingHelper;

        public InProcessClientGateway(ILogger logger, IBookTopProvidersStore bookTopProvidersStore,
                                      IVenueDataProviderStore venueDataProviderStore, IOrderManagementEx orderManagement,
                                      IEnumerable<IBroadcastInfosStore> broadcastInfosStores, string clientIdentity)
            : base(clientIdentity, logger)
        {
            this._logger = logger;
            this.ClientIdentityInternal = string.Format("InProcessClient{0:D3}_{1}",
                                                        Interlocked.Increment(ref _inprocessClientsCounter),
                                                        clientIdentity);

            BuildVersioningInfo currentBuildVersion =
                new BuildVersioningInfo(BuildConstants.CURRENT_BUILD_VERSION,
                                        BuildConstants.MINIMUM_VERSION_NUMBER_OF_COMPATIBLE_BUILD,
                                        BuildConstants.CURRENT_VERSION_CREATED_UTC);

            this.ConnectionChainBuildVersioningInfo =
                new ConnectionChainBuildVersioningInfo(currentBuildVersion, null, currentBuildVersion);

            this._clientToIntegratorConnectorHelper =
                new ClientToIntegratorConnectorHelper(logger, bookTopProvidersStore, venueDataProviderStore, orderManagement,
                                                      broadcastInfosStores,
                                                      this);
            this._orderUpdatesCheckingHelper = new OrderUpdatesCheckingHelper(logger);
        }

        #region ClientGatewayBase implementations

        protected override void LocalResourcesCleanupRoutine()
        {
            this._clientToIntegratorConnectorHelper.CleanupContractWithIntegrator();
        }

        protected override string ClientIdentityInternal { get; set; }

        protected override IntegratorRequestResult SubmitRequestInternal(IntegratorRequestInfo integratorRequestInfo)
        {
            return this._clientToIntegratorConnectorHelper.SubmitRequest(integratorRequestInfo);
        }

        protected override void DispatchUnicastInfo(IntegratorInfoObjectBase unicastInfo)
        {
            this._orderUpdatesCheckingHelper.CheckEventIfExpected(unicastInfo);
            base.DispatchUnicastInfo(unicastInfo);
        }

        //additional condition than just calling IsClosed = true
        // and there is no other additional condition
        protected override bool IsConnectionChannelClosed
        {
            get { return false; }
        }

        #endregion /ClientGatewayBase implementations
    }
}