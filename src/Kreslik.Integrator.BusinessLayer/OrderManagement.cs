﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.FIXMessaging;

namespace Kreslik.Integrator.BusinessLayer
{
    public interface IOrderManagementEx : IOrderManagement
    {
        IntegratorRequestResult CancelAllClientOrders(string clientId, ITakerUnicastInfoForwarder dispatchGateway);
        IntegratorRequestResult CancelAllMyOrders(ITakerUnicastInfoForwarder dispatchGateway);
        void UpdateSettings(BusinessLayerSettings businessSettings);
    }

    public class OrderManagement : IOrderManagementEx
    {
        private List<SingleSymbolDirectionOrderManager>[] _bankpoolOrderManagers;
        private ILogger _logger;
        private Dictionary<string, SingleSymbolDirectionOrderManager> _registeredOrders = new Dictionary<string, SingleSymbolDirectionOrderManager>();
        private IIntegratorPerformanceCounter _performanceCounters;
        //private CompositeOrdersManager _compositeOrdersManager;
        private VenueOrderManager[] _venueOrderManagers = Enumerable.Repeat((VenueOrderManager)null, Counterparty.ValuesCount).ToArray();

        public OrderManagement(IPriceBookStoreEx priceBookStore, IList<IOrderFlowSession> orderFlowSessions,
                               ILogger logger,
                               IRiskManager riskManager, IIntegratorPerformanceCounter performanceCounters,
                               BusinessLayerSettings businessSettings)
        {
            _bankpoolOrderManagers = new List<SingleSymbolDirectionOrderManager>[]
            {
                new List<SingleSymbolDirectionOrderManager>(),
                new List<SingleSymbolDirectionOrderManager>()
            };;
            this._logger = logger;
            this._performanceCounters = performanceCounters;

            IList<IOrderFlowSession> layeredOrderSessions =
                CopyOFSessionsListDuplicatingLayeredSessions(orderFlowSessions);
            foreach (Symbol symbol in Symbol.Values)
            {
                _bankpoolOrderManagers[(int) DealDirection.Buy].Add(new SingleSymbolDirectionOrderManager(OrderEvaluator.BuyOrderEvaluator,
                                                                            priceBookStore.GetChangeableAskPriceBook(
                                                                                symbol),
                                                                            layeredOrderSessions, symbol, DealDirection.Buy,
                                                                            logger, riskManager, businessSettings.MarketableClientOrdersMatchingStrategy));
                _bankpoolOrderManagers[(int)DealDirection.Buy].Last().OrderClosing += this.OnOrderClosing;

                _bankpoolOrderManagers[(int)DealDirection.Sell].Add(new SingleSymbolDirectionOrderManager(OrderEvaluator.SellOrderEvaluator,
                                                                             priceBookStore.GetChangeableBidPriceBook(
                                                                                 symbol),
                                                                             layeredOrderSessions, symbol,
                                                                             DealDirection.Sell,
                                                                             logger, riskManager, businessSettings.MarketableClientOrdersMatchingStrategy));
                _bankpoolOrderManagers[(int)DealDirection.Sell].Last().OrderClosing += this.OnOrderClosing;
            }

            this.UpdateSettings(businessSettings);

            //this._compositeOrdersManager = new CompositeOrdersManager(logger, priceBookStore, orderFlowSessions,
            //                                                          riskManager, this);
            foreach (
                VenueOrderFlowSession venueOrderFlowSession in
                    orderFlowSessions.Where(session => session is VenueOrderFlowSession))
            {
                this._venueOrderManagers[(int) venueOrderFlowSession.Counterparty] =
                    new VenueOrderManager(venueOrderFlowSession, this._logger, riskManager);
            }

            riskManager.OrderTransmissionChanged += stateArgs =>
                {
                    if (!stateArgs.IsOrderTransmissionEnabled)
                    {
                        this._logger.Log(LogLevel.Error,
                                         "Order transmission switched to disabled [{0}], so cancelling all outstanding cliet orders",
                                         stateArgs.Reason);

                        int venueOrdersCnt =
                            _venueOrderManagers.Where(mgr => mgr != null).Select(mgr => mgr.ActiveOrdersCount).Sum();
                        int bankOrdersCnt =
                            _bankpoolOrderManagers.SelectMany(mgr => mgr).Select(mgr => mgr.ActiveOrdersCount).Sum();

                        if (venueOrdersCnt + bankOrdersCnt > 0)
                        {
                            this._logger.Log(LogLevel.Fatal,
                                             "Order manager has registrations for {0} venue and {1} bank active orders. Canceling all of those",
                                             venueOrdersCnt, bankOrdersCnt);
                        }

                        foreach (VenueOrderManager venueOrderManager in _venueOrderManagers.Where(mgr => mgr != null))
                        {
                            venueOrderManager.CancelAllOrders();
                        }

                        //no cancellations for composite orders manager

                        foreach (
                            SingleSymbolDirectionOrderManager singleSymbolDirectionOrderManager in
                                _bankpoolOrderManagers.SelectMany(mgr => mgr))
                        {
                            singleSymbolDirectionOrderManager.CancelAllOrders();
                        }
                    }
                };

            riskManager.GoFlatOnlyStateChanged += goFlatOnlyEnabled =>
            {
                if (goFlatOnlyEnabled)
                {
                    TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(30), () =>
                    {
                        if (riskManager.IsGoFlatOnlyEnabled)
                        {
                            int venueOrdersCnt =
                            _venueOrderManagers.Where(mgr => mgr != null).Select(mgr => mgr.ActiveOrdersCount).Sum();
                            int bankOrdersCnt = _bankpoolOrderManagers.SelectMany(mgr => mgr).Select(mgr => mgr.ActiveOrdersCount).Sum();

                            if (venueOrdersCnt + bankOrdersCnt > 0)
                            {
                                int venueRiskRemovingOrdersCnt =
                                    _venueOrderManagers.Where(mgr => mgr != null)
                                        .Select(mgr => mgr.ActiveRiskRemovingOrdersCount)
                                        .Sum();

                                this._logger.Log((venueOrdersCnt + bankOrdersCnt) == venueRiskRemovingOrdersCnt ? LogLevel.Warn : LogLevel.Fatal,
                                                 "GoFlatOnly is on for a minute but Order Manager has registrations for {0} venue (out of that {1} risk removing - quoting can produce those) and {2} bank active orders. You can cancel those by flipping KillSwitch",
                                                 venueOrdersCnt, venueRiskRemovingOrdersCnt, bankOrdersCnt);
                            }
                        }
                    });
                }
            };
        }

        private static IList<IOrderFlowSession> CopyOFSessionsListDuplicatingLayeredSessions(
            IList<IOrderFlowSession> orderFlowSessions)
        {
            List<IOrderFlowSession> copyList = new List<IOrderFlowSession>(orderFlowSessions);

            foreach (Counterparty lmaxLayeredCounterparty in LmaxCounterparty.LayersOfLM1)
            {
                copyList[(int)lmaxLayeredCounterparty] = copyList[(int)LmaxCounterparty.L01];
            }

            return copyList;
        }

        public void UpdateSettings(BusinessLayerSettings businessSettings)
        {
            foreach (
                SingleSymbolDirectionOrderManager singleSymbolDirectionOrderManager in
                    _bankpoolOrderManagers.SelectMany(mgr => mgr))
            {
                singleSymbolDirectionOrderManager.UpdateSettings(businessSettings);
            }
        }

        public IntegratorRequestResult RegisterOrder(IClientOrder clientOrder, ITakerUnicastInfoForwarder dispatchGateway)
        {
            if (dispatchGateway == null)
            {
                _logger.Log(LogLevel.Fatal, "DispatchGateway is null - cannot register any order");
                return IntegratorRequestResult.CreateFailedResult("DispatchGateway is null - cannot register any order");
            }

            if (clientOrder == null)
            {
                _logger.Log(LogLevel.Fatal, "ClientOrder is null - cannot register it");
                return IntegratorRequestResult.CreateFailedResult("ClientOrder is null - cannot register it");
            }

            if (clientOrder is BankPoolClientOrder)
            {
                BankPoolClientOrder bankPoolClientOrder = clientOrder as BankPoolClientOrder;

                SingleSymbolDirectionOrderManager orderManager =
                    _bankpoolOrderManagers[(int) clientOrder.OrderRequestInfo.IntegratorDealDirection][
                        (int) clientOrder.OrderRequestInfo.Symbol];

                lock (_registeredOrders)
                {
                    if (_registeredOrders.ContainsKey(clientOrder.ClientOrderIdentity))
                    {
                        _logger.Log(LogLevel.Fatal,
                            "Attempt to re-register Client order [{0}]. Ignoring the duplicate attempt",
                            clientOrder.ClientOrderIdentity);
                        return IntegratorRequestResult.CreateFailedResult("Attempt to re-register Client order");
                    }

                    _registeredOrders.Add(clientOrder.ClientOrderIdentity, orderManager);
                }
                orderManager.RegisterOrder(bankPoolClientOrder, dispatchGateway);

                this._performanceCounters.AddInternalOrder();

            }
            else if (clientOrder is VenueClientOrder)
            {
                VenueClientOrder venueClientOrder = clientOrder as VenueClientOrder;

                VenueOrderManager venueOrderManager =
                    this._venueOrderManagers[(int) venueClientOrder.VenueClientOrderRequestInfo.Counterparty];
                if (venueOrderManager == null)
                {
                    this._logger.Log(LogLevel.Error,
                        "Attempt to register Venue order, but VenueOrderFlowSession was null during construction (it's likely not turned on in configuration), so rejecting");
                    dispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                            "VenueOrderFlowSession is not likely configured to be on (it is null)"),
                        clientOrder.OrderRequestInfo.Symbol);
                }
                else
                {
                    venueOrderManager.RegisterVenueOrder(venueClientOrder, dispatchGateway);
                }

            }
            else
            {
                this._logger.Log(LogLevel.Fatal, "Experienced unexpected type ({0}) of clientOrder: {1}",
                    clientOrder.GetType(), clientOrder);
            }

            return IntegratorRequestResult.GetSuccessResult();
        }

        private void OnOrderClosing(string clientOrderId)
        {
            lock (_registeredOrders)
            {
                if (_registeredOrders.Remove(clientOrderId))
                {
                    this._performanceCounters.RemoveInternalOrder();
                }
                else
                {
                    _logger.Log(LogLevel.Fatal,
                                "SingleSymbolDirectionOrderManager is closing an order that is not known to OrderManagement. [{0}]",
                                clientOrderId);
                }
            }
        }

        public bool HasVenueOrder(Counterparty targetVenueCounterparty, string clientOrderId)
        {
            if (targetVenueCounterparty == Counterparty.NULL)
                return false;

            VenueOrderManager venueOrderManager = this._venueOrderManagers[(int)targetVenueCounterparty];
            return venueOrderManager != null && venueOrderManager.HasClientOrder(clientOrderId);
        }

        public IBankPoolClientOrder GetInternalBankpoolOrder(IClientOrder clientOrder)
        {
            return
                this._bankpoolOrderManagers[(int) clientOrder.OrderRequestInfo.IntegratorDealDirection][
                    (int) clientOrder.OrderRequestInfo.Symbol].GetOrder(clientOrder.ClientOrderIdentity);
        }

        public IntegratorRequestResult CancelOrder(CancelOrderRequestInfo cancelOrderRequestInfo,
                                                   ITakerUnicastInfoForwarder dispatchGateway)
        {
            if (dispatchGateway == null)
            {
                _logger.Log(LogLevel.Fatal, "DispatchGateway is null - cannot cancel any order");
                return IntegratorRequestResult.CreateFailedResult("DispatchGateway is null - cannot cancel any order");
            }


            switch (cancelOrderRequestInfo.TradingTargetType.GetTradingTargetCategory())
            {
                case TradingTargetCategory.BankPool:
                    SingleSymbolDirectionOrderManager orderManager;
                    lock (_registeredOrders)
                    {
                        if (!_registeredOrders.TryGetValue(cancelOrderRequestInfo.ClientOrderIdentity, out orderManager))
                        {
                            string error =
                                string.Format(
                                    "Attempt to cancel an order that is not known (it might be already closed): {0}. Ignoring it",
                                    cancelOrderRequestInfo.ClientOrderIdentity);
                            _logger.Log(LogLevel.Error, error);
                            return IntegratorRequestResult.CreateFailedResult(error);
                        }
                    }

                    return orderManager.CancelOrder(cancelOrderRequestInfo, dispatchGateway);
                    break;
                case TradingTargetCategory.Venue:
                    if (cancelOrderRequestInfo.CounterpartyMayBeNull != Counterparty.NULL)
                    {
                        VenueOrderManager venueOrderManager = this._venueOrderManagers[(int)cancelOrderRequestInfo.CounterpartyMayBeNull];
                        if (venueOrderManager == null)
                        {
                            string error = string.Format("Attempt to cancel Venue order, but there is no VenueOrderManager for specified counterparty. {0}", cancelOrderRequestInfo);
                            this._logger.Log(LogLevel.Error, error);
                            dispatchGateway.OnIntegratorClientOrderUpdate(
                                VenueClientOrderUpdateInfo.CreateFailedCancellationUnknownClientOrderUpdateInfo(
                                    cancelOrderRequestInfo, error), Common.Symbol.NULL);
                            return IntegratorRequestResult.CreateFailedResult(error);
                        }
                        else
                        {
                            venueOrderManager.TryCancelClientOrder(cancelOrderRequestInfo, dispatchGateway);
                            return IntegratorRequestResult.GetSuccessResult();
                        }
                    }

                    foreach (VenueOrderManager venueOrderManager in _venueOrderManagers.Where(mgr => mgr != null))
                    {
                        if (venueOrderManager.HasClientOrder(cancelOrderRequestInfo.ClientOrderIdentity))
                        {
                            venueOrderManager.TryCancelClientOrder(cancelOrderRequestInfo, dispatchGateway);
                            return IntegratorRequestResult.GetSuccessResult();
                        }
                    }
                    string errorVenue = string.Format("Unknown client order [{0}], cannot perform cancel action",
                                                      cancelOrderRequestInfo.ClientOrderIdentity);
                    this._logger.Log(LogLevel.Error, errorVenue);
                    dispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateFailedCancellationUnknownClientOrderUpdateInfo(
                            cancelOrderRequestInfo, errorVenue), Common.Symbol.NULL);
                    return IntegratorRequestResult.CreateFailedResult(errorVenue);

                    break;
                default:
                    string errorTargetType =
                        string.Format(
                            "Unknown TradingTargetType [{0}] for client order [{1}], cannot perform cancel action",
                            cancelOrderRequestInfo.TradingTargetType, cancelOrderRequestInfo.ClientOrderIdentity);
                    this._logger.Log(LogLevel.Error, errorTargetType);
                    return IntegratorRequestResult.CreateFailedResult(errorTargetType);
            }
        }

        //public IntegratorRequestResult FinalizeDeal(IntegratorDealInternalFinalizationRequest finalizeRequest,
        //                                           ITakerUnicastInfoForwarder dispatchGateway)
        //{
        //    if (dispatchGateway == null)
        //    {
        //        _logger.Log(LogLevel.Fatal, "DispatchGateway is null - cannot finlize any deal");
        //        return IntegratorRequestResult.CreateFailedResult("DispatchGateway is null - cannot finlize any deal");
        //    }

        //    if (finalizeRequest.Counterparty.TradingTargetCategory == TradingTargetCategory.BankPool)
        //    {
        //        string error = string.Format("Attempt to finalized deal [{0}] on bankpool - ignoring the attempt",
        //                                     finalizeRequest.IntegratorInternalTransactionIdentity);
        //        _logger.Log(LogLevel.Fatal, error);
        //        return IntegratorRequestResult.CreateFailedResult(error);
        //    }

        //    VenueOrderManager venueOrderManager = this._venueOrderManagers[(int)finalizeRequest.Counterparty];
        //    if (venueOrderManager == null)
        //    {
        //        string error = string.Format("Attempt to finalize Venue deal, but there is no VenueOrderManager for specified counterparty. {0}", finalizeRequest);
        //        this._logger.Log(LogLevel.Error, error);
        //        return IntegratorRequestResult.CreateFailedResult(error);
        //    }

        //    return venueOrderManager.TryFinalizeDeal(finalizeRequest);
        //}

        public IntegratorRequestResult CancelAllClientOrders(string clientId, ITakerUnicastInfoForwarder dispatchGateway)
        {
            foreach (SingleSymbolDirectionOrderManager orderManager in _bankpoolOrderManagers.SelectMany(mgr => mgr))
            {
                orderManager.CancelAllOrdersForClient(clientId, dispatchGateway);
            }

            foreach (VenueOrderManager venueOrderManager in _venueOrderManagers.Where(mgr => mgr != null))
            {
                venueOrderManager.CancelAllOrdersForClient(clientId, dispatchGateway);
            }

            return IntegratorRequestResult.GetSuccessResult();
        }

        public IntegratorRequestResult CancelAllMyOrders(ITakerUnicastInfoForwarder dispatchGateway)
        {
            foreach (SingleSymbolDirectionOrderManager orderManager in _bankpoolOrderManagers.SelectMany(mgr => mgr))
            {
                orderManager.CancelAllOrdersForDispatchGateway(dispatchGateway);
            }

            foreach (VenueOrderManager venueOrderManager in _venueOrderManagers.Where(mgr => mgr != null))
            {
                venueOrderManager.CancelAllOrdersForDispatchGateway(dispatchGateway);
            }

            return IntegratorRequestResult.GetSuccessResult();
        }
    }
}
