﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.BusinessLayer
{
    public class LegacySharedHeap<TNode, TIdentity> : IChangeablePriceBook<TNode, TIdentity> where TNode : class
    {
        private enum ToBChangeType
        {
            Improvement,
            Deterioration,
            Replacement
        }

        private TNode[] _heapArray;
        //SpinLock.Exit() (and SpinLock.Exit(true)) generates memory barrier - so volatile is not needed
        private /*volatile*/ TNode _currentTop;
        private Queue<TNode> _currentUnprocessedTopChanges = new Queue<TNode>();
        private Queue<ToBChangeType> _currentUnprocessedTopChangesType = new Queue<ToBChangeType>();
        private Queue<DateTime> _currentUnprocessedTopChangesTimeStamps = new Queue<DateTime>();
        private int _maxSize;
        private int _currentSize;
        private IComparer<TNode> _rankComparer;
        private Func<TNode, TNode, int> _rankCompareFunc;
        private Func<TNode, TNode, int> _topCompareFunc;
        private Func<TNode, TIdentity, bool> _identityEqualsFunc;
        private Func<TNode, TNode, bool> _itemsEqualsFunc;
        private Func<TNode, TNode, bool> _itemsIdenticalFunc;
        private Func<TNode, Guid, bool> _areIdenticalFunc;
        private SpinLock _spinLock = new SpinLock(false);
        private ILogger _logger;
        private TNode _nullNode;
        private volatile bool _isImprovementTick = false;

        public event BookPriceUpdate<TNode> BookTopImproved;
        public event BookPriceUpdate<TNode> BookTopDeterioratedInternal;
        public event BookPriceUpdate<TNode> BookTopDeteriorated;
        public event Action<DateTime> BookTopRemoved;
        public event BookPriceUpdate<TNode> BookTopReplaced;
        public event BookPriceUpdate<TNode> NewNodeArrived;
        public event BookPriceUpdate<TNode> ExistingNodeInvalidated;
        public event BookMultiplePricesUpdate<TNode, TIdentity> NodesRemoved;

        public event Action<TNode> ChangeableBookTopImproved;
        public event Action<TNode> ChangeableBookTopDeteriorated;
        public event Action<TNode> ChangeableBookTopReplaced;

        public LegacySharedHeap(int maxHeapSize, IComparer<TNode> rankComparer, IComparer<TNode> topComparer,
                          IIdentityEqualityComparer<TNode, TIdentity> equalityComparer, TNode nullNode, ILogger logger)
        {
            this._maxSize = maxHeapSize;
            this._currentSize = 0;
            this._heapArray = new TNode[_maxSize];
            this._rankComparer = rankComparer;
            this._rankCompareFunc = rankComparer.Compare;
            this._topCompareFunc = topComparer.Compare;
            this._identityEqualsFunc = equalityComparer.IdentityEquals;
            this._itemsEqualsFunc = equalityComparer.ItemsEquals;
            this._itemsIdenticalFunc = equalityComparer.ItemsAreIdentical;
            this._areIdenticalFunc = equalityComparer.AreIdentical;
            this._logger = logger;
            this._nullNode = nullNode;
            this._currentTop = nullNode;
        }

        public bool IsEmpty
        {
            get { return _currentSize == 0; }
        }

        public TNode[] GetSortedClone(Func<TNode, bool> selectorFunc)
        {
            throw new NotImplementedException();
        }

        public bool IsImprovement { get { return this._isImprovementTick; } }

        /// <summary>
        /// Reads the value acquiring full CPU fence
        /// </summary>
        public TNode MaxNodeInternal
        {
            get
            {
                //Read the value using the Interlocked method as under cover it enforces lock on entire cache line
                // This is not true about memory barriers (or volatile vars) - as they insert specific instructions (barriers:))
                // to prevent reordering - which eventualy also leads to refreshing of caches, but immediacy is not guaranteed
                return Interlocked.CompareExchange(ref _currentTop, null, null);
            }
        }

        public TNode MaxNode
        {
            get
            {
                var maxNode = _currentTop;
                if (Equals(maxNode, _nullNode)) return default(TNode);
                else return maxNode;
            }
        }

        public AtomicDecimal GetEstimatedBestPriceFast()
        {
            throw new NotImplementedException("If needed ask for implementation. LegacySharedHeap is a legacy unmaintained code");
        }

        public decimal GetWeightedPriceForSize(ref decimal maxSizeOnPrice, int maximumBookDepthToBeUsed)
        {
            throw new NotImplementedException("If needed ask for implementation. LegacySharedHeap is a legacy unmaintained code");
        }

        public decimal GetWeightedPriceForExactSize(decimal sizeOnPrice, int maximumBookDepthToBeUsed)
        {
            throw new NotImplementedException("If needed ask for implementation. LegacySharedHeap is a legacy unmaintained code");
        }

        public decimal GetMaximumSizeForExactPrice(decimal worstAcceptablePrice, int maximumBookDepthToBeUsed, decimal maximumSizeToStopProbing)
        {
            throw new NotImplementedException("If needed ask for implementation. LegacySharedHeap is a legacy unmaintained code"); 
        }

        public decimal GetMaximumSizeForExactPrice(decimal worstAcceptablePrice, int maximumBookDepthToBeUsed,
                decimal maximumSizeToStopProbing, Counterparty[] counterpartiesWhitelist)
        {
            throw new NotImplementedException("If needed ask for implementation. LegacySharedHeap is a legacy unmaintained code");
        }

        public DateTime MaxNodeUpdatedUtc { get; private set; }

        public TradingTargetType TradingTargetType
        {
            get { return TradingTargetType.BankPool; }
        }

        public TNode GetIdentical(TNode node)
        {
            TNode nodeToReturn;

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            //Find item (existing items will have idx < _currentSize)
            int idx = 0;
            for (; idx < _currentSize; idx++)
            {
                if (_itemsIdenticalFunc(_heapArray[idx], node))
                {
                    break;
                }
            }

            nodeToReturn = idx < _currentSize ? _heapArray[idx] : default(TNode);

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            return nodeToReturn;
        }

        public TNode[] GetSortedClone()
        {
            TNode[] heapArrayCopy = new TNode[_currentSize];

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            Array.Copy(_heapArray, heapArrayCopy, _currentSize);

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            Array.Sort(heapArrayCopy, _rankComparer);
            //We want the result in descending order
            Array.Reverse(heapArrayCopy);

            return heapArrayCopy;
        }

        public int AddOrUpdate(TNode value)
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            //Find item to update (new inserts will have idx > _currentSize)
            int idx = 0;
            for (; idx < _currentSize; idx++)
            {
                if (_itemsEqualsFunc(_heapArray[idx], value))
                {
                    break;
                }
            }

            int newIdx;
            bool topImproved = false;
            bool topDeclined = false;
            bool topReplaced = false;

            //This is update of existing item
            if (idx < _currentSize)
            {
                newIdx = UpdateAt(idx, value);
            }
            //This is  insert of new item
            else
            {
                newIdx = Insert(value);
            }

            if (idx == 0 || newIdx == 0)
            {
                //the _currentTop can be empty (default(TNode))
                int topRankCompare = Equals(_currentTop, _nullNode) ? 1 : _topCompareFunc(_heapArray[0], _currentTop);

                if (topRankCompare > 0)
                {
                    topImproved = true;
                }
                else if (topRankCompare < 0)
                {
                    topDeclined = true;
                }
                else if (!_itemsIdenticalFunc(_heapArray[0], _currentTop))
                {
                    topReplaced = true;
                }
            }

            //If something changed with the top
            if (topImproved || topDeclined || topReplaced)
            {
                _currentTop = _heapArray[0];
                this.MaxNodeUpdatedUtc = HighResolutionDateTime.UtcNow;

                _currentUnprocessedTopChanges.Enqueue(_currentTop);
                ToBChangeType changeType = topImproved
                                               ? ToBChangeType.Improvement
                                               : (topDeclined ? ToBChangeType.Deterioration : ToBChangeType.Replacement);
                _currentUnprocessedTopChangesType.Enqueue(changeType);
                _currentUnprocessedTopChangesTimeStamps.Enqueue(this.MaxNodeUpdatedUtc);
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            if (topImproved)
            {
                //setting this prior new node action
                this._isImprovementTick = true;
            }

            this.TryInvokeNodeAction(NewNodeArrived, value);

            //after this point _currentTopCan be changed - 'non-changeable' events get correct value from queue
            // changeable events will get actuall value and so it won't get values out of order
            if (topImproved)
            {
                TryInvokeTopImproved(_currentTop);
            }
            else if (topDeclined)
            {
                TryInvokeTopDeteriorated(_currentTop);
            }
            else if (topReplaced)
            {
                TryInvokeTopReplaced(_currentTop);
            }

            return newIdx;
        }


        public void ResortIdentical(TNode value)
        {
            int newIndex = -1;

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            //Find item to resort
            int idx = 0;
            for (; idx < _currentSize; idx++)
            {
                if (_itemsIdenticalFunc(_heapArray[idx], value))
                {
                    break;
                }
            }

            if (idx < _currentSize)
            {
                newIndex = CascadeDown(idx);
            }

            //did we 'downsort' the top?
            bool topDeclined = false;
            bool topReplaced = false;
            if (idx == 0 && newIndex != idx)
            {
                if ((_currentSize == 0 || _topCompareFunc(_currentTop, _heapArray[0]) != 0))
                {
                    topDeclined = true;
                }
                else
                {
                    topReplaced = true;
                }

                _currentTop = _heapArray[0];
                this.MaxNodeUpdatedUtc = HighResolutionDateTime.UtcNow;

                _currentUnprocessedTopChanges.Enqueue(_currentTop);
                _currentUnprocessedTopChangesType
                    .Enqueue(topDeclined ? ToBChangeType.Deterioration : ToBChangeType.Replacement);
                _currentUnprocessedTopChangesTimeStamps.Enqueue(this.MaxNodeUpdatedUtc);

            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            if (topDeclined)
            {
                TryInvokeTopDeteriorated(_currentTop);
            }
            else if (topReplaced)
            {
                TryInvokeTopReplaced(_currentTop);
            }
        }

        public bool RemoveIdentical(TNode value)
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            //Find item to remove
            int idx = 0;
            for (; idx < _currentSize; idx++)
            {
                if (_itemsIdenticalFunc(_heapArray[idx], value))
                {
                    break;
                }
            }

            bool removed = RemoveAtWithEvents(idx, lockTaken);

            if (removed && this.ExistingNodeInvalidated != null)
            {
                this.TryInvokeNodeAction(this.ExistingNodeInvalidated, value);
            }

            return removed;
        }

        public bool RemoveIdentical(Guid identificator)
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            //Find item to remove
            int idx = 0;
            for (; idx < _currentSize; idx++)
            {
                if (_areIdenticalFunc(_heapArray[idx], identificator))
                {
                    break;
                }
            }

            bool removed = RemoveAtWithEvents(idx, lockTaken);

            return removed;
        }

        public bool Remove(TIdentity identity)
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            int idx = 0;
            for (; idx < _currentSize; idx++)
            {
                if (_identityEqualsFunc(_heapArray[idx], identity))
                {
                    break;
                }
            }

            bool removed = RemoveAtWithEvents(idx, lockTaken);

            if (removed && this.NodesRemoved != null)
            {
                this.NodesRemoved(this, identity, HighResolutionDateTime.UtcNow);
            }

            return removed;
        }

        private bool RemoveAtWithEvents(int index, bool lockTaken)
        {
            bool removed = false;
            bool topDeclined = false;
            bool topReplaced = false;

            removed = RemoveAt(index);

            //We were removing the top and the new top (if present) is different
            if (removed && index == 0)
            {
                if ((_currentSize == 0 || _topCompareFunc(_currentTop, _heapArray[0]) != 0))
                {
                    topDeclined = true;
                }
                else
                {
                    topReplaced = true;
                }

                _currentTop = _currentSize == 0 ? this._nullNode : _heapArray[0];
                this.MaxNodeUpdatedUtc = HighResolutionDateTime.UtcNow;

                _currentUnprocessedTopChanges.Enqueue(_currentTop);
                _currentUnprocessedTopChangesType
                    .Enqueue(topDeclined ? ToBChangeType.Deterioration : ToBChangeType.Replacement);
                _currentUnprocessedTopChangesTimeStamps.Enqueue(this.MaxNodeUpdatedUtc);
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            if (topDeclined)
            {
                TryInvokeTopDeteriorated(_currentTop);
            }
            else if (topReplaced)
            {
                TryInvokeTopReplaced(_currentTop);
            }

            return removed;
        }

        private bool RemoveAt(int index)
        {
            bool success = false;

            if (index >= 0 && index < _currentSize)
            {
                _heapArray[index] = _heapArray[--_currentSize];
                int parent = (index - 1) / 2;
                if (_rankCompareFunc(_heapArray[index], _heapArray[parent]) > 0)
                    CascadeUp(index);
                else
                    CascadeDown(index);

                success = true;
            }

            return success;
        }

        private int Insert(TNode value)
        {
            int resultIndex = -1;

            if (_currentSize < _maxSize)
            {
                _heapArray[_currentSize] = value;
                resultIndex = CascadeUp(_currentSize++);
            }

            return resultIndex;
        }

        private int UpdateAt(int index, TNode newValue)
        {
            int resultIndex = -1;

            if (index >= 0 && index < _currentSize)
            {
                TNode oldValue = _heapArray[index];
                _heapArray[index] = newValue;

                if (_rankCompareFunc(oldValue, newValue) < 0)
                    resultIndex = CascadeUp(index);
                else
                    resultIndex = CascadeDown(index);
            }

            return resultIndex;
        }

        private int CascadeUp(int index)
        {
            int parent = (index - 1) / 2;
            TNode bottom = _heapArray[index];
            while (index > 0 && _rankCompareFunc(_heapArray[parent], bottom) < 0)
            {
                _heapArray[index] = _heapArray[parent];
                index = parent;
                parent = (parent - 1) / 2;
            }
            _heapArray[index] = bottom;
            return index;
        }

        private int CascadeDown(int index)
        {
            int largerChild;
            TNode top = _heapArray[index];
            while (index < _currentSize / 2)
            {
                int leftChild = 2 * index + 1;
                int rightChild = leftChild + 1;
                if (rightChild < _currentSize && _rankCompareFunc(_heapArray[leftChild], _heapArray[rightChild]) < 0)
                    largerChild = rightChild;
                else
                    largerChild = leftChild;
                if (_rankCompareFunc(top, _heapArray[largerChild]) >= 0)
                    break;
                _heapArray[index] = _heapArray[largerChild];
                index = largerChild;
            }
            _heapArray[index] = top;
            return index;
        }

        private void TryInvokeNodeAction(BookPriceUpdate<TNode> nodeAction, TNode nodeValue)
        {
            if (nodeAction != null)
            {
                try
                {
                    nodeAction(this, nodeValue, HighResolutionDateTime.UtcNow);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e,
                                              "Experienced unhandled exception during invocation of {0} with value {1}", nodeAction.Method.Name, nodeValue);
                }
            }
        }

        private void TryInvokeInternalNodeAction(Action<TNode> nodeAction, TNode nodeValue)
        {
            if (nodeAction != null)
            {
                try
                {
                    nodeAction(nodeValue);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e,
                                              "Experienced unhandled exception during invocation of {0} (changeable) with value {1}", nodeAction.Method.Name, nodeValue);
                }
            }
        }

        private bool GetNextTopEvent(out TNode value, out ToBChangeType toBChangeType, out DateTime timestamp)
        {
            toBChangeType = ToBChangeType.Deterioration;

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            if (_currentUnprocessedTopChanges.Count <= 0)
            {
                this._logger.Log(LogLevel.Fatal, "PriceBook was trying to invoke top change event, but the changed element is missing");
                _currentUnprocessedTopChangesType.Clear();
                _currentUnprocessedTopChangesTimeStamps.Clear();
                value = default(TNode);
                timestamp = DateTime.MinValue;
                return false;
            }

            value = _currentUnprocessedTopChanges.Dequeue();

            if (_currentUnprocessedTopChangesType.Count <= 0)
            {
                this._logger.Log(LogLevel.Fatal, "PriceBook was trying to invoke top change event, but the event type is missing");
                _currentUnprocessedTopChanges.Clear();
                _currentUnprocessedTopChangesTimeStamps.Clear();
                timestamp = DateTime.MinValue;
                return false;
            }

            toBChangeType = _currentUnprocessedTopChangesType.Dequeue();

            if (_currentUnprocessedTopChangesTimeStamps.Count <= 0)
            {
                this._logger.Log(LogLevel.Fatal, "PriceBook was trying to invoke top change event, but the event timestamp is missing");
                _currentUnprocessedTopChanges.Clear();
                _currentUnprocessedTopChangesType.Clear();
                timestamp = DateTime.MinValue;
                return false;
            }

            timestamp = _currentUnprocessedTopChangesTimeStamps.Dequeue();

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            return true;
        }

        private readonly object _eventsSerializationLocker = new object();

        private void TryInvokeTopEvent()
        {
            lock (_eventsSerializationLocker)
            {
                TNode value;
                ToBChangeType toBChangeType;
                DateTime timestamp;

                if (this.GetNextTopEvent(out value, out toBChangeType, out timestamp))
                {
                    if (toBChangeType == ToBChangeType.Improvement && BookTopImproved != null)
                    {
                        try
                        {
                            BookTopImproved(this, value, timestamp);
                        }
                        catch (Exception e)
                        {
                            this._logger.LogException(LogLevel.Fatal, e,
                                                      "Experienced unhandled exception during invocation of BookTopImproved with value {1}",
                                                      value);
                        }
                    }
                    else if (toBChangeType == ToBChangeType.Deterioration)
                    {
                        try
                        {
                            if (BookTopDeterioratedInternal != null)
                                BookTopDeterioratedInternal(this, value, timestamp);

                            if (Equals(value, _nullNode))
                            {
                                if (BookTopRemoved != null)
                                {
                                    BookTopRemoved(timestamp);
                                }
                            }
                            else
                            {
                                if (BookTopDeteriorated != null)
                                {
                                    BookTopDeteriorated(this, value, timestamp);
                                }
                            }

                        }
                        catch (Exception e)
                        {
                            this._logger.LogException(LogLevel.Fatal, e,
                                                      "Experienced unhandled exception during invocation of BookTopDeteriorated with value {1}",
                                                      value);
                        }
                    }
                    else if (toBChangeType == ToBChangeType.Replacement && BookTopReplaced != null)
                    {
                        try
                        {
                            BookTopReplaced(this, value, timestamp);
                        }
                        catch (Exception e)
                        {
                            this._logger.LogException(LogLevel.Fatal, e,
                                                      "Experienced unhandled exception during invocation of BookTopReplaced with value {1}",
                                                      value);
                        }
                    }
                }
            }
        }

        private void TryInvokeTopImproved(TNode currentTopValue)
        {
            this._isImprovementTick = true;
            TryInvokeInternalNodeAction(ChangeableBookTopImproved, currentTopValue);
            TryInvokeTopEvent();
        }

        private void TryInvokeTopDeteriorated(TNode currentTopValue)
        {
            this._isImprovementTick = false;
            TryInvokeInternalNodeAction(ChangeableBookTopDeteriorated, currentTopValue);
            TryInvokeTopEvent();
        }

        private void TryInvokeTopReplaced(TNode currentTopValue)
        {
            TryInvokeInternalNodeAction(ChangeableBookTopReplaced, currentTopValue);
            TryInvokeTopEvent();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);
            TNode[] heapArrayCopy = new TNode[_currentSize];
            Array.Copy(_heapArray, heapArrayCopy, _currentSize);
            if (lockTaken)
            {
                _spinLock.Exit();
            }

            sb.AppendLine();
            sb.Append("Elements of the Heap Array are : ");
            for (int m = 0; m < _currentSize; m++)
                if (heapArrayCopy[m] != null)
                    sb.Append(heapArrayCopy[m].ToString() + " ");
                else
                    sb.Append("-- ");
            sb.AppendLine();
            int emptyLeaf = 32;
            int itemsPerRow = 1;
            int column = 0;
            int j = 0;
            sb.AppendLine("..............................................................");
            while (_currentSize > 0)
            {
                if (column == 0)
                    for (int k = 0; k < emptyLeaf; k++)
                        sb.Append(' ');
                sb.Append(heapArrayCopy[j].ToString());

                if (++j == _currentSize)
                    break;
                if (++column == itemsPerRow)
                {
                    emptyLeaf /= 2;
                    itemsPerRow *= 2;
                    column = 0;
                    sb.AppendLine();
                }
                else
                    for (int k = 0; k < emptyLeaf * 2 - 2; k++)
                        sb.Append(' ');
            }
            sb.AppendLine();
            sb.AppendLine("..............................................................");

            return sb.ToString();
        }
    }
}
