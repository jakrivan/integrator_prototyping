﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.FIXMessaging;

namespace Kreslik.Integrator.BusinessLayer
{
    public class VenueOrderManager
    {
        private VenueOrderFlowSession _venueOrderFlowSession;
        private ILogger _logger;
        private IRiskManager _riskManager;
        private ConcurrentDictionary<string, string> _clientToExternalOrderMapping = new ConcurrentDictionary<string, string>();
        private ConcurrentDictionary<string, VenueClientOrder> _externalToClientOrderMapping = new ConcurrentDictionary<string, VenueClientOrder>();

        private Counterparty Counterparty { get; set; }

        public VenueOrderManager(VenueOrderFlowSession venueOrderFlowSession, ILogger logger, IRiskManager riskManager)
        {
            if (venueOrderFlowSession == null)
            {
                logger.Log(LogLevel.Error,
                           "OrderFlowSession is null during VenueOrderManager creation. Will not be able to register any Venue orders");
            }

            this._venueOrderFlowSession = venueOrderFlowSession;
            this.Counterparty = venueOrderFlowSession == null ? Counterparty.NULL : venueOrderFlowSession.Counterparty;
            this._logger = logger;
            this._riskManager = riskManager;

            _oldOrdersClearingTimer = new SafeTimer(ClearOldCancellationWaiters, TimeSpan.FromMinutes(1),
                TimeSpan.FromMinutes(1), true)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1sec,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest,
                IsUltraQuickCallback = false
            };
        }

        public bool HasClientOrder(string clientOrderId)
        {
            //return _clientToExternalOrderMapping.ContainsKey(clientOrderId);

            string externalOrderId;
            if (!_clientToExternalOrderMapping.TryGetValue(clientOrderId, out externalOrderId))
            {
                return false;
            }

            VenueClientOrder clientOrder;
            if (!_externalToClientOrderMapping.TryGetValue(externalOrderId, out clientOrder))
            {
                return false;
            }

            //pretend that order with pending LL is not know - since we can receive CancelRequestFaile, order may appear to be active,
            // but we know that we will not receive next deal on it if we already sent reject
            return clientOrder.PendingLLDealsCount <= 0;
        }

        public void RegisterVenueOrder(VenueClientOrder clientOrder, ITakerUnicastInfoForwarder dispatchGateway)
        {
            this._logger.Log(LogLevel.Info, "Registering client order with Venue orders manager: {0}", clientOrder);

            if (this._venueOrderFlowSession == null)
            {
                this._logger.Log(LogLevel.Error, "Attempt to register Venue order, but VenueOrderFlowSession was null during construction (it's likely not turned on in configuration), so rejecting");
                dispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                                                                  "VenueOrderFlowSession is not likely configured to be on (it is null)"), clientOrder.OrderRequestInfo.Symbol);
                return;
            }

            if (this._venueOrderFlowSession.State != SessionState.Running)
            {
                this._logger.Log(LogLevel.Error,
                                 "Attempt to register Venue order, but VenueOrderFlowSession is not running, so rejecting");
                dispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                                                                  "VenueOrderFlowSession is not running"), clientOrder.OrderRequestInfo.Symbol);
                return;
            }

            if (this._riskManager.IsTransmissionDisabled)
            {
                this._logger.Log(LogLevel.Error,
                                 "Attempt to register Venue order {0}, but order transmission is currently disabled",
                                 clientOrder.ClientOrderIdentity);
                dispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder, "Order Transmission is Disabled"), clientOrder.OrderRequestInfo.Symbol);
                return;
            }

            switch (clientOrder.VenueClientOrderRequestInfo.RequestType)
            {
                case VenueRequestType.NotAVenueRequest:
                    //error out
                    this._logger.Log(LogLevel.Error, "An attempt to register non-Venue order with Venue order manager: {0}", clientOrder);
                    dispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                                                                  "Cannot register non-Venue order in Venue order manager"), clientOrder.OrderRequestInfo.Symbol);
                    break;
                case VenueRequestType.SingleOrder:

                    if (!clientOrder.OrderRequestInfo.RequestedPrice.HasValue)
                    {
                        this._logger.Log(LogLevel.Error, "Attempt to register Venue order without requested price {0}", clientOrder);
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder, 
                                                                                  "RequestedPrice is a mandatory field"), clientOrder.OrderRequestInfo.Symbol);
                        return;
                    }

                    if (_clientToExternalOrderMapping.ContainsKey(clientOrder.ClientOrderIdentity))
                    {
                        this._logger.Log(LogLevel.Error, "Attempt to register Venue order that is already registered {0}", clientOrder);
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                                                                  "Client order with same ClientOrderIdentity is already registered"), clientOrder.OrderRequestInfo.Symbol);
                        return;
                    }

                    OrderRequestInfo ori;

                    if (clientOrder.VenueClientOrderRequestInfo.OrderType == OrderType.Pegged)
                    {

                        this._logger.Log(LogLevel.Error,
                                             "Attempt to register pegged venue order for counterparty that doesn't support pegged orders",
                                             clientOrder);
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                                                                             "Selected Venue doesn't support pegged orders"), clientOrder.OrderRequestInfo.Symbol);
                        return;
                    }


                    ori = OrderRequestInfo.CreateLimit(
                        clientOrder.OrderRequestInfo.SizeBaseAbsInitial,
                        clientOrder.OrderRequestInfo.RequestedPrice.Value,
                        clientOrder.OrderRequestInfo.IntegratorDealDirection,
                        clientOrder.OrderRequestInfo.Symbol,
                        clientOrder.VenueClientOrderRequestInfo.TimeInForce,
                        clientOrder.VenueClientOrderRequestInfo.SizeBaseAbsFillMinimum,
                        clientOrder.IsRiskRemovingOrder);


                    string errorMessage;
                    IIntegratorOrderExternal orderExternal = this._venueOrderFlowSession.GetNewExternalOrder(ori, out errorMessage);
                    if (orderExternal == null)
                    {
                        this._logger.Log(LogLevel.Error, "Order {0} failed during submission in messaging layer", clientOrder);
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                                                                             "Failed during creation of external order. Reason: " + errorMessage), clientOrder.OrderRequestInfo.Symbol);
                        return;
                    }

                    var riskResult = this._riskManager.AddInternalOrderAndCheckIfAllowed(clientOrder);
                    if (riskResult != RiskManagementCheckResult.Accepted)
                    {
                        this._logger.Log(LogLevel.Error, "Order {0} failed during risk management check ({1})",
                            clientOrder, riskResult);
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                "Failed during risk checking. Reason: " + riskResult),
                            clientOrder.OrderRequestInfo.Symbol);
                        return;
                    }


                    clientOrder.InitializeInOrderManager(dispatchGateway);
                    _clientToExternalOrderMapping.TryAdd(clientOrder.ClientOrderIdentity, orderExternal.Identity);
                    _externalToClientOrderMapping.TryAdd(orderExternal.Identity, clientOrder);
                    orderExternal.OnOrderChanged += OnOrderChanged;
                    this._logger.Log(LogLevel.Info,
                                     "Venue client order [{0}] mapped to external order [{1}], [{2}]",
                                     clientOrder.ClientOrderIdentity, orderExternal.Identity,
                                     orderExternal.UniqueInternalIdentity);

                    if (this._venueOrderFlowSession.SubmitOrder(orderExternal) == SubmissionResult.Success)
                    {
                        //send only once the order is sent out physically - and put correct status for a case that response came earlier than we executed this
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateOpenClientOrderUpdateInfo(clientOrder), clientOrder.OrderRequestInfo.Symbol);
                    }
                    else
                    {
                        this._logger.Log(LogLevel.Error, "Order {0} failed during submission in messaging layer", clientOrder);
                        this._riskManager.CancelInternalOrder(clientOrder, clientOrder.OrderRequestInfo.SizeBaseAbsInitial);

                        orderExternal.OnOrderChanged -= OnOrderChanged;
                        RemoveMappings(orderExternal.Identity, clientOrder);

                        dispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                                                                      "Failed during submission in messaging layer"), clientOrder.OrderRequestInfo.Symbol);   
                    }

                    break;
                case VenueRequestType.SingleOrderReplace:

                    string externalOrderToReplaceId;
                    if (!_clientToExternalOrderMapping.TryGetValue(clientOrder.VenueClientOrderRequestInfo.OrderIdToCancel, out externalOrderToReplaceId))
                    {
                        this._logger.Log(LogLevel.Info, "Attempt to replace unknown order {0}", clientOrder.ClientOrderIdentity);
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder, 
                                                                                      "Unknown order. It might have been already filled/cancelled/rejected"), clientOrder.OrderRequestInfo.Symbol);
                        return;
                    }

                    VenueClientOrder clientOrderToReplace;
                    if (!_externalToClientOrderMapping.TryGetValue(externalOrderToReplaceId, out clientOrderToReplace))
                    {
                        this._logger.Log(LogLevel.Info, "Attempt to replace unknown order (ClientOrderInfo is unknown) {0}", clientOrder.ClientOrderIdentity);
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder, 
                                                                                      "Unknown order. It might have been already filled/cancelled/rejected"), clientOrder.OrderRequestInfo.Symbol);
                        return;
                    }

                    if (!clientOrder.OrderRequestInfo.RequestedPrice.HasValue)
                    {
                        this._logger.Log(LogLevel.Error, "Attempt to replace Venue order with order without requested price {0}", clientOrder);
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder, 
                                                                                  "RequestedPrice is a mandatory field"), clientOrder.OrderRequestInfo.Symbol);
                        return;
                    }

                    if (_clientToExternalOrderMapping.ContainsKey(clientOrder.ClientOrderIdentity))
                    {
                        this._logger.Log(LogLevel.Error, "Attempt to replace Venue order with order with orderIdentity that is already registered {0}", clientOrder);
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                                                                  "Client order with same ClientOrderIdentity is already registered"), clientOrder.OrderRequestInfo.Symbol);
                        return;
                    }


                    OrderRequestInfo replacingOri = OrderRequestInfo.CreateLimit(
                        clientOrder.OrderRequestInfo.SizeBaseAbsInitial,
                        clientOrder.OrderRequestInfo.RequestedPrice.Value,
                        clientOrder.OrderRequestInfo.IntegratorDealDirection,
                        clientOrder.OrderRequestInfo.Symbol,
                        clientOrder.VenueClientOrderRequestInfo.TimeInForce,
                        clientOrder.VenueClientOrderRequestInfo.SizeBaseAbsFillMinimum,
                        clientOrder.IsRiskRemovingOrder);


                    var riskRes = this._riskManager.AddInternalOrderAndCheckIfAllowed(clientOrder);
                    if (riskRes != RiskManagementCheckResult.Accepted)
                    {
                        this._logger.Log(LogLevel.Error, "Order {0} failed during risk management check ({0})",
                            clientOrder, riskRes);
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                "Failed during risk checking. Reason: " + riskRes),
                            clientOrder.OrderRequestInfo.Symbol);
                        return;
                    }

                    string replacingErrorMessage;
                    IIntegratorOrderExternal replacingOrderExternal = this._venueOrderFlowSession.GetNewExternalOrder(replacingOri, out replacingErrorMessage);
                    if (replacingOrderExternal == null)
                    {
                        this._logger.Log(LogLevel.Error, "Order {0} couldn't generate valid replacement request (it is possible that the client request is invalid)", clientOrder);
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                                                                      "Order couldn't generate valid replacement request. Reason: " + replacingErrorMessage), clientOrder.OrderRequestInfo.Symbol);
                        return;
                    }

                    if (!clientOrderToReplace.TrySetOrderBeingCancelledIfNotClosed())
                    {
                        this._logger.Log(LogLevel.Error, "Attempt to replace closed order {0}", clientOrderToReplace.ClientOrderIdentity);
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                                                                      "Cannot cancel order, it is already closed."), clientOrder.OrderRequestInfo.Symbol);
                        return;
                    }

                    VenueClientOrder toBeReplacedClientOrderInfo = clientOrder;
                    clientOrder.InitializeInOrderManager(dispatchGateway);
                    _clientToExternalOrderMapping.TryAdd(clientOrder.ClientOrderIdentity, replacingOrderExternal.Identity);
                    _externalToClientOrderMapping.TryAdd(replacingOrderExternal.Identity, toBeReplacedClientOrderInfo);
                    replacingOrderExternal.OnOrderChanged += OnOrderChanged;

                    this._logger.Log(LogLevel.Info,
                                         "Venue client order [{0}] (replacing client order [{1}] mapping to external order [{2}]) mapped to external order [{3}], [{4}]",
                                         clientOrder.ClientOrderIdentity, clientOrder.VenueClientOrderRequestInfo.OrderIdToCancel, externalOrderToReplaceId,
                                         replacingOrderExternal.Identity, replacingOrderExternal.UniqueInternalIdentity);

                    if (this._venueOrderFlowSession.ReplaceOrder(externalOrderToReplaceId, replacingOrderExternal))
                    {
                        dispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateOpenClientOrderUpdateInfo(clientOrder), clientOrder.OrderRequestInfo.Symbol);
                    }
                    else
                    {
                        this._logger.Log(LogLevel.Error, "Order {0} failed during submission in messaging layer", clientOrder);
                        this._riskManager.CancelInternalOrder(clientOrder, clientOrder.OrderRequestInfo.SizeBaseAbsInitial);

                        RemoveMappings(replacingOrderExternal.Identity, toBeReplacedClientOrderInfo);
                        replacingOrderExternal.OnOrderChanged -= OnOrderChanged;
                        clientOrderToReplace.SetOrderCancellationDone(false);
                        //clientOrderToReplace might have get filled in parallel
                        RemoveOrderIfDone(clientOrderToReplace);

                        dispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                                                                      "Failed during submission in messaging layer"), clientOrder.OrderRequestInfo.Symbol);
                    }

                    break;
                default:
                    //error out
                    this._logger.Log(LogLevel.Error, "An attempt to register order with unknown type: {0}, order: {1}", clientOrder.VenueClientOrderRequestInfo.RequestType, clientOrder);
                    dispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateFullyRejectedClientOrderUpdateInfo(clientOrder,
                                                                                  "Unknown VenueRequestType"), clientOrder.OrderRequestInfo.Symbol);
                    break;
            }
        }

        private void RemoveOrderIfDone(VenueClientOrder venueOrder)
        {
            if (venueOrder.OrderStatus == ClientOrderStatus.NotActiveInIntegrator)
            {
                string externalOrderId;
                if (_clientToExternalOrderMapping.TryGetValue(venueOrder.ClientOrderIdentity, out externalOrderId))
                {
                    RemoveMappings(externalOrderId, venueOrder);

                    venueOrder.DispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateNotActiveClientOrderUpdateInfo(
                                venueOrder,
                                0m, null, this.Counterparty), venueOrder.Symbol);
                }
            }
        }

        public void TryCancelClientOrder(CancelOrderRequestInfo cancelOrderRequestInfo, ITakerUnicastInfoForwarder dispatchGateway)
        {
            if (this._venueOrderFlowSession == null)
            {
                this._logger.Log(LogLevel.Error,
                                 "Attempt to cancel Venue order, but VenueOrderFlowSession was null during construction (it's likely not turned on in configuration), so rejecting");
                dispatchGateway.OnIntegratorClientOrderUpdate(
                    VenueClientOrderUpdateInfo.CreateFailedCancellationUnknownClientOrderUpdateInfo(
                        cancelOrderRequestInfo,
                        "VenueOrderFlowSession is not likely configured to be on (it is null)"), Common.Symbol.NULL);
                return;
            }

            string externalOrderId;
            if (!_clientToExternalOrderMapping.TryGetValue(cancelOrderRequestInfo.ClientOrderIdentity, out externalOrderId))
            {
                this._logger.Log(LogLevel.Info, "Attempt to cancel unknown order {0}", cancelOrderRequestInfo.ClientOrderIdentity);
                dispatchGateway.OnIntegratorClientOrderUpdate(
                    VenueClientOrderUpdateInfo.CreateFailedCancellationUnknownClientOrderUpdateInfo(
                        cancelOrderRequestInfo, "Order is unknown"), Common.Symbol.NULL);
                return;
            }

            VenueClientOrder clientOrderToCancel;
            if (!_externalToClientOrderMapping.TryGetValue(externalOrderId, out clientOrderToCancel))
            {
                this._logger.Log(LogLevel.Info, "Attempt to replace unknown order (ClientOrderInfo is unknown) {0}", cancelOrderRequestInfo.ClientOrderIdentity);
                dispatchGateway.OnIntegratorClientOrderUpdate(
                    VenueClientOrderUpdateInfo.CreateFailedCancellationUnknownClientOrderUpdateInfo(
                        cancelOrderRequestInfo, "Order is unknown"), Common.Symbol.NULL);
                return;
            }

            if (this._venueOrderFlowSession.State != SessionState.Running)
            {
                this._logger.Log(LogLevel.Error,
                                 "Attempt to cancel Venue order, but VenueOrderFlowSession is not running, so rejecting");
                dispatchGateway.OnIntegratorClientOrderUpdate(

                    VenueClientOrderUpdateInfo.CreateVenueClientOrderUpdateInfo(
                        clientOrderToCancel, clientOrderToCancel.OrderStatus, ClientOrderCancelRequestStatus.CancelRequestFailed, 0m,
                        0m, clientOrderToCancel.SizeBaseAbsActive, "VenueOrderFlowSession is not running", this.Counterparty), clientOrderToCancel.Symbol);
                return;
            }

            //if not active - report it as successfull cancel
            if (clientOrderToCancel.OrderStatus == ClientOrderStatus.NotActiveInIntegrator || clientOrderToCancel.OrderStatus == ClientOrderStatus.RemovedFromIntegrator)
            {
                this._logger.Log(LogLevel.Info, "Attempt to cancel closed order waiting for additional cancellation responces - autocancelling it {0}", clientOrderToCancel.ClientOrderIdentity);
                dispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateFullyCancelledClientOrderUpdateInfo(
                            clientOrderToCancel, ClientOrderStatus.RemovedFromIntegrator, 0,
                            this.Counterparty.TradingTargetType), clientOrderToCancel.Symbol);
                return;
            }

            if (!clientOrderToCancel.TrySetOrderBeingCancelledIfNotClosed())
            {
                this._logger.Log(LogLevel.Error, "Attempt to cancel closed order {0}", clientOrderToCancel.ClientOrderIdentity);
                dispatchGateway.OnIntegratorClientOrderUpdate(
                    VenueClientOrderUpdateInfo.CreateFailedCancellationUnknownClientOrderUpdateInfo(
                        cancelOrderRequestInfo, "Order is closed"), clientOrderToCancel.Symbol);
                return;
            }

            this._logger.Log(LogLevel.Info,
                                 "Venue order manager cancelling external order [{0}] mapped to client order [{1}]",
                                 externalOrderId, cancelOrderRequestInfo.ClientOrderIdentity);
            //dispatch the in progress info first - as after sending cancel request the response might came earlier than
            //  we manage to dispatch this info
            dispatchGateway.OnIntegratorClientOrderUpdate(
                VenueClientOrderUpdateInfo.CreateVenueClientOrderUpdateInfo(
                    clientOrderToCancel, clientOrderToCancel.OrderStatus,
                    ClientOrderCancelRequestStatus.CancelRequestInProgress, 0m, 0m,
                    clientOrderToCancel.SizeBaseAbsActive, null, this.Counterparty), clientOrderToCancel.Symbol);

            CancelRequestResult cancelResult = this._venueOrderFlowSession.CancelOrder(externalOrderId);
            if (cancelResult != CancelRequestResult.Success)
            {
                clientOrderToCancel.SetOrderCancellationDone(false);

                bool failedDueToAlreadyClosed = cancelResult == CancelRequestResult.Failed_OrderNotKnown ||
                                                cancelResult == CancelRequestResult.Failed_OrderAlreadyClosed;

                this._logger.Log(
                    failedDueToAlreadyClosed ? LogLevel.Info : LogLevel.Error,
                    "Order cancellation {0} failed ({1}) during submission in messaging layer",
                    cancelOrderRequestInfo.ClientOrderIdentity, cancelResult);
                dispatchGateway.OnIntegratorClientOrderUpdate(
                    VenueClientOrderUpdateInfo.CreateVenueClientOrderUpdateInfo(
                        clientOrderToCancel,
                        failedDueToAlreadyClosed ? ClientOrderStatus.NotActiveInIntegrator : clientOrderToCancel.OrderStatus,
                        ClientOrderCancelRequestStatus
                            .CancelRequestFailed, 0m, 0m, clientOrderToCancel.SizeBaseAbsActive,
                        "Cancellation failed in messaging layer during submission",
                        this.Counterparty), clientOrderToCancel.Symbol);
                //clientOrderToReplace might have get filled in parallel
                RemoveOrderIfDone(clientOrderToCancel);
            }
        }

        public void CancelAllOrdersForClient(string clientId, ITakerUnicastInfoForwarder dispatchGateway)
        {
            if (this._venueOrderFlowSession != null)
            {
                this._logger.Log(LogLevel.Info,
                                 "CancelAllOrdersForClient on Venue session {0} called for clientId: [{1}]",
                                 this._venueOrderFlowSession.Counterparty, clientId);
                foreach (
                    VenueClientOrder venueClientOrderInfo in
                        _externalToClientOrderMapping.Where(
                            pair =>
                            pair.Value.DispatchGateway == dispatchGateway && pair.Value.ClientIdentity == clientId)
                                                     .Select(pair => pair.Value))
                {
                    this.TryCancelClientOrder(
                        new CancelOrderRequestInfo(venueClientOrderInfo.ClientOrderIdentity,
                                                   venueClientOrderInfo.ClientIdentity, this.Counterparty),
                        venueClientOrderInfo.DispatchGateway);
                }
            }
        }

        public void CancelAllOrdersForDispatchGateway(ITakerUnicastInfoForwarder dispatchGateway)
        {
            if (this._venueOrderFlowSession != null)
            {
                this._logger.Log(LogLevel.Info, "CancelAllOrdersForDispatchGateway on Venue session {0} called",
                                 this._venueOrderFlowSession.Counterparty);
                foreach (
                    VenueClientOrder venueClientOrderInfo in
                        _externalToClientOrderMapping.Where(pair => pair.Value.DispatchGateway == dispatchGateway)
                                                     .Select(pair => pair.Value))
                {
                    this.TryCancelClientOrder(
                        new CancelOrderRequestInfo(venueClientOrderInfo.ClientOrderIdentity,
                                                   venueClientOrderInfo.ClientIdentity, this.Counterparty),
                        venueClientOrderInfo.DispatchGateway);
                }
            }
        }

        public int ActiveOrdersCount
        {
            get { return _externalToClientOrderMapping.Count; }
        }

        public int ActiveRiskRemovingOrdersCount
        {
            get { return _externalToClientOrderMapping.Values.Count(o => o.OrderRequestInfo.IsRiskRemovingOrder); }
        }

        public void CancelAllOrders()
        {
            if (this._venueOrderFlowSession != null)
            {
                this._logger.Log(LogLevel.Info, "CancelAllOrders on Venue session {0} called",
                                 this._venueOrderFlowSession.Counterparty);
                foreach (
                    VenueClientOrder venueClientOrderInfo in
                        _externalToClientOrderMapping.Select(pair => pair.Value))
                {
                    this.TryCancelClientOrder(
                        new CancelOrderRequestInfo(venueClientOrderInfo.ClientOrderIdentity,
                                                   venueClientOrderInfo.ClientIdentity, this.Counterparty),
                        venueClientOrderInfo.DispatchGateway);
                }
            }
        }

        private readonly List<IntegratorOrderExternalChange> _nonconfirmingStates = new List<IntegratorOrderExternalChange>()
            {
                IntegratorOrderExternalChange.NotSubmitted,
                IntegratorOrderExternalChange.Submitted,
                //this is fine to confirm this, as there is othermessage comming in
                //IntegratorOrderExternalChange.PendingCancel,
                IntegratorOrderExternalChange.InBrokenState,
                IntegratorOrderExternalChange.CancelRejected,
            };

        private void OnOrderChanged(IIntegratorOrderExternal integratorOrderExternal, OrderChangeEventArgs orderChangeEventArgs)
        {
            VenueClientOrder venueClientOrderInfo;
            if (!_externalToClientOrderMapping.TryGetValue(integratorOrderExternal.Identity, out venueClientOrderInfo))
            {
                this._logger.Log(LogLevel.Fatal,
                                 "VenueOrderManager receiving external order [{0}] but cannot find matching internal child order. This can happen if unexpectedly multiple execution reports come in.",
                                 integratorOrderExternal.Identity);
                return;
            }

            if (!_nonconfirmingStates.Contains(orderChangeEventArgs.ChangeState))
            {
                venueClientOrderInfo.SetConfiremdByCounterparty();
            }

            switch (orderChangeEventArgs.ChangeState)
            {
                case IntegratorOrderExternalChange.Submitted:                
                case IntegratorOrderExternalChange.PendingCancel:
                    //no need to do anything
                    break;
                case IntegratorOrderExternalChange.ConfirmedNew:        
                    venueClientOrderInfo.SetConfiremdByCounterparty();
                    venueClientOrderInfo.DispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateConfirmedClientOrderUpdateInfo(
                            venueClientOrderInfo,
                            this.Counterparty, venueClientOrderInfo.SizeBaseAbsActive),
                        venueClientOrderInfo.Symbol);

                    break;
                case IntegratorOrderExternalChange.CancelRejected:

                    venueClientOrderInfo.SetOrderCancellationDone(false);

                    if (venueClientOrderInfo.OrderStatus == ClientOrderStatus.NotActiveInIntegrator)
                    {
                        RemoveMappings(integratorOrderExternal.Identity, venueClientOrderInfo);
                    }

                    if (!integratorOrderExternal.HasRemainingAmount && venueClientOrderInfo.SizeBaseAbsActive > 0)
                    {
                        this._logger.Log(LogLevel.Info, "[{0}] mapped to [{1}] has remaining amount of [{2}], despite the external order has no active amount (possible result of pending LL. Pending Ls for external: {3}), zeroing",
                            venueClientOrderInfo.Identity, integratorOrderExternal.Identity, venueClientOrderInfo.SizeBaseAbsActive, integratorOrderExternal.HasPendingLLDeals);
                        venueClientOrderInfo.SubtractAmountAndGetIsFullyDone(venueClientOrderInfo.SizeBaseAbsActive,
                            false);
                    }

                    venueClientOrderInfo.DispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateVenueClientOrderUpdateInfo(
                            venueClientOrderInfo,
                            venueClientOrderInfo.OrderStatus,
                            ClientOrderCancelRequestStatus.CancelRequestFailed, 0m, 0m,
                            venueClientOrderInfo.SizeBaseAbsActive,
                            "Cancellation rejected by Counterparty. Reason: " +
                            orderChangeEventArgs.CounterpartyReason, this.Counterparty), venueClientOrderInfo.Symbol);

                    break;
                case IntegratorOrderExternalChange.NonConfirmedDeal:

                    venueClientOrderInfo.PendingLLDealsCount++;

                    venueClientOrderInfo.DispatchGateway.OnIntegratorUnconfirmedDealInternal(
                        new IntegratorUnconfirmedDealInternal(venueClientOrderInfo.ClientOrderIdentity, null,
                                                   venueClientOrderInfo.ClientIdentity,
                                                   integratorOrderExternal.Identity,
                                                   orderChangeEventArgs.ExecutionInfo.CounterpartyTransactionId,
                                                   orderChangeEventArgs.ExecutionInfo.IntegratorTransactionId,
                                                   orderChangeEventArgs.ExecutionInfo.FilledAmount.Value,
                                                   orderChangeEventArgs.ExecutionInfo.UsedPrice.Value,
                                                   integratorOrderExternal.OrderRequestInfo.Side,
                                                   integratorOrderExternal.OrderRequestInfo.Symbol,
                                                   integratorOrderExternal.IntegratorSentTimeUtc,
                                                   integratorOrderExternal.IntegratorReceivedResultTimeUtc,
                                                   integratorOrderExternal.CounterpartySentResultTimeUtc,
                                                   integratorOrderExternal.CounterpartyTransactionTimeUtc,
                                                   orderChangeEventArgs.ExecutionInfo.SettlementDate.Value,
                                                   orderChangeEventArgs.ExecutionInfo.SettlementTimeCounterpartyUtc,
                                                   orderChangeEventArgs.ExecutionInfo.SettlementTimeCentralBankUtc,
                                                   integratorOrderExternal.Counterparty, false,
                                                   orderChangeEventArgs.ExecutionInfo.FlowSide ??
                                                   integratorOrderExternal.OrderRequestInfo.FlowSideAtSubmitTime,
                                                   integratorOrderExternal.Counterparty.TradingTargetType, null,
                                                   venueClientOrderInfo.IntegratedTradingSystemIdentification,
                                                   orderChangeEventArgs));
                    break;


                case IntegratorOrderExternalChange.Filled:
                case IntegratorOrderExternalChange.PartiallyFilled:

                    decimal filledAmount = orderChangeEventArgs.ExecutionInfo.FilledAmount.Value;
                    decimal usedPrice = orderChangeEventArgs.ExecutionInfo.UsedPrice.Value;
                    this._logger.Log(LogLevel.Info, "VenueOrderManager: External order [{0}] (mapped to internal order {3}) filled (amount: {1} price: {2})",
                                     integratorOrderExternal.Identity, filledAmount, usedPrice, venueClientOrderInfo.ClientOrderIdentity);

                    bool fullyFilled = venueClientOrderInfo.SubtractAmountAndGetIsFullyDone(filledAmount, true);

                    venueClientOrderInfo.PendingLLDealsCount--;

                    if (fullyFilled)
                    {
                        this._logger.Log(LogLevel.Info,
                                         "VenueOrderManager: Internal order {0} fully filled by external order {1}. Removing it from lists",
                                         venueClientOrderInfo.ClientOrderIdentity, integratorOrderExternal.Identity);

                        RemoveMappings(integratorOrderExternal.Identity, venueClientOrderInfo);
                    }

                    string counterpartyTransactionId = orderChangeEventArgs.ExecutionInfo.CounterpartyTransactionId;
                    string internalTransactionId = orderChangeEventArgs.ExecutionInfo.IntegratorTransactionId;

                    venueClientOrderInfo.DispatchGateway.OnIntegratorDealInternal(
                        this.CreateIntegratorDealInternal(venueClientOrderInfo,
                                                   integratorOrderExternal.Identity,
                                                   counterpartyTransactionId,
                                                   internalTransactionId,
                                                   filledAmount, fullyFilled,
                                                   usedPrice,
                                                   integratorOrderExternal.OrderRequestInfo.Side,
                                                   integratorOrderExternal.OrderRequestInfo.Symbol,
                                                   integratorOrderExternal.IntegratorSentTimeUtc,
                                                   integratorOrderExternal.IntegratorReceivedResultTimeUtc,
                                                   integratorOrderExternal.CounterpartySentResultTimeUtc,
                                                   integratorOrderExternal.CounterpartyTransactionTimeUtc,
                                                   orderChangeEventArgs.ExecutionInfo.SettlementDate.Value,
                                                   orderChangeEventArgs.ExecutionInfo.SettlementTimeCounterpartyUtc,
                                                   orderChangeEventArgs.ExecutionInfo.SettlementTimeCentralBankUtc,
                                                   integratorOrderExternal.Counterparty, false,
                                                   orderChangeEventArgs.ExecutionInfo.FlowSide ??
                                                   integratorOrderExternal.OrderRequestInfo.FlowSideAtSubmitTime,
                                                   integratorOrderExternal.Counterparty.TradingTargetType, null,
                                                   orderChangeEventArgs.ExecutionInfo.CounterpartyClientId));

                    if (fullyFilled)
                    {
                        venueClientOrderInfo.DispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateNotActiveClientOrderUpdateInfo(
                                venueClientOrderInfo,
                                0m, null, this.Counterparty), venueClientOrderInfo.Symbol);
                    }

                    orderChangeEventArgs.ExecutionInfo.AddClientSystemAllocation(venueClientOrderInfo.IntegratedTradingSystemIdentification, filledAmount);

                    break;
                case IntegratorOrderExternalChange.Rejected:

                    decimal rejectedAmount = orderChangeEventArgs.RejectionInfo.RejectedAmount.Value;

                    bool isFullyDone = venueClientOrderInfo.SubtractAmountAndGetIsFullyDone(rejectedAmount, false);

                    if (orderChangeEventArgs.RejectionInfo.IsLLContractReject)
                    {
                        venueClientOrderInfo.PendingLLDealsCount--;
                    }

                    if (isFullyDone)
                    {
                        RemoveMappings(integratorOrderExternal.Identity, venueClientOrderInfo);
                    }
                    else
                    {
                        if (!integratorOrderExternal.HasRemainingAmount && !integratorOrderExternal.HasPendingLLDeals)
                        {
                            this._logger.Log(LogLevel.Error,
                                             "External order {0} reject expected to remove all remaining outstanding amount from internal order {1}, but it didn't happen",
                                             integratorOrderExternal.Identity, venueClientOrderInfo.ClientOrderIdentity);
                        }
                    }

                    if (orderChangeEventArgs.RejectionInfo.RejectionType != RejectionType.InternalRejectTriggeredByReplace)
                    {
                        venueClientOrderInfo.DispatchGateway.OnCounterpartyRejectedOrder(
                            new CounterpartyRejectionInfo(venueClientOrderInfo.ClientOrderIdentity,
                                                          venueClientOrderInfo.ClientIdentity,
                                                          null,
                                                          integratorOrderExternal.UniqueInternalIdentity,
                                                          integratorOrderExternal.Identity,
                                                          rejectedAmount,
                                                          integratorOrderExternal.OrderRequestInfo.RequestedPrice,
                                                          integratorOrderExternal.OrderRequestInfo.Side,
                                                          integratorOrderExternal.OrderRequestInfo.Symbol,
                                                          integratorOrderExternal.IntegratorSentTimeUtc,
                                                          integratorOrderExternal.IntegratorReceivedResultTimeUtc,
                                                          integratorOrderExternal.CounterpartySentResultTimeUtc,
                                                          integratorOrderExternal.Counterparty,
                                                          orderChangeEventArgs.RejectionInfo.RejectionReason, false,
                                                          orderChangeEventArgs.RejectionInfo.RejectionType,
                                                          integratorOrderExternal.Counterparty.TradingTargetType,
                                                          venueClientOrderInfo.IntegratedTradingSystemIdentification,
                                                          orderChangeEventArgs.RejectionInfo.CounterpartyClientId,
                                                          orderChangeEventArgs.ExecutionInfo != null ? orderChangeEventArgs.ExecutionInfo.CounterpartyTransactionId : null));
                    }

                    if (isFullyDone)
                    {
                        venueClientOrderInfo.DispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateNotActiveClientOrderUpdateInfo(
                                venueClientOrderInfo,
                                rejectedAmount, orderChangeEventArgs.RejectionInfo.RejectionReason, this.Counterparty),
                            venueClientOrderInfo.Symbol);
                    }
                    else
                    {
                        venueClientOrderInfo.DispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateVenueClientOrderUpdateInfo(
                                venueClientOrderInfo,
                                venueClientOrderInfo.OrderStatus,
                                ClientOrderCancelRequestStatus.CancelNotRequested,
                                0m, rejectedAmount, venueClientOrderInfo.SizeBaseAbsActive, orderChangeEventArgs.RejectionInfo.RejectionReason,
                                this.Counterparty), venueClientOrderInfo.Symbol);
                    }

                    orderChangeEventArgs.RejectionInfo.AddClientSystemAllocation(venueClientOrderInfo.IntegratedTradingSystemIdentification, rejectedAmount);
                    
                    break;

                case IntegratorOrderExternalChange.Cancelled:

                    decimal cancelledAmount = orderChangeEventArgs.RejectionInfo.RejectedAmount.Value;

                    venueClientOrderInfo.SetOrderCancellationDone(false);

                    bool isFullyCancelled =
                        venueClientOrderInfo.SubtractAmountAndGetIsFullyDone(cancelledAmount, false) &&
                        !integratorOrderExternal.HasPendingLLDeals;

                    if (isFullyCancelled)
                    {
                        //only if fully cancelled
                        RemoveMappings(integratorOrderExternal.Identity, venueClientOrderInfo);

                        venueClientOrderInfo.DispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateFullyCancelledClientOrderUpdateInfo(
                            venueClientOrderInfo, ClientOrderStatus.RemovedFromIntegrator, cancelledAmount,
                            this.Counterparty.TradingTargetType), venueClientOrderInfo.Symbol);
                    }
                    else
                    {
                        if (!integratorOrderExternal.HasPendingLLDeals)
                        {
                            this._logger.Log(LogLevel.Error,
                                         "External order {0} expected to remove all remaining outstanding amount from internal order {1}, but it didn't happen. There is {2} remaining active amount and {3} pending LL fills",
                                         integratorOrderExternal.Identity, venueClientOrderInfo.ClientOrderIdentity, venueClientOrderInfo.SizeBaseAbsActive, venueClientOrderInfo.PendingLLDealsCount);

                        }

                        venueClientOrderInfo.DispatchGateway.OnIntegratorClientOrderUpdate(
                            VenueClientOrderUpdateInfo.CreateVenueClientOrderUpdateInfo(
                                venueClientOrderInfo,
                                venueClientOrderInfo.OrderStatus,
                                ClientOrderCancelRequestStatus.CancelRequestAccepted,
                                cancelledAmount, 0, venueClientOrderInfo.SizeBaseAbsActive, orderChangeEventArgs.RejectionInfo.RejectionReason,
                                this.Counterparty), venueClientOrderInfo.Symbol);
                    }

                    

                    break;

                case IntegratorOrderExternalChange.InBrokenState:

                    this._logger.Log(LogLevel.Fatal, "VenueOrderManager: External order [{0}] (mapped to internal order {1}) is in Broken state",
                                     integratorOrderExternal.Identity, venueClientOrderInfo.ClientOrderIdentity);

                    venueClientOrderInfo.SetOrderCancellationDone(true);

                    RemoveMappings(integratorOrderExternal.Identity, venueClientOrderInfo);

                    venueClientOrderInfo.DispatchGateway.OnCounterpartyIgnoredOrder(
                        new CounterpartyOrderIgnoringInfo(venueClientOrderInfo.ClientOrderIdentity,
                                                          venueClientOrderInfo.ClientIdentity,
                                                          null,
                                                          integratorOrderExternal.UniqueInternalIdentity,
                                                          integratorOrderExternal.OrderRequestInfo.SizeBaseAbsInitial,
                                                          integratorOrderExternal.OrderRequestInfo.RequestedPrice,
                                                          integratorOrderExternal.OrderRequestInfo.Side,
                                                          integratorOrderExternal.OrderRequestInfo.Symbol,
                                                          integratorOrderExternal.IntegratorSentTimeUtc,
                                                          integratorOrderExternal.Counterparty,
                                                          integratorOrderExternal.Counterparty.TradingTargetType,
                                                          venueClientOrderInfo.IntegratedTradingSystemIdentification,
                                                          TryExtractGlidingInfoBag(venueClientOrderInfo)));

                    venueClientOrderInfo.DispatchGateway.OnIntegratorClientOrderUpdate(
                        VenueClientOrderUpdateInfo.CreateVenueClientOrderUpdateInfo(
                            venueClientOrderInfo,
                            ClientOrderStatus.BrokenInIntegrator,
                            ClientOrderCancelRequestStatus.CancelNotRequested, 0m, 0m, 0m, string.Empty,
                            this.Counterparty),
                        venueClientOrderInfo.Symbol);

                    break;
                case IntegratorOrderExternalChange.NotSubmitted:
                default:
                    this._logger.Log(LogLevel.Fatal,
                                     "VenueOrderManager: Unexpected IntegratorOrderExternalChange: {0} for external order: {1} (mapped to internal order {2})",
                                     orderChangeEventArgs.ChangeState, integratorOrderExternal, venueClientOrderInfo.ClientOrderIdentity);
                    break;
            }
        }

        private GlidingInfoBag TryExtractGlidingInfoBag(VenueClientOrder venueClientOrder)
        {
            GliderLmaxClientOrder gliderOrder = venueClientOrder as GliderLmaxClientOrder;
            if (gliderOrder != null)
                return gliderOrder.GlidingInfoBag;
            else
                return null;
        }

        private IntegratorDealInternal CreateIntegratorDealInternal(VenueClientOrder venueClientOrder,
                                      string integratorOrderIdentity, string counterpartyTransactionIdentity,
                                      string internalTransactionIdentity, decimal filledAmountBaseAbs, bool isTerminalFill,
                                      decimal usedPrice, DealDirection direction, Symbol symbol,
                                      DateTime integratorSentTimeUtc, DateTime integratorExecutionTimeUtc,
                                      DateTime counterpartyExecutionTimeUtc, DateTime? counterpartyTransactionTimeUtc,
                                      DateTime settlementDateLocal, DateTime settlementTimeCounterpartyUtc,
                                      DateTime settlementTimeCentralBankUtc,
                                      Counterparty counterparty, bool? isPrimaryDealFromQuoted, FlowSide? flowSide,
                                      TradingTargetType senderTradingTargetType,
                                      MarketOnImprovementResult? marketOnImprovementResult,
                                      string counterpartyClientId)
        {
            return IntegratorGliderDealInternal.CreateIntegratorGliderDealInternal(venueClientOrder.ClientOrderIdentity, null,
                    venueClientOrder.ClientIdentity,
                    integratorOrderIdentity, counterpartyTransactionIdentity,
                    internalTransactionIdentity, filledAmountBaseAbs, isTerminalFill,
                    usedPrice, direction, symbol,
                    integratorSentTimeUtc, integratorExecutionTimeUtc,
                    counterpartyExecutionTimeUtc, counterpartyTransactionTimeUtc,
                    settlementDateLocal, settlementTimeCounterpartyUtc, settlementTimeCentralBankUtc,
                    counterparty, isPrimaryDealFromQuoted, flowSide,
                    senderTradingTargetType,
                    marketOnImprovementResult,
                    venueClientOrder.IntegratedTradingSystemIdentification,
                    counterpartyClientId, TryExtractGlidingInfoBag(venueClientOrder));
        }

        private List<string> _oldOrdersWaitingForCancellation = new List<string>();
        private List<string> _newOrdersWaitingForCancellation = new List<string>();
        private SafeTimer _oldOrdersClearingTimer;

        private void ClearOldCancellationWaiters()
        {
            //no need to lock as we operate on collection that is not used for a minute

            VenueClientOrder venueClientOrderInfo;
            foreach (string externalOrderId in _oldOrdersWaitingForCancellation)
            {
                if (_externalToClientOrderMapping.TryRemove(externalOrderId, out venueClientOrderInfo))
                {
                    this._logger.Log(LogLevel.Fatal, "Client order [{0}] mapped to exernal order [{1}] was closed and waiting for cancellation result, but timed out - removing from VenueOrderManager (close operation succeeded: {2})",
                        venueClientOrderInfo.ClientOrderIdentity, externalOrderId, venueClientOrderInfo.TryCloseIfNoPendingCancellation());

                    string unusedStr;
                    if (!_clientToExternalOrderMapping.TryRemove(venueClientOrderInfo.ClientOrderIdentity, out unusedStr))
                    {
                        this._logger.Log(LogLevel.Error, "Attempting to remove client order {0} from VenueOrderManager mappings, but it's missing", venueClientOrderInfo.ClientOrderIdentity);
                    }
                }
            }

            _oldOrdersWaitingForCancellation.Clear();

            //after this point we have in old list what ws in new list
            _oldOrdersWaitingForCancellation = Interlocked.Exchange(ref this._newOrdersWaitingForCancellation, _oldOrdersWaitingForCancellation);
        }

        private void RemoveMappings(string externalOrderId, VenueClientOrder venueClientOrder)
        {
            if (venueClientOrder.PendingLLDealsCount > 0)
            {
                this._logger.Log(LogLevel.Info,
                                    "Client order [{0}] mapped to external order [{1}] has {2} pending LL deals so not removing it from OrderManager mappings yet",
                                 venueClientOrder.ClientOrderIdentity, externalOrderId, venueClientOrder.PendingLLDealsCount);
                return;
            }
            //this will also automatically set to not active
            else if (!venueClientOrder.TryCloseIfNoPendingCancellation())
            {
                this._logger.Log(LogLevel.Info,
                                 "Client order [{0}] mapped to external order [{1}] has pending cancellation(s) so not removing it from OrderManager mappings yet",
                                 venueClientOrder.ClientOrderIdentity, externalOrderId);
                lock(_newOrdersWaitingForCancellation)
                    _newOrdersWaitingForCancellation.Add(externalOrderId);
                return;
            }

            this._logger.Log(LogLevel.Info,
                                 "VenueOrderManager is removing client order [{0}] mapped to exernal order [{1}] from its mappings",
                                 venueClientOrder.ClientOrderIdentity, externalOrderId);

            VenueClientOrder unused;
            string unusedStr;
            if (!_externalToClientOrderMapping.TryRemove(externalOrderId, out unused))
            {
                this._logger.Log(LogLevel.Info, "Attempting to remove external order {0} from VenueOrderManager mappings, but it's missing", externalOrderId);
            }

            if (!_clientToExternalOrderMapping.TryRemove(venueClientOrder.ClientOrderIdentity, out unusedStr))
            {
                this._logger.Log(LogLevel.Info, "Attempting to remove client order {0} from VenueOrderManager mappings, but it's missing", venueClientOrder.ClientOrderIdentity);
            }
        }
    }
}
