﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.BusinessLayer
{
    // Theoretical complexity of this structure is O(n) vs pure heap which is O(log(n))
    //   this is caused by lookups (which are O(n)) - but with very small size of this structure   
    //   it's actually very beneficial to just update items in place while sacrificing theoretical performance for big sizes
    //
    // This can be likely implemented more per formant e.g.:
    //   -lookup array of actual values (or ideally just decimals or values to compare, not pointers) indexable by counterparty
    //   -sorted array with values
    //   Update then know old value to be update in O(1) from lookup, finds it in array in O(log(n)) by binary search
    //    updates it in O(n/2) moves and then updates lookup O(1)
    //   So it looks like win win (O(log(n)) instead of O(n) - however nonsequential access to small array is costly due cache misses)

    //constraint to class would be needed for volatile. however SpinLock.Exit()  generates barrier via usage of Interlocked - so it's not needed
    public sealed class PriceBookInternal : IChangeablePriceBook<PriceObjectInternal, Counterparty>
    {

        private PriceObjectInternal[] _sortedArray;
        //SpinLock.Exit() (and SpinLock.Exit(true)) generates memory barrier - so volatile is not needed
        private /*volatile*/ PriceObjectInternal _currentTop;
        private int _maxSize;
        private int _currentSize;
        private IComparer<PriceObjectInternal> _rankComparer;
        private Func<PriceObjectInternal, PriceObjectInternal, int> _rankCompareFunc;
        private Func<decimal, decimal, int> _priceRankCompareFunc;
        private Func<PriceObjectInternal, PriceObjectInternal, int> _topCompareFunc;
        private Func<PriceObjectInternal, Counterparty, bool> _identityEqualsFunc;
        private Func<PriceObjectInternal, PriceObjectInternal, bool> _itemsEqualsFunc;
        private Func<PriceObjectInternal, PriceObjectInternal, bool> _itemsIdenticalFunc;
        private Func<PriceObjectInternal, Guid, bool> _areIdenticalFunc;
        private SpinLock _spinLock = new SpinLock(false);
        private ILogger _logger;
        private PriceObjectInternal _nullNode;
        private volatile bool _isImprovementTick = false;

        public event BookPriceUpdate<PriceObjectInternal> BookTopImproved;
        public event BookPriceUpdate<PriceObjectInternal> BookTopDeterioratedInternal;
        public event BookPriceUpdate<PriceObjectInternal> BookTopDeteriorated;
        public event Action<DateTime> BookTopRemoved;
        public event BookPriceUpdate<PriceObjectInternal> BookTopReplaced;
        public event BookPriceUpdate<PriceObjectInternal> NewNodeArrived;
        public event BookPriceUpdate<PriceObjectInternal> ExistingNodeInvalidated;
        public event BookMultiplePricesUpdate<PriceObjectInternal, Counterparty> NodesRemoved;

        public event Action<PriceObjectInternal> ChangeableBookTopImproved;
        public event Action<PriceObjectInternal> ChangeableBookTopDeteriorated;
        public event Action<PriceObjectInternal> ChangeableBookTopReplaced;

        public PriceBookInternal(int maxHeapSize, IComparer<PriceObjectInternal> rankComparer, IComparer<decimal> priceRankComparer, IComparer<PriceObjectInternal> topComparer,
                          IIdentityEqualityComparer<PriceObjectInternal, Counterparty> equalityComparer, PriceObjectInternal nullNode, ILogger logger)
        {
            this._maxSize = maxHeapSize;
            this._currentSize = 0;
            this._sortedArray = new PriceObjectInternal[_maxSize];
            this._rankComparer = rankComparer;
            this._rankCompareFunc = rankComparer.Compare;
            this._priceRankCompareFunc = priceRankComparer.Compare;
            this._topCompareFunc = topComparer.Compare;
            this._identityEqualsFunc = equalityComparer.IdentityEquals;
            this._itemsEqualsFunc = equalityComparer.ItemsEquals;
            this._itemsIdenticalFunc = equalityComparer.ItemsAreIdentical;
            this._areIdenticalFunc = equalityComparer.AreIdentical;
            this._logger = logger;
            this._nullNode = nullNode;
            this._currentTop = nullNode;
        }

        public bool IsEmpty
        {
            get { return _currentSize == 0; }
        }

        public bool IsImprovement { get { return this._isImprovementTick; } }

        /// <summary>
        /// Reads the value acquiring full CPU fence
        ///  The returned item is Acquired and needs to be released by the caller!
        /// </summary>
        public PriceObjectInternal MaxNodeInternal
        {
            get
            {
                // Unfortunately there is no other way than using same constrained region as code updating the top
                //  Otherwise the top might be released, than acquired by someone else and we might not notice (ABA problem)
                bool lockTaken = false;
                _spinLock.Enter(ref lockTaken);

                PriceObjectInternal value = _currentTop;
                value.Acquire();

                if (lockTaken)
                {
                    _spinLock.Exit();
                }

                return value;
            }
        }

        public AtomicDecimal GetEstimatedBestPriceFast()
        {
            return (_currentSize == 0 ? this._nullNode : _sortedArray[0]).ConvertedPrice;
        }

        public decimal GetWeightedPriceForSize(ref decimal maxSizeOnPrice, int maximumBookDepthToBeUsed)
        {
            decimal remainingSize, weightedPriceInternal;
            weightedPriceInternal = this.GetWeightedPriceForSize_Internal(maxSizeOnPrice, maximumBookDepthToBeUsed,
                out remainingSize);

            maxSizeOnPrice -= remainingSize;

            if (maxSizeOnPrice > 0)
            {
                return weightedPriceInternal / maxSizeOnPrice;
            }
            else
            {
                return 0;
            }
        }

        public decimal GetMaximumSizeForExactPrice(decimal worstAcceptablePrice, int maximumBookDepthToBeUsed, decimal maximumSizeToStopProbing)
        {
            return this.GetMaximumSizeForExactPrice(worstAcceptablePrice, maximumBookDepthToBeUsed,
                maximumSizeToStopProbing, null);
        }

        public decimal GetMaximumSizeForExactPrice(decimal worstAcceptablePrice, int maximumBookDepthToBeUsed,
                decimal maximumSizeToStopProbing, Counterparty[] counterpartiesWhitelist)
        {
            decimal currentMaxSize = 0m;
            decimal weightedPriceInternal = 0m;

            //we need to have exclusive access as _currentSize can change or PriceObjects can be reclaimed in the meantime
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            for (int idx = 0; idx < Math.Min(_currentSize, maximumBookDepthToBeUsed); idx++)
            {
                if (PriceObjectInternal.IsNullPrice(_sortedArray[idx]) ||
                    (counterpartiesWhitelist != null && !counterpartiesWhitelist.Contains(_sortedArray[idx].Counterparty)))
                    continue;

                decimal currentLayerSizeUsed;
                decimal currentLayerPrice = _sortedArray[idx].Price;
                //we can use current layer in whole
                if (_priceRankCompareFunc(currentLayerPrice, worstAcceptablePrice) >= 0)
                {
                    currentLayerSizeUsed = _sortedArray[idx].SizeBaseAbsRemaining;
                }
                //need to calc how much we can use
                else
                {
                    decimal maxSizeToMeetWorstAcceptablePrice = (currentMaxSize * worstAcceptablePrice -
                                                                 weightedPriceInternal) /
                                                                (_sortedArray[idx].Price - worstAcceptablePrice);

                    currentLayerSizeUsed = Math.Min(maxSizeToMeetWorstAcceptablePrice, _sortedArray[idx].SizeBaseAbsRemaining);

                    if (maxSizeToMeetWorstAcceptablePrice <= 0)
                        break;
                }

                currentMaxSize += currentLayerSizeUsed;

                if (currentMaxSize >= maximumSizeToStopProbing)
                    break;

                weightedPriceInternal += currentLayerSizeUsed * _sortedArray[idx].Price;
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            return Math.Min(currentMaxSize, maximumSizeToStopProbing);
        }

        public decimal GetWeightedPriceForExactSize(decimal sizeOnPrice, int maximumBookDepthToBeUsed)
        {
            decimal remainingSize, weightedPriceInternal;
            weightedPriceInternal = this.GetWeightedPriceForSize_Internal(sizeOnPrice, maximumBookDepthToBeUsed,
                out remainingSize);
            if (remainingSize <= 0 && sizeOnPrice > 0)
            {
                return weightedPriceInternal / sizeOnPrice;
            }
            else
            {
                return 0;
            }
        }

        private decimal GetWeightedPriceForSize_Internal(decimal maxSizeOnPrice, int maximumBookDepthToBeUsed, out decimal remainingSize)
        {

            //TODO: caching?!
            // probably doesn't have a sense - since it would need to capture 
            //   version of PriceBook, maxSizeOnPrice and maximumBookDepthToBeUsed


            remainingSize = maxSizeOnPrice;
            decimal weightedPriceInternal = 0m;

            //we need to have exclusive access as _currentSize can change or PriceObjects can be reclaimed in the meantime
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            for (int idx = 0; idx < Math.Min(_currentSize, maximumBookDepthToBeUsed); idx++)
            {
                if(PriceObjectInternal.IsNullPrice(_sortedArray[idx]))
                    continue;
                decimal currentSize = Math.Min(remainingSize, _sortedArray[idx].SizeBaseAbsRemaining);
                weightedPriceInternal += currentSize * _sortedArray[idx].Price;
                remainingSize -= currentSize;
                if (remainingSize <= 0)
                    break;
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            return weightedPriceInternal;
        }

        /// <summary>
        /// NOTE: this is dangerous call as no constraining or acquiring is being made
        ///   it is only safe to use in throw-away stats (e.g. C-monitor) where tinny chance
        ///   of missed calculation is OK
        /// </summary>
        public PriceObjectInternal MaxNode
        {
            get
            {
                var maxNode = _currentTop;
                if (Equals(maxNode, _nullNode)) return default(PriceObjectInternal);
                else return maxNode;
            }
        }

        public DateTime MaxNodeUpdatedUtc { get; private set; }

        public TradingTargetType TradingTargetType
        {
            get { return TradingTargetType.BankPool; }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        //Find item (existing items will have idx < _currentSize)
        private int GetIndexOfIdenticalItemUnsynchronized(PriceObjectInternal node)
        {
            int idx = 0;
            for (; idx < _currentSize; idx++)
            {
                if (_itemsIdenticalFunc(_sortedArray[idx], node))
                {
                    break;
                }
            }

            return idx;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        //Find item (existing items will have idx < _currentSize)
        private int GetIndexOfEqualItemUnsynchronized(PriceObjectInternal node)
        {
            int idx = 0;
            for (; idx < _currentSize; idx++)
            {
                if (_itemsEqualsFunc(_sortedArray[idx], node))
                {
                    break;
                }
            }

            return idx;
        }

        public PriceObjectInternal GetIdentical(PriceObjectInternal node)
        {
            PriceObjectInternal nodeToReturn;

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            int idx = this.GetIndexOfIdenticalItemUnsynchronized(node);

            nodeToReturn = idx < _currentSize ? _sortedArray[idx] : default(PriceObjectInternal);

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            return nodeToReturn;
        }

        public PriceObjectInternal[] GetSortedClone(Func<PriceObjectInternal, bool> selectorFunc)
        {
            int currentSizeLocal = _currentSize;
            PriceObjectInternal[] heapArrayCopy = new PriceObjectInternal[currentSizeLocal];

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            int currentSizeAtCopyTime = Math.Min(currentSizeLocal, _currentSize);
            int unwantedItems = 0;

            for (int i = 0; i < currentSizeAtCopyTime; i++)
            {
                if (selectorFunc == null || selectorFunc(_sortedArray[i]))
                {
                    _sortedArray[i].Acquire();
                    heapArrayCopy[i - unwantedItems] = _sortedArray[i];
                }
                else
                {
                    unwantedItems++;
                }
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            //if heap shrunk after we allocated it, shrink it now before sorting - as it can contain garbage
            if ((currentSizeAtCopyTime - unwantedItems) != currentSizeLocal)
                Array.Resize(ref heapArrayCopy, (currentSizeAtCopyTime - unwantedItems));

            return heapArrayCopy;
        }

        public PriceObjectInternal[] GetSortedClone()
        {
            return this.GetSortedClone(null);
        }

        public int AddOrUpdate_ForTestsOnly(PriceObjectInternal value)
        {
            bool heldWithoutAcquireCall = true;
            return this.AddOrUpdate(value, ref heldWithoutAcquireCall);
        }

        public int AddOrUpdate(PriceObjectInternal value, ref bool heldWithoutAcquireCall)
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            int idx = this.GetIndexOfEqualItemUnsynchronized(value);

            int newIdx;
            bool topImproved = false;
            bool topDeclined = false;
            bool topReplaced = false;
            PriceObjectInternal removedItem = null;

            //This is update of existing item
            if (idx < _currentSize)
            {
                newIdx = UpdateAt(idx, value, out removedItem);
            }
            //This is  insert of new item
            else
            {
                newIdx = Insert(value);
            }

            if (idx == 0 || newIdx == 0)
            {
                //the _currentTop can be empty (default(PriceObjectInternal))
                int topRankCompare = Equals(_currentTop, _nullNode) ? 1 : _topCompareFunc(_sortedArray[0], _currentTop);

                if (topRankCompare > 0)
                {
                    topImproved = true;
                }
                else if (topRankCompare < 0)
                {
                    topDeclined = true;
                }
                else if (!_itemsIdenticalFunc(_sortedArray[0], _currentTop))
                {
                    topReplaced = true;
                }
            }

            DateTime updateTime;
            PriceObjectInternal updatedTop;

            //If something changed with the top
            if (topImproved || topDeclined || topReplaced)
            {
                _currentTop = _sortedArray[0];
                updatedTop = _currentTop;
                updatedTop.Acquire();
                updateTime = HighResolutionDateTime.UtcNow;
                this.MaxNodeUpdatedUtc = updateTime;
            }
            //this is to make compiler happy
            else
            {
                updateTime = DateTime.MinValue;
                updatedTop = default(PriceObjectInternal);
            }

            //needs to be in constrained region so that it's not released before acquiring
            if (newIdx != -1)
            {
                if (!heldWithoutAcquireCall)
                    heldWithoutAcquireCall = true;
                else
                    value.Acquire();
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            if (topImproved)
            {
                //setting this prior new node action
                this._isImprovementTick = true;
            }

            this.TryInvokeNodeAction(NewNodeArrived, value);

            //after this point _currentTopCan be changed - 'non-changeable' events get correct value from queue
            // changeable events will get actual value and so it won't get values out of order
            if (topImproved)
            {
                TryInvokeTopImproved(updatedTop, updateTime);
            }
            else if (topDeclined)
            {
                TryInvokeTopDeteriorated(updatedTop, updateTime);
            }
            else if (topReplaced)
            {
                TryInvokeTopReplaced(updatedTop, updateTime);
            }

            if (removedItem != null && removedItem != this._nullNode)
                removedItem.Release();

            return newIdx;
        }


        public void ResortIdentical(PriceObjectInternal value)
        {
            int newIndex = -1;

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            //Find item to resort
            int idx = this.GetIndexOfIdenticalItemUnsynchronized(value);

            if (idx < _currentSize)
            {
                PriceObjectInternal dummy;
                newIndex = UpdateAt(idx, _sortedArray[idx], out dummy);
            }

            //did we 'downsort' the top?
            bool topDeclined = false;
            bool topReplaced = false;
            DateTime updateTime;
            PriceObjectInternal updatedTop;
            if (idx == 0 && newIndex != idx)
            {
                if ((_currentSize == 0 || _topCompareFunc(_currentTop, _sortedArray[0]) != 0))
                {
                    topDeclined = true;
                }
                else
                {
                    topReplaced = true;
                }

                _currentTop = _sortedArray[0];
                updatedTop = _currentTop;
                updatedTop.Acquire();
                updateTime = HighResolutionDateTime.UtcNow;
                this.MaxNodeUpdatedUtc = updateTime;

            }
            //this is to make compiler happy
            else
            {
                updateTime = DateTime.MinValue;
                updatedTop = default(PriceObjectInternal);
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            if (topDeclined)
            {
                TryInvokeTopDeteriorated(updatedTop, updateTime);
            }
            else if (topReplaced)
            {
                TryInvokeTopReplaced(updatedTop, updateTime);
            }
        }

        public bool RemoveIdentical(PriceObjectInternal value)
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            //Find item to remove
            int idx = this.GetIndexOfIdenticalItemUnsynchronized(value);

            bool removed = RemoveAtWithEvents(idx, lockTaken);

            if (removed && this.ExistingNodeInvalidated != null)
            {
                this.TryInvokeNodeAction(this.ExistingNodeInvalidated, value);
            }

            return removed;
        }

        public bool RemoveIdentical(Guid identificator)
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            //Find item to remove
            int idx = 0;
            for (; idx < _currentSize; idx++)
            {
                if (_areIdenticalFunc(_sortedArray[idx], identificator))
                {
                    break;
                }
            }

            //if ExistingNodeInvalidated would be about to be called then current node needs to be acquired first

            bool removed = RemoveAtWithEvents(idx, lockTaken);

            return removed;
        }

        public bool Remove(Counterparty identity)
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            int idx = 0;
            for (; idx < _currentSize; idx++)
            {
                if (_identityEqualsFunc(_sortedArray[idx], identity))
                {
                    break;
                }
            }

            bool removed = RemoveAtWithEvents(idx, lockTaken);

            if (removed && this.NodesRemoved != null)
            {
                this.NodesRemoved(this, identity, HighResolutionDateTime.UtcNow);
            }

            return removed;
        }

        private bool RemoveAtWithEvents(int index, bool lockTaken)
        {
            bool removed = false;
            bool topDeclined = false;
            bool topReplaced = false;
            PriceObjectInternal removedItem;

            removed = RemoveAt(index, out removedItem);

            DateTime updateTime;
            PriceObjectInternal updatedTop;
            //We were removing the top and the new top (if present) is different
            if (removed && index == 0)
            {
                if ((_currentSize == 0 || _topCompareFunc(_currentTop, _sortedArray[0]) != 0))
                {
                    topDeclined = true;
                }
                else
                {
                    topReplaced = true;
                }

                _currentTop = _currentSize == 0 ? this._nullNode : _sortedArray[0];
                updatedTop = _currentTop;
                updatedTop.Acquire();
                updateTime = HighResolutionDateTime.UtcNow;
                this.MaxNodeUpdatedUtc = updateTime;
            }
            //this is to make compiler happy
            else
            {
                updateTime = DateTime.MinValue;
                updatedTop = default(PriceObjectInternal);
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            if (topDeclined)
            {
                TryInvokeTopDeteriorated(updatedTop, updateTime);
            }
            else if (topReplaced)
            {
                TryInvokeTopReplaced(updatedTop, updateTime);
            }

            if (removed && removedItem != _nullNode)
                removedItem.Release();

            return removed;
        }

        private bool RemoveAt(int index, out PriceObjectInternal removedItem)
        {
            bool success = false;

            if (index >= 0 && index < _currentSize)
            {
                removedItem = _sortedArray[index];
                _currentSize--;
                while (index < _currentSize)
                {
                    _sortedArray[index] = _sortedArray[++index];
                }

                success = true;
            }
            else
            {
                removedItem = null;
            }

            return success;
        }

        private int Insert(PriceObjectInternal value)
        {
            int currIdx = -1;

            if (_currentSize < _maxSize)
            {
                currIdx = _currentSize;
                for (; currIdx > 0; currIdx--)
                {
                    if(_rankCompareFunc(value, _sortedArray[currIdx - 1]) < 0)
                        break;
                    _sortedArray[currIdx] = _sortedArray[currIdx - 1];
                }
                _sortedArray[currIdx] = value;
                _currentSize++;
            }

            return currIdx;
        }

        private int UpdateAt(int index, PriceObjectInternal newValue, out PriceObjectInternal removedItem)
        {
            int resultIndex = -1;

            if (index >= 0 && index < _currentSize)
            {
                removedItem = _sortedArray[index];

                if (_rankCompareFunc(removedItem, newValue) < 0)
                {
                    for (; index > 0; index--)
                    {
                        if (_rankCompareFunc(newValue, _sortedArray[index - 1]) <= 0)
                            break;
                        _sortedArray[index] = _sortedArray[index - 1];
                    }
                }
                else
                {
                    for (; index < _currentSize-1; index++)
                    {
                        if (_rankCompareFunc(newValue, _sortedArray[index + 1]) >= 0)
                            break;
                        _sortedArray[index] = _sortedArray[index + 1];
                    }
                }

                _sortedArray[index] = newValue;
                resultIndex = index;
            }
            else
            {
                removedItem = null;
            }

            return resultIndex;
        }

        

        private void TryInvokeNodeAction(BookPriceUpdate<PriceObjectInternal> nodeAction, PriceObjectInternal nodeValue)
        {
            if (nodeAction != null)
            {
                try
                {
                    nodeAction(this, nodeValue, HighResolutionDateTime.UtcNow);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e,
                                              "Experienced unhandled exception during invocation of {0} with value {1}", nodeAction.Method.Name, nodeValue);
                }
            }
        }

        private void TryInvokeNodeAction(BookPriceUpdate<PriceObjectInternal> nodeAction, PriceObjectInternal nodeValue, DateTime timeStamp)
        {
            if (nodeAction != null)
            {
                try
                {
                    nodeAction(this, nodeValue, timeStamp);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e,
                                              "Experienced unhandled exception during invocation of {0} with value {1}", nodeAction.Method.Name, nodeValue);
                }
            }
        }

        private void TryInvokeInternalNodeAction(Action<PriceObjectInternal> nodeAction, PriceObjectInternal nodeValue)
        {
            if (nodeAction != null)
            {
                try
                {
                    nodeAction(nodeValue);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e,
                                              "Experienced unhandled exception during invocation of {0} (changeable) with value {1}", nodeAction.Method.Name, nodeValue);
                }
            }
        }

        private void TryInvokeTopImproved(PriceObjectInternal currentTopValue, DateTime timeStamp)
        {
            this._isImprovementTick = true;
            TryInvokeInternalNodeAction(ChangeableBookTopImproved, currentTopValue);
            TryInvokeNodeAction(BookTopImproved, currentTopValue, timeStamp);

            currentTopValue.Release();
        }

        private void TryInvokeTopDeteriorated(PriceObjectInternal currentTopValue, DateTime timeStamp)
        {
            this._isImprovementTick = false;
            TryInvokeInternalNodeAction(ChangeableBookTopDeteriorated, currentTopValue);
            TryInvokeNodeAction(BookTopDeterioratedInternal, currentTopValue, timeStamp);

            if (Equals(currentTopValue, _nullNode))
            {
                if (BookTopRemoved != null)
                {
                    BookTopRemoved(timeStamp);
                }
            }
            else
            {
                if (BookTopDeteriorated != null)
                {
                    BookTopDeteriorated(this, currentTopValue, timeStamp);
                }
            }


            currentTopValue.Release();
        }

        private void TryInvokeTopReplaced(PriceObjectInternal currentTopValue, DateTime timeStamp)
        {
            TryInvokeInternalNodeAction(ChangeableBookTopReplaced, currentTopValue);
            TryInvokeNodeAction(BookTopReplaced, currentTopValue, timeStamp);

            currentTopValue.Release();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);
            PriceObjectInternal[] arrayCopy = new PriceObjectInternal[_currentSize];
            Array.Copy(_sortedArray, arrayCopy, _currentSize);
            if (lockTaken)
            {
                _spinLock.Exit();
            }

            sb.AppendLine();
            sb.Append("Elements of the Heap Array are : ");
            for (int m = 0; m < _currentSize; m++)
                if (arrayCopy[m] != null)
                    sb.Append(arrayCopy[m].ToString() + " ");
                else
                    sb.Append("-- ");

            return sb.ToString();
        }
    }
}
