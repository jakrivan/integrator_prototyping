﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinessLayer
{
    public interface IOrderEvaluator
    {
        //Evaluates whether the given price meets criteria of the order in order to be at least partially filled
        //Beware! this means this should not check size criteria - as partial fills are fine
        bool MeetsPriceCriteria(IClientOrder order, PriceObjectInternal price);

        //This will be used to sort orders and find the least demanding one (for faster matching during new quotes)
        IComparer<IClientOrder> OrderRankComparer { get; }

        //This will be used after set of orders was selected (all of which meets criteria given by the MeetsPriceCriteria member)
        // and before the actuall filling.
        //One can expect that this will be used on Orders that already all can be matched
        IComparer<IClientOrder> FillPreferenceComparer { get; }

        //This is just a doublechecking security feature. The owning class can check if it's using proper evaluator
        bool IsBuyOrdersEvaluator { get; }
    }
}
