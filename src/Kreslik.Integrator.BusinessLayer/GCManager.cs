﻿#define THREADPOOLEX_TRACING

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinessLayer
{
    public class GCManager
    {
        private static readonly TimeSpan _minTimestampsGapToGFO = TimeSpan.FromSeconds(2);
        private SafeTimer _gcCollectionsCountCheckingTimer;
        private ILogger _logger;
        private IRiskManager _riskManager;

        private static TimeSpan _minTimestampsGapToWatchFor;// = TimeSpan.FromMilliseconds(20);
        private static TimeSpan _minTimestampsGapToLogError;// = TimeSpan.FromMilliseconds(100);
        private static TimeSpan _minTimestampsGapToLogFatal;// = TimeSpan.FromMilliseconds(200);
        private SingleThreadedEventsRateChecker _timeGapsRateChecker;
        //events come from multiple threads
        private EventsRateChecker _threadPoolDelaysRateChecker;
        private bool _observeOffHoursEvents;
        public GCManager(ILogger logger, IRiskManager riskManager, BusinessLayerSettings.TimeGapsWatchingSettings timeGapsSettings)
        {
            this._logger = logger;
            this._riskManager = riskManager;
            logger.Log(LogLevel.Info, "Creating GCManager. Current GC mode: {0}, Is server GC: {1}",
                       GCSettings.LatencyMode, GCSettings.IsServerGC);
            if (TradingHoursHelper.Instance.AreKgtTradingHours)
            {
                this.SetLowLatencySustainableMode();
            }
            else
            {
                this.SetInteractiveMode();
            }

            this.UpdateSettings(timeGapsSettings);

            TradingHoursHelper.Instance.KgtTradingHoursStarting += this.GcChangeOnTradingStarting;
            TradingHoursHelper.Instance.KgtTradingHoursEnding += this.SetInteractiveModeInternal;

            this._gcCollectionsCountCheckingTimer = new SafeTimer(CheckGcGen2CollectionCount, TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(5))
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1min,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest,
                IsUltraQuickCallback = true
            };
            //GC.RegisterForFullGCNotification(10, 10);
            //ThreadPool.QueueUserWorkItem(o => WaitForFullGCProc());

            TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromMinutes(1), () =>
            {
                _justInstantiated = false;
                if (_timeGapsRateChecker != null)
                    _timeGapsRateChecker.Reset();
                if(_threadPoolDelaysRateChecker != null)
                    _threadPoolDelaysRateChecker.Reset();
            });
        }

        public void UpdateSettings(BusinessLayerSettings.TimeGapsWatchingSettings timeGapsSettings)
        {
            bool change = false;

            if (timeGapsSettings.MinimumTimeGapToWatchFor < HighResolutionDateTime.TimestampsPollingInterval)
            {
                this._logger.Log(LogLevel.Error,
                    "Attempt to change minimum time gap to watch for to {0} - {1} is exclusive minimum",
                    timeGapsSettings.MinimumTimeGapToWatchFor, HighResolutionDateTime.TimestampsPollingInterval);
                timeGapsSettings.MinimumTimeGapToWatchFor =
                    HighResolutionDateTime.TimestampsPollingInterval.Add(TimeSpan.FromMilliseconds(1));
            }

            if (timeGapsSettings.MinimumTimeGapToLogFatal < timeGapsSettings.MinimumTimeGapToWatchFor)
            {
                this._logger.Log(LogLevel.Error,
                    "Attempt to change minimum time gap to log fatal to {0} - which is lower  than gap to watch for",
                    timeGapsSettings.MinimumTimeGapToLogFatal);
            }

            if (timeGapsSettings.MinimumTimeGapToWatchFor != _minTimestampsGapToWatchFor ||
                timeGapsSettings.MinimumTimeGapToLogFatal != _minTimestampsGapToLogFatal)
            {
                change = true;
            }

            _minTimestampsGapToWatchFor = timeGapsSettings.MinimumTimeGapToWatchFor;
            _minTimestampsGapToLogFatal = timeGapsSettings.MinimumTimeGapToLogFatal;

            if (change)
            {
                _minTimestampsGapToLogError =
                    TimeSpan.FromTicks((_minTimestampsGapToWatchFor.Ticks + _minTimestampsGapToLogFatal.Ticks)/2);
                this._logger.Log(LogLevel.Info,
                    "Settings updated. Minimum gap to watch for: [{0}] Minimum gap to log error: [{1}] Minimum gap to log fatal: [{2}]",
                    _minTimestampsGapToWatchFor, _minTimestampsGapToLogError, _minTimestampsGapToLogFatal);

                if (
                    !HighResolutionDateTime.RegisterHandlerForGapsInPolling(_minTimestampsGapToWatchFor,
                        this.OnGapInTimestamps))
                {
                    _logger.Log(LogLevel.Error,
                        "Highresolution timestamps polling not enabled on current system - so timestamps gaps detection will be turned off");
                }

                if (
                    !ThreadPoolEx.Instance.RegisterHandlerForDelaysInQueue(_minTimestampsGapToWatchFor,
                        this.OnDelayInThreadPoolExQueue))
                {
                    _logger.Log(LogLevel.Fatal,
                        "ThreadPoolEx delays watching could not be turned on");
                }
            }

            _observeOffHoursEvents = timeGapsSettings.LogFatalsDuringOffBusinessHours;
            _timeGapsRateChecker = timeGapsSettings.EnableWatchingForGapsClusters
                ? new SingleThreadedEventsRateChecker(timeGapsSettings.ClusterDuration,
                    timeGapsSettings.NumberOfGapsInClusterToLogFatal)
                : null;
            _threadPoolDelaysRateChecker = timeGapsSettings.EnableWatchingForGapsClusters
                ? new EventsRateChecker(timeGapsSettings.ClusterDuration,
                    timeGapsSettings.NumberOfGapsInClusterToLogFatal)
                : null;
        }

        private const uint TICKS_PER_MILLISECOND = 10000;

        private bool _justInstantiated = true;
        public bool CanLogFatalNow
        {
            get
            {
                return (!_justInstantiated) &&
                       (this._observeOffHoursEvents || TradingHoursHelper.Instance.AreKgtTradingHoursInWorkWeek);
            }
        }

        private void OnGapInTimestamps(long lastTimestampTicks, long newTimestampTicks)
        {
            if (lastTimestampTicks == newTimestampTicks)
            {
                this._logger.Log(LogLevel.Fatal,
                    "Precision timestamps module has some internal issue with constructing timestamp (from value {0}), fallbacking to .NET time",
                    lastTimestampTicks);
                this._riskManager.TurnOnGoFlatOnly("Issue with constructing precise timestamps");
                return;
            }

            this._logger.Log(
                newTimestampTicks - lastTimestampTicks >= _minTimestampsGapToLogError.Ticks
                    ? (newTimestampTicks - lastTimestampTicks >= _minTimestampsGapToLogFatal.Ticks && CanLogFatalNow
                        ? LogLevel.Fatal
                        : LogLevel.Error)
                    : LogLevel.Info,
                "Gap in polled timestamps detected (gap: [{0}] = [{1:yyyy-MM-dd HH:mm:ss.fffffff UTC}] - [{2:yyyy-MM-dd HH:mm:ss.fffffff UTC}])",
                TimeSpan.FromTicks(newTimestampTicks - lastTimestampTicks), new DateTime(newTimestampTicks),
                new DateTime(lastTimestampTicks));

            if (newTimestampTicks < lastTimestampTicks)
            {
                this._logger.Log(LogLevel.Fatal, "MeinBerg clock went backwards by [{0}] (from [{1:yyyy-MM-dd HH:mm:ss.fffffff UTC}] to [{2:yyyy-MM-dd HH:mm:ss.fffffff UTC}]). Backward timestamps are ignored by integrator",
                    TimeSpan.FromTicks(lastTimestampTicks - newTimestampTicks), new DateTime(lastTimestampTicks), new DateTime(newTimestampTicks));
            }

            if (_timeGapsRateChecker != null && !_timeGapsRateChecker.AddNextEventAndCheckIsAllowed())
            {
                this._logger.Log(CanLogFatalNow ? LogLevel.Fatal : LogLevel.Error,
                    "Exceeded {0} time gaps during last {1} interval. This might be caused by increased load or by degrading Garbage Collector performance.",
                    _timeGapsRateChecker.MaximumAllowedEventsPerInterval, _timeGapsRateChecker.TimeIntervalToCheck);
                _timeGapsRateChecker.Reset();
            }

            IntegratorPerformanceCounter.Instance.SetLastTimestampsGapInMs(
                (int) ((newTimestampTicks - lastTimestampTicks)/TICKS_PER_MILLISECOND));

            if (newTimestampTicks - lastTimestampTicks >= _minTimestampsGapToGFO.Ticks)
            {
                this._riskManager.TurnOnGoFlatOnly(
                    string.Format("Gap in polled timestamps [{0}] is over threshold [{1}]",
                        TimeSpan.FromTicks(newTimestampTicks - lastTimestampTicks), _minTimestampsGapToGFO));
            }
        }

        private void OnDelayInThreadPoolExQueue(IWorkItem workItem, TimeSpan queuedSpan)
        {
            this._logger.Log(
                queuedSpan >= _minTimestampsGapToLogError
                    ? (queuedSpan >= _minTimestampsGapToLogFatal && CanLogFatalNow
                        ? LogLevel.Fatal
                        : LogLevel.Error)
                    : LogLevel.Info,
#if THREADPOOLEX_TRACING
                "Detected delay in ThreadPoolEx queue (priority: {0}, id: {1}, delay: {2})", workItem.Priority, workItem.Id, queuedSpan);
#else
                "Detected delay in ThreadPoolEx queue (priority: {0}, delay: {1})", workItem.Priority, queuedSpan);
#endif

            if (_threadPoolDelaysRateChecker != null && !_threadPoolDelaysRateChecker.AddNextEventAndCheckIsAllowed())
            {
                this._logger.Log(CanLogFatalNow ? LogLevel.Fatal : LogLevel.Error,
                    "Exceeded {0} ThreadPoolEx delays during last {1} interval. This might be caused by increased load or by degrading ThreadPoolEx performance.",
                    _threadPoolDelaysRateChecker.MaximumAllowedEventsPerInterval, _threadPoolDelaysRateChecker.TimeIntervalToCheck);
                _threadPoolDelaysRateChecker.Reset();
            }

            if (queuedSpan >= _minTimestampsGapToGFO)
            {
                this._riskManager.TurnOnGoFlatOnly(
                    string.Format("Delay in ThreadPoolEx [{0}] is over threshold [{1}]",
                        queuedSpan, _minTimestampsGapToGFO));
            }
        }

        //TODO: gc collections count
        //notifications
        private void CheckGcGen2CollectionCount()
        {
            this._logger.Log(LogLevel.Info,
                                         "Regular Check - GC Completed Notifications count: {0}, Gc gen 2 count: {1}",
                                         _gcFullollectionNotificationsCount, GC.CollectionCount(2));
        }

        private void GcChangeOnTradingStarting()
        {
            GC.Collect(2, GCCollectionMode.Forced);
            GC.WaitForPendingFinalizers();
            //Second call to make sure objects with finalizers are throwed away
            GC.Collect(2, GCCollectionMode.Forced);
            SetLowLatencySustainableModeInternal();
        }

        private void SetLowLatencySustainableModeInternal()
        {
            SetLowLatencySustainableMode();
        }

        public IntegratorRequestResult SetLowLatencySustainableMode()
        {
            this._logger.Log(LogLevel.Info, "Setting GC to LowLatencySustainable mode");
            GCSettings.LatencyMode = GCLatencyMode.SustainedLowLatency;
            this._logger.Log(LogLevel.Info, "Creating GCManager. Current GC mode: {0}, Is server GC: {1}",
                       GCSettings.LatencyMode, GCSettings.IsServerGC);
            return IntegratorRequestResult.GetSuccessResult();
        }

        public IntegratorRequestResult SetLowLatencyMode()
        {
            this._logger.Log(LogLevel.Info, "Setting GC to LOW LATENCY mode");
            GCSettings.LatencyMode = GCLatencyMode.LowLatency;
            this._logger.Log(LogLevel.Info, "Creating GCManager. Current GC mode: {0}, Is server GC: {1}",
                       GCSettings.LatencyMode, GCSettings.IsServerGC);
            return IntegratorRequestResult.GetSuccessResult();
        }

        private void SetInteractiveModeInternal()
        {
            SetInteractiveMode();
        }

        public IntegratorRequestResult SetInteractiveMode()
        {
            this._logger.Log(LogLevel.Info, "Setting GC to Interactive mode");
            GCSettings.LatencyMode = GCLatencyMode.Interactive;
            this._logger.Log(LogLevel.Info, "Creating GCManager. Current GC mode: {0}, Is server GC: {1}",
                       GCSettings.LatencyMode, GCSettings.IsServerGC);
            return IntegratorRequestResult.GetSuccessResult();
        }

        public IntegratorRequestResult FullCollect()
        {
            this._logger.Log(LogLevel.Info, "Collecting memory");
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            return IntegratorRequestResult.GetSuccessResult();
        }

        private int _gcFullollectionNotificationsCount = 0;
        private bool _exit = false;

        private void WaitForFullGCProc()
        {
            while (!_exit)
            {
                // CheckForNotify is set to true and false in Main. 
                while (true)
                {
                    // Check for a notification of an approaching collection.
                    GCNotificationStatus s = GC.WaitForFullGCApproach();
                    if (s == GCNotificationStatus.Succeeded)
                    {
                        this._logger.Log(LogLevel.Info, "GC Approach Notification raised.");
                    }
                    else if (s == GCNotificationStatus.Canceled)
                    {
                        this._logger.Log(LogLevel.Info, "GC Approach Notification cancelled.");
                        break;
                    }
                    else
                    {
                        // This can occur if a timeout period 
                        // is specified for WaitForFullGCApproach(Timeout)  
                        // or WaitForFullGCComplete(Timeout)   
                        // and the time out period has elapsed. 
                        this._logger.Log(LogLevel.Info, "GC Approach Notification not applicable.");
                        break;
                    }

                    // Check for a notification of a completed collection.
                    s = GC.WaitForFullGCComplete();
                    if (s == GCNotificationStatus.Succeeded)
                    {
                        _gcFullollectionNotificationsCount++;
                        this._logger.Log(LogLevel.Info,
                                         "GC Completed Notification raised. Count: {0}, Gc gen 2 count: {1}",
                                         _gcFullollectionNotificationsCount, GC.CollectionCount(2));
                    }
                    else if (s == GCNotificationStatus.Canceled)
                    {
                        this._logger.Log(LogLevel.Info, "GC Completed Notification not applicable.");
                        break;
                    }
                    else
                    {
                        // Could be a time out.
                        this._logger.Log(LogLevel.Info, "GC Completed Notification not applicable.");
                        break;
                    }
                }
            }
        }

    }
}
