﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.BusinessLayer.Fakes;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinessLayer
{
    public class InProcessClientsManager
    {
        private IBookTopProvidersStore _bookTopProvidersStore;
        private IVenueDataProviderStore _venueDataProviderStore;
        private IOrderManagementEx _orderManagement;
        private IEnumerable<IBroadcastInfosStore> _broadcastInfosStores;
        private ILogger _logger;
        private List<string> _clientFriendlyNames = new List<string>(); 
        private List<InProcessClientGateway> _inProcessClientGateways = new List<InProcessClientGateway>(); 

        private static int _clientsNum = 0;

        public InProcessClientsManager(IBookTopProvidersStore bookTopProvidersStore,
                                       IVenueDataProviderStore venueDataProviderStore, IOrderManagementEx orderManagement,
                                       IEnumerable<IBroadcastInfosStore> broadcastInfosStores, ILogger logger)
        {
            this._bookTopProvidersStore = bookTopProvidersStore;
            this._venueDataProviderStore = venueDataProviderStore;
            this._orderManagement = orderManagement;
            this._broadcastInfosStores = broadcastInfosStores;
            this._logger = logger;
        }

        public void CloseAllClients(string reason)
        {
            foreach (InProcessClientGateway inProcessClientGateway in _inProcessClientGateways.Where(gtw => !gtw.IsClosed))
            {
                inProcessClientGateway.CloseLocalResourcesOnly(reason);
            }
        }

        public IntegratorRequestResult StartNewGuiClientAsync(string commandString)
        {
            this._logger.Log(LogLevel.Info,
                             "InProcessClientsManager received request to start client, command string: [{0}]",
                             commandString);

            string[] parts = commandString.Split(new char[]{';'}, 4, StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length != 4)
            {
                this._logger.Log(LogLevel.Error,
                             "InProcessClientsManager received request to start client with invalid command string: [{0}], it contains only {1} parts",
                             commandString, parts.Length);
                return
                    IntegratorRequestResult.CreateFailedResult(
                        string.Format(
                            "InProcessClientsManager received request to start client with invalid command string: [{0}], it contains only {1} parts",
                            commandString, parts.Length));
            }

            new Thread(new ThreadStart(() => StartNewGuiClientInternal(parts[0], parts[1], parts[2], parts[3]))).Start();

            return IntegratorRequestResult.GetSuccessResult();
        }

        public void StartNewGuiClientAsync(string clientFriendlyName, string assemblyFullPath, string typeName, string configString)
        {
            new Thread(
                new ThreadStart(
                    () => StartNewGuiClientInternal(clientFriendlyName, assemblyFullPath, typeName, configString)))
                .Start();
        }


        private static IIntegratorClient CreateIntegratorClientInstance(string assemblyFullPath, string typeName, ILogger logger)
        {
            IIntegratorClient integratorClient = null;
            try
            {
                Type tradingClientType;

                Assembly clientAssembly = Assembly.LoadFrom(assemblyFullPath);
                tradingClientType = clientAssembly.GetType(typeName, true);

                integratorClient = Activator.CreateInstance(tradingClientType) as IIntegratorClient;
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Fatal, e, "Couldn't create the TradingClient object of type {0}", typeName);
            }

            return integratorClient;
        }

        private InProcessClientGateway CreateClientGateway(string clientIdentity)
        {
            return new InProcessClientGateway((LogFactory.Instance).GetLogger(clientIdentity + "_gateway"), _bookTopProvidersStore,
                                              _venueDataProviderStore, _orderManagement, _broadcastInfosStores,
                                              clientIdentity);
        }

        private void StartNewGuiClientInternal(string clientFriendlyName, string assemblyFullPath, string typeName, string configString)
        {
            this._logger.Log(LogLevel.Info, "InProcessClientsManager is starting client [{0}] of a type [{1}] from an assembly [{2}], passing a configstring [{3}]",
                clientFriendlyName, typeName, assemblyFullPath, configString);

            if (_clientFriendlyNames.Contains(clientFriendlyName))
            {
                this._logger.Log(LogLevel.Fatal, "Client {0} will not be started as client with the same name is already running",
                                 clientFriendlyName);
                return;
            }

            _clientFriendlyNames.Add(clientFriendlyName);

            IIntegratorClient integratorClient = CreateIntegratorClientInstance(assemblyFullPath, typeName, this._logger);

            if (integratorClient == null)
            {
                this._logger.Log(LogLevel.Fatal, "Client {0} will not be started due to assembly loading problems",
                                 clientFriendlyName);
                return;
            }

            InProcessClientGateway clientGateway = null;
            try
            {
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                clientGateway = CreateClientGateway(clientFriendlyName);

                clientGateway.ExceptionDuringDispatchingOccured += exception =>
                {
                    this._logger.LogException(LogLevel.Fatal, exception,
                                                            "Integrator client experienced unhandled exception");
                    bool handled = false;
                    try
                    {
                        handled = integratorClient.TryHandleUnhandledException(exception);
                    }
                    catch (Exception e)
                    {
                        this._logger.LogException(LogLevel.Fatal, e,
                                                                "Integrator client experienced unhandled exception - during handling another unhandled exception");
                    }

                    if (!handled)
                    {
                        this._logger.Log(LogLevel.Info,
                                                       "Client didn't handle the exception - so closing it.");
                        clientGateway.CloseLocalResourcesOnly("Integrator client experienced unhandled exception");
                        _clientFriendlyNames.Remove(clientFriendlyName);
                        _inProcessClientGateways.Remove(clientGateway);
                        Interlocked.Decrement(ref _clientsNum);
                    }
                };

                integratorClient.Configure((LogFactory.Instance).GetLogger(clientFriendlyName), clientGateway, configString);
                integratorClient.Start();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Unexpected exception during Trading Client [{0}] configuration and starting", clientFriendlyName);
                return;
            }

            
            Form form = Form.ActiveForm;
            if (form == null)
            {
                if (Application.OpenForms.Count > _clientsNum)
                {
                    form = Application.OpenForms[_clientsNum];
                }
            }

            if (form == null)
            {
                this._logger.Log(LogLevel.Fatal, "Trading Client [{0}] didn't create any form window - it might not be running correctly", clientFriendlyName);
                return;
            }

            string formName = string.IsNullOrEmpty(form.Name) ? "<Form Name was not specified>" : form.Name;
            this._logger.Log(LogLevel.Info, "Found form {0} created by client {1}. Will start message pump and wait for it to exit.",
                                                   formName, clientFriendlyName);

            Interlocked.Increment(ref _clientsNum);

            //We need to this on FormClosed as it is invoked only once. FormClosing can be reverted and called multiple times
            form.FormClosed += (sender, arguments) =>
                {
                    clientGateway.CloseLocalResourcesOnly("Gateway is closing (probably client disconnected)");
                    _clientFriendlyNames.Remove(clientFriendlyName);
                    _inProcessClientGateways.Remove(clientGateway);
                    Interlocked.Decrement(ref _clientsNum);
                };

            _inProcessClientGateways.Add(clientGateway);

            try
            {

                //this needs to happen just once and wee don't know which client is it
                if (!_isApplicationThreadExceptionHandlerSet)
                {
                    _isApplicationThreadExceptionHandlerSet = true;

                    Application.ThreadException +=
                        (sender, args) =>
                        this._logger.LogException(LogLevel.Fatal,
                                                  "One of Integrator clients experienced unhandled exception",
                                                  args.Exception);
                }

                this._logger.Log(LogLevel.Info, "Found form {0} created by client {1}. Will start message pump and wait for it to exit.",
                                                   formName, clientFriendlyName);

                Application.Run(form);
                this._logger.Log(LogLevel.Info, "Client {0} exited", clientFriendlyName);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Unexpected exception during Trading Client [{0}] window messagepump activation. This might mean a mismatch and inability to start new clients!!!", clientFriendlyName);
            }
        }

        private static bool _isApplicationThreadExceptionHandlerSet = false;

    }
}
