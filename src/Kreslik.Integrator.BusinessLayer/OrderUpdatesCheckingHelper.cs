﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinessLayer
{
    public class OrderUpdatesCheckingHelper
    {
        private HashSet<string> _setOfRemovedOrderIds = new HashSet<string>();
        private ILogger _logger;

        public OrderUpdatesCheckingHelper(ILogger logger)
        {
            TradingHoursHelper.Instance.MarketRollingOver += _setOfRemovedOrderIds.Clear;
            _logger = logger;
        }

        //This should be called in synchronized serialized context (e.g. in dispatching events)
        public void CheckEventIfExpected(IntegratorInfoObjectBase integratorInfoObjectBase)
        {
            if (integratorInfoObjectBase is ClientOrderUpdateInfo)
            {
                ClientOrderUpdateInfo orderUpdateInfo = integratorInfoObjectBase as ClientOrderUpdateInfo;

                if (orderUpdateInfo.OrderStatus == ClientOrderStatus.RemovedFromIntegrator ||
                    orderUpdateInfo.OrderStatus == ClientOrderStatus.RejectedByIntegrator ||
                    orderUpdateInfo.OrderStatus == ClientOrderStatus.NotActiveInIntegrator)
                {
                    _setOfRemovedOrderIds.Add(orderUpdateInfo.ClientOrderIdentity);
                }
            }

            if (integratorInfoObjectBase is IntegratorDealInternal)
            {
                IntegratorDealInternal dealInfo = integratorInfoObjectBase as IntegratorDealInternal;
                if (_setOfRemovedOrderIds.Contains(dealInfo.ClientOrderIdentity))
                {
                    this._logger.Log(LogLevel.Fatal,
                                     "Encountering IntegratorDealInternal for already inactive client order [{0}]. Deal: {1}",
                                     dealInfo.ClientOrderIdentity, dealInfo);
                }
            }
        }
    }
}
