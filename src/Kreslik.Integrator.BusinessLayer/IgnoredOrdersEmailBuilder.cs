﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinessLayer
{
    public class IgnoredOrdersEmailBuilder
    {
        private ILogger _logger;
        private EventsRateChecker[] _emailsRateCheckers =
            Enumerable.Repeat(1, Counterparty.ValuesCount)
                      .Select(foo => new EventsRateChecker(TimeSpan.FromMinutes(1), 10))
                      .ToArray();

        public IgnoredOrdersEmailBuilder(ILogger logger)
        {
            this._logger = logger;
        }

        public void SendIgnoredOrderEmail(IIntegratorOrderExternal orderExternal, BusinessLayerSettings businessSettings)
        {
            IEnumerable<string> toEmails = null;

            if (_emailsRateCheckers[(int) orderExternal.Counterparty].AddNextEventAndCheckIsAllowed())
            {
                var counterprtyInfo =
                    businessSettings.IgnoredOrderCounterpartyContacts.FirstOrDefault(
                        ctp =>
                        ctp.CounterpartyCode.Equals(orderExternal.Counterparty.ToString(),
                                                    StringComparison.InvariantCultureIgnoreCase));
                if (counterprtyInfo != null)
                {
                    toEmails = counterprtyInfo.EmailContacts.Split(new char[] {';'});
                }
            }
            else
            {
                this._logger.Log(LogLevel.Fatal,
                                 "Order [{0}] in broken state exceeded external emails rate checker - order will not be sent externally",
                                 orderExternal.Identity);
            }

            AutoMailer.TrySendMailAsync(this.BuildEmailSubject(orderExternal), this.BuildEmailBody(orderExternal), false,
                                        toEmails, businessSettings.IgnoredOrderEmailCc.Split(new char[] { ';' }));
        }

        public string BuildEmailSubject(IIntegratorOrderExternal orderExternal)
        {
            return string.Format("URGENT: KGT is missing execution report for {0}<>KGT order {1}",
                                 orderExternal.Counterparty, orderExternal.Identity);
        }


//@"Hello,
//
//KGT is missing an execution report (deal or rejection) for the following order:
//
//KGT BUYS 500000 USD/JPY from BOA @ 100.405 SPOT on 10:07:12.6346661 UTC
//
//This is the FIX order message we sent you on 10:07:12.6346661 UTC:
//
//Parsed message: 8 [BeginString]=FIX.4.3 # 9 [BodyLength]=217 # 34
//[MsgSeqNum]=1473 # 35 [MsgType]=D [ORDER_SINGLE] # 49
//[SenderCompID]=KGT-ORD # 52 [SendingTime]=20131115-10:07:12.633 # 56
//[TargetCompID]=BAML-FIX-FX # 11 [ClOrdID]=BOA131112dJUSDJPY573 # 15
//[Currency]=USD # 21 [HandlInst]=1 # 38 [OrderQty]=500000 # 40
//[OrdType]=D [Previously quoted] # 44 [Price]=100.405 # 54 [Side]=1
//[Buy] # 55 [Symbol]=USD/JPY # 60 [TransactTime]=20131115-10:07:12.633
//# 117 [QuoteID]=Oil01WSflG49l66bglG49l6kjPK20000000fzJoj8l600 # 10
//[CheckSum]=238 #
//
//Please let us know within the next 5 minutes if the order was done or not done.
//
//If we don't hear from you within 5 minutes, we consider the deal NOT DONE.
//
//Thanks and best regards,
//Michal
//
//--
//"

        private const string _mailBodyFormat =
@"Hello,

KGT is missing an execution report (deal or rejection) for the following order:

KGT {0}S {1} {2} {3} {4} @ {5} SPOT on {6:yyyy-MM-dd HH:mm:ss.fffffff UTC}

This is the FIX order message we sent you on {6:yyyy-MM-dd HH:mm:ss.fffffff UTC}:

Raw FIX message: {7}

WE CONSIDER THIS DEAL NOT DONE.

Thanks and best regards,
KGT team
--
Sent by automailer service from unmonitored mailbox - please use reply ALL when replying to this email
";

        public string BuildEmailBody(IIntegratorOrderExternal orderExternal)
        {
            DateTime fixSentTime;
            if (!TryGetSendingTime(orderExternal.OutgoingFixMessageRaw, out fixSentTime))
            {
                fixSentTime = orderExternal.IntegratorSentTimeUtc;
            }

            return string.Format(_mailBodyFormat, orderExternal.OrderRequestInfo.Side.ToString().ToUpper(),
                                 orderExternal.OrderRequestInfo.SizeBaseAbsInitial,
                                 orderExternal.OrderRequestInfo.Symbol,
                                 orderExternal.OrderRequestInfo.Side == DealDirection.Buy ? "from" : "to",
                                 orderExternal.Counterparty,
                                 orderExternal.OrderRequestInfo.RequestedPrice,
                                 fixSentTime, orderExternal.OutgoingFixMessageRaw);
        }

        public static string[] DATE_TIME_FORMATS = { "yyyyMMdd-HH:mm:ss.fff", "yyyyMMdd-HH:mm:ss" };
        public static DateTimeStyles DATE_TIME_STYLES = DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal;
        public static CultureInfo DATE_TIME_CULTURE_INFO = CultureInfo.InvariantCulture;

        public bool TryGetSendingTime(string msgRaw, out System.DateTime sendingTime)
        {
            sendingTime = default(DateTime);

            int startIdx = msgRaw.IndexOf("52=");

            if (startIdx < 0)
                return false;

            startIdx += "52=".Length;
            int endIdx = msgRaw.IndexOf('\u0001', startIdx);

            if (endIdx < 0)
                return false;

            string dateStr = msgRaw.Substring(startIdx, endIdx - startIdx).Trim();

            try
            {
                sendingTime = System.DateTime.ParseExact(dateStr, DATE_TIME_FORMATS, DATE_TIME_CULTURE_INFO, DATE_TIME_STYLES);
                return true;
            }
            catch (System.Exception e)
            {
                this._logger.LogException(LogLevel.Error, e,
                                          "Got exception when converting sending time [{0}] from message {1}",
                                          sendingTime, msgRaw);
            }

            return false;
        }
    }
}
