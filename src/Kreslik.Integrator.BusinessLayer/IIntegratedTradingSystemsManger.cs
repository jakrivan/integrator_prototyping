﻿using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.LowLatencyUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.BusinessLayer
{
    public interface IIntegratedTradingSystemsManger
    {
        void Initialize(ILogger logger, IBookTopProvidersStore priceBookStoreProvider, IOrderManagement orderManagement,
                                IRiskManager riskManager, ISymbolsInfo symbolsInfo, IPriceStreamMonitor priceStreamMonitor, ToBReceiver toBReceiver, IEnumerable<IOrderFlowSession> orderFlowSessions,
            IEnumerable<IStreamingChannel> streamingChannels, ITickerProvider lmaxTickerProvider, IUnicastInfoForwardingHub unicastInfoForwardingHub);

        void Start();
    }
}
