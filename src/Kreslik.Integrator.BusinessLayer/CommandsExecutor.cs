﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using Kreslik.Integrator.MessageBus;
using Kreslik.Integrator.SessionManagement;

namespace Kreslik.Integrator.BusinessLayer
{
    public class CommandsExecutor
    {
        private ILogger _logger;
        private ConnectionObjectsCollection _connectionObjectsCollection;
        private InProcessClientsManager _inProcessClientsManager;
        private GCManager _gcManager;
        private bool _isMdOffloadingProcess;

        public CommandsExecutor(ILogger logger, ConnectionObjectsCollection connectionObjectsCollection,
                                InProcessClientsManager inProcessClientsManager, GCManager gcManager, bool isMdOffloadingProcess)
        {
            this._logger = logger;
            this._connectionObjectsCollection = connectionObjectsCollection;
            this._inProcessClientsManager = inProcessClientsManager;
            this._gcManager = gcManager;
            this._isMdOffloadingProcess = isMdOffloadingProcess;
        }

        public void BindCommandDistributor(ILocalCommandDistributor commandDistributor)
        {
            commandDistributor.RegisterWorker("RestartQUOSession",
                counterpartyString =>
                    {
                        Counterparty counterparty = (Counterparty) counterpartyString;

                        if (!_isMdOffloadingProcess &&
                            SessionsSettings.SubscriptionSettings.EnabledCounterpartsWithMDSessionInSeparateProcess
                                            .Contains(counterparty))
                        {
                            return this.SendCommandToHotspotDataProcess("RestartQUOSession", counterparty);
                        }

                        return _connectionObjectsCollection.RestartMarketDataSession(counterparty);
                    });

            commandDistributor.RegisterWorker("StartQUOSession", counterpartyString =>
                {
                    Counterparty counterparty = (Counterparty)counterpartyString;

                    if (!_isMdOffloadingProcess &&
                        SessionsSettings.SubscriptionSettings.EnabledCounterpartsWithMDSessionInSeparateProcess
                                        .Contains(counterparty))
                    {
                        return this.SendCommandToHotspotDataProcess("StartQUOSession", counterparty);
                    }

                    return _connectionObjectsCollection.StartMarketDataSession(counterparty);
                });

            commandDistributor.RegisterWorker("StopQUOSession", counterpartyString =>
                {
                    Counterparty counterparty = (Counterparty)counterpartyString;

                    if (!_isMdOffloadingProcess &&
                        SessionsSettings.SubscriptionSettings.EnabledCounterpartsWithMDSessionInSeparateProcess
                                        .Contains(counterparty))
                    {
                        return this.SendCommandToHotspotDataProcess("StopQUOSession", counterparty);
                    }

                    return _connectionObjectsCollection.StopMarketDataSession(counterparty);
                });

            commandDistributor.RegisterWorker("RestartORDSession",
                                                   counterpart =>
                                                   _connectionObjectsCollection.RestartOrderFlowSession(
                                                       (Counterparty)counterpart));

            commandDistributor.RegisterWorker("RestartSession", counterpartyString =>
                {
                    if (counterpartyString.Equals("ALL", StringComparison.OrdinalIgnoreCase))
                    {
                        _connectionObjectsCollection.StopMarketDataSessions();
                        _connectionObjectsCollection.StopOrderFlowSessions();
                        Thread.Sleep(1000);
                        _connectionObjectsCollection.StartMarketDataSessions();
                        _connectionObjectsCollection.StartOrderFlowSessions();

                        IntegratorRequestResult resultAll = null;
                        if (!_isMdOffloadingProcess)
                            resultAll = this.SendCommandToHotspotDataProcess("RestartSession", "ALL");

                        return IntegratorRequestResult.GetSuccessResult() + resultAll;
                    }

                    Counterparty counterparty = (Counterparty) counterpartyString;

                    IntegratorRequestResult result;

                    if (!_isMdOffloadingProcess &&
                        SessionsSettings.SubscriptionSettings.EnabledCounterpartsWithMDSessionInSeparateProcess
                                        .Contains(counterparty))
                    {
                        result = this.SendCommandToHotspotDataProcess("RestartQUOSession", counterparty);
                    }
                    else
                    {
                        result = _connectionObjectsCollection.RestartMarketDataSession(counterparty);
                    }

                    return
                        result
                        +
                        _connectionObjectsCollection.RestartOrderFlowSession(counterparty);
                });

            commandDistributor.RegisterWorker("StopSession", counterpartyString =>
                {
                    if (counterpartyString.Equals("ALL", StringComparison.OrdinalIgnoreCase))
                    {
                        _connectionObjectsCollection.StopMarketDataSessions();
                        _connectionObjectsCollection.StopOrderFlowSessions();

                        IntegratorRequestResult resultAll = null;
                        if (!_isMdOffloadingProcess)
                            resultAll = this.SendCommandToHotspotDataProcess("StopSession", "ALL");

                        return IntegratorRequestResult.GetSuccessResult() + resultAll;
                    }

                    Counterparty counterparty = (Counterparty)counterpartyString;

                    if (!_isMdOffloadingProcess &&
                        SessionsSettings.SubscriptionSettings.EnabledCounterpartsWithMDSessionInSeparateProcess
                                        .Contains(counterparty))
                    {
                        return
                            this.SendCommandToHotspotDataProcess("StopQUOSession", counterparty)
                            +
                            _connectionObjectsCollection.StopOrderFlowSession((Counterparty) counterpartyString,
                                                                              TimeSpan.Zero);
                    }
                    else
                    {
                        return _connectionObjectsCollection.StopAndReleaseSessions(counterparty);
                    }
                    
                });

            commandDistributor.RegisterWorker("StartSession", counterpartyString =>
                {
                    if (counterpartyString.Equals("ALL", StringComparison.OrdinalIgnoreCase))
                    {
                        _connectionObjectsCollection.StartMarketDataSessions();
                        _connectionObjectsCollection.StartOrderFlowSessions();

                        IntegratorRequestResult resultAll = null;
                        if (!_isMdOffloadingProcess)
                            resultAll = this.SendCommandToHotspotDataProcess("StartSession", "ALL");

                        return IntegratorRequestResult.GetSuccessResult() + resultAll;
                    }

                    Counterparty counterparty = (Counterparty)counterpartyString;

                    IntegratorRequestResult result;

                    if (!_isMdOffloadingProcess &&
                        SessionsSettings.SubscriptionSettings.EnabledCounterpartsWithMDSessionInSeparateProcess
                                        .Contains(counterparty))
                    {
                        result = this.SendCommandToHotspotDataProcess("StartQUOSession", counterparty);
                    }
                    else
                    {
                        result = _connectionObjectsCollection.StartMarketDataSession(counterparty);
                    }

                    return
                        result
                        +
                        _connectionObjectsCollection.StartOrderFlowSession(counterparty);
                });

            

            commandDistributor.RegisterWorker("StartORDSession", counterpartyString =>
                _connectionObjectsCollection.StartOrderFlowSession((Counterparty)counterpartyString));
            commandDistributor.RegisterWorker("StopORDSession", counterpartyString =>
                _connectionObjectsCollection.StopOrderFlowSession((Counterparty)counterpartyString, TimeSpan.Zero));


            commandDistributor.RegisterWorker("RestartSTPSession", stpCtpString => 
                _connectionObjectsCollection.RestartStpSession((STPCounterparty)Enum.Parse(typeof(STPCounterparty), stpCtpString)));
            commandDistributor.RegisterWorker("StartSTPSession", stpCtpString =>
                _connectionObjectsCollection.StartStpSession((STPCounterparty)Enum.Parse(typeof(STPCounterparty), stpCtpString)));
            commandDistributor.RegisterWorker("StopSTPSession", stpCtpString =>
                _connectionObjectsCollection.StopStpSession((STPCounterparty)Enum.Parse(typeof(STPCounterparty), stpCtpString)));

            if(this._inProcessClientsManager != null)
                commandDistributor.RegisterWorker("StartClient", this._inProcessClientsManager.StartNewGuiClientAsync);

            if (this._gcManager != null)
            {
                commandDistributor.RegisterWorker("GCCollect", (command) => this._gcManager.FullCollect());
                commandDistributor.RegisterWorker("GCSustainedLowLatency",
                                                  (command) => this._gcManager.SetLowLatencySustainableMode());
                commandDistributor.RegisterWorker("GCInteractive", (command) => this._gcManager.SetInteractiveMode());
                commandDistributor.RegisterWorker("GCLowLatency", (command) => this._gcManager.SetLowLatencyMode());
            }
        }

        private IntegratorRequestResult SendCommandToHotspotDataProcess(string command, Counterparty counterparty)
        {
            return this.SendCommandToHotspotDataProcess(command, counterparty.ToString());
        }

        private IntegratorRequestResult SendCommandToHotspotDataProcess(string command, string counterpartyString)
        {
            if (_isMdOffloadingProcess)
            {
                string error =
                    string.Format(
                        "Attempt to send command [{0}] to MD offloading process - however current process is MD offloading process");
                this._logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }

            var hotspotDataDiagClient = new IntegratorDiagnosticsClient(
                _logger,
                new IntegratorInstanceProcessUtils(IntegratorProcessType.HotspotMarketData));

            IntegratorRequestResult result = hotspotDataDiagClient.Connect(false);

            if (!result.RequestSucceeded)
                return result;

            return hotspotDataDiagClient.TrySendCommand(command + ";" + counterpartyString);
        }
    }
}
