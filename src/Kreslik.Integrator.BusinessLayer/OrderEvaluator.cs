﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinessLayer
{
    public class OrderEvaluator : IOrderEvaluator
    {
        private static OrderEvaluator _buyOrderEvaluatorInstance = new OrderEvaluator(true);
        private static OrderEvaluator _sellOrderEvaluatorInstance = new OrderEvaluator(false);

        public static OrderEvaluator BuyOrderEvaluator
        {
            get { return _buyOrderEvaluatorInstance; }
        }

        public static OrderEvaluator SellOrderEvaluator
        {
            get { return _sellOrderEvaluatorInstance; }
        }

        private readonly bool _isBuyEvaluator;

        private OrderEvaluator(bool isBuyEvaluator)
        {
            this._isBuyEvaluator = isBuyEvaluator;
            this.OrderRankComparer = new OrderRankComparerClass(isBuyEvaluator);
            this.FillPreferenceComparer = new FillPreferenceComparerClass();
        }

        public bool MeetsPriceCriteria(IClientOrder order, PriceObjectInternal price)
        {
            decimal? requestedPrice = order.OrderRequestInfo.RequestedPrice;

            return 
                requestedPrice.HasValue &&
                _isBuyEvaluator
                       ? requestedPrice >= price.Price
                       : requestedPrice <= price.Price;
        }

        public IComparer<IClientOrder> OrderRankComparer { get; private set; }

        public IComparer<IClientOrder> FillPreferenceComparer { get; private set; }

        public bool IsBuyOrdersEvaluator
        {
            get { return this._isBuyEvaluator; }
        }

        private class OrderRankComparerClass : IComparer<IClientOrder>
        {
            private readonly bool _isBuyComparer;

            public OrderRankComparerClass(bool isBuyEvaluator)
            {
                this._isBuyComparer = isBuyEvaluator;
            }

            public int Compare(IClientOrder x, IClientOrder y)
            {
                decimal xprice;
                decimal yprice;

                //This comparer should be only used for limit prices
                // However if for some reason a market order is put in,
                // the limit orders should be always preffered 
                // (otherwise market would get on very top and it would be satisfiable on every quote
                //  but market orders have a separate filling mechanism)
                if (x != null && x.OrderRequestInfo.RequestedPrice.HasValue)
                {
                    xprice = x.OrderRequestInfo.RequestedPrice.Value;
                }
                else
                {
                    if (y != null && y.OrderRequestInfo.RequestedPrice.HasValue)
                    {
                        return -1;
                    }
                    else
                    {
                        return 0;
                    }
                }

                if (y != null && y.OrderRequestInfo.RequestedPrice.HasValue)
                {
                    yprice = y.OrderRequestInfo.RequestedPrice.Value;
                }
                else
                {
                    return 1;
                }

                return _isBuyComparer
                           ? decimal.Compare(xprice, yprice)
                           : decimal.Compare(yprice, xprice);
            }
        }

        private class FillPreferenceComparerClass : IComparer<IClientOrder>
        {

            public int Compare(IClientOrder x, IClientOrder y)
            {
                return DateTime.Compare(x.CreatedUtc, y.CreatedUtc);
            }
        }
    }
}
