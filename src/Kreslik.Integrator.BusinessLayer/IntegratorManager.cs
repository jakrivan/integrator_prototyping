﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.DAL.DataCollection;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.LowLatencyUtils;
using Kreslik.Integrator.RiskManagement;
using Kreslik.Integrator.SessionManagement;

namespace Kreslik.Integrator.BusinessLayer
{
    public class IntegratorManager
    {
        public PriceBookStore PriceBookStore { get; private set; }
        private FirmPoolBookStore FirmPoolBookStore { get; set; }
        public IBookTopProvidersStore BookTopProvidersStore { get; private set; }
        public IVenueDataProviderStore VenueDataProviderStore { get; private set; }
        public IOrderManagementEx OrderManagement { get; private set; }
        public ILogger BusinessLayerLogger { get; private set; }
        private ILocalCommandDistributor CommandDistributor { get; set; }
        public IRiskManager RiskManager { get; private set; }
        public PriceStreamsMonitor PriceStreamsMonitor { get; private set; }
        public PriceStreamsInfoProvider PriceStreamsInfoProvider { get; private set; }
        public IFIXSessionsDiagnostics FixSessionsDiagnostics { get; private set; }
        public IPriceBookDiagnostics PriceBookDiagnostics { get; private set; }
        public IDealStatistics DealStatistics { get; private set; }
        public GCManager GcManager { get; private set; }
        public CommandsExecutor CommandsExecutor { get; private set; }
        private ConnectionObjectsCollection _connectionObjectsCollection;
        private InProcessClientsManager _inProcessClientsManager;
        private ToBDataCollector _tobDataCollector;
        private MarketDataCollector _marketDataCollector;
        private IgnoredOrdersEmailBuilder _ignoredOrdersEmailBuilder;
        
        //need to gc root this to keep events publishers alive
        private IIntegratedTradingSystemsManger _integratedSystemsManger;

        public IEnumerable<IBroadcastInfosStore> BroadcastInfosStores { get; private set; }

        public IntegratorManager(ILogger businessLayerLogger, IntegratorProcessInfo integratorProcessInfo)
        {
            this.PriceStreamsMonitor = new PriceStreamsMonitor(DiagnosticsManager.DiagLogger, new PriceHistoryProvider());
            this.BusinessLayerLogger = businessLayerLogger;
            SymbolsInfoProvider symbolsInfoProvider = new SymbolsInfoProvider(this.BusinessLayerLogger,new PriceHistoryProvider());
            this.RiskManager = new RiskManager(PriceStreamsMonitor, symbolsInfoProvider.SymbolsInfo, DiagnosticsManager.GetSettlementDatesKeeper(FXCMMMCounterparty.FS1));   
            this.PriceBookStore = new PriceBookStore(businessLayerLogger);
            this.FirmPoolBookStore = new FirmPoolBookStore(businessLayerLogger);
            this.DealStatistics = new DealStatistics(
                new OrdersPersistor(DiagnosticsManager.DiagLogger), this.PriceStreamsMonitor, DiagnosticsManager.DiagLogger);

            IPersistentSettingsReader<BusinessLayerSettings> settingsReader =
                new PersistentSettingsReader<BusinessLayerSettings>(businessLayerLogger,
                                                                    @"Kreslik.Integrator.BusinessLayer.dll",
                                                                    Properties.Resources.Kreslik_Integrator_BusinessLayer_dll);
            _businessSettings = settingsReader.ReadSettings();
            if (_businessSettings == null)
            {
                this.BusinessLayerLogger.Log(LogLevel.Fatal, "Couldn't obtain proper business layer settings from backend. Check logs for details");
                Thread.Sleep(10);
                throw new Exception("Couldn't obtain proper business layer settings from backend. Check logs for details");
            }
                
            settingsReader.RegisterSettingsHandler(UpdateSettings);

            ToBReceiver toBReceiver = new ToBReceiver(DiagnosticsManager.DiagLogger);
            IIntegratorInstanceUtils integratorInstanceUtils = IntegratorInstanceUtils.Instance;
            _connectionObjectsCollection = ConnectionObjectsCollection.CreateCompleteSessionsCollection(
                this.RiskManager, symbolsInfoProvider.SymbolsInfo, this.DealStatistics, integratorInstanceUtils, 
                new UnicastInfoForwarder(UnicastInfoForwardingHub.Instance, (ITakerUnicastInfoForwarder) null), 
                this.BusinessLayerLogger, toBReceiver);

            this.BookTopProvidersStore = 
                new BookTopProvidersStoreInternal(this.PriceBookStore, this.FirmPoolBookStore, _connectionObjectsCollection.TakerConnectionObjectsKeepers, this.BusinessLayerLogger);
            this.VenueDataProviderStore =
                new VenueDataProviderStoreInternal(_connectionObjectsCollection.TakerConnectionObjectsKeepers);
            this.OrderManagement = new OrderManagement(this.PriceBookStore,
                                                       _connectionObjectsCollection.TakerOrderFlowSessions.ToList(),
                                                       businessLayerLogger, RiskManager,
                                                       IntegratorPerformanceCounter.Instance, _businessSettings);
            this.CommandDistributor = new CommandDistributor(businessLayerLogger, true);

            this.PriceBookDiagnostics = new PriceBookDiagnostics(this.PriceBookStore);
            this.PriceStreamsInfoProvider = new PriceStreamsInfoProvider(_connectionObjectsCollection.StreamingChannels);
            
            this.BroadcastInfosStores = new List<IBroadcastInfosStore>()
                {
                    this.RiskManager,
                    this.PriceStreamsMonitor,
                    symbolsInfoProvider,
                    new IntegratorProcessInfoToBroadcastIngoAdapter(this.BusinessLayerLogger, integratorProcessInfo)
                };
            this._inProcessClientsManager = new InProcessClientsManager(this.BookTopProvidersStore, this.VenueDataProviderStore,
                                                                        this.OrderManagement, this.BroadcastInfosStores,
                                                                        this.BusinessLayerLogger);

            //if one of the data collection wouldn't be enabled than this returns null objects (see implementation)
            _tobDataCollector = DataCollectionManager.ToBDataCollector;
            _marketDataCollector = DataCollectionManager.MarketDataCollector;

            //update backend ref prices only from one instance
            if (DataCollectionManager.ToBDataCollectionEnabled)
            {
                //subscribe to events after the OrderManagement is already subscribed
                this.PriceStreamsMonitor.EnableStoringRatesToBackend();
            }

            this.FixSessionsDiagnostics =
                new FIXSessionsDiagnostics(_connectionObjectsCollection.MarketDataSessions,
                                           _connectionObjectsCollection.OrderFlowSessions, integratorInstanceUtils,
                                           _connectionObjectsCollection.StpFlowSessions);

            this.GcManager = new GCManager(LogFactory.Instance.GetLogger("GCManager"), RiskManager, _businessSettings.TimeGapsWatchingBehavior);
            this._ignoredOrdersEmailBuilder = new IgnoredOrdersEmailBuilder(businessLayerLogger);
            this.CommandsExecutor = new CommandsExecutor(businessLayerLogger, _connectionObjectsCollection,
                                                         _inProcessClientsManager, GcManager, false);

            if (TryCreateUninitializedTradingSystemManager(this.BusinessLayerLogger, out _integratedSystemsManger))
            {
                _integratedSystemsManger.Initialize(
                    this.BusinessLayerLogger, this.BookTopProvidersStore, this.OrderManagement, this.RiskManager,
                    symbolsInfoProvider.SymbolsInfo, this.PriceStreamsMonitor, toBReceiver,
                    _connectionObjectsCollection.TakerOrderFlowSessions, _connectionObjectsCollection.StreamingChannels,
                    new LmaxTickerConsolidator(this._connectionObjectsCollection), UnicastInfoForwardingHub.Instance);
            }
            else
            {
                this.BusinessLayerLogger.Log(LogLevel.Fatal, "IntegratedTradingSystemsManger was not created (Public UAT Instance ?), no trading systems will be active as a result.");
            }

            TradingHoursHelper.Instance.MarketRolloverApproaching += () =>
            {
                //perform only if not yet enabled (to prevent unnecessary fatals)
                if (!RiskManager.IsGoFlatOnlyEnabled)
                    RiskManager.TurnOnGoFlatOnly("NY Rollover");
            };
        }

        private const string integratedSystemsAssemblyName = "Kreslik.Integrator.BusinessLayer.Systems";

        public static string GetSystemsAssemblyName()
        {
            string localDir =
                    System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            return System.IO.Path.Combine(localDir, integratedSystemsAssemblyName) + ".dll";
        }
        private static bool TryCreateUninitializedTradingSystemManager(ILogger logger, out IIntegratedTradingSystemsManger tradingSystemManager)
        {
            tradingSystemManager = null;
            try
            {
                Type tradingSystemManagerType;
                string assemblyFullPath = GetSystemsAssemblyName();

                if(!System.IO.File.Exists(assemblyFullPath))
                {
                    logger.Log(LogLevel.Fatal, "Cannot locate assembly with trading systems");
                    return false;
                }

                System.Reflection.Assembly clientAssembly = System.Reflection.Assembly.LoadFrom(assemblyFullPath);
                tradingSystemManagerType = clientAssembly.GetType("Kreslik.Integrator.BusinessLayer.Systems.IntegratedTradingSystemsManger", true);

                tradingSystemManager = Activator.CreateInstance(tradingSystemManagerType) as IIntegratedTradingSystemsManger;
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Fatal, e, "Couldn't create the TradingSystemsManager");
            }

            return tradingSystemManager != null;
        }

        private class LmaxTickerConsolidator : ITickerProvider
        {
            public event Action<VenueDealObjectInternal> NewVenueDeal
            {
                add
                {
                    foreach (
                        VenueMarketDataSession venueMarketDataSession in
                            _cc.TakerConnectionObjectsKeepers.Where(
                                bag =>
                                    bag != null && bag.MarketDataSession is VenueMarketDataSession &&
                                    bag.Counterparty.TradingTargetType == TradingTargetType.LMAX)
                                .Select(bag => bag.MarketDataSession))
                    {
                        venueMarketDataSession.NewVenueDeal += value;
                    }

                }
                remove
                {
                    foreach (
                        VenueMarketDataSession venueMarketDataSession in
                            _cc.TakerConnectionObjectsKeepers.Where(
                                bag =>
                                    bag != null && bag.MarketDataSession is VenueMarketDataSession &&
                                    bag.Counterparty.TradingTargetType == TradingTargetType.LMAX)
                                .Select(bag => bag.MarketDataSession))
                    {
                        venueMarketDataSession.NewVenueDeal -= value;
                    }
                }
            }

            private ConnectionObjectsCollection _cc;

            public LmaxTickerConsolidator(ConnectionObjectsCollection cc)
            {
                _cc = cc;
            }
        }

        public event Action IntegratorRestartRequested;

        //GC rooting this
        private ITradingControlSchedule _tradingControlSchedule;

        private void MountTradingSchedule()
        {
            _tradingControlSchedule = new TradingControlSchedule(BusinessLayerLogger);
            (_tradingControlSchedule).TradingControlScheduleOccured += entry =>
            {
                this.BusinessLayerLogger.Log(LogLevel.Info, "Executing Schedule " + entry.ToString());

                switch (entry.Action)
                {
                    case SystemsTradingControlScheduleAction.ActivateGoFlatOnly:
                        this.RiskManager.ExternalOrdersTransmissionControlScheduling.ScheduledTurnOnGoFlatOnly();
                        break;
                    case SystemsTradingControlScheduleAction.ActivateKillSwitch:
                        if (this.RiskManager.IsGoFlatOnlyEnabled)
                            this.RiskManager.ExternalOrdersTransmissionControlScheduling.ScheduledDisableTransmission();
                        else
                            this.BusinessLayerLogger.Log(LogLevel.Fatal,
                                "Reaching schedule for enabling Kill Switch, but Go Flat Only is not enabled. Ignoring the schedule now. Schedule: " +
                                entry);
                        break;
                    case SystemsTradingControlScheduleAction.RestartIntegrator:
                        if (this.RiskManager.IsTransmissionDisabled)
                        {
                            if (IntegratorRestartRequested != null)
                                IntegratorRestartRequested();
                        }
                        else
                            this.BusinessLayerLogger.Log(LogLevel.Fatal,
                                "Reaching schedule for restarting Integrator, but Kill Switch is not enabled. Ignoring the schedule now. Schedule: " +
                                entry);
                        break;
                    case SystemsTradingControlScheduleAction.NotifyClients:
                        UnicastInfoForwardingHub.Instance.ForwardUnicastInfoIfNeeded(
                            new ImportantIntegratorInformation("System Trading Schedule Notification", entry.Description,
                                1, LogLevel.Info),
                            MulticasInfoUtils.GetNontradingMulticastIndex(MulticastRequestType.FatalEmails));
                        break;
                    case SystemsTradingControlScheduleAction.LogFatal:
                        this.BusinessLayerLogger.Log(LogLevel.Fatal, entry.Description);
                        break;
                    default:
                        this.BusinessLayerLogger.Log(LogLevel.Fatal, "Encountered schedule entry of unrecognized action type: " + entry);
                        break;
                }
            };
        }

        

        private BusinessLayerSettings _businessSettings;
        private void UpdateSettings(BusinessLayerSettings setttings)
        {
            if (setttings == null)
            {
                this.BusinessLayerLogger.Log(LogLevel.Fatal,
                                             "Attempt to update BusinessLayer settings with null object (error during deserializing?)");
                return;
            }

            _businessSettings = setttings;
            this.OrderManagement.UpdateSettings(setttings);
            this.GcManager.UpdateSettings(setttings.TimeGapsWatchingBehavior);
        }

        private void BindHotspotToDataCollection(HotspotMarketDataSession hsms, MarketDataCollector collector)
        {
            if (hsms == null || collector.ExcludedCounterparties.Contains(hsms.Counterparty))
                return;

            hsms.EachNewQuote += collector.AddHotspotPrice;
            hsms.EachQuoteUpdate += collector.AddHotspotQuoteUpdate;
            hsms.EachQuoteCancel += collector.AddHotspotQuoteCancel;
            hsms.CancelAllQuotes += () =>
            {
                foreach (Symbol symbol in Symbol.Values)
                {
                    collector.AddQuoteCancel(symbol, hsms.HotspotCounterparty, null);
                }
            };
        }

        private void BindToBDataCollection(ToBDataCollector dataCollector, IPriceBookStoreEx priceBookStore)
        {
            priceBookStore.BookTopImproved += (sender, priceObject, timestamp) =>
            {
                PriceObjectInternal contraPrice = priceObject.Side == PriceSide.Ask
                    ? PriceBookStore.GetBidPriceBook(priceObject.Symbol).MaxNodeInternal
                    : PriceBookStore.GetAskPriceBook(priceObject.Symbol).MaxNodeInternal;

                dataCollector.AddToBDcData(
                    askPrice:
                        priceObject.Side == PriceSide.Ask
                            ? priceObject
                            : contraPrice,
                    bidPrice:
                        priceObject.Side == PriceSide.Bid
                            ? priceObject
                            : contraPrice,
                    creationTimeUtc: timestamp,
                    triggeringCounterparty: priceObject.Counterparty);

                contraPrice.Release();
            };
        }

        private void BindOutgoingPricesCollection(OutgoingPriceDataCollector dataCollector,
            IEnumerable<IStreamingChannel> streamingChannels)
        {
            foreach (IStreamingChannelDCInfo streamingChannelDcInfo in streamingChannels)
            {
                streamingChannelDcInfo.NewPriceSent += dataCollector.AddOutgoingPrice;
                streamingChannelDcInfo.NewPriceCancelSent += dataCollector.AddOutgoingPriceCancel;
            }
        }

        private void BindFatalNotifications(ConnectionObjectsCollection connectionObjectsCollection)
        {
            foreach (
                TakerConnectionObjectsKeeper connectionObjectsKeeper in connectionObjectsCollection.TakerConnectionObjectsKeepers.Where(obj => obj != null))
            {
                TakerConnectionObjectsKeeper takerConnectionObjects = connectionObjectsKeeper;
                takerConnectionObjects.OrderFlowSession.OnOrderChanged += (order, eventArgs) =>
                {
                    IntegratorOrderExternalChange state = eventArgs.ChangeState;
                    if (state == IntegratorOrderExternalChange.InBrokenState)
                    {
                        try
                        {
                            this._ignoredOrdersEmailBuilder.SendIgnoredOrderEmail(order, this._businessSettings);
                            //Do not block other subscribers that need to know about broken order (as they might react on stopping session and recursively try to cancel it)
                            Task.Factory.StartNew(
                                () =>
                                    connectionObjectsCollection.StopMarketDataSession(
                                        takerConnectionObjects.Counterparty)).ObserveException();
                        }
                        catch (Exception e)
                        {
                            this.BusinessLayerLogger.LogException(LogLevel.Fatal,
                                                                  "Exception during sending ignored order email", e);
                        }
                    }
                    else if (
                            state == IntegratorOrderExternalChange.Rejected
                            && this._businessSettings.FatalRejectedOrderTriggerPhrases != null
                            && !string.IsNullOrEmpty(eventArgs.RejectionInfo.RejectionReason)
                            && this._businessSettings.FatalRejectedOrderTriggerPhrases.Any(trigger => eventArgs.RejectionInfo.RejectionReason.IndexOf(trigger, StringComparison.InvariantCultureIgnoreCase) >= 0))
                    {
                        this.BusinessLayerLogger.Log(LogLevel.Fatal,
                                                     "Order [{0}] rejected, and reason [{1}] matched red flag list",
                                                     order.Identity, eventArgs.RejectionInfo.RejectionReason);
                    }
                };
            }

            this.BusinessLayerLogger.IsLowDiskSpaceChanged += isLowDiskSpace => AutoMailer.TrySendMailAsync(
                string.Format("{1}RUNNING LOW ON DISK SPACE {0}",
                              SettingsInitializator.Instance.InstanceDescription, isLowDiskSpace ? string.Empty : "NOT "), string.Empty,
                false, _businessSettings.StartStopEmailTo.Split(new char[] { ';' }),
                _businessSettings.StartStopEmailCc.Split(new char[] { ';' }));
        }

        public IClientGateway CreateClientGateway(string clientIdentity)
        {
            return new InProcessClientGateway(this.BusinessLayerLogger, this.BookTopProvidersStore,
                                              this.VenueDataProviderStore, this.OrderManagement,
                                              this.BroadcastInfosStores, clientIdentity);
        }

        public void BindComponentsEvents()
        {
            this.BindCommandDistributor();
            BindMessagingEventsToPriceChangesConsumer(this.BusinessLayerLogger,
                _connectionObjectsCollection.TakerConnectionObjectsKeepers,
                this.PriceBookStore, this.FirmPoolBookStore, this._marketDataCollector, this.PriceStreamsMonitor, this.PriceStreamsInfoProvider,
                this.RiskManager.SymbolTrustworthinessInfoProvider as IPriceChangesConsumer);
            BindHotspotToPriceBook(_connectionObjectsCollection.HotspotPricesAdapter, this.PriceBookStore);
            this.BindLoggingToPriceBookEvents(this.BusinessLayerLogger);

            this.BindToBDataCollection(_tobDataCollector, PriceBookStore);
            this.BindOutgoingPricesCollection(DataCollectionManager.OutgoingPriceDataCollector,
                _connectionObjectsCollection.StreamingChannels);
            this.BindFatalNotifications(_connectionObjectsCollection);

            foreach (
                IMarketDataSession marketDataSession in
                    _connectionObjectsCollection.TakerConnectionObjectsKeepers.Where(
                        bag => bag != null && bag.Counterparty.TradingTargetType == TradingTargetType.Hotspot)
                                                .Select(bag => bag.MarketDataSession))
            {
                this.BindHotspotToDataCollection(marketDataSession as HotspotMarketDataSession, this._marketDataCollector);
            }


            foreach (
                VenueMarketDataSession venueMarketDataSession in
                    _connectionObjectsCollection.TakerConnectionObjectsKeepers.Where(
                        bag => bag != null && bag.MarketDataSession is VenueMarketDataSession)
                                                .Select(bag => bag.MarketDataSession))
            {
                venueMarketDataSession.NewVenueDeal += this._marketDataCollector.AddVenuDealData;
            }

            this.MountTradingSchedule();


            //we need this to happen AFTER BindMessagingEventsToPriceChangesConsumer called on pricebook - so that we know that
            // pricebook has always the data first, before the systems
            if (_integratedSystemsManger != null)
            {
                TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(10), _integratedSystemsManger.Start);
            }
        }

        public void StartSessions(bool startMarketDataSessions, bool startOrderFlowSessions)
        {
            List<Task> tasks = new List<Task>();

            if (startMarketDataSessions) tasks.Add(Task.Factory.StartNew(_connectionObjectsCollection.StartMarketDataSessions));
            if (startOrderFlowSessions) tasks.Add(Task.Factory.StartNew(_connectionObjectsCollection.StartOrderFlowSessions));
            tasks.Add(Task.Factory.StartNew(_connectionObjectsCollection.StartStpSessions));

            Task.WaitAll(tasks.ToArray());
        }

        public void Start()
        {
            try
            {
                AutoMailer.TrySendMailAsync(
                string.Format(_businessSettings.StartStopEmailSubject, SettingsInitializator.Instance.InstanceDescription, "Starting"),
                _businessSettings.StartStopEmailBody,
                false,
                _businessSettings.StartStopEmailTo.Split(new char[] { ';' }),
                _businessSettings.StartStopEmailCc.Split(new char[] { ';' }));
            }
            catch (Exception e)
            {
                this.BusinessLayerLogger.LogException(LogLevel.Fatal, "Exception during sending start email", e);
            }


            this.StartSessions(!_businessSettings.StartWithSessionsStopped,
                               !_businessSettings.StartWithSessionsStopped);
        }

        public void Stop()
        {
            try
            {
                AutoMailer.TrySendMailAsync(
                string.Format(_businessSettings.StartStopEmailSubject, SettingsInitializator.Instance.InstanceDescription, "Stopping"),
                _businessSettings.StartStopEmailBody,
                false,
                _businessSettings.StartStopEmailTo.Split(new char[] { ';' }),
                _businessSettings.StartStopEmailCc.Split(new char[] { ';' }));
            }
            catch (Exception e)
            {
                this.BusinessLayerLogger.LogException(LogLevel.Fatal, "Exception during sending stop email", e);
            }

            List<Task> tasks = new List<Task>();

            tasks.Add(Task.Factory.StartNew(_connectionObjectsCollection.StopMarketDataSessions));
            tasks.Add(Task.Factory.StartNew(_connectionObjectsCollection.StopOrderFlowSessions));
            tasks.Add(Task.Factory.StartNew(_connectionObjectsCollection.StopStpSessions));

            Task.Factory.StartNew(() => this.PriceStreamsMonitor.RefreshBackendValues());

            _tobDataCollector.StopDataCollection();
            _marketDataCollector.StopDataCollection();
            DataCollectionManager.OutgoingPriceDataCollector.StopDataCollection();

            this._inProcessClientsManager.CloseAllClients("Integrator is closing");

            Task.WaitAll(tasks.ToArray());

            AutoMailer.AbortAllInstanceEmailsRetires();

            //Here we might want to wait for data collections to finish - with timeout
            //System.Threading.WaitHandle.WaitAll(
            //    new[]
            //        {
            //            _tobDataCollector.DataCollectorCompletelyShuttedDownWaitHandle,
            //            _allPricesDataCollector.DataCollectorCompletelyShuttedDownWaitHandle
            //        }, TimeSpan.FromSeconds(5));
            //Or this is how we would wait for just one of those:
            //_tobDataCollector.DataCollectorCompletelyShuttedDownWaitHandle.WaitOne(TimeSpan.FromSeconds(5));
        }

        private void BindCommandDistributor()
        {
            this.CommandsExecutor.BindCommandDistributor(this.CommandDistributor);
        }

        private void BindLoggingToPriceBookEvents(ILogger logger)
        {
            //Uncomment following to have price updates in log
            //this.PriceBookStore.BookTopImproved +=
            //    (price, timestamp) =>
            //    logger.Log(LogLevel.Info, "Improvement - {0}", price);

            //this.PriceBookStore.BookTopDeterioratedInternal +=
            //    (price, timestamp) =>
            //        {
            //            if (price != null)
            //            {
            //                logger.Log(LogLevel.Info, "Deterioration - {0}", price);
            //            }
            //        };

            //TODO: uncoment this once HOT price book movements needs to be in log
            //if (this._htaMarketDataSession != null)
            //{
            //    this._htaMarketDataSession.BookTopImproved +=
            //        price => logger.Log(LogLevel.Info, "Hotspot (all) Improvement - {0}", price);

            //    this._htaMarketDataSession.BookTopDeteriorated +=
            //         price => logger.Log(LogLevel.Info, "Hotspot (all) Deterioration - {0}", price);
            //}

            //if (this._htfMarketDataSession != null)
            //{
            //    this._htfMarketDataSession.BookTopImproved +=
            //        price => logger.Log(LogLevel.Info, "Hotspot (firm) Improvement - {0}", price);

            //    this._htfMarketDataSession.BookTopDeteriorated +=
            //         price => logger.Log(LogLevel.Info, "Hotspot (firm) Deterioration - {0}", price);
            //}
        }

        private static void CancelAllSymbolsAndLayersOfSubscribers(IPriceChangesConsumer[] priceChangesConsumers,
            Counterparty counterparty, bool nonexecutableDataAllowed)
        {
            foreach (Symbol symbol in Symbol.Values)
            {
                CancelAllLayersOfSubscribers(priceChangesConsumers, counterparty, symbol, nonexecutableDataAllowed);
            }
        }

        private static void CancelAllLayersOfSubscribers(IPriceChangesConsumer[] priceChangesConsumers,
            Counterparty counterparty, Symbol symbol, bool nonexecutableDataAllowed)
        {

            foreach (
                IPriceChangesConsumer priceChangesConsumer in
                    priceChangesConsumers.Where(
                        consumer =>
                            //All consumers interrested in this counterparty
                            consumer.ConsumesDataFromCounterparty(counterparty) &&
                            // that also wants only executable data
                            !(nonexecutableDataAllowed && consumer.ConsumesMDDataIfOFNotRunning(counterparty))))
            {
                if (counterparty == LmaxCounterparty.L01)
                {
                    foreach (Counterparty sublayerCounterparty in LmaxCounterparty.LayersOfLM1)
                    {
                        priceChangesConsumer.OnQuoteCancel(symbol, sublayerCounterparty);
                    }
                }
                else
                {
                    priceChangesConsumer.OnQuoteCancel(symbol, counterparty);
                }
            }
        }

        private static void BindHotspotToPriceBook(IMarketDataSession hotspotSession, IPriceChangesConsumer book)
        {
            hotspotSession.OnNewPrice += book.OnNewPrice;
            hotspotSession.OnIgnoredPrice += book.OnIgnoredPrice;
            hotspotSession.OnCancelLastQuote += book.OnQuoteCancel;
            hotspotSession.OnCancelLastPrice += book.OnPriceCancel;
        }

        internal static void BindMessagingEventsToPriceChangesConsumer(ILogger logger,
            IEnumerable<ITakerConnectionObjectsKeeper> connectionObjectsKeepers, params IPriceChangesConsumer[] priceChangesConsumers)
        {
            foreach (TakerConnectionObjectsKeeper connectionObjectsKeeper in connectionObjectsKeepers.Where(obj => obj != null && obj.MarketDataSession != null))
            {
                Counterparty counterparty = connectionObjectsKeeper.Counterparty;
                TakerConnectionObjectsKeeper takerConnectionObjects = connectionObjectsKeeper;

                //We want to process quotes only if OrderFlowSessions is runnig (and so we can execute quotes)
                if (takerConnectionObjects.OrderFlowSession != null)
                    takerConnectionObjects.OrderFlowSession.OnStopInitiated +=
                        () =>
                        {
                            //unsubscribe from prices
                            logger.Log(LogLevel.Warn,
                                                         "OrderFlow session {0} is stopped so unsubscribing PriceBook from {0} quotes",
                                                         counterparty);
                            foreach (
                                IPriceChangesConsumer priceChangesConsumer in
                                    priceChangesConsumers.Where(
                                        //All consumers interrested in this counterparty that also wants only executable data
                                        consumer =>
                                            consumer.ConsumesDataFromCounterparty(counterparty) &&
                                            !consumer.ConsumesMDDataIfOFNotRunning(counterparty)))
                            {
                                takerConnectionObjects.MarketDataSession.OnNewPrice -= priceChangesConsumer.OnNewPrice;
                                takerConnectionObjects.MarketDataSession.OnNewNonExecutablePrice -= priceChangesConsumer.OnNewNonExecutablePrice;
                                takerConnectionObjects.MarketDataSession.OnIgnoredPrice -= priceChangesConsumer.OnIgnoredPrice;
                                takerConnectionObjects.MarketDataSession.OnCancelLastQuote -= priceChangesConsumer.OnQuoteCancel;
                                takerConnectionObjects.MarketDataSession.OnCancelLastPrice -= priceChangesConsumer.OnPriceCancel;
                            }

                            //and remove the last prices from this counterpart
                            CancelAllSymbolsAndLayersOfSubscribers(priceChangesConsumers, counterparty, true);
                        };

                //We want to process quotes only if OrderFlowSessions is runnig (and so we can execute quotes)
                takerConnectionObjects.MarketDataSession.OnStopped +=
                    () =>
                    {
                        //unsubscribe from prices
                        logger.Log(LogLevel.Warn,
                                                     "MarketData session {0} is stopped so cancelling all {0} quotes from PriceBook",
                                                     counterparty);

                        //and remove the last prices from this counterpart
                        CancelAllSymbolsAndLayersOfSubscribers(priceChangesConsumers, counterparty, false);
                    };

                if (takerConnectionObjects.OrderFlowSession != null)
                    takerConnectionObjects.OrderFlowSession.OnStarted +=
                        () =>
                        {
                            logger.Log(LogLevel.Info,
                                                         "OrderFlow session {0} is running so subscribing PriceBook to {0} quotes",
                                                         counterparty);
                            //just to make sure we don't have double subscriptions
                            foreach (IPriceChangesConsumer priceChangesConsumer in priceChangesConsumers.Where(consumer => consumer.ConsumesDataFromCounterparty(counterparty)))
                            {
                                takerConnectionObjects.MarketDataSession.OnNewPrice -= priceChangesConsumer.OnNewPrice;
                                takerConnectionObjects.MarketDataSession.OnNewPrice += priceChangesConsumer.OnNewPrice;

                                takerConnectionObjects.MarketDataSession.OnNewNonExecutablePrice -= priceChangesConsumer.OnNewNonExecutablePrice;
                                takerConnectionObjects.MarketDataSession.OnNewNonExecutablePrice += priceChangesConsumer.OnNewNonExecutablePrice;

                                takerConnectionObjects.MarketDataSession.OnIgnoredPrice -= priceChangesConsumer.OnIgnoredPrice;
                                takerConnectionObjects.MarketDataSession.OnIgnoredPrice += priceChangesConsumer.OnIgnoredPrice;

                                takerConnectionObjects.MarketDataSession.OnCancelLastQuote -= priceChangesConsumer.OnQuoteCancel;
                                takerConnectionObjects.MarketDataSession.OnCancelLastQuote += priceChangesConsumer.OnQuoteCancel;

                                takerConnectionObjects.MarketDataSession.OnCancelLastPrice -= priceChangesConsumer.OnPriceCancel;
                                takerConnectionObjects.MarketDataSession.OnCancelLastPrice += priceChangesConsumer.OnPriceCancel;
                            }
                        };

                //following two event's are harmles even if OF is not running - they will never lead to matching process
                takerConnectionObjects.MarketDataSession.OnSubscriptionChanged += subscription =>
                {
                    if (subscription.SubscriptionStatus != SubscriptionStatus.Subscribed &&
                        subscription.SubscriptionStatus != SubscriptionStatus.Subscribed_ValidationPending)
                    {
                        CancelAllLayersOfSubscribers(priceChangesConsumers, counterparty,
                            subscription.SubscriptionRequestInfo.Symbol, false);
                    }
                };

                if (takerConnectionObjects.OrderFlowSession != null)
                    takerConnectionObjects.OrderFlowSession.OnOrderChanged += (order, eventArgs) =>
                        {
                            if ((order.OrderStatus == IntegratorOrderExternalStatus.Rejected ||
                                 order.OrderStatus == IntegratorOrderExternalStatus.InBrokenState)
                                &&
                                order.OrderRequestInfo.IntegratorPriceIdentity != Guid.Empty)
                            {
                                foreach (IPriceChangesConsumer priceChangesConsumer in priceChangesConsumers.Where(consumer => consumer.ConsumesDataFromCounterparty(counterparty)))
                                {
                                    priceChangesConsumer.OnPriceInvalidate(order.OrderRequestInfo.Symbol, order.OrderRequestInfo.Side.ToInitiatingPriceDirection(),
                                        order.Counterparty, order.OrderRequestInfo.IntegratorPriceIdentity,
                                        order.OrderRequestInfo.IntegratorPriceReceivedUtc);
                                }
                            }
                        };

                bool areSessionsRunning = 
                    takerConnectionObjects.MarketDataSession.State == SessionState.Running &&
                    takerConnectionObjects.OrderFlowSession != null &&                      
                    connectionObjectsKeeper.OrderFlowSession.State == SessionState.Running;

                foreach (IPriceChangesConsumer priceChangesConsumer in priceChangesConsumers.Where(consumer => 
                    consumer.ConsumesDataFromCounterparty(counterparty) && (areSessionsRunning || consumer.ConsumesMDDataIfOFNotRunning(counterparty))))
                {
                    takerConnectionObjects.MarketDataSession.OnNewPrice -= priceChangesConsumer.OnNewPrice;
                    takerConnectionObjects.MarketDataSession.OnNewPrice += priceChangesConsumer.OnNewPrice;

                    takerConnectionObjects.MarketDataSession.OnNewNonExecutablePrice -= priceChangesConsumer.OnNewNonExecutablePrice;
                    takerConnectionObjects.MarketDataSession.OnNewNonExecutablePrice += priceChangesConsumer.OnNewNonExecutablePrice;

                    takerConnectionObjects.MarketDataSession.OnCancelLastQuote -= priceChangesConsumer.OnQuoteCancel;
                    takerConnectionObjects.MarketDataSession.OnCancelLastQuote += priceChangesConsumer.OnQuoteCancel;

                    takerConnectionObjects.MarketDataSession.OnCancelLastPrice -= priceChangesConsumer.OnPriceCancel;
                    takerConnectionObjects.MarketDataSession.OnCancelLastPrice += priceChangesConsumer.OnPriceCancel;

                    takerConnectionObjects.MarketDataSession.OnIgnoredPrice -= priceChangesConsumer.OnIgnoredPrice;
                    takerConnectionObjects.MarketDataSession.OnIgnoredPrice += priceChangesConsumer.OnIgnoredPrice;
                }
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    /// /////////////////////////////////////////////////////////////////////////////////////////
    /// /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////

    public class IntegratorHotspotDataManager
    {
        public PriceStreamsInfoProvider PriceStreamsInfoProvider { get; private set; }
        public IBookTopProvidersStore BookTopProvidersStore { get; private set; }
        public IVenueDataProviderStore VenueDataProviderStore { get; private set; }
        public ILogger HotspotDataLogger { get; private set; }
        private ILocalCommandDistributor CommandDistributor { get; set; }
        public IFIXSessionsDiagnostics FixSessionsDiagnostics { get; private set; }
        public CommandsExecutor CommandsExecutor { get; private set; }
        private ConnectionObjectsCollection _connectionObjectsCollection;
        private MarketDataCollector _marketDataCollector;

        public IEnumerable<IBroadcastInfosStore> BroadcastInfosStores { get; private set; }

        public IntegratorHotspotDataManager(ILogger businessLayerLogger, IntegratorProcessInfo integratorProcessInfo)
        {
            this.PriceStreamsInfoProvider = new PriceStreamsInfoProvider(null);

            this.HotspotDataLogger = businessLayerLogger;

            IIntegratorInstanceUtils integratorInstanceUtils = IntegratorInstanceUtils.Instance;
            _connectionObjectsCollection =
                ConnectionObjectsCollection.CreateOnlyMarketSessionsCollection(integratorInstanceUtils, this.HotspotDataLogger);

            this.BookTopProvidersStore =
                //it will not be possible to get bankpool book top
                new BookTopProvidersStoreInternal(null, null, _connectionObjectsCollection.TakerConnectionObjectsKeepers, this.HotspotDataLogger);
            this.VenueDataProviderStore =
                new VenueDataProviderStoreInternal(_connectionObjectsCollection.TakerConnectionObjectsKeepers);

            this.CommandDistributor = new CommandDistributor(businessLayerLogger, true);

            this.BroadcastInfosStores = new List<IBroadcastInfosStore>()
                {
                    new IntegratorProcessInfoToBroadcastIngoAdapter(this.HotspotDataLogger, integratorProcessInfo)
                };

            _marketDataCollector = DataCollectionManager.MarketDataCollector;

            this.FixSessionsDiagnostics =
                new FIXSessionsDiagnostics(_connectionObjectsCollection.MarketDataSessions,
                                           _connectionObjectsCollection.OrderFlowSessions, integratorInstanceUtils,
                                           _connectionObjectsCollection.StpFlowSessions);

            this.CommandsExecutor = new CommandsExecutor(businessLayerLogger, _connectionObjectsCollection, null, null, true);
        }

        private void BindHotspotToDataCollection(HotspotMarketDataSession hsms, MarketDataCollector collector)
        {
            if (hsms == null || collector.ExcludedCounterparties.Contains(hsms.Counterparty))
                return;

            hsms.EachNewQuote += collector.AddHotspotPrice;
            hsms.EachQuoteUpdate += collector.AddHotspotQuoteUpdate;
            hsms.EachQuoteCancel += collector.AddHotspotQuoteCancel;
            hsms.CancelAllQuotes += () =>
            {
                foreach (Symbol symbol in Symbol.Values)
                {
                    collector.AddQuoteCancel(symbol, hsms.HotspotCounterparty, null);
                }
            };
        }

        private void BindToBPublishing(HotspotMarketDataSession hsms, ToBSender toBSender)
        {
            hsms.BookTopDeterioratedInternal += (sender, node, stamp) => toBSender.SendPrice(node, hsms.LastSignalSentUtc);
            hsms.BookTopImproved += (sender, node, stamp) => toBSender.SendPrice(node, hsms.LastSignalSentUtc);
            hsms.BookTopReplaced += (sender, node, stamp) => toBSender.SendPrice(node, hsms.LastSignalSentUtc);
            hsms.CancelAllQuotes += () => toBSender.SendCancelAllQuotes(hsms.Counterparty);
        }

        public void BindComponentsEvents()
        {
            this.BindCommandDistributor();
            IntegratorManager.BindMessagingEventsToPriceChangesConsumer(this.HotspotDataLogger,
                                                                        _connectionObjectsCollection
                                                                            .TakerConnectionObjectsKeepers,
                                                                        this.PriceStreamsInfoProvider);
            this.BindMailer();

            //No need to GcRoot this as it is rooted as events subscriber
            ToBSender toBSender = new ToBSender(this.HotspotDataLogger, HotspotCounterparty.HTF);

            foreach (
                IMarketDataSession marketDataSession in
                    _connectionObjectsCollection.TakerConnectionObjectsKeepers.Where(
                        bag => bag != null && bag.Counterparty.TradingTargetType == TradingTargetType.Hotspot)
                                                .Select(bag => bag.MarketDataSession))
            {
                //currently Hotspot ToBs are needed only for H4T
                if(marketDataSession.Counterparty == Counterparty.H4T || marketDataSession.Counterparty == Counterparty.HTF)
                    this.BindToBPublishing(marketDataSession as HotspotMarketDataSession, toBSender);
                this.BindHotspotToDataCollection(marketDataSession as HotspotMarketDataSession, this._marketDataCollector);
            }
        }

        public void StartSessions(bool startMarketDataSessions, bool startOrderFlowSessions, bool startStpSessions)
        {
            List<Task> tasks = new List<Task>();

            if (startMarketDataSessions) tasks.Add(Task.Factory.StartNew(_connectionObjectsCollection.StartMarketDataSessions));
            if (startOrderFlowSessions) tasks.Add(Task.Factory.StartNew(_connectionObjectsCollection.StartOrderFlowSessions));
            if (startStpSessions) tasks.Add(Task.Factory.StartNew(_connectionObjectsCollection.StartStpSessions));

            Task.WaitAll(tasks.ToArray());
        }

        public void Start()
        {
            this.StartSessions(true, false, false);
        }

        public void Stop()
        {
            List<Task> tasks = new List<Task>();

            tasks.Add(Task.Factory.StartNew(_connectionObjectsCollection.StopMarketDataSessions));
            tasks.Add(Task.Factory.StartNew(_connectionObjectsCollection.StopOrderFlowSessions));
            tasks.Add(Task.Factory.StartNew(_connectionObjectsCollection.StopStpSessions));

            _marketDataCollector.StopDataCollection();

            Task.WaitAll(tasks.ToArray());

            AutoMailer.AbortAllInstanceEmailsRetires();
        }

        private void BindMailer()
        {
            this.HotspotDataLogger.IsLowDiskSpaceChanged += isLowDiskSpace => AutoMailer.TrySendMailAsync(
                string.Format("{1}RUNNING LOW ON DISK SPACE {0}",
                              SettingsInitializator.Instance.InstanceDescription, isLowDiskSpace ? string.Empty : "NOT "), string.Empty,
                false, CommonSettings.CommonBehavior.Logging.FatalErrorsEmailTo.Split(new char[] { ';' }),
                CommonSettings.CommonBehavior.Logging.FatalErrorsEmailCc.Split(new char[] { ';' }));
        }

        private void BindCommandDistributor()
        {
            this.CommandsExecutor.BindCommandDistributor(this.CommandDistributor);
        }
    }


    internal class BookTopProvidersStoreInternal : IBookTopProvidersStore
    {
        private MDSessionToPriceBookAdapter[] _venueSessionToBookAdapters = new MDSessionToPriceBookAdapter[Counterparty.ValuesCount];
        private IList<ITakerConnectionObjectsKeeper> _connectionObjectsKeeper;
        private ILogger _logger;

        public BookTopProvidersStoreInternal(IPriceBookStoreEx priceBookStore, IPriceBookStore firmpoolBookStore,
                                             IList<ITakerConnectionObjectsKeeper> connectionObjectsKeepers, ILogger logger)
        {
            this.BankpoolBookTopProvider = priceBookStore;
            this.FirmpoolBookTopProvider = firmpoolBookStore;
            this._connectionObjectsKeeper = connectionObjectsKeepers;
            this._logger = logger;
        }

        public IPriceBookStoreEx BankpoolBookTopProvider { get; private set; }
        public IPriceBookStore FirmpoolBookTopProvider { get; private set; }

        public IBookTopProvider<PriceObjectInternal> GetBookTopProvider(Counterparty counterparty)
        {
            if (counterparty != Counterparty.NULL && counterparty.TradingTargetType == TradingTargetType.Hotspot)
            {
                if (this._connectionObjectsKeeper.Count > (int)counterparty &&
                    this._connectionObjectsKeeper[(int)counterparty] != null &&
                    this._connectionObjectsKeeper[(int)counterparty].MarketDataSession != null &&
                    (this._connectionObjectsKeeper[(int)counterparty].MarketDataSession is HotspotMarketDataSession))
                {
                    return
                        this._connectionObjectsKeeper[(int)counterparty].MarketDataSession as
                        HotspotMarketDataSession;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return this.TryGetBookTopProvider(counterparty);
            }
        }

        public IPriceBookStoreEx TryGetBookTopProvider(Counterparty counterparty)
        {
            if (counterparty == Counterparty.NULL || counterparty.TradingTargetType == TradingTargetType.BankPool)
            {
                return this.BankpoolBookTopProvider;
            }
            else if (counterparty.TradingTargetType == TradingTargetType.Hotspot)
            {
                return null;
            }
            else
            {
                if (_venueSessionToBookAdapters[(int)counterparty] == null)
                {
                    //if the session is available then bind it and return
                    if (this._connectionObjectsKeeper.Count > (int)counterparty &&
                        this._connectionObjectsKeeper[(int)counterparty] != null)
                    {
                        if(counterparty == Counterparty.L01)
                            _venueSessionToBookAdapters[(int)counterparty] = new MDSessionToPriceBookAdapter(counterparty, SessionsSettings.FIXChannel_LM1Behavior.L01LayersNum);
                        else
                            _venueSessionToBookAdapters[(int)counterparty] = new MDSessionToPriceBookAdapter(counterparty);
                        IntegratorManager.BindMessagingEventsToPriceChangesConsumer(
                            this._logger, this._connectionObjectsKeeper, _venueSessionToBookAdapters[(int)counterparty]);
                    }
                }

                return _venueSessionToBookAdapters[(int)counterparty];
            }
        }

        public IEnumerable<IBookTopProvider<PriceObjectInternal>> AllBookTopProviders
        {
            get
            {
                yield return BankpoolBookTopProvider;

                foreach (HotspotCounterparty hotspotCounterparty in HotspotCounterparty.Values)
                {
                    yield return this.GetBookTopProvider(hotspotCounterparty);
                }

                foreach (
                    MDSessionToPriceBookAdapter mdSessionToPriceBookAdapter in
                        _venueSessionToBookAdapters.Where(o => o != null))
                {
                    yield return mdSessionToPriceBookAdapter;
                }
            }
        }
    }

    internal class VenueDataProviderStoreInternal : IVenueDataProviderStore
    {
        private IList<ITakerConnectionObjectsKeeper> _connectionObjectsKeeper;

        public VenueDataProviderStoreInternal(IList<ITakerConnectionObjectsKeeper> connectionObjectsKeepers)
        {
            this._connectionObjectsKeeper = connectionObjectsKeepers;
        }

        public IVenueDataProvider GetVenueDataProvider(Counterparty counterparty)
        {
            if (counterparty == Counterparty.NULL || counterparty.TradingTargetType == TradingTargetType.BankPool)
            {
                return null;
            }
            else if (counterparty.TradingTargetType == TradingTargetType.Hotspot)
            {
                if (this._connectionObjectsKeeper.Count > (int)counterparty &&
                    this._connectionObjectsKeeper[(int)counterparty] != null &&
                    this._connectionObjectsKeeper[(int)counterparty].MarketDataSession != null &&
                    (this._connectionObjectsKeeper[(int)counterparty].MarketDataSession is HotspotMarketDataSession))
                {
                    return
                        this._connectionObjectsKeeper[(int)counterparty].MarketDataSession as
                        HotspotMarketDataSession;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        public IEnumerable<IVenueDataProvider> AllVenueDataProviders
        {
            get
            {
                foreach (HotspotCounterparty hotspotCounterparty in HotspotCounterparty.Values)
                {
                    var dataProvider = this.GetVenueDataProvider(hotspotCounterparty);
                    if (dataProvider != null)
                    {
                        yield return dataProvider;
                    }
                }

                yield break;
            }
        }
    }

    internal static class ToBSenderExtension
    {
        //public static void SessionToBPriceChangingEventHandlerAdapter(this ToBSender toBSender, IBookTop<PriceObjectInternal> sender, PriceObjectInternal price,
        //                                                       DateTime timeStamp)
        //{
        //    toBSender.SendPrice(price);
        //}

        public static void SendPrice(this ToBSender toBSender, PriceObjectInternal price, DateTime priceSignalSentUtc)
        {
            if (price == null)
                return;
            try
            {
                ulong atomicValue = PriceObjectInternal.IsNullPrice(price)
                    ? 0
                    : DecimalUtils.ConvertPriceToAtomicValue(price.Price, price.SizeBaseAbsInitial);

                long offset = ToBconverter.GetOffset(price.Symbol, price.Side, price.Counterparty);

                toBSender.SendConvertedPrice(offset, atomicValue, priceSignalSentUtc, price.Counterparty);
            }
            catch (TobConverterException tobConverterException)
            {
                toBSender.Logger.LogException(LogLevel.Fatal, tobConverterException,
                    "ToBSender receiving price that cannot be converted to atomic value. Price: {0}", price);
            }
            catch (DecimalUtilsException decimalUtilsException)
            {
                toBSender.Logger.LogException(LogLevel.Fatal, decimalUtilsException,
                    "ToBSender receiving price that cannot be converted to atomic value. Price: {0}", price);
            }
            
        }
    }
}
