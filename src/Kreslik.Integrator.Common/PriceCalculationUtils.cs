﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public static class PriceCalculationUtils
    {
        public static decimal BetterPrice(decimal priceA, decimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return Math.Min(priceA, priceB);
            else
                return Math.Max(priceA, priceB);
        }

        public static bool IsBetterPrice(this decimal priceA, decimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA < priceB;
            else
                return priceA > priceB;
        }

        public static bool IsBetterPrice(this decimal priceA, decimal priceB, PriceSide side)
        {
            if (side == PriceSide.Ask)
                return priceA < priceB;
            else
                return priceA > priceB;
        }

        public static bool IsBetterOrEqualPrice(this decimal priceA, decimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA <= priceB;
            else
                return priceA >= priceB;
        }

        public static bool IsWorseOrEqualPrice(this decimal priceA, decimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA >= priceB;
            else
                return priceA <= priceB;
        }

        public static decimal WorsePrice(decimal priceA, decimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return Math.Max(priceA, priceB);
            else
                return Math.Min(priceA, priceB);
        }

        public static decimal ImprovePrice(decimal priceA, decimal increment, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA - increment;
            else
                return priceA + increment;
        }

        public static decimal DeterioratePrice(decimal priceA, decimal increment, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA + increment;
            else
                return priceA - increment;
        }
    }
}
