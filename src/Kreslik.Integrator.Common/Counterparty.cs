﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public interface ICounterparty
    { }
    public interface IBankCounterparty : ICounterparty { }
    public interface IHotspotCounterparty : ICounterparty { }
    public interface IFXallCounterparty : ICounterparty { }
    public interface ILmaxCounterparty : ICounterparty { }
    public interface IFXCMCounterparty : ICounterparty { }
    public interface IFXCMMMCounterparty : ICounterparty { }


    public static class CounterpartyExtensions
    {
        public static TradingTargetCategory GetTradingTargetCategory(this TradingTargetType tradingTargetType)
        {
            if (tradingTargetType == TradingTargetType.BankPool)
                return TradingTargetCategory.BankPool;
            else
                return TradingTargetCategory.Venue;
        }

        //public static string[] TradingTargetTypeStrings = new string[]{"BankPool", "HOT", "FXA", "LMX", "FCM"};

        public static string GetTradingTargetStringCode(this Counterparty counterparty)
        {
            //ToBeRemoved once traiana integration is done
            //if (counterparty.TradingTargetType == TradingTargetType.LMAX)
            //    return "LMX";
            //else if (counterparty.TradingTargetType == TradingTargetType.FXCMMM || counterparty.TradingTargetType == TradingTargetType.FXCM)
            //    return "FastMatch";
            //else
            return counterparty.ToString();
        }
    }

    public enum TradingTargetType
    {
        BankPool = 0,
        Hotspot = 1,
        FXall = 2,
        LMAX = 3,
        FXCM = 4,
        FXCMMM = 5,
        ValuesCount = FXCMMM + 1
    }

    public enum TradingTargetCategory
    {
        BankPool = 0,
        Venue = 1
    }

    public enum STPCounterparty
    {
        CTIPB,
        Traiana
    }

    //TBD: Turn those classes into structs (INT-510) - this would however be very tricky due to used inheritance (unsupported for structs)
    // so that would need to be done via casting operators + the specifying types ('BankpoolCounterparty') will likely probably need two internal ints
    public struct Counterparty : ICounterparty
    {
        private readonly int _value;

        private static readonly Dictionary<string, int> _instanceNamesToIds = new Dictionary<string, int>();
        private static readonly List<string> _instanceIdsToNames = new List<string>();
        private static readonly List<Counterparty> _allSymbols = new List<Counterparty>();

        public static readonly Counterparty NULL = new Counterparty("NULLCounterparty");

        public static readonly Counterparty CRS = new Counterparty(EnumValues.CRS);
        public static readonly Counterparty UBS = new Counterparty(EnumValues.UBS);
        public static readonly Counterparty DBK = new Counterparty(EnumValues.DBK);
        public static readonly Counterparty CTI = new Counterparty(EnumValues.CTI);
        public static readonly Counterparty BOA = new Counterparty(EnumValues.BOA);
        public static readonly Counterparty MGS = new Counterparty(EnumValues.MGS);
        public static readonly Counterparty RBS = new Counterparty(EnumValues.RBS);
        public static readonly Counterparty HSB = new Counterparty(EnumValues.HSB);
        public static readonly Counterparty JPM = new Counterparty(EnumValues.JPM);
        public static readonly Counterparty GLS = new Counterparty(EnumValues.GLS);
        public static readonly Counterparty BNP = new Counterparty(EnumValues.BNP);
        public static readonly Counterparty NOM = new Counterparty(EnumValues.NOM);
        public static readonly Counterparty CZB = new Counterparty(EnumValues.CZB);
        public static readonly Counterparty BRX = new Counterparty(EnumValues.BRX);
        public static readonly Counterparty SOC = new Counterparty(EnumValues.SOC);
        public static readonly Counterparty PX1 = new Counterparty(EnumValues.PX1);

        public static readonly Counterparty HTA = new Counterparty(EnumValues.HTA);
        public static readonly Counterparty HTF = new Counterparty(EnumValues.HTF);
        public static readonly Counterparty HT3 = new Counterparty(EnumValues.HT3);
        public static readonly Counterparty H4T = new Counterparty(EnumValues.H4T);
        public static readonly Counterparty H4M = new Counterparty(EnumValues.H4M);

        public static readonly Counterparty FA1 = new Counterparty(EnumValues.FA1);
        public static readonly Counterparty FL1 = new Counterparty(EnumValues.FL1);

        public static readonly Counterparty L01 = new Counterparty(EnumValues.L01);
        public static readonly Counterparty L02 = new Counterparty(EnumValues.L02);
        public static readonly Counterparty L03 = new Counterparty(EnumValues.L03);
        public static readonly Counterparty L04 = new Counterparty(EnumValues.L04);
        public static readonly Counterparty L05 = new Counterparty(EnumValues.L05);
        public static readonly Counterparty L06 = new Counterparty(EnumValues.L06);
        public static readonly Counterparty L07 = new Counterparty(EnumValues.L07);
        public static readonly Counterparty L08 = new Counterparty(EnumValues.L08);
        public static readonly Counterparty L09 = new Counterparty(EnumValues.L09);
        public static readonly Counterparty L10 = new Counterparty(EnumValues.L10);
        public static readonly Counterparty L11 = new Counterparty(EnumValues.L11);
        public static readonly Counterparty L12 = new Counterparty(EnumValues.L12);
        public static readonly Counterparty L13 = new Counterparty(EnumValues.L13);
        public static readonly Counterparty L14 = new Counterparty(EnumValues.L14);
        public static readonly Counterparty L15 = new Counterparty(EnumValues.L15);
        public static readonly Counterparty L16 = new Counterparty(EnumValues.L16);
        public static readonly Counterparty L17 = new Counterparty(EnumValues.L17);
        public static readonly Counterparty L18 = new Counterparty(EnumValues.L18);
        public static readonly Counterparty L19 = new Counterparty(EnumValues.L19);
        public static readonly Counterparty L20 = new Counterparty(EnumValues.L20);
        public static readonly Counterparty LM2 = new Counterparty(EnumValues.LM2);
        public static readonly Counterparty LM3 = new Counterparty(EnumValues.LM3);
        public static readonly Counterparty LX1 = new Counterparty(EnumValues.LX1);
        public static readonly Counterparty LGA = new Counterparty(EnumValues.LGA);
        public static readonly Counterparty LGC = new Counterparty(EnumValues.LGC);

        public static readonly Counterparty FC1 = new Counterparty(EnumValues.FC1);
        public static readonly Counterparty FC2 = new Counterparty(EnumValues.FC2);

        public static readonly Counterparty FS1 = new Counterparty(EnumValues.FS1);
        public static readonly Counterparty FS2 = new Counterparty(EnumValues.FS2);



        public static readonly IEnumerable<Counterparty> OrdersOnlyCounterparties = new Counterparty[] { Counterparty.LX1, Counterparty.LGC, Counterparty.H4M };

        private static int[] counterpartyAlphaOrderRankMappings;

        static Counterparty()
        {
            counterpartyAlphaOrderRankMappings = new int[ValuesCount];
            int i = 0;
            foreach (int rank in _allSymbols.OrderBy(ctp => ctp.ToString()).Select(ctpp => ctpp._value).ToArray())
            {
                counterpartyAlphaOrderRankMappings[rank - 1] = i++;
            }

            _counterpartiesForStreaming = new List<Counterparty>() { Counterparty.FS1, Counterparty.FS2/*, Counterparty.HTA, Counterparty.HT3*/, Counterparty.FL1 };
            _counterpartiesForTaking = _allSymbols.Where(ctp => !_counterpartiesForStreaming.Contains(ctp));
        }

        //Not needed any more
        //private static void VerifyCorrectInitializationOrder()
        //{
        //    foreach (FieldInfo field in typeof(EnumValues).GetFields())
        //    {
        //        if (!_instanceNamesToIds.ContainsKey(field.GetRawConstantValue().ToString()))
        //        {
        //            throw new Exception(
        //                string.Format(
        //                    "Wrong initialization order of Counterparty type detected! Counterparty needs to be acessed before any of its specialization. {0} is not initialized - it's owning parent specialized type was accessed before accessing Counterparty.", field.GetRawConstantValue().ToString()));
        //        }
        //    }
        //}

        public static bool IsCounterpartyForTakeing(Counterparty counterparty)
        {
            return !_counterpartiesForStreaming.Contains(counterparty);
        }

        public static bool IsCounterpartyForStreaming(Counterparty counterparty)
        {
            return _counterpartiesForStreaming.Contains(counterparty);
        }

        private static IList<Counterparty> _counterpartiesForStreaming;
        private static IEnumerable<Counterparty> _counterpartiesForTaking;

        public static IEnumerable<Counterparty> CounterpartiesForStreaming
        {
            get { return _counterpartiesForStreaming; }
        }

        public static IEnumerable<Counterparty> CounterpartiesForTaking
        {
            get { return _counterpartiesForTaking; }
        }

        public int AlphaOrderRank
        {
            get { return counterpartyAlphaOrderRankMappings[this._value - 1]; }
        }

        public class EnumValues
        {
            public const string CRS = @"CRS";
            public const string UBS = @"UBS";
            public const string DBK = @"DBK";
            public const string CTI = @"CTI";
            public const string BOA = @"BOA";
            public const string MGS = @"MGS";
            public const string RBS = @"RBS";
            public const string HSB = @"HSB";
            public const string JPM = @"JPM";
            public const string GLS = @"GLS";
            public const string BNP = @"BNP";
            public const string NOM = @"NOM";
            public const string CZB = @"CZB";
            public const string BRX = @"BRX";
            public const string SOC = @"SOC";
            public const string HTA = @"HTA";
            public const string HTF = @"HTF";
            public const string HT3 = @"HT3";
            public const string H4T = @"H4T";
            public const string H4M = @"H4M";
            public const string FA1 = @"FA1";
            public const string FL1 = @"FL1";
            public const string L01 = @"L01";
            public const string LM2 = @"LM2";
            public const string LM3 = @"LM3";
            public const string LX1 = @"LX1";
            public const string LGA = @"LGA";
            public const string LGC = @"LGC";
            public const string FC1 = @"FC1";
            public const string FC2 = @"FC2";
            public const string L02 = @"L02";
            public const string L03 = @"L03";
            public const string L04 = @"L04";
            public const string L05 = @"L05";
            public const string L06 = @"L06";
            public const string L07 = @"L07";
            public const string L08 = @"L08";
            public const string L09 = @"L09";
            public const string L10 = @"L10";
            public const string L11 = @"L11";
            public const string L12 = @"L12";
            public const string L13 = @"L13";
            public const string L14 = @"L14";
            public const string L15 = @"L15";
            public const string L16 = @"L16";
            public const string L17 = @"L17";
            public const string L18 = @"L18";
            public const string L19 = @"L19";
            public const string L20 = @"L20";
            public const string FS1 = @"FS1";
            public const string FS2 = @"FS2";
            public const string PX1 = @"PX1";

        }

        private Counterparty(string name)
        {
            this._value = _instanceIdsToNames.Count;

            if (_value > 0)
            {
                _instanceNamesToIds.Add(name, this._value);
                _allSymbols.Add(this);
            }
            _instanceIdsToNames.Add(name);
        }

        private Counterparty(int value)
        {
            this._value = value;
        }

        public static implicit operator Counterparty(BankCounterparty counterparty)
        {
            return new Counterparty((int)counterparty + 1);
        }

        public static implicit operator Counterparty(HotspotCounterparty counterparty)
        {
            return new Counterparty((int)counterparty + 1);
        }

        public static implicit operator Counterparty(FXallCounterparty counterparty)
        {
            return new Counterparty((int)counterparty + 1);
        }

        public static implicit operator Counterparty(LmaxCounterparty counterparty)
        {
            return new Counterparty((int)counterparty + 1);
        }

        public static implicit operator Counterparty(FXCMCounterparty counterparty)
        {
            return new Counterparty((int)counterparty + 1);
        }

        public static implicit operator Counterparty(FXCMMMCounterparty counterparty)
        {
            return new Counterparty((int)counterparty + 1);
        }


        public static explicit operator Counterparty(string name)
        {
            int code;
            if (_instanceNamesToIds.TryGetValue(name, out code))
            {
                return new Counterparty(code);
            }
            else
            {
                throw new InvalidCastException("Unknown Counterpart value: " + name);
            }
        }

        public static explicit operator Counterparty(int value)
        {
            if (_instanceIdsToNames.Count - 1 > value)
            {
                return new Counterparty(value + 1);
            }
            else
            {
                throw new InvalidCastException("Unknown Counterpart integer value: " + value);
            }
        }

        public static bool TryParse(string name, out Counterparty counterparty)
        {
            int code;
            if (_instanceNamesToIds.TryGetValue(name, out code))
            {
                counterparty = new Counterparty(code);
                return true;
            }
            else
            {
                counterparty = Counterparty.NULL;
                return false;
            }
        }

        public static explicit operator String(Counterparty counterparty)
        {
            return _instanceIdsToNames[counterparty._value];
        }

        public static explicit operator int(Counterparty counterparty)
        {
            return counterparty._value - 1;
        }

        public static IEnumerable<string> StringValues
        {
            get { return _instanceNamesToIds.Keys; }
        }

        public static IEnumerable<Counterparty> Values
        {
            get { return _allSymbols; }
        }

        public static int ValuesCount
        {
            get { return _instanceIdsToNames.Count - 1; }
        }

        public override string ToString()
        {
            return _instanceIdsToNames[this._value];
        }

        public TradingTargetType TradingTargetType
        {
            get
            {
                if (BankCounterparty.IsBankCounterparty(this))
                {
                    return TradingTargetType.BankPool;
                }
                else if (HotspotCounterparty.IsHotspotCounterparty(this))
                {
                    return TradingTargetType.Hotspot;
                }
                else if (FXallCounterparty.IsFXallCounterparty(this))
                {
                    return TradingTargetType.FXall;
                }
                else if (LmaxCounterparty.IsLmaxCounterparty(this))
                {
                    return TradingTargetType.LMAX;
                }
                else if (FXCMCounterparty.IsFXCMCounterparty(this))
                {
                    return TradingTargetType.FXCM;
                }
                else if (FXCMMMCounterparty.IsFXCMMMCounterparty(this))
                {
                    return TradingTargetType.FXCMMM;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("counterparty",
                                                          "Unknown type of counterparty - cannot convert it to TradingTargetType");
                }
            }
        }

        public TradingTargetCategory TradingTargetCategory
        {
            get
            {
                if (BankCounterparty.IsBankCounterparty(this))
                {
                    return TradingTargetCategory.BankPool;
                }
                else
                {
                    return TradingTargetCategory.Venue;
                }
            }
        }

        public static bool operator ==(Counterparty a, Counterparty b)
        {
            return a._value == b._value;
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator ==(Counterparty a, Counterparty? b)
        {
            throw new NotImplementedException();
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator !=(Counterparty a, Counterparty? b)
        {
            throw new NotImplementedException();
        }

        public static bool operator !=(Counterparty a, Counterparty b)
        {
            return a._value != b._value;
        }

        private static Counterparty UnboxToCpt(object obj)
        {
            if (obj is Counterparty)
                return (Counterparty)obj;
            if (obj is BankCounterparty)
                return (BankCounterparty)obj;
            if (obj is HotspotCounterparty)
                return (HotspotCounterparty)obj;
            if (obj is LmaxCounterparty)
                return (LmaxCounterparty)obj;
            return Counterparty.NULL;
        }

        public static Counterparty UnboxInterfaceToCounterparty(ICounterparty cpt)
        {
            return UnboxToCpt(cpt);
        }

        public static ICounterparty BoxCounterpartyToInterface(Counterparty cpt)
        {
            switch (cpt.TradingTargetType)
            {
                case TradingTargetType.BankPool:
                    return (IBankCounterparty)(BankCounterparty)cpt;
                case TradingTargetType.Hotspot:
                    return (IHotspotCounterparty)(HotspotCounterparty)cpt;
                case TradingTargetType.FXall:
                    return (IFXallCounterparty)(FXallCounterparty)cpt;
                case TradingTargetType.LMAX:
                    return (ILmaxCounterparty)(LmaxCounterparty)cpt;
                case TradingTargetType.FXCM:
                    return (IFXCMCounterparty)(FXCMCounterparty)cpt;
                case TradingTargetType.FXCMMM:
                    return (IFXCMMMCounterparty)(FXCMMMCounterparty)cpt;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static bool Equals(Counterparty cpt, object obj)
        {
            if (obj == null)
                return false;

            return cpt._value != 0 && UnboxToCpt(obj)._value == cpt._value;
        }

        public override bool Equals(object obj)
        {
            return Counterparty.Equals(this, obj);
        }

        public override int GetHashCode()
        {
            return this._value;
        }
    }



    public struct HotspotCounterparty : IHotspotCounterparty
    {
        private static int _beginValue;
        private static int _endValue;
        private static List<string> _names = new List<string>();
        private static List<HotspotCounterparty> _allCounterparties = new List<HotspotCounterparty>();
        private readonly int _value;

        public static readonly HotspotCounterparty HTA;
        public static readonly HotspotCounterparty HTF;
        public static readonly HotspotCounterparty HT3;
        public static readonly HotspotCounterparty H4T;
        public static readonly HotspotCounterparty H4M;

        static HotspotCounterparty()
        {
            _beginValue = (int)Counterparty.HTA;
            HTA = new HotspotCounterparty((int)Counterparty.HTA + 1);
            HTF = new HotspotCounterparty((int)Counterparty.HTF + 1);
            HT3 = new HotspotCounterparty((int)Counterparty.HT3 + 1);
            H4T = new HotspotCounterparty((int)Counterparty.H4T + 1);
            H4M = new HotspotCounterparty((int)Counterparty.H4M + 1);
            _endValue = (int)Counterparty.H4M;

            for (int idx = _beginValue; idx <= _endValue; idx++)
            {
                _names.Add((string)(Counterparty)idx);
                _allCounterparties.Add((HotspotCounterparty)(Counterparty)idx);
            }
        }

        private HotspotCounterparty(int value)
        {
            this._value = value;
        }

        public static explicit operator HotspotCounterparty(Counterparty counterparty)
        {
            int code = (int)counterparty;
            if (code >= _beginValue && code <= _endValue)
                return new HotspotCounterparty(code + 1);
            else
                throw new InvalidCastException("Unknown HotspotCounterparty value (is a different counterparty): " + counterparty);
        }

        public static bool IsHotspotCounterparty(Counterparty counterparty)
        {
            int code = (int)counterparty;
            return code >= _beginValue && code <= _endValue;
        }

        public static explicit operator HotspotCounterparty(string name)
        {
            return (HotspotCounterparty)(Counterparty)name;
        }

        public static new IEnumerable<string> StringValues
        {
            get { return _names; }
        }

        public static new IEnumerable<HotspotCounterparty> Values
        {
            get { return _allCounterparties; }
        }

        public static new int ValuesCount
        {
            get { return _names.Count; }
        }

        public static explicit operator String(HotspotCounterparty counterparty)
        {
            return (string)(Counterparty)counterparty;
        }

        public static explicit operator int(HotspotCounterparty counterparty)
        {
            return counterparty._value - 1;
        }

        public override string ToString()
        {
            return (string)(Counterparty)this;
        }

        public static bool operator ==(HotspotCounterparty a, HotspotCounterparty b)
        {
            return a._value == b._value;
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator ==(HotspotCounterparty a, HotspotCounterparty? b)
        {
            throw new NotImplementedException();
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator !=(HotspotCounterparty a, HotspotCounterparty? b)
        {
            throw new NotImplementedException();
        }

        public static bool operator !=(HotspotCounterparty a, HotspotCounterparty b)
        {
            return a._value != b._value;
        }

        public override bool Equals(object obj)
        {
            return Counterparty.Equals(this, obj);
        }

        public override int GetHashCode()
        {
            return this._value;
        }
    }

    public struct LmaxCounterparty : ILmaxCounterparty
    {
        private static int _beginValue;
        private static int _endValue;
        private static List<string> _names = new List<string>();
        private static List<LmaxCounterparty> _allCounterparties = new List<LmaxCounterparty>();
        private readonly int _value;

        public static readonly LmaxCounterparty L01;
        public static readonly LmaxCounterparty L02;
        public static readonly LmaxCounterparty L03;
        public static readonly LmaxCounterparty L04;
        public static readonly LmaxCounterparty L05;
        public static readonly LmaxCounterparty L06;
        public static readonly LmaxCounterparty L07;
        public static readonly LmaxCounterparty L08;
        public static readonly LmaxCounterparty L09;
        public static readonly LmaxCounterparty L10;
        public static readonly LmaxCounterparty L11;
        public static readonly LmaxCounterparty L12;
        public static readonly LmaxCounterparty L13;
        public static readonly LmaxCounterparty L14;
        public static readonly LmaxCounterparty L15;
        public static readonly LmaxCounterparty L16;
        public static readonly LmaxCounterparty L17;
        public static readonly LmaxCounterparty L18;
        public static readonly LmaxCounterparty L19;
        public static readonly LmaxCounterparty L20;
        public static readonly LmaxCounterparty LM2;
        public static readonly LmaxCounterparty LM3;
        public static readonly LmaxCounterparty LX1;
        public static readonly LmaxCounterparty LGA;
        public static readonly LmaxCounterparty LGC;

        private static int _sublayersStart, _sublayersEnd;


        static LmaxCounterparty()
        {
            _beginValue = (int)Counterparty.L01;
            L01 = new LmaxCounterparty((int)Counterparty.L01 + 1);
            L02 = new LmaxCounterparty((int)Counterparty.L02 + 1);
            L03 = new LmaxCounterparty((int)Counterparty.L03 + 1);
            L04 = new LmaxCounterparty((int)Counterparty.L04 + 1);
            L05 = new LmaxCounterparty((int)Counterparty.L05 + 1);
            L06 = new LmaxCounterparty((int)Counterparty.L06 + 1);
            L07 = new LmaxCounterparty((int)Counterparty.L07 + 1);
            L08 = new LmaxCounterparty((int)Counterparty.L08 + 1);
            L09 = new LmaxCounterparty((int)Counterparty.L09 + 1);
            L10 = new LmaxCounterparty((int)Counterparty.L10 + 1);
            L11 = new LmaxCounterparty((int)Counterparty.L11 + 1);
            L12 = new LmaxCounterparty((int)Counterparty.L12 + 1);
            L13 = new LmaxCounterparty((int)Counterparty.L13 + 1);
            L14 = new LmaxCounterparty((int)Counterparty.L14 + 1);
            L15 = new LmaxCounterparty((int)Counterparty.L15 + 1);
            L16 = new LmaxCounterparty((int)Counterparty.L16 + 1);
            L17 = new LmaxCounterparty((int)Counterparty.L17 + 1);
            L18 = new LmaxCounterparty((int)Counterparty.L18 + 1);
            L19 = new LmaxCounterparty((int)Counterparty.L19 + 1);
            L20 = new LmaxCounterparty((int)Counterparty.L20 + 1);
            LM2 = new LmaxCounterparty((int)Counterparty.LM2 + 1);
            LM3 = new LmaxCounterparty((int)Counterparty.LM3 + 1);
            LX1 = new LmaxCounterparty((int)Counterparty.LX1 + 1);
            LGA = new LmaxCounterparty((int)Counterparty.LGA + 1);
            LGC = new LmaxCounterparty((int)Counterparty.LGC + 1);
            _endValue = (int)Counterparty.LGC;

            _sublayersStart = (int)Counterparty.L02;
            _sublayersEnd = (int)Counterparty.L20;

            for (int idx = _beginValue; idx <= _endValue; idx++)
            {
                _names.Add((string)(Counterparty)idx);
                _allCounterparties.Add((LmaxCounterparty)(Counterparty)idx);
            }

            LayersOfLM1 = new Counterparty[]
            {
                L01, L02, L03, L04, L05, L06, L07, L08, L09, L10, L11, L12, L13, L14, L15, L16, L17, L18, L19, L20
            };
        }

        public static Counterparty[] LayersOfLM1 { get; private set; }

        public static bool IsItSubLayerPseoudoCounterparty(Counterparty counterparty)
        {
            return (int)counterparty >= _sublayersStart && (int)counterparty <= _sublayersEnd;
        }

        private LmaxCounterparty(int value)
        {
            this._value = value;
        }

        public bool IsSubLayerPseoudoCounterparty { get { return IsItSubLayerPseoudoCounterparty((Counterparty)this); } }

        public static explicit operator LmaxCounterparty(Counterparty counterparty)
        {
            int code = (int)counterparty;
            if (code >= _beginValue && code <= _endValue)
                return new LmaxCounterparty(code + 1);
            else
                throw new InvalidCastException("Unknown LmaxCounterparty value (is a different counterparty): " + counterparty);
        }

        public static bool IsLmaxCounterparty(Counterparty counterparty)
        {
            int code = (int)counterparty;
            return code >= _beginValue && code <= _endValue;
        }

        public static explicit operator LmaxCounterparty(string name)
        {
            return (LmaxCounterparty)(Counterparty)name;
        }

        public static new IEnumerable<string> StringValues
        {
            get { return _names; }
        }

        public static new IEnumerable<LmaxCounterparty> Values
        {
            get { return _allCounterparties; }
        }

        public static new int ValuesCount
        {
            get { return _names.Count; }
        }

        public int OrdinalValueWithinSubcategory
        {
            get { return ((int)(Counterparty)this) - _beginValue; }
        }

        public static explicit operator String(LmaxCounterparty counterparty)
        {
            return (string)(Counterparty)counterparty;
        }

        public static explicit operator int(LmaxCounterparty counterparty)
        {
            return counterparty._value - 1;
        }

        public override string ToString()
        {
            return (string)(Counterparty)this;
        }

        public static bool operator ==(LmaxCounterparty a, LmaxCounterparty b)
        {
            return a._value == b._value;
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator ==(LmaxCounterparty a, LmaxCounterparty? b)
        {
            throw new NotImplementedException();
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator !=(LmaxCounterparty a, LmaxCounterparty? b)
        {
            throw new NotImplementedException();
        }

        public static bool operator !=(LmaxCounterparty a, LmaxCounterparty b)
        {
            return a._value != b._value;
        }

        public override bool Equals(object obj)
        {
            return Counterparty.Equals(this, obj);
        }

        public override int GetHashCode()
        {
            return this._value;
        }
    }

    public struct FXallCounterparty : IFXallCounterparty
    {
        private static int _beginValue;
        private static int _endValue;
        private static List<string> _names = new List<string>();
        private static List<FXallCounterparty> _allCounterparties = new List<FXallCounterparty>();
        private readonly int _value;

        public static readonly FXallCounterparty FA1;
        public static readonly FXallCounterparty FL1;


        static FXallCounterparty()
        {
            _beginValue = (int)Counterparty.FA1;
            FA1 = new FXallCounterparty((int)Counterparty.FA1 + 1);
            FL1 = new FXallCounterparty((int)Counterparty.FL1 + 1);
            _endValue = (int)Counterparty.FL1;

            for (int idx = _beginValue; idx <= _endValue; idx++)
            {
                _names.Add((string)(Counterparty)idx);
                _allCounterparties.Add((FXallCounterparty)(Counterparty)idx);
            }
        }

        private FXallCounterparty(int value)
        {
            this._value = value;
        }

        public static explicit operator FXallCounterparty(Counterparty counterparty)
        {
            int code = (int)counterparty;
            if (code >= _beginValue && code <= _endValue)
                return new FXallCounterparty(code + 1);
            else
                throw new InvalidCastException("Unknown FXallCounterparty value (is a different counterparty): " + counterparty);
        }

        public static bool IsFXallCounterparty(Counterparty counterparty)
        {
            int code = (int)counterparty;
            return code >= _beginValue && code <= _endValue;
        }

        public static explicit operator FXallCounterparty(string name)
        {
            return (FXallCounterparty)(Counterparty)name;
        }

        public static new IEnumerable<string> StringValues
        {
            get { return _names; }
        }

        public static new IEnumerable<FXallCounterparty> Values
        {
            get { return _allCounterparties; }
        }

        public static new int ValuesCount
        {
            get { return _names.Count; }
        }

        public static explicit operator String(FXallCounterparty counterparty)
        {
            return (string)(Counterparty)counterparty;
        }

        public static explicit operator int(FXallCounterparty counterparty)
        {
            return counterparty._value - 1;
        }

        public override string ToString()
        {
            return (string)(Counterparty)this;
        }

        public static bool operator ==(FXallCounterparty a, FXallCounterparty b)
        {
            return a._value == b._value;
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator ==(FXallCounterparty a, FXallCounterparty? b)
        {
            throw new NotImplementedException();
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator !=(FXallCounterparty a, FXallCounterparty? b)
        {
            throw new NotImplementedException();
        }

        public static bool operator !=(FXallCounterparty a, FXallCounterparty b)
        {
            return a._value != b._value;
        }

        public override bool Equals(object obj)
        {
            return Counterparty.Equals(this, obj);
        }

        public override int GetHashCode()
        {
            return this._value;
        }
    }

    public struct FXCMCounterparty : IFXCMCounterparty
    {
        private static int _beginValue;
        private static int _endValue;
        private static List<string> _names = new List<string>();
        private static List<FXCMCounterparty> _allCounterparties = new List<FXCMCounterparty>();
        private readonly int _value;

        public static readonly FXCMCounterparty FC1;
        public static readonly FXCMCounterparty FC2;

        static FXCMCounterparty()
        {
            _beginValue = (int)Counterparty.FC1;
            FC1 = new FXCMCounterparty((int)Counterparty.FC1 + 1);
            FC2 = new FXCMCounterparty((int)Counterparty.FC2 + 1);
            _endValue = (int)Counterparty.FC2;

            for (int idx = _beginValue; idx <= _endValue; idx++)
            {
                _names.Add((string)(Counterparty)idx);
                _allCounterparties.Add((FXCMCounterparty)(Counterparty)idx);
            }
        }

        private FXCMCounterparty(int value)
        {
            this._value = value;
        }

        public static explicit operator FXCMCounterparty(Counterparty counterparty)
        {
            int code = (int)counterparty;
            if (code >= _beginValue && code <= _endValue)
                return new FXCMCounterparty(code + 1);
            else
                throw new InvalidCastException("Unknown FXCMCounterparty value (is a different counterparty): " + counterparty);
        }

        public static bool IsFXCMCounterparty(Counterparty counterparty)
        {
            int code = (int)counterparty;
            return code >= _beginValue && code <= _endValue;
        }

        public static explicit operator FXCMCounterparty(string name)
        {
            return (FXCMCounterparty)(Counterparty)name;
        }

        public static new IEnumerable<string> StringValues
        {
            get { return _names; }
        }

        public static new IEnumerable<FXCMCounterparty> Values
        {
            get { return _allCounterparties; }
        }

        public static new int ValuesCount
        {
            get { return _names.Count; }
        }

        public static explicit operator String(FXCMCounterparty counterparty)
        {
            return (string)(Counterparty)counterparty;
        }

        public static explicit operator int(FXCMCounterparty counterparty)
        {
            return counterparty._value - 1;
        }

        public override string ToString()
        {
            return (string)(Counterparty)this;
        }

        public static bool operator ==(FXCMCounterparty a, FXCMCounterparty b)
        {
            return a._value == b._value;
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator ==(FXCMCounterparty a, FXCMCounterparty? b)
        {
            throw new NotImplementedException();
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator !=(FXCMCounterparty a, FXCMCounterparty? b)
        {
            throw new NotImplementedException();
        }

        public static bool operator !=(FXCMCounterparty a, FXCMCounterparty b)
        {
            return a._value != b._value;
        }

        public override bool Equals(object obj)
        {
            return Counterparty.Equals(this, obj);
        }

        public override int GetHashCode()
        {
            return this._value;
        }
    }

    public struct FXCMMMCounterparty : IFXCMMMCounterparty
    {
        private static int _beginValue;
        private static int _endValue;
        private static List<string> _names = new List<string>();
        private static List<FXCMMMCounterparty> _allCounterparties = new List<FXCMMMCounterparty>();
        private readonly int _value;

        public static readonly FXCMMMCounterparty FS1;
        public static readonly FXCMMMCounterparty FS2;

        static FXCMMMCounterparty()
        {
            _beginValue = (int)Counterparty.FS1;
            FS1 = new FXCMMMCounterparty((int)Counterparty.FS1 + 1);
            FS2 = new FXCMMMCounterparty((int)Counterparty.FS2 + 1);
            _endValue = (int)Counterparty.FS2;

            for (int idx = _beginValue; idx <= _endValue; idx++)
            {
                _names.Add((string)(Counterparty)idx);
                _allCounterparties.Add((FXCMMMCounterparty)(Counterparty)idx);
            }
        }

        private FXCMMMCounterparty(int value)
        {
            this._value = value;
        }

        public static explicit operator FXCMMMCounterparty(Counterparty counterparty)
        {
            int code = (int)counterparty;
            if (code >= _beginValue && code <= _endValue)
                return new FXCMMMCounterparty(code + 1);
            else
                throw new InvalidCastException("Unknown FXCMMMCounterparty value (is a different counterparty): " + counterparty);
        }

        public static bool IsFXCMMMCounterparty(Counterparty counterparty)
        {
            int code = (int)counterparty;
            return code >= _beginValue && code <= _endValue;
        }

        public static explicit operator FXCMMMCounterparty(string name)
        {
            return (FXCMMMCounterparty)(Counterparty)name;
        }

        public static new IEnumerable<string> StringValues
        {
            get { return _names; }
        }

        public static new IEnumerable<FXCMMMCounterparty> Values
        {
            get { return _allCounterparties; }
        }

        public static new int ValuesCount
        {
            get { return _names.Count; }
        }

        public static explicit operator String(FXCMMMCounterparty counterparty)
        {
            return (string)(Counterparty)counterparty;
        }

        public static explicit operator int(FXCMMMCounterparty counterparty)
        {
            return counterparty._value - 1;
        }

        public override string ToString()
        {
            return (string)(Counterparty)this;
        }

        public static bool operator ==(FXCMMMCounterparty a, FXCMMMCounterparty b)
        {
            return a._value == b._value;
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator ==(FXCMMMCounterparty a, FXCMMMCounterparty? b)
        {
            throw new NotImplementedException();
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator !=(FXCMMMCounterparty a, FXCMMMCounterparty? b)
        {
            throw new NotImplementedException();
        }

        public static bool operator !=(FXCMMMCounterparty a, FXCMMMCounterparty b)
        {
            return a._value != b._value;
        }

        public override bool Equals(object obj)
        {
            return Counterparty.Equals(this, obj);
        }

        public override int GetHashCode()
        {
            return this._value;
        }
    }

    public struct BankCounterparty : IBankCounterparty
    {
        private static int _beginValue;
        private static int _endValue;
        private static List<string> _names = new List<string>();
        private static List<BankCounterparty> _allCounterparties = new List<BankCounterparty>();
        private readonly int _value;

        public static readonly BankCounterparty CRS;
        public static readonly BankCounterparty UBS;
        public static readonly BankCounterparty DBK;
        public static readonly BankCounterparty CTI;
        public static readonly BankCounterparty BOA;
        public static readonly BankCounterparty MGS;
        public static readonly BankCounterparty RBS;
        public static readonly BankCounterparty HSB;
        public static readonly BankCounterparty JPM;
        public static readonly BankCounterparty GLS;
        public static readonly BankCounterparty BNP;
        public static readonly BankCounterparty NOM;
        public static readonly BankCounterparty CZB;
        public static readonly BankCounterparty BRX;
        public static readonly BankCounterparty SOC;
        public static readonly BankCounterparty PX1;

        static BankCounterparty()
        {
            _beginValue = (int)Counterparty.CRS;
            CRS = new BankCounterparty((int)Counterparty.CRS + 1);
            UBS = new BankCounterparty((int)Counterparty.UBS + 1);
            DBK = new BankCounterparty((int)Counterparty.DBK + 1);
            CTI = new BankCounterparty((int)Counterparty.CTI + 1);
            BOA = new BankCounterparty((int)Counterparty.BOA + 1);
            MGS = new BankCounterparty((int)Counterparty.MGS + 1);
            RBS = new BankCounterparty((int)Counterparty.RBS + 1);
            HSB = new BankCounterparty((int)Counterparty.HSB + 1);
            JPM = new BankCounterparty((int)Counterparty.JPM + 1);
            GLS = new BankCounterparty((int)Counterparty.GLS + 1);
            BNP = new BankCounterparty((int)Counterparty.BNP + 1);
            NOM = new BankCounterparty((int)Counterparty.NOM + 1);
            CZB = new BankCounterparty((int)Counterparty.CZB + 1);
            BRX = new BankCounterparty((int)Counterparty.BRX + 1);
            SOC = new BankCounterparty((int)Counterparty.SOC + 1);
            PX1 = new BankCounterparty((int)Counterparty.PX1 + 1);
            _endValue = (int)Counterparty.PX1;

            for (int idx = _beginValue; idx <= _endValue; idx++)
            {
                _names.Add((string)(Counterparty)idx);
                _allCounterparties.Add((BankCounterparty)(Counterparty)idx);
            }
        }

        private BankCounterparty(int value)
        {
            this._value = value;
        }

        public static explicit operator BankCounterparty(Counterparty counterparty)
        {
            int code = (int)counterparty;
            if (code >= _beginValue && code <= _endValue)
                return new BankCounterparty(code + 1);
            else
                throw new InvalidCastException("Unknown BankCounterparty value (is a different counterparty): " + counterparty);
        }

        public static bool IsBankCounterparty(Counterparty counterparty)
        {
            int code = (int)counterparty;
            return code >= _beginValue && code <= _endValue;
        }

        public static explicit operator BankCounterparty(string name)
        {
            return (BankCounterparty)(Counterparty)name;
        }

        public static new IEnumerable<string> StringValues
        {
            get { return _names; }
        }

        public static new IEnumerable<BankCounterparty> Values
        {
            get { return _allCounterparties; }
        }

        public static new int ValuesCount
        {
            get { return _names.Count; }
        }

        public static explicit operator String(BankCounterparty counterparty)
        {
            return (string)(Counterparty)counterparty;
        }

        public static explicit operator int(BankCounterparty counterparty)
        {
            return counterparty._value - 1;
        }

        public override string ToString()
        {
            return (string)(Counterparty)this;
        }

        public static bool operator ==(BankCounterparty a, BankCounterparty b)
        {
            return a._value == b._value;
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator ==(BankCounterparty a, BankCounterparty? b)
        {
            throw new NotImplementedException();
        }

        [Obsolete("Comparison to null is disallowed, compare to Counterparty.NULL instead", true)]
        public static bool operator !=(BankCounterparty a, BankCounterparty? b)
        {
            throw new NotImplementedException();
        }

        public static bool operator !=(BankCounterparty a, BankCounterparty b)
        {
            return a._value != b._value;
        }

        public override bool Equals(object obj)
        {
            return Counterparty.Equals(this, obj);
        }

        public override int GetHashCode()
        {
            return this._value;
        }
    }
}
