﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public class EventsRateCheckerEx : EventsRateChecker
    {
        private ILogger _logger;
        private TimeSpan _logMaxOncePerInterval;
        private DateTime _lastLogged;
        private string _messageToLog;

        public EventsRateCheckerEx(TimeSpan timeIntervalToCheck, int maximumAllowedEventsPerInterval, ILogger logger,
            TimeSpan logMaxOncePerInterval, string messageToLog)
            : base(timeIntervalToCheck, maximumAllowedEventsPerInterval)
        {
            this._logger = logger;
            this._logMaxOncePerInterval = logMaxOncePerInterval;
            this._messageToLog = messageToLog;
        }

        protected override void OnRateExceeded(DateTime utcNow)
        {
            if (_lastLogged + _logMaxOncePerInterval < utcNow)
            {
                _lastLogged = utcNow;
                _logger.Log(LogLevel.Fatal, "Exceeded rate of {0} [{1} per {2}] (reported just once per {3})", this._messageToLog,
                    this.MaximumAllowedEventsPerInterval, this.TimeIntervalToCheck, this._logMaxOncePerInterval);
            }
        }
    }

    public class EventsRateChecker
    {
        private readonly TimeSpan _timeIntervalToCheck;
        private readonly int _maximumAllowedEventsPerInterval;
        private readonly Queue<DateTime> _events = new Queue<DateTime>();
        private SpinLock _spinLock = new SpinLock(false);

        public int MaximumAllowedEventsPerInterval
        {
            get { return this._maximumAllowedEventsPerInterval; }
        }

        public TimeSpan TimeIntervalToCheck
        {
            get { return this._timeIntervalToCheck; }
        }

        public EventsRateChecker(TimeSpan timeIntervalToCheck, int maximumAllowedEventsPerInterval)
        {
            this._timeIntervalToCheck = timeIntervalToCheck;
            this._maximumAllowedEventsPerInterval = maximumAllowedEventsPerInterval;
        }

        protected virtual void OnRateExceeded(DateTime utcNow){}

        public bool AddNextEventAndCheckIsAllowed()
        {
            if(_timeIntervalToCheck == TimeSpan.Zero && _maximumAllowedEventsPerInterval == 0)
                return true;

            DateTime now = HighResolutionDateTime.UtcNow;
            bool isAllowed = true;

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            _events.Enqueue(now);
            if (_events.Count > _maximumAllowedEventsPerInterval)
            {
                isAllowed = _events.Dequeue() + _timeIntervalToCheck < now;
                if (!isAllowed)
                    OnRateExceeded(now);
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            return isAllowed;
        }

        public void Reset()
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            _events.Clear();

            if (lockTaken)
            {
                _spinLock.Exit();
            }
        }
    }

    public class SingleThreadedEventsRateChecker
    {
        private TimeSpan _timeIntervalToCheck;
        private int _maximumAllowedEventsPerInterval;
        Queue<DateTime> _events = new Queue<DateTime>();

        public SingleThreadedEventsRateChecker(TimeSpan timeIntervalToCheck, int maximumAllowedEventsPerInterval)
        {
            this._timeIntervalToCheck = timeIntervalToCheck;
            this._maximumAllowedEventsPerInterval = maximumAllowedEventsPerInterval;
        }

        public int MaximumAllowedEventsPerInterval
        {
            get { return this._maximumAllowedEventsPerInterval; }
        }

        public TimeSpan TimeIntervalToCheck
        {
            get { return this._timeIntervalToCheck; }
        }

        public bool AddNextEventAndCheckIsAllowed()
        {
            if (_timeIntervalToCheck == TimeSpan.Zero && _maximumAllowedEventsPerInterval == 0)
                return true;

            DateTime now = DateTime.UtcNow;
            bool isAllowed = true;

            _events.Enqueue(now);
            if (_events.Count > _maximumAllowedEventsPerInterval)
            {
                isAllowed = _events.Dequeue() + _timeIntervalToCheck < now;
            }

            return isAllowed;
        }

        public void Reset()
        {
            _events.Clear();
        }
    }


    public class RemovableEventsRateChecker
    {
        private TimeSpan _timeIntervalToCheck;
        private int _maximumAllowedEventsPerInterval;
        Queue<DateTime> _events = new Queue<DateTime>();
        SpinLock _spinLock = new SpinLock(false);
        private DateTime _lastEvent;
        private bool _lastEventRemoved;

        public int MaximumAllowedEventsPerInterval
        {
            get { return this._maximumAllowedEventsPerInterval; }
        }

        public TimeSpan TimeIntervalToCheck
        {
            get { return this._timeIntervalToCheck; }
        }

        public RemovableEventsRateChecker(TimeSpan timeIntervalToCheck, int maximumAllowedEventsPerInterval)
        {
            this._timeIntervalToCheck = timeIntervalToCheck;
            this._maximumAllowedEventsPerInterval = maximumAllowedEventsPerInterval;
            this._lastEvent = DateTime.MinValue;
            this._lastEventRemoved = true;
        }

        public bool AddNextEventAndCheckIsAllowed()
        {
            if (_timeIntervalToCheck == TimeSpan.Zero && _maximumAllowedEventsPerInterval == 0)
                return true;

            DateTime now = DateTime.UtcNow;
            bool isAllowed = true;

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            if (!_lastEventRemoved)
            {
                _events.Enqueue(_lastEvent);
            }
            _lastEvent = now;
            _lastEventRemoved = false;
            if (_events.Count + 1 > _maximumAllowedEventsPerInterval)
            {
                isAllowed = _events.Dequeue() + _timeIntervalToCheck < now;
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            return isAllowed;
        }

        public void Reset()
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            _events.Clear();

            if (lockTaken)
            {
                _spinLock.Exit();
            }
        }

        public void FlagLastEventAsRemoved()
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            _lastEventRemoved = true;

            if (lockTaken)
            {
                _spinLock.Exit();
            }
        }
    }
}
