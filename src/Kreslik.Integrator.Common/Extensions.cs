﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public static class Extensions
    {
        /// <summary>
        /// Removes all trialing zeros after last significant digit after decimal separator
        ///  Or after decimal separator itself (if there are no decimal places)
        /// </summary>
        /// <param name="value">Number to be normalized</param>
        /// <returns>Normalized result</returns>
        public static decimal Normalize(this decimal value)
        {
            return value / 1.000000000000000000000000000000000m;
        }

        public static decimal? Normalize(this decimal? value)
        {
            return value.HasValue ? value.Value.Normalize() : value;
        }

        /// <summary>
        /// Gets the number of significant digits after the fraction point (in decimal base)
        ///  if normalizeFirst is false then the function is about 5 times faster, but explicit
        ///  zeros after fraction point are also considered signficant. So
        ///  1.25m.NumberOfSignificantFractionDecimals(true) == 1.2500m.NumberOfSignificantFractionDecimals(true)
        ///  but
        ///  1.25m.NumberOfSignificantFractionDecimals(false) != 1.2500m.NumberOfSignificantFractionDecimals(false)
        /// </summary>
        /// <param name="value">Value to inspect</param>
        /// <param name="normalizeFirst">Indicates whether trialing zeros after fraction point should be removed first. (opertes ~5-times slower if specified)</param>
        /// <returns>Number of significant digits after the fraction point (in decimal base)</returns>
        public static int NumberOfSignificantFractionDecimals(this decimal value, bool normalizeFirst)
        {
            var b = decimal.GetBits(normalizeFirst ? value.Normalize() : value);
            return ((b[3] >> 16) & 0x7fff);
        }

        /// <summary>
        /// Indicates whether date is a business day (doesn't consider holidays)
        /// </summary>
        /// <param name="date">date to check</param>
        /// <returns>true if date is a business day (Monday-Friday), false otherwise (Saturday, Sunday)</returns>
        public static bool IsBusinessDay(this DateTime date)
        {
            return
                date.DayOfWeek != DayOfWeek.Saturday &&
                date.DayOfWeek != DayOfWeek.Sunday;
        }

        public static void ObserveException(this Task task)
        {
            task.ContinueWith(t =>
                {
                    if (t.Exception != null)
                    {
                        LogFactory.Instance.GetLogger(null)
                                  .LogException(LogLevel.Fatal, "An unobserved task exception catched", t.Exception);
                    }
                }, TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.OnlyOnFaulted);
        }

        public static void PerformActionWithErrorHandling(Action action)
        {
            try
            {
                action();
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null)
                                  .LogException(LogLevel.Fatal, "An unexpected exception during action", e);
            }
        }

        public static int OwningEnumValuesCount(this Enum @enum)
        {
            return Enum.GetValues(@enum.GetType()).Length;
        }

        public static int OwningEnumValuesCount(this Type enumType)
        {
            return Enum.GetValues(enumType).Length;
        }

        public static IEnumerable<T> GetFlagsEnumIndividualValues<T>(this Enum @enum) where T : struct
        {
            return Enum.GetValues(typeof(T)).Cast<Enum>().Where(@enum.HasFlag).Cast<T>();
        }

        public static IEnumerable<Enum> GetFlagsEnumIndividualValues(this Enum @enum)
        {
            return Enum.GetValues(@enum.GetType()).Cast<Enum>().Where(@enum.HasFlag);
        }

        public static void SafeInvokeSynchronous(this Action action)
        {
            if (action != null)
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    LogFactory.Instance.GetLogger(null)
                        .LogException(LogLevel.Fatal, "Exception during safe invoking of action", e);
                }
        }

        //public static void ClearWhole(this Array array)
        //{
        //    Array.Clear(array, 0, array.Length);
        //}

        public static void ClearWhole<T>(this T[] array) where T: struct
        {
            Array.Clear(array, 0, array.Length);
        }

        public static void ClearWhole<T>(this T[][] array) where T : struct
        {
            for (int idx = 0; idx < array.Length; idx++)
            {
                array[idx].ClearWhole();
            }
        }

        public static void ClearWhole(this string[][] array)
        {
            for (int idx = 0; idx < array.Length; idx++)
            {
                Array.Clear(array[idx], 0, array[idx].Length);
            }
        }

        public static void CopyWholeFrom<T>(this T[] dstArray, T[] srcArray) where T : struct
        {
            if (dstArray.Length != srcArray.Length)
                throw new ArgumentException(
                    string.Format(
                        "CopyWholeFrom: Destination array length ({0}) is unequal to source array length ({1})",
                        dstArray.Length, srcArray.Length));

            Array.Copy(srcArray, dstArray, dstArray.Length);
        }

        public static void CopyWholeFrom<T>(this T[][] dstArray, T[][] srcArray) where T : struct
        {
            if (dstArray.Length != srcArray.Length)
                throw new ArgumentException(
                    string.Format(
                        "CopyWholeFrom([][]): Destination array length ({0}) is unequal to source array length ({1})",
                        dstArray.Length, srcArray.Length));

            for (int idx = 0; idx < dstArray.Length; idx++)
            {
                dstArray[idx].CopyWholeFrom(srcArray[idx]);
            }
        }
    }

    public static class InterlockedEx
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int Read(ref int value)
        {
            return Interlocked.CompareExchange(ref value, 0, 0);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool CheckAndFlipState(ref int value, int newState, int expectedState)
        {
            return Interlocked.CompareExchange(ref value, newState, expectedState) == expectedState;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool CheckAndFlipState<T>(ref T value, T newState, T expectedState) where T: class
        {
            return Interlocked.CompareExchange(ref value, newState, expectedState) == expectedState;
        }
    }

    public static class DateTimeEx
    {
        public static DateTime Max(DateTime a, DateTime b)
        {
            return new DateTime(Math.Max(a.Ticks, b.Ticks));
        }

        public static DateTime Min(DateTime a, DateTime b)
        {
            return new DateTime(Math.Min(a.Ticks, b.Ticks));
        }
    }

    public static class TimeSpanEx
    {
        public static TimeSpan Max(TimeSpan a, TimeSpan b)
        {
            return new TimeSpan(Math.Max(a.Ticks, b.Ticks));
        }

        public static TimeSpan Min(TimeSpan a, TimeSpan b)
        {
            return new TimeSpan(Math.Min(a.Ticks, b.Ticks));
        }

        public static TimeSpan Times(this TimeSpan a, int multiplier)
        {
            return new TimeSpan(a.Ticks * multiplier);
        }
    }

    public static class TaskEx
    {
        //public static void ExecAfterDelayWithErrorHandling(TimeSpan delay, Action action,
        //    TimerTaskFlagUtils.WorkItemPriority taskExecutionPriority,
        //    TimerTaskFlagUtils.TimerPrecision requiredTimerPrecision, bool isUltraQuick)
        //{
        //    HighResolutionDateTime.HighResolutionTimerManager.RegisterTimer(action, delay, taskExecutionPriority,
        //        requiredTimerPrecision, false, isUltraQuick);
        //}

        private static TimeSpan _borderDelayForLowPriTasks = TimeSpan.FromSeconds(3);
        public static void ExecAfterDelayWithErrorHandling(TimeSpan delay, Action action)
        {
            Task.Delay(delay).ContinueWith(t => action()).ObserveException();

            //HighResolutionDateTime.HighResolutionTimerManager.RegisterTimer(action, delay,
            //    delay > _borderDelayForLowPriTasks
            //        ? TimerTaskFlagUtils.WorkItemPriority.Lowest
            //        : TimerTaskFlagUtils.WorkItemPriority.Normal,
            //    TimerTaskFlagUtils.GetTimerPrecisionForDelay(delay), false, false);
        }

        public static void StartNew(Action action, TimerTaskFlagUtils.WorkItemPriority workItemPriority)
        {
            ThreadPoolEx.Instance.QueueWorkItem(new WorkItem(action, workItemPriority));
        }

        public static void StartNew(Action action)
        {
            ThreadPoolEx.Instance.QueueWorkItem(new WorkItem(action, TimerTaskFlagUtils.WorkItemPriority.Normal));
        }

        public static IEnumerable<T> OrderRandomly<T>(this IList<T> sequence)
        {
            Random rand = new Random();
            return Enumerable.Range(0, sequence.Count).OrderBy(x => rand.Next()).Select(i => sequence[i]);
        }
    }

    public static class Constansts
    {
        public static readonly TimeSpan MinimumSpanBetweenLLandWatchdog = TimeSpan.FromMilliseconds(2);
    }

    public static class MathEx
    {
        public static decimal Min(decimal d1, decimal d2, decimal d3, decimal d4)
        {
            return Math.Min(Math.Min(d1, d2), Math.Min(d3, d4));
        }

        public static decimal Min(decimal d1, decimal d2, decimal d3, decimal d4, decimal d5, decimal d6)
        {
            return Math.Min(MathEx.Min(d1, d2, d3, d4), Math.Min(d5, d6));
        }
    }
}
