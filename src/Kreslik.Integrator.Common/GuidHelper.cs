﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public static class GuidHelper
    {
        [DllImport("rpcrt4.dll", SetLastError = true)]
        private static extern int UuidCreateSequential(out Guid value);

        [Flags]
        private enum RetUuidCodes : int
        {
            RPC_S_OK = 0, //The call succeeded.
            RPC_S_UUID_LOCAL_ONLY = 1824, //The UUID is guaranteed to be unique to this computer only.
            RPC_S_UUID_NO_ADDRESS = 1739 //Cannot get Ethernet or token-ring hardware address for this computer.
        }

        //Be carefull about SQL SERVER ORDERING!!! - despite that guids thhemselves are sequential (timestamp is in first sector)
        // sql server clusteres machine guid together
        // reference: http://stackoverflow.com/questions/5585307/sequential-guids
        public static Guid NewSequentialGuid()
        {
            Guid guid;
            int result = UuidCreateSequential(out guid);
            if (result == (int)RetUuidCodes.RPC_S_OK)
                return guid;
            else
                return Guid.NewGuid();
        }
    }
}
