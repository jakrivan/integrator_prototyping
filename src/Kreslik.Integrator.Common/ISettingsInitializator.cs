﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public interface ISettingsInitializator
    {
        string InstanceDescription { get; }
        string EnvironmentName { get; }
        string InstanceName { get; }
        bool IsUat { get; }
        T ReadSettings<T>(string settingName, string xsdConfigurationUri) where T : class;
        bool HasSettings(string settingName);
    }
}
