﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public interface IPooledObject
    {
        void Acquire();
        void Release();
    }

    public interface IPooledBufferSegment : IPooledObject
    {
        Byte[] Buffer { get; }
        int ContentStart { get; }
        int ContentSize { get; }

        string GetContentAsAsciiString();
    }
}
