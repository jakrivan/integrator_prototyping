﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace Kreslik.Integrator.Common
{
    public enum LogLevel
    {
        Trace = 0,
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4,
        Fatal = 5
    }

    public interface ILogger
    {
        void Log(LogLevel level, string message);

        void Log(LogLevel level, IPooledBufferSegment pooledBuffer);

        void Log(LogLevel level, string message, IPooledBufferSegment pooledBuffer);

        //void LogWithDt(LogLevel level, DateTime correctedTime, string message);
        void Log(LogLevel level, string messageFmt, params object[] args);
        //void Log(LogLevel level, IFormatProvider formatProvider, string messageFmt, params object[] args);
        void LogException(LogLevel level, string message, Exception e);
        void LogException(LogLevel level, Exception e, string messageFmt, params object[] args);
        //void LogException(Exception e);
        bool IsEnabled(LogLevel level);

        bool IsLowDiskSpaceConstraintLoggingOn { get; }
        event Action<bool> IsLowDiskSpaceChanged;
    }

    public interface ILogFactory
    {
        ILogger GetLogger(string logName);
        /// <summary>
        /// Creates logger device while specifying the name of log device and importance of log messages
        /// </summary>
        /// <param name="logName">Name of the device (can translte to a filename)</param>
        /// <param name="isLessImportant">false if logger logs very few >=Warn messages</param>
        /// <returns></returns>
        ILogger GetLogger(string logName, bool isLessImportant);

        ILogger GetLogger(string logName, bool isLessImportant, bool doNotLogDuringLowDiskSpace);
    }

    public class LogFactory : ILogFactory
    {
        private static ILogFactory _instance = new LogFactory();
        private static ParentLogger _parentLogger = new ParentLogger();
        private static ILogger _nullLogger = null;

        internal static void InitializeTimers()
        {
            _parentLogger.InitializeTimers();
        }

        public static ILogFactory Instance { get { return _instance; } }

        public ILogger GetLogger(string logName)
        {
            return this.GetLogger(logName, false, false);
        }

        public ILogger GetLogger(string logName, bool isLessImportant)
        {
            return this.GetLogger(logName, isLessImportant, false);
        }

        public ILogger GetLogger(string logName, bool isLessImportant, bool doNotLogDuringLowDiskSpace)
        {
            if (!string.IsNullOrEmpty(logName))
            {
                int loggerId = _parentLogger.AddLogger(logName);

                return new Logger(loggerId, _parentLogger, isLessImportant);
            }
            else
            {
                if (_nullLogger == null)
                {
                    _nullLogger = new Logger(-1, _parentLogger, isLessImportant);
                }

                return _nullLogger;
            }
        }

        public static event Action<string> NewFatalLogEntry
        {
            add { _parentLogger.NewFatalRecord += value; }
            remove { _parentLogger.NewFatalRecord -= value; }
        }

        public static IImportantInfoProvider ImportantInfoProvider
        {
            get { return _parentLogger.BufferedEmailsSender; }
        }

        private class LogEvent
        {
            private DateTime _timeStamp;
            private int _threadId;
            public LogLevel LogLevel { get; private set; }
            public int LoggerId { get; private set; }
            public bool IsLessImportant { get; private set; }

            private string _message;
            private object[] _args;
            private Exception _exception;
            public IPooledBufferSegment PooledBufferSegment { get; private set; }

            private static bool _isAccurateHighResolutionTimeAvailable =
                HighResolutionDateTime.IsAccurateDateTimeAvailable;

            private LogEvent(int loggerId, LogLevel logLevel)
            {
                this._timeStamp = _isAccurateHighResolutionTimeAvailable ? HighResolutionDateTime.UtcNow : DateTime.UtcNow;
                this._threadId = Thread.CurrentThread.ManagedThreadId;
                this.LoggerId = loggerId;
                this.LogLevel = logLevel;
            }

            public LogEvent(int loggerId, LogLevel logLevel, string message, bool isLessImportant)
                : this(loggerId, logLevel)
            {
                this._message = message;
                this.IsLessImportant = isLessImportant;
            }

            public LogEvent(int loggerId, LogLevel logLevel, IPooledBufferSegment pooledBufferSegment, bool isLessImportant)
                : this(loggerId, logLevel)
            {
                pooledBufferSegment.Acquire();
                this.PooledBufferSegment = pooledBufferSegment;
                this.IsLessImportant = isLessImportant;
            }

            public LogEvent(int loggerId, LogLevel logLevel, string message, IPooledBufferSegment pooledBufferSegment, bool isLessImportant)
                : this(loggerId, logLevel)
            {
                pooledBufferSegment.Acquire();
                this.PooledBufferSegment = pooledBufferSegment;
                this._message = message;
                this.IsLessImportant = isLessImportant;
            }

            public LogEvent(int loggerId, LogLevel logLevel, string message, bool isLessImportant, DateTime correctedTimestamp)
                : this(loggerId, logLevel)
            {
                this._message = message;
                this.IsLessImportant = isLessImportant;
                this._timeStamp = correctedTimestamp;
            }

            public LogEvent(int loggerId, LogLevel logLevel, string message, bool isLessImportant, object[] args)
                : this(loggerId, logLevel)
            {
                this._message = message;
                this._args = args;
                this.IsLessImportant = isLessImportant;
            }

            public LogEvent(int loggerId, LogLevel logLevel, string message, Exception exception, bool isLessImportant)
                : this(loggerId, logLevel)
            {
                this._message = message;
                this._exception = exception;
                this.IsLessImportant = isLessImportant;
            }

            public LogEvent(int loggerId, LogLevel logLevel, Exception exception, string message, bool isLessImportant, object[] args)
                : this(loggerId, logLevel)
            {
                this._message = message;
                this._args = args;
                this._exception = exception;
                this.IsLessImportant = isLessImportant;
            }

            public string GetMessagePrefix()
            {
                return string.Format("{0} {1} {2} ", _timeStamp.ToString("yyyy-MM-dd HH:mm:ss.fffffff UTC"), _threadId,
                                  LogLevel.ToString().ToUpper());
            }

            public string GetMessageSuffix()
            {
                string message = _message;
                if (_args != null)
                {
                    try
                    {
                        message = string.Format(message, _args);
                    }
                    catch (Exception e)
                    {
                        message =
                            string.Format(
                                "#MALFORMED PARAMETERS ARRAY FOR LOGGER# Original Level: {0}, Message: {1} Arguments: {2}{3}Exception thrown during formatting: {4}",
                                LogLevel.Fatal,
                                _message,
                                string.Join(";", _args.Select(
                                    o =>
                                    {
                                        try
                                        {
                                            return o == null ? "<NULL>" : o.ToString();
                                        }
                                        catch (Exception innerEx)
                                        {
                                            return string.Format("<ToString() throws: {0}>", innerEx);
                                        }
                                    })), Environment.NewLine, e);
                        this.LogLevel = LogLevel.Error;
                    }
                }
                if (_exception != null)
                {
                    message = string.Format("{0} {1}", message, _exception);
                }

                return message;
            }

            public override string ToString()
            {
                if (_args == null)
                {
                    return string.Format("{0} {1} {2} {3} {4}", _timeStamp.ToString("yyyy-MM-dd HH:mm:ss.fffffff UTC"), _threadId,
                                  LogLevel.ToString().ToUpper(), _message, _exception);
                }
                else
                {
                    return string.Format("{0} {1} {2} {3}", _timeStamp.ToString("yyyy-MM-dd HH:mm:ss.fffffff UTC"), _threadId,
                                  LogLevel.ToString().ToUpper(), string.Format(_message, _args));
                }
            }
        }

        private static class LogArchivingHelper
        {
            [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptions]
            public static void ArchiveDirectory(string target_dir, bool ignoreErrors)
            {
                try
                {
                    ArchiveDirectoryInternal(target_dir, ignoreErrors);
                }
                catch (AccessViolationException e)
                {
                    if (!ignoreErrors)
                        throw new Exception(string.Format("Archivation failed (AV): {0}", e.ToString()));
                }
                catch (Exception e)
                {
                    if (!ignoreErrors)
                        throw;
                }
            }

            private static void ArchiveDirectoryInternal(string target_dir, bool ignoreErrors)
            {
                string renamedDir = target_dir + "_toBeDeleted";
                string zippedDir = target_dir + ".zip";

                if (Directory.Exists(target_dir) && !Directory.Exists(renamedDir) &&
                    !File.Exists(zippedDir))
                {
                    try
                    {
                        //this will inform us abould opened handle and also will claim the folder for us
                        Directory.Move(target_dir, renamedDir);
                    }
                    catch (Exception e)
                    {
                        if (ignoreErrors)
                            return;
                        else if (!InteropSHFileOperationWrapper.RenameDirNativeSilent(target_dir, renamedDir))
                            //Native operation fails if some other process (not system) still holds handle
                            throw new Exception("Cannot rename directory " + target_dir, e);
                    }


                    try
                    {
                        //this should already succeed, as we zip freshly renamed dir
                        System.IO.Compression.ZipFile.CreateFromDirectory(renamedDir, zippedDir);
                    }
                    catch (Exception)
                    {
                        if (!ignoreErrors)
                            throw;
                    }

                    try
                    {
                        //this can fail - as explorer can still keep opened the renamed dir
                        DeleteDirectory(renamedDir);
                    }
                    catch (Exception e)
                    {
                        //this fails if somebody still keeps opened handle 
                        // (this should have been filtered out by rename already, but something could happen in the meantime)
                        if (!InteropSHFileOperationWrapper.DeleteDirNativeSilent(renamedDir) && !ignoreErrors)
                            throw new Exception("Cannot delete directory " + renamedDir, e);

                        //just for sure verify that directory was really removed
                        if (!ignoreErrors && Directory.Exists(renamedDir))
                            throw new Exception("Directory still exists after deletion " + renamedDir, e);
                    }
                }
            }

            private static void DeleteDirectory(string target_dir)
            {
                string[] files = Directory.GetFiles(target_dir);
                string[] dirs = Directory.GetDirectories(target_dir);

                foreach (string file in files)
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                }

                foreach (string dir in dirs)
                {
                    DeleteDirectory(dir);
                }

                Directory.Delete(target_dir, false);
            }
        }

        private class ParentLogger
        {
            private BlockingCollection<LogEvent> _logEvents = new BlockingCollection<LogEvent>(new ConcurrentQueue<LogEvent>());
            private List<StreamWrapper> _streams = new List<StreamWrapper>();
            private List<byte[]> _loggerNames = new List<byte[]>();
            private object _streamsLocker = new object();
            private StreamWrapper _allEventsStream;
            private StreamWrapper _lessImportantEventsStream;
            private StreamWrapper _veryImportantEventsStream;
            private LogLevel _levelThreshold = LogLevel.Warn;
            private volatile bool _flush = false;
            private SafeTimer _timer;
            private DirWrapper _dirWrapper;
            internal Action<string> NewFatalRecord;
            private SafeTimer _logArchivationTimer;
            internal BufferedEmailsSender BufferedEmailsSender { get; private set; }

            public ParentLogger()
            {
                _dirWrapper = new DirWrapper();

                _allEventsStream = new StreamWrapper(_dirWrapper, "EverythingTogether");
                _lessImportantEventsStream = new StreamWrapper(_dirWrapper, "Important");
                _veryImportantEventsStream = new StreamWrapper(_dirWrapper, "VeryImportant");

                BufferedEmailsSender =
                new BufferedEmailsSender(
                    CommonSettings.CommonBehavior.Logging.FatalErrorsEmailTo == null ? null :
                    CommonSettings.CommonBehavior.Logging.FatalErrorsEmailTo.Split(new char[] { ';' }),
                    CommonSettings.CommonBehavior.Logging.FatalErrorsEmailCc == null ? null :
                    CommonSettings.CommonBehavior.Logging.FatalErrorsEmailCc.Split(new char[] { ';' }),
                    "fatal log item",
                    CommonSettings.CommonBehavior.Logging.FatalErrorsEmailMinDelaysBetweenEmails);

                //Multicast delegate keeps reference to an instace owning the method, so if logger references the
                // delegate then the BufferedEmailsSender will not be subject to GC - so no need to keep it in a locla variable
                this.NewFatalRecord += BufferedEmailsSender.AddEntry;

                ThreadPool.QueueUserWorkItem(ProcessLogEvents);
            }

            //this cannot be performed as part of initialization, since that would cause reference loop
            internal void InitializeTimers()
            {
                _timer = new SafeTimer(FlushHandler, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2))
                {
                    RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1sec,
                    TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Normal
                };

                //let's perform first archivation next day (this day folders might be still in use by other processes)
                _logArchivationTimer = new SafeTimer(ArchiveOldLogDirs,
                    //lets start at random time after midnight - this way processes will not step on each others foot
                    TillNextUtcMidnight().Add(TimeSpan.FromMinutes(30 + new Random(Environment.TickCount).NextDouble() * 30)),
                    TimeSpan.FromDays(1), true)
                {
                    RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1min,
                    TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
                };
            }

            internal bool BypassLoggingEntirely { get; private set; }

            private TimeSpan TillNextUtcMidnight()
            {
                DateTime utcNow = DateTime.UtcNow;
                return utcNow.Date.AddDays(1) - utcNow;
            }

            private void ArchiveOldLogDirs()
            {
                foreach (string oldLogDirsInSiblingFolder in _dirWrapper.OldLogDirsInSiblingFolders)
                {
                    try
                    {
                        LogArchivingHelper.ArchiveDirectory(oldLogDirsInSiblingFolder, false);
                    }
                    catch (Exception e)
                    {
                        this.LogAsync(new LogEvent(-1, LogLevel.Fatal, e,
                                               "Cannot perform archivation of log folder [{0}] - it's likely opened somewhere, please close opened handles and integrator will retry after UTC midnight",
                                               false, new[] { oldLogDirsInSiblingFolder }));
                    }
                }
            }

            public int AddLogger(string loggerIdentity)
            {
                lock (_streamsLocker)
                {
                    _streams.Add(new StreamWrapper(_dirWrapper, loggerIdentity));

                    _loggerNames.Add(Encoding.Default.GetBytes(loggerIdentity + " "));

                    return _streams.Count - 1;
                }
            }

            public void LogAsync(LogEvent logEvent)
            {
                this._logEvents.Add(logEvent);
            }

            private bool _isLowDiskSpaceApproaching;
            private bool _isLowDiskSpace;
            public bool IsLowDiskSpace
            {
                get { return this._isLowDiskSpace; }

                set
                {
                    if (this._isLowDiskSpace != value)
                    {
                        this._isLowDiskSpace = value;
                        if (IsLowDiskSpaceChanged != null)
                        {
                            IsLowDiskSpaceChanged(value);
                        }
                    }
                }
            }
            public event Action<bool> IsLowDiskSpaceChanged;


            private byte[] GetLoggerName(int loggerId)
            {
                if (loggerId >= 0)
                {
                    return _loggerNames[loggerId];
                }
                else
                {
                    return new byte[0];
                }
            }

            //memory pool for byte arrays
            private readonly BytesContainer _bytesContainer = new BytesContainer();

            //We need a LONG constant so that multiplying results in long result (and not an overflown int)
            private const long BYTES_IN_GB = 1073741824;

            private DateTime _lastFlushTime = DateTime.UtcNow;
            //bytes in GB - 1073741824
            private readonly long _availableBytesThreshold =
                BYTES_IN_GB * CommonSettings.CommonBehavior.Logging.AvailableDiskSpaceThresholdToStopMDLogging_GB;
            private readonly long _availableBytesThresholdToStartLogging =
                BYTES_IN_GB * (CommonSettings.CommonBehavior.Logging.AvailableDiskSpaceThresholdToStopMDLogging_GB + 1);
            private readonly long _firstWarningBytesThreshold = (long)
                (BYTES_IN_GB * CommonSettings.CommonBehavior.Logging.AvailableDiskSpaceThresholdToStopMDLogging_GB * 1.5);
            private void FlushHandler()
            {
                if (_flush)
                {
                    if (DateTime.UtcNow - _lastFlushTime > TimeSpan.FromMinutes(1))
                    {
                        this.Flush();
                    }
                }
                else
                {
                    _flush = true;
                    _lastFlushTime = DateTime.UtcNow;
                }

                long freeBytes = _dirWrapper.GetAvailableFreeBytes();

                if (IsLowDiskSpace)
                {
                    if (freeBytes > _availableBytesThresholdToStartLogging)
                    {
                        IsLowDiskSpace = false;
                        this.LogAsync(new LogEvent(-1, LogLevel.Warn,
                                                   "Available disk space is {0}, so resuming logging", false,
                                                   new object[] { freeBytes }));
                    }
                }
                else
                {
                    if (freeBytes < _availableBytesThreshold)
                    {
                        IsLowDiskSpace = true;
                        this.LogAsync(new LogEvent(-1, LogLevel.Fatal,
                                                   "Available disk space is {0}, so stopping some logging", false,
                                                   new object[] { freeBytes }));
                    }
                }

                if (_isLowDiskSpaceApproaching)
                {
                    if (freeBytes > _firstWarningBytesThreshold + BYTES_IN_GB)
                    {
                        _isLowDiskSpaceApproaching = false;
                        this.LogAsync(new LogEvent(-1, LogLevel.Warn,
                                                   "Available disk space is {0}, low disk space risk is resolved", false,
                                                   new object[] { freeBytes }));
                    }
                }
                else
                {
                    if (freeBytes < _firstWarningBytesThreshold)
                    {
                        _isLowDiskSpaceApproaching = true;
                        this.LogAsync(new LogEvent(-1, LogLevel.Fatal,
                                                   "Available disk space is {0}, low disk space risk is approaching. Delete some files otherwise [Integrator process: {1}] will soon constraint some logging and (if this is Integrator server) then it will stop data collection",
                                                   false,
                                                   new object[] { freeBytes, this.GetIntegratorProcessType() }));
                    }
                }

                this.BypassLoggingEntirely = File.Exists("BypassLoggingEntirely");
            }

            private string GetIntegratorProcessType()
            {
                return IntegratorPerformanceCounter.CurrentIntegratorProcessType.HasValue
                    ? IntegratorPerformanceCounter.CurrentIntegratorProcessType.ToString()
                    : "Local Process (Splitter or Client)";
            }

            private void Flush()
            {
                _flush = false;

                for (int i = 0; i < _streams.Count; i++)
                {
                    _streams[i].Flush();
                }
                _allEventsStream.Flush();
                _lessImportantEventsStream.Flush();
                _veryImportantEventsStream.Flush();
            }

            private void ProcessLogEvents(object unused)
            {
                foreach (LogEvent logEvent in _logEvents.GetConsumingEnumerable())
                {
                    this.TryProcessLogEntry(logEvent);
                }
            }

            private void TryProcessLogEntry(LogEvent logEvent)
            {
                try
                {
                    _bytesContainer.FillWithLogEvent(logEvent);

                    if (logEvent.LoggerId >= 0)
                    {
                        _streams[logEvent.LoggerId].WriteLine(_bytesContainer);
                    }

                    _allEventsStream.WriteLine(_bytesContainer, GetLoggerName(logEvent.LoggerId));

                    if (logEvent.LogLevel >= _levelThreshold)
                    {
                        if (logEvent.IsLessImportant)
                        {
                            _lessImportantEventsStream.WriteLine(_bytesContainer, GetLoggerName(logEvent.LoggerId));
                        }
                        else
                        {
                            _veryImportantEventsStream.WriteLine(_bytesContainer, GetLoggerName(logEvent.LoggerId));
                        }

                        if (logEvent.LogLevel >= LogLevel.Fatal && this.NewFatalRecord != null)
                        {
                            string logEntry =
                                _bytesContainer.GetEventAsString(
                                    Encoding.Default.GetString(GetLoggerName(logEvent.LoggerId)));
                            System.Console.Out.WriteLine(logEntry);
                            this.NewFatalRecord(logEntry);
                        }
                    }

                    if (logEvent.PooledBufferSegment != null)
                        logEvent.PooledBufferSegment.Release();

                    if (_flush)
                    {
                        this.Flush();
                    }
                }
                catch (Exception e)
                {
                    this.LogAsync(new LogEvent(-1, LogLevel.Fatal, "Unexpected error during logging", e, false));
                }
            }


            //private BlockingCollection<LogEvent> _logEvents = new BlockingCollection<LogEvent>(new ConcurrentQueue<LogEvent>());
            //private List<Stream> _streams = new List<Stream>();
            //private List<byte[]> _loggerNames = new List<byte[]>();
            //private object _streamsLocker = new object();
            //private string _targetDir;
            //private Stream _allEventsStream;
            //private Stream _importantEventsStream;
            //private LogLevel _levelThreshold = LogLevel.Warn;
            //private volatile bool _flush = false;
            //private Timer _timer;

            //public ParentLogger()
            //{
            //    string basedir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            //    _targetDir = System.IO.Path.Combine(basedir, "logs", DateTime.UtcNow.ToString("yyyy-MM-dd"),
            //                                              Process.GetCurrentProcess().Id.ToString());
            //    if (!Directory.Exists(_targetDir))
            //    {
            //        Directory.CreateDirectory(_targetDir);
            //    }

            //    _allEventsStream = File.Open(
            //        System.IO.Path.Combine(_targetDir, "EverythingTogether.log"),
            //        FileMode.Append,
            //        FileAccess.Write,
            //        FileShare.Read);

            //    _importantEventsStream = File.Open(
            //        System.IO.Path.Combine(_targetDir, "Important.log"),
            //        FileMode.Append,
            //        FileAccess.Write,
            //        FileShare.Read);

            //    ThreadPool.QueueUserWorkItem(ProcessLogEvents);
            //    _timer = new Timer((o) => { _flush = true; }, null, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2));
            //}

            //public int AddLogger(string loggerIdentity)
            //{
            //    lock (_streamsLocker)
            //    {
            //        _streams.Add(
            //            File.Open(
            //                System.IO.Path.Combine(_targetDir, loggerIdentity + ".log"),
            //                FileMode.Append,
            //                FileAccess.Write,
            //                FileShare.Read)
            //            );

            //        _loggerNames.Add(Encoding.Default.GetBytes(loggerIdentity + " "));

            //        return _streams.Count - 1;
            //    }
            //}

            //public void LogAsync(LogEvent logEvent)
            //{
            //    this._logEvents.Add(logEvent);
            //}

            //private void ProcessLogEvents(object unused)
            //{
            //    foreach (LogEvent logEvent in _logEvents.GetConsumingEnumerable())
            //    {
            //        string prefix = logEvent.GetMessagePrefix();
            //        string suffix = logEvent.GetMessageSuffix();

            //        byte[] prefixBytes = Encoding.Default.GetBytes(prefix);
            //        byte[] suffixBytes = Encoding.Default.GetBytes(suffix);

            //        _streams[logEvent.LoggerId].Write(prefixBytes, 0, prefixBytes.Length);
            //        _streams[logEvent.LoggerId].Write(suffixBytes, 0, suffixBytes.Length);

            //        _allEventsStream.Write(prefixBytes, 0, prefixBytes.Length);
            //        _allEventsStream.Write(_loggerNames[logEvent.LoggerId], 0, _loggerNames[logEvent.LoggerId].Length);
            //        _allEventsStream.Write(suffixBytes, 0, suffixBytes.Length);

            //        if (logEvent.LogLevel >= _levelThreshold)
            //        {
            //            _importantEventsStream.Write(prefixBytes, 0, prefixBytes.Length);
            //            _importantEventsStream.Write(_loggerNames[logEvent.LoggerId], 0, _loggerNames[logEvent.LoggerId].Length);
            //            _importantEventsStream.Write(suffixBytes, 0, suffixBytes.Length);

            //            System.Console.Out.Write(prefix + Encoding.Default.GetString(_loggerNames[logEvent.LoggerId]) + suffix);
            //        }

            //        if (_flush)
            //        {
            //            _flush = false;

            //            for (int i = 0; i < _streams.Count; i++)
            //            {
            //                _streams[i].Flush();
            //            }
            //            _allEventsStream.Flush();
            //            _importantEventsStream.Flush();
            //        }
            //    }
            //}

            private class BytesContainer
            {
                public BytesContainer()
                {
                    _bytes = new byte[2048];
                }

                public void FillWithLogEvent(LogEvent logEvent)
                {
                    string firstString = logEvent.GetMessagePrefix();

                    try
                    {
                        _firstStringLength = Encoding.Default.GetBytes(firstString, 0, firstString.Length, _bytes, 0);
                    }
                    catch (ArgumentException ex)
                    {
                        int maxCount = Encoding.Default.GetMaxByteCount(firstString.Length);
                        if (_bytes.Length < maxCount)
                        {
                            _bytes = new byte[maxCount + 2048];
                        }
                        _firstStringLength = Encoding.Default.GetBytes(firstString, 0, firstString.Length, _bytes, 0);
                    }


                    string secondString = logEvent.GetMessageSuffix();
                    _secondStringLength = 0;
                    if (secondString != null)
                    {
                        try
                        {
                            _secondStringLength = Encoding.Default.GetBytes(secondString, 0, secondString.Length, _bytes, _firstStringLength);
                        }
                        catch (ArgumentException ex)
                        {
                            int maxCount = Encoding.Default.GetMaxByteCount(secondString.Length);
                            if (_bytes.Length < maxCount + _firstStringLength)
                            {
                                byte[] tmpArr = new byte[maxCount + _firstStringLength];
                                Buffer.BlockCopy(_bytes, 0, tmpArr, 0, _firstStringLength);
                                _bytes = tmpArr;
                            }
                            _secondStringLength = Encoding.Default.GetBytes(secondString, 0, secondString.Length, _bytes, _firstStringLength);
                        }
                    }

                    this._pooledBufferSegment = logEvent.PooledBufferSegment;
                }

                public string GetEventAsString(string middlePart)
                {
                    return Encoding.Default.GetString(_bytes, 0, _firstStringLength) + middlePart +
                    (this._pooledBufferSegment != null
                        ? this._pooledBufferSegment.GetContentAsAsciiString()
                        : Encoding.Default.GetString(_bytes, _firstStringLength, _secondStringLength));
                }

                public void StuffFirstString(Action<byte[], int, int> bytesConsumingAction)
                {
                    bytesConsumingAction(this._bytes, 0, _firstStringLength);
                }

                public void StuffSecondString(Action<byte[], int, int> bytesConsumingAction)
                {
                    if (_secondStringLength != 0)
                        bytesConsumingAction(this._bytes, _firstStringLength, _secondStringLength);

                    if (_pooledBufferSegment != null)
                        bytesConsumingAction(this._pooledBufferSegment.Buffer, this._pooledBufferSegment.ContentStart,
                            this._pooledBufferSegment.ContentSize);
                }

                private byte[] _bytes;
                private int _firstStringLength;
                private int _secondStringLength;
                private IPooledBufferSegment _pooledBufferSegment;
            }

            private class StreamWrapper
            {
                private const int MAX_LINES = 500000;
                private readonly byte[] NEWLINE = Encoding.Default.GetBytes(Environment.NewLine);
                private Stream _stream;
                private string _filename;
                private int _currentWriteCount;
                private Action<byte[], int, int> _streamWriteAction;

                private DirWrapper _dirWrapper;

                public StreamWrapper(DirWrapper dirWrapper, string fileName)
                {
                    this._dirWrapper = dirWrapper;
                    this._filename = fileName;

                    this.FilesCount = 0;
                    this._currentWriteCount = 0;

                    this.CreateStream();
                }

                private void CreateStream()
                {
                    _stream = File.Open(
                            System.IO.Path.Combine(_dirWrapper.GetPath(this), string.Format("{0}_{1}.log", _filename, FilesCount)),
                            FileMode.Append,
                            FileAccess.Write,
                            FileShare.Read);
                    _streamWriteAction = _stream.Write;
                }

                public void WriteLine(BytesContainer bytesContainer)
                {
                    bytesContainer.StuffFirstString(_streamWriteAction);
                    bytesContainer.StuffSecondString(_streamWriteAction);
                    _stream.Write(NEWLINE, 0, NEWLINE.Length);
                    _currentWriteCount++;
                }


                public void WriteLine(BytesContainer bytesContainer, byte[] middleBytes)
                {
                    bytesContainer.StuffFirstString(_streamWriteAction);
                    _stream.Write(middleBytes, 0, middleBytes.Length);
                    bytesContainer.StuffSecondString(_streamWriteAction);
                    _stream.Write(NEWLINE, 0, NEWLINE.Length);
                    _currentWriteCount++;
                }

                public int FilesCount { get; set; }

                public void Flush()
                {
                    _stream.Flush();

                    if (!this._dirWrapper.IsActual(this, this.FilesCount))
                    {
                        _stream.Close();

                        this.FilesCount = 0;
                        this._currentWriteCount = 0;

                        this.CreateStream();
                    }

                    if (_currentWriteCount > MAX_LINES)
                    {
                        _stream.Close();
                        this.FilesCount++;
                        this._currentWriteCount = 0;

                        this.CreateStream();
                    }
                }
            }

            private class DirWrapper
            {
                private string _targetDir;
                private string _basedir;
                private string _oldTagetDir;
                private HashSet<StreamWrapper> _currentStreams = new HashSet<StreamWrapper>();
                private HashSet<StreamWrapper> _oldStreams;
                private int _targetDirCreationDay = -1;
                private DriveInfo _driveInfo;

                private const int MAX_FILES_IN_FOLDER = 60;
                private const string LOGS_DIR = "logs";

                public DirWrapper()
                {
                    _basedir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    _basedir = _basedir ?? string.Empty;
                    _driveInfo = new DriveInfo(Directory.GetDirectoryRoot(_basedir));

                    CreateDir();
                }

                public IEnumerable<string> OldLogDirsInSiblingFolders
                {
                    get
                    {
                        string dirToSearch = _basedir;
                        var parent = Directory.GetParent(_basedir);
                        if (parent != null)
                        {
                            var parentParent = Directory.GetParent(parent.FullName);
                            if (parentParent != null)
                                dirToSearch = parentParent.FullName;
                            else
                                dirToSearch = parent.FullName;
                        }

                        return
                            Directory.GetDirectories(dirToSearch, LOGS_DIR, SearchOption.AllDirectories)
                                     .Select(Directory.GetDirectories)
                                     .SelectMany(dir => dir)
                            //Do not pack todays dirs as they might still be in use. They will be picked up later
                            // or folders that are alredy in process of archivation
                                     .Where(dir => Directory.GetCreationTimeUtc(dir) < DateTime.UtcNow.Date && !dir.EndsWith("_toBeDeleted"))
                                     .ToArray();
                    }
                }

                public long GetAvailableFreeBytes()
                {
                    return _driveInfo.AvailableFreeSpace;
                }

                private void CreateDir()
                {
                    _targetDir = System.IO.Path.Combine(_basedir, LOGS_DIR,
                                                        string.Format("{0:yyyy-MM-dd-HHmm-UTC}_{1}", DateTime.UtcNow, Environment.MachineName),
                                                        Process.GetCurrentProcess().Id.ToString());

                    _targetDirCreationDay = DateTime.UtcNow.Day;

                    if (!Directory.Exists(_targetDir))
                    {
                        DirectorySecurity dirSec = new DirectorySecurity();
                        dirSec.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl,
                                                                      InheritanceFlags.ContainerInherit |
                                                                      InheritanceFlags.ObjectInherit,
                                                                      PropagationFlags.InheritOnly,
                                                                      AccessControlType.Allow));
                        dirSec.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl,
                                                                      InheritanceFlags.ContainerInherit |
                                                                      InheritanceFlags.ObjectInherit,
                                                                      PropagationFlags.None, AccessControlType.Allow));
                        Directory.CreateDirectory(_targetDir, dirSec);
                    }
                }

                private Random _random = new Random();
                public string GetPath(StreamWrapper streamWrapper)
                {
                    if (_oldStreams != null && _oldStreams.Contains(streamWrapper))
                    {
                        _oldStreams.Remove(streamWrapper);
                        if (_currentStreams == null) _currentStreams = new HashSet<StreamWrapper>();
                        _currentStreams.Add(streamWrapper);

                        if (_oldStreams.Count == 0)
                        {
                            _oldStreams = null;
                            string dirToArchive = _oldTagetDir;
                            //atempt to archive it now, if not possible it will be archived after midnight
                            TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(120 + _random.NextDouble() * 200),
                                () => LogArchivingHelper.ArchiveDirectory(Directory.GetParent(dirToArchive).FullName, true));
                        }
                    }

                    if (_currentStreams != null && !_currentStreams.Contains(streamWrapper))
                    {
                        _currentStreams.Add(streamWrapper);
                    }

                    return _targetDir;
                }

                public bool IsActual(StreamWrapper streamWrapper, int fileCnt)
                {
                    if (DateTime.UtcNow.Day != _targetDirCreationDay || fileCnt > MAX_FILES_IN_FOLDER)
                    {
                        _oldStreams = _currentStreams;
                        _currentStreams = new HashSet<StreamWrapper>();
                        _oldTagetDir = _targetDir;

                        foreach (StreamWrapper sw in _oldStreams)
                        {
                            sw.FilesCount = 0;
                        }

                        CreateDir();

                        return false;
                    }
                    else if (_oldStreams != null && _oldStreams.Contains(streamWrapper))
                    {
                        return false;
                    }

                    return true;
                }
            }
        }

        private class Logger : ILogger
        {
            private readonly ParentLogger _parentLogger;
            private readonly int _loggerId;
            private readonly bool _isLessImportant;
            private readonly bool _doNotLogDuringLowDiskSpace;

            internal Logger(int id, ParentLogger parentLogger)
                : this(id, parentLogger, false, true)
            { }

            internal Logger(int id, ParentLogger parentLogger, bool isLessImportant)
                : this(id, parentLogger, isLessImportant, true)
            { }

            internal Logger(int id, ParentLogger parentLogger, bool isLessImportant, bool doNotLogDuringLowDiskSpace)
            {
                this._parentLogger = parentLogger;
                this._loggerId = id;
                this._isLessImportant = isLessImportant;
                this._doNotLogDuringLowDiskSpace = doNotLogDuringLowDiskSpace;
            }



            //
            // Following methods might maybe rather use blocking qeue and an async wroker
            //  that would THEN log to NLOG, as that will likely be far more efficent than using
            //  NLog built in asynchronous wrapper (as there needs to be 1 wrapper per device)
            //

            public void Log(LogLevel level, string message)
            {
                if (!this._parentLogger.BypassLoggingEntirely)
                    _parentLogger.LogAsync(new LogEvent(_loggerId, level, message, this._isLessImportant));
            }

            public void Log(LogLevel level, IPooledBufferSegment pooledBuffer)
            {
                if (!this._parentLogger.BypassLoggingEntirely)
                    _parentLogger.LogAsync(new LogEvent(_loggerId, level, pooledBuffer, this._isLessImportant));
            }

            public void Log(LogLevel level, string message, IPooledBufferSegment pooledBuffer)
            {
                if (!this._parentLogger.BypassLoggingEntirely)
                    _parentLogger.LogAsync(new LogEvent(_loggerId, level, message, pooledBuffer, this._isLessImportant));
            }

            //public void LogWithDt(LogLevel level, DateTime correctedTimestamp, string message)
            //{
            //    _parentLogger.LogAsync(new LogEvent(_loggerId, level, message, this._isLessImportant, correctedTimestamp));
            //}


            // Following is the way how to include caller information in our logging
            //

            //public void Log(
            //    LogLevel level, 
            //    string message,
            //    [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            //    [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            //    [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
            //{
            //    //this._logger.Log(NLog.LogLevel.FromOrdinal((int)level), message);
            //      LogEventInfo logEvent = new LogEventInfo(level, loggerName, null, messageFmt, args);
            //      logEvent.Properties.Add("callerpath", sourceFilePath);
            //      logEvent.Properties.Add("callermember", memberName);
            //      logEvent.Properties.Add("callerline", sourceLineNumber);
            //      //in config layout you can then specify ${event-context:item=callerpath}:${event-context:item=callermember}(${event-context:item=callerline})
            //      //DOWNSIDE - it is NOT possible to combine optional arguments and params argument (withou awkward syntax explicitly creating the object array)
            //      //  so all overloads would need to be specified explicitly
            //}


            public void Log(LogLevel level, string messageFmt, params object[] args)
            {
                if (!this._parentLogger.BypassLoggingEntirely)
                    _parentLogger.LogAsync(new LogEvent(_loggerId, level, messageFmt, this._isLessImportant, args));
            }

            //public void Log(LogLevel level, IFormatProvider formatProvider, string messageFmt, params object[] args)
            //{
            //    this._logger.Log(NLog.LogLevel.FromOrdinal((int)level), formatProvider, messageFmt, args);
            //}

            public void LogException(LogLevel level, string message, Exception e)
            {
                if (!this._parentLogger.BypassLoggingEntirely)
                    _parentLogger.LogAsync(new LogEvent(_loggerId, level, message, e, this._isLessImportant));
            }

            public void LogException(LogLevel level, Exception e, string messageFmt, params object[] args)
            {
                if (!this._parentLogger.BypassLoggingEntirely)
                    _parentLogger.LogAsync(new LogEvent(_loggerId, level, e, messageFmt, this._isLessImportant, args));
            }

            //public void LogException(Exception e)
            //{
            //    this._logger.LogException(NLog.LogLevel.FromOrdinal((int)LogLevel.Error),
            //                              "Experienced unexpected exception", e);
            //}

            public bool IsEnabled(LogLevel level)
            {
                return true;
            }

            public bool IsLowDiskSpaceConstraintLoggingOn
            {
                get { return this._doNotLogDuringLowDiskSpace && this._parentLogger.IsLowDiskSpace; }
            }

            public event Action<bool> IsLowDiskSpaceChanged
            {
                add { this._parentLogger.IsLowDiskSpaceChanged += value; }

                remove { this._parentLogger.IsLowDiskSpaceChanged -= value; }
            }
        }
    }

    public class NLogLogFactory : ILogFactory
    {
        public ILogger GetLogger(string logName)
        {
            return new Logger(NLog.LogManager.GetLogger(logName));
        }

        public ILogger GetLogger(string logName, bool isLessImportant)
        {
            return new Logger(NLog.LogManager.GetLogger(logName));
        }

        public ILogger GetLogger(string logName, bool isLessImportant, bool doNotLogDuringLowDiskSpace)
        {
            return new Logger(NLog.LogManager.GetLogger(logName));
        }

        private class Logger : ILogger
        {
            public Logger(NLog.Logger nlogger)
            {
                this._logger = nlogger;
            }

            private readonly NLog.Logger _logger;

            //
            // Following methods might maybe rather use blocking qeue and an async wroker
            //  that would THEN log to NLOG, as that will likely be far more efficent than using
            //  NLog built in asynchronous wrapper (as there needs to be 1 wrapper per device)
            //

            public void Log(LogLevel level, string message)
            {
                this._logger.Log(NLog.LogLevel.FromOrdinal((int)level), message);
            }


            // Following is the way how to include caller information in our logging
            //

            //public void Log(
            //    LogLevel level, 
            //    string message,
            //    [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            //    [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            //    [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
            //{
            //    //this._logger.Log(NLog.LogLevel.FromOrdinal((int)level), message);
            //      LogEventInfo logEvent = new LogEventInfo(level, loggerName, null, messageFmt, args);
            //      logEvent.Properties.Add("callerpath", sourceFilePath);
            //      logEvent.Properties.Add("callermember", memberName);
            //      logEvent.Properties.Add("callerline", sourceLineNumber);
            //      //in config layout you can then specify ${event-context:item=callerpath}:${event-context:item=callermember}(${event-context:item=callerline})
            //      //DOWNSIDE - it is NOT possible to combine optional arguments and params argument (withou awkward syntax explicitly creating the object array)
            //      //  so all overloads would need to be specified explicitly
            //}


            public void Log(LogLevel level, string messageFmt, params object[] args)
            {
                this._logger.Log(NLog.LogLevel.FromOrdinal((int)level), messageFmt, args);
            }

            public void Log(LogLevel level, IFormatProvider formatProvider, string messageFmt, params object[] args)
            {
                this._logger.Log(NLog.LogLevel.FromOrdinal((int)level), formatProvider, messageFmt, args);
            }

            public void LogException(LogLevel level, string message, Exception e)
            {
                this._logger.LogException(NLog.LogLevel.FromOrdinal((int)level), message, e);
            }

            public void LogException(LogLevel level, Exception e, string messageFmt, params object[] args)
            {
                this._logger.LogException(NLog.LogLevel.FromOrdinal((int)level), string.Format(messageFmt, args), e);
            }

            public void LogException(Exception e)
            {
                this._logger.LogException(NLog.LogLevel.FromOrdinal((int)LogLevel.Error),
                                          "Experienced unexpected exception", e);
            }

            public bool IsEnabled(LogLevel level)
            {
                return this._logger.IsEnabled(NLog.LogLevel.FromOrdinal((int)level));
            }


            public bool IsLowDiskSpaceConstraintLoggingOn
            {
                get { return false; }
            }


            public event Action<bool> IsLowDiskSpaceChanged;


            public void LogWithDt(LogLevel level, DateTime correctedTime, string message)
            {
                this._logger.Log(NLog.LogLevel.FromOrdinal((int)level), message);
            }

            public void Log(LogLevel level, IPooledBufferSegment pooledBuffer)
            {
                this._logger.Log(NLog.LogLevel.FromOrdinal((int)level),
                    System.Text.Encoding.ASCII.GetString(pooledBuffer.Buffer, pooledBuffer.ContentStart,
                        pooledBuffer.ContentSize));
            }

            public void Log(LogLevel level, string message, IPooledBufferSegment pooledBuffer)
            {
                this._logger.Log(NLog.LogLevel.FromOrdinal((int)level), message +
                    System.Text.Encoding.ASCII.GetString(pooledBuffer.Buffer, pooledBuffer.ContentStart,
                        pooledBuffer.ContentSize));
            }
        }


        public void RegisterFatalRecordsConsumer(Action<string> onNewFatalRecord)
        {
            return;
        }
    }
}