﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.MessageBus;

namespace Kreslik.Integrator.IntegratorClientHost
{
    static class Program
    {
        static void PrintHelp()
        {
            Console.WriteLine("Usage: Kreslik.Integrator.IntegratorClientHost.exe [/ConfigurationIdentity <configuration Id>] [/ConfigurationString <configuration string>]");
            Console.WriteLine("Any extra argument will cause display of this help");
            Console.WriteLine("If ConfigurationIdentity is missing, the DefaultIntegratorClientCofigurationIdentity from configuration is used if present (otherwise you'll see this message)");
            Console.WriteLine("If ConfigurationString is missing, the DefaultConfigurationParameter from configuration is used if present (otherwise you'll see this message)");
            Console.WriteLine();
            Console.WriteLine("Press a key to exit...");
            Console.ReadKey();
        }

        static IntegratorClientHostSettings.IntegratorClientConfiguration ReadConfiguration(string[] args)
        {
            string clientConfigurationId =
                IntegratorClientHostSettings.IntegratorClientHostBehavior.DefaultIntegratorClientCofigurationIdentity;
            string clientConfigurationString = null;
            char[] switchPrefixes = new char[] { '/', '-' };

            for (int argIdx = 0; argIdx < args.Length; argIdx++)
            {
                if (switchPrefixes.Contains(args[argIdx][0]) && argIdx + 1 < args.Length)
                {
                    switch (args[argIdx].Substring(1))
                    {
                        case "ConfigurationIdentity":
                            clientConfigurationId = args[argIdx + 1];
                            argIdx++;
                            break;
                        case "ConfigurationString":
                            clientConfigurationString = args[argIdx + 1];
                            argIdx++;
                            break;
                        default:
                            Console.WriteLine("Unexpected argument {0}", args[argIdx]);
                            Console.WriteLine();
                            PrintHelp();
                            return null;
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(clientConfigurationId))
            {
                Console.WriteLine("Client configuration Id not specified on commandiline nor in config file");
                Console.WriteLine();
                PrintHelp();
                return null;
            }

            var clientconfigs =
                IntegratorClientHostSettings.IntegratorClientHostBehavior.IntegratorClientConfigurations.Where(
                    config => config.ConfigurationIdentity.Equals(clientConfigurationId));

            if (clientconfigs.Count() != 1)
            {
                throw new Exception(string.Format("Expected exactly one IntegratorClientConfiguration element with ConfigurationIdentity == {0}, but found {1}", clientConfigurationId, clientconfigs.Count()));
            }

            var clientConfig = clientconfigs.First();

            clientConfigurationString = clientConfigurationString ?? clientConfig.DefaultConfigurationParameter;

            clientConfig.ConfigurationIdentity = clientConfigurationId;
            clientConfig.DefaultConfigurationParameter = clientConfigurationString;

            return clientConfig;
        }

        static IIntegratorClient CreateIntegratorClientInstance(IntegratorClientHostSettings.IntegratorClientConfiguration clientConfig, ILogger tradingClientServiceLogger)
        {
            IIntegratorClient integratorClient = null;
            try
            {
                Type tradingClientType;
                if (string.IsNullOrWhiteSpace(clientConfig.AssemblyFullPath))
                {
                    tradingClientType = Type.GetType(string.Format("{0}, {1}", clientConfig.ClientTypeName, clientConfig.AssemblyNameWithoutExtension), true);
                }
                else
                {
                    Assembly clientAssembly = Assembly.LoadFrom(clientConfig.AssemblyFullPath);
                    tradingClientType = clientAssembly.GetType(clientConfig.ClientTypeName, true);
                }

                integratorClient = Activator.CreateInstance(tradingClientType) as IIntegratorClient;
            }
            catch (Exception e)
            {
                tradingClientServiceLogger.LogException(LogLevel.Fatal, e, "Couldn't create the TradingClient object of type {0}", clientConfig.ClientTypeName);
            }

            return integratorClient;
        }

        private static Counterparty counterpartyToForceInitializationOrder = Counterparty.BOA;

        [STAThread]
        static void Main(string[] args)
        {
            var clientConfig = ReadConfiguration(args);
            if (clientConfig == null)
            {
                return;
            }

            if (clientConfig.ClientInterfaceType == ClientInterfaceType.Form)
            {
                Console.SetOut(TextWriter.Null);
                Win32Imports.FreeConsole();
                Application.EnableVisualStyles();
            }

            ILogFactory logFactory = LogFactory.Instance;
            ILogger tradingClientLogger =
                logFactory.GetLogger("IntegratorClient_" + clientConfig.ClientFriendlyName);
            ILogger tradingClientServiceLogger =
                logFactory.GetLogger("IntegratorClientHost_" + clientConfig.ClientFriendlyName);


            IIntegratorClient integratorClient = CreateIntegratorClientInstance(clientConfig, tradingClientServiceLogger);
            if (integratorClient == null)
            {
                tradingClientServiceLogger.Log(LogLevel.Fatal, "Couldn't create the TradingClient object (missing reference? Incorrect type?)");
                System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
                return;
            }

            TradingClientGateway tradingClientGateway = new TradingClientGateway(clientConfig.ClientFriendlyName, tradingClientServiceLogger);
            if (tradingClientGateway.IsClosed)
            {
                tradingClientServiceLogger.Log(LogLevel.Error, "Couldn't start trading client gateway (was the proxy and integrator running?). Exiting...");
                System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
                return;
            }

            bool closed = false;
            bool started = false;

            tradingClientGateway.GatewayClosed +=
                reason =>
                tradingClientServiceLogger.Log(LogLevel.Warn,
                                               "Signaling stop to TradingClient as gateway is being closed. Reason: {0}",
                                               reason);
            tradingClientGateway.GatewayClosed += s =>
            {
                if (!closed)
                    integratorClient.Stop(s);
            };
            tradingClientGateway.GatewayClosed += s => closed = true;


            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += (sender, eventArgs) =>
            {
                tradingClientServiceLogger.LogException(LogLevel.Fatal, (Exception)eventArgs.ExceptionObject,
                                                        "Integrator client experienced unhandled exception");
                bool handled = false;
                try
                {
                    handled = integratorClient.TryHandleUnhandledException((Exception) eventArgs.ExceptionObject);
                }
                catch (Exception e)
                {
                    tradingClientServiceLogger.LogException(LogLevel.Fatal, e,
                                                        "Integrator client experienced unhandled exception - during handling another unhandled exception");
                }

                if (!closed && !handled)
                {
                    tradingClientServiceLogger.Log(LogLevel.Info, "Client didn't handle the exception - so closing it.");
                    tradingClientGateway.CloseGateway();
                    integratorClient.Stop("Integrator client experienced unhandled exception");
                    closed = true;
                }
            };

            tradingClientGateway.ExceptionDuringDispatchingOccured += exception =>
                {
                    tradingClientServiceLogger.LogException(LogLevel.Fatal, exception,
                                                            "Integrator client experienced unhandled exception");
                    bool handled = false;
                    try
                    {
                        handled = integratorClient.TryHandleUnhandledException(exception);
                    }
                    catch (Exception e)
                    {
                        tradingClientServiceLogger.LogException(LogLevel.Fatal, e,
                                                                "Integrator client experienced unhandled exception - during handling another unhandled exception");
                    }

                    if (!closed && !handled)
                    {
                        tradingClientServiceLogger.Log(LogLevel.Info,
                                                       "Client didn't handle the exception - so closing it.");
                        tradingClientGateway.CloseGateway();
                        integratorClient.Stop("Integrator client experienced unhandled exception");
                        closed = true;
                    }
                };

            tradingClientServiceLogger.Log(LogLevel.Info,
                                           "Configuring and starting IntegratorClient with configuration identifier = [{0}] and configuration parameter = [{1}]",
                                           clientConfig.ConfigurationIdentity,
                                           clientConfig.DefaultConfigurationParameter);
            try
            {
                if (clientConfig.ClientInterfaceType == ClientInterfaceType.Form)
                {
                    Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                }
                integratorClient.Configure(tradingClientLogger, tradingClientGateway, clientConfig.DefaultConfigurationParameter);
                integratorClient.Start();
                started = true;
            }
            catch (Exception e)
            {
                tradingClientLogger.LogException(LogLevel.Fatal, e, "Unexpected exception during Trading Client configuration and starting");
            }

            
            if (started)
            {
                //GUI mode
                if (clientConfig.ClientInterfaceType == ClientInterfaceType.Form)
                {
                    Form form = Form.ActiveForm;
                    if (form == null)
                    {
                        if (Application.OpenForms.Count > 0)
                        {
                            form = Application.OpenForms[0];
                        }
                    }

                    if (form == null)
                    {
                        tradingClientServiceLogger.Log(LogLevel.Fatal,
                                                       "Integrator Client hasn't created any form. Exiting main function without closing the gateway.");
                        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
                        return;
                    }

                    string formName = string.IsNullOrEmpty(form.Name) ? "<Form Name was not specified>" : form.Name;
                    tradingClientServiceLogger.Log(LogLevel.Info,
                                                   "Found form {0} created by client. Will start message pump and wait for it to exit.",
                                                   formName);

                    form.FormClosed += (sender, arguments) => Application.Exit();

                    Application.ThreadException += (sender, eventArgs) =>
                    {
                        tradingClientServiceLogger.LogException(LogLevel.Fatal, (Exception)eventArgs.Exception,
                                                                "Integrator client experienced unhandled exception");
                        bool handled = false;
                        try
                        {
                            handled = integratorClient.TryHandleUnhandledException((Exception)eventArgs.Exception);
                        }
                        catch (Exception e)
                        {
                            tradingClientServiceLogger.LogException(LogLevel.Fatal, e,
                                                                "Integrator client experienced unhandled exception - during handling another unhandled exception");
                        }

                        if (!closed && !handled)
                        {
                            tradingClientServiceLogger.Log(LogLevel.Info, "Client didn't handle the exception - so closing it.");
                            tradingClientGateway.CloseGateway();
                            integratorClient.Stop("Integrator client experienced unhandled exception");
                            closed = true;
                        }
                    };

                    Application.Run(form);
                }
                //Console mode
                else
                {
                    tradingClientServiceLogger.Log(LogLevel.Info,
                                                   "Running client in console mode. Connected to pipe: {0}",
                                                   MessageBusSettings.Sections.ClientConnectionInfo.IntegratorServicePipeName);

                    Console.WriteLine(
                        "Running client in console mode. Connected to pipe: {0}. Press key (twice) to exit client",
                        MessageBusSettings.Sections.ClientConnectionInfo.IntegratorServicePipeName);
                    Console.ReadKey();
                    Console.WriteLine("Pres key once more to exit the running action");
                    Console.ReadKey();
                    Console.WriteLine("Exiting the running integrator");
                }
            }


            if (!closed)
            {
                closed = true;
                tradingClientGateway.CloseGateway();
                integratorClient.Stop("TradingClientService shutdown requested");
            }
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
        }
    }
}
