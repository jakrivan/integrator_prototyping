﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Kreslik.Integrator.Common;
using System.Linq;
using Kreslik.Integrator.IntegratorClientHost.Properties;

namespace Kreslik.Integrator.IntegratorClientHost
{
    public enum ClientInterfaceType
    {
        Console,
        Form
    }

    public class IntegratorClientHostSettings
    {
        public string DefaultIntegratorClientCofigurationIdentity { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public static IntegratorClientHostSettings IntegratorClientHostBehavior
        {
            get
            {
                if (_integratorClientHostBehavior == null)
                {
                    _integratorClientHostBehavior = SettingsInitializator.Instance
                        .ReadSettings<IntegratorClientHostSettings>(
                            @"Kreslik.Integrator.IntegratorClientHost.exe",
                            Resources.Kreslik_Integrator_IntegratorClientHost_exe
                        );
                }

                return _integratorClientHostBehavior;
            }
        }

        private static IntegratorClientHostSettings _integratorClientHostBehavior;

        public List<IntegratorClientConfiguration> IntegratorClientConfigurations { get; set; }

        public class IntegratorClientConfiguration
        {
            public string ConfigurationIdentity { get; set; }
            public ClientInterfaceType ClientInterfaceType { get; set; }
            public string ClientTypeName { get; set; }
            public string AssemblyNameWithoutExtension { get; set; }
            public string AssemblyFullPath { get; set; }
            public string DefaultConfigurationParameter { get; set; }

            private string _clientFriendlyName;

            public string ClientFriendlyName
            {
                get
                {
                    if (string.IsNullOrWhiteSpace(_clientFriendlyName))
                    {
                        _clientFriendlyName = string.Format("{0}_{1}", Environment.MachineName,
                                                            Process.GetCurrentProcess().Id);
                    }

                    return _clientFriendlyName;
                }

                set { _clientFriendlyName = value; }
            }
        }
    }
}
