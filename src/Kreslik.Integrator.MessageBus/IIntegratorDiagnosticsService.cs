﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.MessageBus
{
    public interface IIntegratorDiagnosticsClient
    {
        //[OperationContract(IsOneWay = true)]
        //void DealsStatisticsChanging();
    }

    [ServiceContract(/*CallbackContract = typeof(IIntegratorDiagnosticsClient)*/)]
    public interface IIntegratorDiagnosticsService : IRemoteCommandWorker
    {
        [OperationContract]
        PriceStreamsMonitoringInfo GetPriceStreamsMonitoringInfo();

        [OperationContract]
        void Ping();
    }
}
