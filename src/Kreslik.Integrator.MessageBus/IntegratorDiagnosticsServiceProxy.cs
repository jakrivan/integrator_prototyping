﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.MessageBus
{
    public class IntegratorDiagnosticsServiceProxy : System.ServiceModel.ClientBase<IIntegratorDiagnosticsService>, IIntegratorDiagnosticsService
    {
        public IntegratorDiagnosticsServiceProxy(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        public PriceStreamsMonitoringInfo GetPriceStreamsMonitoringInfo()
        {
            return Channel.GetPriceStreamsMonitoringInfo();
        }

        public void Ping()
        {
            Channel.Ping();
        }

        public IntegratorRequestResult TrySendCommand(string command)
        {
            return Channel.TrySendCommand(command);
        }
    }
}
