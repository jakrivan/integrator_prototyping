﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.MessageBus
{
    //following was needed when callback interface was a composition of multiple interfaces, this isnot true anymore
    ////this is workaround for WCF unable to walh inheritance tree of service callback
    ////see http://social.msdn.microsoft.com/Forums/en-US/wcf/thread/ef896836-dec1-4fa6-9956-e3a4958643ce
    //[ServiceContract(CallbackContract = typeof(IOrderResultDispatchGateway))]
    //public interface IIntegratorServiceBase
    //{
    //    //This must remain empty!
    //}

    [ServiceContract]
    public interface IRemoteClientGateway
    {
        [OperationContract()]
        IntegratorRequestResult SubmitRequest(string localGatewayIdentifier, IntegratorRequestInfo requestInfo);
    }

    [DataContract]
    public class ClientRegistrationResult
    {
        private ClientRegistrationResult(string uniqueClientIdentifier, bool registrationSucceded,
                                         string registrationError,
                                         ConnectionChainBuildVersioningInfo connectionChainBuildVersioningInfo)
        {
            this.UniqueClientIdentifier = uniqueClientIdentifier;
            this.RegistrationSucceded = registrationSucceded;
            this.RegistrationError = registrationError;
            this.ConnectionChainBuildVersioningInfo = connectionChainBuildVersioningInfo;
        }

        public static ClientRegistrationResult CreateSuccessResult(string uniqueClientIdentifier,
                                                                   ConnectionChainBuildVersioningInfo
                                                                       connectionChainBuildVersioningInfo)
        {
            return new ClientRegistrationResult(uniqueClientIdentifier, true, null, connectionChainBuildVersioningInfo);
        }

        public static ClientRegistrationResult CreateFailedResult(string failureReason,
                                                                  ConnectionChainBuildVersioningInfo
                                                                      connectionChainBuildVersioningInfo)
        {
            return new ClientRegistrationResult(null, false, failureReason, connectionChainBuildVersioningInfo);
        }

        [DataMember]
        public string UniqueClientIdentifier { get; private set; }

        [DataMember]
        public bool RegistrationSucceded { get; private set; }

        [DataMember]
        public string RegistrationError { get; private set; }

        [DataMember]
        public ConnectionChainBuildVersioningInfo ConnectionChainBuildVersioningInfo { get; private set; }
    }

    [DataContract]
    public class ClientRegistrationRequest
    {
        public ClientRegistrationRequest(string localGatewayName, PriceStreamTransferMode priceStreamTransferMode,
                                         TimeSpan priceThrottlingInterval,
                                         BuildVersioningInfo registeringClientVersionInfo,
                                         bool forceConnectOnContractVersionMismatch,
                                         bool keepLocalClientNameAsUniqueRemotingIdentifer)
        {
            this.LocalGatewayName = localGatewayName;
            this.PriceStreamTransferMode = priceStreamTransferMode;
            this.PriceThrottlingInterval = priceThrottlingInterval;
            this.RegisteringClientVersionInfo = registeringClientVersionInfo;
            this.ForceConnectOnContractVersionMismatch = forceConnectOnContractVersionMismatch;
            this.KeepLocalClientNameAsUniqueRemotingIdentifer = keepLocalClientNameAsUniqueRemotingIdentifer;
        }

        [DataMember]
        public string LocalGatewayName { get; private set; }

        [DataMember]
        public PriceStreamTransferMode PriceStreamTransferMode { get; private set; }

        [DataMember]
        public TimeSpan PriceThrottlingInterval { get; private set; }

        [DataMember]
        public BuildVersioningInfo RegisteringClientVersionInfo { get; private set; }

        [DataMember]
        public bool ForceConnectOnContractVersionMismatch { get; private set; }

        [DataMember]
        public bool KeepLocalClientNameAsUniqueRemotingIdentifer { get; private set; }
    }

    [ServiceContract]
    public interface IRemoteService
    {
        [OperationContract()]
        ClientRegistrationResult Register(ClientRegistrationRequest clientRegistrationRequest);

        [OperationContract()]
        IntegratorRequestResult Unregister(string localGatewayIdentifier);

        [OperationContract()]
        IntegratorRequestResult RecreateSession(string localGatewayIdentifier);

        [OperationContract()]
        void Ping();
    }


    [ServiceContract(CallbackContract = typeof(ITradingServiceEndpoint))]
    public interface IIntegratorService : IRemoteClientGateway, IRemoteService
    { }
}
