﻿using System;
using System.Collections.Generic;
using System.IO;
using Kreslik.Integrator.Common;
using System.Linq;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.MessageBus.Properties;

namespace Kreslik.Integrator.MessageBus
{
    public class MessageBusSettings
    {
        public enum ConnectionType
        {
            Localhost,
            PrivateIPAddress,
            PublicIPAddress,
            Hostname
        }

        public class MessageBusSettings_IntegratorEndpointConnectionInfo
        {
            public static MessageBusSettings_IntegratorEndpointConnectionInfo CreateDefaultInstance()
            {
                return new MessageBusSettings_IntegratorEndpointConnectionInfo() { ConnectionType = ConnectionType.Localhost };
            }

            public static string SettingName
            {
                get { return @"Kreslik.Integrator.MessageBusSettings_IntegratorEndpointConnectionInfo"; }
            }

            public ConnectionType ConnectionType { get; set; }
        }

        public class MessageBusSettings_ClientConnectionInfo
        {
            public static MessageBusSettings_ClientConnectionInfo CreateDefaultInstance()
            {
                return new MessageBusSettings_ClientConnectionInfo() { IntegratorServicePipeName = "Foo" };
            }

            public static string SettingName
            {
                get { return @"Kreslik.Integrator.MessageBusSettings_ClientConnectionInfo"; }
            }

            public string IntegratorServicePipeName { get; set; }
        }

        public class MessageBusSettings_ConnectionKeeping
        {
            public static MessageBusSettings_ConnectionKeeping CreateDefaultInstance()
            {
                return new MessageBusSettings_ConnectionKeeping();
            }

            public static string SettingName
            {
                get { return @"Kreslik.Integrator.MessageBusSettings_ConnectionKeeping"; }
            }

            [System.Xml.Serialization.XmlIgnore]
            public TimeSpan MaximumAllowedDisconnectedInterval { get; set; }

            [System.Xml.Serialization.XmlElement("MaximumAllowedDisconnectedInterval_Seconds")]
            public int MaximumAllowedDisconnectedIntervalXml
            {
                get { return (int)MaximumAllowedDisconnectedInterval.TotalSeconds; }
                set { MaximumAllowedDisconnectedInterval = TimeSpan.FromSeconds(value); }
            }

            [System.Xml.Serialization.XmlIgnore]
            public TimeSpan DisconnectedSessionReconnectInterval { get; set; }

            [System.Xml.Serialization.XmlElement("DisconnectedSessionReconnectInterval_Seconds")]
            public int DisconnectedSessionReconnectIntervalXml
            {
                get { return (int)DisconnectedSessionReconnectInterval.TotalSeconds; }
                set { DisconnectedSessionReconnectInterval = TimeSpan.FromSeconds(value); }
            }

            [System.Xml.Serialization.XmlIgnore]
            public TimeSpan SessionHealthCheckInterval { get; set; }

            [System.Xml.Serialization.XmlElement("SessionHealthCheckInterval_Seconds")]
            public int SessionHealthCheckIntervalXml
            {
                get { return (int)SessionHealthCheckInterval.TotalSeconds; }
                set { SessionHealthCheckInterval = TimeSpan.FromSeconds(value); }
            }
        }

        public class MessageBusSettings_PriceTransferBehavior
        {
            public static MessageBusSettings_PriceTransferBehavior CreateDefaultInstance()
            {
                return new MessageBusSettings_PriceTransferBehavior();
            }

            public static string SettingName
            {
                get { return @"Kreslik.Integrator.MessageBusSettings_PriceTransferBehavior"; }
            }

            public PriceStreamTransferMode PriceStreamTransferMode { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public TimeSpan PerSymbolPriceThrottlingInterval { get; set; }

            [System.Xml.Serialization.XmlElement("PerSymbolPriceThrottlingInterval_Milliseconds")]
            public int PerSymbolPriceThrottlingIntervalXml
            {
                get { return (int)PerSymbolPriceThrottlingInterval.TotalMilliseconds; }
                set { PerSymbolPriceThrottlingInterval = TimeSpan.FromMilliseconds(value); }
            }
        }

        public class MessageBusSettings_ContractMismatchHandling
        {
            public static MessageBusSettings_ContractMismatchHandling CreateDefaultInstance()
            {
                return new MessageBusSettings_ContractMismatchHandling();
            }

            public static string SettingName
            {
                get { return @"Kreslik.Integrator.MessageBusSettings_ContractMismatchHandling"; }
            }

            public bool ForceConnectRemotingClientOnContractVersionMismatch { get; set; }
        }

        private bool _integratorEndpointConnectionInfoInitialized;
        private MessageBusSettings_IntegratorEndpointConnectionInfo _integratorEndpointConnectionInfo;

        private bool _clientConnectionInfoInitialized;
        private MessageBusSettings_ClientConnectionInfo _clientConnectionInfo;

        private bool _connectionKeepingInitialized;
        private MessageBusSettings_ConnectionKeeping _connectionKeeping;

        private bool _priceTransferBehaviorInitialized;
        private MessageBusSettings_PriceTransferBehavior _priceTransferBehavior;

        private bool _contractMismatchHandlingInitialized;
        private MessageBusSettings_ContractMismatchHandling _contractMismatchHandling;

        public MessageBusSettings_IntegratorEndpointConnectionInfo IntegratorEndpointConnectionInfo
        {
            get
            {
                if (!_integratorEndpointConnectionInfoInitialized)
                {
                    this.ErrorOnUninitializedAccess("SplitterConnectionInfo");
                }
                return this._integratorEndpointConnectionInfo;
            }

            private set { _integratorEndpointConnectionInfo = value; }
        }

        public MessageBusSettings_ClientConnectionInfo ClientConnectionInfo
        {
            get
            {
                if (!_clientConnectionInfoInitialized)
                {
                    this.ErrorOnUninitializedAccess("ClientConnectionInfo");
                }
                return this._clientConnectionInfo;
            }

            private set { _clientConnectionInfo = value; }
        }

        public MessageBusSettings_ConnectionKeeping ConnectionKeeping
        {
            get
            {
                if (!_connectionKeepingInitialized)
                {
                    this.ErrorOnUninitializedAccess("ConnectionKeeping");
                }
                return this._connectionKeeping;
            }

            private set { _connectionKeeping = value; }
        }

        public MessageBusSettings_PriceTransferBehavior PriceTransferBehavior
        {
            get
            {
                if (!_priceTransferBehaviorInitialized)
                {
                    this.ErrorOnUninitializedAccess("PriceTransferBehavior");
                }
                return this._priceTransferBehavior;
            }

            private set { _priceTransferBehavior = value; }
        }

        public MessageBusSettings_ContractMismatchHandling ContractMismatchHandling
        {
            get
            {
                if (!_contractMismatchHandlingInitialized)
                {
                    this.ErrorOnUninitializedAccess("ContractMismatchHandling");
                }
                return this._contractMismatchHandling;
            }

            private set { _contractMismatchHandling = value; }
        }


        private void ErrorOnUninitializedAccess(string settingName)
        {
            LogFactory.Instance.GetLogger(null)
                            .Log(LogLevel.Fatal,
                                 "Attempt to access uninitialized setting {0}, is it configured for your environment [{1}]?",
                                 settingName, SettingsInitializator.Instance.EnvironmentName);
        }

        private MessageBusSettings()
        {
            if (SettingsInitializator.Instance.HasSettings(MessageBusSettings_IntegratorEndpointConnectionInfo.SettingName))
            {
                _integratorEndpointConnectionInfoInitialized = true;
                _integratorEndpointConnectionInfo =
                    SettingsInitializator.Instance.ReadSettings<MessageBusSettings_IntegratorEndpointConnectionInfo>(
                        MessageBusSettings_IntegratorEndpointConnectionInfo.SettingName,
                        Resources.ResourceManager.GetString(MessageBusSettings_IntegratorEndpointConnectionInfo.SettingName.Replace('.', '_')));
            }
            else
            {
                _integratorEndpointConnectionInfo = MessageBusSettings_IntegratorEndpointConnectionInfo.CreateDefaultInstance();
            }

            if (SettingsInitializator.Instance.HasSettings(MessageBusSettings_ClientConnectionInfo.SettingName))
            {
                _clientConnectionInfoInitialized = true;
                _clientConnectionInfo =
                    SettingsInitializator.Instance.ReadSettings<MessageBusSettings_ClientConnectionInfo>(
                        MessageBusSettings_ClientConnectionInfo.SettingName,
                        Resources.ResourceManager.GetString(MessageBusSettings_ClientConnectionInfo.SettingName.Replace('.', '_')));
            }
            else
            {
                _clientConnectionInfo = MessageBusSettings_ClientConnectionInfo.CreateDefaultInstance();
            }

            if (SettingsInitializator.Instance.HasSettings(MessageBusSettings_ConnectionKeeping.SettingName))
            {
                _connectionKeepingInitialized = true;
                _connectionKeeping =
                    SettingsInitializator.Instance.ReadSettings<MessageBusSettings_ConnectionKeeping>(
                        MessageBusSettings_ConnectionKeeping.SettingName,
                        Resources.ResourceManager.GetString(MessageBusSettings_ConnectionKeeping.SettingName.Replace('.', '_')));
            }
            else
            {
                _connectionKeeping = MessageBusSettings_ConnectionKeeping.CreateDefaultInstance();
            }

            if (SettingsInitializator.Instance.HasSettings(MessageBusSettings_PriceTransferBehavior.SettingName))
            {
                _priceTransferBehaviorInitialized = true;
                _priceTransferBehavior =
                    SettingsInitializator.Instance.ReadSettings<MessageBusSettings_PriceTransferBehavior>(
                        MessageBusSettings_PriceTransferBehavior.SettingName,
                        Resources.ResourceManager.GetString(MessageBusSettings_PriceTransferBehavior.SettingName.Replace('.', '_')));
            }
            else
            {
                _priceTransferBehavior = MessageBusSettings_PriceTransferBehavior.CreateDefaultInstance();
            }

            if (SettingsInitializator.Instance.HasSettings(MessageBusSettings_ContractMismatchHandling.SettingName))
            {
                _contractMismatchHandlingInitialized = true;
                _contractMismatchHandling =
                    SettingsInitializator.Instance.ReadSettings<MessageBusSettings_ContractMismatchHandling>(
                        MessageBusSettings_ContractMismatchHandling.SettingName,
                        Resources.ResourceManager.GetString(MessageBusSettings_ContractMismatchHandling.SettingName.Replace('.', '_')));
            }
            else
            {
                _contractMismatchHandling = MessageBusSettings_ContractMismatchHandling.CreateDefaultInstance();
            }
        }

        private static MessageBusSettings _messageBusSettings = new MessageBusSettings();

        public static MessageBusSettings Sections
        {
            get { return _messageBusSettings; }
        }





        
        

        

        

        

        //[System.Xml.Serialization.XmlIgnore]
        //public static MessageBusSettings MessageBusBehavior
        //{
        //    get
        //    {
        //        if (_messageBusBehavior == null)
        //        {
        //            //Allow local configuration of message bus
        //            string callingDir = Path.GetDirectoryName(System.Reflection.Assembly.GetCallingAssembly().Location);
        //            string configXml = Path.Combine(callingDir, @"Kreslik.Integrator.MessageBus.dll.xml");
        //            if (File.Exists(configXml))
        //            {
        //                _messageBusBehavior = SettingsReader.DeserializeConfiguration<MessageBusSettings>(
        //                    @"Kreslik.Integrator.MessageBus.dll.xml",
        //                    @"Kreslik.Integrator.MessageBus.dll.xsd"
        //                    );
        //            }
        //            else
        //            {
        //                _messageBusBehavior = SettingsInitializator.Instance.ReadSettings<MessageBusSettings>(
        //                    @"Kreslik.Integrator.MessageBus.dll",
        //                    @"Kreslik.Integrator.MessageBus.dll.xsd"
        //                    );
        //            }
        //        }

        //        return _messageBusBehavior;
        //    }
        //}

        //private static MessageBusSettings _messageBusBehavior;
    }

    //public class MessageBusSettings
    //{
    //    public string IntegratorServiceHost { get; set; }
    //    public int IntegratorServicePort { get; set; }
    //    public string IntegratorServicePipeName { get; set; }
        
    //    [System.Xml.Serialization.XmlIgnore]
    //    public TimeSpan MaximumAllowedDisconnectedInterval { get; set; }

    //    [System.Xml.Serialization.XmlElement("MaximumAllowedDisconnectedInterval_Seconds")]
    //    public int MaximumAllowedDisconnectedIntervalXml
    //    {
    //        get { return (int)MaximumAllowedDisconnectedInterval.TotalSeconds; }
    //        set { MaximumAllowedDisconnectedInterval = TimeSpan.FromSeconds(value); }
    //    }

    //    [System.Xml.Serialization.XmlIgnore]
    //    public TimeSpan DisconnectedSessionReconnectInterval { get; set; }

    //    [System.Xml.Serialization.XmlElement("DisconnectedSessionReconnectInterval_Seconds")]
    //    public int DisconnectedSessionReconnectIntervalXml
    //    {
    //        get { return (int)DisconnectedSessionReconnectInterval.TotalSeconds; }
    //        set { DisconnectedSessionReconnectInterval = TimeSpan.FromSeconds(value); }
    //    }

    //    [System.Xml.Serialization.XmlIgnore]
    //    public TimeSpan SessionHealthCheckInterval { get; set; }

    //    [System.Xml.Serialization.XmlElement("SessionHealthCheckInterval_Seconds")]
    //    public int SessionHealthCheckIntervalXml
    //    {
    //        get { return (int)SessionHealthCheckInterval.TotalSeconds; }
    //        set { SessionHealthCheckInterval = TimeSpan.FromSeconds(value); }
    //    }

    //    public PriceStreamTransferMode PriceStreamTransferMode { get; set; }


    //    [System.Xml.Serialization.XmlIgnore]
    //    public TimeSpan PerSymbolPriceThrottlingInterval { get; set; }

    //    [System.Xml.Serialization.XmlElement("PerSymbolPriceThrottlingInterval_Milliseconds")]
    //    public int PerSymbolPriceThrottlingIntervalXml
    //    {
    //        get { return (int)PerSymbolPriceThrottlingInterval.TotalMilliseconds; }
    //        set { PerSymbolPriceThrottlingInterval = TimeSpan.FromMilliseconds(value); }
    //    }

    //    public bool ForceConnectRemotingClientOnContractVersionMismatch { get; set; }

    //    [System.Xml.Serialization.XmlIgnore]
    //    public static MessageBusSettings MessageBusBehavior
    //    {
    //       get
    //        {
    //            if (_messageBusBehavior == null)
    //            {
    //                //Allow local configuration of message bus
    //                string callingDir = Path.GetDirectoryName(System.Reflection.Assembly.GetCallingAssembly().Location);
    //                string configXml = Path.Combine(callingDir, @"Kreslik.Integrator.MessageBus.dll.xml");
    //                if (File.Exists(configXml))
    //                {
    //                    _messageBusBehavior = SettingsReader.DeserializeConfiguration<MessageBusSettings>(
    //                        @"Kreslik.Integrator.MessageBus.dll.xml",
    //                        @"Kreslik.Integrator.MessageBus.dll.xsd"
    //                        );
    //                }
    //                else
    //                {
    //                    _messageBusBehavior = SettingsInitializator.Instance.ReadSettings<MessageBusSettings>(
    //                        @"Kreslik.Integrator.MessageBus.dll",
    //                        @"Kreslik.Integrator.MessageBus.dll.xsd"
    //                        );
    //                }
    //            }

    //            return _messageBusBehavior;
    //        } 
    //    }

    //    private static MessageBusSettings _messageBusBehavior;
    //}
}
