﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.MessageBus
{
    public class IntegratorServiceProxy : System.ServiceModel.DuplexClientBase<IIntegratorService>, IIntegratorService
    {
        public IntegratorServiceProxy(InstanceContext callbackInstance, Binding binding, EndpointAddress remoteAddress)
            : base(callbackInstance, binding, remoteAddress)
        {
            ContractDescription cd = this.Endpoint.Contract;

            PooledPricesResolver.AddRedirectingSerializer(cd.Operations.Find("TransferReliablyOnPriceUpdate"));
            PooledPricesResolver.AddRedirectingSerializer(cd.Operations.Find("TransferFastOnPriceUpdate"));
        }

        public ClientRegistrationResult Register(ClientRegistrationRequest clientRegistrationRequest)
        {
            return Channel.Register(clientRegistrationRequest);
        }

        public IntegratorRequestResult Unregister(string localGatewayIdentifier)
        {
            return Channel.Unregister(localGatewayIdentifier);
        }

        public IntegratorRequestResult RecreateSession(string localGatewayIdentifier)
        {
            return Channel.RecreateSession(localGatewayIdentifier);
        }

        //public IntegratorRequestResult Subscribe(string localGatewayIdentifier, IntegratorSubscriptionRequestInfo subscriptionRequestInfo)
        //{
        //    return Channel.Subscribe(localGatewayIdentifier, subscriptionRequestInfo);
        //}

        //public IntegratorRequestResult Unsubscribe(string localGatewayIdentifier, IntegratorSubscriptionRequestInfo subscriptionRequestInfo)
        //{
        //    return Channel.Unsubscribe(localGatewayIdentifier, subscriptionRequestInfo);
        //}

        //public IntegratorRequestResult SubmitOrder(string localGatewayIdentifier, ClientOrderBase clientOrderBase)
        //{
        //    return Channel.SubmitOrder(localGatewayIdentifier, clientOrderBase);
        //}

        //public IntegratorRequestResult CancelOrder(string localGatewayIdentifier, CancelOrderRequestInfo cancelOrderRequestInfo)
        //{
        //    return Channel.CancelOrder(localGatewayIdentifier, cancelOrderRequestInfo);
        //}

        //public IntegratorRequestResult CancelAllClientOrders(string localGatewayIdentifier, string clientId)
        //{
        //    return Channel.CancelAllClientOrders(localGatewayIdentifier, clientId);
        //}

        //public IntegratorRequestResult CancelAllMyOrders(string localGatewayIdentifier)
        //{
        //    return Channel.CancelAllMyOrders(localGatewayIdentifier);
        //}

        public IntegratorRequestResult SubmitRequest(string localGatewayIdentifier, IntegratorRequestInfo requestInfo)
        {
            return Channel.SubmitRequest(localGatewayIdentifier, requestInfo);
        }

        public void Ping()
        {
            Channel.Ping();
        }
    }
}
