﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.MessageBus
{
    public interface ITradingServiceProxy : ITradingService
    {
        event Action<string> OnDisconnected;

        string TradingServerServiceIdentity { get; }

        bool PriceUpdateThrottlingNeeded(PriceUpdateEventArgs priceUpdateEventArgs);
    }

    public interface IClientRequestsHandler
    {
        void RegisterClientGateway(ITradingServiceProxy clientGateway);
        void UnregisterClientGateway(ITradingServiceProxy clientGateway);

        IntegratorRequestResult SubmitRequest(ITradingServiceProxy clientGateway, IntegratorRequestInfo requestInfo);

        void CloseAndSignalDisconnect();
    }

    public enum MessageBusType
    {
        TCP,
        NamedPipe
    }

    public class PooledPricesResolver : DataContractResolver
    {
        private static PooledPricesResolver _instance = new PooledPricesResolver();
        public static PooledPricesResolver Instance { get { return _instance; } }

        private PooledPricesResolver()
        {
            //XmlDictionary dictionary = new XmlDictionary();
            //_typeName = dictionary.Add("Price");
            //_typeNamespace = dictionary.Add("Integrator");
        }

        //XmlDictionaryString _typeName;
        //private XmlDictionaryString _typeNamespace;

        public override bool TryResolveType(Type type, Type declaredType, DataContractResolver knownTypeResolver, out XmlDictionaryString typeName, out XmlDictionaryString typeNamespace)
        {
            if (declaredType == typeof(PriceObject) || declaredType == typeof(PriceObjectInternal))
            {
                typeName = null;//_typeName;
                typeNamespace = null;//_typeNamespace;
                return true;
            }

            return knownTypeResolver.TryResolveType(type, declaredType, null, out typeName, out typeNamespace);
        }

        public override Type ResolveName(string typeName, string typeNamespace, Type declaredType, DataContractResolver knownTypeResolver)
        {
            //if (declaredType == typeof(PriceObject) || declaredType == typeof(WritablePriceObject))
            //{
            //    return typeof(LegacyPriceObject);
            //}

            //if (typeName == "Price" && typeNamespace == "Integrator")
            //{
            //    return typeof(LegacyPriceObject);
            //}

            return knownTypeResolver.ResolveName(typeName, typeNamespace, declaredType, null) ?? declaredType;
        }

        public static void AddRedirectingSerializer(OperationDescription operation)
        {
            DataContractSerializerOperationBehavior serializerBehavior = operation.Behaviors.Find<DataContractSerializerOperationBehavior>();
            if (serializerBehavior == null)
            {
                serializerBehavior = new DataContractSerializerOperationBehavior(operation);
                operation.Behaviors.Add(serializerBehavior);
            }

            serializerBehavior.DataContractResolver = PooledPricesResolver.Instance;
        }
    }

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true)]
    public class IntegratorService : IIntegratorService
    {
        private readonly IClientRequestsHandler _clientRequestsHandler;
        private readonly List<TradingServerServiceProxyWrapper> _tradingServiceProxies = new List<TradingServerServiceProxyWrapper>();
        private ServiceHost _serviceHost;
        private ILogger _logger;
        private string _serviceIdentifier;
        private MessageBusType _messageBusType;
        private int _clientsCounter = 0;
        private ConnectionChainBuildVersioningInfo _connectionChainBuildVersioningInfo;
        private CommunicationServiceState _communicationServiceState = CommunicationServiceState.Inactive;
        private StateMachine<CommunicationServiceState, CommunicationServiceStateAction> _stateMachine;
        private IntegratorProcessInfo _integratorProcessInfo;

        public static IntegratorService CreateIntegratorService(IClientRequestsHandler clientRequestsHandler, ILogger logger, string serviceIdentifier,
                                 ConnectionChainBuildVersioningInfo connectionChainBuildVersioningInfo, IntegratorProcessInfo integratorProcessInfo)
        {
            return new IntegratorService(clientRequestsHandler, logger, serviceIdentifier,
                                         connectionChainBuildVersioningInfo, MessageBusType.TCP, integratorProcessInfo);
        }

        public static IntegratorService CreateSplitterService(IClientRequestsHandler clientRequestsHandler, ILogger logger, string serviceIdentifier,
                                 ConnectionChainBuildVersioningInfo connectionChainBuildVersioningInfo)
        {
            return new IntegratorService(clientRequestsHandler, logger, serviceIdentifier,
                                         connectionChainBuildVersioningInfo, MessageBusType.NamedPipe, null);
        }

        private IntegratorService(IClientRequestsHandler clientRequestsHandler, ILogger logger, string serviceIdentifier,
                                 ConnectionChainBuildVersioningInfo connectionChainBuildVersioningInfo, MessageBusType messageBusType,
                                 IntegratorProcessInfo integratorProcessInfo)
        {
            this._clientRequestsHandler = clientRequestsHandler;
            this._logger = logger;
            this._serviceIdentifier = serviceIdentifier;
            this._messageBusType = messageBusType;
            this._connectionChainBuildVersioningInfo = connectionChainBuildVersioningInfo;
            this._stateMachine = new StateMachine<CommunicationServiceState, CommunicationServiceStateAction>(false, logger);
            this.InitializeStateMachine();
            _integratorProcessInfo = integratorProcessInfo;

            if (integratorProcessInfo != null)
            {
                integratorProcessInfo.IntegratorProcessStateChanged += state =>
                    {
                        switch (state)
                        {
                            case IntegratorProcessState.Running:
                                //this.StartListening();
                                this._stateMachine.FireEvent(CommunicationServiceStateAction.Start);
                                break;
                            case IntegratorProcessState.ShuttingDown:
                                this._stateMachine.FireEvent(CommunicationServiceStateAction.InitiateShutdown);
                                break;
                            case IntegratorProcessState.Inactive:
                                //this.CloseAndSignalDisconnect();
                                this._stateMachine.FireEvent(CommunicationServiceStateAction.ShutDown);
                                break;
                            case IntegratorProcessState.Starting:
                            case IntegratorProcessState.Unknown:
                            default:
                                //nothing
                                break;
                        }
                    };
            }
        }

        private void StartListening()
        {
            Uri listenAddress;
            Binding binding;

            switch (this._messageBusType)
            {
                case MessageBusType.TCP:
                    listenAddress =
                        new Uri("net.tcp://" + _integratorProcessInfo.GetTargetHost(MessageBusSettings.Sections.IntegratorEndpointConnectionInfo.ConnectionType) + ":" +
                                _integratorProcessInfo.TradingAPIPort.Value);
                    binding = new NetTcpBinding(SecurityMode.None);
                    break;
                case MessageBusType.NamedPipe:
                    listenAddress = new Uri("net.pipe://localhost/" + MessageBusSettings.Sections.ClientConnectionInfo.IntegratorServicePipeName);
                    binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("messageBusType");
            }

            _serviceHost = new ServiceHost(this, listenAddress);
            _serviceHost.AddServiceEndpoint(typeof(IIntegratorService), binding, "");
            this._logger.Log(LogLevel.Info, "Starting service endpoint that will listen on {0}", listenAddress);
            this._logger.Log(LogLevel.Info, "Connection chain versioning info: {0}", this._connectionChainBuildVersioningInfo);



            ContractDescription cd = _serviceHost.Description.Endpoints[0].Contract;

            PooledPricesResolver.AddRedirectingSerializer(cd.Operations.Find("TransferReliablyOnPriceUpdate"));
            PooledPricesResolver.AddRedirectingSerializer(cd.Operations.Find("TransferFastOnPriceUpdate"));

            try
            {
                _serviceHost.Open();
            }
            catch (AddressAlreadyInUseException e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Only one instance of IntegratorServer(Proxy) can be run on one machine.");
                //return false;
            }

            //return true;
        }

        private void InitiateShutdown()
        {
            //Stop accepting connections

            this.CloseAndSignalDisconnect();
        }

        private void Shutdown()
        {
            //Disconnect clients
            
        }

        private void InitializeStateMachine()
        {
            this._stateMachine
                .In(CommunicationServiceState.Inactive)
                    .On(CommunicationServiceStateAction.Start).Goto(CommunicationServiceState.Running)
                .In(CommunicationServiceState.Running)
                    .ExecuteOnEntry(StartListening)
                    .On(CommunicationServiceStateAction.InitiateShutdown).Goto(CommunicationServiceState.ShuttingDown)
                    .On(CommunicationServiceStateAction.ShutDown).Goto(CommunicationServiceState.Inactive)
                .In(CommunicationServiceState.ShuttingDown)
                    .ExecuteOnEntry(InitiateShutdown)
                    .On(CommunicationServiceStateAction.ShutDown).Goto(CommunicationServiceState.Inactive)
                    .On(CommunicationServiceStateAction.Start).Goto(CommunicationServiceState.Running)
                .In(CommunicationServiceState.Inactive)
                    .ExecuteOnEntry(Shutdown);

            this._stateMachine.Initialize(CommunicationServiceState.Inactive);
        }

        public void AnnounceCommunicationServiceStateChange(CommunicationStateChangeEventArgs communicationStateChangeEventArgs)
        {
            this._stateMachine.FireEvent(communicationStateChangeEventArgs.CommunicationServiceStateAction);
        }


        //public string Register(string localGatewayName, PriceStreamTransferMode priceStreamTransferMode, TimeSpan priceThrottlingInterval)
        //ClientRegistrationResult Register(ClientRegistrationRequest clientRegistrationRequest);
        private readonly BuildVersioningInfo _currentBuildInfo =
            new BuildVersioningInfo(BuildConstants.CURRENT_BUILD_VERSION,
                                    BuildConstants.MINIMUM_VERSION_NUMBER_OF_COMPATIBLE_BUILD,
                                    BuildConstants.CURRENT_VERSION_CREATED_UTC);

        public ClientRegistrationResult Register(ClientRegistrationRequest clientRegistrationRequest)
        {
            this._logger.Log(LogLevel.Debug, "Register called with gateway name [{0}]", clientRegistrationRequest.LocalGatewayName);
            bool forceConnectedWithMismatchedVersion = false;

            if (this._stateMachine.CurrentState != CommunicationServiceState.Running)
            {
                string error =
                    string.Format(
                        "Attempt to register client [{0}] while service is in state [{1}]",
                        clientRegistrationRequest.LocalGatewayName, this._stateMachine.CurrentState);

                this._logger.Log(LogLevel.Error, error);
                return ClientRegistrationResult.CreateFailedResult(error, this._connectionChainBuildVersioningInfo);
            }

            if (clientRegistrationRequest.RegisteringClientVersionInfo.CurrentBuildVersion <
                BuildConstants.MINIMUM_VERSION_NUMBER_OF_COMPATIBLE_BUILD
                ||
                clientRegistrationRequest.RegisteringClientVersionInfo.CurrentBuildVersion >
                BuildConstants.CURRENT_BUILD_VERSION)
            {
                string error =
                    string.Format(
                        "Attempt to register client with build version: [{0}], but current version is [{1}]",
                        clientRegistrationRequest.RegisteringClientVersionInfo, _currentBuildInfo);

                this._logger.Log(LogLevel.Error, error);

                if (!clientRegistrationRequest.ForceConnectOnContractVersionMismatch)
                {
                    return ClientRegistrationResult.CreateFailedResult(error, this._connectionChainBuildVersioningInfo);
                }
                else
                {
                    forceConnectedWithMismatchedVersion = true;
                    this._logger.Log(LogLevel.Warn, "Force connecting client with mismatched version (per its request).");
                }
            }

            if (clientRegistrationRequest.PriceStreamTransferMode == PriceStreamTransferMode.Reliable && _messageBusType == MessageBusType.TCP)
            {
                string error = string.Format("Attempt to register client with PriceStreamTransferMode.Reliable which is unsupported");
                this._logger.Log(LogLevel.Error, error);
                return ClientRegistrationResult.CreateFailedResult(error, this._connectionChainBuildVersioningInfo);
            }

            string localGatewayIdentifier =
                clientRegistrationRequest.KeepLocalClientNameAsUniqueRemotingIdentifer
                    ? clientRegistrationRequest.LocalGatewayName
                    : string.Format("{0}__{1}_{2}", this._serviceIdentifier,
                                    Interlocked.Increment(ref this._clientsCounter),
                                    clientRegistrationRequest.LocalGatewayName);

            this._logger.Log(LogLevel.Debug, "{0} the identifier [{1}]",
                             clientRegistrationRequest.KeepLocalClientNameAsUniqueRemotingIdentifer
                                 ? "Keeping"
                                 : "Assigning", localGatewayIdentifier);

            //We still need to check the uniqueness of identifier - as they might be transfered between services
            lock (_tradingServiceProxies)
            {
                if (
                    _tradingServiceProxies.Any(
                        cs =>
                        cs.TradingServerServiceIdentity.Equals(localGatewayIdentifier,
                                                               StringComparison.InvariantCultureIgnoreCase)))
                {
                    string error = string.Format("Client {0} is already registered", localGatewayIdentifier);
                    this._logger.Log(LogLevel.Error, error);
                    return ClientRegistrationResult.CreateFailedResult(error, this._connectionChainBuildVersioningInfo);
                }
            }


            ITradingServiceEndpoint callbackStub = OperationContext.Current.GetCallbackChannel<ITradingServiceEndpoint>();
            TradingServerServiceProxyWrapper gatewayClientStub =
                new TradingServerServiceProxyWrapper(callbackStub, clientRegistrationRequest.PriceStreamTransferMode, clientRegistrationRequest.PriceThrottlingInterval,
                                                     localGatewayIdentifier, OperationContext.Current.Channel,
                                                     forceConnectedWithMismatchedVersion, this._logger);

            lock (_tradingServiceProxies)
            {
                _tradingServiceProxies.Add(gatewayClientStub);
            }

            gatewayClientStub.OnDisconnected += (reason) =>
                {
                    lock (_tradingServiceProxies)
                    {
                        _tradingServiceProxies.Remove(gatewayClientStub);
                    }
                };

            InvokeLocalMethod(() =>
                {
                    this._clientRequestsHandler.RegisterClientGateway(gatewayClientStub);
                    return IntegratorRequestResult.GetSuccessResult();
                });
            return ClientRegistrationResult.CreateSuccessResult(localGatewayIdentifier, this._connectionChainBuildVersioningInfo);
        }

        public IntegratorRequestResult Unregister(string localGatewayIdentifier)
        {
            this._logger.Log(LogLevel.Debug, "Unregister called with identifier [{0}]", localGatewayIdentifier);

            TradingServerServiceProxyWrapper gatewayClientStub = this.GetRegisteredClientGateway(localGatewayIdentifier);

            gatewayClientStub.CloseLocalTradingServiceProxyOnly(string.Format("Client {0} is unregistering", localGatewayIdentifier));

            lock (_tradingServiceProxies)
            {
                _tradingServiceProxies.Remove(gatewayClientStub);
            }
            return InvokeLocalMethod(() =>
                {
                    this._clientRequestsHandler.UnregisterClientGateway(gatewayClientStub);
                    return IntegratorRequestResult.GetSuccessResult();
                });
        }

        public IntegratorRequestResult RecreateSession(string localGatewayIdentifier)
        {
            this._logger.Log(LogLevel.Debug, "RecreateSession called with identifier [{0}]", localGatewayIdentifier);

            TradingServerServiceProxyWrapper gatewayClientStub = this.GetRegisteredClientGateway(localGatewayIdentifier);

            ITradingServiceEndpoint tradingServerProxy = OperationContext.Current.GetCallbackChannel<ITradingServiceEndpoint>();
            return gatewayClientStub.ReconnectSession(tradingServerProxy, OperationContext.Current.Channel);
        }

        public IntegratorRequestResult SubmitRequest(string localGatewayIdentifier, IntegratorRequestInfo requestInfo)
        {
            this._logger.Log(LogLevel.Debug, "IntegratorRequest [{0}] called with identifier [{1}]", requestInfo, localGatewayIdentifier);
            TradingServerServiceProxyWrapper gatewayClientStub = this.GetRegisteredClientGateway(localGatewayIdentifier);
            return InvokeLocalMethod(() => this._clientRequestsHandler.SubmitRequest(gatewayClientStub, requestInfo));
        }

        private IntegratorRequestResult InvokeLocalMethod(Func<IntegratorRequestResult> remoteMethodInvocation)
        {
            try
            {
                return remoteMethodInvocation();
            }
            catch (FaultException e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "IntegratorService server experienced exception during the local call.");
                throw;
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "IntegratorService server experienced exception during the local call.");
                throw new FaultException(string.Format("IntegratorService server experienced exception during the local call. {0}", e));
            }
        }

        public void Ping()
        {
            this._logger.Log(LogLevel.Debug, "Replying to ping request");
            return;
        }

        private void CloseAndSignalDisconnect()
        {
            //this is performing local cleanup only
            this._clientRequestsHandler.CloseAndSignalDisconnect();

            List<TradingServerServiceProxyWrapper> tempListOfProxies = new List<TradingServerServiceProxyWrapper>(_tradingServiceProxies);

            foreach (TradingServerServiceProxyWrapper proxyWrapper in tempListOfProxies)
            {
                //this actually passes through the proxy and so is transmitted to clients
                proxyWrapper.CloseLocalResourcesOnly("Server is closing");
            }

            try
            {
                this._serviceHost.Close();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Error, e, "Exception occured during service closing");
            }
        }

        private TradingServerServiceProxyWrapper GetRegisteredClientGateway(string localGatewayIdentifier)
        {
            TradingServerServiceProxyWrapper gatewayClientStub;

            lock (_tradingServiceProxies)
            {
                gatewayClientStub =
                _tradingServiceProxies.FirstOrDefault(
                    cs =>
                    cs.TradingServerServiceIdentity.Equals(localGatewayIdentifier, StringComparison.InvariantCultureIgnoreCase));
            }

            if (gatewayClientStub == null)
            {
                this._logger.Log(LogLevel.Error, "Client {0} is not registered", localGatewayIdentifier);
                throw new FaultException(string.Format("Client {0} is not registered", localGatewayIdentifier));
            }

            return gatewayClientStub;
        }

        private class TradingServerServiceProxyWrapper : ITradingServiceProxy
        {
            private const int _maximumFailingCallAttempts = 10;
            private ManualResetEventSlim _isConnectedEvent;
            private CancellationTokenSource _isFaultedTokenSource;
            private SafeTimer _reconectTimer;
            private ILogger _logger;
            private object _connectionChangesLocker = new object();

            private IContextChannel _channel;
            private ITradingServiceEndpoint _tradingServiceProxy;
            public string TradingServerServiceIdentity { get; private set; }
            private PriceStreamTransferMode _priceStreamTransferMode;
            //separate throttling of enqueuing and sending (both can cause bloating)
            private PriceThrottlingManager _pricesSendingThrottlingManager = null;
            private PriceThrottlingManager _pricesEnqueuingThrottlingManager = null;
            private bool _forceConnectedWithMismatchedVersion;

            public TradingServerServiceProxyWrapper(ITradingServiceEndpoint tradingServiceProxy,
                                                    PriceStreamTransferMode priceStreamTransferMode,
                                                    TimeSpan throttlingInterval, string tradingServerServiceIdentity,
                                                    IContextChannel channel, bool forceConnectedWithMismatchedVersion, ILogger logger)
            {
                this.TradingServerServiceIdentity = tradingServerServiceIdentity;
                this._forceConnectedWithMismatchedVersion = forceConnectedWithMismatchedVersion;
                this._logger = logger;
                this._priceStreamTransferMode = priceStreamTransferMode;
                if (priceStreamTransferMode == PriceStreamTransferMode.FastAndUnreliable)
                {
                    this._pricesSendingThrottlingManager = new PriceThrottlingManager(throttlingInterval);
                    this._pricesEnqueuingThrottlingManager = new PriceThrottlingManager(throttlingInterval);
                }

                this._isConnectedEvent = new ManualResetEventSlim(false);
                this._isFaultedTokenSource = new CancellationTokenSource();
                this._reconectTimer = new SafeTimer(OnDisconnectedTooLong)
                {
                    RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1sec,
                    TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
                };

                this.ReconnectSession(tradingServiceProxy, channel);
            }

            public IntegratorRequestResult ReconnectSession(ITradingServiceEndpoint tradingServiceProxy, IContextChannel channel)
            {
                if (_isFaultedTokenSource.IsCancellationRequested)
                {
                    return IntegratorRequestResult.CreateFailedResult("Service you are trying to connect to is already closed");
                }

                this.UnregisterFromChannel(this._channel);

                lock (_connectionChangesLocker)
                {
                    if (_isFaultedTokenSource.IsCancellationRequested)
                    {
                        return IntegratorRequestResult.CreateFailedResult("Service you are trying to connect to is already closed");
                    }

                    this._reconectTimer.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);

                    this._channel = channel;
                    channel.Faulted += this.OnChannelFaulted;
                    channel.Closed += this.OnChannelClosed;

                    this._tradingServiceProxy = tradingServiceProxy;

                    this._isConnectedEvent.Set();
                }

                return IntegratorRequestResult.GetSuccessResult();
            }

            public bool PriceUpdateThrottlingNeeded(PriceUpdateEventArgs priceUpdateEventArgs)
            {
                return
                    this._priceStreamTransferMode == PriceStreamTransferMode.FastAndUnreliable
                    &&
                    !this._pricesEnqueuingThrottlingManager.CheckPricePassThrotteling(priceUpdateEventArgs);
            }

            public void OnPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
            {
                Action priceUpdateInvocation;
                if (this._priceStreamTransferMode == PriceStreamTransferMode.FastAndUnreliable)
                {
                    priceUpdateInvocation = () =>
                    {
                        if (this._pricesSendingThrottlingManager.CheckPricePassThrotteling(priceUpdateEventArgs))
                            this._tradingServiceProxy.TransferFastOnPriceUpdate(priceUpdateEventArgs);
                    };
                }
                else
                {
                    priceUpdateInvocation = () => this._tradingServiceProxy.TransferReliablyOnPriceUpdate(priceUpdateEventArgs);
                }

                this.InvokeRemoteMethod(priceUpdateInvocation, false);
            }

            public void OnIntegratorUnicastInfo(IntegratorInfoObjectBase integratorInfoObject)
            {
                //this._logger.Log(LogLevel.Debug, "OnIntegratorUnicastInfo ([{0}]) called for client [{1}]",
                //                 integratorInfoObject, this.TradingServerServiceIdentity);
                Action clientunicastInfoInvocation =
                    () =>
                    this._tradingServiceProxy.TransferReliablyIntegratorInfoObject(integratorInfoObject);
                this.InvokeRemoteMethod(clientunicastInfoInvocation, false);
            }

            public void OnIntegratorBroadcastInfo(IntegratorBroadcastInfoBase integratorInfoObject)
            {
                //this._logger.Log(LogLevel.Debug, "OnIntegratorBroadcastInfo ([{0}]) called for client [{1}]",
                //                 integratorInfoObject, this.TradingServerServiceIdentity);
                Action clientBroadcastInfoInvocation =
                    () =>
                    this._tradingServiceProxy.TransferReliablyIntegratorBroadcastInfoObject(integratorInfoObject);
                this.InvokeRemoteMethod(clientBroadcastInfoInvocation, this._forceConnectedWithMismatchedVersion);
            }

            private void InvokeRemoteMethod(Action remoteMethodInvocation, bool contractMismatchesExpected)
            {
                int consecutiveFailures = 0;
                bool completed = false;
                do
                {
                    try
                    {
                        this._isConnectedEvent.Wait(this._isFaultedTokenSource.Token);
                        //there is a slight chance for nullpointerexception but that would mean that
                        // someone was invalidating the session in the meantime, so we want to TryReconnect anyway
                        remoteMethodInvocation();
                        completed = true;
                    }
                    catch (FaultException e)
                    {
                        this._logger.LogException(contractMismatchesExpected ? LogLevel.Info : LogLevel.Error, e,
                                                  "Remote action invocation failed because other end failed.");
                        break;
                    }
                    catch (OperationCanceledException)
                    {
                        this._logger.Log(LogLevel.Warn,
                                         "Remote method invocation failed as proxy cancellation came in the meantime. ClientId: {0}",
                                         this.TradingServerServiceIdentity);
                        break;
                    }
                    catch (Exception e)
                    {
                        this._logger.LogException(LogLevel.Warn, e,
                                                  "Remote method invocation failed due to communication layer issues. Will try to recconect and try later. ClientId: {0}",
                                                  this.TradingServerServiceIdentity);

                        if (++consecutiveFailures > _maximumFailingCallAttempts)
                        {
                            this._logger.Log(LogLevel.Fatal,
                                             "Failed {0} times to perform remote call [{1}]. Not atempting anymore",
                                             consecutiveFailures, remoteMethodInvocation);
                            break;
                        }

                        this.UnregisterFromChannel(this._channel);
                    }
                } while (!completed);
            }

            //this is to be called by the over the wire callers - therefore it closes just locally
            // it doesn't send any messge back to client
            public void CloseLocalTradingServiceProxyOnly(string reason)
            {
                bool wasClosed;
                lock (_connectionChangesLocker)
                {
                    wasClosed = this._isFaultedTokenSource.IsCancellationRequested;
                    this._isFaultedTokenSource.Cancel();
                    this.UnregisterFromChannel(this._channel);
                    this._reconectTimer.Dispose();
                }

                if (!wasClosed)
                {
                    InvokeOnDisconnected(reason);
                }
            }

            //This is to be called by locall callers (not the over the wire) - as it also sends the close request over the wire
            public void CloseLocalResourcesOnly(string reason)
            {
                try
                {
                    this._tradingServiceProxy.CloseLocalResourcesOnly(reason);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Warn, e,
                                              "Got exception when attempting to signal cancel to client {0}.",
                                              this.TradingServerServiceIdentity);
                }

                this.CloseLocalTradingServiceProxyOnly(reason);
            }

            public event Action<string> OnDisconnected;

            private void InvokeOnDisconnected(string reason)
            {
                if (OnDisconnected != null)
                {
                    OnDisconnected(reason);
                }
            }

            private void OnChannelFaulted(object sender, EventArgs args)
            {
                this._logger.Log(LogLevel.Warn, "TradingServerServiceProxyWrapper [{0}] getting to faulted state",
                                 this.TradingServerServiceIdentity);
                this.UnregisterFromChannel((sender as IContextChannel) ?? this._channel);
            }

            private void OnChannelClosed(object sender, EventArgs args)
            {
                this._logger.Log(LogLevel.Warn, "TradingServerServiceProxyWrapper [{0}] getting to closed state",
                                 this.TradingServerServiceIdentity);
                this.UnregisterFromChannel((sender as IContextChannel) ?? this._channel);
            }

            private void UnregisterFromChannel(IContextChannel channel)
            {
                lock (_connectionChangesLocker)
                {
                    //prevent closing of a channel that was recreated in the meantime
                    if (!_isFaultedTokenSource.IsCancellationRequested && channel == this._channel)
                    {
                        this._isConnectedEvent.Reset();
                        this._reconectTimer.Change(
                            MessageBusSettings.Sections.ConnectionKeeping.MaximumAllowedDisconnectedInterval,
                            Timeout.InfiniteTimeSpan);
                        this._channel = null;
                    }
                }

                if (channel != null)
                {
                    channel.Faulted -= this.OnChannelFaulted;
                    channel.Closed -= this.OnChannelClosed;

                    //Following can cause race conditions (ping-pong closing)
                    // client should actually allways close the channel by itself
                    //try
                    //{
                    //   channel.Close();
                    //}
                    //catch (Exception e)
                    //{
                    //    this._logger.LogException(LogLevel.Warn, e, "Exception when closing channel to {0}", this.TradingServerServiceIdentity);
                    channel.Abort();
                    //}
                }
            }

            private void OnDisconnectedTooLong()
            {
                this.CloseLocalResourcesOnly("Client was disconnected for too long");
            }

            private class PriceThrottlingManager
            {
                private TimeSpan _throttlingInterval;
                private DateTime[][] _priceTimeStamps;
                private DateTime[][] _dealsTimeStamps;

                public PriceThrottlingManager(TimeSpan throttlingInterval)
                {
                    this._throttlingInterval = throttlingInterval;
                    this.Reset();
                }

                public void Reset()
                {
                    this._priceTimeStamps = new DateTime[][]
                    {
                        Enumerable.Range(0, Symbol.ValuesCount).Select(idx => DateTime.MinValue).ToArray(),
                        Enumerable.Range(0, Symbol.ValuesCount).Select(idx => DateTime.MinValue).ToArray()
                    };

                    this._dealsTimeStamps = new DateTime[][]
                    {
                        Enumerable.Range(0, Symbol.ValuesCount).Select(idx => DateTime.MinValue).ToArray(),
                        Enumerable.Range(0, Symbol.ValuesCount).Select(idx => DateTime.MinValue).ToArray()
                    };
                }

                public bool CheckPricePassThrotteling(PriceUpdateEventArgs priceUpdateEventArgs)
                {
                    if (priceUpdateEventArgs is SinglePriceChangeEventArgs)
                    {
                        return
                            this.CheckPricePassThrotteling(
                                (priceUpdateEventArgs as SinglePriceChangeEventArgs).PriceObject);
                    }
                    else if (priceUpdateEventArgs is PriceObject)
                    {
                        return this.CheckPricePassThrotteling(
                                (priceUpdateEventArgs as PriceObject));
                    }
                    else if (priceUpdateEventArgs is VenueDealObject)
                    {
                        return this.CheckDealPassThrotteling(priceUpdateEventArgs as VenueDealObject);
                    }

                    return true;
                }

                private bool CheckPricePassThrotteling(PriceObject price)
                {
                    if (price == null || PriceObjectInternal.IsNullPrice(price))
                        return true;

                    bool passThrotteling = this._priceTimeStamps[(int)price.Side][(int)price.Symbol] +
                                           this._throttlingInterval < price.IntegratorReceivedTimeUtc;

                    if (passThrotteling)
                    {
                        this._priceTimeStamps[(int)price.Side][(int)price.Symbol] =
                            price.IntegratorReceivedTimeUtc;
                    }

                    return passThrotteling;
                }

                private bool CheckDealPassThrotteling(VenueDealObject venueDeal)
                {
                    if (venueDeal == null)
                        return true;

                    bool passThrotteling = this._dealsTimeStamps[(int)venueDeal.TradeSide][(int)venueDeal.Symbol] +
                                           this._throttlingInterval < venueDeal.IntegratorReceivedTimeUtc;

                    if (passThrotteling)
                    {
                        this._dealsTimeStamps[(int)venueDeal.TradeSide][(int)venueDeal.Symbol] =
                            venueDeal.IntegratorReceivedTimeUtc;
                    }

                    return passThrotteling;
                }
            }
        }
    }
}
