﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.MessageBus;

namespace Kreslik.Integrator.MessageBus
{
    public sealed class TradingClientGateway : ClientGatewayBase
    {
        private TradingService _tradingService;

        public TradingClientGateway(string clientId, ILogger logger)
            : base(clientId, logger)
        {
            this._logger = logger;
            _tradingService = TradingService.CreateClientService(this, clientId, logger);
            this.ClientIdentityInternal = _tradingService.Identifier;
            _tradingService.OnProxyClosing += this.OnProxyClosingHandler;

            this.ConnectionChainBuildVersioningInfo = new ConnectionChainBuildVersioningInfo(
                new BuildVersioningInfo(BuildConstants.CURRENT_BUILD_VERSION,
                                        BuildConstants.MINIMUM_VERSION_NUMBER_OF_COMPATIBLE_BUILD,
                                        BuildConstants.CURRENT_VERSION_CREATED_UTC),
                _tradingService.IsClosed ? null : _tradingService.ConnectionChainBuildVersioningInfo.SplitterVersionInfo,
                _tradingService.IsClosed ? null : _tradingService.ConnectionChainBuildVersioningInfo.IntegratorVersionInfo);
        }

        public void CloseGateway()
        {
            this._tradingService.CloseProxyAndSignalDisconnect("Local Gateway shutdown requested");
            this.CloseLocalResourcesOnly("Local Gateway shutdown was requested.");
        }


        #region ClientGatewayBase implementations

        protected override void LocalResourcesCleanupRoutine()
        {
            //Everything happens in base, we don't need any more cleanup here
        }

        protected override string ClientIdentityInternal { get; set; }

        protected override IntegratorRequestResult SubmitRequestInternal(IntegratorRequestInfo integratorRequestInfo)
        {
            return this._tradingService.SubmitRequest(integratorRequestInfo);
        }

        protected override bool IsConnectionChannelClosed
        {
            get { return _tradingService.IsClosed; }
        }

        #endregion /ClientGatewayBase implementations
    }
}
