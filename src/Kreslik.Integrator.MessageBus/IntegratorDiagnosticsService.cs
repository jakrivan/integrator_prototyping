﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.MessageBus
{
    public static class IntegratorProcessInfoExtensions
    {
        public static string GetTargetHost(this IntegratorProcessInfo integratorProcessInfo,
                                    MessageBusSettings.ConnectionType connectionType)
        {
            switch (MessageBusSettings.Sections.IntegratorEndpointConnectionInfo.ConnectionType)
            {
                case MessageBusSettings.ConnectionType.Localhost:
                    return "localhost";
                    break;
                case MessageBusSettings.ConnectionType.PrivateIPAddress:
                    return integratorProcessInfo.PrivateIPAddress;
                    break;
                case MessageBusSettings.ConnectionType.PublicIPAddress:
                    return integratorProcessInfo.PublicIPAddress;
                    break;
                case MessageBusSettings.ConnectionType.Hostname:
                    return integratorProcessInfo.Hostname;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("ConnectionType", "Unknown setting for connection type");
            }
        }
    }

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true)]
    public class IntegratorDiagnosticsService : CommandDistributorBase, IIntegratorDiagnosticsService
    {
        private ServiceHost _serviceHost;
        private IPriceStreamsInfoProvider _priceStreamsInfoProvider;
        private IPriceBookDiagnostics _priceBookDiagnostics;
        private ISymbolTrustworthinessInfoProvider _symbolTrustworthinessInfoProvider;
        private IFIXSessionsDiagnostics _fixSessionsDiagnostics;
        private IntegratorProcessInfo _integratorProcessInfo;

        public IntegratorDiagnosticsService(ILogger logger, IPriceStreamsInfoProvider priceStreamsInfoProvider,
                                            IPriceBookDiagnostics priceBookDiagnostics,
                                            ISymbolTrustworthinessInfoProvider symbolTrustworthinessInfoProvider,
                                            IFIXSessionsDiagnostics fixSessionsDiagnostics,
                                            IntegratorProcessInfo integratorProcessInfo)
            :base(logger)
        {
            this._logger = logger;
            this._priceStreamsInfoProvider = priceStreamsInfoProvider;
            this._priceBookDiagnostics = priceBookDiagnostics;
            this._symbolTrustworthinessInfoProvider = symbolTrustworthinessInfoProvider;
            this._fixSessionsDiagnostics = fixSessionsDiagnostics;
            this._integratorProcessInfo = integratorProcessInfo;

            integratorProcessInfo.IntegratorProcessStateChanged += state =>
                {
                    switch (state)
                    {
                        case IntegratorProcessState.Running:
                            this.StartListening();
                            break;
                        case IntegratorProcessState.ShuttingDown:
                        case IntegratorProcessState.Inactive:
                            this.Close();
                            break;
                        case IntegratorProcessState.Starting:
                        case IntegratorProcessState.Unknown:
                        default:
                            //nothing
                            break;
                    }
                };
        }

        private bool StartListening()
        {
            Uri listenAddress;
            Binding binding;

            string endpoint = this._integratorProcessInfo.GetTargetHost(MessageBusSettings.Sections.IntegratorEndpointConnectionInfo.ConnectionType);

            //No port sharing! as it downgradesperformance (there is MiM service)
            listenAddress = new Uri("net.tcp://" + endpoint + ":" + this._integratorProcessInfo.DiagnosticsAPIPort);

            binding = new NetTcpBinding(SecurityMode.None);

            _serviceHost = new ServiceHost(this, listenAddress);
            _serviceHost.AddServiceEndpoint(typeof(IIntegratorDiagnosticsService), binding, "");
            this._logger.Log(LogLevel.Info, "Starting diagnostics service endpoint that will listen on {0}", listenAddress);

            try
            {
                _serviceHost.Open();
            }
            catch (AddressAlreadyInUseException e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Only one instance of Integrator Diagnostics service can be run on one machine.");
                return false;
            }

            return true;
        }

        private bool _closed = false;
        private void Close()
        {
            if(_closed)
                return;
            _closed = true;

            try
            {
                this._serviceHost.Close();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Error, e, "Exception occured during service closing");
            }
        }

        public PriceStreamsMonitoringInfo GetPriceStreamsMonitoringInfo()
        {
            PriceStreamsMonitoringInfo priceStreamsMonitoringInfo;

            try
            {
                priceStreamsMonitoringInfo =
                new PriceStreamsMonitoringInfo(this._priceStreamsInfoProvider.GetPriceStreamInfoEntries(),
                                               this._priceBookDiagnostics == null ? null : this._priceBookDiagnostics.GetTopOfBookInfoEntries(),
                                               this._symbolTrustworthinessInfoProvider == null ? null : this._symbolTrustworthinessInfoProvider.SymbolTrustworthinessInfo,
                                               this._fixSessionsDiagnostics.MarketDataSessionsStatusInfos,
                                               this._fixSessionsDiagnostics.OrderFlowSessionsStatusInfos,
                                               this._fixSessionsDiagnostics.StpFlowSessionInfos);

                //new PriceStreamsMonitoringInfo(
                //    new List<PriceStreamInfoEntry>() {new WritablePriceStreamInfoEntry(Symbol.USD_SGD, Counterparty.MGS)},
                //    new List<PriceStreamInfoEntry>() { new WritablePriceStreamInfoEntry(Symbol.USD_JPY, Counterparty.JPM) }, 
                //    new List<Counterparty>() {Counterparty.GLS, Counterparty.UBS} );
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, "Got exception when obtaining monitoring info", e);
                throw;
            }

            return priceStreamsMonitoringInfo;
        }

        public void Ping()
        {
            this._logger.Log(LogLevel.Info, "Replying to ping request");
        }

        public IntegratorRequestResult TrySendCommand(string command)
        {
            this._logger.Log(LogLevel.Info, "Receiving command: {0}", command);

            return this.CrackCommand(command);
        }
    }
}
