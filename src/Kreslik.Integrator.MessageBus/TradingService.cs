﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.MessageBus
{
    public delegate void ProxyClosingHandler(ITradingServiceEndpoint sender, string reason, bool isLocalOnlyRequest);

    //public interface ISubscriptionCalls
    //{
    //    IntegratorRequestResult SubscribeToPriceUpdates(IntegratorSubscriptionRequestInfo subscriptionRequestInfo);
    //    IntegratorRequestResult UnsubscribeFromPriceUpdates(IntegratorSubscriptionRequestInfo subscriptionRequestInfo);
    //}

    //public interface ICommonGatewayCalls : IAllTargets
    //{
    //    IntegratorRequestResult SubmitOrder(ClientOrderBase clientOrderBase);
    //    IntegratorRequestResult CancelOrder(CancelOrderRequestInfo cancelOrderRequestInfo);
    //    bool IsClosed { get; }
    //}

    //public interface IClientGatewayCalls
    //{
    //    IntegratorRequestResult SubscribeToPriceUpdates(IntegratorSubscriptionRequestInfo subscriptionRequestInfo);
    //    IntegratorRequestResult UnsubscribeFromPriceUpdates(IntegratorSubscriptionRequestInfo subscriptionRequestInfo);

    //    IntegratorRequestResult SubmitOrder(ClientOrderBase clientOrderBase);
    //    IntegratorRequestResult CancelOrder(CancelOrderRequestInfo cancelOrderRequestInfo);
    //    bool IsClosed { get; }
    //}

    //public interface IRemoteClientGatewayProxy
    //{
    //    IntegratorRequestResult SubscribeToPriceUpdates(IntegratorSubscriptionRequestInfo subscriptionRequestInfo);
    //    IntegratorRequestResult UnsubscribeFromPriceUpdates(IntegratorSubscriptionRequestInfo subscriptionRequestInfo);
    //    IntegratorRequestResult SubmitOrder(ClientOrderBase clientOrderBase);
    //    IntegratorRequestResult CancelOrder(CancelOrderRequestInfo cancelOrderRequestInfo);
    //    IntegratorRequestResult CancelAllClientOrders(string clientId);
    //    //bool IsClosed { get; }

    //    //event ProxyClosingHandler OnProxyClosing;
    //    //void CloseProxyAndSignalDisconnect(string reason);
    //}

    public class TradingService : ITradingServiceEndpoint//, IRemoteClientGatewayProxy
    {
        //minimum interval between two consequent reconnect attempt - this is to prevent ping-pong closing
        private readonly TimeSpan _minReconnectDelay = TimeSpan.FromMilliseconds(500);
        private const int _maximumFailingCallAttempts = 10;
        private readonly Uri _connectAddress;
        private readonly Binding _binding;
        private readonly ITradingService _integratorServiceMessagesHandler;
        private IntegratorServiceProxy _integratorServiceProxy;
        private string _identifier;
        private readonly ManualResetEventSlim _isConnectedEvent;
        private readonly CancellationTokenSource _isFaultedTokenSource;
        private readonly object _connectionChangesLocker = new object();
        private readonly SafeTimer _checkConnectionTimer;
        private ILogger _logger;
        private PriceStreamTransferMode _priceStreamTransferMode = PriceStreamTransferMode.Reliable;
        private TimeSpan _perSymbolPriceThrottlingInterval = TimeSpan.MaxValue;
        private readonly bool _forceConnectOnVersionMismatch;
        private readonly bool _keepLocalClientNameAsUniqueRemotingIdentifer;

        public static TradingService CreateSplitterService(ITradingService integratorServiceMessagesHandler,
                                                           string clientName, ILogger logger,
                                                           IIntegratorInstanceProcessUtils instanceProcessUtils,
                                                           bool keepLocalClientNameAsUniqueRemotingIdentifer)
        {
            return new TradingService(integratorServiceMessagesHandler, clientName, MessageBusType.TCP, logger,
                                      instanceProcessUtils, keepLocalClientNameAsUniqueRemotingIdentifer);
        }

        public static TradingService CreateClientService(ITradingService integratorServiceMessagesHandler,
                                                           string clientName, ILogger logger)
        {
            return new TradingService(integratorServiceMessagesHandler, clientName, MessageBusType.NamedPipe, logger, null, false);
        }

        private TradingService(ITradingService integratorServiceMessagesHandler, string clientName,
                               MessageBusType messageBusType, ILogger logger,
                               IIntegratorInstanceProcessUtils instanceProcessUtils,
                               bool keepLocalClientNameAsUniqueRemotingIdentifer)
        {
            this._integratorServiceMessagesHandler = integratorServiceMessagesHandler;
            this._identifier = clientName;
            this._logger = logger;
            this._keepLocalClientNameAsUniqueRemotingIdentifer = keepLocalClientNameAsUniqueRemotingIdentifer;

            this._isConnectedEvent = new ManualResetEventSlim(false);
            this._isFaultedTokenSource = new CancellationTokenSource();

            this._checkConnectionTimer = new SafeTimer(this.CheckConnection)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1sec,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };

            ReconnectionResult reconnectResult = ReconnectionResult.Other;
            do
            {
                switch (messageBusType)
                {
                    case MessageBusType.TCP:
                        this.IntegratorProcessInfo = this.PollIntegratorProcessInfo(instanceProcessUtils);
                        string host =
                            IntegratorProcessInfo.GetTargetHost(
                                MessageBusSettings.Sections.IntegratorEndpointConnectionInfo.ConnectionType);
                        _connectAddress =
                            new Uri("net.tcp://" + host + ":" + IntegratorProcessInfo.TradingAPIPort.Value);
                        NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);
                        binding.MaxReceivedMessageSize *= 10;
                        binding.MaxBufferSize *= 10;
                        _binding = binding;
                        _priceStreamTransferMode =
                            MessageBusSettings.Sections.PriceTransferBehavior.PriceStreamTransferMode;
                        if (_priceStreamTransferMode == PriceStreamTransferMode.FastAndUnreliable)
                            this._perSymbolPriceThrottlingInterval =
                                MessageBusSettings.Sections.PriceTransferBehavior.PerSymbolPriceThrottlingInterval;
                        else
                        {
                            this._logger.Log(LogLevel.Fatal, "PriceStreamTransferMode.Reliable is unsupported");
                            throw new Exception("PriceStreamTransferMode.Reliable is unsupported");
                        }
                        this.IsConnectedLocally =
                            this.IsLocalIpAddress(host);
                        break;
                    case MessageBusType.NamedPipe:
                        _connectAddress =
                            new Uri("net.pipe://localhost/" +
                                    MessageBusSettings.Sections.ClientConnectionInfo.IntegratorServicePipeName);
                        NetNamedPipeBinding bindingPipe = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);
                        bindingPipe.MaxReceivedMessageSize *= 10;
                        bindingPipe.MaxBufferSize *= 10;
                        _binding = bindingPipe;
                        this.IsConnectedLocally = true;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("messageBusType");
                }

                

                this._forceConnectOnVersionMismatch =
                    MessageBusSettings.Sections.ContractMismatchHandling
                                      .ForceConnectRemotingClientOnContractVersionMismatch;

                //We want this synchronously so thatwe receive the proper id from server
                reconnectResult = TryReconnect();

                //If we couldn't reconnect because enpoint was not found - it might mean thatit died.
                // We need to check the backend again - to see if there is a new record and attempt to reconnect to it.
            } while (reconnectResult == ReconnectionResult.EndpointNotFound);
        }

        public bool IsConnectedLocally { get; private set; }

        public IntegratorProcessInfo IntegratorProcessInfo { get; private set; }

        private IntegratorProcessInfo PollIntegratorProcessInfo(IIntegratorInstanceProcessUtils instanceProcessUtils)
        {
            IntegratorProcessInfo targetIntegratorProcessDetail = null;
            IEnumerable<IntegratorProcessInfo> processesDetails = null;

            int attemptsToRetrieveRunningProcessFromDb = 0;
            do
            {
                do
                {
                    try
                    {
                        processesDetails = instanceProcessUtils.GetIntegratorProcessDetails();
                    }
                    catch (Exception e)
                    {
                        this._logger.LogException(LogLevel.Fatal, "Couldn't obtain integrator processes details from backend", e);
                        Thread.Sleep(2000);
                    }
                } while (processesDetails == null);


                foreach (IntegratorProcessInfo integratorProcessDetail in processesDetails)
                {
                    if (integratorProcessDetail.IntegratorProcessState == IntegratorProcessState.Running)
                    {
                        if (targetIntegratorProcessDetail == null)
                            targetIntegratorProcessDetail = integratorProcessDetail;
                        else
                        {
                            this._logger.Log(LogLevel.Fatal, "Multiple target process in running state ({0} and {1})",
                                             targetIntegratorProcessDetail.ProcessIdentifier,
                                             integratorProcessDetail.ProcessIdentifier);
                            throw new Exception("Multiple target process in running state");
                        }
                    }
                }

                if (targetIntegratorProcessDetail == null)
                {
                    this._logger.Log(attemptsToRetrieveRunningProcessFromDb%30 == 0 ? LogLevel.Fatal : LogLevel.Warn,
                                     "No records of the requested process type for the current instance found ({0} attempts)",
                                     attemptsToRetrieveRunningProcessFromDb + 1);
                    Thread.Sleep(2000);
                    attemptsToRetrieveRunningProcessFromDb++;
                }
                    
            } while (targetIntegratorProcessDetail == null);

            return targetIntegratorProcessDetail;
        }

        private bool IsLocalIpAddress(string hostIp)
        {
            if (string.Equals(hostIp, "localhost", StringComparison.InvariantCultureIgnoreCase))
                return true;

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            return
                host.AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork)
                    .Any(ip => string.Equals(hostIp, ip.ToString(), StringComparison.InvariantCultureIgnoreCase));
        }

        public bool IsClosed
        {
            get { return _isFaultedTokenSource.IsCancellationRequested; }
        }

        public string Identifier
        {
            get { return this._identifier; }
        }

        public ConnectionChainBuildVersioningInfo ConnectionChainBuildVersioningInfo { get; private set; }


        public event ProxyClosingHandler OnProxyClosing;

        public void TransferReliablyOnPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            if (this._priceStreamTransferMode == PriceStreamTransferMode.FastAndUnreliable)
            {
                this._logger.Log(LogLevel.Fatal, "Receiving reliable price transfer while asked for fast unreliable transfers");
            }

            ActivityCheckpoint();
            this.InvokeLocalMethod(() => this._integratorServiceMessagesHandler.OnPriceUpdate(priceUpdateEventArgs));
        }

        public void TransferFastOnPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            if (this._priceStreamTransferMode == PriceStreamTransferMode.Reliable)
            {
                this._logger.Log(LogLevel.Fatal, "Receiving fast unreliable price transfer while asked for reliable transfers");
            }

            ActivityCheckpoint();
            this.InvokeLocalMethod(() => this._integratorServiceMessagesHandler.OnPriceUpdate(priceUpdateEventArgs));
        }

        public void TransferReliablyIntegratorInfoObject(IntegratorInfoObjectBase integratorInfoObject)
        {
            ActivityCheckpoint();
            this.InvokeLocalMethod(
                () =>
                this._integratorServiceMessagesHandler.OnIntegratorUnicastInfo(integratorInfoObject));
        }

        public void TransferReliablyIntegratorBroadcastInfoObject(IntegratorBroadcastInfoBase integratorInfoObject)
        {
            ActivityCheckpoint();
            this.InvokeLocalMethod(
                () =>
                this._integratorServiceMessagesHandler.OnIntegratorBroadcastInfo(integratorInfoObject));
        }

        private void InvokeLocalMethod(Action localMethodInvocation)
        {
            try
            {
                localMethodInvocation();
            }
            catch (FaultException e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "TradingService experienced exception during the local call.");
                throw;
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "TradingService experienced exception during the local call.");
                throw new FaultException(string.Format("TradingService experienced exception during the local call. {0}", e));
            }
        }

        //public void TransferAnnounceStateChange(CommunicationStateChangeEventArgs eventArgs)
        //{
        //    this._logger.Log(LogLevel.Info, "{0} Receiving state change: [{1}]", this._identifier, eventArgs);

        //    if (eventArgs.CommunicationServiceStateAction == CommunicationServiceStateAction.ShutDown)
        //    {
        //        this.InvalidateConnection(eventArgs.Reason);
        //    }

        //    this._integratorServiceMessagesHandler.AnnounceStateChange(eventArgs);
        //}

        //this is called through proxies => remotly
        public void CloseLocalResourcesOnly(string reason)
        {
            this._logger.Log(LogLevel.Info, "Closing {0} per request: {1}", this._identifier, reason);

            //this comed from server - so no unregistration

            this.InvalidateConnection(reason, false);

            this._integratorServiceMessagesHandler.CloseLocalResourcesOnly(reason);
        }

        //This is local request
        public void CloseProxyAndSignalDisconnect(string reason)
        {
            this._logger.Log(LogLevel.Info, "Closing {0} per request: {1}", this._identifier, reason);

            try
            {
                _integratorServiceProxy.Unregister(this._identifier);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Warn, e, "Got errot during unregistering client {0}.", this._identifier);
            }


            this.InvalidateConnection(reason, true);

            this._integratorServiceMessagesHandler.CloseLocalResourcesOnly(reason);
        }

        public IntegratorRequestResult SubmitRequest(IntegratorRequestInfo requestInfo)
        {
            Func<IntegratorRequestResult> submitRequestInvoke = () => _integratorServiceProxy.SubmitRequest(this._identifier, requestInfo);
            return this.InvokeRemoteMethod(submitRequestInvoke);
        }

        private IntegratorRequestResult InvokeRemoteMethod(Func<IntegratorRequestResult> remoteMethodInvocation)
        {
            bool completed = false;
            //bool succeeded = false;
            IntegratorRequestResult result = null;
            int consecutiveFailures = 0;
            do
            {
                try
                {
                    this._isConnectedEvent.Wait(this._isFaultedTokenSource.Token);
                    //there is a slight chance for nullpointerexception but that would mean that
                    // someone was invalidating the session in the meantime, so we want to TryReconnect anyway
                    result = remoteMethodInvocation();
                    completed = true;
                    ActivityCheckpoint();
                }
                catch (FaultException e)
                {
                    this._logger.LogException(LogLevel.Error, e, "Remote action invocation failed because other end failed.");
                    result =
                        IntegratorRequestResult.CreateFailedResult(
                            "Remote action invocation failed because other end failed. Exception: " + e);
                    break;
                }
                catch (OperationCanceledException)
                {
                    this._logger.Log(LogLevel.Warn, "Remote action invocation failed as proxy cancellation came in the meantime");
                    result = result ??
                        IntegratorRequestResult.CreateFailedResult(
                            "Remote action invocation failed as proxy cancellation came in the meantime.");
                    break;
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Warn, e,
                        "Remote action invocation failed due to communication layer issues. Will try to recconect and try later.");
                    result =
                        IntegratorRequestResult.CreateFailedResult(
                            "Remote action invocation failed due to communication layer issues. Exception: " + e);

                    if (++consecutiveFailures > _maximumFailingCallAttempts)
                    {
                        this._logger.Log(LogLevel.Fatal, "Failed {0} times to perform remote call [{1}]. Not atempting anymore", consecutiveFailures, remoteMethodInvocation);
                        break;
                    }

                    TryReconnect();
                }
            } while (!completed);

            return result;
        }

        private void OnChannelFaulted(object sender, EventArgs args)
        {
            this._logger.Log(LogLevel.Warn, "LocalGatewayClient [{0}] getting to faulted state.", this._identifier);
            this.TryReconnect();
        }

        private void OnChannelClosed(object sender, EventArgs args)
        {
            this._logger.Log(LogLevel.Warn, "LocalGatewayClient [{0}] getting to closed state", this._identifier);
            this.TryReconnect();
        }

        private void CheckConnection()
        {
            this._logger.Log(LogLevel.Info, "No activity, trying to ping the server");

            try
            {
                _integratorServiceProxy.Ping();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Warn, e, "Ping operation failed.");
                TryReconnect();
            }
        }

        private void ActivityCheckpoint()
        {
            if (!_isFaultedTokenSource.IsCancellationRequested)
            {
                _checkConnectionTimer.Change(MessageBusSettings.Sections.ConnectionKeeping.SessionHealthCheckInterval,
                                             MessageBusSettings.Sections.ConnectionKeeping.SessionHealthCheckInterval);
            }
        }

        private void CloseConnection()
        {
            lock (_connectionChangesLocker)
            {
                if (_integratorServiceProxy != null)
                {
                    _integratorServiceProxy.InnerChannel.Closed -= this.OnChannelClosed;
                    _integratorServiceProxy.InnerChannel.Faulted -= this.OnChannelFaulted;
                    try
                    {
                        _integratorServiceProxy.Close();
                    }
                    catch (Exception e)
                    {
                        this._logger.LogException(LogLevel.Warn, e, "Failed during closing the invalidated connection.");
                        _integratorServiceProxy.Abort();
                    }

                    _integratorServiceProxy = null;
                }

                _isConnectedEvent.Reset();
            }
        }

        private void InvalidateConnection(string reason, bool isLocalOnlyRequest)
        {
            bool wasFaulted;

            lock (_connectionChangesLocker)
            {
                wasFaulted = _isFaultedTokenSource.IsCancellationRequested;
                _isFaultedTokenSource.Cancel();
            }

            this.CloseConnection();
            _isConnectedEvent.Reset();

            if (!wasFaulted)
            {
                _checkConnectionTimer.Dispose();
                if (OnProxyClosing != null)
                {
                    OnProxyClosing(this, reason, isLocalOnlyRequest);
                }
            }
        }

        private volatile bool _connecting;
        private bool _alreadyRegistered = false;
        private DateTime _beforeLastReconnect = DateTime.MinValue;

        private ClientRegistrationRequest CreateRegistrationRequest()
        {
            return new ClientRegistrationRequest(this._identifier, this._priceStreamTransferMode,
                                                 this._perSymbolPriceThrottlingInterval,
                                                 new BuildVersioningInfo(BuildConstants.CURRENT_BUILD_VERSION,
                                                                         BuildConstants
                                                                             .MINIMUM_VERSION_NUMBER_OF_COMPATIBLE_BUILD,
                                                                         BuildConstants.CURRENT_VERSION_CREATED_UTC),
                                                 this._forceConnectOnVersionMismatch,
                                                 this._keepLocalClientNameAsUniqueRemotingIdentifer);
        }

        private enum ReconnectionResult
        {
            Connected,
            EndpointNotFound,
            ServerRefused,
            Other
        }

        private ReconnectionResult TryReconnect()
        {
            lock (_connectionChangesLocker)
            {
                if (_connecting) return ReconnectionResult.Other;
                _connecting = true;
            }

            //this is redundant, but it will stop incoming calls from cycling in failures a bit earlier
            _isConnectedEvent.Reset();

            bool connected = false;
            ReconnectionResult reconnectionResult = ReconnectionResult.Other;
            DateTime beforeCurrentReconnect = DateTime.UtcNow;
            //We want to prevent fast repeated attempts for reconnection with no delay
            if (beforeCurrentReconnect - _beforeLastReconnect < this._minReconnectDelay)
            {
                _isFaultedTokenSource.Token.WaitHandle.WaitOne(this._minReconnectDelay);
                beforeCurrentReconnect = DateTime.UtcNow;
            }
            _beforeLastReconnect = beforeCurrentReconnect;
            while (!connected && !_isFaultedTokenSource.Token.IsCancellationRequested &&
                   _beforeLastReconnect >
                   DateTime.UtcNow.Subtract(MessageBusSettings.Sections.ConnectionKeeping.MaximumAllowedDisconnectedInterval))
            {
                this.CloseConnection();
                if (_isFaultedTokenSource.IsCancellationRequested) break;

                this._logger.Log(LogLevel.Info, "Trying to connect to the server on {0}", this._connectAddress);
                InstanceContext context = new InstanceContext(this);
                _integratorServiceProxy = new IntegratorServiceProxy(context, this._binding,
                                                                     new EndpointAddress(this._connectAddress));

                _integratorServiceProxy.InnerChannel.Closed += this.OnChannelClosed;
                _integratorServiceProxy.InnerChannel.Faulted += this.OnChannelFaulted;

                try
                {
                    if (!_alreadyRegistered)
                    {
                        ClientRegistrationResult registrationResult =
                            this._integratorServiceProxy.Register(this.CreateRegistrationRequest());

                        if (registrationResult.RegistrationSucceded)
                        {
                            this._identifier = registrationResult.UniqueClientIdentifier;
                            this.ConnectionChainBuildVersioningInfo =
                                registrationResult.ConnectionChainBuildVersioningInfo;
                            this._logger.Log(LogLevel.Info,
                                             "Client registered, communication chain versioning info: {0}",
                                             this.ConnectionChainBuildVersioningInfo);
                            _alreadyRegistered = true;
                        }
                        else
                        {
                            this._logger.Log(LogLevel.Error, "Couldn't (re)connect. Server error: {0}.",
                                             registrationResult.RegistrationError);
                            break;
                        }
                    }
                    else
                    {
                        this._integratorServiceProxy.RecreateSession(this._identifier);
                    }
                    connected = true;
                    reconnectionResult = ReconnectionResult.Connected;
                }
                catch (FaultException e)
                {
                    this._logger.LogException(LogLevel.Error, e,
                                              "Couldn't (re)connect. Server bussiness logic refused us (duplicate id?).");
                    reconnectionResult = ReconnectionResult.ServerRefused;
                    break;
                }
                catch (EndpointNotFoundException e)
                {
                    this._logger.LogException(LogLevel.Error, e,
                                              "Couldn't (re)connect. Endpoint not found.");
                    reconnectionResult = ReconnectionResult.EndpointNotFound;
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Warn, e, "Couldn't (re)connect. Will retry.");
                    _isFaultedTokenSource.Token.WaitHandle.WaitOne(
                        MessageBusSettings.Sections.ConnectionKeeping.DisconnectedSessionReconnectInterval);
                }
            }

            if (!connected)
            {
                //since we couldn't reconnect to remote endpoint it's technically a remote request
                //also we get here if remote endpoint stopped responding and we couldn't reconnect in allocated time
                // in that case we need to trigger seamless-transfer logic (so we need to mark request as remote)
                this.InvalidateConnection("Couldn't reconnect to server in allocated time or server refused", false);
            }
            else
            {
                lock (_connectionChangesLocker)
                {
                    _isConnectedEvent.Set();
                    ActivityCheckpoint();
                }
            }

            lock (_connectionChangesLocker)
            {
                _connecting = false;
            }

            IntegratorServiceProxy integratorProxy = _integratorServiceProxy;
            if (integratorProxy != null && integratorProxy.State != CommunicationState.Opened)
            {
                return this.TryReconnect();
            }

            return reconnectionResult;
        }
    }
}
