﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.MessageBus
{
    public class IntegratorDiagnosticsClient : IIntegratorDiagnosticsClient, IRemoteCommandWorker
    {
        private IntegratorDiagnosticsServiceProxy _integratorDiagnosticsServiceProxy;
        private ILogger _logger;
        private IIntegratorInstanceProcessUtils _instanceProcessUtils;

        public IntegratorDiagnosticsClient(ILogger logger, IIntegratorInstanceProcessUtils instanceProcessUtils)
        {
            this._logger = logger;
            this._instanceProcessUtils = instanceProcessUtils;
        }

        public string TargetEndpoint { get; private set; }

        private IntegratorRequestResult CreateThrowableFailedResult(string errorMessage, bool @throw)
        {
            if (@throw)
                throw new Exception(errorMessage);
            else
                return IntegratorRequestResult.CreateFailedResult(errorMessage);
        }

        public IntegratorRequestResult Connect(bool throwOnError)
        {
            IntegratorProcessInfo targetIntegratorProcessDetail = null;
            IEnumerable<IntegratorProcessInfo> processesDetails;

            try
            {
                processesDetails = _instanceProcessUtils.GetIntegratorProcessDetails();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Error, "Couldn't obtain integrator processes details from backend", e);
                return this.CreateThrowableFailedResult("Couldn't obtain integrator processes details from backend", throwOnError);
            }
            
            
            foreach (IntegratorProcessInfo integratorProcessDetail in processesDetails)
            {
                if (integratorProcessDetail.IntegratorProcessState == IntegratorProcessState.Running)
                {
                    if (targetIntegratorProcessDetail == null)
                        targetIntegratorProcessDetail = integratorProcessDetail;
                    else
                    {
                        this._logger.Log(LogLevel.Fatal, "Multiple target process in running state ({0} and {1})",
                                         targetIntegratorProcessDetail.ProcessIdentifier,
                                         integratorProcessDetail.ProcessIdentifier);
                        return this.CreateThrowableFailedResult("Multiple target process in running state", throwOnError);
                    }
                }
            }

            if (targetIntegratorProcessDetail == null)
                return this.CreateThrowableFailedResult("No records of the requested process type for the current instance found", throwOnError);

            this.TargetEndpoint =
                targetIntegratorProcessDetail.GetTargetHost(
                    MessageBusSettings.Sections.IntegratorEndpointConnectionInfo.ConnectionType);
            int port = targetIntegratorProcessDetail.DiagnosticsAPIPort.Value;

            this.Connect(this.TargetEndpoint, port);
            return IntegratorRequestResult.GetSuccessResult();
        }

        public void Connect(string endpoint, int port)
        {
            var connectAddress =
                        new Uri("net.tcp://" + endpoint + ":" + port);
            var binding = new NetTcpBinding(SecurityMode.None);
            binding.MaxReceivedMessageSize *= 10;
            binding.MaxBufferSize *= 10;

            _integratorDiagnosticsServiceProxy = new IntegratorDiagnosticsServiceProxy(binding, new EndpointAddress(connectAddress));
        }

        public PriceStreamsMonitoringInfo GetPriceStreamsMonitoringInfo()
        {
            try
            {
                return _integratorDiagnosticsServiceProxy.GetPriceStreamsMonitoringInfo();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, "Got exception when obtaining monitoring info", e);
                throw;
            }
        }

        public void Ping()
        {
            try
            {
                _integratorDiagnosticsServiceProxy.Ping();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, "Got exception when pinging", e);
                throw;
            }
        }

        public IntegratorRequestResult TrySendCommand(string command)
        {
            try
            {
                return _integratorDiagnosticsServiceProxy.TrySendCommand(command);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Got exception when sending command: {0}", command);
                return IntegratorRequestResult.CreateFailedResult(string.Format("Got exception: {0}", e.GetType().ToString()));
            }
        }
    }
}
