﻿namespace Kreslik.Integrator.NMbgDevIO
{
    using System.Runtime.InteropServices;

    internal static class PerformanceCounterImports
    {
        [DllImport("Kernel32.dll")]
        [System.Security.SuppressUnmanagedCodeSecurity()]
        public static extern bool QueryPerformanceCounter(out long lpPerformanceCount);

        [DllImport("Kernel32.dll")]
        [System.Security.SuppressUnmanagedCodeSecurity()]
        public static extern bool QueryPerformanceFrequency(out long lpFrequency);
    }
}
