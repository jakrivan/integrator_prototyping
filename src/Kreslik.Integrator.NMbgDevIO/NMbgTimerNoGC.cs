﻿namespace Kreslik.Integrator.NMbgDevIO
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.Runtime.InteropServices;
    using System.Threading;

    public class NMbgTimerNoGC
    {
        private static readonly DateTime _epochTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private NMbgDevIOSafeHandle _deviceHandle;

        private const long FRACS_PER_SECOND = uint.MaxValue + 1L;
        private const uint TICKS_PER_SECOND = 10000000;

        private readonly long _performanceCounterFrequency;

        //Following varibales are fol 'polling' timer
        //Such a timer periodically refreshes it's internal timestamps and switches the bool switch
        // indicating which timestamp is refreshed, that way calling code can never use a timestamp
        // that is neing refreshed in the meantime (it may only use slightly outdated timestamp, but
        // it compesates aging with PerformanceCounter value anyway)
        private Timer _pollTimer;
        //private TimeSpan _pollInterval;
        private int _pollIntervalIdx = 0;
        private TimeSpan[] _pollIntervals = new TimeSpan[] { TimeSpan.FromMilliseconds(15), TimeSpan.FromSeconds(1), TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(10) };
        private NMbgDateTime _timeStampA;
        private long _timeStampATakenAtCyclesValue;
        private NMbgDateTime _timeStampB;
        private long _timeStampBTakenAtCyclesValue;
        private bool _isTimeStampATheNewerOne = false;
        private long _timeStampTakenAt;

        private bool _noDeviceAccess = false;
        private static NMbgDateTime _startTime = NMbgDateTimeHelper.FromDateTime(DateTime.UtcNow);
        private static Stopwatch _stopWatch = Stopwatch.StartNew();

        //Factory methods

        public static NMbgTimerNoGC GetAsynchronousTimer()
        {
            return new NMbgTimerNoGC(0, true, TimeSpan.FromMilliseconds(15));
        }

        public static NMbgTimerNoGC GetAsynchronousTimer(uint deviceIdToUse)
        {
            return new NMbgTimerNoGC(deviceIdToUse, false, TimeSpan.FromMilliseconds(15));
        }

        public static NMbgTimerNoGC GetAsynchronousTimer(TimeSpan pollInterval)
        {
            return new NMbgTimerNoGC(0, true, pollInterval);
        }

        public static NMbgTimerNoGC GetAsynchronousTimer(uint deviceIdToUse, TimeSpan pollInterval)
        {
            return new NMbgTimerNoGC(deviceIdToUse, false, pollInterval);
        }

        public static NMbgTimerNoGC GetBasicTimerWithoutMeibergClock()
        {
            return new NMbgTimerNoGC();
        }

        private NMbgTimerNoGC()
        {
            _noDeviceAccess = true;
            IsAccurateDateTimeAvailable = false;
        }

        private bool _isAccurateDateTimeAvailable;
        public bool IsAccurateDateTimeAvailable
        {
            get { return this._isAccurateDateTimeAvailable; }

            private set
            {
                if (value != this._isAccurateDateTimeAvailable && this.AccurateDateTimeAvailabilityChanged != null)
                {
                    try
                    {
                        this.AccurateDateTimeAvailabilityChanged(value);
                    }
                    catch (Exception e)
                    {
                        if (UnhandledAsynchronousException != null)
                        {
                            UnhandledAsynchronousException(e);
                        }
                    }
                }
                this._isAccurateDateTimeAvailable = value;
            }
        }

        public event Action<bool> AccurateDateTimeAvailabilityChanged;

        private NMbgTimerNoGC(uint deviceIdToUse, bool deviceIdNotSpecified, TimeSpan pollInterval)
        {
            //Stopwatch.Frequency below is more direct equivalent of P/Invoke that we would need to do:
            //
            //if (PerformanceCounterImports.QueryPerformanceFrequency(out _performanceCounterFrequency) == false)
            //{
            //    // high-performance counter not supported
            //    throw new NMbgTimerException("PerformanceCounter not supported on this system.");
            //}
            //
            // We might also want to throw if Stopwatch.IsHighPerformance != true and Async timer is requested
            _performanceCounterFrequency = Stopwatch.Frequency;

            MbgDevIOImports.Initialize();

            int devicesNum = MbgDevIOImports.mbg_find_devices();

            if (devicesNum <= 0)
            {
                throw new NMbgTimerException("No Meinberg device found on this system.");
            }

            if (deviceIdNotSpecified && devicesNum > 1)
            {
                throw new NMbgTimerException("There are more then one Meinberg device on this system, you need to specify id of device to be used");
            }

            if (devicesNum < deviceIdToUse + 1)
            {
                throw new NMbgTimerException("There are less Meinberg devices on this system then specified by the id");
            }

            _deviceHandle = MbgDevIOImports.OpenDevice((int)deviceIdToUse);
            if (_deviceHandle.IsInvalid)
            {
                int errorCode = Marshal.GetLastWin32Error();
                throw new NMbgTimerException(string.Format(CultureInfo.CurrentCulture,
                                                           "Opening of Meinberg device failed. Last error code: {0}",
                                                           errorCode));
            }

            //Sanity check that the timer returns synchronized time
            // if not - then the following call will throw and ctor will be aborted
            this.GetUtcTimeStampFromDevice_noGc();

            this.IsAccurateDateTimeAvailable = true;

            if (!_noDeviceAccess)
            {
                _pollIntervals[0] = pollInterval;
                _pollTimer = new Timer(PollDevice);
                PollDevice(null);
            }
        }

        /// <summary>
        /// Returns current UTC timestamp. This is a nonblocking call
        /// WARNING: DateTime is an immutable type so temporary structs - and so memory garbage - needs to be crated by this call
        /// </summary>
        public DateTime UtcNow
        {
            get
            {
                NMbgDateTime tempDatetime = new NMbgDateTime();
                this.GetUtcNow(ref tempDatetime);
                return NMbgDateTimeHelper.ToDateTime(ref tempDatetime);
            }
        }

        public void GetUtcNow(ref NMbgDateTime timestamp)
        {
            if (_noDeviceAccess)
            {
                NMbgDateTimeHelper.InitializeWithSourceTimestamp(ref _startTime, ref timestamp);
                NMbgDateTimeHelper.AddTicks(ref timestamp, _stopWatch.Elapsed.Ticks);
            }
            else
            {
                long timeStampRequestedAt = Stopwatch.GetTimestamp();
                long timeStampTakenAt;
                if (_isTimeStampATheNewerOne)
                {
                    NMbgDateTimeHelper.InitializeWithSourceTimestamp(ref _timeStampA, ref timestamp);
                    timeStampTakenAt = _timeStampATakenAtCyclesValue;
                }
                else
                {
                    NMbgDateTimeHelper.InitializeWithSourceTimestamp(ref _timeStampB, ref timestamp);
                    timeStampTakenAt = _timeStampBTakenAtCyclesValue;
                }

                long ticksDelay = (timeStampRequestedAt - timeStampTakenAt) * TICKS_PER_SECOND /
                                      _performanceCounterFrequency;

                NMbgDateTimeHelper.AddTicks(ref timestamp, ticksDelay);
            }
        }

        public event Action<Exception> UnhandledAsynchronousException;
        public event Action<string> NewInformationAvailable;

        private void PollDevice(object unused)
        {
            try
            {
                if (_isTimeStampATheNewerOne)
                {
                    this.GetUtcTimeStampFromDevice_noGc();
                }
                else
                {
                    this.GetUtcTimeStampFromDevice_noGc();
                }
                if (_pollIntervalIdx != 0)
                {
                    if (NewInformationAvailable != null)
                    {
                        NewInformationAvailable(
                            "Timer device is now working correctly. Reseting the timer device polling interval back to default value.");
                    }
                    _pollIntervalIdx = 0;
                    this.IsAccurateDateTimeAvailable = true;
                }
            }
            catch (Exception e)
            {
                if (UnhandledAsynchronousException != null)
                {
                    UnhandledAsynchronousException(e);
                }
                _pollIntervalIdx = Math.Min(_pollIntervalIdx + 1, _pollIntervals.Length - 1);
                this.IsAccurateDateTimeAvailable = false;
            }

            _pollTimer.Change((int)_pollIntervals[_pollIntervalIdx].TotalMilliseconds, Timeout.Infinite);
        }

        private MbgDevIOImports.PCPS_HR_TIME_CYCLES _timeStruct = new MbgDevIOImports.PCPS_HR_TIME_CYCLES();

        private void GetUtcTimeStampFromDevice_noGc()
        {
            //MbgDevIOImports.PCPS_HR_TIME_CYCLES timeStruct = _timeStruct;

            //if (!_isAsynchronous)
            //{
            //    timeStruct = new MbgDevIOImports.PCPS_HR_TIME_CYCLES();
            //}

            //here is the point where we need to safe the perf counter value to find out card access latency
            _timeStampTakenAt = Stopwatch.GetTimestamp();

            MbgDevIOImports.GetHrTimeCycles(_deviceHandle, ref _timeStruct);

            //DCF77 clock running on xtal
            //  GPS receiver has not verified its position
            if ((_timeStruct.t.status & MbgDevs.PCPS_FREER) == MbgDevs.PCPS_FREER)
            {
                throw new NMbgTimerException("Meinberg clock are NOT synchronized (status contains PCPS_FREER bit).");
            }

            //Invalid time because battery was disconnected.
            //
            // IRIG receiver cards may set the PCPS_INVT bit also if the card's ref time offset 
            // to UTC has not yet been configured, or if the on-board date does not match the 
            // day-of-year number of the incoming IRIG signal. The reason for this is to avoid 
            // accepting wrong times from the IRIG signal due to the ambiguity of the time and
            // day-of-year from the IRIG signal.
            if ((_timeStruct.t.status & MbgDevs.PCPS_INVT) == MbgDevs.PCPS_INVT)
            {
                throw new NMbgTimerException(
                    "Meinberg clock are likely returning wrong time due to battery disconnection or some misconfiguration (status contains PCPS_INVT bit).");
            }

            if ((_timeStruct.t.status & MbgDevs.PCPS_UTC) != MbgDevs.PCPS_UTC)
            {
                throw new NMbgTimerException(
                    "Meinberg clock are likely returning local time instead of UTC due to misconfiguration (status doesn't contain PCPS_UTC bit).");
            }


            //Latency of the call (spend by OS, actuall card access is negligable)
            long ticksLatency = (_timeStruct.cycles - _timeStampTakenAt) * TICKS_PER_SECOND / _performanceCounterFrequency;

            //Ticks are retunred in continuously increasing fromat from 0x0 to 0xFFFFFFFF
            // refer to Meinberg API principles doc for details
            long ticks = ((long)_timeStruct.t.tstamp.frac) * TICKS_PER_SECOND / FRACS_PER_SECOND;

            //Ticks could have overflowen to the next second and so we can have negative here
            // but thats OK - DateTime will handle negative arguments
            ticks -= ticksLatency;

            int seconds = _timeStruct.t.tstamp.sec;

            if (_isTimeStampATheNewerOne)
            {
                NMbgDateTimeHelper.Initialize(ref _timeStampB, seconds, ticks);
                _timeStampBTakenAtCyclesValue = _timeStampTakenAt;
                _isTimeStampATheNewerOne = false;
            }
            else
            {
                NMbgDateTimeHelper.Initialize(ref _timeStampA, seconds, ticks);
                _timeStampATakenAtCyclesValue = _timeStampTakenAt;
                _isTimeStampATheNewerOne = true;
            }
        }
    }
}
