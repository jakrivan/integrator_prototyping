﻿namespace Kreslik.Integrator.NMbgDevIO
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception for wrapping native mbgdevio errors in managed exceptions
    /// </summary>
    [Serializable]
    public class NMbgTimerException : Exception
    {
        public NMbgTimerException()
        {
        }

        public NMbgTimerException(string message)
            : base(message)
        {
        }

        public NMbgTimerException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected NMbgTimerException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}
