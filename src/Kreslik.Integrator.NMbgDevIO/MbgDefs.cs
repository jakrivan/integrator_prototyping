﻿namespace Kreslik.Integrator.NMbgDevIO
{
    internal sealed class MbgDevs
    {
        public const short PCPS_SUCCESS = (short) 0;
        public const short PCPS_FREER = (short) 1;
        public const short PCPS_DL_ENB = (short) 2;
        public const short PCPS_SYNCD = (short) 4;
        public const short PCPS_DL_ANN = (short) 8;
        public const short PCPS_UTC = (short) 16;
        public const short PCPS_LS_ANN = (short) 32;
        public const short PCPS_IFTM = (short) 64;
        public const short PCPS_INVT = (short) 128;
        public const short PCPS_LS_ENB = (short) 256;
        public const short PCPS_ANT_FAIL = (short) 512;
        public const short PCPS_UCAP_OVERRUN = (short) 8192;
        public const short PCPS_UCAP_BUFFER_FULL = (short) 16384;

        // Max value of PCPS_TIME_STAMP::frac + 1 used for scaling
        public const long PCPS_HRT_BIN_FRAC_SCALE = 4294967296; // uint.max + 1 == 0x100000000
    }
}
