﻿namespace Kreslik.Integrator.NMbgDevIO
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.ConstrainedExecution;
    using System.Text;
    using Microsoft.Win32.SafeHandles;

    public class NMbgDevIOSafeHandle : SafeHandleMinusOneIsInvalid
    {
        //This instance of safe handle owns the underlaying handle (the true argument)
        // therefore the ReleaseHandle will be called (as 'critical finalizer')
        private NMbgDevIOSafeHandle() 
            : base(true)
        { }

        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
        protected override bool ReleaseHandle()
        {
            MbgDevIOImports.CloseDevice(ref handle);

            return true;
        }
    }
}
