﻿namespace Kreslik.Integrator.NMbgDevIO
{
    using System;

    public struct NMbgDateTime
    {
        public int SecondsSinceEpoche { get; internal set; }
        public uint Ticks { get; internal set; }
    }

    //Separate helper class will help keeping the struct lean-mean
    public static class NMbgDateTimeHelper
    {
        private const uint TICKS_PER_SECOND = 10000000;
        private static readonly DateTime _epochTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static void AddTicks(ref NMbgDateTime sourceTimeStamp, long ticksCount)
        {
            long ticksTotal = sourceTimeStamp.Ticks + ticksCount;

            sourceTimeStamp.SecondsSinceEpoche += (int) (ticksCount / TICKS_PER_SECOND);

            if (ticksTotal < 0)
            {
                sourceTimeStamp.SecondsSinceEpoche--;
                sourceTimeStamp.Ticks = (uint)((int)(ticksTotal % TICKS_PER_SECOND) + TICKS_PER_SECOND);
            }
            else
            {
                sourceTimeStamp.Ticks = (uint) (ticksTotal % TICKS_PER_SECOND);
            }
        }

        public static void Initialize(ref NMbgDateTime targetTimeStamp, int seconds, long ticksCount)
        {
            targetTimeStamp.SecondsSinceEpoche = seconds;
            targetTimeStamp.Ticks = 0;
            AddTicks(ref targetTimeStamp, ticksCount);
        }

        public static void InitializeWithSourceTimestamp(ref NMbgDateTime sourceTimeStamp, ref NMbgDateTime targetTimeStamp)
        {
            targetTimeStamp.SecondsSinceEpoche = sourceTimeStamp.SecondsSinceEpoche;
            targetTimeStamp.Ticks = sourceTimeStamp.Ticks;
        }

        /// <summary>
        /// Converts the NMbgDateTime to DateTime type
        /// WARNING: this requires creation of Dimmutable DateTime and so means creation of memory garbage
        /// </summary>
        /// <param name="sourceTimeStamp">NMbgDateTime timestamp struct</param>
        /// <returns>Newly allocated and initialized DateTime struct</returns>
        public static DateTime ToDateTime(ref NMbgDateTime sourceTimeStamp)
        {
            DateTime dt = _epochTime.AddSeconds(sourceTimeStamp.SecondsSinceEpoche);
            return dt.AddTicks(sourceTimeStamp.Ticks);
        }

        public static NMbgDateTime FromDateTime(DateTime utcTime)
        {
            return new NMbgDateTime()
                {
                    SecondsSinceEpoche = (int) (utcTime - _epochTime).TotalSeconds,
                    Ticks = (uint) (utcTime.Ticks%TICKS_PER_SECOND)
                };
        }
    }
}
