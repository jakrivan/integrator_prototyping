﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.RiskManagement
{
    internal enum LimitsCheckingMode
    {
        NoCheck,
        CheckWithoutEvents,
        CheckWithEvents
    }

    public class UnsettledNopCreditLineLimitsSettingsPoint
    {
        public UnsettledNopCreditLineLimitsSettingsPoint(AtomicSize maximumUnsettledNop,
            AtomicSize maximumUnsettledNopToLockDestination)
        {
            this.Update(maximumUnsettledNop, maximumUnsettledNopToLockDestination);
        }

        public void Update(AtomicSize maximumUnsettledNop,
            AtomicSize maximumUnsettledNopToLockDestination)
        {
            this.MaximumUnsettledNop = maximumUnsettledNop;
            this.MaximumUnsettledNopToLockDestination = maximumUnsettledNopToLockDestination;
        }

        public AtomicSize MaximumUnsettledNop { get; private set; }
        public AtomicSize MaximumUnsettledNopToLockDestination { get; private set; }
    }

    public class UnsettledNopCreditLineSettingsPoint
    {
        public UnsettledNopCreditLineSettingsPoint(NopSettingsPointPerCreditLine unsettletNopSettingsPoint)
        {
            this.DatabaseId = unsettletNopSettingsPoint.CreditLineId;
            this.CreditLineName = unsettletNopSettingsPoint.CreditLineName;
            this.LimitsTotal = new UnsettledNopCreditLineLimitsSettingsPoint(
                AtomicSize.FromDecimal(unsettletNopSettingsPoint.MaximumUnsettledNopTotal),
                AtomicSize.FromDecimal(unsettletNopSettingsPoint.MaximumUnsettledNopTotalToLockDestination));
            this.LimitsPerValueDate = new UnsettledNopCreditLineLimitsSettingsPoint(
                AtomicSize.FromDecimal(unsettletNopSettingsPoint.MaximumUnsettledNopPerValueDate),
                AtomicSize.FromDecimal(unsettletNopSettingsPoint.MaximumUnsettledNopPerValueDateToLockDestination));
        }

        public void UpdateIfMatch(NopSettingsPointPerCreditLine unsettletNopSettingsPoint)
        {
            if (unsettletNopSettingsPoint.CreditLineId == this.DatabaseId)
            {
                this.CreditLineName = unsettletNopSettingsPoint.CreditLineName;
                this.LimitsTotal.Update(
                    AtomicSize.FromDecimal(unsettletNopSettingsPoint.MaximumUnsettledNopTotal),
                    AtomicSize.FromDecimal(unsettletNopSettingsPoint.MaximumUnsettledNopTotalToLockDestination));
                this.LimitsPerValueDate.Update(
                    AtomicSize.FromDecimal(unsettletNopSettingsPoint.MaximumUnsettledNopPerValueDate),
                    AtomicSize.FromDecimal(unsettletNopSettingsPoint.MaximumUnsettledNopPerValueDateToLockDestination));
            }
        }

        public int DatabaseId { get; private set; }
        public string CreditLineName { get; private set; }
        public UnsettledNopCreditLineLimitsSettingsPoint LimitsTotal { get; private set; }
        public UnsettledNopCreditLineLimitsSettingsPoint LimitsPerValueDate { get; private set; }
    }

    public class UnsettledNopCheckingModule
    {

        //Encapsulate the stats into one class, so that we cann access those via one pointer, that can be atomically swapped,
        // this way can be sure that during rollover procedure we'll not interfere with ongoing trading and this can be achieved without need
        // for locking (that would need to be checked on every stats update anyway)
        private class UnsettledNopStats
        {
            public UnsettledNopStats(int creditLinesNum, int valueDatesNum)
            {
                this._creditLinesNum = creditLinesNum;
                this.UpdateSettlemtDateBaseDate();
                _settlementDatesNum = valueDatesNum;

                NopStatsPerCreditLineOverall = new UnsettledNopStatsPerCreditLine(creditLinesNum);
                NopStatsPerCreditLinePerSettlementDate =
                    Enumerable.Repeat(1, valueDatesNum)
                        .Select(i => new UnsettledNopStatsPerCreditLine(creditLinesNum))
                        .ToArray();
            }

            internal void CopyFrom(UnsettledNopStats statsCopy)
            {
                this._settlementDatesNum = statsCopy._settlementDatesNum;
                this._settlementDateOffsetBase = statsCopy._settlementDateOffsetBase;

                this.NopStatsPerCreditLineOverall.CopyFrom(statsCopy.NopStatsPerCreditLineOverall);

                Array.Resize(ref this.NopStatsPerCreditLinePerSettlementDate,
                    statsCopy.NopStatsPerCreditLinePerSettlementDate.Length);

                for (int arrIdx = 0; arrIdx < this.NopStatsPerCreditLinePerSettlementDate.Length; arrIdx++)
                {
                    if (statsCopy.NopStatsPerCreditLinePerSettlementDate[arrIdx] != null)
                    {
                        if (this.NopStatsPerCreditLinePerSettlementDate[arrIdx] == null)
                            this.NopStatsPerCreditLinePerSettlementDate[arrIdx] =
                                new UnsettledNopStatsPerCreditLine(this._creditLinesNum);

                        this.NopStatsPerCreditLinePerSettlementDate[arrIdx].CopyFrom(
                            statsCopy.NopStatsPerCreditLinePerSettlementDate[arrIdx]);
                    }
                    else
                    {
                        this.NopStatsPerCreditLinePerSettlementDate[arrIdx] = null;
                    }
                }
            }

            internal void RecalculateUsdAggregations(decimal[] usdConversionMultipliers)
            {
                this.NopStatsPerCreditLineOverall.RecalculateUsdAggregations(usdConversionMultipliers);

                for (int arrIdx = 0; arrIdx < this.NopStatsPerCreditLinePerSettlementDate.Length; arrIdx++)
                {
                    if (this.NopStatsPerCreditLinePerSettlementDate[arrIdx] != null)
                    {
                        this.NopStatsPerCreditLinePerSettlementDate[arrIdx].RecalculateUsdAggregations(
                            usdConversionMultipliers);
                    }
                }
            }

            internal void CopyUsdRecalculationsFrom(UnsettledNopStats statsCopy)
            {
                this.NopStatsPerCreditLineOverall.CopyUsdRecalculationsFrom(statsCopy.NopStatsPerCreditLineOverall);

                for (int arrIdx = 0; arrIdx < this.NopStatsPerCreditLinePerSettlementDate.Length; arrIdx++)
                {
                    if (statsCopy.NopStatsPerCreditLinePerSettlementDate[arrIdx] != null)
                    {
                        this.NopStatsPerCreditLinePerSettlementDate[arrIdx].CopyUsdRecalculationsFrom(
                            statsCopy.NopStatsPerCreditLinePerSettlementDate[arrIdx]);
                    }
                }
            }

            internal void CopyCreditZonesFrom(UnsettledNopStats statsCopy)
            {
                this.NopStatsPerCreditLineOverall.CopyCreditZonesFrom(statsCopy.NopStatsPerCreditLineOverall);

                for (int arrIdx = 0; arrIdx < this.NopStatsPerCreditLinePerSettlementDate.Length; arrIdx++)
                {
                    if (statsCopy.NopStatsPerCreditLinePerSettlementDate[arrIdx] != null)
                    {
                        this.NopStatsPerCreditLinePerSettlementDate[arrIdx].CopyCreditZonesFrom(
                            statsCopy.NopStatsPerCreditLinePerSettlementDate[arrIdx]);
                    }
                }
            }

            public void UpdateSettlemtDateBaseDate()
            {
                _settlementDateOffsetBase =
                    DateTimeEx.Min(TradingHoursHelper.Instance.LastCurrentlyUnsettledSettlementDateNY,
                        TradingHoursHelper.Instance.LastCurrentlyUnsettledSettlementDateNZ);
            }

            public void ClearUnsettledNopStats()
            {
                this.NopStatsPerCreditLineOverall.Clear();
                Array.ForEach(this.NopStatsPerCreditLinePerSettlementDate, t => { if (t != null) t.Clear(); });
            }

            public UnsettledNopStatsPerCreditLine GetSettlementDateStats(DateTime settlementDateLocal, ILogger logger)
            {
                int settlementDateIdx = (int)(settlementDateLocal - this._settlementDateOffsetBase).TotalDays;
                if (settlementDateIdx >= this._settlementDatesNum || settlementDateIdx < 0)
                {
                    if (settlementDateIdx > _MAX_REASONABLE_NUM_OF_UNSETTLED_DAYS)
                    {
                        logger.Log(LogLevel.Fatal, "UnsettledNopCheckingModule experienced settlement date [{0}] unexpectedly far in future from base settlement date [{1}] - expected max {2} days. As a result NOP for this value date will not be properly checked",
                            settlementDateLocal, _settlementDateOffsetBase, _MAX_REASONABLE_NUM_OF_UNSETTLED_DAYS);
                        return null;
                    }

                    if (settlementDateIdx < 0)
                    {
                        logger.Log(LogLevel.Fatal, "UnsettledNopCheckingModule experienced settlement date [{0}] unexpectedly in past from base settlement date [{1}]. As a result NOP for this value date will not be properly checked",
                            settlementDateLocal, _settlementDateOffsetBase);
                        return null;
                    }

                    lock (NopStatsPerCreditLinePerSettlementDate)
                    {
                        //double check in case someone else added the date in the meantime
                        if (settlementDateIdx >= this._settlementDatesNum)
                        {
                            UnsettledNopStatsPerCreditLine[] nopStatsPerCreditLinePerSettlementDateLocal =
                                new UnsettledNopStatsPerCreditLine[settlementDateIdx + 1];
                            Array.Copy(NopStatsPerCreditLinePerSettlementDate,
                                nopStatsPerCreditLinePerSettlementDateLocal,
                                NopStatsPerCreditLinePerSettlementDate.Length);
                            UnsettledNopStatsPerCreditLine newStatsPoint = new UnsettledNopStatsPerCreditLine(this._creditLinesNum);
                            nopStatsPerCreditLinePerSettlementDateLocal[settlementDateIdx] = newStatsPoint;
                            this.NopStatsPerCreditLinePerSettlementDate = nopStatsPerCreditLinePerSettlementDateLocal;
                            this._settlementDatesNum = settlementDateIdx + 1;
                            return newStatsPoint;
                        }
                    }
                }

                UnsettledNopStatsPerCreditLine statsPoint =
                    this.NopStatsPerCreditLinePerSettlementDate[settlementDateIdx];

                if (statsPoint == null)
                {
                    lock (NopStatsPerCreditLinePerSettlementDate)
                    {
                        //double check in case someone else added the date in the meantime
                        if (this.NopStatsPerCreditLinePerSettlementDate[settlementDateIdx] == null)
                        {
                            this.NopStatsPerCreditLinePerSettlementDate[settlementDateIdx] = new UnsettledNopStatsPerCreditLine(this._creditLinesNum);
                        }
                        statsPoint = this.NopStatsPerCreditLinePerSettlementDate[settlementDateIdx];
                    }
                }

                return statsPoint;
            }

            public bool TryGetSettlementDateStats(DateTime settlementDateLocal, out UnsettledNopStatsPerCreditLine settlementDateStats)
            {
                int settlementDateIdx = (int)(settlementDateLocal - this._settlementDateOffsetBase).TotalDays;
                settlementDateStats = settlementDateIdx < this._settlementDatesNum && settlementDateIdx >= 0
                    ? this.NopStatsPerCreditLinePerSettlementDate[settlementDateIdx]
                    : null;

                return settlementDateStats != null;
            }

            public DateTime GetSettlementDateLocalForStatsIdx(int statsIdx)
            {
                return _settlementDateOffsetBase.AddDays(statsIdx);
            }

            //Those are in private class - no need for encapsulating in fields (plus fields are unusable with Interlocked operations)
            public UnsettledNopStatsPerCreditLine NopStatsPerCreditLineOverall;
            public UnsettledNopStatsPerCreditLine[] NopStatsPerCreditLinePerSettlementDate;

            private DateTime _settlementDateOffsetBase;
            private int _settlementDatesNum;
            private readonly int _creditLinesNum;
        }

        public class UnsettledNopStatsPerCreditLine
        {
            public UnsettledNopStatsPerCreditLine(int creditLinesNum)
            {
                LongUsdNops = new AtomicSize[creditLinesNum];
                ShortUsdNops = new AtomicSize[creditLinesNum];
                NominalPositions =
                    Enumerable.Repeat(1, creditLinesNum).Select(i => new AtomicSize[Currency.ValuesCount]).ToArray();
                HasAnyRealizedDeal = new bool[creditLinesNum];
                _creditZones = new CreditZone[creditLinesNum];
            }

            public void CopyFrom(UnsettledNopStatsPerCreditLine copy)
            {
                this.LongUsdNops.CopyWholeFrom(copy.LongUsdNops);
                this.ShortUsdNops.CopyWholeFrom(copy.ShortUsdNops);
                this.NominalPositions.CopyWholeFrom(copy.NominalPositions);
                this.HasAnyRealizedDeal.CopyWholeFrom(copy.HasAnyRealizedDeal);
                this._creditZones.CopyWholeFrom(copy._creditZones);
            }

            public void CopyUsdRecalculationsFrom(UnsettledNopStatsPerCreditLine copy)
            {
                this.LongUsdNops.CopyWholeFrom(copy.LongUsdNops);
                this.ShortUsdNops.CopyWholeFrom(copy.ShortUsdNops);
                this._creditZones.CopyWholeFrom(copy._creditZones);
            }

            public void CopyCreditZonesFrom(UnsettledNopStatsPerCreditLine copy)
            {
                this._creditZones.CopyWholeFrom(copy._creditZones);
            }

            public void RecalculateUsdAggregations(decimal[] usdConversionMultipliers)
            {
                for (int creditLineIdx = 0; creditLineIdx < this.LongUsdNops.Length; creditLineIdx++)
                {
                    AtomicSize shortNopUsd = AtomicSize.ZERO;
                    AtomicSize longNopUsd = AtomicSize.ZERO;

                    for (int currIdx = 0; currIdx < Currency.ValuesCount; currIdx++)
                    {
                        AtomicSize nominalPosition = NominalPositions[creditLineIdx][currIdx];
                        if (nominalPosition > AtomicSize.ZERO)
                        {
                            longNopUsd += nominalPosition * usdConversionMultipliers[currIdx];
                        }
                        else if (nominalPosition < AtomicSize.ZERO)
                        {
                            shortNopUsd -= nominalPosition * usdConversionMultipliers[currIdx];
                        }
                    }

                    this.LongUsdNops[creditLineIdx] = longNopUsd;
                    this.ShortUsdNops[creditLineIdx] = shortNopUsd;
                }
            }

            public void Clear()
            {
                this.LongUsdNops.ClearWhole();
                this.ShortUsdNops.ClearWhole();
                this.NominalPositions.ClearWhole();
                this.HasAnyRealizedDeal.ClearWhole();
                this._creditZones.ClearWhole();
            }

            public CreditZone GetCreditZone(int creditLineIdx)
            {
                return this._creditZones[creditLineIdx];
            }

            public bool SetCreditZoneAndGetIsChange(int creditLineIdx, CreditZone newCreditZone)
            {
                if (this._creditZones[creditLineIdx] != newCreditZone)
                {
                    this._creditZones[creditLineIdx] = newCreditZone;

                    return true;
                }

                return false;
            }

            public AtomicSize[][] NominalPositions;
            public AtomicSize[] LongUsdNops;
            public AtomicSize[] ShortUsdNops;
            public bool[] HasAnyRealizedDeal;
            private CreditZone[] _creditZones;
            //TODO: for the (full) restriction zone we need also the list (bitmap) of symbols with opened orders
        }

        private decimal[] _usdConversionMultipliers;
        private int[] _creditLinesMap;
        private bool[] _creditLineAllowedToTradeMap;
        private UnsettledNopStats _unsettledNopStats;
        private UnsettledNopStats _backgroundStatsForConcurrentUpdate;
        private readonly object _backgroundRecalculationLocker = new object();
        private readonly List<IntegratorDealDone> _unsettledDeals = new List<IntegratorDealDone>();
        private UnsettledNopCreditLineSettingsPoint[] _creditLineSettings;
        private readonly ISettlementDatesKeeper _settlementDatesKeeper;
        private readonly ILogger _logger;
        private DateTime _nextStatsRecalculationNeededAfterUtc;
        private const int _MAX_REASONABLE_NUM_OF_UNSETTLED_DAYS = 10;
        private bool _isMaxOrderSizeCheckingAllowed;
        private AtomicSize _maximumAllowedOrderSizeInUsd;
        private IPersistedNopProvider _persistedNopProvider;
        private IPriceStreamMonitor _priceStreamMonitor;

        public UnsettledNopCheckingModule(IPersistedNopProvider persistedNopProvider, IPriceStreamMonitor priceStreamMonitor,
            ISettlementDatesKeeper settlementDatesKeeper, ILogger logger)
        {
            this._usdConversionMultipliers = priceStreamMonitor.UsdConversionMultipliers;
            this._settlementDatesKeeper = settlementDatesKeeper;
            this._logger = logger;
            this._persistedNopProvider = persistedNopProvider;
            this._priceStreamMonitor = priceStreamMonitor;
            this.Initialize(persistedNopProvider.GetNopPolPerCreditLineSettlementAndCurrency(),
                persistedNopProvider.GetCreditLineNopSettings());
            persistedNopProvider.CreditLineNopSettingsChanged += PersistedNopProviderOnCreditLineSettlementSettingsChanged;

            TradingHoursHelper.Instance.NZDRollingOver += RecalculateStatsOnRollover;
            TradingHoursHelper.Instance.MarketRollingOver += RecalculateStatsOnRollover;
            priceStreamMonitor.NewUsdConversionMultipliersAvailable += PriceStreamMonitorOnNewUsdConversionMultipliersAvailable;
        }

        private void PriceStreamMonitorOnNewUsdConversionMultipliersAvailable(decimal[] usdConversionMultipliers)
        {
            this._usdConversionMultipliers = usdConversionMultipliers;
            this.FlushStatsIfChange();
            if (HighResolutionDateTime.UtcNow > _nextStatsRecalculationNeededAfterUtc)
            {
                this.RecalculateStatsOnUsdRateChange();
                _nextStatsRecalculationNeededAfterUtc = HighResolutionDateTime.UtcNow.AddMinutes(5);
            }
        }

        public void Dispose()
        {
            TradingHoursHelper.Instance.NZDRollingOver -= RecalculateStatsOnRollover;
            TradingHoursHelper.Instance.MarketRollingOver -= RecalculateStatsOnRollover;
            _persistedNopProvider.CreditLineNopSettingsChanged -= PersistedNopProviderOnCreditLineSettlementSettingsChanged;
            _priceStreamMonitor.NewUsdConversionMultipliersAvailable -= PriceStreamMonitorOnNewUsdConversionMultipliersAvailable;
        }

        public void Reconfigure(bool isMaxOrderSizeCheckingAllowed, decimal maximumAllowedOrderSizeInUsd)
        {
            if (this._isMaxOrderSizeCheckingAllowed != isMaxOrderSizeCheckingAllowed ||
                this._maximumAllowedOrderSizeInUsd != AtomicSize.FromDecimal(maximumAllowedOrderSizeInUsd))
            {
                this._isMaxOrderSizeCheckingAllowed = isMaxOrderSizeCheckingAllowed;
                this._maximumAllowedOrderSizeInUsd = AtomicSize.FromDecimal(maximumAllowedOrderSizeInUsd);

                lock (_backgroundRecalculationLocker)
                {
                    this.CheckLimits();
                    _backgroundStatsForConcurrentUpdate.CopyCreditZonesFrom(_unsettledNopStats);
                }
            }
        }

        private bool AreCreditLineMappingsSame(IEnumerable<NopSettingsPointPerCreditLine> unsettletNopSettingsPoints)
        {
            //return unsettletNopSettingsPoints.Any(
            //    sett =>
            //        sett.Counterparties.Any(
            //            cpt => this._creditLineSettings[_creditLinesMap[(int) cpt]].DatabaseId != sett.CreditLineId));

            bool[] counterpartiesConfigured = new bool[Counterparty.ValuesCount];

            //Remapping of any counterparty
            foreach (NopSettingsPointPerCreditLine unsettletNopSettingsPoint in unsettletNopSettingsPoints)
            {
                foreach (Counterparty counterparty in unsettletNopSettingsPoint.Counterparties)
                {
                    counterpartiesConfigured[(int)counterparty] = true;
                    if (_creditLinesMap[(int)counterparty] < 0 ||
                        this._creditLineSettings[_creditLinesMap[(int)counterparty]].DatabaseId !=
                        unsettletNopSettingsPoint.CreditLineId)
                        return false;
                }
            }

            //Removing of any counterparty
            for (int ctpIdx = 0; ctpIdx < Counterparty.ValuesCount; ctpIdx++)
            {
                if (_creditLinesMap[ctpIdx] != -1 && !counterpartiesConfigured[ctpIdx])
                    return false;
            }

            return true;
        }

        protected void PersistedNopProviderOnCreditLineSettlementSettingsChanged(IEnumerable<NopSettingsPointPerCreditLine> unsettletNopSettingsPoints)
        {
            //prevent multiple enumerations
            unsettletNopSettingsPoints = unsettletNopSettingsPoints.ToList();

            //First go through mappings and cross check
            if (!AreCreditLineMappingsSame(unsettletNopSettingsPoints))
            {
                string oldMapping = string.Empty;

                foreach (int lineId in _creditLinesMap.GroupBy(lineId => lineId).Select(grp => grp.Key))
                {
                    string lineName = lineId < 0 ? "<NO CREDIT LINE>" : this._creditLineSettings[lineId].CreditLineName;
                    int lineIdLocal = lineId;
                    string counterparties = string.Join(", ",
                        this._creditLinesMap.Select((lnId, cptId) => new { lnId, cptId })
                            .Where(pair => pair.lnId == lineIdLocal)
                            .Select(pair => ((Counterparty)pair.cptId).ToString()));
                    oldMapping += string.Format("[{0}]: ({1});", lineName, counterparties);
                }

                string newMapping = string.Empty;
                foreach (NopSettingsPointPerCreditLine unsettletNopSettingsPoint in unsettletNopSettingsPoints)
                {
                    newMapping += string.Format("[{0}]: ({1});", unsettletNopSettingsPoint.CreditLineName,
                        string.Join(", ", unsettletNopSettingsPoint.Counterparties));
                }

                this._logger.Log(LogLevel.Fatal, "Attempt to alter counterparties<->credit lines mappings - only to original mappings will be used. {0}Old mappings: {1}{0}New mappinds: {2}",
                    Environment.NewLine, oldMapping, newMapping);
            }

            //Update stats
            foreach (NopSettingsPointPerCreditLine unsettletNopSettingsPoint in unsettletNopSettingsPoints)
            {
                foreach (UnsettledNopCreditLineSettingsPoint unsettletNopCreditLineSettingsPoint in _creditLineSettings)
                {
                    unsettletNopCreditLineSettingsPoint.UpdateIfMatch(unsettletNopSettingsPoint);
                }
            }

            //Check if any limits breached (by stats decreasing)
            this.CheckLimits();
            this.RefreshTradingAllowedMap();
        }

        private void RefreshTradingAllowedMap()
        {
            for (int creditLineId = 0; creditLineId < _creditLineSettings.Length; creditLineId++)
            {
                _creditLineAllowedToTradeMap[creditLineId] = this.CanCreditLineTrade(creditLineId);
            }
        }

        private bool CanCreditLineTrade(int creditLineId)
        {
            return this._creditLineSettings[creditLineId].LimitsTotal.MaximumUnsettledNop != AtomicSize.ZERO &&
                   this._creditLineSettings[creditLineId].LimitsPerValueDate.MaximumUnsettledNop != AtomicSize.ZERO
                   ||
                   this._unsettledNopStats.NopStatsPerCreditLineOverall.HasAnyRealizedDeal[creditLineId];
        }

        protected void Initialize(IEnumerable<IntegratorDealDone> unsettletNopInfoPoints,
            IEnumerable<NopSettingsPointPerCreditLine> unsettletNopSettingsPoints)
        {
            //prevent multiple enumerations
            unsettletNopSettingsPoints = unsettletNopSettingsPoints.ToList();
            unsettletNopInfoPoints = unsettletNopInfoPoints.ToList();
            int creditLinesNum = unsettletNopSettingsPoints.Count();
            int distinctSettlemtDates = unsettletNopInfoPoints.GroupBy(deal => deal.SettlementDateLocal)
                .Select(grp => grp.First().SettlementDateLocal).Count();
            distinctSettlemtDates = Math.Min(2, distinctSettlemtDates);
            if (distinctSettlemtDates > _MAX_REASONABLE_NUM_OF_UNSETTLED_DAYS)
            {
                this._logger.Log(LogLevel.Fatal, "UnsettledNopCheckingModule experienced unexpected high number of distinct unsettled dates: {0}. Expected at max: {1}. Will fall back to expectable max. Dates beyond this limit will not get proper nop limit checking",
                    distinctSettlemtDates, _MAX_REASONABLE_NUM_OF_UNSETTLED_DAYS);
                distinctSettlemtDates = _MAX_REASONABLE_NUM_OF_UNSETTLED_DAYS;
            }

            this._creditLinesMap = Enumerable.Repeat(-1, Counterparty.ValuesCount).ToArray();
            this._creditLineAllowedToTradeMap = Enumerable.Repeat(true, creditLinesNum).ToArray();
            this._unsettledNopStats = new UnsettledNopStats(creditLinesNum, distinctSettlemtDates);
            this._backgroundStatsForConcurrentUpdate = new UnsettledNopStats(creditLinesNum, distinctSettlemtDates);
            this._creditLineSettings = new UnsettledNopCreditLineSettingsPoint[creditLinesNum];

            //CAREFULL!! - the credit line id in memory != credit line id in backend (to make them more compact)
            int creditLineIdInMemory = 0;
            foreach (NopSettingsPointPerCreditLine unsettletNopSettingsPoint in unsettletNopSettingsPoints)
            {
                this._creditLineSettings[creditLineIdInMemory] =
                    new UnsettledNopCreditLineSettingsPoint(unsettletNopSettingsPoint);
                foreach (Counterparty counterparty in unsettletNopSettingsPoint.Counterparties)
                {
                    if (this._creditLinesMap[(int)counterparty] != -1)
                    {
                        string error =
                            string.Format(
                                "Invalid credit line mappings configuration - counterparty [{0}] mapped to [{1}] and [{2}]",
                                counterparty,
                                this._creditLineSettings[this._creditLinesMap[(int)counterparty]].CreditLineName,
                                unsettletNopSettingsPoint.CreditLineName);
                        this._logger.Log(LogLevel.Fatal, error);
                        throw new Exception(error);
                    }

                    this._creditLinesMap[(int)counterparty] = creditLineIdInMemory;
                }
                creditLineIdInMemory++;
            }

            this._unsettledDeals.AddRange(unsettletNopInfoPoints);
            foreach (IntegratorDealDone unsettletNopInfoPoint in unsettletNopInfoPoints)
            {
                this.AddDataPointToStats(this._unsettledNopStats, unsettletNopInfoPoint, LimitsCheckingMode.NoCheck);
            }

            this.CheckLimits();

            //keep background completely up to date
            _backgroundStatsForConcurrentUpdate.CopyFrom(_unsettledNopStats);

            this.RefreshTradingAllowedMap();
        }


        public static void CalculateNominalNopDiff(AtomicSize oldPosition, AtomicSize newPosition,
            out AtomicSize longNopDiffNominalPolarized, out AtomicSize shortNopDiffNominalPolarized)
        {
            if (oldPosition >= AtomicSize.ZERO)
            {
                if (newPosition <= AtomicSize.ZERO)
                {
                    longNopDiffNominalPolarized = -oldPosition;
                    shortNopDiffNominalPolarized = -newPosition;
                }
                else
                {
                    longNopDiffNominalPolarized = (newPosition - oldPosition);
                    shortNopDiffNominalPolarized = AtomicSize.ZERO;
                }
            }
            else
            {
                if (newPosition >= AtomicSize.ZERO)
                {
                    longNopDiffNominalPolarized = newPosition;
                    shortNopDiffNominalPolarized = oldPosition;
                }
                else
                {
                    shortNopDiffNominalPolarized = (oldPosition - newPosition);
                    longNopDiffNominalPolarized = AtomicSize.ZERO;
                }
            }
        }

        //Identical code for overall and per settlemt date info
        public static AtomicSize AddDataPointAndGetNop(UnsettledNopStatsPerCreditLine unsettledStatsPerCreditLine,
            int creditLineIdx, IntegratorDealDone integratorDealDone, decimal[] usdConversionMultipliers)
        {
            AtomicSize newBasePos = unsettledStatsPerCreditLine.NominalPositions[creditLineIdx][
                (int)integratorDealDone.Symbol.BaseCurrency].InterlockedAdd(integratorDealDone.SizeBasePolarized);
            AtomicSize newTermPos = unsettledStatsPerCreditLine.NominalPositions[creditLineIdx][
                (int)integratorDealDone.Symbol.TermCurrency].InterlockedAdd(integratorDealDone.SizeTermPolarized);
            unsettledStatsPerCreditLine.HasAnyRealizedDeal[creditLineIdx] = true;

            AtomicSize wasBasePos = newBasePos - integratorDealDone.SizeBasePolarized;
            AtomicSize wasTermPos = newTermPos - integratorDealDone.SizeTermPolarized;

            AtomicSize longDiffBase, shortDiffBase, longDiffTerm, shortDiffTerm;
            CalculateNominalNopDiff(wasBasePos, newBasePos, out longDiffBase, out shortDiffBase);
            CalculateNominalNopDiff(wasTermPos, newTermPos, out longDiffTerm, out shortDiffTerm);

            AtomicSize overalLongDiffUsd = longDiffBase *
                                           usdConversionMultipliers[(int)integratorDealDone.Symbol.BaseCurrency] +
                                           longDiffTerm *
                                           usdConversionMultipliers[(int)integratorDealDone.Symbol.TermCurrency];

            AtomicSize overalShortDiffUsd = shortDiffBase *
                                           usdConversionMultipliers[(int)integratorDealDone.Symbol.BaseCurrency] +
                                           shortDiffTerm *
                                           usdConversionMultipliers[(int)integratorDealDone.Symbol.TermCurrency];


            AtomicSize newLongNop = unsettledStatsPerCreditLine.LongUsdNops[creditLineIdx].InterlockedAdd(overalLongDiffUsd);
            AtomicSize newShortNop = unsettledStatsPerCreditLine.ShortUsdNops[creditLineIdx].InterlockedAdd(overalShortDiffUsd);

            return AtomicSize.Max(newLongNop, newShortNop);
        }

        //Identical code for overall and per settlemt date info
        public static AtomicSize GetNopIfOrderAdded(UnsettledNopStatsPerCreditLine unsettledStatsPerCreditLine,
            int creditLineIdx, IIntegratorOrderInfo orderInfo, decimal[] usdConversionMultipliers, PriceObjectInternal priceObject)
        {
            AtomicSize newBasePos = unsettledStatsPerCreditLine.NominalPositions[creditLineIdx][
                (int)orderInfo.Symbol.BaseCurrency] + orderInfo.GetSizeBasePolarized();
            AtomicSize newTermPos = unsettledStatsPerCreditLine.NominalPositions[creditLineIdx][
                (int)orderInfo.Symbol.TermCurrency] + orderInfo.GetSizeTermPolarized(priceObject == null ? (decimal?)null : priceObject.Price);

            AtomicSize wasBasePos = newBasePos - orderInfo.GetSizeBasePolarized();
            AtomicSize wasTermPos = newTermPos - orderInfo.GetSizeTermPolarized(priceObject == null ? (decimal?)null : priceObject.Price);

            AtomicSize longDiffBase, shortDiffBase, longDiffTerm, shortDiffTerm;
            CalculateNominalNopDiff(wasBasePos, newBasePos, out longDiffBase, out shortDiffBase);
            CalculateNominalNopDiff(wasTermPos, newTermPos, out longDiffTerm, out shortDiffTerm);

            AtomicSize overalLongDiffUsd = longDiffBase *
                                           usdConversionMultipliers[(int)orderInfo.Symbol.BaseCurrency] +
                                           longDiffTerm *
                                           usdConversionMultipliers[(int)orderInfo.Symbol.TermCurrency];

            AtomicSize overalShortDiffUsd = shortDiffBase *
                                           usdConversionMultipliers[(int)orderInfo.Symbol.BaseCurrency] +
                                           shortDiffTerm *
                                           usdConversionMultipliers[(int)orderInfo.Symbol.TermCurrency];


            AtomicSize newLongNop = unsettledStatsPerCreditLine.LongUsdNops[creditLineIdx] + overalLongDiffUsd;
            AtomicSize newShortNop = unsettledStatsPerCreditLine.ShortUsdNops[creditLineIdx] + overalShortDiffUsd;

            return AtomicSize.Max(newLongNop, newShortNop);
        }

        private DateTime _missingSettlementShouldFatalAfter;
        private bool TryGetSettlementDate(Symbol symbol, out DateTime settlementDate)
        {
            DateTime? settlementDateNullable = this._settlementDatesKeeper.GetSettlementDate(symbol);

            if (settlementDateNullable.HasValue)
            {
                settlementDate = settlementDateNullable.Value;
            }
            else
            {
                settlementDate = DateTime.MinValue;
                if (this._missingSettlementShouldFatalAfter <= HighResolutionDateTime.UtcNow)
                {
                    this._missingSettlementShouldFatalAfter = HighResolutionDateTime.UtcNow.AddMinutes(10);
                    this._logger.Log(LogLevel.Fatal, "Unsettled Nop Checker cannot obtain settlement date for {0} - per settlement date NOP checking will be less strict. Reported only once per 10 minutes",
                        symbol);
                }
            }

            return settlementDateNullable.HasValue;
        }

        public static bool QuickCheckIsRequestLikelyToPassForGivenStats(
            int creditLineIdx,
            UnsettledNopCreditLineLimitsSettingsPoint creditLineLimitsSettings,
            UnsettledNopStatsPerCreditLine unsettledStatsPerCreditLine,
            IIntegratorOrderInfo orderInfo)
        {
            switch (unsettledStatsPerCreditLine.GetCreditZone(creditLineIdx))
            {
                case CreditZone.NoCheckingZone:
                    return true;
                //Following zones are getting close to limit - so fast allow only if clearly decreasing
                case CreditZone.SimpleCheckingZone:
                case CreditZone.LockedZone:
                    return
                        //this looks like we will be decreasing the NOP
                        orderInfo.IntegratorDealDirection == DealDirection.Buy
                            ? unsettledStatsPerCreditLine.NominalPositions[creditLineIdx][
                                (int)orderInfo.Symbol.BaseCurrency] < AtomicSize.ZERO &&
                              unsettledStatsPerCreditLine.NominalPositions[creditLineIdx][
                                  (int)orderInfo.Symbol.TermCurrency] > AtomicSize.ZERO
                            : unsettledStatsPerCreditLine.NominalPositions[creditLineIdx][
                                (int)orderInfo.Symbol.BaseCurrency] > AtomicSize.ZERO &&
                              unsettledStatsPerCreditLine.NominalPositions[creditLineIdx][
                                  (int)orderInfo.Symbol.TermCurrency] < AtomicSize.ZERO;
                case CreditZone.OverTheMaxZone:
                default:
                    return false;
            }
        }

        private static readonly AtomicSize _thousand = AtomicSize.FromDecimal(1000m);
        private readonly CheckingStats _checkingStats = new CheckingStats();
        private void FlushStatsIfChange()
        {
            if (_checkingStats.Changed)
            {
                this._logger.Log(LogLevel.Trace, _checkingStats.ToString());
                _checkingStats.Changed = false;
            }
        }

        public class CheckingStats
        {
            public bool Changed;
            public ulong ChecksTotal;
            public ulong FastAllowedTotal;
            public ulong StdAllowedTotal;
            public ulong StdDisallowedTotal;
            public ulong LockedDisallowedTotal;
            public AtomicSize LockedDisallowedNopIncreasingSumUsd;
            public ulong LockedDisallowedIncreasingNopByMax1000Usd;
            public ulong OverTheMaxDisallowedTotal;

            public override string ToString()
            {
                return
                    string.Format(
                        "UnsettledNopStats: ChecksTotal: {0}, FastAllowedTotal: {1}, StdAllowedTotal: {2}, StdDisallowedTotal: {3}, LockedDisallowedTotal: {4}, LockedDisallowedNopIncreasingAvgUsd: {5}, LockedDisallowedIncreasingNopByMax1000Usd: {6}, OverTheMaxDisallowedTotal: {7}",
                        ChecksTotal, FastAllowedTotal, StdAllowedTotal, StdDisallowedTotal, LockedDisallowedTotal,
                        LockedDisallowedTotal != 0 ? LockedDisallowedNopIncreasingSumUsd.ToDecimal() / LockedDisallowedTotal : 0,
                        LockedDisallowedIncreasingNopByMax1000Usd, OverTheMaxDisallowedTotal);
            }
        }

        public static bool CheckIsOutgoingOrderAllowedForGivenStats(
            int creditLineIdx,
            decimal[] usdConversionMultipliers,
            UnsettledNopCreditLineLimitsSettingsPoint creditLineLimitsSettings,
            UnsettledNopStatsPerCreditLine unsettledStatsPerCreditLine,
            IIntegratorOrderInfo orderInfo, bool allowBypassLockingMode,
            PriceObjectInternal priceObject,
            CheckingStats checkingStats)
        {
            CreditZone creditZone = unsettledStatsPerCreditLine.GetCreditZone(creditLineIdx);
            if (allowBypassLockingMode && creditZone == CreditZone.LockedZone)
                creditZone = CreditZone.SimpleCheckingZone;

            checkingStats.Changed = true;
            checkingStats.ChecksTotal++;
            switch (creditZone)
            {
                case CreditZone.NoCheckingZone:
                    checkingStats.FastAllowedTotal++;
                    return true;
                case CreditZone.SimpleCheckingZone:
                    {
                        //green if nop is under max
                        AtomicSize nopOverallAfterOrder = GetNopIfOrderAdded(unsettledStatsPerCreditLine,
                            creditLineIdx, orderInfo, usdConversionMultipliers, priceObject);

                        bool allowed = nopOverallAfterOrder <= creditLineLimitsSettings.MaximumUnsettledNop;

                        if (allowed)
                            checkingStats.StdAllowedTotal++;
                        else
                            checkingStats.StdDisallowedTotal++;

                        return allowed;
                    }
                case CreditZone.LockedZone:
                    {
                        //green if entire nop is decreasing

                        AtomicSize nopOverallAfterOrder = GetNopIfOrderAdded(unsettledStatsPerCreditLine,
                            creditLineIdx, orderInfo, usdConversionMultipliers, priceObject);

                        AtomicSize nopOverallCurrent =
                            AtomicSize.Max(unsettledStatsPerCreditLine.LongUsdNops[creditLineIdx],
                                unsettledStatsPerCreditLine.ShortUsdNops[creditLineIdx]);

                        AtomicSize nopIncrease = nopOverallAfterOrder - nopOverallCurrent;

                        bool allowed = nopIncrease <= AtomicSize.ZERO;

                        if (allowed)
                            checkingStats.StdAllowedTotal++;
                        else
                        {
                            checkingStats.LockedDisallowedTotal++;
                            checkingStats.LockedDisallowedNopIncreasingSumUsd += nopIncrease;
                            if (nopIncrease <= _thousand)
                                checkingStats.LockedDisallowedIncreasingNopByMax1000Usd++;
                        }

                        return allowed;
                    }
                case CreditZone.OverTheMaxZone:
                    {
                        //shortcut for cpts that are blocked by setting Max NOP to 0
                        if (!unsettledStatsPerCreditLine.HasAnyRealizedDeal[creditLineIdx])
                            return false;

                        //green if decreasing each currency NOP

                        AtomicSize newBasePos = unsettledStatsPerCreditLine.NominalPositions[creditLineIdx][
                            (int)orderInfo.Symbol.BaseCurrency] + orderInfo.GetSizeBasePolarized();
                        AtomicSize newTermPos = unsettledStatsPerCreditLine.NominalPositions[creditLineIdx][
                            (int)orderInfo.Symbol.TermCurrency] + orderInfo.GetSizeTermPolarized(priceObject == null ? (decimal?)null : priceObject.Price);

                        AtomicSize wasBasePos = newBasePos - orderInfo.GetSizeBasePolarized();
                        AtomicSize wasTermPos = newTermPos - orderInfo.GetSizeTermPolarized(priceObject == null ? (decimal?)null : priceObject.Price);


                        bool allowed = newBasePos.IsSameSignAndCloserToZeroThan(wasBasePos) &&
                                       newTermPos.IsSameSignAndCloserToZeroThan(wasTermPos);

                        if (allowed)
                            checkingStats.StdAllowedTotal++;
                        else
                            checkingStats.OverTheMaxDisallowedTotal++;

                        return allowed;

                        //TODO: should somehow block the positions (and also register for order done, id registrable)
                    }
                default:
                    LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "UnsettledNopCheckingModule: Unknown zone [{0}] for credit line {1} - disallowing order info. {2}",
                        creditZone, creditLineIdx, orderInfo);
                    return false;
            }
        }

        public bool IsTradeRequestLikelyToPass(IIntegratorOrderInfo orderInfo, PriceObjectInternal priceObject)
        {
            Counterparty cpt = priceObject == null ? orderInfo.Counterparty : priceObject.Counterparty;
            int creditLineIdx = _creditLinesMap[(int)cpt];
            if (creditLineIdx < 0)
            {
                return true;
            }

            //shortcut for settings set to 0 and no active trades
            if (!this._creditLineAllowedToTradeMap[creditLineIdx])
                return false;

            UnsettledNopStats unsettledNopStatsLocal = this._unsettledNopStats;

            bool isAllowedForOverallNop = QuickCheckIsRequestLikelyToPassForGivenStats(
                creditLineIdx, this._creditLineSettings[creditLineIdx].LimitsTotal,
                unsettledNopStatsLocal.NopStatsPerCreditLineOverall,
                orderInfo);

            //shortcut
            if (!isAllowedForOverallNop
                &&
                //perform full check only if quick check failed
                !CheckIsOutgoingOrderAllowedForGivenStats(
                creditLineIdx, _usdConversionMultipliers, this._creditLineSettings[creditLineIdx].LimitsTotal,
                unsettledNopStatsLocal.NopStatsPerCreditLineOverall,
                orderInfo, false, priceObject, _checkingStats))
                return false;

            bool isAllowedForSettlementDateNop;
            DateTime settlementDate;
            UnsettledNopStatsPerCreditLine statsPoint;

            if (this.TryGetSettlementDate(orderInfo.Symbol, out settlementDate) && unsettledNopStatsLocal.TryGetSettlementDateStats(settlementDate, out statsPoint))
            {
                isAllowedForSettlementDateNop =
                    QuickCheckIsRequestLikelyToPassForGivenStats(
                        creditLineIdx, this._creditLineSettings[creditLineIdx].LimitsPerValueDate,
                        statsPoint, orderInfo)
                    ||
                    //perform full check only if quick check failed
                    CheckIsOutgoingOrderAllowedForGivenStats(
                        creditLineIdx, _usdConversionMultipliers,
                        this._creditLineSettings[creditLineIdx].LimitsPerValueDate,
                        statsPoint, orderInfo, false, priceObject, _checkingStats);
            }
            else
            {
                isAllowedForSettlementDateNop = true;
            }

            return isAllowedForSettlementDateNop;
        }

        //public bool IsTradeRequestLikelyToPass(IIntegratorOrderInfo orderInfo)
        //{
        //    return IsTradeRequestLikelyToPass(orderInfo, null);
        //}

        public bool CheckIsOutgoingIncommingOrderInfoAllowed(IIntegratorOrderInfo orderInfo)
        {
            try
            {
                return this.CheckIsOutgoingOrderAllowed(this._unsettledNopStats, orderInfo,
                orderInfo.IntegratorOrderInfoType == IntegratorOrderInfoType.ExternalClientDealRequest);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e,
                    "Order market as disallowed as there was exception during unsettled NOP checking (risk-remove flag can flip this decision)",
                    orderInfo);
                return orderInfo.IsRiskRemovingOrder;
            }
        }

        private bool CheckIsOutgoingOrderAllowed(UnsettledNopStats unsettledNopStatsLocal, IIntegratorOrderInfo orderInfo, bool allowBypassLockingMode)
        {
            //case of internal bankpool order
            if (orderInfo.Counterparty == Counterparty.NULL)
                return true;

            int creditLineIdx = _creditLinesMap[(int)orderInfo.Counterparty];
            if (creditLineIdx < 0)
            {
                this._logger.Log(LogLevel.Fatal, "Attempt to check unsettled NOP for counterparty ({0}) that has no credit line mapping set. No unsettled NOP checking will be performed",
                    orderInfo.Counterparty);
                return true;
            }

            bool isAllowedForOverallNop = CheckIsOutgoingOrderAllowedForGivenStats(
                creditLineIdx, _usdConversionMultipliers, this._creditLineSettings[creditLineIdx].LimitsTotal,
                unsettledNopStatsLocal.NopStatsPerCreditLineOverall,
                orderInfo, allowBypassLockingMode, null, _checkingStats);

            //shortcut
            if (!isAllowedForOverallNop)
                return false;

            bool isAllowedForSettlementDateNop;

            DateTime settlementDate;
            UnsettledNopStatsPerCreditLine statsPoint;

            if (this.TryGetSettlementDate(orderInfo.Symbol, out settlementDate) && unsettledNopStatsLocal.TryGetSettlementDateStats(settlementDate, out statsPoint))
            {
                isAllowedForSettlementDateNop = CheckIsOutgoingOrderAllowedForGivenStats(
                    creditLineIdx, _usdConversionMultipliers, this._creditLineSettings[creditLineIdx].LimitsPerValueDate,
                    statsPoint, orderInfo, allowBypassLockingMode, null, _checkingStats);
            }
            else
            {
                //orderInfo.Requested price should be well initialized here
                AtomicSize orderNop = AtomicSize.Max(
                    AtomicSize.FromDecimal(orderInfo.SizeBaseAbsInitial *
                                           _usdConversionMultipliers[(int)orderInfo.Symbol.BaseCurrency]),
                    AtomicSize.FromDecimal(orderInfo.SizeBaseAbsInitial * orderInfo.RequestedPrice *
                                           _usdConversionMultipliers[(int)orderInfo.Symbol.TermCurrency])
                    );

                isAllowedForSettlementDateNop =
                    orderNop <= this._creditLineSettings[creditLineIdx].LimitsPerValueDate.MaximumUnsettledNop;
            }

            return isAllowedForOverallNop && isAllowedForSettlementDateNop;
        }

        
        private void AddDataPointToStats(UnsettledNopStats unsettledNopStatsLocal,
            IntegratorDealDone unsettletNopInfoPoint, LimitsCheckingMode limitsCheckingMode)
        {
            UnsettledNopStatsPerCreditLine statsPoint =
                unsettledNopStatsLocal.GetSettlementDateStats(unsettletNopInfoPoint.SettlementDateLocal, this._logger);

            int creditLineIdx = _creditLinesMap[(int)unsettletNopInfoPoint.Counterparty];
            if (creditLineIdx < 0)
            {
                this._logger.Log(LogLevel.Fatal, "Attempt to add unsettled NOP for counterparty ({0}) that has no credit line mapping set. No unsettled NOP checking will be performed",
                    unsettletNopInfoPoint.Counterparty);
                return;
            }
            _creditLineAllowedToTradeMap[creditLineIdx] = true;

            AtomicSize nopOverall = AddDataPointAndGetNop(unsettledNopStatsLocal.NopStatsPerCreditLineOverall, creditLineIdx,
                unsettletNopInfoPoint, this._usdConversionMultipliers);

            AtomicSize nopPerValueDate = AtomicSize.ZERO;

            if (statsPoint != null)
                nopPerValueDate = AddDataPointAndGetNop(statsPoint, creditLineIdx, unsettletNopInfoPoint, this._usdConversionMultipliers);

            if (limitsCheckingMode != LimitsCheckingMode.NoCheck)
            {
                //prevent double logging
                if (limitsCheckingMode == LimitsCheckingMode.CheckWithEvents)
                    this._logger.Log(LogLevel.Trace, "Adding new data point to unsettled stats. OverallNop: {0}, PerValueDate: {1}. {2}",
                    nopOverall, nopPerValueDate, unsettletNopInfoPoint);

                this.CheckLimitRaw(this._creditLineSettings[creditLineIdx].LimitsTotal,
                    unsettledNopStatsLocal.NopStatsPerCreditLineOverall, nopOverall, creditLineIdx,
                    limitsCheckingMode == LimitsCheckingMode.CheckWithEvents, null);
                if (statsPoint != null)
                    this.CheckLimitRaw(this._creditLineSettings[creditLineIdx].LimitsPerValueDate, statsPoint,
                        nopPerValueDate, creditLineIdx, limitsCheckingMode == LimitsCheckingMode.CheckWithEvents,
                        unsettletNopInfoPoint.SettlementDateLocal);
            }
        }


        public void AddNewDealToStats(IntegratorDealDone integratorDealDone)
        {
            lock (_backgroundRecalculationLocker)
            {
                //no need to lock - this can be called only inside lock from caller
                this._unsettledDeals.Add(integratorDealDone);

                //update stats in background (to avoid problem of non-atomic update and avoid need for locking order checks to prevent inconsistent state)
                this.AddDataPointToStats(this._backgroundStatsForConcurrentUpdate, integratorDealDone, LimitsCheckingMode.CheckWithEvents);

                //Exchange to foreground only now - so that foreground stats are always consistent and not relaxed
                _backgroundStatsForConcurrentUpdate = Interlocked.Exchange(ref _unsettledNopStats, _backgroundStatsForConcurrentUpdate);

                //foreground and background are swapped - but keep background up to date (for next update)
                this.AddDataPointToStats(this._backgroundStatsForConcurrentUpdate, integratorDealDone, LimitsCheckingMode.CheckWithoutEvents);
            }
        }

        public void RemoveDealFromStats(IntegratorDealDone integratorDealDone)
        {
            lock (_backgroundRecalculationLocker)
            {
                //no need to lock - this can be called only inside lock from caller
                bool removed = this._unsettledDeals.Remove(integratorDealDone);

                if (!removed)
                {
                    this._logger.Log(LogLevel.Fatal,
                        "UnsettledNopChecking Module: Cannot remove [{0}] after late rejection of deal by counterparty - stats may be off as a results",
                        integratorDealDone);
                    return;
                }

                integratorDealDone.Invert();

                //update stats in background (to avoid problem of non-atomic update and avoid need for locking order checks to prevent inconsistent state)
                this.AddDataPointToStats(this._backgroundStatsForConcurrentUpdate, integratorDealDone, LimitsCheckingMode.CheckWithEvents);

                //Exchange to foreground only now - so that foreground stats are always consistent and not relaxed
                _backgroundStatsForConcurrentUpdate = Interlocked.Exchange(ref _unsettledNopStats, _backgroundStatsForConcurrentUpdate);

                //foreground and background are swapped - but keep background up to date (for next update)
                this.AddDataPointToStats(this._backgroundStatsForConcurrentUpdate, integratorDealDone, LimitsCheckingMode.CheckWithoutEvents);
            }
        }

        private void CheckLimitRaw(UnsettledNopCreditLineLimitsSettingsPoint creditLineLimitsSettings,
            UnsettledNopStatsPerCreditLine unsettledStatsPerCreditLine, AtomicSize nop, int creditLineIdx, bool invokeEvents, DateTime? applicableSettlementDate)
        {
            CreditZone creditZone =
                nop >= creditLineLimitsSettings.MaximumUnsettledNopToLockDestination
                    ? (nop >= creditLineLimitsSettings.MaximumUnsettledNop
                        ? CreditZone.OverTheMaxZone
                        : CreditZone.LockedZone)
                    : (this._isMaxOrderSizeCheckingAllowed &&
                       nop <= creditLineLimitsSettings.MaximumUnsettledNop - this._maximumAllowedOrderSizeInUsd
                        ? CreditZone.NoCheckingZone
                        : CreditZone.SimpleCheckingZone);

            if (unsettledStatsPerCreditLine.SetCreditZoneAndGetIsChange(creditLineIdx, creditZone) && invokeEvents)
            {
                this._logger.Log(LogLevel.Trace,
                    "CreditLine [{0}] flipped to [{1}] zone (NOP: {2}; LockMax: {3}; Max: {4}). Applicable valueDate: {5}",
                    this._creditLineSettings[creditLineIdx].CreditLineName, creditZone, nop,
                    creditLineLimitsSettings.MaximumUnsettledNopToLockDestination,
                    creditLineLimitsSettings.MaximumUnsettledNop, applicableSettlementDate);

                _persistedNopProvider.FlipCreditZone(this._creditLineSettings[creditLineIdx].DatabaseId, creditZone,
                    applicableSettlementDate);
            }
        }

        //Same code for overall and per value date
        private void CheckLimit(UnsettledNopCreditLineLimitsSettingsPoint creditLineLimitsSettings,
            UnsettledNopStatsPerCreditLine unsettledNopStatsPerCreditLine, int creditLineId, DateTime? applicableSettlementDate)
        {
            if (unsettledNopStatsPerCreditLine == null)
                return;

            this.CheckLimitRaw(
                creditLineLimitsSettings,
                unsettledNopStatsPerCreditLine,
                AtomicSize.Max(
                    unsettledNopStatsPerCreditLine.ShortUsdNops[creditLineId],
                    unsettledNopStatsPerCreditLine.LongUsdNops[creditLineId]),
                    creditLineId,
                    true, applicableSettlementDate
                );
        }

        private void CheckLimits()
        {
            for (int creditLineId = 0; creditLineId < _creditLineSettings.Length; creditLineId++)
            {
                UnsettledNopCreditLineSettingsPoint currentCreditLineSettings = this._creditLineSettings[creditLineId];

                this.CheckLimit(currentCreditLineSettings.LimitsTotal, this._unsettledNopStats.NopStatsPerCreditLineOverall, creditLineId, null);

                int statsIdx = 0;
                foreach (UnsettledNopStatsPerCreditLine unsettledNopStatsPerCreditLine in this._unsettledNopStats.NopStatsPerCreditLinePerSettlementDate)
                {
                    this.CheckLimit(currentCreditLineSettings.LimitsPerValueDate, unsettledNopStatsPerCreditLine,
                        creditLineId, this._unsettledNopStats.GetSettlementDateLocalForStatsIdx(statsIdx));
                    statsIdx++;
                }
            }
        }

        private void RecalculateStatsOnUsdRateChange()
        {
            //make sure only one thread at time executes - so that we can safely use one background structure
            lock (_backgroundRecalculationLocker)
            {
                //background stats are up to date with foreground
                // recalculate the usd conversions for them now
                _backgroundStatsForConcurrentUpdate.RecalculateUsdAggregations(this._usdConversionMultipliers);

                //Exchange to foreground only now - so that foreground stats are always consistent and not relaxed
                _backgroundStatsForConcurrentUpdate = Interlocked.Exchange(ref _unsettledNopStats, _backgroundStatsForConcurrentUpdate);

                //no other deals could be added in parallel - as we are in constrained region

                //calculate limits in lock and before copying over - so that the zones are correctly copied to background
                this.CheckLimits();

                //keep background completely up to date
                _backgroundStatsForConcurrentUpdate.CopyUsdRecalculationsFrom(_unsettledNopStats);
            }
        }

        private void RecalculateStatsOnRollover()
        {
            //make sure only one thread at time executes - so that we can safely use one background structure
            lock (_backgroundRecalculationLocker)
            {
                //background stats are up to date with foreground - but clear them out to recalc from scratch
                _backgroundStatsForConcurrentUpdate.ClearUnsettledNopStats();

                //No one else can access the list of deals inside the lock

                //filter out the old deals and update settlement base date
                _backgroundStatsForConcurrentUpdate.UpdateSettlemtDateBaseDate();
                DateTime utcNow = HighResolutionDateTime.UtcNow;
                this._unsettledDeals.RemoveAll(deal => deal.SettlementTimeCounterpartyUtc < utcNow);

                //perform recalculation for existing deals
                foreach (IntegratorDealDone unsettletNopInfoPoint in this._unsettledDeals)
                {
                    this.AddDataPointToStats(_backgroundStatsForConcurrentUpdate, unsettletNopInfoPoint, LimitsCheckingMode.NoCheck);
                }

                //Exchange to foreground only now - so that foreground stats are always consistent and not relaxed
                _backgroundStatsForConcurrentUpdate = Interlocked.Exchange(ref _unsettledNopStats,
                    _backgroundStatsForConcurrentUpdate);

                //no other deals could be added in parallel - as we are in constrained region

                //calculate limits in lock and before copying over - so that the zones are correctly copied to background
                this.CheckLimits();

                //keep background completely up to date
                _backgroundStatsForConcurrentUpdate.CopyFrom(_unsettledNopStats);
            }

            this.RefreshTradingAllowedMap();
        }
    }
}
