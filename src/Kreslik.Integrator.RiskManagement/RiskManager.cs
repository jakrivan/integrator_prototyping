﻿using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.RiskManagement.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Kreslik.Integrator.RiskManagement
{
    public class RiskManagerSettingsBag
    {
        public IExternalOrdersTransmissionControl PersistentOrdersTransmissionControl { get; set; }

        public List<string> DisallowTradingOnInstances { get; set; }
        public decimal[] OnlineDeviationCheckRate { get; set; }
        public TimeSpan OnlineDeviationCheckMaxPriceAge { get; set; }
        public decimal[] MaxOrderSizesBaseAbs { get; set; }
        public decimal MaxOrderSizesBaseAbsInUsd { get; set; }
        public bool PriceDeviationCheckAllowed { get; set; }
        public bool SymbolTrustworthyCheckAllowed { get; set; }
        public TimeSpan MinimumSymbolUntrustworthyPeriod { get; set; }
        public bool MaximumOrderSizeRiskCheckAllowed { get; set; }
        public bool NopRiskCheckAllowed { get; set; }
        public bool UnsettledNopRiskCheckAllowed { get; set; }
        public bool CumulativeSizesCheckAllowed { get; set; }
        public bool ExternalOrdersSubmissionRateCheckAllowed { get; set; }
        public TimeSpan ExternalOrdersSubmissionRateTimeIntervalToCheck { get; set; }
        public int ExternalOrdersSubmissionRateMaximumAllowedInstances { get; set; }
        public bool ExternalOrdersRejectionRateCheckAllowed { get; set; }
        public TimeSpan ExternalOrdersRejectionRateTimeIntervalToCheck { get; set; }
        public int ExternalOrdersRejectionRateMaximumAllowedInstances { get; set; }
        public bool FatalLogErrorsRateCheckAllowed { get; set; }
        public TimeSpan FatalLogErrorsRateTimeIntervalToCheck { get; set; }
        public int FatalLogErrorsRateMaximumAllowedInstances { get; set; }
    }

    public class RiskManager: IRiskManager
    {
        private IExternalOrdersTransmissionControl _persistentOrdersTransmissionControl;
        private ILogger _logger; 
        private EventsRateChecker _externalOrdersRateChecker;
        private EventsRateChecker _externalOrdersRejectsRateChecker;
        private EventsRateChecker _fatalLogErrorsRateChecker;

        private RiskManagerSettingsBag _riskManagerSettingsBag;

        private CumulativeSizesChecker _cumulativeSizesChecker;
        private UnsettledNopCheckingModule _unsettledNopCheckingModule;
        private NetOpenPositionModule _netOpenPositionModule;
        private IPersistentSettingsReader<RiskManagementSettings> _persistentSettingsReader;
        private IOnlinePriceDeviationCheckerInternal _onlinePriceDeviationChecker;
        private IPriceStreamMonitor _priceStreamMonitor;
        private bool _currentInstanceTradingDisallowed;
        private ISymbolsInfo _symbolsInfo;
        private SymbolTrustworthyWatcherInternal _symbolTrustworthyWatcher;
        private IPersistedNopProvider _persistedNopProvider;
        private ISettlementDatesKeeper _settlementDatesKeeper;

        

        public static int _instancesCount = 0;
        public RiskManager(IPriceStreamMonitor priceStreamMonitor, ISymbolsInfo symbolsInfo, ISettlementDatesKeeper settlementDatesKeeper)
        {
            if (Interlocked.Increment(ref _instancesCount) > 1)
            {
                throw new Exception("Only one instance of RiskManager is allowed per process");
            }

            ILogger riskManagerLogger = (LogFactory.Instance).GetLogger("RiskManager");
            IPersistentSettingsReader<RiskManagementSettings> settingsReader =
                new PersistentSettingsReader<RiskManagementSettings>(riskManagerLogger,
                                                                     @"Kreslik.Integrator.RiskManagement.dll",
                                                                     Resources.Kreslik_Integrator_RiskManagement_dll);

            RiskManagerSettingsBag riskManagerSettingsBag =
                new RiskManagerSettingsBag()
                    {
                        PersistentOrdersTransmissionControl = new ExternalOrdersTransmissionControl(riskManagerLogger)
                    };

            riskManagerSettingsBag = ReadSettingsFromConfig(riskManagerLogger, riskManagerSettingsBag,
                                                            settingsReader.ReadSettings(), priceStreamMonitor.UsdConversionMultipliers);

            this.Initialize(riskManagerLogger, riskManagerSettingsBag, priceStreamMonitor, new PersistedNopProvider(), symbolsInfo, settlementDatesKeeper);

            _persistentSettingsReader = settingsReader;
            _persistentSettingsReader.RegisterSettingsHandler(CheckSettings);
        }

        public IExternalOrdersTransmissionControlScheduling ExternalOrdersTransmissionControlScheduling
        {
            get { return this._persistentOrdersTransmissionControl as IExternalOrdersTransmissionControlScheduling; }
        }

        private void Initialize(ILogger logger, RiskManagerSettingsBag riskManagerSettingsBag,
                                IPriceStreamMonitor priceStreamMonitor, IPersistedNopProvider persistedNopProvider, ISymbolsInfo symbolsInfo, ISettlementDatesKeeper settlementDatesKeeper)
        {
            this._logger = logger;
            this._persistentOrdersTransmissionControl = riskManagerSettingsBag.PersistentOrdersTransmissionControl;
            this._priceStreamMonitor = priceStreamMonitor;
            this._riskManagerSettingsBag = riskManagerSettingsBag;
            this._symbolsInfo = symbolsInfo;
            this._persistedNopProvider = persistedNopProvider;
            this._settlementDatesKeeper = settlementDatesKeeper;

            this._onlinePriceDeviationChecker = new OnlinePriceDeviationChecker(logger);
            _symbolTrustworthyWatcher = new SymbolTrustworthyWatcherInternal(new TrustworthyWatcherSettingsProvider(),
                logger, priceStreamMonitor, _onlinePriceDeviationChecker);

            this._netOpenPositionModule = new NetOpenPositionModule(persistedNopProvider, priceStreamMonitor, logger);

            this._cumulativeSizesChecker = new CumulativeSizesChecker(logger);

            this.Reconfigure(riskManagerSettingsBag, priceStreamMonitor);

            priceStreamMonitor.NewUsdConversionMultipliersAvailable += usdMultipliers =>
                {
                    decimal[] maxOrderSizesTemp = Enumerable.Repeat(0m, Currency.ValuesCount).ToArray();

                    foreach (Currency usedCurrency in Symbol.UsedCurrencies)
                    {
                        int currencyIdx = (int)usedCurrency;
                        if (usdMultipliers[currencyIdx] > 0m)
                            maxOrderSizesTemp[currencyIdx] =
                                decimal.Ceiling(
                                    this._riskManagerSettingsBag.MaxOrderSizesBaseAbsInUsd
                                    /
                                    usdMultipliers[currencyIdx]);
                    }

                    this._riskManagerSettingsBag.MaxOrderSizesBaseAbs = maxOrderSizesTemp;
                };

            LogFactory.NewFatalLogEntry += LogFactoryOnNewFatalLogEntry;
        }

        private void LogFactoryOnNewFatalLogEntry(string s)
        {
            if (this._fatalLogErrorsRateChecker != null &&
                IntegratorStateInitializer.IntegratorProcessType == IntegratorProcessType.BusinessLogic &&
                !_currentInstanceTradingDisallowed &&
                !this._fatalLogErrorsRateChecker.AddNextEventAndCheckIsAllowed())
            {
                this.TurnOnGoFlatOnly(string.Format("Exceeded {0} fatal log entries per {1} time interval",
                    _fatalLogErrorsRateChecker.MaximumAllowedEventsPerInterval,
                    _fatalLogErrorsRateChecker.TimeIntervalToCheck));
                this._fatalLogErrorsRateChecker.Reset();
            }
        }

        //Direct usage of this constructor is reserved for unit testing purposes
        protected RiskManager(ILogger logger, RiskManagerSettingsBag riskManagerSettingsBag,
                              IPriceStreamMonitor priceStreamMonitor, IPersistedNopProvider persistedNopProvider, ISymbolsInfo symbolsInfo, ISettlementDatesKeeper settlementDatesKeeper)
        {
            this.Initialize(logger, riskManagerSettingsBag, priceStreamMonitor, persistedNopProvider, symbolsInfo, settlementDatesKeeper);
        }

        private void CheckSettings(RiskManagementSettings settings)
        {
            if (settings != null)
            {
                try
                {
                    var riskManagerSettingsBag = new RiskManagerSettingsBag()
                        {
                            PersistentOrdersTransmissionControl =
                                _riskManagerSettingsBag.PersistentOrdersTransmissionControl
                        };
                    this.Reconfigure(
                        ReadSettingsFromConfig(this._logger, riskManagerSettingsBag, settings,
                                               this._priceStreamMonitor.UsdConversionMultipliers),
                        this._priceStreamMonitor);
                    this._riskManagerSettingsBag = riskManagerSettingsBag;
                }
                catch (Exception e)
                {
                    _logger.LogException(LogLevel.Fatal, e, "Unexpected exception during reconfiguring RiskManagement. Ignoring updated config");
                }  
            }
        }

        private void Reconfigure(RiskManagerSettingsBag riskManagerSettingsBag, IPriceStreamMonitor priceStreamMonitor)
        {
            this._riskManagerSettingsBag = riskManagerSettingsBag;

            _currentInstanceTradingDisallowed = riskManagerSettingsBag.DisallowTradingOnInstances != null &&
                                                        riskManagerSettingsBag.DisallowTradingOnInstances.Any(
                                                            instance =>
                                                            instance != null && instance.Equals(
                                                                SettingsInitializator.Instance.InstanceName,
                                                                StringComparison.InvariantCultureIgnoreCase));
            this._persistentOrdersTransmissionControl.AllowPropagationOfStateChangesToBackend =
                !_currentInstanceTradingDisallowed;

            if (riskManagerSettingsBag.ExternalOrdersSubmissionRateCheckAllowed)
            {
                this._externalOrdersRateChecker =
                    new EventsRateChecker(
                        riskManagerSettingsBag.ExternalOrdersSubmissionRateTimeIntervalToCheck,
                        riskManagerSettingsBag.ExternalOrdersSubmissionRateMaximumAllowedInstances);
            }
            else
            {
                this._externalOrdersRateChecker = null;
            }

            if (riskManagerSettingsBag.ExternalOrdersRejectionRateCheckAllowed)
            {
                this._externalOrdersRejectsRateChecker =
                    new EventsRateChecker(
                        riskManagerSettingsBag.ExternalOrdersRejectionRateTimeIntervalToCheck,
                        riskManagerSettingsBag.ExternalOrdersRejectionRateMaximumAllowedInstances);
            }
            else
            {
                this._externalOrdersRejectsRateChecker = null;
            }


            if (riskManagerSettingsBag.FatalLogErrorsRateCheckAllowed)
            {
                _fatalLogErrorsRateChecker =
                    new EventsRateChecker(riskManagerSettingsBag.FatalLogErrorsRateTimeIntervalToCheck,
                        riskManagerSettingsBag.FatalLogErrorsRateMaximumAllowedInstances);
            }
            else
            {
                _fatalLogErrorsRateChecker = null;
            }

            this._onlinePriceDeviationChecker.ResetState(priceStreamMonitor, riskManagerSettingsBag);

            this._cumulativeSizesChecker.Enabled = riskManagerSettingsBag.CumulativeSizesCheckAllowed;

            this._symbolTrustworthyWatcher.UpdateSettings(riskManagerSettingsBag.SymbolTrustworthyCheckAllowed,
                riskManagerSettingsBag.MinimumSymbolUntrustworthyPeriod);

            if (riskManagerSettingsBag.UnsettledNopRiskCheckAllowed)
            {
                if (this._unsettledNopCheckingModule == null)
                {
                    if (!this.IsTransmissionDisabled)
                        this._logger.Log(LogLevel.Fatal,
                            "Enabling unsettled NOP checking while trading is enabled. This is not recommended as some deal can be excluded from stats if occurring concurrently with enabling. Stats can be re-initialized by disabling and enabling the Unsettled NOP checking at any time (enabling recommended during turned off kill switch)");

                    this._unsettledNopCheckingModule = new UnsettledNopCheckingModule(_persistedNopProvider,
                        this._priceStreamMonitor, _settlementDatesKeeper, this._logger);
                }
            }
            else
            {
                this._unsettledNopCheckingModule.Dispose();
                this._unsettledNopCheckingModule = null;
            }

            if (this._unsettledNopCheckingModule != null)
                this._unsettledNopCheckingModule.Reconfigure(riskManagerSettingsBag.MaximumOrderSizeRiskCheckAllowed,
                    riskManagerSettingsBag.MaxOrderSizesBaseAbsInUsd);

            this._netOpenPositionModule.Reconfigure(riskManagerSettingsBag.MaximumOrderSizeRiskCheckAllowed,
                    riskManagerSettingsBag.MaxOrderSizesBaseAbsInUsd);
        }

        public bool AllowPropagationOfStateChangesToBackend
        {
            get { return !this._currentInstanceTradingDisallowed; }
            set
            {
                this._currentInstanceTradingDisallowed = !value;
                this._persistentOrdersTransmissionControl.AllowPropagationOfStateChangesToBackend = value;
            }
        }

        public RiskManagementCheckResult AddExternalOrderAndCheckIfAllowed(IIntegratorOrderExternal externalOrder, out decimal nopEstimate)
        {
            try
            {
                return AddExternalOrderAndCheckIfAllowedInternal(externalOrder, out nopEstimate);
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, e, "Risk manager experienced unexpected exception when checking order [{0}]", externalOrder.Identity);
                this.DisableTransmission(string.Format("Risk manager experienced unexpected exception when checking order [{0}], {1}", externalOrder.Identity, e));
                nopEstimate = 0m;
                return RiskManagementCheckResult.Rejected_UnexpectedError;
            }
        }

        public bool CancelInternalOrder(decimal canceledSizeBaseAbs, PriceObject quotedPrice)
        {
            return this._cumulativeSizesChecker.CancelInternalOrder(canceledSizeBaseAbs, quotedPrice);
        }

        public decimal GetMaximumOrderSizeBaseAbs(Currency baseCurrency)
        {
            if (this._riskManagerSettingsBag.MaximumOrderSizeRiskCheckAllowed)
            {
                return this._riskManagerSettingsBag.MaxOrderSizesBaseAbs[(int) baseCurrency];
            }
            else
            {
                return decimal.MaxValue;
            }
        }

        public ISymbolTrustworthyWatcher GetSymbolTrustworthyWatcher(Symbol symbol)
        {
            return this._symbolTrustworthyWatcher.GetSymbolTrustworthyWatcher(symbol);
        }

        public ISymbolTrustworthinessInfoProvider SymbolTrustworthinessInfoProvider
        {
            get { return this._symbolTrustworthyWatcher; }
        }

        private bool IsSupportedSymbol(IIntegratorOrderInfo orderInfo)
        {
            if (orderInfo.Counterparty != Counterparty.NULL &&
                !_symbolsInfo.IsSupported(orderInfo.Symbol, orderInfo.Counterparty.TradingTargetType))
            {
                this._logger.Log(LogLevel.Fatal,
                                 "Attempt to register order [{0}] on symbol [{1}] which is unsupported for [{2}].",
                                 orderInfo.Identity, orderInfo.Symbol, orderInfo.Counterparty.TradingTargetType);
                return false;
            }

            if (!_symbolsInfo.IsSupported(orderInfo.Symbol.BaseCurrency) ||
                !_symbolsInfo.IsSupported(orderInfo.Symbol.TermCurrency))
            {
                this._logger.Log(LogLevel.Fatal,
                                 "Attempt to register order [{0}] on symbol [{1}] with unsupported currency.",
                                 orderInfo.Identity, orderInfo.Symbol);
                return false;
            }

            return true;
        }

        private RiskManagementCheckResult CheckOrderInfoIfAllowed(IIntegratorOrderInfo orderInfo, EventsRateChecker externalOrdersRateChecker)
        {
            if (_currentInstanceTradingDisallowed)
            {
                this._logger.Log(LogLevel.Error, "Attempt to register external order [{0}] on instance [{1}] with disallowed trading", orderInfo.Identity, SettingsInitializator.Instance.InstanceName);
                return RiskManagementCheckResult.Rejected_TradingDisallowedForInstance;
            }

            if (!this.IsSupportedSymbol(orderInfo))
            {
                if (!orderInfo.IsRiskRemovingOrder)
                    return RiskManagementCheckResult.Rejected_SymbolOrCurrencyNotSupported;
            }

            if (IsTransmissionDisabled)
            {
                return RiskManagementCheckResult.Rejected_TransmissionDisabled;
            }

            if (!orderInfo.IsRiskRemovingOrder)
            {
                if (this.IsGoFlatOnlyEnabled)
                {
                    return RiskManagementCheckResult.Rejected_GoFlatOnlyEnabled;
                }

                if (!_symbolTrustworthyWatcher.GetSymbolTrustworthyWatcher(orderInfo.Symbol).IsTrustworthy)
                {
                    this._logger.Log(LogLevel.Fatal,
                                     "Attempt to register order [{0}] on symbol [{1}] which is not currently trusted.",
                                     orderInfo.Identity, orderInfo.Symbol);
                    return RiskManagementCheckResult.Rejected_SymbolNotTrusted;
                }

                //reconfiguration can change this to null, also this is not performed for streaming prices
                if (externalOrdersRateChecker != null && !externalOrdersRateChecker.AddNextEventAndCheckIsAllowed())
                {
                    string reason =
                        string.Format("External order [{0}] didn't pass the ExternalOrders submission rate check",
                            orderInfo.Identity);
                    this._logger.Log(LogLevel.Fatal, reason);
                    this.DisableTransmission(reason);
                    return RiskManagementCheckResult.Rejected_OrderRateExceeded;
                }

                string priceDeviationReason;
                if (!this._onlinePriceDeviationChecker.MeetsPriceDeviationCriteria(orderInfo, out priceDeviationReason))
                {
                    this._logger.Log(LogLevel.Fatal, priceDeviationReason);
                    this.TurnOnGoFlatOnly(priceDeviationReason);
                    //this.DisableTransmission(priceDeviationReason);
                    return RiskManagementCheckResult.Rejected_PriceDeviation;
                }

                if (_riskManagerSettingsBag.MaximumOrderSizeRiskCheckAllowed
                    &&
                    orderInfo.SizeBaseAbsInitial >
                    _riskManagerSettingsBag.MaxOrderSizesBaseAbs[
                        (int) orderInfo.Symbol.BaseCurrency]
                    )
                {
                    string reason =
                        string.Format(
                            "External order [{0}] didn't pass the maximum order base size in USD check. (discarding it)",
                            orderInfo.Identity);
                    this._logger.Log(LogLevel.Fatal, reason);
                    //this.DisableTransmission(reason);
                    return RiskManagementCheckResult.Rejected_OrderSizeAboveLimit;
                }

                if (this._riskManagerSettingsBag.NopRiskCheckAllowed &&
                    //do not double check internal orders - they already call IsTradeRequestLikelyToPass
                    orderInfo.IsExternal() &&
                    !this._netOpenPositionModule.CheckIsOutgoingIncommingOrderInfoAllowed(orderInfo))
                {
                    string reason =
                        string.Format(
                            "External order [{0}] didn't pass the maximum NOP check",
                            orderInfo.Identity);
                    this._logger.Log(LogLevel.Fatal, reason);
                    this.TurnOnGoFlatOnly(reason);
                    return RiskManagementCheckResult.Rejected_CurrentDayNopAboveLimit;
                }
            }

            var unsettledNopCheckingModuleLocal = this._unsettledNopCheckingModule;
            if (unsettledNopCheckingModuleLocal != null && 
                //do not double check internal orders - they already call IsTradeRequestLikelyToPass
                orderInfo.IsExternal() &&
                !unsettledNopCheckingModuleLocal.CheckIsOutgoingIncommingOrderInfoAllowed(orderInfo))
            {
                return RiskManagementCheckResult.Rejected_UnsettledNopAboveLimit;
            }

            return RiskManagementCheckResult.Accepted;
        }

        private RiskManagementCheckResult AddExternalOrderAndCheckIfAllowedInternal(IIntegratorOrderExternal externalOrder, out decimal nopEstimate)
        {
            nopEstimate = 0m;

            if (externalOrder.OrderRequestInfo == OrderRequestInfo.INVALID_ORDER_REQUEST)
            {
                string reason = string.Format("External order [{0}] was created from invalid order request", externalOrder.Identity);
                this._logger.Log(LogLevel.Fatal, reason);
                this.DisableTransmission(reason);
                return RiskManagementCheckResult.Rejected_UnexpectedError;
            }

            RiskManagementCheckResult result = CheckOrderInfoIfAllowed(externalOrder, this._externalOrdersRateChecker);
            if (result != RiskManagementCheckResult.Accepted)
                return result;

            //Not for any streaming!!!
            if (this._cumulativeSizesChecker != null)
                this._cumulativeSizesChecker.AddExternalOrderAndCheckIfAllowedInternal(externalOrder);

            if(this.IsTransmissionDisabled)
                return RiskManagementCheckResult.Rejected_TransmissionDisabled;
            else
                return RiskManagementCheckResult.Accepted;
        }

        public RiskManagementCheckResult AddInternalOrderAndCheckIfAllowed(IIntegratorOrderInfo clientOrder)
        {
            RiskManagementCheckResult result;

            try
            {
                result = this.CheckOrderInfoIfAllowed(clientOrder, null);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Exception when checking internal order: {0}", clientOrder);
                return RiskManagementCheckResult.Rejected_UnexpectedError;
            }

            if (result == RiskManagementCheckResult.Accepted)
            {
                this._cumulativeSizesChecker.AddInternalOrderAndCheckIfAllowed(clientOrder);
            }

            return result;
        }

        public RiskManagementCheckResult CheckIfSoftPriceAllowed(IIntegratorOrderInfo streamingPrice)
        {
            RiskManagementCheckResult result = CheckOrderInfoIfAllowed(streamingPrice, null);
            if (result != RiskManagementCheckResult.Accepted)
                return result;

            if (this.IsTransmissionDisabled)
                return RiskManagementCheckResult.Rejected_TransmissionDisabled;
            else
                return RiskManagementCheckResult.Accepted;
        }

        public bool CheckIfIncomingClientDealAllowed(IIntegratorOrderInfo rejectableDeal)
        {
            if (CheckOrderInfoIfAllowed(rejectableDeal, null) != RiskManagementCheckResult.Accepted)
                return false;

            return !this.IsTransmissionDisabled;
        }

        public bool CheckIfIncomingClientDealCanBeAccepted(IIntegratorOrderInfo rejectableDeal)
        {
            if (CheckOrderInfoIfAllowed(rejectableDeal, this._externalOrdersRateChecker) != RiskManagementCheckResult.Accepted)
                return false;

            return !this.IsTransmissionDisabled;
        }

        //public void AcceptedClientDealWasLateRejected(IIntegratorOrderInfo rejectableMmDeal)
        //{
        //    this._netOpenPositionModule.RemovePreviouslyExecutedOrderInfoFromNopEstimates(rejectableMmDeal);
        //}

        //public void AcceptedClientDealWasLateRejected(IIntegratorOrderInfo orderInfo, decimal sizeToRemoveBaseAbs)
        //{
        //    this._netOpenPositionModule.RemovePreviouslyExecutedOrderInfoFromNopEstimates(orderInfo, sizeToRemoveBaseAbs);
        //}

        public bool CancelInternalOrder(IClientOrder clientOrder, decimal cancelledAmount)
        {
            return this._cumulativeSizesChecker.CancelInternalOrder(clientOrder, cancelledAmount);
        }

        public bool IsTransmissionDisabled
        {
            get { return this._persistentOrdersTransmissionControl.IsTransmissionDisabled; }
        }

        public bool IsGoFlatOnlyEnabled { get { return this._persistentOrdersTransmissionControl.IsGoFlatOnlyEnabled; } }

        public DateTime GoFlatOnlyLastEnabledTime
        {
            get { return this._persistentOrdersTransmissionControl.GoFlatOnlyLastEnabledTime; }
        }

        public event Action<bool> GoFlatOnlyStateChanged
        {
            add { this._persistentOrdersTransmissionControl.GoFlatOnlyStateChanged += value; }
            remove { this._persistentOrdersTransmissionControl.GoFlatOnlyStateChanged -= value; }
        }

        public event Action<OrderTransmissionStatusChangedEventArgs> OrderTransmissionChanged
        {
            add { this._persistentOrdersTransmissionControl.OrderTransmissionChanged += value; }
            remove { this._persistentOrdersTransmissionControl.OrderTransmissionChanged -= value; }
        }

        /// <summary>
        /// All Broadcast infos from Risk Manager - includes also all position brodcast (requested position, net position etc. per ctp and symbol)
        /// </summary>
        public event NewBroadcastInfoEventHandler NewBroadcastInfo
        {
            add
            {
                this._persistentOrdersTransmissionControl.NewBroadcastInfo += value;
            }
            remove 
            { 
                this._persistentOrdersTransmissionControl.NewBroadcastInfo -= value;
            }
        }

        public IEnumerable<IntegratorBroadcastInfoBase> ExistingInfos
        {
            get { return this._persistentOrdersTransmissionControl.ExistingInfos; }
        }

        public void DisableTransmission(string reason)
        {
            this.TurnOnGoFlatOnly(reason);
            System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(1000));
            this._persistentOrdersTransmissionControl.DisableTransmission(reason);
        }

        //we don't subscribe to event and rather invoke this directly, so that this can happen BEFORE the event
        // (OrderManagement is already hooked on the event and would try to rematch earlier)
        public void UpdateStateOfExternalOrder(IIntegratorOrderExternal externalOrder, OrderChangeEventArgs orderChangeEventArgs)
        {
            this.OnExternalOrderChanged(externalOrder, orderChangeEventArgs);
        }

        public void HandleDealDone(IntegratorDealDone integratorDealDone)
        {
            var unsettledNopCheckingModuleLocal = this._unsettledNopCheckingModule;
            if (unsettledNopCheckingModuleLocal != null)
                unsettledNopCheckingModuleLocal.AddNewDealToStats(integratorDealDone);
            this._netOpenPositionModule.AddNewDealToStats(integratorDealDone);
        }

        public bool IsTradeRequestLikelyToPass(IIntegratorOrderInfo orderInfo)
        {
            return this.IsTradeRequestLikelyToPass(orderInfo, null);
        }

        public bool IsTradeRequestLikelyToPass(IIntegratorOrderInfo orderInfo, PriceObjectInternal priceObject)
        {
            var unsettledNopCheckingModuleLocal = this._unsettledNopCheckingModule;
            return (unsettledNopCheckingModuleLocal == null ||
                    unsettledNopCheckingModuleLocal.IsTradeRequestLikelyToPass(orderInfo, priceObject))
                   &&
                   (!this._riskManagerSettingsBag.NopRiskCheckAllowed ||
                   this._netOpenPositionModule.IsTradeRequestLikelyToPass(orderInfo, priceObject));
        }

        //AcceptedClientDealWasLateRejected
        public void HandleDealLateRejected(IIntegratorOrderInfo orderInfo, IntegratorDealDone integratorDealDone)
        {
            //now the unsettled nop
            var unsettledNopCheckingModuleLocal = this._unsettledNopCheckingModule;
            if (unsettledNopCheckingModuleLocal != null)
                unsettledNopCheckingModuleLocal.RemoveDealFromStats(integratorDealDone);
            this._netOpenPositionModule.RemoveDealFromStats(integratorDealDone);
        }

        private void OnExternalOrderChanged(IIntegratorOrderExternal order, OrderChangeEventArgs orderChangeEventArgs)
        {
            IntegratorOrderExternalChange status = orderChangeEventArgs.ChangeState;

            if (!IsTransmissionDisabled &&
                (
                (status == IntegratorOrderExternalChange.Rejected && orderChangeEventArgs.RejectionInfo.IsGenuineReject)||
                 status == IntegratorOrderExternalChange.InBrokenState)
                )
            {
                var externalOrdersRejectsRateChecker = _externalOrdersRejectsRateChecker;
                if (externalOrdersRejectsRateChecker != null &&
                    !externalOrdersRejectsRateChecker.AddNextEventAndCheckIsAllowed())
                {
                    string reason = string.Format( "External order [{0}] was rejected/broken and didn't pass the integrator-level rejection rate check",
                                     order.Identity);
                    this._logger.Log(LogLevel.Fatal, reason);
                    this.DisableTransmission(reason);
                }
            }

            if (status == IntegratorOrderExternalChange.Rejected ||
                status == IntegratorOrderExternalChange.NotSubmitted ||
                status == IntegratorOrderExternalChange.Cancelled)
            {
                //NotSubmitted doesn't have rejection info
                decimal rejectedAmount = orderChangeEventArgs.RejectionInfo == null
                                             ? order.OrderRequestInfo.SizeBaseAbsInitial
                                             : orderChangeEventArgs.RejectionInfo.RejectedAmount.Value;

                this._cumulativeSizesChecker.CancelExternalOrder(order, rejectedAmount);
            }
        }

        public static RiskManagerSettingsBag ReadSettingsFromConfig(ILogger logger,
                                                                    RiskManagerSettingsBag riskManagerSettingsBag,
                                                                    RiskManagementSettings riskManagementSettings,
                                                                    decimal[] usdConversionMultipliers)
        {
            if (riskManagementSettings == null)
            {
                logger.Log(LogLevel.Fatal, "Risk management settings were null");
                throw new Exception(
                    "Risk management settings were null. Check Risk management log file for details about reading settings from DB.");
            }

            riskManagerSettingsBag.DisallowTradingOnInstances = riskManagementSettings.DisallowTradingOnInstances;

            riskManagerSettingsBag.PriceDeviationCheckAllowed =
                riskManagementSettings.PriceAndPosition.PriceDeviationCheckAllowed;
            riskManagerSettingsBag.OnlineDeviationCheckMaxPriceAge =
                riskManagementSettings.PriceAndPosition.OnlinePriceDeviationCheckMaxPriceAge;
            riskManagerSettingsBag.SymbolTrustworthyCheckAllowed =
                riskManagementSettings.PriceAndPosition.SymbolTrustworthyCheckAllowed;
            riskManagerSettingsBag.MinimumSymbolUntrustworthyPeriod =
                riskManagementSettings.PriceAndPosition.MinimumSymbolUntrustworthyPeriod;

            riskManagerSettingsBag.NopRiskCheckAllowed =
                riskManagementSettings.PriceAndPosition.ExternalNOPRiskCheckAllowed;
            riskManagerSettingsBag.UnsettledNopRiskCheckAllowed =
                riskManagementSettings.PriceAndPosition.ExternalUnsettledNOPRiskCheckAllowed;
            riskManagerSettingsBag.CumulativeSizesCheckAllowed = riskManagementSettings.PriceAndPosition
                                                                                       .ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed;
            riskManagerSettingsBag.ExternalOrdersSubmissionRateCheckAllowed =
                riskManagementSettings.ExternalOrdersSubmissionRate.RiskCheckAllowed;
            riskManagerSettingsBag.ExternalOrdersSubmissionRateTimeIntervalToCheck =
                riskManagementSettings.ExternalOrdersSubmissionRate.TimeIntervalToCheck;
            riskManagerSettingsBag.ExternalOrdersSubmissionRateMaximumAllowedInstances =
                riskManagementSettings.ExternalOrdersSubmissionRate.MaximumAllowedInstancesPerTimeInterval;
            riskManagerSettingsBag.ExternalOrdersRejectionRateCheckAllowed =
                riskManagementSettings.ExternalOrdersRejectionRate.RiskCheckAllowed;
            riskManagerSettingsBag.ExternalOrdersRejectionRateTimeIntervalToCheck =
                riskManagementSettings.ExternalOrdersRejectionRate.TimeIntervalToCheck;
            riskManagerSettingsBag.ExternalOrdersRejectionRateMaximumAllowedInstances =
                riskManagementSettings.ExternalOrdersRejectionRate.MaximumAllowedInstancesPerTimeInterval;
            riskManagerSettingsBag.FatalLogErrorsRateCheckAllowed =
                riskManagementSettings.FatalErrorsRate.RiskCheckAllowed;
            riskManagerSettingsBag.FatalLogErrorsRateMaximumAllowedInstances =
                riskManagementSettings.FatalErrorsRate.MaximumAllowedInstancesPerTimeInterval;
            riskManagerSettingsBag.FatalLogErrorsRateTimeIntervalToCheck =
                riskManagementSettings.FatalErrorsRate.TimeIntervalToCheck;

            riskManagerSettingsBag.OnlineDeviationCheckRate = new decimal[Symbol.ValuesCount];

            riskManagerSettingsBag.MaximumOrderSizeRiskCheckAllowed =
                riskManagementSettings.ExternalOrderMaximumAllowedSize.RiskCheckAllowed;

            if (riskManagementSettings.PriceAndPosition.PriceDeviationCheckAllowed)
            {
                for (int symbolId = 0; symbolId < Symbol.ValuesCount; symbolId++)
                {
                    Symbol symbol = (Symbol) symbolId;

                    var symbolSetting =
                        riskManagementSettings.PriceAndPosition.Symbols.FirstOrDefault(
                            s => s.ShortName.Equals(symbol.ShortName, StringComparison.OrdinalIgnoreCase));

                    //if (symbolSetting == null)
                    //{
                    //    string error =
                    //        string.Format(
                    //            "Settings for symbol {0} not specified in RiskManagement configuration. All symbols are required when PriceDeviation check is allowed",
                    //            symbol);
                    //    logger.Log(LogLevel.Fatal, error);
                    //    throw new Exception(error);
                    //}


                    if (symbolSetting != null && symbolSetting.NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent > 0)
                    {
                        riskManagerSettingsBag.OnlineDeviationCheckRate[symbolId] =
                           symbolSetting.NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent / 100m;
                    }
                    else if (symbolSetting == null
                        && riskManagementSettings.PriceAndPosition.DefaultNewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent >= 0)
                    {
                        riskManagerSettingsBag.OnlineDeviationCheckRate[symbolId] =
                           riskManagementSettings.PriceAndPosition.DefaultNewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent / 100m;
                    }
                    else
                    {
                        string error =
                            string.Format(
                                "NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent setting for symbol {0} not specified (and DefaultNewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent also not specified) or specified incorrectly (<= 0) in RiskManagement configuration. This setting is required for all symbols when PriceDeviation check is allowed",
                                symbol);
                        logger.Log(LogLevel.Fatal, error);
                        throw new Exception(error);
                    }
                }
            }

            riskManagerSettingsBag.MaxOrderSizesBaseAbsInUsd = riskManagementSettings.ExternalOrderMaximumAllowedSize
                                                                                     .MaximumAllowedBaseAbsSizeInUsd;

            if (riskManagementSettings.ExternalOrderMaximumAllowedSize.RiskCheckAllowed)
            {
                riskManagerSettingsBag.MaxOrderSizesBaseAbs = Enumerable.Repeat(0m, Currency.ValuesCount).ToArray();

                foreach (Currency usedCurrency in Symbol.UsedCurrencies)
                {
                    int currencyIdx = (int) usedCurrency;
                    if (usdConversionMultipliers[currencyIdx] > 0m)
                        riskManagerSettingsBag.MaxOrderSizesBaseAbs[currencyIdx] =
                            decimal.Ceiling(
                                riskManagementSettings.ExternalOrderMaximumAllowedSize
                                                      .MaximumAllowedBaseAbsSizeInUsd
                                /
                                usdConversionMultipliers[currencyIdx]);
                }
            }

            return riskManagerSettingsBag;
        }

        public void TurnOnGoFlatOnly(string reason)
        {
            this._persistentOrdersTransmissionControl.TurnOnGoFlatOnly(reason);
        }

        public IOnlinePriceDeviationChecker OnlinePriceDeviationChecker
        {
            get { return this._onlinePriceDeviationChecker; }
        }
    }
}
