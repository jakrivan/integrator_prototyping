﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;

namespace Kreslik.Integrator.RiskManagement
{
    public class CumulativeSizesChecker
    {
        //REMARKS:
        // According to MSDN - Intelocked Add method doesn't throw, it overflows
        // if casting to long would be about to throw, we can perform in unchecked{} context to force overflow
        // long -> ulong casting can never overflow - we don't need to perform in unchecked context

        private long[][] _cumulativeVolumesInternallyRequested =
            {
                new long[Symbol.ValuesCount],
                new long[Symbol.ValuesCount]
            };

        private long[][] _cumulativeVolumesExternallyRequested =
            {
                new long[Symbol.ValuesCount],
                new long[Symbol.ValuesCount]
            };

        private ILogger _logger;

        public CumulativeSizesChecker(ILogger logger)
        {
            this._logger = logger;
        }

        private bool _enabled;
        public bool Enabled
        {
            set
            {
                Volatile.Write(ref this._enabled, value);
            }
        }

        public bool AddExternalOrderAndCheckIfAllowedInternal(IIntegratorOrderInfo orderInfo)
        {
            if (!_enabled)
                return true;

            DealDirection orderDirection = orderInfo.IntegratorDealDirection;
            Symbol orderSymbol = orderInfo.Symbol;


            ulong externalSize = (ulong)
                Interlocked.Add(
                ref _cumulativeVolumesExternallyRequested[(int)orderDirection][(int)orderSymbol],
                (long)orderInfo.SizeBaseAbsInitial);

            ulong internalSize = (ulong)_cumulativeVolumesInternallyRequested[(int)orderDirection][(int)orderSymbol];

            if (externalSize > internalSize)
            {
                this._logger.Log(LogLevel.Fatal,
                                 "External order [{0}] didn't pass the check of cumulative external sizes (BaseAbs) ({1}) below or equal to internal cumulative sizes {2}",
                                 orderInfo.Identity,
                                 externalSize, internalSize);

                //this.DisableTransmission();
                //return false;
            }
            //else
            //{
            //    this._logger.Log(LogLevel.Debug, "New {0} {1} Externally requested cumulative volume (BaseAbs): {2}",
            //                     orderDirection, orderSymbol, externalSize);
            //}

            return true;
        }

        public bool AddInternalOrderAndCheckIfAllowed(IIntegratorOrderInfo clientOrder)
        {
            return this.AddInternalOrderAndCheckIfAllowedInternal(clientOrder.SizeBaseAbsInitial,
                                                                  clientOrder.IntegratorDealDirection,
                                                                  clientOrder.Symbol);
        }

        private bool AddInternalOrderAndCheckIfAllowedInternal(decimal requestedSizeBaseAbs, DealDirection orderDirection, Symbol orderSymbol)
        {
            if (!_enabled)
                return true;

            //We expect than single order will fit in long/MaxValue limit
            //otherwise we'd need intermediate step:
            //long orderSize;
            //unchecked
            //{
            //    orderSize = (long) (ulong) clientOrder.OrderRequestInfo.SizeBaseAbsInitial;
            //}

            ulong newInternalVolume = (ulong)
                Interlocked.Add(
                    ref _cumulativeVolumesInternallyRequested[(int)orderDirection][(int)orderSymbol],
                    (long)requestedSizeBaseAbs);

            //this._logger.Log(LogLevel.Debug, "New {0} {1} Internaly requested cumulative volume: {2}",
            //                     orderDirection, orderSymbol, newInternalVolume);

            return true;
        }

        public bool CancelInternalOrder(IClientOrder clientOrder, decimal cancelledAmount)
        {
            return this.CancelInternalOrderInternal(cancelledAmount, clientOrder.OrderRequestInfo.IntegratorDealDirection,
                                                    clientOrder.OrderRequestInfo.Symbol);
        }

        public bool CancelInternalOrder(decimal canceledSizeBaseAbs, PriceObject quotedPrice)
        {
            return this.CancelInternalOrderInternal(canceledSizeBaseAbs, quotedPrice.Side.ToOutgoingDealDirection(),
                                                    quotedPrice.Symbol);
        }

        private bool CancelInternalOrderInternal(decimal cancelledAmount, DealDirection orderDirection, Symbol orderSymbol)
        {
            if (!_enabled)
                return true;

            ulong newInternalVolume = (ulong)
                Interlocked.Add(
                    ref _cumulativeVolumesInternallyRequested[(int)orderDirection][(int)orderSymbol],
                    (long)-cancelledAmount);

            //this._logger.Log(LogLevel.Debug, "New {0} {1} Internaly requested cumulative volume: {2}",
            //                     orderDirection, orderSymbol, newInternalVolume);

            return true;
        }

        public bool CancelExternalOrder(IIntegratorOrderExternal externalOrder, decimal rejectedAmount)
        {
            if (!_enabled)
                return true;

            DealDirection orderDirection = externalOrder.OrderRequestInfo.Side;
            Symbol orderSymbol = externalOrder.OrderRequestInfo.Symbol;

            ulong newExternalVolume = (ulong)
                Interlocked.Add(
                    ref _cumulativeVolumesExternallyRequested[(int)orderDirection][(int)orderSymbol],
                    (long)-rejectedAmount);

            //this._logger.Log(LogLevel.Debug, "New {0} {1} Externally requested cumulative volume: {2}",
            //                     orderDirection, orderSymbol, newExternalVolume);

            return true;
        }
    }
}
