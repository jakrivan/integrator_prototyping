﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Kreslik.Integrator.Common;
using System.Linq;

namespace Kreslik.Integrator.RiskManagement
{
    public class RiskManagementSettings
    {
        public string ConfigLastUpdated { get; set; }

        [XmlArray(ElementName = "DisallowTradingOnInstances")]
        [XmlArrayItem(ElementName = "Instance")]
        public List<string> DisallowTradingOnInstances
        {
            get;
            set;
        }

        public ExternalOrdersSubmissionRateSettings ExternalOrdersSubmissionRate { get; set; }
        public ExternalOrdersRejectionRateSettings ExternalOrdersRejectionRate { get; set; }
        public FatalErrorsRateSettings FatalErrorsRate { get; set; }
        public ExternalOrderMaximumAllowedSizeSettings ExternalOrderMaximumAllowedSize { get; set; }
        public PriceAndPositionSettings PriceAndPosition { get; set; }

        private static RiskManagementSettings _riskManagementBehavior;

        public class ExternalOrdersSubmissionRateSettings
        {
            public bool RiskCheckAllowed { get; set; }

            public int MaximumAllowedInstancesPerTimeInterval { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public TimeSpan TimeIntervalToCheck { get; set; }

            [System.Xml.Serialization.XmlElement("TimeIntervalToCheck_Seconds")]
            public int TimeIntervalToCheckXml
            {
                get { return (int)TimeIntervalToCheck.TotalSeconds; }
                set { TimeIntervalToCheck = System.TimeSpan.FromSeconds(value); }
            }
        }

        public class ExternalOrdersRejectionRateSettings
        {
            public bool RiskCheckAllowed { get; set; }

            public int MaximumAllowedInstancesPerTimeInterval { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public TimeSpan TimeIntervalToCheck { get; set; }

            [System.Xml.Serialization.XmlElement("TimeIntervalToCheck_Seconds")]
            public int TimeIntervalToCheckXml
            {
                get { return (int)TimeIntervalToCheck.TotalSeconds; }
                set { TimeIntervalToCheck = System.TimeSpan.FromSeconds(value); }
            }
        }

        public class FatalErrorsRateSettings
        {
            public bool RiskCheckAllowed { get; set; }

            public int MaximumAllowedInstancesPerTimeInterval { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public TimeSpan TimeIntervalToCheck { get; set; }

            [System.Xml.Serialization.XmlElement("TimeIntervalToCheck_Seconds")]
            public int TimeIntervalToCheckXml
            {
                get { return (int)TimeIntervalToCheck.TotalSeconds; }
                set { TimeIntervalToCheck = System.TimeSpan.FromSeconds(value); }
            }
        }

        public class ExternalOrderMaximumAllowedSizeSettings
        {
            public bool RiskCheckAllowed { get; set; }

            public decimal MaximumAllowedBaseAbsSizeInUsd { get; set; }
        }

        public class PriceAndPositionSettings
        {
            public bool ExternalNOPRiskCheckAllowed { get; set; }
            public bool ExternalUnsettledNOPRiskCheckAllowed { get; set; }

            public bool PriceDeviationCheckAllowed { get; set; }
            [System.Xml.Serialization.XmlIgnore]
            public TimeSpan OnlinePriceDeviationCheckMaxPriceAge { get; set; }

            [System.Xml.Serialization.XmlElement("OnlinePriceDeviationCheckMaxPriceAge_Minutes")]
            public int OnlinePriceDeviationCheckMaxPriceAgeXml
            {
                get { return (int)OnlinePriceDeviationCheckMaxPriceAge.TotalMinutes; }
                set { OnlinePriceDeviationCheckMaxPriceAge = System.TimeSpan.FromMinutes(value); }
            }

            public bool SymbolTrustworthyCheckAllowed { get; set; }

            public TimeSpan MinimumSymbolUntrustworthyPeriod { get; set; }

            [System.Xml.Serialization.XmlElement("MinimumSymbolUntrustworthyPeriod_Seconds")]
            public int MinimumSymbolUntrustworthyPeriodXml
            {
                get { return (int)MinimumSymbolUntrustworthyPeriod.TotalSeconds; }
                set { MinimumSymbolUntrustworthyPeriod = System.TimeSpan.FromSeconds(value); }
            }

            public bool ExternalOrdersCumulativeSizeLessOrEqualToInternalOrdersCumulativeSizeCheckAllowed { get; set; }

            public decimal DefaultNewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent { get; set; }

            public List<Symbol> Symbols { get; set; }

            public class Symbol
            {
                [System.Xml.Serialization.XmlAttribute]
                public string ShortName { get; set; }
                [System.Xml.Serialization.XmlAttribute]
                public decimal NewOrderPriceMaximumDeviationFromOnlineMedianPriceInPercent { get; set; }
            }
        }
    }
}
