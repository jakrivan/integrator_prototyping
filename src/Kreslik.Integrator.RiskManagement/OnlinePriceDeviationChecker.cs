﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.RiskManagement
{
    internal interface IOnlinePriceDeviationCheckerInternal : IOnlinePriceDeviationChecker, IPriceDeviationCheckState
    {
        void ResetState(IPriceStreamMonitor priceStreamMonitor,
                        RiskManagerSettingsBag riskManagerSettingsBag);
    }

    public class OnlinePriceDeviationChecker : IOnlinePriceDeviationCheckerInternal
    {
        private IPriceStreamMonitor _priceStreamMonitor;
        private ILogger _logger;
        private RiskManagerSettingsBag _riskManagerSettingsBag;
        private TimeSpan _maxPriceAge;

        public OnlinePriceDeviationChecker(ILogger logger)
        {
            this._logger = logger;
        }

        public void ResetState(IPriceStreamMonitor priceStreamMonitor,
                                RiskManagerSettingsBag riskManagerSettingsBag)
        {
            _priceStreamMonitor = priceStreamMonitor;
            _maxPriceAge = riskManagerSettingsBag.OnlineDeviationCheckMaxPriceAge;
            this._riskManagerSettingsBag = riskManagerSettingsBag;
        }

        public bool GetIsInitialized(Symbol symbol)
        {
            return this._riskManagerSettingsBag.PriceDeviationCheckAllowed &&
                   this._priceStreamMonitor.GetHasFreshDataForSymbol(symbol, _maxPriceAge);
        }

        public bool MeetsPriceDeviationCriteria(IIntegratorOrderInfo orderInfo, out string reason)
        {
            if (this._riskManagerSettingsBag.PriceDeviationCheckAllowed && orderInfo.IntegratorOrderInfoType != IntegratorOrderInfoType.InternalMktOrder)
            {
                decimal medianPrice;
                DateTime oldestPriceAge;
                if (this._priceStreamMonitor.TryGetOnlineMedianPrice(orderInfo.Symbol, orderInfo.IntegratorDealDirection.ToInitiatingPriceDirection(), out medianPrice,
                                                                     out oldestPriceAge))
                {
                    if (oldestPriceAge < HighResolutionDateTime.UtcNow - _maxPriceAge)
                    {
                        reason =
                            string.Format(
                                "External order [{0}] didn't pass the {2} price deviation check as the oldest price was from {1:yyyy-MM-dd-HH:mm:ss.fffffff UTC} which exceeds the max median price age threshold",
                                orderInfo.Identity, oldestPriceAge, orderInfo.Symbol);
                        return false;
                    }

                    //this._logger.Log(LogLevel.Trace,
                    //                 "Current {0} {1} median price: {2}, checking the order [{3}] ({4}) with price {5}",
                    //                 orderInfo.IntegratorDealDirection.ToInitiatingPriceDirection(),orderInfo.Symbol, 
                    //                 medianPrice, orderInfo.Identity, orderInfo.IntegratorDealDirection, orderInfo.RequestedPrice);

                    if (
                        //we exceeded maximum buy price
                        orderInfo.IntegratorDealDirection == DealDirection.Buy &&
                        orderInfo.RequestedPrice >
                        medianPrice *
                        (1m + this._riskManagerSettingsBag.OnlineDeviationCheckRate[(int)orderInfo.Symbol])
                        ||
                        //or we didn't met minimum sell price
                        orderInfo.IntegratorDealDirection == DealDirection.Sell &&
                        orderInfo.RequestedPrice <
                        medianPrice *
                        (1m - this._riskManagerSettingsBag.OnlineDeviationCheckRate[(int)orderInfo.Symbol]))
                    {
                        reason = string.Format("External order [{0}] ({1} {2} at {3}) didn't pass the price deviation check (against current median price {4})",
                                              orderInfo.Identity, orderInfo.IntegratorDealDirection, orderInfo.Symbol, orderInfo.RequestedPrice, medianPrice);
                        return false;
                    }
                }
                else
                {
                    reason = string.Format("Order {0} coming in but online {1} price deviation check is still not fully initialized - cannot use online value",
                                     orderInfo.Identity, orderInfo.Symbol);
                    return false;
                }

            }

            reason = null;
            return true;
        }
    }
}
