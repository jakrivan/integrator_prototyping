﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.RiskManagement
{

    public abstract class NopSettingsPoint
    {
        protected NopSettingsPoint(NopSettingsPointDAL nopSettingsPoint)
        {
            this.LimitsPerTradeDateTotal = new UnsettledNopCreditLineLimitsSettingsPoint(
                AtomicSize.FromDecimal(nopSettingsPoint.MaximumTradeDayNopTotal),
                AtomicSize.FromDecimal(nopSettingsPoint.MaximumTradeDayNopTotalToLockDestination));
        }

        protected void Update(NopSettingsPointDAL nopSettingsPoint)
        {
            this.LimitsPerTradeDateTotal.Update(
                AtomicSize.FromDecimal(nopSettingsPoint.MaximumTradeDayNopTotal),
                AtomicSize.FromDecimal(nopSettingsPoint.MaximumTradeDayNopTotalToLockDestination));
        }

        public UnsettledNopCreditLineLimitsSettingsPoint LimitsPerTradeDateTotal { get; private set; }

        public abstract string DescribeName { get; }
        public abstract int? DatabaseIdNullable { get; }
    }

    public class OverallNopSettingsPoint : NopSettingsPoint
    {
        public OverallNopSettingsPoint(NopSettingsPointOverall nopSettingsPoint)
            :base(nopSettingsPoint)
        { }

        public void Update(NopSettingsPointOverall nopSettingsPoint)
        {
            base.Update(nopSettingsPoint);
        }

        public override string DescribeName { get { return "Across All PBs"; } }
        public override int? DatabaseIdNullable { get { return null; } }
    }

    public class PbNopSettingsPoint : NopSettingsPoint
    {
        public PbNopSettingsPoint(NopSettingsPointPerPrimeBroker nopSettingsPoint)
            : base(nopSettingsPoint)
        {
            this.DatabaseId = nopSettingsPoint.PrimeBrokerId;
            this.PrimeBrokerName = nopSettingsPoint.PrimeBrokerName;
        }

        public void UpdateIfMatch(NopSettingsPointPerPrimeBroker nopSettingsPoint)
        {
            if (nopSettingsPoint.PrimeBrokerId == this.DatabaseId)
            {
                this.PrimeBrokerName = nopSettingsPoint.PrimeBrokerName;
                base.Update(nopSettingsPoint);
            }
        }

        public int DatabaseId { get; private set; }
        public string PrimeBrokerName { get; private set; }

        public override string DescribeName { get { return this.PrimeBrokerName; } }
        public override int? DatabaseIdNullable { get { return this.DatabaseId; } }
    }


    public class NetOpenPositionModule
    {
        private class NopStatsHolder
        {
            public NopStatsHolder(int numOfPbs)
            {
                this.NopOverallStats = new NopPerPbStats();
                this.NopStatsPerPb = Enumerable.Repeat(0, numOfPbs).Select(i => new NopPerPbStats()).ToArray();
            }

            internal void CopyFrom(NopStatsHolder statsCopy)
            {
                this.NopOverallStats.CopyFrom(statsCopy.NopOverallStats);

                for (int arrIdx = 0; arrIdx < this.NopStatsPerPb.Length; arrIdx++)
                {
                    this.NopStatsPerPb[arrIdx].CopyFrom(statsCopy.NopStatsPerPb[arrIdx]);
                }
            }

            internal void RecalculateUsdAggregations(decimal[] usdConversionMultipliers)
            {
                this.NopOverallStats.RecalculateUsdAggregations(usdConversionMultipliers);

                foreach (NopPerPbStats nopPerPbStatse in NopStatsPerPb)
                {
                    nopPerPbStatse.RecalculateUsdAggregations(usdConversionMultipliers);
                }
            }

            internal void CopyUsdRecalculationsFrom(NopStatsHolder statsCopy)
            {
                this.NopOverallStats.CopyUsdRecalculationsFrom(statsCopy.NopOverallStats);

                for (int arrIdx = 0; arrIdx < this.NopStatsPerPb.Length; arrIdx++)
                {
                    this.NopStatsPerPb[arrIdx].CopyUsdRecalculationsFrom(
                        statsCopy.NopStatsPerPb[arrIdx]);
                }
            }

            internal void CopyCreditZonesFrom(NopStatsHolder statsCopy)
            {
                this.NopOverallStats.CopyCreditZoneFrom(statsCopy.NopOverallStats);

                for (int arrIdx = 0; arrIdx < this.NopStatsPerPb.Length; arrIdx++)
                {
                    this.NopStatsPerPb[arrIdx].CopyCreditZoneFrom(
                        statsCopy.NopStatsPerPb[arrIdx]);
                }
            }

            public void ClearNopStats()
            {
                this.NopOverallStats.Clear();
                Array.ForEach(this.NopStatsPerPb, t => t.Clear());
            }

            public NopPerPbStats NopOverallStats { get; private set; }
            public NopPerPbStats[] NopStatsPerPb { get; private set; }
        }

        public class NopPerPbStats
        {
            public void CopyFrom(NopPerPbStats copy)
            {
                this.LongUsdNop = copy.LongUsdNop;
                this.ShortUsdNop = copy.ShortUsdNop;
                this.NominalPositions.CopyWholeFrom(copy.NominalPositions);
                this.HasAnyRealizedDeal = copy.HasAnyRealizedDeal;
                this._creditZone = copy._creditZone;
            }

            public void CopyUsdRecalculationsFrom(NopPerPbStats copy)
            {
                this.LongUsdNop = copy.LongUsdNop;
                this.ShortUsdNop = copy.ShortUsdNop;
                this._creditZone = copy._creditZone;
            }

            public void CopyCreditZoneFrom(NopPerPbStats copy)
            {
                this._creditZone = copy._creditZone;
            }

            public void RecalculateUsdAggregations(decimal[] usdConversionMultipliers)
            {
                AtomicSize shortNopUsd = AtomicSize.ZERO;
                AtomicSize longNopUsd = AtomicSize.ZERO;

                for (int currIdx = 0; currIdx < Currency.ValuesCount; currIdx++)
                {
                    AtomicSize nominalPosition = NominalPositions[currIdx];
                    if (nominalPosition > AtomicSize.ZERO)
                    {
                        longNopUsd += nominalPosition * usdConversionMultipliers[currIdx];
                    }
                    else if (nominalPosition < AtomicSize.ZERO)
                    {
                        shortNopUsd -= nominalPosition * usdConversionMultipliers[currIdx];
                    }
                }

                this.LongUsdNop = longNopUsd;
                this.ShortUsdNop = shortNopUsd;
            }

            public void Clear()
            {
                this.LongUsdNop.InterlockedZeroOut();
                this.ShortUsdNop.InterlockedZeroOut();
                this.NominalPositions.ClearWhole();
                this.HasAnyRealizedDeal = false;
                this._creditZone = CreditZone.NoCheckingZone;
            }

            public CreditZone GetCreditZone()
            {
                return this._creditZone;
            }

            public bool SetCreditZoneAndGetIsChange(CreditZone newCreditZone)
            {
                if (this._creditZone != newCreditZone)
                {
                    this._creditZone = newCreditZone;

                    return true;
                }

                return false;
            }



            public AtomicSize[] NominalPositions = new AtomicSize[Currency.ValuesCount];
            public AtomicSize LongUsdNop;
            public AtomicSize ShortUsdNop;
            public bool HasAnyRealizedDeal;
            private CreditZone _creditZone;
        }

        //TODO: extract common with UnsettledNopTrackin to common single object
        // especially list of unsettled deals (but per trading date!), etc.

        private decimal[] _usdConversionMultipliers;
        private NopStatsHolder _nopStats;
        private NopStatsHolder _backgroundStatsForConcurrentUpdate;
        private readonly object _backgroundRecalculationLocker = new object();
        private int[] _primeBrokersMap;
        private PbNopSettingsPoint[] _primeBrokersSettings;
        private OverallNopSettingsPoint _overallSettings;
        private bool _isMaxOrderSizeCheckingAllowed;
        private AtomicSize _maximumAllowedOrderSizeInUsd;
        private IPersistedNopProvider _persistedNopProvider;
        private IPriceStreamMonitor _priceStreamMonitor;
        private readonly ILogger _logger;
        private DateTime _nextStatsRecalculationNeededAfterUtc;

        public NetOpenPositionModule(IPersistedNopProvider persistedNopProvider, IPriceStreamMonitor priceStreamMonitor,
            ILogger logger)
        {
            this._usdConversionMultipliers = priceStreamMonitor.UsdConversionMultipliers;
            this._logger = logger;
            this._persistedNopProvider = persistedNopProvider;
            this._priceStreamMonitor = priceStreamMonitor;
            this.Initialize(persistedNopProvider.GetNopPolPerCreditLineSettlementAndCurrency(),
                persistedNopProvider.GetOverallNopSettings(), persistedNopProvider.GetPrimeBrokerNopSettings());
            persistedNopProvider.PrimeBrokerNopSettingsChanged += PersistedNopProviderOnPrimeBrokerNopSettingsChanged;
            persistedNopProvider.OverallNopSettingsChanged += PersistedNopProviderOnOverallNopSettingsChanged;

            TradingHoursHelper.Instance.MarketRollingOver += RecalculateStatsOnRollover;
            priceStreamMonitor.NewUsdConversionMultipliersAvailable += PriceStreamMonitorOnNewUsdConversionMultipliersAvailable;
        }

        private void PriceStreamMonitorOnNewUsdConversionMultipliersAvailable(decimal[] usdConversionMultipliers)
        {
            this._usdConversionMultipliers = usdConversionMultipliers;
            if (HighResolutionDateTime.UtcNow > _nextStatsRecalculationNeededAfterUtc)
            {
                this.RecalculateStatsOnUsdRateChange();
                _nextStatsRecalculationNeededAfterUtc = HighResolutionDateTime.UtcNow.AddMinutes(5);
            }
        }

        public void Dispose()
        {
            TradingHoursHelper.Instance.MarketRollingOver -= RecalculateStatsOnRollover;
            _priceStreamMonitor.NewUsdConversionMultipliersAvailable -= PriceStreamMonitorOnNewUsdConversionMultipliersAvailable;

            _persistedNopProvider.PrimeBrokerNopSettingsChanged -= PersistedNopProviderOnPrimeBrokerNopSettingsChanged;
            _persistedNopProvider.OverallNopSettingsChanged -= PersistedNopProviderOnOverallNopSettingsChanged;
        }

        private void PersistedNopProviderOnOverallNopSettingsChanged(NopSettingsPointOverall nopSettingsPointOverall)
        {
            this._overallSettings.Update(nopSettingsPointOverall);
        }

        private bool AreCreditLineMappingsSame(IEnumerable<NopSettingsPointPerPrimeBroker> nopSettingsPointPerPrimeBroker)
        {
            //return unsettletNopSettingsPoints.Any(
            //    sett =>
            //        sett.Counterparties.Any(
            //            cpt => this._creditLineSettings[_creditLinesMap[(int) cpt]].DatabaseId != sett.CreditLineId));

            bool[] counterpartiesConfigured = new bool[Counterparty.ValuesCount];

            //Remapping of any counterparty
            foreach (NopSettingsPointPerPrimeBroker nopSettingsPoint in nopSettingsPointPerPrimeBroker)
            {
                foreach (Counterparty counterparty in nopSettingsPoint.Counterparties)
                {
                    counterpartiesConfigured[(int)counterparty] = true;
                    if (_primeBrokersMap[(int)counterparty] < 0 ||
                        this._primeBrokersSettings[_primeBrokersMap[(int)counterparty]].DatabaseId !=
                        nopSettingsPoint.PrimeBrokerId)
                        return false;
                }
            }

            //Removing of any counterparty
            for (int ctpIdx = 0; ctpIdx < Counterparty.ValuesCount; ctpIdx++)
            {
                if (_primeBrokersMap[ctpIdx] != -1 && !counterpartiesConfigured[ctpIdx])
                    return false;
            }

            return true;
        }

        private void PersistedNopProviderOnPrimeBrokerNopSettingsChanged(IEnumerable<NopSettingsPointPerPrimeBroker> nopSettingsPointPerPrimeBroker)
        {
            //prevent multiple enumerations
            nopSettingsPointPerPrimeBroker = nopSettingsPointPerPrimeBroker.ToList();

            //verify mapping is same
            if (!AreCreditLineMappingsSame(nopSettingsPointPerPrimeBroker))
            {
                string oldMapping = string.Empty;

                foreach (int pbId in _primeBrokersMap.GroupBy(pbId => pbId).Select(grp => grp.Key))
                {
                    string pbName = pbId < 0 ? "<NO PRIME BROKER>" : this._primeBrokersSettings[pbId].PrimeBrokerName;
                    int lineIdLocal = pbId;
                    string counterparties = string.Join(", ",
                        this._primeBrokersMap.Select((lnId, cptId) => new { lnId, cptId })
                            .Where(pair => pair.lnId == lineIdLocal)
                            .Select(pair => ((Counterparty)pair.cptId).ToString()));
                    oldMapping += string.Format("[{0}]: ({1});", pbName, counterparties);
                }

                string newMapping = string.Empty;
                foreach (NopSettingsPointPerPrimeBroker nopSettingsPoint in nopSettingsPointPerPrimeBroker)
                {
                    newMapping += string.Format("[{0}]: ({1});", nopSettingsPoint.PrimeBrokerName,
                        string.Join(", ", nopSettingsPoint.Counterparties));
                }

                this._logger.Log(LogLevel.Fatal, "Attempt to alter counterparties<->prime brokers mappings - only to original mappings will be used. {0}Old mappings: {1}{0}New mappings: {2}",
                    Environment.NewLine, oldMapping, newMapping);
            }


            //update
            foreach (NopSettingsPointPerPrimeBroker settingsPointPerPrimeBroker in nopSettingsPointPerPrimeBroker)
            {
                foreach (PbNopSettingsPoint primeBrokersSetting in _primeBrokersSettings)
                {
                    primeBrokersSetting.UpdateIfMatch(settingsPointPerPrimeBroker);
                }
            }

            //Check if any limits breached (by stats decreasing)
            this.CheckLimits();
        }

        public void Reconfigure(bool isMaxOrderSizeCheckingAllowed, decimal maximumAllowedOrderSizeInUsd)
        {
            if (this._isMaxOrderSizeCheckingAllowed != isMaxOrderSizeCheckingAllowed ||
                this._maximumAllowedOrderSizeInUsd != AtomicSize.FromDecimal(maximumAllowedOrderSizeInUsd))
            {
                this._isMaxOrderSizeCheckingAllowed = isMaxOrderSizeCheckingAllowed;
                this._maximumAllowedOrderSizeInUsd = AtomicSize.FromDecimal(maximumAllowedOrderSizeInUsd);

                lock (_backgroundRecalculationLocker)
                {
                    this.CheckLimits();
                    _backgroundStatsForConcurrentUpdate.CopyCreditZonesFrom(_nopStats);
                }
            }
        }

        private void Initialize(IEnumerable<IntegratorDealDone> unsettletNopInfoPoints,
            NopSettingsPointOverall nopSettingsPointOverall, IEnumerable<NopSettingsPointPerPrimeBroker> pbSettingsPoints)
        {
            unsettletNopInfoPoints = unsettletNopInfoPoints.ToList();
            pbSettingsPoints = pbSettingsPoints.ToList();
            int pbsNum = pbSettingsPoints.Count();

            this._primeBrokersMap = Enumerable.Repeat(-1, Counterparty.ValuesCount).ToArray();

            this._nopStats = new NopStatsHolder(pbsNum);
            this._backgroundStatsForConcurrentUpdate = new NopStatsHolder(pbsNum);
            this._primeBrokersSettings = new PbNopSettingsPoint[pbsNum];
            this._overallSettings = new OverallNopSettingsPoint(nopSettingsPointOverall);

            //CAREFULL!! - the prime broker id in memory != prime broker id in backend (to make them more compact)
            int primeBrokerIdInMemory = 0;
            foreach (NopSettingsPointPerPrimeBroker pbNopSettingsPoint in pbSettingsPoints)
            {
                this._primeBrokersSettings[primeBrokerIdInMemory] =
                    new PbNopSettingsPoint(pbNopSettingsPoint);
                foreach (Counterparty counterparty in pbNopSettingsPoint.Counterparties)
                {
                    if (this._primeBrokersMap[(int)counterparty] != -1)
                    {
                        string error =
                            string.Format(
                                "Invalid prime broker mappings configuration - counterparty [{0}] mapped to [{1}] and [{2}]",
                                counterparty,
                                this._primeBrokersSettings[this._primeBrokersMap[(int)counterparty]].PrimeBrokerName,
                                pbNopSettingsPoint.PrimeBrokerName);
                        this._logger.Log(LogLevel.Fatal, error);
                        throw new Exception(error);
                    }

                    this._primeBrokersMap[(int)counterparty] = primeBrokerIdInMemory;
                }
                primeBrokerIdInMemory++;
            }

            foreach (IntegratorDealDone unsettletNopInfoPoint in unsettletNopInfoPoints.Where(d => d.TradeDateUtc == DateTime.UtcNow.Date))
            {
                this.AddDataPointToStats(this._nopStats, unsettletNopInfoPoint, LimitsCheckingMode.NoCheck);
            }

            this.CheckLimits();

            //keep background completely up to date
            _backgroundStatsForConcurrentUpdate.CopyFrom(_nopStats);
        }

        //Identical code for overall and per PB info
        public static AtomicSize AddDataPointAndGetNop(NopPerPbStats statsPerPb,
            IntegratorDealDone integratorDealDone, decimal[] usdConversionMultipliers)
        {
            AtomicSize newBasePos = statsPerPb.NominalPositions[
                (int)integratorDealDone.Symbol.BaseCurrency].InterlockedAdd(integratorDealDone.SizeBasePolarized);
            AtomicSize newTermPos = statsPerPb.NominalPositions[
                (int)integratorDealDone.Symbol.TermCurrency].InterlockedAdd(integratorDealDone.SizeTermPolarized);
            statsPerPb.HasAnyRealizedDeal = true;

            AtomicSize wasBasePos = newBasePos - integratorDealDone.SizeBasePolarized;
            AtomicSize wasTermPos = newTermPos - integratorDealDone.SizeTermPolarized;

            AtomicSize longDiffBase, shortDiffBase, longDiffTerm, shortDiffTerm;
            UnsettledNopCheckingModule.CalculateNominalNopDiff(wasBasePos, newBasePos, out longDiffBase, out shortDiffBase);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(wasTermPos, newTermPos, out longDiffTerm, out shortDiffTerm);

            AtomicSize overalLongDiffUsd = longDiffBase *
                                           usdConversionMultipliers[(int)integratorDealDone.Symbol.BaseCurrency] +
                                           longDiffTerm *
                                           usdConversionMultipliers[(int)integratorDealDone.Symbol.TermCurrency];

            AtomicSize overalShortDiffUsd = shortDiffBase *
                                           usdConversionMultipliers[(int)integratorDealDone.Symbol.BaseCurrency] +
                                           shortDiffTerm *
                                           usdConversionMultipliers[(int)integratorDealDone.Symbol.TermCurrency];


            AtomicSize newLongNop = statsPerPb.LongUsdNop.InterlockedAdd(overalLongDiffUsd);
            AtomicSize newShortNop = statsPerPb.ShortUsdNop.InterlockedAdd(overalShortDiffUsd);

            return AtomicSize.Max(newLongNop, newShortNop);
        }

        private void AddDataPointToStats(NopStatsHolder nopStatsLocal,
            IntegratorDealDone unsettletNopInfoPoint, LimitsCheckingMode limitsCheckingMode)
        {
            int primeBrokerIdx = _primeBrokersMap[(int)unsettletNopInfoPoint.Counterparty];

            NopPerPbStats pbStatsPoint = null;
            if (primeBrokerIdx < 0)
            {
                this._logger.Log(LogLevel.Fatal, "Attempt to add NOP for counterparty ({0}) that has no prime broker mapping set. NOP per PB checking will not be performed",
                    unsettletNopInfoPoint.Counterparty);
            }
            else
            {
                pbStatsPoint = nopStatsLocal.NopStatsPerPb[primeBrokerIdx];
            }

            AtomicSize nopOverall = AddDataPointAndGetNop(nopStatsLocal.NopOverallStats,
                unsettletNopInfoPoint, this._usdConversionMultipliers);

            AtomicSize nopPerPrimeBroker = AtomicSize.ZERO;

            if (pbStatsPoint != null)
                nopPerPrimeBroker = AddDataPointAndGetNop(pbStatsPoint, unsettletNopInfoPoint, this._usdConversionMultipliers);

            if (limitsCheckingMode != LimitsCheckingMode.NoCheck)
            {
                //prevent double logging
                if (limitsCheckingMode == LimitsCheckingMode.CheckWithEvents)
                    this._logger.Log(LogLevel.Trace, "Adding new data point to NOP stats. OverallNop: {0}, PerPB: {1}. {2}",
                    nopOverall, nopPerPrimeBroker, unsettletNopInfoPoint);

                this.CheckLimitRaw(this._overallSettings, nopStatsLocal.NopOverallStats, nopOverall,
                    limitsCheckingMode == LimitsCheckingMode.CheckWithEvents);
                if (pbStatsPoint != null)
                    this.CheckLimitRaw(this._primeBrokersSettings[primeBrokerIdx], pbStatsPoint,
                        nopPerPrimeBroker, limitsCheckingMode == LimitsCheckingMode.CheckWithEvents);
            }
        }

        private void CheckLimitRaw(NopSettingsPoint nopSettingsPoint,
            NopPerPbStats nopStatsPb, AtomicSize nop, bool invokeEvents)
        {
            UnsettledNopCreditLineLimitsSettingsPoint nopLimitsSettings = nopSettingsPoint.LimitsPerTradeDateTotal;

            CreditZone creditZone =
                nop >= nopLimitsSettings.MaximumUnsettledNopToLockDestination
                    ? (nop >= nopLimitsSettings.MaximumUnsettledNop
                        ? CreditZone.OverTheMaxZone
                        : CreditZone.LockedZone)
                    : (this._isMaxOrderSizeCheckingAllowed &&
                       nop <= nopLimitsSettings.MaximumUnsettledNop - this._maximumAllowedOrderSizeInUsd
                        ? CreditZone.NoCheckingZone
                        : CreditZone.SimpleCheckingZone);

            if (nopStatsPb.SetCreditZoneAndGetIsChange(creditZone) && invokeEvents)
            {
                this._logger.Log(LogLevel.Trace, "PB [{0}] flipped to [{1}] zone (NOP: {2}; LockMax: {3}; Max: {4}).",
                    nopSettingsPoint.DescribeName, creditZone, nop,
                    nopLimitsSettings.MaximumUnsettledNopToLockDestination, nopLimitsSettings.MaximumUnsettledNop);

                _persistedNopProvider.FlipCreditZone(nopSettingsPoint.DatabaseIdNullable, creditZone);
            }
        }

        //Same code for overall and per PB
        private void CheckLimit(NopSettingsPoint nopSettingsPoint,
            NopPerPbStats nopStatsPb)
        {
            if (nopStatsPb == null)
                return;

            this.CheckLimitRaw(
                nopSettingsPoint,
                nopStatsPb,
                AtomicSize.Max(
                    nopStatsPb.ShortUsdNop,
                    nopStatsPb.LongUsdNop),
                    true);
        }

        private void CheckLimits()
        {
            this.CheckLimit(this._overallSettings, this._nopStats.NopOverallStats);

            for (int primeBrokerId = 0; primeBrokerId < _primeBrokersSettings.Length; primeBrokerId++)
            {
                NopSettingsPoint currentPbSettings = this._primeBrokersSettings[primeBrokerId];

                this.CheckLimit(currentPbSettings, this._nopStats.NopStatsPerPb[primeBrokerId]);
            }
        }

        private void RecalculateStatsOnUsdRateChange()
        {
            //make sure only one thread at time executes - so that we can safely use one background structure
            lock (_backgroundRecalculationLocker)
            {
                //background stats are up to date with foreground
                // recalculate the usd conversions for them now
                _backgroundStatsForConcurrentUpdate.RecalculateUsdAggregations(this._usdConversionMultipliers);

                //Exchange to foreground only now - so that foreground stats are always consistent and not relaxed
                _backgroundStatsForConcurrentUpdate = Interlocked.Exchange(ref _nopStats, _backgroundStatsForConcurrentUpdate);

                //no other deals could be added in parallel - as we are in constrained region

                //calculate limits in lock and before copying over - so that the zones are correctly copied to background
                this.CheckLimits();

                //keep background completely up to date
                _backgroundStatsForConcurrentUpdate.CopyUsdRecalculationsFrom(_nopStats);
            }
        }

        private void RecalculateStatsOnRollover()
        {
            //make sure only one thread at time executes - so that we can safely use one background structure
            lock (_backgroundRecalculationLocker)
            {
                //background stats are up to date with foreground - but clear them out to recalc from scratch
                _backgroundStatsForConcurrentUpdate.ClearNopStats();

                //Exchange to foreground only now - so that foreground stats are always consistent and not relaxed
                _backgroundStatsForConcurrentUpdate = Interlocked.Exchange(ref _nopStats,
                    _backgroundStatsForConcurrentUpdate);

                this.CheckLimits();

                //we only need to copy all the no checking zones
                _backgroundStatsForConcurrentUpdate.CopyCreditZonesFrom(this._nopStats);
            }
        }

        //Identical code for overall and per settlemt date info
        public static AtomicSize GetNopIfOrderAdded(NopPerPbStats perPbStats,
            IIntegratorOrderInfo orderInfo, decimal[] usdConversionMultipliers, PriceObjectInternal priceObject)
        {
            AtomicSize newBasePos = perPbStats.NominalPositions[
                (int)orderInfo.Symbol.BaseCurrency] + orderInfo.GetSizeBasePolarized();
            AtomicSize newTermPos = perPbStats.NominalPositions[
                (int)orderInfo.Symbol.TermCurrency] + orderInfo.GetSizeTermPolarized(priceObject == null ? (decimal?)null : priceObject.Price);

            AtomicSize wasBasePos = newBasePos - orderInfo.GetSizeBasePolarized();
            AtomicSize wasTermPos = newTermPos - orderInfo.GetSizeTermPolarized(priceObject == null ? (decimal?)null : priceObject.Price);

            AtomicSize longDiffBase, shortDiffBase, longDiffTerm, shortDiffTerm;
            UnsettledNopCheckingModule.CalculateNominalNopDiff(wasBasePos, newBasePos, out longDiffBase, out shortDiffBase);
            UnsettledNopCheckingModule.CalculateNominalNopDiff(wasTermPos, newTermPos, out longDiffTerm, out shortDiffTerm);

            AtomicSize overalLongDiffUsd = longDiffBase *
                                           usdConversionMultipliers[(int)orderInfo.Symbol.BaseCurrency] +
                                           longDiffTerm *
                                           usdConversionMultipliers[(int)orderInfo.Symbol.TermCurrency];

            AtomicSize overalShortDiffUsd = shortDiffBase *
                                           usdConversionMultipliers[(int)orderInfo.Symbol.BaseCurrency] +
                                           shortDiffTerm *
                                           usdConversionMultipliers[(int)orderInfo.Symbol.TermCurrency];


            AtomicSize newLongNop = perPbStats.LongUsdNop + overalLongDiffUsd;
            AtomicSize newShortNop = perPbStats.ShortUsdNop + overalShortDiffUsd;

            return AtomicSize.Max(newLongNop, newShortNop);
        }

        public static bool CheckIsOutgoingOrderAllowedForGivenStats(
            decimal[] usdConversionMultipliers,
            UnsettledNopCreditLineLimitsSettingsPoint tradeDayLimitsSettings,
            NopPerPbStats perPbStats,
            IIntegratorOrderInfo orderInfo, bool allowBypassLockingMode,
            PriceObjectInternal priceObject)
        {
            CreditZone creditZone = perPbStats.GetCreditZone();
            if (allowBypassLockingMode && creditZone == CreditZone.LockedZone)
                creditZone = CreditZone.SimpleCheckingZone;

            switch (creditZone)
            {
                case CreditZone.NoCheckingZone:
                    return true;
                case CreditZone.SimpleCheckingZone:
                    {
                        //green if nop is under max
                        AtomicSize nopOverallAfterOrder = GetNopIfOrderAdded(perPbStats,
                            orderInfo, usdConversionMultipliers, priceObject);

                        bool allowed = nopOverallAfterOrder <= tradeDayLimitsSettings.MaximumUnsettledNop;
                        return allowed;
                    }
                case CreditZone.LockedZone:
                    {
                        //green if entire nop is decreasing

                        AtomicSize nopOverallAfterOrder = GetNopIfOrderAdded(perPbStats,
                            orderInfo, usdConversionMultipliers, priceObject);

                        AtomicSize nopOverallCurrent = AtomicSize.Max(perPbStats.LongUsdNop, perPbStats.ShortUsdNop);

                        AtomicSize nopIncrease = nopOverallAfterOrder - nopOverallCurrent;

                        bool allowed = nopIncrease <= AtomicSize.ZERO;

                        return allowed;
                    }
                case CreditZone.OverTheMaxZone:
                    {
                        //shortcut for cpts that are blocked by setting Max NOP to 0
                        if (!perPbStats.HasAnyRealizedDeal)
                            return false;

                        //green if decreasing each currency NOP

                        AtomicSize newBasePos = perPbStats.NominalPositions[(int)orderInfo.Symbol.BaseCurrency] 
                            + orderInfo.GetSizeBasePolarized();
                        AtomicSize newTermPos = perPbStats.NominalPositions[(int)orderInfo.Symbol.TermCurrency]
                            + orderInfo.GetSizeTermPolarized(priceObject == null ? (decimal?)null : priceObject.Price);

                        AtomicSize wasBasePos = newBasePos - orderInfo.GetSizeBasePolarized();
                        AtomicSize wasTermPos = newTermPos - orderInfo.GetSizeTermPolarized(priceObject == null ? (decimal?)null : priceObject.Price);


                        bool allowed = newBasePos.IsSameSignAndCloserToZeroThan(wasBasePos) &&
                                       newTermPos.IsSameSignAndCloserToZeroThan(wasTermPos);

                        return allowed;

                        //TODO: should somehow block the positions (and also register for order done, id registrable)
                    }
                default:
                    LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "NetOpenPositionModule: Unknown zone [{0}] - disallowing order info. {1}",
                        creditZone, orderInfo);
                    return false;
            }
        }

        public static bool QuickCheckIsRequestLikelyToPassForGivenStats(
            UnsettledNopCreditLineLimitsSettingsPoint creditLineLimitsSettings,
            NopPerPbStats perPbStats,
            IIntegratorOrderInfo orderInfo)
        {
            switch (perPbStats.GetCreditZone())
            {
                case CreditZone.NoCheckingZone:
                    return true;
                //Following zones are getting close to limit - so fast allow only if clearly decreasing
                case CreditZone.SimpleCheckingZone:
                case CreditZone.LockedZone:
                    return
                        //this looks like we will be decreasing the NOP
                        orderInfo.IntegratorDealDirection == DealDirection.Buy
                            ? perPbStats.NominalPositions[(int)orderInfo.Symbol.BaseCurrency] < AtomicSize.ZERO &&
                              perPbStats.NominalPositions[(int)orderInfo.Symbol.TermCurrency] > AtomicSize.ZERO
                            : perPbStats.NominalPositions[(int)orderInfo.Symbol.BaseCurrency] > AtomicSize.ZERO &&
                              perPbStats.NominalPositions[(int)orderInfo.Symbol.TermCurrency] < AtomicSize.ZERO;
                case CreditZone.OverTheMaxZone:
                default:
                    return false;
            }
        }

        public bool IsTradeRequestLikelyToPass(IIntegratorOrderInfo orderInfo, PriceObjectInternal priceObject)
        {
            NopStatsHolder nopStats = this._nopStats;

            bool isAllowedForOverallNop = QuickCheckIsRequestLikelyToPassForGivenStats(
                this._overallSettings.LimitsPerTradeDateTotal,
                nopStats.NopOverallStats, orderInfo);

            //shortcut
            if (!isAllowedForOverallNop
                &&
                //perform full check only if quick check failed
                !CheckIsOutgoingOrderAllowedForGivenStats(
                _usdConversionMultipliers, this._overallSettings.LimitsPerTradeDateTotal,
                nopStats.NopOverallStats,
                orderInfo, false, priceObject))
                return false;

            Counterparty cpt = priceObject == null ? orderInfo.Counterparty : priceObject.Counterparty;
            int primeBrokerIdx = _primeBrokersMap[(int)cpt];
            if (primeBrokerIdx < 0)
            {
                return true;
            }


            return QuickCheckIsRequestLikelyToPassForGivenStats(
                this._primeBrokersSettings[primeBrokerIdx].LimitsPerTradeDateTotal,
                nopStats.NopStatsPerPb[primeBrokerIdx], orderInfo)
                   ||
                   //perform full check only if quick check failed
                   CheckIsOutgoingOrderAllowedForGivenStats(
                       _usdConversionMultipliers,
                       this._primeBrokersSettings[primeBrokerIdx].LimitsPerTradeDateTotal,
                       nopStats.NopStatsPerPb[primeBrokerIdx], orderInfo, false, priceObject);
        }

        public bool CheckIsOutgoingIncommingOrderInfoAllowed(IIntegratorOrderInfo orderInfo)
        {
            try
            {
                return this.CheckIsOutgoingOrderAllowed(this._nopStats, orderInfo,
                orderInfo.IntegratorOrderInfoType == IntegratorOrderInfoType.ExternalClientDealRequest);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e,
                    "Order market as disallowed as there was exception during unsettled NOP checking (risk-remove flag can flip this decision)",
                    orderInfo);
                return orderInfo.IsRiskRemovingOrder;
            }
        }

        private bool CheckIsOutgoingOrderAllowed(NopStatsHolder nopStatsLocal, IIntegratorOrderInfo orderInfo, bool allowBypassLockingMode)
        {
            //case of internal bankpool order
            if (orderInfo.Counterparty == Counterparty.NULL)
                return true;

            bool isAllowedForOverallNop = CheckIsOutgoingOrderAllowedForGivenStats(
                _usdConversionMultipliers, this._overallSettings.LimitsPerTradeDateTotal,
                nopStatsLocal.NopOverallStats,
                orderInfo, allowBypassLockingMode, null);

            //shortcut
            if (!isAllowedForOverallNop)
                return false;

            int primeBrokerIdx = _primeBrokersMap[(int)orderInfo.Counterparty];
            if (primeBrokerIdx < 0)
            {
                this._logger.Log(LogLevel.Fatal, "Attempt to check unsettled NOP for counterparty ({0}) that has no credit line mapping set. No unsettled NOP checking will be performed",
                    orderInfo.Counterparty);
                return true;
            }

            return CheckIsOutgoingOrderAllowedForGivenStats(
                _usdConversionMultipliers, this._primeBrokersSettings[primeBrokerIdx].LimitsPerTradeDateTotal,
                nopStatsLocal.NopStatsPerPb[primeBrokerIdx],
                orderInfo, allowBypassLockingMode, null);
        }

        public void AddNewDealToStats(IntegratorDealDone integratorDealDone)
        {
            lock (_backgroundRecalculationLocker)
            {
                //update stats in background (to avoid problem of non-atomic update and avoid need for locking order checks to prevent inconsistent state)
                this.AddDataPointToStats(this._backgroundStatsForConcurrentUpdate, integratorDealDone, LimitsCheckingMode.CheckWithEvents);

                //Exchange to foreground only now - so that foreground stats are always consistent and not relaxed
                _backgroundStatsForConcurrentUpdate = Interlocked.Exchange(ref _nopStats, _backgroundStatsForConcurrentUpdate);

                //foreground and background are swapped - but keep background up to date (for next update)
                this.AddDataPointToStats(this._backgroundStatsForConcurrentUpdate, integratorDealDone, LimitsCheckingMode.CheckWithoutEvents);
            }
        }

        public void RemoveDealFromStats(IntegratorDealDone integratorDealDone)
        {
            lock (_backgroundRecalculationLocker)
            {
                integratorDealDone.Invert();

                //update stats in background (to avoid problem of non-atomic update and avoid need for locking order checks to prevent inconsistent state)
                this.AddDataPointToStats(this._backgroundStatsForConcurrentUpdate, integratorDealDone, LimitsCheckingMode.CheckWithEvents);

                //Exchange to foreground only now - so that foreground stats are always consistent and not relaxed
                _backgroundStatsForConcurrentUpdate = Interlocked.Exchange(ref _nopStats, _backgroundStatsForConcurrentUpdate);

                //foreground and background are swapped - but keep background up to date (for next update)
                this.AddDataPointToStats(this._backgroundStatsForConcurrentUpdate, integratorDealDone, LimitsCheckingMode.CheckWithoutEvents);
            }
        }

    }
}
