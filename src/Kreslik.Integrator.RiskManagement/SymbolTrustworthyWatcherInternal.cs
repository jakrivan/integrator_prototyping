﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.RiskManagement
{
    internal class SymbolTrustworthyWatcherInternal : IPriceChangesConsumer, ISymbolTrustworthinessInfoProvider
    {
        #region IPriceChangesConsumer implementation

        public void OnNewPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            if(price != null && price.Counterparty == Counterparty.L01)
                _trustworthyWatchers[(int)price.Symbol].OnNewPrice(price);
        }

        public void OnIgnoredPrice(PriceObjectInternal price, ref bool heldWithoutAcquireCall)
        {
            //nothing as ignored prices are not consumed
        }

        public void OnNewNonExecutablePrice(PriceObjectInternal price)
        {
            //nothing
        }

        public void OnQuoteCancel(Symbol symbol, Counterparty counterparty)
        {
            if(counterparty != Counterparty.L01)
                return;

            _trustworthyWatchers[(int)symbol].OnQuoteCancel();
        }

        public void OnPriceCancel(Symbol symbol, Counterparty counterparty, PriceSide side)
        {
            if (counterparty != Counterparty.L01)
                return;

            _trustworthyWatchers[(int)symbol].OnPriceCancel(side);
        }

        public void OnPriceInvalidate(Symbol symbol, PriceSide side, Counterparty counterparty, Guid integratorPriceIdnetity, DateTime integratorPriceReceivedUtc)
        {
            //Do nothing - we do not know if quote was already replaced or not
            //also trustworthy watchers do not handle this as almost each rejection would lead to untrustworthiness
        }

        public bool ConsumesDataFromCounterparty(Counterparty counterparty)
        {
            return counterparty == Counterparty.L01;
        }

        public bool ConsumesMDDataIfOFNotRunning(Counterparty counterparty)
        {
            return false;
        }

        public bool ConsumesIgnoredPrices
        {
            get { return false; }
        }
        public bool ConsumesNonExecutablePrices
        {
            get { return false; }
        }

        #endregion IPriceChangesConsumer implementation

        #region ISymbolTrustworthinessInfoProvider implementation
        public List<SymbolTrustworthinessInfo> SymbolTrustworthinessInfo { get { return _symbolTrustworthinessInfo; } }

        #endregion ISymbolTrustworthinessInfoProvider implementation

        private TimeSpan _minUntrustworthyPeriod;
        private SafeTimer _periodicCheckTimer;
        private ITrustworthyWatcherSettingsProvider _settingsProvider;
        private readonly List<SymbolTrustworthinessInfo> _symbolTrustworthinessInfo =
            Symbol.Values.Select(s => new SymbolTrustworthinessInfo()).ToList();
        private readonly SingleSymbolTrustworthyWatcherInternalUnsynchronized[] _trustworthyWatchers;

        public SymbolTrustworthyWatcherInternal(ITrustworthyWatcherSettingsProvider settingsProvider,
            ILogger logger, IPriceStreamMonitor priceStreamMonitor, IPriceDeviationCheckState priceDeviationCheckState)
        {
            this._settingsProvider = settingsProvider;
            this._periodicCheckTimer = new SafeTimer(PeriodicCheck)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo100ms,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Normal
            };

            this._trustworthyWatchers =
                Symbol.Values.Select(
                    s =>
                        new SingleSymbolTrustworthyWatcherInternalUnsynchronized(s, logger,
                            this._symbolTrustworthinessInfo[(int)s], priceDeviationCheckState)).ToArray();

            (priceStreamMonitor as IBroadcastInfosStore).NewBroadcastInfo += info =>
            {
                if (info is SymbolExchangeRatesArgs)
                {
                    SymbolExchangeRatesArgs symbolExchangeRates = info as SymbolExchangeRatesArgs;

                    for (int symbolIdx = 0; symbolIdx < Symbol.ValuesCount; symbolIdx++)
                    {
                        _trustworthyWatchers[symbolIdx].UpdateBpConversion(
                            symbolExchangeRates.SymbolExchangeRates[symbolIdx]);
                    }
                }
            };
        }

        public void UpdateSettings(bool checkAllowed, TimeSpan minUntrustworthyPeriod)
        {
            this._minUntrustworthyPeriod = minUntrustworthyPeriod;
            //this._checkAllowed = checkAllowed;

            if (!checkAllowed)
            {
                this._periodicCheckTimer.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
                //possible events or checkc should run to completion to prevent interleaved flipping of state
                Thread.Sleep(20);
                foreach (var watcher in _trustworthyWatchers)
                {
                    watcher.DisableWatching();
                }
            }
            else
            {
                this._periodicCheckTimer.Change(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));

                IList<ITrustworthyWatcherSettings> settingsList = _settingsProvider.GetSettings();
                foreach (Symbol symbol in Symbol.Values)
                {
                    ITrustworthyWatcherSettings setting = settingsList.FirstOrDefault(sett => sett.Symbol == symbol) ??
                                                          _settingsProvider.GetEmptySettings(symbol);

                    this._trustworthyWatchers[(int)symbol].UpdateSettings(setting);
                }
            }
        }

        private void PeriodicCheck()
        {
            DateTime utcNow = HighResolutionDateTime.UtcNow;
            foreach (var watcher in _trustworthyWatchers)
            {
                watcher.PerformPeriodicCheck(utcNow, this._minUntrustworthyPeriod);
            }
        }

        public ISymbolTrustworthyWatcher GetSymbolTrustworthyWatcher(Symbol symbol)
        {
            return this._trustworthyWatchers[(int) symbol];
        }


        private class SingleSymbolTrustworthyWatcherInternalUnsynchronized : ISymbolTrustworthyWatcher
        {
            private AtomicDecimal _lastAsk = AtomicDecimal.Zero;
            private AtomicDecimal _lastBid = AtomicDecimal.Zero;
            private DateTime _lastAskUpdated = DateTime.MinValue;
            private DateTime _lastBidUpdated = DateTime.MinValue;
            private decimal _maxAllowedSpreadBp = 0m;
            private decimal _minAllowedSpreadBp = 0m;
            private AtomicDecimal _maxAllowedSpread = AtomicDecimal.Zero;
            private AtomicDecimal _minAllowedSpreadInverted = AtomicDecimal.Zero;
            private TimeSpan _maxAllowedTickAge = TimeSpan.Zero;
            private DateTime _untrustedFrom = DateTime.MinValue;
            private bool _isTrustworthy = false;
            private bool _isWatchingEnabled = false;
            private Symbol _symbol;
            private ILogger _logger;
            private IPriceDeviationCheckState _priceDeviationCheckState;
            private SymbolTrustworthinessInfo _symbolTrustworthinessInfo;

            public SingleSymbolTrustworthyWatcherInternalUnsynchronized(Symbol symbol, ILogger logger, SymbolTrustworthinessInfo symbolTrustworthinessInfo, IPriceDeviationCheckState priceDeviationCheckState)
            {
                this._logger = logger;
                this._isWatchingEnabled = false;
                this._symbol = symbol;
                this._priceDeviationCheckState = priceDeviationCheckState;
                this._symbolTrustworthinessInfo = symbolTrustworthinessInfo;
                //this will not invoke events, as backing field is already false
                this.SetTrustworthyState(false, SymbolTrustState.Untrusted_Initial);
            }

            internal void OnNewPrice(PriceObjectInternal price)
            {
                if (!_isWatchingEnabled)
                    return;

                if (price.Side == PriceSide.Ask)
                {
                    this._lastAsk = price.ConvertedPrice;
                    this._lastAskUpdated = price.IntegratorReceivedTimeUtc;
                }
                else
                {
                    this._lastBid = price.ConvertedPrice;
                    this._lastBidUpdated = price.IntegratorReceivedTimeUtc;
                }

                
                //LMAX specific: lmax sends all bids and then all asks in one quote; we want to evaluate only for current quote 
                if (price.Side == PriceSide.Ask)
                {
                    this.CheckSpread();
                }
            }

            private void CheckSpread()
            {
                // There is not much added perf gain, as those usually leads to threee more comparison evaluations
                //     and subtraction is perfwise same as comparison
                //if (//only if set, then there is a chance to change the state
                //    _maxAllowedSpread != AtomicDecimal.Zero && _minAllowedSpreadInverted != AtomicDecimal.Zero && _lastAsk != _lastBid)
                //{
                    SymbolTrustState symbolTrustState = SymbolTrustState.Trusted;

                    //std spread
                    if (_lastAsk > _lastBid)
                    {
                        if (_lastAsk - _lastBid > _maxAllowedSpread)
                            symbolTrustState = SymbolTrustState.Untrusted_SpreadOverMax;
                    }
                    //inverted spread
                    else
                    {
                        if (_lastBid - _lastAsk > _minAllowedSpreadInverted)
                            symbolTrustState = SymbolTrustState.Untrusted_SpreadUnderMin;
                    }

                    //update flags allways if not trustworthy, otherwise leave the change on timer
                    if (symbolTrustState != SymbolTrustState.Trusted)
                    {
                        this.SetTrustworthyState(false, symbolTrustState);
                    }
                //}
            }

            internal void OnQuoteCancel()
            {
                if (!_isWatchingEnabled)
                    return;

                this._lastAskUpdated = DateTime.MinValue;
                this._lastBidUpdated = DateTime.MinValue;

                this.SetTrustworthyState(false, SymbolTrustState.Untrusted_PriceCancelled);
            }

            internal void OnPriceCancel(PriceSide side)
            {
                if (!_isWatchingEnabled)
                    return;

                if (side == PriceSide.Ask)
                {
                    this._lastAskUpdated = DateTime.MinValue;
                }
                else
                {
                    this._lastBidUpdated = DateTime.MinValue;
                }

                this.SetTrustworthyState(false, SymbolTrustState.Untrusted_PriceCancelled);
            }

            internal void PerformPeriodicCheck(DateTime checkTime, TimeSpan untrustworthyPeriod)
            {
                if (!_isWatchingEnabled)
                    return;

                DateTime minRequiredUpdateTime = checkTime - _maxAllowedTickAge;
                if (_lastAskUpdated < minRequiredUpdateTime || _lastBidUpdated < minRequiredUpdateTime)
                {
                    this.SetTrustworthyState(false, SymbolTrustState.Untrusted_PriceTooOld);
                }
                else if (!this.IsTrustworthy)
                {
                    //first recheck spread, it might stay in untrusted state for longer time - then just prolong untrustworthiness
                    this.CheckSpread();

                    if (_untrustedFrom + untrustworthyPeriod < checkTime)
                    {
                        if (!_priceDeviationCheckState.GetIsInitialized(_symbol))
                            this.SetTrustworthyState(false, SymbolTrustState.Untrusted_DeviationCheckNotInitialized);
                        else if (TradingHoursHelper.Instance.IsInRollover(this._symbol, checkTime))
                            this.SetTrustworthyState(false, SymbolTrustState.Untrusted_Rollover);
                        else
                            this.SetTrustworthyState(true, SymbolTrustState.Trusted);
                    }
                }
            }

            internal void UpdateSettings(ITrustworthyWatcherSettings newSettings)
            {
                this._isWatchingEnabled = newSettings.AreSettingsComplete;
                if (!newSettings.AreSettingsComplete)
                    this.SetTrustworthyState(false, SymbolTrustState.Untrusted_NotConfigured);
                this._maxAllowedSpreadBp = newSettings.MaxAllowedSpreadBp;
                this._symbolTrustworthinessInfo.MaxTrustedSpreadBp = newSettings.MaxAllowedSpreadBp;
                this._minAllowedSpreadBp = newSettings.MinAllowedSpreadBp;
                this._maxAllowedSpread = newSettings.MaxAllowedSpread;
                this._symbolTrustworthinessInfo.MaxTrustedSpread = newSettings.MaxAllowedSpread.ToDecimal();
                this._minAllowedSpreadInverted = newSettings.MinAllowedSpreadInverted;
                this._maxAllowedTickAge = newSettings.MaxAllowedTickAge;
            }

            internal void UpdateBpConversion(decimal referencePrice)
            {
                _maxAllowedSpread = AtomicDecimal.ConvertToAtomicDecimalCuttingExcessScale(_maxAllowedSpreadBp * referencePrice / 10000m);
                this._symbolTrustworthinessInfo.MaxTrustedSpread = _maxAllowedSpread.ToDecimal();
                _minAllowedSpreadInverted = AtomicDecimal.ConvertToAtomicDecimalCuttingExcessScale(-_minAllowedSpreadBp * referencePrice / 10000m);
            }

            internal void DisableWatching()
            {
                this._isWatchingEnabled = false;
                this.SetTrustworthyState(true, SymbolTrustState.Trusted_CheckTurnedOff);
            }

            private void SetTrustworthyState(bool isTrustworthy, SymbolTrustState trustState)
            {
                if (!isTrustworthy)
                    _untrustedFrom = HighResolutionDateTime.UtcNow;
                if (this._isTrustworthy != isTrustworthy)
                {
                    this._isTrustworthy = isTrustworthy;
                    this._logger.Log(LogLevel.Info, "Changing {0} trustworthiness to {1}. {2}", _symbol, isTrustworthy, trustState);
                    if (TrustworthynessChanged != null)
                        TrustworthynessChanged(isTrustworthy);
                }
                this._symbolTrustworthinessInfo.SetTrustState(isTrustworthy, trustState);
                this._isTrustworthy = isTrustworthy;
            }

            public bool IsTrustworthy
            {
                get
                {
                    return this._isTrustworthy;
                }
            }

            public event Action<bool> TrustworthynessChanged;
        }
    }
}
