﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.MessageBus;

namespace Kreslik.Integrator.DiagnosticsDashboard
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            var initialSettings = GetSettings(args);
            if (initialSettings != null)
            {
                Application.Run(new Form1(initialSettings));
            }
            else
            {
                MessageBox.Show("You haven't chosen any settings to be used, so application is closing now.");
            }
        }

        private static DiagnosticsDashboardSettings GetSettings(string[] args)
        {
            string settingName = null;
            DiagnosticsDashboardSettings diagnosticsDashboardSettings = null;
            bool close = false;

            if (args.Length == 1)
            {
                if (args[0] == "/?" || args[0] == "/help" || args[0] == "-?" || args[0] == "-help" || args[0] == "?" ||
                    args[0] == "help")
                    DisplayHelp();
                else
                    settingName = args[0];
            }
            else if(args.Length > 1)
            {
                DisplayHelp();
            }

            do
            {
                if (string.IsNullOrEmpty(settingName))
                {
                    using (
                        var loadDialog =
                            new LoadBackendSettingsDialog(
                                new CounterpartyMonitorSettingsHelper().GetCurrentCMonitorSettingsFriendlyName(
                                    SettingsInitializator.Instance.EnvironmentName), null))
                    {
                        loadDialog.ShowDialog();

                        settingName = loadDialog.SettingName;
                        close = !loadDialog.SettingChosen;
                    }
                }

                if (!close)
                {
                    try
                    {
                        diagnosticsDashboardSettings = DiagnosticsDashboardSettings.DeserializeConfigurationFromString(
                                new CounterpartyMonitorSettingsHelper().GetSettingsXmlString(settingName), settingName);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(
                            string.Format(
                                "Problem during loading setting from backend (code: {0}). Press OK and try again",
                                e.GetType()), "Warning", MessageBoxButtons.OK);
                        settingName = null;
                    }
                }

            } while (diagnosticsDashboardSettings == null && !close);

            return diagnosticsDashboardSettings;
        }

        private static void DisplayHelp()
        {
            MessageBox.Show(
                "You can start C-monitor with no arguments and choose initial settings during startup or with one single argument which is a name of desired setting. Enclose the setting name in quotes if it is a multi word name");
        }
    }
}
