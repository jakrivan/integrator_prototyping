﻿namespace Kreslik.Integrator.DiagnosticsDashboard
{
    partial class SessionActionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.grpBxSessionSelection = new System.Windows.Forms.GroupBox();
            this.comboSessionSelection = new System.Windows.Forms.ComboBox();
            this.chckSessionSelection = new System.Windows.Forms.CheckBox();
            this.grpBxSessionSelection.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(22, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select action for session";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(186, 65);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(29, 13);
            this.linkLabel1.TabIndex = 1;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Stop";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(186, 92);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(29, 13);
            this.linkLabel2.TabIndex = 2;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Start";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Location = new System.Drawing.Point(186, 118);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(41, 13);
            this.linkLabel3.TabIndex = 3;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "Restart";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // grpBxSessionSelection
            // 
            this.grpBxSessionSelection.Controls.Add(this.comboSessionSelection);
            this.grpBxSessionSelection.Location = new System.Drawing.Point(25, 65);
            this.grpBxSessionSelection.Name = "grpBxSessionSelection";
            this.grpBxSessionSelection.Size = new System.Drawing.Size(135, 52);
            this.grpBxSessionSelection.TabIndex = 4;
            this.grpBxSessionSelection.TabStop = false;
            // 
            // comboSessionSelection
            // 
            this.comboSessionSelection.FormattingEnabled = true;
            this.comboSessionSelection.Items.AddRange(new object[] {
            "Both",
            "Quotes",
            "Orders"});
            this.comboSessionSelection.Location = new System.Drawing.Point(6, 20);
            this.comboSessionSelection.Name = "comboSessionSelection";
            this.comboSessionSelection.Size = new System.Drawing.Size(110, 21);
            this.comboSessionSelection.TabIndex = 6;
            // 
            // chckSessionSelection
            // 
            this.chckSessionSelection.AutoSize = true;
            this.chckSessionSelection.Location = new System.Drawing.Point(32, 65);
            this.chckSessionSelection.Name = "chckSessionSelection";
            this.chckSessionSelection.Size = new System.Drawing.Size(110, 17);
            this.chckSessionSelection.TabIndex = 5;
            this.chckSessionSelection.Text = "Session Selection";
            this.chckSessionSelection.UseVisualStyleBackColor = true;
            this.chckSessionSelection.CheckedChanged += new System.EventHandler(this.chckSessionSelection_CheckedChanged);
            // 
            // SessionActionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 170);
            this.Controls.Add(this.chckSessionSelection);
            this.Controls.Add(this.grpBxSessionSelection);
            this.Controls.Add(this.linkLabel3);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label1);
            this.Name = "SessionActionForm";
            this.Text = "SessionAction";
            this.grpBxSessionSelection.ResumeLayout(false);
            this.grpBxSessionSelection.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.GroupBox grpBxSessionSelection;
        private System.Windows.Forms.ComboBox comboSessionSelection;
        private System.Windows.Forms.CheckBox chckSessionSelection;
    }
}