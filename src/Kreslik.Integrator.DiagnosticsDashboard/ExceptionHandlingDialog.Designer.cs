﻿namespace Kreslik.Integrator.DiagnosticsDashboard
{
    partial class ExceptionHandlingDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeader = new System.Windows.Forms.Label();
            this.btnDetails = new System.Windows.Forms.Button();
            this.btnEmail = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btnCancelCountDown = new System.Windows.Forms.Button();
            this.lblCountDown = new System.Windows.Forms.Label();
            this.lblCountdownValue = new System.Windows.Forms.Label();
            this.btnCloseApp = new System.Windows.Forms.Button();
            this.btnRestartApp = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Location = new System.Drawing.Point(24, 31);
            this.lblHeader.MaximumSize = new System.Drawing.Size(584, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(359, 13);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.Text = "Application experienced unexpected error (code: xx), and cannot continue.";
            // 
            // btnDetails
            // 
            this.btnDetails.Location = new System.Drawing.Point(27, 68);
            this.btnDetails.Name = "btnDetails";
            this.btnDetails.Size = new System.Drawing.Size(75, 23);
            this.btnDetails.TabIndex = 1;
            this.btnDetails.Text = "Details >>>";
            this.btnDetails.UseVisualStyleBackColor = true;
            this.btnDetails.Click += new System.EventHandler(this.btnDetails_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.Location = new System.Drawing.Point(426, 174);
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(142, 23);
            this.btnEmail.TabIndex = 2;
            this.btnEmail.Text = "Email To KGT Support";
            this.btnEmail.UseVisualStyleBackColor = true;
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.richTextBox1);
            this.panel1.Controls.Add(this.btnEmail);
            this.panel1.Location = new System.Drawing.Point(27, 98);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(575, 204);
            this.panel1.TabIndex = 3;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(3, 3);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(565, 165);
            this.richTextBox1.TabIndex = 4;
            this.richTextBox1.Text = "";
            // 
            // btnCancelCountDown
            // 
            this.btnCancelCountDown.Location = new System.Drawing.Point(325, 366);
            this.btnCancelCountDown.Name = "btnCancelCountDown";
            this.btnCancelCountDown.Size = new System.Drawing.Size(115, 23);
            this.btnCancelCountDown.TabIndex = 4;
            this.btnCancelCountDown.Text = "Cancel CountDown";
            this.btnCancelCountDown.UseVisualStyleBackColor = true;
            this.btnCancelCountDown.Click += new System.EventHandler(this.btnCancelCountDown_Click);
            // 
            // lblCountDown
            // 
            this.lblCountDown.AutoSize = true;
            this.lblCountDown.Location = new System.Drawing.Point(27, 336);
            this.lblCountDown.Name = "lblCountDown";
            this.lblCountDown.Size = new System.Drawing.Size(273, 13);
            this.lblCountDown.TabIndex = 5;
            this.lblCountDown.Text = "If no action will be taken application will autorestart after ";
            // 
            // lblCountdownValue
            // 
            this.lblCountdownValue.AutoSize = true;
            this.lblCountdownValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblCountdownValue.Location = new System.Drawing.Point(297, 337);
            this.lblCountdownValue.Name = "lblCountdownValue";
            this.lblCountdownValue.Size = new System.Drawing.Size(72, 13);
            this.lblCountdownValue.TabIndex = 6;
            this.lblCountdownValue.Text = "20 seconds";
            // 
            // btnCloseApp
            // 
            this.btnCloseApp.Location = new System.Drawing.Point(447, 366);
            this.btnCloseApp.Name = "btnCloseApp";
            this.btnCloseApp.Size = new System.Drawing.Size(75, 23);
            this.btnCloseApp.TabIndex = 7;
            this.btnCloseApp.Text = "Close App";
            this.btnCloseApp.UseVisualStyleBackColor = true;
            this.btnCloseApp.Click += new System.EventHandler(this.btnCloseApp_Click);
            // 
            // btnRestartApp
            // 
            this.btnRestartApp.Location = new System.Drawing.Point(528, 366);
            this.btnRestartApp.Name = "btnRestartApp";
            this.btnRestartApp.Size = new System.Drawing.Size(75, 23);
            this.btnRestartApp.TabIndex = 8;
            this.btnRestartApp.Text = "Restart App";
            this.btnRestartApp.UseVisualStyleBackColor = true;
            this.btnRestartApp.Click += new System.EventHandler(this.btnRestartApp_Click);
            // 
            // ExceptionHandlingDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 398);
            this.Controls.Add(this.btnRestartApp);
            this.Controls.Add(this.btnCloseApp);
            this.Controls.Add(this.lblCountdownValue);
            this.Controls.Add(this.lblCountDown);
            this.Controls.Add(this.btnCancelCountDown);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnDetails);
            this.Controls.Add(this.lblHeader);
            this.Name = "ExceptionHandlingDialog";
            this.Text = "Unexpected Error Occured";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Button btnDetails;
        private System.Windows.Forms.Button btnEmail;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCancelCountDown;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label lblCountDown;
        private System.Windows.Forms.Label lblCountdownValue;
        private System.Windows.Forms.Button btnCloseApp;
        private System.Windows.Forms.Button btnRestartApp;
    }
}