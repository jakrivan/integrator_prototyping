﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.DiagnosticsDashboard
{
    public partial class ExceptionHandlingDialog : Form
    {
        public ExceptionHandlingDialog(Exception ex, bool autorestartEnabled, Form centerToForm)
        {
            _exception = ex;
            InitializeComponent();
            AplicationActionRequested = AplicationActionRequest.Restart;
            DrawInitialUI(_exception, autorestartEnabled);

            this.PositionOnCenterOfForm(centerToForm);
        }

        public enum AplicationActionRequest
        {
            Close,
            Restart,
            None
        }

        public AplicationActionRequest AplicationActionRequested { get; private set; }

        private void DrawInitialUI(Exception e, bool autorestartEnabled)
        {
            lblHeader.Text = string.Format(
                "Application experienced unexpected error (code: {0}), and cannot continue.",
                _exception.GetType().ToString());
            richTextBox1.Text = _exception.ToString();
            

            _detailsShown = false;
            RefreshUi(_detailsShown);

            if (autorestartEnabled)
            {
                Task.Factory.StartNew(() =>
                    {
                        for (int i = _initialCountDownSeconds; i > 0; i--)
                        {
                            Thread.Sleep(TimeSpan.FromSeconds(1));
                            if (_countDownCancelled)
                                return;
                            //System.Console.Beep();
                            lblCountdownValue.Invoke(
                                (MethodInvoker) delegate { lblCountdownValue.Text = i + " seconds"; });
                        }

                        AplicationActionRequested = AplicationActionRequest.Restart;
                        this.Invoke((MethodInvoker) this.Close);
                    });
            }
            else
            {
                CancelCountDown();
            }
        }

        private void CancelCountDown()
        {
            _countDownCancelled = true;
            lblCountDown.Visible = false;
            lblCountdownValue.Visible = false;
            btnCancelCountDown.Visible = false;
        }

        private Exception _exception;
        private int _initialCountDownSeconds = 19;
        private bool _detailsShown = true;
        private bool _countDownCancelled = false;
        private int _detailsPanelExpandedHeight = 204;

        private void MoveYAxisLocation(Control controlToMove, int yAxisIncrement)
        {
            controlToMove.Location = new Point(controlToMove.Location.X, controlToMove.Location.Y + yAxisIncrement);
        }

        private void RefreshUi(bool detailsPanelShown)
        {
            if (detailsPanelShown)
            {
                btnDetails.Text = "Details <<<";
                panel1.Height = _detailsPanelExpandedHeight;
                this.Height += _detailsPanelExpandedHeight;

                MoveYAxisLocation(this.lblCountDown, _detailsPanelExpandedHeight);
                MoveYAxisLocation(this.lblCountdownValue, _detailsPanelExpandedHeight);
                MoveYAxisLocation(this.btnCancelCountDown, _detailsPanelExpandedHeight);
                MoveYAxisLocation(this.btnCloseApp, _detailsPanelExpandedHeight);
                MoveYAxisLocation(this.btnRestartApp, _detailsPanelExpandedHeight);
            }
            else
            {
                btnDetails.Text = "Details >>>";
                panel1.Height = 0;
                this.Height -= _detailsPanelExpandedHeight;

                MoveYAxisLocation(this.lblCountDown, -_detailsPanelExpandedHeight);
                MoveYAxisLocation(this.lblCountdownValue, -_detailsPanelExpandedHeight);
                MoveYAxisLocation(this.btnCancelCountDown, -_detailsPanelExpandedHeight);
                MoveYAxisLocation(this.btnCloseApp, -_detailsPanelExpandedHeight);
                MoveYAxisLocation(this.btnRestartApp, -_detailsPanelExpandedHeight);
            }
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            _detailsShown = !_detailsShown;
            RefreshUi(_detailsShown);
        }

        private void btnCancelCountDown_Click(object sender, EventArgs e)
        {
            CancelCountDown();
        }

        private void btnCloseApp_Click(object sender, EventArgs e)
        {
            AplicationActionRequested = AplicationActionRequest.Close;
            this.Close();
        }

        private void btnRestartApp_Click(object sender, EventArgs e)
        {
            AplicationActionRequested = AplicationActionRequest.Restart;
            this.Close();
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            CancelCountDown();

            if (
                this.ShowCenteredMessageBox("Do you really want to sent error details to KGT support?", "Confirmation",
                                            MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                AutoMailer.TrySendMailAsync("New C-Monitor Error Report", _exception.ToString(), false, CommonSettings.CommonBehavior.Logging.FatalErrorsEmailTo.Split(new char[] { ';' }),
                     CommonSettings.CommonBehavior.Logging.FatalErrorsEmailCc.Split(new char[] { ';' }), true);
            }
        }
    }
}
