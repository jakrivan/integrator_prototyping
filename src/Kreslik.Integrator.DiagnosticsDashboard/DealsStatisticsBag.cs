﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.DiagnosticsDashboard
{
    public class DealsStatisticsBag
    {
        public DealsStatisticsBag()
        {
            this.Clear();
        }

        public void Clear()
        {
            this.OveralNumberOfDealsToday = 0;
            this.OveralNumberOfKGTRejectionsToday = 0;
            this.OveralNumberOfCtpRejectionsToday = 0;
            this.OveralKGTRejectionRateToday = 0m;
            this.OveralCtpRejectionRateToday = 0m;
            this.OveralVolumeToday = 0m;
            this.OveralAverageDealSizeToday = 0m;
            this.AverageQuoteToOrderLatencyToday = TimeSpan.Zero;
            this.AverageOrderToDealLatencyToday = TimeSpan.Zero;

            this.NumberOfDealsPerCounterpartyToday = Enumerable.Repeat(0, Counterparty.ValuesCount).ToList();
            this.NumberOfKGTRejectionsPerCounterpartyToday = Enumerable.Repeat(0, Counterparty.ValuesCount).ToList();
            this.NumberOfCtpRejectionsPerCounterpartyToday = Enumerable.Repeat(0, Counterparty.ValuesCount).ToList();
            this.KGTRejectionRatePerCounterpartyToday = Enumerable.Repeat(0m, Counterparty.ValuesCount).ToList();
            this.CtpRejectionRatePerCounterpartyToday = Enumerable.Repeat(0m, Counterparty.ValuesCount).ToList();
            this.TotalVolumePerCounterpartyToday = Enumerable.Repeat(0m, Counterparty.ValuesCount).ToList();
            this.VolumeSharePerCounterpartyToday = Enumerable.Repeat(0m, Counterparty.ValuesCount).ToList();
            this.AverageDealSizePerCounterpartyToday = Enumerable.Repeat(0m, Counterparty.ValuesCount).ToList();
            this.AverageQuoteToOrderLatencyPerCounterpartyToday = Enumerable.Repeat(TimeSpan.Zero, Counterparty.ValuesCount).ToList();
            this.AverageOrderToDealLatencyPerCounterpartyToday = Enumerable.Repeat(TimeSpan.Zero, Counterparty.ValuesCount).ToList();
        }

        private void CopyList<T>(List<T> from, List<T> to)
        {
            for (int i = 0; i < Math.Max(from.Count, to.Count); i++)
            {
                to[i] = from[i];
            }
        }

        
        public List<int> NumberOfDealsPerCounterpartyToday { get; set; }
        
        public int OveralNumberOfDealsToday { get; set; }
        
        public List<int> NumberOfKGTRejectionsPerCounterpartyToday { get; set; }
        public List<int> NumberOfCtpRejectionsPerCounterpartyToday { get; set; }
        
        public int OveralNumberOfKGTRejectionsToday { get; set; }
        public int OveralNumberOfCtpRejectionsToday { get; set; }
        
        public List<decimal> KGTRejectionRatePerCounterpartyToday { get; set; }
        public List<decimal> CtpRejectionRatePerCounterpartyToday { get; set; }
        
        public decimal OveralKGTRejectionRateToday { get; set; }
        public decimal OveralCtpRejectionRateToday { get; set; }
        
        public List<decimal> TotalVolumePerCounterpartyToday { get; set; }
        
        public decimal OveralVolumeToday { get; set; }
        
        public List<decimal> VolumeSharePerCounterpartyToday { get; set; }
        
        public List<decimal> AverageDealSizePerCounterpartyToday { get; set; }
        
        public decimal OveralAverageDealSizeToday { get; set; }
        
        public List<TimeSpan> AverageQuoteToOrderLatencyPerCounterpartyToday { get; set; }

        public TimeSpan AverageQuoteToOrderLatencyToday { get; set; }

        public List<TimeSpan> AverageOrderToDealLatencyPerCounterpartyToday { get; set; }

        public TimeSpan AverageOrderToDealLatencyToday { get; set; }
    }
}
