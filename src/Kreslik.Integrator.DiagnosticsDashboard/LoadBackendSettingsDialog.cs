﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.DiagnosticsDashboard
{
    public partial class LoadBackendSettingsDialog : Form
    {
        public LoadBackendSettingsDialog(string originalSettingName, Form centerToForm)
        {
            this._originalSettingName = originalSettingName;
            InitializeComponent();
            _settingsList = new DAL.CounterpartyMonitorSettingsHelper().GetSettingsList();
            InitializeGrid();
            this.PositionOnCenterOfForm(centerToForm);
        }

        private List<SettingsDetail> _settingsList;
        private string _originalSettingName;

        private void InitializeGrid()
        {
            ////Create instance for datagrid view link column class
            //DataGridViewLinkColumn linkColumn = new DataGridViewLinkColumn();
            //linkColumn.UseColumnTextForLinkValue = true;
            ////Enter your link button column header text
            //linkColumn.HeaderText = "Click to select";
            ////set the property name for link column
            //linkColumn.DataPropertyName = "lnkColumn";
            ////set default active color for link
            //linkColumn.ActiveLinkColor = Color.White;
            //linkColumn.LinkBehavior = LinkBehavior.SystemDefault;
            ////set default link color 
            //linkColumn.LinkColor = Color.Blue;
            ////set default link text
            //linkColumn.Text = "Select";
            ////set visited color means if user click that link what color would be change
            //linkColumn.VisitedLinkColor = Color.YellowGreen;
            //dataGridView1.Columns.Add(linkColumn);

            int height = dataGridView1.ColumnHeadersHeight;

            //dataGridView1.DefaultCellStyle.SelectionBackColor = 

            foreach (SettingsDetail settingsDetail in _settingsList)
            {
                dataGridView1.Rows.Add(new DataGridViewRow());
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0] = GetLinkCell(settingsDetail.FriendlyName);
                height += dataGridView1.Rows[dataGridView1.Rows.Count - 1].Height;
                if (settingsDetail.FriendlyName == _originalSettingName)
                {
                    dataGridView1.Rows[dataGridView1.Rows.Count - 1].Selected = true;
                    dataGridView1.CurrentCell = dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0];
                }
            }
            dataGridView1.Height = height;

            this.Height = dataGridView1.Height + dataGridView1.Location.Y + 100;
            btn_Cancel.Location = new Point(btn_Cancel.Location.X, this.Height - 65);
        }

        private DataGridViewLinkCell GetLinkCell(string text)
        {
            DataGridViewLinkCell cell = new DataGridViewLinkCell();
            cell.Value = text;

            return cell;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex >= 0 && e.RowIndex < _settingsList.Count)
            {
                SettingsDetail settingsDetail = _settingsList[e.RowIndex];

                if (settingsDetail.FriendlyName == _originalSettingName ||
                this.ShowCenteredMessageBox(string.Format("You selected setting \"{0}\" (currently you are using \"{1}\"). do you wish to proceed with this setting?", settingsDetail.FriendlyName, _originalSettingName), "Confirmation",
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    SettingChosen = true;
                    SettingName = settingsDetail.FriendlyName;
                    this.Close();
                    
                }
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public bool SettingChosen { get; private set; }
        public string SettingName { get; private set; }
    }
}
