﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.DiagnosticsDashboard
{
    public partial class SaveBackendSettingsDialog : Form
    {
        public SaveBackendSettingsDialog(string initialSettingName, Form centerToForm)
        {
            InitializeComponent();
            _settingsList = LoadSettingsList();
            InitializeGrid(initialSettingName);
            this.txt_settingName.Text = initialSettingName;
            this.PositionOnCenterOfForm(centerToForm);
        }

        private List<SettingsDetail> _settingsList;

        public bool SettingNameChosen { get; private set; }
        public string SettingName { get; private set; }

        private List<SettingsDetail> LoadSettingsList()
        {
            try
            {
                return new DAL.CounterpartyMonitorSettingsHelper().GetSettingsList();
            }
            catch (Exception e)
            {
                this.ShowCenteredMessageBox(
                    string.Format("Error occured during loading settings list ({0}). Please try later",
                                  e.GetType().ToString()), "Error");
                return new List<SettingsDetail>();
            }
        }

        private void InitializeGrid(string initialSettingName)
        {
            ////Create instance for datagrid view link column class
            //DataGridViewLinkColumn linkColumn = new DataGridViewLinkColumn();
            //linkColumn.UseColumnTextForLinkValue = true;
            ////Enter your link button column header text
            //linkColumn.HeaderText = "Click to select";
            ////set the property name for link column
            //linkColumn.DataPropertyName = "lnkColumn";
            ////set default active color for link
            //linkColumn.ActiveLinkColor = Color.White;
            //linkColumn.LinkBehavior = LinkBehavior.SystemDefault;
            ////set default link color 
            //linkColumn.LinkColor = Color.Blue;
            ////set default link text
            //linkColumn.Text = "Select";
            ////set visited color means if user click that link what color would be change
            //linkColumn.VisitedLinkColor = Color.YellowGreen;
            //dataGridView1.Columns.Add(linkColumn);

            int height = dataGridView1.ColumnHeadersHeight;

            foreach (SettingsDetail settingsDetail in _settingsList)
            {
                dataGridView1.Rows.Add(new DataGridViewRow());
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0] = GetLinkCell(settingsDetail.FriendlyName);
                height += dataGridView1.Rows[dataGridView1.Rows.Count - 1].Height;
                if (settingsDetail.FriendlyName == initialSettingName)
                {
                    dataGridView1.Rows[dataGridView1.Rows.Count - 1].Selected = true;
                    dataGridView1.CurrentCell = dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0];
                }
            }
            dataGridView1.Height = height;

            this.Height = dataGridView1.Height + dataGridView1.Location.Y + 130;

            lbl_SaveSettingsAs.Location = new Point(lbl_SaveSettingsAs.Location.X, dataGridView1.Height + dataGridView1.Location.Y + 15);
            txt_settingName.Location = new Point(txt_settingName.Location.X, dataGridView1.Height + dataGridView1.Location.Y + 15);

            btn_Cancel.Location = new Point(btn_Cancel.Location.X, this.Height - 65);
            btn_save.Location = new Point(btn_save.Location.X, this.Height - 65);
        }

        private DataGridViewLinkCell GetLinkCell(string text)
        {
            DataGridViewLinkCell cell = new DataGridViewLinkCell();
            cell.Value = text;

            return cell;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex >= 0 && e.RowIndex < _settingsList.Count)
            {
                SettingsDetail settingsDetail = _settingsList[e.RowIndex];

                txt_settingName.Text = settingsDetail.FriendlyName;
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            string settingsName = txt_settingName.Text;

            if (string.IsNullOrEmpty(settingsName))
            {
                this.ShowCenteredMessageBox("Setting Name needs to be populated.", "Warning");
            }
            else if (_settingsList.Any(s => s.FriendlyName == settingsName))
            {
                if (
                this.ShowCenteredMessageBox(string.Format("Setting \"{0}\" already exists, do you want to overwrite it?", settingsName), "Confirmation",
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.SettingNameChosen = true;
                    this.SettingName = settingsName;
                    this.Close();
                }
            }
            else
            {
                this.SettingNameChosen = true;
                this.SettingName = settingsName;
                this.Close();
            }
        }
    }
}
