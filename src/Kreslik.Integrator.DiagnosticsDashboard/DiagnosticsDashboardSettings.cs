﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Serialization;
using Kreslik.Integrator.Common;
using System.Linq;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.DiagnosticsDashboard
{

    public class DiagnosticsDashboardSettings
    {
        [System.Xml.Serialization.XmlIgnore]
        public System.TimeSpan RefreshTimeout { get; set; }

        [System.Xml.Serialization.XmlElement("RefreshTimeout_Miliseconds")]
        public int UnconfirmedSubscriptionTimeoutXml
        {
            get { return (int)RefreshTimeout.TotalMilliseconds; }
            set { RefreshTimeout = System.TimeSpan.FromMilliseconds(value); }
        }

        [System.Xml.Serialization.XmlIgnore]
        public System.TimeSpan FirstSpreadAgeLimit { get; set; }

        [System.Xml.Serialization.XmlElement("FirstSpreadAgeLimit_Seconds")]
        public int FirstSpreadAgeLimitXml
        {
            get { return (int)FirstSpreadAgeLimit.TotalSeconds; }
            set { FirstSpreadAgeLimit = System.TimeSpan.FromSeconds(value); }
        }

        [System.Xml.Serialization.XmlIgnore]
        public System.TimeSpan SecondSpreadAgeLimit { get; set; }

        [System.Xml.Serialization.XmlElement("SecondSpreadAgeLimit_Seconds")]
        public int SecondSpreadAgeLimitXml
        {
            get { return (int)SecondSpreadAgeLimit.TotalSeconds; }
            set { SecondSpreadAgeLimit = System.TimeSpan.FromSeconds(value); }
        }

        [System.Xml.Serialization.XmlIgnore]
        public System.TimeSpan ThirdSpreadAgeLimit { get; set; }

        [System.Xml.Serialization.XmlElement("ThirdSpreadAgeLimit_Seconds")]
        public int ThirdSpreadAgeLimitXml
        {
            get { return (int)ThirdSpreadAgeLimit.TotalSeconds; }
            set { ThirdSpreadAgeLimit = System.TimeSpan.FromSeconds(value); }
        }

        [System.Xml.Serialization.XmlIgnore]
        public System.TimeSpan CutOffSpreadAgeLimit { get; set; }

        [System.Xml.Serialization.XmlElement("CutOffSpreadAgeLimit_Hours")]
        public int CutOffSpreadAgeLimitXml
        {
            get { return (int)CutOffSpreadAgeLimit.TotalHours; }
            set { CutOffSpreadAgeLimit = System.TimeSpan.FromHours(value); }
        }

        [System.Xml.Serialization.XmlIgnore]
        public System.TimeSpan SessionActionTimeout { get; set; }

        [System.Xml.Serialization.XmlElement("SessionActionTimeout_Seconds")]
        public int SessionActionTimeoutXml
        {
            get { return (int)SessionActionTimeout.TotalSeconds; }
            set { SessionActionTimeout = System.TimeSpan.FromSeconds(value); }
        }


        private List<Symbol> _enabledSymbols;
            
            
        [System.Xml.Serialization.XmlIgnore]
        public List<Symbol> EnabledSymbols
        {
            get
            {
                if (_enabledSymbols == null)
                {
                    _enabledSymbols = EnabledSymbolsXml == null
                                          ? new List<Symbol>()
                                          : EnabledSymbolsXml.Select(symStr => (Symbol) symStr).ToList();
                }

                return _enabledSymbols;
            }

            set 
            { 
                _enabledSymbols = value;
                EnabledSymbolsXml = value == null ? new List<string>() : value.Select(sym => sym.ToString()).ToList();
            }
        }

        [XmlArray(ElementName = "EnabledSymbols")]
        [XmlArrayItem(ElementName = "Symbol")]
        public List<string> EnabledSymbolsXml
        {
            get; set; 
        }

        private List<Counterparty> _enabledCounterparties;


        [System.Xml.Serialization.XmlIgnore]
        public List<Counterparty> EnabledCounterparties
        {
            get
            {
                if (_enabledCounterparties == null)
                {
                    _enabledCounterparties = EnabledCounterpartiesXml == null
                                          ? new List<Counterparty>()
                                          : EnabledCounterpartiesXml.Select(ctpStr => (Counterparty)ctpStr).ToList();
                }

                return _enabledCounterparties;
            }

            set
            {
                _enabledCounterparties = value;
                EnabledCounterpartiesXml = value == null ? new List<string>() : value.Select(ctp => ctp.ToString()).ToList();
            }
        }

        [XmlArray(ElementName = "EnabledCounterparties")]
        [XmlArrayItem(ElementName = "Counterparty")]
        public List<string> EnabledCounterpartiesXml
        {
            get;
            set;
        }

        public List<CounterpartyInfoRowType> EnabledCounterpartyInfoRowTypes { get; set; }

        public static DiagnosticsDashboardSettings DeserializeConfigurationFromString(string configString, string settingName)
        {
            var settings = SettingsReader
                .DeserializeConfigurationFromString<DiagnosticsDashboardSettings>(
                    configString,
                    Properties.Resources.Kreslik_Integrator_DiagnosticsDashboard_exe
                );
            settings.SettingsName = settingName;
            return settings;
        }

        public static DiagnosticsDashboardSettings GetSettingsFromBackend()
        {
            var setting = SettingsInitializator.Instance.ReadSettings<DiagnosticsDashboardSettings>(
                        @"Kreslik.Integrator.DiagnosticsDashboard.exe",
                        Properties.Resources.Kreslik_Integrator_DiagnosticsDashboard_exe
                        );
            setting.SettingsName =
                new CounterpartyMonitorSettingsHelper().GetCurrentCMonitorSettingsFriendlyName(
                    SettingsInitializator.Instance.EnvironmentName);
            return setting;
        }

        public DiagnosticsDashboardSettings(string settingsName)
        {
            this.SettingsName = settingsName;
        }

        public DiagnosticsDashboardSettings()
        {
            
        }

        [System.Xml.Serialization.XmlIgnore]
        public string SettingsName { get; private set; }
    }
}
