﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.MessageBus;

namespace Kreslik.Integrator.DiagnosticsDashboard
{
    public partial class SessionActionForm : Form
    {
        private String _counterpartyString;
        private DateTime[] _sessionActionTimes;
        private DateTime _stpSessionTime;
        private IntegratorDiagnosticsClient _idc;
        private DiagnosticsDashboardSettings _settings;

        public SessionActionForm(string counterpartyString, DateTime[] sessionActionTimes, DiagnosticsDashboardSettings settings, IntegratorDiagnosticsClient idc, Form centerToForm)
        {
            this._counterpartyString = counterpartyString;
            this._sessionActionTimes = sessionActionTimes;
            this._idc = idc;
            this._settings = settings;
            InitializeComponent();
            InitializeUI(counterpartyString);
            this.PositionOnCenterOfForm(centerToForm);
        }

        public enum SessionActionType
        {
            RestartSession,
            StopSession,
            StartSession
        }

        private string[] SessionSubtypes = new string[]{"Session", "QUOSession", "ORDSession"};

        private void InitializeUI(string counterpartyString)
        {
            grpBxSessionSelection.Enabled = false;
            comboSessionSelection.SelectedIndex = 0;

            if (Counterparty.StringValues.Contains(counterpartyString))
            {
                this.label1.Text = string.Format("Select action for {0} sessions", counterpartyString);
            }
            else
            {
                this.label1.Text = string.Format("Select action for {0} session", counterpartyString);
                grpBxSessionSelection.Visible = false;
                chckSessionSelection.Visible = false;
            }
        }

        private void DoSessionActionIfAllowed(int ctpIdx, Func<IntegratorRequestResult> action)
        {
            IntegratorRequestResult result;
            lock (_sessionActionTimes)
            {
                if (DateTime.UtcNow - _sessionActionTimes[ctpIdx] > _settings.SessionActionTimeout)
                {
                    result = action();
                    _sessionActionTimes[ctpIdx] = DateTime.UtcNow;
                }
                else
                {
                    result =
                        IntegratorRequestResult.CreateFailedResult(
                            "Only one session action per 5 seconds per counterparty is allowed");
                }
            }

            if (result.RequestSucceeded)
            {
                this.ShowCenteredMessageBox("Action succeeded", "Success");
            }
            else
            {
                this.ShowCenteredMessageBox("Action failed: " + result.ErrorMessage, "Warning");
            }
        }


        private STPCounterparty unused;
        private void DoSessionAction(string counterpartString, SessionActionType sessionActionType)
        {
            bool allSessionsAction = _counterpartyString.Equals("ALL");

            if(STPCounterparty.TryParse(_counterpartyString, true, out unused))
            {
                DoSessionActionIfAllowed(Counterparty.ValuesCount,
                                             () =>
                                             _idc.TrySendCommand(sessionActionType.ToString().Replace("Session", "STPSession") + ";" + _counterpartyString));
            }
            else if (!Counterparty.StringValues.Contains(counterpartString) && !allSessionsAction)
            {
                this.ShowCenteredMessageBox("Action failed - Unrecognized counterpart string: " + counterpartString,
                                            "Warning");
            }
            else
            {
                string commandString = sessionActionType.ToString();
                if (chckSessionSelection.Checked)
                    commandString = commandString.Replace("Session",
                                                          SessionSubtypes[comboSessionSelection.SelectedIndex]);

                int ctpIdx = allSessionsAction ? 0 : (int) (Counterparty) counterpartString;

                DoSessionActionIfAllowed(ctpIdx,
                                         () =>
                                         _idc.TrySendCommand(commandString + ";" + counterpartString));
            }
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (
                this.ShowCenteredMessageBox("Do you really want to restart the selected session?", "Confirmation",
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DoSessionAction(_counterpartyString, SessionActionType.RestartSession);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (
                this.ShowCenteredMessageBox("Do you really want to stop the selected session?", "Confirmation",
                                            MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DoSessionAction(_counterpartyString, SessionActionType.StopSession);
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (
                this.ShowCenteredMessageBox("Do you really want to start the selected session?", "Confirmation",
                                            MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DoSessionAction(_counterpartyString, SessionActionType.StartSession);
            }
        }

        private void chckSessionSelection_CheckedChanged(object sender, EventArgs e)
        {
            grpBxSessionSelection.Enabled = chckSessionSelection.Checked;
        }
    }
}
