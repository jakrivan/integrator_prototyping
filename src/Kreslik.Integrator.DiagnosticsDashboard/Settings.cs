﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.DiagnosticsDashboard
{
    public partial class Settings : Form
    {
        private Action<DiagnosticsDashboardSettings> _settingsChanged;
        //private DiagnosticsDashboardSettings _diagnosticsDashboardSettings;
        private string _setingsName;

        public Settings(DiagnosticsDashboardSettings diagnosticsDashboardSettings, Action<DiagnosticsDashboardSettings> settingsChanged, Form centerToForm)
        {
            this._settingsChanged = settingsChanged;
            //this._diagnosticsDashboardSettings = diagnosticsDashboardSettings;
            this._setingsName = diagnosticsDashboardSettings.SettingsName;

            InitializeComponent();
            InitializeSymbolsCheckboxes(listViewIncludeSymbols);
            InitializeCounterpartiesCheckboxes(listViewCounterparties);
            InitializeCounterpartyInfosCheckboxes(listViewCounterpartyInfos);
            UpdateUIBasedOnSettings(diagnosticsDashboardSettings);
            this.PositionOnCenterOfForm(centerToForm);
        }

        private void UpdateUIBasedOnSettings(DiagnosticsDashboardSettings diagnosticsDashboardSettings)
        {
            foreach (ListViewItem item in listViewIncludeSymbols.Items)
            {
                item.Checked = diagnosticsDashboardSettings.EnabledSymbols.Contains((Symbol) item.Text);
            }

            foreach (ListViewItem item in listViewCounterparties.Items)
            {
                item.Checked = diagnosticsDashboardSettings.EnabledCounterparties.Contains((Counterparty) item.Text);
            }

            foreach (ListViewItem item in listViewCounterpartyInfos.Items)
            {
                item.Checked = diagnosticsDashboardSettings.EnabledCounterpartyInfoRowTypes.Select(type => type.ToString()).Contains(item.Name);
            }

            numericRefreshTimeout.Value = (decimal) diagnosticsDashboardSettings.RefreshTimeout.TotalMilliseconds;
            numericSpreadAgeFirst.Value = (decimal) diagnosticsDashboardSettings.FirstSpreadAgeLimit.TotalSeconds;
            numericSpreadAgeSecond.Value = (decimal) diagnosticsDashboardSettings.SecondSpreadAgeLimit.TotalSeconds;
            numericSpreadAgeThird.Value = (decimal) diagnosticsDashboardSettings.ThirdSpreadAgeLimit.TotalSeconds;
            numericSpreadAgeCutOff.Value = (decimal) diagnosticsDashboardSettings.CutOffSpreadAgeLimit.TotalHours;
            numericSessionActionTimeout.Value = (decimal) diagnosticsDashboardSettings.SessionActionTimeout.TotalSeconds;
        }

        private void InitializeSymbolsCheckboxes(ListView listView)
        {
            listView.View = View.List;
            // Display check boxes.
            listView.CheckBoxes = true;
            // Display grid lines.
            //listViewIncludeSymbols.GridLines = true;
            // Sort the items in the list in ascending order.
            listView.Sorting = SortOrder.Ascending;

            foreach (Symbol symbol in Symbol.Values)
            {
                ListViewItem item = new ListViewItem((string) symbol);
                listView.Items.Add(item);
            }
        }

        private void InitializeCounterpartiesCheckboxes(ListView listView)
        {
            listView.View = View.List;
            // Display check boxes.
            listView.CheckBoxes = true;
            // Display grid lines.
            //listViewIncludeSymbols.GridLines = true;
            // Sort the items in the list in ascending order.
            listView.Sorting = SortOrder.Ascending;

            foreach (Counterparty counterparty in Counterparty.Values)
            {
                ListViewItem item = new ListViewItem((string)counterparty);
                listView.Items.Add(item);
            }
        }

        private void InitializeCounterpartyInfosCheckboxes(ListView listView)
        {
            listView.View = View.List;
            listView.CheckBoxes = true;
            //listView.Sorting = SortOrder.Ascending;

            foreach (RowInfoBag rowInfoBag in RowInfoBag.RowInfoBagList)
            {
                ListViewItem item = new ListViewItem(rowInfoBag.RowText.Trim());
                if (!string.IsNullOrEmpty(rowInfoBag.RowToolTip))
                    item.ToolTipText = rowInfoBag.RowToolTip;
                item.Name = rowInfoBag.RowType.ToString();
                listView.Items.Add(item);
            }
        }

        private void chckBxAdvancedSettings_CheckedChanged(object sender, EventArgs e)
        {
            grpBxAdvancedSettings.Enabled = chckBxAdvancedSettings.Checked;
        }

        private void linkLblAll_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (ListViewItem item in listViewIncludeSymbols.Items)
            {
                item.Checked = true;
            }
        }

        private void LinkLblNone_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (ListViewItem item in listViewIncludeSymbols.Items)
            {
                item.Checked = false;
            }
        }

        private void linkLblAllCtps_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (ListViewItem item in listViewCounterparties.Items)
            {
                item.Checked = true;
            }
        }

        private void linkLblNoneCtp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (ListViewItem item in listViewCounterparties.Items)
            {
                item.Checked = false;
            }
        }

        private void linkLblAllCtpInfos_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (ListViewItem item in listViewCounterpartyInfos.Items)
            {
                item.Checked = true;
            }
        }

        private void linkLblNoneCtpInfos_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (ListViewItem item in listViewCounterpartyInfos.Items)
            {
                item.Checked = false;
            }
        }

        private void btnLoad_Click(object sender, EventArgs eargs)
        {
            using (LoadBackendSettingsDialog loadDialog = new LoadBackendSettingsDialog(this._setingsName, this))
            {
                try
                {
                    loadDialog.ShowDialog();
                    if (loadDialog.SettingChosen)
                    {
                        var settings =
                            DiagnosticsDashboardSettings.DeserializeConfigurationFromString(
                                new CounterpartyMonitorSettingsHelper().GetSettingsXmlString(loadDialog.SettingName),
                                loadDialog.SettingName);
                        this._setingsName = loadDialog.SettingName;
                        this.UpdateUIBasedOnSettings(settings);
                    }
                }
                catch (Exception e)
                {
                    var dialogTypeName = "System.Windows.Forms.PropertyGridInternal.GridErrorDlg";
                    var dialogType = typeof(Form).Assembly.GetType(dialogTypeName);

                    // Create dialog instance.
                    var dialogEx = (Form)Activator.CreateInstance(dialogType, new PropertyGrid());

                    // Populate relevant properties on the dialog instance.
                    dialogEx.Text = "Loading of settings failed";
                    dialogType.GetProperty("Details").SetValue(dialogEx, string.Format("Loading of settings {0} failed with error:{1}{2}. You can try to select different setting.",
                                                      loadDialog.SettingName, Environment.NewLine, e.ToString()), null);
                    dialogType.GetProperty("Message")
                              .SetValue(dialogEx,
                                        string.Format("Loading Failed: {0}", e.GetType()), null);

                    dialogEx.ShowDialog();
                }  
            }
        }

        private void btnDiscard_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTry_Click(object sender, EventArgs e)
        {
            var settings = this.GetSelectedSettings();
            this._settingsChanged(settings);
            this.Close();
        }

        private DiagnosticsDashboardSettings GetSelectedSettings()
        {
            DiagnosticsDashboardSettings settings = new DiagnosticsDashboardSettings(this._setingsName);

            var enabledSymbols = listViewIncludeSymbols.Items.Cast<ListViewItem>()
                                  .Where(item => item.Checked)
                                  .Select(item => (Symbol)item.Text)
                                  .ToList();
            settings.EnabledSymbols = enabledSymbols;

            var enabledCounterparties = listViewCounterparties.Items.Cast<ListViewItem>()
                                  .Where(item => item.Checked)
                                  .Select(item => (Counterparty)item.Text)
                                  .ToList();
            settings.EnabledCounterparties = enabledCounterparties;

            var enabledCounterpartyInfos = listViewCounterpartyInfos.Items.Cast<ListViewItem>()
                                  .Where(item => item.Checked)
                                  .Select(item => (CounterpartyInfoRowType)Enum.Parse(typeof(CounterpartyInfoRowType), item.Name))
                                  .ToList();
            settings.EnabledCounterpartyInfoRowTypes = enabledCounterpartyInfos;

            settings.RefreshTimeout = TimeSpan.FromMilliseconds((double) numericRefreshTimeout.Value);
            settings.FirstSpreadAgeLimit = TimeSpan.FromSeconds((double)numericSpreadAgeFirst.Value);
            settings.SecondSpreadAgeLimit = TimeSpan.FromSeconds((double)numericSpreadAgeSecond.Value);
            settings.ThirdSpreadAgeLimit = TimeSpan.FromSeconds((double)numericSpreadAgeThird.Value);
            settings.CutOffSpreadAgeLimit = TimeSpan.FromHours((double)numericSpreadAgeCutOff.Value);
            settings.SessionActionTimeout = TimeSpan.FromSeconds((double) numericSessionActionTimeout.Value);

            return settings;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.SafeSettingAndClose();
        }

        private void SafeSettingAndClose()
        {
            var settings = this.GetSelectedSettings();

            try
            {
                new CounterpartyMonitorSettingsHelper().UpdateCMonitorSettings(
                    SettingsInitializator.Instance.EnvironmentName, this._setingsName,
                    SettingsReader.SerializeConfiguration(settings));
                this._settingsChanged(settings);
            }
            catch (Exception e)
            {
                var dialogTypeName = "System.Windows.Forms.PropertyGridInternal.GridErrorDlg";
                var dialogType = typeof(Form).Assembly.GetType(dialogTypeName);

                // Create dialog instance.
                var dialogEx = (Form)Activator.CreateInstance(dialogType, new PropertyGrid());

                // Populate relevant properties on the dialog instance.
                dialogEx.Text = "Saving of settings failed";
                dialogType.GetProperty("Details").SetValue(dialogEx, string.Format("Saving of settings {0} failed with error:{1}{2}. You can try to select different setting.",
                                                  settings.SettingsName, Environment.NewLine, e.ToString()), null);
                dialogType.GetProperty("Message")
                          .SetValue(dialogEx,
                                    string.Format("Saving Failed: {0}", e.GetType()), null);

                dialogEx.ShowDialog();
            } 

            this.Close();
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            using (var saveAsDialog = new SaveBackendSettingsDialog(this._setingsName, this))
            {
                saveAsDialog.ShowDialog();
                if (saveAsDialog.SettingNameChosen)
                {
                    this._setingsName = saveAsDialog.SettingName;
                    this.SafeSettingAndClose();
                }
            }
        }
    }
}
