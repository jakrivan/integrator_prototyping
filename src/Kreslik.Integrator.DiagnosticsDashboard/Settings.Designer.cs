﻿namespace Kreslik.Integrator.DiagnosticsDashboard
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LinkLblNoneSymbol = new System.Windows.Forms.LinkLabel();
            this.linkLblAllSymbols = new System.Windows.Forms.LinkLabel();
            this.listViewIncludeSymbols = new System.Windows.Forms.ListView();
            this.grpBxAdvancedSettings = new System.Windows.Forms.GroupBox();
            this.numericSessionActionTimeout = new System.Windows.Forms.NumericUpDown();
            this.lblSessionTimeout = new System.Windows.Forms.Label();
            this.numericSpreadAgeCutOff = new System.Windows.Forms.NumericUpDown();
            this.numericSpreadAgeThird = new System.Windows.Forms.NumericUpDown();
            this.numericSpreadAgeSecond = new System.Windows.Forms.NumericUpDown();
            this.numericSpreadAgeFirst = new System.Windows.Forms.NumericUpDown();
            this.numericRefreshTimeout = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chckBxAdvancedSettings = new System.Windows.Forms.CheckBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnSaveAs = new System.Windows.Forms.Button();
            this.btnDiscard = new System.Windows.Forms.Button();
            this.btnTry = new System.Windows.Forms.Button();
            this.groupBoxCounterparties = new System.Windows.Forms.GroupBox();
            this.listViewCounterparties = new System.Windows.Forms.ListView();
            this.linkLblNoneCtp = new System.Windows.Forms.LinkLabel();
            this.linkLblAllCtps = new System.Windows.Forms.LinkLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listViewCounterpartyInfos = new System.Windows.Forms.ListView();
            this.linkLblNoneCtpInfos = new System.Windows.Forms.LinkLabel();
            this.linkLblAllCtpInfos = new System.Windows.Forms.LinkLabel();
            this.groupBox1.SuspendLayout();
            this.grpBxAdvancedSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericSessionActionTimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSpreadAgeCutOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSpreadAgeThird)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSpreadAgeSecond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSpreadAgeFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRefreshTimeout)).BeginInit();
            this.groupBoxCounterparties.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LinkLblNoneSymbol);
            this.groupBox1.Controls.Add(this.linkLblAllSymbols);
            this.groupBox1.Controls.Add(this.listViewIncludeSymbols);
            this.groupBox1.Location = new System.Drawing.Point(13, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(411, 627);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Displayed Symbols";
            // 
            // LinkLblNoneSymbol
            // 
            this.LinkLblNoneSymbol.AutoSize = true;
            this.LinkLblNoneSymbol.Location = new System.Drawing.Point(58, 20);
            this.LinkLblNoneSymbol.Name = "LinkLblNoneSymbol";
            this.LinkLblNoneSymbol.Size = new System.Drawing.Size(33, 13);
            this.LinkLblNoneSymbol.TabIndex = 2;
            this.LinkLblNoneSymbol.TabStop = true;
            this.LinkLblNoneSymbol.Text = "None";
            this.LinkLblNoneSymbol.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLblNone_LinkClicked);
            // 
            // linkLblAllSymbols
            // 
            this.linkLblAllSymbols.AutoSize = true;
            this.linkLblAllSymbols.Location = new System.Drawing.Point(25, 20);
            this.linkLblAllSymbols.Name = "linkLblAllSymbols";
            this.linkLblAllSymbols.Size = new System.Drawing.Size(18, 13);
            this.linkLblAllSymbols.TabIndex = 1;
            this.linkLblAllSymbols.TabStop = true;
            this.linkLblAllSymbols.Text = "All";
            this.linkLblAllSymbols.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLblAll_LinkClicked);
            // 
            // listViewIncludeSymbols
            // 
            this.listViewIncludeSymbols.Location = new System.Drawing.Point(11, 37);
            this.listViewIncludeSymbols.Name = "listViewIncludeSymbols";
            this.listViewIncludeSymbols.Size = new System.Drawing.Size(394, 584);
            this.listViewIncludeSymbols.TabIndex = 0;
            this.listViewIncludeSymbols.UseCompatibleStateImageBehavior = false;
            // 
            // grpBxAdvancedSettings
            // 
            this.grpBxAdvancedSettings.Controls.Add(this.numericSessionActionTimeout);
            this.grpBxAdvancedSettings.Controls.Add(this.lblSessionTimeout);
            this.grpBxAdvancedSettings.Controls.Add(this.numericSpreadAgeCutOff);
            this.grpBxAdvancedSettings.Controls.Add(this.numericSpreadAgeThird);
            this.grpBxAdvancedSettings.Controls.Add(this.numericSpreadAgeSecond);
            this.grpBxAdvancedSettings.Controls.Add(this.numericSpreadAgeFirst);
            this.grpBxAdvancedSettings.Controls.Add(this.numericRefreshTimeout);
            this.grpBxAdvancedSettings.Controls.Add(this.label5);
            this.grpBxAdvancedSettings.Controls.Add(this.label4);
            this.grpBxAdvancedSettings.Controls.Add(this.label3);
            this.grpBxAdvancedSettings.Controls.Add(this.label2);
            this.grpBxAdvancedSettings.Controls.Add(this.label1);
            this.grpBxAdvancedSettings.Enabled = false;
            this.grpBxAdvancedSettings.Location = new System.Drawing.Point(430, 450);
            this.grpBxAdvancedSettings.Name = "grpBxAdvancedSettings";
            this.grpBxAdvancedSettings.Size = new System.Drawing.Size(353, 202);
            this.grpBxAdvancedSettings.TabIndex = 1;
            this.grpBxAdvancedSettings.TabStop = false;
            // 
            // numericSessionActionTimeout
            // 
            this.numericSessionActionTimeout.Location = new System.Drawing.Point(192, 168);
            this.numericSessionActionTimeout.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericSessionActionTimeout.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericSessionActionTimeout.Name = "numericSessionActionTimeout";
            this.numericSessionActionTimeout.Size = new System.Drawing.Size(120, 20);
            this.numericSessionActionTimeout.TabIndex = 11;
            this.numericSessionActionTimeout.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblSessionTimeout
            // 
            this.lblSessionTimeout.AutoSize = true;
            this.lblSessionTimeout.Location = new System.Drawing.Point(26, 171);
            this.lblSessionTimeout.Name = "lblSessionTimeout";
            this.lblSessionTimeout.Size = new System.Drawing.Size(130, 13);
            this.lblSessionTimeout.TabIndex = 10;
            this.lblSessionTimeout.Text = "Session action timeout (s):";
            // 
            // numericSpreadAgeCutOff
            // 
            this.numericSpreadAgeCutOff.Location = new System.Drawing.Point(192, 139);
            this.numericSpreadAgeCutOff.Maximum = new decimal(new int[] {
            168,
            0,
            0,
            0});
            this.numericSpreadAgeCutOff.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericSpreadAgeCutOff.Name = "numericSpreadAgeCutOff";
            this.numericSpreadAgeCutOff.Size = new System.Drawing.Size(120, 20);
            this.numericSpreadAgeCutOff.TabIndex = 9;
            this.numericSpreadAgeCutOff.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericSpreadAgeThird
            // 
            this.numericSpreadAgeThird.Location = new System.Drawing.Point(192, 112);
            this.numericSpreadAgeThird.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericSpreadAgeThird.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericSpreadAgeThird.Name = "numericSpreadAgeThird";
            this.numericSpreadAgeThird.Size = new System.Drawing.Size(120, 20);
            this.numericSpreadAgeThird.TabIndex = 8;
            this.numericSpreadAgeThird.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericSpreadAgeSecond
            // 
            this.numericSpreadAgeSecond.Location = new System.Drawing.Point(192, 85);
            this.numericSpreadAgeSecond.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericSpreadAgeSecond.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericSpreadAgeSecond.Name = "numericSpreadAgeSecond";
            this.numericSpreadAgeSecond.Size = new System.Drawing.Size(120, 20);
            this.numericSpreadAgeSecond.TabIndex = 7;
            this.numericSpreadAgeSecond.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericSpreadAgeFirst
            // 
            this.numericSpreadAgeFirst.Location = new System.Drawing.Point(192, 58);
            this.numericSpreadAgeFirst.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericSpreadAgeFirst.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericSpreadAgeFirst.Name = "numericSpreadAgeFirst";
            this.numericSpreadAgeFirst.Size = new System.Drawing.Size(120, 20);
            this.numericSpreadAgeFirst.TabIndex = 6;
            this.numericSpreadAgeFirst.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericRefreshTimeout
            // 
            this.numericRefreshTimeout.Location = new System.Drawing.Point(192, 29);
            this.numericRefreshTimeout.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericRefreshTimeout.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericRefreshTimeout.Name = "numericRefreshTimeout";
            this.numericRefreshTimeout.Size = new System.Drawing.Size(120, 20);
            this.numericRefreshTimeout.TabIndex = 5;
            this.numericRefreshTimeout.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Cutoff spread age limit (hrs):";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Spread age limit 3 (s):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Spread age limit 2 (s):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Spread age limit 1 (s):";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Refresh timeout (ms):";
            // 
            // chckBxAdvancedSettings
            // 
            this.chckBxAdvancedSettings.AutoSize = true;
            this.chckBxAdvancedSettings.Location = new System.Drawing.Point(439, 450);
            this.chckBxAdvancedSettings.Name = "chckBxAdvancedSettings";
            this.chckBxAdvancedSettings.Size = new System.Drawing.Size(116, 17);
            this.chckBxAdvancedSettings.TabIndex = 2;
            this.chckBxAdvancedSettings.Text = "Advanced Settings";
            this.chckBxAdvancedSettings.UseVisualStyleBackColor = true;
            this.chckBxAdvancedSettings.CheckedChanged += new System.EventHandler(this.chckBxAdvancedSettings_CheckedChanged);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(296, 673);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(67, 23);
            this.btnLoad.TabIndex = 3;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(372, 673);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(67, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Location = new System.Drawing.Point(447, 673);
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.Size = new System.Drawing.Size(67, 23);
            this.btnSaveAs.TabIndex = 5;
            this.btnSaveAs.Text = "Save As";
            this.btnSaveAs.UseVisualStyleBackColor = true;
            this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
            // 
            // btnDiscard
            // 
            this.btnDiscard.Location = new System.Drawing.Point(714, 673);
            this.btnDiscard.Name = "btnDiscard";
            this.btnDiscard.Size = new System.Drawing.Size(75, 23);
            this.btnDiscard.TabIndex = 6;
            this.btnDiscard.Text = "Cancel";
            this.btnDiscard.UseVisualStyleBackColor = true;
            this.btnDiscard.Click += new System.EventHandler(this.btnDiscard_Click);
            // 
            // btnTry
            // 
            this.btnTry.Location = new System.Drawing.Point(589, 673);
            this.btnTry.Name = "btnTry";
            this.btnTry.Size = new System.Drawing.Size(119, 23);
            this.btnTry.TabIndex = 7;
            this.btnTry.Text = "Apply Without Saving";
            this.btnTry.UseVisualStyleBackColor = true;
            this.btnTry.Click += new System.EventHandler(this.btnTry_Click);
            // 
            // groupBoxCounterparties
            // 
            this.groupBoxCounterparties.Controls.Add(this.listViewCounterparties);
            this.groupBoxCounterparties.Controls.Add(this.linkLblNoneCtp);
            this.groupBoxCounterparties.Controls.Add(this.linkLblAllCtps);
            this.groupBoxCounterparties.Location = new System.Drawing.Point(430, 25);
            this.groupBoxCounterparties.Name = "groupBoxCounterparties";
            this.groupBoxCounterparties.Size = new System.Drawing.Size(153, 419);
            this.groupBoxCounterparties.TabIndex = 8;
            this.groupBoxCounterparties.TabStop = false;
            this.groupBoxCounterparties.Text = "Displayed Counterparties";
            // 
            // listViewCounterparties
            // 
            this.listViewCounterparties.Location = new System.Drawing.Point(11, 37);
            this.listViewCounterparties.Name = "listViewCounterparties";
            this.listViewCounterparties.Size = new System.Drawing.Size(136, 376);
            this.listViewCounterparties.TabIndex = 2;
            this.listViewCounterparties.UseCompatibleStateImageBehavior = false;
            // 
            // linkLblNoneCtp
            // 
            this.linkLblNoneCtp.AutoSize = true;
            this.linkLblNoneCtp.Location = new System.Drawing.Point(58, 20);
            this.linkLblNoneCtp.Name = "linkLblNoneCtp";
            this.linkLblNoneCtp.Size = new System.Drawing.Size(33, 13);
            this.linkLblNoneCtp.TabIndex = 1;
            this.linkLblNoneCtp.TabStop = true;
            this.linkLblNoneCtp.Text = "None";
            this.linkLblNoneCtp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLblNoneCtp_LinkClicked);
            // 
            // linkLblAllCtps
            // 
            this.linkLblAllCtps.AutoSize = true;
            this.linkLblAllCtps.Location = new System.Drawing.Point(25, 20);
            this.linkLblAllCtps.Name = "linkLblAllCtps";
            this.linkLblAllCtps.Size = new System.Drawing.Size(18, 13);
            this.linkLblAllCtps.TabIndex = 0;
            this.linkLblAllCtps.TabStop = true;
            this.linkLblAllCtps.Text = "All";
            this.linkLblAllCtps.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLblAllCtps_LinkClicked);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listViewCounterpartyInfos);
            this.groupBox2.Controls.Add(this.linkLblNoneCtpInfos);
            this.groupBox2.Controls.Add(this.linkLblAllCtpInfos);
            this.groupBox2.Location = new System.Drawing.Point(589, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(196, 419);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Displayed Counterparty info";
            // 
            // listViewCounterpartyInfos
            // 
            this.listViewCounterpartyInfos.Location = new System.Drawing.Point(7, 37);
            this.listViewCounterpartyInfos.Name = "listViewCounterpartyInfos";
            this.listViewCounterpartyInfos.Size = new System.Drawing.Size(182, 376);
            this.listViewCounterpartyInfos.TabIndex = 2;
            this.listViewCounterpartyInfos.UseCompatibleStateImageBehavior = false;
            // 
            // linkLblNoneCtpInfos
            // 
            this.linkLblNoneCtpInfos.AutoSize = true;
            this.linkLblNoneCtpInfos.Location = new System.Drawing.Point(47, 19);
            this.linkLblNoneCtpInfos.Name = "linkLblNoneCtpInfos";
            this.linkLblNoneCtpInfos.Size = new System.Drawing.Size(33, 13);
            this.linkLblNoneCtpInfos.TabIndex = 1;
            this.linkLblNoneCtpInfos.TabStop = true;
            this.linkLblNoneCtpInfos.Text = "None";
            this.linkLblNoneCtpInfos.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLblNoneCtpInfos_LinkClicked);
            // 
            // linkLblAllCtpInfos
            // 
            this.linkLblAllCtpInfos.AutoSize = true;
            this.linkLblAllCtpInfos.Location = new System.Drawing.Point(19, 20);
            this.linkLblAllCtpInfos.Name = "linkLblAllCtpInfos";
            this.linkLblAllCtpInfos.Size = new System.Drawing.Size(18, 13);
            this.linkLblAllCtpInfos.TabIndex = 0;
            this.linkLblAllCtpInfos.TabStop = true;
            this.linkLblAllCtpInfos.Text = "All";
            this.linkLblAllCtpInfos.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLblAllCtpInfos_LinkClicked);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 704);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBoxCounterparties);
            this.Controls.Add(this.btnTry);
            this.Controls.Add(this.btnDiscard);
            this.Controls.Add(this.btnSaveAs);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.chckBxAdvancedSettings);
            this.Controls.Add(this.grpBxAdvancedSettings);
            this.Controls.Add(this.groupBox1);
            this.Name = "Settings";
            this.Text = "Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpBxAdvancedSettings.ResumeLayout(false);
            this.grpBxAdvancedSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericSessionActionTimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSpreadAgeCutOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSpreadAgeThird)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSpreadAgeSecond)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSpreadAgeFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRefreshTimeout)).EndInit();
            this.groupBoxCounterparties.ResumeLayout(false);
            this.groupBoxCounterparties.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grpBxAdvancedSettings;
        private System.Windows.Forms.CheckBox chckBxAdvancedSettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericSpreadAgeFirst;
        private System.Windows.Forms.NumericUpDown numericRefreshTimeout;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericSpreadAgeCutOff;
        private System.Windows.Forms.NumericUpDown numericSpreadAgeThird;
        private System.Windows.Forms.NumericUpDown numericSpreadAgeSecond;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnSaveAs;
        private System.Windows.Forms.Button btnDiscard;
        private System.Windows.Forms.Button btnTry;
        private System.Windows.Forms.LinkLabel LinkLblNoneSymbol;
        private System.Windows.Forms.LinkLabel linkLblAllSymbols;
        private System.Windows.Forms.ListView listViewIncludeSymbols;
        private System.Windows.Forms.GroupBox groupBoxCounterparties;
        private System.Windows.Forms.ListView listViewCounterparties;
        private System.Windows.Forms.LinkLabel linkLblNoneCtp;
        private System.Windows.Forms.LinkLabel linkLblAllCtps;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView listViewCounterpartyInfos;
        private System.Windows.Forms.LinkLabel linkLblNoneCtpInfos;
        private System.Windows.Forms.LinkLabel linkLblAllCtpInfos;
        private System.Windows.Forms.NumericUpDown numericSessionActionTimeout;
        private System.Windows.Forms.Label lblSessionTimeout;
    }
}