﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.DiagnosticsDashboard
{
    public partial class InfoForm : Form
    {
        public InfoForm(Form centerToForm)
        {
            this.RestartRequested = false;
            InitializeComponent();
            InitializeLabels();

            this.PositionOnCenterOfForm(centerToForm);
        }

        public bool RestartRequested { get; private set; }

        private void InitializeLabels()
        {
            lblVer.Text =
                new BuildVersioningInfo(BuildConstants.CURRENT_BUILD_VERSION,
                                        BuildConstants.MINIMUM_VERSION_NUMBER_OF_COMPATIBLE_BUILD,
                                        BuildConstants.CURRENT_VERSION_CREATED_UTC).ToString();

            lblVer.Text += string.Format("{0}Build version: {1}", Environment.NewLine, Process.GetCurrentProcess().MainModule.FileVersionInfo.FileVersion);

            DateTime? buildTime = RetrieveLinkerTimestamp();
            if (buildTime.HasValue)
            {
                lblVer.Text += string.Format("{0}Built on: {1:yyyy-MM-dd HH:mm:ss UTC}", Environment.NewLine,
                                             buildTime.Value);
            }

            lblRunFrom.Text = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
        }

        private DateTime? RetrieveLinkerTimestamp()
        {
            string filePath = System.Reflection.Assembly.GetCallingAssembly().Location;
            const int c_PeHeaderOffset = 60;
            const int c_LinkerTimestampOffset = 8;
            byte[] b = new byte[2048];
            System.IO.Stream s = null;

            try
            {
                try
                {
                    s = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    s.Read(b, 0, 2048);
                }
                finally
                {
                    if (s != null)
                    {
                        s.Close();
                    }
                }

                int i = System.BitConverter.ToInt32(b, c_PeHeaderOffset);
                int secondsSince1970 = System.BitConverter.ToInt32(b, i + c_LinkerTimestampOffset);
                DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0);
                dt = dt.AddSeconds(secondsSince1970);
                dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            if (
                this.ShowCenteredMessageBox("Do you really want to restart the Monitor application?", "Confirmation",
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.RestartRequested = true;
                this.Close();
            }
        }
    }
}
