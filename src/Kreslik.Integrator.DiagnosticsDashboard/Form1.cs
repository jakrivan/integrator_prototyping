﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.MessageBus;

namespace Kreslik.Integrator.DiagnosticsDashboard
{
    public partial class Form1 : Form
    {
        private DiagnosticsDashboardSettings _diagnosticsDashboardSettings;
        private IntegratorDiagnosticsClient _businessLogicDiagClient;
        private IntegratorDiagnosticsClient _hotspotDataDiagClient;
        private IPersistedDealsStatisticsProvider _persistedDealsStatisticsProvider;
        private Color[] _backColors = new Color[Counterparty.ValuesCount];
        private Color _black = Color.Black;
        private System.Threading.Timer _pricesInfoTimer;
        private System.Threading.Timer _delasInfoTimer;
        private int[] _counterpartyCollIndexes = Enumerable.Repeat(-1, Counterparty.ValuesCount).ToArray();
        private int[] _symbolRowIndexes = Enumerable.Repeat(-1, Symbol.ValuesCount).ToArray();
        private int[] _additionalCounterpartiesInfoRowIndexes = Enumerable.Repeat(-1, (int)CounterpartyInfoRowType.MaximumItems).ToArray();
        private const int TOPOFBOOKCOLLIDX = 1;
        private const int TRUSTCOLLIDX = 2;
        private const int BESTBIDCPTCOLLIDX = 3;
        private const int BESTBIDCOLLIDX = 4;
        private const int BESTASKCOLLIDX = 5;
        private const int BESTASKCPTCOLLIDX = 6;    
        private ILogger _diagnosticClientLogger;
        private DataGridViewVertLabelCell _vertLabelCell;
        private bool _freezUIUpdates = false;

        private List<int> _makingCptIdx = new List<int>();
        private List<int> _takingCptIdxes;

        public Form1(DiagnosticsDashboardSettings diagnosticsDashboardSettings)
        {
            Application.ThreadException += (sender, args) => this.HandleAsyncException(args.Exception);
            TaskScheduler.UnobservedTaskException += (sender, args) => this.HandleAsyncException(args.Exception);
            AppDomain.CurrentDomain.UnhandledException += (sender, args) => this.HandleAsyncException((Exception) args.ExceptionObject);

            _diagnosticsDashboardSettings = diagnosticsDashboardSettings;
            _diagnosticClientLogger = (LogFactory.Instance).GetLogger("DiagClientLogger");
            this._persistedDealsStatisticsProvider = new PersistedDealsStatisticsProvider();
            _businessLogicDiagClient = new IntegratorDiagnosticsClient(_diagnosticClientLogger,
                                                   new IntegratorInstanceProcessUtils(
                                                       IntegratorProcessType.BusinessLogic));

            if (SessionsSettings.SubscriptionSettings.EnabledCounterpartsWithMDSessionInSeparateProcess.Any())
            {
                _hotspotDataDiagClient = new IntegratorDiagnosticsClient(_diagnosticClientLogger,
                                                   new IntegratorInstanceProcessUtils(
                                                       IntegratorProcessType.HotspotMarketData));
            }

            _takingCptIdxes = Counterparty.CounterpartiesForTaking.Select(cpt => (int) cpt).ToList();

            if (SessionsSettings.FIXChannel_HT3Behavior.DealConfirmationSupported)
            {
                _takingCptIdxes.Remove((int)Counterparty.HT3);
            }

            if (SessionsSettings.FIXChannel_HTABehavior.DealConfirmationSupported)
            {
                _takingCptIdxes.Remove((int)Counterparty.HTA);
            }

            if (SessionsSettings.FIXChannel_HTFBehavior.DealConfirmationSupported)
            {
                _takingCptIdxes.Remove((int)Counterparty.HTF);
            }

            if (SessionsSettings.FIXChannel_H4MBehavior.DealConfirmationSupported)
            {
                _takingCptIdxes.Remove((int)Counterparty.H4M);
            }

            //Task.Factory.StartNew(ConnectToService)
            //    .ContinueWith(t => InitializeTargetLabel(_idc.TargetEndpoint));

            InitializeColors();
            InitializeIndirectIndexesMappings();
            InitializeComponent();
            InitializeGrid();
            InitializeStpLabels();
            UpdateHeader(SettingsInitializator.Instance.EnvironmentName, _diagnosticsDashboardSettings.SettingsName);

            _pricesInfoTimer = new System.Threading.Timer(TimerTick, null, this._diagnosticsDashboardSettings.RefreshTimeout, Timeout.InfiniteTimeSpan);
            _delasInfoTimer = new System.Threading.Timer(DealsTimerTick, null, TimeSpan.FromSeconds(1), Timeout.InfiniteTimeSpan);
            ThreadPool.QueueUserWorkItem(ConnectToService);
        }

        public Form1()
            : this(DiagnosticsDashboardSettings.GetSettingsFromBackend())
        { }

        private bool _connected = false;
        private void ConnectToService(object unused)
        {
            try
            {
                _businessLogicDiagClient.Connect(true);
                if (_hotspotDataDiagClient != null)
                    _hotspotDataDiagClient.Connect(true);
                _connected = true;
                string connectedString = string.Format("BL:{0}", _businessLogicDiagClient.TargetEndpoint);
                if (_hotspotDataDiagClient != null)
                    connectedString += string.Format(" HD:{0}", _hotspotDataDiagClient.TargetEndpoint);
                this.InitializeTargetLabel(connectedString);
            }
            catch (Exception e)
            {
                this.HandleException(e, true);
            }
        }

        private void UpdateHeader(string environmentName, string settingsName)
        {
            this.Text = string.Format("Counterparty Monitor. Environemnt: [{0}], Settings: [{1}]", environmentName,
                                      settingsName);
        }

        private void UpdateSettings(DiagnosticsDashboardSettings settings)
        {
            //First remove the old UI elements according to old settings

            //while ((int)RowInfo.FirstSymbolIndex < dataGridView1.Rows.Count)
            //{
            //    //by removing this row, the next one will move up, so we can keep removing on the same index
            //    dataGridView1.Rows.RemoveAt((int)RowInfo.FirstSymbolIndex);
            //}

            dataGridView1.Rows.Clear();

            //Symbols Column and Summary Column
            int firstCounterpartyColumnIndex = 2;
            while (firstCounterpartyColumnIndex < dataGridView1.Columns.Count)
            {
                //by removing this row, the next one will move up, so we can keep removing on the same index
                dataGridView1.Columns.RemoveAt(firstCounterpartyColumnIndex);
            }

            //then update all internal settings

            this._diagnosticsDashboardSettings = settings;

            _symbolRowIndexes = Enumerable.Repeat(-1, Symbol.ValuesCount).ToArray();
            _counterpartyCollIndexes = Enumerable.Repeat(-1, Counterparty.ValuesCount).ToArray();
            _additionalCounterpartiesInfoRowIndexes = Enumerable.Repeat(-1, (int)CounterpartyInfoRowType.MaximumItems).ToArray();
            InitializeIndirectIndexesMappings();

            
            //Finally update the UI

            AddInitialInfoColumnsToGrid();
            AddCounterpartiesColumnsToGrid();
            AddHeaderRowsToGrid();
            AddSymbolRowsToGrid();

            UpdateHeader(SettingsInitializator.Instance.EnvironmentName, _diagnosticsDashboardSettings.SettingsName);

            //Refresh data in grid
            Volatile.Write(ref _freezUIUpdates, false);
            DealsTimerTick(null);
        }

        private void InitializeTargetLabel(string targetHostname)
        {
            lblTarget.Invoke((MethodInvoker)delegate { lblTarget.Text += targetHostname; });

            Task.Factory.StartNew(
                () => System.Net.Dns.GetHostEntry(targetHostname).HostName)
                .ContinueWith(t =>
                    {
                        if (t.IsFaulted)
                        {
                            var nullObj = t.Exception;
                        }
                        else
                        {
                            lblTarget.Invoke((MethodInvoker) delegate { lblTarget.Text += "(" + t.Result + ")"; });
                        }
                    });
        }

        private bool _backColorsInitialized = false;
        private void InitializeColors()
        {
           _backColors = GetBackColorSequence(Counterparty.ValuesCount);
            _backColorsInitialized = true;
        }

        private Color[] GetBackColorSequence(int sequenceLength)
        {
            if (sequenceLength == _backColors.Length && _backColorsInitialized)
            {
                return _backColors;
            }

            Color[] backColors = new Color[sequenceLength];

            if (sequenceLength <= 0)
            {
                return backColors;
            }

            backColors[0] = Color.Yellow;

            if (sequenceLength == 1)
            {
                return backColors;
            }

            if (sequenceLength == 2)
            {
                backColors[1] = System.Drawing.Color.FromArgb(0, 255, 0);
                return backColors;
            }

            int increment = 255 / (sequenceLength - 2);
            int red = 0;
            int green = 255;

            for (int i = 1; i < sequenceLength; i++)
            {
                backColors[i] = System.Drawing.Color.FromArgb(red, green, 0);
                red += increment;
                green -= increment;
            }

            return backColors;
        }

        private void InitializeIndirectIndexesMappings()
        {
            //0 - headers and symbol names
            //1 - Trust
            //2 - Sum
            //3 - bid cpt
            //4 - Bid
            //5 - Ask Cpt
            //6 - Ask
            //7 - start of ctps
            int i = 7;
            foreach (string counterparty in _diagnosticsDashboardSettings.EnabledCounterparties.Select(s => s.ToString()).OrderBy(s => s).Select(s => s))
            {
                _counterpartyCollIndexes[(int) ((Counterparty) counterparty)] = i++;
            }

            //All indexes already initialized to -1 - so we need to initialize only the enabled ones
            int additionalInfoRowIdx = 0;
            foreach (RowInfoBag rowInfoBag in 
                RowInfoBag.RowInfoBagList.Where(rib => _diagnosticsDashboardSettings.EnabledCounterpartyInfoRowTypes.Contains(rib.RowType)))
            {
                _additionalCounterpartiesInfoRowIndexes[(int)rowInfoBag.RowType] = additionalInfoRowIdx++;
            }

            //int j = (int) RowInfo.FirstSymbolIndex;
            int j = _additionalCounterpartiesInfoRowIndexes.Count(idx => idx >= 0);

            //ensure that we add indexes in the alphabetic order and not in EnabledSymbols (which can be unordered)
            foreach (Symbol symbol in this._diagnosticsDashboardSettings.EnabledSymbols.OrderBy(s => s.ToString()))
            {
                _symbolRowIndexes[(int)symbol] = j++;
            }
        }

        private void TimerTick(object unused)
        {
            PriceStreamsMonitoringInfo priceStreamMonitoringInfo;

            if (!_freezUIUpdates && _connected)
            {
                try
                {
                    var info1 = _businessLogicDiagClient.GetPriceStreamsMonitoringInfo();
                    if (_hotspotDataDiagClient != null)
                    {
                        var info2 = _hotspotDataDiagClient.GetPriceStreamsMonitoringInfo();
                        info1 = info1.MergeMonitoringInfo(info2);
                    }

                    priceStreamMonitoringInfo = info1;
                }
                catch (Exception ex)
                {
                    this.HandleExceptionDuringUiRefresh(ex);
                    return;
                }

                dataGridView1.Invoke((MethodInvoker)delegate { UpdateUI(priceStreamMonitoringInfo); });
            }

            _pricesInfoTimer.Change(this._diagnosticsDashboardSettings.RefreshTimeout, Timeout.InfiniteTimeSpan);
        }

        private void HandleExceptionDuringUiRefresh(Exception ex)
        {
            this.HandleException(ex, true);
        }

        private void HandleAsyncException(Exception ex)
        {
            this.HandleException(ex, false);
        }


        private static bool _handlingException = false;
        private void HandleException(Exception ex, bool autorestartEnabled)
        {
            this._diagnosticClientLogger.LogException(LogLevel.Fatal, "Unexpected exception during processing - will lead to app restart", ex);

            if (_handlingException)
                return;

            _handlingException = true;

            this.Invoke((MethodInvoker) delegate
                {
                    using (ExceptionHandlingDialog exDialog = new ExceptionHandlingDialog(ex, autorestartEnabled, this))
                    {
                        exDialog.ShowDialog(this);
                        if (exDialog.AplicationActionRequested ==
                            ExceptionHandlingDialog.AplicationActionRequest.Restart)
                        {
                            RestartApp();
                        }
                        else
                        {
                            this.Close();
                        }
                    }
                });
        }

        private void RestartApp()
        {
            ProcessStartInfo pci = new ProcessStartInfo(Process.GetCurrentProcess().MainModule.FileName);
            //if (Environment.GetCommandLineArgs().Count() > 1)
            //{
            //    pci.Arguments = string.Join(" ", Environment.GetCommandLineArgs());
            //}
            pci.Arguments = string.Format("\"{0}\"", this._diagnosticsDashboardSettings.SettingsName);
            Process.Start(pci);

            this.Invoke((MethodInvoker)this.Close);
        }

        private void DealsTimerTick(object unused)
        {
            if (!_freezUIUpdates)
            {
                DealsStatisticsBag bag;

                try
                {
                    bag = GetStatisticsFromBackend(this._diagnosticsDashboardSettings.EnabledCounterparties);
                }
                catch (Exception ex)
                {
                    this.HandleExceptionDuringUiRefresh(ex);
                    return;
                }

                dataGridView1.Invoke((MethodInvoker)delegate { UpdateDealsUI(bag); });
            }

            //Minimum 600ms, max 10s, otherwise refresh interval from settings
            TimeSpan refreshTimespan =
                TimeSpan.FromMilliseconds(
                Math.Min(10000,
                Math.Max(600, this._diagnosticsDashboardSettings.RefreshTimeout.TotalMilliseconds))
                );

            _delasInfoTimer.Change(refreshTimespan, Timeout.InfiniteTimeSpan);
        }

        private DealsStatisticsBag GetStatisticsFromBackend(List<Counterparty> enabledCounterparties)
        {
            DealsStatisticsBag dealsStatisticsBag = new DealsStatisticsBag();

            long totalIntegratorLatencyNanoseconds = 0;
            long totalCounterpartyLatencyNanoseconds = 0;
            int totalIntegratorLatencyCases = 0;
            int totalDealsForRejectionRateCalculation = 0;
            int totalCounterpartyLatencyCases = 0;

            foreach (PersistedCounterpartyStatistics persistedCounterpartyStatistics in this._persistedDealsStatisticsProvider.GetCompleteStatisticsPerCounterparty())
            {
                if (enabledCounterparties.Contains(persistedCounterpartyStatistics.Counterparty))
                {
                    int ctpIdx = (int) persistedCounterpartyStatistics.Counterparty;

                    dealsStatisticsBag.NumberOfDealsPerCounterpartyToday[ctpIdx] =
                        persistedCounterpartyStatistics.DealsNum;
                    dealsStatisticsBag.OveralNumberOfDealsToday += persistedCounterpartyStatistics.DealsNum;
                    dealsStatisticsBag.NumberOfKGTRejectionsPerCounterpartyToday[ctpIdx] =
                        persistedCounterpartyStatistics.KGTRejectionsNum;
                    dealsStatisticsBag.NumberOfCtpRejectionsPerCounterpartyToday[ctpIdx] =
                        persistedCounterpartyStatistics.CtpRejectionsNum;


                    dealsStatisticsBag.OveralNumberOfKGTRejectionsToday += persistedCounterpartyStatistics.KGTRejectionsNum;
                    dealsStatisticsBag.OveralNumberOfCtpRejectionsToday += persistedCounterpartyStatistics.CtpRejectionsNum;
                    totalDealsForRejectionRateCalculation += persistedCounterpartyStatistics.DealsNum;


                    //Only takers stats should be sumarized
                    if (Counterparty.IsCounterpartyForTakeing(persistedCounterpartyStatistics.Counterparty))
                    {
                        if (persistedCounterpartyStatistics.AvgIntegratorLatencyNanoseconds > 0)
                        {
                            totalIntegratorLatencyNanoseconds +=
                                persistedCounterpartyStatistics.AvgIntegratorLatencyNanoseconds *
                                (persistedCounterpartyStatistics.DealsNum + persistedCounterpartyStatistics.CtpRejectionsNum);
                            totalIntegratorLatencyCases += persistedCounterpartyStatistics.DealsNum +
                                                           persistedCounterpartyStatistics.CtpRejectionsNum;
                        }
                    }

                    
                    dealsStatisticsBag.KGTRejectionRatePerCounterpartyToday[ctpIdx] =
                        persistedCounterpartyStatistics.KGTRejectionsRate;
                    dealsStatisticsBag.CtpRejectionRatePerCounterpartyToday[ctpIdx] =
                        persistedCounterpartyStatistics.CtpRejectionsRate;
                    dealsStatisticsBag.TotalVolumePerCounterpartyToday[ctpIdx] =
                        persistedCounterpartyStatistics.VolumeBaseAbsInUsd;
                    dealsStatisticsBag.OveralVolumeToday += persistedCounterpartyStatistics.VolumeBaseAbsInUsd;
                    dealsStatisticsBag.AverageDealSizePerCounterpartyToday[ctpIdx] =
                        persistedCounterpartyStatistics.AvgDealSizeUsd;
                    dealsStatisticsBag.AverageQuoteToOrderLatencyPerCounterpartyToday[ctpIdx] =
                        TimeSpan.FromTicks(persistedCounterpartyStatistics.AvgIntegratorLatencyNanoseconds/100);
                    
                    dealsStatisticsBag.AverageOrderToDealLatencyPerCounterpartyToday[ctpIdx] =
                        TimeSpan.FromTicks(persistedCounterpartyStatistics.AvgCounterpartyLatencyNanoseconds/100);
                    if (persistedCounterpartyStatistics.AvgCounterpartyLatencyNanoseconds > 0)
                    {
                        totalCounterpartyLatencyNanoseconds +=
                            persistedCounterpartyStatistics.AvgCounterpartyLatencyNanoseconds*
                            persistedCounterpartyStatistics.DealsNum;
                        totalCounterpartyLatencyCases += persistedCounterpartyStatistics.DealsNum;
                    }
                }
            }

            if (dealsStatisticsBag.OveralVolumeToday > 0)
            {
                dealsStatisticsBag.VolumeSharePerCounterpartyToday =
                    dealsStatisticsBag.TotalVolumePerCounterpartyToday.Select(
                        volume => volume / dealsStatisticsBag.OveralVolumeToday).ToList();
            }

            if (dealsStatisticsBag.OveralNumberOfDealsToday > 0)
            {
                dealsStatisticsBag.OveralAverageDealSizeToday = dealsStatisticsBag.OveralVolumeToday/
                                                                dealsStatisticsBag.OveralNumberOfDealsToday;
            }

            if (dealsStatisticsBag.OveralNumberOfKGTRejectionsToday > 0)
            {
                dealsStatisticsBag.OveralKGTRejectionRateToday = dealsStatisticsBag.OveralNumberOfKGTRejectionsToday * 1.0m /
                                                              (dealsStatisticsBag.OveralNumberOfKGTRejectionsToday +
                                                               totalDealsForRejectionRateCalculation);
            }

            if (dealsStatisticsBag.OveralNumberOfCtpRejectionsToday > 0)
            {
                dealsStatisticsBag.OveralCtpRejectionRateToday = dealsStatisticsBag.OveralNumberOfKGTRejectionsToday * 1.0m /
                                                              (dealsStatisticsBag.OveralNumberOfCtpRejectionsToday +
                                                               totalDealsForRejectionRateCalculation);
            }

            if (totalIntegratorLatencyCases > 0)
            {
                dealsStatisticsBag.AverageQuoteToOrderLatencyToday =
                    TimeSpan.FromTicks((long) (totalIntegratorLatencyNanoseconds/100m/totalIntegratorLatencyCases));
            }

            if (totalCounterpartyLatencyCases > 0)
            {
                dealsStatisticsBag.AverageOrderToDealLatencyToday =
                    TimeSpan.FromTicks((long)(totalCounterpartyLatencyNanoseconds / 100m / totalCounterpartyLatencyCases));
            }

            return dealsStatisticsBag;
        }

        private List<Label> _stpStateLabels = new List<Label>(); 

        private void InitializeStpLabels()
        {
            foreach (STPCounterparty stpCounterparty in Enum.GetValues(typeof(STPCounterparty)))
            {
                Label stpLabel = new Label();

                stpLabel.AutoSize = true;
                stpLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
                stpLabel.Location = new System.Drawing.Point(254, 22);
                stpLabel.Name = stpCounterparty.ToString();
                stpLabel.Size = new System.Drawing.Size(264, 17);
                stpLabel.Text = string.Format("[{0}]: N/A", stpCounterparty);
                stpLabel.ForeColor = Color.Red;
                stpLabel.Click += new System.EventHandler(this.lblStpFlowState_Click);
                this.Controls.Add(stpLabel);
                _stpStateLabels.Add(stpLabel);
            }

            PositionStpLabels();
        }

        private void PositionStpLabels()
        {
            for (int lblIdx = 1; lblIdx < _stpStateLabels.Count; lblIdx++)
            {
                int currentXPos = _stpStateLabels[lblIdx - 1].Location.X + _stpStateLabels[lblIdx - 1].Width;
                _stpStateLabels[lblIdx].Location = new Point(currentXPos, _stpStateLabels[lblIdx].Location.Y);
            }
        }

        private void InitializeGrid()
        {
            dataGridView1.RowHeadersVisible = false;
            dataGridView1.EnableHeadersVisualStyles = false;

            DataGridViewRolloverCellColumn SymbolCol = new DataGridViewRolloverCellColumn();

            SymbolCol.HeaderText = @"Information\Counterparty";
            SymbolCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            SymbolCol.Name = "Column1";
            //SymbolCol.Resizable = DataGridViewTriState.False;
            SymbolCol.ReadOnly = true;
            SymbolCol.Width = 170;
            //SymbolCol.DefaultCellStyle.BackColor = System.Drawing.Color.DeepSkyBlue;
            SymbolCol.DefaultCellStyle.BackColor = System.Drawing.Color.Black;
            SymbolCol.DefaultCellStyle.ForeColor = Color.LightGray;
            SymbolCol.DefaultCellStyle.Font = new Font(new FontFamily("Arial"), 8, FontStyle.Bold);
            SymbolCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns.Add(SymbolCol);

            dataGridView1.Columns.Add(this.CreateColumn("\u03A3", 45));
            AddInitialInfoColumnsToGrid();
            AddCounterpartiesColumnsToGrid();

            dataGridView1.CellClick += DataGridView1OnCellClick;

            AddHeaderRowsToGrid();
            AddSymbolRowsToGrid();

        }

        private void AddInitialInfoColumnsToGrid()
        {
            dataGridView1.Columns.Add(this.CreateColumn("Trust", 45));
            var cellb = this.CreateColumn("Best Bid Cpt", 45);
            cellb.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            cellb.DefaultCellStyle.Font = new Font(new FontFamily("Arial"), 8, FontStyle.Bold);
            dataGridView1.Columns.Add(cellb);
            dataGridView1.Columns.Add(this.CreateColumn("Best Bid", 59));
            dataGridView1.Columns.Add(this.CreateColumn("Best Ask", 63));
            var cella = this.CreateColumn("Best Ask Cpt", 45);
            cella.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            cella.DefaultCellStyle.Font = new Font(new FontFamily("Arial"), 8, FontStyle.Bold);
            dataGridView1.Columns.Add(cella);
        }

        private DataGridViewRolloverCellColumn CreateColumn(string columnName, int width)
        {
            DataGridViewRolloverCellColumn col = new DataGridViewRolloverCellColumn();
            //col.Resizable = DataGridViewTriState.False;
            col.HeaderText = columnName;
            //col.HeaderCell.Style.BackColor = System.Drawing.Color.DeepSkyBlue;
            col.HeaderCell.Style.BackColor = System.Drawing.Color.Black;
            col.HeaderCell.Style.ForeColor = Color.LightGray;
            col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.HeaderCell.Style.Font = new Font(new FontFamily("Arial"), 8, FontStyle.Bold);

            col.Width = width;
            col.Name = columnName; 
            col.ReadOnly = true;
            col.DefaultCellStyle.BackColor = Color.LightGray;
            col.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            //dataGridView1.Columns.Add(col);
            return col;
        }

        private void AddCounterpartiesColumnsToGrid()
        {
            foreach (string value in _diagnosticsDashboardSettings.EnabledCounterparties.Select(s => s.ToString()).OrderBy(s => s).Select(s => s))
            {
                DataGridViewRolloverCellColumn col = new DataGridViewRolloverCellColumn();
                //col.Resizable = DataGridViewTriState.False;
                col.HeaderText = value;
                //col.HeaderCell.Style.BackColor = System.Drawing.Color.DeepSkyBlue;
                col.HeaderCell.Style.BackColor = System.Drawing.Color.Black;
                col.HeaderCell.Style.ForeColor = Color.LightGray;
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                col.HeaderCell.Style.Font = new Font(new FontFamily("Arial"), 8, FontStyle.Bold);

                col.Width = 45;
                col.Name = value;
                col.ReadOnly = true;
                dataGridView1.Columns.Add(col);
            }
        }

        private void AddHeaderRowsToGrid()
        {
            //First of all hide the vertical label or otherwise it may remain in future as phantom
            if (verticalLabel != null && _vertLabelCell != null)
            {
                this.dataGridView1.Controls.Remove(verticalLabel);

                verticalLabel.Visible = false;
                verticalLabel.Hide();
                verticalLabel.Refresh();
                verticalLabel.Dispose();
                verticalLabel = null;
                _vertLabelCell.VerticalLabel.Visible = false;
                _vertLabelCell.VerticalLabel = null;
                _vertLabelCell.Dispose();
                _vertLabelCell = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();

                dataGridView1.Refresh();
            }


            //Add the empty rows
            int additionalCtpInfoRowsCount = _additionalCounterpartiesInfoRowIndexes.Count(idx => idx >= 0);
            if (additionalCtpInfoRowsCount > 0)
            {
                dataGridView1.Rows.Add(_additionalCounterpartiesInfoRowIndexes.Count(idx => idx >= 0));
            }


            //Add the verticall cell to the dailystatistics
            int? firstEnabledDealsInfoRow =
                Enumerable.Range((int)CounterpartyInfoRowType.FirstDailyInfoRow,
                                 (int)CounterpartyInfoRowType.LastDailyInfoRow -
                                 (int)CounterpartyInfoRowType.FirstDailyInfoRow + 1)
                          .Where(idx => _additionalCounterpartiesInfoRowIndexes[idx] >= 0)
                          .Min(idx => (int?)_additionalCounterpartiesInfoRowIndexes[idx]);
            int? lastEnabledDealsInfoRow =
                Enumerable.Range((int)CounterpartyInfoRowType.FirstDailyInfoRow,
                                 (int)CounterpartyInfoRowType.LastDailyInfoRow -
                                 (int)CounterpartyInfoRowType.FirstDailyInfoRow + 1)
                          .Where(idx => _additionalCounterpartiesInfoRowIndexes[idx] >= 0)
                          .Max(idx => (int?)_additionalCounterpartiesInfoRowIndexes[idx]);

            if (firstEnabledDealsInfoRow.HasValue && lastEnabledDealsInfoRow.HasValue)
            {
                //22px for each row
                int lblHeight = (lastEnabledDealsInfoRow.Value - firstEnabledDealsInfoRow.Value + 1) * 22;

                verticalLabel = new VerticalLabel();
                verticalLabel.NewText = "Today (UTC)        Today (UTC)        Today (UTC)        ";
                ToolTip lblTooltip = new ToolTip();
                lblTooltip.SetToolTip(verticalLabel, "Today (UTC)");
                verticalLabel.BorderStyle = BorderStyle.FixedSingle;
                verticalLabel.Visible = true;
                verticalLabel.AutoSize = false;
                verticalLabel.Font = new Font(new FontFamily("Arial"), 8, FontStyle.Bold);
                verticalLabel.Name = "VertLbl";
                verticalLabel.Size = new System.Drawing.Size(20, lblHeight);
                verticalLabel.BackColor = Color.Black;
                verticalLabel.ForeColor = Color.LightGray;
                verticalLabel.TabIndex = 8;

                this.dataGridView1.Controls.Add(verticalLabel);

                _vertLabelCell = new DataGridViewVertLabelCell() { VerticalLabel = verticalLabel };
                dataGridView1.Rows[lastEnabledDealsInfoRow.Value].Cells[0] = _vertLabelCell;
            }

            foreach (RowInfoBag rowInfoBag in RowInfoBag.RowInfoBagList)
            {
                int rowIdx = _additionalCounterpartiesInfoRowIndexes[(int) rowInfoBag.RowType];

                if(rowIdx < 0)
                    continue;

                if (rowInfoBag.IsDivisorRow)
                {
                    //Add the dividing row
                    dataGridView1.Rows[rowIdx].Resizable = DataGridViewTriState.False;
                    dataGridView1.Rows[rowIdx].DefaultCellStyle.BackColor = Color.SlateGray;
                    dataGridView1.Rows[rowIdx].Height = 6;
                }
                else
                {
                    dataGridView1.Rows[rowIdx].Resizable = DataGridViewTriState.False;
                    dataGridView1.Rows[rowIdx].Cells[0].Value = rowInfoBag.RowText;
                    if (!string.IsNullOrEmpty(rowInfoBag.RowToolTip))
                        dataGridView1.Rows[rowIdx].Cells[0].ToolTipText = rowInfoBag.RowToolTip;
                    dataGridView1.Rows[rowIdx].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dataGridView1.Rows[rowIdx].Cells[TOPOFBOOKCOLLIDX].Style.BackColor = Color.Black;
                    dataGridView1.Rows[rowIdx].Cells[TRUSTCOLLIDX].Style.BackColor = Color.Black;
                    dataGridView1.Rows[rowIdx].Cells[BESTASKCOLLIDX].Style.BackColor = Color.Black;
                    dataGridView1.Rows[rowIdx].Cells[BESTBIDCOLLIDX].Style.BackColor = Color.Black;
                    dataGridView1.Rows[rowIdx].Cells[BESTASKCPTCOLLIDX].Style.BackColor = Color.Black;
                    dataGridView1.Rows[rowIdx].Cells[BESTBIDCPTCOLLIDX].Style.BackColor = Color.Black;
                    dataGridView1.Rows[rowIdx].Cells[TOPOFBOOKCOLLIDX].Style.ForeColor = Color.White;
                }
            }
        }

        private void AddSymbolRowsToGrid()
        {
            if (this._diagnosticsDashboardSettings.EnabledSymbols.Count > 0)
            {
                dataGridView1.Rows.Add(this._diagnosticsDashboardSettings.EnabledSymbols.Count);

                foreach (Symbol symbol in this._diagnosticsDashboardSettings.EnabledSymbols)
                {
                    dataGridView1.Rows[_symbolRowIndexes[(int) symbol]].Resizable = DataGridViewTriState.False;
                    dataGridView1.Rows[_symbolRowIndexes[(int) symbol]].Cells[0].Value = symbol.ToString();
                }
            }
        }

        private void DataGridView1OnCellClick(object sender, DataGridViewCellEventArgs dataGridViewCellEventArgs)
        {
            if (dataGridViewCellEventArgs.RowIndex == -1 
                && dataGridViewCellEventArgs.ColumnIndex != 0
                && dataGridViewCellEventArgs.ColumnIndex != TRUSTCOLLIDX
                && dataGridViewCellEventArgs.ColumnIndex != BESTASKCOLLIDX
                && dataGridViewCellEventArgs.ColumnIndex != BESTBIDCOLLIDX
                && dataGridViewCellEventArgs.ColumnIndex != BESTASKCPTCOLLIDX
                && dataGridViewCellEventArgs.ColumnIndex != BESTBIDCPTCOLLIDX)
            {
                string counterpartyString = "not specified";
                if (dataGridViewCellEventArgs.ColumnIndex == TOPOFBOOKCOLLIDX)
                {
                    counterpartyString = "ALL";
                }
                else
                {
                    for (int i = 0; i < _counterpartyCollIndexes.Length; i++)
                    {
                        if (dataGridViewCellEventArgs.ColumnIndex == _counterpartyCollIndexes[i])
                            counterpartyString = ((Counterparty) i).ToString();
                    }
                }

                SessionActionForm saf = new SessionActionForm(counterpartyString, _sessionActionTimes, this._diagnosticsDashboardSettings, _businessLogicDiagClient, this);
                saf.ShowDialog();
            }
        }

        //last entry is for stp session
        private DateTime[] _sessionActionTimes = Enumerable.Repeat(DateTime.MinValue, Counterparty.ValuesCount + 1).ToArray();

        private void UpdateDealsUI(DealsStatisticsBag dealsStatisticsBag)
        {
            try
            {
                UpdateDealsUIInternal(dealsStatisticsBag);
            }
            catch (Exception e)
            {
                this.HandleExceptionDuringUiRefresh(e);
            }
        }

        //private string SessionStateToDescription(SessionState sessionState, bool configured)
        //{
        //    if (!configured)
        //        return "N/A";

        //    switch (sessionState)
        //    {
        //        case SessionState.Idle:
        //        case SessionState.Stopping:
        //        case SessionState.LoggingOn:
        //            return "Discon";
        //        case SessionState.PhysicallyConnected:
        //            return "Conn";
        //        case SessionState.LoggedOn:
        //        case SessionState.Starting:
        //            return "Logged";
        //        case SessionState.Running:
        //            return "Run";
        //        default:
        //            return "UNKNOWN";
        //    }
        //}

        private string SessionStateToDescription(SessionStatusInfo sessionStatusInfo)
        {
            if ((!sessionStatusInfo.Configured || !sessionStatusInfo.Claimed) && sessionStatusInfo.SessionState == SessionState.Idle)
                return "N/A";

            switch (sessionStatusInfo.SessionState)
            {
                case SessionState.Idle:
                case SessionState.Stopping:
                case SessionState.LoggingOn:
                    return "Discon";
                case SessionState.PhysicallyConnected:
                    return "Conn";
                case SessionState.LoggedOn:
                case SessionState.Starting:
                    return "Logged";
                case SessionState.Running:
                    return "Run";
                default:
                    return "UNKNOWN";
            }
        }

        private string SessionStateToTooltip(SessionStatusInfo sessionStatusInfo)
        {
            if (!sessionStatusInfo.Configured)
                return sessionStatusInfo.Claimed ? "Not Configured (but claimed)" : "Not Configured (and not claimed)";

            if (!sessionStatusInfo.Claimed)
                return sessionStatusInfo.SessionState == SessionState.Idle
                           ? "Not Globally Claimed (from DB)"
                           : ("Not Globally Claimed (from DB), but state is " + sessionStatusInfo.SessionState);

            return sessionStatusInfo.SessionState.ToString();
        }

        private string BothSessionsStateToTooltip(SessionStatusInfo mdSessionStatusInfo, SessionStatusInfo ofSessionStatusInfo)
        {
            if (!mdSessionStatusInfo.Configured)
                return mdSessionStatusInfo.Claimed
                           ? "Sessions not Configured (but claimed)"
                           : "Sessions not Configured (and not claimed)";

            if (!mdSessionStatusInfo.Claimed)
                return "Sessions Not Globally Claimed (from DB)";

            if (mdSessionStatusInfo.Inactivated && ofSessionStatusInfo.Inactivated)
                return "Sessions are inactivated";

            if (mdSessionStatusInfo.Inactivated || ofSessionStatusInfo.Inactivated)
                return "One of the sessions is inactivated";

            return "Sessions are allowed to run";
        }

        private Color SessionStateToBackColor(SessionStatusInfo sessionStatusInfo)
        {
            if ((!sessionStatusInfo.Configured || !sessionStatusInfo.Claimed) && sessionStatusInfo.SessionState == SessionState.Idle)
                return Color.Red;

            switch (sessionStatusInfo.SessionState)
            {
                case SessionState.LoggedOn:
                case SessionState.Starting:
                case SessionState.PhysicallyConnected:
                    return Color.DarkOrange;
                case SessionState.Running:
                    return Color.GreenYellow;
                case SessionState.Idle:
                case SessionState.Stopping:
                case SessionState.LoggingOn:
                default:
                    return Color.Red;
            }
        }

        private void UpdateSessionsStateUI(PriceStreamsMonitoringInfo priceStreamMonitoringInfo)
        {
            ////Reset coloring of counterparties
            foreach (Counterparty enabledCounterparty in _diagnosticsDashboardSettings.EnabledCounterparties)
            {
                int counterpartyIdx = (int)enabledCounterparty;
                SessionStatusInfo mdInfo = priceStreamMonitoringInfo.MarketDataSessionsStatusInfos[counterpartyIdx];
                SessionStatusInfo ofInfo = priceStreamMonitoringInfo.OrderFlowSessionsStatusInfos[counterpartyIdx];

                //Color disconnected counterparties
                if (priceStreamMonitoringInfo.IsCounterpartyOffline(enabledCounterparty))
                {
                    var col = dataGridView1.Columns[_counterpartyCollIndexes[counterpartyIdx]];
                    col.HeaderCell.Style.BackColor = System.Drawing.Color.Red;
                    col.HeaderCell.Style.ForeColor = Color.White;
                }
                else
                {
                    var col = dataGridView1.Columns[_counterpartyCollIndexes[counterpartyIdx]];
                    col.HeaderCell.Style.BackColor = System.Drawing.Color.Black;
                    col.HeaderCell.Style.ForeColor = Color.LightGray;
                }

                bool sessionOff = !mdInfo.Configured || !ofInfo.Configured || mdInfo.Inactivated || ofInfo.Inactivated ||
                                  !mdInfo.Claimed || !ofInfo.Claimed;

                AddRowValueIfRowEnabled(CounterpartyInfoRowType.Active, _counterpartyCollIndexes[counterpartyIdx],
                                        sessionOff ? "Off" : "On",
                                        BothSessionsStateToTooltip(mdInfo, ofInfo),
                                        (sessionOff)
                                            ? System.Drawing.Color.Red
                                            : System.Drawing.Color.GreenYellow);

                AddRowValueIfRowEnabled(
                    CounterpartyInfoRowType.MarketData, _counterpartyCollIndexes[counterpartyIdx],
                    SessionStateToDescription(mdInfo),
                    SessionStateToTooltip(mdInfo),
                    SessionStateToBackColor(mdInfo));

                AddRowValueIfRowEnabled(
                    CounterpartyInfoRowType.OrderFlow, _counterpartyCollIndexes[counterpartyIdx],
                    SessionStateToDescription(ofInfo),
                    SessionStateToTooltip(ofInfo),
                    SessionStateToBackColor(ofInfo));
            }
        }

        private void AddRowValueIfRowEnabled(CounterpartyInfoRowType rowType, int colIdx, object value, string tooltip, Color backColor, Color foreColor)
        {
            int rowIdx = _additionalCounterpartiesInfoRowIndexes[(int)rowType];
            if (rowIdx < 0)
                return;

            dataGridView1.Rows[rowIdx].Cells[colIdx].Value = value;

            if (!string.IsNullOrEmpty(tooltip))
                dataGridView1.Rows[rowIdx].Cells[colIdx].ToolTipText = tooltip;

            if (backColor != Color.Empty)
                dataGridView1.Rows[rowIdx].Cells[colIdx].Style.BackColor = backColor;

            if (foreColor != Color.Empty)
                dataGridView1.Rows[rowIdx].Cells[colIdx].Style.ForeColor = foreColor;
        }

        private void AddRowValueIfRowEnabled(CounterpartyInfoRowType rowType, int colIdx, object value, string tooltip, Color backColor)
        {
            AddRowValueIfRowEnabled(rowType, colIdx, value, tooltip, backColor, Color.Empty);
        }

        private void AddRowValueIfRowEnabled(CounterpartyInfoRowType rowType, int colIdx, object value)
        {
            AddRowValueIfRowEnabled(rowType, colIdx, value, null, Color.Empty);
        }

        private void UpdateDealsUIInternal(DealsStatisticsBag dealsStatisticsBag)
        {
            AddRowValueIfRowEnabled(CounterpartyInfoRowType.Deals, TOPOFBOOKCOLLIDX,
                                    dealsStatisticsBag.OveralNumberOfDealsToday);

            AddRowValueIfRowEnabled(CounterpartyInfoRowType.KGTRejections, TOPOFBOOKCOLLIDX,
                                    dealsStatisticsBag.OveralNumberOfKGTRejectionsToday);

            AddRowValueIfRowEnabled(CounterpartyInfoRowType.CtpRejections, TOPOFBOOKCOLLIDX,
                                    dealsStatisticsBag.OveralNumberOfCtpRejectionsToday);

            AddRowValueIfRowEnabled(CounterpartyInfoRowType.KGTRejectionRate, TOPOFBOOKCOLLIDX,
                                    (dealsStatisticsBag.OveralKGTRejectionRateToday * 100).ToString("F1"));

            AddRowValueIfRowEnabled(CounterpartyInfoRowType.CtpRejectionRate, TOPOFBOOKCOLLIDX,
                                    (dealsStatisticsBag.OveralCtpRejectionRateToday * 100).ToString("F1"));

            AddRowValueIfRowEnabled(CounterpartyInfoRowType.AvgDeal, TOPOFBOOKCOLLIDX,
                                    (dealsStatisticsBag.OveralAverageDealSizeToday / 1000000).ToString("F3"));

            AddRowValueIfRowEnabled(CounterpartyInfoRowType.Volume, TOPOFBOOKCOLLIDX,
                                    (dealsStatisticsBag.OveralVolumeToday / 1000000).ToString("F1"));

            AddRowValueIfRowEnabled(CounterpartyInfoRowType.KgtAvgLat, TOPOFBOOKCOLLIDX,
                                    (dealsStatisticsBag.AverageQuoteToOrderLatencyToday.TotalMilliseconds * 1000).ToString("F0"));

            AddRowValueIfRowEnabled(CounterpartyInfoRowType.CounterpartyAvgLat, TOPOFBOOKCOLLIDX,
                                    (dealsStatisticsBag.AverageOrderToDealLatencyToday.TotalMilliseconds).ToString("F1"));

            List<int> counterpartiesIdxWithoutDealsAndRejects = new List<int>();

            foreach (Counterparty enabledCounterparty in _diagnosticsDashboardSettings.EnabledCounterparties)
            {
                int counterpartyIdx = (int) enabledCounterparty;

                if (dealsStatisticsBag.NumberOfDealsPerCounterpartyToday[counterpartyIdx] == 0 &&
                    dealsStatisticsBag.NumberOfCtpRejectionsPerCounterpartyToday[counterpartyIdx] == 0 &&
                    dealsStatisticsBag.NumberOfKGTRejectionsPerCounterpartyToday[counterpartyIdx] == 0)
                {
                    counterpartiesIdxWithoutDealsAndRejects.Add(counterpartyIdx);
                }

                AddRowValueIfRowEnabled(CounterpartyInfoRowType.Deals, _counterpartyCollIndexes[counterpartyIdx],
                                    dealsStatisticsBag.NumberOfDealsPerCounterpartyToday[counterpartyIdx]);

                AddRowValueIfRowEnabled(CounterpartyInfoRowType.KGTRejections, _counterpartyCollIndexes[counterpartyIdx],
                    dealsStatisticsBag.NumberOfKGTRejectionsPerCounterpartyToday[counterpartyIdx]);

                AddRowValueIfRowEnabled(CounterpartyInfoRowType.CtpRejections, _counterpartyCollIndexes[counterpartyIdx],
                    dealsStatisticsBag.NumberOfCtpRejectionsPerCounterpartyToday[counterpartyIdx]);

                AddRowValueIfRowEnabled(CounterpartyInfoRowType.KGTRejectionRate, _counterpartyCollIndexes[counterpartyIdx],
                    (dealsStatisticsBag.KGTRejectionRatePerCounterpartyToday[counterpartyIdx] * 100).ToString("F1"));

                AddRowValueIfRowEnabled(CounterpartyInfoRowType.CtpRejectionRate, _counterpartyCollIndexes[counterpartyIdx],
                    (dealsStatisticsBag.CtpRejectionRatePerCounterpartyToday[counterpartyIdx] * 100).ToString("F1"));

                AddRowValueIfRowEnabled(CounterpartyInfoRowType.AvgDeal, _counterpartyCollIndexes[counterpartyIdx],
                                    (dealsStatisticsBag.AverageDealSizePerCounterpartyToday[counterpartyIdx] / 1000000).ToString("F3"));

                AddRowValueIfRowEnabled(CounterpartyInfoRowType.Volume, _counterpartyCollIndexes[counterpartyIdx],
                                    (dealsStatisticsBag.TotalVolumePerCounterpartyToday[counterpartyIdx] / 1000000).ToString("F1"));

                AddRowValueIfRowEnabled(CounterpartyInfoRowType.VolumeShare, _counterpartyCollIndexes[counterpartyIdx],
                                    (dealsStatisticsBag.VolumeSharePerCounterpartyToday[counterpartyIdx] * 100).ToString("F1"));

                AddRowValueIfRowEnabled(CounterpartyInfoRowType.KgtAvgLat, _counterpartyCollIndexes[counterpartyIdx],
                   (dealsStatisticsBag.AverageQuoteToOrderLatencyPerCounterpartyToday[counterpartyIdx].TotalMilliseconds * 1000).ToString("F0"),
                    //this cell is not sorted and colored for streaming ctps - so need to have white forecolor to be visible
                   dealsStatisticsBag.AverageQuoteToOrderLatencyPerCounterpartyToday[counterpartyIdx].TotalMilliseconds.ToString("# [ms]"), Color.Empty,
                   Counterparty.IsCounterpartyForStreaming(enabledCounterparty) && dealsStatisticsBag.AverageQuoteToOrderLatencyPerCounterpartyToday[counterpartyIdx] != TimeSpan.Zero
                   ? Color.White : Color.Empty);

                AddRowValueIfRowEnabled(CounterpartyInfoRowType.CounterpartyAvgLat, _counterpartyCollIndexes[counterpartyIdx],
                                    (dealsStatisticsBag.AverageOrderToDealLatencyPerCounterpartyToday[counterpartyIdx].TotalMilliseconds).ToString("F1"));
            }

            ColorCounterpartiesSequence(CounterpartyInfoRowType.Deals, dealsStatisticsBag.NumberOfDealsPerCounterpartyToday, counterpartiesIdxWithoutDealsAndRejects, true);
            ColorCounterpartiesSequence(CounterpartyInfoRowType.AvgDeal, dealsStatisticsBag.AverageDealSizePerCounterpartyToday, counterpartiesIdxWithoutDealsAndRejects, true);
            ColorCounterpartiesSequence(CounterpartyInfoRowType.Volume, dealsStatisticsBag.TotalVolumePerCounterpartyToday, counterpartiesIdxWithoutDealsAndRejects, true);
            ColorCounterpartiesSequence(CounterpartyInfoRowType.VolumeShare, dealsStatisticsBag.VolumeSharePerCounterpartyToday, counterpartiesIdxWithoutDealsAndRejects, true);
            ColorCounterpartiesSequence(CounterpartyInfoRowType.CounterpartyAvgLat, dealsStatisticsBag.AverageOrderToDealLatencyPerCounterpartyToday,
                _diagnosticsDashboardSettings.EnabledCounterparties
                .Select(ctp => (int)ctp).Where(idx => dealsStatisticsBag.AverageOrderToDealLatencyPerCounterpartyToday[idx] == TimeSpan.Zero).ToList(), false); 


            //streaming counterparties should not be colored for following stats
            //counterpartiesIdxWithoutOrders.AddRange(Counterparty.Values.Where(Counterparty.IsCounterpartyForStreaming).Select(ctp => (int) ctp));
            ColorCounterpartiesSequence(CounterpartyInfoRowType.KGTRejections, dealsStatisticsBag.NumberOfKGTRejectionsPerCounterpartyToday, counterpartiesIdxWithoutDealsAndRejects.Union(_takingCptIdxes).ToList(), false);
            ColorCounterpartiesSequence(CounterpartyInfoRowType.KGTRejectionRate, dealsStatisticsBag.KGTRejectionRatePerCounterpartyToday, counterpartiesIdxWithoutDealsAndRejects.Union(_takingCptIdxes).ToList(), false);
            ColorCounterpartiesSequence(CounterpartyInfoRowType.CtpRejections, dealsStatisticsBag.NumberOfCtpRejectionsPerCounterpartyToday, counterpartiesIdxWithoutDealsAndRejects.Union(Counterparty.CounterpartiesForStreaming.Select(ctp => (int)ctp)).ToList(), false);
            ColorCounterpartiesSequence(CounterpartyInfoRowType.CtpRejectionRate, dealsStatisticsBag.CtpRejectionRatePerCounterpartyToday, counterpartiesIdxWithoutDealsAndRejects.Union(Counterparty.CounterpartiesForStreaming.Select(ctp => (int)ctp)).ToList(), false);

            ColorCounterpartiesSequence(CounterpartyInfoRowType.KgtAvgLat,
                dealsStatisticsBag.AverageQuoteToOrderLatencyPerCounterpartyToday,
                _diagnosticsDashboardSettings.EnabledCounterparties
                    .Select(ctp => (int) ctp)
                    .Where(
                        idx =>
                            dealsStatisticsBag.AverageQuoteToOrderLatencyPerCounterpartyToday[idx] == TimeSpan.Zero ||
                            Counterparty.IsCounterpartyForStreaming((Counterparty) idx))
                    .ToList(), false);

        }

        private void ColorCounterpartiesSequence<T>(CounterpartyInfoRowType rowType, List<T> values, List<int> counterpartiesIdxWithoutOrders, bool sortReverse)
        {
            int rowIndex = _additionalCounterpartiesInfoRowIndexes[(int)rowType];
            if (rowIndex < 0)
                return;

            int idx = 0;
            Tuple<T, int>[] valuesToSort = values.Select(v => new Tuple<T, int>(v, idx++)).ToArray();

            if (sortReverse)
            {
                Array.Sort(valuesToSort, (tupple1, tupple2) => Comparer<T>.Default.Compare(tupple2.Item1, tupple1.Item1));
            }
            else
            {
                Array.Sort(valuesToSort, (tupple1, tupple2) => Comparer<T>.Default.Compare(tupple1.Item1, tupple2.Item1));
            }
            

            Color[] backColors;
            backColors =
                GetBackColorSequence(
                    //Number of enabled counterparties that have some valid values (orders)
                    //and then get just unique values
                    valuesToSort.Where(tupple => _diagnosticsDashboardSettings.EnabledCounterparties.Contains((Counterparty)tupple.Item2))
                                .Where(tupple => !counterpartiesIdxWithoutOrders.Contains(tupple.Item2))
                                .GroupBy(tupple => tupple.Item1)
                                .Select(grp => grp.First())
                                .Count());

            //now let's recolor
            int backColIdx = 0;
            T previous = default(T);
            bool firstItemColored = false;
            for (int j = 0; j < Counterparty.ValuesCount; j++)
            {
                //Counterparty not enabled for viewing
                if (_counterpartyCollIndexes[valuesToSort[j].Item2] == -1)
                {
                    continue;
                }
                if (counterpartiesIdxWithoutOrders.Contains(valuesToSort[j].Item2))
                {
                    dataGridView1.Rows[rowIndex].Cells[_counterpartyCollIndexes[valuesToSort[j].Item2]].Style.BackColor
                        = Color.Black;
                }
                else
                {
                    if (firstItemColored && !valuesToSort[j].Item1.Equals(previous))
                    {
                        backColIdx++;
                    }
                    dataGridView1.Rows[rowIndex].Cells[_counterpartyCollIndexes[valuesToSort[j].Item2]].Style.BackColor =
                    backColors[backColIdx];
                    firstItemColored = true;
                }

                previous = valuesToSort[j].Item1;
            }
        }

        private void UpdateUI(PriceStreamsMonitoringInfo priceStreamMonitoringInfo)
        {
            try
            {
                UpdateUIInternal(priceStreamMonitoringInfo);
            }
            catch (Exception e)
            {
                HandleExceptionDuringUiRefresh(e);
            }
        }

        private class PriceInfoEntrySpreadComparer: IComparer<WritablePriceStreamInfoEntry>
        {
            public static PriceInfoEntrySpreadComparer Default = new PriceInfoEntrySpreadComparer();

            public int Compare(WritablePriceStreamInfoEntry x, WritablePriceStreamInfoEntry y)
            {
                if (x == y)
                {
                    return 0;
                }

                if (x == null)
                {
                    return int.MinValue;
                }

                if (y == null)
                {
                    return int.MaxValue;
                }

                return Comparer<decimal?>.Default.Compare(x.SpreadBp, y.SpreadBp);
            }
        }

        private void UpdateUIInternal(PriceStreamsMonitoringInfo priceStreamMonitoringInfo)
        {
            List<PriceStreamInfoEntry> priceStreamInfoEntries = priceStreamMonitoringInfo.PriceStreamInfoEntries;

            WritablePriceStreamInfoEntry[][] entrieasArray = new WritablePriceStreamInfoEntry[Symbol.ValuesCount][];

            foreach (Symbol enabledSymbol in this._diagnosticsDashboardSettings.EnabledSymbols)
            {
                entrieasArray[(int)enabledSymbol] = new WritablePriceStreamInfoEntry[Counterparty.ValuesCount];
            }

            bool[] couterpartiesStreamingSomeSymbols = Enumerable.Repeat(false, Counterparty.ValuesCount).ToArray();

            //Insert spread infos and bordering
            foreach (WritablePriceStreamInfoEntry priceStreamInfoEntry in priceStreamInfoEntries)
            {
                if (this._diagnosticsDashboardSettings.EnabledSymbols.Contains(priceStreamInfoEntry.Symbol) && _diagnosticsDashboardSettings.EnabledCounterparties.Contains(priceStreamInfoEntry.Counterparty))
                {
                    entrieasArray[(int) priceStreamInfoEntry.Symbol][(int) priceStreamInfoEntry.Counterparty] =
                        priceStreamInfoEntry as WritablePriceStreamInfoEntry;

                    DataGridViewBorderedCell cell =
                        dataGridView1.Rows[_symbolRowIndexes[(int) priceStreamInfoEntry.Symbol]].Cells[
                            _counterpartyCollIndexes[(int) priceStreamInfoEntry.Counterparty]] as
                        DataGridViewBorderedCell;

                    InsertSpreadInfoIntoCell(cell, priceStreamInfoEntry);

                    if (priceStreamInfoEntry.SpreadBp.HasValue)
                    {
                        couterpartiesStreamingSomeSymbols[(int) priceStreamInfoEntry.Counterparty] = true;
                    }
                }
            }

            bool[] symbolsStreamedByAllBanks = Enumerable.Repeat(false, Symbol.ValuesCount).ToArray();

            //color spreads
            foreach (Symbol enabledSymbol in this._diagnosticsDashboardSettings.EnabledSymbols)
            {
                int symbolIdx = (int) enabledSymbol;
                //set it temporarily to true - unless ther is at least 1 counterparty not streaming this
                symbolsStreamedByAllBanks[symbolIdx] = true;

                foreach (Counterparty enabledCounterparty in _diagnosticsDashboardSettings.EnabledCounterparties)
                {
                    int counterpartyIdx = (int) enabledCounterparty;

                    if (!entrieasArray[symbolIdx][counterpartyIdx].SpreadBp.HasValue)
                    {
                        //If bank is not completely offline (or is not streaming any symbols), remove this symbol from rank calculation
                        if (!priceStreamMonitoringInfo.IsCounterpartyOffline(enabledCounterparty) &&
                            couterpartiesStreamingSomeSymbols[counterpartyIdx])
                        {
                            symbolsStreamedByAllBanks[symbolIdx] = false;
                        }
                    }
                }

                //let's make a local copy - as sortin will move items in place
                WritablePriceStreamInfoEntry[] tempSymbolEntriesArray = new WritablePriceStreamInfoEntry[Counterparty.ValuesCount];
                entrieasArray[symbolIdx].CopyTo(tempSymbolEntriesArray, 0);
                Array.Sort(tempSymbolEntriesArray, PriceInfoEntrySpreadComparer.Default);
                //calculate colors
                Color[] backColors;
                backColors = GetBackColorSequence(tempSymbolEntriesArray.Where(tupple => tupple != null && tupple.SpreadBp.HasValue).GroupBy(tupple => tupple.SpreadBp).Select(grp => grp.First()).Count());

                decimal? previousVal = null;
                int backColorIdx = -1;

                //Aray is not sorted in place so indexes are no more valid - 
                // we need to traverse whole array and skip nulls instead using just EnabledCounterparties
                for (int counterpartyIdx = 0; counterpartyIdx < Counterparty.ValuesCount; counterpartyIdx++)
                {
                    PriceStreamInfoEntry entry = tempSymbolEntriesArray[counterpartyIdx];

                    if (entry != null && entry.SpreadBp.HasValue)
                    {
                        DataGridViewCell cell =
                            dataGridView1.Rows[_symbolRowIndexes[symbolIdx]].Cells[
                                _counterpartyCollIndexes[(int) entry.Counterparty]];

                        if (entry.SpreadBp != previousVal)
                        {
                            backColorIdx++;
                        }

                        cell.Style.BackColor = backColors[backColorIdx];
                        previousVal = entry.SpreadBp;
                    }
                }
            }

            InsertAndColorTopOfBook(priceStreamMonitoringInfo.TopOfBookInfoEntries);

            InsertAndColorTrustInfo(priceStreamMonitoringInfo.SymbolTrustworthinessInfo);

            UpdateSessionsStateUI(priceStreamMonitoringInfo);

            int offlineOrNonstreamingOrNotEnabledCounterpartiesCount =
                Counterparty.Values.Count(
                    counterparty =>
                    !_diagnosticsDashboardSettings.EnabledCounterparties.Contains(counterparty) ||
                    priceStreamMonitoringInfo.IsCounterpartyOffline(counterparty) ||
                    !couterpartiesStreamingSomeSymbols[(int) counterparty]);

            InsertSpreadRankingInfoAndColoring(entrieasArray, symbolsStreamedByAllBanks,
                                         offlineOrNonstreamingOrNotEnabledCounterpartiesCount);

            //UpdateStpFlowLabel(priceStreamMonitoringInfo.StpFlowSessionState);
            UpdateStpFlowLabels(priceStreamMonitoringInfo.StpFlowSessionInfos);

            lblTime.Text = DateTime.UtcNow.ToString();
        }

        static string[] stpFlowStateDescriptions = new string[]{"Disconnected", "Connected - Not subscribed", "Connected - Subscribed"};
        static Color[] stpFlowStateColors = new Color[]{Color.Red, Color.OrangeRed, Color.Green};

        //private void UpdateStpFlowLabel(StpFlowSessionState stpFlowSessionState)
        //{
        //    lblStpFlowState.Text = stpFlowStateDescriptions[(int) stpFlowSessionState];
        //    lblStpFlowState.ForeColor = stpFlowStateColors[(int) stpFlowSessionState];
        //}

        private void UpdateStpFlowLabels(List<StpFlowSessionInfo> stpFlowSessionInfos)
        {
            foreach (Label stpStateLabel in _stpStateLabels)
            {
                StpFlowSessionInfo stpInfo =
                    stpFlowSessionInfos.FirstOrDefault(info => info.StpCounterparty.ToString() == stpStateLabel.Name);

                if (stpInfo != null)
                {
                    stpStateLabel.Text = string.Format("[{0}]: {1}", stpInfo.StpCounterparty, stpFlowStateDescriptions[(int)stpInfo.StpFlowSessionState]);
                    stpStateLabel.ForeColor = stpFlowStateColors[(int)stpInfo.StpFlowSessionState];
                }
            }

            PositionStpLabels();
        }

        private class CounterpartyRankTupple
        {
            public CounterpartyRankTupple(Counterparty counterparty, decimal rank)
            {
                this.Counterparty = counterparty;
                this.Rank = rank;
            }

            public Counterparty Counterparty { get; private set; }
            public decimal Rank { get; set; }
        }

        private void InsertSpreadRankingInfoAndColoring(PriceStreamInfoEntry[][] entrieasArray,
                                                  bool[] symbolsStreamedByAllBanks, int offlineCounterpartiesCount)
        {
            int spreadRankRowIdx = _additionalCounterpartiesInfoRowIndexes[(int)CounterpartyInfoRowType.SpreadRank];

            if(spreadRankRowIdx < 0)
                return;

            //first let's collor everything black;
            foreach (Counterparty enabledCounterparty in _diagnosticsDashboardSettings.EnabledCounterparties)
            {
                int counterpartyIdx = (int)enabledCounterparty;
                dataGridView1.Rows[spreadRankRowIdx].Cells[_counterpartyCollIndexes[counterpartyIdx]].Style.BackColor = _black;
            }

            //If there is no common symbol and so we don't have any ranking data --> than just collor everything black and we're done
            if (!symbolsStreamedByAllBanks.Contains(true))
            {
                return;
            }

            CounterpartyRankTupple[] counterpartiesRank =
                Enumerable.Range(0, Counterparty.ValuesCount)
                          .Select(idx => new CounterpartyRankTupple((Counterparty) idx, int.MaxValue))
                          .ToArray();


            foreach (Symbol enabledSymbol in this._diagnosticsDashboardSettings.EnabledSymbols)
            {
                int symbolIdx = (int) enabledSymbol;

                if (symbolsStreamedByAllBanks[symbolIdx])
                {
                    foreach (Counterparty enabledCounterparty in _diagnosticsDashboardSettings.EnabledCounterparties)
                    {
                        int counterpartyIdx = (int) enabledCounterparty;
                        //int counterpartyIdx = (int) entrieasArray[symbolIdx][j].Counterparty;

                        if (entrieasArray[symbolIdx][counterpartyIdx].SpreadBp.HasValue)
                        {
                            if (counterpartiesRank[counterpartyIdx].Rank == int.MaxValue)
                            {
                                counterpartiesRank[counterpartyIdx].Rank = entrieasArray[symbolIdx][counterpartyIdx].SpreadBp.Value;
                            }
                            else
                            {
                                counterpartiesRank[counterpartyIdx].Rank += entrieasArray[symbolIdx][counterpartyIdx].SpreadBp.Value;
                            }
                        }
                    }
                }
            }

            Array.Sort(counterpartiesRank, (tupple1, tupple2) => decimal.Compare(tupple1.Rank, tupple2.Rank));
            Color[] spreadBackColors;
            spreadBackColors = GetBackColorSequence(counterpartiesRank.Where(t => t.Rank != int.MaxValue).GroupBy(t => t.Rank).Count());

            //now let's recolor just first N ranks (it should be all ranks except for offline banks)
            decimal previousRank = decimal.MinValue;
            int backColorIdx = -1;
            for (int j = 0; j < Counterparty.ValuesCount - offlineCounterpartiesCount; j++)
            {
                CounterpartyRankTupple rankTupple = counterpartiesRank[j];
                DataGridViewCell cell =
                    dataGridView1.Rows[spreadRankRowIdx].Cells[_counterpartyCollIndexes[(int)rankTupple.Counterparty]];

                if (rankTupple.Rank != previousRank)
                {
                    backColorIdx++;
                    previousRank = rankTupple.Rank;
                }

                cell.Style.BackColor = spreadBackColors[backColorIdx];
                cell.Value = backColorIdx + 1;
                cell.ToolTipText = string.Format("Cumulative SpreadBp: {0}", rankTupple.Rank);
            }
        }

        private void InsertAndColorTrustInfo(List<SymbolTrustworthinessInfo> symbolTrustworthinessInfo)
        {
            foreach (Symbol symbol in this._diagnosticsDashboardSettings.EnabledSymbols)
            {
                int symbolIdx = (int) symbol;
                SymbolTrustworthinessInfo trustInfo = symbolTrustworthinessInfo[symbolIdx];

                DataGridViewBorderedCell symbolcell =
                    dataGridView1.Rows[_symbolRowIndexes[symbolIdx]].Cells[0] as DataGridViewBorderedCell;
                DataGridViewBorderedCell trustcell =
                    dataGridView1.Rows[_symbolRowIndexes[symbolIdx]].Cells[TRUSTCOLLIDX] as DataGridViewBorderedCell;

                symbolcell.Style.BackColor = trustInfo.IsTrusted ? Color.Black : Color.Red;
                symbolcell.Style.ForeColor = trustInfo.IsTrusted ? Color.LightGray : Color.Black;
                trustcell.Style.BackColor = trustInfo.IsTrusted ? Color.GreenYellow : Color.Red;

                trustcell.Value = trustInfo.MaxTrustedSpreadBp.ToString("F1");
                trustcell.ToolTipText = string.Format("Max spread: {0:G7}. [{2}]. Last trust change: {1:HH:mm:ss} UTC",
                    trustInfo.MaxTrustedSpread, trustInfo.LastTrustStateChange, trustInfo.LastTrustState);
                symbolcell.ToolTipText = string.Format("{0} from {1:HH:mm:ss} UTC, last state: {2}",
                    trustInfo.IsTrusted ? "Trusted" : "Untrusted", trustInfo.LastTrustStateChange, trustInfo.LastTrustState);
            }
        }

        private void InsertAndColorTopOfBook(List<ToBPriceStreamInfoEntry> topOfBookInfoEntries)
        {
            var topOfBookInfoEntriesForEnabledSymbols =
                topOfBookInfoEntries.Where(entry => this._diagnosticsDashboardSettings.EnabledSymbols.Contains(entry.Symbol)).ToArray();

            //insert ToB spread infos
            int undefinedToBs = 0;
            foreach (ToBPriceStreamInfoEntry priceStreamInfoEntry in topOfBookInfoEntriesForEnabledSymbols)
            {
                DataGridViewBorderedCell spreadcell =
                    dataGridView1.Rows[_symbolRowIndexes[(int)priceStreamInfoEntry.Symbol]].Cells[TOPOFBOOKCOLLIDX] as
                    DataGridViewBorderedCell;
                DataGridViewBorderedCell askcell =
                    dataGridView1.Rows[_symbolRowIndexes[(int)priceStreamInfoEntry.Symbol]].Cells[BESTASKCOLLIDX] as
                    DataGridViewBorderedCell;
                DataGridViewBorderedCell askCptcell =
                    dataGridView1.Rows[_symbolRowIndexes[(int)priceStreamInfoEntry.Symbol]].Cells[BESTASKCPTCOLLIDX] as
                    DataGridViewBorderedCell;
                DataGridViewBorderedCell bidcell =
                    dataGridView1.Rows[_symbolRowIndexes[(int)priceStreamInfoEntry.Symbol]].Cells[BESTBIDCOLLIDX] as
                    DataGridViewBorderedCell;
                DataGridViewBorderedCell bidCptcell =
                    dataGridView1.Rows[_symbolRowIndexes[(int)priceStreamInfoEntry.Symbol]].Cells[BESTBIDCPTCOLLIDX] as
                    DataGridViewBorderedCell;

                //spreadcell.Style.BackColor = Color.White;
                askCptcell.Style.BackColor = priceStreamInfoEntry.AskCounterparty == Counterparty.NULL ? Color.Black : Color.Empty;
                bidCptcell.Style.BackColor = priceStreamInfoEntry.BidCounterparty == Counterparty.NULL ? Color.Black : Color.Empty;
                askCptcell.Value = priceStreamInfoEntry.AskCounterparty;
                bidCptcell.Value = priceStreamInfoEntry.BidCounterparty;
                InsertToBInfoIntoCell(askcell, priceStreamInfoEntry, PriceSide.Ask);
                InsertToBInfoIntoCell(bidcell, priceStreamInfoEntry, PriceSide.Bid);
                InsertSpreadInfoIntoCell(spreadcell, priceStreamInfoEntry);
                

                if (!priceStreamInfoEntry.SpreadBp.HasValue)
                {
                    undefinedToBs++;
                }
            }

            ToBPriceStreamInfoEntry[] tobPriceEntries = topOfBookInfoEntriesForEnabledSymbols;
            Array.Sort(tobPriceEntries, (first, second) => Comparer<decimal?>.Default.Compare(first.SpreadBp, second.SpreadBp));
            Color[] tobBackColors;
            tobBackColors = GetBackColorSequence(tobPriceEntries.Where(entry => entry.SpreadBp.HasValue).GroupBy(entry => entry.SpreadBp).Count());

            decimal? previousVal = null;
            int backColorIdx = -1;
            for (int i = undefinedToBs; i < this._diagnosticsDashboardSettings.EnabledSymbols.Count; i++)
            {
                if (tobPriceEntries[i].SpreadBp.HasValue)
                {
                    if (tobPriceEntries[i].SpreadBp != previousVal)
                    {
                        backColorIdx++;
                    }

                    DataGridViewCell cell =
                        dataGridView1.Rows[_symbolRowIndexes[(int) tobPriceEntries[i].Symbol]].Cells[TOPOFBOOKCOLLIDX];
                    cell.Style.BackColor = tobBackColors[backColorIdx];
                    previousVal = tobPriceEntries[i].SpreadBp;
                }
            }
        }

        private void InsertSpreadInfoIntoCell(DataGridViewBorderedCell cell, PriceStreamInfoEntryBase priceStreamInfoEntry)
        {
            TimeSpan cutOffSpreadAge = this._diagnosticsDashboardSettings.CutOffSpreadAgeLimit;
            TimeSpan thirdSpreadAgeLimit = this._diagnosticsDashboardSettings.ThirdSpreadAgeLimit;

            TimeSpan askAge = priceStreamInfoEntry.LastAskAge;
            TimeSpan bidAge = priceStreamInfoEntry.LastBidAge;

            TimeSpan greaterAge = askAge > bidAge ? askAge : bidAge;

            string ageString = string.Empty;

            if (greaterAge > TimeSpan.Zero)
            {
                ageString = string.Format("{0} / {1}", askAge > cutOffSpreadAge ? "Never" : askAge.ToString(),
                    bidAge > cutOffSpreadAge ? "Never" : bidAge.ToString());
            }

            cell.ToolTipText = ageString;

            this.InsertInfoIntoCell(cell, greaterAge, priceStreamInfoEntry.SpreadBp, "F1");

            if (greaterAge > thirdSpreadAgeLimit)
            {
                priceStreamInfoEntry.ResetSpreadBp();
            }
        }

        private void InsertToBInfoIntoCell(DataGridViewBorderedCell cell, ToBPriceStreamInfoEntry priceStreamInfoEntry, PriceSide side)
        {
            TimeSpan cutOffSpreadAge = this._diagnosticsDashboardSettings.CutOffSpreadAgeLimit;
            TimeSpan thirdSpreadAgeLimit = this._diagnosticsDashboardSettings.ThirdSpreadAgeLimit;

            TimeSpan age = side == PriceSide.Ask ? priceStreamInfoEntry.LastAskAge : priceStreamInfoEntry.LastBidAge;
            string ageString = string.Empty;
            if (age > TimeSpan.Zero)
            {
                ageString = age > cutOffSpreadAge ? "Never" : age.ToString();
            }

            cell.ToolTipText = ageString;

            cell.Style.BackColor = Color.Empty;
            this.InsertInfoIntoCell(cell, age, side == PriceSide.Ask ? priceStreamInfoEntry.Ask : priceStreamInfoEntry.Bid, "G7");
        }

        private void InsertInfoIntoCell(DataGridViewBorderedCell cell, TimeSpan age, decimal? value, string formatSpecifier)
        {
            TimeSpan cutOffSpreadAge = this._diagnosticsDashboardSettings.CutOffSpreadAgeLimit;
            TimeSpan thirdSpreadAgeLimit = this._diagnosticsDashboardSettings.ThirdSpreadAgeLimit;
            TimeSpan firstSpreadAgeLimit = this._diagnosticsDashboardSettings.FirstSpreadAgeLimit;
            TimeSpan secondSpreadAgeLimit = this._diagnosticsDashboardSettings.SecondSpreadAgeLimit;

            cell.Value = string.Empty;

            if (value.HasValue && age < thirdSpreadAgeLimit)
            {
                cell.Value = value.Value.ToString(formatSpecifier);
            }
            else if (value.HasValue && age > thirdSpreadAgeLimit)
            {
                cell.Style.BackColor = Color.FromArgb(40, 40, 40);
            }
            else if (!value.HasValue && age < -cutOffSpreadAge)
            {
                cell.Style.BackColor = Color.Aqua;
            }
            else
            {
                cell.Style.BackColor = _black;
            }

            if (age > thirdSpreadAgeLimit)
            {
                cell.IsBordered = false;
            }
            else if (age > secondSpreadAgeLimit)
            {
                cell.IsBordered = true;
                cell.BorderWidth = 4;
            }
            else if (age > firstSpreadAgeLimit)
            {
                cell.IsBordered = true;
                cell.BorderWidth = 2;
            }
            else
            {
                cell.IsBordered = false;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            using (InfoForm info = new InfoForm(this))
            {
                info.ShowDialog();
                if (info.RestartRequested)
                {
                    RestartApp();
                }
            }
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            if (_vertLabelCell != null && verticalLabel != null)
            {
                if (_vertLabelCell.Displayed == false)
                    verticalLabel.Visible = false;
                else
                    verticalLabel.Visible = true;
            }
        }

        private void butnSettings_Click(object sender, EventArgs e)
        {
            Volatile.Write(ref _freezUIUpdates, true);
            using (Settings settings = new Settings(this._diagnosticsDashboardSettings, this.UpdateSettings, this))
            {
                settings.ShowDialog();
            }
            Volatile.Write(ref _freezUIUpdates, false);
        }

        private void lblStpFlowState_Click(object sender, EventArgs e)
        {
            SessionActionForm saf = new SessionActionForm((sender as Label).Name, _sessionActionTimes, this._diagnosticsDashboardSettings, _businessLogicDiagClient, this);
            saf.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Set window location
            if (Properties.Settings.Default.WindowLocation != null)
            {
                this.Location = Properties.Settings.Default.WindowLocation;
            }

            // Set window size
            if (Properties.Settings.Default.WindowSize != null)
            {
                this.Size = Properties.Settings.Default.WindowSize;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            // Copy window location to app settings
            Properties.Settings.Default.WindowLocation = this.Location;

            // Copy window size to app settings
            if (this.WindowState == FormWindowState.Normal)
            {
                Properties.Settings.Default.WindowSize = this.Size;
            }
            else
            {
                Properties.Settings.Default.WindowSize = this.RestoreBounds.Size;
            }

            // Save settings
            Properties.Settings.Default.Save();

            base.OnClosing(e);
        }
    }

    public enum CounterpartyInfoRowType
    {
        Active,
        MarketData,
        OrderFlow,
        SessionsAndDealsDividerRow,
        Deals,
        FirstDailyInfoRow = Deals,
        KGTRejections,
        CtpRejections,
        KGTRejectionRate,
        CtpRejectionRate,
        AvgDeal,
        Volume,
        VolumeShare,
        KgtAvgLat,
        CounterpartyAvgLat,
        LastDailyInfoRow = CounterpartyAvgLat,
        SpreadRank,
        DealsAndSymbolsDividerRow,
        MaximumItems = DealsAndSymbolsDividerRow + 1
        //MaximumIndex = SpreadRank,
        ////there is one dividing row between header and symbols
        //FirstSymbolIndex = MaximumIndex + 2
    }

    public class RowInfoBag
    {
        private RowInfoBag() { }

        public static IEnumerable<RowInfoBag> RowInfoBagList
        {
            get { return _rowInfoBagList; }
        }

        private static List<RowInfoBag> _rowInfoBagList;

        static RowInfoBag()
        {
            _rowInfoBagList = new List<RowInfoBag>()
                {
                    new RowInfoBag() {RowType = CounterpartyInfoRowType.Active, RowText = "  Is Active", RowToolTip = "Are both sessions configured and not stopped"},
                    new RowInfoBag() {RowType = CounterpartyInfoRowType.MarketData, RowText = "  MarketData Session"},
                    new RowInfoBag() {RowType = CounterpartyInfoRowType.OrderFlow, RowText = "  OrderFlow Session"},

                    new RowInfoBag() {RowType = CounterpartyInfoRowType.SessionsAndDealsDividerRow, RowText="<Divider Row>", IsDivisorRow = true},

                    new RowInfoBag() {RowType = CounterpartyInfoRowType.Deals, RowText = "  Deals"},
                    new RowInfoBag() {RowType = CounterpartyInfoRowType.CtpRejections, RowText = "  Cpt Rejections"},
                    new RowInfoBag() {RowType = CounterpartyInfoRowType.CtpRejectionRate, RowText = "  Cpt Rejection rate [%]"},
                    new RowInfoBag() {RowType = CounterpartyInfoRowType.KGTRejections, RowText = "  KGT Rejections"},
                    new RowInfoBag() {RowType = CounterpartyInfoRowType.KGTRejectionRate, RowText = "  KGT Rejection rate [%]"},
                    new RowInfoBag() {RowType = CounterpartyInfoRowType.AvgDeal, RowText = "  Avg deal [M USD]"},
                    new RowInfoBag() {RowType = CounterpartyInfoRowType.Volume, RowText = "  Volume [M USD]"},
                    new RowInfoBag() {RowType = CounterpartyInfoRowType.VolumeShare, RowText = "  Volume share [%]"},
                    new RowInfoBag() {RowType = CounterpartyInfoRowType.KgtAvgLat, RowText = "  KGT avg lat. [µs]", RowToolTip = "KGT Quote-to-order average latency [µs]"},
                    new RowInfoBag() {RowType = CounterpartyInfoRowType.CounterpartyAvgLat, RowText = "      Counterparty avg lat. [ms]", RowToolTip = "Counterparty order-to-deal average latency [ms]"},
                    new RowInfoBag() {RowType = CounterpartyInfoRowType.SpreadRank, RowText = "SpreadRank"},

                    new RowInfoBag() {RowType = CounterpartyInfoRowType.DealsAndSymbolsDividerRow, RowText = "<Divider Row>",IsDivisorRow = true}
                };
        }

        public CounterpartyInfoRowType RowType { get; private set; }
        public string RowText { get; private set; }
        public string RowToolTip { get; private set; }
        public bool IsDivisorRow { get; private set; }

        //public bool IsEnabled { get; set; }
    }

    public class DataGridViewBorderedCell : DataGridViewTextBoxCell
    {
        public bool IsBordered { get; set; }
        public int BorderWidth { get; set; }

        protected override void Paint(
            Graphics graphics,
            Rectangle clipBounds,
            Rectangle cellBounds,
            int rowIndex,
            DataGridViewElementStates cellState,
            object value,
            object formattedValue,
            string errorText,
            DataGridViewCellStyle cellStyle,
            DataGridViewAdvancedBorderStyle advancedBorderStyle,
            DataGridViewPaintParts paintParts)
        {
            // Call the base class method to paint the default cell appearance.
            base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState,
                value, formattedValue, errorText, cellStyle,
                advancedBorderStyle, paintParts);

            if (IsBordered)
            {
                Rectangle newRect = new Rectangle(cellBounds.X + BorderWidth/2,
                    cellBounds.Y + BorderWidth/2, cellBounds.Width - BorderWidth - 1,
                    cellBounds.Height - BorderWidth - 1);
                using (Pen pen = new Pen(System.Drawing.Color.Black, BorderWidth))
                {
                    graphics.DrawRectangle(pen, newRect);
                }
            }
        }
    }

    public class DataGridViewRolloverCellColumn : DataGridViewColumn
    {
        public DataGridViewRolloverCellColumn()
        {
            this.CellTemplate = new DataGridViewBorderedCell();
        }
    }

    public class VerticalLabel : System.Windows.Forms.Label
    {
        private string _textToDisplay;
        private string _wholeText;

        private System.Windows.Forms.Timer _timer;
        private int _scrll;

        public VerticalLabel()
        {
            _timer = new System.Windows.Forms.Timer();
            _timer.Tick += new EventHandler(this.TimerTick);
            _timer.Interval = 200;
            _timer.Start();

            this.Click += (sender, args) =>
                {
                    if (_timer.Enabled)
                        _timer.Stop();
                    else
                        _timer.Start();
                };
        }

        private void TimerTick(object sender, EventArgs e)
        {
            ScrollLabel();
        }

        private void ScrollLabel()
        {
            _scrll = _scrll == _wholeText.Length ? 0 : _scrll + 1;
            _textToDisplay = _wholeText.Substring(_scrll, _wholeText.Length - _scrll) + _wholeText.Substring(0, _scrll);

            this.Invalidate();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if(_timer != null)
                    _timer.Dispose();
            }

            base.Dispose(disposing);
        }

        public string NewText
        {
            set { this._wholeText = value; }
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            Brush b = new SolidBrush(this.ForeColor);
            e.Graphics.TranslateTransform(0, this.Height);
            e.Graphics.RotateTransform(270);
            e.Graphics.DrawString(this._textToDisplay, this.Font, b, 0, 2);
            base.OnPaint(e);
        }
    }

    public class DataGridViewVertLabelCell : DataGridViewTextBoxCell
    {
        public VerticalLabel VerticalLabel;

        protected override void Paint(
            Graphics graphics,
            Rectangle clipBounds,
            Rectangle cellBounds,
            int rowIndex,
            DataGridViewElementStates cellState,
            object value,
            object formattedValue,
            string errorText,
            DataGridViewCellStyle cellStyle,
            DataGridViewAdvancedBorderStyle advancedBorderStyle,
            DataGridViewPaintParts paintParts)
        {
            if (VerticalLabel != null)
            {
                // Call the base class method to paint the default cell appearance.
                base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState,
                           value, formattedValue, errorText, cellStyle,
                           advancedBorderStyle, paintParts);
                VerticalLabel.Location = new Point(cellBounds.X,
                                                   cellBounds.Y - this.VerticalLabel.Height +
                                                   this.DataGridView.RowTemplate.Height);
            }
        }
    }


}
