﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.RemoteSplitterConsoleRunner
{
    //We need this as SynchronizedCollection doesn't have thread safe enumeration and System.Collections.Concurrent create copy on each enumeartion

    public class ThreadSafeList<T>: IList<T>
    {
        // the (thread-unsafe) collection that actually stores everything
        private readonly List<T> _innerList = new List<T>();
        private readonly ReaderWriterLockSlim _rwLocker = new ReaderWriterLockSlim();

        public int IndexOf(T item)
        {
            _rwLocker.EnterReadLock();

            try
            {
                return _innerList.IndexOf(item);
            }
            finally
            {
                _rwLocker.ExitReadLock();
            }
        }

        public void Insert(int index, T item)
        {
            _rwLocker.EnterWriteLock();

            try
            {
                _innerList.Insert(index, item);
            }
            finally
            {
                _rwLocker.ExitWriteLock();
            }
        }

        public void RemoveAt(int index)
        {
            _rwLocker.EnterWriteLock();

            try
            {
                _innerList.RemoveAt(index);
            }
            finally
            {
                _rwLocker.ExitWriteLock();
            }
        }

        public T this[int index]
        {
            get
            {
                _rwLocker.EnterReadLock();

                try
                {
                    return _innerList[index];
                }
                finally
                {
                    _rwLocker.ExitReadLock();
                }
            }
            set
            {
                _rwLocker.EnterWriteLock();

                try
                {
                    _innerList[index] = value;
                }
                finally
                {
                    _rwLocker.ExitWriteLock();
                }
            }
        }

        public void Add(T item)
        {
            _rwLocker.EnterWriteLock();

            try
            {
                _innerList.Add(item);
            }
            finally
            {
                _rwLocker.ExitWriteLock();
            }
        }

        public void Clear()
        {
            _rwLocker.EnterWriteLock();

            try
            {
                _innerList.Clear();
            }
            finally
            {
                _rwLocker.ExitWriteLock();
            }
        }

        public bool Contains(T item)
        {
            _rwLocker.EnterReadLock();

            try
            {
                return _innerList.Contains(item);
            }
            finally
            {
                _rwLocker.ExitReadLock();
            }
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _rwLocker.EnterReadLock();

            try
            {
                _innerList.CopyTo(array, arrayIndex);
            }
            finally
            {
                _rwLocker.ExitReadLock();
            }
        }

        public int Count
        {
            get
            {
                _rwLocker.EnterReadLock();

                try
                {
                    return _innerList.Count;
                }
                finally
                {
                    _rwLocker.ExitReadLock();
                }
            }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            _rwLocker.EnterWriteLock();

            try
            {
                return _innerList.Remove(item);
            }
            finally
            {
                _rwLocker.ExitWriteLock();
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new SafeEnumerator(() => _innerList.GetEnumerator(), _rwLocker);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }



        public class SafeEnumerator : IEnumerator<T>
        {
            // this is the (thread-unsafe)
            // enumerator of the underlying collection
            private readonly IEnumerator<T> _innerEnumerator;
            // this is the object we shall lock on. 
            private readonly ReaderWriterLockSlim _rwLocker;

            public SafeEnumerator(Func<IEnumerator<T>> getInnerEnumerator, ReaderWriterLockSlim rwLocker)
            {
                _rwLocker = rwLocker;
                // entering lock in constructor
                _rwLocker.EnterReadLock();
                _innerEnumerator = getInnerEnumerator();
            }

            #region Implementation of IDisposable

            public void Dispose()
            {
                // .. and exiting lock on Dispose()
                // This will be called when foreach loop finishes
                _rwLocker.ExitReadLock();
            }

            #endregion

            #region Implementation of IEnumerator

            // we just delegate actual implementation
            // to the inner enumerator, that actually iterates
            // over some collection

            public bool MoveNext()
            {
                return _innerEnumerator.MoveNext();
            }

            public void Reset()
            {
                _innerEnumerator.Reset();
            }

            public T Current
            {
                get { return _innerEnumerator.Current; }
            }

            object IEnumerator.Current
            {
                get { return Current; }
            }

            #endregion
        }
    }
}
