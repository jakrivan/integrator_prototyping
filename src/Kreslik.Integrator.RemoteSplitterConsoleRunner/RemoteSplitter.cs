﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.MessageBus;

namespace Kreslik.Integrator.RemoteSplitterConsoleRunner
{
    public class RemoteTradingClientGatewayProxy: ClientGatewayEventsDispatcherBase
    {
        private ITradingServiceProxy _clientGateway;

        public RemoteTradingClientGatewayProxy(string clientId, ILogger logger, ITradingServiceProxy clientGateway)
            :base(logger, clientId, false)
        {
            this._clientGateway = clientGateway;
        }

        protected override void DispatchPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            this._clientGateway.OnPriceUpdate(priceUpdateEventArgs);
        }

        protected override bool ThrottlePriceUpdateNeeded(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            return this._clientGateway.PriceUpdateThrottlingNeeded(priceUpdateEventArgs);
        }

        protected override void DispatchUnicastInfo(IntegratorInfoObjectBase unicastInfo)
        {
            _clientGateway.OnIntegratorUnicastInfo(unicastInfo);
        }

        protected override void DispatchBroadcastInfo(IntegratorBroadcastInfoBase broadcastInfo)
        {
            _clientGateway.OnIntegratorBroadcastInfo(broadcastInfo);
        }

        protected override void DispatchGatewayClosed_CloseLocalResourcesOnly(string reason)
        {
            //this is it
        }
    }


    public class MulticastsSubscriptionHelper
    {
        private ImmutableArrayWrapper<RemoteTradingClientGatewayProxy>[] _unicastInfoHandlers;
        private ILogger _logger;
        private TradingService[] _tradingServices;
        private object _subscriptionLocker = new object();

        public MulticastsSubscriptionHelper(ILogger logger, TradingService[] tradingServices)
        {
            this._logger = logger;
            this._tradingServices = tradingServices;

            _unicastInfoHandlers =
                new ImmutableArrayWrapper<RemoteTradingClientGatewayProxy>[MulticasInfoUtils.MutlicastEventsCount];
        }

        public void InvalidateTradingServiceReference(TradingService tradingServiceToReset)
        {
            for (int i = 0; i < _tradingServices.Length; i++)
            {
                if (_tradingServices[i] == tradingServiceToReset)
                    _tradingServices[i] = null;
            }
        }

        public void ResetTradingServiceIfInvalidatedAndResubscribe(TradingService tradingService, IntegratorProcessType targetProcessType)
        {
            if(this._tradingServices[(int) targetProcessType] != null)
                return;

            this._tradingServices[(int)targetProcessType] = tradingService;

            List<MulticastRequestInfo> subscriptions = new List<MulticastRequestInfo>();
            lock (_subscriptionLocker)
            {
                for (int idx = 0; idx < MulticasInfoUtils.MutlicastEventsCount; idx++)
                {
                    if (this._unicastInfoHandlers[idx] != null
                                &&
                                !this._unicastInfoHandlers[idx].Empty)
                    {
                        subscriptions.Add(new MulticastRequestInfo(idx, true));
                    }
                }
            }

            foreach (MulticastRequestInfo subscriptionRequestInfo in subscriptions)
            {
                var result = tradingService.SubmitRequest(subscriptionRequestInfo);

                if (!result.RequestSucceeded)
                {
                    this._logger.Log(LogLevel.Fatal,
                                     "Couldn't autoresubscribe to [{0}]. Reason: {1}. Clients can try again",
                                     subscriptionRequestInfo, result.ErrorMessage);

                    lock (_subscriptionLocker)
                    {
                        _unicastInfoHandlers[subscriptionRequestInfo.MulticastTypeIndex].Clear();
                    }
                }
            }
        }

        public IntegratorRequestResult PerformForwardingSubscriptionRequest(ITradingServiceProxy clientGateway,
                                                                     RemoteTradingClientGatewayProxy clientGatewayProxy,
                                                                     MulticastRequestInfo
                                                                         multicastRequestInfo)
        {
            if (multicastRequestInfo.Subscribe)
                return this.Subscribe(clientGateway, clientGatewayProxy, multicastRequestInfo);
            else
                return this.Unsubscribe(clientGateway, clientGatewayProxy, multicastRequestInfo);
        }

        private IntegratorRequestResult Subscribe(
            ITradingServiceProxy clientGateway, RemoteTradingClientGatewayProxy clientGatewayProxy,
            MulticastRequestInfo multicastRequestInfo)
        {
            bool needsSubscription = false;
            IntegratorRequestResult result = IntegratorRequestResult.GetSuccessResult();

            ImmutableArrayWrapper<RemoteTradingClientGatewayProxy> subscribedClientGateways;
            lock (_subscriptionLocker)
            {
                subscribedClientGateways = _unicastInfoHandlers[multicastRequestInfo.MulticastTypeIndex];
                if (subscribedClientGateways == null)
                {
                    subscribedClientGateways = new ImmutableArrayWrapper<RemoteTradingClientGatewayProxy>();
                    _unicastInfoHandlers[multicastRequestInfo.MulticastTypeIndex] = subscribedClientGateways;
                }

                if (subscribedClientGateways.Empty)
                {
                    needsSubscription = true;
                }
            }

            if (needsSubscription)
            {
                //this will block during the entire time of request to the server
                // However it's by design - if other clients requests same pairs in the meantime, they should wait
                lock (subscribedClientGateways)
                {
                    //There was no successful request in the meantime
                    if (subscribedClientGateways.Empty)
                    {
                        subscribedClientGateways.Add(clientGatewayProxy);
                        result = SubmitRequestInfoToIntegrator(multicastRequestInfo);

                        if (!result.RequestSucceeded)
                        {
                            subscribedClientGateways.Remove(clientGatewayProxy);
                            this._logger.Log(LogLevel.Error,
                                             "Couldn't subscribe to [{0}] based on client [{1}] request. Reason: {2}. Clients can try again",
                                             multicastRequestInfo, clientGateway.TradingServerServiceIdentity, result.ErrorMessage);

                            //and try to inform all remote processes about not being subscribed any more
                            SubmitRequestInfoToIntegrator(
                                MulticastRequestInfo.CreateInvertedRequestInfo(multicastRequestInfo));
                        }
                    }
                }
            }
            //If we are already subscribed - don't resubscribe again
            else if (!subscribedClientGateways.Contains(clientGatewayProxy))
            {
                subscribedClientGateways.Add(clientGatewayProxy);
            }

            return result;
        }

        private IntegratorRequestResult SubmitRequestInfoToIntegrator(MulticastRequestInfo multicastRequestInfo)
        {
            IntegratorRequestResult result = null;
            for (int i = 0; i < _tradingServices.Length; i++)
            {
                TradingService tradingServiceLocal = _tradingServices[i];

                if (tradingServiceLocal == null || tradingServiceLocal.IsClosed)
                    result +=
                        IntegratorRequestResult.CreateFailedResult(
                            string.Format(
                                "Attempt to {0}subscribe but {1} server is not available, {0}subscribing just localy",
                                multicastRequestInfo.Subscribe ? string.Empty : "un", (IntegratorProcessType) i));
                else
                    result += tradingServiceLocal.SubmitRequest(multicastRequestInfo);
            }

            return result;
        }

        private IntegratorRequestResult Unsubscribe(ITradingServiceProxy clientGateway, RemoteTradingClientGatewayProxy clientGatewayProxy, MulticastRequestInfo multicastRequestInfo)
        {
            bool needsUnsubscription = false;
            IntegratorRequestResult result = IntegratorRequestResult.GetSuccessResult();

            ImmutableArrayWrapper<RemoteTradingClientGatewayProxy> subscribedClientGateways;
            lock (_subscriptionLocker)
            {
                subscribedClientGateways = _unicastInfoHandlers[multicastRequestInfo.MulticastTypeIndex];

                if (subscribedClientGateways == null || subscribedClientGateways.Empty)
                {
                    string error =
                    String.Format(
                        "Couldn't find active subscription for [{0}], so cannot process request from client [{1}]",
                        multicastRequestInfo, clientGateway.TradingServerServiceIdentity);
                    this._logger.Log(LogLevel.Error, error);
                    result = IntegratorRequestResult.CreateFailedResult(error);
                }
                else
                {
                    subscribedClientGateways.Remove(clientGatewayProxy);
                    needsUnsubscription = subscribedClientGateways.Empty;
                }
            }

            if (needsUnsubscription)
            {
                result = SubmitRequestInfoToIntegrator(multicastRequestInfo);
            }

            return result;
        }


        public void UnsubscribeAll(ITradingServiceProxy clientGateway,
                                   RemoteTradingClientGatewayProxy clientGatewayProxy)
        {
            List<MulticastRequestInfo> subscriptions = new List<MulticastRequestInfo>();
            lock (_subscriptionLocker)
            {
                for (int idx = 0; idx < MulticasInfoUtils.MutlicastEventsCount; idx++)
                {
                    if (this._unicastInfoHandlers[idx] != null
                                &&
                                this._unicastInfoHandlers[idx].Contains(clientGatewayProxy))
                    {
                        subscriptions.Add(new MulticastRequestInfo(idx, false));
                    }
                }
            }

            foreach (MulticastRequestInfo subscriptionRequestInfo in subscriptions)
            {
                this.Unsubscribe(clientGateway, clientGatewayProxy, subscriptionRequestInfo);
            }
        }


        public void OnForwardingInfo(IntegratorMulticastInfo multicastInfo)
        {
            ImmutableArrayWrapper<RemoteTradingClientGatewayProxy> subscribedClientGateways =
                _unicastInfoHandlers[multicastInfo.MulticastTypeIndex];

            if (subscribedClientGateways == null || subscribedClientGateways.Empty)
            {
                this._logger.Log(LogLevel.Warn,
                                 "Receiving new IntegratorMulticastInfo [{0}] but no trading clients are subscribed to this",
                                 multicastInfo);
                return;
            }

            IIndexable<RemoteTradingClientGatewayProxy> clientGatewaysToNotify =
                subscribedClientGateways.IndexableAccessor;

            for (int idx = 0; idx < clientGatewaysToNotify.Length; idx++)
            {
                clientGatewaysToNotify[idx].OnIntegratorUnicastInfo(multicastInfo);
            }
        }
    }

    public delegate IntegratorTransferableSubscriptionRequest CreateSubscriptionRequestInfoDelegate(
        Symbol symbol, Counterparty counterparty, bool subscribe);

    public interface ISubscriptionRoutningHelper
    {
        TradingService TradingService { get; }

        IntegratorProcessType TargetMdServiceType { get; }

        IntegratorRequestResult PerformSubscribtionRequest(ITradingServiceProxy clientGateway,
                                                           RemoteTradingClientGatewayProxy clientGatewayProxy,
                                                           IntegratorTransferableSubscriptionRequest
                                                               subscriptionRequestInfo);
    }

    public class ToBSubscriptionRoutingHelper : ISubscriptionRoutningHelper
    {
        public ToBSubscriptionRoutingHelper(ILogger logger, TradingService tradingService, SubscriptionType subscriptionType, IntegratorProcessType targetMdServiceType)
        {
            this._logger = logger;
            this._tradingService = tradingService;
            this._counterparty = Counterparty.NULL;
            this._subscriptionType = subscriptionType;
            this.TargetMdServiceType = targetMdServiceType;
        }

        public ToBSubscriptionRoutingHelper(ILogger logger, TradingService tradingService, Counterparty counterparty, SubscriptionType subscriptionType, IntegratorProcessType targetMdServiceType)
        {
            this._logger = logger;
            this._tradingService = tradingService;
            this._counterparty = counterparty;
            this._subscriptionType = subscriptionType;
            this.TargetMdServiceType = targetMdServiceType;
        }

        public void InvalidateTradingServiceReference(TradingService tradingServiceToReset)
        {
            if(this._tradingService != tradingServiceToReset)
                return;

            this._tradingService = null;

            _bookTops = new[]
            {
                Enumerable.Range(0, Symbol.ValuesCount).Select(idx => PriceObject.GetNullBid((Symbol) idx)).ToArray(),
                Enumerable.Range(0, Symbol.ValuesCount).Select(idx => PriceObject.GetNullAsk((Symbol) idx)).ToArray()
            };
            _bookTopTimestamps = new[]
            {
                Enumerable.Range(0, Symbol.ValuesCount).Select(idx => DateTime.MinValue).ToArray(),
                Enumerable.Range(0, Symbol.ValuesCount).Select(idx => DateTime.MinValue).ToArray()
            };

            //Todo send prices removed events?
        }

        public void ResetTradingServiceIfInvalidatedAndResubscribe(TradingService tradingService)
        {
            if(this._tradingService != null)
                return;

            this._tradingService = tradingService;


            List<IntegratorTransferableSubscriptionRequest> subscriptions;
            lock (_subscriptionLocker)
            {
                subscriptions = new List<IntegratorTransferableSubscriptionRequest>();
                for (int symbolIdx = 0; symbolIdx < Symbol.ValuesCount; symbolIdx++)
                {
                    if (!_clientSubscriptions[symbolIdx].Empty)
                        subscriptions.Add(CreateSubscriptionRequestInfo((Symbol)symbolIdx, _counterparty, true));
                }
            }

            foreach (IntegratorTransferableSubscriptionRequest subscriptionRequestInfo in subscriptions)
            {
                var result = this._tradingService.SubmitRequest(subscriptionRequestInfo);

                if (!result.RequestSucceeded)
                {
                    this._logger.Log(LogLevel.Fatal,
                                     "Couldn't autoresubscribe to [{0}]. Reason: {1}. Clients can try again",
                                     subscriptionRequestInfo.Symbol, result.ErrorMessage);

                    _clientSubscriptions[(int) subscriptionRequestInfo.Symbol].Clear();
                }
            }
        }

        private ILogger _logger;
        private TradingService _tradingService;
        private Counterparty _counterparty;
        private SubscriptionType _subscriptionType;
        private ImmutableArrayWrapper<RemoteTradingClientGatewayProxy>[] _clientSubscriptions = Enumerable.Range(0, Symbol.ValuesCount).Select(i => new ImmutableArrayWrapper<RemoteTradingClientGatewayProxy>()).ToArray();
        private PriceObject[][] _bookTops =
            {
                Enumerable.Range(0, Symbol.ValuesCount).Select(idx => PriceObject.GetNullBid((Symbol) idx)).ToArray(),
                Enumerable.Range(0, Symbol.ValuesCount).Select(idx => PriceObject.GetNullAsk((Symbol) idx)).ToArray()
            };
        private DateTime[][] _bookTopTimestamps =
            {
                Enumerable.Range(0, Symbol.ValuesCount).Select(idx => DateTime.MinValue).ToArray(),
                Enumerable.Range(0, Symbol.ValuesCount).Select(idx => DateTime.MinValue).ToArray()
            };
        //private bool _isInModeTransferingAllNewPrices = false;
        private object _subscriptionLocker = new object();
        private ManualResetEventSlim _firstPriceUpdateEvent = new ManualResetEventSlim(true);

        public void OnPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            PriceObject priceToCache = PriceUpdateEventArgsHelper.GetPriceIfCachingOfLastPriceRequired(priceUpdateEventArgs);
            if (priceToCache != null)
            {
                //If the client is subscribing now, wait for it to first enqueue last price
                _firstPriceUpdateEvent.Wait(TimeSpan.FromMilliseconds(1));

                //first update internal list, for case of newly subscribing client, so that it can get price immediately
                _bookTops[(int)priceToCache.Side][(int)priceToCache.Symbol] = priceToCache;
                _bookTopTimestamps[(int)priceToCache.Side][(int)priceToCache.Symbol] =
                    priceUpdateEventArgs.IntegratorEventTimeUtc;
            }

            Symbol updateSymbol = PriceUpdateEventArgsHelper.GetSymbolIfPresent(priceUpdateEventArgs);
            IIndexable<RemoteTradingClientGatewayProxy> clientGatewaysToNotify;

            if (updateSymbol == Symbol.NULL)
            {
                //all of them
                clientGatewaysToNotify = new IndexableArrayWrapper<RemoteTradingClientGatewayProxy>(
                    _clientSubscriptions.SelectMany(x => x).Distinct().ToArray());
            }
            else
            {
                ImmutableArrayWrapper<RemoteTradingClientGatewayProxy> subscribedClientGateways
                    = _clientSubscriptions[(int)updateSymbol];

                if (subscribedClientGateways.Empty)
                {
                    this._logger.Log(LogLevel.Warn,
                                     "Receiving new PriceUpdateEventArgs for symbol [{0}] but no trading clients are subscribed to this symbol",
                                     updateSymbol);
                    return;
                }

                clientGatewaysToNotify = subscribedClientGateways.IndexableAccessor;
            }

            for (int idx = 0; idx < clientGatewaysToNotify.Length; idx++)
            {
                clientGatewaysToNotify[idx].OnPriceUpdate(priceUpdateEventArgs);
            }
        }


        private IntegratorTransferableSubscriptionRequest CreateSubscriptionRequestInfo(Symbol symbol, Counterparty counterparty, bool subscribe)
        {
            return new IntegratorTransferableSubscriptionRequest(symbol, counterparty, subscribe, this._subscriptionType);
        }

        public void UnsubscribeAll(ITradingServiceProxy clientGateway, RemoteTradingClientGatewayProxy clientGatewayProxy)
        {
            //unsubscribe
            List<IntegratorTransferableSubscriptionRequest> subscriptions;
            lock (_subscriptionLocker)
            {
                subscriptions = new List<IntegratorTransferableSubscriptionRequest>();
                for (int symbolIdx = 0; symbolIdx < Symbol.ValuesCount; symbolIdx++)
                {
                    if (_clientSubscriptions[symbolIdx].Contains(clientGatewayProxy))
                        subscriptions.Add(CreateSubscriptionRequestInfo((Symbol)symbolIdx, _counterparty, false));
                }
            }

            foreach (IntegratorTransferableSubscriptionRequest subscriptionRequestInfo in subscriptions)
            {
                this.Unsubscribe(clientGateway, clientGatewayProxy, subscriptionRequestInfo);
            }
        }

        public TradingService TradingService 
        {
            get { return this._tradingService; }
        }

        public IntegratorProcessType TargetMdServiceType { get; private set; }

        public IntegratorRequestResult PerformSubscribtionRequest(ITradingServiceProxy clientGateway,
                                                                  RemoteTradingClientGatewayProxy clientGatewayProxy,
                                                                  IntegratorTransferableSubscriptionRequest
                                                                      subscriptionRequestInfo)
        {
            if (subscriptionRequestInfo.Subscribe)
                return this.Subscribe(clientGateway, clientGatewayProxy, subscriptionRequestInfo);
            else
                return this.Unsubscribe(clientGateway, clientGatewayProxy, subscriptionRequestInfo);
        }

        private IntegratorRequestResult Subscribe(ITradingServiceProxy clientGateway, RemoteTradingClientGatewayProxy clientGatewayProxy, IntegratorTransferableSubscriptionRequest subscriptionRequestInfo)
        {
            bool needsSubscription = false;
            IntegratorRequestResult result = IntegratorRequestResult.GetSuccessResult();
            TradingService tradingServiceLocal = _tradingService;

            ImmutableArrayWrapper<RemoteTradingClientGatewayProxy> subscribedClientGateways;
            lock (_subscriptionLocker)
            {
                subscribedClientGateways = _clientSubscriptions[(int) subscriptionRequestInfo.Symbol];
                if (subscribedClientGateways.Empty)
                {
                    needsSubscription = true;
                }
            }

            if (needsSubscription)
            {
                //this will block during the entire time of request to the server
                // However it's by design - if other clients requests same pairs in the meantime, they should wait
                lock (subscribedClientGateways)
                {
                    //There was no successful request in the meantime
                    if (subscribedClientGateways.Empty)
                    {
                        subscribedClientGateways.Add(clientGatewayProxy);
                        result = tradingServiceLocal.SubmitRequest(subscriptionRequestInfo);

                        if (!result.RequestSucceeded)
                        {
                            subscribedClientGateways.Remove(clientGatewayProxy);
                            this._logger.Log(LogLevel.Error,
                                             "Couldn't subscribe to [{0}] based on client [{1}] request. Reason: {2}. Clients can try again",
                                             subscriptionRequestInfo.Symbol, clientGateway.TradingServerServiceIdentity, result.ErrorMessage);
                        }
                    }
                }
            }
            //If we are already subscribed - don't resubscribe again
            else if (!subscribedClientGateways.Contains(clientGatewayProxy))
            {
                //Manual reset event slim Reset is not thread safe (what an irony), therefore locking here
                lock (_firstPriceUpdateEvent)
                {
                    _firstPriceUpdateEvent.Reset();
                    PriceObject lastAsk = _bookTops[(int)PriceSide.Ask][(int)subscriptionRequestInfo.Symbol];
                    DateTime lastAskTimestamp = _bookTopTimestamps[(int)PriceSide.Ask][(int)subscriptionRequestInfo.Symbol];
                    PriceObject lastBid = _bookTops[(int)PriceSide.Bid][(int)subscriptionRequestInfo.Symbol];
                    DateTime lastBidTimestamp = _bookTopTimestamps[(int)PriceSide.Bid][(int)subscriptionRequestInfo.Symbol];
                    this.SendFirstPrice(clientGatewayProxy, lastAsk, lastAskTimestamp);
                    this.SendFirstPrice(clientGatewayProxy, lastBid, lastBidTimestamp);
                    subscribedClientGateways.Add(clientGatewayProxy);
                    _firstPriceUpdateEvent.Set();
                }
            }

            return result;
        }

        private TradingTargetType TradingTargetType
        {
            get
            {
                if (_counterparty == Counterparty.NULL)
                    return TradingTargetType.BankPool;
                else
                    return _counterparty.TradingTargetType;
            }
        }

        private void SendFirstPrice(RemoteTradingClientGatewayProxy clientGatewayProxy, PriceObject price, DateTime timestamp)
        {
            if (price == null || PriceObjectInternal.IsNullPrice(price))
            {
                clientGatewayProxy.OnPriceUpdate(new SinglePriceChangeEventArgs(PriceUpdateType.PriceOnTopOfBookRemoved, timestamp, price, this.TradingTargetType));
            }
            else
            {
                clientGatewayProxy.OnPriceUpdate(new SinglePriceChangeEventArgs(PriceUpdateType.PriceOnTopOfBookAdded, timestamp, price, this.TradingTargetType));
            }
        }

        private IntegratorRequestResult Unsubscribe(ITradingServiceProxy clientGateway, RemoteTradingClientGatewayProxy clientGatewayProxy, IntegratorTransferableSubscriptionRequest unsubscriptionRequestInfo)
        {
            bool needsUnsubscription = false;
            IntegratorRequestResult result = IntegratorRequestResult.GetSuccessResult();

            ImmutableArrayWrapper<RemoteTradingClientGatewayProxy> subscribedClientGateways;
            lock (_subscriptionLocker)
            {
                subscribedClientGateways = _clientSubscriptions[(int) unsubscriptionRequestInfo.Symbol];
                if (subscribedClientGateways.Empty)
                {
                    string error =
                    String.Format(
                        "Couldn't find active subscription for [{0}], so cannot process request from client [{1}]",
                        unsubscriptionRequestInfo.Symbol, clientGateway.TradingServerServiceIdentity);
                    this._logger.Log(LogLevel.Error, error);
                    result = IntegratorRequestResult.CreateFailedResult(error);
                }
                else
                {
                    subscribedClientGateways.Remove(clientGatewayProxy);
                    needsUnsubscription = subscribedClientGateways.Empty;
                }
            }

            if (needsUnsubscription)
            {
                TradingService tradingServiceLocal = _tradingService;

                if (tradingServiceLocal == null || tradingServiceLocal.IsClosed)
                    return
                        IntegratorRequestResult.CreateFailedResult(
                            "Attempt to unsubscribe but server is not available, unsubscribin just localy");

                result = tradingServiceLocal.SubmitRequest(unsubscriptionRequestInfo);
            }

            return result;
        }
    }

    public class SimpleSubscriptionRoutingHelper : ISubscriptionRoutningHelper
    {
        public SimpleSubscriptionRoutingHelper(ILogger logger, TradingService tradingService,
                                               Counterparty counterparty,
                                               SubscriptionType subscriptionType, IntegratorProcessType targetMdServiceType)
        {
            this._logger = logger;
            this._tradingService = tradingService;
            this._counterparty = counterparty;
            this._subscriptionType = subscriptionType;
            this.TargetMdServiceType = targetMdServiceType;
        }

        private ILogger _logger;
        private TradingService _tradingService;
        private Counterparty _counterparty;
        private SubscriptionType _subscriptionType;
        private ImmutableArrayWrapper<RemoteTradingClientGatewayProxy>[] _clientSubscriptions = Enumerable.Range(0, Symbol.ValuesCount).Select(i => new ImmutableArrayWrapper<RemoteTradingClientGatewayProxy>()).ToArray();
        private object _subscriptionLocker = new object();

        public void InvalidateTradingServiceReference(TradingService tradingServiceToReset)
        {
            if(this._tradingService != tradingServiceToReset)
                return;

            this._tradingService = null;
        }

        private IntegratorTransferableSubscriptionRequest CreateSubscriptionRequestInfo(Symbol symbol, Counterparty counterparty, bool subscribe)
        {
            return new IntegratorTransferableSubscriptionRequest(symbol, counterparty, subscribe, this._subscriptionType);
        }

        public void ResetTradingServiceIfInvalidatedAndResubscribe(TradingService tradingService)
        {
            if(this._tradingService != null)
                return;

            this._tradingService = tradingService;

            List<IntegratorTransferableSubscriptionRequest> subscriptions;
            lock (_subscriptionLocker)
            {
                subscriptions = new List<IntegratorTransferableSubscriptionRequest>();
                for (int symbolIdx = 0; symbolIdx < Symbol.ValuesCount; symbolIdx++)
                {
                    if(!_clientSubscriptions[symbolIdx].Empty)
                        subscriptions.Add(CreateSubscriptionRequestInfo((Symbol) symbolIdx, _counterparty, true));
                }
            }

            foreach (IntegratorTransferableSubscriptionRequest subscriptionRequestInfo in subscriptions)
            {
                var result = this._tradingService.SubmitRequest(subscriptionRequestInfo);

                if (!result.RequestSucceeded)
                {
                    this._logger.Log(LogLevel.Fatal,
                                     "Couldn't autoresubscribe to [{0}]. Reason: {1}. Clients can try again",
                                     subscriptionRequestInfo.Symbol, result.ErrorMessage);

                    _clientSubscriptions[(int) subscriptionRequestInfo.Symbol].Clear();
                }
            }
        }

        public void OnPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            Symbol updateSymbol = PriceUpdateEventArgsHelper.GetSymbolIfPresent(priceUpdateEventArgs);
            IIndexable<RemoteTradingClientGatewayProxy> clientGatewaysToNotify;

            if (updateSymbol == Symbol.NULL)
            {
                //all of them
                clientGatewaysToNotify =
                    new IndexableArrayWrapper<RemoteTradingClientGatewayProxy>(
                        _clientSubscriptions.SelectMany(x => x).Distinct().ToArray());
            }
            else
            {
                ImmutableArrayWrapper<RemoteTradingClientGatewayProxy> subscribedClientGateways
                    = _clientSubscriptions[(int) updateSymbol];

                if (subscribedClientGateways.Empty)
                {
                    this._logger.Log(LogLevel.Warn,
                                     "Receiving new PriceUpdateEventArgs for symbol [{0}] but no trading clients are subscribed to this symbol",
                                     updateSymbol);
                    return;
                }

                clientGatewaysToNotify = subscribedClientGateways.IndexableAccessor;
            }

            for (int idx = 0; idx < clientGatewaysToNotify.Length; idx++)
            {
                clientGatewaysToNotify[idx].OnPriceUpdate(priceUpdateEventArgs);
            }
        }

        public void UnsubscribeAll(ITradingServiceProxy clientGateway, RemoteTradingClientGatewayProxy clientGatewayProxy)
        {
            //unsubscribe
            List<IntegratorTransferableSubscriptionRequest> subscriptions;
            lock (_subscriptionLocker)
            {
                subscriptions = new List<IntegratorTransferableSubscriptionRequest>();
                for (int symbolIdx = 0; symbolIdx < Symbol.ValuesCount; symbolIdx++)
                {
                    if (_clientSubscriptions[symbolIdx].Contains(clientGatewayProxy))
                        subscriptions.Add(CreateSubscriptionRequestInfo((Symbol)symbolIdx, _counterparty, false));
                }
            }

            foreach (IntegratorTransferableSubscriptionRequest subscriptionRequestInfo in subscriptions)
            {
                this.Unsubscribe(clientGateway, clientGatewayProxy, subscriptionRequestInfo);
            }
        }

        public TradingService TradingService
        {
            get { return this._tradingService; }
        }

        public IntegratorProcessType TargetMdServiceType { get; private set; }

        public IntegratorRequestResult PerformSubscribtionRequest(ITradingServiceProxy clientGateway,
                                                                  RemoteTradingClientGatewayProxy clientGatewayProxy,
                                                                  IntegratorTransferableSubscriptionRequest
                                                                      subscriptionRequestInfo)
        {
            if (subscriptionRequestInfo.Subscribe)
                return this.Subscribe(clientGateway, clientGatewayProxy, subscriptionRequestInfo);
            else
                return this.Unsubscribe(clientGateway, clientGatewayProxy, subscriptionRequestInfo);
        }

        private IntegratorRequestResult Subscribe(ITradingServiceProxy clientGateway, RemoteTradingClientGatewayProxy clientGatewayProxy, IntegratorTransferableSubscriptionRequest subscriptionRequestInfo)
        {
            bool needsSubscription = false;
            IntegratorRequestResult result = IntegratorRequestResult.GetSuccessResult();
            TradingService tradingServiceLocal = _tradingService;

            ImmutableArrayWrapper<RemoteTradingClientGatewayProxy> subscribedClientGateways;
            lock (_subscriptionLocker)
            {
                subscribedClientGateways = _clientSubscriptions[(int)subscriptionRequestInfo.Symbol];

                if (subscribedClientGateways.Empty)
                {
                    needsSubscription = true;
                }
            }

            if (needsSubscription)
            {
                //this will block during the entire time of request to the server
                // However it's by design - if other clients requests same pairs in the meantime, they should wait
                lock (subscribedClientGateways)
                {
                    //There was no successful request in the meantime
                    if (subscribedClientGateways.Empty)
                    {
                        subscribedClientGateways.Add(clientGatewayProxy);
                        result = tradingServiceLocal.SubmitRequest(subscriptionRequestInfo);

                        if (!result.RequestSucceeded)
                        {
                            subscribedClientGateways.Remove(clientGatewayProxy);
                            this._logger.Log(LogLevel.Error,
                                             "Couldn't subscribe to [{0}] deals based on client [{1}] request. Reason: {2}. Clients can try again",
                                             subscriptionRequestInfo.Symbol, clientGateway.TradingServerServiceIdentity, result.ErrorMessage);
                        }
                    }
                }
            }
            //If we are already subscribed - don't resubscribe again
            else if (!subscribedClientGateways.Contains(clientGatewayProxy))
            {
                subscribedClientGateways.Add(clientGatewayProxy);
            }

            return result;
        }

        private IntegratorRequestResult Unsubscribe(ITradingServiceProxy clientGateway, RemoteTradingClientGatewayProxy clientGatewayProxy, IntegratorTransferableSubscriptionRequest subscriptionRequestInfo)
        {
            bool needsUnsubscription = false;
            IntegratorRequestResult result = IntegratorRequestResult.GetSuccessResult();

            ImmutableArrayWrapper<RemoteTradingClientGatewayProxy> subscribedClientGateways;
            lock (_subscriptionLocker)
            {
                subscribedClientGateways = _clientSubscriptions[(int) subscriptionRequestInfo.Symbol];
                if (subscribedClientGateways.Empty)
                {
                    string error =
                    String.Format(
                        "Couldn't find active subscription for [{0}] deals, so cannot process request from client [{1}]",
                        subscriptionRequestInfo.Symbol, clientGateway.TradingServerServiceIdentity);
                    this._logger.Log(LogLevel.Error, error);
                    result = IntegratorRequestResult.CreateFailedResult(error);
                }
                else
                {
                    subscribedClientGateways.Remove(clientGatewayProxy);
                    needsUnsubscription = subscribedClientGateways.Empty;
                }
            }

            if (needsUnsubscription)
            {
                TradingService tradingServiceLocal = _tradingService;

                if (tradingServiceLocal == null || tradingServiceLocal.IsClosed)
                    return
                        IntegratorRequestResult.CreateFailedResult(
                            "Attempt to unsubscribe but server is not available, unsubscribin just localy");

                result = tradingServiceLocal.SubmitRequest(subscriptionRequestInfo);
            }

            return result;
        }
    }

    public class RemoteSplitter : BroadcastInfosStoreBase, ITradingService, IClientRequestsHandler
    {
        private TradingService _tradingService;
        private TradingService _offloadedMarketdataService;
        private ILogger _logger;

        private ConcurrentDictionary<ITradingServiceProxy, RemoteTradingClientGatewayProxy> _tradingClientGatewayProxies = new ConcurrentDictionary<ITradingServiceProxy, RemoteTradingClientGatewayProxy>();
        private ConcurrentDictionary<string, RemoteTradingClientGatewayProxy> _clientsById = new ConcurrentDictionary<string, RemoteTradingClientGatewayProxy>(); 

        private ToBSubscriptionRoutingHelper[] _toBSubscriptionRoutingHelpers;
        private SimpleSubscriptionRoutingHelper[] _venueDealsSubscriptionHelpers;
        private SimpleSubscriptionRoutingHelper[] _individualPricesSubscriptionHelpers;
        private MulticastsSubscriptionHelper _multicastSubscriptionHelper;

        public RemoteSplitter(string splitterName, ILogger logger)
            :base(logger)
        {
            this._logger = logger;
            _tradingService = TradingService.CreateSplitterService(this, splitterName, logger, new IntegratorInstanceProcessUtils(
                                                       IntegratorProcessType.BusinessLogic), false);
            _tradingService.OnProxyClosing += this.OnIntegratorProxyClosing;
            this.Identifier = _tradingService.Identifier;

            bool hasOffloadingMdProcess =
                SessionsSettings.SubscriptionSettings.EnabledCounterpartsWithMDSessionInSeparateProcess.Any();
            if (hasOffloadingMdProcess)
            {
                _offloadedMarketdataService = TradingService.CreateSplitterService(this, this.Identifier, _logger, new IntegratorInstanceProcessUtils(
                                                       IntegratorProcessType.HotspotMarketData), true);
                _offloadedMarketdataService.OnProxyClosing += this.OnIntegratorProxyClosing;
                this._offloadMdServiceReady = true;
            }

            _toBSubscriptionRoutingHelpers = new ToBSubscriptionRoutingHelper[Counterparty.ValuesCount];
            ToBSubscriptionRoutingHelper banksToBSubscriptionRoutingHelper = new ToBSubscriptionRoutingHelper(logger, _tradingService, SubscriptionType.ToBPricesSubscription, IntegratorProcessType.BusinessLogic);
            foreach (BankCounterparty bankCounterparty in BankCounterparty.Values)
            {
                _toBSubscriptionRoutingHelpers[(int)bankCounterparty] = banksToBSubscriptionRoutingHelper;
            }
            foreach (Counterparty venueCounterparty in Counterparty.Values.Where(c => !BankCounterparty.Values.Select(c1 => (Counterparty)c1).Contains(c)))
            {
                _toBSubscriptionRoutingHelpers[(int)venueCounterparty] = 
                    new ToBSubscriptionRoutingHelper(logger, this.GetMarektDataTradingServiceForCounterparty(venueCounterparty), venueCounterparty, SubscriptionType.ToBPricesSubscription, this.GetTargetMdProcessTypeForCounterparty(venueCounterparty));
            }

            _venueDealsSubscriptionHelpers = new SimpleSubscriptionRoutingHelper[Counterparty.ValuesCount];
            _venueDealsSubscriptionHelpers[(int)HotspotCounterparty.HTA] =
                new SimpleSubscriptionRoutingHelper(logger, this.GetMarektDataTradingServiceForCounterparty(HotspotCounterparty.HTA), HotspotCounterparty.HTA, SubscriptionType.VenueDealsSubscriptions, this.GetTargetMdProcessTypeForCounterparty(HotspotCounterparty.HTA));
            _venueDealsSubscriptionHelpers[(int)HotspotCounterparty.HTF] =
                new SimpleSubscriptionRoutingHelper(logger, this.GetMarektDataTradingServiceForCounterparty(HotspotCounterparty.HTF), HotspotCounterparty.HTF, SubscriptionType.VenueDealsSubscriptions, this.GetTargetMdProcessTypeForCounterparty(HotspotCounterparty.HTF));
            _venueDealsSubscriptionHelpers[(int)HotspotCounterparty.HT3] =
                new SimpleSubscriptionRoutingHelper(logger, this.GetMarektDataTradingServiceForCounterparty(HotspotCounterparty.HT3), HotspotCounterparty.HT3, SubscriptionType.VenueDealsSubscriptions, this.GetTargetMdProcessTypeForCounterparty(HotspotCounterparty.HT3));
            _venueDealsSubscriptionHelpers[(int)HotspotCounterparty.H4T] =
                new SimpleSubscriptionRoutingHelper(logger, this.GetMarektDataTradingServiceForCounterparty(HotspotCounterparty.H4T), HotspotCounterparty.H4T, SubscriptionType.VenueDealsSubscriptions, this.GetTargetMdProcessTypeForCounterparty(HotspotCounterparty.H4T));
            _venueDealsSubscriptionHelpers[(int)HotspotCounterparty.H4M] =
                new SimpleSubscriptionRoutingHelper(logger, this.GetMarektDataTradingServiceForCounterparty(HotspotCounterparty.H4M), HotspotCounterparty.H4M, SubscriptionType.VenueDealsSubscriptions, this.GetTargetMdProcessTypeForCounterparty(HotspotCounterparty.H4M));


            _individualPricesSubscriptionHelpers = new SimpleSubscriptionRoutingHelper[Counterparty.ValuesCount];
            SimpleSubscriptionRoutingHelper banksIndividualPricesSubscriptionHelper = 
                new SimpleSubscriptionRoutingHelper(logger, _tradingService, Counterparty.NULL, SubscriptionType.IndividualPricesSubscription, IntegratorProcessType.BusinessLogic);
            foreach (BankCounterparty bankCounterparty in BankCounterparty.Values)
            {
                //TODO: cannot subscribe to individual prices in offloaded instance
                _individualPricesSubscriptionHelpers[(int)bankCounterparty] = banksIndividualPricesSubscriptionHelper;
            }
            //_individualPricesSubscriptionHelpers[(int)FXallCounterparty.FA1] = new SimpleSubscriptionRoutingHelper(logger, _tradingService, FXallCounterparty.FA1, SubscriptionType.IndividualPricesSubscription);

            TradingService[] allTradingServices = hasOffloadingMdProcess
                ? new TradingService[] {_tradingService, _offloadedMarketdataService}
                : new TradingService[] {_tradingService};
            _multicastSubscriptionHelper = new MulticastsSubscriptionHelper(logger, allTradingServices);

            this._connectionServiceReady = true;
            this.OnIntegratorBroadcastInfo(new ConnectionStateChangeEventArgs(ConnectionServiceState.Running, IntegratorProcessType.BusinessLogic){Reason = "Splitter is ready"});
        }

        private IntegratorProcessType GetTargetMdProcessTypeForCounterparty(Counterparty counterparty)
        {
            if (
                SessionsSettings.SubscriptionSettings.EnabledCounterpartsWithMDSessionInSeparateProcess.Contains(
                    counterparty))
                return IntegratorProcessType.HotspotMarketData;
            else
                return IntegratorProcessType.BusinessLogic;
        }

        private TradingService GetMarektDataTradingServiceForCounterparty(Counterparty counterparty)
        {
            if (
                SessionsSettings.SubscriptionSettings.EnabledCounterpartsWithMDSessionInSeparateProcess.Contains(
                    counterparty))
                return this._offloadedMarketdataService;
            else
                return this._tradingService;
        }

        private bool _connectionServiceReady = false;
        private bool _offloadMdServiceReady = false;

        public bool Closed
        {
            get { return _tradingService.IsClosed || (_offloadedMarketdataService != null && _offloadedMarketdataService.IsClosed); }
        }

        public string Identifier { get; private set; }

        public IntegratorProcessInfo IntegratorProcessInfo
        {
            get { return _tradingService.IntegratorProcessInfo; }
        }

        public BuildVersioningInfo IntegratorVersioningInfo
        {
            get { return _tradingService.ConnectionChainBuildVersioningInfo.IntegratorVersionInfo; }
        }

        //those are received from server
        #region ITradingService implementations

        public void OnPriceUpdate(PriceUpdateEventArgs priceUpdateEventArgs)
        {
            PriceUpdateCategory priceUpdateCategory =
                PriceUpdateEventArgsHelper.GetPriceUpdateEventCategory(priceUpdateEventArgs);

            int helperIndex;
            //if sender is BankPool than do not look on actual counterparty - it might be LMAX as well (as it's part of bankpool)
            if (priceUpdateEventArgs.SenderTradingTargetType == TradingTargetType.BankPool 
                //TODO: HECK: currently only BankPool can subscribe to individual prices; using it to route LMAX individual updates to BankPool
                || priceUpdateCategory == PriceUpdateCategory.IndividualPriceUpdate)
                helperIndex = 0;
            else
            //otherwise do need to look on actual counterparty (e.g. to distinguish ToB update of HTA and HTF)
                helperIndex = (int) priceUpdateEventArgs.Counterparty;

            switch (priceUpdateCategory)
            {
                case PriceUpdateCategory.TobOfBookUpdate:
                    this._toBSubscriptionRoutingHelpers[helperIndex].OnPriceUpdate(priceUpdateEventArgs);
                    return;
                case PriceUpdateCategory.IndividualPriceUpdate:
                    var subscriptionHelper =
                        this._individualPricesSubscriptionHelpers[helperIndex];
                    if (subscriptionHelper != null)
                    {
                        subscriptionHelper.OnPriceUpdate(priceUpdateEventArgs);
                    }
                    else
                    {
                        this._logger.Log(LogLevel.Fatal,
                                 "Receiving Individual Price Update for unexpected counterparty - {0}. Object: {1}",
                                 priceUpdateEventArgs.Counterparty, priceUpdateEventArgs);
                    }
                    return;
                case PriceUpdateCategory.VenueDealUpdate:
                    var dealsSubscriptionHelper =
                        this._venueDealsSubscriptionHelpers[helperIndex];
                    if (dealsSubscriptionHelper != null)
                    {
                        dealsSubscriptionHelper.OnPriceUpdate(priceUpdateEventArgs);
                    }
                    else
                    {
                        this._logger.Log(LogLevel.Fatal,
                                 "Receiving Venue Deal Update for unexpected counterparty - {0}. Object: {1}",
                                 priceUpdateEventArgs.Counterparty, priceUpdateEventArgs);
                    }
                    return;
                case PriceUpdateCategory.Unknow:
                default:
                    this._logger.Log(LogLevel.Fatal,
                                 "Receiving PriceUpdateEventArgs of unexpected type - {0}",
                                 priceUpdateEventArgs);
                    return;
            }
        }

        public void OnIntegratorUnicastInfo(IntegratorInfoObjectBase integratorInfoObject)
        {
            if (integratorInfoObject is IntegratorMulticastInfo)
            {
                this._multicastSubscriptionHelper.OnForwardingInfo(integratorInfoObject as IntegratorMulticastInfo);
                return;
            }

            if (integratorInfoObject is IntegratorTradingInfoObjectBase)
            {
                IntegratorTradingInfoObjectBase integratorTradingInfo =
                    integratorInfoObject as IntegratorTradingInfoObjectBase;

                RemoteTradingClientGatewayProxy clientGatewayProxy;
                if (!_clientsById.TryGetValue(integratorTradingInfo.ClientIdentity, out clientGatewayProxy))
                {
                    this._logger.Log(LogLevel.Error,
                        "Receiving IntegratorInfoObject ({0}) for client [{1}], but trading client is not registered",
                        integratorTradingInfo, integratorTradingInfo.ClientIdentity);
                    return;
                }

                clientGatewayProxy.OnIntegratorUnicastInfo(integratorInfoObject);
            }
            else
            {
                this._logger.Log(LogLevel.Fatal, "Receiving IntegratorInfoObjectBase of unknown type [{0}]: {1}",
                    integratorInfoObject.GetType(), integratorInfoObject);
            }
        }

        public void OnIntegratorBroadcastInfo(IntegratorBroadcastInfoBase integratorInfoObject)
        {
            this.OnNewBroadcastInfo(integratorInfoObject);

            foreach (RemoteTradingClientGatewayProxy clientGatewayProxy in _tradingClientGatewayProxies.Values)
            {
                clientGatewayProxy.OnIntegratorBroadcastInfo(integratorInfoObject);
            }
        }

        
        private void CloseServiceProxy(TradingService tradingServiceToInvalidate)
        {
            if (tradingServiceToInvalidate != null)
                tradingServiceToInvalidate.OnProxyClosing -= this.OnIntegratorProxyClosing;

            if (tradingServiceToInvalidate == this._tradingService)
            {
                //mark this splitter as temporary unavailable for client requests
                Volatile.Write(ref this._connectionServiceReady, false);
                this._tradingService = null;
            }
            else if (tradingServiceToInvalidate == this._offloadedMarketdataService)
            {
                //mark this splitter as temporary unavailable for client subscriptions requests
                Volatile.Write(ref this._offloadMdServiceReady, false);
                this._offloadedMarketdataService = null;
            }
        }

        private void ReconnectServiceProxy(TradingService tradingServiceToReconnect, IntegratorProcessType initiatingIntegratorProcessType)
        {
            if (initiatingIntegratorProcessType == IntegratorProcessType.BusinessLogic)
            {
                //mark this splitter as temporary unavailable for client requests
                Volatile.Write(ref this._connectionServiceReady, true);
                this._tradingService = tradingServiceToReconnect;
            }
            else if (initiatingIntegratorProcessType == IntegratorProcessType.HotspotMarketData)
            {
                //mark this splitter as temporary unavailable for client subscriptions requests
                Volatile.Write(ref this._offloadMdServiceReady, true);
                this._offloadedMarketdataService = tradingServiceToReconnect;
            }
        }

        //this is coming from Integrator server
        private void OnIntegratorProxyClosing(ITradingServiceEndpoint sender, string reason, bool isLocalOnlyRequest)
        {
            if (isLocalOnlyRequest)
            {
                this._logger.Log(LogLevel.Warn,
                                 "Receiving proxy closing event (Reason: {0}), which is marked as local only => ignoring",
                                 reason);
                return;
            }

            IntegratorProcessType initiatingIntegratorProcessType;

            if(sender == _tradingService)
                initiatingIntegratorProcessType = IntegratorProcessType.BusinessLogic;
            else if(sender == _offloadedMarketdataService)
                initiatingIntegratorProcessType = IntegratorProcessType.HotspotMarketData;
            else
            {
                this._logger.Log(LogLevel.Fatal, "Receiving Closing event from unknown TradingSerice - don't knowhow to proceed, so ignoring");
                return;
            }

            TradingService tradingServiceToReset = sender as TradingService;

            this.OnIntegratorBroadcastInfo(new ConnectionStateChangeEventArgs(ConnectionServiceState.ShutDown, initiatingIntegratorProcessType)
                {
                    Reason = string.Format("Server disconnected, reason: {0}", reason)
                });

            //invalidate reference to tradingservice
            {
                //invalidate tradingservice reference
                foreach (
                    ToBSubscriptionRoutingHelper toBSubscriptionRoutingHelper in
                        _toBSubscriptionRoutingHelpers.Where(o => o != null).Distinct())
                {
                    toBSubscriptionRoutingHelper.InvalidateTradingServiceReference(tradingServiceToReset);
                }

                //invalidate tradingservice reference from venue deals
                foreach (
                    SimpleSubscriptionRoutingHelper dealsSubscriptionRoutingHelper in
                        _venueDealsSubscriptionHelpers.Where(o => o != null).Distinct())
                {
                    dealsSubscriptionRoutingHelper.InvalidateTradingServiceReference(tradingServiceToReset);
                }

                //invalidate tradingservice reference from individual prices
                foreach (
                    SimpleSubscriptionRoutingHelper pricesSubscriptionRoutingHelper in
                        _individualPricesSubscriptionHelpers.Where(o => o != null).Distinct())
                {
                    pricesSubscriptionRoutingHelper.InvalidateTradingServiceReference(tradingServiceToReset);
                }

                _multicastSubscriptionHelper.InvalidateTradingServiceReference(tradingServiceToReset);
            }

            this.CloseServiceProxy(tradingServiceToReset);

            tradingServiceToReset = TradingService.CreateSplitterService(this, this.Identifier, this._logger, new IntegratorInstanceProcessUtils(
                                                       initiatingIntegratorProcessType), true);
            tradingServiceToReset.OnProxyClosing += this.OnIntegratorProxyClosing;

            //We could have get contract mismatch or so
            if (tradingServiceToReset.IsClosed)
            {
                this._logger.Log(LogLevel.Fatal, "Couldn't reconnect to Integrator ({0}), shuting the splitter and clients", initiatingIntegratorProcessType);
                //TODO: alter this if disconnection after failed hotspot data reconnect is not desired
                this.CloseLocalResourcesOnly_ForLocalCallersOnly(string.Format("Could not reconnect to Integrator ({0})", initiatingIntegratorProcessType));
                return;
            }

            this.OnIntegratorBroadcastInfo(new ConnectionStateChangeEventArgs(ConnectionServiceState.Reconnected, initiatingIntegratorProcessType)
                {
                    Reason = "Splitter is reconnected and is initiating resubscription on clients behalf"
                });

            //perform all resubscriptions - but only once per helper!
            {
                //invalidate tradingservice reference
                foreach (
                    ToBSubscriptionRoutingHelper toBSubscriptionRoutingHelper in
                        _toBSubscriptionRoutingHelpers.Where(o => o != null && o.TargetMdServiceType == initiatingIntegratorProcessType).Distinct())
                {
                    toBSubscriptionRoutingHelper.ResetTradingServiceIfInvalidatedAndResubscribe(tradingServiceToReset);
                }

                //invalidate tradingservice reference from venue deals
                foreach (
                    SimpleSubscriptionRoutingHelper dealsSubscriptionRoutingHelper in
                        _venueDealsSubscriptionHelpers.Where(o => o != null && o.TargetMdServiceType == initiatingIntegratorProcessType).Distinct())
                {
                    dealsSubscriptionRoutingHelper.ResetTradingServiceIfInvalidatedAndResubscribe(tradingServiceToReset);
                }

                //invalidate tradingservice reference from individual prices
                foreach (
                    SimpleSubscriptionRoutingHelper pricesSubscriptionRoutingHelper in
                        _individualPricesSubscriptionHelpers.Where(o => o != null && o.TargetMdServiceType == initiatingIntegratorProcessType).Distinct())
                {
                    pricesSubscriptionRoutingHelper.ResetTradingServiceIfInvalidatedAndResubscribe(tradingServiceToReset);
                }

                _multicastSubscriptionHelper.ResetTradingServiceIfInvalidatedAndResubscribe(tradingServiceToReset, initiatingIntegratorProcessType);
            }

            ReconnectServiceProxy(tradingServiceToReset, initiatingIntegratorProcessType);

            this.OnIntegratorBroadcastInfo(new ConnectionStateChangeEventArgs(ConnectionServiceState.Running, initiatingIntegratorProcessType) { Reason = "Splitter is ready" });
        }

        //this should be comming only from remote request after shutting connection
        public void CloseLocalResourcesOnly(string reason)
        {
            this._logger.Log(LogLevel.Warn,
                             "Got signal from server to close. Reason: {0}. Not processing this further as reconnect logic probably took place",
                             reason);

            //this object is not usable any more anyway, so not disposing other internals
        }

        private void CloseLocalResourcesOnly_ForLocalCallersOnly(string reason)
        {
            this._logger.Log(LogLevel.Warn, "Got signal from server to close. Passing this to clients. Reason: {0}", reason);

            foreach (RemoteTradingClientGatewayProxy clientGatewayProxy in _tradingClientGatewayProxies.Values)
            {
                //this will finish the local queues processing and will close them for adding
                clientGatewayProxy.CloseLocalResourcesOnly(reason);
            }

            foreach (ITradingServiceProxy tradingServiceProxy in _tradingClientGatewayProxies.Keys)
            {
                //this will signal disconect to remote trading client services
                tradingServiceProxy.CloseLocalResourcesOnly(reason);
            }

            this.CloseServiceProxy(this._tradingService);
            this.CloseServiceProxy(this._offloadedMarketdataService);

            //this object is not usable any more anyway, so not disposing other internals
        }

        //public void AnnounceStateChange(CommunicationStateChangeEventArgs eventArgs)
        //{
        //    throw new NotImplementedException();
        //}

        #endregion /ITradingService implementations

        #region IClientRequestsHandler implementations

        public void RegisterClientGateway(ITradingServiceProxy clientGateway)
        {
            //Allow to connect clients in disconnected status
            //if (!this._connectionServiceReady)
            //{
            //    throw new Exception(
            //        "Attempt to register client to a proxy that is in closed state (as it disconnected itself from integrator)");
            //}

            RemoteTradingClientGatewayProxy remoteTradingClientGatewayProxy = new RemoteTradingClientGatewayProxy(clientGateway.TradingServerServiceIdentity, _logger, clientGateway);
            _tradingClientGatewayProxies[clientGateway] = remoteTradingClientGatewayProxy;
            clientGateway.OnDisconnected += reason => this.FinalizeClientGateway(clientGateway, reason);


            if (!_clientsById.TryAdd(clientGateway.TradingServerServiceIdentity, remoteTradingClientGatewayProxy))
            {
                this._logger.Log(LogLevel.Error, "Failed to add client [{0}] into remote proxy dictionary - same id is probably already used", clientGateway.TradingServerServiceIdentity);

                throw new Exception(
                    string.Format(
                        "Failed to add client [{0}] into remote proxy dictionary - same id is probably already used",
                        clientGateway.TradingServerServiceIdentity));
            }

            foreach (IntegratorBroadcastInfoBase integratorBroadcastInfoBase in this.ExistingInfos)
            {
                clientGateway.OnIntegratorBroadcastInfo(integratorBroadcastInfoBase);
            }
        }

        public void UnregisterClientGateway(ITradingServiceProxy clientGateway)
        {
            //We will get unregister in the OnDisconnested event
            //this.FinalizeClientGateway(clientGateway, "Client is unregistering");
        }

        private void FinalizeClientGateway(ITradingServiceProxy clientGateway, string reason)
        {
            RemoteTradingClientGatewayProxy clientGatewayProxy;

            if (_tradingClientGatewayProxies.TryGetValue(clientGateway, out clientGatewayProxy))
            {
                if (this._connectionServiceReady)
                {
                    //cancel orders
                    _tradingService.SubmitRequest(new IntegratorCancelAllOrdersForSingleClientRequestInfo(clientGateway.TradingServerServiceIdentity));
                }

                //stopping queueu
                _tradingClientGatewayProxies[clientGateway].CloseLocalResourcesOnly(reason);

                //unsubscribe
                foreach (ToBSubscriptionRoutingHelper toBSubscriptionRoutingHelper in _toBSubscriptionRoutingHelpers.Where(o => o != null))
                {
                    toBSubscriptionRoutingHelper.UnsubscribeAll(clientGateway, clientGatewayProxy);
                }

                //unsubscribe from venue deals
                foreach (SimpleSubscriptionRoutingHelper dealsSubscriptionRoutingHelper in _venueDealsSubscriptionHelpers.Where(o => o != null))
                {
                    dealsSubscriptionRoutingHelper.UnsubscribeAll(clientGateway, clientGatewayProxy);
                }

                //unsubscribe from individual prices
                foreach (SimpleSubscriptionRoutingHelper pricesSubscriptionRoutingHelper in _individualPricesSubscriptionHelpers.Where(o => o != null))
                {
                    pricesSubscriptionRoutingHelper.UnsubscribeAll(clientGateway, clientGatewayProxy);
                }

                this._multicastSubscriptionHelper.UnsubscribeAll(clientGateway, clientGatewayProxy);

                RemoteTradingClientGatewayProxy unused;
                if (!_clientsById.TryRemove(clientGateway.TradingServerServiceIdentity, out unused))
                {
                    this._logger.Log(LogLevel.Fatal,
                                     "Unregistering client [{0}], but it is already not in internal lists",
                                     clientGateway.TradingServerServiceIdentity);
                }

                _tradingClientGatewayProxies.TryRemove(clientGateway, out unused);
            }
        }

        private bool IsMdServiceReady(TradingService tradingService)
        {
            if (tradingService == this._tradingService)
                return this._connectionServiceReady;
            else if (tradingService == this._offloadedMarketdataService)
                return this._offloadMdServiceReady;
            else
            {
                this._logger.Log(LogLevel.Fatal, "Unexpected TradingService called - not know");
                return false;
            }
        }

        private IntegratorRequestResult PerformSubscriptionRequest(ITradingServiceProxy clientGateway, IntegratorTransferableSubscriptionRequest subscriptionRequestInfo)
        {
            if (!this._connectionServiceReady)
            {
                return
                    IntegratorRequestResult.CreateFailedResult("Splitter is not in ready state, so it cannot accept client requests");
            }

            RemoteTradingClientGatewayProxy clientGatewayProxy = _tradingClientGatewayProxies[clientGateway];
            ISubscriptionRoutningHelper subscriptionRoutningHelper;

            if (subscriptionRequestInfo.SubscriptionType == SubscriptionType.ToBPricesSubscription)
            {
                int idx = subscriptionRequestInfo.Counterparty == Counterparty.NULL ? 0 : (int)subscriptionRequestInfo.Counterparty;

                subscriptionRoutningHelper = this._toBSubscriptionRoutingHelpers[idx];
            }
            else if (subscriptionRequestInfo.SubscriptionType == SubscriptionType.VenueDealsSubscriptions)
            {
                subscriptionRoutningHelper = subscriptionRequestInfo.Counterparty == Counterparty.NULL
                                 ? null
                                 : this._venueDealsSubscriptionHelpers[(int)subscriptionRequestInfo.Counterparty];

                if (subscriptionRoutningHelper == null)
                {
                    string error =
                        string.Format(
                            "Receiving (un)subscribe request for VenueDeals for unexpected counterparty - {0}. Subscription: {1}",
                            subscriptionRequestInfo.Counterparty, subscriptionRequestInfo);
                    this._logger.Log(LogLevel.Fatal, error);
                    return IntegratorRequestResult.CreateFailedResult(error);
                }
            }
            else if (subscriptionRequestInfo.SubscriptionType == SubscriptionType.IndividualPricesSubscription)
            {
                if (!_tradingService.IsConnectedLocally)
                {
                    return IntegratorRequestResult.CreateFailedResult("Not connected localy (only localy running clients can subscribe to all individual prices consumption)");
                }

                int idx = subscriptionRequestInfo.Counterparty == Counterparty.NULL ? 0 : (int)subscriptionRequestInfo.Counterparty;
                if (idx > _individualPricesSubscriptionHelpers.Length ||
                    _individualPricesSubscriptionHelpers[idx] == null)
                {
                    string error =
                        string.Format(
                            "Receiving Subscribe request for Individual Prices for unexpected counterparty - {0}. Subscription: {1}",
                            subscriptionRequestInfo.Counterparty, subscriptionRequestInfo);
                    this._logger.Log(LogLevel.Fatal, error);
                    return IntegratorRequestResult.CreateFailedResult(error);
                }
                else
                {
                    subscriptionRoutningHelper = this._individualPricesSubscriptionHelpers[idx];
                }
            }
            else
            {
                string error =
                        string.Format(
                            "Receiving unexpected type of (un)subscription: {0}", subscriptionRequestInfo);
                this._logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }

            if(!this.IsMdServiceReady(subscriptionRoutningHelper.TradingService))
                return
                    IntegratorRequestResult.CreateFailedResult("Splitter connection to needed MD provider is not in ready state, so it cannot accept client requests");

            return subscriptionRoutningHelper.PerformSubscribtionRequest(clientGateway, clientGatewayProxy,
                                                                         subscriptionRequestInfo);
        }

        public IntegratorRequestResult SubmitRequest(ITradingServiceProxy clientGateway, IntegratorRequestInfo requestInfo)
        {
            if (!this._connectionServiceReady)
            {
                return
                    IntegratorRequestResult.CreateFailedResult("Splitter is not in ready state, so it cannot accept client requests");
            }


            if (requestInfo is ClientOrderBase || 
                requestInfo is CancelOrderRequestInfo ||
                requestInfo is IntegratorCancelAllOrdersForSingleClientRequestInfo /*||
                requestInfo is IntegratorDealInternalFinalizationRequest*/)
            {
                return _tradingService.SubmitRequest(requestInfo);
            }
            else if(requestInfo is MulticastRequestInfo)
            {
                return _multicastSubscriptionHelper.PerformForwardingSubscriptionRequest(
                    clientGateway, _tradingClientGatewayProxies[clientGateway], requestInfo as MulticastRequestInfo);
            }
            else if (requestInfo is IntegratorTransferableSubscriptionRequest)
            {
                IntegratorTransferableSubscriptionRequest integratorSubscriptionRequestInfo =
                    requestInfo as IntegratorTransferableSubscriptionRequest;

                return this.PerformSubscriptionRequest(clientGateway, integratorSubscriptionRequestInfo);

            }
            else
            {
                string error = string.Format(
                    "Receiving IntegratorRequestInfo of unknown type: {0}, info: {1}. Splitter will not route this request",
                    requestInfo.GetType(), requestInfo);
                _logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }
        }

        //This is used to close the whole proxy (splitter) - tear EVERYTHING down
        public void CloseAndSignalDisconnect()
        {
            //small chance for race condition, but this would anyway happen during splitter teardown
            if(_tradingService != null)
                _tradingService.CloseProxyAndSignalDisconnect("RemoteSplitter shutdown was requested");

            this.CloseLocalResourcesOnly_ForLocalCallersOnly("RemoteSplitter shutdown was requested");
        }

        #endregion /IClientRequestsHandler implementations
    }    
}
