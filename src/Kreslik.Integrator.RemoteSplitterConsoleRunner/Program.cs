﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.MessageBus;

namespace Kreslik.Integrator.RemoteSplitterConsoleRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            string splitterName;
            if (args.Length > 0)
            {
                splitterName = args[0];
            }
            else
            {
                splitterName = Environment.MachineName;
            }

            Console.WriteLine("Splitter name: {0}. This can be specified as first command line argument", splitterName);
            Console.WriteLine("Starting RUN action. Details will be in log files");
            Console.WriteLine("Action will be started by key press and will be closed by two key press. Please press a key twice...");

            Console.ReadKey();
            Console.ReadKey();


            ILogFactory logFactory = LogFactory.Instance;
            ILogger splitterLogger = logFactory.GetLogger("RemoteSplitter");

            RemoteSplitter remoteSplitter = new RemoteSplitter(splitterName, splitterLogger);

            if (remoteSplitter.Closed)
            {
                splitterLogger.Log(LogLevel.Error, "Couldn't start the remote splitter (was the integrator running?). Exiting...");
                Console.WriteLine("Pres key to exit.");
                Console.ReadKey();
                return;
            }

            string connectionInfo = string.Format("Splitter registered as: [{0}], exposed pipename: {3} (and connected to {1}, price transfer mode: {2}{4})",
                remoteSplitter.Identifier, remoteSplitter.IntegratorProcessInfo.GetTargetHost(MessageBusSettings.Sections.IntegratorEndpointConnectionInfo.ConnectionType),
                MessageBusSettings.Sections.PriceTransferBehavior.PriceStreamTransferMode,
                MessageBusSettings.Sections.ClientConnectionInfo.IntegratorServicePipeName,
                MessageBusSettings.Sections.PriceTransferBehavior.PriceStreamTransferMode == PriceStreamTransferMode.Reliable
                    ? string.Empty
                    : string.Format(", Per symbol throttling period: {0}",
                                    MessageBusSettings.Sections.PriceTransferBehavior.PerSymbolPriceThrottlingInterval));

            Console.WriteLine(connectionInfo);
            splitterLogger.Log(LogLevel.Info, connectionInfo);

            ConnectionChainBuildVersioningInfo connectionChainBuildVersioningInfo = new ConnectionChainBuildVersioningInfo(null, new BuildVersioningInfo(BuildConstants.CURRENT_BUILD_VERSION,
                                    BuildConstants.MINIMUM_VERSION_NUMBER_OF_COMPATIBLE_BUILD,
                                    BuildConstants.CURRENT_VERSION_CREATED_UTC), remoteSplitter.IntegratorVersioningInfo);

            string versioningInfo = string.Format("Current connection chain: {0}", connectionChainBuildVersioningInfo);
            Console.WriteLine(versioningInfo);
            splitterLogger.Log(LogLevel.Info, versioningInfo);

            IntegratorService integratorService = IntegratorService.CreateSplitterService(remoteSplitter, splitterLogger,
                                                                        remoteSplitter.Identifier,
                                                                        connectionChainBuildVersioningInfo);
            integratorService.AnnounceCommunicationServiceStateChange(new CommunicationStateChangeEventArgs(CommunicationServiceStateAction.Start));
            //integratorService.StartListening(MessageBusType.NamedPipe);

            Console.ReadKey();
            Console.WriteLine("Pres key once more to exit the running action");
            Console.ReadKey();
            Console.WriteLine("Exiting the running integrator");

            integratorService.AnnounceCommunicationServiceStateChange(new CommunicationStateChangeEventArgs(CommunicationServiceStateAction.InitiateShutdown));
            integratorService.AnnounceCommunicationServiceStateChange(new CommunicationStateChangeEventArgs(CommunicationServiceStateAction.ShutDown));
        }
    }
}
