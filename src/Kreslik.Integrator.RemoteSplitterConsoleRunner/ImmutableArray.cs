﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.RemoteSplitterConsoleRunner
{
    public interface IIndexable<out T>
    {
        int Length { get; }
        T this[int index] { get; }
    }

    public class IndexableArrayWrapper<T>: IIndexable<T>
    {
        private T[] _data;

        public IndexableArrayWrapper(T[] data)
        {
            this._data = data;
        }

        public int Length 
        {
            get { return this._data.Length; }
        }

        public T this[int index]
        {
            get { return this._data[index]; }
        }
    }

    public class ImmutableArrayWrapper<T> : IEnumerable<T> where T : class
    {
        private ImmutableArray<T> _immutableArray = ImmutableArray<T>.CreateEmpty();
        //private object _locker = new object();

        public void Add(T item)
        {
            _immutableArray = _immutableArray.Add(item);
        }

        public void Remove(T item)
        {
            _immutableArray = _immutableArray.Remove(item);
        }

        public void Clear()
        {
            _immutableArray = ImmutableArray<T>.CreateEmpty();
        }

        public bool Empty
        {
            get { return this._immutableArray.Empty; }
        }

        public bool Contains(T item)
        {
            return this._immutableArray.Contains(item); 
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _immutableArray.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Gets indexable acccessor - it's snapshot in time. so it's safe to use it for traversal, but
        ///  it may refer to historic version of structure
        /// </summary>
        public IIndexable<T> IndexableAccessor
        {
            get { return this._immutableArray; }
        }
    }

    //T constrained to class in order to be able to use '=='
    // if this will be needed also for value types, then we can constraint just to IEquitable<T>
    public class ImmutableArray<T> : IEnumerable<T>, IIndexable<T> where T : class
    {
        private T[] _data;

        private ImmutableArray(T[] data)
        {
            this._data = data;
        }

        public static ImmutableArray<T> CreateEmpty()
        {
            return new ImmutableArray<T>(new T[0]);
        }

        public ImmutableArray<T> Add(T item)
        {
            T[] newData = new T[this._data.Length + 1];
            Array.Copy(this._data, newData, this._data.Length);
            newData[this._data.Length] = item;
            return new ImmutableArray<T>(newData);
        }

        public ImmutableArray<T> Remove(T item)
        {
            int idxToRemove;
            for (idxToRemove = 0; idxToRemove < this._data.Length; idxToRemove++)
            {
                if (this._data[idxToRemove] == item)
                    break;
            }

            if (idxToRemove < this._data.Length)
            {
                T[] newData = new T[this._data.Length - 1];
                Array.Copy(this._data, newData, idxToRemove);
                Array.Copy(this._data, idxToRemove + 1, newData, idxToRemove, this._data.Length - idxToRemove - 1);
                return new ImmutableArray<T>(newData);
            }
            else
            {
                return this;
            }
        }

        public bool Empty
        {
            get { return this._data.Length == 0; }
        }

        public bool Contains(T item)
        {
            for (int idxToRemove = 0; idxToRemove < this._data.Length; idxToRemove++)
            {
                if (this._data[idxToRemove] == item)
                    return true;
            }

            return false;
        }

        public int Length { get { return this._data.Length; } }

        public T this[int index]
        {
            get { return this._data[index]; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new ImmutableArrayEnumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        internal class ImmutableArrayEnumerator : IEnumerator<T>
        {
            private readonly T[] _dataSnapshot;
            private int _index = -1;

            public ImmutableArrayEnumerator(ImmutableArray<T> array)
            {
                this._dataSnapshot = array._data;
            }

            #region IEnumerator<T> Membri di

            public T Current
            {
                get { return this._dataSnapshot[_index]; }
            }

            #endregion

            #region IDisposable Membri di

            public void Dispose()
            { }

            #endregion

            #region IEnumerator Membri di

            object IEnumerator.Current
            {
                get { return this.Current; }
            }

            public bool MoveNext()
            {
                _index++;
                return (_index < this._dataSnapshot.Length);
            }

            public void Reset()
            {
                _index = -1;
            }

            #endregion
        }
    }
}
