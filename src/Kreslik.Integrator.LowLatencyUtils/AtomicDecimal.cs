﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public struct AtomicDecimal
    {
        //1 MLD - 9 decimal places behind decimal point
        public const ulong CONST_DIVISOR = 1000000000;
        //1 MLD - 9 decimal places behind decimal point
        public const int CONST_SCALE = 9;
        //10 MLD - 10 decimal places before deciaml point
        public const ulong MAX_MANTISA = 10000000000;

        private readonly ulong _convertedValue;

        public static AtomicDecimal Zero = new AtomicDecimal(0);
        public static AtomicDecimal MaxValue = new AtomicDecimal(ulong.MaxValue);
        public static decimal MaxValueDecimal = (new AtomicDecimal(ulong.MaxValue)).ToDecimal();

        public decimal ToDecimal()
        {
            //decimal is struct so new results in stackalloca - no GC garbage
            return new decimal((int)_convertedValue, (int)(_convertedValue >> 32), 0, false, CONST_SCALE);
        }


        public AtomicDecimal(decimal d)
            : this(d, false)
        { }

        public AtomicDecimal DivideByPowerOf2(int powerOf2)
        {
            return new AtomicDecimal(this._convertedValue >> powerOf2);
        }

        public AtomicDecimal MultiplyByPowerOf2(int powerOf2)
        {
            return new AtomicDecimal(this._convertedValue << powerOf2);
        }

        public AtomicDecimal(decimal d, bool normalizeFirst)
        {
            if (normalizeFirst)
                d = d.Normalize();

            if (d > MaxValueDecimal)
            {
                if (d == decimal.MaxValue)
                {
                    _convertedValue = ulong.MaxValue;
                    return;
                }
                throw new Exception("Too large number");
            }

            if (d < 0)
            {
                throw new Exception("Negative numbers are unsupported");
            }

            int[] bits = decimal.GetBits(d);

            int scale = bits[3] >> 16;
            
            if (scale > CONST_SCALE)
                throw new Exception("Too large scale");

            //BEWARE: just converting to ulong can create some mess in upper int (e.g. ulong ul = (ulong) -2109254592) - as conversion can silently overflow
            _convertedValue = ((((ulong)(uint)bits[1]) << 32) | ((ulong)(uint)bits[0])) * (ulong)Math.Pow(10, CONST_SCALE - scale);
        }

        public static AtomicDecimal ConvertToAtomicDecimalCuttingExcessScale(decimal d)
        {
            return new AtomicDecimal(decimal.Round(d, CONST_SCALE, MidpointRounding.AwayFromZero), true);
        }

        public AtomicDecimal(int i)
        {
            if(i < 0)
                throw new Exception("Negative numbers are unsupported");

            _convertedValue = ((ulong) i) * CONST_DIVISOR;
        }

        internal AtomicDecimal(int mantisa, int exponent)
        {
            _convertedValue = (ulong)mantisa * (ulong)Math.Pow(10, CONST_SCALE - exponent);
        }

        internal AtomicDecimal(ulong convertedValue)
        {
            this._convertedValue = convertedValue;
        }


        public static bool operator ==(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue == d2._convertedValue;
        }

        public static bool operator !=(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue != d2._convertedValue;
        }

        public static bool operator <(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue < d2._convertedValue;
        }

        public static bool operator <=(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue <= d2._convertedValue;
        }

        public static bool operator >(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue > d2._convertedValue;
        }

        public static bool operator >=(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue >= d2._convertedValue;
        }

        public static AtomicDecimal operator -(AtomicDecimal d1, AtomicDecimal d2)
        {
            if(d1 < d2)
                throw new ArithmeticException(string.Format("AtomicDecimal cannot subtract larger ({0}) number from smaller ({1})", d2, d1));

            return new AtomicDecimal(d1._convertedValue - d2._convertedValue);
        }

        public static AtomicDecimal Difference(AtomicDecimal d1, AtomicDecimal d2)
        {
            if(d1 > d2)
                return new AtomicDecimal(d1._convertedValue - d2._convertedValue);
            else
                return new AtomicDecimal(d2._convertedValue - d1._convertedValue);
        }

        public static AtomicDecimal operator +(AtomicDecimal s1, AtomicDecimal s2)
        {
            return new AtomicDecimal(s1._convertedValue + s2._convertedValue);
        }

        public static implicit operator AtomicDecimal(int value)
        {
            if (value == 0)
                return AtomicDecimal.Zero;

            return new AtomicDecimal(value);
        }

        public static int Compare(AtomicDecimal d1, AtomicDecimal d2)
        {
            //return Comparer<ulong>.Default.Compare(d1._convertedValue, d2._convertedValue);

            if (d1._convertedValue < d2._convertedValue)
                return -1;
            else if (d1._convertedValue > d2._convertedValue)
                return 1;
            else
                return 0;
        }

        public override string ToString()
        {
            return string.Format("Atomic decimal: {0} (stored as {1})", this.ToDecimal(), this._convertedValue);
        }

        public static AtomicDecimal Max(AtomicDecimal d1, AtomicDecimal d2, AtomicDecimal d3)
        {
            return d1._convertedValue > d2._convertedValue
                ? (d1._convertedValue > d3._convertedValue ? d1 : d3)
                : (d2._convertedValue > d3._convertedValue ? d2 : d3);
        }

        public static AtomicDecimal Max(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue > d2._convertedValue ? d1 : d2;
        }
    }

    public static class PriceCalculationUtilsEx
    {
        public static bool IsWorseOrEqualPrice(this AtomicDecimal priceA, AtomicDecimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA >= priceB;
            else
                return priceA <= priceB;
        }

        public static bool IsBetterPrice(this AtomicDecimal priceA, AtomicDecimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA < priceB;
            else
                return priceA > priceB;
        }
    }
}
