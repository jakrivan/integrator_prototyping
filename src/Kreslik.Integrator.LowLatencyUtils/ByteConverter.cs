﻿//using System;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.LowLatencyUtils
{
    [Serializable]
    public class ByteConvertingException : Exception
    {
        public ByteConvertingException() { }
        public ByteConvertingException(string message) : base(message) { }
        public ByteConvertingException(string message, Exception inner) : base(message, inner) { }
        protected ByteConvertingException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    public static class ByteConverter
    {
        private static Action<decimal, byte[]> GetDecimalBytes = GetDecimalConvertingMethod();

        private static Action<decimal, byte[]> GetDecimalConvertingMethod()
        {
            MethodInfo method = typeof(decimal).GetMethod("GetBytes", BindingFlags.NonPublic | BindingFlags.Static);

            var parameter1 = method.GetParameters().First();
            var parameter2 = method.GetParameters().Single(p => p != parameter1);

            var argument1 = Expression.Parameter(typeof(Decimal), "decimalArgument");
            var argument2 = Expression.Parameter(typeof(byte[]), "byteArrayArgument");

            var methodCall = Expression.Call(
                method,
                Expression.Convert(argument1, parameter1.ParameterType),
                Expression.Convert(argument2, parameter2.ParameterType)
                );

            Action<decimal, byte[]> GetDecimalBytes = Expression.Lambda<Action<decimal, byte[]>>(
                Expression.Convert(methodCall, typeof(void)),
                argument1, argument2
                ).Compile();

            return GetDecimalBytes;
        }

        public static Action<decimal, int[]> SetDecimalBits = GetDecimalSettingMethod();

        private static Action<decimal, int[]> GetDecimalSettingMethod()
        {
            MethodInfo method = typeof(decimal).GetMethod("SetBits", BindingFlags.NonPublic | BindingFlags.Instance);

            var parameter1 = method.GetParameters().First();
            //var parameter2 = method.GetParameters().Single(p => p != parameter1);
            var instance = Expression.Parameter(typeof(decimal), "instance");

            //var argument1 = Expression.Parameter(typeof(Decimal), "decimalArgument");
            var argument1 = Expression.Parameter(typeof(int[]), "intArrayArgument");

            var methodCall = Expression.Call(
                instance,
                method,
                Expression.Convert(argument1, parameter1.ParameterType)
                );

            Action<decimal, int[]> SetDecimalBits = Expression.Lambda<Action<decimal, int[]>>(
                Expression.Convert(methodCall, typeof(void)),
                instance, argument1
                ).Compile();

            return SetDecimalBits;
        }

        public static Func<int, int, int, int, decimal> CreateDecimalFromBits = GetDecimalCtorMethod();

        private static Func<int, int, int, int, decimal> GetDecimalCtorMethod()
        {
            ConstructorInfo constructor = typeof(decimal).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new[] { typeof(int), typeof(int), typeof(int), typeof(int) }, null);

            var parameter1 = constructor.GetParameters().First();
            var parameter2 = constructor.GetParameters().First(p => p != parameter1);
            var parameter3 = constructor.GetParameters().First(p => p != parameter1 && p != parameter2);
            var parameter4 = constructor.GetParameters().First(p => p != parameter1 && p != parameter2 & p != parameter3);


            var argument1 = Expression.Parameter(typeof(int), "lo");
            var argument2 = Expression.Parameter(typeof(int), "mid");
            var argument3 = Expression.Parameter(typeof(int), "hi");
            var argument4 = Expression.Parameter(typeof(int), "flags");

            var methodCall = Expression.New(
                constructor,
                Expression.Convert(argument1, parameter1.ParameterType),
                Expression.Convert(argument2, parameter2.ParameterType),
                Expression.Convert(argument3, parameter3.ParameterType),
                Expression.Convert(argument4, parameter4.ParameterType)
                );

            Func<int, int, int, int, decimal> createDecimalFromBits = Expression.Lambda<Func<int, int, int, int, decimal>>(
                Expression.Convert(methodCall, typeof(decimal)),
                argument1, argument2, argument3, argument4
                ).Compile();

            return createDecimalFromBits;
        }


        //public static void TryAppend(BufferSegmentItem bufferSegmentItem, long value)
        //{
        //    bufferSegmentItem.NumberOfBytesUsed += 8;
        //    if (bufferSegmentItem.NumberOfBytesUsed > bufferSegmentItem.SegmentSize)
        //        return;
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 8] = (byte)value;
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 7] = (byte)(value >> 8);
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 6] = (byte)(value >> 16);
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 5] = (byte)(value >> 24);
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 4] = (byte)(value >> 32);
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 3] = (byte)(value >> 40);
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 2] = (byte)(value >> 48);
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 1] = (byte)(value >> 56);
        //}

        //public static void TryAppend(BufferSegmentItem bufferSegmentItem, DateTime value)
        //{
        //    TryAppend(bufferSegmentItem, value.Ticks);
        //}

        //public static void TryAppend(BufferSegmentItem bufferSegmentItem, int value)
        //{
        //    bufferSegmentItem.NumberOfBytesUsed += 4;
        //    if (bufferSegmentItem.NumberOfBytesUsed > bufferSegmentItem.SegmentSize)
        //        return;
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 4] = (byte)value;
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 3] = (byte)(value >> 8);
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 2] = (byte)(value >> 16);
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 1] = (byte)(value >> 24);
        //}

        //public static void TryAppend(BufferSegmentItem bufferSegmentItem, byte value)
        //{
        //    bufferSegmentItem.NumberOfBytesUsed += 1;
        //    if (bufferSegmentItem.NumberOfBytesUsed > bufferSegmentItem.SegmentSize)
        //        return;
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 1] = value;
        //}

        //public static void TryAppend(BufferSegmentItem bufferSegmentItem, bool value)
        //{
        //    bufferSegmentItem.NumberOfBytesUsed += 1;
        //    if (bufferSegmentItem.NumberOfBytesUsed > bufferSegmentItem.SegmentSize)
        //        return;
        //    bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 1] = value ? (byte)1 : (byte)0;
        //}

        //public static void TryAppend(BufferSegmentItem bufferSegmentItem, string asciiString)
        //{

        //    if (bufferSegmentItem.NumberOfBytesUsed > bufferSegmentItem.SegmentSize + asciiString.Length)
        //        return;

        //    for (int strIdx = 0; strIdx < asciiString.Length; strIdx++)
        //    {
        //        if (asciiString[strIdx] > byte.MaxValue)
        //            throw new ByteConvertingException("Not an ascii string");

        //        bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed++] = (byte)asciiString[strIdx];
        //    }
        //}

        //public static void TryAppend(BufferSegmentItem bufferSegmentItem, byte[] tempBuffer, decimal value)
        //{
        //    bufferSegmentItem.NumberOfBytesUsed += 16;
        //    if (bufferSegmentItem.NumberOfBytesUsed > bufferSegmentItem.SegmentSize)
        //        return;

        //    GetDecimalBytes(value, tempBuffer);

        //    Buffer.BlockCopy(tempBuffer, 0, bufferSegmentItem.Buffer, bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - 16, 16);
        //}

        //private static int _guidSize = Marshal.SizeOf(typeof(Guid));

        //public static void TryAppend(BufferSegmentItem bufferSegmentItem, Guid value)
        //{
        //    bufferSegmentItem.NumberOfBytesUsed += _guidSize;
        //    if (bufferSegmentItem.NumberOfBytesUsed > bufferSegmentItem.SegmentSize)
        //        return;

        //    unsafe
        //    {
        //        fixed (byte* b = &bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + bufferSegmentItem.NumberOfBytesUsed - _guidSize])
        //        {
        //            IntPtr p = new IntPtr(b);
        //            Marshal.StructureToPtr(value, p, false);
        //        }
        //    }
        //}

        //public static Guid ReadGuid(BufferSegmentItem bufferSegmentItem)
        //{
        //    //TODO check size

        //    byte[] tmp = new byte[16];
        //    Buffer.BlockCopy(bufferSegmentItem.Buffer, bufferSegmentItem.ContentStart, tmp, 0, 16);
        //    Guid value = new Guid(tmp);

        //    return value;

        //    unsafe
        //    {
        //        fixed (byte* b = &bufferSegmentItem.Buffer[bufferSegmentItem.ContentStart])
        //        {
        //            IntPtr p = new IntPtr(b);

        //            //this throws! doesn't accept value arguments
        //            Marshal.PtrToStructure(p, (object)value);
        //        }
        //    }

        //    return value;
        //}

        public static void AppendType<T>(BufferSegmentItem bufferSegmentItem, T value)
        {
            int typeSize = Marshal.SizeOf(typeof(T));

            bufferSegmentItem.IncreaseContentSize(typeSize);

            unsafe
            {
                fixed (byte* b = &bufferSegmentItem.Buffer[bufferSegmentItem.ContentStart])
                {
                    IntPtr p = new IntPtr(b);
                    Marshal.StructureToPtr(value, p, false);
                }
            }
        }

        public static T ReadType<T>(BufferSegmentItem bufferSegmentItem, int startOffsetInSegment, T uninitializedInstance) where T : class
        {
            int typeSize = Marshal.SizeOf(typeof(T));

            if (bufferSegmentItem.ContentSize < typeSize)
                throw new ByteConvertingException("Segment contains less data than what's needed for the type");

            unsafe
            {
                fixed (byte* b = &bufferSegmentItem.Buffer[bufferSegmentItem.SegmentStart + startOffsetInSegment])
                {
                    IntPtr p = new IntPtr(b);

                    //this throws! doesn't accept value arguments - therefore the generic constraint (T - class) in definition
                    Marshal.PtrToStructure(p, uninitializedInstance);
                }
            }

            return uninitializedInstance;
        }

        /// <summary>
        /// Copies ascii string into preallocated buffer starting from a given index and filling possible extra space with nulls
        ///  Null or empty string will result in buffer filled with zeroes
        /// </summary>
        /// <param name="asciiString">ascii string to be copied. Exception will be thrown if contains non-ascii chars</param>
        /// <param name="buffer">destination buffer. Exception will be thrown if not sufficient</param>
        /// <param name="startIndex">index to start copying</param>
        /// <param name="bufferSizeToFill">size of the buffer to fill. Extra remaining space will be filled with '\0'</param>
        /// <exception cref="ByteConvertingException">Thrown for non-ascii strings or insufficent buffers</exception>
        public static void CopyAsciiStringToByteBuffer(string asciiString, byte[] buffer, int startIndex, int bufferSizeToFill)
        {
            if(buffer.Length < startIndex + bufferSizeToFill)
                throw new ByteConvertingException("bufferSizeToFill is reaching beyond the underlaying buffer length");

            int strLen = string.IsNullOrEmpty(asciiString) ? 0 : asciiString.Length;

            if (strLen > bufferSizeToFill)
                throw new ByteConvertingException(string.Format("Passed ascii string is longer than bufferSizeToFill. {0}", asciiString));

            for (int strIdx = 0; strIdx < strLen; strIdx++)
            {
                if (asciiString[strIdx] > byte.MaxValue)
                    throw new ByteConvertingException(string.Format("Passed ascii string has nonascii char at position {0}. {1}", strIdx, asciiString));

                buffer[startIndex + strIdx] = (byte)asciiString[strIdx];
            }

            Array.Clear(buffer, startIndex + strLen, bufferSizeToFill - strLen);
        }

        /// <summary>
        /// Copies ascii string into preallocated buffer given by pointer, starting from a given index and filling possible extra space with nulls
        ///  Null or empty string will result in buffer filled with zeroes
        /// </summary>
        /// <param name="asciiString">ascii string to be copied. Exception will be thrown if contains non-ascii chars</param>
        /// <param name="buffer">destination buffer. Exception will be thrown if not sufficient</param>
        /// <param name="startIndex">index to start copying</param>
        /// <param name="bufferSizeToFill">size of the buffer to fill. Extra remaining space will be filled with '\0'</param>
        /// <exception cref="ByteConvertingException">Thrown for non-ascii strings or insufficent buffers</exception>
        public unsafe static void CopyAsciiStringToByteBuffer(string asciiString, byte* buffer, int startIndex, int bufferSizeToFill)
        {
            //unfortunately cannot verify that we are not reaching beyond array represented by pointer

            int strLen = string.IsNullOrEmpty(asciiString) ? 0 : asciiString.Length;

            if (strLen > bufferSizeToFill)
                throw new ByteConvertingException(string.Format("Passed ascii string is longer than bufferSizeToFill. {0}", asciiString));

            for (int strIdx = 0; strIdx < strLen; strIdx++)
            {
                if (asciiString[strIdx] > byte.MaxValue)
                    throw new ByteConvertingException(string.Format("Passed ascii string has nonascii char at position {0}. {1}", strIdx, asciiString));

                buffer[startIndex + strIdx] = (byte)asciiString[strIdx];
            }

            for (int bufferIdx = startIndex + strLen; bufferIdx < bufferSizeToFill; bufferIdx++)
            {
                buffer[bufferIdx] = (byte)0;
            }
        }
    }
}
