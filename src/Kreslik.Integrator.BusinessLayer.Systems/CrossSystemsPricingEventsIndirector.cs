﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public class CrossSystemsPricingEventsIndirector
    {
        private static Counterparty[] _preferenceOrder = new Counterparty[]
        {
            Counterparty.L01, Counterparty.LM2, Counterparty.FC1, Counterparty.HTF, Counterparty.HT3, Counterparty.FC2,
            Counterparty.FA1
        };

        private static int[] _counterpartiesRank = new int[Counterparty.ValuesCount];

        static CrossSystemsPricingEventsIndirector()
        {
            for (int idx = 0; idx < _counterpartiesRank.Length; idx++)
            {
                _counterpartiesRank[idx] = Counterparty.ValuesCount;
            }

            for (int idx = 0; idx < _preferenceOrder.Length; idx++)
            {
                _counterpartiesRank[(int)_preferenceOrder[idx]] = idx;
            }
        }

        private static int GetPreferenceOrderForCounterparty(Counterparty counterparty)
        {
            return _counterpartiesRank[(int)counterparty];
        }

        private IChangeablePriceBook<PriceObjectInternal, Counterparty> _priceBook;
        public CrossSystemsPricingEventsIndirector(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            this._priceBook = priceBook;
        }

        //private BookPriceUpdate<PriceObjectInternal>[] _pricingIndirectors = new BookPriceUpdate<PriceObjectInternal>[Counterparty.ValuesCount];
        private Action<PriceObjectInternal>[] _pricingIndirectors = new Action<PriceObjectInternal>[Counterparty.ValuesCount];

        private int _subscribedToParentEvent = 0;

        public void RegisterForIndirectPricingEvent(Counterparty counterparty, Action<PriceObjectInternal> handler)
        {
            //if noone else was subscribed
            if (this._pricingIndirectors.All(o => o == null))
                //make sure we are realy the first one
                if (Interlocked.Exchange(ref this._subscribedToParentEvent, 1) == 0)
                    //and subscribe to parent
                    this._priceBook.ChangeableBookTopImproved += PriceBookOnChangeableBookTopImproved;
                    //this._priceBook.NewNodeArrived += PriceBookOnNewNodeArrived;

            _pricingIndirectors[GetPreferenceOrderForCounterparty(counterparty)] += handler;
        }

        private void PriceBookOnChangeableBookTopImproved(PriceObjectInternal priceObject)
        {
            for (int idx = 0; idx < _pricingIndirectors.Length; idx++)
            {
                if (_pricingIndirectors[idx] != null)
                    _pricingIndirectors[idx].Invoke(priceObject);
            }
        }

        //private void PriceBookOnNewNodeArrived(IBookTop<PriceObjectInternal> sender, PriceObjectInternal node, DateTime timeStamp)
        //{
        //    for (int idx = 0; idx < _pricingIndirectors.Length; idx++)
        //    {
        //        if (_pricingIndirectors[idx] != null)
        //            _pricingIndirectors[idx].Invoke(node);
        //    }
        //}

        public void DeregisterFromIndirectPricingEvent(Counterparty counterparty, Action<PriceObjectInternal> handler)
        {
            _pricingIndirectors[GetPreferenceOrderForCounterparty(counterparty)] -= handler;

            //if no one else is subscribed now
            if (this._pricingIndirectors.All(o => o == null))
                //make sure we were realy the last one
                if (Interlocked.Exchange(ref this._subscribedToParentEvent, 0) == 1)
                    //and unsubscribe from parent
                    this._priceBook.ChangeableBookTopImproved -= PriceBookOnChangeableBookTopImproved;
                    //this._priceBook.NewNodeArrived -= PriceBookOnNewNodeArrived;
        }
    }

    public class PricingIndirectorsCache
    {
        public static PricingIndirectorsCache Instance { get; private set; }

        static PricingIndirectorsCache()
        {
            Instance = new PricingIndirectorsCache();
        }

        private
            ConcurrentDictionary<IChangeablePriceBook<PriceObjectInternal, Counterparty>, CrossSystemsPricingEventsIndirector>
            _cache2 =
                new ConcurrentDictionary
                    <IChangeablePriceBook<PriceObjectInternal, Counterparty>, CrossSystemsPricingEventsIndirector>();

        public CrossSystemsPricingEventsIndirector GetMember(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            CrossSystemsPricingEventsIndirector indirector;
            if (!_cache2.TryGetValue(priceBook, out indirector))
            {
                indirector = _cache2.GetOrAdd(priceBook, book => new CrossSystemsPricingEventsIndirector(book));
            }

            return indirector;
        }
    }
}
