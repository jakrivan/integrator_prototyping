﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public class FxallSmartCrossSystem : VenueSmartCrossSystemBase
    {
        public FxallSmartCrossSystem(
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askReferenceCrossingPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidReferenceCrossingPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askReferenceToCrossPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidReferenceToCrossPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askBankpoolPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidBankpoolPriceBook,
            IVenueToBProvider venueTobProvider,
            Symbol symbol,
            Counterparty destinationVenueCounterparty, IUnderlyingSessionState destinationOrdSession,
            ISymbolsInfo symbolsInfo, IOrderManagement orderManagement, IRiskManager riskManager,
            ITradingGroupsCache tradingGroupsCache, IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
            IIntegratedSystemSettingsProvider<IVenueSmartCrossSystemSettings> settingsProvider)
            : base(
                askReferenceCrossingPriceBook, bidReferenceCrossingPriceBook, askReferenceToCrossPriceBook,
                bidReferenceToCrossPriceBook, askBankpoolPriceBook, bidBankpoolPriceBook, venueTobProvider,
                symbol, destinationVenueCounterparty, destinationOrdSession, symbolsInfo,
                orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider)
        {
        }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol, decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateFXallOrder(
                sizeBaseAbs, _settings.MinimumFillSize, price, dealDirection, symbol, TimeInForce.Day);
        }

        protected override VenueClientOrder CreateVenueOrder(
            string orderIdentity, string clientIdentity, VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateFXallClientOrder(
                orderIdentity, clientIdentity, venueOrderRequest as FXallClientOrderRequestInfo,
                integratedTradingSystemIdentification);
        }
    }

    public class LmaxSmartCrossSystem : VenueSmartCrossSystemBase
    {
        public LmaxSmartCrossSystem(
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askReferenceCrossingPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidReferenceCrossingPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askReferenceToCrossPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidReferenceToCrossPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askBankpoolPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidBankpoolPriceBook,
            IVenueToBProvider venueTobProvider,
            Symbol symbol,
            Counterparty destinationVenueCounterparty, IUnderlyingSessionState destinationOrdSession,
            ISymbolsInfo symbolsInfo, IOrderManagement orderManagement, IRiskManager riskManager,
            ITradingGroupsCache tradingGroupsCache, IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
            IIntegratedSystemSettingsProvider<IVenueSmartCrossSystemSettings> settingsProvider)
            : base(
                askReferenceCrossingPriceBook, bidReferenceCrossingPriceBook, askReferenceToCrossPriceBook,
                bidReferenceToCrossPriceBook, askBankpoolPriceBook, bidBankpoolPriceBook, venueTobProvider,
                symbol, destinationVenueCounterparty, destinationOrdSession, symbolsInfo,
                orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider)
        {
        }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol, decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxOrder(
                sizeBaseAbs, price, dealDirection, symbol, TimeInForce.Day, (LmaxCounterparty)_destinationVenueCounterparty);
        }

        protected override VenueClientOrder CreateVenueOrder(
            string orderIdentity, string clientIdentity, VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxClientOrder(
                orderIdentity, clientIdentity, venueOrderRequest as LmaxClientOrderRequestInfo,
                integratedTradingSystemIdentification);
        }
    }

    public class HotspotSmartCrossSystem : VenueSmartCrossSystemBase
    {
        public HotspotSmartCrossSystem(
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askReferenceCrossingPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidReferenceCrossingPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askReferenceToCrossPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidReferenceToCrossPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askBankpoolPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidBankpoolPriceBook,
            IVenueToBProvider venueTobProvider,
            Symbol symbol,
            Counterparty destinationVenueCounterparty, IUnderlyingSessionState destinationOrdSession,
            ISymbolsInfo symbolsInfo, IOrderManagement orderManagement, IRiskManager riskManager,
            ITradingGroupsCache tradingGroupsCache, IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
            IIntegratedSystemSettingsProvider<IVenueSmartCrossSystemSettings> settingsProvider)
            : base(
                askReferenceCrossingPriceBook, bidReferenceCrossingPriceBook, askReferenceToCrossPriceBook,
                bidReferenceToCrossPriceBook, askBankpoolPriceBook, bidBankpoolPriceBook, venueTobProvider,
                symbol, destinationVenueCounterparty, destinationOrdSession, symbolsInfo,
                orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider)
        {
        }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol, decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateHotspotOrder
                (sizeBaseAbs, _settings.MinimumFillSize, price, dealDirection, symbol, TimeInForce.Day, (HotspotCounterparty)_destinationVenueCounterparty);
        }

        protected override VenueClientOrder CreateVenueOrder(
            string orderIdentity, string clientIdentity, VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateHotspotClientOrder(
                orderIdentity, clientIdentity, venueOrderRequest as HotspotClientOrderRequestInfo,
                integratedTradingSystemIdentification);
        }
    }

    public abstract class VenueSmartCrossSystemBase : IntegratedSystemBase<IVenueSmartCrossSystemSettings>
    {
        private IChangeablePriceBook<PriceObjectInternal, Counterparty> _venueToCrossAskPricesBook; //L01
        private IChangeablePriceBook<PriceObjectInternal, Counterparty> _venueToCrossBidPricesBook; //L01
        private IChangeablePriceBook<PriceObjectInternal, Counterparty> _bankpoolAskPricesBook;
        private IChangeablePriceBook<PriceObjectInternal, Counterparty> _bankpoolBidPricesBook;
        private IVenueToBProvider _venueTobProvider; // Destination cpt
        private DateTime _nextTradeNotAllowedBefore = DateTime.MinValue;

        protected VenueSmartCrossSystemBase(
            //FC1
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askReferenceCrossingPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidReferenceCrossingPriceBook,
            //L0*
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askReferenceToCrossPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidReferenceToCrossPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askBankpoolPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidBankpoolPriceBook,
            IVenueToBProvider venueTobProvider,
            Symbol symbol,
            Counterparty destinationVenueCounterparty, IUnderlyingSessionState destinationOrdSession,
            ISymbolsInfo symbolsInfo, IOrderManagement orderManagement, IRiskManager riskManager,
            ITradingGroupsCache tradingGroupsCache, IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
            IIntegratedSystemSettingsProvider<IVenueSmartCrossSystemSettings> settingsProvider)
            : base(
                askReferenceCrossingPriceBook, bidReferenceCrossingPriceBook, symbol, destinationVenueCounterparty,
                destinationOrdSession, symbolsInfo, orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger,
                settingsProvider, IntegratedTradingSystemType.SmartCross, string.Empty, false,
                null)
        {
            this._venueToCrossAskPricesBook = askReferenceToCrossPriceBook;
            this._venueToCrossBidPricesBook = bidReferenceToCrossPriceBook;
            this._bankpoolAskPricesBook = askBankpoolPriceBook;
            this._bankpoolBidPricesBook = bidBankpoolPriceBook;
            this._venueTobProvider = venueTobProvider;
        }

        protected override void ProcessEvent()
        { }

        protected override void UnsubscribeFromBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            priceBook.ChangeableBookTopImproved -= OnNewPriceAvailable;
            priceBook.ChangeableBookTopReplaced -= OnNewPriceAvailable;
        }

        protected override void SubscribeToBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            priceBook.ChangeableBookTopImproved += OnNewPriceAvailable;
            priceBook.ChangeableBookTopReplaced += OnNewPriceAvailable;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool AppearsToBeInDiscount(PriceObjectInternal priceObject)
        {
            return priceObject.Side == PriceSide.Ask
                ? _venueToCrossBidPricesBook.GetEstimatedBestPriceFast() > priceObject.ConvertedPrice
                : _venueToCrossAskPricesBook.GetEstimatedBestPriceFast() < priceObject.ConvertedPrice;
        }

        private const int _SIZE_FOR_WEIGHTED_PRICE = 500000;
        private void OnNewPriceAvailable(PriceObjectInternal priceObject)
        {
            if (PriceObjectInternal.IsNullPrice(priceObject) ||
                priceObject.SizeBaseAbsRemainingToClaim < _settings.MinimumSizeToTrade ||
                !this.IsPriceValid(priceObject) ||
                !this.AppearsToBeInDiscount(priceObject) ||
                this._nextTradeNotAllowedBefore > priceObject.IntegratorReceivedTimeUtc)
                return;

            if (!this.IsReadyToProcessEvents())
                return;


            decimal crossedVenuePrice =
                (priceObject.Side == PriceSide.Ask ? this._venueToCrossBidPricesBook : _venueToCrossAskPricesBook)
                    .GetWeightedPriceForExactSize(_SIZE_FOR_WEIGHTED_PRICE, int.MaxValue);

            //unavailable size in book
            if(crossedVenuePrice <= 0)
                return;


            decimal askPrice = priceObject.Side == PriceSide.Ask ? priceObject.Price : crossedVenuePrice;
            decimal bidPrice = priceObject.Side == PriceSide.Ask ? crossedVenuePrice : priceObject.Price;

            if (bidPrice - askPrice < _settings.MinimumDecimalDiscountToTrade)
                return;

            //we'll find out best available bankpool price against the direction of trading (for covering)
            decimal bestBankpoolCoverPrice = (priceObject.Side == PriceSide.Ask
                ? _bankpoolAskPricesBook
                : _bankpoolBidPricesBook)
                .GetWeightedPriceForExactSize(this._settings.MinimumSizeToTrade, int.MaxValue);

            if(bestBankpoolCoverPrice == 0m)
                return;

            decimal availableCoverSizeBaseAbs =
                (priceObject.Side == PriceSide.Ask ? _bankpoolAskPricesBook : _bankpoolBidPricesBook)
                    .GetMaximumSizeForExactPrice(bestBankpoolCoverPrice, int.MaxValue, _settings.MaximumSizeToTrade);

            decimal priceToBeSent = PriceCalculationUtils.ImprovePrice(bestBankpoolCoverPrice,
                this._settings.MinimumTrueGainGrossDecimal, priceObject.Side.ToOppositeSide().ToOutgoingDealDirection());

            availableCoverSizeBaseAbs = SymbolsInfoUtils.RoundToIncrement(RoundingKind.AlwaysToZero, _orderMinimumSizeIncrement, availableCoverSizeBaseAbs);

            if (availableCoverSizeBaseAbs < _settings.MinimumSizeToTrade)
                return;

            //for sells we want to increase and vice versa
            RoundingKind improvingRoundingKind = priceObject.Side == PriceSide.Ask
                ? RoundingKind.AlwaysAwayFromZero
                : RoundingKind.AlwaysToZero;
            decimal priceToBeSentRounded;
            if (!this._symbolsInfo.TryRoundToSupportedPriceIncrement(
                this._symbol, _destinationVenueCounterparty.TradingTargetType, improvingRoundingKind,
                priceToBeSent,
                out priceToBeSentRounded))
            {
                this._logger.Log(LogLevel.Error,
                    this._systemFriendlyName + " - Submitting Venue leg failed due to unability to roundprice");
                return;
            }

            //This is redundant, but extremely dangerous if not met - therefore double checking
            if (availableCoverSizeBaseAbs > _settings.MaximumSizeToTrade)
            {
                this._logger.Log(LogLevel.Fatal, "{0} trading system generated size request of [{1}] - cutting it to max size [{2}]",
                    this._systemFriendlyName, availableCoverSizeBaseAbs, _settings.MaximumSizeToTrade);
                availableCoverSizeBaseAbs = _settings.MaximumSizeToTrade;
            }

            if (TryEnterSingleAccessRegion())
            {
                this._nextTradeNotAllowedBefore = priceObject.IntegratorReceivedTimeUtc + _settings.MinimumTimeBetweenSingleCounterpartySignals;
                this._logger.Log(LogLevel.Trace, "{0} Discount situation encountered: Venue {1} price: {2} crossed (after rounding we will send {3} on {4}), maximalized size for min acceptable gain price ({5}): {6}, crossing price: {7}",
                    this._systemFriendlyName, priceObject.Side.ToOppositeSide(), crossedVenuePrice, priceObject.Side.ToOppositeSide().ToOutgoingDealDirection(), priceToBeSentRounded, bestBankpoolCoverPrice, availableCoverSizeBaseAbs, priceObject.ToString());

                this.PerformCross(priceObject, priceToBeSentRounded, availableCoverSizeBaseAbs);
            }
        }

        private void PerformCross(PriceObjectInternal crossingAggressingPrice, decimal crossedVenuePrice, decimal sizeToSend)
        {
            this._currentHeadOrderPolrity = crossingAggressingPrice.Side.ToOppositeSide().ToOutgoingDealDirection();
            VenueClientOrderRequestInfo venueOri = this.CreateVenueOrderRequest(crossedVenuePrice,
                this._currentHeadOrderPolrity, crossingAggressingPrice.Symbol, sizeToSend);

            int currentOrderCnt = Interlocked.Increment(ref _headOrdersCnt);
            this._venueHeadClientOrder = this.CreateVenueOrder(this._systemFriendlyName + "_HEAD_" + currentOrderCnt,
                                            this._systemIdentity, venueOri, this._integratedTradingSystemIdentification);

            var result = this.RegisterOrder(this._venueHeadClientOrder);
            if (!result.RequestSucceeded)
            {
                this._logger.Log(LogLevel.Error, "Submitting Venue leg of {0} failed: {1}", this._systemFriendlyName, result.ErrorMessage);
                ExitSingleAccessRegion();
            }
            else
            {
                TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromMilliseconds(100), AfterDisablingProcedure);
            }
        }
        protected override DealDirection CurrentHeadOrderPolarity { get { return this._currentHeadOrderPolrity; } }


        protected override void AfterDisablingProcedure()
        {
            this.RequestOrderCancel(this._venueHeadClientOrder);
        }

        protected override void OnResetProcedure()
        {
            if (this._venueHeadClientOrder != null)
            {
                this._logger.Log(LogLevel.Fatal,
                                 "{0} system has non-null pending order [{1}]. Resetting it, but situation should be reviewed manually.",
                                 this._systemFriendlyName, this._venueHeadClientOrder);
                this.RequestOrderCancel(this._venueHeadClientOrder);
                this._venueHeadClientOrder = null;
            }

            Volatile.Write(ref this._bankOrdersFullyDone, 0);
        }

        //private bool _venueLegIsFullyDone;
        private volatile VenueClientOrder _venueHeadClientOrder;
        private int _bankOrdersFullyDone;
        private DealDirection _currentHeadOrderPolrity;

        private bool IsCorssFullyDone()
        {
            return this._venueHeadClientOrder == null && _bankOrdersFullyDone >= _bankSubOrdersCnt;
        }

        protected override void HandleClientOrderUpdateInfo(ClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            if (clientOrderUpdateInfo.SizeBaseAbsTotalRemaining == 0m)
            {
                if (clientOrderUpdateInfo is VenueClientOrderUpdateInfo)
                    this._venueHeadClientOrder = null;

                if (clientOrderUpdateInfo is BankPoolClientOrderUpdateInfo)
                    Interlocked.Increment(ref this._bankOrdersFullyDone);

                if (this.IsCorssFullyDone())
                {
                    this._logger.Log(LogLevel.Info, "Closed next ({0}) {1} cross (with {2} bank suborders), total acquired term amount polarized: {3}",
                             this._headOrdersCnt, this._systemFriendlyName, this._bankSubOrdersCnt, this._totalAcquiredAmountTermPol);

                    this.ResetInternal(false);
                }
            }
        }

        protected override void HandleIntegratorDealInternal(IntegratorDealInternal integratorDealInternal, AtomicSize currentPositionBasePol)
        {
            //MarketOnImprovement is the second leg
            if (integratorDealInternal.SenderTradingTargetType.GetTradingTargetCategory() == TradingTargetCategory.Venue)
            {
                DealDirection coverDirection = integratorDealInternal.Direction.ToOpositeDirection();
                decimal requestedPriceUnrounded = PriceCalculationUtils.ImprovePrice(integratorDealInternal.Price,
                    _settings.CoverDistanceGrossDecimal, coverDirection);

                decimal requestedPrice;
                if (
                    !PriceCalculationUtilsEx.TryRoundToSupportedPriceIncrement(this._symbolsInfo, coverDirection, true,
                        this._symbol, TradingTargetType.BankPool, requestedPriceUnrounded, out requestedPrice))
                {
                    this._logger.Log(LogLevel.Fatal, "{0} failed to round improved price ({1}) of incomming cross fill. Will cover with plain MKT",
                        this._systemFriendlyName, requestedPriceUnrounded);
                    this.SendBankOrder(integratorDealInternal.FilledAmountBaseAbs, coverDirection,
                        integratorDealInternal.Symbol,
                        TimeSpan.Zero, integratorDealInternal.ClientOrderIdentity);
                    return;
                }

                this._logger.Log(
                    requestedPrice.IsBetterOrEqualPrice(integratorDealInternal.Price, coverDirection)
                        ? LogLevel.Info
                        : LogLevel.Fatal,
                    "{0} received venue fill and sending bankpool limit to {1} for {2} (from unrounded {3})",
                    this._systemFriendlyName, coverDirection, requestedPrice, requestedPriceUnrounded);


                this.SendLimitTillTimeoutBankOrder(integratorDealInternal.FilledAmountBaseAbs, requestedPrice, coverDirection, integratorDealInternal.Symbol,
                    this._settings.CoverTimeout, integratorDealInternal.ClientOrderIdentity);
            }
        }
    }
}
