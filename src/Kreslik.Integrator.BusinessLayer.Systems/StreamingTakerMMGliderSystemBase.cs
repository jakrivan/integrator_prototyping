﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public sealed class StreamingTakerMMLmaxGliderSystem: StreamingTakerMMGliderSystemBase
    {
        public StreamingTakerMMLmaxGliderSystem(
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxAskPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxBidPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolAskPricebook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolBidPricebook,
            IMedianProvider<decimal> askMedianProvider, IMedianProvider<decimal> bidMedianProvider,
            Symbol symbol,
            PriceSide observedBookSide, Counterparty softPricingVenueCounterparty,
            IOrderFlowSession destinationOrdSession, ISymbolsInfo symbolsInfo,
            IMedianProvider<decimal> medianPriceProvider, IOrderManagement orderManagement, IRiskManager riskManager,
            ITradingGroupsCache tradingGroupsCache, IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
            IIntegratedSystemSettingsProvider<IVenueGliderStreamMMSystemSettings> settingsProvider,
            EventsRateCheckerEx outgoingOrdersRateCheck)
            : base(
                lmaxAskPriceBook, lmaxBidPriceBook, bankpoolAskPricebook, bankpoolBidPricebook,
                askMedianProvider, bidMedianProvider, symbol, observedBookSide,
                softPricingVenueCounterparty, destinationOrdSession, symbolsInfo,
                medianPriceProvider, orderManagement, riskManager, tradingGroupsCache,
                unicastInfoForwardingHub, logger, settingsProvider,
                true, outgoingOrdersRateCheck)
        {
        }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol, decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateHotspotOrder
                (sizeBaseAbs, _settings.MinimumFillSize, price, dealDirection, symbol, TimeInForce.Day, (HotspotCounterparty)_destinationVenueCounterparty);
        }

        protected override VenueClientOrderRequestInfo CreateVenueReplaceOrderRequest(string replacedOrderId, VenueClientOrderRequestInfo replacingOrderRequest)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateHotspotReplaceOrder(
                replacedOrderId, replacingOrderRequest as HotspotClientOrderRequestInfo);
        }

        protected override VenueClientOrder CreateVenueOrder(
            string orderIdentity, string clientIdentity, VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateHotspotClientOrder(
                orderIdentity, clientIdentity, venueOrderRequest as HotspotClientOrderRequestInfo,
                integratedTradingSystemIdentification);
        }

        protected override VenueClientOrderRequestInfo CreateVenueGliderOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol,
            decimal sizeBaseAbs)
        {
            //TODO: add a switch logic based on target venue. How about minFillSize
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxOrder(
                sizeBaseAbs, price, dealDirection, symbol, TimeInForce.Day, (LmaxCounterparty)_settings.GlidingCounterparty);
        }

        protected override VenueClientOrderRequestInfo CreateVenueGliderReplaceOrderRequest(string replacedOrderId,
            VenueClientOrderRequestInfo replacingOrderRequest)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxReplaceOrder(replacedOrderId,
                 replacingOrderRequest as LmaxClientOrderRequestInfo);
        }

        protected override GliderLmaxClientOrder CreateVenueGlidingOrder(string orderIdentity, string clientIdentity,
            VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification, GlidingInfoBag glidingInfoBag)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateGliderLmaxClientOrder(orderIdentity, clientIdentity,
                venueOrderRequest as LmaxClientOrderRequestInfo, integratedTradingSystemIdentification, glidingInfoBag);
        }
    }

    public abstract class StreamingTakerMMGliderSystemBase : StreamingTakerMMSystemBase<IVenueGliderStreamMMSystemSettings>
    {
        private static class GliderSystemConstants
        {
            internal static readonly int MinimumGranularityPointsImprovementFromLmaxBestPrice = 1;
            //we should use _symbolsInfo.TryRoundToSupportedSizeIncrement, but this is faster now
            internal static readonly decimal LmaxGranularity = 1000;
        }

        protected StreamingTakerMMGliderSystemBase(
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxAskPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxBidPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolAskPricebook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolBidPricebook,
            IMedianProvider<decimal> askMedianProvider, IMedianProvider<decimal> bidMedianProvider,
            Symbol symbol,
            PriceSide observedBookSide, Counterparty softPricingVenueCounterparty,
            IOrderFlowSession destinationOrdSession, ISymbolsInfo symbolsInfo,
            IMedianProvider<decimal> medianPriceProvider, IOrderManagement orderManagement, IRiskManager riskManager,
            ITradingGroupsCache tradingGroupsCache, IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
            IIntegratedSystemSettingsProvider<IVenueGliderStreamMMSystemSettings> settingsProvider,
            bool repetitiveCancelRequiredDuringSweepDefence,
            EventsRateCheckerEx outgoingOrdersRateCheck)
            : base(
                lmaxAskPriceBook, lmaxBidPriceBook, bankpoolAskPricebook, bankpoolBidPricebook, symbol, observedBookSide,
                softPricingVenueCounterparty, destinationOrdSession, symbolsInfo, medianPriceProvider, orderManagement,
                riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider, repetitiveCancelRequiredDuringSweepDefence,
                outgoingOrdersRateCheck,
                IntegratedTradingSystemType.Glider)
        {
            this._askMedianProvider = askMedianProvider;
            this._bidMedianProvider = bidMedianProvider;
        }

        private decimal _minimumGlidingPriceIncrement;
        private IMedianProvider<decimal> _askMedianProvider;
        private IMedianProvider<decimal> _bidMedianProvider;

        protected override Counterparty AlternateDestinationCounterparty { get { return _settings.GlidingCounterparty; } }

        protected override void SettingsUpdating(IVenueGliderStreamMMSystemSettings newSettings)
        {
            base.SettingsUpdating(newSettings);

            decimal? minimumPriceIncrementLocal = this._symbolsInfo.GetMinimumPriceIncrement(this._symbol, TradingTargetType.LMAX);
            if (minimumPriceIncrementLocal.HasValue)
                this._minimumGlidingPriceIncrement = minimumPriceIncrementLocal.Value;
            else
            {
                this._logger.Log(LogLevel.Fatal, "{0} system cannot obtain its minimum price increment - will use 0", this._systemFriendlyName);
                this._minimumGlidingPriceIncrement = 0;
            }

            if (newSettings.GlidingCounterparty.TradingTargetType != TradingTargetType.LMAX)
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with non-LMAX gliding counterparty ({1}). This is currently disallowed. Hardblocking the system",
                    this._systemFriendlyName, newSettings.GlidingCounterparty);
                this.HardBlock();
                return;
            }

            if (
                !CheckTargetSymbolSupported(this._symbolsInfo, this._symbol,
                    newSettings.GlidingCounterparty.TradingTargetType))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} for symbol {1} with gliding counterparty ({2}) that does not support this symbol. This is disallowed. Hardblocking the system",
                    this._systemFriendlyName, _symbol, newSettings.GlidingCounterparty);
                this.HardBlock();
                return;
            }
        }

        protected override void DuringEnableProcedure()
        {
            base.DuringEnableProcedure();
            if (this.AllGlidingProcessingEvent != null)
                this.AllGlidingProcessingEvent(gib => this.ProcessGlidingEvent(gib, false));
        }

        private decimal? GetCurrentApplicableToBPriceForDirectClosingOfLLDeals(GlidingInfoBag glidingInfoBag, decimal sizeBaseAbsToCheckPrice)
        {
            return GetReferenceTopPrice(this._observedBookSide, sizeBaseAbsToCheckPrice, glidingInfoBag);
        }

        private decimal? CurrentLmaxToBPriceForClosingAccumulatedHedgingPosition
        {
            get
            {
                decimal? price;

                var topPriceObject = (this._observedBookSide == PriceSide.Ask
                ? this._lmaxBidPricebook
                : this._lmaxAskPricebook).MaxNodeInternal;

                if (PriceObjectInternal.IsNullPrice(topPriceObject))
                    price = null;
                else
                    price = topPriceObject.Price;

                topPriceObject.Release();

                return price;
            }
        }

        private decimal? GetCurrentApplicableToBPriceForClosingAccumulatedHedgingPosition(GlidingInfoBag glidingInfoBag, decimal sizeBaseAbsToCheckPrice)
        {
            return GetReferenceTopPrice(this._observedBookSide.ToOppositeSide(), sizeBaseAbsToCheckPrice, glidingInfoBag);
        }

        private decimal? GetReferenceTopPrice(PriceSide side, decimal sizeBaseAbsToCheckPrice, GlidingInfoBag glidingInfoBag)
        {
            decimal price = 0m;
            PriceObjectInternal topPriceObject = null;

            if (this._settings.PreferLLDestinationCheckToLmax)
            {
                price = (side == PriceSide.Ask
                    ? this._lmaxAskPricebook
                    : this._lmaxBidPricebook).GetWeightedPriceForExactSize(sizeBaseAbsToCheckPrice, int.MaxValue);
            }

            if (price == 0m)
            {
                price = (side == PriceSide.Ask
                     ? this._bankpoolAskPricebook
                     : this._bankpoolBidPricebook).GetWeightedPriceForExactSize(sizeBaseAbsToCheckPrice, int.MaxValue);
            }

            if (price == 0m)
            {
                price = (side == PriceSide.Ask
                    ? this._askMedianProvider
                    : this._bidMedianProvider).Median;
            }

            if (price <= 0m)
                return null;
            else
                return price;
        }



        protected override bool HandleUnconfirmedDealAfterAllChecksPassed(AutoRejectableDeal deal)
        {
            DateTime now = HighResolutionDateTime.UtcNow;
            DateTime cutoffTime = DateTimeEx.Min(now + _settings.KGTLLTime,
                deal.MaxProcessingCutoffTime - Constansts.MinimumSpanBetweenLLandWatchdog) - this._settings.GlidingCutoffSpan;

            decimal worstAcceptableFillPriceUnrounded;
            decimal worstAcceptableFillPrice;

            //We have a request for deal, so we need to try to cover it on opposite side, for somewhat better price
            worstAcceptableFillPriceUnrounded = PriceCalculationUtils.ImprovePrice(deal.CounterpartyRequestedPrice,
                this._settings.MinimumDecimalDifferenceGrossGain, deal.IntegratorDealDirection.ToOpositeDirection());

            if (
                    !PriceCalculationUtilsEx.TryRoundToSupportedPriceIncrement(this._symbolsInfo,
                        deal.IntegratorDealDirection.ToOpositeDirection(), true, deal.Symbol, TradingTargetType.LMAX,
                        worstAcceptableFillPriceUnrounded, out worstAcceptableFillPrice))
            {
                this._logger.Log(LogLevel.Fatal, "{0} could not perform rounding to calculate worst acceptable price for deal - so rejecting it. Deal: {1}",
                    this._systemFriendlyName, deal);
                return false;
            }

            GlidingInfoBag gib = new GlidingInfoBag(deal, deal.SizeBaseAbsInitial - deal.SizeBaseAbsInitial % GliderSystemConstants.LmaxGranularity, worstAcceptableFillPrice, cutoffTime);

            this._logger.Log(LogLevel.Info, "{0} starting to glide the deal [{1}], size - {2}, worst acceptable price: {3}",
                this._systemFriendlyName, deal.Identity, gib.InitialGlidingSizeBaseAbs, gib.WorstAcceptableFillPrice);

            this.AllGlidingProcessingEvent += gib.PerformActionOnActiveGlider;

            //this.ProcessGlidingEvent(gib, false);
            ThreadPoolEx.Instance.QueueWorkItem(new WorkItem(() => this.ProcessGlidingEvent(gib, false), TimerTaskFlagUtils.WorkItemPriority.Highest));
            return true;
        }

        private struct FinalGlideDecisioningResult
        {
            public decimal pnlOnAccept;
            public decimal pnlOnReject;
            public bool accept;
            public bool doNotCoverImmediately;
        }


        private FinalGlideDecisioningResult PerformDecisioning(GlidingInfoBag glidingInfoBag,
            decimal totalGlideFilledBasePolLocal, decimal totalGlideFilledTermPolLocal)
        {
            FinalGlideDecisioningResult result = new FinalGlideDecisioningResult();

            decimal llAcquiredSizeBasePol = (glidingInfoBag.ClientDealRequest.IntegratorDealDirection ==
                                             DealDirection.Buy
                ? glidingInfoBag.ClientDealRequest.CounterpartyRequestedSizeBaseAbs
                : -glidingInfoBag.ClientDealRequest.CounterpartyRequestedSizeBaseAbs);

            decimal? currentToBPriceForDirectClosingOfLLDeals =
                this.GetCurrentApplicableToBPriceForDirectClosingOfLLDeals(glidingInfoBag,
                    Math.Abs(llAcquiredSizeBasePol + totalGlideFilledBasePolLocal));

            //this is case of full glide without known actual price - just compare glide and ll
            if (!currentToBPriceForDirectClosingOfLLDeals.HasValue &&
                totalGlideFilledBasePolLocal + llAcquiredSizeBasePol == 0)
                currentToBPriceForDirectClosingOfLLDeals = 0;

            decimal? currentToBPriceForClosingAccumulatedHedgingPosition =
                totalGlideFilledBasePolLocal == 0
                    ? 0m
                    : this.GetCurrentApplicableToBPriceForClosingAccumulatedHedgingPosition(glidingInfoBag,
                        Math.Abs(totalGlideFilledBasePolLocal));

            if (currentToBPriceForDirectClosingOfLLDeals.HasValue)
            {
                result.pnlOnAccept =
                    //value of accumulated glider fills
                    totalGlideFilledTermPolLocal
                    -
                    //value of the client LL deal
                    llAcquiredSizeBasePol*glidingInfoBag.ClientDealRequest.CounterpartyRequestedPrice
                    +
                    //cost of closing remaining position that was not glider-filled
                    //remaining position (base abs)
                    (llAcquiredSizeBasePol + totalGlideFilledBasePolLocal)*
                    //price for closing
                    currentToBPriceForDirectClosingOfLLDeals.Value;
            }

            if (currentToBPriceForClosingAccumulatedHedgingPosition.HasValue)
            {
                result.pnlOnReject = totalGlideFilledBasePolLocal*
                                     currentToBPriceForClosingAccumulatedHedgingPosition.Value +
                                     totalGlideFilledTermPolLocal;
            }
            else
            {
                result.pnlOnReject = 0;
            }

            if (totalGlideFilledBasePolLocal == 0)
            {
                decimal minimumAcceptPrice =
                    PriceCalculationUtils.ImprovePrice(glidingInfoBag.ClientDealRequest.CounterpartyRequestedPrice,
                        this._settings.MinimumDecimalGrossGainIfZeroGlideFill,
                        glidingInfoBag.ClientDealRequest.IntegratorDealDirection.ToOpositeDirection());

                result.accept = currentToBPriceForDirectClosingOfLLDeals.HasValue && currentToBPriceForDirectClosingOfLLDeals.Value.IsBetterOrEqualPrice(minimumAcceptPrice,
                    glidingInfoBag.ClientDealRequest.IntegratorDealDirection.ToOpositeDirection());

                this._logger.Log(LogLevel.Info,
                    "{0} {1}Accepting deal - no size filled so far, worst acceptable price (on zero fill): {2} current price: {3}",
                    this._systemFriendlyName, result.accept ? string.Empty : " NOT", minimumAcceptPrice, currentToBPriceForDirectClosingOfLLDeals);
            }
            else if (result.pnlOnAccept == 0 && result.pnlOnReject == 0)
            {
                result.accept = false;
                this._logger.Log(LogLevel.Warn, "{0} NOT Accepting deal - cannot obtain any reference price (accept and reject PnLs are 0)",
                    this._systemFriendlyName);
            }
            else
            {
                result.accept = result.pnlOnAccept >= result.pnlOnReject;
                this._logger.Log(LogLevel.Info,
                        "{0} {1}Accepting deal - glidePnLTermGrossOnAccept: {2} glidePnLTermGrossOnReject: {3}",
                        this._systemFriendlyName, result.accept ? string.Empty : " NOT", result.pnlOnAccept,
                        result.pnlOnReject);
            }

            return result;
        }

        private void PerformLateFlattingAfterLateOrderUpdateReceived(GlidingInfoBag glidingInfoBag, bool hasActiveOrder)
        {
            if(hasActiveOrder)
                return;

            bool transitionedToFlattening = InterlockedEx.CheckAndFlipState(ref glidingInfoBag.CurrentState,
                GlidingInfoBag.STATE_FLATTENING, GlidingInfoBag.STATE_WAITINGFORFINALGLIDE);
            decimal positionBasePol = glidingInfoBag.TotalFilledSizeBasePol.ToDecimal();

            this._logger.Log(LogLevel.Info, "{0} glide {1} experiencing event after gliding stopped, with base pol position: {2} no pending orders. Was able to transit to flattening: {3}",
                    this._systemFriendlyName, glidingInfoBag.ClientDealRequest.Identity, positionBasePol, transitionedToFlattening);

            if (transitionedToFlattening)
            {
                if (positionBasePol == 0m)
                {
                    this.TryCloseGlide(glidingInfoBag);
                }
                else
                {
                    this._logger.Log(LogLevel.Info, "{0} glide {1} experiencing event after gliding stopped, with base pol position: {2} no pending orders - sending flatenning order",
                    this._systemFriendlyName, glidingInfoBag.ClientDealRequest.Identity, positionBasePol);

                    this.SendBankOrderWithoutDirectionCheck(
                        Math.Abs(positionBasePol),
                        positionBasePol > 0m
                            ? DealDirection.Sell
                            : DealDirection.Buy,
                        glidingInfoBag.ClientDealRequest.Symbol, false,
                        this._systemFriendlyName + glideClosingOrderNamer +
                        Interlocked.Increment(ref this._headOrdersCnt),
                        glidingInfoBag);
                }
            }
        }

        protected void PerformFinalDecisioning(GlidingInfoBag glidingInfoBag)
        {
            bool hasActiveOrder = glidingInfoBag.OrderCurrent != null || glidingInfoBag.OrderNext != null;

            if (!glidingInfoBag.StopGliding())
            {
                this._logger.Log(LogLevel.Warn, "{0} experienced duplicate attempt for closing glide of deal {1}. HasActiveOrder: {2}",
                    this._systemFriendlyName, glidingInfoBag.ClientDealRequest.Identity, hasActiveOrder);

                if (hasActiveOrder)
                {
                    CancelAllOrdersOfASingleGlide(glidingInfoBag, false);
                }
                else
                {
                    this.PerformLateFlattingAfterLateOrderUpdateReceived(glidingInfoBag, hasActiveOrder); 
                }
                return;
            }

            FinalGlideDecisioningResult decisioningResult;
            decimal totalFilledBasePolLocal = glidingInfoBag.TotalInTimeFilledSizeBasePol.ToDecimal();
            decimal llAcquiredSizeBasePol = (glidingInfoBag.ClientDealRequest.IntegratorDealDirection ==
                                             DealDirection.Buy
                ? glidingInfoBag.ClientDealRequest.SizeBaseAbsInitial
                : -glidingInfoBag.ClientDealRequest.SizeBaseAbsInitial);

            if (hasActiveOrder)
            {
                this._logger.Log(LogLevel.Error,
                    "{0} experiencing active order for glide {1} after cutoff time. Examining the pnls of all combinations of Gliding Cpt and KGT fill and reject (first the Gliding Cpt Reject, then the accept)",
                    this._systemFriendlyName, glidingInfoBag.ClientDealRequest.Identity);
                this.CancelAllOrdersOfASingleGlide(glidingInfoBag, false);

                FinalGlideDecisioningResult decisioningResultOnLmaxRejects = PerformDecisioning(glidingInfoBag,
                    totalFilledBasePolLocal,
                    glidingInfoBag.TotalInTimeFilledSizeTermPol.ToDecimal());
                FinalGlideDecisioningResult decisioningResultOnLmaxAccepts = PerformDecisioning(glidingInfoBag,
                    //if lmax accepts fully - then we are fully filled
                    -llAcquiredSizeBasePol,
                    //term amount current
                    glidingInfoBag.TotalInTimeFilledSizeTermPol.ToDecimal() + 
                    //term amount of what would be filled if last glide step is accepted
                        //if we are selling during glide (term is going to be positive), then llAcquired was bought ans is positive (so we need same direction as llAcquired)
                    (llAcquiredSizeBasePol + totalFilledBasePolLocal)*glidingInfoBag.LastGlidePrice);

                //find the lowest PnL variant
                decimal lowestPnl = MathEx.Min(decisioningResultOnLmaxRejects.pnlOnAccept,
                    decisioningResultOnLmaxRejects.pnlOnReject, decisioningResultOnLmaxAccepts.pnlOnAccept,
                    decisioningResultOnLmaxAccepts.pnlOnReject);

                bool accept = (decisioningResultOnLmaxAccepts.pnlOnReject == lowestPnl ||
                               decisioningResultOnLmaxRejects.pnlOnReject == lowestPnl);
                   
                decisioningResult = new FinalGlideDecisioningResult(){accept = accept, doNotCoverImmediately = true};

                this._logger.Log(LogLevel.Info, "{0} Glide {1} - lowest pnl variant: {2}, so {3}Accepting the ll deal",
                    this._systemFriendlyName, glidingInfoBag.ClientDealRequest.Identity, lowestPnl, accept ? string.Empty : " NOT");
            }
            else
            {
                decisioningResult = PerformDecisioning(glidingInfoBag, totalFilledBasePolLocal,
                    glidingInfoBag.TotalInTimeFilledSizeTermPol.ToDecimal());
            }

            var deal = glidingInfoBag.ClientDealRequest;
            bool accepted = false;
            if (decisioningResult.accept)
            {
                //if we need to liquidate position than we are mitigating risk by accepting
                deal.IsRiskRemovingOrder = totalFilledBasePolLocal != 0m;
                if (this.AcceptDeal(deal))
                {
                    //position keeping - DO NOT PERFORM HERE! - as Hotspot will send the final confirmation
                    //_currentPositionBasePol.InterlockedAdd(deal.IntegratorDealDirection == DealDirection.Buy ? deal.IntegratorFilledAmountBaseAbs : -deal.IntegratorFilledAmountBaseAbs);
                    //_totalAcquiredAmountTermPol.InterlockedAdd(deal.IntegratorDealDirection == DealDirection.Buy ? -deal.IntegratorFilledAmountBaseAbs * deal.CounterpartyRequestedPrice : deal.IntegratorFilledAmountBaseAbs * deal.CounterpartyRequestedPrice);
                    //so that we reflect actual state and can end up on zero
                    glidingInfoBag.TotalInTimeFilledSizeBasePol.InterlockedAdd(llAcquiredSizeBasePol);
                    //Do not add it here! - so that our final calculation of what needs to be covered is OK
                    //totalFilledBaseAbsLocal += llAcquiredSizeBasePol;
                    glidingInfoBag.TotalInTimeFilledSizeTermPol.InterlockedAdd(-llAcquiredSizeBasePol *
                                                                         glidingInfoBag.ClientDealRequest.CounterpartyRequestedPrice);
                    accepted = true;
                }
                else
                {
                    this._logger.Log(LogLevel.Fatal,
                        "System {0} attempted to accept deal, but underlying layer rejected it. Details are in log file.",
                        this._systemFriendlyName);
                }
            }
            else
            {
                this.RejectDeal(deal, RejectableMMDeal.DealRejectionReason.DealMissed);
            }

            if (!decisioningResult.doNotCoverImmediately)
            {
                if (!InterlockedEx.CheckAndFlipState(ref glidingInfoBag.CurrentState,
                    GlidingInfoBag.STATE_FLATTENING, GlidingInfoBag.STATE_DECISIONING))
                {
                    this._logger.Log(LogLevel.Fatal, "{0} glide {1} experienced glide in unexpected status ({2}) after end of decisioning - might be unable close it correctly. position (base pol: {3}) Attempting to continue in glide finalization.",
                        this._systemFriendlyName, glidingInfoBag.ClientDealRequest.Identity, glidingInfoBag.CurrentState, glidingInfoBag.TotalFilledSizeBasePol);
                }

                //closing procedures
                decimal sizeToBeCovered = Math.Abs(accepted
                    ? llAcquiredSizeBasePol + totalFilledBasePolLocal
                    : totalFilledBasePolLocal);

                if (sizeToBeCovered > 0)
                {
                    this.SendBankOrderWithoutDirectionCheck(sizeToBeCovered,
                        accepted ? deal.IntegratorDealDirection.ToOpositeDirection() : deal.IntegratorDealDirection,
                        deal.Symbol, this._settings.PreferLmaxDuringHedging,
                        this._systemFriendlyName + glideClosingOrderNamer +
                        Interlocked.Increment(ref this._headOrdersCnt),
                        glidingInfoBag);
                }
                else
                {
                    this.TryCloseGlide(glidingInfoBag);
                }
            }
            else
            {
                if (!InterlockedEx.CheckAndFlipState(ref glidingInfoBag.CurrentState,
                    GlidingInfoBag.STATE_WAITINGFORFINALGLIDE, GlidingInfoBag.STATE_DECISIONING))
                {
                    this._logger.Log(LogLevel.Fatal, "{0} glide {1} experienced glide in unexpected status ({2}) after end of decisioning - unable to close it correctly. position (base pol: {3}) MIGHT NEED TO BE FLATTED MANUALLY. HardBlocking",
                        this._systemFriendlyName, glidingInfoBag.ClientDealRequest.Identity, glidingInfoBag.CurrentState, glidingInfoBag.TotalFilledSizeBasePol);
                    this.HardBlock();
                    return;
                }

                //just for sure if deal arrived during decisioning
                this.PerformLateFlattingAfterLateOrderUpdateReceived(glidingInfoBag,
                    glidingInfoBag.OrderCurrent != null || glidingInfoBag.OrderNext != null);
            }

            this.SetLLEvaluationDone();
        }

        private void RejectDeal(IClientDealRequest dealRequest, RejectableMMDeal.DealRejectionReason rejectionReason)
        {
            AutoRejectableDeal deal = dealRequest as AutoRejectableDeal;
            if (deal != null)
                this.RejectSoftDeal(deal, (AutoRejectableDeal.RejectableTakerDealRejectionReason)(int)rejectionReason);
            else
            {
                this._logger.Log(LogLevel.Fatal, "{0} experienced object model mismatch. {1}", this._systemFriendlyName,
                    dealRequest);
                this.HardBlock();
            }
        }

        private bool AcceptDeal(IClientDealRequest dealRequest)
        {
            AutoRejectableDeal deal = dealRequest as AutoRejectableDeal;
            if (deal != null)
                return this.AcceptSoftDeal(deal);
            else
            {
                this._logger.Log(LogLevel.Fatal, "{0} experienced object model mismatch. {1}", this._systemFriendlyName,
                    dealRequest);
                this.HardBlock();
                return false;
            }
        }

        const string glideClosingOrderNamer = "_CLOSING_";

        private void ForceCancelAllOrdersOfASingleGlide(GlidingInfoBag glidingInfoBag)
        {
            this.CancelAllOrdersOfASingleGlide(glidingInfoBag, true);
        }

        private void CancelAllOrdersOfASingleGlide(GlidingInfoBag glidingInfoBag, bool forceCancelation)
        {
            if (Interlocked.Increment(ref glidingInfoBag.FinalCancellationAttemptsCnt) > 50)
            {
                this._logger.Log(LogLevel.Fatal, "{0} glide {1} experienced over 10 attempts to close all it's pending orders. Giving up now (postion base pol: {2}) and marking system as broken",
                    this._systemFriendlyName, glidingInfoBag.ClientDealRequest.Identity, glidingInfoBag.TotalFilledSizeBasePol);
                this.AllGlidingProcessingEvent -= glidingInfoBag.PerformActionOnActiveGlider;

                if(_enabled)
                    this.HardBlock();
                return;
            }

            //Cancel the next first, as it can 'disappear' under hands and move to current
            var orderNext = glidingInfoBag.OrderNext;
            if (forceCancelation || (orderNext != null && !orderNext.HasPendingCancellation))
                this.RequestOrderCancel(orderNext);
            var orderCurrent = glidingInfoBag.OrderCurrent;
            if (forceCancelation || (orderCurrent != null && !orderCurrent.HasPendingCancellation))
                this.RequestOrderCancel(orderCurrent);
        }

        private void CheckStaleGlide(GlidingInfoBag glidingInfoBag)
        {
            if (glidingInfoBag.CutoffTime < DateTime.UtcNow.AddSeconds(-5))
            {
                this._logger.Log(LogLevel.Fatal, "{0} has a glide ({1}) which was not dismissed for over 5 seconds. HardBlocking. Stale glides are reportet only once",
                    this._systemFriendlyName, glidingInfoBag.ClientDealRequest.Identity);
                this.CancelAllOrdersOfASingleGlide(glidingInfoBag, true);
                this.AllGlidingProcessingEvent -= glidingInfoBag.PerformActionOnActiveGlider;
                this.HardBlock();
            }
        }

        //TODO: should be allowed only for LMAX glide order
        protected override bool DirectCancelOfOrderCurrentAllowed { get { return true; } }

        protected virtual void ScheduleNextGlidingEvaluation(GlidingInfoBag glidingInfoBag, DateTime nextDueTime,
            bool invokedFromTimer)
        {
            if (glidingInfoBag.Timer != null)
                HighResolutionDateTime.HighResolutionTimerManager.RescheduleTimer(glidingInfoBag.Timer, nextDueTime,
                    invokedFromTimer);
            else
                glidingInfoBag.Timer =
                    HighResolutionDateTime.HighResolutionTimerManager.RegisterTimer(
                        () => this.ProcessGlidingEvent(glidingInfoBag, true), nextDueTime,
                        TimerTaskFlagUtils.WorkItemPriority.Highest,
                        TimerTaskFlagUtils.TimerPrecision.Highest, true, false);
        }

        protected virtual DateTime UtcNow
        {
            get { return HighResolutionDateTime.UtcNow; }
        }

        //After 
        // - timer expire
        // - order finalization (not needed after partial fill)
        // - re-enabling
        //
        // invokedFromTimer - if true - MUST reschedule, if false - MIGHT NEED to reschedule
        protected void ProcessGlidingEvent(GlidingInfoBag glidingInfoBag, bool invokedFromTimer)
        {
            DateTime now = this.UtcNow;

            if (now >= glidingInfoBag.CutoffTime - HighResolutionDateTime.ResolutionThreshold)
            {
                this.CancelAllOrdersOfASingleGlide(glidingInfoBag, false);

                if (now >= glidingInfoBag.CutoffTime + this._settings.GlidingCutoffSpan - HighResolutionDateTime.ResolutionThreshold)
                {
                    if ((glidingInfoBag.OrderCurrent != null || glidingInfoBag.OrderNext != null)
                        && now < glidingInfoBag.ClientDealRequest.MaxProcessingCutoffTime - Constansts.MinimumSpanBetweenLLandWatchdog - HighResolutionDateTime.ResolutionThreshold)
                    {
                        this._logger.Log(LogLevel.Error,
                            "{0} experiencing active order for glide {1} after cutoff time. Attempting to cancel those and postponing the LL decision to latest possible time",
                            this._systemFriendlyName, glidingInfoBag.ClientDealRequest.Identity);
                        this.CancelAllOrdersOfASingleGlide(glidingInfoBag, false);
                        this.ScheduleNextGlidingEvaluation(glidingInfoBag, glidingInfoBag.ClientDealRequest.MaxProcessingCutoffTime - Constansts.MinimumSpanBetweenLLandWatchdog, invokedFromTimer);
                    }
                    else
                    {
                        PerformFinalDecisioning(glidingInfoBag);
                    }
                }
                else
                {
                    this.ScheduleNextGlidingEvaluation(glidingInfoBag, glidingInfoBag.CutoffTime + this._settings.GlidingCutoffSpan, invokedFromTimer);
                }
                return;
            }

            //control if we have only order current - otherwise wait (and likely schedule )
            bool readyToProcess = this.IsReadyToProcessEvents();
            bool orderNextNonNull = glidingInfoBag.OrderNext != null;
            bool glidedWholeSize = Math.Abs(glidingInfoBag.TotalInTimeFilledSizeBasePol.ToDecimal()) ==
                                   glidingInfoBag.InitialGlidingSizeBaseAbs;
            if (!readyToProcess || orderNextNonNull || glidedWholeSize)
            {
                this._logger.Log(LogLevel.Info, "{0} (glide {1}) cannot perform next step (whole size glided: {2}, or some switch switched: {3}, or no response to Next: {4} ({5})) - waiting for new event or timeout",
                    this._systemFriendlyName, glidingInfoBag.ClientDealRequest.Identity, glidedWholeSize, !readyToProcess, orderNextNonNull, glidingInfoBag.OrderNext);
                bool hasPendingOrder = glidingInfoBag.OrderNext != null || glidingInfoBag.OrderCurrent != null;
                DateTime dueTime = glidingInfoBag.CutoffTime;
                if (!hasPendingOrder)
                    dueTime += this._settings.GlidingCutoffSpan;
                ScheduleNextGlidingEvaluation(glidingInfoBag, dueTime, invokedFromTimer);
                return;
            }

            lock (glidingInfoBag)
            {
                if (glidingInfoBag.OrderNext != null)
                {
                    ScheduleNextGlidingEvaluation(glidingInfoBag, glidingInfoBag.CutoffTime, invokedFromTimer);
                    return;
                }

                var orderCurrentlocal = glidingInfoBag.OrderCurrent;
                now = this.UtcNow;

                if (orderCurrentlocal != null &&
                    orderCurrentlocal.CreatedUtc + _settings.GlidePriceLife - HighResolutionDateTime.ResolutionThreshold > now)
                {
                    ScheduleNextGlidingEvaluation(glidingInfoBag,
                        orderCurrentlocal.CreatedUtc + _settings.GlidePriceLife, invokedFromTimer);
                    return;
                }

                int stepsEdgesCount = (int)((glidingInfoBag.CutoffTime - now).Ticks / this._settings.GlidePriceLife.Ticks);

                DateTime nextDueTime;
                decimal newPrice;
                //if there is no time for stepping then use the worse acceptable price
                if (stepsEdgesCount <= 1)
                {
                    nextDueTime = glidingInfoBag.CutoffTime;
                    newPrice = glidingInfoBag.WorstAcceptableFillPrice;

                    this._logger.Log(LogLevel.Info,
                        "{0} Performing last gliding step. Price: {1} NextDue: {2:HH:mm:ss.fffffff}",
                        this._systemFriendlyName, newPrice, nextDueTime);
                }
                else
                {
                    nextDueTime = now + this._settings.GlidePriceLife;

                    //min acceptable or last price or current price level on opposite side of book (!)
                    //We need to be careful that we are accumulating against the direction of the deal
                    // But also we try to get best possible price - so we start near the opposite side of book
                    decimal? currentLmaxToBPriceForClosingAccumulatedHedgingPosition =
                        CurrentLmaxToBPriceForClosingAccumulatedHedgingPosition;
                    decimal currentGlideMaxPriceLevel =
                        currentLmaxToBPriceForClosingAccumulatedHedgingPosition.HasValue
                            ? PriceCalculationUtils.DeterioratePrice(
                                currentLmaxToBPriceForClosingAccumulatedHedgingPosition.Value,
                                this._minimumGlidingPriceIncrement,
                                glidingInfoBag.ClientDealRequest.IntegratorDealDirection.ToOpositeDirection())
                            : glidingInfoBag.WorstAcceptableFillPrice;
                    decimal newPriceUnrounded;

                    if (glidingInfoBag.LastGlidePrice > 0)
                    {
                        //we can try to reevaluate too early - no change in that case
                        if (glidingInfoBag.StepsEdgesRemaining == stepsEdgesCount)
                            newPriceUnrounded = glidingInfoBag.LastGlidePrice;
                        else
                            //the step size is already polarized - so no need to care about polarization now
                            newPriceUnrounded = glidingInfoBag.LastGlidePrice -
                                                (glidingInfoBag.LastGlidePrice -
                                                 glidingInfoBag.WorstAcceptableFillPrice) /
                                                stepsEdgesCount;

                        newPriceUnrounded = PriceCalculationUtils.WorsePrice(newPriceUnrounded, currentGlideMaxPriceLevel,
                            glidingInfoBag.ClientDealRequest.IntegratorDealDirection.ToOpositeDirection());
                    }
                    else
                    {
                        newPriceUnrounded = currentGlideMaxPriceLevel;
                    }

                    newPriceUnrounded = PriceCalculationUtils.BetterPrice(glidingInfoBag.WorstAcceptableFillPrice,
                        newPriceUnrounded, glidingInfoBag.ClientDealRequest.IntegratorDealDirection.ToOpositeDirection());

                    //decimal newPrice;
                    //round to worse here
                    if (
                        !PriceCalculationUtilsEx.TryRoundToSupportedPriceIncrement(this._symbolsInfo,
                            glidingInfoBag.ClientDealRequest.IntegratorDealDirection.ToOpositeDirection(), false, this._symbol, TradingTargetType.LMAX,
                            newPriceUnrounded, out newPrice))
                    {
                        this._logger.Log(LogLevel.Fatal,
                            "{0} Could not correctly round the gliding price - so using the minimum acceptable price",
                            this._systemFriendlyName);
                        newPrice = glidingInfoBag.WorstAcceptableFillPrice;
                    }

                    //recheck with worse acceptable here
                    newPrice = PriceCalculationUtils.BetterPrice(glidingInfoBag.WorstAcceptableFillPrice, newPrice,
                        glidingInfoBag.ClientDealRequest.IntegratorDealDirection.ToOpositeDirection());

                    this._logger.Log(LogLevel.Info,
                        "{0} Performing next gliding step. Price: {1} (MaxLevel: {2}, unrounded: {3} (from tob: {6}), previous: {4}), NextDue: {5:HH:mm:ss.fffffff}",
                        this._systemFriendlyName, newPrice, currentGlideMaxPriceLevel, newPriceUnrounded,
                        glidingInfoBag.LastGlidePrice, nextDueTime, currentLmaxToBPriceForClosingAccumulatedHedgingPosition);
                }


                glidingInfoBag.LastGlidePrice = newPrice;
                glidingInfoBag.StepsEdgesRemaining = stepsEdgesCount;


                if (glidingInfoBag.OrderCurrent == null ||
                    glidingInfoBag.OrderCurrent.OrderRequestInfo.RequestedPrice != newPrice)
                {
                    decimal newSize;

                    //Since Cancel/Replace always maintain CumQty and LeavesQty
                    if (glidingInfoBag.OrderCurrent != null)
                    {
                        newSize = glidingInfoBag.OrderCurrent.SizeBaseAbsInitial;
                    }
                    else
                    {
                        newSize = glidingInfoBag.InitialGlidingSizeBaseAbs - Math.Abs(glidingInfoBag.TotalInTimeFilledSizeBasePol.ToDecimal());
                    }

                    if (newSize <= 0)
                    {
                        ScheduleNextGlidingEvaluation(glidingInfoBag,
                            glidingInfoBag.CutoffTime + this._settings.GlidingCutoffSpan, invokedFromTimer);
                        return;
                    }

                    var venueOri = this.CreateVenueGliderOrderRequest(newPrice,
                        glidingInfoBag.ClientDealRequest.IntegratorDealDirection.ToOpositeDirection(), glidingInfoBag.ClientDealRequest.Symbol,
                        newSize);

                    if (glidingInfoBag.OrderCurrent != null)
                    {
                        venueOri = this.CreateVenueGliderReplaceOrderRequest(glidingInfoBag.OrderCurrent.ClientOrderIdentity,
                            venueOri);
                    }

                    int currentOrderCnt = Interlocked.Increment(ref _headOrdersCnt);

                    //Try create and send order
                    glidingInfoBag.OrderNext =
                        this.CreateVenueGlidingOrder(
                            this._systemFriendlyName + "_" + glidingInfoBag.ClientDealRequest.Identity + "_" + currentOrderCnt,
                            this._systemIdentity, venueOri, this._integratedTradingSystemIdentification, glidingInfoBag);

                    var result = this.RegisterOrder(glidingInfoBag.OrderNext);
                    if (!result.RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Submitting Venue leg of {0} failed: {1}",
                            this._systemFriendlyName,
                            result.ErrorMessage);
                        glidingInfoBag.OrderNext = null;
                        nextDueTime = glidingInfoBag.CutoffTime;
                    }
                }
                else
                {
                    nextDueTime = DateTimeEx.Min(nextDueTime + this._settings.GlidePriceLife, glidingInfoBag.CutoffTime);
                }

                ScheduleNextGlidingEvaluation(glidingInfoBag, nextDueTime, invokedFromTimer);
            }
        }

        protected abstract VenueClientOrderRequestInfo CreateVenueGliderOrderRequest(decimal price,
            DealDirection dealDirection, Symbol symbol,
            decimal sizeBaseAbs);

        protected abstract VenueClientOrderRequestInfo CreateVenueGliderReplaceOrderRequest(
            string replacedOrderId, VenueClientOrderRequestInfo replacingOrderRequest);

        protected abstract GliderLmaxClientOrder CreateVenueGlidingOrder(string orderIdentity, string clientIdentity,
            VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification, GlidingInfoBag glidingInfoBag);

        protected override void AfterDisablingProcedure()
        {
            base.AfterDisablingProcedure();

            //stop all gliding
            if (AllGlidingProcessingEvent != null)
                AllGlidingProcessingEvent(this.ForceCancelAllOrdersOfASingleGlide);

            //Unsubscribe all gliding info bags??
        }

        protected override void RegularCheck(DateTime now)
        {
            base.RegularCheck(now);

            if (AllGlidingProcessingEvent != null)
                AllGlidingProcessingEvent(this.CheckStaleGlide);
        }

        private event Action<Action<GlidingInfoBag>> AllGlidingProcessingEvent;

        protected override void HandleIntegratorDealInternal(IntegratorDealInternal integratorDealInternal, AtomicSize currentPositionBasePol)
        {
            IntegratorGliderDealInternal gliderDeal = integratorDealInternal as IntegratorGliderDealInternal;

            if (gliderDeal == null || gliderDeal.GlidingInfoBag == null)
            {
                //This is it - confirmation of deal from cpt
                if (integratorDealInternal.SenderTradingTargetType == TradingTargetType.BankPool
                    // or ECN confirm of ll head deal or reject flatting order
                    || (integratorDealInternal.Counterparty == this._destinationVenueCounterparty
                        && (integratorDealInternal.ClientOrderIdentity.Contains("_ECNRejectFlatting_")
                            || integratorDealInternal.ClientOrderIdentity.Contains("_HEAD_"))
                        )
                    )
                {
                    //This is it
                    return;
                }
                else
                {
                    this._logger.Log(LogLevel.Fatal, "{0} Receiving deal of unexpected type, hard blocking system. {1}",
                        this._systemFriendlyName, integratorDealInternal);
                    this.HardBlock();
                    return;
                }
            }
            else
            {
                switch (gliderDeal.GlidingInfoBag.AddGliderDeal(gliderDeal, this._logger))
                {
                    case GlidingInfoBag.AddingDealResult.None:
                        break;
                    case GlidingInfoBag.AddingDealResult.GlideCanBeClosed:
                        this.TryCloseGlide(gliderDeal.GlidingInfoBag);
                        break;
                    case GlidingInfoBag.AddingDealResult.Unexpected:
                    default:
                        this._logger.Log(LogLevel.Error, "{0} experienced unexpected gliding deal",
                            this._systemFriendlyName);
                        this.HardBlock();
                        break;
                }
            }
        }

        private void TryCloseGlide(GlidingInfoBag gib)
        {
            if (gib.CloseCompletely())
            {
                this._logger.Log(LogLevel.Info, "{0} completely closing glide {1}, Acquired Pnl in term: {2}.", this._systemFriendlyName, gib.ClientDealRequest.Identity, -(gib.TotalInTimeFilledSizeTermPol.ToDecimal() + gib.TotalLateFilledSizeTermPol.ToDecimal()));
                this.AllGlidingProcessingEvent -= gib.PerformActionOnActiveGlider;

                //NOT USED IN TAKER MM
                //again stream normal sizes
                //this.AddAvailableSizeBack(gib.Deal.SizeBaseAbsInitial);
            }
            else
            {
                this._logger.Log(LogLevel.Fatal, "{0} attempted to repeatedly or too early close glide (in state {2}) {1}. (Since position went to zero multiple times). Ignoring now. Position should be watched", 
                    this._systemFriendlyName, gib.ClientDealRequest.Identity, gib.CurrentState);
                this.HardBlock();
            }
        }

        protected override void HandleClientOrderUpdateInfo(ClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            GliderClientOrderUpdateInfo gliderOrderInfo = clientOrderUpdateInfo as GliderClientOrderUpdateInfo;

            if (gliderOrderInfo != null)
            {
                bool shouldReprocessEvents =
                    this.HandleClientOrderUpdateInfoHelper(clientOrderUpdateInfo, gliderOrderInfo.GlidingInfoBag)
                    &&
                    //As for hotspot systems we put next to current already after flippping to OpenInIntegrator - which happens during sending
                    clientOrderUpdateInfo.OrderStatus != ClientOrderStatus.OpenInIntegrator;

                if (shouldReprocessEvents)
                {
                    this.ProcessGlidingEvent(gliderOrderInfo.GlidingInfoBag, false);
                }
            }
            else
            {
                base.HandleClientOrderUpdateInfo(clientOrderUpdateInfo);
            }
        }
    }
}
